#used to include googletest
macro( optilab_include_googletest )
    # add  gtests
    add_subdirectory("${PROJECT_SOURCE_DIR}/googletest")
    enable_testing()
    include_directories("${gtest_SOURCE_DIR}/include"  "${gtest_SOURCE_DIR}")
    include_directories("${gmock_SOURCE_DIR}/include"  "${gmock_SOURCE_DIR}")
endmacro( optilab_include_googletest )

#creates list of files, scanning subdirectories
#starting from path
#files string must end with endmask
#all files containing blackmask will be ignored
#all files contained in blacklist will be ignored
macro(do_file_list ext_list path endmask blackmask blacklist)
    set(inFiles "")
    FILE(GLOB_RECURSE inFiles RELATIVE "${path}"
        "${path}/*${endmask}")

    set(blck "${blacklist}")
    FOREACH(infileName ${inFiles})
        if (NOT ${infileName} IN_LIST blck)
            if ("${blackmask}" STREQUAL "")
                set(${ext_list} ${${ext_list}} "${path}/${infileName}")
            else()
                #excluding every possible blackmask
                SET( valid 1 )
                FOREACH(blackmask_iteration ${blackmask})
                    if(${infileName} MATCHES "${blackmask_iteration}")
                        SET( valid 0 )
                    endif()
                ENDFOREACH(blackmask_iteration)
                if(valid EQUAL 1)
                    set(${ext_list} ${${ext_list}} "${path}/${infileName}")
                endif() #if 0 string is not included in results
            endif()
        endif()
    ENDFOREACH(infileName)
endmacro()

MACRO(subdirlist result curdir)
    FILE(GLOB_RECURSE children LIST_DIRECTORIES true RELATIVE ${curdir} ${curdir}/*)
    SET(dirlist "")
    FOREACH(child ${children})
        IF(IS_DIRECTORY ${curdir}/${child})
            LIST(APPEND dirlist ${curdir}/${child})
        ENDIF()
    ENDFOREACH()
    SET(${result} ${dirlist})
ENDMACRO()
