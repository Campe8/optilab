if( UNIX )
	if( NOT DEPS_ALREADY_THERE )
		set( LSB_RELEASED_ID_SHORT "")
		find_program(LSB_RELEASE lsb_release)
		execute_process(COMMAND ${LSB_RELEASE} -is
		OUTPUT_VARIABLE LSB_RELEASE_ID_SHORT
		OUTPUT_STRIP_TRAILING_WHITESPACE
		)
		message( "DEPS: Unix/Linux OS detected: ${LSB_RELEASE_ID_SHORT}" )
		message( "DEPS: Attempting automatic binary dependencies installation" )
		
		if( LSB_RELEASE_ID_SHORT MATCHES "Ubuntu" )
			execute_process(COMMAND sh "${PROJECT_SOURCE_DIR}/cmake/dependencies/Ubuntu.sh"
			)
		endif()
		set(DEPS_ALREADY_THERE TRUE CACHE BOOL "Have the dependencies already been installed?" FORCE)
	else()
		message( "DEPS: Skipping automatic binary dependencies installation" )
	endif()
endif()
