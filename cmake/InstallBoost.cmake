# For some external project macros
include(ExternalProject)

# Correct linking for boost windows 
IF(CMAKE_BUILD_TYPE MATCHES Debug)
  set(BOOST_BUILD_TYPE "")
  set(BOOST_LIB_SUFFIX "mt-gd")
else()
  set(BOOST_BUILD_TYPE "release")
  set(BOOST_LIB_SUFFIX "mt")
ENDIF(CMAKE_BUILD_TYPE MATCHES Debug)

set( Boost_Bootstrap_Command )
if( UNIX )
  set( Boost_Configure_Command ./bootstrap.sh --with-libraries=filesystem,system,regex,program_options toolset=gcc variant=debug link=static install --prefix=${CMAKE_CURRENT_BINARY_DIR}/boostinstall)
  set( Boost_Build_Command ./b2 )
else()
  if( WIN32 )
    set( Boost_Configure_Command bootstrap.bat )
    # removed: link=static runtime-link=static
    set( Boost_Build_Command b2.exe -j9 --with-filesystem --with-system --with-regex --with-program_options ${BOOST_BUILD_TYPE} --prefix=${CMAKE_CURRENT_BINARY_DIR}/boostinstall)
  endif()
endif()

ExternalProject_Add(
    customboost
    PREFIX ${CMAKE_CURRENT_BINARY_DIR}/customboost
    URL "http://dl.bintray.com/boostorg/release/1.66.0/source/boost_1_66_0.tar.gz"
    CONFIGURE_COMMAND ${Boost_Configure_Command}
    BUILD_COMMAND ${Boost_Build_Command}
    BUILD_IN_SOURCE 1
    INSTALL_COMMAND ""
)



#set(BOOST_INCLUDE_DIRS ${CMAKE_CURRENT_BINARY_DIR}/customboost/include)
set(BOOST_REGEX_LIBRARY ${CMAKE_CURRENT_BINARY_DIR}/customboost/src/customboost/stage/lib/libboost_regex.a)
set(BOOST_SYSTEM_LIBRARY ${CMAKE_CURRENT_BINARY_DIR}/customboost/src/customboost/stage/lib/libboost_system.a)
set(BOOST_FILESYSTEM_LIBRARY ${CMAKE_CURRENT_BINARY_DIR}/customboost/src/customboost/stage/lib/libboost_filesystem.a)
set(BOOST_PROGRAM_OPTIONS_LIBRARY ${CMAKE_CURRENT_BINARY_DIR}/customboost/src/customboost/stage/lib/libboost_program_options.a)

if( WIN32 )
  #set(BOOST_INCLUDE_DIRS ${CMAKE_CURRENT_BINARY_DIR}/customboost/include)
  set(BOOST_REGEX_LIBRARY ${CMAKE_CURRENT_BINARY_DIR}/customboost/src/customboost/stage/lib/libboost_regex-vc141-${BOOST_LIB_SUFFIX}-x64-1_66.lib)
  set(BOOST_SYSTEM_LIBRARY ${CMAKE_CURRENT_BINARY_DIR}/customboost/src/customboost/stage/lib/libboost_system-vc141-${BOOST_LIB_SUFFIX}-x64-1_66.lib)
  set(BOOST_FILESYSTEM_LIBRARY ${CMAKE_CURRENT_BINARY_DIR}/customboost/src/customboost/stage/lib/libboost_filesystem-vc141-${BOOST_LIB_SUFFIX}-x64-1_66.lib)
  set(BOOST_PROGRAM_OPTIONS_LIBRARY ${CMAKE_CURRENT_BINARY_DIR}/customboost/src/customboost/stage/lib/libboost_program_options-vc141-${BOOST_LIB_SUFFIX}-x64-1_66.lib)
endif()