# Platform specific includes
include_directories( "${PROJECT_SOURCE_DIR}/windows/flexbison" )
# Editline/Readline support
include_directories( "${PROJECT_SOURCE_DIR}/windows/readline" )

if(MSVC)
    add_compile_options(-bigobj)

    # needed to directly link dll/lib libraries
    SET(CMAKE_FIND_LIBRARY_PREFIXES "")
    SET(CMAKE_FIND_LIBRARY_SUFFIXES ".lib" ".LIB")

    set(CMAKE_INSTALL_PREFIX "${PROJECT_SOURCE_DIR}/install/OptiLab")

    find_library(Readline_LIBRARY edit_a HINTS "${CMAKE_CURRENT_SOURCE_DIR}/lib/windows/${CMAKE_BUILD_TYPE}")

    # addresses error LNK2038
    # when linking under windows to avoid
    # value 'MTd_StaticDebug' doesn't match value 'MDd_DynamicDebug'
    # error
    set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

    # Build only release when building on windows
    # set(CMAKE_BUILD_TYPE Release)


    # Setting preprocessor platform flag
    add_compile_definitions(IS_WINDOWS)

    # Force usage of static libs
    set(Boost_USE_STATIC_LIBS   ON)

    # Disable boost auto-linking
    # preventing extra unused libs to be wrongly linked
    add_definitions("-DBOOST_ALL_NO_LIB")

    set(BOOST_CONFIG_SUPPRESS_OUTDATED_MESSAGE TRUE)

    set(Windows_linker_force_multiple "-FORCE:multiple")

    # Multithread compilation
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /MP8 /W0 /O2")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} /MP8 /W0 /O2")

    set(CMAKE_CXX_FLAGS_RELEASE "/O2")
endif()
