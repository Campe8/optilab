# Finding readline external library
find_package(Readline)

# Setting preprocessor platform flag
add_compile_definitions(IS_LINUX)

set(DL_INCLUDE "dl")

# Set empty
set(Windows_linker_force_multiple "")

# Release compiler optimizions
set(CMAKE_CXX_FLAGS_RELEASE "-O3")