#linking statically libstdc++ is needed by amazonlinux docker container
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static-libstdc++")

#copying configs into build directory
file(COPY "${PROJECT_SOURCE_DIR}/resources" DESTINATION "${PROJECT_SOURCE_DIR}/build")
