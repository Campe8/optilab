set(CPACK_PACKAGE_NAME "optilab")
set(CPACK_PACKAGE_VENDOR "Optilab Team")
set(CPACK_VERSION_MAJOR 0)
set(CPACK_VERSION_MINOR 1)
set(CPACK_VERSION_PATCH 0)
set(CPACK_PACKAGE_DESCRIPTION_FILE "${PROJECT_SOURCE_DIR}/install/description.txt")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "OptiLab, an end-to-end framework for combinatorial problems.")
set(CPACK_PACKAGE_FILE_NAME ${CPACK_PACKAGE_NAME}-${CPACK_VERSION_MAJOR}.${CPACK_VERSION_MINOR}.${CPACK_VERSION_PATCH} )
#set(CPACK_PACKAGE_ICON "") #A branding image that will be displayed inside the installer (used by GUI installers).
set(CPACK_RESOURCE_FILE_LICENSE ${PROJECT_SOURCE_DIR}/install/resource_license.txt)
set(CPACK_RESOURCE_FILE_README ${PROJECT_SOURCE_DIR}/install/readme.txt)
set(CPACK_RESOURCE_FILE_WELCOME ${PROJECT_SOURCE_DIR}/install/welcome.txt)
if(WIN32 AND NOT UNIX)
  set(CMAKE_INSTALL_UCRT_LIBRARIES TRUE)
  include(InstallRequiredSystemLibraries)
  # There is a bug in NSI that does not handle full unix paths properly. Make
  # sure there is at least one set of four (4) backslashes.
  #set(CPACK_PACKAGE_ICON "${CMAKE_SOURCE_DIR}/Utilities/Release\\\\InstallIcon.bmp")
  set(CPACK_PACKAGE_EXECUTABLES
    "OptiLab.exe" "Optilab Optimization Laboratory"
  )
  set(CPACK_CREATE_DESKTOP_LINKS
    "OptiLab.exe"
  )
  set(CPACK_NSIS_INSTALLED_ICON_NAME "bin\\\\OptiLab.exe")
  set(CPACK_NSIS_DISPLAY_NAME "OptiLab")
  set(CPACK_NSIS_HELP_LINK "http:\\\\\\\\www.optilabtech.com")
  set(CPACK_NSIS_URL_INFO_ABOUT "http:\\\\\\\\www.optilabtech.com")
  set(CPACK_NSIS_CONTACT "info@optilabtech.com")
  set(CPACK_NSIS_EXECUTABLES_DIRECTORY ".")
  set(CPACK_NSIS_MODIFY_PATH ON)
  set(CPACK_NSIS_EXTRA_INSTALL_COMMANDS "
      CreateShortCut \\\"$DESKTOP\\\\OptiLab.lnk\\\" \\\"$INSTDIR\\\\install\\\\OptiLab.exe\\\"
  ")
  set(CPACK_NSIS_EXTRA_UNINSTALL_COMMANDS "
      Delete \\\"$DESKTOP\\\\OptiLab.lnk\\\"
  ")
endif()
if(UNIX AND NOT WIN32)
  set(CPACK_GENERATOR "DEB")
  set(CPACK_DEBIAN_PACKAGE_MAINTAINER "Claudio Desideri") #required
  if(CPACK_GENERATOR MATCHES "DEB")
      set(CPACK_PACKAGING_INSTALL_PREFIX "/opt")
  elseif(CPACK_GENERATOR MATCHES "TGZ")
      set(CPACK_PACKAGING_INSTALL_PREFIX "")
  endif()
endif()

set(CPACK_PACKAGE_EXECUTABLES "OptiLab" "OptiLab Optimization Laboratory")

include(CPack)
