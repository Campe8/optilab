On some architectures and in particular on Mac OsX
in order to link some dynamically linked libs like boost regex
the system requires you to change the path of the lib in order
to make it visibile to the linker (the name of the path is not 
global by default).
In order to do so:
1 - cd into OptiLab/lib
2 - run
    install_name_tool -id "<global path>/OptiLab/lib/libboost_regex.dylib" libboost_regex.dylib
    where <global path> is the path to the OptiLab folder, e.g., "/Users/federico".

The libraries are:
- boost:
  - regex
  - filesystem
  - program_options
- OptiLab:
  - parser
- Google:
  -gtest
