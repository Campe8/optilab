const exec = require('child_process').exec;
const crypto = require("crypto");
const fs = require('fs');

process.env['PATH'] = process.env['PATH'] + ':' + process.env['LAMBDA_TASK_ROOT'];

exports.handler = function(event, context, callback) {
  
    // Set the event command, i.e., running OptiLab with the option '-f'
    event.cmd = './bin/Solver -f';
    if (!event.cmd) {
      return callback('Please specify a command to run as event.cmd');
    }
    
    // Generate unique ID for the input model
    var fileID = crypto.randomBytes(3*4).toString('base64');
    fileID += '.opt';
    console.log(fileID);
    
    // Save the file with the model in tmp
    var tmpDirFile = '/tmp/' + fileID;
    fs.writeFile(tmpDirFile, event.model, function(err) {
        if(err) {
            return console.log(err);
        }
        console.log("The file is saved in tmp");
    });
    
    // Set the command input
    event.cmd += ' ' + tmpDirFile;
    const child = exec(event.cmd, (error, stdout, stderr) => {
        if (error instanceof Error) {
            // Resolve with result of process
            callback(error, 'Error in running OptiLab');
        }
        callback(null, stdout);
        
    });

    // Log process stdout and stderr
    child.stdout.on('data', console.log);
    child.stderr.on('data', console.error);
};
