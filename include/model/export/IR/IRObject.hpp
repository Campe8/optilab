//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/11/2017
//
// Base object for the IR Tree.
// It represents a node/object in the tree.
//

#pragma once

#include "IRExportDefs.hpp"

#include "IRDefs.hpp"

/**
 * High level structure of IR Objects:
 *
 *                             +--------+
 *                             | Object |
 *                             +--------+
 *                                 ^
 *                                 |
 *           +-----------------------------------------+
 *           ^           ^         ^         ^         ^
 *           |           |         |         |         |
 *       +--------+ +---------+ +------+ +--------+ +------+
 *  +--<>| Module | | Context | | Node | | Symbol | | Type |
 *  |    +--------+ +---------+ +------+ +--------+ +------+
 *  |    +-------+                  ^
 *  +----| Block |<>--+             |
 *       +-------+    |   +---------------------------------------------+
 *       +-------+    |   ^         ^           ^           ^           ^
 *       | Stmt  |----+   |         |           |           |           |
 *       +-------+      +------+ +---------+ +---------+ +---------+ +---------+
 *                      | Expr | | DomDecl | | ConDecl | | SrcDecl | | VarDecl |
 *                      +------+ +---------+ +---------+ +---------+ +---------+
 */

// Forward declarations
namespace IR {
  class IRVisitor;
}// end namespace IR

namespace IR {
  
  class IR_EXPORT_CLASS IRObject {
  public:
    enum ObjectMark : char {
        VISITED = 0
      , DEFAULT
    };
    
  public:
    virtual ~IRObject() = default;
    
    inline IRObjectType type() const { return pObjectType; }
    
    /// Marks object
    inline void markObject(ObjectMark aMark) { pMark = aMark; }
    
    /// Returns the mark of the object
    inline ObjectMark getMark() const { return pMark; }
    
    /// Visitor accept
    virtual void acceptVisitor(IRVisitor* aVisitor) = 0;
    
  protected:
    IRObject(IRObjectType aType);
   
  private:
    /// Type of the object
    IRObjectType pObjectType;
    
    /// Mark of the object
    ObjectMark pMark;
  };
  
}// end namespace IR
