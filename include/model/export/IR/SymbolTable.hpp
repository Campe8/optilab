//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/16/2017
//
// The SymbolTable is a map that is "specialized" for handling Symbol keys,
// and Symbol entries. It automatically updates parent links and symbol tables
// upon changes on symbols.
// When entries are inserted into and removed from this table,
// the associated symbol table will be automatically updated.
// Similarly, parent links get updated automatically.
// For more information about Symbol Tables,
// see https://en.wikipedia.org/wiki/Symbol_table.
//

#pragma once

#include "IRExportDefs.hpp"

#include "SymbolTableEntry.hpp"

#include <unordered_map>
#include <memory>

namespace IR {
  
  class IR_EXPORT_CLASS SymbolTable {
  public:
    typedef std::shared_ptr<SymbolTable> SymbTablePtr;
    typedef const SymbTablePtr& SymbTablePtrConstRef;
    typedef SymbolTableEntry::SymbolTableEntryKey SymbolKey;
    typedef std::shared_ptr<SymbolTableEntry> SymbolTableEntrySPtr;
    typedef std::unordered_map<SymbolKey, SymbolTableEntrySPtr> SymbolTableMap;
    typedef SymbolTableMap::iterator SymbTableIter;
    typedef SymbolTableMap::const_iterator SymbTableCIter;
    
  public:
    SymbolTable(SymbTablePtrConstRef aParent=nullptr);
    
    ~SymbolTable() = default;
    
    inline void setParent(SymbTablePtrConstRef aParent)
    {
      pParent = aParent;
    }
    
    inline SymbTablePtr getParent() const
    {
      return pParent;
    }
    
    /// Returns true if this is the global symbol table,
    /// false otherwise
    inline bool isGlobal() const
    {
      return pParent == nullptr;
    }
    
    /// Returns true if the table contains an element
    /// with key "aKey", false otherwise
    bool contains(SymbolKey aKey);
    
    /// Returns an iterator to the entry with key "key"
    /// or iterator end() if element is not present in the table
    SymbTableIter find(SymbolKey aKey);
    SymbTableCIter find(SymbolKey aKey) const;
    
    /// Returns the entry with the given symbol name or nullptr if no such
    /// entry exists
    SymbolTableEntrySPtr lookup(const IRSymbol::SymbolName& aSymbName);
    
    /// Inserts "aEntry" in the symbol table and returns true
    /// if such element is inserted, false if an element
    /// with the same key was already present in the table.
    /// In the later case, no element is inserted in the table
    bool insert(const SymbolTableEntrySPtr& aEntry);
    
    /// Removes the entry pointed by iterator "aIt"
    void erase(SymbTableCIter aIt);
    
    /// Removes the entry with key "aKey" and
    /// returns true on success.
    /// If no such entry exists in the table,
    /// returns false
    bool erase(SymbolKey aKey);
    
    /// Range-based for loop - begin
    SymbTableIter begin();
    SymbTableCIter begin() const;
    
    /// Range-based for loop - end
    SymbTableIter end();
    SymbTableCIter end() const;
    
  private:
    /// Pointer to the parent symbol table
    SymbTablePtr pParent;
    
    /// Data structure implementing the actual symbol table
    SymbolTableMap pSymbTable;
  };
  
}// end namespace IR

