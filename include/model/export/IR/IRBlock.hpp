//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/15/2017
//
// A Block is a container of statements.
// In this context, it represents the
// lexical, syntactical and semantical structure of model
// which is grouped together and lives in the scope owning the block.
// In the C family blocks are delimited by curly braces - "{" and "}".
// For example:
// {
//		x:1..10;	// stmt1;
//		y:1..2;		// stmt2;
//		z:2..5;		// stmt3;
// }
// is a block.
//
// @TODO:
// A Block can also contain "regions".
// An IRRegion is a set of statements that are
// linked together and can contain other sub-regions.
// Each region as an "entry" node.
// For example:
// if (expr) do
//	Region_1
// else
//	Region_2
// endif
// is represented by the following region block:
//					 expr -> ExprNode - Region's entry node
//            +
//            |
//     +-------------+
//     |             |
//  Region_1      Region_2
//
// For more information about regions,
// see "A Simple Graph-Based Intermediate Representation"
// by Cliff Click and Michael Paleczny,
// http://www.oracle.com/technetwork/java/javase/tech/c2-ir95-150110.pdf
//

#pragma once

// Nodes of the scope
#include "IRNode.hpp"

// Tree structure
#include "Tree.hpp"

#include <list>
#include <memory>

namespace DSTree = DataStructure::Graph::Tree;

// Forward declarations
namespace IR {
  class IRModule;
}//end namespace IR

namespace IR {
  class IRModule;
  
  class IR_EXPORT_CLASS IRBlock {
  public:
    /// A statement is a tree-structure of IRNodes
    typedef DSTree::NTree<IRNode::IRStmtNode> IRStmt;
    typedef std::shared_ptr<IRStmt> IRStmtSPtr;
    typedef std::list<IRStmtSPtr> StmtList;
    typedef StmtList::iterator StmtListIter;
    typedef StmtList::const_iterator StmtListCIter;
    typedef std::vector<IRNode::IRNodeSPtr> VectorNode;
    
  public:
    IRBlock();
    
    ~IRBlock() = default;
    
    /// Returns a pointer to the stmt containing the given stmt node
    static IRStmtSPtr Stmt(const IRNode::IRStmtNodeSPtr& aStmtNode);
    
    /// Returns true if this is an empty block,
    /// false otherwise
    bool isEmpty() const;
    
    inline void setModule(const std::shared_ptr<IRModule>& aModule)
    {
      pModule = aModule;
    }
    
    /// Returns the module this block belongs to
    inline std::shared_ptr<IRModule> getModule() const
    {
      return pModule;
    }
    
    /// Returns true if this block contains "aStmt",
    /// false otherwise
    bool contains(const IRStmtSPtr& aStmt) const;
    
    /// @Todo implement
    /// void appendRegion(...);
    /// void removeRegion(...);
    
    /// Appends a stmt to the block
    void appendStmt(const IRStmtSPtr& aStmt);
    
    /// Removes a stmt from the block
    void removeStmt(const IRStmtSPtr& aStmt);
    void removeStmt(StmtListCIter& aStmtIter);
    
    /// Returns an iterator to the stmt in this block
    /// containing the node pointed by "aNode".
    /// Returns end() if no such stmt exists
    StmtListIter find(const IRNode::IRNodeSPtr& aNode);
    StmtListCIter find(const IRNode::IRNodeSPtr& aNode) const;
    
    /// Returns an iterator to the stmt in this block.
    /// Returns end() if no such stmt exists
    StmtListIter find(const IRStmtSPtr& aStmt);
    StmtListCIter find(const IRStmtSPtr& aStmt) const;
    
    /// Range-based for loop - begin
    StmtListIter begin();
    StmtListCIter begin() const;
    
    /// Range-based for loop - end
    StmtListIter end();
    StmtListCIter end() const;
    
    /// Returns the ordered vector of block nodes sorted
    /// according to the pre-order visit of this block.
    /// @note stmts and regions are visited sequentially
    /// as they have been inserted.
    /// @note returns the visit of (shared_ptr) objects of type T.
    /// @note T must implement isa
    template<typename T>
    std::vector<std::shared_ptr<T>> preOrderVisit() const
    {
      std::vector<std::shared_ptr<T>> visit;
      if (isEmpty())
      {
        return visit;
      }
      
      for (auto it = begin(); it != end(); ++it)
      {
        auto stmtVisit = preOrderVisit<T>(it);
        visit.insert(visit.end(), stmtVisit.begin(), stmtVisit.end());
      }
      
      return visit;
    }//preOrderVisit
    
    /// Returns the pre-order visit of the statement pointed by "aIter".
    /// Returns an empty visit for non valid iterators.
    /// @note returns the visit of (shared_ptr) objects of type T.
    /// @note T must implement isa
    template<typename T>
    std::vector<std::shared_ptr<T>> preOrderVisit(StmtListCIter& aIter) const
    {
      std::vector<std::shared_ptr<T>> visit;
      if (aIter == end())
      {
        return visit;
      }
      
      auto stmt = *aIter;
      assert(stmt);
      
      auto it = stmt->preOrderBegin();
      for (; it != stmt->preOrderEnd(); ++it)
      {
        assert((*it).data());
        if(T::isa((*it).data().get()))
        {
          visit.push_back(std::static_pointer_cast<T>((*it).data()));
        }
      }
      
      return visit;
    }//preOrderVisit
    
  private:
    /// Block instance
    std::shared_ptr<StmtList> pStmtList;
    
    /// Module this block belongs to
    std::shared_ptr<IRModule> pModule;
    
    /// Clears current block, leaving it empty
    void clear();
  };
  
}// end namespace IR
