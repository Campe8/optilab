//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/19/2017
//
// IRDomDecl is a node of the IR (tree) that
// represents domain declarations.
// For example, the statement:
// x:10..11;
// has "10..11" encoded as a domain declaration.
//



#pragma once

#include "IRNode.hpp"

#include "IRExprConst.hpp"

#include <vector>

/*
 * The IRDomDecl hierarchy is as follows:
 *
 *									IRDomDecl
 *                      +
 *                      |
 *            +-------------------+
 *            |                   |
 *     IRDomDeclBounds       IRDomDeclExt
 *         [x, y]             {x, y, z}
 */

namespace IR {
  
  class IR_EXPORT_CLASS IRDomDecl : public IRNode {
  private:
    using DomainConst = std::shared_ptr<IRExprConst>;
    
  public:
    enum DomDeclType : short {
        DOM_DECL_BOUNDS = 0
      , DOM_DECL_EXTENSIONAL
    };
    
  public:
    typedef std::shared_ptr<IRExprConst> ExprConstSPtr;
    
  public:
    virtual ~IRDomDecl() = default;
    
    static bool isa(const IRNode* aNode)
    {
      return aNode != nullptr &&
      aNode->getNodeType() == IRNodeType::IR_NODE_DOM_DECL;
    }
    
    static IRDomDecl* cast(IRNode* aNode)
    {
      if (!isa(aNode)) return nullptr;
      return static_cast<IRDomDecl*>(aNode);
    }
    
    static const IRDomDecl* cast(const IRNode* aNode)
    {
      if (!isa(aNode)) return nullptr;
      return static_cast<const IRDomDecl*>(aNode);
    }
    
    inline DomDeclType getDeclarationType() const
    {
      return pDeclType;
    }
    
    const inline std::shared_ptr<IRType> getDomainType() const
    {
      return getConstType();
    }
    
  protected:
    IRDomDecl(const std::shared_ptr<IRBlock>& aParentBlock, DomDeclType aDomDeclType);
    
    /// Removes all the stored (domain) constants
    void clearConstants();
    
    /// Returns the number of constants in this domain
    std::size_t getNumConstants() const;
    
    /// Returns the DomainConst at position "aIdx".
    /// @note "aIdx" is 0-based
    DomainConst getConstant(std::size_t aIdx) const;
    
    /// Adds a domain element represented as a constant expr.
    /// @note all constants must have the same type
    void addConstant(const DomainConst& aExprConst);
    
    /// Returns the base type of the constants representing the domain
    inline std::shared_ptr<IRType> getConstType() const
    {
      return pType;
    }
    
  private:
    DomDeclType pDeclType;
    
    /// Type of the domain elements
    std::shared_ptr<IRType> pType;
  };
  
}// end namespace IR
