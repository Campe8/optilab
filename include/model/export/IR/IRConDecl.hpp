//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/14/2017
//
// IRConDecl is a node of the IR (tree) that
// represents constraint declarations.
// For example, the statement:
// lq(x, y)
// is encoded as a constraint declaration.
// Similar to a fcn call, the name of the constraint is "lq",
// while its argument list is "[x, y]".
//


#pragma once

#include "IRNode.hpp"

#include "IRExprFcn.hpp"
#include "IRDomDecl.hpp"

namespace IR {
  
  class IR_EXPORT_CLASS IRConDecl : public IRNode {
  public:
    enum PropagationType : int {
        PT_BOUNDS = 0
      , PT_DOMAIN
    };
    
  public:
    static bool isa(const IRNode* aNode)
    {
      return aNode != nullptr && aNode->getNodeType() == IRNodeType::IR_NODE_CON_DECL;
    }
    
    static IRConDecl* cast(IRNode* aNode)
    {
      if (!isa(aNode)) return nullptr;
      return static_cast<IRConDecl*>(aNode);
    }
    
    static const IRConDecl* cast(const IRNode* aNode)
    {
      if (!isa(aNode)) return nullptr;
      return static_cast<const IRConDecl*>(aNode);
    }
    
    /// Sets fcn call expression representing the constraint call
    void setFcnCon(const std::shared_ptr<IRExprFcn>& aFcn);
    
    /// Returns the ExprFcn for this constraint
    std::shared_ptr<IRExprFcn> fcnCon() const;
    
    inline void setPropagationType(PropagationType aPropType)
    {
      pPropType = aPropType;
    }
    
    inline PropagationType getPropagationType() const
    {
      return pPropType;
    }
    
    inline void setID(const std::string& aID)
    {
      pID = aID;
    }
    
    inline std::string getID() const
    {
      return pID;
    }
    
    void acceptVisitor(IRVisitor* aVisitor) override;
    
  protected:
    friend class IRConstructionFacade;
    
    IRConDecl(const std::shared_ptr<IRBlock>& aParentBlock);
    
  private:
    /// Propagation strategy type
    PropagationType pPropType;
    
    /// Context ID
    std::string pID;
  };
  
}// end namespace IR
