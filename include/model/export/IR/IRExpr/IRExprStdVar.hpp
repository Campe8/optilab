//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 08/11/2017
//
// IRExprStdVar is a node of the IR (tree) that holds
// SymbolVar objects and represents a standard variable.
// @note ExprDecVar are leaf expressions.
// @note This should be enanched with a symbol address
// representing the address to the memory location
// containing the actual data.
// The size of the allocated memory is sizeof(type),
// where "type" is the type of this IRExprStdVar.
// The symbol address is a symbol const and the
// address is an ExprConst.
// The address points to the location in the memory pool
// of the given module.
// If the symbol table acts as a memory pool, the address
// could simply be the name of the (unique) symbol in the
// symbol table.
//


#pragma once

#include "IRExpr.hpp"

// Symbol variable contained in this node
#include "IRSymbolVar.hpp"

#include <memory>
#include <cassert>

namespace IR {
  
  class MODEL_EXPORT_CLASS IRExprStdVar : public IRExpr {
  public:
    using SymbVarSPtr = std::shared_ptr<IRSymbolVar>;
    using SymbVarSPtrRef = std::shared_ptr<IRSymbolVar>&;
    using SymbVarSPtrCRef = const std::shared_ptr<IRSymbolVar>&;
    
  public:
    static bool isa(const IRNode* aNode);
    static IRExprStdVar* cast(IRNode* aNode);
    static const IRExprStdVar* cast(const IRNode* aNode);
    
    /// Set type w.r.t. the type of the symbol variable
    void setType(const std::shared_ptr<IRType>& aType) override;
    
    /// Sets the symbol var for this node
    void setVar(SymbVarSPtrCRef aVar);
    
    /// Returns the symbol var in this node
    SymbVarSPtr getVar() const;
    
    void acceptVisitor(IRVisitor* aVisitor) override;
    
  protected:
    friend class IRConstructionFacade;
    
    IRExprStdVar(const std::shared_ptr<IRBlock>& aParentBlock);
    
    bool isSemanticallyEqual(const IRExpr* aExpr) const override;
  };
  
}// end namespace IR
