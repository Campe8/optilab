//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/11/2017
//
// IRExprConst is a node of the IR (tree) that holds
// SymbolConst objects.
// @note ExprConst are leaf expressions.
//

#pragma once

#include "IRExpr.hpp"

// Symbol variable contained in this node
#include "IRSymbolConst.hpp"

#include <memory>
#include <cassert>

namespace IR {
  
  class MODEL_EXPORT_CLASS IRExprConst : public IRExpr {
	public:
		typedef std::shared_ptr<IRSymbolConst> SymbConstSPtr;
		typedef std::shared_ptr<IRSymbolConst>& SymbConstSPtrRef;
		typedef const std::shared_ptr<IRSymbolConst>& SymbConstSPtrCRef;

  public:
    virtual ~IRExprConst() = default;
    
    static bool isa(const IRNode* aNode);
    static IRExprConst* cast(IRNode* aNode);
    static const IRExprConst* cast(const IRNode* aNode);

		/// Sets the symbol const for this node
		void setConst(SymbConstSPtrCRef aConst);

		/// Returns the symbol const in this node
		SymbConstSPtr getConst() const;
    
    void acceptVisitor(IRVisitor* aVisitor) override;
    
  protected:
    friend class IRConstructionFacade;
    
		IRExprConst(const std::shared_ptr<IRBlock>& aParentBlock);
    
    bool isSemanticallyEqual(const IRExpr* aExpr) const override;
  };
  
}// end namespace IR
