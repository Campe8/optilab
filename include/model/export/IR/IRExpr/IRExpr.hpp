//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/11/2017
//
// IRExpr is a node of the IR (tree) that
// represents expressions.
//


#pragma once

#include "IRNode.hpp"

// Type of this expression
#include "IRType.hpp"

// IR Symbols
#include "IRSymbol.hpp"
#include "SymbolTable.hpp"

#include <memory>
#include <cassert>

namespace IR {
  
  class MODEL_EXPORT_CLASS IRExpr : public IRNode {
	public:
    virtual ~IRExpr() = default;

    static bool isa(const IRNode* aNode);
    static IRExpr* cast(IRNode* aNode);
    static const IRExpr* cast(const IRNode* aNode);

    /// Sets the inner type of this expression
    virtual void setType(const std::shared_ptr<IRType>& aType);

		/// Returns the inner type for this expression
    inline std::shared_ptr<IRType> getType() const { return pType; }

		/// Returns the type that identifies this expression
		inline IRExprType getExprType() const { return pExprType; }
    
    /// Returns true if the given expression is semantically equal
    /// to this expression.
    /// @note this is not a pointer comparison
    bool isEqual(const IRExpr* aExpr) const;
    
  protected:
		IRExpr(const std::shared_ptr<IRBlock>& aParentBlock, IRExprType aExprType);

    /// Sets expression type
    inline void setExprType(IRExprType aExprType) { pExprType = aExprType; }
    
    /// Returns true if "aExpr" is semantically equal to this expression.
    /// @note the given expression is guaranteed to be non nullptr and different
    /// from this pointer
    virtual bool isSemanticallyEqual(const IRExpr*) const { return false; };
    
	private:
		/// Expression type identifier
		IRExprType pExprType;

		/// Type of this expression
		std::shared_ptr<IRType> pType;
  };
  
  /// Links "aExpr" to "aSymb" by adding "aExpr" to the list of expressions using
  /// "aSymb" in the symbol table entry containing "aSymb"
  MODEL_EXPORT_FUNCTION void linkExprToSymbol(const std::shared_ptr<IRSymbol>& aSymb, const std::shared_ptr<IRExpr>& aExpr);

}// end namespace IR
