//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/14/2017
//
// IRExprFcn is a node of the IR (tree) that holds
// SymbolFcn objects.
// @note ExprFcn are leaf expressions.
//


#pragma once

#include "IRExpr.hpp"

// Symbol variable contained in this node
#include "IRSymbolFcn.hpp"

#include <vector>
#include <memory>
#include <cassert>

namespace IR {
  
  class MODEL_EXPORT_CLASS IRExprFcn : public IRExpr {
  public:
    typedef std::shared_ptr<IRSymbolFcn> SymbFcnSPtr;
    typedef std::shared_ptr<IRSymbolFcn>& SymbFcnSPtrRef;
    typedef const std::shared_ptr<IRSymbolFcn>& SymbFcnSPtrCRef;
    
    typedef std::shared_ptr<IRExpr> IRExprSPtr;
    typedef const std::shared_ptr<IRExpr>& IRExprSPtrCRef;
    
  public:
    static bool isa(const IRNode* aNode)
    {
      return aNode != nullptr && IRExpr::isa(aNode) &&
      static_cast<const IRExpr*>(aNode)->getExprType() == IRExprType::IR_EXPR_FCN;
    }
    
    static IRExprFcn* cast(IRNode* aNode)
    {
      if (!isa(aNode)) return nullptr;
      return static_cast<IRExprFcn*>(aNode);
    }
    
    static const IRExprFcn* cast(const IRNode* aNode)
    {
      if (!isa(aNode)) return nullptr;
      return static_cast<const IRExprFcn*>(aNode);
    }
    
    /// Sets the type of this expression, cannot set
    /// a type which is different from the type of the
    /// symbol fcn in this expr, if any
    void setType(const std::shared_ptr<IRType>& aType) override;
    
    /// Returns the number of arguments of this ExprFcn
    std::size_t numArgs() const
    {
      return pNumArgs;
    }
    
    /// Sets the "aIdx^th" argument.
    /// @note "aIdx" must be consistent with the function set.
    /// @note index is 0-based
    void setArg(std::size_t aIdx, IRExprSPtrCRef aArg);
    
    /// Gets the "aIdx^th" argument.
    /// @note "aIdx" must be consistent with the function set
    IRExprSPtr getArg(std::size_t aIdx) const;
    
    /// Returns the output type
    std::shared_ptr<IRType> getOutputType() const;
    
    /// Returns the type of the the "aIdx^th" argument
    std::shared_ptr<IRType> getArgType(std::size_t aIdx) const;
    
    /// Sets the expr function, i.e., the callee for this node.
    /// This method overrides the type of this expr with the
    /// type of "aFcn"
    void setFcn(IRExprSPtrCRef aFcn);
    
    /// Returns the function, i.e., the callee
    IRExprSPtr fcn() const;
    
    /// Returns the symbol function, i.e., the callee
    SymbFcnSPtr getFcn() const;
    
    void acceptVisitor(IRVisitor* aVisitor) override;
    
  protected:
    friend class IRConstructionFacade;
    
    IRExprFcn(const std::shared_ptr<IRBlock>& aParentBlock);
    
  private:
    /// Number of arguments
    std::size_t pNumArgs;
  };
  
}// end namespace IR

