//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 08/11/2017
//
// SymbolVar objects and semantically represents
// a decision variable.
// @note ExprVar are leaf expressions.
//


#pragma once

#include "IRExprStdVar.hpp"

// Symbol variable contained in this node
#include "IRSymbolVar.hpp"

#include <memory>
#include <cassert>

// Forward declaration
namespace IR {
  class IRSrcDecl;
  using SrcDeclSPtr = std::shared_ptr<IRSrcDecl>;
}// end namespace IR

namespace IR {
  
  class MODEL_EXPORT_CLASS VarSearchSemantic {
  public:
    /**
     * This class represents the semantic of the
     * variable w.r.t. the search declaration
     * the variable is part of.
     * Note that a variable can have zero or more
     * search semantics.
     */
    
    /// Constructor takes the pointer to the search declaration
    /// defining this semantic.
    /// @note a variable doesn't need to be part of the branching set
    /// in order to have its search semantic defined.
    /// For example:
    /// x:1..2;
    /// y:2..3;
    /// nq(x, y)
    /// branch(x)
    /// defines a search semantic on y
    /// where y is not a branching variable but it is reachable
    VarSearchSemantic(const SrcDeclSPtr& aSrc);
    ~VarSearchSemantic() = default;
    
    /// Returns the pointer to the search declaration
    /// that represents this 
    inline SrcDeclSPtr getSearchDeclaration() const
    {
      return pSearchDecl;
    }
    
    inline void setReachability(bool aReachability=true)
    {
      pReachable = aReachability;
    }
    
    inline bool getReachability() const
    {
      return pReachable;
    }
    
    inline void setInputOrder(std::size_t aInputOrder)
    {
      pInputOrder = aInputOrder;
    }
    
    inline std::size_t getInputOrder() const
    {
      return pInputOrder;
    }
    
    inline void setBranchingPolicy(bool aBranchingPolicy)
    {
      pBranchingPolicy = aBranchingPolicy;
    }
    
    /// Returns branching policy
    inline bool getBranchingPolicy() const
    {
      return pBranchingPolicy;
    }
    
  private:
    /// Pointer to the search declaration generating this semantic
    SrcDeclSPtr pSearchDecl;
    
    /// Reachability flag
    bool pReachable;
    
    /// Input order
    std::size_t pInputOrder;
    
    /// Branching policy
    bool pBranchingPolicy;
  };
  
  class MODEL_EXPORT_CLASS IRExprVar : public IRExprStdVar {
  public:
    using SymbVarSPtr = std::shared_ptr<IRSymbolVar>;
    using SymbVarSPtrRef = std::shared_ptr<IRSymbolVar>&;
    using SymbVarSPtrCRef = const std::shared_ptr<IRSymbolVar>&;
    using VarSearchSemanticSPtr = std::shared_ptr<VarSearchSemantic>;
    using VSSIter = std::vector<VarSearchSemanticSPtr>::iterator;
    
    enum VarSemantic : short {
        VS_DECISION = 0
      , VS_SUPPORT
      , VS_OBJECTIVE_MAX
      , VS_OBJECTIVE_MIN
    };
    
    enum VarOutput : short {
        VO_DEFAULT = 0 // output given by the semantic of the variable
      , VO_FORCED_ON   // output on
      , VO_FORCED_OFF  // output off
    };
    
  public:
    static bool isa(const IRNode* aNode);
    static IRExprVar* cast(IRNode* aNode);
    static const IRExprVar* cast(const IRNode* aNode);
    
    /// Set/get output
    inline void setOutput(VarOutput aOut) { pVarOutput = aOut; }
    inline VarOutput getOutput() const { return pVarOutput; }
    
    /// Sets the semantic for this variable
    inline void setSemantic(VarSemantic aSemantic)
    {
      pVarSemantic = aSemantic;
    }
    
    std::size_t numSearchSemantics() const
    {
      return pSearchSemantics.size();
    }
    
    /// Adds a new search semantic to this variable expr
    void addSearchSemantic(const VarSearchSemanticSPtr& aSrcSemantic);
    
    /// Returns the search semantic built w.r.t. the given search declaration.
    /// Returns nullptr if no search semantic can be found
    VarSearchSemanticSPtr lookupSearchSemantic(const SrcDeclSPtr& aSrcDecl);
    
    /// Return the "aIdx" search semantic of this variable expr
    VarSearchSemanticSPtr getSearchSemantic(std::size_t aIdx) const;
    
    /// Return the semantic of this variable
    inline VarSemantic getSemantic() const
    {
      return pVarSemantic;
    }

    // Range-for iterator for search semantics
    VSSIter begin()
    {
      return pSearchSemantics.begin();
    }
    VSSIter end()
    {
      return pSearchSemantics.end();
    }
    
    void acceptVisitor(IRVisitor* aVisitor) override;
    
  protected:
    friend class IRConstructionFacade;
    
    IRExprVar(const std::shared_ptr<IRBlock>& aParentBlock);
    
  private:
    /// Semantic of this variable
    VarSemantic pVarSemantic;
    
    /// Output for this variable
    VarOutput pVarOutput;
    
    /// List of the search semantics of this variable
    std::vector<VarSearchSemanticSPtr> pSearchSemantics;
  };
  
}// end namespace IR
