//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 12/04/2017
//
// IRExprMatrixSub represents an access subscript
// operator of a matrix.
// For example:
// m[t[20]];
//

#pragma once

#include "IRExpr.hpp"
#include "IRSymbol.hpp"

#include <memory>

namespace IR {
  
  class MODEL_EXPORT_CLASS IRExprMatrixSub : public IRExpr {
  public:
		static bool isa(const IRNode* aNode)
		{
			return aNode && IRExpr::isa(aNode) &&
				static_cast<const IRExpr*>(aNode)->getExprType() == IRExprType::IR_EXPR_MAT_SUB;
		}

		static IRExprMatrixSub* cast(IRNode* aNode)
		{
			if (!isa(aNode)) return nullptr;
			return static_cast<IRExprMatrixSub*>(aNode);
		}

		static const IRExprMatrixSub* cast(const IRNode* aNode)
		{
			if (!isa(aNode)) return nullptr;
			return static_cast<const IRExprMatrixSub*>(aNode);
		}
    
    /// Sets the matrix expression to subscribe into
    void setMatrix(const std::shared_ptr<IRExpr>& aMat);
    
    /// Returns the matrix expression subscribing into
    std::shared_ptr<IRExpr> getMatrix() const;
    
    /// Sets the subscript expression
    void setSubscript(const std::shared_ptr<IRExpr>& aSub);
    
    /// Returns the subscript expression
    std::shared_ptr<IRExpr> getSubscript() const;
    
    void acceptVisitor(IRVisitor* aVisitor) override;
    
  protected:
    friend class IRConstructionFacade;
    
		IRExprMatrixSub(const std::shared_ptr<IRBlock>& aParentBlock);
    
    bool isSemanticallyEqual(const IRExpr* aExpr) const override;
  };
  
}// end namespace IR
