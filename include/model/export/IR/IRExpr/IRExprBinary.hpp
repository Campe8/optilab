//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 5/8/2018
//
// IR node for binary expressions.
//

#pragma once

#include "IRExpr.hpp"

#include <memory>

namespace IR {
  
  class MODEL_EXPORT_CLASS IRExprBinary : public IRExpr {
  public:
    /// Sets lhs and rhs
    void setLhs(const std::shared_ptr<IRExpr>& aLHS);
    void setRhs(const std::shared_ptr<IRExpr>& aRHS);
    
    /// Returns lhs and rhs
    std::shared_ptr<IRExpr> lhs() const;
    std::shared_ptr<IRExpr> rhs() const;
    
  protected:
		IRExprBinary(const std::shared_ptr<IRBlock>& aParentBlock, IRExprType aExprType);
  };

}// end namespace IR
