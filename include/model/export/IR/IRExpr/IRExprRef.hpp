//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 11/27/2017
//
// IRExprRef represents the use of an alias in IR.
// In C syntax, it represents the "reference" operator '&'.
//

#pragma once

#include "IRExpr.hpp"
#include "IRSymbol.hpp"

#include <memory>

namespace IR {
  
  class MODEL_EXPORT_CLASS IRExprRef : public IRExpr {
	public:
		using SymbSPtr = std::shared_ptr<IRSymbol>;
		using SymbSPtrRef = std::shared_ptr<IRSymbol>&;
		using SymbSPtrCRef = const std::shared_ptr<IRSymbol>&;

  public:
    static bool isa(const IRNode* aNode);
    static IRExprRef* cast(IRNode* aNode);
    static const IRExprRef* cast(const IRNode* aNode);

		/// Sets the referenced expression
		void setReferenceExpr(const std::shared_ptr<IRExpr>& aRef);

		/// Returns the referenced expr
		std::shared_ptr<IRExpr> getReferenceExpr() const;
    
    void acceptVisitor(IRVisitor* aVisitor) override;
    
  protected:
    friend class IRConstructionFacade;
    
		IRExprRef(const std::shared_ptr<IRBlock>& aParentBlock);
    
    bool isSemanticallyEqual(const IRExpr* aExpr) const override;
  };
  
  /// Returns the referenced symbol from "aRef"
  IR_EXPORT_FUNCTION std::shared_ptr<IR::IRSymbol> getReferencedSymbol(const std::shared_ptr<IRExprRef>& aRef);
  IR_EXPORT_FUNCTION std::shared_ptr<IR::IRSymbol> getReferencedSymbol(const IRExprRef* aRef);
  
}// end namespace IR
