//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 5/8/2018
//
// IR node for assignment expressions.
//

#pragma once

#include "IRExprBinary.hpp"

namespace IR {
  
  class MODEL_EXPORT_CLASS IRExprAssign : public IRExprBinary {
  public:
    static bool isa(const IRNode* aNode);
    static IRExprAssign* cast(IRNode* aNode);
    static const IRExprAssign* cast(const IRNode* aNode);
    
    void acceptVisitor(IRVisitor* aVisitor) override;
    
  protected:
    friend class IRConstructionFacade;
    
		IRExprAssign(const std::shared_ptr<IRBlock>& aParentBlock);
    
    bool isSemanticallyEqual(const IRExpr* aExpr) const override;
  };

}// end namespace IR
