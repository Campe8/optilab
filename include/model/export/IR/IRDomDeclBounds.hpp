//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/19/2017
//



#pragma once

#include "IRDomDecl.hpp"

#include <vector>

namespace IR {
  
  class IR_EXPORT_CLASS IRDomDeclBounds : public IRDomDecl {
	public:
    virtual ~IRDomDeclBounds() = default;
    
    static bool isa(const IRNode* aNode)
    {
      return IRDomDecl::isa(aNode) &&
      IRDomDecl::cast(aNode)->getDeclarationType() == IRDomDecl::DomDeclType::DOM_DECL_BOUNDS;
    }
    
    static IRDomDeclBounds* cast(IRNode* aNode)
    {
      if (!isa(aNode)) return nullptr;
      return static_cast<IRDomDeclBounds*>(aNode);
    }
    
    static const IRDomDeclBounds* cast(const IRNode* aNode)
    {
      if (!isa(aNode)) return nullptr;
      return static_cast<const IRDomDeclBounds*>(IRDomDecl::cast(aNode));
    }

    /// Sets the domain bounds
    void setBounds(const IRDomDecl::ExprConstSPtr& aLB, const IRDomDecl::ExprConstSPtr& aUB);
    
    /// Returns bounds
    IRDomDecl::ExprConstSPtr getLowerBound() const;
    IRDomDecl::ExprConstSPtr getUpperBound() const;
    
    inline bool isBoolean() const
    {
      return pBoolean;
    }
    
    void acceptVisitor(IRVisitor* aVisitor) override;
    
  protected:
    friend class IRConstructionFacade;
    
    IRDomDeclBounds(const std::shared_ptr<IRBlock>& aParentBlock);
    
    inline void setBoolean()
    {
      pBoolean = true;
    }
    
  private:
    bool pBoolean;
  };
  
}// end namespace IR
