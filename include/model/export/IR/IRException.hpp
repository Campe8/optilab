//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 12/09/2017
//

#pragma once

#include <string>
#include <exception>

#include <boost/exception/all.hpp>

#define IR_TRY_BLOCK try {
#define IR_CATCH_BLOCK(aMsg)  } catch (boost::exception &e) { \
e << ir_errmsg_info{aMsg}; \
throw; }

#define ir_exception boost::exception
#define ir_get_exception_info(e) *boost::get_error_info<ir_errmsg_info>(e)

#define ir_throw(aMsg) throw IRException{aMsg}

#define ir_assert(expr) \
((expr) ? (void)0 : ir_throw(("")))

#define ir_assert_msg(expr, msg) \
((expr) ? (void)0 : ir_throw((msg)))

using ir_errmsg_info = boost::error_info<struct tag_errmsg, std::string>;

namespace IR {
  
  struct IRException : public boost::exception, public std::exception {
    IRException(const std::string& aMsg)
    : pMsg(aMsg)
    {
    }
    
    const char *what() const noexcept { return pMsg.c_str(); }
    
  private:
    /// Exception message
    std::string pMsg;
  };
  
}// end namespace IR
