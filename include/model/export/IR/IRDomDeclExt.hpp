//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/21/2017
//



#pragma once

#include "IRDomDecl.hpp"

#include <set>

namespace IR {
  
  class IR_EXPORT_CLASS IRDomDeclExt : public IRDomDecl {
	public:
    virtual ~IRDomDeclExt() = default;
    
    static bool isa(const IRNode* aNode)
    {
      return IRDomDecl::isa(aNode) &&
      IRDomDecl::cast(aNode)->getDeclarationType() == IRDomDecl::DomDeclType::DOM_DECL_EXTENSIONAL;
    }
    
    static IRDomDeclExt* cast(IRNode* aNode)
    {
      if (!isa(aNode)) return nullptr;
      return static_cast<IRDomDeclExt*>(aNode);
    }
    
    static const IRDomDeclExt* cast(const IRNode* aNode)
    {
      if (!isa(aNode)) return nullptr;
      return static_cast<const IRDomDeclExt*>(IRDomDecl::cast(aNode));
    }
    
    /// Adds an expr constant to the domain declaration
    void addDomainConstant(const IRDomDecl::ExprConstSPtr& aConst);
    
    /// Returns the set of domain constants
    std::set<IRDomDecl::ExprConstSPtr> getExtensionalDomain();
    
    void acceptVisitor(IRVisitor* aVisitor) override;
    
  protected:
    friend class IRConstructionFacade;
    
    IRDomDeclExt(const std::shared_ptr<IRBlock>& aParentBlock);
    
  private:
    /// Set used to check whether constants are unique
    std::set<std::string> pConstSet;
  };
  
}// end namespace IR
