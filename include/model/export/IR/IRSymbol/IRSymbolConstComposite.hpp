//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/13/2017
//

#pragma once

#include "IRSymbolConst.hpp"

#include "IRTypeComposite.hpp"

namespace IR {

	class IR_EXPORT_CLASS IRSymbolConstComposite : public IRSymbolConst {
	public:
		static bool isa(const IRSymbol* aSymb);

		inline std::shared_ptr<IRTypeComposite> getType() const
		{
			return std::static_pointer_cast<IRTypeComposite>(IRSymbolConst::getType());
		}

	protected:
		IRSymbolConstComposite(const std::shared_ptr<IRModule>& aModule,
			                     const std::string& aConst,
			                     const std::shared_ptr<IRTypeComposite>& aType);
	};

}// end namespace IR

