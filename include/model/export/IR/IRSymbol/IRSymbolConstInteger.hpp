//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/13/2017
//

#pragma once

#include "IRSymbolConstNumeric.hpp"
#include "IRTypeInteger.hpp"

#include "GlobalsType.hpp"

namespace IR {

	class IR_EXPORT_CLASS IRSymbolConstInteger : public IRSymbolConstNumeric {
	public:
    
    static bool isa(const IRSymbol* aSymb);
    static bool isa(const std::shared_ptr<IRSymbol>& aSymb);
    static IRSymbolConstInteger* cast(IRSymbol* aSymb);
    static const IRSymbolConstInteger* cast(const IRSymbol* aSymb);
    static std::shared_ptr<IRSymbolConstInteger> cast(const std::shared_ptr<IRSymbol>& aSymb);
    
		inline INT_64_T getInt() const
		{
			return pSymbolInt;
		}

		inline std::shared_ptr<IRTypeInteger> getType() const
		{
			return std::static_pointer_cast<IRTypeInteger>(IRSymbolConst::getType());
		}

	protected:
		friend class IRSymbolFactory;

		IRSymbolConstInteger(const std::shared_ptr<IRModule>& aModule,
			                   const std::string& aConst,
			                   const std::shared_ptr<IRTypeInteger>& aType);

	private:
		INT_64_T pSymbolInt;
	};

}// end namespace IR

