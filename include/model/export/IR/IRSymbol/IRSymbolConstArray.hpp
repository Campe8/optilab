//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/13/2017
//

#pragma once

#include "IRSymbolConstComposite.hpp"
#include "IRTypeArray.hpp"

#include <vector>

namespace IR {
  
  class IR_EXPORT_CLASS IRSymbolConstArray : public IRSymbolConstComposite {
  public:
    using SymbPtr = std::shared_ptr<IRSymbol>;
    
  public:
    static bool isa(const IRSymbol* aSymb);
    static IRSymbolConstArray* cast(IRSymbol* aSymb);
    static const IRSymbolConstArray* cast(const IRSymbol* aSymb);
    
    /// Returns array's size
    inline std::size_t size() const { return pNumElements; }
    
    bool isEqual(IRSymbol* aOther) override;
    
    /// Returns the base type of this array
    std::shared_ptr<IRType> getBaseType() const;
    
    /// Adds "aSymb" in position "aPos" which must be consistent
    /// with the size of the array.
    /// @note throws if "aSymb" has a type different from the type
    /// of this array
    void addSymbol(const SymbPtr& aSymb, std::size_t aPos);
    
    /// Returns the symbol at position "aPos" in the array
    SymbPtr getSymbol(std::size_t aPos) const;
    
    /// Returns the type of this array
    inline std::shared_ptr<IRTypeArray> getType() const
    {
      return std::static_pointer_cast<IRTypeArray>(IRSymbolConst::getType());
    }
    
    /// Complex symbol needs a class-specific pretty print
    void acceptVisitor(IRVisitor* aVisitor) override;
    
  protected:
    friend class IRSymbolFactory;
    
    IRSymbolConstArray(const std::shared_ptr<IRModule>& aModule,
                       const std::string& aName,
                       const std::shared_ptr<IRTypeArray>& aType);
    
  private:
    using Array = std::vector<SymbPtr>;
    
  private:
    /// Number of elements in the array
    std::size_t pNumElements;
    
    /// Array of const symbols
    Array pArray;
  };
  
}// end namespace IR

