//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/14/2017
//
// IRSymbolFcn represents a symbol function in the model,
// e.g., the symbol "ctx" in "ctx(x)".
//

#pragma once

#include "IRSymbolConst.hpp"

#include "IRTypeFunction.hpp"

namespace IR {
  
  class IR_EXPORT_CLASS IRSymbolFcn : public IRSymbolConst {
  public:
    virtual ~IRSymbolFcn() = default;
    
    /// Returns the type for this variable
    inline std::shared_ptr<IRType> getType() const
    {
      return pType;
    }
    
  protected:
    friend class IRSymbolFactory;
    
    IRSymbolFcn(const std::shared_ptr<IRModule>& aModule,
                const std::string& aFcnName,
                const std::shared_ptr<IRTypeFunction>& aFcnType);
    
  private:
    /// Type of this symbol var
    std::shared_ptr<IRTypeFunction> pType;
  };
  
}// end namespace IR
