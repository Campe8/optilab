//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/13/2017
//

#pragma once

#include "IRSymbolConst.hpp"

#include "IRTypeScalar.hpp"

namespace IR {

	class IR_EXPORT_CLASS IRSymbolConstScalar : public IRSymbolConst {
	public:
		virtual ~IRSymbolConstScalar() = default;
    
    static bool isa(const IRSymbol* aSymb);
    static IRSymbolConstScalar* cast(IRSymbol* aSymb);
    static const IRSymbolConstScalar* cast(const IRSymbol* aSymb);
    
		/// Returns the type for this variable
		inline std::shared_ptr<IRTypeScalar> getType() const
		{
      return std::static_pointer_cast<IRTypeScalar>(IRSymbolConst::getType());
		}

	protected:
		IRSymbolConstScalar(const std::shared_ptr<IRModule>& aModule,
			                  const std::string& aConst,
			                  const std::shared_ptr<IRTypeScalar>& aConstType);
	};

}// end namespace IR

