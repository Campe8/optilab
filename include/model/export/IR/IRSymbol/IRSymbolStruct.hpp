//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 10/07/2018
//

#pragma once

#include "IRSymbol.hpp"
#include "IRTypeStruct.hpp"

#include <cstddef>    // for std::size_t
#include <unordered_map>
#include <vector>

namespace IR {
  
  class IR_EXPORT_CLASS IRSymbolStruct : public IRSymbol {
  public:
    using SymbPtr = std::shared_ptr<IRSymbol>;
    
  public:
    static bool isa(const IRSymbol* aSymb);
    static IRSymbolStruct* cast(IRSymbol* aSymb);
    static const IRSymbolStruct* cast(const IRSymbol* aSymb);
    
    /// Returns true if this struct has a member with given name,
    /// false otherwise
    inline bool isMember(const std::string& aMember) const
    {
      return pStructMap.find(aMember) != pStructMap.end();
    }
    
    /// Returns the member of the struct "aMember"
    /// or nullptr if such member is not part of this struct
    SymbPtr getMember(const std::string& aMember) const;
    
    /// Adds a member struct with its value "aSymbol" as the "aIdx"-th
    /// member of this strcut.
    /// @note the type of the symbol MUST agree with the "aIdx"-th element type
    /// of the struct type of this SymbolStruct.
    /// @note both "aMember" and "aSymbol" must NOT be empty/nullptr
    void addMember(const std::string& aMember, const SymbPtr& aSymbol, std::size_t aIdx);
    
    bool isEqual(IRSymbol* aOther) override;
    
    /// Complex symbol needs a class-specific pretty print
    void acceptVisitor(IRVisitor* aVisitor) override;
    
  protected:
    friend class IRSymbolFactory;
    
    IRSymbolStruct(const std::shared_ptr<IRModule>& aModule,
                   const std::shared_ptr<IRTypeStruct>& aType);
    
  private:
    using structMap = std::unordered_map<std::string, SymbPtr>;
    
  private:
    /// Map of <member, value-symbols> representing the struct
    structMap pStructMap;
  };
  
}// end namespace IR

