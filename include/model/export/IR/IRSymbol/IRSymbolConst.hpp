//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/16/2017
//
// IRSymbolConst represents a symbol constant in the model,
// e.g., the symbol "10" in "x:10..11".
//

#pragma once

#include "IRSymbol.hpp"

#include "IRType.hpp"

/*
 * In general a "constant" is everything that has a "readable constant value", a rhs expression.
 * For example, 10, 10.5, [1, 2, 3], 0x000000123, are all constants.
 *
 *                                                   IRSymbolConst
 *                                                         +
 *                                                         |
 *                                  +----------------------+--------------------+
 *                                  |                      |                    |
 *                         IRSymbolConstScalar    IRSymbolConstComposite       ...
 *                                  |                      |
 *                         IRSymbolConstNumeric    IRSymbolConstMatrix
 *                                  |                      |
 *            +---------------------+-----------+  IRSymbolConstArray
 *            |                     |           |
 *   IRSymbolConstDouble  IRSymbolConstInteger ...
 */

namespace IR {

	class IR_EXPORT_CLASS IRSymbolConst : public IRSymbol {
	public:
    static bool isa(const IRSymbol* aSymb);
    
		/// Returns the type for this variable
		inline std::shared_ptr<IRType> getType() const
		{
			return pType;
		}

  protected:
    /// Constructor: "aCont" represents the actual symbol,
    /// for example: "10.1" as a string for the symbol name
    IRSymbolConst(const std::shared_ptr<IRModule>& aModule,
                  const std::string& aConst,
                  const std::shared_ptr<IRType>& aConstType);
    
	private:
		/// Type of this symbol var
		std::shared_ptr<IRType> pType;
	};

}// end namespace IR

