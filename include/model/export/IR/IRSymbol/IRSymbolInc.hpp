//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/13/2017
//

#pragma once

#include "IRSymbolConstInteger.hpp"
#include "IRSymbolConstDouble.hpp"
#include "IRSymbolConstBoolean.hpp"
#include "IRSymbolConstArray.hpp"
#include "IRSymbolVar.hpp"
#include "IRSymbolFcn.hpp"
#include "IRSymbolStruct.hpp"

namespace IR {

	typedef std::shared_ptr<IRSymbolConstInteger> IRSymbolConstIntegerSPtr;
	typedef std::shared_ptr<IRSymbolConstDouble> IRSymbolConstDoubleSPtr;
	typedef std::shared_ptr<IRSymbolConstBoolean> IRSymbolConstBooleanSPtr;
  typedef std::shared_ptr<IRSymbolConstArray> IRSymbolConstArraySPtr;
	typedef std::shared_ptr<IRSymbolVar> IRSymbolVarSPtr;
  typedef std::shared_ptr<IRSymbolFcn> IRSymbolFcnSPtr;
  typedef std::shared_ptr<IRSymbolStruct> IRSymbolStructSPtr;
  
}// end namespace IR
