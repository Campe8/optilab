//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/13/2017
//

#pragma once

#include "IRSymbolConstScalar.hpp"

#include "IRTypeNumeric.hpp"

namespace IR {

	class IR_EXPORT_CLASS IRSymbolConstNumeric : public IRSymbolConstScalar {
	public:
		static bool isa(const IRSymbol* aSymb);
    static IRSymbolConstNumeric* cast(IRSymbol* aSymb);
    static const IRSymbolConstNumeric* cast(const IRSymbol* aSymb);
    
    
		/// Returns the type for this variable
		inline std::shared_ptr<IRTypeNumeric> getType() const
		{
      return std::static_pointer_cast<IRTypeNumeric>(IRSymbolConst::getType());
		}

	protected:
		IRSymbolConstNumeric(const std::shared_ptr<IRModule>& aModule,
			                   const std::string& aConst,
			                   const std::shared_ptr<IRTypeNumeric>& aType);
	};

}// end namespace IR
