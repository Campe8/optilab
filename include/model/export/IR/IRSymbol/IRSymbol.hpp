//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 08/16/2017
// Update note on 10/7/2018
//
// IRSymbol represents a symbol in the model,
// e.g., a variable.
//
// @todo create a symbol const address representing
// a memory location address. The symbol const address
// should be used to store the pointer to the
// memory location where other symbols or data is stored.
// For example:
// struct MyStruct {
//     int a;
//     double b;
// };
// MyStruct var;
// should be represented as an expr standard variable
// with name "var" (SymbolVar) and address 0x012345,
// i.e, a SymbolConstAdress.
// The symbol address should be stored in the symbol table
// paired with a (void) pointer to the area of memory
// of sizeof(MyStruct) bytes where the actual structure
// "MyStruct" is allocated.
// @note instead of a void pointer and raw memory
// it would be possible to have a pointer to a C++ object
// representing the memory (e.g., memory pool),
// both for POD types and more complex types defined based
// on the given IRType.
// @note following the above approach, each entry in the
// symbol table should have a (void) pointer to the area
// of memory where the symbol is allocated,
// see also https://en.wikipedia.org/wiki/Symbol_table
// Ideally, each symbol should have a correspondent address
// where it is located in memory, meaning that the
// IRSymbolFactory should return both the symbol and
// its corresponding IRSymbolConstAddress.
// Then the symbols that are not IRSymbolConstAddress would
// store the memory address given by the
// correspondent IRSymbolConstAddress.
// Another solution would be to implement the above but
// to directly register the memory address of the symbols
// that are not IRSymbolConstAddress, i.e., skipping
// the IRSymbol-IRSymbolConstAddress pairing.
//

#pragma once

#include "IRObject.hpp"
#include "IRDefs.hpp"

#include "IRType.hpp"

#include <string>
#include <memory>

#include <cassert>

// Forward declarations
namespace IR {
  class IRModule;
}//end namespace IR

namespace IR {
  
  class IR_EXPORT_CLASS IRSymbol : public IRObject {
  public:
    typedef std::string SymbolName;
    typedef std::shared_ptr<IRSymbol> IRSymbolSPtr;
    typedef std::shared_ptr<IRSymbol>& IRSymbolSPtrRef;
    typedef const std::shared_ptr<IRSymbol>& IRSymbolSPtrCRef;
    
  public:
    /// Constructor takes the module this symbol belongs to
    IRSymbol(const std::shared_ptr<IRModule>& aModule,
             IRSymbolType aSymbType,
             const SymbolName& aSymbolName);
    
    IRSymbol(const std::shared_ptr<IRModule>& aModule,
             IRSymbolType aSymbType,
             const SymbolName& aSymbolName,
             const std::shared_ptr<IRType>& aType);
    
    virtual ~IRSymbol() = default;
    
    /// Returns true if this symbol is equal to "aOther",
    /// false otherwise
    virtual bool isEqual(IRSymbol* aOther);
    
    /// Returns the symbol name
    inline SymbolName getName() const
    {
      return pName;
    }
    
    /// Sets "aModule" as module containing this symbol,
    /// and updates symbol tables
    void setModule(const std::shared_ptr<IRModule>& aModule);
    
    /// Returns the type of this symbol
    std::shared_ptr<IRType> getType() const;
    
    /// Returns the module this block belongs to
    inline std::shared_ptr<IRModule> getModule() const
    {
      return pModule;
    }
    
    inline IRSymbolType getSymbolType() const
    {
      return pSymbType;
    }
    
    /// Returns true if this is a global symbol,
    /// false otherwise
    bool isGlobal() const;
    
    /// Unlinks the type of this symbol,
    /// removing its type from the symbol table
    void unlinkType() const;
    
    void acceptVisitor(IRVisitor* aVisitor) override;
    
  protected:
    /// Registers the type of this symbol updating
    /// the symbol table
    void registerType(const std::shared_ptr<IRType>& aType);
    
  private:
    /// Symbol type
    IRSymbolType pSymbType;
    
    /// Symbol name
    SymbolName pName;
    
    /// Module this symbol belongs to
    std::shared_ptr<IRModule> pModule;
  };
  
}// end namespace IR
