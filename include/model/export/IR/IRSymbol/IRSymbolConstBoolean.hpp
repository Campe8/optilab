//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Luca Foschiani on 10/14/2017
//

#pragma once

#include "IRSymbolConstNumeric.hpp"
#include "IRTypeBoolean.hpp"

#include "GlobalsType.hpp"

namespace IR {

	class IR_EXPORT_CLASS IRSymbolConstBoolean : public IRSymbolConstNumeric {
	public:
    static bool isa(const IRSymbol* aSymb);
    static bool isa(const std::shared_ptr<IRSymbol>& aSymb);
    static IRSymbolConstBoolean* cast(IRSymbol* aSymb);
    static const IRSymbolConstBoolean* cast(const IRSymbol* aSymb);
    static std::shared_ptr<IRSymbolConstBoolean> cast(const std::shared_ptr<IRSymbol>& aSymb);

		inline BOOL_T getBool() const
		{
			return pSymbolBoolean;
		}

		inline std::shared_ptr<IRTypeBoolean> getType() const
		{
			return std::static_pointer_cast<IRTypeBoolean>(IRSymbolConst::getType());
		}

	protected:
		friend class IRSymbolFactory;

		IRSymbolConstBoolean(const std::shared_ptr<IRModule>& aModule, bool aBool, const std::shared_ptr<IRTypeBoolean>& aType);

	private:
		BOOL_T pSymbolBoolean;
	};

}// end namespace IR

