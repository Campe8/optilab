//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/13/2017
//

#pragma once

#include "ModelExportDefs.hpp"

#include "IRInc.hpp"
#include "IRSymbolInc.hpp"

#include "IRTypeFactory.hpp"

#include <sstream>
#include <string>
#include <utility>
#include <vector>

namespace IR {
  
  class IR_EXPORT_CLASS IRSymbolFactory {
	public:
    using IRTypeSPtr = std::shared_ptr<IRType>;
    using IRSymbolPtr = std::shared_ptr<IRSymbol>;
    
  public:
		IRSymbolFactory();

    ~IRSymbolFactory() = default;
    
		/// Sets "aMod" as current module.
		/// Returns the old module
		IRModuleSPtr setModule(const IRModuleSPtr& aMod);

		/// Returns a symbol const integer for the value aInt
		IRSymbolConstIntegerSPtr symbolConstInteger(INT_64_T aInt);

		/// Returns a symbol const double for the value aDouble
		IRSymbolConstDoubleSPtr symbolConstDouble(REAL_T aDouble);

		/// Returns a symbol const boolean for the value aBool
		IRSymbolConstBooleanSPtr symbolConstBoolean(BOOL_T aBool);
    
    /// Returns a symbol function
    IRSymbolFcnSPtr symbolFcn(const std::string& aFcnName, const IRTypeFunctionSPtr& aFcnType);
    
    /// Returns a symbol variable with name and type "aType"
		IRSymbolVarSPtr symbolVar(const std::string& aName, const IRTypeSPtr& aType);
    IRSymbolVarSPtr symbolVarInt(const std::string& aName);
    IRSymbolVarSPtr symbolVarDouble(const std::string& aName);
    IRSymbolVarSPtr symbolVarBool(const std::string& aName);
    
    /// Returns a symbol const array for the input array of Integer numbers
    IRSymbolConstArraySPtr symbolConstArray(const std::vector<INT_64_T>& aIntArray, const std::string& aName="");
    
    /// Returns a symbol const array for the input array of symbols
    IRSymbolConstArraySPtr symbolConstArray(const std::vector<IRSymbolPtr>& aArray, const std::string& aName="");
    
    /// Greates a symbol struct with name "aStructName" (representing its type) and
    /// the list of members <name, symbol> in "aMembers"
    IRSymbolStructSPtr symbolStruct(const std::string& aStructName,
                                    const std::vector<std::pair<std::string, IRSymbolPtr>>& aMembers);
    
  private:
		/// Module to use for symbol generation
		IRModuleSPtr pIRModule;

		/// Returns current module
		inline IRModuleSPtr getModule() const
		{
			return pIRModule;
		}

		/// Returns current type factory
		std::shared_ptr<IRTypeFactory> getTypeFactory() const;
  };
  
}// end namespace IR
