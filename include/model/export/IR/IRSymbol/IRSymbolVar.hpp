//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/16/2017
//
// IRSymbolVar represents a symbol variable in the model,
// e.g., the symbol "x" in "x:10..11".
//

#pragma once

#include "IRSymbol.hpp"
#include "IRType.hpp"

namespace IR {
  
  class IR_EXPORT_CLASS IRSymbolVar : public IRSymbol {
  public:
    static bool isa(const IRSymbol* aSymb);
    static IRSymbolVar* cast(IRSymbol* aSymb);
    static const IRSymbolVar* cast(const IRSymbol* aSymb);
    
    /// Returns true if this variable has an assigned types,
    /// false otherwise
    inline bool hasType() const
    {
      return pType != nullptr;
    }
    
    /// Sets the type for this variable
    void setType(const std::shared_ptr<IRType>& aVarType);
    
    /// Returns the type for this variable
    inline std::shared_ptr<IRType> getType() const
    {
      return pType;
    }
    
  protected:
    friend class IRSymbolFactory;
    
    IRSymbolVar(const std::shared_ptr<IRModule>& aModule,
                const std::string& aVarName,
                const std::shared_ptr<IRType>& aVarType = nullptr);
    
  private:
    /// Type of this symbol var
    std::shared_ptr<IRType> pType;
  };
  
}// end namespace IR

