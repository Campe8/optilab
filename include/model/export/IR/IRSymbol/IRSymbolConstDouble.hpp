//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Luca Foschiani on 10/14/2017
//

#pragma once

#include "IRSymbolConstNumeric.hpp"
#include "IRTypeDouble.hpp"

#include "GlobalsType.hpp"

namespace IR {

	class IR_EXPORT_CLASS IRSymbolConstDouble : public IRSymbolConstNumeric {
	public:
		virtual ~IRSymbolConstDouble() = default;

		static bool isa(const IRSymbolConst* aSymb)
		{
			return aSymb &&
				IRTypeDouble::isa(aSymb->getType().get());
		}

		static bool isa(const std::shared_ptr<IRSymbolConst>& aSymb)
		{
			return aSymb &&
				IRTypeDouble::isa(aSymb->getType().get());
		}

		static IRSymbolConstDouble* cast(IRSymbolConst* aSymb)
		{
			if (!isa(aSymb)) return nullptr;
			return static_cast<IRSymbolConstDouble*>(aSymb);
		}

		static const IRSymbolConstDouble* cast(const IRSymbolConst* aSymb)
		{
			if (!isa(aSymb)) return nullptr;
			return static_cast<const IRSymbolConstDouble*>(aSymb);
		}

		static std::shared_ptr<IRSymbolConstDouble> cast(const std::shared_ptr<IRSymbolConst>& aSymb)
		{
			if (!isa(aSymb)) return nullptr;
			return std::static_pointer_cast<IRSymbolConstDouble>(aSymb);
		}

		inline REAL_T getDouble() const
		{
			return pSymbolDouble;
		}

		inline std::shared_ptr<IRTypeDouble> getType() const
		{
			return std::static_pointer_cast<IRTypeDouble>(IRSymbolConst::getType());
		}

	protected:
		friend class IRSymbolFactory;

		IRSymbolConstDouble(const std::shared_ptr<IRModule>& aModule,
                        const std::string& aConst,
                        const std::shared_ptr<IRTypeDouble>& aType);

	private:
		REAL_T pSymbolDouble;
	};

}// end namespace IR

