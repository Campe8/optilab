//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/28/2017
//
// The IR context is an important data structure
// that owns and manages the core "global" module and
// data of the IR's infrastructure.
// For example, see
// http://llvm.org/doxygen/classllvm_1_1LLVMContext.html#details
//

#pragma once

// IR base object
#include "IRObject.hpp"

#include "IRModule.hpp"

#include "IRTypeFactory.hpp"

// Forward declarations
namespace IR {
  class IRContext;
  typedef std::shared_ptr<IRContext> IRContextSPtr;
}// end namespace IR

namespace IR {
  
  class IR_EXPORT_CLASS IRContextSpec {
  public:
    /**
     * Context specification information.
     * This class holds meta-information about a context,
     * for example, name of the model the context is build on, etc.
     * This class it is used to store all the information that is not
     * inherently related to the (IR) model.
     */
    IRContextSpec();
    
    ~IRContextSpec() = default;
    
    inline void setModelName(const std::string& aName)
    {
      pModelName = aName;
    }
    
    inline std::string getModelName() const
    {
      return pModelName;
    }
    
    inline void setSolutionLimit(long long aLimit)
    {
      pSolutionLimit = aLimit;
    }
    
    inline long long getSolutionLimit() const
    {
      return pSolutionLimit;
    }
    
  private:
    /// Model name
    std::string pModelName;
    
    /// Limit on the number of solution to find
    long long pSolutionLimit;
  };
  
  class IR_EXPORT_CLASS IRContext : public IRObject {
  public:
    enum ContextModule : int {
        CM_GLOBAL = 0
      , CM_VARIABLE
      , CM_CONSTRAINT
      , CM_PARAMETER
      , CM_SEARCH
      , CM_UNDEF // must be last
    };
    
  public:
    virtual ~IRContext() = default;
    
    /// Returns the name of the given module
    static std::string getModuleName(ContextModule aCtxMod);
    
    /// Returns the global module owned by this context
    inline IRModuleSPtr globalModule() const
    {
      return pGlobalModule;
    }
    
    inline std::shared_ptr<IRTypeFactory> typeFactory() const
    {
      return pTypeFactory;
    }
    
    IRContextSpec* getContextSpec()
    {
      return pContextSpec.get();
    }
    
    const IRContextSpec* getContextSpec() const
    {
      return pContextSpec.get();
    }
    
    void acceptVisitor(IRVisitor* aVisitor) override;
    
  private:
    IRContext();
    
    /// Pointer to the context specification
    std::unique_ptr<IRContextSpec> pContextSpec;
    
    IRModuleSPtr pGlobalModule;
    
    /// Type factory hold by this context,
    /// this should be the factory used by all the
    /// transformations and module under this context
    /// in order to limit the number of type created
    std::shared_ptr<IRTypeFactory> pTypeFactory;
    
    friend IR_EXPORT_FUNCTION IRContextSPtr createIRContext();
  };
  
  IR_EXPORT_FUNCTION IRContextSPtr createIRContext();
  
}// end namespace IR

