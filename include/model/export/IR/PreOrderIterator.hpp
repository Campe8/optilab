//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/11/2017
//
// This class implements an iterator for the (n) tree.
//

#pragma once

#include "IRExportDefs.hpp"

#include <boost/iterator/iterator_facade.hpp>

#include <vector>
#include <stack>
#include <memory>

namespace IR {
  namespace _private {
    
    template<typename T>
    class IR_EXPORT_CLASS PreOrderIterator : public boost::iterator_facade<PreOrderIterator<T>, T, boost::forward_traversal_tag> {
    private:
      typedef std::reference_wrapper<T> DataRefWrapper;
      typedef std::stack<DataRefWrapper, std::vector<DataRefWrapper>> Stack;
      
    public:
      PreOrderIterator() = default;
      
      PreOrderIterator(T& aData)
      {
        pData = aData;
        pStack = std::make_shared<Stack>();
        stack().push(aData);
      }
      
      PreOrderIterator(const T& aData)
      {
        pData = aData;
        pStack = std::make_shared<Stack>();
        stack().push(pData);
      }
      
      PreOrderIterator(const PreOrderIterator& aPreOrderIter)
      {
        pData = aPreOrderIter.pData;
        pStack = aPreOrderIter.pStack;
      }
      
    private:
      friend class boost::iterator_core_access;
      
      /// Current data
      T pData;
      
      /// Stack used for pre-order visit
      std::shared_ptr<Stack> pStack;
      
      /*
       * Dereference, equal, and increment are needed
       * by the iterator_facade class instance
       */
      
      T& dereference() const
      {
        T data = pData;
        return data;
      }
      
      bool equal(const PreOrderIterator<T>& aIt) const
      {
        return aIt.pData == pData ? true : false;
      }
      
      /*
       * Pre-order visit:
       * visit(node)
       *	stack.clear();
       *	while (!stack.empty() || node != NULL)
       *		if(node != null)
       *			do_something(node);
       *      if(node.right != null)
       *				stack.push(node.right)
       *      endif
       *      node = node.left;
       *    else
       *			node = stack.top()
       *      stack.pop()
       *		endif
       *	endwhile
       */
      void increment()
      {
        if (stack().empty())
        {
          pData = nullptr;
          return;
        }
        
        // Top of stack has current node
        // and vice-versa, current node is always
        // the top of the stack
        stack().pop();
        auto it = currData()->rbegin();
        for (; it != currData()->rend(); ++it)
        {
          stack().push(*it);
        }
        
        if (!stack().empty())
        {
          currData() = stack().top();
        }
        else
        {
          pData = nullptr;
        }
      }//increment
      
      /// Utility function: returns the reference to the stack
      inline Stack& stack()
      {
        return *pStack;
      }
      
      /// Utility function: returns the reference to the current node
      inline const T& currData() const
      {
        return pData;
      }
      
      inline T& currData()
      {
        return pData;
      }
      
    };
    
  }
}// end namespace DataStructure/graph/tree
