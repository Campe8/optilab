//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/16/2017
//
// Entry for the symbol table.
//

#pragma once

#include "IRExportDefs.hpp"

#include "IRSymbol.hpp"
#include "IRType.hpp"

#include <string>
#include <memory>
#include <set>
#include <cassert>

// Forward declarations
namespace IR {
  class IRExpr;
}// end namespace IR

namespace IR {
  
  class IR_EXPORT_CLASS SymbolTableEntry {
  public:
    using SymbolTableEntryKey = IRSymbol::SymbolName;
    using LinkSet = std::set<std::shared_ptr<IRExpr>>;
    
  public:
    SymbolTableEntry(IRSymbol::SymbolName aSymbolName, IRSymbolType aSymbolType);
    
    static SymbolTableEntry::SymbolTableEntryKey getKeyFromName(const IRSymbol::SymbolName& aName)
    {
      return aName;
    }
    
    inline SymbolTableEntryKey getKey() const
    {
      // @todo think about a more efficient key
      // than a std string
      return getName();
    }
    
    inline IRSymbol::SymbolName getName() const
    {
      return pSymbolName;
    }
    
    inline IRSymbolType getSymbolType() const
    {
      return pSymbolType;
    }
    
    /// Sets the type for this symbol table entry
    void setType(const std::shared_ptr<IRType>& aType);
    
    inline std::shared_ptr<IRType> getType() const
    {
      return pType;
    }
    
    /// Adds the pointer to the symbol that owns this entry
    inline void addSymbol(const IRSymbol::IRSymbolSPtr& aSymbol) { pSymbol = aSymbol; }
    
    /// Returns the pointer to the symbol owning this entry
    inline IRSymbol::IRSymbolSPtr getSymbol() const { return pSymbol; }
    
    /// Adds a link to an expression using the symbol
    /// which logically owns this symbol table entry
    /// Sets the type if no type has been set yet.
    /// @note throws if "aLink" as a type different that
    /// the one currently set.
    /// @note throws if "aLink" type is nullptr
    void addExprLink(const std::shared_ptr<IRExpr>& aLink);
    
    /// Deletes a link from the set of expression links
    void removeExprLink(const std::shared_ptr<IRExpr>& aLink);
    
    /// Deletes all links from the set of expression links
    void removeAllExprLinks();
    
    inline const LinkSet& getLinkSet() const
    {
      return pExprSet;
    }
    
  private:
    /// Name of this symbol entry
    IRSymbol::SymbolName pSymbolName;
    
    /// Pointer to the symbol
    IRSymbol::IRSymbolSPtr pSymbol;
    
    /// Type of this symbol
    IRSymbolType pSymbolType;
    
    /// IRType of this symbol
    std::shared_ptr<IRType> pType;
    
    /// Pointers to the expressions using the symbol
    /// which this symbol table entry belongs to.
    /// @note this is done for performance on lookups.
    /// @note this does not ensure the validity
    ///       of the pointers in the set
    LinkSet pExprSet;
  };
  
}// end namespace IR

