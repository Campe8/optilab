//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/18/2017
//


#pragma once

#include "IRExpr.hpp"
#include "IRType.hpp"
#include "IRModule.hpp"
#include "IRExprVar.hpp"
#include "IRExprConst.hpp"

#include <memory>

namespace IR {
	namespace Utils {

		/// Returns the base type of the given expr
		IR_EXPORT_FUNCTION std::shared_ptr<IRType> getBaseType(const std::shared_ptr<IRExpr>& aExpr);

    /// Strips the given type returning its base type
    IR_EXPORT_FUNCTION std::shared_ptr<IRType> stripType(const std::shared_ptr<IRType>& aExpr);
    
    /// Swaps "aExpr1" with "aExor2", i.e., in the node tree, replaces "aExpr1" with "aExpr2"
    IR_EXPORT_FUNCTION void swapExpr(const std::shared_ptr<IRExpr>& aExpr1, const std::shared_ptr<IRExpr>& aExpr2);
    
		/// Looks up for the variable with name "aName" in the variable module
    IR_EXPORT_FUNCTION std::shared_ptr<IR::IRExprVar> lookupExprVar(const std::string& aName, const IR::IRContextSPtr& aCtx);
    
    /// Looks up for the parameter with name "aName" in the parameter module
    IR_EXPORT_FUNCTION std::shared_ptr<IR::IRExprConst> lookupExprConstPar(const std::string& aName, const IR::IRContextSPtr& aCtx);
    
    /// Looks up for the expression with ID "aName" from all the modules in the context.
    /// @note this lookup could be expensive, use with caution!
    IR_EXPORT_FUNCTION std::shared_ptr<IR::IRExpr> lookupExprByName(const std::string& aName, const IR::IRContextSPtr& aCtx);
    
    /// Returns true if the two expressions are the same, false otherwise.
    /// @note this is not a pointer comparison but a semantic comparison of the two expressions
    IR_EXPORT_FUNCTION bool isEqualExpr(const IR::IRExpr* aBase, const IR::IRExpr* aComp);
    
    // Lookup "aExpr" in the symbol table of the given module and return its entry symbol.
    // The typename of the function is the class of the given expression while the module is the module
    // to look for the expression.
    // @note returns nullptr if no expr has been found.
    template<typename T>
    IR::IRSymbol::IRSymbolSPtr lookupSymbolFromExpr(const std::shared_ptr<IR::IRExpr>& aExpr, const IR::IRModuleSPtr& aMod)
    {
      assert(aExpr && T::isa(aExpr.get()));
      assert(aMod);
      
      auto baseExpr = T::cast(aExpr.get());
      auto symbolTable = aMod->getSymbolTable();
      assert(symbolTable);
      
      for(const auto& entry : *symbolTable)
      {
        const auto& linkSet = (entry.second)->getLinkSet();
        for(const auto& expr : linkSet)
        {
          if(auto castExpr = T::cast(expr.get()))
          {
            if(isEqualExpr(baseExpr, castExpr)) { return (entry.second)->getSymbol(); }
          }
        }
      }
      
      return nullptr;
    }//lookupSymbolFromExpr
    
	}// end namespace Utils
}// end namespace IR
