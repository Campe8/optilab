//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Luca Foschiani on 08/22/2017
//


#pragma once

#include "IRTypeNumeric.hpp"

namespace IR {
  
  class IR_EXPORT_CLASS IRTypeInteger : public IRTypeNumeric {
    
	public:
    using NumBits = std::size_t;
    
  public:
    
    bool isEqual(const IRType* aOther) const override;
    
    static bool isa(const IRType* aIRType)
    {
      return aIRType != nullptr &&
        static_cast<const IRType*>(aIRType)->getTypeID() == IRTypeID::IR_TYPE_INTEGER;
    }
    
    inline NumBits getNumBits() const
    {
      return pNumBits;
    }
    
    static IRTypeInteger* cast(IRType* aIRType)
    {
      if (!isa(aIRType)) return nullptr;
      return static_cast<IRTypeInteger*>(aIRType);
    }
    
    static const IRTypeInteger* cast(const IRType* aIRType)
    {
      if (!isa(aIRType)) return nullptr;
      return static_cast<const IRTypeInteger*>(aIRType);
    }
    
    void prettyPrint(std::ostream& aOut) override
    {
      aOut << "int" << pNumBits;
    }
    
  protected:
    friend class IRTypeFactory;
    
    IRTypeInteger(std::size_t numBits);
    
  private:
    NumBits pNumBits;
  };
  
}// end namespace IR
