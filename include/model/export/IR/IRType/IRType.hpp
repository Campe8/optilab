//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/16/2017
//
// IRType represents any type of the objects in IR.
//

#pragma once

#include "IRObject.hpp"
#include "IRDefs.hpp"

#include <iostream>
#include <vector>
#include <memory>
#include <cassert>

/*
 * The IRType hierarchy is as follows, and it is similar to the
 * hierarchy used by LLVM for its type system.
 * It is a hierarchy that takes into account C hierarchy and LLVM.
 * For more info, see http://llvm.org/doxygen/classllvm_1_1Type.html#details.
 *
 *											                          IRType
 *                                                  |
 *                   +-------------------+----------+--------+-------------------+
 *                   |                   |                   |                   |
 *            IRCompositeType      IRFunctionType       IRScalarType         IRVoidType
 *                   |                                       |
 *         +---------+---------+                   +---------+---------+
 *         |                   |                   |                   |
 *   IRSequentialType     IRStructType       IRNumericType       IRPointerType
 *         |                                       |
 *    IRArrayType                        +---------+---------+
 *                                       |                   |
 *                                 IRIntegerType            ...
 *
 */

namespace IR {
  
  class IR_EXPORT_CLASS IRType : public IRObject {
  public:
    virtual ~IRType() = default;
    
    /// Equality method
    virtual bool isEqual(const IRType* aOther) const = 0;
    
    inline IRTypeID getTypeID() const
    {
      return pTypeID;
    }
    
    /// Returns the number of types contained in this type.
    /// For example, the number of types of the arguments of a function type.
    inline std::size_t numContainedTypes() const
    {
      return pContainedTypes.size();
    }
    
    /// Returns the "aIdx"th type contained in this type
    inline std::shared_ptr<IRType> getContainedType(std::size_t aIdx) const
    {
      assert(aIdx < numContainedTypes());
      return pContainedTypes[aIdx];
    }
    
    void acceptVisitor(IRVisitor* aVisitor) override;
    
    /// Prints the current type into aOut
    virtual void prettyPrint(std::ostream& aOut) = 0;
    
  protected:
    IRType(IRTypeID aTypeID);
    
    /// Adds "aType" as contained type
    inline void addContainedType(const std::shared_ptr<IRType>& aType)
    {
      pContainedTypes.push_back(aType);
    }
    
  private:
    /// Id of this type
    IRTypeID pTypeID;
    
    /// Array of pointers to the types contained by this type.
    /// For example, this is used to store the types of the
    /// arguments of a function type.
    std::vector<std::shared_ptr<IRType>> pContainedTypes;
  };
  
}// end namespace IR

