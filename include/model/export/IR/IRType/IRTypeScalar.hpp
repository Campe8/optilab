//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/12/2017
//
// IRTypeScalar represents scalar types,
// for example Integer and Pointer types.
//


#pragma once

#include "IRType.hpp"

namespace IR {
  
  class IR_EXPORT_CLASS IRTypeScalar : public IRType {
  public:
    static bool isa(const IRType* aType);
  
  protected:
    IRTypeScalar(IRTypeID aTypeID);
  };
  
}// end namespace IR

