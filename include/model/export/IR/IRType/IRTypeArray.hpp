//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/10/2017
//
// See
// https://llvm.org/docs/LangRef.html#type-system
// for more information about the LLVM type system.
// For example:
// - [40 x int] is an array of 40 elements of type int
// - [3 x [4 x int]] is a 3 x 4 array of elements of type int
//

#pragma once

#include "IRTypeSequential.hpp"

namespace IR {
  
  class IR_EXPORT_CLASS IRTypeArray : public IRTypeSequential {
  public:
		virtual ~IRTypeArray() = default;

		static bool isa(const IRType* aIRType);
		static IRTypeArray* cast(IRType* aIRType);
		static const IRTypeArray* cast(const IRType* aIRType);

		bool isEqual(const IRType* aOther) const override;

		void prettyPrint(std::ostream& aOut) override;

	protected:
		friend class IRTypeFactory;

		IRTypeArray(std::shared_ptr<IRType> aBaseType, std::size_t aNumElem);
  };
  
}// end namespace IR
