//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Luca Foschiani on 09/08/2017
//

#pragma once

#include "IRType.hpp"

namespace IR {
  
  class IR_EXPORT_CLASS IRTypeFunction : public IRType {
  public:
    using IRTypeSPtr = std::shared_ptr<IRType>;
    
  public:
    bool isEqual(const IRType* aOther) const override;
    
    static bool isa(const IRType* aIRType)
    {
      return aIRType != nullptr &&
        static_cast<const IRType*>(aIRType)->getTypeID() == IRTypeID::IR_TYPE_FUNCTION;
    }
    
    /// The return type is in the first position.
    inline IRTypeSPtr getReturnType() const
    {
      return getContainedType(0);
    }
    
    /// Returns the type of the "aIdx" parameter.
    /// @note indexing is 0 based
    inline IRTypeSPtr getParamType(std::size_t aIdx) const
    {
      assert(aIdx < getNumParams());
      return getContainedType(aIdx + 1);
    }
    
    /// Returns the number of actual parameters
    /// (this doesn't count the return type).
    inline std::size_t getNumParams() const
    {
      assert(numContainedTypes() > 0);
      return numContainedTypes() - 1;
    }
    
    static IRTypeFunction* cast(IRType* aIRType)
    {
      if (!isa(aIRType)) return nullptr;
      return static_cast<IRTypeFunction*>(aIRType);
    }
    
    static const IRTypeFunction* cast(const IRType* aIRType)
    {
      if (!isa(aIRType)) return nullptr;
      return static_cast<const IRTypeFunction*>(aIRType);
    }
    
    void prettyPrint(std::ostream& aOut) override;
    
  protected:
    friend class IRTypeFactory;
    
    /// The return type of the function will be the first
    /// element of pContainedTypes in IRType.hpp
    IRTypeFunction(const IRTypeSPtr& aResultType, const std::vector<IRTypeSPtr>& aParamType);
  };
  
}// end namespace IR
