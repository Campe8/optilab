//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Luca Foschiani on 09/06/2017
//


#pragma once

#include "IRType.hpp"

namespace IR {
  
  class IR_EXPORT_CLASS IRTypeVoid : public IRType {
  
  public:
    bool isEqual(const IRType* aOther) const override
    {
      return isa(aOther);
    }
    
    static bool isa(const IRType* aIRType)
    {
      return aIRType != nullptr &&
        static_cast<const IRType*>(aIRType)->getTypeID() == IRTypeID::IR_TYPE_VOID;
    }
    
    static IRTypeVoid* cast(IRType* aIRType)
    {
      if (!isa(aIRType)) return nullptr;
      return static_cast<IRTypeVoid*>(aIRType);
    }
    
    static const IRTypeVoid* cast(const IRType* aIRType)
    {
      if (!isa(aIRType)) return nullptr;
      return static_cast<const IRTypeVoid*>(aIRType);
    }
    
    void prettyPrint(std::ostream& aOut) override
    {
      aOut << "void";
    }
    
  protected:
    friend class IRTypeFactory;
    
    IRTypeVoid();
  };
  
}// end namespace IR
