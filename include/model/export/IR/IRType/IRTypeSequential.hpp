//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/10/2017
// 
// IRSequentialType repreents a sequential container.
// It holds a given number of types which must be
// of the same type.
//

#pragma once

#include "IRTypeComposite.hpp"

namespace IR {
  
  class IR_EXPORT_CLASS IRTypeSequential : public IRTypeComposite {
  public:
    virtual ~IRTypeSequential() = default;
    
		inline std::shared_ptr<IRType> getBaseType() const
		{
			return pBaseType;
		}

		inline std::size_t getNumElements() const
		{
			return pNumElements;
		}

  protected:
		IRTypeSequential(IRTypeID aTypeID, std::shared_ptr<IRType> aBaseType, std::size_t aNumElem);

	private:
		/// Base type for the sequential type
		std::shared_ptr<IRType> pBaseType;

		/// Number of element sequentially stored
		std::size_t pNumElements;
  };
  
}// end namespace IR
