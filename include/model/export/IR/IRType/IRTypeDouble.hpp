//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Luca Foschiani on 08/30/2017
//


#pragma once

#include "IRTypeNumeric.hpp"

namespace IR {
  
  class IR_EXPORT_CLASS IRTypeDouble : public IRTypeNumeric {
  
  public:
    bool isEqual(const IRType* aOther) const override
    {
      return isa(aOther);
    }
    
    static bool isa(const IRType* aIRType)
    {
      return aIRType != nullptr &&
        static_cast<const IRType*>(aIRType)->getTypeID() == IRTypeID::IR_TYPE_DOUBLE;
    }
    
    static IRTypeDouble* cast(IRType* aIRType)
    {
      if (!isa(aIRType)) return nullptr;
      return static_cast<IRTypeDouble*>(aIRType);
    }
    
    static const IRTypeDouble* cast(const IRType* aIRType)
    {
      if (!isa(aIRType)) return nullptr;
      return static_cast<const IRTypeDouble*>(aIRType);
    }
    
    void prettyPrint(std::ostream& aOut) override
    {
      aOut << "double";
    }
    
  protected:
    friend class IRTypeFactory;
    
    IRTypeDouble();
  };
  
}// end namespace IR
