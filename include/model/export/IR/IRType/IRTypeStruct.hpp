//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 10/06/2018
// 
// IRTypeStruct reprents a struct type.
// This type must have a name and, similarly to LLVM,
// the body of a struct type are laid out in memory
// consecutively with the elements directly
// one after the other.
// For example:
// Struct MyStruct {
//    int a;
//    double b;
// };
// Where the body is
// <int, double>
// and the name is
// "MyStruct"
// which will identify the above struct type.
// @note name and body must NOT be empty!
//

#pragma once

#include "IRTypeComposite.hpp"

#include <algorithm>  // for std::find
#include <cstddef>    // for std::size_t
#include <string>
#include <utility>    // for std::pair

namespace IR {
  
  class IR_EXPORT_CLASS IRTypeStruct : public IRTypeComposite {
  public:
    using IRTypeSPtr  = std::shared_ptr<IRType>;
    
  public:
    virtual ~IRTypeStruct() = default;
    
    static bool isa(const IRType* aType);
    static IRTypeStruct* cast(IRType* aType);
    static const IRTypeStruct* cast(const IRType* aType);
    
    /// Returns the name of this type stuct
    inline std::string getName() const { return pName; }
    
    /// Sets the body of the struct as an ordered list
    /// of pairs <id, type> where the id is an alias
    /// for the position of the type in the the list of elements.
    /// @note all ids must be unique
    void setBody(const std::vector<IRTypeSPtr>& aBody);
    
    /// Returns the type of the "aIdx" element.
    /// @note indexing is 0 based
    inline IRTypeSPtr getElement(std::size_t aIdx) const
    {
      assert(aIdx < getNumElements());
      return getContainedType(aIdx);
    }
    
    /// Returns the number of elements
    inline std::size_t getNumElements() const { return numContainedTypes(); }
    
    /// Is equal method
    bool isEqual(const IRType* aOther) const override;
    
    /// Pretty print method
    void prettyPrint(std::ostream& aOut) override;
    
  protected:
    friend class IRTypeFactory;
    
    /// The type requires a name
    IRTypeStruct(const std::string& aName);

	private:
		/// Name of this type struct
    std::string pName;
  };
  
}// end namespace IR
