//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Luca Foschiani on 08/23/2017
//
// Boolean type is a numeric type since,
// in OptiLab, it is seen as an Integer
// type where 0 if false and everything else
// is true.
//


#pragma once

#include "IRTypeNumeric.hpp"

namespace IR {
  
  class IR_EXPORT_CLASS IRTypeBoolean : public IRTypeNumeric {
  
  public:
    bool isEqual(const IRType* aOther) const override
    {
      return isa(aOther);
    }
    
    static bool isa(const IRType* aIRType)
    {
      return aIRType != nullptr &&
        static_cast<const IRType*>(aIRType)->getTypeID() == IRTypeID::IR_TYPE_BOOLEAN;
    }
    
    static IRTypeBoolean* cast(IRType* aIRType)
    {
      if (!isa(aIRType)) return nullptr;
      return static_cast<IRTypeBoolean*>(aIRType);
    }
    
    static const IRTypeBoolean* cast(const IRType* aIRType)
    {
      if (!isa(aIRType)) return nullptr;
      return static_cast<const IRTypeBoolean*>(aIRType);
    }
    
    void prettyPrint(std::ostream& aOut) override
    {
      aOut << "bool";
    }
    
  protected:
    friend class IRTypeFactory;
    
    IRTypeBoolean();
  };
  
}// end namespace IR
