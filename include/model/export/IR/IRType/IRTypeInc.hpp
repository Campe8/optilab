//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Luca Foschiani on 08/30/2017
//

#pragma once

#include "IRType.hpp"
#include "IRTypeArray.hpp"
#include "IRTypeBoolean.hpp"
#include "IRTypeDouble.hpp"
#include "IRTypeFunction.hpp"
#include "IRTypeInteger.hpp"
#include "IRTypeStruct.hpp"
#include "IRTypeVoid.hpp"

namespace IR {
  
  using IRTypeSPtr = std::shared_ptr<IRType>;
  using IRTypeArraySPtr = std::shared_ptr<IRTypeArray>;
  using IRTypeBooleanSPtr = std::shared_ptr<IRTypeBoolean>;
  using IRTypeDoubleSPtr = std::shared_ptr<IRTypeDouble>;
  using IRTypeFunctionSPtr = std::shared_ptr<IRTypeFunction>;
  using IRTypeIntegerSPtr = std::shared_ptr<IRTypeInteger>;
  using IRTypeStructSPtr = std::shared_ptr<IRTypeStruct>;
  using IRTypeVoidSPtr = std::shared_ptr<IRTypeVoid>;
  
}// end namespace IR
