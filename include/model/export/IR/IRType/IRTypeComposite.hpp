//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/10/2017
//
// IRCompositeType represents the super class
// of composite types like sequential (array)
// and struct types.
//

#pragma once

#include "IRType.hpp"

/*
 * The IRType hierarchy is as follows, and it is similar to the
 * hierarchy used by LLVM for its type system.
 * For more info,
 * see http://llvm.org/doxygen/classllvm_1_1CompositeType.html#a46ef9732149ad230a60db4f4b8a403e5.
 *
 *            IRTypeComposite
 *                   ^
 *                   |
 *         +-------------------+
 *         ^                   ^
 *         |                   |
 *   IRTypeSequential     IRStructType
 *         ^
 *         |
 *    IRArrayType
 */

namespace IR {
  
  class IR_EXPORT_CLASS IRTypeComposite : public IRType {
  public:
    virtual ~IRTypeComposite() = default;
    
    static bool isa(const IRType* aType);
    static IRTypeComposite* cast(IRType* aType);
    static const IRTypeComposite* cast(const IRType* aType);
    
  protected:
		IRTypeComposite(IRTypeID aTypeID);
  };
  
}// end namespace IR
