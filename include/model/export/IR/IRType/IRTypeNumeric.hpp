//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/12/2017
//
// IRTypeNumeric represents numeric types,
// for example Integer and Double.
//


#pragma once

#include "IRTypeScalar.hpp"

namespace IR {
  
  class IR_EXPORT_CLASS IRTypeNumeric : public IRTypeScalar {
  public:
    static bool isa(const IRType* aType);
  
  protected:
    IRTypeNumeric(IRTypeID aTypeID);
  };
  
}// end namespace IR

