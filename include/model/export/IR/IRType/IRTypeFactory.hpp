//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Luca Foschiani on 08/30/2017
//

#pragma once

#include "IRTypeInc.hpp"
#include "IRType.hpp"

#include <map>
#include <unordered_map>

namespace IR {
  
  class IR_EXPORT_CLASS IRTypeFactory {
    
  public:
    /// Returns a Void type
    IRTypeVoidSPtr typeVoid();
    
    /// Returns a Boolean type
    IRTypeBooleanSPtr typeBoolean();
    
    /// Returns a 64 bit Integer type
    IRTypeIntegerSPtr typeInteger64();
    
    /// Returns a Double type
    IRTypeDoubleSPtr typeDouble();
    
    /// Returns a Function type. We don't store non-constraint functions in any structure.
    IRTypeFunctionSPtr typeFunction(const IRTypeSPtr& aResultType, const std::vector<IRTypeSPtr>& aParamType);
    
    /// Returns a Function type with a IRTypeVoid as return type
    IRTypeFunctionSPtr typeFunctionConstraint(const std::vector<IRTypeSPtr>& aParamType);
    
    /// Returns an array type with "aNumElements" of type "aBaseType"
    IRTypeArraySPtr typeArray(std::size_t aNumElements, const IRTypeSPtr& aBaseType);
    
    /// Returns a struct type with given name and elements
    IRTypeStructSPtr typeStruct(const std::string& aName, const std::vector<IRTypeStruct::IRTypeSPtr>& aElements);
    
  private:
    /**
     * Base map to store types w.r.t. their
     * base type "IRTypeID".
     * Each entry of the table is the pointer
     * to the instance of the type.
     */
    
    /// Utility function, converts TypeID to
    /// an integer
    inline int typeIDToInt(IRTypeID aID) const
    {
      return static_cast<int>(aID);
    }
    
    /// Hash table for types
    std::unordered_map<int, IRTypeSPtr> pPrimitiveTypeRegister;
    
    /// Returns a new instance of a primitive type
    /// with ID "aID".
    /// @note returns nullptr for non primitive types
    IRType* createPrimitiveType(IRTypeID aID);
    
    /// Returns the primitive type with ID "aID".
    /// If such type is not present in the hash table,
    /// creates a new entry in the hash table and
    /// returns the entry.
    /// @note for non primitive types returns nullptr
    IRTypeSPtr getOrCreatePrimitive(IRTypeID aID);
    
  private:
    /**
     * A different hash table is used to store
     * instances of fcn types for fcn constraints.
     * The hash table has the scope size as key
     * and a list of lists of types as elements.
     * For example, the following types:
     * nq(1, true)
     * nq(1, 1)
     * plus(1, 1, 2)
     * will be stored as follows:
     * +---+  +------------------------+  +-------------------+
     * | 2 |->| <<int, bool>, ptr>     |->| <<int, int>, ptr> |
     * +---+  +------------------------+  +-------------------+
     * | 3 |->| <<int, int, int>, ptr> |
     * +---+  +------------------------+
     */
    
    /// struct containing the vector of fcn arguments and
    /// the type to return
    struct FcnConTypeElem {
      
      FcnConTypeElem()
      : fcnConType(nullptr)
      {
      }
      
      ~FcnConTypeElem() = default;
      
      bool match(const std::vector<IRTypeSPtr>& aOther)
      {
        for(std::size_t index=0; index<aOther.size(); ++index)
        {
          if(!argTypeVec[index]->isEqual(aOther[index].get())) return false;
        }
        return true;
      }
      
      /// return type
      IRTypeFunctionSPtr fcnConType;
      
      /// vector of arguments
      std::vector<IRTypeSPtr> argTypeVec;
    };
    
    typedef std::shared_ptr<FcnConTypeElem> FcnConTypePtr;
    typedef std::vector<FcnConTypePtr> MapConRegEntry;
    typedef std::size_t MapConRegKey;
    
    /// Hash table for constraint function types
    std::map<MapConRegKey, MapConRegEntry> pConTypeReg;
    
  private:
    /**
     * A different hash table is used to store
     * instances of array types.
     * The hash table has the base type of the array as key
     * and a list of lists of number of array entries as elements.
     * For example, the following types:
     * int a[2]
     * int b[3]
     * double c[1]
     * will be stored as follows:
     * +--------+  +----------+  +----------+
     * | int    |->| <2, ptr> |->| <3, ptr> |
     * +--------+  +----------+  +----------+
     * | double |->| <1, ptr> |
     * +--------+  +----------+
     */
    
    struct ArrayTypeElem {
      ArrayTypeElem()
      : arrayNumElements(0)
      , arrayBaseType(nullptr)
      , arrayType(nullptr)
      {
      }
      
      ~ArrayTypeElem() = default;
      
      bool match(const std::shared_ptr<IRType>& aBaseType, std::size_t aNumElem)
      {
        assert(aBaseType);
        return aNumElem == arrayNumElements &&
        aBaseType->isEqual(arrayBaseType.get());
      }
      
      std::size_t arrayNumElements;
      
      /// Array base type
      std::shared_ptr<IRType> arrayBaseType;
      
      /// Type to return
      IRTypeArraySPtr arrayType;
    };
    
    typedef std::shared_ptr<ArrayTypeElem> ArrayTypeElemPtr;
    typedef std::vector<ArrayTypeElemPtr>  ArrayTypeMapEntry;
    
    /// Hash table for array types
    std::map<int, ArrayTypeMapEntry> pArrayTypeReg;
  };
  
}// end namespace IR
