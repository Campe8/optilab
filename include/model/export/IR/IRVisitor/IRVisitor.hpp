//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 11/28/2017
//
// Visitor pattern, visiting IR objects.
//

#pragma once

#include "IRExportDefs.hpp"
#include "IRVisitorForward.hpp"

namespace IR {
  
  class IR_EXPORT_CLASS IRVisitor {
  public:
    REGISTER_VISIT(IRModule);
    REGISTER_VISIT(IRContext);
    REGISTER_VISIT(IRConDecl);
    REGISTER_VISIT(IRVarDecl);
    REGISTER_VISIT(IRExprStdVar);
    REGISTER_VISIT(IRExprVar);
    REGISTER_VISIT(IRExprAssign);
    REGISTER_VISIT(IRSymbol);
    REGISTER_VISIT(IRExprConst);
    REGISTER_VISIT(IRExprFcn);
    REGISTER_VISIT(IRDomDeclBounds);
    REGISTER_VISIT(IRDomDeclExt);
    REGISTER_VISIT(IRType);
    REGISTER_VISIT(IRSrcDecl);
    REGISTER_VISIT(IRExprRef);
    REGISTER_VISIT(IRExprMatrixSub);
    REGISTER_VISIT(IRSymbolConstArray);
  };
  
}// end namespace IR

