//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 11/28/2017
//
// Visitor pattern for pretty printing IR.
//

#pragma once

// Base class
#include "IRVisitor.hpp"

#include <string>
#include <sstream>
#include <ostream>

namespace IR {
  
  class IR_EXPORT_CLASS IRPrettyPrinter : public IRVisitor {
  public:
		IRPrettyPrinter();
    virtual ~IRPrettyPrinter() = default;
    
    UPLOAD_VISIT(IRModule);
    UPLOAD_VISIT(IRContext);
    UPLOAD_VISIT(IRConDecl);
    UPLOAD_VISIT(IRVarDecl);
    UPLOAD_VISIT(IRExprStdVar);
    UPLOAD_VISIT(IRExprVar);
    UPLOAD_VISIT(IRExprAssign);
    UPLOAD_VISIT(IRSymbol);
    UPLOAD_VISIT(IRExprConst);
    UPLOAD_VISIT(IRExprFcn);
    UPLOAD_VISIT(IRDomDeclBounds);
    UPLOAD_VISIT(IRDomDeclExt);
    UPLOAD_VISIT(IRType);
    UPLOAD_VISIT(IRSrcDecl);
    UPLOAD_VISIT(IRExprRef);
    UPLOAD_VISIT(IRExprMatrixSub);
    UPLOAD_VISIT(IRSymbolConstArray);

		/// Sends the content of the internal stream to the given stream
		void toStream(std::ostream& aOut);

	private:
		/// Internal stream
		std::stringstream pOS;

		/// Initializes internal stream
		virtual void initializeStream();

		/// Sends "aStr" to the internal stream
		void toOS(const std::string& aStr);

		/// Sends "aStr" to the internal stream and
		/// adds a new line at the end of "aStr"
		void toOSNL();
		void toOSNL(const std::string& aStr);
  };
  
}// end namespace IR

