//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 11/30/2017
//

#pragma once

#define REGISTER_VISIT(Class) virtual void visit(Class* aClass) = 0
#define UPLOAD_VISIT(Class) void visit(Class* aClass) override

// Forward declarations
namespace IR {
	class IRModule;
	class IRContext;
	class IRConDecl;
	class IRVarDecl;
	class IRDomDeclBounds;
	class IRDomDeclExt;
  class IRExprStdVar;
	class IRExprVar;
  class IRExprAssign;
	class IRSymbol;
	class IRType;
	class IRExprConst;
	class IRExprFcn;
	class IRSrcDecl;
  class IRExprRef;
  class IRExprMatrixSub;
  class IRSymbolConstArray;
}// end namespace IR

