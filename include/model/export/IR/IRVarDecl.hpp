//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/19/2017
//
// IRVarDecl is a node of the IR (tree) that
// represents variable declarations.
// For example, the statement:
// x:10..11;
// is encoded as a variable declaration
// where the lhs of the declaration ":" is an IRVarExpr,
// and the rhs is a DomainDecl node.
//


#pragma once

#include "IRNode.hpp"

#include "IRExprVar.hpp"
#include "IRDomDecl.hpp"

namespace IR {
  
  class IR_EXPORT_CLASS IRVarDecl : public IRNode {
	public:
    virtual ~IRVarDecl() = default;

    static bool isa(const IRNode* aNode);
    static IRVarDecl* cast(IRNode* aNode);
    static const IRVarDecl* cast(const IRNode* aNode);
    
    /// Sets lhs and rhs
    void setLhs(const std::shared_ptr<IRExprVar>& aVar);
    void setRhs(const std::vector<std::shared_ptr<IRDomDecl>>& aDom);
    
    /// Returns the number of (rhs) domain declarations
    std::size_t getNumRhs() const;
    
    /// Returns lhs and rhs
    std::shared_ptr<IRExprVar> lhs() const;
    std::shared_ptr<IRDomDecl> rhs(std::size_t aIdx) const;
    
    void acceptVisitor(IRVisitor* aVisitor) override;
    
  protected:
    friend class IRConstructionFacade;
    
		IRVarDecl(const std::shared_ptr<IRBlock>& aParentBlock);
  };
  
}// end namespace IR
