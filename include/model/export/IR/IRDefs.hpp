//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/11/2017
//

#pragma once

#include "ModelExportDefs.hpp"

namespace IR {
  
  enum IRObjectType : int {
		  IR_OBJ_MODULE = 0
    , IR_OBJ_CONTEXT
    , IR_OBJ_NODE
    , IR_OBJ_SYMBOL
    , IR_OBJ_TYPE
  };
  
  enum IRSymbolType : int {
      IR_SYMB_VAR = 0
    , IR_SYMB_CONST
    , IR_SYMB_STRUCT
  };
  
  enum IRNodeType : int {
		  IR_NODE_EXPR = 0
    , IR_NODE_VAR_DECL
    , IR_NODE_DOM_DECL
    , IR_NODE_CON_DECL
    , IR_NODE_SRC_DECL
  };
  
  enum IRExprType : int {
		  IR_EXPR_VAR = 0
    , IR_EXPR_STD_VAR
    , IR_EXPR_CONST
    , IR_EXPR_REF
    , IR_EXPR_ASSIGN
    , IR_EXPR_MAT_SUB
    , IR_EXPR_FCN
  };
  
  enum IRTypeID : int {
		  IR_TYPE_VOID = 0
    , IR_TYPE_DOUBLE
    , IR_TYPE_INTEGER
    , IR_TYPE_BOOLEAN
    , IR_TYPE_FUNCTION
    , IR_TYPE_STRUCT
    , IR_TYPE_ARRAY
    , IR_TYPE_POINTER
  };
  
}// end namespace IR

