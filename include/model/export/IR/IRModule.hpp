//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/11/2017
//
// An IR Module is the top level container for all other
// IR objects. Each module contains a list of global variables, functions, etc.
// This concecpt of module is very similar to the LLVM IR Module.
// For example, see
// http://llvm.org/doxygen/classllvm_1_1Module.html
//

#pragma once

// Base class
#include "IRObject.hpp"

// List of blocks in the module
#include "IRBlock.hpp"

// Symbol table for this module
#include "SymbolTable.hpp"

// Iterators
#include "PreOrderIterator.hpp"

#include <string>
#include <vector>
#include <unordered_set>
#include <memory>

namespace DSTree = DataStructure::Graph::Tree;

// Forward declarations
namespace IR {
  class IRContext;
  
  typedef std::shared_ptr<IRContext>IRContextSPtr;
  typedef std::unique_ptr<IRModule> IRModuleUPtr;
  typedef std::shared_ptr<IRModule> IRModuleSPtr;
}// end namespace IR

namespace IR {
  
  class IR_EXPORT_CLASS IRModule : public IRObject {
  public:
    using ChildrenList = std::vector<IRModuleSPtr>;
    using ModuleIter = ChildrenList::iterator;
    using ModuleCIter = ChildrenList::const_iterator;
    using ModuleRIter = ChildrenList::reverse_iterator;
    using ModuleCRIter = ChildrenList::const_reverse_iterator;
    using ModulePreOrderIter = _private::PreOrderIterator<IRModuleSPtr>;
    
  public:
    virtual ~IRModule() = default;
    
    /// Returns iterators to pre-order visit of "aModule"
    /// and its sub-modules
    static ModulePreOrderIter preOrderBegin(const IRModuleSPtr& aModule);
    static ModulePreOrderIter preOrderEnd();
    
    /// Returns true if this is the global module,
    /// i.e., the "model" module root of the IR.
    /// Returns false otherwise
    bool isGlobalModule() const
    {
      return getParent() == nullptr;
    }
    
    /// Returns the name of this scope
    inline std::string getName() const
    {
      return pModuleName;
    }
    
    /// Returns the context this module belongs to
    inline IRContextSPtr getContext() const
    {
      return pContext;
    }
    
    /// Returns the pointer to the parent module
    inline IRModuleSPtr getParent() const
    {
      return pParent;
    }
    
    inline std::shared_ptr<SymbolTable> getSymbolTable() const
    {
      return pSymbTable;
    }
    
    /// Adds the given IRModule as a child of this module
    void addChild(const IRModuleSPtr& aChild);
    
    /// Returns true if "aChild" is a (non direct) child of this module
    bool contains(const IRModuleSPtr& aChild) const;
    
    /// Returns true if a module with name "aChildName"
    /// is a (non direct) child of this module.
    /// @note returns true for first child that matches
    bool contains(const std::string& aChildName) const;
    
    /// Returns a pointer to the sub module (may be nested) with
    /// name "aName".
    /// Returns nullptr if there is no such module
    IRModuleSPtr getSubModule(const std::string& aName) const;
    
    /// Returns the number of direct children of this module
    inline std::size_t numChildren() const
    {
      return pChildren.size();
    }
    
    /// Return the block hold by this module
    inline std::shared_ptr<IRBlock> getBlock() const
    {
      return pBlock;
    }
    
    /// Range-based for loop
    ModuleIter begin();
    ModuleCIter begin() const;
    
    ModuleIter end();
    ModuleCIter end() const;
    
    /// Range-based for loop
    ModuleRIter rbegin();
    ModuleCRIter rbegin() const;
    
    ModuleRIter rend();
    ModuleCRIter rend() const;
    
    void acceptVisitor(IRVisitor* aVisitor) override;
    
  private:
    friend class IRConstructionFacade;
    
    IRModule(const IRContextSPtr& aCtx, const std::string& aModuleName, const IRModuleSPtr& aParent = nullptr);
    
    inline void setBlock(const std::shared_ptr<IRBlock>& aBlock)
    {
      pBlock = aBlock;
    }
    
  private:
    /// Name of this scope
    std::string pModuleName;
    
    /// Context this module belongs to
    IRContextSPtr pContext;
    
    /// Parent scope
    IRModuleSPtr pParent;
    
    /// Symbol table
    SymbolTable::SymbTablePtr pSymbTable;
    
    /// Lexical, syntactical and semantical structure of model
    /// which is grouped together and lives in this scope.
    /// @note In the C family blocks are delimited by
    /// curly braces - "{" and "}"
    std::shared_ptr<IRBlock> pBlock;
    
    /// List of children of this scope
    ChildrenList pChildren;
  };
  
  /// Lookup "aSymb" in "aMod" and bubble up the query to the parents if not found.
  /// If "aSymb" can be found, returns nullptr
  IR_EXPORT_FUNCTION SymbolTable::SymbolTableEntrySPtr lookupSymbol(const IRModuleSPtr& aMod,
                                                                    const IRSymbol::SymbolName& aSymb);
  
  /// Returns true if "aMod" is a leaf module, false otherwise.
  /// @note returns true if "aMod" is nullptr
  IR_EXPORT_FUNCTION bool isLeafModule(const IRModuleSPtr& aMod);
  
  /// Returns a vector of all the leaves of "aMod".
  /// @note if "aMod" is a leaf, returns "aMod"
  IR_EXPORT_FUNCTION std::vector<IRModuleSPtr> getLeaves(const IRModuleSPtr& aMod);
  
  /// Returns true if "aMod" has an empty block, i.e., if there are
  /// not stmts added in the given module
  IR_EXPORT_FUNCTION bool hasEmptyBlock(const IRModuleSPtr& aMod);
  
}// end namespace IR
