//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 08/21/2017
//

#pragma once

#include "ModelExportDefs.hpp"

#include "IRSymbolFactory.hpp"

#include "IRInc.hpp"

namespace IR {
  
  class IR_EXPORT_CLASS IRConstructionFacade {
  public:
    using BlockPtr = std::shared_ptr<IRBlock>;
    using BlockPtrRef = std::shared_ptr<IRBlock>&;
    using BlockCPtr = const std::shared_ptr<IRBlock>&;
    using SymbolFactoryPtr = std::shared_ptr<IRSymbolFactory>;
    using IRExprSPtr = IRExprFcn::IRExprSPtr;
    
  public:
    /// Constructor used to initialize an "empty" facade,
    /// i.e., a facade which is meant to be used by different blocks
    /// by setting themselves using the "setBlock" method
    IRConstructionFacade();
    
    /// Constructor takes one block and uses it
    /// to construct nodes and module.
    /// All the nodes constructed by this constructor facade
    /// will have "aBlock" as base block (or the block provided
    /// to the "setBlock" method).
    /// @note if this constructor is used to construct different modules
    /// they all will have the same block which may result
    /// in inconsistencies in IR.
    /// Use the "setBlock" method to set a different block.
    IRConstructionFacade(BlockCPtr aBlock);
    
    ~IRConstructionFacade() = default;
    
    /// Returns a stmt node generated from the given node
    IRBlock::IRStmtSPtr stmt(const IRNode::IRNodeSPtr& aNode);
    
    /// Sets "aBlock" as new block for this constructor facade.
    /// Returns the current block.
    /// If "aBlock" is not nullptr, sets also the module it links to
    /// to the internal factories, for example, symbol factory
    BlockPtr setBlock(BlockCPtr aBlock);
    
    inline SymbolFactoryPtr symbolFactory() const
    {
      return pSymbolFactory;
    }
    
    /// Returns a module with name "aName" and parent "aParent".
    /// @note "aParent" can be null.
    /// @note this method sets the current block as module's block.
    /// Use "setBlock" method to set current internal block
    IRModuleSPtr module(const IRContextSPtr& aCtx, const std::string& aName, const IRModuleSPtr& aParent);
    
    /// Returns an assignment expression.
    /// The LHS must be an lvalue.
    IRExprAssignSPtr exprAssign(const IRExprSPtr& aLHS, const IRExprSPtr& aRHS);
    
    /// Returns an expression for a standard var built on "aSVar"
    IRExprStdVarSPtr exprStdVar(IRExprVar::SymbVarSPtrCRef aSVar);
    
    /// Returns an expression var built on "aSVar"
    IRExprVarSPtr exprVar(IRExprVar::SymbVarSPtrCRef aSVar);
    
    /// Returns an expression const built on "aSConst"
    IRExprConsSPtr exprConst(IRExprConst::SymbConstSPtrCRef aSConst);
    
    /// Returns a domain declaration for bound domain
    IRDomDeclBoundsSPtr domDeclBounds(const IRExprConsSPtr& aLowerBound, const IRExprConsSPtr& aUpperBound);
    
    /// Return a domain declaration for Boolean domain
    IRDomDeclBoundsSPtr domDeclBoolean();
    IRDomDeclBoundsSPtr domDeclBoolean(bool aSingVal);
    
    /// Returns a domain declaration for extensional domain
    IRDomDeclExtSPtr domDeclExt(const std::set<IRExprConsSPtr>& aSet);
    
    /// Returns a variable declaration node
    IRVarDeclSPtr varDecl(const IRExprVarSPtr& aVar, const std::vector<IRDomDeclSPtr>& aDomDecl);
    
    /// Returns a function expression from "aSFcn" without arguments which must be set later
    IRExprFcnSPtr exprFcn(IRExprFcn::SymbFcnSPtrCRef aSFcn);
    
    /// Returns a function expression from "aSFcn" with the ordered arguments in "aArgs"
    IRExprFcnSPtr exprFcn(IRExprFcn::SymbFcnSPtrCRef aSFcn, const std::vector<IRExprFcn::IRExprSPtr>& aArgs);
    
    /// Returns a reference expression from "aRef"
    IRExprRefSPtr exprRef(const std::shared_ptr<IRExpr>& aRefExpr);
    
    /// Returns a matrix subscript expression
    IRExprMatrixSubSPtr exprMatrixSub(const std::shared_ptr<IRExpr>& aMatrix, const std::shared_ptr<IRExpr>& aSubscript);
    
    /// Returns a constraint declaration node (with an ID if specified)
    IRConDeclSPtr conDecl(const IRExprFcnSPtr& aFcn);
    IRConDeclSPtr conDecl(const IRExprFcnSPtr& aFcn, std::string& aCID);
    
    /// Returns a search declaration node identified by "aID" if any
    IRSrcDeclSPtr srcDecl();
    IRSrcDeclSPtr srcDecl(const std::string& aID);
    
  private:
    /// Block used to construct nodes
    BlockPtr pBlock;
    
    /// Symbol factory
    std::shared_ptr<IRSymbolFactory> pSymbolFactory;
  };
  
}// end namespace IR
