//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/20/2017
//
// IRSrcDecl is a node of the IR (tree) that
// represents search declarations.
// For example, the statement:
// search(first_fail, indomain_min)
// is encoded as a search declaration.
//


#pragma once

#include "IRNode.hpp"

#include "IRExprVar.hpp"

#include <cstddef>  // for std::size_t
#include <memory>
#include <string>
#include <vector>

// Forward declarations
namespace Model {
  class SearchInformation;
}//end namespace Model

namespace IR {
  
  class IR_EXPORT_CLASS IRSrcDecl : public IRNode {
  public:
		enum SearchType : int {
			  ST_DFS = 0
      , ST_GENETIC
			, ST_UNDEF
		};
    
  public:
    virtual ~IRSrcDecl() = default;
    
    static bool isa(const IRNode* aNode);
    static IRSrcDecl* cast(IRNode* aNode);
    static const IRSrcDecl* cast(const IRNode* aNode);
   
    /// Sets a string ID for this search declaration
    inline void setID(const std::string& aID) { pID = aID; }
    
    /// Returns the string ID of this search declaration
    inline std::string getID() const { return pID; }
    
    /// Sets the search information instance
    inline void setSearchInformation(const std::shared_ptr<Model::SearchInformation>& aInfoPtr)
    {
      pSrcInfoPtr = aInfoPtr;
    }
    
    /// Returns the pointer to the search information instace
    inline std::shared_ptr<Model::SearchInformation> getSearchInformation() const { return pSrcInfoPtr; }
    
    /// Adds a semantic dependent variable
    inline void addSemanticDependentVar(const std::string& aID) { pVarSemanticIDList.push_back(aID); }
    
    /// Returns the list of semantic depents variables
    inline const std::vector<std::string>& getSemanticDependentVarList() const { return pVarSemanticIDList; }
    
    /// Sets the (ordered) list of branching variables
    void setBranchingVarList(const std::vector<std::shared_ptr<IRExprVar>>& aBranchingList);
    
    /// Returns the number of branching variables
    std::size_t numBranchingVars() const;
    
    /// Returns the "aIdx" branching variable.
    /// @note "aIdx" is 0-based
    std::shared_ptr<IRExprVar> getBranchingVar(std::size_t aIdx) const;
    
    /// Sets/returns the search type
    inline void setSearchType(SearchType aSearchType) { pSrcType = aSearchType; }
		inline SearchType getSearchType() const { return pSrcType; }
    
    /// Accept visitor method on this IRNode
    void acceptVisitor(IRVisitor* aVisitor) override;
    
  protected:
    friend class IRConstructionFacade;
    
		IRSrcDecl(const std::shared_ptr<IRBlock>& aParentBlock);
    
  private:
    /// Search declaration ID
    std::string pID;
    
		/// Search type
		SearchType pSrcType;
    
    /// List of all the variable IDs that have a semantic
    /// that depends upon this search declaration
    std::vector<std::string> pVarSemanticIDList;
    
    /// Pointer to the information instance
    std::shared_ptr<Model::SearchInformation> pSrcInfoPtr;
  };
  
}// end namespace IR
