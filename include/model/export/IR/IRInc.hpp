//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 08/21/2017
//

#pragma once

#include "IRBlock.hpp"
#include "IRModule.hpp"
#include "IRExprStdVar.hpp"
#include "IRExprAssign.hpp"
#include "IRExprVar.hpp"
#include "IRExprConst.hpp"
#include "IRDomDeclBounds.hpp"
#include "IRDomDeclExt.hpp"
#include "IRVarDecl.hpp"
#include "IRConDecl.hpp"
#include "IRExprFcn.hpp"
#include "IRExprRef.hpp"
#include "IRSrcDecl.hpp"
#include "IRExprMatrixSub.hpp"

#include <memory>

namespace IR {

	typedef std::shared_ptr<IRBlock> IRBlockSPtr;
  typedef std::shared_ptr<IRExprStdVar> IRExprStdVarSPtr;
  typedef std::shared_ptr<IRExprAssign> IRExprAssignSPtr;
	typedef std::shared_ptr<IRExprVar> IRExprVarSPtr;
	typedef std::shared_ptr<IRExprConst> IRExprConsSPtr;
  typedef std::shared_ptr<IRDomDecl> IRDomDeclSPtr;
  typedef std::shared_ptr<IRDomDeclBounds> IRDomDeclBoundsSPtr;
  typedef std::shared_ptr<IRDomDeclExt> IRDomDeclExtSPtr;
  typedef std::shared_ptr<IRVarDecl> IRVarDeclSPtr;
  typedef std::shared_ptr<IRConDecl> IRConDeclSPtr;
  typedef std::shared_ptr<IRExprFcn> IRExprFcnSPtr;
  typedef std::shared_ptr<IRExprRef> IRExprRefSPtr;
  typedef std::shared_ptr<IRSrcDecl> IRSrcDeclSPtr;
  typedef std::shared_ptr<IRExprMatrixSub> IRExprMatrixSubSPtr;
  
}// end namespace IR
