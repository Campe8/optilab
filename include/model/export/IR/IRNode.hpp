//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/11/2017
//
// IRNode is a node of the IR (tree).
// An IRNode contains some data, i.e., other IRObjects
// and has a semantic meaning.
//


#pragma once

#include "IRObject.hpp"

#include "TreeNode.hpp"

#include <vector>
#include <memory>

namespace DSTree = DataStructure::Graph::Tree;

// Forward declarations
namespace IR {
  class IRBlock;
}// end namespace IR

namespace IR {
  
  class IR_EXPORT_CLASS IRNode : public IRObject {
  public:
    typedef std::shared_ptr<IRNode> IRNodeSPtr;
    typedef DSTree::TreeNode<IRNodeSPtr> IRStmtNode;
    typedef std::shared_ptr<IRStmtNode> IRStmtNodeSPtr;
    
  public:
    virtual ~IRNode() = default;
    
    /// Returns a pointer to the IRStmtNode containing "aNode"
    static IRStmtNodeSPtr StmtNode(const IRNodeSPtr& aNode);
    
    /// Returns the type of this node
    inline IRNodeType getNodeType() const
    {
      return pNodeType;
    }
    
    /// Sets the parent node
    inline void setParent(const IRNodeSPtr& aParent)
    {
      pParent = aParent;
    }
    
    /// Returns the parent node
    inline IRNodeSPtr getParent() const
    {
      return pParent;
    }
    
    /// Returns the stmt node containing this node
    inline IRStmtNodeSPtr getStmtNode() const
    {
      return pStmtNode;
    }
    
    /// Sets the pointer to the block containing this node
    void setBlock(const std::shared_ptr<IRBlock>& aBlock);
    
    /// Returns the pointer to the block containing this node
    inline std::shared_ptr<IRBlock> getBlock() const
    {
      return pBlock;
    }
    
    /// Returns the number of contained objects
    inline std::size_t numContainedObj() const
    {
      return pContainedObj.size();
    }
    
    /// Returns the "aIdx"th object or nullptr if there is
    /// no such object
    std::shared_ptr<IRObject> getContainedObj(std::size_t aIdx) const;
    
  protected:
    IRNode(const std::shared_ptr<IRBlock>& aParentBlock, IRNodeType aNodeType);
    
    /// Adds an object to the contained object vector.
    /// If "aIdx" is unspecified, add an object at the end of the
    /// current list of object.
    /// If "aIdx" >= 0, reserve enough space in the list and store "aObj" at
    /// the given index
    void addContainedObject(const std::shared_ptr<IRObject>& aObj, int aIdx = -1);
    
    /// Adds "aNode" as the "aIdx" child of this (stmt) node.
    /// If "aIdx" >= numChildren(), add aIdx - numChildren() nullptr
    /// and then add "aNode".
    /// @note is in position "aIdx" there is already a node, replace it with "aNode"
    void addChild(const std::shared_ptr<IRNode>& aNode, std::size_t aIdx);
    
    /// Returns the "aIdx" child of this node
    std::shared_ptr<IRNode> getChild(std::size_t aIdx) const;
    
  private:
    typedef std::shared_ptr<IRBlock> BlockPtr;
    typedef std::shared_ptr<IRObject> ObjPtr;
    typedef std::vector<ObjPtr> ObjContainer;
    
  private:
    /// Type of this node
    IRNodeType pNodeType;
    
    /// Parent node
    IRNodeSPtr pParent;
    
    /// Pointer to the statement node containing this node
    IRStmtNodeSPtr pStmtNode;
    
    /// Block this node belongs to
    BlockPtr pBlock;
    
    /// Vector of the objects contained in this node
    ObjContainer pContainedObj;
  };
  
}// end namespace IR
