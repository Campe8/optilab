//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/27/2017
//

#pragma once

#include "ModuleTransform.hpp"

namespace IR {
	namespace Transforms {

		/**
		 * Set the type for constraint parameters, i.e., constraint scope
     * if not already set.
     * For example, for reif constraints, set the reif const expr,
     * if any, to Boolean instead of int.
		 */
		class MODEL_EXPORT_CLASS ConstraintScopeTypeInference : public ModuleTransform {
		public:
			ConstraintScopeTypeInference() = default;
			virtual ~ConstraintScopeTypeInference() = default;

			std::string transformName() const override;
		protected:
			void transformModule(std::shared_ptr<IRModule>& aModule) const override;
		};

}// end namespace Transforms
}// end namespace IR
