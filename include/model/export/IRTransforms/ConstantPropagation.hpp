//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 12/05/2017
//

#pragma once

#include "ModuleTransform.hpp"

namespace IR {
	namespace Transforms {

		/**
		 * Perform constant propagation.
     * For more information see
     * https://en.wikipedia.org/wiki/Constant_folding
		 */
		class MODEL_EXPORT_CLASS ConstantPropagation : public ModuleTransform {
		public:
			ConstantPropagation() = default;
			virtual ~ConstantPropagation() = default;

			std::string transformName() const override;
		protected:
			void transformModule(std::shared_ptr<IRModule>& aModule) const override;
		};

}// end namespace Transforms
}// end namespace IR
