//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/27/2017
//

#pragma once

#include "ModelExportDefs.hpp"

#include "IRContext.hpp"
#include "ModuleTransform.hpp"

#include <boost/core/noncopyable.hpp>

#include <vector>
#include <utility>
#include <string>
#include <functional>

namespace IR {
  namespace Transforms {
    
    class MODEL_EXPORT_CLASS TransformEngine : private boost::noncopyable {
    public:
      TransformEngine();
      virtual ~TransformEngine() = default;
      
      /// Sets debug, if true run pretty printer
      /// on each transformation
      inline void setDebug(bool aDebug)
      {
        pDebug = aDebug;
      }
      
      /// Sets the module the transforms will run on
      inline void setContext(const std::shared_ptr<IR::IRContext>& aCtx)
      {
        pCtx = aCtx;
      }
      
      /// Registers "aTransform".
      /// @note this transformation can be registered multiple times
      /// and its registration determines its execution order
      void registerModuleTransform(const std::shared_ptr<ModuleTransform>& aTransform);
      
      /// Runs the registered transforms
      void runTransforms();
      
    private:
      using TransformHandler = std::function<void(std::shared_ptr<IRModule>&)>;
      using TransformHandlerRegister = std::vector<TransformHandler>;
      
      using TransformPtr = std::shared_ptr<ModuleTransform>;
      using TransformInfo = std::pair<std::string, TransformPtr>;
      using TransformRegister = std::vector<TransformInfo>;
      using TransformDriver = std::pair<TransformHandlerRegister, TransformRegister>;
      
    private:
      /// Debug flag
      bool pDebug;
      
      /// Register of IR transforms
      TransformDriver pTransformEngine;
      
      /// IR context containing the modules the transforms run on
      std::shared_ptr<IR::IRContext> pCtx;
      
      /// Initializes the engine registering all the module transforms to run
      void initialzieTransformEngine();
    };
    
  }// end namespace Transforms
}// end namespace IR
