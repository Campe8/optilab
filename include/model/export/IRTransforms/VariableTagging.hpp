//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 12/11/2017
//

#pragma once

#include "ModuleTransform.hpp"

namespace IR {
	namespace Transforms {

		/**
		 * Tag variables:
		 * - branching, w.r.t. the search module
		 * - reachable, w.r.t. the constraint graph
		 */
		class MODEL_EXPORT_CLASS VariableTagging : public ModuleTransform {
		public:
			VariableTagging() = default;
			virtual ~VariableTagging() = default;

			std::string transformName() const override;
		protected:
			void transformModule(std::shared_ptr<IRModule>& aModule) const override;
		};

  }// end namespace Transforms
}// end namespace IR
