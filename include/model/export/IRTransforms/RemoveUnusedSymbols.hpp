//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 18/05/2018
//

#pragma once

#include "ModuleTransform.hpp"

namespace IR {
	namespace Transforms {

		/**
		 * Remove unused symbols and assignment expressions in arameter module.
		 */
		class MODEL_EXPORT_CLASS RemoveUnusedSymbols : public ModuleTransform {
		public:
			RemoveUnusedSymbols() = default;
			virtual ~RemoveUnusedSymbols() = default;

			std::string transformName() const override;
      
		protected:
			void transformModule(std::shared_ptr<IRModule>& aModule) const override;
		};

}// end namespace Transforms
}// end namespace IR
