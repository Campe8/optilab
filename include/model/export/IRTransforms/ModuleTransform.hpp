//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/27/2017
//

#pragma once

#include "ModelExportDefs.hpp"

#include "IRModule.hpp"

#include <string>

namespace IR {
	namespace Transforms {

		class MODEL_EXPORT_CLASS ModuleTransform {
		public:
			ModuleTransform() = default;
			virtual ~ModuleTransform() = default;

			/// Applies this transformation to the given module
			void apply(std::shared_ptr<IRModule>& aModule) const;

			/// Returns transform's name
			virtual std::string transformName() const = 0;

		protected:
			virtual void transformModule(std::shared_ptr<IRModule>& aModule) const = 0;
		};

}// end namespace Transforms
}// end namespace IR
