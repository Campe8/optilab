//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 12/01/2017
//

#pragma once

#include "ModelExportDefs.hpp"

#include "IRContext.hpp"
#include "ModuleTransform.hpp"

#include <boost/core/noncopyable.hpp>

#include <vector>
#include <utility>
#include <string>
#include <functional>

// Used by the transform engine
#define LOG_XFORM(xform_name, xform_idx) logIR(xform_name, xform_idx, glbModule, pDebug);

namespace IR {
	namespace Transforms {
		
		// Prints "aModule" on the file "aFileName"
		MODEL_EXPORT_FUNCTION void logTransform(const std::string& aFileName, const std::shared_ptr<IRModule>& aModule);

}// end namespace Transforms
}// end namespace IR
