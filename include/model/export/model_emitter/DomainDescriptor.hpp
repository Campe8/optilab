//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 5/13/2018
//
// Descriptor class for variable domains.
// It describes the domains that will be generated
// according to their IR representation.
//

#pragma once

#include "ModelExportDefs.hpp"
#include "VariableDefs.hpp"

#include <vector>

namespace Model {
  
  template<typename T>
  class DomainDescriptor {
  public:
    enum DomainRepresentation {
        SINGLETON = 0
      , BOUNDS
      , LIST
    };
    
  public:
    /// Returns the representation (see enum) of the domain
    inline void setDomainRepresentaton(DomainRepresentation aDomRep) { pDomainRep = aDomRep; }
    inline DomainRepresentation getDomainRepresentation() const { return pDomainRep; }
    
    /// Returns the number of domain elements stored in the vector of domain elements.
    /// @note this does not represent the actual number of elements in the domain.
    /// For example, if representation is BOUNDS and the domain is [1, 10]
    /// the actual number of elements is 10 but this function will return 2 (1 and 10)
    const std::size_t getNumDomainElements() const { return pDomainElements.size(); }
    
    /// Returns the vector of domain elements
    void addDomainElement(T aElement) { pDomainElements.push_back(aElement); }
    const std::vector<T>& getDomainElements() const { return pDomainElements; }
    
    /// Returns the type of the domain represented as VariableTypeClas
    void setDomainType(Core::VariableTypeClass aType) { pType = aType; }
    Core::VariableTypeClass getDomainType() const { return pType; }
    
  private:
    /// Domain representation
    DomainRepresentation pDomainRep;
    
    /// Domain elements
    std::vector<T> pDomainElements;
    
    /// Type of the elements in the domain,
    /// which defines the type of the domain
    Core::VariableTypeClass pType;
  };

}// end namespace Model
