//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 5/16/2018
//
// Model generator: IR back-end generator.
// Transforms the input IR into a model
// ready to run on the back-end solver.
//

#pragma once

#include "ModelExportDefs.hpp"

// Model
#include "Model.hpp"

// IR
#include "IRContext.hpp"

// Generator for model objects
#include "ModelObjectGenerator.hpp"

#include <vector>
#include <utility>
#include <memory>

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelGenerator {
  public:
		ModelGenerator();
    
    virtual ~ModelGenerator() = default;
    
		/// Generates the target model from the given IR context
		ModelSPtr generateModel(const IR::IRContextSPtr& aIRCtx);
    
  private:
    using ObjectGenerator = std::shared_ptr<ModelObjectGenerator>;
    using ObjectGeneratorMap = std::vector<ObjectGenerator>;
    
  private:
    /// Map of object generators
    ObjectGeneratorMap pGeneratorMap;
    
    /// Ordered list of key for the generator map.
    /// The list contains pairs of
    /// <ModelObjectClass, idx>
    /// where idx is the index in the map of the generator
    /// to use to generate objects of class ModelObjectClass
    std::vector<std::pair<int, std::size_t>> pGeneratorKeyMap;
    
    /// Registers the object generator into the map
    void registerGenerators();
    
    /// Parse the context and generate model objects.
    /// Model objects are added to the model
    void addModelObjectsToModel(ModelSPtr& aModel, const IR::IRContextSPtr& aCtx);
    
    /// Returns the context module to use when parsing IR w.r.t. the given class of model object to create
    ObjectGenerator getGeneratorGivenObjectClass(ModelObjectClass aObjectClass);
    
    /// Returns the vector of model objects generated from the given module and corresponding to the
    /// given class of model objects.
    /// @note the IR module must be the global module
    std::vector<ModelObject*> generateObjectsGivenModelObjectClass(ModelObjectClass aObjectClass, const IR::IRModuleSPtr& aModule, const Model* aModel);
  };
  
}// end namespace Model
