//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 09/06/2017
//
// Generator of Modelvariable objects.
// It generates a model variable object from an IRNode
// of type IRVarDecl.
//

#pragma once

#include "ModelObjectGenerator.hpp"

#include "DomainDescriptor.hpp"
#include "VariableFactory.hpp"

namespace Model {

	class MODEL_EXPORT_CLASS VariableGenerator : public ModelObjectGenerator {
	public:
    VariableGenerator();
    
		virtual ~VariableGenerator() = default;

		ModelObject* generateObject(IR::IRNode* aNode, const Model* aModel) override;

  private:
    /// Factory for variables
    Core::VariableFactoryUPtr pVarFactory;
    
    /// Returns the factory for variables
    inline Core::VariableFactory& getVarFactory() { return *pVarFactory; }
	};

}// end namespace Model
