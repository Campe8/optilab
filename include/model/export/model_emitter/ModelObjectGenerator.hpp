//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 09/06/2017
//
// Base class for model object generators:
// IRNode --> ModelObject
//

#pragma once

#include "ModelExportDefs.hpp"

#include "ModelDefs.hpp"
#include "IRNode.hpp"

// Forward declarations
namespace Model {
  class Model;
	class ModelObject;
}// end namespace Model

namespace Model {

	class MODEL_EXPORT_CLASS ModelObjectGenerator {
	public:
		virtual ~ModelObjectGenerator() = default;
    
    /// Generate a model object from its IR representation and information
    /// contained in the model
		virtual ModelObject* generateObject(IR::IRNode* aNode, const Model* aModel) = 0;

	protected:
		ModelObjectGenerator() = default;
	};

}// end namespace Model
