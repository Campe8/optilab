//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 09/06/2017
//
// Generator of ModelParameter objects.
// It generates a model parameter object from an IRExpressions.
//

#pragma once

#include "ModelObjectGenerator.hpp"

#include "ConstraintParameterFactory.hpp"

namespace Model {

	class MODEL_EXPORT_CLASS ParameterGenerator : public ModelObjectGenerator {
	public:
    ParameterGenerator();
    
		virtual ~ParameterGenerator() = default;

		ModelObject* generateObject(IR::IRNode* aNode, const Model* aModel) override;

  private:
    /// Factory for constraints parameters
    std::unique_ptr<Core::ConstraintParameterFactory> pParamFactory;
	};

}// end namespace Model
