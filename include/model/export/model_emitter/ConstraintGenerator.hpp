//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 09/06/2017
//
// Generator of ModelConstraint objects.
// It generates a model constraint object from an IRNode
// of type IRConDecl.
//

#pragma once

#include "ModelObjectGenerator.hpp"

// Factories
#include "ConstraintParameterFactory.hpp"
#include "ConstraintFactory.hpp"

#include "IRExpr.hpp"

namespace Model {

	class MODEL_EXPORT_CLASS ConstraintGenerator : public ModelObjectGenerator {
	public:
    ConstraintGenerator();
    
		virtual ~ConstraintGenerator() = default;

		ModelObject* generateObject(IR::IRNode* aNode, const Model* aModel) override;

  private:
    /// Factory for constraints parameters
    std::unique_ptr<Core::ConstraintParameterFactory> pParamFactory;
    
    /// Factory for constraints
    std::unique_ptr<Core::ConstraintFactory> pConstraintFactory;
    
    /// Returns the factory for variables
    inline Core::ConstraintParameterFactory& getParamFactory() { return *pParamFactory; }
    
    /// Returns the factory for constraints
    inline Core::ConstraintFactory& getConstraintFactory() { return *pConstraintFactory; }
    
    /// Generate an instace of a constraint
    Core::ConstraintSPtr generateConstraint(Core::ConstraintId aConID,
                                            const std::vector<std::shared_ptr<Core::ConstraintParameter>>& aScope,
                                            Core::PropagatorStrategyType aPropType,
                                            bool aOptimization);
	};

  /// Returns the constraint parameter descriptor corresponding for the given IR expression
  MODEL_EXPORT_FUNCTION std::shared_ptr<Core::ConstraintParameter>
  getConstraintParameter(const std::shared_ptr<IR::IRExpr>& aExpr, const Model* aModel,
                         Core::ConstraintParameterFactory& aCPF);
  
}// end namespace Model
