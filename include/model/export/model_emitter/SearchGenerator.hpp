//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 09/06/2017
//
// Generator of ModelSearch objects.
// It generates a model search object from an IRNode
// of type IRSrcDecl.
//

#pragma once

#include "ModelObjectGenerator.hpp"

namespace Model {

	class MODEL_EXPORT_CLASS SearchGenerator : public ModelObjectGenerator {
	public:
    SearchGenerator() = default;
    
		virtual ~SearchGenerator() = default;

		ModelObject* generateObject(IR::IRNode* aNode, const Model* aModel) override;
	};

}// end namespace Model
