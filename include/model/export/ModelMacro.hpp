//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 04/19/2017
//

#pragma once

// Macros for AST Model Abstract Syntax Tree
#include "ModelASTMacro.hpp"
#include "ModelDefs.hpp"

#include <string>
#include <exception>

#include <boost/exception/all.hpp>

// Macros for Model evaluator and interpreter
#define MDL_EVAL_DEFAULT_CTX_DELIM '\n'

// Macros for JSON Model Abstract Syntax Tree Error handling
#define MODEL_ABSTRACT_SYNTAX_TREE_ERROR(aStringError) do {      \
throw std::invalid_argument("Model Abstract Syntax Tree ERROR: " #aStringError);  \
} while (0)

#define MODEL_ASSERT_MSG(expr, msg) \
((expr) ? (void)0 : Model::throwLogicErrorWithMessage((msg)))

#define CTX_TRY_BLOCK try {
#define CTX_CATCH_BLOCK(aMsg)  } catch (...) { \
assertAndThrow(false, aMsg); }

#define model_exception boost::exception
#define model_get_exception_info(e) *boost::get_error_info<model_errmsg_info>(e)
#define model_throw(aMsg) throw ModelException{aMsg}

#define model_assert(expr) \
((expr) ? (void)0 : model_throw(("")))

#define model_assert_msg(expr, msg) \
((expr) ? (void)0 : model_throw((msg)))

using model_errmsg_info = boost::error_info<struct tag_errmsg, std::string>;

namespace Model {
  
  struct ModelException : public boost::exception, public std::exception {
    ModelException(const std::string& aMsg)
    : pMsg(aMsg)
    {
    }
    
    const char *what() const noexcept { return pMsg.c_str(); }
    
  private:
    /// Exception message
    std::string pMsg;
  };
  
}// end namespace Model
