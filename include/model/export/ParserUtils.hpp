//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 12/03/2017
//
// Utilities for parsing: context to IR.
//

#pragma once

#include "ModelExportDefs.hpp"

#include "DataObject.hpp"
#include "IRModule.hpp"
#include "IRConstructionFacade.hpp"

namespace Model { namespace Utils { namespace Parser {
  
  /// Returns the ID of a variable given the corresponding DataObject
  MODEL_EXPORT_FUNCTION std::string getVarIDFromVarObj(const ::Interpreter::DataObject& aDO);
  
  // Returns the IR expression encoding the given data object that will be defined in the constraint module "aMod"
  MODEL_EXPORT_FUNCTION std::shared_ptr<IR::IRExpr> getExprForDataObject(const ::Interpreter::DataObject& aArg,
                                                                         const IR::IRModuleSPtr& aMod,
                                                                         ::IR::IRConstructionFacade& aCF);
  
  /// Returns the IR symbol linked to the IR expression representing the given data object.
  /// @note the module is the global module
  MODEL_EXPORT_FUNCTION IR::IRSymbolFactory::IRSymbolPtr getSymbolForDataObject(const ::Interpreter::DataObject& aDO,
                                                                                const ::IR::IRModuleSPtr& aMod,
                                                                                ::IR::IRConstructionFacade& aCF);
  
  /// Returns the primitive DataObject from a primitive (returns itself) or a scalar data object
  MODEL_EXPORT_FUNCTION const ::Interpreter::DataObject&
  getDataObjectFromPrimitiveOrScalarType(const ::Interpreter::DataObject& aDO);
  
}}} // end namespace Model/Utils/Parser
