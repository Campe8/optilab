//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 10/08/2019
//
// Model context-specific parser: search context.
// Generates the IR for any search statement.
// @todo port branch parser under search parser.
//

#pragma once

// Base class
#include "ModelParserCtx.hpp"

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelParserCtxSearch : public ModelParserCtx {
  public:
    virtual ~ModelParserCtxSearch() = default;

		/// Parses the model context "aMdlCtx" and generates the corresponding IR module in "aMod".
		/// For example:
		/// Model Context Variable --> IR Module Variable
		void parseModelContext(const ModelContext& aCtx, const IR::IRModuleSPtr& aMod) override;

	protected:
		friend class ModelParser;

		ModelParserCtxSearch();

	private:
    /// Adds a default branch declaration node into the IR "search" module
    void addDefaultIRSearchDeclNode(const IR::IRModuleSPtr& aMod);
    
    /// Adds a search declaration node into the IR "search" module
    void addIRSearchDeclNode(const ModelContext::CtxObjSPtr& aSrc, const IR::IRModuleSPtr& aMod);
  };
  
}// end namespace Model
