//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 04/12/2017
//
// Parser for model objects:
// JSON model -> parser -> ModelObjects
//

#pragma once

#include "ModelExportDefs.hpp"

#include "IRContext.hpp"
#include "IRConstructionFacade.hpp"

#include "ModelParserCtx.hpp"

#include <istream>
#include <memory>

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelParser {
  public:
    ModelParser();
    
    virtual ~ModelParser() = default;
    
    /// Parses the model described by the given context,
    /// i.e., its AST and generates the correspondent IR:
    ///
    ///   +---------------+   +------------+
    ///		|	Model Context |-->| IR Context |
    ///   +---------------+   +------------+
    ///
    /// @note the generated IR is not in its low-level state.
    /// It is meant to be given to the model optimizer before
    /// the back-end produces a model for the solver.
    /// @note returns the pointer to the IR context containing
    /// the global module
    IR::IRContextSPtr parse(const ModelContext& aCtx);
    
  private:
    /// Facade used for IR construction
    std::unique_ptr<IR::IRConstructionFacade> pIRConstructionFacade;
    
    /// Register of context-specific parsers
    std::unordered_map<int, std::shared_ptr<ModelParserCtx>> pParserCtxRegister;
    
    /// Initialize pParserCtxRegister with ctx parsers
    void initializeParserRegister();
    
    /// Parses a model context and sets the correspondent IR module.
    /// @note "aMod" must be the global module
    void parseModelContext(const ModelContext& aMdlCtx, const IR::IRModuleSPtr& aMod);
    
    /// Return the parser for the given type of context
    std::shared_ptr<ModelParserCtx> getParserCtx(ModelParserCtx::ModelParserCtxType aType) const;
  };
  
}// end namespace Model
