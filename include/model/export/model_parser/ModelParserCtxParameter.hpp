//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 11/23/2017
//
// Model context-specific parser: parameter context.
//

#pragma once

// Base class
#include "ModelParserCtx.hpp"

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelParserCtxParameter: public ModelParserCtx {
  public:
		/// Parses the model context "aMdlCtx" and generates the corresponding IR module in "aMod".
		/// For example:
		/// Model Context Variable --> IR Module Variable
		void parseModelContext(const ModelContext& aCtx, const IR::IRModuleSPtr& aMod) override;

	protected:
		friend class ModelParser;

		ModelParserCtxParameter();

	private:
		/// Adds the parameter declaration node into the IR parameters module
		void addIRParamNode(const ModelContext::CtxObjSPtr& aPar, const IR::IRModuleSPtr& aMod);
  };
  
}// end namespace Model
