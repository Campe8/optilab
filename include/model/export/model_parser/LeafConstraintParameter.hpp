//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 04/21/2017
//
// Leaf constraint parameter for model abstract syntax tree.
// Constraint parameters are represented as composite/primitive objects.
//

#pragma once

#include "ModelExportDefs.hpp"

// Scalar type defs
#include "DomainTypeDefs.hpp"
#include "DomainDefs.hpp"

#include <string>
#include <vector>
#include <memory>
#include <boost/logic/tribool.hpp>

namespace Model {
  
  // === Base class for COMPOSITE pattern === //
  
  class MODEL_EXPORT_CLASS LeafConstraintParameter {
  public:
    // Types of constraint parameters parameters
    enum LeafConstraintParameterType : int {
        LC_PARAM_VAR_ID = 0
      , LC_PARAM_INT64
      , LC_PARAM_BOOL
      , LC_PARAM_UNDEF
    };
    
    virtual ~LeafConstraintParameter() = default;
    
    virtual bool isComposite() const = 0;
    
  protected:
    LeafConstraintParameter() = default;
    
  };
  
  // === Primitive class for leaf constraint parameter === //
  
  class MODEL_EXPORT_CLASS LeafConstraintParameterPrimitive : public LeafConstraintParameter {
  public:
    virtual ~LeafConstraintParameterPrimitive() = default;
    
    inline LeafConstraintParameterType leafConstraintParameterType() const
    {
      return pLeafConstraintParameterType;
    }
    
    inline bool isComposite() const override
    {
      return false;
    }
    
  protected:
    LeafConstraintParameterPrimitive(LeafConstraintParameter::LeafConstraintParameterType aLeafConstraintParameterType);
    
  private:
    LeafConstraintParameterType pLeafConstraintParameterType;
  };
  
  // === Composite class for leaf constraint parameter === //
  
  class MODEL_EXPORT_CLASS LeafConstraintParameterComposite : public LeafConstraintParameter {
  public:
    LeafConstraintParameterComposite() = default;
    
    virtual ~LeafConstraintParameterComposite() = default;
    
    inline void addLeafConstraintParameter(const std::shared_ptr<LeafConstraintParameter>& aLeafConstraintParameterPtr)
    {
      pLeafConstraintParamterRegister.push_back(aLeafConstraintParameterPtr);
    }
    
    inline std::size_t numLeafConstraintParameters() const
    {
      return pLeafConstraintParamterRegister.size();
    }
    
    inline std::shared_ptr<LeafConstraintParameter> getLeafConstraintParameters(std::size_t aLeafConParamIdx) const
    {
      return pLeafConstraintParamterRegister.at(aLeafConParamIdx);
    }
    
    inline bool isComposite() const override
    {
      return true;
    }
    
  private:
    std::vector<std::shared_ptr<LeafConstraintParameter>> pLeafConstraintParamterRegister;
  };
  
  // === Derived primitive classes for leaf constraint parameters === //
  
  // === INT_64 === //
  class MODEL_EXPORT_CLASS LeafConstraintParameterPrimitiveInt64 : public LeafConstraintParameterPrimitive {
  public:
    LeafConstraintParameterPrimitiveInt64(INT_64 aScalarInt);
    
    virtual ~LeafConstraintParameterPrimitiveInt64() = default;
    
    /// Override internal value and set it to +INF
    inline void overrideValueWithPosInf()
    {
      pValue = Core::Limits::int64Max();
    }
    
    /// Override internal value and set it to -INF
    inline void overrideValueWithNegInf()
    {
      pValue = Core::Limits::int64Min();
    }
    
    inline INT_64 getValue() const
    {
      return pValue;
    }
    
  private:
    INT_64 pValue;
  };
  
  // === Bool === //
  class MODEL_EXPORT_CLASS LeafConstraintParameterPrimitiveBool : public LeafConstraintParameterPrimitive {
  public:
    LeafConstraintParameterPrimitiveBool();
    
    virtual ~LeafConstraintParameterPrimitiveBool() = default;
    
    /// Override internal value and set it to "aBoolValue"
    inline void overrideValue(bool aBoolValue)
    {
      pValue = aBoolValue;
    }
    
    inline bool isIndeterminate() const
    {
      return boost::logic::indeterminate(pValue);
    }
    
    inline bool getValue() const
    {
      return pValue;
    }
    
  private:
    boost::logic::tribool pValue;
  };
  
  // === Var ID === //
  class MODEL_EXPORT_CLASS LeafConstraintParameterPrimitiveVarId : public LeafConstraintParameterPrimitive {
  public:
    LeafConstraintParameterPrimitiveVarId(const std::string& aVarId);
    
    virtual ~LeafConstraintParameterPrimitiveVarId() = default;
    
    inline std::string getValue() const
    {
      return pValue;
    }
    
  private:
    std::string pValue;
  };
  
}// end namespace Model
