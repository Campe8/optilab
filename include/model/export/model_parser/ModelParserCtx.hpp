//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 08/29/2017
//
// Model context-specific parser.
//

#pragma once

#include "ModelExportDefs.hpp"

#include "ModelContext.hpp"
#include "IRContext.hpp"

#include "IRConstructionFacade.hpp"

#include <memory>

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelParserCtx {
	public:
		enum ModelParserCtxType {
			  MDLPRS_VARIABLE = 0
			, MDLPRS_CONSTRAINT
      , MDLPRS_PARAMETER
			, MDLPRS_SEARCH
		};

  public:
    virtual ~ModelParserCtx() = default;

		static int ModelParserCtxTypeToInt(ModelParserCtxType aType) { return static_cast<int>(aType); }
		inline ModelParserCtxType getModelParserType() const { return pModelParserType; }
    
		/// Parses the model context "aCtx" and generates the corresponding IR module in "aMod".
		/// For example:
		/// Model Context Variable --> IR Module Variable
		virtual void parseModelContext(const ModelContext& aCtx, const IR::IRModuleSPtr& aMod) = 0;

	protected:
		ModelParserCtx(ModelParserCtxType aType);
    
    /// Returns the instace of the IR construction facade
		inline IR::IRConstructionFacade* getConstructionFacade() { return pConstructionFacade.get(); }
    
	private:
		/// Type of this context parser
		ModelParserCtxType pModelParserType;

		/// IR construction facade
		std::unique_ptr<IR::IRConstructionFacade> pConstructionFacade;
  };
  
}// end namespace Model
