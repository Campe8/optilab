//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/14/2017
//
// Model context-specific parser: constraint context.
//

#pragma once

// Base class
#include "ModelParserCtx.hpp"

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelParserCtxConstraint : public ModelParserCtx {
  public:
    virtual ~ModelParserCtxConstraint() = default;
    
    /// Parses the model context "aMdlCtx" and generates the corresponding IR module in "aMod".
    /// For example:
    /// Model Context Variable --> IR Module Variable
    void parseModelContext(const ModelContext& aCtx, const IR::IRModuleSPtr& aMod) override;
    
  protected:
    friend class ModelParser;
    
    ModelParserCtxConstraint();
    
  private:
    /// Adds the con declaration node int the IR module for constraint declaration
    void addIRConDeclNode(const ModelContext::CtxObjSPtr& aCon, const IR::IRModuleSPtr& aMod);
  };
  
}// end namespace Model
