//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 08/29/2017
//
// Model context-specific parser: variable context.
//

#pragma once

// Base class
#include "ModelParserCtx.hpp"

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelParserCtxVariable : public ModelParserCtx {
  public:
    virtual ~ModelParserCtxVariable() = default;

		/// Parses the model context "aMdlCtx" and generates the corresponding IR module in "aMod".
		/// For example:
		/// Model Context Variable --> IR Module Variable
		void parseModelContext(const ModelContext& aCtx, const IR::IRModuleSPtr& aMod) override;

	protected:
		friend class ModelParser;

		ModelParserCtxVariable();

	private:
		/// Adds the var declaration node corresponding to "aCtx" into the IR module
		/// for variable declaration
    void addIRVarDeclNode(const ModelContext::CtxObjSPtr& aVar, const IR::IRModuleSPtr& aMod);
  };
  
}// end namespace Model
