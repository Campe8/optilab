//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 12/12/2017
//
// Utilities for interpreters: CLI to Context.
//

#pragma once

#include "ModelExportDefs.hpp"
#include "ModelASTMacro.hpp"
#include "ModelContext.hpp"
#include "ContextView.hpp"
#include "GlobalsType.hpp"
#include "CLIUtils.hpp"

#include <boost/optional.hpp>

#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>

namespace Model {
  namespace Utils {
    namespace Interpreter {
      
      using ReferenceMap = std::unordered_map<std::string, std::unordered_set<std::size_t>>;
      
      enum ExprType : int {
          AT_ID = 0       // my_var
        , AT_INT          // 10
        , AT_ARRAY        // [1, 2, x, y]
        , AT_SUBSCRIPT    // my_array[10]
        , AT_UNDEF
      };
      
      /// Returns the ID that identifies the object interpreted from "aStmt" if any
      MODEL_EXPORT_FUNCTION boost::optional<std::string> getIDFromStmt(const CLI::CLIStmt& aStmt);
      
      /// Parses "aExpr" as an expression and returns its interpreted version.
      /// If "aExpr" can't be interpreted w.r.t. the current view, returns boost::none.
      /// @note "aRefMap" is the map used to store information about references and subscripts to
      /// determine if there are any circular dependencies between expression definitions
      MODEL_EXPORT_FUNCTION boost::optional<std::string> interpretExpr(const std::string& aExpr, const ContextView& aViewm, ReferenceMap* aRefMap=nullptr);
      
      /// Parses "aStr" as an expression and returns its type according to "ExprType"
      MODEL_EXPORT_FUNCTION ExprType getExprType(const std::string& aExpr);
      
      /// Given a "AT_SUBSCRIPT" string returns the pair <matrix_id, <index>>,
      /// returns an empty pair if "aSubscript" is not a subscript expression
      MODEL_EXPORT_FUNCTION std::pair<std::string, std::vector<std::string>> getSubscriptMatrixAndIndex(const std::string& aSubscript);
      
      /// Given a "AT_ARRAY" string returns the vector of all its elements,
      /// returns an empty vector if "aArray" is not an array expression
      MODEL_EXPORT_FUNCTION std::vector<std::string> getArrayElements(const std::string& aArray);
      
    }// end namespace Interpreter
  } // end namespace Utils
} // end namespace Model
