//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 01/013/2017
//

#pragma once

// Macros Abstract Syntax Tree branching declarations

#define MDL_AST_SUBTREE_SEARCH  "search"
#define MDL_AST_LEAF_BRC        "brc"
#define MDL_AST_LEAF_BRC_KEY    "key"                                  // string key _c<long long key>
#define MDL_AST_LEAF_BRC_KEY_PREFIX "_b"                               // string prefix "_b"
#define MDL_AST_LEAF_BRC_BRANCHING_SET "branching_set"								 // [x1, x2, ...]
#define MDL_AST_LEAF_BRC_VAR_CHOICE "variable_choice"         // input_order, first_fail, etc.
#define MDL_AST_LEAF_BRC_VAL_CHOICE "value_choice"            // indomain_min, indomain_max, etc.

// Variable choice
#define MDL_AST_LEAF_BRC_VAR_DEFAULT "default"
#define MDL_AST_LEAF_BRC_VAR_INPUT_ORDER "input_order"
#define MDL_AST_LEAF_BRC_VAR_LARGEST "largest"
#define MDL_AST_LEAF_BRC_VAR_SMALLEST "smallest"
#define MDL_AST_LEAF_BRC_VAR_FIRST_FAIL "first_fail"
#define MDL_AST_LEAF_BRC_VAR_ANTI_FIRST_FAIL "anti_first_fail"
#define MDL_AST_LEAF_BRC_VAR_DEFAULT_VAL MDL_AST_LEAF_BRC_VAR_INPUT_ORDER

// Value choice
#define MDL_AST_LEAF_BRC_VAL_DEFAULT "default"
#define MDL_AST_LEAF_BRC_VAL_INDOMAIN_MAX "indomain_max"
#define MDL_AST_LEAF_BRC_VAL_INDOMAIN_MIN "indomain_min"
#define MDL_AST_LEAF_BRC_VAL_INDOMAIN_RANDOM "indomain_random"
#define MDL_AST_LEAF_BRC_VAL_DEFAULT_VAL MDL_AST_LEAF_BRC_VAL_INDOMAIN_MIN
