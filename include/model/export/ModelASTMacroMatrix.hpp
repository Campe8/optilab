//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 22/11/2017
//

#pragma once

// Macros for Abstract Syntax Tree - matrix declaration

// Context id
#define MDL_AST_SUBTREE_PAR "parameters"

// Identifiers for sub-context-leaves
#define MDL_AST_LEAF_MAT                          "mat"
#define MDL_AST_LEAF_MAT_ID                       "id"          // string
#define MDL_AST_LEAF_MAT_TYPE                     "type"        // integer, boolean, etc.
#define MDL_AST_LEAF_MAT_NROWS                    "nrows"       // integer
#define MDL_AST_LEAF_MAT_NCOLS                    "ncols"       // integer
#define MDL_AST_LEAF_MAT_VALS                     "values"      // [1, 2, ...]
