//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/09/2017
//

#pragma once

// Macros Abstract Syntax Tree search semantic declarations

#define MDL_AST_SUBTREE_SEARCH  "search"
#define MDL_AST_LEAF_SEARCH     "search"
#define MDL_AST_LEAF_SEARCH_TYPE "type"                                   // dfs, etc.
#define MDL_AST_LEAF_SEARCH_BRANCHING_SET "branching_set"									// [x1, x2, ...]
#define MDL_AST_LEAF_SEARCH_SEMANTIC "semantic"
#define MDL_AST_LEAF_SEARCH_SEMANTIC_VAR_CHOICE "variable_choice"         // input_order, first_fail, etc.
#define MDL_AST_LEAF_SEARCH_SEMANTIC_VAL_CHOICE "value_choice"            // indomain_min, indomain_max, etc.
#define MDL_AST_LEAF_SEARCH_NUM_SOLUTIONS "num_solutions"									// 0, 1, 2, ...

// Search type
#define MDL_AST_LEAF_SEARCH_TYPE_ALGO_DFS "dfs"

// Variable choice
#define MDL_AST_LEAF_SEARCH_SEMANTIC_VAR_DEFAULT "default"
#define MDL_AST_LEAF_SEARCH_SEMANTIC_VAR_INPUT_ORDER "input_order"
#define MDL_AST_LEAF_SEARCH_SEMANTIC_VAR_LARGEST "largest"
#define MDL_AST_LEAF_SEARCH_SEMANTIC_VAR_SMALLEST "smallest"
#define MDL_AST_LEAF_SEARCH_SEMANTIC_VAR_FIRST_FAIL "first_fail"
#define MDL_AST_LEAF_SEARCH_SEMANTIC_VAR_ANTI_FIRST_FAIL "anti_first_fail"

// Value choice
#define MDL_AST_LEAF_SEARCH_SEMANTIC_VAL_DEFAULT "default"
#define MDL_AST_LEAF_SEARCH_SEMANTIC_VAL_INDOMAIN_MAX "indomain_max"
#define MDL_AST_LEAF_SEARCH_SEMANTIC_VAL_INDOMAIN_MIN "indomain_min"
#define MDL_AST_LEAF_SEARCH_SEMANTIC_VAL_INDOMAIN_RANDOM "indomain_random"

