//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/09/2017
//

#pragma once

// Macros for Abstract Syntax Tree constraint declarations

#define MDL_AST_SUBTREE_CON  "constraints"
#define MDL_AST_LEAF_CON     "con"

#define MDL_AST_LEAF_CON_ID "id"                      // string
#define MDL_AST_LEAF_CON_KEY "key"										// string key _c<long long key>
#define MDL_AST_LEAF_CON_KEY_PREFIX "_c"							// string prefix "_c"
#define MDL_AST_LEAF_CON_NAME "name"                  // string
#define MDL_AST_LEAF_DECLARATION_TYPE_FCN_CALL "fcn_call"
#define MDL_AST_LEAF_DECLARATION_TYPE_EXPR "expr"
#define MDL_AST_LEAF_PROPAGATION_TYPE "propagation"   // string: "bounds", "domain"
#define MDL_AST_LEAF_CON_SCOPE_TYPE "scope_type"      // ["int", "var_id", "bool", ...]
#define MDL_AST_LEAF_CON_SCOPE "scope"                // [1, "a",  true, ...]
#define MDL_AST_LEAF_CON_SCOPE_TYPE_BOOL "bool"
#define MDL_AST_LEAF_CON_SCOPE_TYPE_INT "int"
#define MDL_AST_LEAF_CON_SCOPE_TYPE_VAR_ID "var_id"

// Macros for JSON string constraint propagation type
#define MDL_AST_LEAF_PROPAGATION_TYPE_BOUND "bounds"
#define MDL_AST_LEAF_PROPAGATION_TYPE_DOMAIN "domain"

// Macros for JSON string constraint identifiers
#define MDL_AST_CON_ID_LQ "lq"                          // V0 <= V1
#define MDL_AST_CON_ID_GQ "gq"                          // V0 >= V1
#define MDL_AST_CON_ID_NQ "nq"                          // V0 != V1
#define MDL_AST_CON_ID_EQ "eq"                          // V0 == V1
#define MDL_AST_CON_ID_LIN_EQ "lin_eq"                  // Sum_i(coeff_i * V_i) == c
#define MDL_AST_CON_ID_LIN_NQ "lin_nq"                  // Sum_i(coeff_i * V_i) != c
#define MDL_AST_CON_ID_LIN_LQ "lin_lq"                  // Sum_i(coeff_i * V_i) <= c
#define MDL_AST_CON_ID_LIN_LT "lin_lt"                  // Sum_i(coeff_i * V_i) < c
#define MDL_AST_CON_ID_TIMES "times"                    // V0 * V1 = V2
#define MDL_AST_CON_ID_ABS "abs"                        // |V0| = V1
#define MDL_AST_CON_ID_DIV "div"                        // V0/V1 = V2
#define MDL_AST_CON_ID_LT "lt"                          // V0 < V1
#define MDL_AST_CON_ID_GT "gt"                          // V0 > V1
#define MDL_AST_CON_ID_MAX "max"                        // max(V0, V1) = V2
#define MDL_AST_CON_ID_MIN "min"                        // min(V0, V1) = V2
#define MDL_AST_CON_ID_PLUS "plus"                      // V0 + V1 = V2
#define MDL_AST_CON_ID_EQ_REIF "eq_reif"                // (V0 == V1) <-> b
#define MDL_AST_CON_ID_EQ_HREIF_R "eq_hreif_r"          // (V0 == V1)  -> b
#define MDL_AST_CON_ID_EQ_HREIF_L "eq_hreif_l"          // (V0 == V1) <-  b
#define MDL_AST_CON_ID_LQ_REIF "lq_reif"                // (V0 <= V1) <-> b
#define MDL_AST_CON_ID_LQ_HREIF_R "lq_hreif_r"          // (V0 <= V1)  -> b
#define MDL_AST_CON_ID_LQ_HREIF_L "lq_hreif_l"          // (V0 <= V1) <-  b
#define MDL_AST_CON_ID_LT_REIF "lt_reif"            // (V0 < V1) <-> b
#define MDL_AST_CON_ID_LT_HREIF_R "lt_hreif_r"      // (V0 < V1)  -> b
#define MDL_AST_CON_ID_LT_HREIF_L "lt_hreif_l"      // (V0 < V1) <-  b
#define MDL_AST_CON_ID_NQ_REIF "nq_reif"            // (V0 != V1) <-> b
#define MDL_AST_CON_ID_NQ_HREIF_R "nq_hreif_r"      // (V0 != V1)  -> b
#define MDL_AST_CON_ID_NQ_HREIF_L "nq_hreif_l"      // (V0 != V1) <-  b
#define MDL_AST_CON_ID_LIN_EQ_REIF "lin_eq_reif"        // (Sum_i(coeff_i * V_i) == c) <-> b
#define MDL_AST_CON_ID_LIN_EQ_HREIF_R "lin_eq_hreif_r"  // (Sum_i(coeff_i * V_i) == c)  -> b
#define MDL_AST_CON_ID_LIN_EQ_HREIF_L "lin_eq_hreif_l"  // (Sum_i(coeff_i * V_i) == c) <-  b
#define MDL_AST_CON_ID_LIN_LQ_REIF "lin_lq_reif"        // (Sum_i(coeff_i * V_i) <= c) <-> b
#define MDL_AST_CON_ID_LIN_LQ_HREIF_R "lin_lq_hreif_r"  // (Sum_i(coeff_i * V_i) <= c)  -> b
#define MDL_AST_CON_ID_LIN_LQ_HREIF_L "lin_lq_hreif_l"  // (Sum_i(coeff_i * V_i) <= c) <-  b
#define MDL_AST_CON_ID_LIN_NQ_REIF "lin_nq_reif"        // (Sum_i(coeff_i * V_i) != c) <-> b
#define MDL_AST_CON_ID_LIN_NQ_HREIF_R "lin_nq_hreif_r"  // (Sum_i(coeff_i * V_i) != c)  -> b
#define MDL_AST_CON_ID_LIN_NQ_HREIF_L "lin_nq_hreif_l"  // (Sum_i(coeff_i * V_i) != c) <-  b

#define MDL_AST_CON_ID_BOOL_AND "bool_and"              // d0 /\ d1 OR (d0 /\ d1) <-> b
#define MDL_AST_CON_ID_BOOL_EQ "bool_eq"                // d0 == d1
#define MDL_AST_CON_ID_MOD "mod"                        // d0 % d1 = d2

#define MDL_AST_CON_ID_ALLDIFF "alldiff"                // di != dj for all i, j in 1..n
#define MDL_AST_CON_ID_DOMAIN "domain"                  // domain constraint
#define MDL_AST_CON_ID_ELEMENT "element"                // element constraint
