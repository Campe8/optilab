//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 04/18/2017
//

#pragma once

#include "ModelExportDefs.hpp"
#include "ConstraintDefs.hpp"

#include <sparsepp/spp.h>
#include <boost/core/noncopyable.hpp>

#include <string>
#include <vector>

namespace Model {
  namespace Utils {
    
    class MODEL_EXPORT_CLASS ConstraintHelper : private boost::noncopyable {
    public:
      // Argument type
      struct ArgType {
        enum Type : char {
            T_VAR = 0
          , T_INT
          , T_BOOL
          , T_UNDEF
        };
        
        // Types allowed for this argument
        std::vector<Type> argType;
        
        // Flag for array arguments
        bool argArray = false;
      };
      
      // List of types that a constraint argument can have
      using ArgTypeList = std::vector<ArgType>;
      
    public:
      /**
       * Helper class for constraint declaration.
       * @note singleton class.
       * @note this is a "read-only" class.
       */
      static ConstraintHelper& getInstance();
      
      /// Returns true if the given name has a correspondent constraint
      /// registered in the helper map, false otherwise
      inline bool isConstraintRegistered(const std::string& aName) const { return lookup(aName) != nullptr; }
      
      /// Returns true if the given name corresponds to a global constraint, false otherwise.
      /// @note asserts if the constraint is not registered into the internal map
      bool isConstraintGlobal(const std::string& aName) const;
      
      /// Returns true if the given name corresponds to a reified constraint, false otherwise.
      /// @note asserts if the constraint is not registered into the internal map
      bool isConstraintReified(const std::string& aName) const;
      
      /// Returns the number of accepted arguments for the constraint registered with the given name.
      /// @note asserts if the constraint is not registered into the internal map
      std::size_t numConstraintArguments(const std::string& aName) const;
      
      /// Returns true if the argument at position "aIdx" of the constraint "aName"
      /// can be an array, false otherwise.
      /// @note if the method returns true that doesn't mean that the argument cannot have
      /// a non-array type, it just means that the type can be an array.
      /// It returns false if the argument at given position cannot be an array.
      /// @note throws if no constraint of given name is registered or the index is out of bounds
      bool isConstraintArgumentArrayAllowed(const std::string& aName, std::size_t aIdx) const;
      
      /// Returns the allowed non array types for the constraint argument at position "aIdx".
      /// @note if the constraint argument at given position only accepts array, the returned vector will be empty.
      /// @note throws if no constraint of given name is registered or the index is out of bounds
      std::vector<ArgType::Type> getConstraintArgumentNonArrayAllowedTypes(const std::string& aName, std::size_t aIdx) const;
      
      /// Returns the allowed array types for the constraint argument at position "aIdx".
      /// @note if the constraint argument at given position only accepts non-array types, the returned vector will be empty.
      /// @note throws if no constraint of given name is registered or the index is out of bounds
      std::vector<ArgType::Type> getConstraintArgumentArrayAllowedTypes(const std::string& aName, std::size_t aIdx) const;
      
      /// Returns true if the constraint registered with given name has "same size" argument
      /// @note asserts if the constraint is not registered into the internal map
      bool hasSameSizeArgumentConstraint(const std::string& aName) const;
      
      /// Returns the vector of lists of arguments that must have same size.
      /// For example:
      /// [[1, 3], [2, 4, 5]]
      /// indicates that the argument 1 and 3 must have same size,
      /// and the argument 2, 4, and 5 must have same size
      /// @note asserts if the constraint is not registered into the internal map
      std::vector<std::vector<int>> getSameSizeArgumentConstraint(const std::string& aName) const;
      
      /// Returns the syntax for the constraint registered with given name.
      /// @note asserts if the constraint is not registered into the internal map
      std::string constraintSyntax(const std::string& aName) const;
      
      /// Returns the description for the constraint registered with given name.
      /// @note asserts if the constraint is not registered into the internal map
      std::string constraintDescription(const std::string& aName) const;
      
      /// Returns the posting example for the constraint registered with given name.
      /// @note asserts if the constraint is not registered into the internal map
      std::string constraintExample(const std::string& aName) const;
      
      /// Returns a vector containing all the names of the registered constraints
      std::vector<std::string> getAllRegisteredConstraintNames() const;
      
      /// Returns the core constraint ID correspondent to the given
      /// constraint string ID
      Core::ConstraintId stringIDToCoreID(const std::string& aID) const;
      
    private:
      ConstraintHelper();
      
      struct ConstraintSpec {
        // Constraint name
        std::string name = "";
        // Flag for global constraints
        bool global = false;
        // Flag for reif constraints
        bool reif = false;
        // List of input arguments and the types they can have
        std::vector<ArgTypeList> argList;
        // List of input arguments that must have same size.
        // Each sub-vector is a list of the arguments that must
        // have same size
        std::vector<std::vector<int>> sameSizeArgList;
        // Syntax for posting the constraint
        std::string syntax = "";
        // Description of the constraint
        std::string description = "";
        // Example of usage
        std::string example = "";
      };
      
    private:
      /// Map between constraint string IDs and their correspondent Core IDs
      static spp::sparse_hash_map<std::string, Core::ConstraintId> StringToCoreIDMap;
      
      /// Map between constraint ids and their specifications
      spp::sparse_hash_map<std::string, std::unique_ptr<ConstraintSpec>> pConSpecMap;
      
      /// Loads the constraint specifications from the
      /// XML file descriptions
      void loadConstraintSpecifications();
      
      /// Looks up a constraint specification by constraint name.
      /// Returns nullptr if there is no such entry registered in the map
      ConstraintSpec* lookup(const std::string& aName) const;
      
      /// Adds the given constraint specification to the map
      void addConstraintSpecToMap(ConstraintSpec* aSpec);
      
      /// Parse the constraint specification and return the
      /// correspondent specification structure.
      /// Returns nullptr if an error occurred
      ConstraintSpec* parseConstraintSpecification(std::vector<char>& aSpec);
      
      /// Returns the argument type list at position "aIdx" of the constraint "aName".
      /// @note asserts if the constraint is not registered into the internal map or
      /// the index is out of bounds
      const ArgTypeList& getArgTypeList(const std::string& aName, std::size_t aIdx) const;
    };
    
    /// Returns a name for an unnamed/undefined model
    MODEL_EXPORT_FUNCTION std::string getUndefModelName();
    
    /// Returns true if "aModelName" represents an undefined model name,
    /// false otherwise
    MODEL_EXPORT_FUNCTION bool isUndefinedModelName(const std::string& aModelName);
    
    /// Returns a name for an unnamed variable.
    /// @note if "aResetSeed" is true, resets the seed used to generate the
    /// variable name
    MODEL_EXPORT_FUNCTION std::string getUndefVarName(bool aResetSeed=false);
    
  } // end namespace Utils
} // end namespace Model
