//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 10/9/2018
//
// Base class for search-specific context object information.
// @note this class may be included from deeper classes
// within the engine. Therefore, this class should have
// as few dipendences as possible.
// @note this class and its derived classes should
// only be used to setup the InfoMap NOT to return
// values and/or choices. The InfoMap is the data structure
// that will be carried around during translation into IR
// and into the model objects.
//

#pragma once

#include "ModelExportDefs.hpp"

// Heterogeneous map to store
// search specific information
#include "Hetermap.hpp"

#include <memory>
#include <stdexcept>  // for std::runtime_error

// Forward declarations
namespace Interpreter {
  class BaseObject;
}// end namespace Interpreter

namespace Model {
  
  class MODEL_EXPORT_CLASS SearchInformation {
  public:
    using InfoMap = DataStructure::Map::Hetermap<std::string>;
    using InfoMapSPtr = std::shared_ptr<InfoMap>;
    
  public:
    enum SearchInformationType : char {
        SI_DFS = 0
      , SI_GENETIC
    };
    
  public:
    virtual ~SearchInformation() = default;
    
    /// Information key for timeout
    static const std::string TimeoutKey;
    
    inline SearchInformationType getSearchInfoType() const { return pType; }
    
    /// Returns the map storing search information
    inline InfoMapSPtr getMap() const { return pInfoMap; }
    
    /// Adds a new member with specified value as part of
    /// the search information set.
    /// The type of the added value is T and its value is "aVal".
    /// The member's name is the given key "aKey".
    /// @note throws std::runtime_error if a member with given key is already
    /// present in the map
    template<typename T>
    void addMember(const std::string& aKey, const T& aVal)
    {
      if (pInfoMap->isRegistered(aKey))
      {
        throw std::runtime_error("SearchInformation:: key " + aKey + " already in the map");
      }
      pInfoMap->insert(aKey, aVal);
    }
    
    /// Updates an existing member with a specified value as part of
    /// the search information set.
    /// If the member is not present, adds it to the map
    template<typename T>
    void updateMember(const std::string& aKey, const T& aVal)
    {
      pInfoMap->insert(aKey, aVal);
    }
    
    /// Initalizes the search information with the content of the given BaseObject
    virtual void readSearchOptionsObject(Interpreter::BaseObject* aBaseObject) = 0;
    
  protected:
    SearchInformation(SearchInformationType aType);
    
  private:
    /// Search information type
    SearchInformationType pType;
    
    /// Heterogeneous map storing search specific information
    /// as <std::string, type> pairs
    InfoMapSPtr pInfoMap;
  };
  
}//end namespace Model
