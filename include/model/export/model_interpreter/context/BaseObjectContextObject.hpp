//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 10/20/2018
//

#pragma once

// Base class
#include "ContextObject.hpp"

namespace Model {
  
  class MODEL_EXPORT_CLASS BaseObjectContextObject : public ContextObject {
  public:
    BaseObjectContextObject(const std::string& aID, const Interpreter::DataObject& aDO);
  };
  
}//end namespace Model
