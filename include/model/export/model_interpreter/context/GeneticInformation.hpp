//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 10/9/2018
//
// Search information for Genetic search strategies.
//

#pragma once

#include "CBLSInformation.hpp"

namespace Model {
  
  class MODEL_EXPORT_CLASS GeneticInformation : public CBLSInformation {
  public:
    GeneticInformation();
    
    virtual ~GeneticInformation() = default;
    
    /// Strategy name
    static const std::string StrategyName;
    
    /// Information key for population size
    static const std::string PopulationSizeKey;
    
    /// Information key for number of generations
    static const std::string NumGenerationsKey;
    
    /// Information key mutation chance percentage
    static const std::string MutationChanceKey;
    
    /// Information key for number of genes to randomly mutate
    static const std::string MutationSizeKey;
    
  protected:
    void readStrategySearchOptionsObject(Interpreter::BaseObject* aObj) override;
  };
  
}//end namespace Model
