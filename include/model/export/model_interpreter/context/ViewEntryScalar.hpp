//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 12/14/2017
//
// Entry for representing fully interpreted scalars.
//

#pragma once

// Base class
#include "ViewEntry.hpp"
#include "CLIUtils.hpp"

#include <sstream>

namespace Model {
  
  class MODEL_EXPORT_CLASS ViewEntryScalar : public ViewEntry {
  public:
    
    ViewEntryScalar(const std::string& aID, const EntryValue& aScalar);
    
    static bool isa(const ViewEntry* aEntry);
    static ViewEntryScalar* cast(ViewEntry* aEntry);
    
    ViewEntry* clone() override;
    
    inline std::string getScalar() const
    {
      return pScalar;
    }
    
    /// Sets interpreted value if scalar wasn't already interpreted
    void setInterpretedValue(const std::string& aScalar);
    
    std::string serialize() const override;
    
  private:
    /// Serialized version of this entry
    mutable std::string pSerialization;
    
    /// Scalar contained in this entry
    std::string pScalar;
    
    /// Serializes this entry
    void serializeScalar() const;
  };
  
}// end namespace Model

