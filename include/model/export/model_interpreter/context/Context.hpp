//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 18/07/2017
//
// This class represents the context in which stmts
// are evaluated and the model is generated.
//

#pragma once

#include "ModelExportDefs.hpp"

#include "BoostInc.hpp"

#include <string>
#include <vector>
#include <iostream>
#include <utility>
#include <cassert>

namespace pt = boost::property_tree;

namespace Model {

  class MODEL_EXPORT_CLASS Context {
  public:
    typedef std::vector<std::string> Path;
    
    /// Creates unnamed context
    Context();
    
    /// Creates a named context:
    /// {
    ///   "aContextId": ""
    /// }
    Context(const std::string& aContextId);
    
    Context(const Context& aOther);
    Context(Context&& aOther);
    Context& operator=(const Context& aOther);
    Context& operator=(Context&& aOther);
    
    ~Context() = default;
    
    inline std::string getContextId() const
    {
      return pCtxId;
    }
    
    /// Returns true if this context is empty,
    /// false otherwise.
    /// @note the following
    /// {
    ///   "id": ""
    /// }
    /// and
    /// {
    /// }
    /// are both empty context
    bool isEmpty() const;
    
    /// Adds an empty sub-context identified by the path in "aPath"
    /// i.e., each element of "aPath" is the id of a sub-context.
    /// For example:
    /// current context (a is context id)
    /// {
    ///   "a": ""
    /// }
    /// addSubContext({"b"}, ctx):
    /// {
    ///   "a": {
    ///        "b" : ctx
    ///   }
    /// @note aPath MUST be a non empty string
    /// @note context are replaced if added with with same path
    void addSubContext(const Path& aPath, const Context& aCtx);
    
    /// Expands the sub-context addressed by "aPath" by inserting into
    /// a list the context "aCtx"
    void expandSubContext(const Path& aPath, const Context& aCtx);
    
    /// Returns the context addressed by "aPath" if any.
    /// For example, given the following context:
    ///  {
    ///    "ctx": {
    ///      "a": {
    ///        "ctx1": {
    ///          "b": "1"
    ///        }
    ///      }
    ///    }
    ///  }
    /// getSubContext("a") on context "ctx"
    /// returns:
    /// {
    ///   "a": {
    ///     "ctx1": {
    ///       "b": "1"
    ///     }
    ///   }
    /// }
    /// Same for "a.ctx1", returning context "ctx1".
    /// @note the path for the sub-context to return starts from the address of
    /// the child node, i.e., the name "this" should not be parth of "aPath"
    Context getSubContext(const Path& aPath) const;
    
    /// Returns the vector of model context in a list of context.
    /// For example:
    /// {
    ///   "subsubCtx": [
    ///                 {
    ///                 "c1": "v1"
    ///                 },
    ///                 {
    ///                 "c2": "v2"
    ///                 }
    ///                 ]
    /// }
    /// returns the vector containing:
    /// {
    /// "c1": "v1"
    /// },
    /// {
    /// "c2": "v2"
    /// }
    /// @note, if called on context that don't have a list as sub-context
    /// returns the vector with the sub-context as elements.
    /// @note the path for the sub-context to return starts from the address of
    /// the child node, i.e., the name "this" should not be parth of "aPath"
    std::vector<Context> getSubContextList(const Path& aPath) const;
    
    /// Adds a single value "aVal" into the context addressed by "aPath".
    /// @note using the same path for different elements will override previous elements.
    /// For example:
    /// current context (a is context id)
    /// {
    ///   "a": ""
    /// }
    /// addvalue({"tag1", "tag2"}, "value"):
    /// {
    ///   "a": {
    ///        "tag1": {
    ///              "tag2" : "value"
    ///          }
    ///        }
    /// }
    template <class T>
    void addValue(const Path& aPath, T aVal)
    {
      assert(!aPath.empty());
      
      // Recreate path
      auto path = getExtendedPath(aPath);
      pCtx.put<T>(path, aVal);
    }
    
    /// Adds a single value "aVal" into a list in the context
    /// addressed by "aPath".
    /// For example:
    /// current context (a is context id)
    /// {
    ///   "a": ""
    /// }
    /// addvalue({"tag1"}, "value"):
    /// {
    ///   "a": {
    ///        "tag1": [ "value" ]
    ///        }
    /// }
    template <class T>
    void addValueList(const Path& aPath, T aVal)
    {
      assert(!aPath.empty());
      
      // Recreate path
      auto path = getExtendedPath(aPath);
      
      if (!pCtx.get_child_optional(path))
      {
        Context valueArray("");
        addSubContext(aPath, valueArray);
      }
      
      pt::ptree cell;
      cell.put_value(aVal);
      pCtx.get_child(path).push_back(std::make_pair("", cell));
    }
    
    template <class T>
    void addValueList(const Path& aPath, const std::vector<T>& aArray)
    {
      assert(!aPath.empty());
      
      // Recreate path
      auto path = getExtendedPath(aPath);
      
      if (!pCtx.get_child_optional(path))
      {
        Context valueArray("");
        addSubContext(aPath, valueArray);
      }
      
      for (const auto& v : aArray)
      {
        pt::ptree cell;
        cell.put_value(v);
        pCtx.get_child(path).push_back(std::make_pair("", cell));
      }
    }
    
    template <class T>
    void addValueList(const Path& aPath, const std::vector<std::vector<T>>& aMatrix)
    {
      assert(!aPath.empty());
      
      // Recreate path
      auto path = getExtendedPath(aPath);
      
      if (!pCtx.get_child_optional(path))
      {
        Context valueArray("");
        addSubContext(aPath, valueArray);
      }
      
      for (const auto& rowMatrix : aMatrix)
      {
        pt::ptree row;
        for (const auto& col : rowMatrix)
        {
          pt::ptree cell;
          cell.put_value(col);
          row.push_back(std::make_pair("", cell));
        }
        // Add the row to the matrix
        pCtx.get_child(path).push_back(std::make_pair("", row));
      }
    }
    
    
    /// Returns a value from the context addressed by "aPath"
    /// @note throws if "aPath" does not address a valid path.
    /// @note the name of this context should not be in the path.
    /// For example, given the following context:
    /// {
    ///   "a": {
    ///     "ctx1": {
    ///       "b": "1"
    ///     }
    ///   }
    /// }
    /// getValue<int>({"ctx1", "b"})
    /// returns 1
    /// @note the path to "1" is ctx1.b NOT a.ctx1.b
    template <class T>
    T getValue(const Path& aPath) const
    {
      assert(!aPath.empty());
      
      // Recreate path
      auto path = getExtendedPath(aPath);
      return pCtx.get<T>(path);
    }
    
    template <class T>
    boost::optional<T> getValueOptional(const Path& aPath) const
    {
      assert(!aPath.empty());
      
      // Recreate path
      auto path = getExtendedPath(aPath);
      return pCtx.get_optional<T>(path);
    }
    
    /// Returns the context containing "aVal" for key "aKey"
    /// at address "aPath"
    template <class T>
    Context getValue(const Path& aPath, const std::string& aKey, T aVal) const
    {
      assert(!aPath.empty());
      
      std::string p{""};
      if(aPath.size() == 1 && aPath[0] == pCtxId)
      {
        p = getExtendedPath({""});
      }
      else
      {
        p = getExtendedPath(aPath);
      }
      
      for(auto& c : pCtx.get_child(p))
      {
        if(c.second.get<T>(aKey) == aVal)
        {
          if(c.first.empty())
          {
            return createUnnamedContext(c);
          }
          else
          {
            return createNamedContext(c);
          }
        }
      }// for
      
      // Return empty context
      return Context();
    }
    
    /// Returns a vector containing the list of values
    /// at address "aPath"
    template <class T>
    std::vector<T> getValueList(const Path& aPath) const
    {
      assert(!aPath.empty());
      
      // Recreate path
      auto path = getExtendedPath(aPath);
      
      std::vector<T> vals;
      if (!pCtx.get_child_optional(path))
      {
        return vals;
      }
      
      for(auto& val : pCtx.get_child(path))
      {
        vals.push_back(val.second.get_value<T>());
      }
      
      return vals;
    }
    
    template <class T>
    std::vector<std::vector<T>> getValueMatrix(const Path& aPath) const
    {
      assert(!aPath.empty());
      
      // Recreate path
      auto path = getExtendedPath(aPath);
      
      std::vector<std::vector<T>> matrix;
      if (!pCtx.get_child_optional(path))
      {
        return matrix;
      }
      
      for (auto& row : pCtx.get_child(path))
      {
        std::vector<T> vals;
        for (auto& v : row.second)
        {
          vals.push_back(v.second.get_value<T>());
        }
        matrix.push_back(vals);
      }
      
      return matrix;
    }
    
    /// Deletes the sub-context at address "aPath" if any
    void deleteSubContext(const Path& aPath);
    
    /// Deletes the sub-context at address "aPath" identified by "akey" with
    /// value "aVal"
    template<typename T>
    void deleteSubContext(const Path& aPath, const std::string& aKey, T aVal)
    {
      assert(!aPath.empty());
      
      std::string p{""};
      if(aPath.size() == 1 && aPath[0] == pCtxId)
      {
        p = getExtendedPath({""});
      }
      else
      {
        p = getExtendedPath(aPath);
      }
      
      auto it = pCtx.get_child(p).begin();
      for(; it != pCtx.get_child(p).end(); ++it)
      {
        if((*it).second.get<T>(aKey) == aVal)
        {
          pCtx.get_child(p).erase(it);
          break;
        }
      }
    }//deleteSubContext
    
    /// Serializes the context and writes it into "aOut"
    void serializeContext(std::ostream& aOut) const;
    
  private:
    typedef pt::ptree PTContext;
    
    /// Id for this context
    std::string pCtxId;
    
    /// Actual data structure representing the context
    PTContext pCtx;
    
    /// Returns an empty context
    PTContext getEmptyContext() const;
    
    /// Returns full path string
    std::string getExtendedPath(const Path& aPath) const;
    
    /// Returns an unnamed model context from a value_type
    Context createUnnamedContext(const pt::ptree::value_type& aValType) const;
    
    /// Returns a named model context from a value_type
    Context createNamedContext(const pt::ptree::value_type& aValType) const;
  };
  
  /// Sets the context "aPath" with ["aVals"] "aSize" times
  template<typename T>
  void addValueListToCtx(Context& aCtx, Context::Path& aPath, const std::vector<T>& aVals, std::size_t aSize)
  {
    if (aSize == 0) { return; }
    if (aSize == 1)
    {
      aCtx.addValueList<T>(aPath, aVals);
      return;
    }
    std::vector<std::vector<T>> matrix(aSize, aVals);
    aCtx.addValueList<T>(aPath, matrix);
  }
  
}// end namespace Model

