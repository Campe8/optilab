//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 12/14/2017
//
// ModelContextView represents the current view of the
// ModelContext and it contains (parts of) it semantic meaning.
// The view is built upon the (fully or partially) interpreted
// objects in the model context.
// It is a "symbol table" keeping track of the defined objects
// in the context. This table supports interpreting context.
//

#pragma once

#include "ModelExportDefs.hpp"
#include "ViewEntry.hpp"

#include <string>
#include <vector>
#include <unordered_map>
#include <memory>

namespace Model {
  
  class MODEL_EXPORT_CLASS ContextView {
  public:
    /// Key used to identify an object in the view
    using ViewKey  = std::string;
    using ViewObj  = ViewEntry;
    using ViewObjSPtr = std::shared_ptr<ViewObj>;
    
  public:
    ContextView();
    
    /// Merges "aOther" into this view
    void mergeView(const std::shared_ptr<ContextView>& aOther);
    
    /// Adds an alias to the alias table
    void addAlias(const std::string& aAlias);
    
    /// Returns true if the set of alias contains "aAlias",
    /// false otherwise
    bool findAlias(const std::string& aAlias) const;
    
    /// Adds a new entry in the map.
    /// If there is an entry with same ID, overrides
    /// previous entry
    void addEntry(const ViewObjSPtr& aEntry);
    
    /// Removes the entry "aID" from the view
    void removeEntry(std::string& aID);
    
    /// Performs lookup and returns the raw pointer to the entry
    /// with "aID" as ID.
    /// Returns nullptr if no such entry is found.
    /// @note the returned pointer SHOULD NOT be modified.
    /// @note returning a raw pointer to increase efficiency
    ViewObj* lookup(std::string& aID) const;
    
    /// Performs a lookup and returns the shared pointer
    /// to the looked-up entry
    ViewObjSPtr getEntry(std::string& aID) const;
    
    /// Returns this serialized view
    std::string getSerializedView() const;
    
  private:
    using ViewMap = std::unordered_map<ViewKey, ViewObjSPtr>;
    
  private:
    /// Flag for serialization
    mutable bool pDirtyCache;
    
    /// Cache of the view
    mutable std::string pViewCache;
    
    /// View map
    ViewMap pViewMap;
    
    /// Set for alias recording
    std::vector<std::string> pAliasSet;
  };
  
}// end namespace Model
