//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 18/07/2017
//
// ModelContext represents the container of all the
// objects that define the model.
// It is parsed by the parser to generate IR.
//

#pragma once

#include "ModelExportDefs.hpp"

#include "ContextObject.hpp"

#include <vector>
#include <memory>

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelContext {
  public:
    using CtxObjSPtr = std::shared_ptr<ContextObject>;
    using CtxObjVector = std::vector<CtxObjSPtr>;
    
  public:
    /// Constructor create a new instance of a model context and sets a default name
    ModelContext();
    
    /// Constructor create a new instance of a model context  with given name
    ModelContext(const std::string& aName);
    
    /// Returns model's name
    inline std::string getModelName() const { return pModelName; }
    
    /// Adds a context object into the context
    void addContextObject(const CtxObjSPtr& aCtxObj);
    
    /// Returns a vector containing all the objects in the context of given type
    const CtxObjVector& getContextObjectVectorByType(ContextObject::Type aType) const;
    
  private:
    /// Name of the model the context is representing
    std::string pModelName;
    
    /// Context object registry
    std::vector<CtxObjVector> pObjReg;
  };
  
}// end namespace Model
