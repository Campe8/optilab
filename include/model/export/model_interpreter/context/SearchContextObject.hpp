//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 10/8/2018
//
// Search context objects represent all the search
// information in a given model.
// For example, they encode branch statements information
// or cbls statements information.
//

#pragma once

// Base class
#include "ContextObject.hpp"

// Search information
#include "SearchInformation.hpp"

#include <memory>

namespace Model {
  
  class MODEL_EXPORT_CLASS SearchContextObject : public ContextObject {
  public:
    SearchContextObject(const std::string& aID, const Interpreter::DataObject& aDO);
    
    /// Returns the context object ID prefix
    static std::string getSearchContextObjectIdPrefix();
    
    /// Returns the search-specific information class
    inline std::shared_ptr<SearchInformation> getSearchStrategyInformation() const
    {
      return pSrcInformation;
    }
    
  private:
    /// Search specific information class
    std::shared_ptr<SearchInformation> pSrcInformation;
    
    /// Sets the search information as specified in "aDO"
    void setSearchInformation(const Interpreter::DataObject& aDO);
  };
  
}//end namespace Model
