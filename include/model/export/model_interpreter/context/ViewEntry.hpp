//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 12/14/2017
//
// Base class for entries in the context view.
// It represents a (potentialy) fully interpreted
// context object.
//

#pragma once

#include "ModelExportDefs.hpp"

#include <string>
#include <utility>

namespace Model {
  
  class MODEL_EXPORT_CLASS ViewEntry {
  public:
    /// Type of entry
    enum EntryCategory : int {
        EC_SCALAR = 0
      , EC_ARRAY
    };
    
    /// Represents the values stored in each entry of the view:
    /// - first: actual value
    /// - second: flag indicating whether "first" is fully interpreted or not
    /// For example:
    ///    <1, true>
    ///   <x[5], false>
    using EntryValue = std::pair<std::string, bool>;
    
  public:
    virtual ~ViewEntry() = default;
    
    /// Returns a cloned instance of this ViewEntry
    virtual ViewEntry* clone() = 0;
    
    inline EntryCategory getCategory() const
    {
      return pCategory;
    }
    
    inline std::string getID() const
    {
      return pID;
    }
    
    inline bool isFullyInterpreted() const
    {
      return pInterpreted;
    }
    
    /// Returns the symbol used by the view to
    /// represent non-interpreted expressions
    static std::string getNotInterpExpr();
    
    /// Returns this entrie's serialization.
    /// @note serialization should be implemented using
    /// the language structure of the ExprTk library.
    /// For more information see
    /// http://partow.net/programming/exprtk/index.html
    virtual std::string serialize() const = 0;
    
  protected:
    /// Symbol used by the view to
    /// represent non-interpreted expressions
    static std::string ninterp;
    
  protected:
    ViewEntry(const std::string& aID, EntryCategory aCat);
    
    /// Sets the interpreted flag:
    /// true: the view if fully interpreted;
    /// false: otherwise.
    /// @note by default this flag is false
    inline void setInterpreted(bool aInterp)
    {
      pInterpreted = aInterp;
    }
    
  private:
    /// Entry's ID
    std::string pID;
    
    /// Category of this entry
    EntryCategory pCategory;
    
    /// Fully interpreted entry flag
    bool pInterpreted;
  };
  
}// end namespace Model
