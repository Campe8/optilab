//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 2/19/2018
//

#pragma once

// Base class
#include "ContextObject.hpp"

#include <boost/logic/tribool.hpp>

namespace Model {
  
  class MODEL_EXPORT_CLASS VariableContextObject : public ContextObject {
  public:
    /// Constructor: sets type and dimensions of the variable
    VariableContextObject(const std::string& aVarName, const Interpreter::DataObject& aDO);
    
    /// Suppresses the output for this variable
    inline void suppressOutput() { pOutput = false; }
    
    /// Forces output for this variables
    inline void forceOutput() { pOutput = true; }
    
    /// Returns the semantic for the output on this variable.
    /// @note indeterminate state means "default output"
    inline boost::logic::tribool isOutput() const { return pOutput; }
    
  private:
    /// Output variable flag
    boost::logic::tribool pOutput;
  };
  
}//end namespace Model
