//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 10/9/2018
//
// Search information for DFS search strategies.
//

#pragma once

// Base class
#include "SearchInformation.hpp"

#include <string>
#include <vector>

namespace Model {
  
  class MODEL_EXPORT_CLASS DFSInformation : public SearchInformation {
  public:
    DFSInformation();
    
    ~DFSInformation() = default;
    
    /// Strategy name
    static const std::string StrategyName;
    
    /// Information key for variable selection choice
    static const std::string VariableSelectionKey;
    
    /// Information key for value selection choice
    static const std::string ValueSelectionKey;
    
    /// List of values for variable selection.
    /// @note value at index 0 is the default value
    static const std::vector<std::string> VariableSelectionChoiceList;
    
    /// List of values for domain value selection
    /// @note value at index 0 is the default value
    static const std::vector<std::string> ValueSelectionChoiceList;
    
    /// Returns the default heuristic choice for variable selection
    static std::string getDefaultVariableChoice() { return VariableSelectionChoiceList[0]; }
    
    /// Returns the default heuristic choice for value selection
    static std::string getDefaultValueChoice() { return ValueSelectionChoiceList[0]; }
    
    /// Utility function: sets variable choice
    /// @note throws if the given value is not one of the registered values
    void setVariableChoice(const std::string& aVarChoice);
    
    /// Utility function: sets value choice
    /// @note throws if the given value is not one of the registered values
    void setValueChoice(const std::string& aValChoice);
    
    /// Reads search options and initializes this search information object
    void readSearchOptionsObject(Interpreter::BaseObject* aObj) override;
  };
  
}//end namespace Model
