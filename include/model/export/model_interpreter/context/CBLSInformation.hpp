//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 10/9/2018
//
// Base class for cbls search information.
//

#pragma once

// Base class
#include "SearchInformation.hpp"

#include <cstddef>  // for std::size_t

namespace Model {
  
  class MODEL_EXPORT_CLASS CBLSInformation : public SearchInformation {
  public:
    virtual ~CBLSInformation() = default;
    
    /// Information key for neighborhood size
    static const std::string NeighborhoodSizeKey;
    
    /// Information key for number of neighborhoods
    static const std::string NumNeighborhoodsKey;
    
    /// Information key for aggressive cbls
    static const std::string AggressiveCBLSKey;
    
    /// Information key for random seed
    static const std::string RandomSeedKey;
    
    /// Information key for random seed
    static const std::string WarmStartKey;
    
    /// Reads search options and initializes this search information object
    void readSearchOptionsObject(Interpreter::BaseObject* aObj) override;
    
  protected:
    CBLSInformation(SearchInformationType aType);
    
    /// Strategy specific search options
    virtual void readStrategySearchOptionsObject(Interpreter::BaseObject* aObj){};
  };
  
}//end namespace Model
