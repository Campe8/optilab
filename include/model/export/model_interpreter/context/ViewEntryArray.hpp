//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 12/14/2017
//
// Entry for representing (fully) interpreted arrays.
//

#pragma once

// Base class
#include "ViewEntry.hpp"
#include "CLIUtils.hpp"

#include <vector>
#include <unordered_map>
#include <sstream>

namespace Model {
  
  class MODEL_EXPORT_CLASS ViewEntryArray : public ViewEntry {
  public:
    /// Creates a view entry of type Array given its identifier and the vector of strings representing the array
    ViewEntryArray(const std::string& aID, const std::vector<EntryValue>& aArray);
    
    static bool isa(const ViewEntry* aEntry);
    static ViewEntryArray* cast(ViewEntry* aEntry);
    
    ViewEntry* clone() override;
    
    std::size_t getArraySize() const
    {
      return getArray().size();
    }
    
    inline const std::vector<std::string>& getArray() const
    {
      return pArray;
    }
    
    /// Returns true if the element in the array at index "aIdx"
    /// needs interpretation, false otherwise
    inline bool needsInterpretation(std::size_t aIdx) const
    {
      return pNonInterpMap.find(aIdx) != pNonInterpMap.end();
    }
    
    /// Returns the value at index "aIdx" that needs interpretation.
    /// @note throws if no such value exists
    inline std::string getNeedsInterpretationValue(std::size_t aIdx) const
    {
      return pNonInterpMap.at(aIdx);
    }
    
    /// Sets the interpreted value at index aIdx.
    /// @note throws if "aIdx" is not a valid index
    void setInterpretedValue(std::size_t aIdx, const std::string& aValue);
    
    std::string serialize() const override;
    
  private:
    /// Dirty flag for serialization
    mutable bool pDirtyCache;
    
    /// Serialized version of this entry
    mutable std::string pSerialization;
    
    /// View array
    std::vector<std::string> pArray;
    
    /// Parses "aArrays" initializing internal data structures
    void parseArray(const std::vector<EntryValue>& aArray);
    
    /// Serializes this entry, i.e., pArray
    void serializeEntry() const;
    
    /// Map <array_index, value> of non-interpreted values
    std::unordered_map<std::size_t, std::string> pNonInterpMap;
  };
  
}// end namespace Model

