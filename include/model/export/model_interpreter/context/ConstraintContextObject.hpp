//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 2/20/2018
//

#pragma once

// Base class
#include "ContextObject.hpp"

namespace Model {
  
  class MODEL_EXPORT_CLASS ConstraintContextObject : public ContextObject {
  public:
    enum PropagationType : char {
        PT_BOUNDS = 0
      , PT_DOMAIN
      , PT_UNDEF
    };
    
  public:
    ConstraintContextObject(const std::string& aID, const Interpreter::DataObject& aDO);
    
    /// Set/get propagation type
    inline void setPropagationType(PropagationType aPropType) { pPropType = aPropType; }
    inline PropagationType getPropagationType() const { return pPropType; }
    
  private:
    /// Constraint propagation type
    PropagationType pPropType;
  };
  
}//end namespace Model
