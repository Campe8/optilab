//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 2/19/2018
//
// Base class for objects in the model context.
// This is basically a wrapper class around an
// instance of a data object.
// This is used to provide auxiliary information
// for some specific data objects.
// For example, data objects containing Variable
// (base) objects are treated in a different way
// w.r.t. any other object (e.g., pretty printer).
//

#pragma once

#include "ModelExportDefs.hpp"

// Data Object: the payload
#include "DataObject.hpp"

#include <string>

namespace Model {
  
  class MODEL_EXPORT_CLASS ContextObject {
  public:
    enum Type : int {
        CTX_OBJ_VAR = 0
      , CTX_OBJ_CON
      , CTX_OBJ_SRC
      , CTX_OBJ_DBO // DataObject Base Object
      , CTX_OBJ_STD // Standard model parameters (scalar, list, matrix)
      , CTX_OBJ_TYPE_LAST // must be last
    };
    
  public:
    /// Constructor: instantiates a new context object given its name (ID)
    /// and the data object it wraps
    ContextObject(const std::string& aID, const Interpreter::DataObject& aDO);
    
    virtual ~ContextObject();
    
    /// Returns ID
    inline std::string getID() const { return pID; }
    
    /// Returns this context object's payload
    inline Interpreter::DataObject& payload() { return pPayload; }
    inline const Interpreter::DataObject& payload() const { return pPayload; }
    
    /// Returns context object type
    inline Type getType() const { return pType; }
    
  protected:
    inline void setType(Type aType) { pType = aType; }
    
  private:
    /// Context object ID: every object in the context
    /// can be identified by a string name
    std::string pID;
    
    /// Type of the context object
    Type pType;
    
    /// The payload of the context object
    /// is represented by the DataObject it wraps
    Interpreter::DataObject pPayload;
  };
  
}//end namespace Model
