//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 12/11/2018
//
// PrettyPrinter tools
//

#pragma once

#include "ModelExportDefs.hpp"

#include "DataObject.hpp"

#include <iostream>  // for std::ostream
#include <string>
#include <vector>

namespace PrettyPrinter { namespace Tools {
  
  /// Pretty prints the given base object into the given output stream.
  /// If "aIndent" is provided, it uses indentation (double spaces).
  /// @note Skips printing the properties with keys in "aBlackList"
  MODEL_EXPORT_FUNCTION std::ostream& toStream(Interpreter::BaseObject* aObj, std::ostream& aOut,
                                               int aIndent=0,
                                               const std::vector<std::string>& aBlackList={});
  
  /// Pretty prints the given DataObject into the given output stream.
  /// If "aIndent" is provided, it uses indentation (double spaces).
  /// @note Skips printing the properties with keys in "aBlackList" if the given object
  /// is a class object
  MODEL_EXPORT_FUNCTION std::ostream& toStream(const Interpreter::DataObject& aObj,
                                               std::ostream& aOut, int aIndent=0,
                                               const std::vector<std::string>& aBlackList={});
  
  /// Pretty prints the type of the given object
  MODEL_EXPORT_FUNCTION std::ostream& typeToStream(const Interpreter::DataObject& aObj,
                                                   std::ostream& aOut);
  
  /// Returns true if the given string spans on multiple lines, false otherwise
  MODEL_EXPORT_FUNCTION bool spansMultipleLines(const std::string& aString);
  
  /// Indent the given string with an "aIndent" indentation level
  MODEL_EXPORT_FUNCTION void indentString(std::string& aString, int aIndent=0);
  
}} // end namespace Tools/PrettyPrinter
