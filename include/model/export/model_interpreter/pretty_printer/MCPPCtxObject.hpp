//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 11/05/2018
//
// This is a pretty printer for all the BaseObjects that are
// deriving from CtxObj.
//

#pragma once

// Base class
#include "ModelContextPrettyPrinter.hpp"

namespace Model {
  
	class MODEL_EXPORT_CLASS MCPPCtxObject : public ModelContextPrettyPrinter {
	public:
		void prettyPrint(const std::shared_ptr<ContextObject>& aObj, std::ostream& aOut,
                     bool aFullPrettyPrint) override;

	private:
		/// Pretty prints dimensions
		void prettyPrintDimensions(Interpreter::BaseObject* aPar, std::ostream& aOut, int aLevel=0);
	};

}// end namespace Model
