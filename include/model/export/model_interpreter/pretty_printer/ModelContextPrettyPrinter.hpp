//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 12/15/2017
//
// PrettyPrinter for ModelContext.
//

#pragma once

#include "ModelExportDefs.hpp"
#include "ContextObject.hpp"

// Forward declarations
namespace Interpreter {
  class BaseObject;
}// end namespace Interpreter

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelContextPrettyPrinter {
  public:
		/// Pretty prints the given data object which is identified by the given name
    virtual void prettyPrint(const std::shared_ptr<ContextObject>& aObj, std::ostream& aOut,
                             bool aFullPrettyPrint) = 0;

	protected:
    /// Returns the indentation used to pretty print the tags
    std::string getIndent(int aLevel = 0);
    
    /// Returns a new line and the indentation used to pretty print the tags
    std::string getIndentLn(int aLevel = 0);
    
    /// Pretty prints helper function, pretty prints "aTag" from <tag, value> pair
    /// on "aOut" at level of indent "aLevel".
    /// @note level of indentation is 0 by default
    void outTag(const std::string& aTag, std::ostream& aOut, int aLevel = 0);
    
    /// As outTag but with new line
    void outTagLn(const std::string& aTag, std::ostream& aOut, int aLevel = 0);
    
    /// Utility function: prints the type of the DataObject it wraps.
    /// For example, if "aObj" is a VariableContextObject, its type is "Variable"
    void printType(const std::shared_ptr<ContextObject>& aObj, std::ostream& aOut, int aLevel = 0);
    
    /// Utility function: prints the ID of the given ContextObject and the
    /// type of the DataObject it wraps.
    /// For example, if "aObj" is a VariableContextObject, its type is "Variable"
    /// @note type of object is printed only when "aFullPrettyPrint" is true
    void printIDAndType(const std::shared_ptr<ContextObject>& aObj, std::ostream& aOut,
                        bool aFullPrettyPrint);
    
    /// Utility function: prints the ID of the given ContextObject
    void printID(const std::shared_ptr<ContextObject>& aObj, std::ostream& aOut);
    
    /// Utility function: pretty prints fully interpreted information
    void prettyPrintFullyInterpreted(Interpreter::BaseObject* aObj, std::ostream& aOut);
    void prettyPrintFullyInterpreted(bool aFullyInterpreted, std::ostream& aOut);
  };
  
}// end namespace Model
