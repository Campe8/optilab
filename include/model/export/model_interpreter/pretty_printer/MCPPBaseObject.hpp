//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 12/15/2017
//

#pragma once

// Base class
#include "ModelContextPrettyPrinter.hpp"

namespace Model {
  
	class MODEL_EXPORT_CLASS MCPPBaseObject : public ModelContextPrettyPrinter {
	public:
		void prettyPrint(const std::shared_ptr<ContextObject>& aObj, std::ostream& aOut,
                     bool aFullPrettyPrint) override;
	};

}// end namespace Model
