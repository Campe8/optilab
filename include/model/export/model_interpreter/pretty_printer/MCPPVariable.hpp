//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 12/15/2017
//

#pragma once

// Base class
#include "ModelContextPrettyPrinter.hpp"

namespace Model {
  
	class MODEL_EXPORT_CLASS MCPPVariable : public ModelContextPrettyPrinter {
	public:
		void prettyPrint(const std::shared_ptr<ContextObject>& aObj, std::ostream& aOut, bool aFullPrettyPrint) override;

	private:
		/// Pretty prints variable ID
		void prettyPrintID(const std::string& aName, std::ostream& aOut);
    
    /// Pretty prints variable input order policy
    void prettyPrintInputOrder(Interpreter::BaseObject* aVar, std::ostream& aOut);
    
    /// Pretty prints variable semantic
    void prettyPrintSemantic(Interpreter::BaseObject* aVar, std::ostream& aOut, bool aFullPrettyPrint);
    
		/// Pretty prints the domain of the variable
		void prettyPrintDomain(Interpreter::BaseObject* aVar, std::ostream& aOut);
    
    /// Pretty prints variable dimensions
    void prettyPrintDimensions(Interpreter::BaseObject* aVar, std::ostream& aOut);
    
    /// Pretty prints the domain and its dimensions
    void prettyPrintVarDomain(Interpreter::BaseObject* aVar, std::ostream& aOut);
    
    /// Pretty prints the domain of a subscript variable
    void prettyPrintVarSubscriptDomain(Interpreter::BaseObject* aVar, std::ostream& aOut);
    
    /// Pretty prints the subscript indices
    void prettyPrintSubscript(Interpreter::BaseObject* aVar, std::ostream& aOut);
	};

}// end namespace Model
