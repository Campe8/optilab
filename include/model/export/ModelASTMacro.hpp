//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/03/2017
//

#pragma once

#define MDL_AST_TRUE "true"
#define MDL_AST_FALSE "false"

#define MDL_AST_INSTANCE "instance"

// Macros for Model Abstract Syntax Tree
#include "ModelASTMacroVariable.hpp"
#include "ModelASTMacroConstraint.hpp"
#include "ModelASTMacroMatrix.hpp"
#include "ModelASTMacroBranch.hpp"
#include "ModelASTMacroSearchSemantic.hpp"

#define MDL_AST_OBJ_KEY               "_objkey"		// unique key of each obj
#define MDL_AST_OBJ_TYPE              "_objtype"	// object type
#define MDL_AST_OBJ_FINTERP           "_finterp"  // object is fully interpreted
#define MDL_AST_ROOT_NAME             "name"			// string
#define MDL_AST_ROOT_FRAMEWORK_TYPE   "type"			// CSP, COP, ...
#define MDL_AST_ROOT_SOLUTION_LIMIT   "solution"	// -1, 0, 1, 2, ...

#define MDL_AST_ROOT_FRAMEWORK_TYPE_CSP "CSP"
#define MDL_AST_ROOT_FRAMEWORK_TYPE_COP "COP"

#define MDL_AST_OBJ_TYPE_VAR	"var"
#define MDL_AST_OBJ_TYPE_CON	"con"
#define MDL_AST_OBJ_TYPE_MAT  "mat"
#define MDL_AST_OBJ_TYPE_BRC	"brc"
#define MDL_AST_OBJ_TYPE_SRC  "src"
