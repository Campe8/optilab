//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 03/27/2017
//

#pragma once

#include "ModelExportDefs.hpp"

namespace Model {
  
  enum ModelObjectClass : int {
      MODEL_OBJ_VARIABLE = 0
    , MODEL_OBJ_CONSTRAINT
    , MODEL_OBJ_PARAMETER
    , MODEL_OBJ_SEARCH_SEMANTIC
    , MODEL_OBJ_LAST // must be last
  };
  
  MODEL_EXPORT_FUNCTION void throwLogicErrorWithMessage(const char* aMsg);
  
} // end namespace Model
