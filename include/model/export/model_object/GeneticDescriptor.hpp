//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 10/12/2018
//
// Genetic search sescriptor class for genetic CBLS strategy.
//

#pragma once

// Base class
#include "CBLSDescriptor.hpp"

namespace Model {
  
  class GeneticDescriptor : public CBLSDescriptor {
  public:
    GeneticDescriptor();
    
    virtual ~GeneticDescriptor() = default;
    
    /// Returns size of the population
    inline std::size_t getPopulationSize() const { return pPopulationSize; }
    
    /// Returns number of generations
    inline std::size_t getNumGenerations() const { return pNumGenerations; }
    
    /// Returns mutation chance
    inline std::size_t getMutationChance() const { return pMutationChance; }
    
    /// Returns number of genes to mutate
    inline std::size_t getMutationSize() const { return pMutationSize; }
    
  protected:
    /// Initialize genetic strategy specific information
    void initCBLSSearchDescriptor(const SearchInformation* const aSrcInfo) override;
    
  private:
    /// Population size
    std::size_t pPopulationSize = 0;
    
    /// Number of generations
    std::size_t pNumGenerations = 0;
    
    /// Mutation chance
    std::size_t pMutationChance = 0;
    
    /// Mutation size
    std::size_t pMutationSize = 0;
  };

}// end namespace Model
