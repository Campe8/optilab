//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 10/11/2018
//
// Base descriptor class for CBLS search strategy.
//

#pragma once

#include "SearchDescriptor.hpp"

#include <cstddef>  // for std::size_t
#include <exception> // for std::bad_alloc

namespace Model {
  
  class CBLSDescriptor : public SearchDescriptor {
  public:
    virtual ~CBLSDescriptor() = default;
    
    /// Returns the value for the random seed
    inline int getRandomSeed() const { return pRandomSeed; }
    
    /// Returns true for warm start CBLS, false otherwise
    inline bool getWarmStart() const { return pWarmStart; }
    
    /// Returns size of neighborhoods
    inline int getNeighborhoodSize() const { return pNeighbohroodSize; }
    
    /// Returns number of neighborhoods
    inline int getNumNeighborhoods() const { return pNumNeighborhoods; }
    
    /// Returns true if cbls is aggressive, false otherwise
    inline bool isAggressiveCBLS() const { return pAggressiveCBLS; }
    
  protected:
    CBLSDescriptor(Search::SearchEngineStrategyType aSrcType);
    
    /// Initalizes derived search descriptors from the given SearchInformation instance
    virtual void initInnerSearchDescriptor(const SearchInformation* const aSrcInfo) override;
    
    /// Initialize CBLS strategy specific information
    virtual void initCBLSSearchDescriptor(const SearchInformation* const aSrcInfo) {};
    
    /// Checks "aVals <= aLimit" and throws a bad_alloc if not
    template<typename T>
    void checkAndThrow(T aVal, std::size_t aLimit)
    {
      if (aVal > static_cast<T>(aLimit)) throw std::bad_alloc();
    }
    
  private:
    /// Random seed to be used for the random number generator
    int pRandomSeed = 0;
    
    /// Warm start flag
    bool pWarmStart = true;
    
    /// Neighborhood size
    int pNeighbohroodSize = -1;
    
    /// Num neighborhoods
    int pNumNeighborhoods = 1;
    
    /// Agressive cbls
    bool pAggressiveCBLS = true;
  };

}// end namespace Model
