//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 03/27/2017
//
// Model object that wraps a parameter entity.
// A parameter in a model is extra information used
// by constraints for their propagation.
// For example, a parameter could be a table of data
// for the table constraint.
// Therefore, a parameter object encapsulates a
// ConstraintParameter object.
// In turn, a ConstraintParameter object can be:
// - variable
// - domain
// - matrix of domains
// - ...
//

#pragma once

// Base class
#include "ModelObject.hpp"

// Parameter
#include "ConstraintParameter.hpp"

#include <memory>

// Forward declarations
namespace Model {
  class Model;
}//end namespace model 

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelParameter : public ModelObject {
  public:
    using ParameterSPtr = std::shared_ptr<Core::ConstraintParameter>;
    
  public:
		virtual ~ModelParameter() = default;

		static bool isa(const ModelObject* aModelObject);
		static ModelParameter* cast(ModelObject* aModelObject);
		static const ModelParameter* cast(const ModelObject* aModelObject);
    
		std::string getID() const override;
    
    /// Returns the parameter object encapsulated by this model object
    inline ParameterSPtr parameter() const { return pParameter; }
    
  protected:
    friend class ModelObjectBuilderPar;
    friend class ParameterGenerator;
    
    ModelParameter(const std::string& aParamID, const ParameterSPtr& aParam);

	private:
    /// Parameter ID
    std::string pID;
    
		/// Parameter instance
		ParameterSPtr pParameter;
  };
  
}// end namespace Model
