//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 03/27/2017
//
// ModelObject is the base class for the objects
// describing a decision or optimization model:
// - variable: wrapper around a CoreObj Variable
// - constraints: wrapper around a CoreObj Constraint
// - parameters: list or domains (constraint parameters)
// - etc.
//

#pragma once

#include "ModelExportDefs.hpp"
#include "ModelDefs.hpp"

#include <boost/core/noncopyable.hpp>

#include <string>
#include <memory>

// Forward declarations
namespace Model {
  class ModelObject;
  using ModelObjectUPtr = std::unique_ptr<ModelObject>;
}// end namespace Model

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelObject : private boost::noncopyable {
  public:
    virtual ~ModelObject() = default;
    
    /// Returns the string ID of this model object.
    /// This is the unique ID that the object used to be identified
    /// in the model context
    virtual std::string getID() const = 0;
    
    /// Returns the class of this ModelObject
    inline ModelObjectClass getModelObjectClass() const { return pClass; }
    
  protected:
    ModelObject(ModelObjectClass aClass);
    
  private:
    ModelObjectClass pClass;
  };
  
}// end namespace Model
