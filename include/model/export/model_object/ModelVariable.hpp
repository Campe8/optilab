//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 03/27/2017
//
// Model object that wraps a decision variable entity.
//

#pragma once

// Base class
#include "ModelObject.hpp"

#include "VariableSemantic.hpp"

#include <string>
#include <vector>
#include <memory>

// Forward declarations
namespace Core {
  class Variable;
}// end namespace Core

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelVariable : public ModelObject {
	public:
		using VariableSPtr = std::shared_ptr<Core::Variable>;
    
    /// Semantic list mapping search enviornment IDs with the semantic
    /// of the variable
    using SemanticList = std::vector<std::pair<std::string, Core::VariableSemanticSPtr>>;

  public:
		virtual ~ModelVariable() = default;

		static bool isa(const ModelObject* aModelObject);
		static ModelVariable* cast(ModelObject* aModelObject);
		static const ModelVariable* cast(const ModelObject* aModelObject);
    
		std::string getID() const override;

    /// Returns the variable encapsulated by this model object
    inline VariableSPtr variable() const { return pVariable; }
    
    /// Set/get semantic list
    inline void setSemanticList(const SemanticList& aList) { pSemanticList = aList; }
    inline const SemanticList& getSemanticList() const { return pSemanticList; }
    
  protected:
    friend class ModelObjectBuilderVar;
    friend class VariableGenerator;
    
		ModelVariable(const VariableSPtr& aVar);
    
  private:
		/// Variable hold by this object
		VariableSPtr pVariable;
    
    /// List of the semantics of the variable.
    /// @note a variable can have multiple-semantics
    /// depending on which search environment it belongs to
    SemanticList pSemanticList;
  };
  
}// end namespace Model
