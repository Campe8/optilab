//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 11/25/2017
//
// Model object that wraps search information used
// by the back-end solver to explore the search space.
//

#pragma once

// Base class
#include "ModelObject.hpp"

#include "SearchDescriptor.hpp"

#include <vector>
#include <utility>
#include <memory>

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelSearch : public ModelObject {

  public:
		virtual ~ModelSearch() = default;

		static bool isa(const ModelObject* aModelObject);
		static ModelSearch* cast(ModelObject* aModelObject);
		static const ModelSearch* cast(const ModelObject* aModelObject);
    
		std::string getID() const override;
    
    /// Returns the search descriptor
    inline std::shared_ptr<SearchDescriptor> getSearchDescriptor() const { return pDescriptor; }
    
  protected:
    friend class ModelObjectBuilderSrc;
    friend class SearchGenerator;
    
    ModelSearch(const std::string& aID, const std::shared_ptr<SearchDescriptor>& aDescriptor);
    
  private:
    /// Search semantic identifies
    std::string pID;
    
    /// Search descriptor
    std::shared_ptr<SearchDescriptor> pDescriptor;
  };
  
}// end namespace Model
