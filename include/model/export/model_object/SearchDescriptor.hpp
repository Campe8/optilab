//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 5/14/2018
//
// Descriptor class for search semantics.
// It describes the search environment.
//

#pragma once

#include "ModelExportDefs.hpp"

#include "SearchHeuristicDefs.hpp"
#include "SearchDefs.hpp"
#include "VariableSemantic.hpp"

#include <vector>

// Forward declarations
namespace Model {
  class SearchInformation;
}// end namespace Model

namespace Model {
  
  class SearchDescriptor {
  public:
    using VarSemPair = std::pair<std::string, Core::VariableSemanticSPtr>;
    using SemanticList = std::vector<VarSemPair>;
    
  public:
    virtual ~SearchDescriptor() = default;
    
    /// Initializes the descriptor with the information
    /// contained in the given SearchInformation instance
    void initSearchDescriptor(const SearchInformation* const aSrcInfo);
    
    /// Returns the search engine strategy
    inline Search::SearchEngineStrategyType getSearchStrategy() const { return pSearchStrategyType; }
    
    /// Sets the semantic list
    inline void setSemanticList(const SemanticList& aSemList) { pSemanticList = aSemList; }
    
    /// Returns the semantic list
    inline const SemanticList& getSemanticList() const { return pSemanticList; }
    
    /// Returns the timeout in seconds.
    /// @note a negative timeout means no timeout
    inline int getTimeout() const { return pTimeout; }
    
    /// Returns the variable choice heuristic
    inline Search::VariableChoiceMetricType getVariableChoice() const { return pVarChoice; }
    
    /// Returns the value choice heuristic
    inline Search::ValueChoiceMetricType getValueChoice() const { return pValChoice; }
    
  protected:
    /**
     * SearchDescriptor: describes the search environment and heuristics.
     * @note when more advanced search environment will be available
     * (current version OptiLab 1.0), make this a base class and
     * derive from this.
     * @note 10/03/2018 deriving from this class for genetic algorithm
     * search descriptors
     */
    SearchDescriptor(Search::SearchEngineStrategyType aStrategyType);
    
    /// Initalizes derived search descriptors from the given SearchInformation instance
    virtual void initInnerSearchDescriptor(const SearchInformation* const) {};
    
    /// Sets the timeout to the specified value
    inline void setTimeout(int timeout)
    {
      pTimeout = timeout < 0 ? -1 : timeout;
    }
    
    /// Sets choices for variable and value selection
    void setVarValChoiceHeuristic(Search::VariableChoiceMetricType aVarChoice,
                                  Search::ValueChoiceMetricType aValChoice);
    
  private:
    /// Timeout for serch in seconds
    int pTimeout;
    
    /// Search strategy type
    Search::SearchEngineStrategyType pSearchStrategyType;
    
    /// Variable choice
    Search::VariableChoiceMetricType pVarChoice;
    
    /// Value choice
    Search::ValueChoiceMetricType pValChoice;
    
    /// List of the variable IDs and their semantics
    SemanticList pSemanticList;
  };

}// end namespace Model
