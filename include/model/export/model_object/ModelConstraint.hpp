//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 03/27/2017
//
// Model object that wraps a constraint entity.
//

#pragma once

// Base class
#include "ModelObject.hpp"

#include "ConstraintDefs.hpp"
#include "ConstraintParameter.hpp"

#include <vector>

// Forward declarations
namespace Core {
  class Constraint;
}// end namespace Core

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelConstraint : public ModelObject {
  public:
    using ConstraintSPtr = std::shared_ptr<Core::Constraint>;
  
  public:
    virtual ~ModelConstraint() = default;
    
		static bool isa(const ModelObject* aModelObject);
		static ModelConstraint* cast(ModelObject* aModelObject);
		static const ModelConstraint* cast(const ModelObject* aModelObject);
    
		std::string getID() const override;

    /// Returns the constraint object encapsulated by this model object
		inline ConstraintSPtr constraint() const { return pConstraint; }
    
  protected:
    friend class ModelObjectBuilderCon;
    friend class ConstraintGenerator;
    
    ModelConstraint(const std::string& aCtxID, const ConstraintSPtr& aConstraint);
    
  private:
    /// Constraint context ID is the ID given by the context
    /// this constraint has been generated from
    std::string pCtxID;
    
    /// Constraint instance
    ConstraintSPtr pConstraint;
  };
  
}// end namespace Model
