//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 10/11/2018
//
// Descriptor class for DFS search strategies.
//

#pragma once

#include "SearchDescriptor.hpp"

namespace Model {
  
  class DFSDescriptor : public SearchDescriptor {
  public:
    DFSDescriptor();
    
    virtual ~DFSDescriptor() = default;
    
  protected:
    /// Initalizes derived search descriptors from the given SearchInformation instance
    void initInnerSearchDescriptor(const SearchInformation* const aSrcInfo) override;
  };

}// end namespace Model
