//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/09/2017
//

#pragma once

// Macros for Abstract Syntax Tree - variable declaration

// Context id
#define MDL_AST_SUBTREE_VAR  "variables"

// Identifiers for sub-context-leaves
#define MDL_AST_LEAF_VAR                          "var"
#define MDL_AST_LEAF_VAR_ID                       "id"          // string
#define MDL_AST_LEAF_VAR_TYPE                     "type"        // integer, boolean, etc.
#define MDL_AST_LEAF_VAR_BRANCHING                "branching"   // true, false  optional
#define MDL_AST_LEAF_VAR_INPUT_ORDER              "input_order" // 0, 1, 2, ... optional
#define MDL_AST_LEAF_VAR_OUTPUT                   "output"      // true, false
#define MDL_AST_LEAF_VAR_DOMAIN                   "domain"
#define MDL_AST_LEAF_VAR_SEMANTIC_TYPE_DECISION   "decision"
#define MDL_AST_LEAF_VAR_SEMANTIC_TYPE_SUPPORT    "support"
#define MDL_AST_LEAF_VAR_SEMANTIC_TYPE_OBJECTIVE  "objective"
#define MDL_AST_LEAF_VAR_SEMANTIC                 "semantic"    // decision, support, objective

#define MDL_AST_LEAF_VAR_DIMENSIONS               "dimensions"

#define MDL_AST_LEAF_VAR_SEMANTIC_TYPE            "type"     // decision, support, ...
#define MDL_AST_LEAF_VAR_SEMANTIC_OPT_EXTREMUM    "extremum" // maximize, minimize
#define MDL_AST_LEAF_VAR_DOM_TYPE                 "type"     // bounds, extensional
#define MDL_AST_LEAF_VAR_DOM_VALS                 "values"   // [1, 2, ...]

#define MDL_AST_LEAF_VAR_SEMANTIC_OPT_EXTREMUM_MAX "maximize"
#define MDL_AST_LEAF_VAR_SEMANTIC_OPT_EXTREMUM_MIN "minimize"

#define MDL_AST_LEAF_VAR_TYPE_INT   "integer"
#define MDL_AST_LEAF_VAR_TYPE_BOOL  "boolean"

#define MDL_AST_LEAF_VAR_DOM_TYPE_BOUNDS      "bounds"
#define MDL_AST_LEAF_VAR_DOM_TYPE_EXTENSIONAL "extensional"
