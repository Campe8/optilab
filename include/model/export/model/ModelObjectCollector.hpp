//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 11/29/2017
//
// The collector is a hash table that maps
// model object to their IDs (string, UUID, etc.)
// and allows the client to perform different lookups.
//

#pragma once

#include "ModelExportDefs.hpp"

// Base object class used on UUID(s)
#include "CoreObject.hpp"

// Model object
#include "ModelDefs.hpp"
#include "ModelObject.hpp"

// i love u <3
#include <string>
#include <memory>
#include <unordered_map>
#include <utility>
#include <vector>

namespace Model {

  class MODEL_EXPORT_CLASS ModelObjectCollector {
  public:
    using ObjMapKey = std::pair<int, std::size_t>;
    
  public:
    
    /// Maps the object into the collector.
    /// It uses UUIDs as primary keys.
    /// It uses string IDs as secondary keys if
    /// the object has reference string IDs (e.g., variables).
    /// Returns the key used to map "aMObj"
    ObjMapKey mapObject(ModelObjectUPtr aMObj);
    
    /// Returns the number of objects of a given object class
    std::size_t numObjOfClass(ModelObjectClass aClass) const;
    
    /// Returns the "aIdx" instance "aModelObjectClass" object,
    /// Returns nullptr if no such obj can be found
    const ModelObject* getObjOfClass(ModelObjectClass aClass, std::size_t aIdx) const;
    
    /// Lookup by UUID, returns nullptr if no object can be found
    ModelObject* lookup(const Core::UUIDCoreObj& aUUID) const;
    
    /// Lookup by string ID, returns nullptr if no object can be found
    ModelObject* lookup(const std::string& aSID) const;
    
  protected:
    friend class Model;
    ModelObjectCollector() = default;
    
  private:
    using UUID = Core::UUIDCoreObj;
    using ModelObjPtr = ModelObject*;
    using ModelObjSPtr = std::shared_ptr<ModelObject>;
    using ModelObjClass = int;
    using ObjVector = std::vector<ModelObjSPtr>;
    
  private:
    /// Map a model object to its class
    std::unordered_map<ModelObjClass, ObjVector> pClassToObjMap;
    
    /// Map UUIDs to obj map keys
    std::unordered_map<UUID, ObjMapKey> pUUIDToObjKeyMap;
    
    /// Map string ID to obj map keys
    std::unordered_map<std::string, ObjMapKey> pStringIDToObjKeyMap;
    
    /// Maps "aObj" to its class, returns the key into the object map
    ObjMapKey mapObjectToClass(ModelObject* aObj);
    
    /// Returns the object mapped with "aObjMapKey" or nullptr
    /// if no such object is mapped
    ModelObject* getObjectFromClass(const ObjMapKey& aObjMapKey) const;
    
    /// Maps UUIDS to the given object key
    void mapUUID(const UUID& aUUID, const ObjMapKey& aObjMapKey);
    
    /// Maps string ID to the given object key
    void mapStringID(const std::string& aSID, const ObjMapKey& aObjMapKey);
  };
  
} // end namespace Model
