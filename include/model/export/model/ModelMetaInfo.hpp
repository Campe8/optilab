//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 01/13/2018
//

#pragma once

#include "ModelExportDefs.hpp"

#include "MetricsManager.hpp"

#include <memory>
#include <string>
#include <unordered_set>

// Forward declarations
namespace Model {
  class ModelMetaInfo;
  using ModelMetaInfoSPtr = std::shared_ptr<ModelMetaInfo>;
}// end namespace Model

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelMetaInfo {
  public:
    /// Constructor provides a model name and a limit on the number of solutions.
    /// If the model name is an empty string, the model name will be a random generated string
    /// starting with "__";
    ModelMetaInfo(const std::string& aModelName="", long long aSolutionLimit=1);
    
    /// Enable metrics reporting
    inline void enableMetrics() { pMetricsEnabled = true; }
    
    /// Returns true if metrics for this model are enabled, false otherwise.
    /// @note by default metrics are disabled
    inline bool isMetricsEnabled() const { return pMetricsEnabled || pMetrics.empty(); }
    
    /// Adds a metrics category on the categories of metrics to report
    inline void addMetricsCategory(Base::Tools::MetricsManager::MetricsCategory aCategory)
    {
      pMetrics.insert(static_cast<int>(aCategory));
    }
    
    /// Returns true if the given metric category should be reported, false otherwise
    inline bool isMetricCategoryEnabled(Base::Tools::MetricsManager::MetricsCategory aCategory) const
    {
      return pMetrics.find(static_cast<int>(aCategory)) != pMetrics.end();
    }
    
    /// Sets the name of the model, i.e., its identifier
    inline void setModelName(const std::string& aName) { pModelName = aName; }
    
    /// Sets the limit on the number of solutions
    inline void setSolutionLimit(long long aSolutionLimit) { pSolutionLimit = aSolutionLimit; }
    
    /// Returns the name of the model, i.e., its identifier
    inline std::string modelName() const { return pModelName; }
    
    /// Returns the limit on the number of solutions
    inline long long solutionLimit() const { return pSolutionLimit; }
    
  private:
    using MetricsCategories = std::unordered_set<int>;
    
  private:
    /// Model name
    std::string pModelName;
    
    /// Solution limit
    long long pSolutionLimit;
    
    /// Flag for enabling metrics
    bool pMetricsEnabled;
    
    /// Set of metrics categories to report
    MetricsCategories pMetrics;
  };
  
} // end namespace Model
