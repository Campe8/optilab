//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/09/2016
//

#pragma once

namespace Core {
  
  enum CoreObjectClass : int {
      CORE_OBJECT_VARIABLE = 0
    , CORE_OBJECT_CONSTRAINT
    , CORE_OBJECT_UNDEF
  };
  
} // end namespace Core
