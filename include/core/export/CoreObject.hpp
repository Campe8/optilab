//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/09/2016
//

#pragma once

// UUID definitions
#include "BaseUtils.hpp"

// Export definitions
#include "CoreExportDefs.hpp"

// Types and macros
#include "CoreObjectDefs.hpp"

namespace Core {
  
  typedef UUIDTag UUIDCoreObj;
  
  class CORE_EXPORT_CLASS CoreObject {
  public:
    virtual ~CoreObject();
    
    inline CoreObjectClass getCoreObjectClass() const
    {
      return pCoreObjectClass;
    }
    
    virtual bool operator==(const CoreObject& aOtherObject) const
    {
      return pUUIDTag == aOtherObject.pUUIDTag;
    }
    
    // Returns the UUID tag of this object
    inline UUIDCoreObj getUUID() const
    {
      return pUUIDTag;
    }
  
  protected:
    CoreObject(CoreObjectClass aCoreObjectClass);
    
  private:
    /// UUID tag - derive from model obj
    UUIDCoreObj pUUIDTag;
    
    /// Semantic class this variables belongs to
    CoreObjectClass pCoreObjectClass;
  };
  
} // end namespace Core
