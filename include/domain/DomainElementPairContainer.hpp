//
// Copyright OptiLab 2016-2017. All rights reserved.
//
// Created by Federico Campeotto on 11/24/2016
//
// Domain container for pairs of values.
// This container DOES NOT depend on the types of DomainElement
// contained in this container.
//

#pragma once

// Base class
#include "DomainElementContainer.hpp"

// Domain definitions and macros
#include "DomainDefs.hpp"

// Domain element manager
#include "DomainElementManager.hpp"

// Domain storage
#include "DomainConcreteStorage.hpp"

#include <utility>
#include <cassert>
#include <unordered_set>
#include <memory>

// Forward decl.
namespace Core {
  // Domain elements
  class DomainElement;
  
  // Decorator as observer
  class DomainDecorator;
}

namespace Core {
  
  class DomainElementPairContainer : public DomainElementContainer {
  public:
    /// Constructor for a pair <"aFirst", "aSecond">
    /// @note "aFirst" must be less than or equal to "aSecond"
    DomainElementPairContainer(DomainElement *aFirst, DomainElement *aSecond);
    
    DomainElementPairContainer(const DomainElementPairContainer& aOther);
    
    DomainElementPairContainer(DomainElementPairContainer&& aOther);
    
    virtual ~DomainElementPairContainer();
    
    DomainElementPairContainer& operator= (const DomainElementPairContainer& aOther);
    
    DomainElementPairContainer& operator= (DomainElementPairContainer&& aOther);
    
    static bool isa(const DomainElementContainer* aContainer)
    {
      assert(aContainer);
      return aContainer->getContainerClass() == DomainElementContainerClass::DOM_ELEM_CONTAINER_PAIR;
    }
    
    static DomainElementPairContainer* cast(DomainElementContainer* aContainer)
    {
      assert(aContainer);
      assert(isa(aContainer));
      
      return static_cast<DomainElementPairContainer*>(aContainer);
    }
    
    /// Return true if the domain contains "aDomainElement", false otherwise
    bool contains(DomainElement *aDomainElement) override;
  
    /// Returns a snapshot of the current internal sate
    std::unique_ptr<DomainState> getDomainSnapshot() override;
    
  protected:
    inline DomainElement* getFirst() const
    {
      return pDomainElementPair.first;
    }
    
    inline DomainElement* getSecond() const
    {
      return pDomainElementPair.second;
    }
    
    /// Get domain size
    std::size_t getDomainSize() const override;
    
    /// Get domain's lower bound
    DomainElement *getLowerBound() override;
    
    /// Get domain's upper bound
    DomainElement *getUpperBound() override;
    
    /// Get element greater than given element, return VoidElementVoid otherwise
    DomainElement *getElementGreaterThan(DomainElement *aDomainElement) override;
    
    /// Get element less than given element, return VoidElementVoid otherwise
    DomainElement *getElementLessThan(DomainElement *aDomainElement) override;
    
    /// Get "index"^th element where 1 <= index <= getDomainSize(), return VoidElementVoid otherwise
    DomainElement *getThElement(std::size_t aElementThIndex) override;
    
    /// Shrink domain to specified bounds <"aLowerBound", "aUpperBound">
    void shrinkOnBounds(DomainElement *aLowerBound, DomainElement *aUpperBound) override;
    
    /// Intersect the elements in this container with the elements in "aDomainElementContainer"
    void intersectWithDomainElementContainer(DomainElementContainer *aDomainElementContainer) override;
    
    /// Intersect the elements in this container with the elements in "aDomainElementArray"
    void intersectWithArray(DomainElementArray *aDomainElementArray) override;
    
    /// Subtract "aDomainElement" from the set of elements in the domain
    void subtractElement(DomainElement *aDomainElement) override;
    
    /// Subtract "aDomainElement" from the set of elements in the domain
    void subtractDomainElementContainer(DomainElementContainer *aDomainElementContainer) override;
    
    /// Subtract "aDomainElement" from the set of elements in the domain
    void subtractArray(DomainElementArray *aDomainElementArray) override;
    
    /// Shrink domain to <"aDomainElement", upperBound> if aDomainElement > lowerBound
    void shrinkOnMinBound(DomainElement *aDomainElement) override;
    
    /// Shrink domain to <lowerBound, "aDomainElement"> if aDomainElement < upperBound
    void shrinkOnMaxBound(DomainElement *aDomainElement) override;
    
    /// Updates internal state to "aDomainSnapshot"
    void reinstateInternalDomainSnapshot(const DomainState* aDomainSnapshot) override;
    
  private:
    /// Pair of elements in this container
    std::pair<DomainElement*, DomainElement*> pDomainElementPair;
    
    /// Empty domain
    void emptyDomain();
  };
  
} // end namespace Core
