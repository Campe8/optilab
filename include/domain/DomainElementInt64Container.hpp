//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 10/06/2016
//

#pragma once

// Base class
#include "DomainElementContainer.hpp"

// Domain definitions and macros
#include "DomainDefs.hpp"

// Domain element manager
#include "DomainElementManager.hpp"

// Domain storage
#include "DomainConcreteStorage.hpp"

#include <cassert>
#include <unordered_set>
#include <memory>

// Forward decl.
namespace Core {
  // Domain elements
  class DomainElement;
  class DomainElementInt64;
  
  // Decorator as observer
  class DomainDecorator;
}

namespace Core {
  
  class DomainElementInt64Container : public DomainElementContainer {
  public:
    DomainElementInt64Container(DomainElementInt64 *aLowerBound, DomainElementInt64 *aUpperBound);
    
    DomainElementInt64Container(const std::unordered_set<DomainElementInt64 *>& aSetOfElements);
    
    DomainElementInt64Container(const DomainElementInt64Container& aOther);
    
    DomainElementInt64Container(DomainElementInt64Container&& aOther);
    
    virtual ~DomainElementInt64Container();
    
    DomainElementInt64Container& operator= (const DomainElementInt64Container& aOther);
    
    DomainElementInt64Container& operator= (DomainElementInt64Container&& aOther);
    
    static bool isa(const DomainElementContainer* aContainer)
    {
      assert(aContainer);
      return aContainer->getContainerClass() == DomainElementContainerClass::DOM_ELEM_CONTAINER_INT64;
    }
    
    static DomainElementInt64Container* cast(DomainElementContainer* aContainer)
    {
      assert(aContainer);
      assert(isa(aContainer));
      
      return static_cast<DomainElementInt64Container*>(aContainer);
    }
    
    /// Return true if the domain contains "aDomainElement", false otherwise
    bool contains(DomainElement *aDomainElement) override;
    
    /// Returns a snapshot of the current internal sate
    std::unique_ptr<DomainState> getDomainSnapshot() override;
  
  protected:
    /// Get domain size
    std::size_t getDomainSize() const override;
    
    /// Get domain's lower bound
    DomainElement *getLowerBound() override;
    
    /// Get domain's upper bound
    DomainElement *getUpperBound() override;
    
    /// Get element greater than given element, return VoidElementVoid otherwise
    DomainElement *getElementGreaterThan(DomainElement *aDomainElement) override;
    
    /// Get element less than given element, return VoidElementVoid otherwise
    DomainElement *getElementLessThan(DomainElement *aDomainElement) override;
    
    /// Get "index"^th element where 1 <= index <= getDomainSize(), return VoidElementVoid otherwise
    DomainElement *getThElement(std::size_t aElementThIndex) override;
    
    /// Shrink domain to specified bounds <"aLowerBound", "aUpperBound">
    void shrinkOnBounds(DomainElement *aLowerBound, DomainElement *aUpperBound) override;
    
    /// Intersect the elements in this container with the elements in "aDomainElementContainer"
    void intersectWithDomainElementContainer(DomainElementContainer *aDomainElementContainer) override;
    
    /// Intersect the elements in this container with the elements in "aDomainElementArray"
    void intersectWithArray(DomainElementArray *aDomainElementArray) override;
    
    /// Subtract "aDomainElement" from the set of elements in the domain
    void subtractElement(DomainElement *aDomainElement) override;
    
    /// Subtract "aDomainElement" from the set of elements in the domain
    void subtractDomainElementContainer(DomainElementContainer *aDomainElementContainer) override;
    
    /// Subtract "aDomainElement" from the set of elements in the domain
    void subtractArray(DomainElementArray *aDomainElementArray) override;
    
    /// Shrink domain to <"aDomainElement", upperBound> if aDomainElement > lowerBound
    void shrinkOnMinBound(DomainElement *aDomainElement) override;
    
    /// Shrink domain to <lowerBound, "aDomainElement"> if aDomainElement < upperBound
    void shrinkOnMaxBound(DomainElement *aDomainElement) override;
    
    /// Updates internal state to "aDomainSnapshot"
    void reinstateInternalDomainSnapshot(const DomainState* aDomainSnapshot) override;
    
  private:
    /// It can be bitmap or list or other
    std::unique_ptr<DomainConcreteStorage> pConcreteStorage;
    
    /// Get maximum size to be used for DomainConcreteStorageBitmap
    inline std::size_t getDomainConcreteStorageBitmapMaxAllocSize() const
    {
      return MAX_CONCRETE_STORAGE_BITMAP_SIZE;
    }
    
    /// Get strict maximum size to be used for DomainConcreteStorageBitmap
    /// This is used with some data structures such as set which is not always
    /// convinient to switch to bitmap if the number of elements to set into
    /// the bitmap is greater than a "strict" size
    inline std::size_t getDomainConcreteStorageBitmapMaxAllocSizeStrict() const
    {
      return MAX_CONCRETE_STORAGE_BITMAP_SIZE_STRICT;
    }
    
    /// Get the current storage type
    inline DomainConcreteStorageType getStorageType() const
    {
      assert(pConcreteStorage);
      return pConcreteStorage->getConcreteStorageType();
    }
    
    /// Get current domain size
    inline std::size_t size() const
    {
      assert(pConcreteStorage);
      return pConcreteStorage->getSize();
    }
    
    /// Get domain's lower bound
    DomainElement *upperBound();
    
    /// Get domain's upper bound
    DomainElement *lowerBound();
    
    /// Get the size of the domain ["aLowerBound", "aUpperBound"]
    UINT_64 getDomainSizeFromBounds(INT_64 aLowerBound, INT_64 aUpperBound) const;
    
    /// Switch current representation to bitmap
    void switchCurrentStorageToStorageBitmap();
    
    /// Switch representation bounds to bitmap
    void switchStorageBoundsToStorageBitmap();
    
    /// Switch representation set to bitmap
    void switchStorageSetToStorageBitmap();

    /// Empty domain
    void emptyDomain();

    /// Subtract "aContainer" from container
    /// if container if of type DOMAIN_CONCRETE_STORAGE_SET
    void subtractOnStorageSet(DomainElementContainer *aContainer);
    
    /// Subtract ["aLowerBound", "aUpperBound"] from container
    /// if container if of type DOMAIN_CONCRETE_STORAGE_BOUNDS
    void subtractOnStorageBounds(DomainElement *aLowerBound, DomainElement* aUpperBound);
    
    /// Subtract "aDomainElementArray" from container
    /// if container if of type DOMAIN_CONCRETE_STORAGE_BOUNDS
    void subtractOnStorageBounds(DomainElementArray *aDomainElementArray);
  };
  
} // end namespace Core
