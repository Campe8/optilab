//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 10/06/2016
//

#pragma once

// Export definitions
#include "DomainExportDefs.hpp"

// Domain elements
#include "DomainElement.hpp"
// Events as meta-information
#include "DomainEvent.hpp"
#include "DomainEventManager.hpp"
// DomainContainer as subjects
#include "DomainElementContainer.hpp"

#include <vector>
#include <cassert>
#include <memory>

namespace Core {
  
  /**
   * DomainDecorator is a meta representation of the domain
   * of a variable.
   * It contains high level information about the domain.
   */
  
  class DomainDecorator {
  public:
    DomainDecorator(DomainElementContainer *aContainer);
    
    DomainDecorator(const DomainDecorator& aOther);
    
    DomainDecorator(DomainDecorator&& aOther);
    
    virtual ~DomainDecorator();
    
    DomainDecorator& operator= (const DomainDecorator& aOther);
    
    DomainDecorator& operator= (DomainDecorator&& aOther);
    
    inline DomainEvent *getEvent()
    {
      return pEvent;
    }
    
    /// Update this decorator with the new values
    /// of domain size and lower and upper bound.
    /// This will trigger a new domain event.
    /// @note "aDomainSize" can be either bigger or
    /// smaller than the current internal size
    void update(std::size_t aDomainSize,
                DomainElement *aNewLowerBound,
                DomainElement *aNewUpperBound);
    
    inline DomainElement *getLowerBound()
    {
      return pLowerBound;
    }
    
    inline DomainElement *getUpperBound()
    {
      return pUpperBound;
    }
    
    inline std::size_t getDomainSize() const
    {
      return pSize;
    }
    
  private:
    /// Current event
    DomainEvent *pEvent;
    
    /// Event manager
    std::unique_ptr<DomainEventManager> pEventManager;
    
    /// Domain's lower bound
    DomainElement *pLowerBound;
    
    /// Domain's upper bound
    DomainElement *pUpperBound;
    
    /// Domain's size
    std::size_t pSize;
    
    /// Resets internal status as if this object
    /// was just instantiated and calls update
    void resetInternalStatus(std::size_t aDomainSize,
                             DomainElement *aNewLowerBound,
                             DomainElement *aNewUpperBound);
  };
  
} // end namespace Core
