//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 10/16/2016
//

#pragma once

// Export definitions
#include "DomainExportDefs.hpp"

// Base class
#include "DomainConcreteStorage.hpp"

// Boost
#include "boost/dynamic_bitset.hpp"
#include "boost/optional.hpp"

#include <cassert>
#include <limits>
#include <unordered_map>

namespace Core {
  
  /**
   * DomainConcreteStorageBitmap extends DomainConcreteStorage
   * and it is used to represent a set a contiguous elements.
   *
   * @note This class represents a contiguous set of DomainElementInt64 elements.
   */
  
  class DomainConcreteStorageBitmap : public DomainConcreteStorage {
  public:
    /// Creates a new bitmap storage for all the elements in {"aLowerBound", ..., "aUpperBound"}
    DomainConcreteStorageBitmap(DomainElementInt64 *aLowerBound,
                                DomainElementInt64 *aUpperBound,
                                DomainElementManager *aDomainElementManager);
    
    virtual ~DomainConcreteStorageBitmap();
    
    inline std::size_t getSize() const override
    {
      return pBitset.count();
    }
    
    DomainElement *getLowerBound() const override;
    
    DomainElement *getUpperBound() const override;
    
    /// Given lowerBound <= "aDomainElement" <= upperBound, return successor of "aDomainElement".
    /// If "aDomainElement" == upperBound, return upperBound.
    /// Throws if "aDomainElement" is DomainElementVoid.
    DomainElement *getNextElement(DomainElement *aDomainElement) override;
    
    /// Given lowerBound <= "aDomainElement" <= upperBound, return predecessor of "aDomainElement".
    /// If "aDomainElement" == lowerBound, return lowerBound.
    /// Throws if "aDomainElement" is DomainElementVoid.
    DomainElement *getPrevElement(DomainElement *aDomainElement) override;
    
    /// Returns the "aElementIndex" - 1 ^th DomainElement, where
    /// 0 <= "aElementIndex" < domain size.
    DomainElement *getElementByIndex(std::size_t aElementIndex) override;
    
    /// Shrink domain to specified bounds <"aLowerBound", "aUpperBound">
    void shrink(DomainElement *aLowerBound, DomainElement *aUpperBound) override;
    
    /// Subtract "aDomainElement" from the set of elements in the domain
    void subtract(DomainElement *aDomainElement) override;
    
    /// Shrink domain to <"aDomainElement", upperBound> if aDomainElement > lowerBound
    void inMin(DomainElement *aDomainElement) override;
    
    /// Shrink domain to <lowerBound, "aDomainElement"> if aDomainElement < upperBound
    void inMax(DomainElement *aDomainElement) override;
    
    /// Return true if the domain contains "aDomainElement", false otherwise
    bool contains(DomainElement *aDomainElement) override;
    
  private:
    /// Get the offset index for the bitset.
    /// If the index is < 0, returns an empty object.
    /// If the index is >= bitset.size() return index.
    boost::optional<std::size_t> getOffsetValue(INT_64 aValue) const;
    
    inline INT_64 getOriginalValueFromOffsetValue(std::size_t aOffsetValue)
    {
      return static_cast<INT_64>(aOffsetValue - static_cast<INT_64>(pBitsetOffset));
    }
    
    /// Empty domain, if aLowerBound and aUpperBound are given, update current bounds
    void emptyDomain();
    
    /// Check if key is present in the map, if not store it
    void checkAndStoreDomainElement(DomainElement *aDomainElement);
    
    /// Update internal lower bound considering the range "aLowerBound", "aUpperBound" in the bitset
    /// i.e., "aLowerBound" and "aUpperBound" must be indexes of the bitset
    void updateLowerBound(std::size_t aLowerBound, std::size_t aUpperBound);
    
    /// Update internal upper bound considering the range "aLowerBound", "aUpperBound" in the bitset
    /// i.e., "aLowerBound" and "aUpperBound" must be indexes of the bitset
    void updateUpperBound(std::size_t aLowerBound, std::size_t aUpperBound);
    
    /// Offset for bitset map to be used in "getOffsetValue"
    INT_64 pBitsetOffset;
    
    /// Current lower bound
    std::size_t pLowerBound;
    
    /// Current upper bound
    std::size_t pUpperBound;
    
    /// Bitset data structure for contiguous elements
    boost::dynamic_bitset<> pBitset;
    
    /// Map between bitset indexes and DomainElementKeys
    std::unordered_map<std::size_t, DomainElementInt64*> pBitmapToDomainElementInt64Map;
  };
  
} // end namespace Core
