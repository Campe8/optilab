//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 10/06/2016
//

#pragma once

// Export definitions
#include "DomainExportDefs.hpp"

// Element manager
#include "DomainElementManager.hpp"
// Domain elements
#include "DomainElement.hpp"
// Domain keys
#include "DomainElementKey.hpp"


namespace Core {
  
  enum DomainConcreteStorageType : int {
      DOMAIN_CONCRETE_STORAGE_BITMAP = 0  // Contiguous set of elements
    , DOMAIN_CONCRETE_STORAGE_BOUNDS      // Set as <min, max> bounds
    , DOMAIN_CONCRETE_STORAGE_SET         // Standard set of elements
  };
  
  /**
   * DomainConcreteStorage is the class which holds the
   * actual data structure used to represent domain and
   * operations on domains.
   * It is generally a map between the actual data structure
   * and the elements of the domain.
   */
  
  class DomainConcreteStorage {
  public:
    virtual ~DomainConcreteStorage();
    
    inline DomainConcreteStorageType getConcreteStorageType() const
    {
      return pStorageType;
    }
    
    /// Get domain's size
    virtual std::size_t getSize() const = 0;
    
    /// Get domain's lower bound, if empty returns undefined element
    virtual DomainElement *getLowerBound() const = 0;
    
    /// Get domain's upper bound, if empty returns undefined element
    virtual DomainElement *getUpperBound() const = 0;
    
    /// Given lowerBound <= "aDomainElement" <= upperBound, return successor of "aDomainElement".
    /// If "aDomainElement" == upperBound, return upperBound.
    /// Throws if "aDomainElement" is DomainElementVoid.
    virtual DomainElement *getNextElement(DomainElement *aDomainElement) = 0;
    
    /// Given lowerBound <= "aDomainElement" <= upperBound, return predecessor of "aDomainElement".
    /// If "aDomainElement" == lowerBound, return lowerBound.
    /// Throws if "aDomainElement" is DomainElementVoid.
    virtual DomainElement *getPrevElement(DomainElement *aDomainElement) = 0;
    
    /// Returns the "aElementIndex" - 1 ^th DomainElement, where
    /// 0 <= "aElementIndex" < domain size.
    virtual DomainElement *getElementByIndex(std::size_t aElementIndex) = 0;
    
    /// Shrink domain to specified bounds <"aLowerBound", "aUpperBound">
    virtual void shrink(DomainElement *aLowerBound, DomainElement *aUpperBound) = 0;
    
    /// Subtract "aDomainElement" from the set of elements in the domain
    virtual void subtract(DomainElement *aDomainElement) = 0;
    
    /// Shrink domain to <"aDomainElement", upperBound> if aDomainElement > lowerBound
    virtual void inMin(DomainElement *aDomainElement) = 0;
    
    /// Shrink domain to <lowerBound, "aDomainElement"> if aDomainElement < upperBound
    virtual void inMax(DomainElement *aDomainElement) = 0;
    
    /// Return true if the domain contains "aDomainElement", false otherwise
    virtual bool contains(DomainElement *aDomainElement) = 0;
    
  protected:
    DomainConcreteStorage(DomainConcreteStorageType aStorageType, DomainElementManager *aDomainElementManager);
    
    DomainElementManager& getDomainElementManager();
    
  private:
    // Storage type
    DomainConcreteStorageType pStorageType;
    
    // DomainElement manager
    DomainElementManager *pDomainElementManager;
  };
  
} // end namespace Core
