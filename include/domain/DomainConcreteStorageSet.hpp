//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 10/18/2016
//

#pragma once

// Export definitions
#include "DomainExportDefs.hpp"

// Base class
#include "DomainConcreteStorage.hpp"

// Boost
#include "boost/dynamic_bitset.hpp"
#include "boost/optional.hpp"

#include <cassert>
#include <unordered_map>
#include <unordered_set>

namespace Core {
  
  /**
   * DomainConcreteStorageBounds extends DomainConcreteStorage
   * and it is used to represent a large set of elements by
   * storing its bounds.
   */
  
  class DomainConcreteStorageSet : public DomainConcreteStorage {
  public:
    /// Creates a new bitmap storage for all the elements in {"aLowerBound", ..., "aUpperBound"}
    DomainConcreteStorageSet(const std::unordered_set<DomainElement *>& aSetOfElements,
                             DomainElementManager *aDomainElementManager);
    
    virtual ~DomainConcreteStorageSet();
    
    inline std::size_t getSize() const override
    {
      return pBitset.count();
    }
    
    DomainElement *getLowerBound() const override;
    
    DomainElement *getUpperBound() const override;
    
    /// Given lowerBound <= "aDomainElement" <= upperBound, return successor of "aDomainElement".
    /// If "aDomainElement" == upperBound, return upperBound.
    /// Throws if "aDomainElement" is DomainElementVoid.
    DomainElement *getNextElement(DomainElement *aDomainElement) override;
    
    /// Given lowerBound <= "aDomainElement" <= upperBound, return predecessor of "aDomainElement".
    /// If "aDomainElement" == lowerBound, return lowerBound.
    /// Throws if "aDomainElement" is DomainElementVoid.
    DomainElement *getPrevElement(DomainElement *aDomainElement) override;
    
    /// Returns the "aElementIndex" - 1 ^th DomainElement, where
    /// 0 <= "aElementIndex" < domain size.
    /// Note: return an element without any order.
    DomainElement *getElementByIndex(std::size_t aElementIndex) override;
    
    /// Shrink domain to specified bounds <"aLowerBound", "aUpperBound">
    void shrink(DomainElement *aLowerBound, DomainElement *aUpperBound) override;
    
    /// Subtract "aDomainElement" from the set of elements in the domain
    void subtract(DomainElement *aDomainElement) override;
    
    /// Shrink domain to <"aDomainElement", upperBound> if aDomainElement > lowerBound
    void inMin(DomainElement *aDomainElement) override;
    
    /// Shrink domain to <lowerBound, "aDomainElement"> if aDomainElement < upperBound
    void inMax(DomainElement *aDomainElement) override;
    
    /// Return true if the domain contains "aDomainElement", false otherwise
    bool contains(DomainElement *aDomainElement) override;
    
  private:
    /// Map between bitmap positions and domain elements
    std::unordered_map<std::size_t, DomainElement *> pBitsetToDomainElementMap;
    
    /// Map between key values and bitmap positions
    std::unordered_map<DomainElementKey, std::size_t, DomainElementKeyHasher> pDomainElementKeyToBitmapMap;
    
    /// Bitset data structure representing the set of elements
    boost::dynamic_bitset<> pBitset;
    
    /// Empty domain, if aLowerBound and aUpperBound are given, update current bounds
    void emptyDomain();
    
    /// Check if key is present in the map, if not store it
    void checkAndStoreDomainElement(DomainElement *aDomainElement);
    
    /// Update internal lower and upper bounds
    void updateBounds();
    
    /// Offset for bitset map
    std::size_t pBitsetOffset;
    
    /// Current lower bound
    std::size_t pLowerBound;
    
    /// Current upper bound
    std::size_t pUpperBound;
  };
  
} // end namespace Core
