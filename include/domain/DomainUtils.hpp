//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 12/03/2016
//
// Utility class for Domain
//

#pragma once

// Export definitions
#include "DomainExportDefs.hpp"

#include "DomainTypeDefs.hpp"
#include "DomainDefs.hpp"

#include <cassert>

namespace Core { namespace DomainUtils {

  /// Returns ceil(x/y) where x >= 0 and y >= 0
  inline INT_64 ceilDivPP(INT_64 x, INT_64 y)
  {
    assert((x >= 0) && (y >= 0));
    return ((x % y) == 0) ? x/y : (x/y + 1);
  }
  
  /// Returns ceil(x/y) where x >= 0 and y >= 0
  inline INT_64 floorDivPP(INT_64 x, INT_64 y)
  {
    assert((x >= 0) && (y >= 0));
    return x / y;
  }
  
  /// Returns ceil(x/y) where x >= 0
  inline INT_64 ceilDivPX(INT_64 x, INT_64 y)
  {
    assert(x >= 0);
    return (y >= 0) ? ceilDivPP(x,y) : -floorDivPP(x,-y);
  }
  
  /// Returns ceil(x/y) where x >= 0
  inline INT_64 floorDivPX(INT_64 x, INT_64 y)
  {
    assert(x >= 0);
    return (y >= 0) ? floorDivPP(x,y) : -ceilDivPP(x,-y);
  }
  
  /// Returns ceil(x/y) where y >= 0
  inline INT_64 ceilDivXP(INT_64 x, INT_64 y)
  {
    assert(y >= 0);
    return (x >= 0) ? ceilDivPP(x,y) : -floorDivPP(-x,y);
  }
  
  /// Returns ceil(x/y) where y >= 0
  inline INT_64 floorDivXP(INT_64 x, INT_64 y)
  {
    assert(y >= 0);
    return (x >= 0) ? floorDivPP(x,y) : -ceilDivPP(-x,y);
  }
  
  /// Returns ceil(x/y)
  inline INT_64 ceilDivXX(INT_64 x, INT_64 y)
  {
    return (x >= 0) ? ceilDivPX(x,y) : -floorDivPX(-x,y);
  }
  
  /// Returns ceil(x/y)
  inline INT_64 floorDivXX(INT_64 x, INT_64 y)
  {
    return (x >= 0) ? floorDivPX(x,y) : -ceilDivPX(-x,y);
  }
  
}} // end namespace Core::DomainUtils
