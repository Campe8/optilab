//
// Copyright OptiLab 2016-2017. All rights reserved.
//
// Created by Federico Campeotto on 10/06/2016
//

#pragma once

// Export definitions
#include "DomainExportDefs.hpp"

// Element manager
#include "DomainElementManager.hpp"

#include "DArrayMatrix.hpp"

// Memento semantics
#include "DomainState.hpp"

#include <vector>

// Forward decl.
namespace Core {
  // Domain elements
  class DomainElement;
  
  // Decorator as observer
  class DomainDecorator;
  
  // Iterator
  class DomainIterator;
}

namespace Core {
  
  class DomainElementContainer {
  public:
    virtual ~DomainElementContainer();
    
    inline DomainElementContainerClass getContainerClass() const
    {
      return pContainerClass;
    }
    
    void registerDecorator(DomainDecorator *aDecorator);
    
    /// Shrink domain to specified bounds <"aLowerBound", "aUpperBound">
    void shrink(DomainElement *aLowerBound, DomainElement *aUpperBound);
    
    /// Subtract "aDomainElement" from the set of elements in the domain
    void subtract(DomainElement *aDomainElement);
    
    /// Subtract this domain with the domain "aDomain"
    void subtract(DomainElementContainer *aDomainContainer);
    
    /// Subtract this domain with the domain elements in "aDomainElementArray"
    void subtract(DomainElementArray *aDomainElementArray);
    
    /// Shrink domain to <"aDomainElement", upperBound> if aDomainElement > lowerBound
    void inMin(DomainElement *aDomainElement);
    
    /// Shrink domain to <lowerBound, "aDomainElement"> if aDomainElement < upperBound
    void inMax(DomainElement *aDomainElement);
    
    /// Intersect this domain with the domain "aDomain"
    void intersect(DomainElementContainer *aDomainContainer);
    
    /// Intersect the elements in this container with the elements in "aDomainElementArray"
    void intersect(DomainElementArray *aDomainElementArray);
    
    /// Return true if the domain contains "aDomainElement", false otherwise
    virtual bool contains(DomainElement *aDomainElement) = 0;
    
    /// Returns a snapshot of the current internal sate
    virtual std::unique_ptr<DomainState> getDomainSnapshot() = 0;
    
    /// Updates internal state to "aDomainSnapshot"
    void reinstateDomainSnapshot(const DomainState* aDomainSnapshot);
    
  protected:
    friend class DomainIterator;
    
    DomainElementContainer(DomainElementContainerClass aClass);
    
    DomainElementContainer(const DomainElementContainer& aOther);
    
    DomainElementContainer(DomainElementContainer&& aOther);
    
    DomainElementContainer& operator= (const DomainElementContainer& aOther);
    
    DomainElementContainer& operator= (DomainElementContainer&& aOther);
    
    inline DomainElementManager *getDomainElementManager()
    {
      return pDomainElementManager;
    }
    
    /// Updates internal state to "aDomainSnapshot"
    virtual void reinstateInternalDomainSnapshot(const DomainState* aDomainSnapshot) = 0;
    
    /// Returns domain size
    virtual std::size_t getDomainSize() const = 0;
    
    /// Returns domain's lower bound, return nullptr on empty domain
    virtual DomainElement *getLowerBound() = 0;
    
    /// Returns domain's upper bound, return nullptr on empty domain
    virtual DomainElement *getUpperBound() = 0;
    
    /// Returns element greater than given element, return VoidElementVoid otherwise
    virtual DomainElement *getElementGreaterThan(DomainElement *aDomainElement) = 0;
    
    /// Returns element less than given element, return VoidElementVoid otherwise
    virtual DomainElement *getElementLessThan(DomainElement *aDomainElement) = 0;
    
    /// Returns "index"^th element where 1 <= index <= getDomainSize(), return VoidElementVoid otherwise
    virtual DomainElement *getThElement(std::size_t aElementThIndex) = 0;
    
    /// Shrinks domain to specified bounds <"aLowerBound", "aUpperBound">
    virtual void shrinkOnBounds(DomainElement *aLowerBound, DomainElement *aUpperBound) = 0;
    
    /// Intersects the elements in this container with the elements in "aDomainElementContainer"
    virtual void intersectWithDomainElementContainer(DomainElementContainer *aDomainElementContainer) = 0;
    
    /// Intersects the elements in this container with the elements in "aDomainElementArray"
    virtual void intersectWithArray(DomainElementArray *aDomainElementArray) = 0;
    
    /// Subtracts "aDomainElement" from the set of elements in the domain
    virtual void subtractElement(DomainElement *aDomainElement) = 0;
    
    /// Subtracts "aDomainElement" from the set of elements in the domain
    virtual void subtractDomainElementContainer(DomainElementContainer *aDomainContainer) = 0;
    
    /// Subtracts "aDomainElement" from the set of elements in the domain
    virtual void subtractArray(DomainElementArray *aDomainElementArray) = 0;

    /// Shrinks domain to <"aDomainElement", upperBound> if aDomainElement > lowerBound
    virtual void shrinkOnMinBound(DomainElement *aDomainElement) = 0;
    
    /// Shrinks domain to <lowerBound, "aDomainElement"> if aDomainElement < upperBound
    virtual void shrinkOnMaxBound(DomainElement *aDomainElement) = 0;
    
    /// Returns a pair <min, max> corresponding to the minimum and maximum elements from "aArray"
    std::pair<DomainElement *, DomainElement*> getMinMaxFromArray(DomainElementArray *aArray);
    
  private:
    DomainElementContainerClass pContainerClass;
    
    /// Notify domain decorators about changes after operations on domains
    void notifyDomainDecorators(DomainElement *aLowerBound, DomainElement *aUpperBound);
    
    /// Domain element manager
    DomainElementManager *pDomainElementManager;
    
    /// Decorators to notify upon domain changes
    std::vector<DomainDecorator*> pDecoratorRegister;
  };
  
} // end namespace Core
