//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 10/18/2016
//

#pragma once

// Export definitions
#include "DomainExportDefs.hpp"

// Base class
#include "DomainConcreteStorage.hpp"

// Boost optional
#include "boost/optional.hpp"

#include <cassert>
#include <unordered_map>

namespace Core {
  
  /**
   * DomainConcreteStorageBounds extends DomainConcreteStorage
   * and it is used to represent a large set of elements by
   * storing its bounds.
   */
  
  class DomainConcreteStorageBounds : public DomainConcreteStorage {
  public:
    /// Creates a new bitmap storage for all the elements in {"aLowerBound", ..., "aUpperBound"}
    DomainConcreteStorageBounds(DomainElement *aLowerBound,
                                DomainElement *aUpperBound,
                                DomainElementManager *aDomainElementManager);
    
    virtual ~DomainConcreteStorageBounds();
    
    inline std::size_t getSize() const override
    {
      return pDomainSize;
    }
    
    inline DomainElement *getLowerBound() const override
    {
      return pLowerBound;
    }
    
    inline DomainElement *getUpperBound() const override
    {
      return pUpperBound;
    }
    
    /// Given lowerBound <= "aDomainElement" <= upperBound, return successor of "aDomainElement".
    /// If "aDomainElement" == upperBound, return upperBound.
    /// Throws if "aDomainElement" is DomainElementVoid.
    DomainElement *getNextElement(DomainElement *aDomainElement) override;
    
    /// Given lowerBound <= "aDomainElement" <= upperBound, return predecessor of "aDomainElement".
    /// If "aDomainElement" == lowerBound, return lowerBound.
    /// Throws if "aDomainElement" is DomainElementVoid.
    DomainElement *getPrevElement(DomainElement *aDomainElement) override;
    
    /// Returns the "aElementIndex" - 1 ^th DomainElement, where
    /// 0 <= "aElementIndex" < domain size.
    DomainElement *getElementByIndex(std::size_t aElementIndex) override;
    
    /// Shrink domain to specified bounds <"aLowerBound", "aUpperBound">
    void shrink(DomainElement *aLowerBound, DomainElement *aUpperBound) override;
    
    /// Subtract "aDomainElement" from the set of elements in the domain
    void subtract(DomainElement *aDomainElement) override;
    
    /// Shrink domain to <"aDomainElement", upperBound> if aDomainElement > lowerBound
    void inMin(DomainElement *aDomainElement) override;
    
    /// Shrink domain to <lowerBound, "aDomainElement"> if aDomainElement < upperBound
    void inMax(DomainElement *aDomainElement) override;
    
    /// Return true if the domain contains "aDomainElement", false otherwise
    bool contains(DomainElement *aDomainElement) override;
    
  private:
    /// Lower bound
    DomainElement *pLowerBound;
    
    /// Upper bound
    DomainElement *pUpperBound;
    
    std::size_t pDomainSize;
    
    /// Set current domain size w.r.t. bound keys
    void setSize();
    
    /// Empty domain
    void emptyDomain();
  };
  
} // end namespace Core
