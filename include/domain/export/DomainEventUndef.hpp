//
// Copyright OptiLab 2016-2017. All rights reserved.
//
// Created by Federico Campeotto on 10/13/2016
//

#pragma once

// Export definitions
#include "DomainExportDefs.hpp"

// Include base class
#include "DomainEvent.hpp"

namespace Core {
  
  class CORE_DOMAIN_EXPORT_CLASS DomainEventUndef : public DomainEvent {
  public:
    DomainEventUndef();
    
    virtual ~DomainEventUndef();
    
    static bool isa(DomainEvent *aEvent);
    
    static DomainEventUndef* cast(DomainEvent *aEvent);
    
    bool overlaps(const DomainEvent *aDomainEvent) const override;
  };
  
} // end namespace Core
