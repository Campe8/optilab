//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 10/22/2016
//

#pragma once

// Domain Class
#include "Domain.hpp"

// Domain container used to iterate over the domain's elements
#include "DomainElementContainer.hpp"

// Domain Element manager
#include "DomainElementManager.hpp"

// Forward declarations
namespace Base { namespace Tools {
  class RandomGenerator;
}}

namespace Core {
  
  typedef DomainIterator iterator;
  typedef ptrdiff_t difference_type;
  typedef size_t size_type;
  typedef DomainElement value_type;
  typedef DomainElement* pointer;
  typedef DomainElement& reference;
  
  class CORE_DOMAIN_EXPORT_CLASS DomainIterator {
  public:
    DomainIterator(DomainElementContainer *aDomainContainer);
    
    /// Shallow copy
    DomainIterator(const iterator& aDomainIterator);
    
    /// Move constructor
    DomainIterator(iterator&& aDomainIterator);
    
    virtual ~DomainIterator();
    
    /// Move assignment operator
    iterator& operator= (DomainIterator&& aDomainIterator);
    
    /// Shallow copy
    iterator& operator= (const iterator& aDomainIterator);
    
    bool operator==(const iterator& aDomainIterator);
    
    inline bool operator!=(const iterator& aDomainIterator)
    {
      return !(*this == aDomainIterator);
    }
    
    /// Returns an iterator pointing to the first element in the domain, i.e., its minimum element
    iterator begin();
    
    /// Returns a reverse iterator pointing to the last element in the domain
    /// i.e., its reverse beginning, the maximum element
    iterator rbegin();
    
    /// Returns an iterator to the past-the-end element in the domain
    iterator end();
    
    /// Returns an iterator pointing to a random element in the domain.
    /// If "aRandGen" is not nullptr, it will use an internal random generator
    /// with uniform distribution
    iterator randBegin(Base::Tools::RandomGenerator* aRandGen=nullptr);
    
    /// Pre-increment operator, returns an iterator pointing to the next greater element
    iterator & operator++();
    
    /// Post-increment operator, returns an iterator pointing to the same element
    /// and increase internal pointer as side-effect
    iterator operator++ (int);
    
    /// Returns the reference to the DomainElement pointed by this iterator
    reference operator*();
    
    /// Return the pointer to the DomainElement pointed by this iterator
    pointer operator->() const;
    
  private:
    /// Flag indicating whethr the iterator is a reverse iterator or not
    bool pReverseIter;
    
    /// Domain container to iterate over
    DomainElementContainer *pDomainContainer;
    
    /// Current DomainElement pointed to
    DomainElement *pDomainElement;
    
    /// Domain element manager
    DomainElementManager *pDomainElementManager;
  };
  
} // end namespace Core
