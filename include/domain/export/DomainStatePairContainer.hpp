//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 2/17/2017
//

#pragma once

// Base class
#include "DomainState.hpp"

// Storage
#include "DomainConcreteStorage.hpp"

// Element manager for copying domain elements
#include "DomainElementManager.hpp"

#include <cassert>

namespace Core {
  
  class CORE_DOMAIN_EXPORT_CLASS DomainStatePairContainer : public DomainState {
  public:
    DomainStatePairContainer();

    virtual ~DomainStatePairContainer();

    static bool isa(const DomainState* aDomainState)
    {
      assert(aDomainState);
      return aDomainState->getDomainStateType() == DomainStateType::DOM_STATE_PAIR_CONT;
    }

    static DomainStatePairContainer* cast(DomainState* aDomainState)
    {
      if (!isa(aDomainState))
      {
        return nullptr;
      }
      return static_cast<DomainStatePairContainer*>(aDomainState);
    }
    
    static const DomainStatePairContainer* cast(const DomainState* aDomainState)
    {
      if (!isa(aDomainState))
      {
        return nullptr;
      }
      return static_cast<const DomainStatePairContainer*>(aDomainState);
    }
    
    inline bool validState() const
    {
      return pDomainElementPair.first != nullptr && pDomainElementPair.second != nullptr;
    }

    /// Sets snapshot of "aStorage" as internal state
    inline void setInternalState(const std::pair<DomainElement*, DomainElement*>& aPair)
    {
      assert(aPair.first);
      assert(aPair.second);
      pDomainElementPair.first = DomainElementManager::getInstance().clone(aPair.first);
      pDomainElementPair.second = DomainElementManager::getInstance().clone(aPair.second);
    }

    inline std::pair<DomainElement*, DomainElement*> releaseState() const
    {
      std::pair<DomainElement*, DomainElement*> stateCopy = pDomainElementPair;
      resetInternalState();
      return stateCopy;
    }
    
    /// Returns a copy of the internal state
    inline std::pair<DomainElement*, DomainElement*> getInternalStateCopy() const
    {
      return pDomainElementPair;
    }

  private:
    /// Pair of elements in this container
    mutable std::pair<DomainElement*, DomainElement*> pDomainElementPair;
    
    /// Resets internal state
    inline void resetInternalState() const
    {
      pDomainElementPair.first = nullptr;
      pDomainElementPair.second = nullptr;
    }
  };
  
} // end namespace Core
