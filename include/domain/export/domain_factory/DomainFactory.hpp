//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 1/27/2017
//

#pragma once

#include "DomainInteger.hpp"
#include "DomainBoolean.hpp"

#include <memory>

// Forward declarations
namespace Core {
  class DomainFactory;
  
  typedef DomainFactory* DomainFactoryPtr;
  typedef std::unique_ptr<DomainFactory> DomainFactoryUPtr;
  typedef std::shared_ptr<DomainFactory> DomainFactorySPtr;
}// end namespace Core

namespace Core {

  class DomainFactory {
  public:
    DomainFactory();

    virtual ~DomainFactory();
    
    /// Returns a shared pointer that owns the given pointer
    DomainSPtr getSharedFromRawDomainPointer(DomainPtr aDomainPtr);
    
    /// Returns a singleton domain {"aDomainElement"} with a type that can
    /// represent the value "aDomainElement".
    /// "aDomainClass" must be specified, this method tries to force the returned domain
    /// to have the given type.
    /// For example, if "aDomainElement" has a DET_INT_64 type, "aDomainClass" can be either
    /// DOM_INT64 or DOM_BOOL.
    /// If "aDomainElement" has a DET_REAL type, trying to force "aDomainClass" to be DOM_BOOL
    /// won't have any effect and this method will return nullptr.
    /// @note throws if "aDomainElement" has void element type
    DomainPtr domain(DomainElement* aDomainElement, DomainClass aDomainClass);
    
    // Returns a domain [aDomainElementLowerBound, aDomainElementUpperBound] with a type that can
    /// represent the values in the given range.
    /// "aDomainClass" must specified, this method tries to force the returned domain
    /// to have the given type.
    /// For example, if "aDomainElementLowerBound" has a DET_INT_64 type, "aDomainClass" can be either
    /// DOM_INT64 or DOM_BOOL.
    /// If "aDomainElementLowerBound" has a DET_REAL type, trying to force "aDomainClass" to be DOM_BOOL
    /// this method will return nullptr.
    /// @note throws if "aDomainElementLowerBound" and "aDomainElementUpperBound" have different type.
    /// @note throws if "aDomainElement" has void element type
    DomainPtr domain(DomainElement* aDomainElementLowerBound, DomainElement* aDomainElementUpperBound,
                     DomainClass aDomainClass);
    
    
    DomainIntegerPtr domainInteger(INT_64 aDomainElement);
    DomainIntegerPtr domainInteger(INT_64 aLowerBound, INT_64 aUpperBound);
    DomainIntegerPtr domainInteger(const std::unordered_set<INT_64>& aSetOfElements);
    DomainIntegerPtr domainInteger(const std::unordered_set<INT_64>&& aSetOfElements);
    
    DomainIntegerPtr domainInteger(DomainElementInt64* aLowerBound, DomainElementInt64* aUpperBound);
    DomainIntegerPtr domainInteger(const std::unordered_set<DomainElementInt64 *>& aSetOfElements);
    DomainIntegerPtr domainInteger(std::unordered_set<DomainElementInt64 *>&& aSetOfElements);

    DomainBooleanPtr domainBoolean();
    DomainBooleanPtr domainBoolean(bool aBoolVal);
    
  private:
    /// Returns true if aDomainClass can represent a domain element of type "aDomainElementType",
    /// false otherwise
    bool isValidTypeCoercion(DomainClass aDomainClass, DomainElementType aDomainElementType);
  };

}// end namespace Core
