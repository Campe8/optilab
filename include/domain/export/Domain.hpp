//
// Copyright OptiLab 2016-2017. All rights reserved.
//
// Created by Federico Campeotto on 10/22/2016
//

#pragma once

// Export definitions
#include "DomainExportDefs.hpp"

// Domain definitions
#include "DomainDefs.hpp"

// Domain container
#include "DomainElementContainer.hpp"
// Domain decorator
#include "DomainDecorator.hpp"

// Array
#include "DArrayMatrix.hpp"

// Constraint observers
#include "ConstraintObserver.hpp"

// Backtrack semantics
#include "BacktrackableObject.hpp"

#include <unordered_set>
#include <cassert>

// Forward declarations
namespace Core {
  // Events
  class DomainEvent;
  // Domain elements
  class DomainElement;
  // Domain iterator
  class DomainIterator;
  // Specular domain
  class DomainSpecular;
  
  class Domain;
  typedef Domain* DomainPtr;
  typedef std::unique_ptr<Domain> DomainUPtr;
  typedef std::shared_ptr<Domain> DomainSPtr;
  
  // Observers on this domain
  class Constraint;
}// end namespace Core

namespace Core {
  
  class CORE_DOMAIN_EXPORT_CLASS Domain : public Semantics::ConstraintSubject, public Search::BacktrackableObject {
  public:
    virtual ~Domain();
    
    inline DomainClass getDomainType() const
    {
      return pDomainClass;
    }
    
    DomainIterator* const getIterator();
    
    DomainEvent *getEvent();
    
    virtual std::size_t getSize() const;
    
    virtual bool contains(DomainElement *aElement);
    
    virtual DomainElement* lowerBound();
    
    virtual DomainElement* upperBound();
    
    virtual void shrink(DomainElement* aLowerBound, DomainElement* aUpperBound);
    
    virtual void subtract(DomainElement *aElement);
    
    virtual void shrinkOnLowerBound(DomainElement *aElement);
    
    virtual void shrinkOnUpperBound(DomainElement *aElement);
    
    /// Intersect this domain with the domain "aDomain"
    virtual void intersect(Domain *aDomain);
    
    /// Intersect this domain with the domain elements in "aDomainElementArray"
    virtual void intersect(DomainElementArray *aDomainElementArray);
    
    /// Subtract this domain with the domain "aDomain"
    virtual void subtract(Domain *aDomain);
    
    /// Subtract this domain with the domain elements in "aDomainElementArray"
    virtual void subtract(DomainElementArray *aDomainElementArray);
    
  protected:
    /// DomainSpecular as a friend of Domain
    friend class DomainSpecular;
    
    /// Iterator as a friend of Domain
    friend class DomainIterator;
    
    Domain(DomainClass aDomainClass);
    
    Domain(const Domain& aOther);
    
    Domain(Domain&& aOther);
    
    Domain& operator= (const Domain& aOther);
    
    Domain& operator= (Domain&& aOther);
    
    /// Cast this class to the given class
    inline void castToClass(DomainClass aDomainClass)
    {
      pDomainClass = aDomainClass;
    }
    
    /// Subject notification action override
    void triggerActionBasedOnEvent(Semantics::ConstraintObserver* aConstraintObserver,
                                   const Core::ConstraintObserverEvent* aConstraintEvent) override;
    
    /// Creates a snapshot of the object in the current state.
    /// @note snapshot is a MementoState object
    std::unique_ptr<Search::MementoState> snapshot() override;
    
    /// Reinstate a previous snapshot of the object.
    /// @note snapshot is a MementoState object
    void reinstateSnapshot(const Search::MementoState* aSnapshot) override;
    
    virtual DomainElementContainer* getDomainContainer() const = 0;
    
    virtual DomainDecorator* getDomainDecorator() const = 0;
    
  private:
    DomainClass pDomainClass;
    
    /// Iterator on domain
    std::unique_ptr<DomainIterator> pIterator;
    
    /// Temporary event used for observer notification
    DomainEvent* pTemporaryEvent;
    
    /// Notify observers, e.g., backtrack manager if any,
    /// upon a change in this domain
    void notifyChange();
  };
  
} // end namespace Core
