//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 2/16/2017
//

#pragma once

// Base class
#include "DomainState.hpp"

// Storage
#include "DomainConcreteStorage.hpp"

#include <cassert>

namespace Core {
  
  class CORE_DOMAIN_EXPORT_CLASS DomainStateInt64Container : public DomainState {
  public:
    DomainStateInt64Container();

    virtual ~DomainStateInt64Container();

    static bool isa(const DomainState* aDomainState)
    {
      assert(aDomainState);
      return aDomainState->getDomainStateType() == DomainStateType::DOM_STATE_INT64_CONT;
    }

    static DomainStateInt64Container* cast(DomainState* aDomainState)
    {
      if (!isa(aDomainState))
      {
        return nullptr;
      }
      return static_cast<DomainStateInt64Container*>(aDomainState);
    }
    
    static const DomainStateInt64Container* cast(const DomainState* aDomainState)
    {
      if (!isa(aDomainState))
      {
        return nullptr;
      }
      return static_cast<const DomainStateInt64Container*>(aDomainState);
    }
    
    inline bool validState() const
    {
      return pConcreteStorageInstance != nullptr;
    }

    /// Sets snapshot of "aStorage" as internal state
    inline void setInternalState(const DomainConcreteStorage* aStorage)
    {
      createState(aStorage);
    }
    
    /// Returns a copy of the internal state
    inline std::unique_ptr<DomainConcreteStorage> getInternalStateCopy() const
    {
      DomainConcreteStorage* storage = pConcreteStorageInstance;
      
      // Hold a copy of the state
      createState(pConcreteStorageInstance);
      
      return std::unique_ptr<DomainConcreteStorage>(storage);
    }
    
    /// Releases internal state
    inline DomainConcreteStorage* releaseState() const
    {
      DomainConcreteStorage* stateToReturn = pConcreteStorageInstance;
      
      // Reset state
      resetInternalState();
      
      return stateToReturn;
    }

  private:
    mutable DomainConcreteStorage* pConcreteStorageInstance;
    
    // Resets internal state
    inline void resetInternalState() const
    {
      pConcreteStorageInstance = nullptr;
    }
    
    /// Creates new internal state as a copy of "aStorage"
    void createState(const DomainConcreteStorage* aStorage) const;
  };
  
} // end namespace Core
