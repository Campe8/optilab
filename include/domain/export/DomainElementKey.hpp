//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 09/27/2016
//

#pragma once

// Export definitions
#include "DomainExportDefs.hpp"

// Domain type macros
#include "DomainTypeDefs.hpp"

#include <iostream>
#include <cassert>

#include <boost/functional/hash.hpp>

namespace Core {
  struct DomainElementKeyHasher;
}

namespace Core {
  
  class CORE_DOMAIN_EXPORT_CLASS DomainElementKey {
    
  public:
    explicit DomainElementKey(INT_64 aKeyVal, DomainElementType aDomainElementType);
    
    explicit DomainElementKey(REAL aKeyVal, DomainElementType aDomainElementType);
    
    DomainElementKey(const DomainElementKey& aOther);
    
    DomainElementKey(DomainElementKey&& aOther);
    
    DomainElementKey& operator=(const DomainElementKey& aOther);
    
    DomainElementKey& operator=(DomainElementKey&& aOther);
    
    virtual ~DomainElementKey();
    
    bool operator==(const DomainElementKey &aOther) const;

    inline DomainElementType getDomainElementType() const
    {
      return pDomainElementType;
    }
    
  private:
    friend struct DomainElementKeyHasher;
    
    /// Key value int_64
    INT_64 pKeyValInt64;
    
    /// Key value real
    REAL pKeyValReal;
    
    /// Domain element type
    DomainElementType pDomainElementType;
  };
  
  struct CORE_DOMAIN_EXPORT_STRUCT DomainElementKeyHasher
  {
    std::size_t operator()(const DomainElementKey& p) const
    {      
      using boost::hash_value;
      using boost::hash_combine;
      
      std::size_t seed = 0;
      switch(p.pDomainElementType)
      {
        case DomainElementType::DET_REAL:
        {
          hash_combine(seed, hash_value(p.pKeyValReal));
          break;
        }
        default:
          assert(p.pDomainElementType == DomainElementType::DET_INT_64);
          hash_combine(seed, hash_value(p.pKeyValInt64));
          break;
      }//switch
      
      return seed;
    }
  };
  
} // end namespace Core
