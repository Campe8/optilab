//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 09/27/2016
//

#pragma once

// Base class
#include "DomainElement.hpp"
// Definitions
#include "DomainDefs.hpp"

#include <unordered_map>
#include <memory>

// Forward declaration
namespace Core {
  class DomainElementKey;
}

namespace Core {
  
  class CORE_DOMAIN_EXPORT_CLASS DomainElementInt64 : public DomainElement {
  public:
    DomainElementInt64(INT_64 aVal);
    
    DomainElementInt64(const DomainElementInt64& aOther);
    
    DomainElementInt64(DomainElementInt64&& aOther);
    
    virtual ~DomainElementInt64();
    
    DomainElementInt64& operator=(const DomainElementInt64& aOther);
    
    DomainElementInt64& operator=(DomainElementInt64&& aOther);
    
    static bool isa(const DomainElement* aDomainElement);
    
    static DomainElementInt64* cast(DomainElement* aDomainElement);
    
    static DomainElementInt64* cast(const DomainElement* aDomainElement);
    
    inline std::string toString() const override
    {
      return std::to_string(pDomainValue);
    }//toString
    
    inline INT_64 getValue() const
    {
      return pDomainValue;
    }
    
    inline DomainElementKey* getDomainKey() override
    {
      return pDomainKey.get();
    }
    
    bool isEqual(const DomainElement* aOther) const override;
    
    bool isNotEqual(const DomainElement* aOther) const override;
    
    bool isLessThan(const DomainElement* aOther) const override;
    
    bool isLessThanOrEqual(const DomainElement* aOther) const override;
    
    DomainElement* zero() const override;
    
    DomainElement* mirror() const override;
    
    DomainElement* abs() const override;
    
    DomainElement* successor() const override;
    
    DomainElement* predecessor() const override;
    
    DomainElement* min(const DomainElement* aOther) const override;
    
    DomainElement* max(const DomainElement* aOther) const override;
    
    DomainElement* div(const DomainElement* aOther) const override;
    
    DomainElement* plus(const DomainElement* aOther) const override;
    
    DomainElement* times(const DomainElement* aOther) const override;
    
    std::size_t sizeSpan(const DomainElement* aOther) const override;
    
  private:
    /// Private base constructor
    DomainElementInt64();
    
    /// Internal domain value
    INT_64 pDomainValue;
    
    /// Domain unique key
    std::unique_ptr<DomainElementKey> pDomainKey;
    
    /// Returns the domain element saturated on INT_64
    DomainElement *getElementOverSaturation(REAL aVal) const;
    
    static DomainElementInt64& getZeroDomainElementInt64()
    {
      static DomainElementInt64 zeroElement(0);
      return zeroElement;
    }//getZeroDomainElementInt64
    
    static DomainElementInt64& getMaxDomainElementInt64()
    {
      static DomainElementInt64 maxElement(Limits::int64Max());
      return maxElement;
    }//getMaxDomainElementInt64
    
    static DomainElementInt64& getMinDomainElementInt64()
    {
      static DomainElementInt64 minElement(Limits::int64Min());
      return minElement;
    }//getMinDomainElementInt64
  };
  
} // end namespace Core
