//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 2/15/2017
//
// Object representing the state, i.e., a snapshot
// of a Domain object, used in Memento pattern.
//

#pragma once

// Export definitions
#include "DomainExportDefs.hpp"

// Domain definitions
#include "DomainDefs.hpp"

namespace Core {
  
  class CORE_DOMAIN_EXPORT_CLASS DomainState {
  public:
    virtual ~DomainState();
    
    inline DomainStateType getDomainStateType() const
    {
      return pDomStateType;
    }
    
  protected:
    DomainState(DomainStateType aDomStateType);
    
  private:
    DomainStateType pDomStateType;
  };
  
} // end namespace Core
