//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 03/18/2017
//
// Array - Matrix template include file.
//

#pragma once

// Array
#include "DArray.hpp"

// Matrix
#include "DMatrix2D.hpp"

// Forward declarations
namespace Core {
  
  // Forward declarations
  class Domain;
  class DomainElement;
  
  /// Typedefs for arrays
  typedef DArray<Domain*> DomainArray;
  typedef DArray<std::shared_ptr<Domain>> DomainSPtrArray;
  typedef DArray<DomainElement*> DomainElementArray;
  
  /// Smart pointers
  typedef std::unique_ptr<DomainArray> DomainArrayUPtr;
  typedef std::shared_ptr<DomainArray> DomainArraySPtr;
  typedef std::unique_ptr<DomainSPtrArray> DomainSPtrArrayUPtr;
  typedef std::shared_ptr<DomainSPtrArray> DomainSPtrArraySPtr;
  typedef std::unique_ptr<DomainElementArray> DomainElementArrayUPtr;
  typedef std::shared_ptr<DomainElementArray> DomainElementArraySPtr;
  
  template class DArray<Domain*>;
  template class DArray<std::shared_ptr<Domain>>;
  template class DArray<DomainElement*>;
}// end namespace Core

namespace Core {
  
  // Forward declarations
  class Domain;
  class DomainElement;
  template <typename T> class DMatrix2D;
  
  /// Typedefs for Matrix
  typedef DMatrix2D<Domain*> DomainMatrix2D;
  typedef DMatrix2D<std::shared_ptr<Domain>> DomainSPtrMatrix2D;
  typedef DMatrix2D<DomainElement*> DomainElementMatrix2D;
  
  /// Smart pointers
  typedef std::unique_ptr<DomainMatrix2D> DomainMatrix2DUPtr;
  typedef std::shared_ptr<DomainMatrix2D> DomainMatrix2DSPtr;
  typedef std::unique_ptr<DomainSPtrMatrix2D> DomainSPtrMatrix2DUPtr;
  typedef std::shared_ptr<DomainSPtrMatrix2D> DomainSPtrMatrix2DSPtr;
  typedef std::unique_ptr<DomainElementMatrix2D> DomainElementMatrix2DUPtr;
  typedef std::shared_ptr<DomainElementMatrix2D> DomainElementMatrix2DSPtr;
  
  template class DMatrix2D<Domain*>;
  template class DMatrix2D<std::shared_ptr<Domain>>;
  template class DMatrix2D<DomainElement*>;
}// end namespace Core

