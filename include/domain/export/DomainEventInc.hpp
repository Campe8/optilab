//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 10/06/2016
//

#pragma once

#include "DomainEventVoid.hpp"
#include "DomainEventSingleton.hpp"
#include "DomainEventBound.hpp"
#include "DomainEventLwb.hpp"
#include "DomainEventUpb.hpp"
#include "DomainEventChange.hpp"
#include "DomainEventFail.hpp"
#include "DomainEventUndef.hpp"

