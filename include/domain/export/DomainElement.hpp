//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 09/27/2016
//

#pragma once

// Export definitions
#include "DomainExportDefs.hpp"

// Domain type macros
#include "DomainTypeDefs.hpp"

#include <string>
#include <unordered_map>
#include <memory>
#include <cassert>

// Forward declarations
namespace Core {
  // DomainElement keys
  class DomainElementKey;
  
  // DomainElement key hash
  struct DomainElementKeyHasher;
  
  // Fwd declaration for DomainElementCategoryMap
  class DomainElement;
  
  // Domain element manager
  class DomainElementManager;
  
  typedef std::unique_ptr<DomainElement> DomainElementUPtr;
  typedef std::shared_ptr<DomainElement> DomainElementSPtr;
}// end namespace Core

namespace Core {
  
  typedef std::unordered_map<DomainElementKey, DomainElement*, DomainElementKeyHasher> DomainElementCategoryMap;
  
  /**
   * DomainElement represents the unit belonging to
   * a domain collection, i.e., the value that can be
   * assigned to a variable.
   * For example, DomainElement can be int, float, or
   * other types user-defined.
   */
  
  class CORE_DOMAIN_EXPORT_CLASS DomainElement {
  public:
    // Rounding mode for operations
    // such as division "div"
    enum RoundingMode {
        DE_ROUNDING_CEILING = 0
      , DE_ROUNDING_FLOOR
      , DE_ROUNDING_UNSPEC  // C default rounding mode
    };
    
    virtual ~DomainElement();
    
    DomainElement(const DomainElement& aOther);
    
    DomainElement(DomainElement&& aOther);
    
    DomainElement& operator=(const DomainElement& aOther);
    
    DomainElement& operator=(DomainElement&& aOther);
    
    /// Set rounding mode, default is DE_ROUNDING_UNSPEC
    inline void setRoundingMode(RoundingMode aRoundingMode)
    {
      pRoundingMode = aRoundingMode;
    }
    
    /// Get current rounding mode
    inline RoundingMode getRoundingMode() const
    {
      return pRoundingMode;
    }
    
    /// Send DomainElement's value to ostream
    friend std::ostream& operator<<(std::ostream &out, DomainElement &aDomainElement);
    
    inline DomainElementType getType() const
    {
      return pDomainElementType;
    }
    
    static bool isEqual(const DomainElement* aElem1, const DomainElement* aElem2)
    {
      return aElem1->isEqual(aElem2);
    }
    
    static bool isNotEqual(const DomainElement* aElem1, const DomainElement* aElem2)
    {
      return aElem1->isNotEqual(aElem2);
    }
    
    static bool lessThan(const DomainElement* aElem1, const DomainElement* aElem2)
    {
      return aElem1->isLessThan(aElem2);
    }
    
    static bool lessThanOrEqual(const DomainElement* aElem1, const DomainElement* aElem2)
    {
      return aElem1->isLessThanOrEqual(aElem2);
    }
    
    // Returns the zero element according to the DomainElementType "aElem"
    static DomainElement* zero(const DomainElement* aElem)
    {
      return aElem->zero();
    }
    
    // Returns the mirror element
    static DomainElement* mirror(const DomainElement* aElem)
    {
      return aElem->mirror();
    }
    
    static DomainElement* abs(const DomainElement* aElem)
    {
      return aElem->abs();
    }
    
    static DomainElement* successor(const DomainElement* aElem)
    {
      return aElem->successor();
    }
    
    static DomainElement* predecessor(const DomainElement* aElem)
    {
      return aElem->predecessor();
    }
    
    /// Returns a DomainElement equal to min("aElem1", "aElem2")
    static DomainElement* min(const DomainElement* aElem1, const DomainElement* aElem2)
    {
      return aElem1->min(aElem2);
    }
    
    /// Returns a (new) DomainElement equal to max("aElem1", "aElem2")
    static DomainElement* max(const DomainElement* aElem1, const DomainElement* aElem2)
    {
      return aElem1->max(aElem2);
    }
    
    static DomainElement* div(const DomainElement* aElem1, const DomainElement* aElem2)
    {
      return aElem1->div(aElem2);
    }
    
    static DomainElement* plus(const DomainElement* aElem1, const DomainElement* aElem2)
    {
      return aElem1->plus(aElem2);
    }
    
    static DomainElement* times(const DomainElement* aElem1, const DomainElement* aElem2)
    {
      return aElem1->times(aElem2);
    }
    
    static std::size_t sizeSpan(const DomainElement* aElem1, const DomainElement* aElem2)
    {
      return aElem1->sizeSpan(aElem2);
    }
    
    virtual std::string toString() const = 0;
    
    /// Returns this DomainElementKey
    virtual DomainElementKey* getDomainKey() = 0;
    
    /// Returns true if this == aOther, false otherwise
    virtual bool isEqual(const DomainElement* aOther) const = 0;
    
    /// Returns true if this != aOther, false otherwise
    virtual bool isNotEqual(const DomainElement* aOther) const = 0;
    
    /// Returns true if this < aOther, false otherwise
    virtual bool isLessThan(const DomainElement* aOther) const = 0;
    
    /// Returns true if this <= aOther, false otherwise
    virtual bool isLessThanOrEqual(const DomainElement* aOther) const = 0;
    
    /// Returns zero(this), i.e., the zero element
    virtual DomainElement* zero() const = 0;
    
    /// Returns mirror(this), i.e., the specular element
    virtual DomainElement* mirror() const = 0;
    
    /// Returns abs(this)
    virtual DomainElement* abs() const = 0;
    
    /// Returns DomainElement successor of this
    virtual DomainElement* successor() const = 0;
    
    /// Returns DomainElement predecessor of this
    virtual DomainElement* predecessor() const = 0;
    
    /// Returns min in {this, "aOther"}
    virtual DomainElement* min(const DomainElement* aOther) const = 0;
    
    /// Returns max in {this, "aOther"}
    virtual DomainElement* max(const DomainElement* aOther) const = 0;
    
    /// Returns this / "aOther"
    virtual DomainElement* div(const DomainElement* aOther) const = 0;
    
    /// Returns this + "aOther"
    virtual DomainElement* plus(const DomainElement* aOther) const = 0;
    
    /// Returns this * "aOther"
    virtual DomainElement* times(const DomainElement* aOther) const = 0;
    
    /// Return the number of elements between this and "aOther"
    virtual std::size_t sizeSpan(const DomainElement* aOther) const = 0;
    
  protected:
    DomainElement(DomainElementType aType);
    
    /// Get domain element manager used to create new instances of domain elements
    DomainElementManager& getDomainElementManager() const;
    
  private:
    DomainElementType pDomainElementType;
    
    /// Rounding mode
    RoundingMode pRoundingMode;
  };
  
} // end namespace Core
