#pragma once

#include "GlobalsType.hpp"

#define UBOOL   BOOL_T
#define INT_32  INT_32_T
#define INT_64  INT_64_T
#define UINT_64 UINT_64_T
#define SINGLE  SINGLE_T
#define REAL    REAL_T

namespace Core {
  
  enum DomainElementType {
      DET_INT_64 = 0
    , DET_REAL
    , DET_VOID
    , DET_UNDEF
  };
  
} // end namespace Core

