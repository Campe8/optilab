//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 2/15/2017
//
// Memento state for backtrackable objects
// of class Domain.
//

#pragma once

// Base class
#include "MementoState.hpp"

// Domain state
#include "DomainState.hpp"


namespace Search {
  
  class SEARCH_EXPORT_CLASS MementoStateDomain : public MementoState {
  public:
    MementoStateDomain(std::unique_ptr<Core::DomainState> aDomainState);
    
    ~MementoStateDomain();
    
    static bool isa(const MementoState* aMementoState)
    {
      assert(aMementoState);
      return aMementoState->getMementoStateId() == MementoStateId::MS_DOMAIN;
    }
    
    static MementoStateDomain* cast(MementoState* aMementoState)
    {
      if (!isa(aMementoState))
      {
        return nullptr;
      }
      return static_cast<MementoStateDomain*>(aMementoState);
    }
    
    static const MementoStateDomain* cast(const MementoState* aMementoState)
    {
      if (!isa(aMementoState))
      {
        return nullptr;
      }
      return static_cast<const MementoStateDomain*>(aMementoState);
    }
    
    inline std::unique_ptr<Core::DomainState> getDomainState()
    {
      return std::move(pDomainState);
    }
    
    inline const Core::DomainState* getDomainState() const
    {
      return pDomainState.get();
    }
    
  private:
    std::unique_ptr<Core::DomainState> pDomainState;
  };
  
} // end namespace Search
