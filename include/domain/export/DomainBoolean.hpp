//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/24/2016
//

#pragma once

// Export definitions
#include "DomainExportDefs.hpp"

// Base class
#include "DomainInteger.hpp"

#include <memory>
#include <unordered_set>
#include <cassert>

// Forward declarations
namespace Core {
  class DomainBoolean;
  
  typedef DomainBoolean* DomainBooleanPtr;
  typedef std::unique_ptr<DomainBoolean> DomainBooleanUPtr;
  typedef std::shared_ptr<DomainBoolean> DomainBooleanSPtr;
}// end namespace Core

namespace Core {
  
  class CORE_DOMAIN_EXPORT_CLASS DomainBoolean : public DomainInteger {
  public:
    DomainBoolean();
    
    DomainBoolean(const DomainBoolean& aOther);
    
    DomainBoolean(DomainBoolean&& aOther);
    
    virtual ~DomainBoolean();
    
    DomainBoolean& operator= (const DomainBoolean& aOther);
    
    DomainBoolean& operator= (DomainBoolean&& aOther);
    
    static bool isa(const Domain *aDomain)
    {
      assert(aDomain);
      return aDomain->getDomainType() == DomainClass::DOM_BOOL;
    }
    
    static DomainBoolean* cast(Domain *aDomain)
    {
      if(!isa(aDomain))
      {
        return nullptr;
      }
      return static_cast<DomainBoolean*>(aDomain);
    }
    
    /// Sets domain to singleton {True}.
    /// If True is not present, empty domain.
    void setTrue();
    
    /// Sets domain to singleton {False}.
    /// If False is not present, empty domain.
    void setFalse();
    
    /// Returns true if the domain is ground and set to True,
    /// False otherwise
    bool isTrue() const;
    
    /// Returns true if the domain is ground and set to False,
    /// False otherwise
    bool isFalse() const;
    
  protected:
    inline DomainElementContainer* getDomainContainer() const override
    {
      return pElementContainer.get();
    }
    
    inline DomainDecorator* getDomainDecorator() const override
    {
      return pDecorator.get();
    }
    
  private:
    /// Domain element container
    std::unique_ptr<DomainElementContainer> pElementContainer;
    
    /// Domain decorator
    std::unique_ptr<DomainDecorator> pDecorator;
  };
  
} // end namespace Core
