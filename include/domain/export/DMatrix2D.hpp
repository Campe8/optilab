//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/20/2016
//
// 2D Matrix container for domain elements.
//

#pragma once

// Export definitions
#include "DomainExportDefs.hpp"

// Domain type macros
#include "DomainTypeDefs.hpp"

// Boost multi-array data structure for matrices
#include "boost/multi_array.hpp"
#include <utility>
#include <cassert>

namespace Core {
  
  template <typename T>
  class CORE_DOMAIN_EXPORT_TEMPLATE DMatrix2D {
  public:
    /// Constructor: matrix has fixed size.
    /// Allocates a "aNumRows" x "aNumCols" matrix with all values initialized to nullptr.
    DMatrix2D(std::size_t aNumRows, std::size_t aNumCols)
    : pNumRows(aNumRows)
    , pNumCols(aNumCols)
    , pMatrix(boost::extents[aNumRows][aNumCols])
    {
      initMatrix();
    }
    
    DMatrix2D(const DMatrix2D<T>& aOther)
    : pNumRows(aOther.pNumRows)
    , pNumCols(aOther.pNumCols)
    , pMatrix(boost::extents[pNumRows][pNumCols])
    {
      pMatrix = aOther.pMatrix;
    }
    
    DMatrix2D(DMatrix2D<T>&& aOther)
    : pNumRows(aOther.pNumRows)
    , pNumCols(aOther.pNumCols)
    , pMatrix(boost::extents[pNumRows][pNumCols])
    {
      pMatrix = aOther.pMatrix;
      
      aOther.pNumRows = 0;
      aOther.pNumCols = 0;
      aOther.pMatrix.resize(boost::extents[0][0]);
    }
    
    virtual ~DMatrix2D()
    {
    }
    
    DMatrix2D& operator=(const DMatrix2D<T>& aOther)
    {
      if (&aOther != this)
      {
        pNumRows = aOther.pNumRows;
        pNumCols = aOther.pNumCols;
        pMatrix.resize(boost::extents[pNumRows][pNumCols]);
        pMatrix = aOther.pMatrix;
      }
      
      return *this;
    }
    
    DMatrix2D& operator=(DMatrix2D<T>&& aOther)
    {
      if (&aOther != this)
      {
        pNumRows = aOther.pNumRows;
        pNumCols = aOther.pNumCols;
        pMatrix.resize(boost::extents[pNumRows][pNumCols]);
        pMatrix = aOther.pMatrix;
        
        aOther.pNumRows = 0;
        aOther.pNumCols = 0;
        aOther.pMatrix.resize(boost::extents[0][0]);
      }
      
      return *this;
    }
    
    inline std::size_t numRows() const
    {
      return pNumRows;
    }
    
    inline std::size_t numCols() const
    {
      return pNumCols;
    }
    
    /// Clears array, i.e., replaces array elements with nullptr
    inline void clear()
    {
      initMatrix();
    }
    
    /// Returns the (total) number of elements contained in the matrix
    inline std::size_t size() const
    {
      return pMatrix.num_elements();
    }
    
    /// Returns true if the matrix2D contains only nullptrs
    inline bool empty() const
    {
      return false;
    }//empty
    
    /// Returns the pair <row, column> idx s.t. *this[row][column] == "aElement",
    /// or returns <numRows, numCols> if "aElement" is not present in DMatrix2D
    std::pair<std::size_t, std::size_t> find(T aElement)
    {
      for(std::size_t aRowIdx = 0; aRowIdx < pNumRows; ++aRowIdx)
      {
        for(std::size_t aColIdx = 0; aColIdx < pNumCols; ++aColIdx)
        {
          if(pMatrix[aRowIdx][aColIdx] == aElement)
          {
            return std::make_pair(aRowIdx, aColIdx);
          }
        }
      }
      return std::make_pair(pNumRows, pNumCols);
    }
    
    ///  Assigns "aDomainElementToAssign" to the matrix cell ["aRowIdx", "aColIdx"].
    /// @note throws out of range exception if "aRowIdx" or "aColIdx" do not match matrix dimensions
    void assignElementToCell(std::size_t aRowIdx, std::size_t aColIdx, T aElement)
    {
      if (aRowIdx >= pNumRows || aColIdx >= pNumCols)
      {
        throw std::out_of_range("DMatrix2D out_of_range exception");
      }
      
      pMatrix[aRowIdx][aColIdx] = aElement;
    }//assignDomainElementToCell
    
    inline boost::multi_array<T, 1> operator[](std::size_t idx) const
    {
      return pMatrix[idx];
    }
    
  private:
    typedef boost::multi_array<T, 2> matrix2Dtype;
    
    /// Number of rows
    std::size_t pNumRows;
    
    /// Number of columns
    std::size_t pNumCols;
    
    /// Matrix data structure
    matrix2Dtype pMatrix;
    
    void initMatrix()
    {
      for(std::size_t aRowIdx = 0; aRowIdx < pNumRows; ++aRowIdx)
      {
        for(std::size_t aColIdx = 0; aColIdx < pNumCols; ++aColIdx)
        {
          pMatrix[aRowIdx][aColIdx] = nullptr;
        }
      }
    }//initMatrix
    
  };
  
  template <typename T>
  class CORE_DOMAIN_EXPORT_TEMPLATE DMatrix2D<T*> {
  public:
    /// Constructor: matrix has fixed size.
    /// Allocates a "aNumRows" x "aNumCols" matrix with all values initialized to nullptr.
    DMatrix2D(std::size_t aNumRows, std::size_t aNumCols)
    : pNumRows(aNumRows)
    , pNumCols(aNumCols)
    , pMatrix(boost::extents[aNumRows][aNumCols])
    {
      initMatrix();
    }
    
    DMatrix2D(const DMatrix2D<T*>& aOther)
    : pNumRows(aOther.pNumRows)
    , pNumCols(aOther.pNumCols)
    , pMatrix(boost::extents[pNumRows][pNumCols])
    {
      pMatrix = aOther.pMatrix;
    }
    
    DMatrix2D(DMatrix2D<T*>&& aOther)
    : pNumRows(aOther.pNumRows)
    , pNumCols(aOther.pNumCols)
    , pMatrix(boost::extents[pNumRows][pNumCols])
    {
      pMatrix = aOther.pMatrix;
      
      aOther.pNumRows = 0;
      aOther.pNumCols = 0;
      aOther.pMatrix.resize(boost::extents[0][0]);
    }
    
    virtual ~DMatrix2D()
    {
    }
    
    DMatrix2D& operator=(const DMatrix2D<T*>& aOther)
    {
      if (&aOther != this)
      {
        pNumRows = aOther.pNumRows;
        pNumCols = aOther.pNumCols;
        pMatrix.resize(boost::extents[pNumRows][pNumCols]);
        pMatrix = aOther.pMatrix;
      }
      
      return *this;
    }
    
    DMatrix2D& operator=(DMatrix2D<T*>&& aOther)
    {
      if (&aOther != this)
      {
        pNumRows = aOther.pNumRows;
        pNumCols = aOther.pNumCols;
        pMatrix.resize(boost::extents[pNumRows][pNumCols]);
        pMatrix = aOther.pMatrix;
        
        aOther.pNumRows = 0;
        aOther.pNumCols = 0;
        aOther.pMatrix.resize(boost::extents[0][0]);
      }
      
      return *this;
    }
    
    inline std::size_t numRows() const
    {
      return pNumRows;
    }
    
    inline std::size_t numCols() const
    {
      return pNumCols;
    }
    
    /// Clears array, i.e., replaces array elements with nullptr
    inline void clear()
    {
      initMatrix();
    }
    
    /// Returns the (total) number of elements contained in the matrix
    inline std::size_t size() const
    {
      return pMatrix.num_elements();
    }
    
    /// Returns true if the matrix2D contains only nullptrs
    inline bool empty() const
    {
      for(std::size_t aRowIdx = 0; aRowIdx < pNumRows; ++aRowIdx)
      {
        for(std::size_t aColIdx = 0; aColIdx < pNumCols; ++aColIdx)
        {
          if(pMatrix[aRowIdx][aColIdx] != nullptr)
          {
            return false;
          }
        }
      }
      return true;
    }//empty
    
    /// Returns the pair <row, column> idx s.t. *this[row][column] == "aElement",
    /// or returns <numRows, numCols> if "aElement" is not present in DMatrix2D
    std::pair<std::size_t, std::size_t> find(T *aElement)
    {
      for(std::size_t aRowIdx = 0; aRowIdx < pNumRows; ++aRowIdx)
      {
        for(std::size_t aColIdx = 0; aColIdx < pNumCols; ++aColIdx)
        {
          if(pMatrix[aRowIdx][aColIdx] == aElement)
          {
            return std::make_pair(aRowIdx, aColIdx);
          }
        }
      }
      return std::make_pair(pNumRows, pNumCols);
    }
    
    ///  Assigns "aDomainElementToAssign" to the matrix cell ["aRowIdx", "aColIdx"].
    /// @note throws out of range exception if "aRowIdx" or "aColIdx" do not match matrix dimensions
    void assignElementToCell(std::size_t aRowIdx, std::size_t aColIdx, T* aElement)
    {
      if (aRowIdx >= pNumRows || aColIdx >= pNumCols)
      {
        throw std::out_of_range("DMatrix2D out_of_range exception");
      }
      
      pMatrix[aRowIdx][aColIdx] = aElement;
    }//assignDomainElementToCell
    
    inline boost::multi_array<T*, 1> operator[](std::size_t idx) const
    {
      return pMatrix[idx];
    }
    
  private:
    typedef boost::multi_array<T*, 2> matrix2Dtype;
    
    /// Number of rows
    std::size_t pNumRows;
    
    /// Number of columns
    std::size_t pNumCols;
    
    /// Matrix data structure
    matrix2Dtype pMatrix;
    
    void initMatrix()
    {
      for(std::size_t aRowIdx = 0; aRowIdx < pNumRows; ++aRowIdx)
      {
        for(std::size_t aColIdx = 0; aColIdx < pNumCols; ++aColIdx)
        {
          pMatrix[aRowIdx][aColIdx] = nullptr;
        }
      }
    }//initMatrix
  };
  
} // end namespace Core
