//
// Copyright OptiLab 2016-2017. All rights reserved.
//
// Created by Federico Campeotto on 11/20/2016
//
// Array container for Domain and DomainElement.
//

#pragma once

// Export definitions
#include "DomainExportDefs.hpp"

// Domain type macros
#include "DomainTypeDefs.hpp"

#include <vector>
#include <cassert>
#include <memory>

namespace Core {
  
  template <typename T>
  class CORE_DOMAIN_EXPORT_TEMPLATE DArray {
  public:
    using DArrayIter = typename std::vector<T>::iterator;
    
  public:

    /// Allocates an array of "aArraySize"
    DArray(std::size_t aArraySize)
    : pArray(aArraySize)
    {
    }
    
    virtual ~DArray()
    {
    }
    
    inline std::size_t size() const
    {
      return pArray.size();
    }
    
    void clear()
    {
      std::size_t sizeArray = size();
      pArray.clear();
      pArray = std::vector<T>(sizeArray);
    }// clear

    inline bool empty() const
    {
      return false;
    }

    /// Returns the index idx s.t. operator[](idx) == "aElement",
    /// or returns size() if "aElement" is not present in DArray
    std::size_t find(T aElement)
    {
      for(std::size_t idx = 0; idx < size(); ++idx)
      {
        if(operator[](idx) == aElement)
        {
          return idx;
        }
      }
      return size();
    }
    
    ///  Assigns "aElement" to the array cell ["aCell"].
    /// @note throws out of range exception if "aCell" does not match array dimensions
    void assignElementToCell(std::size_t aCell, T aElement)
    {
      if (aCell >= size())
      {
        throw std::out_of_range("DArray out_of_range exception");
      }
      
      pArray[aCell] = aElement;
    }
    
    inline T operator[](std::size_t aIdx)
    {
      assert(aIdx < size());
      return pArray[aIdx];
    }
    
    inline const T operator[](std::size_t aIdx) const
    {
      assert(aIdx < size());
      return pArray[aIdx];
    }
    
    /// Range-loop iterators
    inline DArrayIter begin() { return pArray.begin(); }
    inline DArrayIter end()   { return pArray.end();   }
    
  private:
    std::vector<T> pArray;
  };
  
  template <typename T>
  class CORE_DOMAIN_EXPORT_TEMPLATE DArray<T*> {
  public:
    DArray(std::size_t aArraySize)
    : pArrayPtr(aArraySize)
    {
    }
    
    virtual ~DArray()
    {
    }
    
    void clear()
    {
      auto sizeArray = size();
      pArrayPtr.clear();
      pArrayPtr = std::vector<T*>(sizeArray, nullptr);
    }// clear
    
    bool empty() const
    {
      for(auto ptr : pArrayPtr)
      {
        if(ptr != nullptr)
        {
          return false;
        }
      }
      return true;
    }//empty
    
    inline std::size_t size() const
    {
      return pArrayPtr.size();
    }
    
    std::size_t find(T* aElement)
    {
      for(std::size_t idx = 0; idx < size(); ++idx)
      {
        if(pArrayPtr[idx] == aElement)
        {
          return idx;
        }
      }
      return pArrayPtr.size();
    }
    
    void assignElementToCell(std::size_t aCell, T* aElement)
    {
      if (aCell >= size())
      {
        throw std::out_of_range("DArray out_of_range exception");
      }
      
      pArrayPtr[aCell] = aElement;
    }
    
    inline T* operator[](std::size_t aIdx)
    {
      assert(aIdx < size());
      return pArrayPtr[aIdx];
    }
    
    inline const T* operator[](std::size_t aIdx) const
    {
      assert(aIdx < size());
      return pArrayPtr[aIdx];
    }
    
    /// Range-loop iterators
    inline typename std::vector<T*>::iterator begin() { return pArrayPtr.begin(); }
    inline typename std::vector<T*>::iterator end()   { return pArrayPtr.end();   }
    
  private:
    std::vector<T*> pArrayPtr;
  };
  
} // end namespace Core
