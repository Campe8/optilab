//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 09/29/2016
//

#pragma once

// Base class
#include "DomainElement.hpp"

#include "DomainElementManager.hpp"

#include <unordered_map>
#include <memory>
#include <cassert>

// Forward declaration
namespace Core {
  class DomainElementKey;
}

namespace Core {
  
  class CORE_DOMAIN_EXPORT_CLASS DomainElementVoid : public DomainElement {
  public:
    DomainElementVoid();
    
    virtual ~DomainElementVoid();
    
    static bool isa(const DomainElement* aDomainElement);
    
    static DomainElementVoid* cast(DomainElement* aDomainElement);
    
    std::string toString() const override
    {
      return std::string{};
    }//toString
    
    inline DomainElementKey* getDomainKey() override
    {
      return nullptr;
    }
    
    inline bool isEqual(const DomainElement* aOther) const override
    {
      assert(aOther);
      if(isa(aOther))
      {
        return true;
      }
      
      return false;
    }
    
    inline bool isNotEqual(const DomainElement* aOther) const override
    {
      return !isEqual(aOther);
    }
    
    inline bool isLessThan(const DomainElement* aOther) const override
    {
      (void)aOther;
      
      return false;
    }
    
    inline bool isLessThanOrEqual(const DomainElement* aOther) const override
    {
      (void)aOther;
      
      return false;
    }
    
    inline DomainElement* min(const DomainElement* aOther) const override
    {
      (void) aOther;
      return getDomainElementManager().getVoidElement();
    }
    
    inline DomainElement* max(const DomainElement* aOther) const override
    {
      (void) aOther;
      return getDomainElementManager().getVoidElement();
    }
    
    inline DomainElement* zero() const override
    {
      return getDomainElementManager().getVoidElement();
    }
    
    inline DomainElement* mirror() const override
    {
      return getDomainElementManager().getVoidElement();
    }
    
    inline DomainElement* abs() const override
    {
      return getDomainElementManager().getVoidElement();
    }
    
    inline DomainElement* successor() const override
    {
      return getDomainElementManager().getVoidElement();
    }
    
    inline DomainElement* predecessor() const override
    {
      return getDomainElementManager().getVoidElement();
    }
    
    inline DomainElement* div(const DomainElement* aOther) const override
    {
      (void)aOther;
      return getDomainElementManager().getVoidElement();
    }
    
    inline DomainElement* plus(const DomainElement* aOther) const override
    {
      (void)aOther;
      return getDomainElementManager().getVoidElement();
    }
    
    inline DomainElement* times(const DomainElement* aOther) const override
    {
      (void)aOther;
      return getDomainElementManager().getVoidElement();
    }
    
    std::size_t sizeSpan(const DomainElement* aOther) const override
    {
      return 0;
    }//sizeSpan
    
  };
  
} // end namespace Core
