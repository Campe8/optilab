//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/01/2016
//

#pragma once

// Base class
#include "DomainElement.hpp"
// Definitions
#include "DomainDefs.hpp"

#include <unordered_map>
#include <memory>

#include <boost/multiprecision/cpp_dec_float.hpp>

// Forward declaration
namespace Core {
  class DomainElementKey;
}

namespace Core {
  
  class CORE_DOMAIN_EXPORT_CLASS DomainElementReal : public DomainElement {
  public:
    explicit DomainElementReal(REAL aVal);
    
    DomainElementReal(const DomainElementReal& aOther);
    
    DomainElementReal(DomainElementReal&& aOther);
    
    virtual ~DomainElementReal();
    
    DomainElementReal& operator=(const DomainElementReal& aOther);
    
    DomainElementReal& operator=(DomainElementReal&& aOther);
    
    static bool isa(const DomainElement* aDomainElement);
    
    static DomainElementReal* cast(DomainElement* aDomainElement);
    
    static DomainElementReal* cast(const DomainElement* aDomainElement);
    
    /// Get the value truncated after the given precision
    static REAL getValueTruncateAfterPrecision(REAL aVal, REAL aPrecision);
    
    inline std::string toString() const override
    {
      return std::to_string(pDomainValue);
    }//toString
    
    inline REAL getValue() const
    {
      return pDomainValue;
    }
    
    inline DomainElementKey* getDomainKey() override
    {
      return pDomainKey.get();
    }
    
    /// Set the step to use in internal operations such as
    /// successor/predecessor.
    /// Default is set to REAL_DFT_STEP.
    inline void setRealStep(REAL aStep)
    {
      pRealStep = aStep;
    }
    
    /// Return step used in internal operations such as
    /// successor/predecessor.
    /// Default is set to REAL_DFT_STEP.
    inline REAL getRealStep() const
    {
      return pRealStep;
    }
    
    bool isEqual(const DomainElement* aOther) const override;
    
    bool isNotEqual(const DomainElement* aOther) const override;
    
    bool isLessThan(const DomainElement* aOther) const override;
    
    bool isLessThanOrEqual(const DomainElement* aOther) const override;
    
    DomainElement* zero() const override;
    
    DomainElement* mirror() const override;
    
    DomainElement* abs() const override;
    
    DomainElement* successor() const override;
    
    DomainElement* predecessor() const override;
    
    DomainElement* min(const DomainElement* aOther) const override;
    
    DomainElement* max(const DomainElement* aOther) const override;
    
    DomainElement* div(const DomainElement* aOther) const override;
    
    DomainElement* plus(const DomainElement* aOther) const override;
    
    DomainElement* times(const DomainElement* aOther) const override;
    
    std::size_t sizeSpan(const DomainElement* aOther) const override;
    
  private:
    /// Private base constructor
    DomainElementReal();
    
    /// Internal domain value
    REAL pDomainValue;
    
    /// Step to use between real number on operations
    REAL pRealStep;
    
    /// Domain unique key
    std::unique_ptr<DomainElementKey> pDomainKey;
    
    static DomainElementReal& getZeroDomainElementReal()
    {
      static DomainElementReal zeroElement(0.0);
      return zeroElement;
    }//getZeroDomainElementReal
    
    static DomainElementReal& getMaxDomainElementReal()
    {
      static DomainElementReal maxElement(Limits::realMax());
      return maxElement;
    }//getMaxDomainElementReal
    
    static DomainElementReal& getMinDomainElementReal()
    {
      static DomainElementReal minElement(Limits::realMin());
      return minElement;
    }//getMinDomainElementReal
  };
  
} // end namespace Core
