//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 10/24/2016
//

#pragma once

// Export definitions
#include "DomainExportDefs.hpp"

// Base
#include "Domain.hpp"

#include <memory>
#include <unordered_set>

// Forward declarations
namespace Core {
  class DomainInteger;
  
  typedef DomainInteger* DomainIntegerPtr;
  typedef std::unique_ptr<DomainInteger> DomainIntegerUPtr;
  typedef std::shared_ptr<DomainInteger> DomainIntegerSPtr;
}// end namespace Core

namespace Core {
  
  class CORE_DOMAIN_EXPORT_CLASS DomainInteger : public Domain {
  public:
    DomainInteger(DomainElementInt64 *aLowerBound, DomainElementInt64 *aUpperBound);
    
    DomainInteger(const std::unordered_set<DomainElementInt64 *>& aSetOfElements);
    
    DomainInteger(const DomainInteger& aOther);
    
    DomainInteger(DomainInteger&& aOther);
    
    virtual ~DomainInteger();
    
    DomainInteger& operator= (const DomainInteger& aOther);
    
    DomainInteger& operator= (DomainInteger&& aOther);
    
    static bool isa(const Domain *aDomain)
    {
      assert(aDomain);
      return aDomain->getDomainType() == DomainClass::DOM_INT64;
    }
    
    static DomainInteger* cast(Domain *aDomain)
    {
      if(!isa(aDomain))
      {
        return nullptr;
      }
      return static_cast<DomainInteger*>(aDomain);
    }
    
  protected:
    inline DomainElementContainer* getDomainContainer() const override
    {
      return pElementContainer.get();
    }
    
    inline DomainDecorator* getDomainDecorator() const override
    {
      return pDecorator.get();
    }
    
  private:
    /// Domain element container
    std::unique_ptr<DomainElementContainer> pElementContainer;
    
    /// Domain decorator
    std::unique_ptr<DomainDecorator> pDecorator;
  };
  
} // end namespace Core
