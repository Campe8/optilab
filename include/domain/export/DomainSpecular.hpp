//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 12/02/2016
//
// Wrapper class around Domain to perform specular
// operations on Domain's element.
// Given a domain D, a specular domain D* is defined
// on D, i.e, D*(D), s.t., D* = -D.
//

#pragma once

// Base class
#include "Domain.hpp"

namespace Core {
  
  class CORE_DOMAIN_EXPORT_CLASS DomainSpecular : public Domain {
  public:
    DomainSpecular(Domain *aDomainBase);
    
    virtual ~DomainSpecular();
    
    inline DomainElement* lowerBound() override
    {
      return DomainElement::mirror(pDomainBase->upperBound());
    }
    
    inline DomainElement* upperBound() override
    {
      return DomainElement::mirror(pDomainBase->lowerBound());
    }
    
    inline void shrinkOnLowerBound(DomainElement *aElement) override
    {
      pDomainBase->shrinkOnUpperBound(DomainElement::mirror(aElement));
    }
    
    inline void shrinkOnUpperBound(DomainElement *aElement) override
    {
      pDomainBase->shrinkOnLowerBound(DomainElement::mirror(aElement));
    }
    
    inline void shrik(DomainElement *aLowerBoundElement, DomainElement *aUpperBoundElement)
    {
      pDomainBase->shrink(DomainElement::mirror(aUpperBoundElement), DomainElement::mirror(aLowerBoundElement));
    }
    
  protected:
    inline DomainElementContainer* getDomainContainer() const override
    {
      return pDomainBase->getDomainContainer();
    }
    
    inline DomainDecorator* getDomainDecorator() const override
    {
      return pDomainBase->getDomainDecorator();
    }
    
  private:
    Domain *pDomainBase;
  };
  
} // end namespace Core
