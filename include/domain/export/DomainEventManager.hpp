//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 10/06/2016
//

#pragma once

// Export definitions
#include "DomainExportDefs.hpp"

// Events
#include "DomainEventInc.hpp"

#include <unordered_map>

namespace Core {
  
  typedef std::unordered_map<DomainEventType, DomainEvent*, DomainEventHash> EventStoreMap;
  
  class CORE_DOMAIN_EXPORT_CLASS DomainEventManager {
  public:
    DomainEventManager();
    
    virtual ~DomainEventManager();
    
    DomainEvent *getEvent(DomainEventType aEventType);
    
  private:
    EventStoreMap* pEventStore;
    
    DomainEvent *getNewEventInstance(DomainEventType aEventType);
  };
  
} // end namespace Core
