//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 10/06/2016
//

#pragma once

// Export definitions
#include "DomainExportDefs.hpp"

// Domain type macros
#include "DomainTypeDefs.hpp"
#include "DomainDefs.hpp"

// Element Key
#include "DomainElementKey.hpp"

#include <vector>
#include <memory>
#include <unordered_map>
#include <cassert>
#include <utility>

// Forward declaration for DomainElement
namespace Core {
  // Fwd declaration for DomainElementCategoryMap
  class DomainElement;
  class DomainElementInt64;
  class DomainElementReal;
}// end namespace Core

namespace Core {
  
  typedef std::unordered_map<DomainElementKey, DomainElement*, DomainElementKeyHasher> DomainElementCategoryMap;
  typedef std::unordered_map<INT_64, DomainElementInt64*> DomainElementInt64CategoryMap;
  typedef std::unordered_map<REAL, DomainElementReal*> DomainElementRealCategoryMap;
  
  /**
   * DomainElementManager manages all the DomainElement instances of the
   * model and it is responsable for their instantiation and deallocation.
   */
  
  class CORE_DOMAIN_EXPORT_CLASS DomainElementManager {
  public:
    
    /// Get singleton instance
    static DomainElementManager& getInstance();
    
    virtual ~DomainElementManager();
    
    DomainElementManager(const DomainElementManager& aOther) = delete;
    
    DomainElementManager& operator=(const DomainElementManager& aOther) = delete;
    
    /// Delete all DomainElements instances
    void clear();
    
    /// Clones and register "aDomainElement" if not already present.
    /// Returns the cloned element.
    DomainElement* clone(DomainElement *aDomainElement);
    
    /// Finds the DomainElement with key "aDomainElementKey".
    /// Returns nullptr if not present.
    DomainElement* find(DomainElementKey *aDomainElementKey);
    
    /// Finds the DomainElementInt64 with val "aVal".
    /// Returns nullptr if not present.
    DomainElementInt64* findDomainElementInt64(INT_64 aVal);
    
    /// Creates a DomainElementInt64 with aVal as a value if not already
    /// present in the map and returns the pointer to the its instance.
    DomainElementInt64* createDomainElementInt64(INT_64 aVal);
    
    /// Creates a "local" DomainElementInt64 with aVal as a value if not already
    /// present in the map and returns the pointer to the its instance.
    /// @note the returned elment is not stored in the manager
    DomainElementInt64 createLocalDomainElementInt64(INT_64 aVal);
    
    /// Finds the DomainElementInt64 with val "aVal".
    /// Returns nullptr if not present.
    DomainElementReal* findDomainElementReal(REAL aVal);
    
    /// Creates a DomainElementReal with "aVal" as value
    // if not already present in the map and returns the pointer to the its instance.
    DomainElementReal* createDomainElementReal(REAL aVal);
    
    /// Creates "local" DomainElementReal with "aVal" as value
    // if not already present in the map and returns the pointer to the its instance.
    /// @note the returned elment is not stored in the manager
    DomainElementReal createLocalDomainElementReal(REAL aVal);
    
    /// Returns void element
    DomainElement *getVoidElement();
    
    /// Returns the number of DomainElements stored in the manager
    std::size_t size() const;
    
  private:
    /// Singleton class
    DomainElementManager();
    
    /// Map of DomainElement instances
    std::unique_ptr<DomainElementCategoryMap> pDomainElementMap;
    
    /// Map of DomainElementInt64 instances
    std::unique_ptr<DomainElementInt64CategoryMap> pDomainElementInt64Map;
    
    /// Map of DomainElementReal instances
    std::unique_ptr<DomainElementRealCategoryMap> pDomainElementRealMap;
    
    /// Void element
    DomainElement *voidElement;
  };
  
} // end namespace Core
