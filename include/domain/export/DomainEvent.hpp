//
// Copyright OptiLab 2016-2017. All rights reserved.
//
// Created by Federico Campeotto on 10/13/2016
//

#pragma once

// Export definitions
#include "DomainExportDefs.hpp"

#include <vector>
#include <set>
#include <memory>

// Must define events, with isa operations and casts. Must define an event manager: one for each domain.
// Must define operations such as change variables etc.
// Event manager should contain singleton instances of events (hash table if not present instantiate)
// If causes must added, take the singleton instance of the event and change causes.

namespace Core {
  
  enum DomainEventType : int {
      VOID_EVENT = 0    // Default domain isn't changed
    , SINGLETON_EVENT   // Domain is singleton
    , BOUND_EVENT       // Domain changed bounds
    , LWB_EVENT         // Domain increases lower bound
    , UPB_EVENT         // Domain decreases upper bound
    , CHANGE_EVENT      // Domain has been modified
    , FAIL_EVENT        // Empty domain
    , UNDEF_EVENT       // Undefined event
    , NUM_EVENTS        // Must be last
  };
  
  struct DomainEventHash
  {
    template <typename T>
    std::size_t operator()(T t) const
    {
      return static_cast<std::size_t>(t);
    }
  };
  
  class CORE_DOMAIN_EXPORT_CLASS DomainEvent {
  public:
    virtual ~DomainEvent();
    
    inline DomainEventType getEventType() const
    {
      return pEventType;
    }
    
    friend bool operator==(const DomainEvent& aLhs, const DomainEvent& aRhs)
    {
      return aLhs.getEventType() == aRhs.getEventType();
    }
    
    friend bool operator!=(const DomainEvent& aLhs, const DomainEvent& aRhs)
    {
      return aLhs.getEventType() != aRhs.getEventType();
    }
 
    /// Returns true if this event overlaps "aDomainEvent", i.e.,
    /// if this event happens, then aDomainEvent happens as well.
    /// False otherwise.
    /// @example if this event is LWB_EVENT, then when this event
    /// happens, CHANGE_EVENT happens as well and this function
    /// returns true (if aDomainEvent has CHANGE_EVENT type).
    /// @note returns false if not enough information is available
    /// to say if this event overlaps "aDomainEvent"
    virtual bool overlaps(const DomainEvent *aDomainEvent) const = 0;
    
  protected:
    DomainEvent(DomainEventType aEvent);
    
  private:
    DomainEventType pEventType;
  };
  
} // end namespace Core
