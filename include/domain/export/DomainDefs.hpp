//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 10/19/2016
//

#pragma once

#include "DomainTypeDefs.hpp"
#include <limits>

#define MAX_CONCRETE_STORAGE_BITMAP_SIZE 1024
#define MAX_CONCRETE_STORAGE_BITMAP_SIZE_STRICT 128

#define REAL_DFT_STEP 0.0001

namespace Core {
  
  namespace Limits {
    // Bool
    UBOOL boolMax();
    UBOOL boolMin();
    
    // Int64
    INT_64 int64Max();
    INT_64 int64Min();
    
    // Real
    REAL realMax();
    REAL realMin();
  }// end namespace Limits
  
  enum DomainClass : int {
      DOM_INT64 = 0
    , DOM_BOOL
    , DOM_REAL
    , DOM_UNDEF
  };
  
  enum DomainElementContainerClass : int {
      DOM_ELEM_CONTAINER_INT64 = 0
    , DOM_ELEM_CONTAINER_PAIR
    , DOM_ELEM_CONTAINER_UNDEF
  };
  
  enum DomainStateType : int {
      DOM_STATE_INT64_CONT = 0 // domain state on int64 containers
    , DOM_STATE_PAIR_CONT
    , DOM_STATE_UNDEF
  };
  
}// end namespace Core

