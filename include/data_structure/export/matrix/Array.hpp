//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/13/2017
//
// This class implements a data structure for an array.
//

#pragma once

#include "DataStructureExportDefs.hpp"

#include <vector>
#include <memory>
#include <exception>

namespace DataStructure { namespace Matrix {

  template<typename T>
  class DATA_STRUCTURE_EXPORT_CLASS Array {
  public:
    typedef std::size_t msize;
    
    Array(msize aSize)
    : pArray(std::make_shared<std::vector<T>>(aSize))
    {
    }
    
    ~Array() = default;
    
    inline bool empty() const
    {
      return false;
    }
    
    /// Returns the size of this array
    inline msize size() const
    {
      return pArray->size();
    }
    
    /// Clears the content of the matrix
    inline void clear()
    {
      msize numElem = size();
      pArray->clear();
      pArray = std::make_shared<std::vector<T>>(numElem);
    }
    
    ///	Returns a const reference of the i-th element of the array
    const T& operator()(msize aIdx) const
    {
      return pArray->operator[](aIdx);
    }
    
    ///	Returns a reference of the i-th element of the array
    T& operator()(msize aIdx)
    {
      return pArray->operator[](aIdx);
    }
    
    /// Returns the index idx such that
    /// (*this)(idx) is equal to (the first) "aVal".
    /// Returns size() if "aVal" is not present
    msize find(const T& aVal) const
    {
      for(msize it = 0; it < size(); ++it)
      {
        if(this->operator()(it) == aVal)
        {
          return it;
        }
      }
      
      return size();
    }
    
    ///  Assigns "aElement" to the array cell ["aCell"].
    /// @note throws out of range exception if "aCell" does not match array dimensions
    void assignElementToCell(msize aIdx, const T& aVal)
    {
      if(aIdx >= size())
      {
        throw std::out_of_range("Array out_of_range exception");
      }
      
      this->operator()(aIdx) = aVal;
    }//assignDomainElementToCell
    
  private:
    std::shared_ptr<std::vector<T>> pArray;
  };

  template<typename T>
  class DATA_STRUCTURE_EXPORT_CLASS Array<T*> {
  public:
    typedef std::size_t msize;
    
    Array(msize aSize)
    : pArray(std::make_shared<std::vector<T*>>(aSize, nullptr))
    {
    }
    
    ~Array() = default;
    
    bool empty() const
    {
      for(auto ptr : *pArray)
      {
        if(!ptr)
        {
          return false;
        }
      }
      return true;
    }//empty
    
    /// Returns the size of this array
    inline msize size() const
    {
      return pArray->size();
    }
    
    /// Clears the content of the matrix
    inline void clear()
    {
      msize numElem = size();
      pArray->clear();
      pArray = std::make_shared<std::vector<T*>>(numElem, nullptr);
    }
    
    ///	Returns a const reference of the i-th element of the array
    const T* operator()(msize aIdx) const
    {
      return pArray->operator[](aIdx);
    }
    
    ///	Returns a reference of the i-th element of the array
    T* operator()(msize aIdx)
    {
      return pArray->operator[](aIdx);
    }
    
    /// Returns the index idx such that
    /// (*this)(idx) is equal to (the first) "aVal".
    /// Returns size() if "aVal" is not present
    msize find(const T* aVal) const
    {
      for(msize it = 0; it < size(); ++it)
      {
        if(this->operator()(it) == aVal)
        {
          return it;
        }
      }
      
      return size();
    }
    
    ///  Assigns "aElement" to the array cell ["aCell"].
    /// @note throws out of range exception if "aCell" does not match array dimensions
    void assignElementToCell(msize aIdx, T* aVal)
    {
      if(aIdx >= size())
      {
        throw std::out_of_range("Array out_of_range exception");
      }
      
      (*pArray)[aIdx] = aVal;
    }//assignDomainElementToCell
    
  private:
    std::shared_ptr<std::vector<T*>> pArray;
  };
  
}}// end namespace DataStructure/Matrix
