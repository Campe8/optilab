//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/13/2017
//
// This class implements a data structure for a matrix.
//

#pragma once

#include "DataStructureExportDefs.hpp"

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>

#include <memory>
#include <exception>

namespace DataStructure { namespace Matrix {

  template<typename T>
  class DATA_STRUCTURE_EXPORT_CLASS Matrix {
  public:
    typedef std::size_t msize;
    
    Matrix(msize aNumRows, msize aNumCols)
    : pMatrix(std::make_shared<boost::numeric::ublas::matrix<T>>(aNumRows, aNumCols))
    {
    }
    
    ~Matrix() = default;
    
    /// Clears the content of the matrix
    inline void clear()
    {
      pMatrix->clear();
    }
    
    /// Returns the (total) number of elements contained in the matrix
    inline msize size() const
    {
      return numRows() * numCols();
    }
    
    /// Returns the number of rows
    inline msize numRows() const
    {
      return pMatrix->size1();
    }
    
    /// Returns the number of columns
    inline msize numCols() const
    {
      return pMatrix->size2();
    }
    
    ///	Returns a const reference of the j-th element in the i-th row
    const T& operator()(msize aRow, msize aCol) const
    {
      return pMatrix->operator()(aRow, aCol);
    }
    
    ///	Returns a reference of the j-th element in the i-th row
    T& operator()(msize aRow, msize aCol)
    {
      return pMatrix->operator()(aRow, aCol);
    }
    
    /// Returns a pair <row, column> such that
    /// (*this)(row, column) is equal to (the first) "aVal".
    /// Returns <numRows, numCols> if "aVal" is not present
    std::pair<msize, msize> find(const T& aVal) const
    {
      for(auto itr = 0; itr < numRows(); ++itr)
      {
        for(auto itc = 0; itc < numCols(); ++itc)
        {
          if(this->operator()(itr, itc) == aVal)
          {
            return std::make_pair(itr, itc);
          }
        }//for itc
      }//for itr
      
      return std::make_pair(numRows(), numCols());
    }
    
    /// Assigns "aVal" to the matrix cell ["aRow", "aCol"].
    /// @note throws out of range exception if "aRowIdx" or "aColIdx" do not match matrix dimensions
    void assignElementToCell(msize aRow, msize aCol, const T& aVal)
    {
      if(aRow >= numRows() || aCol >= numCols())
      {
        throw std::out_of_range("Matrix out_of_range exception");
      }
      
      this->operator()(aRow, aCol) = aVal;
    }//assignDomainElementToCell
    
  private:
    std::shared_ptr<boost::numeric::ublas::matrix<T>> pMatrix;
  };

}}// end namespace DataStructure/Matrix
