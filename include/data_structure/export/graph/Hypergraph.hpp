//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 04/27/2017
//
// This class is a data structure for an hypergraph.
// An hypergraph is a pair G = (V,E) where
// a) V is a set of elements called nodes;
// b) E is a set of non-empty subsets of V called hyperedges or edges.
// For more information about hypergraphs, see
// https://en.wikipedia.org/wiki/Hypergraph
//

#pragma once

#include "DataStructureExportDefs.hpp"

// UUID tags
#include "BaseUtils.hpp"

#include <iostream>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <memory>
#include <cassert>

// Forward declarations
namespace DataStructure { namespace Graph {
    class NodeCore;
    
    typedef std::shared_ptr<NodeCore> NodeCoreSPtr;
}}// end namespace DataStructure/Graph

namespace DataStructure { namespace Graph {
  
  class DATA_STRUCTURE_EXPORT_CLASS NodeCore {
  public:
    typedef UUIDTag NodeCoreId;
    
    virtual ~NodeCore() = default;
    
    /// Basic class that represents the core
    /// of a node in the hypergraph
    virtual NodeCoreId getId() const = 0;
  };
  
  class DATA_STRUCTURE_EXPORT_CLASS Hypergraph {
  public:
    typedef std::size_t GraphId;
    typedef GraphId NodeId;
    typedef GraphId EdgeId;
    
    typedef std::pair<EdgeId, std::map<NodeCore::NodeCoreId, NodeId>> EdgeDescriptor;
    
  private:
    class Node {
    public:
      Node(const NodeCoreSPtr& aNodeCore, NodeId aNodeId);
      
      ~Node() = default;
      
      inline NodeId getNodeId() const
      {
        return pNodeId;
      }
      
      inline NodeCoreSPtr getCore() const
      {
        return pNodeCore;
      }
      
      inline NodeCore::NodeCoreId getId() const
      {
        return pNodeCore->getId();
      }
      
      /// Links this node with the edge "aEdgeId"
      inline void connectToEdge(EdgeId aEdgeId)
      {
        pEdgeSet.insert(aEdgeId);
      }
      
      inline const std::unordered_set<EdgeId>& getIncidentEdges() const
      {
        return pEdgeSet;
      }
      
    private:
      /// Unique Id for this node
      NodeId pNodeId;
      
      /// Node core object hold by
      /// this graph node
      NodeCoreSPtr pNodeCore;
      
      /// Set of edges this node is connected to
      /// Set of nodes this edge is connected to
      std::unordered_set<EdgeId> pEdgeSet;
    };
    
    class Edge {
    public:
      Edge(EdgeId aEdgeId);
      
      ~Edge() = default;
      
      inline EdgeId getEdgeId() const
      {
        return pEdgeId;
      }
      
      /// Links this edge to the node with id "aNodeId"
      /// in the graph "aGraph"
      void addNode(Hypergraph* aGraph, NodeId aNodeId);
      
      inline const std::unordered_set<NodeId>& getLinkedNodes() const
      {
        return pNodeSet;
      }
      
    private:
      /// Unique Id for this edge
      EdgeId pEdgeId;
      
      /// Set of nodes this edge is connected to
      std::unordered_set<NodeId> pNodeSet;
    };
    
  public:
    Hypergraph() = default;
    
    ~Hypergraph() = default;
    
    /// Returns the node core of the node
    /// identified by "aNodeId"
    inline NodeCoreSPtr getNodeCore(NodeId aNodeId) const
    {
      assert(pNodeSet.find(aNodeId) != pNodeSet.end());
      return pNodeSet.at(aNodeId)->getCore();
    }
    
    /// Adds an edge to the hypergraph.
    /// For each NodeCore pointer in the set, it creates a new
    /// graph node and it links it to the edge.
    /// Returns a pair where the first element is the ID
    /// of the edge while the second is the map between UUIDs
    /// and node ids.
    /// @note if a NodeType is already present in the graph,
    /// all connections are automatically updated and
    /// the returned node id is the same.
    /// @note "aNodeTypeSet" MUST NOT be empty NOR contain nullptr(s)
    EdgeDescriptor addEdge(const std::set<NodeCoreSPtr>& aNodeTypeSet);
    
    /// Returns true if the two nodes "aNodeU" and "aNodeV" are connected,
    /// false otherwise
    inline bool connectedNodes(const NodeCoreSPtr& aNodeU, const NodeCoreSPtr& aNodeV)
    {
      assert(aNodeU);
      assert(aNodeV);
      return connectedNodes(pUUIDNodeSet.at(aNodeU->getId()), pUUIDNodeSet.at(aNodeU->getId()));
    }
    inline bool connectedNodes(NodeId aNodeU, NodeId aNodeV)
    {
      std::unordered_set<NodeId> visitedNodes;
      return connectedNodesRec(aNodeU, aNodeV, &visitedNodes);
    }
    
  protected:
    /// Returns a new GraphId
    inline GraphId generateId()
    {
      static GraphId pGraphId = 0;
      return pGraphId++;
    }
    
    /// Returns the pointer to the node "aNodeId"
    /// or nullptr if no such node exists
    Node* getNode(NodeId aNodeId) const;
    
  private:
    /// Set of all the nodes in the graph mapped to their NodeId
    std::unordered_map<NodeId, std::shared_ptr<Node>> pNodeSet;
    
    /// Set of all the nodes in the graph mapped to their UUID
    std::unordered_map<NodeCore::NodeCoreId, NodeId>  pUUIDNodeSet;
    
    /// Set of all the edges in the graph mapped to their EdgeId
    std::unordered_map<EdgeId, std::shared_ptr<Edge>> pEdgeSet;
    
    /// Returns true if the two nodes "aNodeU" and "aNodeV" are connected,
    /// false otherwise
    bool connectedNodesRec(NodeId aNodeU, NodeId aNodeV, std::unordered_set<NodeId>* aVisitedNodes);
  };
  
}}// end namespace DataStructure/graph
