//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/11/2017
//
// This class implements an iterator for the (n) tree.
//

#pragma once

#include "DataStructureExportDefs.hpp"

#include <boost/iterator/iterator_facade.hpp>

#include <vector>
#include <stack>
#include <memory>

namespace DataStructure { namespace Graph { namespace Tree {
  
	namespace _private {

		template<typename NodeType>
		class DATA_STRUCTURE_EXPORT_CLASS NodeLeftIterator : public boost::iterator_facade<NodeLeftIterator<NodeType>, NodeType, boost::forward_traversal_tag>
		{
			/**
			 * Wrapper class for iterators on node tree objects.
			 * @Note this iterator is a "left-to-right" iterator.
			 */

		private:
			// Typedef types (not objects inside classes) before
			// being used in constructors and methods
			typedef typename NodeType::NodeContainer NodeContainerType;
			typedef typename NodeContainerType::iterator IterType;
			typedef NodeLeftIterator<NodeType> SelfType;

			friend class IteratorUtility;

		public:
			NodeLeftIterator() {
				pIt = pEnd;
			}

			explicit NodeLeftIterator(const IterType& aBegin, const IterType& aEnd)
			{
				pIt = aBegin;
				pEnd = aEnd;
			}

		private:
			friend class boost::iterator_core_access;

			/// Iterator on NodeContainer
			IterType pIt;

			/// End iterator
			IterType pEnd;

			/*
			 * Dereference, equal, and increment are needed
			 * by the iterator_facade class instance
			 */
			inline bool equal(SelfType const& aOther) const
			{
				return aOther.pIt == pIt;
			}

			inline NodeType& dereference() const
			{
				return *pIt;
			}

			inline void increment()
			{
				++pIt;
			}

			/// Returns internal iterator
			inline IterType iter()
			{
				return pIt;
			}

		};

		template<typename NodeType>
		class DATA_STRUCTURE_EXPORT_CLASS NodeRightIterator : public boost::iterator_facade<NodeRightIterator<NodeType>, NodeType, boost::forward_traversal_tag>
		{
			/**
			* Wrapper class for iterators on node tree objects.
			* @Note this iterator is a "right-to-left" iterator.
			*/

		private:
			// Typedef types (not objects inside classes) before
			// being used in constructors and methods
			typedef typename NodeType::NodeContainer NodeContainerType;
			typedef typename NodeContainerType::reverse_iterator IterType;
			typedef NodeRightIterator<NodeType> SelfType;

			friend class IteratorUtility;

		public:
			NodeRightIterator() {
				pIt = pEnd;
			}

			explicit NodeRightIterator(const IterType& aBegin, const IterType& aEnd)
			{
				pIt = aBegin;
				pEnd = aEnd;
			}

		private:
			friend class boost::iterator_core_access;

			/// Iterator on NodeContainer
			IterType pIt;

			/// End iterator
			IterType pEnd;

			/*
			* Dereference, equal, and increment are needed
			* by the iterator_facade class instance
			*/
			bool equal(const SelfType& aOther) const
			{
				return aOther.pIt == pIt;
			}

			NodeType& dereference() const
			{
				return *pIt;
			}

			void increment()
			{
				++pIt;
			}

			/// Returns internal iterator
			inline IterType iter()
			{
				return pIt;
			}

		};

		class DATA_STRUCTURE_EXPORT_CLASS IteratorUtility {
		public:
			template<typename Iterator>
			static typename Iterator::IterType iter(Iterator aIter) {
				return aIter.iter();
			}
		};

	}// end namespace _private

}}}// end namespace DataStructure/graph/tree
