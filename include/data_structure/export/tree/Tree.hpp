//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/11/2017
//
// This class implements a data structure for an (n) tree.
//

#pragma once

#include "DataStructureExportDefs.hpp"

// Tree node
#include "TreeNode.hpp"

// Iterator on the tree
#include "PreOrderTreeIterator.hpp"

#include <vector>
#include <memory>

namespace DataStructure { namespace Graph { namespace Tree {
  
  template<typename NodeType>
  class DATA_STRUCTURE_EXPORT_CLASS NTree {
  public:
    typedef NodeType Node;
    typedef typename Node::NodeHandle NodeHandle;
    
    typedef _private::PreOrderIterator<Node> PreOrderIter;
    
    NTree()
    : pRoot(nullptr)
    {
    }
    
    NTree(const std::shared_ptr<Node>& aNode)
    {
      pRoot = aNode;
    }
    
    NTree(const Node& aNode)
    {
      pRoot = std::make_shared<Node>(aNode);
    }
    
    ~NTree() = default;
    
    /// Clears the tree
    void clear()
    {
      root().clear();
    }
    
    Node& root()
    {
      return *pRoot;
    }
    
    const Node& root() const
    {
      return *pRoot;
    }
    
    bool isRoot(const NodeHandle& aNodeHandle) const
    {
      return aNodeHandle == root().handle();
    }
    
    PreOrderIter preOrderBegin()
    {
      PreOrderIter it(root());
      return it;
    }
    
    PreOrderIter preOrderEnd()
    {
      PreOrderIter it;
      return it;
    }
    
  private:
    /// Root of the tree
    std::shared_ptr<Node> pRoot;
  };
  
}}}// end namespace DataStructure/graph/tree
