//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/11/2017
//
// This class implements an iterator for the (n) tree.
//

#pragma once

#include "DataStructureExportDefs.hpp"

#include <boost/iterator/iterator_facade.hpp>

#include <vector>
#include <stack>
#include <memory>

namespace DataStructure { namespace Graph { namespace Tree {
  
	namespace _private {

		template<typename NodeType>
		class DATA_STRUCTURE_EXPORT_CLASS PreOrderIterator : public boost::iterator_facade<PreOrderIterator<NodeType>, NodeType, boost::forward_traversal_tag> {
		private:
			typedef std::reference_wrapper<NodeType> NodeRefWrapper;
			typedef std::stack<NodeRefWrapper, std::vector<NodeRefWrapper>> Stack;

		public:
			PreOrderIterator() = default;

			PreOrderIterator(NodeType& aRoot)
			{
				pNode = std::make_shared<NodeType>(aRoot);
				pStack = std::make_shared<Stack>();
				stack().push(aRoot);
			}

			PreOrderIterator(const PreOrderIterator& aPreOrderIter)
			{
				pNode = aPreOrderIter.pNode;
				pStack = aPreOrderIter.pStack;
			}

		private:
			friend class boost::iterator_core_access;

			/// Pointer to current node
			std::shared_ptr<NodeType> pNode;

			/// Stack used for pre-order visit
			std::shared_ptr<Stack> pStack;

			/*
			 * Dereference, equal, and increment are needed
			 * by the iterator_facade class instance
			 */

			NodeType& dereference() const
			{
				return currNode();
			}

			bool equal(const PreOrderIterator<NodeType>& aIt) const
			{
				return aIt.pNode == pNode ? true : false;
			}

			/*
			 * Pre-order visit:
			 * visit(node)
			 *	stack.clear();
			 *	while (!stack.empty() || node != NULL)
			 *		if(node != null)
			 *			do_something(node);
			 *      if(node.right != null)
			 *				stack.push(node.right)
			 *      endif
			 *      node = node.left;
			 *    else
			 *			node = stack.top()
			 *      stack.pop()
			 *		endif
			 *	endwhile
			 */
			void increment()
			{
				if (stack().empty())
				{
					pNode = nullptr;
					return;
				}

				// Top of stack has current node
				// and vice-versa, current node is always
				// the top of the stack
				stack().pop();
				auto it = currNode().rbegin();
				for (; it != currNode().rend(); ++it)
				{
					stack().push(*it);
				}

				if (!stack().empty())
				{
					currNode() = stack().top();
				}
				else
				{
					pNode = nullptr;
				}
			}//increment

			/// Utility function: returns the reference to the stack
			inline Stack& stack()
			{
				return *pStack;
			}

			/// Utility function: returns the reference to the current node
			inline NodeType& currNode() const
			{
				return *pNode;
			}

		};

	}
}}}// end namespace DataStructure/graph/tree
