//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/10/2017
//
// This class implements a generic template node
// for a n-tree data structure.
//

#pragma once

#include "DataStructureExportDefs.hpp"

// Iterator on the tree
#include "TreeIterator.hpp"

#include <vector>
#include <memory>
#include <cassert>


namespace DataStructure { namespace Graph { namespace Tree {
  
  namespace _private {
    
    template<class NodeType>
    class DATA_STRUCTURE_EXPORT_CLASS NodeHandleImpl {
    public:
      NodeHandleImpl(const NodeType* const aPtr = nullptr)
      : pHandle(const_cast<const NodeType*>(aPtr))
      {
      }
      
      NodeHandleImpl(const NodeHandleImpl<NodeType>& aNode)
      : pHandle(aNode.pHandle)
      {
      }
      
      NodeHandleImpl(const NodeType& aNode)
      : pHandle(aNode.handle().pHandle)
      {
      }
      
      ~NodeHandleImpl()
      {
        pHandle = nullptr;
      }
      
      bool operator==(const NodeHandleImpl<NodeType>& aNode) const
      {
        return aNode.pHandle == pHandle;
      }
      
      bool operator==(const NodeType& aNode) const
      {
        return aNode.handle().pHande == pHandle;
      }
      
      NodeHandleImpl<NodeType>& operator=(const NodeHandleImpl<NodeType>& aNode)
      {
        pHandle = aNode.pHandle;
        return *this;
      }
      
      operator bool() const
      {
        return pHandle ? true : false;
      }
      
    private:
      friend class NodeUtility;
      
      /// Actual pointer to handle
      const NodeType* pHandle;
      
      /// Returns the handled pointer
      const NodeType* const pointer() const
      {
        return const_cast<const NodeType* const>(pHandle);
      }
    };
    
    class DATA_STRUCTURE_EXPORT_CLASS NodeUtility {
    public:
      /*
       * Utility class for retrieving
       * the handled pointer from a node handle.
       */
      template<typename NodeType>
      static const NodeType* const getNodeInternal(const _private::NodeHandleImpl<NodeType>& aNodeHandle)
      {
        return const_cast<const NodeType* const>(aNodeHandle.pHandle);
      }
    };
    
  }// end namespace _private
  
  // Node Handle
  template<class NodeType>
  class DATA_STRUCTURE_EXPORT_CLASS NodeHandleWrapper : public _private::NodeHandleImpl<NodeType> {
  public:
    /*
     * Node handle class: acts as a smart point around
     * raw node pointers.
     * Wrapper around NodeHandleImpl.
     */
    NodeHandleWrapper(const NodeType* const aPtr = nullptr)
    :  _private::NodeHandleImpl<NodeType>(aPtr)
    {
    }
    
    NodeHandleWrapper(const _private::NodeHandleImpl<NodeType>& aNode)
    : _private::NodeHandleImpl<NodeType>(aNode)
    {
    }
    
    NodeHandleWrapper(const NodeType& aNode)
    : _private::NodeHandleImpl<NodeType>(aNode)
    {
    }
  };
  
  template<typename NodeDataType>
  class DATA_STRUCTURE_EXPORT_CLASS TreeNode {
  private:
    typedef TreeNode<NodeDataType> NodeType;
    typedef std::vector<TreeNode<NodeDataType>> NodeContainer;
    
  public:
    /**
     * Class representing a Node in the Tree.
     * The tree is a (root) node itself with sub-trees
     * as children.
     */
    typedef TreeNode<NodeDataType>& NodeRef;
    typedef const TreeNode<NodeDataType>& ConstNodeRef;
    typedef NodeHandleWrapper<TreeNode<NodeDataType>> NodeHandle;
    typedef _private::NodeLeftIterator<TreeNode<NodeDataType>> NodeIter;
    typedef _private::NodeRightIterator<TreeNode<NodeDataType>> NodeRIter;
    
    TreeNode(const NodeHandle& aParent = NodeHandle())
    : pParent(aParent)
    , pChildren(std::make_shared<NodeContainer>())
    {
    }
    
    TreeNode(const NodeDataType& aData, const NodeHandle& aParent = NodeHandle())
    : pData(aData)
    , pParent(aParent)
    , pChildren(std::make_shared<NodeContainer>())
    {
    }
    
    TreeNode(const TreeNode& aNode)
    {
      pParent = aNode.pParent;
      pData = aNode.pData;
      pChildren = aNode.pChildren;
    }
    
    ~TreeNode() = default;
    
    bool operator==(ConstNodeRef aOther) const
    {
      if ((pParent == aOther.pParent) && (pData == aOther.pData) && (pChildren == aOther.pChildren))
      {
        return true;
      }
      return false;
    }
    
    /// Access to children by index
    NodeRef operator[](const std::size_t aIdx)
    {
      return children().at(aIdx);
    }
    
    /// Clears the tree.
    /// @note it does not destroy data()
    void clear()
    {
      removeChildren();
      pParent = nullptr;
    }
    
    /// Returns parent node
    inline NodeHandle& parentHandle()
    {
      return pParent;
    }
    
    inline const NodeHandle& parentHandle() const
    {
      return pParent;
    }
    
    inline const NodeType& parent()
    {
      return *(_private::NodeUtility::getNodeInternal(parentHandle()));
    }
    
    NodeHandle handle() const
    {
      NodeHandle ret(this);
      return ret;
    }
    
    /// Returns true if this node is a root node,
    /// false otherwise
    inline bool isRoot() const
    {
      return pParent == nullptr;
    }
    
    /// Returns true if this node is a leaf node,
    /// false otherwise
    inline bool isLeaf() const
    {
      return !pChildren  || children().empty();
    }
    
    /// Returns data
    inline NodeDataType& data()
    {
      return pData;
    }
    
    /// Returns data
    inline const NodeDataType& data() const
    {
      return pData;
    }
    
    /// Returns true if this Node contains "aNodeVal",
    /// false otherwise
    bool contains(const NodeDataType& aNodeVal)
    {
      if (pData == aNodeVal)
      {
        return true;
      }
      
      for (auto& child : children())
      {
        if (child.contains(aNodeVal))
        {
          return true;
        }
      }
      return false;
    }//contains
    
    /// Returns the number of children in this node
    inline std::size_t numChildren() const
    {
      if (!pChildren)
      {
        return 0;
      }
      return children().size();
    }
    
    /// Replaces child ad position "aIdx" which must be less than or equal
    /// the number of children.
    /// If "aIdx" is equal the number of children, adds "aNodeVal"
    void replaceChild(const NodeDataType& aNodeVal, std::size_t aIdx)
    {
      assert(aIdx <= numChildren());
      if (aIdx == numChildren())
      {
        addChild(aNodeVal);
        return;
      }
      
      // aIdx < numChildren
      const TreeNode<NodeDataType> node(aNodeVal, handle());
      children()[aIdx] = node;
    }
    
    /// Replaces child ad position "aIdx" which must be less than or equal
    /// the number of children.
    /// If "aIdx" is equal the number of children, adds "aNodeVal"
    void replaceChild(NodeRef aNode, std::size_t aIdx)
    {
      assert(aIdx <= numChildren());
      if (aIdx == numChildren())
      {
        addChild(aNode);
        return;
      }
      
      // aIdx < numChildren
      aNode.pParent = handle();
      children()[aIdx] = aNode;
    }
    
    /// Replaces child ad position "aIdx" which must be less than or equal
    /// the number of children.
    /// If "aIdx" is equal the number of children, adds "aNodeVal"
    void replaceChild(ConstNodeRef aNode, std::size_t aIdx)
    {
      assert(aIdx <= numChildren());
      if (aIdx == numChildren())
      {
        addChild(aNode);
        return;
      }
      
      // aIdx < numChildren
      aNode.pParent = handle();
      children()[aIdx] = aNode;
    }
    
    /// Adds a node as a child given its value
    /// @note parent will be automatically set to this node
    void addChild(const NodeDataType& aNodeVal)
    {
      const TreeNode<NodeDataType> node(aNodeVal, handle());
      children().push_back(node);
    }
    
    /// Adds a node as a child
    /// @note parent will be automatically set to this node
    void addChild(NodeRef aNode)
    {
      
      aNode.pParent = handle();
      children().push_back(aNode);
    }
    
    /// Adds a node as a child
    /// @note parent will be automatically set to this node
    void addChild(ConstNodeRef aNode)
    {
      aNode.pParent = handle();
      children().push_back(aNode);
    }
    
    /// Removes a child.
    /// @note "aIt" must be valid and dereferenceable, i.e., end() is valid
    /// but not dereferenceable and it cannot be used here
    void removeChild(const NodeIter& aIt)
    {
      children().erase(_private::IteratorUtility::iter(aIt));
    }
    
    ///  Removes all children
    void removeChildren()
    {
      children().clear();
    }
    
    /// Range-based for loop - begin
    NodeIter begin() {
      NodeIter it(children().begin(), children().end());
      return it;
    }
    
    /// Range-based for loop - end
    NodeIter end() {
      NodeIter it(children().end(), children().end());
      return it;
    }
    
    /// Range-based for loop - rbegin
    NodeRIter rbegin() {
      NodeRIter it(children().rbegin(), children().rend());
      return it;
    }
    
    /// Range-based for loop - rend
    NodeRIter rend() {
      NodeRIter it(children().rend(), children().rend());
      return it;
    }
    
  private:
    /// Iterator is friend of node
    friend NodeIter;
    friend NodeRIter;
    
    /// Actual data contained in this node
    NodeDataType pData;
    
    /// Container for children nodes
    std::shared_ptr<NodeContainer> pChildren;
    
    /// Pointer to parent node
    NodeHandle pParent;
    
    /// Returns the children container
    inline NodeContainer& children()
    {
      return *pChildren;
    }
    
    inline const NodeContainer& children() const
    {
      return *pChildren;
    }
  };
  
}}}// end namespace DataStructure/graph/tree
