EXPORT_HEADERS += map/Bimap.hpp
EXPORT_HEADERS += graph/Hypergraph.hpp
EXPORT_HEADERS += matrix/Array.hpp
EXPORT_HEADERS += matrix/Matrix.hpp
EXPORT_HEADERS += tree/PreOrderTreeIterator.hpp
EXPORT_HEADERS += tree/Tree.hpp
EXPORT_HEADERS += tree/TreeIterator.hpp
EXPORT_HEADERS += tree/TreeNode.hpp
