//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/09/2017
//
// This class implements a data structure for a bimap.
//

#pragma once

#include "DataStructureExportDefs.hpp"

#include <memory>  // for std::shared_ptr
#include <string>

#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>

namespace DataStructure {
  
  class DATA_STRUCTURE_EXPORT_CLASS JSON {
  public:
    using SPtr = std::shared_ptr<JSON>;
    
    /// General JSON object
    using Object = rapidjson::Value;
    using ObjectSPtr = std::shared_ptr<Object>;
    
  public:
    /// Default constructor, creates an empty json object
    JSON();
    
    /// Creates a new JSON object from the given string in JSON format.
    /// @throw std::invalid_argument if the given string is not a valid JSON string
    JSON(const std::string& json);
    
    template<typename T>
    void addValue(const std::string& name, T& value)
    {
      pDocument.AddMember(Object(name.c_str(), getAllocator()), value, getAllocator());
    }
    
    /// Returns an array object
    inline JSON::Object createArray() { return JSON::Object(rapidjson::kArrayType); }
    
    /// Adds the given object to the given array
    inline void addObjToArray(JSON::Object& array, JSON::Object& object)
    {
      array.PushBack(object, getAllocator());
    }
    
    /// Returns a generic object
    inline JSON::Object createObject()
    {
      return JSON::Object(rapidjson::kObjectType);
    }
    
    /// Adds an object "obj2" with key "key" as child of another generic object "obj1"
    inline void addObjToObject(const std::string& key, JSON::Object& obj1, JSON::Object& obj2)
    {
      obj1.AddMember(Object(key.c_str(), getAllocator()), obj2, getAllocator());
    }
    
    /// Returns a general object of type T
    template<typename T>
    JSON::Object createObject(const T& value)
    {
      return JSON::Object(value);
    }
    
    /// Returns true if this JSON object has and object with given key.
    /// Returns false otherwise.
    inline bool hasObject(const std::string& key)
    {
      return pDocument.HasMember(key.c_str());
    }
    
    /// Returns the object with given key
    JSON::Object& getObject(const std::string& key);
    
    /// Returns the string representation of this JSON object
    std::string toString() const;
    
  private:
    /// Document representing the main JSON object
    rapidjson::Document pDocument;
    
    /// Utility function: returns the allocator for this root object
    inline rapidjson::Document::AllocatorType& getAllocator() { return pDocument.GetAllocator(); }
  };

  template<>
  void JSON::addValue<std::string>(const std::string& name, std::string& value);
  
  template<>
  JSON::Object JSON::createObject<std::string>(const std::string& value);
  
}//end namespace DataStructure
