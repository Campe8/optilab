//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 10/10/2018
//
// This class implements a data structure for a
// heterogeneous unordered map.
//

#pragma once

#include "DataStructureExportDefs.hpp"

#include <cassert>
#include <typeinfo>  // for std::type_info
#include <unordered_map>

#include <boost/any.hpp>

namespace DataStructure { namespace Map {

  template<typename T>
  class DATA_STRUCTURE_EXPORT_CLASS Hetermap {
  public:
    using any_t  = boost::any;
    using hmap_t = std::unordered_map<T, any_t>;
    using hmap_iter = typename hmap_t::iterator;
    
  public:
    Hetermap() = default;
    ~Hetermap() = default;
    
    inline std::size_t size() const { return pInternalMap.size(); }
    
    inline void clear() { pInternalMap.clear(); }
    
    inline hmap_iter find(const T& aKey) { return pInternalMap.find(aKey); }
    
    inline bool isRegistered(const T& aKey) { return find(aKey) != pInternalMap.end(); }
    
    any_t lookup(const T& aKey) const { return pInternalMap.at(aKey); }
    
    /// Looks up the key and converts it to the given type U.
    /// @note throws std::out_of_range if the there is no value registered
    /// with given key.
    /// @note throws boost::bad_any_cast if U does not match the actual
    /// type of the stored value
    template<typename U>
    U lookupValue(const T& aKey) const { return boost::any_cast<U>(lookup(aKey)); }
    
    template<typename U>
    void insert(T aKey, const U& aVal) { pInternalMap[aKey] = aVal; }
    
    template<typename U>
    bool isType(T aKey)
    {
      if(!isRegistered(aKey)) return false;
      return typeid(U).name() == lookup(aKey).type().name();
    }
    
    void erase(const T& aKey)
    {
      if(!isRegistered(aKey)) return;
      pInternalMap.erase(aKey);
    }
    
    /// Iterators
    inline hmap_iter begin() { return pInternalMap.begin(); }
    inline hmap_iter end()   { return pInternalMap.end();   }
    
  private:
    /// Internal unordered map using
    /// boost::any as value for any key type T
    hmap_t pInternalMap;
  };
  
}}//end namespace Map/DataStructure
