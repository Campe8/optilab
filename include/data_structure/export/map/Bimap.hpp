//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/09/2017
//
// This class implements a data structure for a bimap.
//

#pragma once

#include "DataStructureExportDefs.hpp"

#include <unordered_map>

#include <cassert>

namespace DataStructure {
  namespace Map {

  template<typename T, typename U>
  class DATA_STRUCTURE_EXPORT_CLASS Bimap {
  public:
    using LMap = std::unordered_map<T, U>;
    using RMap = std::unordered_map<U, T>;
    using LMapIter = typename LMap::iterator;
    using RMapIter = typename RMap::iterator;
    using LMapCIter = typename LMap::const_iterator;
    using RMapCIter = typename RMap::const_iterator;
    
  public:
    Bimap() = default;
    ~Bimap() = default;
    
    void clear()
    {
      pLMap.clear();
      pRMap.clear();
    }
    
    T lookup(const U& aVal) const
    {
      return pRMap.at(aVal);
    }
    
    U lookup(const T& aKey) const
    {
      return pLMap.at(aKey);
    }
    
    void insert(T aKey, U aVal)
    {
      pLMap[aKey] = aVal;
      pRMap[aVal] = aKey;
    }
    
    void insert(U aVal, T aKey)
    {
      insert(aKey, aVal);
    }
    
    inline void erase(const T& aKey)
    {
      if(pLMap.find(aKey) == pLMap.end())
      {
        return;
      }
      U val = pLMap[aKey];
      assert(pRMap.find(val) != pRMap.end());
      
      pLMap.erase(aKey);
      pRMap.erase(val);
    }
    
    inline void erase(const U& aVal)
    {
      if(pRMap.find(aVal) == pRMap.end())
      {
        return;
      }
      T key = pRMap[aVal];
      assert(pLMap.find(key) != pLMap.end());
      
      pLMap.erase(key);
      pRMap.erase(aVal);
    }
    
    inline LMapIter endLeft()
    {
      return pLMap.end();
    }
    
    inline LMapCIter endLeft() const
    {
      return pLMap.end();
    }
    
    inline LMapIter find(const T& aKey)
    {
      return pLMap.find(aKey);
    }
    
    inline LMapCIter find(const T& aKey) const
    {
      return pLMap.find(aKey);
    }
    
    inline RMapIter endRight()
    {
      return pRMap.end();
    }
    
    inline RMapCIter endRight() const
    {
      return pRMap.end();
    }
    
    inline RMapIter find(const U& aVal)
    {
      return pRMap.find(aVal);
    }
    
    inline RMapCIter find(const U& aVal) const
    {
      return pRMap.find(aVal);
    }

    
  private:
    /// Left map
    LMap pLMap;
    
    /// Right map
    RMap pRMap;
  };
    
  }//end namespace Map
}//end namespace DataStructure
