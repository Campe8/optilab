//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 01/30/2017
//
// Register for branching strategies.
//

#pragma once

#include "SearchExportDefs.hpp"
#include "BranchingStrategyDefs.hpp"

// Branching strategy
#include "BranchingStrategy.hpp"

// Branching constraint factory
#include "BranchingConstraintFactory.hpp"

#include <unordered_map>

// Forward declarations
namespace Search {
  class BranchingStrategyRegister;
  
  typedef BranchingStrategyRegister* BranchingStrategyRegisterPtr;
}// end namespace Search

namespace Search {

  class SEARCH_EXPORT_CLASS BranchingStrategyRegister {
  public:
    BranchingStrategyRegister();

    virtual ~BranchingStrategyRegister();

    /// Returns an instance of branching strategy of type "aBrcStrType"
    BranchingStrategyPtr branchingStrategy(BranchingStrategyType aBrcStrType);

  private:
    /// Map of the registered strategies
    std::unordered_map<int, BranchingStrategyUPtr> pBranchingStrategyRegister;

    /// Factory for branching constraints
    Core::BranchingConstraintFactoryUPtr pBranchingConstraintFactory;
    
    /// Register branching strategies on initialization
    void registerStrategies();
  };

}// end namespace Search
