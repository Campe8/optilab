//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 01/27/2017
//
// Domain splitting branching strategy:
// the domain of the branching variable is split
// into two subdomains.
// For example, if the value 10 is chosen for
// the branching variable "x", then two branches
// are generated: x <= 10 and x > 10.
//

#pragma once

// Base class 
#include "BranchingStrategy.hpp"

// Branching constraint factory
#include "BranchingConstraintFactory.hpp"

namespace Search {

  class SEARCH_EXPORT_CLASS BranchingStrategyDomainSplitting : public BranchingStrategy {
  public:
    BranchingStrategyDomainSplitting(Core::BranchingConstraintFactoryPtr aBCFactory);

    virtual ~BranchingStrategyDomainSplitting();

    /// Returns the branching decision set, i.e., a pair consisting 
    /// of the branching variable and the ordered vector of 
    /// branching constraints on the branching variable.
    /// @note the vector is ordered with the left-most branch being 
    /// the most promising branch
    BranchingChoiceSet getBranchingSet(SearchHeuristicPtr aSearchHeuristic) const override;
  };

}// end namespace Search
