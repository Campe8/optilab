//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 01/26/2017
//
// Base class for branching strategies.
// A branching strategy is the strategy used by
// a search engine to branch on nodes of the
// search tree.
//

#pragma once

#include "SearchExportDefs.hpp"
#include "SearchHeuristicDefs.hpp"
#include "BranchingStrategyDefs.hpp"

#include "BranchingConstraintFactory.hpp"

// Heuristic
#include "SearchHeuristic.hpp"

// Variable
#include "Variable.hpp"

// Constraint
#include "Constraint.hpp"

#include <vector>
#include <utility>

namespace Search {
  class BranchingStrategy;
  class BranchingConstraint;
  
  typedef BranchingStrategy* BranchingStrategyPtr;
  typedef std::unique_ptr<BranchingStrategy> BranchingStrategyUPtr;
  typedef std::shared_ptr<BranchingStrategy> BranchingStrategySPtr;
  
  typedef std::pair<Core::VariableSPtr, std::vector<Core::BranchingConstraintSPtr>> BranchingChoiceSet;
}// end namespace Search

namespace Search {
  
  class SEARCH_EXPORT_CLASS BranchingStrategy {
  public:
    virtual ~BranchingStrategy();
    
    inline BranchingStrategyType getBranchingStrategyType() const
    {
      return pBranchingStrategyType;
    }
    
    /// Returns the branching decision set, i.e., a pair consisting
    /// of the branching variable and the ordered vector of
    /// branching constraints on the branching variable.
    /// @note the vector is ordered with the left-most branch being
    /// the most promising branch
    virtual BranchingChoiceSet getBranchingSet(SearchHeuristicPtr aSearchHeuristic) const = 0;
    
  protected:
    /// Constructor: receives the search heuristic used for this
    BranchingStrategy(BranchingStrategyType aBranchingStrategyType, Core::BranchingConstraintFactoryPtr aBCFactory);
    
    inline Core::BranchingConstraintFactoryPtr getBranchingConstraintFactory() const
    {
      return pBranchingConstraintFactory;
    }
    
  private:
    /// Strategy type
    BranchingStrategyType pBranchingStrategyType;
    
    /// Factory for branching constraints
    Core::BranchingConstraintFactoryPtr pBranchingConstraintFactory;
  };
  
}// end namespace Search
