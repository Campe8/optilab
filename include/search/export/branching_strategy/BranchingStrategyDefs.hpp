//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 1/26/2017
//

#pragma once

namespace Search {
  
  enum BranchingStrategyType : int {
      BRANCHING_BINARY_CHOICE_POINT = 0
    , BRANCHING_DOMAIN_SPLITTING
  };
  
}// end namespace Search
