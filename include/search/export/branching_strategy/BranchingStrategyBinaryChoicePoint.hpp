//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 01/27/2017
//
// Binary choice point branching strategy:
// the branching variable is instantiated to
// some value in its domain.
// For example, if the value 1 is chosen for
// the branching variable "x", then two branches
// are generated: x = 1 and x != 1.
//

#pragma once

// Base class 
#include "BranchingStrategy.hpp"

// Branching constraint factory
#include "BranchingConstraintFactory.hpp"

namespace Search {

  class SEARCH_EXPORT_CLASS BranchingStrategyBinaryChoicePoint : public BranchingStrategy {
  public:
    BranchingStrategyBinaryChoicePoint(Core::BranchingConstraintFactoryPtr aBCFactory);

    virtual ~BranchingStrategyBinaryChoicePoint();

    /// Returns the branching decision set, i.e., a pair consisting 
    /// of the branching variable and the ordered vector of 
    /// branching constraints on the branching variable.
    /// @note the vector is ordered with the left-most branch being 
    /// the most promising branch
    BranchingChoiceSet getBranchingSet(SearchHeuristicPtr aSearchHeuristic) const override;
  };

}// end namespace Search
