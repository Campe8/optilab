//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 2/20/2017
//
// Object representing the state, i.e., a snapshot
// of a SearchHeuristic object, used in Memento pattern.
//

#pragma once

// Export definitions
#include "SearchExportDefs.hpp"

#include <vector>

namespace Search {
  
  class SEARCH_EXPORT_CLASS SearchHeuristicState {
  public:
    SearchHeuristicState(long int aChoiceVarIdx, std::size_t aPartitionSetIdx,
                         const std::vector<std::size_t>& aBranchingVarPartitionSet);
    
    virtual ~SearchHeuristicState();
    
    inline long int choiceVarIdx() const
    {
      return pChoiceVarIdx;
    }
    
    inline std::size_t partitionSetIdx() const
    {
      return pPartitionSetIdx;
    }
    
    inline const std::vector<std::size_t>& branchingVarPartitionSet() const
    {
      return pBranchingVarPartitionSet;
    }
    
  private:
    /// Index corresponding to the current selected variable
    /// chosen according to the heuristic
    long int pChoiceVarIdx;
    
    /// Index on the "pBranchingVarPartitionSet"
    /// of the search heuristic
    std::size_t pPartitionSetIdx;
    
    /// Vector of indexes sorting the branching variable set
    /// into two subsets:
    /// 1) the assigned variables
    /// 2) the unassigned variables
    std::vector<std::size_t> pBranchingVarPartitionSet;
  };
  
} // end namespace Search
