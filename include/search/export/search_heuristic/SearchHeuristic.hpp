//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 01/23/2017
//
// Search Heuristic base class.
// Search heuristic defines variable and value ordering
// strategies and heuristics to be used during
// the search process.
// A variable ordering heuristic is used to select the 
// next variable to branch on, and the ordering of
// branches is determined by a value ordering heuristic.
//

#pragma once

#include "SearchExportDefs.hpp"
#include "SearchHeuristicDefs.hpp"

// Metrics
#include "ValueChoiceMetric.hpp"
#include "VariableChoiceMetric.hpp"

// Variable
#include "Variable.hpp"

// Memento semantics
#include "BacktrackableObject.hpp"

#include <memory>
#include <vector>

// Forward declarations
namespace Base {
  namespace Tools {
    class RandomGenerator;
  }// end namespace Tools
}// end namespace Base

namespace Search {
  class SearchHeuristic;
  
  typedef SearchHeuristic* SearchHeuristicPtr;
  typedef std::unique_ptr<SearchHeuristic> SearchHeuristicUPtr;
  typedef std::shared_ptr<SearchHeuristic> SearchHeuristicSPtr;
}// end namespace Search

namespace Search {

  class SEARCH_EXPORT_CLASS SearchHeuristic : public BacktrackableObject {
  public:
    /// Constructor.
    /// @param "aBranchingVariableList" list of branching variables,
    ///        i.e., the set the choice variable is chosen from;
    /// @param "aVarChoiceMetricType" metric to be used for
    ///        choosing the variable to branch on;
    /// @param "aValChoiceMetricType" metric to be used to order
    ///         the branches;
    /// @note all not primitive type variables in "aBranchingVariableSet"
    ///       are exploded into sets of primitive type variables
    SearchHeuristic(const std::vector<Core::VariableSPtr>& aBranchingVariableList,
                    VariableChoiceMetricType aVarChoiceMetricType,
                    ValueChoiceMetricType aValChoiceMetricType);

    virtual ~SearchHeuristic() = default;
    
    /// Returns the size of the branching variable set
    /// that contains only the branching variables which this
    /// heuristic will choose from, for example, the current number
    /// of unassigned variables in the branching variable set
    std::size_t choiceSetSize() const;
    
    /// Returns true if the size of the branching variable set is zero, false otherwise.
    /// For example, if the number of current unassigned variables in the branching set
    /// is greater than one, this method returns true
    bool isChoiceSetEmpty() const;
    
    /// Returns the list of all the variables (ground and non-ground)
    /// considered for this heuristic
    inline const std::vector<Core::VariableSPtr>& getBranchingVarList() const
    {
      return pBranchingVarVector;
    }
    
    /// Returns the next variable to branch on according to
    /// this heuristic.
    /// Returns nullptr if there is no variable to branch on
    Core::VariableSPtr getChoiceVariable();

    /// Returns the next value in the ordered list of domain values
    /// of the domain of the variable retrieved by "getChoiceVariable".
    /// The left-most value is the most promising value according
    /// to this heuristic.
    /// Returns void element if getChoiceVariable returns nullptr.
    /// @note this method returns the value from the domain of
    /// the variable currently returned by getChoiceVariable, 
    /// i.e., every time getChoiceVariable is called, it updates
    /// the internal status of this heuristic setting the correspondent
    /// domain getChoiceValue takes the value to return from.
    /// @note if "aVar" is nullptr uses the variable selected by "getChoiceVariable",
    /// otherwise returns the value for the given variable
    Core::DomainElement* getChoiceValue(Core::VariablePtr aVar=nullptr);
    
    /// Sets the random generator to use for the heuristics.
    /// If none is provided, it will use a default one with uniform distribution
    void setRandomGenerator(const std::shared_ptr<Base::Tools::RandomGenerator>& aRandomGenerator);
    
  protected:
    /// Creates a snapshot of the object in the current state.
    /// @note snapshot is a MementoState object
    std::unique_ptr<MementoState> snapshot() override;
    
    /// Reinstate a previous snapshot of the object.
    /// @note snapshot is a MementoState object
    void reinstateSnapshot(const MementoState* aSnapshot) override;
  
  private:
    /// Index in "pBranchingVariableVector" corresponding
    /// to the current selected variable chosen 
    /// according to "pVarChoiceMetric".
    /// @note if < 0 no choice has been made yet
    long int pChoiceVarIdx;

    /// Ordered vector of branching variables, i.e., the set
    /// of variables to branch on
    std::vector<Core::VariableSPtr> pBranchingVarVector;

    /// Pointer to the value choice metric
    /// to use for this heuristic
    ValueChoiceMetricSPtr pValChoiceMetric;
    
    /// Pointer to the variable choice metric
    /// to use for this heuristic
    VariableChoiceMetricSPtr pVarChoiceMetric;
    
    /// Index on the "pBranchingVarPartitionSet".
    /// This index holds the following invariant:
    /// for all v in "pBranchingVarVector" s.t.
    /// v = pBranchingVarVector[pBranchingVarPartitionSet[i]]
    /// v is assigned iff i < "pPartitionSetIdx"
    std::size_t pPartitionSetIdx;
    
    /// Custom random generator use for heuristics
    std::shared_ptr<Base::Tools::RandomGenerator> pRandomGenerator;
    
    /// Vector of indexes sorting the branching variable set
    /// into two subsets:
    /// 1) the assigned variables
    /// 2) the unassigned variables
    std::vector<std::size_t> pBranchingVarPartitionSet;
    
    /// Sort partition set and update partition set index
    /// s.t. the invariant holds
    void sortPartitionSet();
    
    /// Returns the current selected variable
    /// or nullptr if no choice has been made yet
    inline Core::Variable* getInternalChoiceVar() const
    {
      assert(pChoiceVarIdx < static_cast<long int>(pBranchingVarVector.size()));
      return pChoiceVarIdx >= 0 ? pBranchingVarVector.at(pChoiceVarIdx).get() : nullptr;
    }
  };

}// end namespace Search
