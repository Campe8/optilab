//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 2/20/2017
//
// Memento state for backtrackable objects
// of class SearchHeuristic.
//

#pragma once

// Base class
#include "MementoState.hpp"

// Search heuristic state
#include "SearchHeuristicState.hpp"


namespace Search {
  
  class SEARCH_EXPORT_CLASS MementoStateSearchHeuristic : public MementoState {
  public:
    MementoStateSearchHeuristic(std::unique_ptr<SearchHeuristicState> aSearchHeuristicState);
    
    ~MementoStateSearchHeuristic();
    
    static bool isa(const MementoState* aMementoState)
    {
      assert(aMementoState);
      return aMementoState->getMementoStateId() == MementoStateId::MS_SEARCH_HEURISTIC;
    }
    
    static MementoStateSearchHeuristic* cast(MementoState* aMementoState)
    {
      if (!isa(aMementoState))
      {
        return nullptr;
      }
      return static_cast<MementoStateSearchHeuristic*>(aMementoState);
    }
    
    static const MementoStateSearchHeuristic* cast(const MementoState* aMementoState)
    {
      if (!isa(aMementoState))
      {
        return nullptr;
      }
      return static_cast<const MementoStateSearchHeuristic*>(aMementoState);
    }
    
    inline const SearchHeuristicState* getSearchHeuristicState() const
    {
      return pSearchHeuristicState.get();
    }
    
  private:
    std::unique_ptr<SearchHeuristicState> pSearchHeuristicState;
  };
  
} // end namespace Search
