//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 1/24/2017
//

#pragma once

namespace Search {

  enum VariableMetricEval : int {
        VAR_METRIC_EQ = 0
      , VAR_METRIC_LT
      , VAR_METRIC_GT
  };

  enum VariableChoiceMetricType : int {
      VAR_CM_INPUT_ORDER = 0
    , VAR_CM_FIRST_FAIL
    , VAR_CM_ANTI_FIRST_FAIL
    , VAR_CM_SMALLEST
    , VAR_CM_LARGEST
    , VAR_CM_UNDEF
  };
  
  enum ValueChoiceMetricType : int {
      VAL_METRIC_INDOMAIN_MIN = 0
    , VAL_METRIC_INDOMAIN_MAX
    , VAL_METRIC_INDOMAIN_RANDOM
    , VAL_METRIC_UNDEF
  };
  
}// end namespace Search
