//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 02/06/2017
//

#pragma once

// Macro for solution manager: all solutions
#define SM_SOLUTION_LIMIT_NONE -1

// Macro for solution manager: solution limit defined
#define SM_SOLUTION_LIMIT_SET   0

#define RETURN_SEARCH_STATUS(aSearchStatus) do {      \
setGlobalSearchStatus(aSearchStatus);   \
return aSearchStatus;       \
} while (0)

// Return search status to the caller
#define RETURN_STATUS(aSearchStatus) do {      \
if(aSearchStatus == SearchStatus::SEARCH_FORCED_FAIL || aSearchStatus == SearchStatus::SEARCH_TERMINATE)  \
{ \
return SearchStatus::SEARCH_SUCCESS;  \
} \
return aSearchStatus;       \
} while (0)

// Verify and returns whether given status is a failure status
#define VERIFY_FAILURE(aSearchStatus) \
recursiveSearchStatus == SearchStatus::SEARCH_FAIL || \
recursiveSearchStatus == SearchStatus::SEARCH_FORCED_FAIL
