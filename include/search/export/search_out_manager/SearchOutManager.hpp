//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 02/05/2017
//

#pragma once

#include "SearchExportDefs.hpp"

namespace Search {
  
  class SEARCH_EXPORT_CLASS SearchOutManager {
  public:
    SearchOutManager();
    
    virtual ~SearchOutManager();
  };
  
}// end namespace Search
