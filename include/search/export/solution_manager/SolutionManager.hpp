//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 02/05/2017
//

#pragma once

#include "SearchExportDefs.hpp"
#include "SearchDefs.hpp"
#include "SearchMacro.hpp"

#include "Model.hpp"
#include "Variable.hpp"

#include <string>
#include <vector>
#include <memory>  // for std::shared_ptr
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <utility>

// Forward declarations
namespace Search {
  class SearchEnvironment;
}// end namespace Search

namespace Search {
  
  class SEARCH_EXPORT_CLASS SolutionManager {
  public:
    using SPtr = std::shared_ptr<SolutionManager>;
    
    // Define solution as a mapping between variable IDs
    // and its list of pair of bounds [LB, UB].
    // @note if the variable is a primitive variable, the list has length 1
    using DomElemPtr = Core::DomainElement*;
    using BoundsPair = std::pair<DomElemPtr, DomElemPtr>;
    using Solution = spp::sparse_hash_map<std::string, std::vector<BoundsPair>>;
    
    // Solution counter
    using SolutionCtr = long long int;
    
    // List of the variables that must be part of a solution.
    // @note this may be all the set of the variables in a model
    using VarMapSPtr = Model::ModelVarMapSPtr;
    
  public:
    SolutionManager(const VarMapSPtr& aVarMap);
    
    /// Sets "aSEnv" as current search environment within the solution manager.
    /// This allows the solution manager to change its status upon notification
    /// w.r.t. the current search environment.
    /// @note the search environment must be instantiated on a (sub)set of
    /// the variables used to instantiate this solution manager
    void setSearchEnvironment(const SearchEnvironment* aSEnv);
    
    /// Returns the number of recorded solutions
    inline std::size_t getNumberOfRecordedSolutions() const
    {
      return !needsMoreSolutions() ? pSolutionLimit : pSolutionsCounter;
    }
    
    /// Set the upper bound on the number of solutions to find.
    /// @note default is 1.
    /// @note SM_SOLUTION_LIMIT_NONE means compute all solutions
    void setSolutionLimit(SolutionCtr aSolutionLimit);
    
    /// Returns the solution limit
    inline SolutionCtr getSolutionLimit() const
    {
      return pMetaSolutionLimit < static_cast<SolutionCtr>(SM_SOLUTION_LIMIT_SET) ?
      pMetaSolutionLimit :
      pSolutionLimit;
    }
    
    /// Notifies this solution manager that a new solution has been found,
    /// i.e., it's not possible to proceed with the search w.r.t. the current
    /// search tree.
    /// This notification checks the internal state w.r.t. the current
    /// environment and returns a SolutionStatus that describes
    /// how to continue with the search process.
    /// @note notifications must come from a search process which is exploring
    /// a search tree built on the same search environment set in this manager.
    /// @note if aForceDuplicateSolutionCheck is true, forces the manager to check
    /// if the current (to be) solution is a duplicate solution. If so, it skips registering it
    SolutionStatus notify(bool aForceDuplicateSolutionCheck=false);
    
    /// Returns last solution
    inline Solution getLastSolution()
    {
      return getSolution(getNumberOfRecordedSolutions());
    }
    
    /// Returns the solution "aIdx"
    /// @note the indexing is 1-based: 1, 2, 3, ...
    Solution getSolution(std::size_t aIdx);
    
    /// Reset internal solution register and counter.
    /// @note it DOES NOT reset the solution limit
    void resetRegister();
    
    /// Returns true if the solution limit has not been reached yet,
    /// false otherwise
    inline bool needsMoreSolutions() const
    {
      return (pMetaSolutionLimit < SM_SOLUTION_LIMIT_SET) || (pSolutionsCounter < pSolutionLimit);
    }
    
  private:
    /// A SolutionDescriptor describes the domain bounds
    /// of each variable part of a solution.
    /// Each variable 'x' has a vector of DomainList: one for each
    /// primitive variable composing 'x'.
    /// If the variable is already primitive the vector will contain
    /// only 1 DomainList.
    /// Each DomainList is a vector of bound pairs,
    /// one for each solution found
    using DomainList = std::vector<BoundsPair>;
    using SolutionDescriptor = std::vector<DomainList>;
    
  private:
    /// Flag for no solution limit/optimization limit
    int pMetaSolutionLimit;
    
    /// Flag indicating if there is at least one variable in the
    /// set of solution variables
    bool pOutputVariables;
    
    /// Number of solutions required
    SolutionCtr pSolutionLimit;
    
    /// Current number of solutions found
    SolutionCtr pSolutionsCounter;
    
    /// List of variables used to define a solution
    VarMapSPtr pVarMap;
    
    inline Model::ModelVarMap& varMap()
    {
      return *pVarMap;
    }
    
    /// List of IDs on pVarMap of solution variables,
    /// i.e., variables that are part of the solution
    /// of the current model, e.g., branching variables
    std::vector<std::string> pSolVarList;
    
    /// List of branching variables in the current search environment
    std::vector<std::string> pBrcVarList;
    
    /// Map of <ID, {Var}> for non primitive variables
    std::unordered_map<std::string, std::vector<Core::VariableSPtr>> pExplodedVarSet;
    
    /// Map pairing UUID of variables with their solution descriptors
    std::unordered_map<std::string, SolutionDescriptor> pSolutionRegister;
    
    /// Set of solution hashes
    std::unordered_set<std::string> pSolutionHashSet;
    
    /// Registers internally the set of output variables
    void registerVariables();
    
    /// Returns the pointer to the variable with id "aID" from the internal map
    /// or nullptr if no such variable exists
    Core::Variable* getVariableFromMap(const std::string aID);
    
    /// Create a new solution from the set of registered variables
    /// and add it to the solution register
    void registerSolution(bool aForceDuplicateSolutionCheck);
    
    /// Adds the pair <aVar_LB, aVar_UB> to the given solution descriptor of var index "aVarIdx"
    void addBoundPairToSolutionDescriptor(Core::Variable* aVar, std::size_t aVarIdx,
                                          SolutionDescriptor& aSolDesc);
    
    /// Checks if notified assignment is a solution
    bool checkSolutionSoundness();
    
    /// Increments solution counter after checks,
    /// for example, after checking the output set size
    void incrementSolutionCounter();
    
    /// Returns the hash of the current solution
    std::string calculateSolutionHash();
  };
  
}// end namespace Search
