//
// Copyright OptiLab 2019. All rights reserved.
//
// Created by Federico Campeotto on 05/02/2019
//
// Hybrid stack used by the HybridBacktrackManager to store and
// retrieve memento objects.
// This will allow the backtrack manager to perform an hybrid form
// of state cloning and state recomputation.
//

#pragma once

#include "SearchExportDefs.hpp"

#include <memory>   // for std::shared_ptr
#include <stack>
#include <utility>  // for std::pair

#include <sparsepp/spp.h>

#include "BacktrackableObject.hpp"
#include "BranchingConstraint.hpp"
#include "ConstraintStore.hpp"
#include "SearchDefs.hpp"

namespace Search {
  
  /// Stack size, level of the stack to pop from
  using StackLevel = std::size_t;
  
  /// Set of backtrackable ids
  using BacktrackableIdSet = spp::sparse_hash_set<UUIDBacktrackableObj>;
  
  using BacktrackableObjectRegister =
  spp::sparse_hash_map<UUIDBacktrackableObj, BacktrackableObjectSPtr>;
  
  class SEARCH_EXPORT_CLASS HybridTrailStack {
  public:
    /// Constructor, it takes the constraint store used to register constraint
    /// on recomputation.
    /// @throw std::invalid_argument if the given ConstraintStore is empty
    HybridTrailStack(const Semantics::ConstraintStoreSPtr& aConstraintStore);
    
    HybridTrailStack(const HybridTrailStack& aOther) = delete;
    HybridTrailStack(HybridTrailStack&& aOther) = delete;
    
    virtual ~HybridTrailStack() = default;
    
    HybridTrailStack& operator= (const HybridTrailStack& aOther) = delete;
    HybridTrailStack& operator= (HybridTrailStack&& aOther) = delete;
    
    /// Returns the size of this trail stack
    inline bool empty() const { return pStack.empty(); }
    
    /// Returns the top level of the stack
    inline StackLevel getStackLevel() const
    {
      return empty() ? 0 : pStack.top().level;
    }
    
    /// Pushes all the backtrackable objects in "aBacktrackableIds" on the stack.
    /// These objects are retrieved from the given register of backtrackable objefts
    void push(BacktrackableIdSet& aBacktrackableIds,
              BacktrackableObjectRegister* aBacktrackableRegister);
    
    /// Pushes the current braching choice on top of the stack of branching choices linked
    /// to the current level of the stack
    void pushNodeChoice(const std::shared_ptr<Core::BranchingConstraint>& aChoice);
    
    /// Restores all the backtrackable objects that are at the top the stack
    /// PLUS
    /// all the backtrackable objects in "aBacktrackableIds".
    /// Pops the top of the stack afterwards.
    /// @note aBacktrackableIds can be empty
    void pop(BacktrackableIdSet& aBacktrackableIds,
             BacktrackableObjectRegister* aBkjRegister);
    
  private:
    struct StackNode {
    public:
      /// Stack of (node) choices used for recomputation
      using ChoiceStack = std::deque<std::shared_ptr<Core::BranchingConstraint>>;
      
    public:
      StackNode(StackLevel stackLevel, StackLevel maxSizeRecoStack)
      : level(stackLevel)
      , recomputationStackMaxSize(maxSizeRecoStack)
      {
      }
      
      /// Level this stack node is at
      StackLevel level;
      
      /// Maximum size allowed for the recomputation stack
      StackLevel recomputationStackMaxSize;
      
      /// Stack of branching choices used for recomputation
      ChoiceStack recomputationStack;
      
      /// Set of ids for the backtrackable objects that are changed at this stack node's level
      BacktrackableIdSet backtrackableIdSet;
      
      spp::sparse_hash_map<UUIDBacktrackableObj, MementoSPtr> state;
    };
    
    /// List of the memento objects for each stack level
    using TrailStackInfoNode = std::deque<std::pair<StackLevel, MementoUPtr>>;
    
  private:
    /// Internal stack level.
    /// @note pStackLevel != getStackLevel()
    StackLevel pStackLevel{0};
    
    /// Maximum size of the choice stack.
    /// This defines how much recomputation and how much cloning the engine will perform
    std::size_t pChoiceStackMaxSize;
    
    /// Constraint store used to perform recomputation
    Semantics::ConstraintStoreSPtr pRecomputationStore;
    
    /// Stack data structure implemented as a deque
    std::stack<StackNode, std::vector<StackNode>> pStack;
  };
  
}// end namespace Search
