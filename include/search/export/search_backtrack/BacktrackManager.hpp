//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 02/09/2017
//
// Backtrack manager or Caretaker, is reponsible
// for the memento's safekeeping.
// It never operates on or examines the contents
// of a memento.
//

#pragma once

#include "BaseBacktrackManager.hpp"

#include <cassert>
#include <memory>

#include "SearchDefs.hpp"
#include "TrailStack.hpp"

namespace Search {
  
  class SEARCH_EXPORT_CLASS BacktrackManager : public BaseBacktrackManager {
  public:
    BacktrackManager();
    
    BacktrackManager(const BacktrackManager& aOther) = delete;
    BacktrackManager(BacktrackManager&& aOther) = delete;
    
    virtual ~BacktrackManager() = default;
    
    /// Attaches "aBckObj" to the list of backtrackable objects for
    /// saving and restoring state
    void attachBacktrackableObject(const BacktrackableObjectSPtr& aBckObj) override;
    
    /// Detaches "aBckObj" from the list of backtrackable objects for
    /// saving and restoring state
    void detachBacktrackableObject(const BacktrackableObjectSPtr& aBckObj) override;
    
    /// Saves all the mementos of all the backtrackable objectes that
    /// notified this backtrack manager after the last call to "saveState".
    /// If no notification happened between the previous call of this method
    /// and the current call, no snapshot will be saved.
    /// To force saving all the mementos even if no notification happened,
    /// use "aForceSave" set to true
    void saveState(bool aForceSave = false) override;
    
    /// Pops from the top of the internal stack the list of mementos which
    /// will be restored
    void restoreState() override;
    
    /// Returns the current trailstack level
    StackLevel getLevel() const override;
  protected:
    /// Notify this manager upon a change on the backtrackable object's state.
    /// The given backtrackable will be stored on the internal stack
    /// next time "saveState" will be called
    void notifyChange(BacktrackableObject* aBktObject) override;
    
  private:
    /// Internal stack
    std::unique_ptr<TrailStack> pTrailStack;
    
    /// Map of the backtrackable objects registered in this manager
    BacktrackableObjectRegister pBacktrackableRegister;
    
    /// Set of all the backtrackable objects that changed their status since
    /// last time they where pushed on the internal stack
    ChangedUUIDSet pChangedBacktrackables;
    
    /// Insert the aBjkObjUUID into the set of changed backtrackable objects
    inline void insertBacktrackableObjectInChangedSet(UUIDBacktrackableObj aBjkObjUUID)
    {
      assert(pBacktrackableRegister.find(aBjkObjUUID) != pBacktrackableRegister.end());
      pChangedBacktrackables.insert(aBjkObjUUID);
    }
    
    /// Sets as changed all the backtrackable registered in this manager
    void forceFullSnapshot()
    {
      for(auto it = pBacktrackableRegister.begin(); it != pBacktrackableRegister.end(); ++it)
      {
        notifyChange((it->second).get());
      }
    }
    
  };
  
}// end namespace Search
