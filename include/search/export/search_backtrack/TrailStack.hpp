//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 02/10/2017
//
// Stack used by the BacktrackManager to store and
// retrieve memento objects.
//

#pragma once

#include "SearchExportDefs.hpp"

#include <deque>
#include <memory>
#include <utility>  // for std::pair

#include <sparsepp/spp.h>

#include "SearchDefs.hpp"

// Objects stored into the stack
#include "BacktrackableObject.hpp"

namespace Search {
  
  /// Stack size, level of the stack to pop from
  using StackLevel = std::size_t;
  
  /// Set of changed backtrackable ids at each stack level
  using ChangedUUIDSet = spp::sparse_hash_set<UUIDBacktrackableObj>;
  
  /// Node of the stack represented as a pair
  /// <StackLevel, ChangedUUIDSet>
  /// where StackLevel is the current stack level of the stack and ChangedUUIDSet
  /// is the set of backtrackable objects changed at that level
  using StackNode = std::pair<StackLevel, ChangedUUIDSet>;
  
  using  BacktrackableObjectRegister =
  spp::sparse_hash_map<UUIDBacktrackableObj, BacktrackableObjectSPtr>;
  
  class SEARCH_EXPORT_CLASS TrailStack {
  public:
    TrailStack();
    
    TrailStack(const TrailStack& aOther) = delete;
    TrailStack(TrailStack&& aOther) = delete;
    
    virtual ~TrailStack() = default;
    
    TrailStack& operator= (const TrailStack& aOther) = delete;
    TrailStack& operator= (TrailStack&& aOther) = delete;
    
    inline bool empty() const
    {
      return pStack.empty();
    }
    
    /// Returns the top level of the stack
    inline StackLevel getStackLevel() const
    {
      return empty() ? 0 : pStack.front().first;
    }
    
    void push(ChangedUUIDSet& aBacktrackableObjSet,
              BacktrackableObjectRegister* aBkjRegister);
    
    /// Restores the backtrackable objects the top node of the stack PLUS
    /// the backtrackable objects in "aBacktrackableObjSet" (it can be empty).
    /// Pops the top of the stack afterwards.
    /// "aBacktrackableObjSet" will contain the set of changed backtrackable objects
    void pop(ChangedUUIDSet& aBacktrackableObjSet,
             BacktrackableObjectRegister* aBkjRegister);
    
  private:
    /// Internal stack level.
    /// @note pStackLevel != getStackLevel()
    StackLevel pStackLevel;
    
    /// Auxiliary information hold for each node of the stack
    using TrailStackInfoNode = std::deque<std::pair<StackLevel, MementoUPtr>>;
    
    /// Stack data structure implemented as a deque
    std::deque<StackNode> pStack;
    
    /// This map is used to implement a fast restore of the states of the
    /// backtrackable objects, by providing an average constant access to
    /// the informations associated with a backtrackable object and a
    /// time linear in the number of levels removed from the stack to retrieve
    /// the state to restore.
    /// Every object id is associated with al list of pairs. Every pair of the list
    /// stores level of the stack associated with the states as first element and
    /// the states as second element.
    /// For example, let's assume that object identified by id_0 belongs to the set
    /// of changed backtrackable objects and let's also assume that id_0
    /// has the following associated list of pairs [level, memento]:
    /// id_0 -> [5, M_5]--[2, M_2]--[0, M_0]
    /// If the trailstack has to restore level 7, with id_0
    /// will be restored with memento M_5 since 5 (level) < 7 (level).
    /// However, [5, M_5] won't be removed from the list until the trailStack
    /// has to restore level 5.
    /// In order to restore level 1, instead, [5,M_5]--[2,M_2] will be popped
    /// out of the list and M_0 will be used to restore the object associated with
    /// id_0. Again, [0,M_0] won't be removed since 0 (level) < 1 (level)
    spp::sparse_hash_map<UUIDBacktrackableObj, TrailStackInfoNode> pTrailStackInfo;
    
    /// Restores memento of the backtrackable object identified by "bktObjUUID"
    void restoreBacktrackable(const UUIDBacktrackableObj& bktObjUUID,
                              BacktrackableObjectRegister* aBkjRegister);
  };
  
}// end namespace Search
