//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 02/09/2017
//
// Internal Memento state, should be derived
// as it represents the snapshot of the objects
// that should be backtracked.
//

#pragma once

#include "SearchExportDefs.hpp"
#include "SearchDefs.hpp"

#include <memory>
#include <cassert>

namespace Search {
  
  class SEARCH_EXPORT_CLASS MementoState {
  public:
    MementoState(const MementoState& aOther);
    MementoState(MementoState&& aOther);
    
    virtual ~MementoState();
    
    MementoState& operator= (const MementoState& aOther);
    MementoState& operator= (MementoState&& aOther);
    
    inline MementoStateId getMementoStateId() const
    {
      return pMementoStateId;
    }
    
  protected:
    MementoState(MementoStateId aMementoStateId);
    
  private:
    MementoStateId pMementoStateId;
    
  };
  
}// end namespace Search
