//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 02/09/2017
//
// A Memento object represents the internal "state"
// of a "backtrackable" object.
// This allows objects to store their current memento
// and if needed, restore it later on.
// See also "Design Patterns"
// by Gamma, Helm, Johnson, Vlissides (published by Addison Wesley)
// for Memento design pattern.
//

#pragma once

#include "SearchExportDefs.hpp"

#include "MementoState.hpp"

#include <memory>

// forward declarations
namespace Search {
  class BacktrackableObject;
  
  class Memento;
  typedef std::unique_ptr<Memento> MementoUPtr;
  typedef std::shared_ptr<Memento> MementoSPtr;
}// end namespace Search

namespace Search {
  
  class SEARCH_EXPORT_CLASS Memento {
  public:
    Memento(const Memento& aOther);
    Memento(Memento&& aOther);
    
    /// Narrow down the interface to the originator,
    /// i.e., the bracktrackable object
    virtual ~Memento();
    
    Memento& operator= (const Memento& aOther);
    Memento& operator= (Memento&& aOther);
    
    bool hasValidState() const
    {
      return pState != nullptr;
    }
    
  protected:
    friend class BacktrackableObject;
    
    Memento();
    
    /// Sets internal state
    inline void setState(std::unique_ptr<MementoState> aState)
    {
      pState = std::move(aState);
    }
    
    /// Returns internal state
    inline const MementoState* getState() const
    {
      return pState.get();
    }
    
  private:
    /// State of the backtrackable object
    std::unique_ptr<MementoState> pState;
    
  };
  
}// end namespace Search
