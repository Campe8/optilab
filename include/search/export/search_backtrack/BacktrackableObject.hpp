//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 02/09/2017
//
// The "Originator" component of the Memento
// design pattern. It creates a memento containing
// a snapshot of its current internal state.
// It uses the memento to restore its internal state.
//

#pragma once

// UUID defs
#include "BaseUtils.hpp"

#include "CoreObjectDefs.hpp"

#include "SearchExportDefs.hpp"
#include "SearchDefs.hpp"

// Memento
#include "Memento.hpp"

#include <memory>
#include <cassert>

// Forward declarations
namespace Search {
  class BacktrackableObject;
  
  typedef BacktrackableObject* BacktrackableObjectPtr;
  typedef std::unique_ptr<BacktrackableObject> BacktrackableObjectUPtr;
  typedef std::shared_ptr<BacktrackableObject> BacktrackableObjectSPtr;
  
  class BaseBacktrackManager;
}

namespace Search {
  
  typedef UUIDTag UUIDBacktrackableObj;
  
  class SEARCH_EXPORT_CLASS BacktrackableObject {
  public:
    virtual ~BacktrackableObject();
    
    bool operator==(const BacktrackableObject& aOther) const
    {
      return pUUIDTag == aOther.pUUIDTag;
    }
    
    /// Returns the UUID tag of this backtrackable object
    inline UUIDBacktrackableObj getUUID() const
    {
      return pUUIDTag;
    }
    
    /// Returns true if this backtrackable object is
    /// attached to a Caretaker object,
    /// e.g., a BacktrackManager
    inline bool isAttachedToCaretaker() const
    {
      return pBaseBacktrackManager != nullptr;
    }
    
    /// Returns a snapshot of the current state hold
    /// by the returned Memento
    std::unique_ptr<Memento> getMemento();
    
    /// Restores previous state hold by "aMemento"
    void setMemento(std::unique_ptr<Memento> aMemento);
    
    /// Restores previous state hold by "aMemento".
    /// @note this method does not get ownership of the
    /// memento object given as input
    void setMemento(Memento* aMemento);
    
  protected:
    friend class BacktrackManager;
    friend class HybridBacktrackManager;
    
    BacktrackableObject();
    
    /// Attach backtrack manager acting as an observer of this object
    inline void attachBacktrackManager(BaseBacktrackManager* aBaseBacktrackManager)
    {
      pBaseBacktrackManager = aBaseBacktrackManager;
    }
    
    /// Detach backtrack manager if any
    inline void detachBacktrackManager()
    {
      pBaseBacktrackManager = nullptr;
    }
    
    /// Notify the backtrack manager upon a changed of internal state
    void notifyBacktrackManager();
    
    /// Creates a snapshot of the object in the current state.
    /// @note snapshot is a MementoState object
    virtual std::unique_ptr<MementoState> snapshot() = 0;
    
    /// Reinstate a previous snapshot of the object.
    /// @note snapshot is a MementoState object
    virtual void reinstateSnapshot(const MementoState* aSnapshot) = 0;
    
  private:
    /// UUID tag - derive from model obj
    UUIDBacktrackableObj pUUIDTag;
    
    /// Backtrack manager observer
    BaseBacktrackManager* pBaseBacktrackManager;
  };
  
}// end namespace Search
