//
// Copyright OptiLab 2019. All rights reserved.
//
// Created by Federico Campeotto on 05/01/2019
//
// Base backtra manager interface class
//

#pragma once

#include "SearchExportDefs.hpp"
#include "TrailStack.hpp"

// Forward declarations
namespace Search {
  class BacktrackableObject;
}

namespace Search {
  
  class SEARCH_EXPORT_CLASS BaseBacktrackManager {
  public:
    BaseBacktrackManager() = default;
    virtual ~BaseBacktrackManager() = default;
    
    BaseBacktrackManager(const BaseBacktrackManager& aOther) = delete;
    BaseBacktrackManager(BaseBacktrackManager&& aOther) = delete;
    
    /// Attaches "aBckObj" to the list of backtrackable objects for
    /// saving and restoring state
    virtual void attachBacktrackableObject(const BacktrackableObjectSPtr& aBckObj) = 0;
    
    /// Detaches "aBckObj" from the list of backtrackable objects for
    /// saving and restoring state
    virtual void detachBacktrackableObject(const BacktrackableObjectSPtr& aBckObj) = 0;
    
    /// Saves all the mementos of all the backtrackable objectes that
    /// notified this backtrack manager after the last call to "saveState".
    /// If no notification happened between the previous call of this method
    /// and the current call, no snapshot will be saved.
    /// To force saving all the mementos even if no notification happened,
    /// use "aForceSave" set to true
    virtual void saveState(bool aForceSave = false) = 0;
    
    /// Pops from the top of the internal stack the list of mementos which
    /// will be restored
    virtual void restoreState() = 0;
    
    /// Returns the current trailstack level
    virtual StackLevel getLevel() const = 0;
    
  protected:
    friend class BacktrackableObject;
    
    /// Notify this manager upon a change on the backtrackable object's state.
    /// The given backtrackable will be stored on the internal stack
    /// next time "saveState" will be called
    virtual void notifyChange(BacktrackableObject* aBktObject) = 0;
  };
  
}// end namespace Search
