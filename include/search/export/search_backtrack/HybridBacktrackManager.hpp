//
// Copyright OptiLab 2019. All rights reserved.
//
// Created by Federico Campeotto on 05/01/2019
//
// Hybrid backtrack manager or Caretaker, is reponsible
// for the memento's safekeeping. It never operates on or
// examines the contents of a memento.
// However, it peforms backtrack of the state by an hybrid
// approach between cloning and recomputation.
//

#pragma once

#include "BaseBacktrackManager.hpp"

#include <cassert>
#include <memory>
#include <stack>

#include "ConstraintStore.hpp"
#include "HybridTrailStack.hpp"
#include "SearchDefs.hpp"

namespace Search {
  class Node;
}

namespace Search {
  
  class SEARCH_EXPORT_CLASS HybridBacktrackManager : public BaseBacktrackManager {
  public:
    /// Constructor, it takes the given constraint store to perform recomputation.
    /// @throw std::invalid_argument if the given ConstraintStore is empty
    HybridBacktrackManager(const Semantics::ConstraintStoreSPtr& aConstraintStore);
    
    HybridBacktrackManager(const HybridBacktrackManager& aOther) = delete;
    HybridBacktrackManager(HybridBacktrackManager&& aOther) = delete;
    
    virtual ~HybridBacktrackManager() = default;
    
    /// Attaches "aBckObj" to the list of backtrackable objects for
    /// saving and restoring state
    void attachBacktrackableObject(const BacktrackableObjectSPtr& aBckObj) override;
    
    /// Detaches "aBckObj" from the list of backtrackable objects for
    /// saving and restoring state
    void detachBacktrackableObject(const BacktrackableObjectSPtr& aBckObj) override;
    
    /// Saves all the mementos of all the backtrackable objectes that
    /// notified this backtrack manager after the last call to "saveState".
    /// If no notification happened between the previous call of this method
    /// and the current call, no snapshot will be saved.
    /// To force saving all the mementos even if no notification happened,
    /// use "aForceSave" set to true
    void saveState(bool aForceSave = false) override;
    
    /// Saves the current (tree node) choice for recomputation
    void saveNodeChoice(Search::Node* aNode);

    /// Pops from the top of the internal stack the list of mementos which
    /// will be restored
    void restoreState() override;
    
    /// Returns the current trailstack level
    StackLevel getLevel() const override;
  protected:    
    /// Notify this manager upon a change on the backtrackable object's state.
    /// The given backtrackable will be stored on the internal stack
    /// next time "saveState" will be called
    void notifyChange(BacktrackableObject* aBktObject) override;
    
  private:
    /// Internal stack
    std::unique_ptr<HybridTrailStack> pTrailStack;
    
    /// Map of the backtrackable objects registered in this manager
    BacktrackableObjectRegister pBacktrackableRegister;
    
    /// Set of all the backtrackable objects that changed their status since
    /// last time they where pushed on the internal stack
    ChangedUUIDSet pChangedBacktrackables;
    
    /// Insert the aBjkObjUUID into the set of changed backtrackable objects
    inline void insertBacktrackableObjectInChangedSet(UUIDBacktrackableObj aBjkObjUUID)
    {
      assert(pBacktrackableRegister.find(aBjkObjUUID) != pBacktrackableRegister.end());
      pChangedBacktrackables.insert(aBjkObjUUID);
    }
    
    /// Sets as changed all the backtrackable registered in this manager
    void forceFullSnapshot()
    {
      for(auto it = pBacktrackableRegister.begin(); it != pBacktrackableRegister.end(); ++it)
      {
        notifyChange((it->second).get());
      }
    }
    
  };
  
}// end namespace Search
