//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 04/02/2017
//
// Semantics of a search engine.
// Similarly to the Constraint Logic Programming notion
// of semantics, this class represents a virtual interpreter
// that maintains a pair (G, S) during execution.
// The first element of this pair is called goal,
// here inteded as the objective variable, the second element
// is called constraint store.
//

#pragma once

#include "SearchExportDefs.hpp"

#include "Variable.hpp"
#include "ConstraintStore.hpp"
#include "SolutionManager.hpp"

// Branching constraints used as optimization constraints
#include "BranchingConstraintFactory.hpp"

#include <set>
#include <unordered_map>
#include <memory>

namespace Search {
  
  class SEARCH_EXPORT_CLASS SearchSemantics {
  public:
    /// ObjectiveValue mapping variable IDs to their domain values.
    /// @note the vector of domain elements is used to stored the values of the domains
    /// of a variable array
    using ObjectiveValue = SolutionManager::Solution;
    
  public:
    SearchSemantics();
    
    virtual ~SearchSemantics() = default;
    
    /// Returns the instance of constraint store of this search semantic
    inline Semantics::ConstraintStoreSPtr constraintStore() const { return pConstraintStore; }
    
    /// Returns the objective variable
    inline Core::VariableSPtr variableObjective() const { return pGoal; }
    
    /// Returns the exploded objective variable in a list
    inline const std::vector<Core::VariableSPtr>& variableObjectiveList() const
    {
      return pExplodedGoalList;
    }
    
    /// Returns true if there is an objective variable
    /// associated to this semantics, false otherwise
    inline bool hasVariableObjective() const { return pGoal != nullptr; }
    
    /// Sets "aVariableObjective" as objective variable with "aExtremum" as extremum to optimize for.
    /// @note the objective variable must be a primitive variable or an array of variables.
    /// The array must containt either primitive variables or array of variables.
    /// @note if there is already an objective variable set, this will reset it
    void setVariableObjective(const Core::VariableSPtr& aObjective);
    
    /// Returns true if at least one objective value has been set.
    /// Returns false otherwise
    inline bool isObjectiveValueSet() const { return !pGoalValue.empty(); }
    
    /// Sets the objective value.
    /// optimizeObjective will post objective constraints based on the value given to this function
    inline void setObjectiveValue(const ObjectiveValue& aObjVal) { pGoalValue = aObjVal; }
    
    /// Returns the current objective value
    inline const ObjectiveValue& getObjectiveValue() const { return pGoalValue; }
    
    /**
     * Optimize objective as follows:
     * 1 - removes current optimization constraints from constraint store;
     * 2 - creates new optimization constraints w.r.t. the domain values of
     *     the objective variables;
     * 3 - registers newly created optimization constraints into the constraint store.
     * @note if the extremum is "MIN", optimization constraints are based on the
     * lower bound of each objective variable. If the extremum is "MAX",
     * optimization constraints are based on the upper bound of each objective variable.
     * @note throws if there is no objective value
     */
    void optimizeObjective();
    
    /// Forces the re-evaluation of all objective constraints currently hoold by
    /// this search semantics.
    /// Objective constraints are automatically registered in the constraint store when calling
    /// the method "optimizeObjective()".
    /// This method forces the constraint store to insert all objective constraints into the queue
    /// of constraints ready for re-evaluation
    void forceObjectiveReevaluation();
    
    /// Cleans up all optimization objective constraints posted for objective optimization
    inline void cleanupOptimizationObjective() { pOptimizationConstraintSet.clear(); }
    
    /// Returns true if nogood learning is active, false otherwise
    inline bool useNogoodLearning() const { return pNogoodLeaning; }
    
    /// Performs nogood learning by registering the nogood constraint into the constraint store.
    /// The nogood constraint is created on the variables and domains specified as arguments
    /// to the function.
    /// @note the domains are supposed to be singleton and their singleton value is used
    /// to define the nogood 
    void storeNogood(const std::vector<Core::VariableSPtr>& aNogoodVars,
                     const std::vector<Core::DomainSPtr>& aNogoodDoms);
    
    /// Remove all stored nogood constraints
    inline void cleanupNogoods() { pNogoodStore.clear(); }
    
    /// Forces the re-evaluation of all nogood constraints currently hoold by this search semantics.
    /// Nogood constraints are automatically registered in the constraint store when calling
    /// the method "storeNogood()".
    /// This method forces the constraint store to insert all nogood constraints into the queue
    /// of constraints ready for re-evaluation
    void forceNogoodsReevaluation();
    
  private:
    /// Objective variable - goal describing this semantics
    Core::VariableSPtr pGoal;
    
    /// Current value of the goal
    ObjectiveValue pGoalValue;
    
    /// Nogood learning activation flag
    bool pNogoodLeaning;
    
    // Factory used for optimization constraints
    std::unique_ptr<Core::BranchingConstraintFactory> pBranchingConstraintFactory;
    
    /// Exploded goal set
    std::vector<Core::VariableSPtr> pExplodedGoalList;
    
    /// Set of current active objective constraints
    std::set<Core::BranchingConstraintSPtr> pOptimizationConstraintSet;
    
    /// Vector of nogood constraints
    std::vector<Core::ConstraintSPtr> pNogoodStore;
    
    /// Constraint store describing this semantics
    Semantics::ConstraintStoreSPtr pConstraintStore;
  };
  
}// end namespace Search
