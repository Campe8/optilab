//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 1/16/2018
//
// Depth first search strategy.
// @note DFS is an instance of a base search,
// which, in turn, is a combinator.
//

#pragma once

#include "SearchExportDefs.hpp"

#include <cstdint>  // for int64_t

#include "BaseBacktrackManager.hpp"
#include "BaseSearch.hpp"
#include "SearchDefs.hpp"

// Forward declarations
namespace Core {
  class Constraint;
}

namespace Search {
  
  class SEARCH_EXPORT_CLASS DepthFirstSearch : public BaseSearch {
	public:
		using SemanticPtr = std::unique_ptr<SearchSemantics>;
		using ConstraintSPtr = std::shared_ptr<Core::Constraint>;

  public:
		DepthFirstSearch(SemanticPtr aSemantics, EnvironmentList& aEnvironmentList);
    
    /// Adds "aConstraint" to the semantics of this engine.
    /// This allows clients to add constraints at run time
    void registerConstraint(const ConstraintSPtr& aConstraint);
    
    /// Returns dfs metrics
    std::shared_ptr<Base::Tools::MetricsRegister> getMetrics() override;
    
  protected:
		/// Setup code called before labelTree,
		/// i.e., before search and labeling of variables
		void setupLabeling() override;

    /// Cleanup code called after labelTree,
    /// i.e., after search and labeling of variables
    void cleanupLabeling() override;
    
    /// Label the search tree defined on the
    /// given search environment by the given
    /// search semantics.
    /// Returns the status of the search
    SearchStatus labelTree(Node* aRootNode) override;
    
  private:
    /// Variable recording the number of explored nodes
    int64_t pNumNodes;
    
    /// Variable recording the number of failures
    int64_t pNumFailures;
    
    /// Variable recording the max depth of the search tree
    int64_t pMaxDepth;
    
    /// Backtrack manager to restore the space
		std::unique_ptr<BaseBacktrackManager> pBacktrackManager;
    
    /// Initialize backtrack semantics by registering
    /// the backtrackable objects into the backtrack manager
    void initBacktrackSemantics();
    
    /// Performs DepthFirst search on the tree rooted at "aNode"
    SearchStatus dfs(Node* aNode);
    
    /// Post a new objective constraint to find better solutions
    /// with respect the current value of the objective variable
    inline void postObjectiveConstraint()
    {
      assert(searchSemantics()->hasVariableObjective());
      searchSemantics()->optimizeObjective();
    }
    
    inline void postNogoodConstraint(const std::vector<Core::VariableSPtr>& aNogoodVars,
                                     const std::vector<Core::DomainSPtr>& aNogoodDoms)
    {
      searchSemantics()->storeNogood(aNogoodVars, aNogoodDoms);
    }//postNogoodConstraint
 
    inline Semantics::StoreConsistencyStatus runConsistency()
    {
      return searchSemantics()->constraintStore()->runConsistency();
    }
    
    /// Saves the current local minima to be used as a base line
    /// for the next optimization constraint during optimization search
    void saveCurrentMinima() const;
    
    /// Store nogoods and perform nogood learning.
    /// See also:
    /// Nogood Recording from Restarts
    /// Christophe Lecoutre et al., IJCAI-07
    void storeNogoods(const Node* aNode);
  };
  
}// end namespace Search
