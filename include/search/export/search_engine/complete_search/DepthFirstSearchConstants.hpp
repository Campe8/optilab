//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 12/10/2018
//

#pragma once

namespace Search {
  namespace DFSMetrics {
    
    extern const char* NUM_FAILURES;
    extern const char* NUM_NODES;
    extern const char* MAX_DEPTH;
    
  }// end namespace DFSMetrics
}// end namespace Search
