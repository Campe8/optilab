//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 09/13/2018
//
// Genetic algorithm applied to CBLS.
//

#pragma once

#include "SearchExportDefs.hpp"
#include "SearchDefs.hpp"

// Base class
#include "LocalBaseSearch.hpp"

#include <cstdint>  // for int64_t

#include <cstdio>   // for std::size_t
#include <memory>   // for std::shared_ptr
#include <utility>  // for std::pair
#include <vector>

// Forward declarations
namespace Core {
  class DomainElement;
}//end namespace Core

namespace Search {
  
  class SEARCH_EXPORT_CLASS GeneticSearch : public LocalBaseSearch {
	public:
		using SemanticPtr = std::unique_ptr<SearchSemantics>;
		using ConstraintSPtr = std::shared_ptr<Core::Constraint>;

  public:
    /// Constructor for GeneticSearch.
    /// Search configuration parameters (e.g., population size) are stored
    /// in the environment list
    GeneticSearch(SemanticPtr aSemantics, EnvironmentList& aEnvironmentList);
    
    /// Returns minimum size for population
    static std::size_t getGeneticMinimumPopulationSize();
    
    /// Returns maximum size for population
    static std::size_t getGeneticMaximumPopulationSize();
    
    /// Returns maximum number of generations
    static std::size_t getGeneticMaximumGenerations();
    
    /// Returns dfs metrics
    std::shared_ptr<Base::Tools::MetricsRegister> getMetrics() override;
    
  protected:
    /// Constructor:
    /// - aSemantics: semantics used by this local base search
    /// - aEnvironmentList: list of environments used by this local base search
    /// - aSizeNeighbhorhoods: size of each large neighborhood used by this local base search
    /// - aNumNeighborhoods: number of large neighborhoods used by this local base search
    /// - aPopulationSize: size of the initial population
    /// - aNumGenerations: maximum number of evolving generations
    /// - aMutationChance: a number between 0 and 100 that represents
    ///                    the probability of a random mutation after the crossover
    /// - aMutationSize: number of genes to randomly mutate on each individual.
    ///                  it must be between 1 and the size of the neighborhood (default is 1)
    /// - aRunAggressiveSearch: if false, store all improving assignments. If true
    ///                         store only improving solutions
    /// @note using two parents to produce every child of the next generation
    /// @note in what follows "fitness" and "cost" are used as synonyms
    /// @note this is a protected constructor for one-time intialization.
    ///       The constructor used by the SolverEngine doesn't set the values of the search
    ///       but those values (e.g., population size) are set during initialization
    ///       of the current environment
		GeneticSearch(SemanticPtr aSemantics,
                  EnvironmentList& aEnvironmentList,
                  std::size_t aSizeNeighborhoods,
                  std::size_t aNumNeighborhoods,
                  std::size_t aPopulationSize,
                  std::size_t aNumGenerations,
                  std::size_t aMutationChance,
                  std::size_t aMutationSize,
                  bool aRunAggressiveSearch);
    
    /// Applies genetic algorithm to the given neighborhood.
    /// Returns the status of the search
    SearchStatus neighborhoodSearch(Neighborhood* aNeighborhood) override;
    
    /// Reset internal state
    void resetNeighborhoodSearchState(bool aWarmStart) override;
    
    /// Init genetic search
    /// @note throws if "aEnv" is not a GeneticSearchEnvironment
    void initLocalSearchEnvironment(LocalSearchEnvironment* aEnv) override;
    
  private:
    /// An individual is an ordered assignment of values to variables,
    /// i.e., a list of domain elements.
    /// A population is a list of individuals.
    /// @note for each Individual there are two lists of assignments, the first is for the variable
    /// that are part of the neighborhood under exploration, and the second is for the variables
    /// that are part of the objective (i.e., objective variables).
    /// @note the objective variables can also be part of the current neighborhood under exploration
    using DomainElementList = std::vector<Core::DomainElement*>;
    using Individual = std::pair<DomainElementList, DomainElementList>;
    using Population = std::vector<Individual>;
    
    /// The cost of an individual is represented by its actual cost
    /// and the index in the Population where the individual resides
    using IndividualCost = std::pair<NeighborhoodCost, std::size_t>;
    using PopulationCost = std::vector<IndividualCost>;
    
  private:
    /// Flag indicating whether the current run is a warm start or not
    bool pWarmStart;
    
    /// Variable to record the number of generation explored before returning
    int64_t pGenerationCount;
    
    /// Number of improving populations
    int64_t pImprovingPopulationsCount;
    
    /// Index on the list of populations indicating which
    /// population is currently active
    std::size_t pActivePopulation;
    
    /// Size of the population
    std::size_t pPopulationSize;
    
    /// Number of generations to produce
    std::size_t pNumGenerations;
    
    /// Probability of mutation after the crossover
    std::size_t pMutationChance;
    
    /// Number of genes to randomly mutate
    std::size_t pMutationSize;
    
    /// Best fitness found so far
    NeighborhoodCost pBestFitness;
    
    /// List of lists of assignments representing the current population
    std::vector<Population> pPopulationList;
    
    /// List of lists of the cost of each individual
    std::vector<PopulationCost> pPopulationCostList;
    
    /// Pointer to the current active neighborood
    Neighborhood* pActiveNeighborhood;
    
    /// Utility function: returns the pointer to the neighborhood the genetic search works on
    /// i.e., the neighborhood passed to the neighborhoodSearch method
    inline Neighborhood* getActiveNeighborhood() const { return pActiveNeighborhood; }
    
    /// Initializes genetic algorithm parameters
    void initGeneticParameters();
    
    /// Generates the initial population of (random) individuals.
    /// Each individual has a cost.
    /// While generating individuals, it stores the cost in
    /// the population cost list
    void generatePopulation();
    
    /// Generates a random individual and stores
    /// the elements of in the given individual.
    /// Returns the cost of the generated individual
    NeighborhoodCost generateRandomIndividual(Individual& aIndividual);
    
    /// Sorts the population by increasing fitness
    void sortPopulation();
    
    /// Evolves the current population into the
    /// next generation of individuals
    void nextGeneration();
    
    /// Generate a pair of individuals by merging the two parents
    /// on the crossover point
    Population reproduce(const Individual& aParent1, const Individual& aParent2, int aCrossoverPoint);
    
    /// Perform a random mutation of the genes of an individual
    /// following the probability "pMutationChance"
    void mutateGenes(Individual& aIndividual);
    
    /// Returns the current active population
    inline Population& getActivePopulation() { return pPopulationList[pActivePopulation]; }
    
    /// Returns the current active population cost
    inline PopulationCost& getActivePopulationCost() { return pPopulationCostList[pActivePopulation]; }
    
    /// Returns the reference to the population that will be active
    /// next, i.e., after the current active population
    Population& getNextActivePopulation();
    
    /// Returns the reference to the population cost that will be active
    /// next, i.e., after the current active population cost
    PopulationCost& getNextActivePopulationCost();
    
    /// Changes the current active population to the next one
    void changeActivePopulation();
    
    /// Returns the indeces of the parents used for reproduction
    /// in order to generate an individual.
    /// @note default returns two parents
    std::vector<std::size_t> selectParents(std::size_t aNumParents=2);
    
    /// Returns the cost, a.k.a., fitness of the given individual.
    /// @note the cost is calculated by running fitness propagation on the neighborhood
    /// that has the values given by the individual. This propagation will automatically assign
    /// values to the objective variables. This values are set in the given individual instance.
    /// In other words, the workflow is the following:
    /// 1) use individual neighborhood domain elements to "label the variables in the neighborhood;
    /// 2) run propagation and fitness consistency;
    /// 3) use the values given by propagation to set the individual optimization domain elements;
    /// 4) return the cost of the current assignment.
    /// @note for each individual there is an optimization cost associated with it (in case of
    /// optimization problems)
    NeighborhoodCost calculateIndividualFitness(Individual& aIndividual);
    
    /// Sets the given individual on the current active neighborhood.
    /// @note if "aSetObjIndividual" is true, this method sets the individual's objective values,
    /// otherwise sets only the neighborhood's values
    void setIndividualOnNeighborhood(const Individual& aIndividual, bool aSetObjIndividual);
  };
  
}// end namespace Search
