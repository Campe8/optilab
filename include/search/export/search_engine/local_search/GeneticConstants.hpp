//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 12/10/2018
//

#pragma once

#include <cstdio>  // for std::size_t

namespace Search {
  namespace GeneticMetrics {
    
    extern const char* NUM_BEST_UPDATES;
    extern const char* NUM_GENERATIONS;
    
  }// end namespace GeneticMetrics
  
  namespace GeneticLimits {
    
    extern const std::size_t MAX_GENERATIONS;
    extern const std::size_t MAX_POPULATION_SIZE;
    extern const std::size_t MIN_POPULATION_SIZE;
    
  }// end namespace GeneticLimits
  
}// end namespace Search
