//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 02/04/2017
//
// Base class for search engines.
//

#pragma once

#include <memory>  // for std::shared_ptr

#include "SearchExportDefs.hpp"
#include "SearchDefs.hpp"

// Search strategy used by the search engine
#include "SearchStrategy.hpp"

namespace Search {
  
  class SEARCH_EXPORT_CLASS SearchEngine {
  public:
    using SPtr = std::shared_ptr<SearchEngine>;
    
  public:
    /// Constructor: constructs the a search engine
    /// on the given search strategy which will be used to label the search tree
    SearchEngine(std::unique_ptr<SearchStrategy> aSearchStrategy);
    
    virtual ~SearchEngine() = default;
    
    /// Labels the search tree rooted at "aRootNode" using
    /// the search strategy defining this search engine.
    /// Returns the status of the search
    SearchStatus label(CombinatorNode* aRootNode);
    
  private:
    /// Search strategy defining by this search engine
    SearchStrategy::SPtr pSearchStrategy;
  };
  
}// end namespace Search
