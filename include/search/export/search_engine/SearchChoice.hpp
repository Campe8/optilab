//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 09/11/2018
//
// SearchChoice represents the base class for
// any choice made during a search process.
// This base class is extended by nodes of the
// search tree and by neighborhoods of the
// local search strategies.
//
//           +--------------+
//           | SearchChoice |
//           +-------+------+
//                   |
//      +------------+------------+
//      |                         |
//   +--+---+             +-------+------+
//   | Node |             | Neighborhood |
//   +------+             +--------------+
//

#pragma once

#include "SearchExportDefs.hpp"
#include "SearchDefs.hpp"

namespace Search {
  
  class SEARCH_EXPORT_CLASS SearchChoice {
  public:
    SearchChoice(SearchChoiceType aType);
    
    virtual ~SearchChoice() = default;
    
    /// Returns the type of this choice
    inline SearchChoiceType getSearchChoiceType() const { return pType; }
    
  private:
    /// Type fo this choice
    SearchChoiceType pType;
  };
  
}// end namespace Search
