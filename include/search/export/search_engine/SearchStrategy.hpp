//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 1/16/2018
//
// Base class for search strategies.
// A search strategy is a search combinator.
//

#pragma once

#include "Combinator.hpp"

#include <memory>  // for std::shared_ptr

#include "SearchDefs.hpp"

namespace Search {
  
  class SEARCH_EXPORT_CLASS SearchStrategy : public Combinator {
  public:
    using SPtr = std::shared_ptr<SearchStrategy>;
    
  public:
		/// Constructor: constructs a new search strategy based on the given combinator
		SearchStrategy(const CombinatorSPtr& aSearch);

		virtual ~SearchStrategy() = default;

		/// Combinator interface: start 
		void start(CombinatorNode* aRoot) override;

		/// Combinator interface: init 
		void init(CombinatorNode* aParent, CombinatorNode* aChild) override;

		/// Combinator interface: exit 
		void exit(CombinatorNode* aNode) override;

	private:
		/// Search combinator defining this search strategy
		CombinatorSPtr pSearch;
  };
  
}// end namespace Search
