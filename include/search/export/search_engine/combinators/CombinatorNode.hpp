//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 1/16/2018
//
// CombinatorNode base class.
// CombinatorNodes are the parameters of the message protocol
// specified by a combinator. Each such node keeps track of a
// Solver State and the information associated by combinators
// to that State.
// In particular, a combinator node acts as a wrapper around
// a SearchChoice by holding additional info used by search
// combinators.
// For more information see
// https://arxiv.org/abs/1203.1095
//

#pragma once

#include "SearchExportDefs.hpp"
#include "SearchDefs.hpp"

#include "Node.hpp"
#include "Neighborhood.hpp"

#include <memory>

namespace Search {
  
  class SEARCH_EXPORT_CLASS CombinatorNode {
	public:
		/// Constructor: creates a new combinator node around the given search node
		CombinatorNode(std::unique_ptr<SearchChoice> aChoice);

		virtual ~CombinatorNode() = default;

		/// Sets the status of the search on this combinator node
		inline void setStatus(SearchStatus aStatus)
		{
			pSearchStatus = aStatus;
		}

		/// Returns the status of the search of this combinator node
		inline SearchStatus getStatus() const
		{
			return pSearchStatus;
		}
    
    /// Returns true if the search choice is set,
    /// false otherwise
    inline bool isSearchChoiceSet() const { return pSearchChoice != nullptr; }

		/// Returns the search node contained in this combinator node
    /// or nullptr if the search choice is not a search node
		inline Node* searchNode() const
		{
      return Node::cast(pSearchChoice.get());
		}
    
    /// Returns the search neighborhood contained in this combinator node
    /// or nullptr if the search choice is not a neighborhood
    inline Neighborhood* searchNeighborhood() const
    {
      return Neighborhood::cast(pSearchChoice.get());
    }
    
		/// Resets internal search node to "aNode"
		void resetSearchChoice(std::unique_ptr<SearchChoice> aChoice);

	private:
		/// Status of the search hold by this node
		SearchStatus pSearchStatus;

		/// Search node hold by this combinator node
		std::shared_ptr<SearchChoice> pSearchChoice;
	};
  
}// end namespace Search
