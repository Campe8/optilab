//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 1/16/2018
//
// Search combinator base class.
// A combinator represents a "message protocol", 
// an interface that allows combinators to be "combined"
// in order to define modular search strategies.
// In other words, these objects combine search heuristics
// (which can be basic or themselves constructed using combinators)
// into more complex heuristics.
// For more information see
// Search Combinators, by Schrijvers et al. or follow the link:
// https://arxiv.org/abs/1203.1095
//

#pragma once

#include "SearchExportDefs.hpp"

#include <memory>

// Forward declarations
namespace Search {
	class Combinator;
	class CombinatorNode;

	using CombinatorSPtr = std::shared_ptr<Combinator>;
}// end namespace Search

namespace Search {
  
  class SEARCH_EXPORT_CLASS Combinator {
  public:
		Combinator();

		virtual ~Combinator() = default;

		/// Start message needs a root node
		virtual void start(CombinatorNode*) {};

		/// Enter message needs the current node
		virtual void enter(CombinatorNode*) {};

		/// Exit message needs the current node
		virtual void exit(CombinatorNode*) {};

		/// Init message needs parent and child node
		virtual void init(CombinatorNode*, CombinatorNode*) {};

		/// Sets parent combinator
		inline void setParentCombinator(Combinator* aParent)
		{
			pParent = aParent;
		}

		/// Returns parent combinator
		inline Combinator* getParentCombinator() const
		{
			return pParent;
		}

		/// Sets top combinator
		inline void setRootCombinator(Combinator* aRoot)
		{
			pRoot = aRoot;
		}

		/// Returns top combinator
		inline Combinator* getRootCombinator() const
		{
			return pRoot;
		}

	private:
		/// Parent combinator
		Combinator* pParent;

		/// Top-Root combinator
		Combinator* pRoot;
  };
  
}// end namespace Search
