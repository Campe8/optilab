//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 1/16/2018
//
// BaseSearch is the base class for a search strategy
// working on variables and heuristics for variable
// and domain selection.
// Note that a base search is an instance of a combinator.
//

#pragma once

// Base class
#include "BaseSearchScheme.hpp"

// Utilities
#include "Timer.hpp"

// Forward declarations
namespace Search {
	class Node;
}// end namespace Search

namespace Search {
  
	class SEARCH_EXPORT_CLASS BaseSearch : public BaseSearchScheme {
	public:
		BaseSearch(BaseSearch&) = delete;
		BaseSearch& operator=(BaseSearch&) = delete;

		virtual ~BaseSearch() = default;
    
		/// Combinator interface: start the search on the root node
    /// generated by "getRootNode"
		void start(CombinatorNode* aRootNode) override;

		/// Combinator interface: exit
		void exit(CombinatorNode* aCurrNode) override;

	protected:
		/// Constructor
		/// Semantic: contains the (search) semantic for this base search,
		///           i.e., the constraint store, the goal, etc.
		/// aEnvironmentList: the list of environments passed to this base search,
		///           i.e., the list of sets of variables to branch on, heuristics, etc.
		BaseSearch(SemanticPtr aSemantics,
			         EnvironmentList& aEnvironmentList,
			         SearchEngineStrategyType aSearchEngineType);

		/// Label the search tree rooted at "RootNode " and defined on the
		/// given search environment by the given search semantics.
		/// Returns the status of the search.
    /// @note resets the content of the solution manager before labeling
		SearchStatus label(Node* aRootNode);

		/// Setup code called before labelTree,
		/// i.e., before search and labeling of variables
		virtual void setupLabeling() {};

		/// Cleanup code called after labelTree,
		/// i.e., after search and labeling of variables
		virtual void cleanupLabeling() {};

		/// Method called by "label" after
		/// calling "setupLabeling" and before
		/// calling "cleanupLabeling"
		virtual SearchStatus labelTree(Node* aRootNode) = 0;

    /// Create a new root combinator node wrapping a
    /// rootNode for a complete search.
    /// The root node is the root of the search tree
    CombinatorNode* createRootCombinatorNode(SearchEnvironment* aEnv, SearchSemantics* aSem) override;
    
    /// Resets and starts the timer for timeout
    /// @note this method simply returns if the timeout value is less than zero
    void resetTimer();
    
    /// Returns true if the search is in timeout, false otherwise
    inline bool searchTimeout() const
    {
      return pTimer ? (pTimer->getWallClockTimeSec() >= pTimeout) : false;
    }
    
  private:
    /// Timeout for the search in seconds
    int pTimeout;
    
    /// Timer for search
    std::unique_ptr<Base::Tools::Timer> pTimer;
	};

}// end namespace Search
