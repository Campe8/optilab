//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 02/02/2017
//

#pragma once

namespace Search {
  
  //============ Combinators =============//
  
  enum SearchChoiceType : short {
      SEARCH_CHOICE_NODE = 0
    , SEARCH_CHOICE_NEIGHBORHOOD
  };
  
  //============== Search ================//
  
  enum SearchStatus : int {
      SEARCH_SUCCESS = 0  // search succeeded with solutions
    , SEARCH_TERMINATE    // search forced to be terminated
    , SEARCH_FAIL         // search failed - no solutions
    , SEARCH_FORCED_FAIL  // search forced to fail - e.g., find another solution
    , SEARCH_UNDEF
  };
  
  // Search strategies
  enum SearchEngineStrategyType : int {
      SE_DEPTH_FIRST_SEARCH = 0
    
    , SE_CBLS_STRATEGIES  // After this value cbls only
    
    , SE_GENETIC
    , SE_UNDEF // must be last
  };
  
  //========= Solution Manager ===========//
  
  enum SolutionStatus : int {
      SOLUTION_NOT_VALID = 0  // used to communicate that the current
                              // domains of the branching variables
                              // do not represent a solution
    , SOLUTION_LAST           // the current solution is the last solution
    , SOLUTION_NEED_MORE      // need more solutions
  };
  
  //========= Search Backtrack ===========//
  
  enum MementoStateId : int {
      MS_DOMAIN = 0
    , MS_SEARCH_HEURISTIC
    , MS_CONSTRAINT_STORE
    , MS_UNDEF
  };
  
} // end namespace Core

