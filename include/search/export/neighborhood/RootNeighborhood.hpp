//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 09/11/2018
//
// Root neighborhood of a CBLS strategy.
//

#pragma once

// Base class
#include "Neighborhood.hpp"
#include "SearchHeuristic.hpp"
#include "BaseTools.hpp"

namespace Search {

  class SEARCH_EXPORT_CLASS RootNeighborhood : public Neighborhood {
  public:
    /// Constructor needs a search heuristic which, in turn,
    /// is defined on a set of branching variables.
    /// This neighborhood is the root neighborhood
    /// defined on the search space represented by the set
    /// of branching variables given by the search heuristic.
    /// @note the search heuristic should be consistent with the
    /// selection of the values for the variables in the neighborhood.
    /// For example, it is usually the case where the variables in
    /// the search space are labeled with random values from their domains
    RootNeighborhood(const SearchHeuristicSPtr& aSearchHeuristic,
                     std::size_t aSizeNeighborhoods,
                     std::size_t aNumNeighborhoods,
                     const Base::Tools::RandomGeneratorSPtr& aRandomGenerator);

    virtual ~RootNeighborhood() = default;
    
  private:
    /// Search space
    NeighborhoodSearchSpace pSearchSpaceRoot;
    
    /// Search heuristic used for defining the neighborhoods.
    /// @note a copy is hold here to avoid heuristic to 
    /// be deleted before this root neighborhood is deleted
    SearchHeuristicSPtr pSearchHeuristicSPtr;
    
    /// Pointer to the random generator instance to use for selecting the neighborhoods
    Base::Tools::RandomGeneratorSPtr pRandomGenerator;
  };

}// end namespace Search
