//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 09/11/2018
//
// Neighborhood choice in a CBLS strategy.
// A neighborhood n = {v1, v2, ..., vn } is a set of variables
// where each variable vi belongs to the search environment.
// This set represents the relation on the search space encoding
// all the positions reachable in one search step.
// The set of variables is split into large neighborhoods
// and used to perform Large Neighborhood Search, or LNS.
// The search process is applied on each large neighborhood
// and the one leading to the best local optima is retained.
// For more information see, for example:
// Multi-Objective Large Neighborhood Search
// Pierre Schaus and Renaud Hartert, CP13.
//

#pragma once

// Base class
#include "SearchChoice.hpp"

#include "Variable.hpp"
#include "SearchHeuristic.hpp"

// Forward declarations
namespace Base { namespace Tools {
  class RandomGenerator;
}}// end namespace Tools/Base

namespace Search {
  
  /// Search space for neighborhood.
  /// It represents the set of variables the neighborhood is choosen from
  using NeighborhoodSearchSpace = std::vector<Core::VariableSPtr>;
  
  /// Specification of the neighborhood.
  /// It represents the set of indeces in the NeighborhoodSearchSpace
  /// for the variables representing that are part of the
  /// current neighborhood
  using NeighborhoodSpec = std::vector<std::size_t>;
  
  /// Neighborhood selector function used to select the next neighborhood.
  /// Input:
  /// - the search space
  /// - the size of the neighborhood to generate
  /// - The random generator function to use
  /// Output:
  /// - a NeighborhoodSpec
  typedef NeighborhoodSpec (*NeighborhoodSelector)(
  NeighborhoodSearchSpace*, std::size_t, Base::Tools::RandomGenerator*);
  
  namespace Internal {
    
    /// Selector for generating random neighborhoods
    NeighborhoodSpec randomNeighborhoodSelector(NeighborhoodSearchSpace* aSearchSpace,
                                                std::size_t aSize,
                                                Base::Tools::RandomGenerator* aRandomGenerator);
    
  }//end namespace internal
  
  class SEARCH_EXPORT_CLASS Neighborhood : public SearchChoice {
  public:
    virtual ~Neighborhood() = default;
    
    static bool isa(const SearchChoice* aChoice);
    static Neighborhood* cast(SearchChoice* aChoice);
    static const Neighborhood* cast(const SearchChoice* aChoice);
    
    /// Returns the search heuristic used by this neighborhood
    inline const SearchHeuristicPtr getSearchHeuristic() const { return pSearchHeuristic; }
    
    /// Returns the search space this neighborhood is defined on
    inline const NeighborhoodSearchSpace* getSearchSpace() const { return pSearchSpace; }
    
    /// Returns the next neighborhood of the set of large neighborhoods.
    /// Returns nullptr if this neighborhood is the last one
    Neighborhood* next();
    
    /// Returns true if this neighborhood has at least one successor.
    /// Returns false otherwise
    bool hasSuccessor();
    
    /// Returns the list of variables that are part of the current neighborhood
    NeighborhoodSearchSpace getNeighborhood() const;
    
  protected:
    /// Base Constructor
    Neighborhood(std::size_t aNeighborhoodId,
                 std::size_t aNumNeighborhoods,
                 NeighborhoodSelector aNeighborhoodSelector,
                 Base::Tools::RandomGenerator* aRandomGenerator);
    
    /// Constructor:
    /// - aSearchSpace: set of variables that are part are used to
    ///                 generate the neighborhood;
    /// - aSearchHeuristic: search heuristic to use for choosing the values
    ///                     for the variables in the neighborhood;
    /// - aNeighborhoodSpec: list of indeces on the search space
    ///                      of the variables defining this neighborhood;
    /// - aNeighborhoodId: id for this neighborhood, it is a value
    ///                    between 0 and the total number of neighborhoods;
    /// - aNumNeighborhoods: total number of neighborhoods.
    /// - aRandomGenerator: random number generator
    /// @note protected constructor,
    /// only root neighborhood can be instantiated
    Neighborhood(NeighborhoodSearchSpace* aSearchSpace,
                 SearchHeuristicPtr aSearchHeuristic,
                 const NeighborhoodSpec& aNeighborhoodSpec,
                 std::size_t aNeighborhoodId,
                 std::size_t aNumNeighborhoods,
                 NeighborhoodSelector aNeighborhoodSelector,
                 Base::Tools::RandomGenerator* aRandomGenerator);
    
    inline void setSearchSpace(NeighborhoodSearchSpace* aSearchSpace)
    {
      pSearchSpace = aSearchSpace;
    }
    
    inline void setSearchHeuristic(SearchHeuristicPtr aSearchHeuristic)
    {
      pSearchHeuristic = aSearchHeuristic;
    }
    
    /// Extends this node by calculating the neighborhood variables
    /// belonging to this neighborhood
    void extendNeighborhood();
    
    /// Sets the neighborhood spec
    void setNeighborhoodSpec(const NeighborhoodSpec& aNeighborhoodSpec);
    
    /// Returns this neighborhoodSpec
    inline const NeighborhoodSpec& getNeighborhoodSpec() const { return pNeighborhoodSpec; }
    
  private:
    /// Id of this neighborhood
    std::size_t pNeighborhoodId;
    
    /// Size of the set of neighborhoods.
    /// It corresponds to the total number of neighborhoods
    std::size_t pNeighborhoodSetSize;
    
    /// Random number generator
    NeighborhoodSelector pNeighborhoodSelector;
    
    /// Indices for the variables in this neighborhood
    NeighborhoodSpec pNeighborhoodSpec;
    
    /// Set of variables defining the search space
    NeighborhoodSearchSpace* pSearchSpace;
    
    /// List of variables that are part of the neighborhood
    mutable NeighborhoodSearchSpace pNeighborhoodVars;
    
    /// Search heuristic used on this node
    SearchHeuristicPtr pSearchHeuristic;
    
    /// Extended neighborhood
    NeighborhoodSpec pNextNeighborhoodSpec;
    
    /// Pointer to the random number generator
    Base::Tools::RandomGenerator* pRandomGenerator;
    
    /// Returns true if this neighborhood has been extended,
    /// false otherwise
    inline bool isNeighborhoodExtended() const { return !pNextNeighborhoodSpec.empty(); }
  };
  
}// end namespace Search
