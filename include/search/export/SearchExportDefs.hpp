#pragma once

#include "dll_export.h"

/* This header is being included by files inside this module */

#define SEARCH_EXPORT_CLASS      DLL_EXPORT_SYM
#define SEARCH_EXPORT_STRUCT     DLL_EXPORT_SYM
#define SEARCH_EXPORT_FRIEND     DLL_EXPORT_SYM
#define SEARCH_EXPORT_FUNCTION   DLL_EXPORT_SYM
#define SEARCH_EXPORT_TEMPLATE   DLL_EXPORT_TEMPLATE
#define SEARCH_EXPORT_EXTERN_C   extern "C" DLL_EXPORT_SYM

