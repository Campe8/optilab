//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 01/31/2017
//
// Root node of the search tree that is going to
// be explored starting from the given branching
// variables set.
//

#pragma once

// Base class
#include "Node.hpp"

namespace Search {

  class SEARCH_EXPORT_CLASS RootNode : public Node {
  public:
    /// Constructor need a search heuristic which, in turn,
    /// is defined on a set of branching variables.
    /// This node is the root node of the search tree
    /// that is explored by branching on the variables
    /// defining the given search heuristic
    RootNode(const SearchHeuristicSPtr& aSearchHeuristic,
             const Semantics::ConstraintStoreSPtr& aConstraintStore);

    virtual ~RootNode();
    
  private:
    /// Branching path starting from the root node
    std::unique_ptr<BranchingPath> pBranchingPathRoot;

    /// Search heuristic used on the search tree rooted
    /// at this node.
    /// @note a copy is hold here to avoid heuristic to 
    /// be deleted before this node is deleted
    SearchHeuristicSPtr pSearchHeuristic;

    /// Strategy register
    std::shared_ptr<BranchingStrategyRegister> pBranchingStrategyRegister;
    
    /// Deregisters all registered constraints from the constraint store,
    /// if any, deallocates memory and cleans up the search tree rooted
    /// at this Node
    void cleanupTree();
  };

}// end namespace Search
