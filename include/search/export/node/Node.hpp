//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 01/20/2017
//
// Search tree node.
// A node p = {b1, ... bj} in the search tree is a
// set of *branching constraints*, where bi 1 <= i <= j,
// is a branching constraint at level i in the search tree.
// For example:
// level    node    set
// 1        p1      { b1 }
// 2        p2      { b1, b2 }
// ...      ...     ...
// j        pj      { b1, b2, ..., bi, ..., bj  }
// A node pj is extended by adding the branches
//      pj U {b{j+1}^1}, ..., pj U {b{j+1}^k}
// for some branching constraints b_{j+1}^i, 1 <= i <= k.
// A *value ordering heuristic* chooses the next branch and hence,
// if {b{j+1}^t} is the chosen branch,
// pj+1 = pj U {b{j+1}^t} = { b1, b2, ..., bj, b{j+1}^t }.
//

#pragma once

// Base class
#include "SearchChoice.hpp"

#include "Variable.hpp"
#include "SearchHeuristic.hpp"

// Branching strategy
#include "BranchingStrategyRegister.hpp"
#include "BranchingConstraint.hpp"

// Constraint store
#include "ConstraintStore.hpp"

#include <map>
#include <utility>
#include <memory>

// Forward declarations
namespace Search {
  class Node;
  
  typedef std::unique_ptr<Node> NodeUPtr;
  typedef std::shared_ptr<Node> NodeSPtr;
}// end namespace Search

namespace Search {
  
  /// Map containing the set of branching constraints
  /// posted at each level of the search tree.
  /// The map contains pairs:
  /// <j, bj>
  /// where j is the level of the search tree and bj the
  /// branching constraint posted at level j.
  /// The path represents the different choices made during search
  typedef std::map<std::size_t, Core::BranchingConstraintSPtr> BranchingPath;
  typedef BranchingPath* BranchingPathPtr;
  
  /// Branching set, i.e., the set of all the branches
  /// extending this constraint.
  /// This set is ordered with the left-most branch being
  /// the most promising
  typedef std::vector<Core::BranchingConstraintSPtr> BranchingSet;
  
  /// Degree id, i.e., a pair where the first element is
  /// the branch sequence number (starting from 1) in the
  /// ordered branching set the node is deriving from,
  /// and the second element is the total number of branches,
  /// i.e., the branching degree of the extended node
  typedef std::pair<std::size_t, std::size_t> NodeDegreeId;
  
  class SEARCH_EXPORT_CLASS Node : public SearchChoice {
  public:
    virtual ~Node();
    
    static bool isa(const SearchChoice* aChoice);
    static Node* cast(SearchChoice* aChoice);
    static const Node* cast(const SearchChoice* aChoice);
    
    /// Returns the next node of the search tree,
    /// i.e., extends this node with the branching
    /// constraint chosen according to the value ordering heuristic
    /// applied to the variable chosen by the variable ordering heuristic
    /// applied to the level j+1 where j is the current level.
    /// For example, if this is node pj, then
    /// pj.next() == pj+1.
    /// Returns nullptr if this node cannot be further extended
    Node* next();
    
    /// Returns true if this node has at least one successor,
    /// i.e., if this node can be extended with one or more branches.
    /// Returns false otherwise.
    /// In a search tree, this method is equivalent to checking
    /// whether this is a leaf node or not
    bool hasSuccessor();
    
    /// Returns true if this node is the rightmost node,
    /// i.e., it is attached to the rightmost branch of the
    /// parent node.
    /// Returns false otherwise
    inline bool isRightmostNode() const
    {
      return pDegreeId.first == pDegreeId.second;
    }
    
    inline const BranchingPathPtr getBranchingPath() const
    {
      return pBranchingPath;
    }
    
    /// Returns the tree level this node is at
    inline std::size_t getTreeLevel() const
    {
      return (pSearchTreeLevel == 0) ? 0 : (pSearchTreeLevel - 1);
    }
    
  protected:
    /// Default constructor
    Node(std::size_t aSearchTreeLevel);
    
    /// Constructor:
    /// - BranchingPathPtr: pointer to the current set
    ///                     of branching constraints
    ///                     representing this node;
    /// - aBranchingVar: variable to branch on when
    ///                  extending this node;
    /// - aSearchHeuristic: search heuristic to use for
    ///                     the branching variables defining
    ///                     the given branching path;
    /// - aBrcStrRegister: register for branching strategies;
    /// - aDegreeId: degree identifier for this node.
    /// @note protected constructor,
    /// only root node can be instantiated
    Node(BranchingPathPtr aBranchingPath,
         const Core::VariableSPtr& aBranchingVar,
         SearchHeuristicPtr aSearchHeuristic,
         BranchingStrategyRegisterPtr aBrcStrRegister,
         const Semantics::ConstraintStoreSPtr& aConstraintStore,
         NodeDegreeId aDegreeId,
         std::size_t aSearchTreeLevel);
    
    /// Returns true if this node has been already extended,
    /// i.e., if "extendNode" has been previously called
    inline bool isNodeExtended() const
    {
      return pNextBranchId > 0;
    }
    
    inline void setBranchingPath(BranchingPathPtr aBranchingPath)
    {
      pBranchingPath = aBranchingPath;
    }
    
    inline void setSearchHeuristic(SearchHeuristicPtr aSearchHeuristic)
    {
      pSearchHeuristic = aSearchHeuristic;
    }
    
    inline void setBranchingVariable(const Core::VariableSPtr& aBranchingVar)
    {
      pBranchingVar = aBranchingVar;
    }
    
    inline void setBranchingStrategyRegister(BranchingStrategyRegisterPtr aBrcStrRegister)
    {
      pBranchingStrategyRegister = aBrcStrRegister;
    }
    
    inline void setConstraintStore(const Semantics::ConstraintStoreSPtr& aConstraintStore)
    {
      pConstraintStore = aConstraintStore;
    }
    
    inline BranchingSet& getBranchingSet()
    {
      return pBranchingChoiceSet.second;
    }
    
    inline Core::VariableSPtr getNextBranchingVariable()
    {
      return pBranchingChoiceSet.first;
    }
    
    /// Extends this node by calculating all the branches
    /// belonging to the branching set
    void extendNode();
    
  private:
    /// Level of the search tree this node belongs to
    std::size_t pSearchTreeLevel;
    
    /// Index of the next branching choice.
    /// @note since it is "next" it is 1-based
    std::size_t pNextBranchId;
    
    /// Set of branching constraints defining the
    /// path from the root node to this node
    BranchingPathPtr pBranchingPath;
    
    /// Variable to branch on associated with this node.
    /// @note this variable is given by the
    /// variable ordering heuristic used for this node
    /// and it represents the actual variable used to
    /// create the set of branches extending this node
    Core::VariableSPtr pBranchingVar;
    
    /// Branching set containing the variable
    /// chosen for branching and all the branches
    /// extending this node
    Search::BranchingChoiceSet pBranchingChoiceSet;
    
    /// Search heuristic used on this node
    SearchHeuristicPtr pSearchHeuristic;
    
    /// Branching strategies register
    BranchingStrategyRegisterPtr pBranchingStrategyRegister;
    
    /// Constraint store the branching constraint representing
    /// node will be registered in
    Semantics::ConstraintStoreSPtr pConstraintStore;
    
    /// Degree id of this node
    NodeDegreeId pDegreeId;
    
    /// Extends current branching path with
    /// the given branching constraint.
    /// @note this method registers the given branching
    /// constraint into the constraint store
    void extendBranchingPath(const Core::BranchingConstraintSPtr& aBranchingConstraint);
    
    /// Returns next branching constraint according
    /// to the value ordering heuristic used on this node.
    /// Returns nullptr if there are no more branching constraints
    /// Branching constraints are ordered with the left-most branch
    /// being the most promising
    inline Core::BranchingConstraintSPtr getNextBranchingConstraint()
    {
      assert(pNextBranchId - 1 < getBranchingSet().size());
      return getBranchingSet().at(pNextBranchId - 1);
    }
  };
  
}// end namespace Search
