//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 10/02/2018
//
// Search environment for genetic search.
//

#pragma once

// Base class
#include "LocalSearchEnvironment.hpp"

namespace Search {
  
  class SEARCH_EXPORT_CLASS GeneticSearchEnvironment : public LocalSearchEnvironment {
  public:
    GeneticSearchEnvironment(const std::vector<VarSem>& aEnvironment,
                             int aSizeNeighborhoods,
                             int aNumNeighborhoods,
                             bool aRunAggressiveSearch,
                             const std::string& aEnvId="");
    
    ~GeneticSearchEnvironment() = default;
    
    /// Sets size of population
    void setPopulationSize(std::size_t aPopSize);
    
    /// Sets number of generations
    void setNumGenerations(std::size_t aNumGen);
    
    /// Sets mutation chance
    void setMutationChance(std::size_t aMutChance);
    
    /// Sets mutation size
    void setMutationSize(std::size_t aMutSize);
    
    /// Returns size of the population
    inline std::size_t getPopulationSize() const { return pPopulationSize; }
    
    /// Returns number of generations
    inline std::size_t getNumGenerations() const { return pNumGenerations; }
    
    /// Returns the probability of mutation
    inline std::size_t getMutationChance() const { return pMutationChance; }
    
    /// Returns the number of genes to mutate
    inline std::size_t getMutationSize() const { return pMutationSize; }
    
  private:
    /// Size of population, i.e., number of individuals
    std::size_t pPopulationSize;
    
    /// Number of generation
    std::size_t pNumGenerations;
    
    /// Chance of mutation of genes
    std::size_t pMutationChance;
    
    /// Number of genes to randomly mutate
    std::size_t pMutationSize;
    
  };
  
}// end namespace Search
