//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 02/04/2017
//
// Environment of a search engine.
// The environemnt holds the information about the heuristic to use,
// the variables to branch on, and all the meta information related to
// "how" to perform the search.
//

#pragma once

#include "SearchExportDefs.hpp"

#include "Variable.hpp"
#include "SearchHeuristic.hpp"
#include "SearchDefs.hpp"

#include <memory>  // for std::shared_ptr
#include <vector>
#include <utility>

// Forward declarations
namespace Search {
  class SearchEngine;
}// end namespace Search

namespace Search {
  
  class SEARCH_EXPORT_CLASS SearchEnvironment {
  public:
    using SPtr = std::shared_ptr<SearchEnvironment>;
    
    using VarSPtr = Core::VariableSPtr;
    using SemSPtr = Core::VariableSemanticSPtr;
    using VarSem  = std::pair<VarSPtr, SemSPtr>;
    
  public:
    /// Search environment constructor:
    /// aEnvironment: vector of pairs
    /// <variable, semantic>
    /// defining the environment of the search engine.
    /// The environment must represent a set of variables,
    /// i.e., a variable must not appear more than once in the vector.
    /// aVarChoice: variable choice metric
    /// aValChoice: value choice metric
    /// @note "environment" variables are defined as:
    /// - branching variables OR
    /// - output variables OR
    /// - objective variables.
    /// @note use "isEnvironmentVariable" to identify environment variables
    /// @note it is possible to give to the current environment an Id.
    /// By default, the id is set as an empty string
    SearchEnvironment(const std::vector<VarSem>& aEnvironment,
                      VariableChoiceMetricType aVarChoice,
                      ValueChoiceMetricType aValChoice,
                      const std::string& aId="");
    
    virtual ~SearchEnvironment() = default;
    
    /// Returns true if "aVar" is and environment variable,
    /// false otherwise, same for the semantic version of this method
    static bool isEnvironmentVariable(const Core::VariableSPtr& aVar);
    static bool isEnvironmentSemantic(const Core::VariableSemantic* aSem);
    
    inline const std::string& getEnvironmentId() const { return pEnvironmentId; }
    
    /// Returns the search heuristic using by this environment
    inline SearchHeuristicSPtr searchHeuristic() const { return pSearchHeuristic; }
    
    /// Sets the timeout in seconds
    inline void setTimeout(int timeoutSec) { pTimeout = (timeoutSec < 0) ? -1 : timeoutSec; }
    
    /// Returns the timeout in seconds.
    /// @note a number lower than zero means no timeout
    inline int getTimeout() const { return pTimeout; }
    
    /// Returns true if this is a search environment for cbls, false otherwise
    virtual bool isLocalSearchEnvironment() const { return false; }
    
    /// Returns the vector of output variables in this search environment
    std::vector<Core::VariableSPtr> getObjectiveVariables() const;
    
    /// Returns the vector of branching variables in this search environment
    std::vector<Core::VariableSPtr> getBranchingVariables() const;
    
    /// Returns the set of environment variables
    const std::vector<Core::VariableSPtr>& getEnvironmentVariables() const;
    
  private:
    /// Id for this environment
    std::string pEnvironmentId;
    
    /// Timeout used for the search in seconds
    int pTimeout;
    
    /// Search heuristic defining this environment
    SearchHeuristicSPtr pSearchHeuristic;
    
    /// Vector environment variables
    std::vector<VarSPtr> pEnvVars;
    
    /// Vector semantics for the environment variables
    std::vector<SemSPtr> pEnvSems;
    
    /// Set of all the branching variables in this environment
    std::set<Core::VariableSPtr> pEnvironmentVariableSet;
    
    /// Vector of indexes to the environment objective variables
    std::vector<std::size_t> pObjVars;
    
    /// Vector of indexes to the environment branching variables
    std::vector<std::size_t> pBrcVars;
    
    /// Initializes the environment w.r.t. the variables and their semantic
    void initializeEnvironment(const std::vector<VarSem>& aEnvironment,
                               VariableChoiceMetricType aVarChoice,
                               ValueChoiceMetricType aValChoice);
  };
  
}// end namespace Search
