//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 10/02/2018
//
// Environment for local search engines.
// The environemnt holds the information about the heuristic to use,
// and the neighborhood description.
//

#pragma once

// Base class
#include "SearchEnvironment.hpp"

namespace Search {
  
  class SEARCH_EXPORT_CLASS LocalSearchEnvironment : public SearchEnvironment {
  public:
    ~LocalSearchEnvironment() = default;
    
    bool isLocalSearchEnvironment() const override { return true; }
    
    /// Set/get random seed.
    /// @note by default the random seed is -1
    inline void setRandomSeed(int aRandomSeed) { pRandomSeed = aRandomSeed; };
    inline int getRandomSeed() const { return pRandomSeed; }
    
    /// Sets the flag for warm (re)start.
    /// @note by default warm start is true
    inline void setWarmStart(bool aWarmStart) { pWarmStart = aWarmStart; }
    
    /// Returns the type of the search engine strategy
    /// using this environment
    inline SearchEngineStrategyType getSearchEngineStrategyType() const { return pStrategyType; }
    
    /// Returns size of the neighborhoods
    inline int getNeighborhoodSize() const { return pNeighborhoodSize; }
    
    /// Returns number of neighborhoods
    inline int getNumNeighborhoods() const { return pNumNeighborhoods; }
    
    /// Returns true for aggressive local search, false otherwise.
    /// @note aggressive means that it doesn't return the best assignment so far but it keeps
    /// looking for a solution until timeout
    inline bool isAggressiveSearch() const { return pRunAggressiveSearch; }
    
    /// Returns true for warm (re)start, false otherwise.
    /// @note a warm start preserves the status of the local search between one re-start and another
    inline bool isWarmStart() const { return pWarmStart; }
    
  protected:
    /// Constructor:
    /// - SearchEnvironment arguments;
    /// - aSizeNeighborhoods: size of each large neighborhood;
    /// - aNumNeighborhoods: number of large neighborhoods;
    /// - aRunAggressiveSearch: if true, consider only assignments that are solutions.
    ///                         If false, consider any improving solution;
    /// - aStrategyType: strategy type of the strategy using this environment
    /// - aEnvId: an identifier for this environment
    /// @note constructor is private.
    /// SearchEnvironments should be derived to hold specific information
    /// for each search strategy
    LocalSearchEnvironment(const std::vector<VarSem>& aEnvironment,
                           VariableChoiceMetricType aVarChoice,
                           ValueChoiceMetricType aValChoice,
                           int aSizeNeighborhoods,
                           int aNumNeighborhoods,
                           bool aRunAggressiveSearch,
                           SearchEngineStrategyType aStrategyType,
                           const std::string& aEnvId="");
    
  private:
    /// Size of the neighborhoods
    int pNeighborhoodSize;
    
    /// Number of neighborhoods
    int pNumNeighborhoods;
    
    /// Flag for aggressive search
    bool pRunAggressiveSearch;
    
    /// Flag for warm (re)start
    bool pWarmStart;
    
    /// Seed for the random number generator
    int pRandomSeed;
    
    /// Strategy type for the strategy this environment is bound to
    SearchEngineStrategyType pStrategyType;
  };
  
}// end namespace Search
