//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 1/25/2017
//
// Factory for variable and value choice metrics.
//

#pragma once

#include "VariableChoiceMetricInputOrder.hpp"
#include "VariableChoiceMetricFirstFail.hpp"
#include "VariableChoiceMetricAntiFirstFail.hpp"
#include "VariableChoiceMetricSmallest.hpp"
#include "VariableChoiceMetricLargest.hpp"
