//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 01/24/2017
//
// Indomain min metric: min value is the most promising one.
//

#pragma once

#include "ValueChoiceMetric.hpp"

#include "Domain.hpp"
#include "DomainElement.hpp"

namespace Search {

  class SEARCH_EXPORT_CLASS ValueChoiceMetricIndomainMin : public ValueChoiceMetric {
  public:
    ValueChoiceMetricIndomainMin();

    virtual ~ValueChoiceMetricIndomainMin();

    inline Core::DomainElement* getValue(Core::Domain*  aDomain) override
    {
      assert(aDomain);
      return aDomain->lowerBound();
    }
  };

}// end namespace Search
