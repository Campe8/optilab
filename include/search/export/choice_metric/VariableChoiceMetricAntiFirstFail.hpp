//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 02/24/2017
//
// Anti First fail metric: the most promising variable is
// defined by the largest current domain.
// For example, [1, 4] will be chosen over [2, 3].
//

#pragma once

#include "VariableChoiceMetric.hpp"

namespace Search {

  class SEARCH_EXPORT_CLASS VariableChoiceMetricAntiFirstFail : public VariableChoiceMetric {
  public:
    VariableChoiceMetricAntiFirstFail();

    virtual ~VariableChoiceMetricAntiFirstFail();

  protected:
    /// Returns the metric value for "aVar"
    double getMetricValue(Core::Variable* aVar) override;
  };

}// end namespace Search
