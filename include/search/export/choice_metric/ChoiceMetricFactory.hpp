//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 1/25/2017
//
// Factory for variable and value choice metrics.
//

#pragma once

#include "SearchHeuristicDefs.hpp"
#include "ValueChoiceMetricInc.hpp"
#include "VariableChoiceMetricInc.hpp"

#include <cassert>

namespace Search {

  class ValueChoiceMetricFactory {
  public:
    /// Returns a value choice metric of type "aValMetricType"
    static ValueChoiceMetricSPtr getValueChoiceMetric(ValueChoiceMetricType aValMetricType)
    {
      switch (aValMetricType)
      {
        case Search::ValueChoiceMetricType::VAL_METRIC_INDOMAIN_RANDOM:
          return std::make_shared<ValueChoiceMetricIndomainRandom>();
        case Search::ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MAX:
          return std::make_shared<ValueChoiceMetricIndomainMax>();
        default:
          assert(aValMetricType == Search::ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MIN);
          return std::make_shared<ValueChoiceMetricIndomainMin>();
      }
    }
    
  };
  
  class VariableChoiceMetricFactory {
  public:
    /// Returns a variable choice metric of type "aVarMetricType"
    static VariableChoiceMetricSPtr getVariableChoiceMetric(VariableChoiceMetricType aVarMetricType)
    {
      switch (aVarMetricType)
      {
        case Search::VariableChoiceMetricType::VAR_CM_FIRST_FAIL:
          return std::make_shared<VariableChoiceMetricFirstFail>();
        case Search::VariableChoiceMetricType::VAR_CM_SMALLEST:
          return std::make_shared<VariableChoiceMetricSmallest>();
        case Search::VariableChoiceMetricType::VAR_CM_LARGEST:
          return std::make_shared<VariableChoiceMetricLargest>();
        default:
          assert(aVarMetricType == Search::VariableChoiceMetricType::VAR_CM_INPUT_ORDER);
          return std::make_shared<VariableChoiceMetricInputOrder>();
      }
    }
  };
  
}// end namespace Search
