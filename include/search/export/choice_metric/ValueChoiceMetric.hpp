//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 01/24/2017
//
// Value choice metric used to "compare" 
// domain elements.
//

#pragma once

#include "SearchExportDefs.hpp"
#include "SearchHeuristicDefs.hpp"

#include <memory>

// Forward declarations
namespace Core {
  class Domain;
  class DomainElement;
}

namespace Search {
  class ValueChoiceMetric;
  
  typedef std::unique_ptr<ValueChoiceMetric> ValueChoiceMetricUPtr;
  typedef std::shared_ptr<ValueChoiceMetric> ValueChoiceMetricSPtr;
}// end namespace Search

namespace Search {

  class SEARCH_EXPORT_CLASS ValueChoiceMetric {
  public:
    virtual ~ValueChoiceMetric();

    inline ValueChoiceMetricType getValueChoiceMetricType() const
    {
      return pValMetricType;
    }

    /// Returns the most promising value in "aDomain",
    /// according to this metric
    virtual Core::DomainElement* getValue(Core::Domain*  aDomain) = 0;

  protected:
    ValueChoiceMetric(ValueChoiceMetricType aVarMetricType);

  private:
    ValueChoiceMetricType pValMetricType;
  };

}// end namespace Search
