//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 02/27/2017
//
// Indomain max metric: max value is the most promising one.
//

#pragma once

#include "ValueChoiceMetric.hpp"

#include "Domain.hpp"
#include "DomainElement.hpp"

namespace Search {

  class SEARCH_EXPORT_CLASS ValueChoiceMetricIndomainMax : public ValueChoiceMetric {
  public:
    ValueChoiceMetricIndomainMax();

    virtual ~ValueChoiceMetricIndomainMax();

    inline Core::DomainElement* getValue(Core::Domain*  aDomain) override
    {
      assert(aDomain);
      return aDomain->upperBound();
    }
  };

}// end namespace Search
