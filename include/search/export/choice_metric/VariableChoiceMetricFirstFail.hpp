//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 02/24/2017
//
// First fail metric: the most promising variable is
// defined by the smallest current domain.
// For example, [2, 3] will be chosen over [-1, 100].
//

#pragma once

#include "VariableChoiceMetric.hpp"

namespace Search {

  class SEARCH_EXPORT_CLASS VariableChoiceMetricFirstFail : public VariableChoiceMetric {
  public:
    VariableChoiceMetricFirstFail();

    virtual ~VariableChoiceMetricFirstFail();

  protected:
    /// Returns the metric value for "aVar"
    double getMetricValue(Core::Variable* aVar) override;
  };

}// end namespace Search
