//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 02/27/2017
//
// Indomain random metric: random value is the most promising one.
//

#pragma once

#include "ValueChoiceMetric.hpp"

#include "Domain.hpp"
#include "DomainElement.hpp"

#include <memory>

// Forward declarations
namespace Base { namespace Tools {
  class RandomGenerator;
}}// end namespace Tools/Base

namespace Search {
  
  class SEARCH_EXPORT_CLASS ValueChoiceMetricIndomainRandom : public ValueChoiceMetric {
  public:
    ValueChoiceMetricIndomainRandom();
    
    ~ValueChoiceMetricIndomainRandom() override = default;
    
    Core::DomainElement* getValue(Core::Domain*  aDomain) override;
  
    inline void setRandomGenerator(Base::Tools::RandomGenerator* aRndGen)
    {
      pRandomGenerator = aRndGen;
    }
    
  private:
    /// Pointer to the random generator to be used
    /// when selecting a random value.
    /// If nullptr, use default random generator
    Base::Tools::RandomGenerator* pRandomGenerator;
    
  };

}// end namespace Search
