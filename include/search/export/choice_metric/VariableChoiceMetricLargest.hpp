//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 02/24/2017
//
// Largest metric: the most promising variable is
// defined by the domain with the largest value.
// For example, [2, 4] will be chosen over [2, 3].
//

#pragma once

#include "VariableChoiceMetric.hpp"

namespace Search {

  class SEARCH_EXPORT_CLASS VariableChoiceMetricLargest : public VariableChoiceMetric {
  public:
    VariableChoiceMetricLargest();

    virtual ~VariableChoiceMetricLargest();

    /// Compares "aVar1" to "aVar2", returns:
    /// 1) VAR_METRIC_EQ: both variables have the same metric score
    /// 2) VAR_METRIC_LT: "aVar1" has a lower metric score than "aVar2", 
    ///                   i.e., "aVar2" is a "better" choice according to this metric
    /// 3) VAR_METRIC_GT:  "aVar1" has a higher metric score than "aVar2", 
    ///                   i.e., "aVar1" is a "better" choice according to this metric
    VariableMetricEval compare(Core::Variable* aVar1, Core::Variable* aVar2) override;

  protected:
    /// Returns the metric value for "aVar".
    /// @note cannot convert domain element to double
    double getMetricValue(Core::Variable* aVar) override;
  };

}// end namespace Search
