//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 01/24/2017
//
// Input order metric: the most promising variable is
// defined by the order of variable declaration.
//

#pragma once

#include "VariableChoiceMetric.hpp"

namespace Search {

  class SEARCH_EXPORT_CLASS VariableChoiceMetricInputOrder : public VariableChoiceMetric {
  public:
    VariableChoiceMetricInputOrder();

    virtual ~VariableChoiceMetricInputOrder();

  protected:
    /// Returns the metric value for "aVar"
    double getMetricValue(Core::Variable* aVar) override;
  };

}// end namespace Search
