//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 01/24/2017
//
// Variable choice metric used to "compare" 
// branching variables.
//

#pragma once

#include "SearchExportDefs.hpp"
#include "SearchHeuristicDefs.hpp"

#include "Variable.hpp"

#include <memory>

// Forward declarations
namespace Search {
  class VariableChoiceMetric;

  typedef std::unique_ptr<VariableChoiceMetric> VariableChoiceMetricUPtr;
  typedef std::shared_ptr<VariableChoiceMetric> VariableChoiceMetricSPtr;
}// end namespace Search

namespace Search {

  class SEARCH_EXPORT_CLASS VariableChoiceMetric {
  public:
    virtual ~VariableChoiceMetric();

    inline VariableChoiceMetricType getVariableChoiceMetricType() const
    {
      return pVarMetricType;
    }

    /// Compares "aVar1" to "aVar2", returns:
    /// 1) VAR_METRIC_EQ: both variables have the same metric score
    /// 2) VAR_METRIC_LT: "aVar1" has a lower metric score than "aVar2", 
    ///                   i.e., "aVar2" is a "better" choice according to this metric
    /// 3) VAR_METRIC_GT:  "aVar1" has a higher metric score than "aVar2", 
    ///                   i.e., "aVar1" is a "better" choice according to this metric
    virtual VariableMetricEval compare(Core::Variable*  aVar1, Core::Variable* aVar2);

  protected:
    VariableChoiceMetric(VariableChoiceMetricType aVarMetricType);

    /// Returns the metric value for "aVar".
    /// @note the HIGHER the value, the BETTER the metric is
    virtual double getMetricValue(Core::Variable* aVar) = 0;

    /// Compares "aMetricVal1" to "aMetricVal2" and returns:
    /// -1 if aMetricVal1 < aMetricVal2
    ///  0 if aMetricVal1 == aMetricVal2
    /// +1 if aMetricVal1 > aMetricVal2
    inline int compare(double aMetricVal1, double aMetricVal2)
    {
      if (aMetricVal1 < aMetricVal2)
      {
        return -1;
      }
      if (aMetricVal1 == aMetricVal2)
      {
        return 0;
      }
      return 1;
    }

  private:
    VariableChoiceMetricType pVarMetricType;
  };

}// end namespace Search
