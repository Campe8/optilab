//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 1/25/2017
//
// Factory for variable and value choice metrics.
//

#pragma once

#include "ValueChoiceMetricIndomainMin.hpp"
#include "ValueChoiceMetricIndomainMax.hpp"
#include "ValueChoiceMetricIndomainRandom.hpp"
