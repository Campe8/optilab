//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/29/2017
//

#pragma once

#include "MVCMacroController.hpp"

#include <exception>
#include <boost/exception/all.hpp>

#define MVC_FILE_MODEL_EXT ".opt"

#define mvc_exception boost::exception
#define mvc_get_exception_info(e) *boost::get_error_info<mvc_errmsg_info>(e)
#define mvc_throw(aMsg) throw MVCException{aMsg}

#define mvc_assert(expr) \
((expr) ? (void)0 : mvc_throw(("")))

#define mvc_assert_msg(expr, msg) \
((expr) ? (void)0 : mvc_throw((msg)))

using mvc_errmsg_info = boost::error_info<struct tag_errmsg, std::string>;

namespace MVC {
  
  struct MVCException : public boost::exception, public std::exception {
    MVCException(const std::string& aMsg)
    : pMsg(aMsg)
    {
    }
    
    const char *what() const noexcept { return pMsg.c_str(); }
    
  private:
    /// Exception message
    std::string pMsg;
  };
  
}// end namespace MVC

