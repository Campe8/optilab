//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/29/2017
//

#pragma once

#define MVC_C_NUM_SOL_STR(num) "Number of solutions: " + (num)
#define MVC_C_CUR_SOL_STR(num) "Solution: " + (num)

