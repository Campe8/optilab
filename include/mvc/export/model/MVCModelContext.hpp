//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 07/15/2017
//
// Model context: it represents the context of the model.
// From the model context it is possible to generate
// the model that the back-end engine runs.
//

#pragma once

#include "MVCExportDefs.hpp"

#include "ModelContextPrettyPrinter.hpp"

#include "CtxObjectCollector.hpp"

// Object stored in the model context
#include "BaseObject.hpp"
#include "ContextObject.hpp"

// CLI stmt
#include "CLIDefs.hpp"

// Maps
#include "Bimap.hpp"
#include <sparsepp/spp.h>

// Boost
#include <boost/core/noncopyable.hpp>

#include <string>
#include <unordered_map>
#include <unordered_set>
#include <memory>

// Forward declarations
namespace MVC {
  class MVCModel;
}// end namespace MVC

namespace MVC {
  
  /*
   * Pass-key idiom to restrict the scope of friend the
   * friend's declaration's scope to one or more methods
   * instead of the whole class.
   * Restriction if given to all except MVCModel.
   */
  template <typename T>
  class PassKey {
  private:
    friend T;
    PassKey() {}
    PassKey(const PassKey&) {}
    PassKey(PassKey&&) {}
    PassKey& operator=(const PassKey&) = delete;
    PassKey& operator=(PassKey&&) = delete;
  };
  
  class MVC_EXPORT_CLASS MVCModelContext : private boost::noncopyable {
  public:
    using MdlObj = Interpreter::DataObject;
    using MdlCtxObjKey = CtxObjectCollector::MdlCtxObjKey;
    using CtxObjSPtr = std::shared_ptr<Model::ContextObject>;
    using CtxObjMap = spp::sparse_hash_map<MdlCtxObjKey, CtxObjSPtr>;
    using CtxObjIter = CtxObjMap::const_iterator;
    
    static MdlCtxObjKey contextKeyInf;
    static std::string MVCModelContextBaseObjectValue;
    static Interpreter::BaseObject::PropertyKey MVCModelContextBaseObjectKey;
    
    enum PrettyPrintLevel : int {
        PP_NONE = 0
      , PP_LOW
      , PP_HIGH
    };
    
  public:
    MVCModelContext();
    
    virtual ~MVCModelContext() = default;
    
    /// Returns the key for the context objective with identifier "aID".
    /// @note returns MVCModelContext::contextKeyInf if
    /// "aID" does not identify a variable in the current context
    inline MdlCtxObjKey getContextKeyFromID(const std::string& aID) const
    {
      return pCtxObjCollector.lookupKeyByID(aID);
    }
    
    /// Returns the list of keys of context objects that are not fully interpreted
    std::vector<MdlCtxObjKey> getListOfNotFullyInterpretedContextObjects() const;
    
    /// Returns true if the context object identified by given key is fully interpreted,
    /// false otherwise.
    /// If there is no such object returns true.
    /// Undefined behavior if "aKey" is "contextKeyInf"
    bool isContextObjectFullyInterpreted(MdlCtxObjKey aKey) const;
    
    /// Returns true if the model context if fully interpreted,
    /// false otherwise
    bool isModelContextFullyInterpreted() const;

    /// Returns a unique name for an object
    std::string getUnusedNameForContextObject() const;
    
    /// Sets the pretty print level:
    /// - none: disable pretty print
    /// - low: minimal information (default)
    /// - high: full pretty print
    /// @note returns the pretty print level before setting
    PrettyPrintLevel setPrettyPrintLevel(PrettyPrintLevel aPPL) const;
    
    /// Returns current pretty print level
    inline PrettyPrintLevel getPrettyPrintLevel() const { return pPrettyPrintLevel; }
    
    /// Pretty prints the context onto the given output stream
    void prettyPrint(std::ostream& aOut) const;
    
    /// Pretty prints the context object identified by "aKey" onto
    /// the given output stream
    void prettyPrint(MdlCtxObjKey aKey, std::ostream& aOut) const;
    
    /// For loop range iterator over the objects.
    // @note there is no specific order of iteration over the objects
    CtxObjIter begin() const { return pModelContext->begin(); }
    CtxObjIter end()   const { return pModelContext->end();   }
    
    //============= MVCModel only callable methods =============//
    
    /// Clears all the content of the context
    inline void clear(PassKey<MVCModel>) { clear(); }
    
    /// Removes from the context the object with given key, if any
    inline void erase(PassKey<MVCModel>, MdlCtxObjKey aKey) { erase(aKey); }
    
    /// Adds a named model object to the context and returns its key and type.
    /// The key is used as ID in the context
    inline std::pair<MdlCtxObjKey, Model::ContextObject::Type>
    addNamedObjectToContext(PassKey<MVCModel>, const std::string& aName, const MdlObj& aDO)
    {
      return addNamedObjectToContext(aName, aDO);
    }
    
    /// Adds an unnamed model object to the context and returns its key and type.
    /// The key is used as ID in the context.
    /// @note the context generates a new unique name for the given object
    inline std::pair<MdlCtxObjKey, Model::ContextObject::Type>
    addUnnamedObjectToContext(PassKey<MVCModel>, const MdlObj& aDO)
    {
      return addNamedObjectToContext(getUnusedNameForContextObject(), aDO);
    }
    
    /// Performs a lookup of the key and returns the correspondent context object if any
    inline CtxObjSPtr lookup(PassKey<MVCModel>, MdlCtxObjKey aKey) const { return lookup(aKey); }
    
    //==========================================================//
    
  private:
    /// Pretty print information level
    mutable PrettyPrintLevel pPrettyPrintLevel;
    
    /// Context object collector mapping ctx object keys
    /// to their type and ID
    CtxObjectCollector pCtxObjCollector;
    
    /// Input order variable declaration
    std::size_t pVariableInputOrderCtr;
    
    /// This map represents the actual context, i.e., the mapping
    /// between context keys and the model objects
    std::unique_ptr<CtxObjMap> pModelContext;

    /// List of keys for not fully interpreted data objects
    std::vector<MdlCtxObjKey> pReqLaterInterpList;
    
    /// Adds a named model object to the context and returns its key
    /// which is used as ID in the context
    std::pair<MdlCtxObjKey, Model::ContextObject::Type> addNamedObjectToContext(const std::string& aName,
                                                                                const MdlObj& aDO);
    
    /// Performs a lookup of the key and returns the correspondent context object if any
    CtxObjSPtr lookup(MdlCtxObjKey aKey) const;
    
    /// Clears all the content of the context
    void clear();
    
    /// Removes from the context the object with given key, if any
    void erase(MdlCtxObjKey aKey);
    
    /// Return a key which is not already used
    inline MdlCtxObjKey getUnusedObjKey() const { return pCtxObjCollector.getUnusedKey(); }
    
    /// Map of pretty printers for context
    std::unordered_map<int, std::shared_ptr<Model::ModelContextPrettyPrinter>> pMCPPMap;
    
    /// Returns the model context instance
    inline CtxObjMap& modelContext() { return *pModelContext; }
    inline const CtxObjMap& modelContext() const { return *pModelContext; }
    
    /// Adds the given key on the list of keys that requires later interpetation
    void addKeyForLaterInterpretation(MVCModelContext::MdlCtxObjKey aKey);
    
    /// Removes the given key on the list of keys that requires later interpetation
    void removeKeyForLaterInterpretation(MVCModelContext::MdlCtxObjKey aKey);
    
    /// Returns a ContextObject of type "aObjType" wrapping the given DataObject
    std::shared_ptr<Model::ContextObject> createContextObject(const std::string& aName,
                                                              const MdlObj& aDO);
    
    /// Returns a VariableContextObject from "aDO" which is assumed to represent a variable
    CtxObjSPtr createVariableContextObject(const std::string& aID, const MdlObj& aDO);
    
    /// Returns a ConstraintContextObject from "aDO" which is assumed to represent a constraint
    CtxObjSPtr createConstraintContextObject(const std::string& aID, const MdlObj& aDO);
    
    /// Creates a context object around the given model object and maps it to its name and key.
    /// Returns the type of the context object added to the context
    Model::ContextObject::Type addObjectToContext(const std::string& aName, const MdlObj& aDO,
                                                  MdlCtxObjKey aKey);
    
    /// Maps the given name into the BaseObject wrapped by the given MdlObj.
    /// @note returns it "aDO" is not a class object
    void mapModelContextKeyIntoBaseObject(const MdlObj& aDO, const std::string& aName);
    
    /// Resets mappings, i.e., clears the maps
    void resetMappings();
    
    /// Registers pretty printers in the internal map
    void registerPrettyPrinters();
    
    /// Returns the instance of the pretty printer for the context object of type "aType"
    Model::ModelContextPrettyPrinter* getMCPP(const Model::ContextObject::Type aType) const;
    
    /// Pretty prints a model object
    /// @note if "aFullPrettyPrint is true, the pretty print is "full" i.e., it has more information
    inline void prettyPrint(const Model::ContextObject::Type aType, const CtxObjSPtr& aCtxObj,
                            std::ostream& aOut, bool aFullPrettyPrint = false) const
    {
      assert(getMCPP(aType));
      getMCPP(aType)->prettyPrint(aCtxObj, aOut, aFullPrettyPrint);
    }
  };
  
  /// Returns a context object collector type given a context object type
  MVC_EXPORT_FCN CtxObjectCollector::MdlCtxType ctxObjectTypeToCollectorType(Model::ContextObject::Type aType);
  
  /// Returns a context object type given a context object collector type
  MVC_EXPORT_FCN Model::ContextObject::Type collectorTypeToCtxObjectType(CtxObjectCollector::MdlCtxType aType);
  
}//end namespace MVC
