//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 12/10/2018
//

#pragma once

namespace MVC {
  namespace ModelMetrics {
    
    extern const char* COMPILER_TIME;
    extern const char* METRICS;
    extern const char* SEARCH_TIME;
    extern const char* TIME_METRICS;
    extern const char* WALLCLOCK_TIME;
    
  }// end namespace ModelMetrics
  
  namespace ModelConst {
    
    extern const char* CTX_VARIABLE_NAME;
    extern const char* MODEL_SATISFIED;
    
  }// end namespace ModelConst
  
}// end namespace MVC
