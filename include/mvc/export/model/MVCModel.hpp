//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 06/14/2017
//
// MVC pattern: Model.
//

#pragma once
#include "MVCExportDefs.hpp"
#include "MVCObserver.hpp"

#include <cstdint>
#include <memory>

#include <boost/core/noncopyable.hpp>

#include "DataObject.hpp"
#include "ModelCallbackHandler.hpp"
#include "ModelContext.hpp"
#include "ModelGenerator.hpp"
#include "ModelParser.hpp"
#include "MVCEventFactory.hpp"
#include "MVCMacro.hpp"
#include "MVCEventFactory.hpp"
#include "MVCModelContext.hpp"
#include "SolverApi.hpp"
#include "SolverEngine.hpp"
#include "SolverSolutionCollector.hpp"
#include "TransformEngine.hpp"

namespace MVC {
  
  class MVC_EXPORT_CLASS MVCModel : public MVCSubject, private boost::noncopyable {
  public:
    MVCModel();
    
    virtual ~MVCModel() = default;
    
    /// Returns the pointer to this subject
    static MVCSubject* getSubject(MVCModel* aModel) { return static_cast<MVCSubject*>(aModel); }
    
    /// Adds the object "aDOC" to the context.
    /// @note that the model will give a unique identifier to the
    /// object since a name is not provided.
    /// This is used, for example, to post constraints.
    /// @note throws MVCException in case of error
    void addObjectToContext(const Interpreter::DataObject& aDO);
    
    /// Adds the declaration "aName" of the object "aDO" to the context.
    /// @note throws MVCException in case of error
    void addObjectToContext(const std::string& aName, const Interpreter::DataObject& aDO);
    
    /// Updates the context variable with the given data object which must be
    /// a variable domain object
    void updateCtxVariable(const Interpreter::DataObject& aDO);
    
    /// Removes all the elements from the model contexts
    void clearModelContext();
    
    /// Removes the context object identified by the given key, if any
    void eraseContextObject(MVCModelContext::MdlCtxObjKey aKey);
    
    /// Runs the interpreted on all the context objects in the model.
    /// @note it skips objects already fully interpreted.
    /// @note it does not pretty print re-interpreted objects on output.
    void interpretModelContext();
    
    /// Runs the interpreted on the context object identified by the given key if:
    /// - the context object exists and
    /// - the context object is not fully interpreted
    /// @note it prints on the view the re-interpreted object
    void interpretContextObject(MVCModelContext::MdlCtxObjKey aKey);
    
    /// Returns the name of the context variable
    inline std::string getContextVarName() const { return pCtxVarName; }
    
    /// Generates and returns a model from the content of the model context
    Model::ModelSPtr generateModel();
    
    /// Runs the solver on "aModel" and notifies the observers about all the solutions found
    void solveModel(const Model::ModelSPtr& aModel);
    
    /// Returns the model context (read only)
    inline const MVCModelContext& getModelContext() const { return pModelContext; }
    
  private:
    /// Context variable name
    std::string pCtxVarName;
    
    /// Key for the context object variable
    MVCModelContext::MdlCtxObjKey pCtxVarKey;
    
    /// Variable for recording the time spent in building the model
    uint64_t pCompileTimeMsec;
    
    /// Variable for recording the time spent during search
    uint64_t pSearchTimeMsec;
    
    /// Context of the model
    MVCModelContext pModelContext;
    
    /// Engine for IR transforms
    std::unique_ptr<IR::Transforms::TransformEngine> pTransformEngine;
    
    /// Model parser from stream to AST
    std::unique_ptr<Model::ModelParser> pModelParser;
    
    /// Model builder to generate the model
    std::unique_ptr<Model::ModelGenerator> pModelGenerator;
    
    /// Solver API
    std::unique_ptr<Solver::SolverApi> pSolverApi;
    
    /// Handler for API callbacks
    Solver::ModelCallbackHandler::SPtr pCallbackHandler;
    
    /// Factory for events
    std::unique_ptr<MVCEventFactory> pEventFactory;
    
    /// List of keys for not fully interpreted data objects
    std::vector<MVCModelContext::MdlCtxObjKey> pReqLaterInterpList;
    
    /// List of keys for search statements, used to clean-up
    /// the context after running the solver engine
    std::vector<MVCModelContext::MdlCtxObjKey> pSrcCtxKeyList;
    
    /// Generates the model context from the MVCModelContext
    Model::ModelContext getModelCtx();
    
    /// Updates current context w.r.t. the given solution,
    /// i.e., the updated context will contain only the given solution
    void updateModelContext(const Model::ModelSPtr& aModel,
                            const Solver::ModelCallbackHandler::Solution& aSol);
    
    /// Utility function: removes all the branch declarations from the context,
    /// this is used, for example, at the end of a run
    void removeBranchDeclsFromContext();
    
    /// Utility function: returns a vector containing the ModelContext from MVCModelContext
    /// which are registered with the keys in "aKeyList"
    std::vector<MVCModelContext::CtxObjSPtr>
    getContextObjectListFromKeysList(const std::vector<MVCModelContext::MdlCtxObjKey>& aKeyList);
    
    /// Utility function: returns the ModelContext from MVCModelContext which is registered
    /// with the given key
    MVCModelContext::CtxObjSPtr
    getContextObjectListFromKey(const MVCModelContext::MdlCtxObjKey& aKey);
    
    /// Runs post-solving evalutation.
    /// Handles solutions and notifies observers
    void postSolvingEvaluation(const Model::ModelSPtr& aModel);
    
    /// Handles post-solving evaluation when no solutions are available
    void postSolvingEvaluationNoSolutions(const Model::ModelSPtr& aModel);
    
    /// Handles post-solving evaluation when solutions are available
    void postSolvingEvaluationSolutions(const Model::ModelSPtr& aModel);
    
    /// Utility function: collect metrics into a register and adds to the metrics to the manager
    void collectMetrics(const Model::ModelSPtr& aModel);
    
    /// Utility function: calls the pretty print on the given context object and sends the
    /// results to the view
    void printContextObject(MVCModelContext::MdlCtxObjKey aKey);
  };
  
}//end namespace MVC
