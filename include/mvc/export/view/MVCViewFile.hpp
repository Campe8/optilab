//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 07/31/2018
//
// MVC pattern: view for file interface.
//

#pragma once

// Base class
#include "MVCView.hpp"

namespace MVC {
  
  class MVC_EXPORT_CLASS MVCViewFile : public MVCView {
  public:
    MVCViewFile(const std::string& aFilePath);
    
    /// Initializes the view.
    /// Open the file-stream or set it
    /// to the exit command if no such file is present
    void initializeView() override;
    
    /// Returns next input line
    std::string getNextInputLine(const std::string& aPrompt) override;
    
    /// Dumps into the current outstream
    /// the content of the internal output buffer
    void writeOutStream() override;
    
  private:
    /// Path to the file
    std::string pFilePath;
    
    /// Index to the next input line from the input stream
    std::size_t pInputPC;
    
    /// Flag used to determine when the exit command must be sent
    /// into the input buffer
    bool pExitCommand;
    
    /// Collection of the strings representing the input stream
    std::vector<std::string> pInputStream;
    
    /// Set get exit command flag
    inline void setExitCommand() { pExitCommand = true; }
    inline bool isExitCommandSet() const { return pExitCommand; }
    
    /// Checks if there is another input line from the stream
    inline bool inputStreamHasNextLine() const { return pInputPC < pInputStream.size(); }
    
    /// Returns the next input line from the stream
    inline std::string getNextLineFromInputStream() { return pInputStream[pInputPC++]; }
    
    /// Fills the input stream vector with the content of the given string stream
    void fillInputStream(std::stringstream& aStringStream);
    
    /// Checks if the outbuffer is empty or has
    /// empty content (for example, empty strings).
    /// If so, returns true, otherwise returns false
    bool isOutBufferEmpty();
  };
  
}//end namespace MVC
