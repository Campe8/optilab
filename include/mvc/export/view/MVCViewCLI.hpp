//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/29/2017
//
// MVC pattern: view for Comman Line Interface.
//

#pragma once

// Base class
#include "MVCView.hpp"

namespace MVC {
  
  class MVC_EXPORT_CLASS MVCViewCLI : public MVCView {
  public:
    /// Constructor: create a new instance of MVCViewCLI.
    /// If "aUseSmartCLI" is true, it uses a smart cli (e.g., readline) to run the CLI.
    /// @note by default "aUseSmartCLI" is false
    MVCViewCLI(bool aUseSmartCLI=false);
    
    /// Execute view on command line interface
    void exe() override;
    
    /// Initializes the view
    void initializeView() override;
    
    /// Returns next input line
    std::string getNextInputLine(const std::string& aPrompt) override;
    
    /// Dumps into the current outstream
    /// the content of the internal output buffer
    void writeOutStream() override;
    
  private:
    bool pUseSmartCLI;
    
    /// Exe flag
    bool pRunning;
    
    /// Input line
    std::string pInLine;
    
    /// Flag true when outBuffer has been used
    bool pOutBufferUsed;
    
    /// Prints welcome screen on aOut
    void printWelcomeScreen(std::ostream &aOut);
    
    /// Reads and stores next line from input stream.
    /// Before reading sets "aPrompt" as prompt symbol
    void readLine(const std::string& aPrompt);
    
    /// Takes actions based on the content of "pInLine"
    void handleLine();
    
    /// Empties input buffer
    void cleanInLine();
    
    /// Sends the content of the output buffer to the current output stream
    void outBuffer();
    
    /// Returns true if output needs a new line,
    /// false otherwise
    bool printNewLine();
    
    /// Checks if the outbuffer is empty or has
    /// empty content (for example, empty strings).
    /// If so, returns true, otherwise returns false
    bool isOutBufferEmpty();

    /// return true   if gnu readline support is enabled
    ///        false  otherwise
    bool isReadlineEnabled();
  };
  
}//end namespace MVC
