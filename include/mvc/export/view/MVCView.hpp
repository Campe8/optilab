//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/29/2017
//
// MVC pattern: View.
//

#pragma once

#include "MVCExportDefs.hpp"
#include "MVCMacro.hpp"

// Event factory
#include "MVCEventFactory.hpp"

// Event dispatcher
#include "MVCEventDispatcher.hpp"

// Observer
#include "MVCObserver.hpp"

#include <boost/optional.hpp>
#include <boost/core/noncopyable.hpp>

#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <memory>

namespace MVC {
  
  class MVC_EXPORT_CLASS MVCView : public MVCSubject, public MVCObserver, private boost::noncopyable {
  public:
    virtual ~MVCView() = default;
    
    /// Returns the pointer to this subject
    static MVCSubject* getSubject(MVCView* aView);
    
    /// Deprecated
    /// Executes view on in/out streams.
    /// @note by default the input stream is standard input,
    /// while the output stream is standard output
    virtual void exe() {};
    
    /// Initializes the view
    virtual void initializeView() {};
    
    /// Returns next input line printing "aPrompt" as prompt input symbol
    virtual std::string getNextInputLine(const std::string& aPrompt) { return aPrompt; };
    
    /// Dumps into the current outstream
    /// the content of the internal output buffer
    virtual void writeOutStream() {};
    
    /// Sets current input stream to aInStream
    void setInStream(std::istream* aInStream);
    
    /// Removes "aInStream" if it is the current input stream
    /// and reset it to the stream used before setting "aInStream"
    void resetInStream(std::istream* aInStream);
    
    /// Sets current output stream to "aOutStream"
    void setOutStream(std::ostream* aOutStream);
    
    /// Removes "aOutStream" if it is the current output stream
    /// and reset it to the stream used before setting "aOutStream"
    void resetOutStream(std::ostream* aOutStream);
    
    /// Updates this controller based on the received event "aEvent"
    void update(const std::shared_ptr<MVCEvent>& aEvent) override;
    
  protected:
    /// Constructor takes default input stream and default output stream
    MVCView(std::istream* aDefaultInStream, std::ostream* aDefaultOutStream);
    
    /// Reads current input stream and returns the next line
    /// until the delimitation char '\n'.
    /// @note This may be a blocking call!!!
    boost::optional<std::string> readInStream();
    
    /// Returns true if the active input stream is the default one,
    /// false otherwise.
    /// @note default input stream is expected to be the standard input stream
    inline bool isDefaultInStreamActive() const
    {
      return pInStream.size() == 1;
    }
    
    /// Returns true if the active output stream is the default one,
    /// false otherwise.
    /// @note default output stream is expected to be the standard output stream
    inline bool isDefaultOutStreamActive() const
    {
      return pOutStream.size() == 1;
    }
    
    /// Returns current input stream
    inline std::istream& getInStream()
    {
      return *pInStream.back();
    }
    
    /// Returns default input stream
    inline std::istream& getDefaultInStream()
    {
      return *pInStream[0];
    }
    
    /// Returns current output stream
    inline std::ostream& getOutStream()
    {
      return *pOutStream.back();
    }
    
    /// Returns default output stream
    inline std::ostream& getDefaultOutStream()
    {
      return *pOutStream[0];
    }
    
    /// Returns output buffer
    inline const std::vector<std::string>& getOutBuffer() const
    {
      return pOutBuffer;
    }
    
    /// Clears the content of the output buffer
    inline void clearOutBuffer()
    {
      pOutBuffer.clear();
    }
    
    /// Sends input and parsed string to controller
    void sendStmtToController(const std::string& aString, const std::string& aSrcString);
    
  private:
    /// Output buffer
    std::vector<std::string> pOutBuffer;
    
    /// Input stream to read from
    std::vector<std::istream*> pInStream;
    
    /// Vector of output streams to write into
    std::vector<std::ostream*> pOutStream;
    
    /// Event factory
    std::unique_ptr<MVCEventFactory> pEventFactory;
    
    /// Dispatcher for events
    std::unique_ptr<MVCEventDispatcher> pEventDispatcher;
    
    /// Register event handles in the event dispatcher
    void registerEventHandlers();
    
    /// Writes the content of "aEvent" into the internal out buffer
    void handleWriteOutputEvent(MVCEvent* aEvent);
    
    /// Handles error event
    void handleErrorEvent(MVCEvent* aEvent);
    
    /// Handles error event
    void handleNoSolutionEvent(MVCEvent* aEvent);
    
    /// Handles change out-stream event
    void handleChangeOutStreamEvent(MVCEvent* aEvent);
  };
  
}//end namespace MVC

