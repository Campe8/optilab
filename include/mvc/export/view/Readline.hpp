//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Claudio Desideri on 09/02/2018
//
#pragma once

#include "MVCExportDefs.hpp"

#include <iostream>
#include <string>
#include <list>
#include <vector>

#include <boost/optional.hpp>
#include <boost/core/noncopyable.hpp>

/**
 * Hides GNU readline C style internals
 */
namespace Readline {
  /**
   * @brief Initialize readline library
   * @param name name of the virtual terminal
   * @param history enable/disable terminal history, bool value 
   */
  MVC_EXPORT_FCN void initialize(std::string aName, bool aHistory);

  /** @return boost::optional because of compatibility with MVCView::readInStream
   * 
   *  TODO: readline can handle errors?
   */
  MVC_EXPORT_FCN boost::optional<std::string> getLine(std::string aPrompt);

  /**
   * @brief Add an entry to the history stack.
   * @param std::string aLine command to be put in history
   */
  MVC_EXPORT_FCN void addHistory(std::string aLine);

  /**
   * @brief load list of function names as strings to
   *        be used with autocompletion
   */
  MVC_EXPORT_FCN void loadFunctions();

  /**
   * @brief Function directly needed by readline
   */
  char* completionGenerator(const char * aText, int aState);

  /**
   * @brief Function directly needed by readline
   */
  char** completer(const char* text, int start, int end);

  /**
   * @brief Contains shared data used by readline library
   */
  class Data{
    public:
      static std::vector<std::string> pFunctions;
  };
};
