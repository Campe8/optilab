//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 12/13/2017
//

#pragma once

#include "MVCDefs.hpp"

#include "GlobalsType.hpp"
#include "DataObject.hpp"
#include "ModelContext.hpp"
#include "MVCModelContext.hpp"
#include "ContextObject.hpp"
#include "ModelCallbackHandler.hpp"
#include "VariableContextObject.hpp"

namespace MVC { namespace Utils {

  /// Replaces the domain values in "aCtx" with the given vector of (vector of)
  /// values (e.g., for matrices).
  /// @note "aCtx" must be a context for an object type "var"
  MVC_EXPORT_FCN void replaceDomainInVarCtxObj(const MVCModelContext::CtxObjSPtr& aCtxObj,
                                               const std::vector<std::vector<std::string>>& aDom);
  
  /// Replaces the domain values in "aCtx" with the given domain list.
  /// Returns true if the replaced domain was a singleton domain, returns false otherwise
  /// @note "aCtx" must be a context for an object type "var"
  MVC_EXPORT_FCN bool replaceDomainInVarCtxObj(const MVCModelContext::CtxObjSPtr& aCtxObj,
                                               const Solver::ModelCallbackHandler::DomainList& aDom);
  
  /// Returns the name of the variable which has assigned the given data object representing a domain
  MVC_EXPORT_FCN std::string getLinkedVariableIDFromDomainDataObject(const Interpreter::DataObject& aDO);
  
  /// Returns true if the given data object is a variable matrix subscript
  MVC_EXPORT_FCN bool isVarMatrixSubscr(const Interpreter::DataObject& aDO);
  
  /// Returns true if the given data object is an optimization variable
  MVC_EXPORT_FCN bool isVarOptimization(const Interpreter::DataObject& aDO);
  
  /// Returns the ID for the given named object, if any
  MVC_EXPORT_FCN std::string getIDFromDataObject(const Interpreter::DataObject& aDO);
  
  /// Returns the ID for the given base object, if any
  MVC_EXPORT_FCN std::string getIDFromDataObject(const Interpreter::BaseObject* aBaseObject);
  
}}//end namespace MVC/Utils
