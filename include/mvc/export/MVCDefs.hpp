//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 06/03/2017
//

#pragma once

#include <vector>
#include <string>

namespace MVC {
  
  static std::string EndBlockStmt = "end";
  
  static std::vector<std::string> BlockOpeningStmtSet =
  {
      "for "
    , "if "
  };
  
  static std::vector<std::string> BlockClosingStmtSet =
  {
    "end"
  };
  
  
  static std::vector<std::string> MultiLineOpeningStmtSet =
  {
      "["
  };
  
  static std::vector<std::string> MultiLineClosingStmtSet =
  {
      "]"
  };
  
  enum MVCEventType : int {
      MVC_EVT_EVAL_STMT = 0
    , MVC_EVT_EVAL_CODE_OBJ
    , MVC_EVT_CHANGE_OUTSTREAM
    , MVC_EVT_SOLUTION
    , MVC_EVT_METRICS
    , MVC_EVT_NO_SOLUTION
    , MVC_EVT_READ_FILE
    , MVC_EVT_WRITE_OUTPUT
    , MVC_EVT_ERROR
  };
  
} // end namespace MVC
