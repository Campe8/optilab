//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 07/28/2017
//
// Dispatcher of MVC events.
//

#pragma once

#include "MVCExportDefs.hpp"
#include "MVCDefs.hpp"

#include "MVCEvent.hpp"

#include <boost/optional.hpp>

#include <iostream>
#include <queue>
#include <unordered_map>
#include <memory>
#include <functional>
#include <exception>

namespace MVC {
  
  using EventHandlerFcn = std::function<void(MVCEvent*)>;
  
  class MVC_EXPORT_CLASS MVCEventDispatcher {
  public:
    MVCEventDispatcher();
    
    /// Register "aFcn" as event handler for an event of type "aEventType".
    /// @note when registering a member function, use bind with the instance
    /// of the class the member function must be called upon.
    /// For example:
    /// registerEventHandler(std::bind(&MyClass::fcn, this, std::placeholders::_1), aEventType);
    /// Where "fcn" is a member function of "MyClass" and "registerEventHandler" is called
    /// inside "this" object of type MyClass
    void registerEventHandler(EventHandlerFcn aFcn, MVCEventType aEventType);
    
    /// Dispatches "aEvent"
    void dispatch(const std::shared_ptr<MVCEvent>& aEvent);
    
    /// Clears the queue of events
    void clearEventQueue();
    
  private:
    /// Lock on dispatcher to handle dispatching
    /// of events sequentially
    bool pEventDispatcherLock;
    
    /// Queue of events to dispatch
    std::queue<std::shared_ptr<MVCEvent>> pEventQueue;
    
    /// Map of event handler functors paired with the type
    /// of events they handle
    std::unordered_map<int, EventHandlerFcn> pHandlerMap;
    
    /// Dispatches given event to the assigned handler, if any
    void dipatchEventToFunctor(MVCEvent* aEvent);
    
    /// Returns the handler for the given event type, if previously registered
    boost::optional<EventHandlerFcn> findEventHandler(int aEventType) const;
  };
  
}//end namespace MVC
