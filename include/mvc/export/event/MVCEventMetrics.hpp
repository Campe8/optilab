//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 12/09/2018
//

#pragma once

// Base class
#include "MVCEvent.hpp"

// Solver Solution
#include "SolverSolution.hpp"

#include <memory>

// Forward declarations
namespace Interpreter {
  class BaseObject;
}// end namespace Interpreter

namespace MVC {
  
  class MVC_EXPORT_CLASS MVCEventMetrics : public MVCEvent {
  public:
    /*
     * metrics event: notify about metrics.
     */
    static bool isa(const MVCEvent* aEvent);
    static MVCEventMetrics* cast(MVCEvent* aEvent);
    
    /// Returns the object of type Metrics
    std::shared_ptr<Interpreter::BaseObject> getMetrics();
    
  protected:
    friend class MVCEventFactory;
    
    MVCEventMetrics(MVCEvent::MVCEventActor aSrc, MVCEvent::MVCEventActor aDst);
    
  private:
    /// Pointer to the metrics object instance
    std::shared_ptr<Interpreter::BaseObject> pMetrics;
  };
  
}//end namespace MVC
