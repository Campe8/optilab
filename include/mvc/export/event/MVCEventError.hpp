//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 07/16/2017
//

#pragma once

// Base class
#include "MVCEvent.hpp"

#include <string>

namespace MVC {
  
  class MVC_EXPORT_CLASS MVCEventError : public MVCEvent {
  public:
    enum ErrorLevel : int {
        LEVEL_WARNING = 0
      , LEVEL_ERROR
      , LEVEL_NONE
    };
    
    /*
     * Error event: notify about an error.
     */
    static bool isa(const MVCEvent* aEvent);
    
    static MVCEventError* cast(MVCEvent* aEvent);
    
    inline ErrorLevel getErrorLevel() const
    {
      return pLevel;
    }
    
    inline std::string what() const
    {
      return pErrorMsg;
    }
    
  protected:
    friend class MVCEventFactory;
    
    MVCEventError(const std::string& aMsg,
                  ErrorLevel aLevel,
                  MVCEvent::MVCEventActor aSrcActor,
                  MVCEvent::MVCEventActor aDstActor);
    
  private:
    /// Error message
    std::string pErrorMsg;
    
    /// Error level
    ErrorLevel pLevel;
  };
  
}//end namespace MVC
