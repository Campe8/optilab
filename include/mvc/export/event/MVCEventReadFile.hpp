//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/28/2017
//

#pragma once

// Base class
#include "MVCEvent.hpp"

#include <string>

namespace MVC {
  
  class MVC_EXPORT_CLASS MVCEventReadFile : public MVCEvent {
  public:
    /*
     * Read file event: notify the view to open and read a file.
     * This event is assumed to have VIEW as destination.
     */
    static bool isa(const MVCEvent* aEvent);
    
    static MVCEventReadFile* cast(MVCEvent* aEvent);
    
    inline std::string getPath() const
    {
      return pPath;
    }
    
  protected:
    friend class MVCEventFactory;
    
    /// Constructor.
    /// @note destiantion is always VIEW
    MVCEventReadFile(const std::string& aPath, MVCEvent::MVCEventActor aSrc);
    
  private:
    /// Path to the file to read
    std::string pPath;
  };
  
}//end namespace MVC
