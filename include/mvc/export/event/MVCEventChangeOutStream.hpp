//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 25/12/2017
//

#pragma once

// Base class
#include "MVCEvent.hpp"

#include <ostream>

namespace MVC {
  
  class MVC_EXPORT_CLASS MVCEventChangeOutStream : public MVCEvent {
  public:
    /*
     * Change out stream event: notify about changing the output stream.
     * This event is assumed to have MODEL as source and
     * VIEW as destination.
     */
    static bool isa(const MVCEvent* aEvent);
    
    static MVCEventChangeOutStream* cast(MVCEvent* aEvent);
    
    /// Sets the output stream to be changed into
    void setOutStream(std::ostream* aOutStream);
    
    /// Returns the reset flag
    inline bool resetOutStream() const
    {
      return pResetStream;
    }
    
    /// Returns the output stream to be change into
    inline std::ostream* getOutStream() const
    {
      return pOutStream;
    }
    
  protected:
    friend class MVCEventFactory;

    /// Constructor with "aReset" flag: if true, this events carries a "reset" message.
    /// For example, reset the stream to the default one.
    /// @note "aReset" has a priority over the internal stream, i.e., if "aReset" is true, this
    /// events carries a reset message even if getOutStream() returns a stream that is not nullptr
		MVCEventChangeOutStream(bool aReset, MVCEvent::MVCEventActor aSrc, MVCEvent::MVCEventActor aDst);
    
  private:
    /// Reset stream flag
    bool pResetStream;
    
    /// Pointer to the output stream to set
    std::ostream* pOutStream;
  };

}//end namespace MVC
