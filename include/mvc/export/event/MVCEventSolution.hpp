//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 06/18/2017
//

#pragma once

// Base class
#include "MVCEvent.hpp"

// Solver Solution
#include "SolverSolution.hpp"

#include <string>
#include <list>
#include <cassert>

namespace MVC {
  
  class MVC_EXPORT_CLASS MVCEventSolution : public MVCEvent {
  public:
    using Iter = std::list<std::string>::iterator;
    
  public:
    /*
     * Solution event: notify about new solution found by the model.
     * This event is assumed to have MODEL as source and
     * CONTROLLER as destination.
     */
    static bool isa(const MVCEvent* aEvent);
    
    static MVCEventSolution* cast(MVCEvent* aEvent);
    
    /// Returns the list of solutions
    inline const std::list<std::string>& getSolutionList() const { return pSolutionList; }
    
    /// Returns the total number of solution found
    inline std::size_t getNumSolutions() const { return pSolutions; }
    
    /// For range iterator
    Iter begin() { return pSolutionList.begin(); }
    Iter end()   { return pSolutionList.end();   }
    
  protected:
    friend class MVCEventFactory;
    
    /// Constructor:
    /// - "aSolutionList": list of solutions
    MVCEventSolution(std::size_t aNumSolutions, const std::list<std::string>& aSolutionList,
                     MVCEvent::MVCEventActor aSrc, MVCEvent::MVCEventActor aDst);
    
  private:
    /// Total number of solutions found
    std::size_t pSolutions;
    
    /// List of "string" solutions
    std::list<std::string> pSolutionList;
  };
  
}//end namespace MVC
