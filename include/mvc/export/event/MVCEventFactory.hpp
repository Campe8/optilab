//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/30/2017
//

#pragma once

#include "MVCExportDefs.hpp"
#include "MVCMacro.hpp"

// Events
#include "MVCEventChangeOutStream.hpp"
#include "MVCEventError.hpp"
#include "MVCEventEvalStmt.hpp"
#include "MVCEventEvaluateCodeObject.hpp"
#include "MVCEventNoSolution.hpp"
#include "MVCEventMetrics.hpp"
#include "MVCEventReadFile.hpp"
#include "MVCEventSolution.hpp"
#include "MVCEventWriteOutput.hpp"

#include <utility>
#include <memory>

namespace MVC {
  
  class MVC_EXPORT_CLASS MVCEventFactory {
  public:
    enum EventSrcDst : short {
        EVT_V2C = 0
      , EVT_V2M
      , EVT_C2V
      , EVT_C2M
      , EVT_M2V
      , EVT_M2C
    };
    
    /// Converts actors to EventSrcDst enum
    static EventSrcDst actorsToSrcDst(MVCEvent::MVCEventActor aAct1, MVCEvent::MVCEventActor aAct2);
    
    /// Event for evaluate statement
    MVCEventEvalStmt* mvcEventEvalStmt(const CLI::CLIString& aStmt1, EventSrcDst aSD);
    MVCEventEvalStmt* mvcEventEvalStmt(const CLI::CLIString& aStmt1, const CLI::CLIString& aSrcStmt,
                                       EventSrcDst aSD);
    
    /// Event for evaluating a code object
    MVCEventEvaluateCodeObject* mvcEventEvalCodeObject(const Interpreter::DataObject& aDataObject,
                                                       EventSrcDst aSD);
    
    /// Event for solutions:
    /// @param aNum is the number of solutions found
    /// @param aList is the list of solutions to print
    /// @note aNum doesn't need to be the same as the size of the solutions to print
    MVCEventSolution* mvcEventSolution(std::size_t aNum, const std::list<std::string>& aList,
                                       EventSrcDst aSD);
    
    /// Event for no solutions on unsatisfiable models
    MVCEventNoSolution* mvcEventNoSolution(EventSrcDst aSD);
    
    /// Event for reporting strings on output through view
    MVCEventWriteOutput* mvcEventWriteOutput(const std::list<std::string>& aStringList,
                                             EventSrcDst aSD);
    
    /// Event for reporting errors
    MVCEventError* mvcEventError(const std::string& aErrMsg, EventSrcDst aSD);
    MVCEventError* mvcEventWarning(const std::string& aErrMsg, EventSrcDst aSD);
    
    /// Event for reading files
    MVCEventReadFile* mvcEventReadFile(const std::string& aPath, EventSrcDst aSD);
    
    /// Event for changing outstream to "aOStream"
    MVCEventChangeOutStream* mvcEventChangeOutStream(std::ostream* aOStream, EventSrcDst aSD);
    MVCEventChangeOutStream* mvcEventResetOutStream(EventSrcDst aSD);
    
    /// Event for metrics
    MVCEventMetrics* mvcEventMetrics();
  };

}//end namespace MVC
