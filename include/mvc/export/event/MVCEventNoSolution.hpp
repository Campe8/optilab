//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/22/2017
//

#pragma once

// Base class
#include "MVCEvent.hpp"

#include <string>
#include <cassert>

namespace MVC {
  
  class MVC_EXPORT_CLASS MVCEventNoSolution : public MVCEvent {
  public:
    /*
     * Solution event: notify about new solution found by the model.
     * This event is assumed to have MODEL as source and
     * CONTROLLER as destination.
     */
    ~MVCEventNoSolution() = default;
    
    static bool isa(const MVCEvent* aEvent);
    
    static MVCEventNoSolution* cast(MVCEvent* aEvent);
    
  protected:
    friend class MVCEventFactory;

		MVCEventNoSolution(MVCEvent::MVCEventActor aSrc, MVCEvent::MVCEventActor aDst);
  };

}//end namespace MVC
