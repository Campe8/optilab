//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/30/2017
//

#pragma once

#include "MVCExportDefs.hpp"
#include "MVCDefs.hpp"

namespace MVC {
  
  class MVC_EXPORT_CLASS MVCEvent {
  public:
    enum MVCEventActor : int {
        MVC_ACTOR_VIEW = 0
      , MVC_ACTOR_MODEL
      , MVC_ACTOR_CONTROLLER
    };
    
    ~MVCEvent() = default;
    
    inline MVCEventType getEventType() const
    {
      return pEventType;
    }
    
    inline MVCEventActor getEventSource() const
    {
      return pEventSrc;
    }
    
    inline MVCEventActor getEventDestination() const
    {
      return pEventDst;
    }
    
  protected:
    MVCEvent(MVCEventActor aSrcEvent, MVCEventActor aDstEvent, MVCEventType aEventType);
    
  private:
    /// Type of this event
    MVCEventType pEventType;
    
    /// Source actor of this event
    MVCEventActor pEventSrc;
    
    /// Destination actor for this event
    MVCEventActor pEventDst;
  };

}//end namespace MVC
