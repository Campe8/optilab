//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/30/2017
//

#pragma once

#include "MVCExportDefs.hpp"
#include "MVCMacro.hpp"

#include "MVCEvent.hpp"

#include <unordered_map>
#include <vector>

#include <memory>
#include <cassert>

// Forward declarations
namespace MVC {
  class MVCObserver;
}// end namespace MVC

namespace MVC {
  
  class MVC_EXPORT_CLASS MVCSubject {
  public:
    MVCSubject() = default;
    
    virtual ~MVCSubject() = default;
    
    /// Registers "aObserver" to this subject and pairs it to the type of actor "aEventActor"
    inline void registerObserver(MVCEvent::MVCEventActor aEventActor, MVCObserver* aObserver)
    {
      assert(aObserver);
      pObserverRegister[static_cast<int>(aEventActor)].push_back(aObserver);
    }
    
    /// Notifies the destination actor about the event "aEvent"
    void notify(const std::shared_ptr<MVCEvent>& aEvent) const;
    
  private:
    /// Register of observers attached to this subject
    std::unordered_map<int, std::vector<MVCObserver*>> pObserverRegister;
  };

  class MVC_EXPORT_CLASS MVCObserver {
  public:
    MVCObserver(MVCEvent::MVCEventActor aActor);
    
    virtual ~MVCObserver() = default;
    
    /// Observes "aSubject"
    void observe(MVCSubject* aSubject);
    
    /// Updates this observer based on "aEvent"
    virtual void update(const std::shared_ptr<MVCEvent>& aEvent) = 0;
    
  private:
    MVCEvent::MVCEventActor pEventActor;
  };
  
}//end namespace MVC
