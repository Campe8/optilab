//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 06/03/2017
//

#pragma once

// Base class
#include "MVCEvent.hpp"

// CLI stmt
#include "CLIUtils.hpp"

#include <string>
#include <cassert>

namespace MVC {
  
  class MVC_EXPORT_CLASS MVCEventEvalStmt : public MVCEvent {
  public:
    /*
     * Evaluate statement event.
     * This event is assumed to have VIEW as source and 
     * CONTROLLER as destination.
     */
    static bool isa(const MVCEvent* aEvent);
    
    static MVCEventEvalStmt* cast(MVCEvent* aEvent);
    
    inline void setSourceStmt(const CLI::CLIString& aStr)
    {
      pSrcStmt = aStr;
    }
    
    inline CLI::CLIString getSourceStmt() const
    {
      return pSrcStmt;
    }
    
    /// Returns the CLIStmt hold by this event
    inline CLI::CLIStmt getEvalStmt() const
    {
      return pStmt;
    }
    
  protected:
    friend class MVCEventFactory;
    
    MVCEventEvalStmt(const std::string& aStmt, MVCEvent::MVCEventActor aSrc, MVCEvent::MVCEventActor aDst);
    
  private:
    /// Statement to evaluate
    CLI::CLIStmt pStmt;
    
    /// Original source statement string
    CLI::CLIString pSrcStmt;
  };

}//end namespace MVC
