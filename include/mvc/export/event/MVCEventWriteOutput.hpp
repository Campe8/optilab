//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 06/18/2017
//

#pragma once

// Base class
#include "MVCEvent.hpp"

#include <string>
#include <list>
#include <cassert>

namespace MVC {
  
  class MVC_EXPORT_CLASS MVCEventWriteOutput : public MVCEvent {
  public:
    /*
     * Write on output event.
     * This event is assumed to have CONTROLLER as source and
     * VIEW as destination.
     */
    static bool isa(const MVCEvent* aEvent);
    
    static MVCEventWriteOutput* cast(MVCEvent* aEvent);
    
    /// Returns the number of strings carried by this event
    inline std::size_t size() const
    {
      return pStringRegister.size();
    }
    
    /// Returns the string at index "aSolIndex"
    std::string getString(std::size_t aIdx) const;
    
  protected:
    friend class MVCEventFactory;
    
    /// Constructor
    /// - "aOutStrings" is a vector containing all the strings to output
    MVCEventWriteOutput(const std::list<std::string>& aOutStrings, MVCEvent::MVCEventActor aSrc, MVCEvent::MVCEventActor aDst);
    
  private:
    std::list<std::string> pStringRegister;
  };

}//end namespace MVC
