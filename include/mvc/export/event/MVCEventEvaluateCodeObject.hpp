//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 2/22/2018
//

#pragma once

// Base class
#include "MVCEvent.hpp"

#include "CodeObject.hpp"
#include "DataObject.hpp"

namespace MVC {
  
  class MVC_EXPORT_CLASS MVCEventEvaluateCodeObject : public MVCEvent {
  public:
    
    /*
     * Evaluate code object event: notify evaluation of a code object.
     */
    static bool isa(const MVCEvent* aEvent);
    static MVCEventEvaluateCodeObject* cast(MVCEvent* aEvent);
    
    inline const Interpreter::DataObject& getDataObject() const { return pDataObject; }
    inline const Interpreter::CodeObject* getCodeObject() const { return pDataObject.getByteCode().get(); }
    
  protected:
    friend class MVCEventFactory;
    
    // The given data object contains the code object to interpret
    MVCEventEvaluateCodeObject(const Interpreter::DataObject& aDataObject,
                               MVCEvent::MVCEventActor aSrcActor,
                               MVCEvent::MVCEventActor aDstActor);
    
  private:
    /// Data object containing the code object
    Interpreter::DataObject pDataObject;
  };
  
}//end namespace MVC
