//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 12/10/2018
//

#pragma once

namespace MVC {
  
  namespace ControllerConst {
    
    extern const char* COMMAND_ERR;
    extern const char* SATURATION_ERR;
    
  }// end namespace ControllerConst
  
}// end namespace MVC
