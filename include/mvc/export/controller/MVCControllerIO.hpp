//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 22/01/2018
//
// Controller class for I/O:
// Reads input, process statements, and writes
// output by coordinating directly the view
// and the model.
// This controller is responsable ONLY for
// handling input/output from/to the view, i.e.,
// its task is to manage the view allowing the user
// to interact with the model using the view.
//

#pragma once

// Base class
#include "MVCController.hpp"

namespace MVC {
  
  class MVC_EXPORT_CLASS MVCControllerIO : public MVCController {
  public:
    MVCControllerIO(const std::shared_ptr<MVCView>& aView, const std::shared_ptr<MVCModel>& aModel);
    virtual ~MVCControllerIO() = default;
    
    /// Sets the symbol to use as prompt symbol
    inline void setPromptSymbol(const std::string& aSymb) { pPromptSymb = aSymb; }
    inline void setPromptMultilineSymbol(const std::string& aSymb) { pPromptMultilineSymb = aSymb; }
    
    /// Executes read input -> process input -> write output
    /// just once
    void step() override;

    /// Runs the controller which operates on the model
    /// by working on I/O with the view
    void exe() override;

  private:
    std::string pPromptSymb;
    std::string pPromptMultilineSymb;
    
    /// Current line to process
    std::vector<std::string> pInBuffer;
    
    /// Boolean flag for termination
    bool pTerminate;
    
    /// Clears input buffer
    void clearInBuffer();
    
    /// Reads next input from view
    void readInput();
    
    /// Processes input line
    void processInput();
    
    /// Writes output, ask the view to dump
    /// any received output
    void writeOutput();
    
    /// Checks exit status and returns true if the controller
    /// has to terminate, false otherwise
    bool exit() const;
  };

}//end namespace MVC
