//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 01/04/2018
//
// Controller implementation acts as a layer on the controller.
//

#pragma once

#include "MVCExportDefs.hpp"

// Event management
#include "MVCEvent.hpp"
#include "MVCEventFactory.hpp"
#include "MVCEventDispatcher.hpp"

#include "InterpreterDefs.hpp"

#include <list>
#include <memory>

// Forward declaration
namespace MVC {
  class MVCController;
}// end namespace MVC

namespace MVC {
  
  namespace _private {
    class MVC_EXPORT_CLASS MVCControllerImpl {
      /*
       * Controller class implementation,
       * implementing the controller subject-observer pattern.
       */
      
    protected:
      friend MVCController;
      
      MVCControllerImpl(MVCController* aController);
      
      /// Updates the controller that owns this instance
      /// on the received event "aEvent".
      /// It handles one event at a time and it waits the event in proces
      /// to be completed before handling a new event
      void update(const std::shared_ptr<MVCEvent>& aEvent);
      
    private:
      /// Pointer to the instance of the controller
      /// owning this implementation
      MVCController* pController;

      /// Event dispatcher
      std::unique_ptr<MVCEventDispatcher> pEventDispatcher;
      
      /// Event factory
      std::unique_ptr<MVCEventFactory> pEventFactory;
      
      /// Utility method: registers event handles
      /// in the event dispatcher
      void registerEventHandlers();
      
      /// Utility function: sends "aMsg" message to the view
      void sendMsgEventToView(const std::string& aMsg);
      
      /// Utility function: sends "aErr" message
      /// as a warning event with message "aErr" to the view
      void sendErrorEventToView(const std::string& aErr, MVCEventError::ErrorLevel aErrLevel);
      
      /// === Handlers for events === ///
      
      /// Notifies view about a solution
      void handleSolutionEvent(MVCEvent* aEvent);
      
      /// Notifies view about an error
      void handleErrorEvent(MVCEvent* aEvent);
      
      /// Notifies view about metrics
      void handleMetricsEvent(MVCEvent* aEvent);
      
      /// Handles exceptions captured by the virtual machine
      void handleVMException(Interpreter::InterpreterException::ExceptionType aEType,
                             const std::string& aEValue);
    };
  }// end namespace _private
  
}//end namespace MVC
