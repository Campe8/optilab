//
// Copyright OptiLab 2018-2019. All rights reserved.
//
// Created by Federico Campeotto on 2/18/2018
//
// Post constraint function: post the constraint
// received as a function data object into the
// context of the model.
//

#pragma once

// Base class
#include "FunctionConstraint.hpp"

namespace MVC {
  
  class FunctionPostConstraint : public FunctionConstraint {
  public:
		FunctionPostConstraint();
    
    bool requiresFcnObjectAsArgument() const override;
    
    Interpreter::DataObject callback(const std::vector<Interpreter::DataObject>& aArgs) override;

		std::string name() const override;
    
    bool hasOutput() const override;
  };
  
} // end namespace MVC
