//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 12/13/2018
//
// "getMetricsOptions" function: creates a metrics options
//  object in the context.
//

#pragma once

// Base class
#include "Function.hpp"

namespace MVC {
  
  class FunctionGetMetricsOptions : public Function {
  public:
    FunctionGetMetricsOptions();
    
    Interpreter::DataObject callback(const std::vector<Interpreter::DataObject>& aArgs) override;
    
    std::string name() const override;
    
    bool hasOutput() const override;
    
  private:
    /// Actual getMetricsOptions function: creates a metrics options object
    Interpreter::DataObject getMetricsOptions();
  };
  
} // end namespace MVC
