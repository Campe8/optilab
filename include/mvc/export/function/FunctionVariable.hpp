//
// Copyright OptiLab 2019. All rights reserved.
//
// Created by Federico Campeotto on 04/09/2019
//
// Variable object constructor
//

#pragma once

// Base class
#include "Function.hpp"

namespace MVC {
  
  class FunctionVariable : public Function {
  public:
    /// Call the constructor of a Variable object:
    /// Variable (arg1, [arg2], [arg3])
    /// where:
    /// - arg1 must be a non empty list of domain elements
    /// - arg2 is the list of domain dimensions
    /// - arg3 is a string "minimize" "maximize"
    /// @note arg2 and arg3 can be exchanged
		FunctionVariable();
    
    /// Creates a variable object
    Interpreter::DataObject callback(const std::vector<Interpreter::DataObject>& aArgs) override;

    bool hasOutput() const override;
    
		std::string name() const override;

  private:
		/// Actual Variable constructor
		Interpreter::DataObject Variable(const std::vector<Interpreter::DataObject>& domain,
                                     const std::vector<Interpreter::DataObject>& dimensions,
                                     const std::string& optimization);
  };
  
} // end namespace MVC
