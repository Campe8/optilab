//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 11/4/2018
//
// "search" function: create a search environment object in the context.
// The search object defines the set of variables involved
// in the search tree and the options for searching.
//

#pragma once

// Base class
#include "FunctionBranch.hpp"

namespace MVC {
  
  class FunctionSearch : public FunctionBranch {
  public:
    FunctionSearch();
    
    Interpreter::DataObject callback(const std::vector<Interpreter::DataObject>& aArgs) override;
    
    bool requiresFcnObjectAsArgument() const override;
    
    std::string name() const override;
    
    bool hasOutput() const override;
  };
  
} // end namespace MVC
