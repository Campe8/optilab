//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 08/06/2017
//
// Base class for (inline) functions invoked by the
// virtual machine.
//

#pragma once

#include "FunctionRegister.hpp"
#include "VirtualMachine.hpp"

namespace MVC {
  
  class Function {
  public:
    using FcnCallback = Interpreter::VirtualMachine::FcnHandler;
    
  public:
    virtual ~Function() = default;
    
    /// Returns this function ID
    inline FunctionId getFcnId() const { return pFcnId; }
    
    /// Sets the instance of the view
    inline void setView(const std::shared_ptr<MVCView>& aView) { pView = aView; }
    
    /// Sets the instance of the model
    inline void setModel(const std::shared_ptr<MVCModel>& aModel) { pModel = aModel; }
    
    /// Sets the instance of the controller.
    /// @note there is no ownership of the controller pointer,
    /// in particular, the owner of this function instance is
    /// the controller itself
    inline void setController(MVCController* aController) { pController = aController; }
    
    /// Sets this callback function into the given function register
    virtual void setCallback(FunctionRegister* aReg);
    
    /// Returns true if the this functions requires the data object
    /// used by the virtual machine to run this function.
    /// If true, the virtual machine will set the (function) data object
    /// as only argument in the function callback.
    /// If false, the virtual machine will invoke this function with
    /// the vector of arguments as parsed by the driver.
    /// @note if this function requires the data object as argument,
    /// the actual function arguments are set in the "composite" vector
    /// inside the function data object.
    /// For example:
    /// - nq(1, 2) requires the function object as argument since that
    ///            represents the object stored in the model context
    /// - ctx(x)   does not require the function object as argument
    ///            since it runs on the argument x without altering the
    ///            model context
    virtual bool requiresFcnObjectAsArgument() const { return false; };
    
    /// Returns true if this function returns a non void DataObject, false otherwise.
    /// @note void DataObject has output represents a void type
    /// has output in the signature of the function
    virtual bool hasOutput() const { return false; };
    
    /// Callback function invoked as inline function by the virtual machine
    /// to execute this function
    virtual Interpreter::DataObject callback(const std::vector<Interpreter::DataObject>& aArgs) = 0;
    
    /// Returns the name of this function
    virtual std::string name() const = 0;
    
  protected:
    enum NotificationLevel : char {
        LEVEL_NONE = 0
      , LEVEL_WARNING
      , LEVEL_ERROR
    };
    
  protected:
    Function(FunctionId aFcnId);
    
    /// Resets this function id.
    /// Use with caution!!!
    inline void resetFunctionId(FunctionId aFcnId) { pFcnId = aFcnId; }
    
    /// Returns the controller, i.e., the caller of this function
    inline MVCController* getController() const { return pController; }
    
    /// Returns internal pointer to the instance of the current view
    inline std::shared_ptr<MVCView> getView() const { return pView; }
    
    /// Returns internal pointer to the instance of the current model
    inline std::shared_ptr<MVCModel> getModel() const { return pModel; }
    
    /// Returns internal event factory pointer
    inline MVCEventFactory* getEventFactory() const { return pEventFactory.get(); }
    
    /// Notifies the controller of this function.
    /// @note if "aLevel" is NONE, notify the controller about a message
    void notifyController(const std::string& aMsg, NotificationLevel aLevel=LEVEL_NONE);
    
    /// Checks input Boolean value and, if not true,
    /// sends notification "aMsg" to view through the controller
    void assertAndNotify(bool aTest, const std::string& aMsg);
    
  private:
    /// Function id
    FunctionId pFcnId;
    
    /// Event factory to send event to view as side-effect of
    /// the callback function
    std::unique_ptr<MVCEventFactory> pEventFactory;
    
    /// Controller calling this function
    MVCController* pController;
    
    /// View to execute the function on
    std::shared_ptr<MVCView> pView;
    
    /// Model to execute the function on
    std::shared_ptr<MVCModel> pModel;
  };
  
} // end namespace MVC
