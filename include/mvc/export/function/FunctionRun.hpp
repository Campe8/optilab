//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/28/2017
//
// "run" function: read and run input models.
//

#pragma once

// Base class
#include "Function.hpp"

namespace MVC {
  
  class FunctionRun : public Function {
  public:
		FunctionRun();
    
    Interpreter::DataObject callback(const std::vector<Interpreter::DataObject>& aArgs) override;

		std::string name() const override;

	private:
		/// Actual run function
    void run(const std::string& aPath);
    
    /// File reader
    std::unique_ptr<std::fstream> pFileReaderStream;
    
    /// Opens the file "aPath" if possible and sets the
    /// input stream to the opened file reader
    void switchOnFileReader(const std::string& aPath);
    
    /// Switches off file reader
    void switchOffFileReader();
  };
  
} // end namespace MVC
