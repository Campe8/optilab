//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/07/2017
//

#pragma once

#include "FunctionDefs.hpp"

// Macros for messages
#define ERR_MSG_NUM_ARGS       "invalid number of arguments"
#define ERR_MSG_INPUT_ARG      "invalid input argument"
#define ERR_MSG_INPUT_ARG_TYPE "invalid input argument"
#define ERR_MSG_INPUT_ARG_SIZE "invalid input argument size"
#define ERR_MSG_FILE_NOT_FOUND "file not found"
#define ERR_MSG_FILE_INVALID   "invalid file"
#define ERR_MSG_ERR_MODEL      "invalid model"
#define ERR_MSG_OBJ_NOT_FOUND  "context object not found"
#define ERR_MSG_UNDEF_FUNCTION "undefined function"
#define ERR_MSG_INVALID_INTERP "invalid object"

#define FCN_ASSERT_MSG(expr, msg) \
	((expr) ? (void)0 : MVC::throwWithMessage((msg)))
