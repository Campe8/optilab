//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/06/2017
//
// "verbose" function: enable/disable pretty print.
//

#pragma once

// Base class
#include "Function.hpp"

namespace MVC {
  
  class FunctionVerbose : public Function {
  public:
		FunctionVerbose();
    
    Interpreter::DataObject callback(const std::vector<Interpreter::DataObject>& aArgs) override;

		std::string name() const override;

  private:
		/// Actual verbose function
		void verbose(int aVerboseLevel);
  };
  
} // end namespace MVC
