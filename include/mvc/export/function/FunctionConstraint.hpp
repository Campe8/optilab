//
// Copyright OptiLab 2019. All rights reserved.
//
// Created by Federico Campeotto on 4/10/2019
//
// Constraint object constructor
//

#pragma once

// Base class
#include "Function.hpp"

namespace MVC {
  
  class FunctionConstraint : public Function {
  public:
    /// Constructor for Constraint objects:
    /// Constraint("name", arg1, arg2, ...);
		FunctionConstraint();
    
    /// Creates a new object constraint.
    /// Arguments are (in order):
    /// - string name of the constraint to create;
    /// - list of constraint arguments
    Interpreter::DataObject callback(const std::vector<Interpreter::DataObject>& aArgs) override;

		std::string name() const override;
    
    /// The callback method returns the constraint object
    bool hasOutput() const override;
    
  protected:
    /// Actual callback constructor method.
    /// It takes "aConDesc" which is a data object of string type containing
    /// the constraint name, and having the constraint arguments as composite objects
    Interpreter::DataObject Constraint(const Interpreter::DataObject& aConDesc);
  };
  
} // end namespace MVC
