//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 08/06/2017
//
// "solve" function: runs the solver on the
// current context.
//

#pragma once

// Base class
#include "Function.hpp"

// Forward declarations
namespace Interpreter {
  class BaseObject;
}// end namespace Interpreter

namespace MVC {
  
  class FunctionSolve : public Function {
  public:
    FunctionSolve();
    
    Interpreter::DataObject callback(const std::vector<Interpreter::DataObject>& aArgs) override;
    
    std::string name() const override;
    
  private:
    /// Actual solve function: runs the engine on the model.
    /// This will call the engine on the registered model setting the number of solutions
    /// to the given value. Also, if "aMetricsOpts" is not nullptr, it will look for
    /// the parameters of "aMetricsOpts" describing the metrics to report
    void solve(long long aNumSol, Interpreter::BaseObject* aMetricsOpts);
  };
  
} // end namespace MVC
