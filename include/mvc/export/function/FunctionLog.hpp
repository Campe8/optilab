//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 12/25/2017
//
// "log" function: re-direct output to file.
//

#pragma once

// Base class
#include "Function.hpp"

#include <fstream>
#include <memory>

namespace MVC {
  
  class FunctionLog : public Function {
  public:
    FunctionLog();
    
    Interpreter::DataObject callback(const std::vector<Interpreter::DataObject>& aArgs) override;

    std::string name() const override;
    
  private:
    /// Log active flag
    bool pLogOn;
    
    /// Name of the file to open for log
    std::string pLogOut;
    
    /// Output stream to write to if "pLogOn" is true
    std::unique_ptr<std::fstream> pOutStream;
    
    /// Log function: logs output to file if "aOn" is true
    void log(bool aLogOn);
    
    /// Opens output stream.
    /// If output stream is already opened, closes the stream
    /// and open a new one
    void openOutStream();
    
    /// Closes output stream if any
    void closeOutStream();
    
    /// Sets the log output file
    inline void setOutFile(const std::string& aFile)
    {
      pLogOut = aFile;
    }
    
  };
  
} // end namespace MVC
