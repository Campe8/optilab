//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/06/2017
//
// Registry of functions.
// Functions are invoked from CLI.
//

#pragma once

#include "MVCExportDefs.hpp"

#include "FunctionDefs.hpp"

// MVC
#include "MVCModel.hpp"
#include "MVCView.hpp"

#include <boost/optional.hpp>

#include <utility>
#include <unordered_map>
#include <memory>

// Forward declarations
namespace MVC {
  class Function;
  class MVCController;
}// end namespace MVC

namespace MVC {
  
  class MVC_EXPORT_CLASS FunctionRegister {
  public:
    using FcnSPtr = std::shared_ptr<Function>;
    
  public:
    FunctionRegister();
    
    /// Sets the view
    inline void setView(const std::shared_ptr<MVCView>& aView) { pView = aView; }
    
    /// Sets the model
    inline void setModel(const std::shared_ptr<MVCModel>& aModel) { pModel = aModel; }
    
    /// Sets the controller
    inline void setController(MVCController* aController) { pController = aController; }
    
    /// Returns register size which corresponds to the number of registered functions
    inline std::size_t getRegisterSize() const { return pFcnRegMap.size(); }
    
    /// Returns the "aIdx" registered function name.
    /// @note throws if "aIdx? is greater than the number of registered functions
    std::string getRegisteredFcnNameByIndex(std::size_t aIdx) const;
    
    /// Returns the registered function "aFcn"
    FcnSPtr getFunction(const std::string& aFcn) const;
    
    /// Returns the callback handler for the registered function "aFcn"
    boost::optional<CallbackFcn> getCallbackFunction(const std::string& aFcn) const;
    
    /// Register "aCallback" as a callback for a function of type "aFcnId".
    /// @note when registering a member function, use bind with the instance
    /// of the class the member function must be called upon.
    /// For example:
    /// registerCallbackHandler(std::bind(&MyClass::fcn, this, std::placeholders::_1), aFcnId);
    /// Where "fcn" is a member function of "MyClass" and "registerCallbackHandler" is called
    /// inside "this" object of type MyClass
    void registerCallbackHandler(CallbackFcn aCallback, FunctionId aFcnId);
    
  protected:
    /// Registers "aFcn" as a callback function and mapped to its FunctionId and name
    void registerFunction(const std::string& aFcnName, FunctionId aFcnID, const std::shared_ptr<Function>& aFcn);
    
    /// Gets model
    std::shared_ptr<MVCModel> getModel() const { return pModel; }
    
  private:
    /// Function register map mapping function names to their ids
    std::vector<std::pair<std::string, FunctionId>> pFcnRegMap;
    
    /// Map of callback handler functors paired with the type
    /// of function they handle
    std::unordered_map<FunctionIdType, CallbackFcn> pHandlerReg;
    
    /// Reverse map to lookup callback functions by their name
    std::unordered_map<FunctionIdType, FcnSPtr> pFcnReg;
    
    /// Controller, i.e., the function caller
    MVCController* pController;
    
    /// Vierw to execute the function on
    std::shared_ptr<MVCView> pView;
    
    /// Model to execute the function on
    std::shared_ptr<MVCModel> pModel;
    
    /// Instantiates and registers function callbacks
    void populateRegister();
    
    /// Register the post constraint function "aFcn" on all constraints
    void registerPostConstraint(const std::shared_ptr<Function>& aFcn, FunctionId aFcnID);
  };
  
}//end namespace MVC
