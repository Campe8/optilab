//
// Copyright OptiLab 2019. All rights reserved.
//
// Created by Federico Campeotto on 04/17/2019
//
// "Object" function: constructor for objects.
//

#pragma once

// Base class
#include "Function.hpp"

namespace MVC {
  
  class FunctionObject : public Function {
  public:
    FunctionObject();
    
    Interpreter::DataObject callback(const std::vector<Interpreter::DataObject>& aArgs) override;
    
    std::string name() const override;
    
    bool hasOutput() const override;
  };
  
} // end namespace MVC
