//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/06/2017
//
// "ctx" function: prints the context or
// and element in it.
//

#pragma once

// Base class
#include "Function.hpp"

namespace MVC {
  
  class FunctionCtx : public Function {
  public:
    FunctionCtx();
    
    Interpreter::DataObject callback(const std::vector<Interpreter::DataObject>& aArgs) override;
    
    std::string name() const override;

  private:
    /// Actual ctx function: prints the current context
    void ctx();
    
    /// Actual ctx function: prints the object "aDO"
    void ctx(const Interpreter::DataObject& aDO);
    
    /// Sets the pretty print level to print objects on output.
    /// @note returns the current pretty print level
    MVCModelContext::PrettyPrintLevel setPrettyPrintLevel();
  };
  
} // end namespace MVC
