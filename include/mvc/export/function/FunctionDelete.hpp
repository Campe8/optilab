//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/06/2017
//
// "delete" function: delete the context
// or an object in it.
//

#pragma once

// Base class
#include "Function.hpp"

namespace MVC {
  
  class FunctionDelete : public Function {
  public:
		FunctionDelete();
    
    Interpreter::DataObject callback(const std::vector<Interpreter::DataObject>& aArgs) override;
    
    std::string name() const override;

  private:
		/// Actual delete function: delete the entire context
		void deleteCtx();

		/// Actual delete function: delete "aDO" from the context
		void deleteCtx(const Interpreter::DataObject& aDO);
  };
  
} // end namespace MVC
