//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 10/17/2018
//
// "getCLBSOptions" function: creates a clbs options
//  object in the context.
//

#pragma once

// Base class
#include "Function.hpp"

namespace MVC {
  
  class FunctionGetCBLSOptions : public Function {
  public:
    FunctionGetCBLSOptions();
    
    Interpreter::DataObject callback(const std::vector<Interpreter::DataObject>& aArgs) override;
    
    std::string name() const override;
    
    bool hasOutput() const override;
    
  private:
    /// Actual getCLBSOptions function: creates a CLBS options object
    /// of the type given by "aDO"
    Interpreter::DataObject getCBLSOptions(const Interpreter::DataObject& aDO);
  };
  
} // end namespace MVC
