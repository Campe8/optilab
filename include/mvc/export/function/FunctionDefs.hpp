//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/06/2017
//

#pragma once

#include "MVCExportDefs.hpp"
#include "VirtualMachine.hpp"

#include <string>
#include <vector>
#include <unordered_map>
#include <functional>

// Forward declaration
namespace MVC {
  class Function;
}// end namespace MVC

namespace MVC {
  
  typedef int FunctionIdType;
  typedef Function* (*fcnPoster) ();
  typedef std::unordered_map<FunctionIdType, fcnPoster> FcnPosterMap;
  typedef std::function<Interpreter::DataObject(const std::vector<Interpreter::DataObject>&)> CallbackFcn;
  
  MVC_EXPORT_FCN void throwWithMessage(const char* aMsg);
  
  enum FunctionId : FunctionIdType {
      FCN_BRANCH = 0
    , FCN_CTX
    , FCN_CONSTRAINT
    , FCN_DELETE
    , FCN_LOG
    , FCN_SOLVE
    , FCN_OBJECT
    , FCN_OUTPUT
    , FCN_VARIABLE
    , FCN_VERBOSE
    , FCN_RUN
    , FCN_POST_CONSTRAINT
    , FCN_GET_CBLS_OPTIONS
    , FCN_GET_BRANCH_OPTIONS
    , FCN_GET_METRICS_OPTIONS
    , FCN_GET_UNUSED_CTX_VAR_NAME
    , FCN_SEARCH
    , FCN_UNDEF // must be last
    , FCN_TEST  // testing -- do not use
  };
  
  class FcnPosterMapRegister {
  protected:
    friend class FunctionRegister;
    
    FcnPosterMapRegister() = default;
    
    ~FcnPosterMapRegister() = default;
    
    inline FcnPosterMap& getPosterMap()
    {
      return pFcnPosterMap;
    }
    
  private:
    /// Function poster map creation
    static FcnPosterMap pFcnPosterMap;
  };
  
} // end namespace MVC
