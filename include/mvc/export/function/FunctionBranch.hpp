//
// Copyright OptiLab 2019. All rights reserved.
//
// Created by Federico Campeotto on 4/11/2019
//
// Branch object constructor.
//

#pragma once

// Base class
#include "Function.hpp"

namespace MVC {
  
  class FunctionBranch : public Function {
  public:
    FunctionBranch();
    
    /// Callback function: creates and returns a Branch object given:
    /// - a list object of variable identifiers;
    /// - a (optional) BranchOptions object
    Interpreter::DataObject callback(const std::vector<Interpreter::DataObject>& aArgs) override;
    
    std::string name() const override;
    
    bool hasOutput() const override;
    
  protected:
    /// Constructor function for a Branch object
    Interpreter::DataObject Branch(const Interpreter::DataObject& aDO);
    
  private:
    /// Checks consistency of the arguments given to the search function.
    /// @note aDO is a composite object where the value at index 0 must be a list of variable,
    /// while the value at index 1 must be a BranchOptions object.
    /// @note the BranchOptions object is optional
    void checkFunctionArguments(const Interpreter::DataObject& aDO);
  };
  
} // end namespace MVC
