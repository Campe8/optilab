//
// Copyright OptiLab 2019. All rights reserved.
//
// Created by Federico Campeotto on 04/15/2019
//
// "getUnusedCtxVarName" function: create an STRING object
// holding the name of an unused context variable
//

#pragma once

// Base class
#include "Function.hpp"

namespace MVC {
  
  class FunctionGetUnusedCtxVarName : public Function {
  public:
    FunctionGetUnusedCtxVarName();
    
    Interpreter::DataObject callback(const std::vector<Interpreter::DataObject>& aArgs) override;
    
    std::string name() const override;
    
    bool hasOutput() const override;
    
  private:
    /// Returns a data object of STRING type containing an unused context variable name
    Interpreter::DataObject getName();
  };
  
} // end namespace MVC
