//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 29/03/2018
//
// "output" function: sets the output variables,
// i.e. the set of variables to print when
// a solution is found.
//

#pragma once

// Base class
#include "Function.hpp"

namespace MVC {
  
  class FunctionOutput : public Function {
  public:
    FunctionOutput();
    
    Interpreter::DataObject callback(const std::vector<Interpreter::DataObject>& aArgs) override;
    
    std::string name() const override;

  private:
    /// Clears all the ouput semantics of the variables in the context
    void clearOutputSemantic();
    
    /// Set the output semantic on all the variables specified
    /// in the given object
    void output(const Interpreter::DataObject& aDO);
  };
  
} // end namespace MVC
