#pragma once

#include "dll_export.h"

/* This header is being included by files inside this module */

#define MVC_EXPORT            DLL_EXPORT_SYM
#define MVC_EXPORT_CLASS      DLL_EXPORT_SYM
#define MVC_EXPORT_STRUCT     DLL_EXPORT_SYM
#define MVC_EXPORT_FRIEND     DLL_EXPORT_SYM
#define MVC_EXPORT_FCN        DLL_EXPORT_SYM
#define MVC_EXPORT_TEMPLATE   DLL_EXPORT_TEMPLATE
#define MVC_EXPORT_EXTERN_C   extern "C" DLL_EXPORT_SYM
