//
// Copyright OptiLab 2019. All rights reserved.
//
// Created by Federico Campeotto on 04/18/2019
//

#pragma once

namespace MVC {
namespace Constants {
  
namespace View {

  extern const char* COMMENT_SYMB;
  extern const char* ERROR_MESSAGE;
  extern const char* EXIT_COMMAND;
  extern const char* PROMPT_MULTILINE_SYMB;
  extern const char* PROMPT_SYMB;
  extern const char* UNSAT_MODEL_MESSAGE;
  extern const char* WARNING_MESSAGE;
  extern const char* WELCOME_COPYRIGHTS;
  extern const char* WELCOME_OPTILAB;
  extern const char* WELCOME_RIGHTS;
  
}  // namespace View
  
}  // namespace Constants
}  // namespace MVC
