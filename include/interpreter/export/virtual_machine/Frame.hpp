//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 1/23/2018
//
// A Frame manages the bits of state, global and local
// namespaces, reference to the calling frame, and
// last bytecode instruction executed.
//

#pragma once

#include "InterpreterExportDefs.hpp"

#include "DataObject.hpp"
#include "InterpUtils.hpp"

#include <sparsepp/spp.h>
#include <vector>
#include <string>
#include <algorithm>
#include <memory>

// Forward declarations
namespace Interpreter {
  class Frame;
  
  using FramePtr  = Frame*;
  using FrameUPtr = std::unique_ptr<Frame>;
  using FrameSPtr = std::shared_ptr<Frame>;
}//end namespace Interpreter

namespace Interpreter {
  
  struct INTERPRETER_EXPORT_STRUCT Block {
    /**
     * Blocks are used for control flow, specifically for looping.
     * Blocks keep the data stack in an appropriate state when
     * looping operations are done.
     */
    enum BlockType : short {
        BT_EXCEPT_HANDLER = 0
      , BT_LOOP
      , BT_FINALLY
    };
    
    Block(BlockType aType, std::size_t aHandler, std::size_t aLevel)
    : btype(aType)
    , bhandler(aHandler)
    , blevel(aLevel)
    {
    }
    
    /// Type of this block
    BlockType   btype;
    /// Handler to the next instruction to jump to
    std::size_t bhandler;
    /// Level of the block stack this block is at
    std::size_t blevel;
  };
  
  class INTERPRETER_EXPORT_CLASS Frame {
  public:
    using PC_T = std::size_t;
    using NameMap = spp::sparse_hash_map<std::string, DataObject>;
    using DataStack = std::vector<DataObject>;
    using BlockStack = std::vector<Block>;
    using NameMapSPtr = std::shared_ptr<NameMap>;
    
  public:
    Frame(const CodeObject& aCodeObject, const NameMapSPtr& aGlbNames, const NameMapSPtr& aLocNames, FramePtr aPrevFrame);
    
    inline std::size_t getDataStackSize() const { return pDataStack.size(); }
    inline DataStack& getDataStack() { return pDataStack; }
    
    inline std::size_t getBlockStackSize() const { return pBlockStack.size(); }
    inline BlockStack& getBlockStack() { return pBlockStack; }
    
    void storeLocalName(const std::string& aName, const DataObject& aObj);
    void storeGlobalName(const std::string& aName, const DataObject& aObj);
    
    inline bool containsLocalName(const std::string& aName) const  { return pLocalNames->find(aName)  != pLocalNames->end();  }
    inline bool containsGlobalName(const std::string& aName) const { return pGlobalNames->find(aName) != pGlobalNames->end(); }
    
    inline DataObject getLocalName(const std::string& aName) const  { return pLocalNames->at(aName);  }
    inline DataObject getGlobalName(const std::string& aName) const { return pGlobalNames->at(aName); }
    
    void deleteLocalName(const std::string& aName);
    void deleteGlobalName(const std::string& aName);
    
    inline const NameMap& getLocalNames() const  { return *pLocalNames;  }
    inline const NameMap& getGlobalNames() const { return *pGlobalNames; }
    inline NameMapSPtr localNamesPtr()  const { return pLocalNames;  }
    inline NameMapSPtr globalNamesPtr() const { return pGlobalNames; }
    
    inline PC_T getPC() const { return pPC; }
    inline void advancePC(PC_T aPC=1) { pPC += aPC; }
    inline void resetPC(PC_T aPC) { pPC = aPC; }
    
    inline const CodeObject& getCodeObject() const
    {
      return pCode;
    }
    
    /// Returns the byte at position "aPC" in the code
    inline byte_T getByteFromCode(PC_T aPC)
    {
      return aPC < pCode.COCode.size() ? pCode.COCode[aPC] : 0xFF;
    }
    
  private:
    /// Index of the last instruction executed
    PC_T pPC;
    
    /// Pointer to previous frame
    FramePtr pPrevFrame;
    
    /// Local names
    NameMapSPtr pLocalNames;
    
    /// Global names
    NameMapSPtr pGlobalNames;
    
    /// Data stack
    DataStack pDataStack;
    
    /// Block stack
    BlockStack pBlockStack;
    
    /// Object Code
    CodeObject pCode;
  };
  
}// end namespace Interprete

