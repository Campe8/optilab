//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 1/23/2018
//
// Base object stored into frames.
//

#pragma once

#include "InterpreterExportDefs.hpp"

#include "DataObjectFcn.hpp"

#include <string>
#include <functional>
#include <typeinfo>
#include <iostream>
#include <cmath>
#include <vector>
#include <memory>
#include <cassert>
#include <type_traits>
#include <sstream>
#include <stdexcept>

// Forward declarations
namespace Interpreter {
  struct CodeObject;
  class BaseObject;
}// end namespace Interpreter

namespace Interpreter {
  
  struct Data_T {
    virtual ~Data_T() {}
    virtual Data_T* clone()  const = 0;
    virtual bool    op_not() const = 0;
    virtual Data_T* op_neg() const = 0;
    virtual Data_T* op_pow(Data_T*) const = 0;
    virtual Data_T* op_mul(Data_T*) const = 0;
    virtual Data_T* op_div(Data_T*) const = 0;
    virtual Data_T* op_mod(Data_T*) const = 0;
    virtual Data_T* op_add(Data_T*) const = 0;
    virtual Data_T* op_sub(Data_T*) const = 0;
    virtual Data_T* op_and(Data_T*) const = 0;
    virtual Data_T* op_or(Data_T*)  const = 0;
    virtual Data_T* op_xor(Data_T*) const = 0;
    
    // Comparison operators
    virtual Data_T* op_lt(Data_T*) const = 0;
    virtual Data_T* op_le(Data_T*) const = 0;
    virtual Data_T* op_eq(Data_T*) const = 0;
    virtual Data_T* op_ne(Data_T*) const = 0;
    virtual Data_T* op_gt(Data_T*) const = 0;
    virtual Data_T* op_ge(Data_T*) const = 0;
    
    // Type info
    virtual const std::type_info& type_id() const = 0;
  };
  
#pragma warning(disable:4244)
#pragma warning(disable:4804)
  
  namespace _private {
    template<class T>
    struct Data_T_Impl : public Data_T {
      
      /*
       * @note
       * Operators on strings are not supported.
       */
      
      Data_T_Impl(const T& aData)
      : data(aData)
      {
      }
      
      inline Data_T* clone() const override { return new Data_T_Impl<T>(data); }
      inline bool op_not() const override { return !data; }
      inline Data_T* op_neg() const override { return new Data_T_Impl<T>(-1 * data); }
      inline Data_T* op_pow(Data_T* aDT) const override {
        if(aDT && (aDT->type_id() == typeid(std::string))) throw std::logic_error("Operator not supported");
        return new Data_T_Impl<double>(std::pow(data, static_cast<Data_T_Impl<T>*>(aDT)->data));
      }
      inline Data_T* op_mul(Data_T* aDT) const override {
        if (aDT && (aDT->type_id() == typeid(std::string))) throw std::logic_error("Operator not supported");
        return new Data_T_Impl<T>(data * static_cast<Data_T_Impl<T>*>(aDT)->data);
      }
      inline Data_T* op_div(Data_T* aDT) const override {
        if (aDT && (aDT->type_id() == typeid(std::string))) throw std::logic_error("Operator not supported");
        return new Data_T_Impl<T>(data / static_cast<Data_T_Impl<T>*>(aDT)->data);
      }
      inline Data_T* op_mod(Data_T* aDT) const override {
        if (aDT && (aDT->type_id() == typeid(std::string))) throw std::logic_error("Operator not supported");
        return new Data_T_Impl<int>(static_cast<int>(data) %
                                    static_cast<Data_T_Impl<int>*>(aDT)->data);
      }
      inline Data_T* op_add(Data_T* aDT) const override {
        if (aDT && (aDT->type_id() == typeid(std::string))) throw std::logic_error("Operator not supported");
        return new Data_T_Impl<T>(data + static_cast<Data_T_Impl<T>*>(aDT)->data);
      }
      inline Data_T* op_sub(Data_T* aDT) const override {
        if (aDT && (aDT->type_id() == typeid(std::string))) throw std::logic_error("Operator not supported");
        return new Data_T_Impl<T>(data - static_cast<Data_T_Impl<T>*>(aDT)->data);
      }
      inline Data_T* op_and(Data_T* aDT) const override {
        if (aDT && (aDT->type_id() == typeid(std::string))) throw std::logic_error("Operator not supported");
        return new Data_T_Impl<std::size_t>(static_cast<std::size_t>(data) &
                                            static_cast<Data_T_Impl<std::size_t>*>(aDT)->data);
      }
      inline Data_T* op_or(Data_T* aDT) const override {
        if (aDT && (aDT->type_id() == typeid(std::string))) throw std::logic_error("Operator not supported");
        return new Data_T_Impl<std::size_t>(static_cast<std::size_t>(data) |
                                            static_cast<Data_T_Impl<std::size_t>*>(aDT)->data);
      }
      inline Data_T* op_xor(Data_T* aDT) const override {
        if (aDT && (aDT->type_id() == typeid(std::string))) throw std::logic_error("Operator not supported");
        return new Data_T_Impl<std::size_t>(static_cast<std::size_t>(data) ^
                                            static_cast<Data_T_Impl<std::size_t>*>(aDT)->data);
      }
      
      inline Data_T* op_lt(Data_T* aDT) const override {
        if (aDT && (aDT->type_id() == typeid(std::string))) throw std::logic_error("Operator not supported");
        return new Data_T_Impl<bool>(data < static_cast<Data_T_Impl<T>*>(aDT)->data);
      }
      inline Data_T* op_le(Data_T* aDT) const override {
        if (aDT && (aDT->type_id() == typeid(std::string))) throw std::logic_error("Operator not supported");
        return new Data_T_Impl<bool>(data <= static_cast<Data_T_Impl<T>*>(aDT)->data);
      }
      inline Data_T* op_eq(Data_T* aDT) const override {
        if (aDT && (aDT->type_id() == typeid(std::string))) throw std::logic_error("Operator not supported");
        return new Data_T_Impl<bool>(data == static_cast<Data_T_Impl<T>*>(aDT)->data);
      }
      inline Data_T* op_ne(Data_T* aDT) const override {
        if (aDT && (aDT->type_id() == typeid(std::string))) throw std::logic_error("Operator not supported");
        return new Data_T_Impl<bool>(data != static_cast<Data_T_Impl<T>*>(aDT)->data);
      }
      inline Data_T* op_gt(Data_T* aDT) const override {
        if (aDT && (aDT->type_id() == typeid(std::string))) throw std::logic_error("Operator not supported");
        return new Data_T_Impl<bool>(data > static_cast<Data_T_Impl<T>*>(aDT)->data);
      }
      inline Data_T* op_ge(Data_T* aDT) const override {
        if (aDT && (aDT->type_id() == typeid(std::string))) throw std::logic_error("Operator not supported");
        return new Data_T_Impl<bool>(data >= static_cast<Data_T_Impl<T>*>(aDT)->data);
      }
      
      inline const std::type_info& type_id() const override { return typeid(T); }
      
      T data;
    };
    
    template<>
    struct Data_T_Impl<std::string> : public Data_T {
      
      /*
       * @note
       * Only add - concatenation is supported for string data types.
       */
      
      Data_T_Impl(const std::string& aData)
      : data(aData)
      {
      }
      
      inline Data_T* clone() const override { return new Data_T_Impl<std::string>(data); }
      inline bool op_not() const override {
        throw std::logic_error("Operator not supported");
        return false;
      }
      inline Data_T* op_neg() const override {
        throw std::logic_error("Operator not supported");
        return nullptr;
      }
      inline Data_T* op_pow(Data_T* aDT) const override {
        throw std::logic_error("Operator not supported");
        return nullptr;
      }
      inline Data_T* op_mul(Data_T* aDT) const override {
        throw std::logic_error("Operator not supported");
        return nullptr;
      }
      inline Data_T* op_div(Data_T* aDT) const override {
        throw std::logic_error("Operator not supported");
        return nullptr;
      }
      inline Data_T* op_mod(Data_T* aDT) const override {
        throw std::logic_error("Operator not supported");
        return nullptr;
      }
      inline Data_T* op_add(Data_T* aDT) const override {
        if (aDT && (type_id() == aDT->type_id()))
        {
          return new Data_T_Impl<std::string>(data +
                                              static_cast<Data_T_Impl<std::string>*>(aDT)->data);
        }
        throw std::logic_error("Operator not supported");
        return nullptr;
      }
      inline Data_T* op_sub(Data_T* aDT) const override {
        throw std::logic_error("Operator not supported");
        return nullptr;
      }
      inline Data_T* op_and(Data_T* aDT) const override {
        throw std::logic_error("Operator not supported");
        return nullptr;
      }
      inline Data_T* op_or(Data_T* aDT) const override {
        throw std::logic_error("Operator not supported");
        return nullptr;
      }
      inline Data_T* op_xor(Data_T* aDT) const override {
        throw std::logic_error("Operator not supported");
        return nullptr;
      }
      
      inline Data_T* op_lt(Data_T* aDT) const override {
        throw std::logic_error("Operator not supported");
        return nullptr;;
      }
      inline Data_T* op_le(Data_T* aDT) const override {
        throw std::logic_error("Operator not supported");
        return nullptr;
      }
      inline Data_T* op_eq(Data_T* aDT) const override {
        throw std::logic_error("Operator not supported");
        return nullptr;
      }
      inline Data_T* op_ne(Data_T* aDT) const override {
        throw std::logic_error("Operator not supported");
        return nullptr;
      }
      inline Data_T* op_gt(Data_T* aDT) const override {
        throw std::logic_error("Operator not supported");
        return nullptr;
      }
      inline Data_T* op_ge(Data_T* aDT) const override {
        throw std::logic_error("Operator not supported");
        return nullptr;
      }
      
      inline const std::type_info& type_id() const override { return typeid(data); }
      
      std::string data;
    };
    
  }// end namespace _private
  
#pragma warning(default:4804)
#pragma warning(default:4244)
  
  /*
   * A DataObject represents the "basic" building block of all the objects in the interpreter.
   * It is a class to hold every type that can be constructed from primitive types.
   * The following represents the DataObject hierarchy:
   *
   *                                   +----------------+
   *                                   |   DataObject   |
   *                                   +----------------+
   *                                            ^
   *                                            |
   *          +-----------------------------------------------------------------+
   *          |                     |                     |                     |
   *  +----------------+    +----------------+    +----------------+    +----------------+
   *  | Primitive Obj. |    | Composite Obj. |    | Function Obj.  |    | Class Obj.     |
   *  +----------------+    +----------------+    +----------------+    +----------------+
   *          ^                     ^                                           ^
   *          |                     |                                           | has a
   *    DataObjectType:     Vector of DataObject                        +----------------+
   *    - Bool                                                          | BaseObject     |
   *    - Int                                                           +----------------+
   *    - Double                                                                ^
   *    - Size_t                                                                |
   *    - String                                                        Classes:
   *    - Void                                                          - List
   *                                                                    - Variable
   *                                                                    - Parameter
   *                                                                    - ...
   *
   */
  
  struct INTERPRETER_EXPORT_STRUCT DataObject {
    
    // Base data object type
    enum DataObjectType : char {
        DOT_BOOL = 0
      , DOT_INT
      , DOT_DOUBLE
      , DOT_SIZE_T
      , DOT_STRING
      , DOT_VOID
    };
    
    using ObjArray = std::vector<DataObject>;
    using DOIter = ObjArray::iterator;
    using DOCIter = ObjArray::const_iterator;
    
    DataObject();
    DataObject(const DataObject& aOther);
    DataObject(DataObject&& aOther);
    DataObject& operator=(const DataObject& aOther);
    DataObject& operator=(DataObject&& aOther);
    
    DataObject(Data_T* aData, DataObjectType aType=DOT_VOID)
    : pData(aData->clone()) { pDOType = aType; }
    DataObject(bool aData)
    : pData(new _private::Data_T_Impl<bool>(aData)) { pDOType = DOT_BOOL; }
    DataObject(int aData)
    : pData(new _private::Data_T_Impl<int>(aData)) { pDOType = DOT_INT; }
    DataObject(double aData)
    : pData(new _private::Data_T_Impl<double>(aData)) { pDOType = DOT_DOUBLE; }
    DataObject(std::size_t aData)
    : pData(new _private::Data_T_Impl<std::size_t>(aData)) { pDOType = DOT_SIZE_T; }
    DataObject(char const* aData)
    : pData(new _private::Data_T_Impl<std::string>(std::string(aData))) { pDOType = DOT_STRING; }
    
    /// Function call operator valid only if this is a function data object.
    /// The arguments are stored in the given composite object.
    /// Returns the list of results produced by calling the function.
    /// @note see the different types of functions (i.e., inline, object, etc.)
    /// for the semantic of the results
    std::vector<DataObject> operator()(const std::vector<DataObject>& aArgs);
    
    /// Data object operators
    Data_T* getData()    const { return pData.get(); }
    Data_T* operator->() const { return pData.get(); }
    DataObject& operator[](std::size_t aIdx);
    const DataObject& operator[](std::size_t aIdx) const;
    
    inline bool isComposite() const { return !pComposite.empty(); }
    void compose(const std::vector<DataObject>& aDS);
    inline std::size_t composeSize() const { return pComposite.size(); }
    inline std::vector<DataObject>& getCompose() { return pComposite; }
    inline const std::vector<DataObject>& getCompose() const { return pComposite; }
    
    // For-loop range iterator
    DOIter begin() { return pComposite.begin(); }
    DOIter end() { return pComposite.end(); }
    DOCIter begin() const { return pComposite.cbegin(); }
    DOCIter end() const { return pComposite.cend(); }
    
    // Returns the value of the data of type T hold by this DataObject.
    // @note throws if this is not a primitive object
    template<typename T>
    T getDataValue() const
    {
      assert(pData);
      return static_cast<_private::Data_T_Impl<T>*>(getData())->data;
    }
    
    // Returns the value of the data hold by this object casted to a type T.
    // @note throws if this is not a primitive object.
    // @note type must be:
    // - bool
    // - int
    // - double
    // - std::size_t
    // - std::string
    template<typename T>
    T castDataValue() const
    {
      assert(pData);
      
      switch (dataObjectType())
      {
        case DataObjectType::DOT_BOOL:
        {
          return static_cast<T>(getDataValue<bool>());
        }
        case DataObjectType::DOT_INT:
        {
          return static_cast<T>(getDataValue<int>());
        }
        case DataObjectType::DOT_DOUBLE:
        {
          return static_cast<T>(getDataValue<double>());
        }
        case DataObjectType::DOT_SIZE_T:
        {
          return static_cast<T>(getDataValue<std::size_t>());
        }
        default:
        {
          assert(dataObjectType() == DataObjectType::DOT_STRING);
          throw std::logic_error("Casting not allowed");
        }
          break;
      }
    }
    
    /// Returns true if this is a fully interpreted object, false otherwise
    inline bool isFullyInterpreted() const { return pFullyInterpreted; }
    
    /// Returns the base type of this data object
    inline DataObjectType dataObjectType() const { return pDOType; }
    
    /// Change data object type.
    /// This can be useful, for example, when creating a data object from Data_T.
    /// The client must be aware of the risk of changing the object type
    inline void changeDataObjectType(DataObjectType aType) { pDOType = aType; }
    
    /// Returns true if this is an empty object, false otherwise
    inline bool empty() const { return pData == nullptr; }
    
    /// Sets a data object function on this DataObject which makes this DataObject
    /// a function object itself. Therefore it must have been constructed with a name
    /// that represents the callback "aFcn".
    /// Moreover, this object will take ownership of the lifetime of "aFcn".
    /// @note a factory method would be better here
    inline void setFcnObject(DataObjectFcn* aFcn) { pFcn.reset(aFcn); }
    
    /// Returns true if this is a function data object, false otherwise
    inline bool isFcnObject() const { return pFcn != nullptr; }
    
    /// Returns the function object hold by this object
    inline DataObjectFcn& getFcn() { assert(pFcn); return *pFcn; }
    inline const DataObjectFcn& getFcn() const { assert(pFcn); return *pFcn; }
    inline DataObjectFcn* getFcnPtr() const { assert(pFcn); return pFcn.get(); }
    
    /// Sets fully interpreted flag
    inline void setFullyInterpretedTag(bool aFI) { pFullyInterpreted = aFI; }
    
    /// Sets an iterator on the given DataObject as follows:
    /// - if aDO is a composite object, the internal iterator points to the beginning
    ///   of "aDO"'s composite array
    void setIterator(const DataObject& aDO);
    
    /// Returns true if the iterator has another object to point to,
    /// false otherwise
    inline bool hasNext() const { return pIter < pIterArray.size(); }
    
    /// Returns the pointer the next object pointed by the internal iterator
    /// advancing the iterator.
    /// Returns nullptr if no such object exists
    inline const DataObject& next() { return pIterArray[pIter++]; }
    
    /// Returns true if this data object is a class object,
    /// i.e., it has a pointer to a BaseObject or an object
    /// which derives from a base object.
    /// Returns false otherwise
    inline bool isClassObject() const {
      return pClassObjInstance != nullptr;
    }
    
    /// Sets a class object instance
    inline void setClassObject(const std::shared_ptr<BaseObject>& aObj) { pClassObjInstance = aObj; }
    
    /// Returns the pointer to the class object encoded in this DataObject
    inline std::shared_ptr<BaseObject> getClassObject() const { return pClassObjInstance; }
    inline BaseObject& classObject() { return *pClassObjInstance; }
    
    /// Sets the pointer to a bytecode object
    inline void setByteCode(const std::shared_ptr<CodeObject>& aByteCode) { pObjCode = aByteCode; }
    
    /// Returns the pointer to the pointed bytecode
    inline std::shared_ptr<CodeObject> getByteCode() const { return pObjCode; }
    
  private:
    /// Flag determining whether this is a fully interpreted object or not
    bool pFullyInterpreted = true;
    
    /// Data object type
    DataObjectType pDOType = DOT_VOID;
    
    /// Iterator pointer
    std::size_t pIter = 0;
    
    /// Data hold by this object: it represents its primitive type
    std::unique_ptr<Data_T> pData = nullptr;
    
    /// Composite object: represents the fact that this object
    /// is composed by other objects, for example, an array object
    /// is composed by its elements
    ObjArray pComposite;
    
    /// Iterator object: used for loop iterators
    ObjArray pIterArray;
    
    /// Pointer to function if this is a function data object
    std::unique_ptr<DataObjectFcn> pFcn = nullptr;
    
    /// Pointer to the code object
    std::shared_ptr<CodeObject> pObjCode = nullptr;
    
    /// Pointer to the class (object) instance this DataObject encodes
    std::shared_ptr<BaseObject> pClassObjInstance = nullptr;
  };
  
  template<> std::string DataObject::castDataValue<std::string>() const;
  
  /// Returns true if the object has VOID type
  INTERPRETER_EXPORT_FUNCTION inline bool isVoidObjectType(const DataObject& aDO)
  {
    return aDO.dataObjectType() == DataObject::DataObjectType::DOT_VOID;
  }
  
  /// Returns true if the given DataObject is a composite type DataObject, false otherwise
  INTERPRETER_EXPORT_FUNCTION inline bool isCompositeType(const DataObject& aDO)
  {
    return aDO.isComposite();
  }
  
  /// Returns true if the given DataObject is a function type DataObject, false otherwise
  INTERPRETER_EXPORT_FUNCTION inline bool isFunctionType(const DataObject& aDO)
  {
    return aDO.isFcnObject();
  }
  
  /// Returns true if the given DataObject is a class object type DataObject, false otherwise
  INTERPRETER_EXPORT_FUNCTION inline bool isClassObjectType(const DataObject& aDO)
  {
    return aDO.isClassObject();
  }
  
  /// Returns true if the given DataObject is a primitive type DataObject, false otherwise
  INTERPRETER_EXPORT_FUNCTION inline bool isPrimitiveType(const DataObject& aDO)
  {
    auto primType = !isCompositeType(aDO) && !isFunctionType(aDO) && !isClassObjectType(aDO);
    return primType && !isVoidObjectType(aDO);
  }
  
  /// Returns the string value of the "any" object, i.e., returns the "_" string
  INTERPRETER_EXPORT_FUNCTION inline std::string getAnyObjectData()
  {
    static std::string anyObjectStr("_");
    return anyObjectStr;
  }//getAnyObject
  
  /// Returns a "mock" object use to represent "any" object.
  /// @note the object has a string type with value "_";
  INTERPRETER_EXPORT_FUNCTION inline DataObject getAnyObject()
  {
    static DataObject anyObject(getAnyObjectData().c_str());
    return anyObject;
  }//getAnyObject
  
  /// Returns true if the given DataObject is "any" object, false otherwise
  INTERPRETER_EXPORT_FUNCTION inline bool isAnyObjectType(const DataObject& aDO)
  {
    return isPrimitiveType(aDO) && (aDO.dataObjectType() == DataObject::DataObjectType::DOT_STRING)
    && (aDO.getDataValue<std::string>() == getAnyObjectData());
  }//getAnyObject
}// end namespace Interpreter
