//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 1/23/2018
//
// Class for code objects.
//

#pragma once


#include "InterpreterExportDefs.hpp"

#include "DataObject.hpp"

#include <sparsepp/spp.h>
#include <string>
#include <vector>

namespace Interpreter {

	using byte_T = unsigned char;
	using ByteArray = std::vector<byte_T>;

}// end namespace Interpreter

namespace Interpreter {
  
	struct INTERPRETER_EXPORT_STRUCT CodeObject {
		/// Constant symbols
		std::vector<DataObject> COConstants;
		/// Global symbols
		std::vector<DataObject> COGlbNames;
		/// Local symbols
		std::vector<DataObject> COLocNames;
		/// Byte code
		ByteArray COCode;
	};
  
  struct INTERPRETER_EXPORT_STRUCT CodeObjectImage : public CodeObject {
    using NameMap = spp::sparse_hash_map<std::string, DataObject>;
    
    /**
     * Image of a running code object.
     * Stores information about current environment
     */
    
    /// Constructor copies the base code object (constant and names) but NOT the byte code
    /// which must be set manually.
    /// Makes an internal copy (deep) of global and local names
    CodeObjectImage(const CodeObject& aBase, const NameMap& aGlobalNames, const NameMap& aLocalNames)
    {
      // Copy base code object
      COConstants = aBase.COConstants;
      COGlbNames  = aBase.COGlbNames;
      COLocNames  = aBase.COLocNames;
      
      /// Copy environment
      GlobalNames = aGlobalNames;
      LocalNames = aLocalNames;
    }
    
    /// Environment where this image is being created
    NameMap GlobalNames;
    NameMap LocalNames;
  };
  
}// end namespace Interpreter
