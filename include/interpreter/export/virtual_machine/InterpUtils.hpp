//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 1/23/2018
//
// Utility functions
//

#pragma once

#include "CodeObject.hpp"

namespace Interpreter { namespace Utils {

	/// Converts a byte into an integer
	INTERPRETER_EXPORT_FUNCTION int byteToInt(byte_T aByte);

	/// Converts and integer into an array of 4 bytes
	INTERPRETER_EXPORT_FUNCTION ByteArray intToByte(int aInt);

	/// Converts and integer into an array of 4 bytes and returns the least significant byte
	INTERPRETER_EXPORT_FUNCTION byte_T intToLSByte(int aInt);
  
  /// Add a composite object to another object
  INTERPRETER_EXPORT_FUNCTION DataObject opAddOnCompositeObjects(const DataObject& aData1,
                                                                 const DataObject& aData2);
  
  /// Add a list to another object.
  /// @note "aData1" must be a list object
  INTERPRETER_EXPORT_FUNCTION DataObject opAddOnListObjects(const DataObject& aData1,
                                                            const DataObject& aData2);
  
  /// Add a parameter to another object.
  /// @note "aData1" must be a parameter object
  INTERPRETER_EXPORT_FUNCTION DataObject opAddOnParameterObjects(const DataObject& aData1,
                                                                 const DataObject& aData2);
  
  /// Add two class objects together, at least one input must be a parameter object
  INTERPRETER_EXPORT_FUNCTION DataObject opAddOnClassObjects(const DataObject& aData1,
                                                             const DataObject& aData2);
  
}}// end namespace Interpreter
