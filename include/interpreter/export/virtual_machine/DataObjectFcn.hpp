//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 2/12/2018
//
// Base function object.
//

#pragma once

#include "InterpreterExportDefs.hpp"

#include <string>
#include <functional>

// Forward declarations
namespace Interpreter {
	class VirtualMachine;
}// end namespace Interpreter

namespace Interpreter {

	class INTERPRETER_EXPORT_CLASS DataObjectFcn {
  public:
    enum FcnType : char {
        FCN_INLINE = 0 // Inline functions, generally from MVC
      , FCN_INTRINSIC  // Functions implemented by VM code
      , FCN_OBJECT     // Functions stored as base object properties
    };
    
	public:
		virtual ~DataObjectFcn() = default;
    
		inline std::string getFcnName() const { return pFcnName; }

    /// Informs that this function returns a value
    virtual void setAsOutputFcn() { pHasOutput = true; };
    
    /// Returns true if this is an output function, false otherwise
    virtual bool hasOutput() const { return pHasOutput; };
    
    /// Returns the type of this function
    virtual FcnType getFcnType() const { return pFcnType; }
    
		/// Returns a clone of this instance
		virtual DataObjectFcn* clone() = 0;

	protected:
    DataObjectFcn(const std::string& aFcnName, FcnType aFcnType, VirtualMachine* aVM=nullptr);
    
		inline VirtualMachine* getVM() const { return pVirtualMachine; }

    // Clone base members from this instance of DataObjectFcn into "aFcn"
    void cloneInto(DataObjectFcn* aFcn);
    
	private:
		/// Function name
		std::string pFcnName;
    
    /// Flag for output functions
    bool pHasOutput;

    /// Type of this function
    FcnType pFcnType;
    
		/// Virtual machine instance used to run this function's code object
		VirtualMachine* pVirtualMachine;
	};

}// end namespace Interpreter
