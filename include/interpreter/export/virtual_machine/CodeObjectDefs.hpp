//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 1/23/2018
//

#pragma once

#include "OPCode.hpp"

#include <sparsepp/spp.h>

#include <string>
#include <vector>

namespace Interpreter {
  
  enum DO_TYPE : char {
      DOT_INT = 0
    , DOT_STR
    , DOT_DBL
    , DOT_BOOL
  };
  
  static int CO_OPHaveArguments = HAVE_ARGUMENT;
  
  /// Vector of bytecodes that have const arguments
  static std::vector<int> CO_OPConstArg = {
      LOAD_CONST
    , LOAD_CONST_BOOL
    , DUP_TOPX
  };
  
  /// Vector of bytecodes that have name arguments
  static std::vector<int> CO_OPGlbArg = {
      LOAD_NAME
    , LOAD_ATTR
    , STORE_ATTR
    , STORE_NAME
    , DELETE_NAME
    , STORE_GLOBAL
    , LOAD_GLOBAL
    , DELETE_GLOBAL
    , STORE_VAR
  };
  
  /// Vector of bytecodes that have local name arguments (e.g., variables)
  static std::vector<int> CO_OPLocArg = {
      LOAD_FAST
    , STORE_FAST
    , DELETE_FAST
  };
  
  /// Vector of bytecodes that have absolute jump target
  static std::vector<int> CO_OPAbsJmp = {
      JUMP_IF_FALSE_OR_POP
    , JUMP_IF_TRUE_OR_POP
    , JUMP_ABSOLUTE
    , POP_JUMP_IF_FALSE
    , POP_JUMP_IF_TRUE
    , CONTINUE_LOOP
  };
  
  /// Vector of bytecodes that have relative jump target
  static std::vector<int> CO_OPRelJmp = {
      FOR_ITER
    , JUMP_FORWARD
    , SETUP_LOOP
  };
  
  /// Vector of bytecodes that have comparison operators
  static std::vector<int> CO_OPHasComp = {
    COMPARE_OP
  };
  
  static spp::sparse_hash_map<int, std::string> CO_OPName = {
     { POP_TOP, "POP_TOP" }
    ,{ ROT_TWO, "ROT_TWO" }
    ,{ DUP_TOP, "DUP_TOP" }
    ,{ DUP_TOP_TWO, "DUP_TOP_TWO" }
    ,{ DUP_ROT_TWO, "DUP_ROT_TWO" }
    ,{ RETURN_VALUE, "RETURN_VALUE" }
    ,{ DUP_TOPX, "DUP_TOPX" }
    ,{ UNARY_NOT, "UNARY_NOT" }
    ,{ UNARY_NEGATIVE, "UNARY_NEGATIVE" }
    ,{ BINARY_POWER, "BINARY_POWER" }
    ,{ BINARY_MULTIPLY, "BINARY_MULTIPLY" }
    ,{ BINARY_DIVIDE, "BINARY_DIVIDE" }
    ,{ BINARY_MODULO, "BINARY_MODULO" }
    ,{ BINARY_ADD, "BINARY_ADD" }
    ,{ BINARY_SUBTRACT, "BINARY_SUBTRACT" }
    ,{ BINARY_AND, "BINARY_AND" }
    ,{ BINARY_OR, "BINARY_OR" }
    ,{ BINARY_XOR, "BINARY_XOR" }
    ,{ BINARY_SUBSCR, "BINARY_SUBSCR" }
    ,{ INPLACE_ADD, "INPLACE_ADD" }
    ,{ INPLACE_SUBTRACT, "INPLACE_SUBTRACT" }
    ,{ INPLACE_MULTIPLY, "INPLACE_MULTIPLY" }
    ,{ INPLACE_DIVIDE, "INPLACE_DIVIDE" }
    ,{ INPLACE_MODULO, "INPLACE_MODULO" }
    ,{ INPLACE_POWER, "INPLACE_POWER" }
    ,{ INPLACE_AND, "INPLACE_AND" }
    ,{ INPLACE_XOR, "INPLACE_XOR" }
    ,{ INPLACE_OR, "INPLACE_OR" }
    ,{ LOAD_CONST, "LOAD_CONST" }
    ,{ LOAD_NAME, "LOAD_NAME" }
    ,{ STORE_NAME, "STORE_NAME" }
    ,{ DELETE_NAME, "DELETE_NAME" }
    ,{ LOAD_FAST, "LOAD_FAST" }
    ,{ STORE_FAST, "STORE_FAST" }
    ,{ DELETE_FAST, "DELETE_FAST" }
    ,{ LOAD_GLOBAL, "LOAD_GLOBAL" }
    ,{ STORE_GLOBAL, "STORE_GLOBAL" }
    ,{ DELETE_GLOBAL, "STORE_GLOBAL" }
    ,{ COMPARE_OP, "COMPARE_OP" }
    ,{ BUILD_LIST, "BUILD_LIST" }
    ,{ JUMP_FORWARD, "JUMP_FORWARD" }
    ,{ JUMP_ABSOLUTE, "JUMP_ABSOLUTE" }
    ,{ POP_JUMP_IF_FALSE, "POP_JUMP_IF_FALSE" }
    ,{ POP_JUMP_IF_TRUE, "POP_JUMP_IF_TRUE" }
    ,{ JUMP_IF_FALSE_OR_POP, "JUMP_IF_FALSE_OR_POP" }
    ,{ JUMP_IF_TRUE_OR_POP, "JUMP_IF_TRUE_OR_POP" }
    ,{ SETUP_LOOP, "SETUP_LOOP" }
    ,{ GET_ITER, "GET_ITER" }
    ,{ FOR_ITER, "FOR_ITER" }
    ,{ BREAK_LOOP, "BREAK_LOOP" }
    ,{ CONTINUE_LOOP, "CONTINUE_LOOP" }
    ,{ POP_BLOCK, "POP_BLOCK" }
    ,{ CALL_FUNCTION, "CALL_FUNCTION" }
    ,{ BUILD_DOMAIN, "BUILD_DOMAIN" }
    ,{ STORE_VAR, "STORE_VAR" }
    ,{ ADD_MARK, "ADD_MARK" }
    ,{ PRINT_TOP, "PRINT_TOP" }
    ,{ LOAD_CONST_BOOL, "LOAD_CONST_BOOL" }
    ,{ RANGE_LIST, "RANGE_LIST" }
    ,{ SET_VAR_SPEC, "SET_VAR_SPEC" }
    ,{ UNARY_POSITIVE, "UNARY_POSITIVE" }
    ,{ STORE_SUBSCR, "STORE_SUBSCR" }
    ,{ LOAD_ATTR, "LOAD_ATTR" }
    ,{ STORE_ATTR, "STORE_ATTR" }
    ,{ BUILD_TUPLE, "BUILD_TUPLE" }
  };
  
}// end namespace Interpreter
