//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 2/13/2018
//
// Inline function object.
//

#pragma once

#include "DataObject.hpp"
#include "DataObjectFcn.hpp"

#include <utility>

namespace Interpreter {

	class INTERPRETER_EXPORT_CLASS InlineFcn : public DataObjectFcn {
	public:
		/// Callback function prototype for any callback of an inline function
		using FcnHandler = std::function<DataObject(const std::vector<DataObject>& aArgs)>;

	public:
		/// Constructor for inline function, takes the function name, the virtual machine
		/// instance to run  the object code on and the flag indicating whether or not this
		/// function requires the function object representing this function as an argument.
		/// If so this function becomes a "meta-function" object having itself as an object
		InlineFcn(const std::string& aFcnName, VirtualMachine* aVM, bool aNeedsFcnObject=false);
		
		/// Call the callback function on "aArgs".
    /// Returns a pair where:
    /// - first: value EXIT_SUCCESS on success or EXIT_FAILURE on failure
    /// - second: returned value, void if the handler doesn't return a value.
    /// @note if first is EXIT_FAILURE, second is always void
    std::pair<DataObject, DataObject> operator()(const std::vector<DataObject>& aArgs);
    
		/// Returns a clone of this instance
		DataObjectFcn* clone() override;

		/// Returns true if this inline function needs itself as an object input
		inline bool needsFcnObject() const { return pMetaFunction; }

		/// Sets the callback function representing this inline function
		inline void setCallbackFcn(FcnHandler aFcn) { pCallBack = aFcn; }
    
    /// Returns the callback function
    inline FcnHandler getCallbackFcn() const { return pCallBack; }
    
	private:
		/// Meta-function flag
		bool pMetaFunction;
    
		/// Callback function
		FcnHandler pCallBack;
	};

}// end namespace Interpreter
