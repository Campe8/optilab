//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 1/23/2018
//
// Virtual machine implementing the
// interpreter of the OptiLab language.
// Manages the call stack of frames and contains
// a mapping of instructions to operations.
//

#pragma once

#include "InterpreterExportDefs.hpp"

#include "InterpreterDefs.hpp"
#include "Frame.hpp"

#include <functional>
#include <utility>
#include <vector>

namespace Interpreter {
  
  class INTERPRETER_EXPORT_CLASS VirtualMachine {
  public:
    /// Environment for the virtual machine:
    /// first: global names
    /// second: local names
    using VMEnv = std::pair<Frame::NameMapSPtr, Frame::NameMapSPtr>;
    using FcnHandler = std::function<DataObject(const std::vector<DataObject>&)>;
    
  public:
    VirtualMachine();
    
    /// Entry point to execute the code using the virtual machine.
    /// It takes the code object to interpret and a virtual machine environment.
    /// Returns the interpreted environment.
    /// If "aReinterpretationCode" it adds additional checks for re-interpreted bytecode
    VMEnv runCode(const CodeObject& aCode, const VMEnv& aEnv = { nullptr, nullptr }, bool aReinterpretationCode = false);
    
    /// Clears current state, i.e., clears the environment of the current frame.
    /// @note only the environment of the current frame will be cleared
    void clearState();
    
    /// Registers an inline callback function.
    /// The virtual machine will call the callback whenever a function call statement
    /// is going to be interpreted and the name of the function in the statement
    /// matches one of the registered callbacks.
    /// The parameters of the methods are the following:
    /// 1) aFcnName: name of the callback function;
    /// 2) aFcn: callback function handler;
    /// 3) aFcnObjectAsInput: if true, when the callback is invoked the virtual machine will use the data object
    ///    representing the function "aFcn" as input parameter, otherwise it will use the arguments from the stack.
    ///    Note that the data object representing the function contains the parameters as a composite object
    /// 4) aHasOutput: indicates whether or not the function handler returns a value
    void registerInlineCallbackFcn(const std::string& aFcnName, FcnHandler aFcn, bool aFcnObjectAsInput, bool aHasOutput);
    
    /// Returns true if a callback function with given name is registered, false otheriwise
    bool isInlineCallbackRegistered(const std::string& aFcnName) const;
    
    /// Returns the handler of a registered callback function.
    /// @note throws if the function with given name is not registered
    FcnHandler getInlineCallbackFcn(const std::string& aFcnName);
    
    /// Returns true if the last execution of "runCode" failed for some reasons,
    /// and has thrown an exception.
    /// Returns false otherwise
    inline bool hasThrownException() const { return pControllerExceptionType != InterpreterException::ExceptionType::ET_UNSPEC; }
    inline InterpreterException::ExceptionType getExceptionType() const { return pControllerExceptionType; }
    inline std::string getExceptionValue() const { return pControllerExceptionValue; }
    
    //== Model context specific-functions ==//
    
    /// Registers the callback invoked when an object is declared and must be stored
    /// as a local variable.
    /// For example, declarations like "x:a..b" will create an object "x" which will
    /// be given to the registered callback function
    void registerCallbackForModelCtxDeclarations(std::function<void(const std::string&, const DataObject&)> aCallback);
    
    /// Registers the callback invoked when an object from the stack needs to be reported to the caller.
    /// For example, declarations like "x;" will print the context object "x" if any
    void registerCallbackForObjectReporting(std::function<void(const DataObject&, bool)> aCallback);
    
  private:
    /// A bytecode is represented by the pair
    /// <ByteFcn, DataObject_argument>
    /// If ByteFcn is an empty string, the bytecode is not a legal bytecode
    using ByteCodeInstr = std::pair<std::string, DataObject>;
    
    // Result of executing a byteCode method
    enum ExeRes : int {
      ER_NONE = 0
      , ER_BREAK
      , ER_CONTINUE
      , ER_EXCEPTION
      , ER_CONTROLLED_EXCEPTION
    };
    
    using Fcn = std::function<void(void)>;
    using ByteFcn = std::function<void(const DataObject&)>;
    using UnaryFcn = std::function<DataObject(const DataObject&)>;
    using BinaryFcn = std::function<DataObject(const DataObject&, const DataObject&)>;
    using CallbackRegister = spp::sparse_hash_map<std::string, DataObject>;
    
  private:
    /// Current frame
    FramePtr pFrame;
    
    /// Value to return
    DataObject pReturnValue;
    
    /// Result of the execution of the current bytecode
    ExeRes pExeResult;
    
    /// Controlled exception handling: type
    InterpreterException::ExceptionType pControllerExceptionType;
    
    /// Flag indicating if the virtual machine is running in reinterpreting mode
    bool pReinterpretationModeOn;
    
    /// Flag indicating whether or not to skip next byte code instruction
    bool pSkipNextInstruction;
    
    /// Flag indicating that a callback function set the top of the stack to be printed
    bool pCallbackPrint;
    
    /// Controlled exception handling: value
    std::string pControllerExceptionValue;
    
    /// Stack of frames
    std::vector<FrameSPtr> pFrameStack;
    
    /// Stack of PC markers
    std::vector<Frame::PC_T> pPCStack;
    
    /// Register of unary functions
    std::vector<Fcn> pFcnReg;
    
    /// Register of unary functions
    std::vector<UnaryFcn> pUnaryFcnReg;
    
    /// Register of binary functions
    std::vector<BinaryFcn> pBinaryFcnReg;
    
    /// Register of byte functions
    std::vector<ByteFcn> pByteFcnReg;
    
    /// Register of callback functions mapping function names to their
    /// DataObject functions
    CallbackRegister pCallbackReg;
    
    /// Sets the reinterpretation mode flag to the given Boolean value
    /// and returns the value of the flag before the change
    bool setReinterpetationMode(bool aReinterpret);
    
    /// Returns true if the virtual machine runs in reinterpretation mode, false otherwise
    inline bool isReinterpretationModeOn() const { return pReinterpretationModeOn; }
    
    //== Model specific callbacks ==//
    
    /// Context object declaration callback
    std::function<void(const std::string&, const DataObject&)> pModelDeclCallback;
    
    /// Context object printing
    std::function<void(const DataObject&, bool)> pModelObjReportCallback;
    
    /// Mapping operators codes to indices in their correspondent indices in their registers
    static std::vector<std::pair<std::string, std::size_t>> OpFcnIdxMap;
    
    /// Mapping byte function codes to indices in their correspondent indices in their register
    static std::vector<std::string> ByteFcnIdxMap;
    
    /// Utility function returns current frame
    inline Frame& getFrame() { assert(pFrame); return *pFrame; }
    
    /// Returns the key index in the array of functions for
    /// retrieving the operator "aOP"
    std::size_t getFunctionEntryKey(const std::string& aOP);
    
    /// Returns a new instance of a frame
    FramePtr makeFrame(const CodeObject& aCodeObject,
                       const Frame::NameMapSPtr& aGlbNames = Frame::NameMapSPtr(nullptr),
                       const Frame::NameMapSPtr& aLocNames = Frame::NameMapSPtr(nullptr));
    
    /// Runs the given frame
    DataObject runFrame(const FrameSPtr& aFrame);
    
    /// Pushes the given frame into the frame stack.
    /// @note it creates a smart pointer wrapper around "aFrame"
    /// and updates the pointer to the current frame
    void pushFrame(const FrameSPtr& aFrame);
    
    /// Pops the frame stack updating the pointer to the current frame
    void popFrame();
    
    /// Manages a frame block stack on thrown exception
    ExeRes manageBlockStack(ExeRes aException);
    
    /// Block unwinding
    void unwindBlock(Block& aBlock);
    
    /// Adds a "mark" to the PC stack, i.e., adds the current PC to the PC stack
    /// therefore "marking" it.
    /// @note adds an the given offset to the current PC
    inline void addMarkToPCStack(int aOffset = 0);
    
    /// Pops and returns the marked PC at the top of the stack
    inline Frame::PC_T popPCStack() { auto pc = pPCStack.back();  pPCStack.pop_back(); return pc; }
    
    /// Returns a copy of the CodeObject in [aBegin, aEnd)
    CodeObject* getMarkedCodeObject(Frame::PC_T aBegin, Frame::PC_T aEnd);
    
    /// Parses 3 bytes of bytecode into an instruction
    /// and arguments if any and returns the correspondent
    /// ByteCode instruction
    ByteCodeInstr parseBytes();
    
    /// Dispatches by bytename to the corresponded method
    ExeRes dispatch(const std::string& aByteName, const DataObject& aArg);
    
    /// Set exception information for controlled exception
    void setExceptionInfo(InterpreterException& aIE);
    
    /// Reset exception information for controller exception
    void resetExceptionInfo();
    
    /// Helper functions to work on frames ///
    
    /// Moves the PC of the current frame to "aJump" to allows jumps
    /// in the bytecode
    inline void jump(Frame::PC_T aJump) { assert(pFrame); pFrame->resetPC(aJump); }
    
    /// Pushes a block on the block stack of the current frame.
    /// @note if "aHandler" is -1, uses the data stack as handler
    void pushBlock(Block::BlockType aType, Frame::PC_T aHandler = 0, std::size_t aLevel = 0);
    
    /// Pops the top block from the current frame's block stack
    inline void popBlock() { assert(pFrame); getFrame().getBlockStack().pop_back(); }
    
    /// Returns the object at the top of the stack on the current frame
    DataObject& top();
    std::vector<DataObject> topn(std::size_t aNum);
    
    /// Pops and returns the object at the top of the stack on the current frame
    DataObject pop();
    
    /// Pushes data objects on the stack on the current frame
    void push(const DataObject& aDO);
    void push(const std::vector<DataObject>& aDS);
    
    /// Pops n values from the data stack of the current frame and returns
    /// the correspondent list.
    /// @note The deepest value first
    std::vector<DataObject> popn(std::size_t aNum);
    
    /// Registers all the functions used by this VM
    void registerFcns();
    
    /// Calls the unary operator "aOP"
    void unaryOperator(const std::string& aOP);
    
    /// Calls the binary operator "aOP"
    void binaryOperator(const std::string& aOP);
    
    /// Calls the inplace operator "aOP"
    void inplaceOperator(const std::string& aOP);
    
    /// Returns the function with given byte code
    ByteFcn& getByteFcn(const std::string& aByteFcnCode);
    
    /// Stores into the frame's local name the given data object
    /// mapped to the given name
    void storeName(const std::string& aName, const DataObject& aObj);
    
    /// Stores into "aObj" the property "aPropValue" with key "aPropKey"
    void storeProperty(const DataObject& aObj, const std::string& aPropKey,
                       const DataObject& aPropValue);
    
    /// Maps the name of the class object into the model context
    void mapNameClassObjectIntoModelContext(const std::string& aName, DataObject& aObj);
    
    /// Calls an internal function.
    /// Following python's interpreter,
    /// low byte of "aDO" is the number of positional parameters,
    /// which must be on the stack.
    /// If the function to call is an inline function and it is not registered,
    /// throws a controlled exception of type ET_FCN_UNDEF with the
    /// name of the function as value
    void callFunction(const DataObject& aDO);
    
    /// Returns the byte code instruction that will be executed
    /// after the instruction currently addressed by the pc
    std::string getNextByteCodeInstruction() const;
    
    /// Skip execution of the instruction pointed next by the pc
    inline void skipNextInstruction() { pSkipNextInstruction = true; }
    
    /// Set the flag indicating that a callback function set the top to print
    inline void setCallbackPrintTop() { pCallbackPrint = true; }
    
    //== Operators ==//
    
    DataObject opNot(const DataObject&);
    DataObject opNeg(const DataObject&);
    DataObject opPos(const DataObject&);
    DataObject opPow(const DataObject&, const DataObject&);
    DataObject opMul(const DataObject&, const DataObject&);
    DataObject opDiv(const DataObject&, const DataObject&);
    DataObject opMod(const DataObject&, const DataObject&);
    DataObject opAdd(const DataObject&, const DataObject&);
    DataObject opSub(const DataObject&, const DataObject&);
    DataObject opAnd(const DataObject&, const DataObject&);
    DataObject opOr(const DataObject&, const DataObject&);
    DataObject opXor(const DataObject&, const DataObject&);
    DataObject opLT(const DataObject&, const DataObject&);
    DataObject opLE(const DataObject&, const DataObject&);
    DataObject opEQ(const DataObject&, const DataObject&);
    DataObject opNE(const DataObject&, const DataObject&);
    DataObject opGT(const DataObject&, const DataObject&);
    DataObject opGE(const DataObject&, const DataObject&);
    DataObject opSubScr(const DataObject&, const DataObject&);
    void byte_fcn_COMPARE_OP(const DataObject&);
    
    //== Stack Manipulation ==//
    void byte_fcn_LOAD_CONST(const DataObject&);
    void byte_fcn_LOAD_CONST_BOOL(const DataObject&);
    void byte_fcn_POP_TOP(const DataObject&);
    void byte_fcn_DUP_TOP(const DataObject&);
    void byte_fcn_DUP_TOPX(const DataObject&);
    void byte_fcn_DUP_TOP_TWO(const DataObject&);
    void byte_fcn_ROT_TWO(const DataObject&);
    
    //== Names ==//
    void byte_fcn_LOAD_NAME(const DataObject&);
    void byte_fcn_STORE_NAME(const DataObject&);
    void byte_fcn_DELETE_NAME(const DataObject&);
    void byte_fcn_LOAD_FAST(const DataObject&);
    void byte_fcn_STORE_FAST(const DataObject&);
    void byte_fcn_DELETE_FAST(const DataObject&);
    void byte_fcn_LOAD_GLOBAL(const DataObject&);
    void byte_fcn_STORE_GLOBAL(const DataObject&);
    void byte_fcn_DELETE_GLOBAL(const DataObject&);
    void byte_fcn_LOAD_ATTR(const DataObject&);
    void byte_fcn_STORE_ATTR(const DataObject&);
    void byte_fcn_STORE_SUBSCR(const DataObject&);
    
    //== Building ==//
    void byte_fcn_BUILD_LIST(const DataObject&);
    void byte_fcn_BUILD_TUPLE(const DataObject&);
    
    //== Jumps ==//
    void byte_fcn_JUMP_FORWARD(const DataObject&);
    void byte_fcn_JUMP_ABSOLUTE(const DataObject&);
    void byte_fcn_POP_JUMP_IF_TRUE(const DataObject&);
    void byte_fcn_POP_JUMP_IF_FALSE(const DataObject&);
    void byte_fcn_JUMP_IF_TRUE_OR_POP(const DataObject&);
    void byte_fcn_JUMP_IF_FALSE_OR_POP(const DataObject&);
    
    //== Blocks ==//
    void byte_fcn_SETUP_LOOP(const DataObject&);
    void byte_fcn_GET_ITER(const DataObject&);
    void byte_fcn_FOR_ITER(const DataObject&);
    void byte_fcn_BREAK_LOOP(const DataObject&);
    void byte_fcn_CONTINUE_LOOP(const DataObject&);
    void byte_fcn_POP_BLOCK(const DataObject&);
    
    //== Functions ==//
    // byte_fcn_MAKE_FUNCTION
    void byte_fcn_CALL_FUNCTION(const DataObject&);
    // Returns whatever is on the stack or 0 if the stack is empty
    void byte_fcn_RETURN_VALUE(const DataObject&);
    
    //== Model Construction ==//
    /// The deepest values are the domain values
    /// while the top of the stack is the domain type.
    void byte_fcn_BUILD_DOMAIN(const DataObject&);
    
    /// Sets variable specifications, for example,
    /// declares that the variable is an optimization variable
    void byte_fcn_SET_VAR_SPEC(const DataObject&);
    
    void byte_fcn_STORE_VAR(const DataObject&);
    void byte_fcn_ADD_MARK(const DataObject&);
    
    /// Prints the top of the stack
    void byte_fcn_PRINT_TOP(const DataObject&);
    
    /// Pops 2 objects from the stack a, b and
    /// creates the list [a, b].
    /// Pushes the list on the stack
    void byte_fcn_RANGE_LIST(const DataObject&);
  };
  
} // end namespace Interpreter

