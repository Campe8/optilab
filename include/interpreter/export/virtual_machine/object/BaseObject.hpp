//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 4/4/2018
//
// Template for the "BaseObject" the object
// all other classes derive from.
// An object is "basically" a map of
// <key, value>
// pairs where keys are strings naming the
// property of the object and values
// are property values encoded as DataObjects.
//

#pragma once

#include "InterpreterExportDefs.hpp"

#include "DataObject.hpp"

#include <sparsepp/spp.h>

#include <string>
#include <vector>

// Forward declarations
namespace Interpreter {
  class ObjectFcn;
  using BaseObjectSPtr = std::shared_ptr<BaseObject>;
}// end namespace Interpreter

namespace Interpreter {
  
  class INTERPRETER_EXPORT_CLASS BaseObject {
  public:
    using PropertyKey = std::string;
    using PropertyValue = DataObject;
    
  public:
    /// Creates a new instance of this object
    friend BaseObject* newObject();
    
    /// Destructor will call the destructor of the parent objects recursively
    virtual ~BaseObject();
    
    /// Returns a clone of this object.
    /// @note this is a DEEP copy of the object, meaning that all
    /// parent objects will be copied.
    /// @note the caller takes ownership of the cloned object
    virtual BaseObject* clone();
    
    /// This method allows the caller to set a inheritance property between
    /// this object and the given object:
    ///        aParent
    ///           ^
    ///           |
    ///         this
    /// In particular a clone of the parent object is generated
    /// and this parent will point to the cloned parent.
    /// Cloning the parent will allow this object to inherit all
    /// parent's properties but not share any instance
    void deriveFrom(BaseObject* aParent);
    
    /// Returns true if this object is an instance of (i.e., has the
    /// same type or derives from) the given input object "aObj"
    bool isInstanceOf(BaseObject* aObj);
    
    /// Returns true if this object has the given property, false otherwise.
    /// @note this method does not distinguish between "get" and "set" properties
    bool hasProperty(const PropertyKey& aKey) const;
    
    /// Removes the property with given key from this object, if the property is present
    void removeProperty(const PropertyKey& aKey);
    
    /// Returns the value of the given property as a MODIFIABLE value.
    /// Use with caution !!!
    /// @note this method DOES NOT distinguish between "get" and "set" properties.
    /// @note throws if the property is not present
    PropertyValue& lookupProperty(const PropertyKey& aKey);
    
    /// Returns the value of the given property.
    /// The get property method allows the client to differentiate between
    /// a get and a set method using the same value as PropertyKey.
    /// In particular, given a PropertyKey "propertyName",
    /// this method first looks if there is any function registered as a get property.
    /// If such function is found, it operates on the correspondent value.
    /// If such property is not found, it looks for
    /// "PropertyKey" and operates on the corresponding value.
    /// When the propertyValue is retrieved, if it is a function object,
    /// then the callback is applied to this object and getProperty returns its output.
    /// Else, the propertyValue is returned.
    /// @note the "get" prefix can be retrieved by calling
    /// "getPrefixPropertyForRegisteringGetMethod()"
    /// @note throws if the property is not present
    PropertyValue getProperty(const PropertyKey& aKey) const;
    
    /// Sets the value of the given property.
    /// The set property method allows the client to differentiate between
    /// a get and a set method using the same value for the PropertyKey.
    /// In particular, given a PropertyKey "propertyName",
    /// this method first looks if there is any function registered as a set property.
    /// If such function is found, it operates on the correspondent value.
    /// If such property is not found, it looks for
    /// "PropertyKey" and operates on the corresponding value.
    /// When the propertyValue is retrieved, if it is a function object,
    /// then the callback is applied to this object and the given input value.
    /// Else, the propertyValue set to the given input value.
    /// @note the "set" prefix can be retrieved by calling
    /// "getPrefixPropertyForRegisteringSetMethod()"
    /// @note throws if the property is not present
    void setProperty(const PropertyKey& aKey, const PropertyValue& aValue);
    
    /// Sets the visibility of the given property w.r.t. the given flag
    void setPropertyVisibility(const PropertyKey& aKey, bool aIsVisible);
    
    /// Returns true if the property is a visible property, false otherwise
    bool isPropertyVisibile(const PropertyKey& aKey) const;
    
    /// Sets this property to be costant w.r.t. the given flag
    void setConstProperty(const PropertyKey& aKey, bool aIsConst);
    
    /// Returns true if the property is a const property, false otherwise
    bool isConstProperty(const PropertyKey& aKey) const;
    
    /// Adds a new property with given key and empty value
    /// @note it will replace any existing value if the property is already present.
    /// @note the key must be non-empty
    void addProperty(const PropertyKey& aKey);
    
    /// Adds a new property with given key and given value.
    /// @note it will replace any existing value if the property is already present.
    /// @note the key must be non-empty
    void addProperty(const PropertyKey& aKey, const PropertyValue& aVal);
    
    /// Specifies a get function to be used when querying for the property with given key
    void attachFcnToGetProperty(const PropertyKey& aKey, ObjectFcn* aFcn);
    
    /// Specifies a set function to be used when querying for the property with given key
    void attachFcnToSetProperty(const PropertyKey& aKey, ObjectFcn* aFcn);
    
    /// Returns the list of all the keys in the object
    std::vector<PropertyKey> getKeys();
    
    /// Returns the list of all visible keys
    std::vector<PropertyKey> getVisibleKeys();
    
    /// Serializes this object into a string
    virtual std::string toString();
    
  protected:
    BaseObject() = default;
    
  private:
    using PropertyMap = spp::sparse_hash_map<PropertyKey, PropertyValue>;
    
  private:
    /// Pointer to the parent class this
    /// object derives from
    BaseObject * pParent = nullptr;
    
    /// Map of the properties of the object
    PropertyMap pPropertyMap;
    
    /// Vector of non-visible property keys
    std::vector<PropertyKey> pHiddenProperties;
    
    /// Vector of constant properties
    std::vector<PropertyKey> pConstProperties;
    
    /// Returns the prefix for properties that must be handled differently
    /// w.r.t. the get method and the set method.
    /// @note this prefix must be used only when registering the property
    /// if the property needs to be different than the one retrieved with
    /// the set method.
    /// After registering it, it will be safe to use the property key
    /// without any prefix
    static PropertyKey getPrefixPropertyForRegisteringGetMethod();
    
    /// Returns the prefix for properties that must be handled differently
    /// w.r.t. the set method and the get method.
    /// @note this prefix must be used only when registering the property
    /// if the property needs to be different than the one retrieved with
    /// the get method.
    /// After registering it, it will be safe to use the property key
    /// without any prefix
    static PropertyKey getPrefixPropertyForRegisteringSetMethod();
    
    /// Returns the full property name for get method (i.e., including prefix)
    static PropertyKey getFullPropertyNameForRegisteringGetMethod(const std::string& aProp);
    
    /// Returns the full property name for set method (i.e., including prefix)
    static PropertyKey getFullPropertyNameForRegisteringSetMethod(const std::string& aProp);
  };
  
  /// Returns an instance of a BaseObject
  BaseObject* newObject();
  
}// end namespace Interpreter

