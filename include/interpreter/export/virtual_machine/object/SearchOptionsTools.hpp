//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 10/14/2018
//
// Tools for creating and manipulating objects
// that derive from BaseObject and are specific
// for search options.
//

#pragma once

#include "ObjectTools.hpp"

#include <string>
#include <vector>

namespace Interpreter { namespace SearchOptionsTools {
  
  /// Returns the name of the base class
  INTERPRETER_EXPORT_FUNCTION std::string getSearchOptionsTypeName();
  
  /// Returns the name of the CBLS class
  INTERPRETER_EXPORT_FUNCTION std::string getCBLSSearchOptionsTypeName();
  
  /// Returns the name of the DFS class
  INTERPRETER_EXPORT_FUNCTION std::string getDFSSearchOptionsTypeName();
  
  /// Returns the name of the Genetic class
  INTERPRETER_EXPORT_FUNCTION std::string getGeneticSearchOptionsTypeName();
  
  /// Returns the name of the property class type
  INTERPRETER_EXPORT_FUNCTION BaseObject::PropertyKey getClassTypePropName();
  
  /// Returns the name of the string property strategy
  INTERPRETER_EXPORT_FUNCTION BaseObject::PropertyKey getStrategyPropName();
  
  /// Returns the name of the property timeout
  INTERPRETER_EXPORT_FUNCTION BaseObject::PropertyKey getTimeoutPropName();
  
  /****************************************************
   *                                                  *
   *      Helper functions for SEARCH OPTIONS objects *
   * @note "all" these functions assert if the given  *
   * object is not a variable object.                 *
   ****************************************************/
  
  /// Returns true if the given object is a SearchOptions object.
  /// Returns false otherwise
  INTERPRETER_EXPORT_FUNCTION bool isSearchOptionsObject(BaseObject* aObj);
  
  /// Returns the name of the type of the given SearchOptions object
  INTERPRETER_EXPORT_FUNCTION char getClassType(BaseObject* aObj);
  
  /// Returns a new instance of a DFS SearchOptions object
  INTERPRETER_EXPORT_FUNCTION BaseObject* getDFSSearchOptionsObject();
  
  /// Returns a new instance of a Genetic SearchOptions object
  INTERPRETER_EXPORT_FUNCTION BaseObject* getGeneticSearchOptionsObject();
  
}}// end namespace SearchOptionsTools/Interpreter
