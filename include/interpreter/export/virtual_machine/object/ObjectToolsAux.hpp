//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 12/13/2018
//
// Tools for creating and manipulating objects
// that derive from BaseObject and are any utility object.
//

#pragma once

#include "ObjectTools.hpp"

#include <string>
#include <vector>

namespace Interpreter { namespace ObjectTools {
  
  /****************************************************
   * MetricsOptions                                   *
   ****************************************************/
  
  /// Returns the name of the time metrics option parameter
  INTERPRETER_EXPORT_FUNCTION std::string getMetricsTimeOptionsName();
  
  /// Returns the name of the search metrics option parameter
  INTERPRETER_EXPORT_FUNCTION std::string getMetricsSearchOptionsName();
  
  /// Returns a new instance of a metrics options object.
  /// @note by default sets time options as enabled and search options as disabled
  INTERPRETER_EXPORT_FUNCTION BaseObject* getMetricsOptionsObject(bool aTimeOptions=true,
                                                                  bool aSearchOptions=false);
  
}}// end namespace ObjectTools/Interpreter
