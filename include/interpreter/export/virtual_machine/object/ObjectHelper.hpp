//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 4/11/2018
//

#pragma once

#include <boost/core/noncopyable.hpp>

#include "BaseObject.hpp"
#include "DataObject.hpp"

#include <string>
#include <vector>
#include <utility>
#include <memory>

namespace Interpreter { namespace ObjectTools {

	class INTERPRETER_EXPORT_CLASS ObjectHelper : private boost::noncopyable {
	public:
		struct PropertySpec {
			// Property name
			std::string name = "";
			// Is hidden flag
      bool hidden = true;
			// Is constant flag
			bool constant = true;
		};

		using PropertySpecList = std::vector<PropertySpec>;

	public:
		/**
	 	 * Helper class for object declaration.
		 * @note singleton class.
		 * @note this is a "read-only" class.
		 */
		static ObjectHelper& getInstance();

		/// Returns true if the object class with given name is registered,
		/// returns false otherwise
		bool isObjectClassRegistered(const std::string& aClassName) const;

		/// Returns the class ID of the object with given class name.
		/// @note throws if such name is not registered
		std::size_t getIDForClassName(const std::string& aClassName) const;

		/// Returns the class name of the object with given ID.
		/// Returns an empty string if no such object is found
		std::string getClassNameForObjectID(std::size_t aID) const;

		/// Returns the list of properties for a given class.
		/// @note throws if such name is not registered
		const PropertySpecList& getPropertyListForObjectClass(const std::string& aClassName) const;

		/// Returns the list of properties for a given class ID.
		/// @note throws if such name is not registered
		const PropertySpecList& getPropertyListForObjectClass(std::size_t aID) const;

		/// Creates a new BaseObject instance with the properties specified by the PropertySpec of the given class.
		/// If "aBaseClass" is not null, it derives the object from the given base class.
		/// @note throws if the class name does not correspond to any registered class
		BaseObject* createObjectInstance(const std::string& aClassName, BaseObject* aBaseClass=nullptr) const;

	private:
		ObjectHelper();

		struct ObjectSpec {
			// Object name
			std::string name = "";
			// Object ID
			std::size_t id;
			// Vector of properties
			PropertySpecList properties;
		};

		using ObjectSpectSPtr = std::shared_ptr<ObjectSpec>;
		using ObjectSpecPair = std::pair<std::string, ObjectSpectSPtr>;

	private:
		std::vector<ObjectSpecPair> pObjRegister;

		/// Loads the objects specifications from the
		/// XML file descriptions
		void loadObjectSpecifications();
    
		/// Parse the object specification and adds it to the object register
		void parseObjectSpecification(std::vector<char>& aSpec);
	};
  
  /// Initializes static instances in the helper functions.
  /// This is done to optimize efficiency later when the CLI is running
  void initStaticObjectInstances();
  
  /****************************************************
   *                                                  *
   *      Helper functions for SCALAR objects         *
   * @note "all" these functions assert if the given  *
   * object is not a scalar object.                   *
   ****************************************************/
  INTERPRETER_EXPORT_FUNCTION std::string getScalarClassName();
  INTERPRETER_EXPORT_FUNCTION bool isScalarObject(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION DataObject& getScalarValue(BaseObject* aObj);
  
  /****************************************************
   *                                                  *
   *      Helper functions for LIST objects           *
   * @note "all" these functions assert if the given  *
   * object is not a list object.                     *
   ****************************************************/
  
  INTERPRETER_EXPORT_FUNCTION std::string getListClassName();
  INTERPRETER_EXPORT_FUNCTION bool isListObject(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION int getListSize(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION void setListSize(BaseObject* aObj, int aSize);
  INTERPRETER_EXPORT_FUNCTION DataObject& getListValues(BaseObject* aObj);
  
  /// Given two lists returns a new list which is the concatenation of the first with the second
  INTERPRETER_EXPORT_FUNCTION BaseObject* concatLists(BaseObject* aList1, BaseObject* aList2);
  
  /// Returns the element at given index in the list.
  /// @note throws if the index is out of bounds
  /// @note the index must be a primitive int DataObject
  INTERPRETER_EXPORT_FUNCTION DataObject& getListElement(BaseObject* aList, const DataObject& aIdx);

  /// Sets the element at given index in the list.
  /// @note throws if the index is out of bounds
  /// @note the index must be a primitive int DataObject
  INTERPRETER_EXPORT_FUNCTION void setListElement(BaseObject* aList, const DataObject& aIdx,
                                                  const DataObject& aElem);
  
  /****************************************************
   *                                                  *
   *      Helper functions for MATRIX objects         *
   * @note "all" these functions assert if the given  *
   * object is not a matrix object.                   *
   ****************************************************/
  INTERPRETER_EXPORT_FUNCTION std::string getMatrixClassName();
  INTERPRETER_EXPORT_FUNCTION bool isMatrixObject(BaseObject* aObj);
  
  // Returns a the vector of the dimensions of the matrix
  INTERPRETER_EXPORT_FUNCTION std::vector<int> getMatrixDimensions(BaseObject* aObj);
  
  // Returns all the rows of the matrix concatenated into a unique vector or raw DataObjects
  INTERPRETER_EXPORT_FUNCTION std::vector<DataObject> getVectorizedMatrixData(BaseObject* aObj);
  
  /// Given a composite object representing the indices,returns the correspondent element.
  /// @note throws if the indices are out of bound.
  INTERPRETER_EXPORT_FUNCTION DataObject& getMatrixElement(BaseObject* aMatrix,
                                                           const DataObject& aIdx);
  
  /// Given a composite object representing the indices,returns the correspondent element.
  /// @note throws if the indices are out of bound.
  INTERPRETER_EXPORT_FUNCTION void setMatrixElement(BaseObject* aMatrix, const DataObject& aIdx,
                                                    const DataObject& aElem);
  
  /// Concatenates the two given matrices.
  /// The two matrices must have the same 2nd, 3rd, ..., nth dimension.
  /// For example:
  /// [1, 2, 3;   [3, 4;
  ///  4, 5, 6] .  5, 6]
  /// throws an "ET_OUT_OF_BOUND" error.
  /// For example:
  /// [1, 2, 3;   [3, 4, 5]
  ///  4, 5, 6] .
  /// gives the following matrix
  /// [1, 2, 3;
  ///  4, 5, 6;
  ///  3, 4, 5]
  INTERPRETER_EXPORT_FUNCTION BaseObject* concatMatrix(BaseObject* aMatrix1, BaseObject* aMatrix2);
  
  /****************************************************
   *                                                  *
   *      Helper functions for SEARCH objects         *
   * @note "all" these functions assert if the given  *
   * object is not a search object.                   *
   ****************************************************/
  
  INTERPRETER_EXPORT_FUNCTION std::string getSearchClassName();
  INTERPRETER_EXPORT_FUNCTION bool isSearchObject(BaseObject* aObj);
  
  // Returns the list object representing the scope of the search object
  INTERPRETER_EXPORT_FUNCTION DataObject& getSearchScope(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION DataObject& getSearchOptions(BaseObject* aObj);
  
  /****************************************************
   *                                                  *
   *      Helper functions for CONSTRAINT objects     *
   * @note "all" these functions assert if the given  *
   * object is not a constraint object.               *
   ****************************************************/
  
  INTERPRETER_EXPORT_FUNCTION std::string getConstraintClassName();
  INTERPRETER_EXPORT_FUNCTION bool isConstraintObject(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION DataObject& getConstraintPostFcn(BaseObject* aObj);
  
  /****************************************************
   *                                                  *
   *      Helper functions for VARIABLE objects       *
   * @note "all" these functions assert if the given  *
   * object is not a variable object.                 *
   ****************************************************/
  
  INTERPRETER_EXPORT_FUNCTION std::string getVariableClassName();
  INTERPRETER_EXPORT_FUNCTION bool isVariableObject(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION std::string getVarID(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION void setVarID(BaseObject* aObj, const std::string& aID);
  INTERPRETER_EXPORT_FUNCTION int getVarSemantic(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION void setVarSemantic(BaseObject* aObj, int aSem);
  INTERPRETER_EXPORT_FUNCTION std::size_t getVarInputOrder(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION void setVarInputOrder(BaseObject* aObj, std::size_t aInputOrder);
  INTERPRETER_EXPORT_FUNCTION int getVarDomainType(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION void setVarDomainType(BaseObject* aObj, int aDomType);
  INTERPRETER_EXPORT_FUNCTION DataObject& getVarDomain(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION BaseObject* getVarDomainList(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION DataObject& getVarDomainDimensions(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION BaseObject* getVarDomainDimensionsList(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION std::size_t getVarDomainNumDimensions(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION std::pair<int, int> getVarDomainDimensionsVals(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION int getVarDomainDimensionsSize(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION DataObject& getVarDomainArray(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION BaseObject* getVarDomainArrayList(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION DataObject& getVarDomainSubscriptIndices(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION BaseObject* getVarDomainSubscriptIndicesList(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION bool isVarSubscript(BaseObject* aObj);
  
  /// Returns the domain of a matrix variable according to the given indices.
  /// @note throws if the variable is not a matrix variable
  /// @note throws if the indices are out of bound
  /// @note throws if the indices are not represented as a composite object
  /// @note only vectors and 2D matrices are supported
  INTERPRETER_EXPORT_FUNCTION DataObject& getVarElement(BaseObject* aObj, const DataObject& aIndices);
  
  /****************************************************
   *                                                  *
   *      Helper functions for PARAMETER objects      *
   * @note PARAMETERS are the following BaseObjects:  *
   *  - scalars                                       *
   *  - lists                                         *
   *  - matrices                                      *
   * @note "all" these functions assert if the given  *
   * object is not a parameter object.                *
   ****************************************************/
  INTERPRETER_EXPORT_FUNCTION bool isParameterObject(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION bool isScalarParameterObject(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION bool isListParameterObject(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION bool isMatrixParameterObject(BaseObject* aObj);
  
  /// This function returns the primitive data object representing the scalar.
  /// @note throws if the object is not a scalar parameter object
  INTERPRETER_EXPORT_FUNCTION DataObject& getScalarParameterValue(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION std::vector<int> getParameterDimensions(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION BaseObject* getParameterDimensionsList(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION DataObject& getParameterValues(BaseObject* aObj);
  INTERPRETER_EXPORT_FUNCTION BaseObject* getParameterValuesList(BaseObject* aObj);
  
  /// Returns a new parameter which is the concatenations of the first to the second.
  /// @note the id of the parameter will match the id of the first parameter
  /// @note parameters must agree on dimensions,
  /// in particular:
  /// 1) an empty dimension can be concatenated to every parameter
  /// 2) if both parameter have 1 dimension there is no problem
  /// 3) both parameters must have the same number n of dimensions and the last n-1 dimensions must be the same
  /// If the two parameter do not agree on dimensions, it throws an out_of_range error
  INTERPRETER_EXPORT_FUNCTION BaseObject* concatParameters(BaseObject* aParam1, BaseObject* aParam2);
  
  /// Given a composite object representing the indices, looks up and returns the correspondent element.
  /// @note throws if the indices are out of bound.
  /// @note throws if the data object representing the index is not a composite object
  INTERPRETER_EXPORT_FUNCTION DataObject& getParameterElement(BaseObject* aParam,
                                                              const DataObject& aIndices);
  
  /// Given a composite object representing the indices, looks up and set the given element
  /// at the correspondent indices.
  /// @note throws if the indices are out of bound.
  /// @note throws if the data object representing the index is not a composite object
  INTERPRETER_EXPORT_FUNCTION void setParameterElement(BaseObject* aParam,
                                                       const DataObject& aIndices,
                                                       const DataObject& aElement);
  
}}// end namespace Interpreter/ObjectTools
