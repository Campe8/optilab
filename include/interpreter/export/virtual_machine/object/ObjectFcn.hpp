//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 4/14/2018
//
// Data function object for Objects.
//

#pragma once

#include "DataObjectFcn.hpp"
#include "DataObject.hpp"

// Forward declarations
namespace Interpreter {
  class BaseObject;
}//end namespace Interpreter

namespace Interpreter {

	class INTERPRETER_EXPORT_CLASS ObjectFcn : public DataObjectFcn {
  public:
    /// Callback function prototype for any callback of an object function.
    /// @param aObj base object instance to apply the callback to
    /// @param aArgs vector of input arguments
    /// @out output data object
    using ObjFcnHandler = std::function<DataObject(BaseObject* aObj,
                                                   const std::vector<DataObject>& aArgs)>;
    
	public:
		ObjectFcn(const std::string& aFcnName);
    
    /// Sets the callback function representing this inline function
    inline void setCallbackFcn(ObjFcnHandler aFcn) { pCallBack = aFcn; }
    
    /// Call the callback function on "aArgs" and returns either void (if no output is given
    /// by the callback) or the output value of the callback function
    DataObject operator()(BaseObject* aObj, const std::vector<DataObject>& aArgs);
    
    /// Returns a clone of this instance
    DataObjectFcn* clone() override;
    
  private:
    /// Callback function
    ObjFcnHandler pCallBack;
	};

}// end namespace Interpreter
