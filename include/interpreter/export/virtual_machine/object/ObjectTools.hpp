//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 4/5/2018
//
// Tools for creating and manipulating objects
// that derive from BaseObject.
//

#pragma once

#include "BaseObject.hpp"
#include "ObjectFcn.hpp"
#include "OPCode.hpp"

namespace Interpreter { namespace ObjectTools {

  /// Returns the value of the given property from the given object.
  /// Returns the default value of type T if no such property is found
  /// in the given object
  template<typename T>
  T getObjectPropertyValue(BaseObject* aObj, const std::string& aProperty)
  {
    if(!aObj || !aObj->hasProperty(aProperty)) return T();
    return aObj->getProperty(aProperty).getDataValue<T>();
  }
  
  /// Sets the value of the given property on the given object.
  template<typename T>
  void setObjectPropertyValue(BaseObject* aObj, const std::string& aProperty, const T& aValue)
  {
    if(!aObj || !aObj->hasProperty(aProperty)) return;
    aObj->setProperty(aProperty, DataObject(aValue));
  }
  
  template<typename T=std::string>
  void setObjectPropertyValue(BaseObject* aObj, const std::string& aProperty, const std::string& aValue)
  {
    if(!aObj || !aObj->hasProperty(aProperty)) return;
    aObj->setProperty(aProperty, DataObject(aValue.c_str()));
  }
  
  /// Returns the name of the property holding the type of an object
  INTERPRETER_EXPORT_FUNCTION BaseObject::PropertyKey getTypePropName();
  
	/// Returns the name of the property holding type information of a model context object
	INTERPRETER_EXPORT_FUNCTION BaseObject::PropertyKey getObjectTypePropName();

	/// Returns the name of the property holding fully interpreted information of a model context object
	INTERPRETER_EXPORT_FUNCTION  BaseObject::PropertyKey getObjectInterpretedPropName();

  /// Returns the name of the property holding the id of the object of a model context object
  INTERPRETER_EXPORT_FUNCTION  BaseObject::PropertyKey getObjectIDPropName();
  
  /// Creates a VOID object
  INTERPRETER_EXPORT_FUNCTION BaseObject* createObject();
  
  /// Creates a new base object.
  /// @note every object must have a (non empty) type
  INTERPRETER_EXPORT_FUNCTION BaseObject* createObject(const std::string& aType);
  
  /// Creates an object that derives from BaseObject and represents
  /// an object of the model context.
  /// The function takes the object type as argument (e.g., MDL_OBJ_TYPE_DOM, see OPCode.hpp).
  /// Properties of the object:
  /// 1) "objectType": type of this object, see OPCode for values.
  ///                  This property is not visible;
  /// 2) "fullyInterpreted": Boolean property stating if this object is fully interpreted.
  ///                        Default is true.
  ///
  INTERPRETER_EXPORT_FUNCTION BaseObject* createModelContextObject(int aObjType);
  
  /// Returns a pointer to a DataObjectFunction
  INTERPRETER_EXPORT_FUNCTION ObjectFcn* createObjectFcn(const std::string& aFcnName,
                                                         ObjectFcn::ObjFcnHandler aFcn, bool aHasOutput);
  
  /// Creates a scalar object.
  /// Properties of the object:
  /// 1) data: hidden property representing the scalar value
  /// @note asserts if the given data object is not a primitive type data object
  INTERPRETER_EXPORT_FUNCTION BaseObject* createScalarObject(const DataObject& aScalar);
  
  /// Creates a list object.
  /// A "list" is a (sequential) heterogeneous container of DataObjects.
  /// Each DataObject can be a primitive object, a class object, a function, etc.
  /// The following properties are available:
  /// 1) "data": the actual list of stored objects;
  /// 2) "size": the size of "values", it is a function that gets the size
  ///            or sets the size of the values objects.
  ///            Therefore, it MUST be called with the proper get/set methods
  ///            of the base object;
  INTERPRETER_EXPORT_FUNCTION BaseObject* createListObject(const std::vector<DataObject>& aList);
  INTERPRETER_EXPORT_FUNCTION BaseObjectSPtr createListObjectSPtr(const std::vector<DataObject>& aList);

  /// Creates a matrix object from:
  ///    a) vector of primitive objects or scalar base objects: this will create a row vector
  ///    b) vector of lists (BaseObjects): each list is a row of the matrix and they must all have
  ///       the same length
  ///    c) vector of matrix (BaseObjects): each matrix must have the same size
  ///       and the resulting matrix is the concatenation of all matrices
  ///    d) vector of composite data objects: each composite data object is a row of the matrix and
  ///       they must all have the same length
  /// Internally it is implemented as a list of lists.
  /// Properties of the object:
  /// 1) dims: list of the dimensions of the matrix
  /// 2) data: list of lists of values
  /// This function computes the dimensions of the matrix.
  /// For example:
  /// [2, 3, 4]
  /// generates a parameter vector with dimensions [1 x 3].
  /// The following input:
  /// [[2, 3, 4], [5, 6, 7]]
  /// generates a new matrix with dimensions [2 x 3].
  /// @note if "aVals" is empty, this function creates an empty matrix
  /// @note dimensions must be consistent, i.e., rows must have the same length.
  /// For example:
  /// [[2, 3, 4], [5, 6]]
  /// will throw a std::logic_error
  /// @note dimensions are calculated automatically from the input values
  INTERPRETER_EXPORT_FUNCTION BaseObject* createMatrixObject(const std::vector<DataObject>& aVals);
  
  /// Creates an object that derives from ModelContextObject and that represents a search object
  /// in the model context.
  /// This object contains information about the set of variables to search on and the
  /// search options which is another BaseObject.
  /// The following properties are available:
  /// 1) scope: a base object list or a composite object containing the variables to search on;
  /// 2) searchOptions: options for search.
  /// @note if no search options is specified, by default creates a DFS search options object.
  /// @note it doesn not perform any consistency checks on the list of variables (i.e., the caller
  /// is responsible for passing the right list of arguments
  INTERPRETER_EXPORT_FUNCTION BaseObject* createObjectSearch(const DataObject& aVarsList);
  INTERPRETER_EXPORT_FUNCTION BaseObject* createObjectSearch(const DataObject& aVarsList,
                                                             const DataObject& aSearchOptions);
  
  /// Creates an object that derives from ModelContextObject and that represents a constraint
  /// in the model context.
  /// This object is mainly a wrapper around a DataObject of type function, where the function
  /// if posts the constraints into the model.
  /// The following properties are available:
  /// 1) name: constraint name, e.g., nq, eq, etc.;
  /// 2) __post_constraint_fcn: post constraint DataObject function.
  INTERPRETER_EXPORT_FUNCTION BaseObject* createObjectConstraint(const DataObject& aPostFcn);
  
  /// Creates an object that derives from ModelContextObject and
  /// represents a variable of the model context.
  /// This object is mainly a wrapper around a Domain with additional information
  /// stored as properties.
  /// The following properties are available:
  /// 1) id: ID of the variable
  /// 2) domainType: SINGLETON, BOUNDS, ...
  /// 3) domain: list of domain elements
  /// 4) aSubscriptIdx: subscript indices if the variable is a subscript variable, e.g., x[2, 3]
  ///                   its domain is stored as the property "aDomain"
  /// 5) aSemantic: value for semantic, VAR_SPEC_NONE, VAR_SPEC_MIN, etc. see OPCode.hpp
  INTERPRETER_EXPORT_FUNCTION BaseObject* createObjectVariable(  const std::string& aVarID
                                                               , int aDomainType
                                                               , const std::vector<DataObject>& aDomain
                                                               , int aSemantic=VAR_SPEC_NONE);
  
  /// Creates a an object that derives from ModelContextObject and
  /// represents a variable of the model context.
  /// This object is mainly a wrapper around a Domain with additional information
  /// stored as properties.
  /// The following properties are available:
  /// 1) aVarID: ID of the variable
  /// 2) domainType: SINGLETON, BOUNDS, ...
  /// 3) domain: list of domain elements
  /// 4) domainDimensions: list of dimensions of the domain, if empty the domain is singleton (i.e., not a matrix)
  /// 5) domainArray: list of (sub) domains composing the matrix if dimensions are not empty
  /// 6) domainSubscript: list of subscript indices if the variable is a subscript variable, e.g., x[2, 3]
  ///                     its domain is stored as the property "aDomain"
  /// 7) aSemantic: value for semantic, VAR_SPEC_NONE, VAR_SPEC_MIN, etc. see OPCode.hpp
  /// @note aDomainArray is a vector of domains, i.e., a vector of lists of domain elements
  INTERPRETER_EXPORT_FUNCTION BaseObject* createObjectMatrixVariable(const std::string& aVarID,
                                                                     int aDomainType,
                                                                     const std::vector<DataObject>& aDomain,
                                                                     const std::vector<DataObject>& aDomainDims,
                                                                     const std::vector<DataObject>& aDomainArray,
                                                                     int aSemantic=VAR_SPEC_NONE);
  
  /// Creates a an object that derives from ModelContextObject and
  /// represents a subscript variable of a variable from the model context.
  /// @param "aVar" the variable to subscript into
  /// @param "aIndices" indexing for the subscript operator
  /// @return a new subscript variable object with the same ID and domain dimensions
  ///         of the given variable. The domain is retrieved from the given variable at given indices.
  ///         The original domain dimensions and the indices can be used to vectorized the matrix later on.
  /// For example:
  /// Given x matrix variable with domain [[1, 4], [5, 6]] and subscript [0]
  /// returns the subscript x[1] with domain [1, 4].
  /// @note throws if the given object is not a variable.
  /// @note throws out_of_bound if the indices are not consistent with the size of the matrix
  /// @note only 1D and 2D matrices are supported
  INTERPRETER_EXPORT_FUNCTION BaseObject* createObjectMatrixVariableSubscr(BaseObject* aVar,
                                                                           const DataObject& aIndices);
}}// end namespace Interpreter/ObjectTools
