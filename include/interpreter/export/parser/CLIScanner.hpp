//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 01/30/2018
//

#pragma once
#if ! defined(yyFlexLexerOnce)
#include "FlexLexer.h"
#endif

#include "CLIParserExportDefs.hpp"

#include "CLITypes.hpp"
#include "cli_parser.tab.hh"
#include "location.hh"

namespace CLI {

class CLI_PARSER_EXPORT_CLASS CLIScanner : public yyFlexLexer{
public:
  
  /// Constructor:
  /// gets the input stream to scan and a Boolean flag indicating
  /// if (true) the entry point for the parser is a single line
  /// or (false) a file
   CLIScanner(std::istream *in, bool aLineEntryPoint=true)
  : pCheckEntryPoint(true)
  , pLineEntryPoint(aLineEntryPoint)
  , yyFlexLexer(in) {};
  
   virtual ~CLIScanner() {};

  /// Returns true if the scanner needs to check the type of entry point,
  /// false otherwise
  inline bool needsCheckOnEntryPoint() const { return pCheckEntryPoint; }
  
  /// Reset the check entry point flag
  inline void resetCheckOnEntryPoint()  { pCheckEntryPoint = false; }
  
  /// Returns true if the entry point is for a line input stream,
  /// false if the entry point is for a file stream
  inline bool isLineEntryPoint() const { return pLineEntryPoint; }

  
  // get rid of override virtual function warning
  using FlexLexer::yylex;

   virtual
   int yylex( CLI::CLIParser::semantic_type * const lval,
              CLI::CLIParser::location_type *location );
   // YY_DECL defined in cli_lexer.l
   // Method body created by flex in cli_lexer.yy.cc


private:
  // Reset flag for checking the entry point
  bool pCheckEntryPoint;
  
  // Entry point
  bool pLineEntryPoint;
  
  /* yyval ptr */
  CLI::CLIParser::semantic_type *yylval = nullptr;
};

} /* end namespace MC */
