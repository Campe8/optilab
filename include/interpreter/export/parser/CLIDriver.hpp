//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 01/30/2018
//

#pragma once

#include "CLIParserExportDefs.hpp"

#include "CLIScanner.hpp"
#include "CLICoreObj.hpp"
#include "cli_parser.tab.hh"

#include <iterator>  // for std::advance
#include <istream>
#include <list>
#include <string>
#include <stack>
#include <vector>
#include <cstddef>
#include <memory>
#include <stdexcept>  // for std::runtime_error
#include <cassert>

namespace CLI {
  
  class CLI_PARSER_EXPORT_CLASS CLIDriver {
  public:
    class ConstType {
    public:
      ConstType(CLI_P_INT aVal)
      : pInt(aVal)
      , pType(0) {}
      
      ConstType(const CLI_P_STR& aVal)
      : pString(aVal)
      , pType(1) {}
      
      /// Returns the type of the constant as:
      /// 0 - integer
      /// 1 - string
      inline int getType() const { return pType; }
      
      /// Returns internal values
      inline CLI_P_INT getInt() const { return pInt; }
      inline CLI_P_STR getString() const { return pString; }
      
    private:
      int pType;
      CLI_P_INT pInt;
      CLI_P_STR pString;
    };
    
    using byte_T = unsigned char;
    using ByteArray = std::vector<byte_T>;
    using NameArray = std::vector<CLI_P_STR>;
    using ConstList = std::vector<ConstType>;
    using ConstArray = std::vector<ConstList>;
    
    struct CLI_PARSER_EXPORT_STRUCT ByteCode {
      /// Global names
      NameArray global_array;
      /// Local names
      NameArray local_array;
      /// Constants: array of tuples
      /// every tuple as usually size one
      /// but for some cases, e.g., lists,
      /// it can have size greater than one
      ConstArray const_array;
      /// Byte code
      ByteArray byte_code;
    };
    
  public:
    CLIDriver();
    
    virtual ~CLIDriver() = default;
    
    /// parse - parse from a c++ input stream
    /// @param is - std::istream&, valid input stream
    void parse(std::istream& aISS);
    
    /// Sets the internal stream source flag:
    /// true: the input stream to parse is from a line
    /// false: the input stream to parse is from a file
    inline void setInputStreamLine(bool aInStreamIsLine) {
      pInStreamIsLine = aInStreamIsLine;
    };
    
    /// Returns the byte code representation of the internal stack
    /// after parsing the input stream.
    /// It takes a (possibly empty) bytecode as current environment
    ByteCode byteCode(const ByteCode& aEnvironment = ByteCode());
    
    /// Returns true if the driver encountered an error during parsing. false otherwise
    inline bool hasError() const { return pLexerError || pParserError; }
    
    /// Returns an error message if any (check with "hasError()")
    inline std::string getErrorMsg() const { return pErrMsg; }
    
    /// Prints the internal stack on std out
    void printStack();
    
  protected:
    friend class CLIParser;
    
    /// Prints "str" on standard output
    void debug(const std::string& str);

    /// Add a LHS Start pointer to the LHS Start pointers to mark
    /// the beginning of an assignment
    void setLHSStartAssign();
    void popLHSStartAssign();
    
    /// Add a LHS End pointer to the LHS End pointers to mark
    /// the ending of a LHS expr on an assignment
    void setLHSEndAssign();
    
    /// Add a RHS Start pointer to the RHS Start pointers to mark
    /// the beginning of the RHS expr of an assignment
    void setRHSStartAssign();
    
    /// Add a RHS End pointer to the RHS Start pointers to mark
    /// the ending of a RHS expr on an assignment
    void setRHSEndAssign();
    
    /// Add a sequence expression pointer to the SeqExpr Start pointers stack to mark
    /// the beginning of a sequence expression
    void setSeqExprStart();
    
    /// Pop an object from the stack
    inline void popStack() { pStack.pop(); }
    
    /// Adds an assignment expression on the stack,
    /// i.e., adds an assignment LHS: RHS depending on the type of LHS and RHS expressions.
    /// The algorithms is as follows:
    /// given
    /// [ ... | abc | LHSStart | x | LHSEnd | RHSStart | 10 | RHSEnd | def |... ]
    /// switches
    /// [RHSStart | 10 | RHSEnd]
    /// with
    /// [LHSStart | x | LHSEnd]
    /// abd removes the tags to obtain the following stack
    /// [ ... | abc | 10 | x | STORE_NAME | def |... ]
    /// which will be later translated into
    /// LOAD_CONST 10
    /// LOAD_NAME  x
    /// STORE_NAME
    void addAssignExpr();
    
    /// Adds a Integer scalar object type on the stack
    inline void addCoreExpr(CLI_P_INT aObj)
    {
      pStack.push(CLICoreObj(aObj));
    }
    
    /// Adds a Boolean scalar object type on the stack
    inline void addCoreExpr(CLI_P_BOOL aObj)
    {
      pStack.push(CLICoreObj(aObj));
    }
    
    /// Adds a String object type on the stack
    inline void addCoreExpr(const CLI_P_STR& aObj)
    {
      pStack.push(CLICoreObj(aObj));
    }
    
    /// Adds the bytecode to load the property "aProp"
    void addLoadProperty(const std::string& aProp);
    
    /// Adds unary operators (false: minus, true: plus) on the stack
    void addUnaryOperators(const std::vector<bool>& aOperators);
    
    /// Adds a list object on the stack where "aSize" is the number of elements
    /// in list and "aType" is the type of list.
    /// The type of list can be:
    /// - OT_LIST_ARGS: for list arguments, e.g., (a, b, c);
    /// - OT_LIST_ARRAY: for lists, e.g., [a, b, c];
    /// - OT_RNG_LIST: for range expressions, e.g., [a..b].
    inline void addList(int aSize, CLICoreObj::ObjType aType)
    {
      pStack.push(CLICoreObj(aSize, aType));
    }
    
    /// Pushes the expression of type "aEOP" on the stack.
    /// Reduces elements if possible, i.e., compute binary expressions
    void addExpr(CLICoreObj::ObjType aEOP);
    
    /// Marks the end of a statement taking actions in byte code generation.
    /// Set the input argument to true if the statement is an empty statement, false otherwise
    void markEndOfStmt(bool aIsEmpty = false);
    
    /// Adds an end if block object on the stack.
    /// This sets the PC at the beginning of the IF block
    /// to jump to the current instruction (aJumpToNextInstruction = false)
    /// or to the next instruction (aJumpToNextInstruction = true)
    void addEndIfBlock(bool aJumpToNextInstruction = false);
    
    /// Adds an end if block object on the stack
    inline void addElseBlock()
    {
      // Mark the beginning of an if stmt on the stack
      pStack.push(CLICoreObj(CLICoreObj::ObjType::OT_ELSE_BLK));
    }
    
    /// Adds a if block object on "aID" iterator on the stack
    inline void addIfBlock()
    {
      // Mark the beginning of an if stmt on the stack
      pStack.push(CLICoreObj(CLICoreObj::ObjType::OT_IF_BLK));
    }

    /// Evaluates the sequence expression and the object preceding it and, if appropriate,
    /// set the object as an object with a list
    void setObjectWithList();
    
    /// Adds a end block object on the stack
    void addEndBlock();
    
    /// Adds a for-loop range iterator w.r.t. the 2 values
    /// representing the range bounds on the stack
    void addForLoopRangeIter(const std::string& aID, CLICoreObj::ObjType aIterType, int aListLength);
    
    /// Adds a for-loop block object on "aID" iterator on the stack
    inline void addForLoopBlock()
    {
      // Mark the beginning of a for-loop stmt on the stack
      pStack.push(CLICoreObj(CLICoreObj::ObjType::OT_FOR_LOOP));
    }
    
    /// Sets lexer error
    void setLexerError(const std::string& aStr);
    
    /// Marks the object code by inserting a mark opcode
    inline void markObjectCode() { pStack.push(CLICoreObj(CLICoreObj::ObjType::OT_MARK)); }
    
    /// Sets parser error
    void setParserError(const std::string& aStr, const CLIParser::location_type& aLoc);
    
    /// Adds a "let" (assignment) expression on the stack provided the "aID",
    /// and the rhs expression
    void addLetExpr(const std::string& aID);
    
    /// Adds an assignment expression on the stack provided the lhs "aID",
    /// and the rhs expression with its size
    void addVarDecl(const std::string& aID, CLICoreObj::ObjType aType, int aDomSize, int aOpt);
    
    /// Adds an assignment expression for a matrix decision variable on the stack,
    /// provided the lhs "aID", and the rhs expression with its size
    void addMatrixVarDecl(const std::string& aID, CLICoreObj::ObjType aType, int aDomSize, int aOpt);
    
  private:
    class DriverStack {
    public:
      using Stack = std::list<CLICoreObj>;
      using StackIter = std::list<CLICoreObj>::iterator;
      
    public:
      DriverStack() = default;
      DriverStack(const Stack& aStack) : pStack(aStack) {}
      
      inline void clear() { pStack.clear(); }
      
      inline void pop() { pStack.pop_back(); }
      
      inline CLICoreObj& top() { return pStack.back(); }
      
      /// Removes from the stack the element pointer by the given iterator.
      /// @note returns the pointer to the element following the removed element
      inline StackIter remove(StackIter iter) { return pStack.erase(iter); }
      
      /// Returns the reference to the idx to the top object in the stck.
      /// @note throws std::runtime_error if aIdx greater than or equal the size
      /// of the stack
      CLICoreObj& peek(std::size_t aIdx)
      {
        if (aIdx >= pStack.size())
        {
          throw std::runtime_error("Index outside range");
        }
        auto it = pStack.rbegin();
        std::advance(it, aIdx);
        return *it;
      }
      
      /// popBack() is the same as pop()
      inline void popBack() { pop(); }
      
      /// Pops an element from the front of the stack.
      /// This makes the stack behave like a list
      inline void popFront() { pStack.pop_front(); }
      
      /// Merges "aStack" onto the top of this stack
      inline void merge(DriverStack& aStack)
      {
        pStack.insert(pStack.end(), aStack.pStack.begin(), aStack.pStack.end());
      }
      
      /// Inserts "aStack" in this stack before the given "iter" position
      inline void insert(const DriverStack& aStack, StackIter& iter)
      {
        pStack.insert(iter, aStack.pStack.begin(), aStack.pStack.end());
      }
      
      inline void push(const CLICoreObj& obj) { pStack.push_back(obj); }
      
      StackIter getFirstElementPtr()
      {
        return pStack.begin();
      }
      
      StackIter getLastElementPtr()
      {
        auto it = pStack.end();
        it--;
        return it;
      }
      
      StackIter begin()
      {
        return pStack.begin();
      }
      
      StackIter end()
      {
        return pStack.end();
      }
      
      inline bool isEmpty() const { return getSize() == 0; }
      inline std::size_t getSize() const { return pStack.size(); }
      
      /// Extracts from this stack the elements in the range [aStart, aEnd].
      /// @note aEnd element is NOT included in the returned stack
      DriverStack getSubStack(StackIter& aStart, StackIter& aEnd)
      {
        Stack substack;
        auto endPtr = aEnd;
        substack.splice(substack.begin(), pStack, aStart, ++endPtr);
        return DriverStack(substack);
      }
      
      /// Prints the types in the stack in FIFO order
      void printFIFOStackType()
      {
        for (auto& it : pStack)
        {
          std::cout << it.typeToString() << std::endl;
        }
      }// printFIFOStackType
      
    private:
      Stack pStack;
    };
    
    using PtrStack = std::vector<DriverStack::StackIter>;
    
  private:
    DriverStack pStack;
    
    /// Stack used to collect LHS start pointer on assignment
    PtrStack pLHSStartAssignStack;
    
    /// Stack used to collect LHS end pointer on assignment
    PtrStack pLHSEndAssignStack;
    
    /// Stack used to collect RHS start pointer on assignment
    PtrStack pRHSStartAssignStack;
    
    /// Stack used to collect RHS end pointer on assignment
    PtrStack pRHSEndAssignStack;
    
    /// Stack used to collect Sequence expression start pointers
    PtrStack pSeqExprStartStack;
    
    /// Input stream flag
    bool pInStreamIsLine;
    
    /// Error flags
    bool pLexerError;
    bool pParserError;
    
    /// Error message
    std::string pErrMsg;
    
    /// Parser
    std::unique_ptr<CLI::CLIParser> pParser;
    
    /// Scanner
    std::unique_ptr<CLI::CLIScanner> pScanner;
    
    /// Stack of program counter for handling blocks with jumps
    std::vector<std::size_t> pBlockStackPC;
    
    /// Resets internal data structures
    void resetDriver();
    
    /// Utility function: given an iterator on the stack, check if the object pointer by the
    //// iterator is within a loop-statement. Returns true is so, false otherwise
    bool isFlowStmtWithinLoop(DriverStack::StackIter it);
    
    /// Helper method for parsing the given input stream
    void parseHelper(std::istream &aStream);
    
    /// Error handling.
    void error(const std::string& aStr, const CLIParser::location_type& aLoc);
    void error(const std::string& aStr);
  };
  
  void printByteCode(const CLIDriver::ByteCode& aBC);
  
} /* end namespace CLI */

