//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 01/31/2018
//

#pragma once

// Data types
#define CLI_P_INT			long
#define CLI_P_DOUBLE  double
#define CLI_P_BOOL		bool
#define CLI_P_STR			std::string

// Macros for CLI tokens
#define CLI_P_INT_INF std::numeric_limits<CLI_P_INT>::max()
#define CLI_P_TRUE  1
#define CLI_P_FALSE 0
