//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 2/2/2018
//

#pragma once

#include "CLIParserExportDefs.hpp"

#include "CLITypes.hpp"

#include <string>

namespace CLI {
  
  struct CLI_PARSER_EXPORT_STRUCT CLICoreObj {
    enum class ObjType : char {
        OT_INT = 0
      , OT_BOOL
      , OT_STRING
      , OT_ATTR        // Load an attribute name
      , OT_ASSIGN_ATTR // Store an attribue
      , OT_LIST_ARGS
      , OT_LIST_ARRAY
      , OT_LIST_DOMAIN
      , OT_LIST_PROP   // List of object properties
      , OT_UNARY_NEG
      , OT_UNARY_POS
      , OT_PLUS
      , OT_MIN
      , OT_PROD
      , OT_DIV
      , OT_LT
      , OT_LE
      , OT_GT
      , OT_GE
      , OT_EQ
      , OT_NQ
      , OT_AND
      , OT_OR
      , OT_REIF
      , OT_SUBSCR
      , OT_CALL_FCN
      , OT_TUPLE
      , OT_POP_TOP
      , OT_LET
      , OT_RETURN
      , OT_CONTINUE
      , OT_BREAK
      , OT_IF_BLK     // If block
      , OT_ELSE_BLK   // Else block
      , OT_END_IF_BLK // End if block object type
      , OT_RNG_LIST   // Range list expression type (i.e., [a..b])
      , OT_ASSIGN     // Assignment expression type
      , OT_ASSIGN_SUBSCR // Assignment expression type for subscript operators
      , OT_SNG        // Singleton expression type
      , OT_RNG        // Range expression type
      , OT_SET        // Set expression type
      , OT_RNG_DOMAIN // Domain declared as min..max
      , OT_SNG_M      // Singleton matrix expression type
      , OT_RNG_M      // Range matrix expression type
      , OT_SET_M      // Set matrix expression type
      , OT_FOR_LOOP   // For loop object type
      , OT_WHILE_LOOP // While loop object type
      , OT_FOR_LOOP_RANGE_ITER   // For loop range iterator object type
      , OT_END_BLK    // End block object type
      , OT_PRINT_TOP  // Prints the object at the top of the stack
      , OT_MARK
      , OT_OPT_MIN    // Optimization minimize
      , OT_OPT_MAX    // Optimization maximize
      , OT_LHS_START_MARKER // Dummy object type used as marker for LHS
      , OT_LHS_END_MARKER   // Dummy object type used as marker for LHS
      , OT_RHS_START_MARKER // Dummy object type used as marker for RHS
      , OT_RHS_END_MARKER   // Dummy object type used as marker for RHS
      , OT_SEQ_EXPR_START_MARKER  // Dummy object type used as marker for start sequence expressions
      , OT_STACK_MARKER     // Dummy object type used to mark the beginning of the stack
      , OT_UNDEF
    };
    
    /// Scalar object
    CLICoreObj() : objType(ObjType::OT_UNDEF){}
    CLICoreObj(ObjType aType) : objType(aType) {}
    CLICoreObj(CLI_P_INT aObj) : objType(ObjType::OT_INT), intObj(aObj) {}
    CLICoreObj(bool aObj) : objType(ObjType::OT_BOOL), intObj(aObj ? CLI_P_TRUE : CLI_P_FALSE) {}
    CLICoreObj(const CLI_P_STR& aObj) : objType(ObjType::OT_STRING), strObj(aObj) {}
    
    /// Composite object
    CLICoreObj(int aSize, ObjType aType) : size(aSize), objType(aType) {}
    
    ObjType objType;
    CLI_P_INT intObj;
    CLI_P_STR strObj;
    int size = -1;
    bool objWithList = false;
    ObjType objOptimization = ObjType::OT_UNDEF;

    /// Utility function: converts type to string
    std::string typeToString()
    {
      switch(objType)
      {
        case ObjType::OT_INT:
          return "OT_INT";
        case ObjType::OT_BOOL:
          return "OT_BOOL";
        case ObjType::OT_STRING:
          return "OT_STRING";
        case ObjType::OT_ATTR:
          return "OT_ATTR";
        case ObjType::OT_ASSIGN_ATTR:
          return "OT_ASSIGN_ATTR";
        case ObjType::OT_LIST_ARGS:
          return "OT_LIST_ARGS";
        case ObjType::OT_LIST_ARRAY:
          return "OT_LIST_ARRAY";
        case ObjType::OT_LIST_DOMAIN:
          return "OT_LIST_DOMAIN";
        case ObjType::OT_LIST_PROP:
          return "OT_LIST_PROP";
        case ObjType::OT_UNARY_NEG:
          return "OT_UNARY_NEG";
        case ObjType::OT_UNARY_POS:
          return "OT_UNARY_POS";
        case ObjType::OT_PLUS:
          return "OT_PLUS";
        case ObjType::OT_MIN:
          return "OT_MIN";
        case ObjType::OT_PROD:
          return "OT_PROD";
        case ObjType::OT_DIV:
          return "OT_DIV";
        case ObjType::OT_LT:
          return "OT_LT";
        case ObjType::OT_LE:
          return "OT_LE";
        case ObjType::OT_GT:
          return "OT_GT";
        case ObjType::OT_GE:
          return "OT_GE";
        case ObjType::OT_EQ:
          return "OT_EQ";
        case ObjType::OT_NQ:
          return "OT_NQ";
        case ObjType::OT_AND:
          return "OT_AND";
        case ObjType::OT_OR:
          return "OT_OR";
        case ObjType::OT_REIF:
          return "OT_REIF";
        case ObjType::OT_SUBSCR:
          return "OT_SUBSCR";
        case ObjType::OT_CALL_FCN:
          return "OT_CALL_FCN";
        case ObjType::OT_TUPLE:
          return "OT_TUPLE";
        case ObjType::OT_POP_TOP:
          return "OT_POP_TOP";
        case ObjType::OT_LET:
          return "OT_LET";
        case ObjType::OT_RETURN:
          return "OT_RETURN";
        case ObjType::OT_CONTINUE:
          return "OT_CONTINUE";
        case ObjType::OT_BREAK:
          return "OT_BREAK";
        case ObjType::OT_IF_BLK:
          return "OT_IF_BLK";
        case ObjType::OT_ELSE_BLK:
          return "OT_ELSE_BLK";
        case ObjType::OT_END_IF_BLK:
          return "OT_END_IF_BLK";
        case ObjType::OT_RNG_LIST:
          return "OT_RNG_LIST";
        case ObjType::OT_ASSIGN:
          return "OT_ASSIGN";
        case ObjType::OT_ASSIGN_SUBSCR:
          return "OT_ASSIGN_SUBSCR";
        case ObjType::OT_SNG:
          return "OT_SNG";
        case ObjType::OT_RNG:
          return "OT_RNG";
        case ObjType::OT_SET:
          return "OT_SET";
        case ObjType::OT_RNG_DOMAIN:
          return "OT_RNG_DOMAIN";
        case ObjType::OT_SNG_M:
          return "OT_SNG_M";
        case ObjType::OT_RNG_M:
          return "OT_RNG_M";
        case ObjType::OT_SET_M:
          return "OT_SET_M";
        case ObjType::OT_FOR_LOOP:
          return "OT_FOR_LOOP";
        case ObjType::OT_WHILE_LOOP:
          return "OT_WHILE_LOOP";
        case ObjType::OT_FOR_LOOP_RANGE_ITER:
          return "OT_FOR_LOOP_RANGE_ITER";
        case ObjType::OT_END_BLK:
          return "OT_END_BLK";
        case ObjType::OT_PRINT_TOP:
          return "OT_PRINT_TOP";
        case ObjType::OT_MARK:
          return "OT_MARK";
        case ObjType::OT_OPT_MIN:
          return "OT_OPT_MIN";
        case ObjType::OT_OPT_MAX:
          return "OT_OPT_MAX";
        case ObjType::OT_LHS_START_MARKER:
          return "OT_LHS_START_MARKER";
        case ObjType::OT_LHS_END_MARKER:
          return "OT_LHS_END_MARKER";
        case ObjType::OT_RHS_START_MARKER:
          return "OT_RHS_START_MARKER";
        case ObjType::OT_RHS_END_MARKER:
          return "OT_RHS_END_MARKER";
        case ObjType::OT_SEQ_EXPR_START_MARKER:
          return "OT_SEQ_EXPR_START_MARKER";
        case ObjType::OT_STACK_MARKER:
          return "OT_STACK_MARKER";
        default:
          return "OT_UNDEF";
      }
    }// typeToString
    
  };
  
}// end namespace CLI
