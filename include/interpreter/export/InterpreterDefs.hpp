//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 2/12/2018
//

#pragma once

#include "InterpreterExportDefs.hpp"

#include <string>
#include <exception>

#define itp_throw(aMsg) throw InterpreterException{aMsg}
#define itp_type_throw(aType) throw InterpreterException(aType, "")
#define itp_type_full_throw(aType, aMsg) throw InterpreterException(aType, aMsg)

#define itp_assert(expr, aType) \
((expr) ? (void)0 : itp_type_throw((aType)))

#define itp_assert_type_msg(expr, aType, aMsg) \
((expr) ? (void)0 : itp_type_full_throw((aType), (aMsg)))

#define itp_assert_msg(expr, msg) \
((expr) ? (void)0 : itp_throw((msg)))

namespace Interpreter {

	struct INTERPRETER_EXPORT_STRUCT InterpreterException : public std::exception {

		enum ExceptionType : char {
        ET_FCN_UNDEF = 0	  // undefined function
      , ET_FCN_ERR          // error in calling a function
      , ET_NON_INTERP			  // non interpreted value
      , ET_INVALID_INTERP   // cannot interpret the value
      , ET_BAD_ALLOC        // bad allocation
      , ET_OUT_OF_BOUND     // out of bound exception
      , ET_BAD_SUBSCR       // bad subscript (e.g., no subscript operator on the object)
      , ET_WRONG_TYPE       // using a non admissible data type
      , ET_FEATURE_OFF      // feature off or in progress
      , ET_WRONG_TYPE_OBJ   // the element is not an object
      , ET_PROP_NOT_FOUND   // property not found
      , ET_PROP_INVALID_KEY // invalid key
      , ET_PROP_CONST       // const property
      , ET_LOGIC_ERROR      // general logic error
			, ET_UNSPEC
		};

		InterpreterException(ExceptionType aType, const std::string& aMsg="")
			: pType(aType)
			, pMsg(aMsg)
		{
		}

		InterpreterException(const std::string& aMsg)
			: pType(ExceptionType::ET_UNSPEC)
			, pMsg(aMsg)
		{
		}

		const char *what() const noexcept { return pMsg.c_str(); }

		ExceptionType getType() const { return pType; }

	private:
		/// Exception type
		ExceptionType pType;

		/// Exception message
		std::string pMsg;
	};

}// end namespace Interpreter
