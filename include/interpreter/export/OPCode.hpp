//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 2/5/2018
//

#pragma once

#define POP_TOP                1
#define ROT_TWO                2
#define DUP_TOP                3
#define DUP_TOP_TWO            4
#define DUP_ROT_TWO            5
#define RETURN_VALUE           6
#define GET_ITER               7
#define BREAK_LOOP             8
#define POP_BLOCK              9
#define UNARY_NOT             10
#define UNARY_NEGATIVE        11
#define BINARY_POWER          12
#define BINARY_MULTIPLY       13
#define BINARY_DIVIDE         14
#define BINARY_MODULO         15
#define BINARY_ADD            16
#define BINARY_SUBTRACT       17
#define BINARY_AND            18
#define BINARY_OR             19
#define BINARY_XOR            20
#define BINARY_SUBSCR         21
#define INPLACE_ADD           22
#define INPLACE_SUBTRACT      23
#define INPLACE_MULTIPLY      24
#define INPLACE_DIVIDE        25
#define INPLACE_MODULO        26
#define INPLACE_POWER         27
#define INPLACE_AND           28
#define INPLACE_XOR           29
#define INPLACE_OR            30
#define ADD_MARK              31
#define PRINT_TOP             32
#define RANGE_LIST            33
#define UNARY_POSITIVE        34
#define STORE_SUBSCR          35

#define HAVE_ARGUMENT         41

#define DUP_TOPX              41
#define LOAD_CONST            42
#define LOAD_NAME             43
#define STORE_NAME            44
#define DELETE_NAME           45
#define LOAD_FAST             46
#define STORE_FAST            47
#define DELETE_FAST           48
#define LOAD_GLOBAL           49
#define STORE_GLOBAL          50
#define DELETE_GLOBAL         51
#define COMPARE_OP            52
#define BUILD_LIST            53
#define JUMP_FORWARD          54
#define JUMP_ABSOLUTE         55
#define POP_JUMP_IF_FALSE     56
#define POP_JUMP_IF_TRUE      57
#define JUMP_IF_FALSE_OR_POP  58
#define JUMP_IF_TRUE_OR_POP   59
#define SETUP_LOOP            60
#define FOR_ITER              61
#define CONTINUE_LOOP         62
#define CALL_FUNCTION         63
#define BUILD_DOMAIN          64
#define STORE_VAR             65
#define LOAD_CONST_BOOL       66
#define SET_VAR_SPEC          67
#define LOAD_ATTR             68 // index in name list
#define STORE_ATTR            69 // name to store
#define BUILD_TUPLE           70 // number of elements in the tuple

// Comparison operators code
#define OP_LT 0
#define OP_LE 1
#define OP_EQ 2
#define OP_NE 3
#define OP_GT 4
#define OP_GE 5

// Domain type code
#define DOM_SINGLETON   0
#define DOM_BOUNDS      1
#define DOM_LIST        2
#define DOM_SINGLETON_M 3
#define DOM_BOUNDS_M    4
#define DOM_LIST_M      5

// Var specification code
#define VAR_SPEC_NONE     0
#define VAR_SPEC_OPT_MIN  1
#define VAR_SPEC_OPT_MAX  2
#define VAR_SPEC_DECISION 3
#define VAR_SPEC_SUPPORT  4

// Domain bounds with open range
#define DOM_BOUNDS_OPEN       10
#define DOM_BOUNDS_OPEN_RIGHT 11
#define DOM_BOUNDS_OPEN_LEFT  12

// Model object types
#define MDL_OBJ_TYPE_DOM  0
#define MDL_OBJ_TYPE_LIST 1

// Code used for exceptions
#define EXCEPTION_CODE  255
