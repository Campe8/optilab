//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 3/9/2017
//
// Memento state for backtrackable objects
// of class ConstraintStore.
//

#pragma once

// Base class
#include "MementoState.hpp"

// Search heuristic state
#include "ConstraintStoreState.hpp"


namespace Search {

  class SEARCH_EXPORT_CLASS MementoStateConstraintStore : public MementoState {
  public:
    MementoStateConstraintStore(std::unique_ptr<Semantics::ConstraintStoreState> aConstraintStoreState);

    ~MementoStateConstraintStore();

    static bool isa(const MementoState* aMementoState)
    {
      assert(aMementoState);
      return aMementoState->getMementoStateId() == MementoStateId::MS_CONSTRAINT_STORE;
    }
    
    static MementoStateConstraintStore* cast(MementoState* aMementoState)
    {
      if (!isa(aMementoState))
      {
        return nullptr;
      }
      return static_cast<MementoStateConstraintStore*>(aMementoState);
    }
    
    static const MementoStateConstraintStore* cast(const MementoState* aMementoState)
    {
      if (!isa(aMementoState))
      {
        return nullptr;
      }
      return static_cast<const MementoStateConstraintStore*>(aMementoState);
    }
    
    inline const Semantics::ConstraintStoreState* getConstraintStoreState() const
    {
      return pConstraintStoreState.get();
    }
    
  private:
    std::unique_ptr<Semantics::ConstraintStoreState> pConstraintStoreState;
  };
  
} // end namespace Search
