//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 11/10/2018
//
// Wrapper around a hash map data structure
// for (de)registering constraints into the
// constraint store.
//

#pragma once

// Export definitions
#include "SemanticsExportDefs.hpp"

#include "CoreObject.hpp"

#include <sparsepp/spp.h>

// Forward declarations
namespace Core {
  class Constraint;
}//end namespace Core

namespace Semantics {
  
  class SEMANTICS_EXPORT_CLASS ConstraintRegister {
  public:
    using RegisterKey = Core::UUIDCoreObj;

  public:
    ConstraintRegister() = default;
    ~ConstraintRegister() = default;
    
    /// Removes all registered constraints from the register.
    /// @note use with caution, this can be an expensive operation
    void clearRegister();
    
    /// Removes registered and stored constraints from the register
    void deepClearRegister();
    
    /// Returns the number of registered constraints
    inline std::size_t getNumRegisteredConstraints() const { return pRegister.size(); }
    
    /// Returns true if the given key is a registered key, false otherwise
    inline bool isKeyRegistered(const RegisterKey& aKey) const { return pRegister.find(aKey) != pRegister.end(); }
    
    /// Registers the given constraint
    void registerConstraint(Core::Constraint* aConstraint);
    
    /// Similar to register constraint but, with store,
    /// the constraint can be removed only using a deep clear
    void storeConstraint(Core::Constraint* aConstraint);
    
    /// Returns the registered constraint given key.
    /// @note asserts if the key is not present
    Core::Constraint* getConstraint(const RegisterKey& aKey) const;
    
    /// Removes the registered constraints
    void removeConstraint(const RegisterKey& aKey);
    
    /// Removes stored constraint
    void removeStoredConstraint(const RegisterKey& aKey);
    
  private:
    /// Actual map for pairs "key - Constraint"
    spp::sparse_hash_map<RegisterKey, Core::Constraint*> pRegister;
    
    /// Unordered set for registered key
    spp::sparse_hash_set<RegisterKey> pKeyStore;
  };
  
} // end namespace Semantics
