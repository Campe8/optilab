//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 3/9/2017
//
// Object representing the state, i.e., a snapshot
// of a ConstraintStore object, used in Memento pattern.
//

#pragma once

// Export definitions
#include "SemanticsExportDefs.hpp"

#include "CoreObject.hpp"
#include "PropagationCost.hpp"

#include <sparsepp/spp.h>
#include <unordered_set>
#include <unordered_map>

namespace Semantics {
  
  class SEMANTICS_EXPORT_CLASS ConstraintStoreState {
  public:
    ConstraintStoreState(std::size_t aReevaluationCost,
      const spp::sparse_hash_set<Core::UUIDCoreObj>& aSubsumedConstraintSet,
      const spp::sparse_hash_map<Core::PropagationCost, spp::sparse_hash_set<Core::UUIDCoreObj>>& aReevaluationSet);
    
    virtual ~ConstraintStoreState() = default;
    
    inline std::size_t reevaluationCost() const
    {
      return pCurrentReevaluationCost;
    }

    inline const spp::sparse_hash_set<Core::UUIDCoreObj>& subsumedConstraintSet() const
    {
      return pSubsumedConstraintSet;
    }
    
    inline const spp::sparse_hash_map<Core::PropagationCost, spp::sparse_hash_set<Core::UUIDCoreObj>>& reevaluationSet() const
    {
      return pReevaluationSet;
    }
    
  private:
    /// Index on "pPropagationCostOrderVector" for the current set of
    /// constraints ready for reevaluation
    std::size_t pCurrentReevaluationCost;

    /// Set of subsumed constraints
    spp::sparse_hash_set<Core::UUIDCoreObj> pSubsumedConstraintSet;

    /// Set of constraints ready for reevaluation.
    /// The set is partitioned by the cost of each propagator in order
    /// to reevaluate constraints according to their cost.
    /// This set represents the "constraint queue"
    spp::sparse_hash_map<Core::PropagationCost, spp::sparse_hash_set<Core::UUIDCoreObj>> pReevaluationSet;
  };
  
} // end namespace Semantics
