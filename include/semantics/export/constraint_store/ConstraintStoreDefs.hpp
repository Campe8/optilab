//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 01/23/2016
//

#pragma once

namespace Semantics {
  
  enum StoreConsistencyStatus : int {
      CONSISTENCY_SUBSUMED = 0
    , CONSISTENCY_FAILED
    , CONSISTENCY_FIXPOINT
    , CONSISTENCY_NO_FIXPOINT
  };
  
} // end namespace Semantics

