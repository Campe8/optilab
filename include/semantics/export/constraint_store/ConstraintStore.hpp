//
// Copyright OptiLab 2016-2018. All rights reserved.
//
// Created by Federico Campeotto on 01/06/2017
//
// Base class for the sonstraint store.
// The constraint store is a set that contains
// the constraints that are currently assumed satisfiable.
// Together with the set of the variables to label, the 
// constraint store represents the semantics of a constraint model.
// In other words, the constraint store is the conjunction
// of constraints representing the semantic of the model at any 
// time during the search-labeling process.
//
// @note in general a constraint is registered in only one 
// constraint store at any given time.
// On the other hand, there can be multiple constraint stores
// running at any time during the search-labeling process.
//

#pragma once

#include "SemanticsExportDefs.hpp"
#include "ConstraintStoreDefs.hpp"

#include "CoreObject.hpp"
#include "PropagationCost.hpp"
#include "ConstraintDefs.hpp"
#include "BacktrackableObject.hpp"
#include "ConstraintRegister.hpp"

#include <cassert>
#include <utility>

// Forward declarations
namespace Core {
  class Constraint;
  class NogoodConstraint;
} // end namespace Core

namespace Semantics {
  class ConstraintStore;
  
  typedef ConstraintStore* ConstraintStorePtr;
  typedef std::shared_ptr<Semantics::ConstraintStore> ConstraintStoreSPtr;
} // end namespace Semantics

namespace Semantics {

  class SEMANTICS_EXPORT_CLASS ConstraintStore : public Search::BacktrackableObject {
  public:
    ConstraintStore();

    /// @note Shallow copies
    ConstraintStore(const ConstraintStore& aOther);

    /// @note Shallow copies
    ConstraintStore(ConstraintStore&& aOther);

    /// @note constraints are deregistered
    virtual ~ConstraintStore() = default;

    /// @note Shallow copies
    ConstraintStore& operator=(const ConstraintStore& aOther);

    /// @note Shallow copies
    ConstraintStore& operator=(ConstraintStore&& aOther);

    /// Returns the number of registered constraints
    inline std::size_t getNumRegisteredConstraints() const
    {
      return pConstraintRegister.getNumRegisteredConstraints();
    }
    
    /**
     * Runs the consistency process on the queue of constraints
     * scheduled for propagation.
     * It runs propagation on each registered constraint until
     * fix point or failure is reached.
     * Returns the event triggered by running consistency on this
     * constraint store:
     * - CONSISTENCY_SUBSUMED: if all constraints are subsumed
     * - CONSISTENCY_FIXPOINT: if all constraints reached the fix point
     * - CONSISTENCY_FAILED: if at least one constraint failed
     * - CONSISTENCY_FIXPOINT: if at least one constraints did not reach the fix point
     * @note returns CONSISTENCY_SUBSUMED if there are no registered constraints.
     *
     *       +-----------------+
     *       | Post Constraint |
     *       +-----------------+
     *                |
     *                |<--------------+
     *                |               |
     *     +----------------------+   |
     *     | Propagate Constraint |   |
     *     +----------------------+   |
     *                | re-evaluation |
     *                +---------------+
     *                |
     *          +-----------+
     *          | Fix-point |
     *          +-----------+
     *
     * Posting constraint may add constraints to the queue
     * of constraints to propagate.
     * For example, posting
     *    x = 2
     * may add 
     *    x != y
     * in the constraint queue.
     */
    virtual StoreConsistencyStatus runConsistency();
    
    /**
     * Runs the consistency process on the queue of constraints scheduled for propagation.
     * It runs propagation on each registered constraint until
     * there are no more constraints to propagate.
     * In particular, it does not stop if a failure is reached.
     * This means that different constraints may be unsatisfied.
     * It stores the number of unsatisfied constraints.
     * The number of unsatisfied constraints can be queried with the method
     * getNumUnsatConstraints();
     */
    virtual StoreConsistencyStatus runAggressiveConsistency();
    
    /**
     * Runs the consistency process on the queue of optimization constraints that are
     * scheduled for propagation. The behavior is similar to "runConsistency()" but the only
     * constraints considered in the constraint queue are optimization constraints,
     * i.e., constraints involving at least one optimization variable.
     * Returns a pair <StoreConsistencyStatus, fitness> where the fitness value is
     * Constraint::FitnessMin if StoreConsistencyStatus is not CONSISTENCY_FAILED.
     * Fitness is the fitness value of the failed constraint if StoreConsistencyStatus
     * is CONSISTENCY_FAILED.
     */
    virtual std::pair<StoreConsistencyStatus, double> runOptimizationConsistency();
    
    /// Returns the number of unsatisfied constraints after consistency
    inline std::size_t getNumUnsatConstraint() const { return pUnsatConstraintsCtr; }
    
    /*
     * Calculates the fitness value of constraints scheduled for propagation.
     * The fitness is calculated as the sum of the values returned by the fitness function
     * run on each constraint in the constraint queue.
     * The fitness value can be calculated only on constraints with a "ground" scope, i.e.,
     * on constraints that have all the variables in their scope (if any) that are all ground.
     * If the fitness value cannot be calculated, this method returns a negative value.
     * A fitness value of 0 means that each registered constraint is satisfied, i.e., its fitness
     * cannot be further "improved".
     * In other words, since the set of queue of constraints represents the semantic of a model,
     * this method returns the fitness of such semantic in a given configuration (an assignment
     * of the variables that are part of the constraint scope of the constraints in the queue
     * can be seen as a "configuration" or "state").
     * @note the complexity of this method is O(n) with n being the number of registered constraint.
     * @note this method does not remove constraints from the constraint queue.
     * @note the fitness value returned by this method is the sum of all fitness values
     * on all constraints in the fitness constraint queue
     */
    virtual double calculateSemanticFitness();
    
    /// Returns the fitness value calculated on "standard" constraints.
    /// @note here "standard" means all constraints except nogood constraints
    inline double getStdSemanticFitness() const { return pStdConstraintFitness; }
    
    /// Returns the fitness value calculated on nogood constraints
    inline double getNogoodSemanticFitness() const { return pNogoodConstraintFitness; }
    
    /// This method creates a snapshot of the current state of the constraint store (the semantics
    /// of the model), and saves it internally.
    /// @note this method is supposed to be used in pair with "uploadSemanticState()"
    void saveSemanticState();
    
    /// This method uploads a previously saved semantic state, reverting the current state of
    /// the constraints store to the saved snapshot.
    /// If there is no saved snapshot, this method returns.
    /// @note this method is supposed to be used in pair with "saveSemanticState()"
    void uploadSemanticState();
    
  protected:
    /// Constraint as a friend class
    friend Core::Constraint;
    friend Core::NogoodConstraint;
    
    /// Register "aConstraint" into this constraint store.
    /// "aConstraint" is added to the set of constraints to propagate
    /// during the consistency evaluation process.
    /// @note automatically reevaluates "aConstraint"
    void registerConstraint(Core::Constraint* aConstraint);
    
    /// Store "aConstraint" into this constraint store.
    /// "aConstraint" is added to the set of constraints to propagate
    /// during the consistency evaluation process.
    /// @note automatically reevaluates "aConstraint"
    /// @note the life-time management of this constraint is
    /// different from the life-time management of registered constraints.
    /// Removing this constraint from the constraint store must be
    /// explicitly called using the removeStoredConstraint method.
    /// It doesn't follow backtracking rules for de-registration
    void storeConstraint(Core::Constraint* aConstraint);
    
    /// De-register "aConstraint" from this constraint store.
    void deregisterConstraint(Core::Constraint* aConstraint);
    
    /// Removes a stored constraint
    void removeStoredConstraint(Core::Constraint* aConstraint);
    
    /// Reevaluate "aConstraint" due to some changes happened on the
    /// domains of the variables in the scope of "aConstraint".
    /// This method is called by constraints after being notified
    /// by their subjects about changes in the respective domains
    void reevaluateConstraint(Core::Constraint* aConstraint);
    
    /// Returns the next constraint in the reevaluation set ready
    /// for reevaluation, nullptr otherwise.
    /// @note it does not return subsumed constraints
    Core::Constraint* getNextConstraintToReevaluate();
    
    /// Creates a snapshot of the object in the current state.
    /// @note snapshot is a MementoState object
    std::unique_ptr<Search::MementoState> snapshot() override;
    
    /// Reinstate a previous snapshot of the object.
    /// @note snapshot is a MementoState object
    void reinstateSnapshot(const Search::MementoState* aSnapshot) override;
    
    /// Resets the counter of unsatisfied constraints
    inline void resetUnsatConstraintsCtr() { pUnsatConstraintsCtr = 0; }
    
    /// Adds "aNumUnsat" unsatisfied constraint to the unsat constraint counter
    inline void addUnsatConstraints(std::size_t aNumUnsat=1) { pUnsatConstraintsCtr += aNumUnsat; }
    
  private:
    std::size_t pUnsatConstraintsCtr = 0;
    
    /// Fitness value for standard constraints
    double pStdConstraintFitness = 0;
    
    /// Fitness value for no-good constraints
    double pNogoodConstraintFitness = 0;
    
    /// Map of constraints currently registered in this constraint store
    ConstraintRegister pConstraintRegister;

    /// Snapshot of the state of this instance
    std::shared_ptr<Search::MementoState> pStateSnapshot;
    
    /// Returns true if "aConstraint" is registered in this constraint store,
    /// false otherwise
    bool isConstraintRegistered(Core::Constraint* aConstraint) const;
    
    /// Returns true if "aConstraint" id is registered in this constraint store,
    /// false otherwise
    inline bool isConstraintRegistered(const Core::UUIDCoreObj& aUUIDCoreObj) const
    {
      return pConstraintRegister.isKeyRegistered(aUUIDCoreObj);
    }
    
    /// Index on "pPropagationCostOrderVector" for the current set of
    /// constraints ready for reevaluation
    std::size_t pCurrentReevaluationCost;
    
    /// Sorted vector of propagation costs
    std::vector<Core::PropagationCost> pPropagationCostOrderVector;
    
    inline void resetReevaluationCost() { pCurrentReevaluationCost = 0; }
    
    /// Initialiazes the vector of propagation costs
    void initPropagationCostOrderVector(std::vector<Core::PropagationCost>& aCostVector);
    
    /// Set of subsumed constraints
    spp::sparse_hash_set<Core::UUIDCoreObj> pSubsumedConstraintSet;
    
    /// Set of constraints yet to post
    spp::sparse_hash_set<Core::UUIDCoreObj> pPostConstraintSet;
    
    /// Returns true if "aUUIDCoreObj" is an id of a subsumed constraint,
    /// false otherwise
    inline bool isConstraintSubsumed(const Core::UUIDCoreObj& aUUIDCoreObj) const
    {
      return pSubsumedConstraintSet.find(aUUIDCoreObj) != pSubsumedConstraintSet.end();
    }
    
    /// Set of constraints ready for reevaluation.
    /// The set is partitioned by the cost of each propagator in order
    /// to reevaluate constraints according to their cost.
    /// This set represents the "constraint queue"
    spp::sparse_hash_map<Core::PropagationCost, spp::sparse_hash_set<Core::UUIDCoreObj>> pReevaluationSet;
    
    /// Returns true if there are more constraints to reevaluate of "aCurrentCost"
    /// index cost in pPropagationCostOrderVector
    inline bool moreConstraintToReevaluateGivenCost(std::size_t aCurrentCost) const
    {
      if(pReevaluationSet.find(pPropagationCostOrderVector[aCurrentCost]) != pReevaluationSet.end())
      {
        return !pReevaluationSet.at(pPropagationCostOrderVector[aCurrentCost]).empty();
      }
      return false;
    }
    
    /// Post all constraints registered into this constraint store.
    /// It runs post on each registered constraint and removes from
    /// register all the subsumed constraint.
    /// Returns the event triggered by running post on this
    /// constraint store:
    /// - CONSISTENCY_SUBSUMED: if all constraints are subsumed
    /// - CONSISTENCY_FAILED: if at least one constraint failed
    /// - CONSISTENCY_FIXPOINT: all other cases
    virtual StoreConsistencyStatus postConstraints();
    
    /// Runs an aggresive post constraint on all constraints in the queue.
    /// Returns the IDs of the failed constraints during post
    virtual spp::sparse_hash_set<Core::UUIDCoreObj> aggressivePostConstraints();
    
    /// Post all optimization constraints registered into this constraint store.
    /// It runs post on each registered optimization constraint and removes from
    /// register all the subsumed constraint.
    /// Returns a pair with first the event triggered by running post on this
    /// constraint store:
    /// - CONSISTENCY_SUBSUMED: if all constraints are subsumed
    /// - CONSISTENCY_FAILED: if at least one constraint failed
    /// - CONSISTENCY_FIXPOINT: all other cases
    /// and second the fitness value of the constraint that caused CONSISTENCY_FAILED, if any.
    /// If the consistency is not failed, second is Constraint::FitnessMin
    virtual std::pair<StoreConsistencyStatus, double> postOptimizationConstraints();
    
    /// Reverts subsumed constraints to "aSubsumedConstraints"
    void revertSubsumedConstraints(const spp::sparse_hash_set<Core::UUIDCoreObj>& aSubsumedConstraints);
    
    /// Returns the list of keys of all constraints in the constraint queue, sorted by constraints
    /// with lower cost
    std::vector<ConstraintRegister::RegisterKey> getConstraintQueueKeys();
    
    /// Clean internal status
    void cleanStatus();
  };

} // end namespace Semantics
