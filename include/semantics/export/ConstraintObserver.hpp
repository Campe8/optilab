//
// Copyright OptiLab 2016-2017. All rights reserved.
//
// Created by Federico Campeotto on 01/06/2017
//
// Observer patter used for linking domain
// events triggered by propagation to the constraints
// domains are involved in.
//

#pragma once

#include "CoreObject.hpp"
#include "ConstraintDefs.hpp"

#include <set>
#include <cassert>

// Forward declarations.
namespace Core {
  // Events
  class DomainEvent;
  // Constraint
  class Constraint;
} // end namespace Core

namespace Core {
  class ConstraintObserverEvent {
  public:
    enum ConstraintObserverPropagationStatus : int {
      CON_EVT_PROPAGATION_INIT = 0
      , CON_EVT_PROPAGATION_TERM
    };
    
    /// Constructor: "aConstraint" is the constraint generating
    /// this ConstraintObserverEvent
    ConstraintObserverEvent(const Constraint* aConstraint);
    
    ~ConstraintObserverEvent();
    
    /// Returns the owner of this event
    inline Core::UUIDCoreObj getEventOwner() const
    {
      return pConstraintObserverID;
    }
    
    inline void setConstraintPropagationStatus(ConstraintObserverPropagationStatus aPropagationStatus)
    {
      pPropagationStatus = aPropagationStatus;
    }
    
    inline void setConstraintPropagationEvent(PropagationEvent aPropagationEvent)
    {
      pPropagationEvent = aPropagationEvent;
    }
    
    inline ConstraintObserverPropagationStatus constraintPropagationStatus() const
    {
      return pPropagationStatus;
    }
    
    inline PropagationEvent constraintPropagationEvent() const
    {
      return pPropagationEvent;
    }
    
  private:
    Core::UUIDCoreObj pConstraintObserverID;
    
    /// Status of propagation
    ConstraintObserverPropagationStatus pPropagationStatus;
    
    /// Event occurred during constraint propagation
    PropagationEvent pPropagationEvent;
  };
}// end namespace Core

namespace Semantics {
  
  class ConstraintObserver {
  public:
    ConstraintObserver(Core::UUIDCoreObj aConstraintID);
    
    virtual ~ConstraintObserver();
    
    inline Core::UUIDCoreObj getConstraintObserverID() const
    {
      return pConstraintID;
    }
    
    /// Returns true if "aConstraintObserverEvent" requires to be notified to this ConstraintObserver,
    /// false otherwise
    virtual bool requiresNotification(const Core::ConstraintObserverEvent* aConstraintObserverEvent) = 0;
    
    /// Update the Domain
    virtual void updateOnDomainEvent(Core::DomainEvent* aDomainEvent) = 0;
    
  private:
    Core::UUIDCoreObj pConstraintID;
  };
  
  class ConstraintSubject {
  public:
    virtual ~ConstraintSubject();
    
    /// Attach a constraint observer, modifications on this subject
    /// will be notified to the set of attached observers
    inline virtual void attachConstraintObserver(ConstraintObserver *aConstraintObserver)
    {
      assert(aConstraintObserver);
      pConstraintObserverSet.insert(aConstraintObserver);
    }
    
    /// Detach "aConstraintObserver" from the set of observers
    inline virtual void detachConstraintObserver(ConstraintObserver *aConstraintObserver)
    {
      assert(aConstraintObserver);
      pConstraintObserverSet.erase(aConstraintObserver);
    }
    
    /// Update internal status or notify observers
    /// according to the type of "aConstraintEvent"
    virtual void updateOnConstraintEvent(const Core::ConstraintObserverEvent* aConstraintEvent);
    
  protected:
    virtual void triggerActionBasedOnEvent(ConstraintObserver* aConstraintObserver,
                                           const Core::ConstraintObserverEvent* aConstraintEvent) = 0;
    
  private:
    std::set<ConstraintObserver*> pConstraintObserverSet;
  };
  
} // end namespace Semantics
