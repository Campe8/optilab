//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/15/2016
//
// Semantic for decision variables.
//

#pragma once

// Export definitions
#include "VariableExportDefs.hpp"

// Types and macros
#include "VariableDefs.hpp"

// Base class
#include "VariableSemantic.hpp"

#include <cassert>
#include <string>
#include <memory>

namespace Core {
  
  class CORE_VARIABLE_EXPORT_CLASS VariableSemanticDecision : public VariableSemantic {
  public:
    /// Constructor: if aVariableName is empty, sets a random identifier
    /// with prefix 'D'
    VariableSemanticDecision(const std::string& aVariableName="");
    
    VariableSemantic* clone() override;
    
    static bool isa(const VariableSemantic* aSemantic);
    static VariableSemanticDecision* cast(const VariableSemantic* aSemantic);
    
    /// Returns the variable's name identifier
    inline std::string getVariableNameIdentifier() const override
    {
      return pVariableStringNameIdentifier;
    }
    
    /// Sets variable solution element flag
    inline void setAsSolutionElement(bool aSol)
    {
      pSolutionElement = aSol;
    }
    
    /// Solution element.
    /// @note for branching variables the default is true
    inline bool isSolutionElement() const override
    {
      return pSolutionElement;
    }
    
  protected:
    /// Returns the specified branching semantic policy
    inline bool getBranchingPolicy(bool aBranchingSemanticPolicy) const override
    {
      return aBranchingSemanticPolicy;
    }
    
  private:
    /// Solution element flag
    bool pSolutionElement;
    
    //// Variable's string name identifier
    mutable std::string pVariableStringNameIdentifier;
  };
  
} // end namespace Core
