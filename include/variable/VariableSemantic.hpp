//
// Copyright OptiLab 2016-2017. All rights reserved.
//
// Created by Federico Campeotto on 10/25/2016
//
// VariableSemantic represents the semantic meaning and class
// of a variable.
// For example, it contains all the high level information about
// the variable such as its (string) name identifier,
// or its class type.
//

#pragma once

// Export definitions
#include "VariableExportDefs.hpp"

// Types and macros
#include "VariableDefs.hpp"

#include "BranchingStrategyDefs.hpp"

#include <boost/logic/tribool.hpp>
#include <string>
#include <memory>

// Forward declarations
namespace Core {
  class VariableSemantic;
  using VariableSemanticUPtr = std::unique_ptr<VariableSemantic>;
  using VariableSemanticSPtr = std::shared_ptr<VariableSemantic>;
}// end namespace Core

namespace Core {
  
  class CORE_VARIABLE_EXPORT_CLASS VariableSemantic {
  public:
    virtual ~VariableSemantic() = default;
    
    /// Clone this instance
    virtual VariableSemantic* clone() = 0;
    
    inline VariableSemanticClassType getSemanticClassType() const
    {
      return pVariableSemanticClassType;
    }
    
    /// Set the branching semantic policy on the variable associated with this semantic
    void setBranchingPolicy(bool aBranchingSemanticPolicy);
    
    /// Set reachability, either it is a branching variable
    /// or it is a variable that can be reached from a branching variable
    /// in the constraint graph
    void setReachability(bool aReachability);
    
    inline bool isBranching() const
    {
      return  pBranchingPolicy;
    }
    
    /// Set input order value.
    /// @note this is the order the variables are defined
    /// in the model and it may be used by search heuristics
    void setInputOrder(std::size_t aOrderValue);
    
    inline std::size_t getInputOrder() const
    {
      return pInputOrder;
    }
    
    inline bool hasInputOrder() const
    {
      return pInputOrderDef;
    }
    
    /// Set the branching strategy to use when
    /// the semantic has a branching policy.
    /// @note default is BRANCHING_BINARY_CHOICE_POINT
    inline void setBranchingStrategyType(Search::BranchingStrategyType aBranchingStrategyType)
    {
      pBranchingStrategyType = aBranchingStrategyType;
    }
    
    inline Search::BranchingStrategyType getBranchingStrategyType() const
    {
      return pBranchingStrategyType;
    }
    
    /// Returns true if the variable is reachable, i.e.,
    /// the constraint graph connects the variable this semantic
    /// belongs to to any branching variable.
    /// @note branching variables are reachable by definition
    bool isReachable() const;
    
    /// Force on/off output of this variable w.r.t.
    /// the given input flag
    void forceOutput(bool aOutput);
    
    /// Returns true if the variable owning this semantic
    /// is forced to be an output variable
    /// Returns false otherwise.
    /// If no option is selected, it returns indeterminate
    virtual boost::logic::tribool isOutput() const;
    
    /// Returns true if the variable owning this semantic
    /// is a solution component, i.e., if it belongs to
    /// the set of variables that are part of a solution.
    /// Returns false otherwise
    virtual bool isSolutionElement() const = 0;
    
    /// Returns the variable's name identifier
    virtual std::string getVariableNameIdentifier() const = 0;
    
  protected:
    VariableSemantic(VariableSemanticClassType aVarSemanticClassType);
    
    /// Returns branching policy according to the specific semantic implementation
    virtual bool getBranchingPolicy(bool aBranchingSemanticPolicy) const = 0;
    
  private:
    /// Branching strategy
    Search::BranchingStrategyType pBranchingStrategyType;
    
    /// Semantic class type
    VariableSemanticClassType pVariableSemanticClassType;
    
    /// Bool output flag
    boost::logic::tribool pOutput;
    
    /// Branching semantic policy
    bool pBranchingPolicy;
    
    /// Reachability flag
    bool pReachability;
    
    /// Input order defined flag
    bool pInputOrderDef;
    
    /// Input order value
    std::size_t pInputOrder;
  };
  
} // end namespace Core
