//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 01/27/2017
//

#pragma once

// Export definitions
#include "VariableExportDefs.hpp"

// Types and macros
#include "DomainDefs.hpp"
#include "VariableDefs.hpp"

#include "Variable.hpp"

#include <vector>
#include <unordered_set>

// Forward declarations
namespace Core {
  class DomainElementInt64;
  
  class VariableFactory;
  typedef VariableFactory* VariableFactoryPtr;
  typedef std::unique_ptr<VariableFactory> VariableFactoryUPtr;
  typedef std::shared_ptr<VariableFactory> VariableFactorySPtr;
} // end namespace Core

namespace Core {
  
  class CORE_VARIABLE_EXPORT_CLASS VariableFactory {
  public:
    VariableFactory();
    
    virtual ~VariableFactory();
    
    /// VariableInteger given INT64 values
    Variable* variableInteger(INT_64 aSingleton, VariableSemanticUPtr aVariableSemantic);
    Variable* variableInteger(INT_64 aLowerBound, INT_64 aUpperBound, VariableSemanticUPtr aVariableSemantic);
    Variable* variableInteger(const std::unordered_set<INT_64>& aSetOfElements, VariableSemanticUPtr aVariableSemantic);
    
    /// VariableInteger given domain elements
    Variable* variableInteger(DomainElementInt64 *aSingleton, VariableSemanticUPtr aVariableSemantic);
    Variable* variableInteger(DomainElementInt64 *aLowerBound, DomainElementInt64 *aUpperBound, VariableSemanticUPtr aVariableSemantic);
    Variable* variableInteger(const std::unordered_set<DomainElementInt64 *>& aSetOfElements, VariableSemanticUPtr aVariableSemantic);
    
    /// Variable Boolean
    Variable* variableBoolean(VariableSemanticUPtr aVariableSemantic);
    Variable* variableBoolean(bool aSingleton, VariableSemanticUPtr aVariableSemantic);
    
    /// Variable Array
    Variable* variableArray(const std::vector<VariableSPtr>& aVarArray, VariableSemanticUPtr aVariableSemantic);
    Variable* variableArray(std::size_t aNumCells, VariableSemanticUPtr aVariableSemantic);
    
    /// Variable matrix 2D
    /// @note all columns must have the same size
    Variable* variableMatrix2D(const std::vector<std::vector<VariableSPtr>>& aVarArray, VariableSemanticUPtr aVariableSemantic);
    Variable* variableMatrix2D(std::size_t aNumRows, std::size_t aNumCols, VariableSemanticUPtr aVariableSemantic);
  };
  
} // end namespace Core
