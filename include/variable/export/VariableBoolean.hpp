//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/15/2016
//
// Variable Boolean
// @note it is not derived from VariableInteger since
// it may be useful to instantiate a DomainBoolean.
//

#pragma once

// Export definitions
#include "VariableExportDefs.hpp"

// Types and macros
#include "VariableDefs.hpp"

// Base class
#include "Variable.hpp"

#include <memory>
#include <unordered_set>

// Forward declarations
namespace Core {
  class DomainBoolean;
  class DomainElementInt64;
}

namespace Core {
  
  class CORE_VARIABLE_EXPORT_CLASS VariableBoolean : public Variable {
  public:
    /// Copy constructor
    VariableBoolean(const VariableBoolean& aOther);
    
    /// Move constructor
    VariableBoolean(VariableBoolean&& aOther);
    
    virtual ~VariableBoolean();
    
    /// Assignment operator
    VariableBoolean& operator= (const VariableBoolean& aOther);
    
    /// Move assignment operator
    VariableBoolean& operator= (VariableBoolean&& aOther);
    
    static bool isa(const Variable* aVariable)
    {
      assert(aVariable);
      assert(aVariable->getType());
      return aVariable->getType()->getTypeClass() == VariableTypeClass::VAR_TYPE_BOOLEAN;
    }
    
    static VariableBoolean* cast(const Variable* aVariable)
    {
      if(!isa(aVariable))
      {
        return nullptr;
      }
      return static_cast<VariableBoolean*>(const_cast<Variable*>(aVariable));
    }
    
    bool isAssigned() const override;
    
    inline const DomainListPtr domainList() const override
    {
      return &pDomainList;
    }
    
  protected:
    friend class VariableFactory;
    
    /// Create a variable Boolean with domain [false, true]
    VariableBoolean(VariableSemanticUPtr aVariableSemantic);
    
  private:
    std::shared_ptr<DomainBoolean> pDomain;
    
    /// Domain list
    mutable DomainList pDomainList;
  };
  
} // end namespace Core
