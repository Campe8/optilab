//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/17/2016
//
// Variable Array, i.e. a (static) array of Variables.
// Variables must be of base types:
// - VariableBoolean
// - VariableInteger
//

#pragma once

// Export definitions
#include "VariableExportDefs.hpp"

// Types and macros
#include "VariableDefs.hpp"

// Base class
#include "VariableMatrix.hpp"

// Boost array def.
#include "boost/multi_array.hpp"

namespace Core {
  
  class CORE_VARIABLE_EXPORT_CLASS VariableArray : public VariableMatrix {
  public:
    virtual ~VariableArray() = default;
    
    static bool isa(const Variable* aVariable);
    static VariableArray* cast(const Variable* aVariable);
    
    /// Return the number of dimensions of the matrix
    inline std::size_t numberOfDimensions() const override { return pVariableArray.num_dimensions(); }
    
    /// Returns the (total) number of elements contained in the matrix
    inline std::size_t size() const override { return pVariableArray.num_elements(); }
    
    ///  Assigns "aVarToAssign" to the array cell ["aCell"].
    /// @note "aVarToAssign" must be either a nullptr or a variable of primitive type, otherwise throws logic_error.
    /// @note throws out of range exception if "aCell" does not match array dimensions
    void assignVariableToCell(std::size_t aCell,  VariableSPtr aVarToAssign);
    
    /// Returns the pointer to the variable stored in ["aCell"]
    VariableSPtr at(std::size_t aCell) const;
    
    /// Returns a reference to the "idx"^th column
    inline VariableSPtr operator[](std::size_t idx) const { return pVariableArray[idx]; }
    
    /// Returns true if all the variables in the matrix are assigned,
    /// false otherwise
    bool isAssigned() const override;
    
    const DomainListPtr domainList() const override;
    
  protected:
    friend class VariableFactory;
    
    /// Constructor: creates an empty array of size "aNumCells".
    /// Constructor used only inside VariableFactory
    VariableArray(std::size_t aNumCells, VariableSemanticUPtr aVariableSemantic);
    
    /// Upload the semantic on each array variable
    void uploadSemanticToDimensions(const VariableSemanticSPtr& aSemantic) override;
    
  private:
    typedef boost::multi_array<VariableSPtr, 1> arrayType;
    
    /// Actual data structure
    arrayType pVariableArray;
    
    /// Initialize matrix with nullptr
    void initArray();
    
    /// Flag set everytime list of domains is changed
    mutable bool pDomainListMod;
    /// Temporary domain list used to store current domains
    mutable DomainList pDomainList;
    
  };
  
} // end namespace Core
