//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/15/2016
//
// Variable Matrix, i.e. a (static) matrix of Variables.
// Variables must be of base types:
// - VariableBoolean
// - VariableInteger
//

#pragma once

// Export definitions
#include "VariableExportDefs.hpp"

// Types and macros
#include "VariableDefs.hpp"

// Base class
#include "Variable.hpp"

namespace Core {
  
  class CORE_VARIABLE_EXPORT_CLASS VariableMatrix : public Variable {
  public:
    virtual ~VariableMatrix() = default;
    
    /// Return the number of dimensions of the matrix
    virtual std::size_t numberOfDimensions() const = 0;
    
    /// Returns the (total) number of elements contained in the matrix
    virtual std::size_t size() const = 0;
    
    /// Override the semantic method, first calling the uploadSeamantic
    /// of the base class, then setting the semantic to each element
    /// of the composite/matrix variable
    void uploadSemantic(const VariableSemanticSPtr& aSemantic) override;
    
  protected:
    /// Constructor
    VariableMatrix(VariableSemanticUPtr aVariableSemantic);
    
    /// Upload the given semantic to each element of each dimension
    virtual void uploadSemanticToDimensions(const VariableSemanticSPtr& aSemantic) = 0;
  };
  
} // end namespace Core
