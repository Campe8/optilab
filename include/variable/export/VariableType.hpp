//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 01/26/2017
//


#pragma once

// Export definitions
#include "VariableExportDefs.hpp"

// Types and macros
#include "VariableDefs.hpp"

#include <memory>

// Forward declarations
namespace Core {
  class VariableType;
  
  typedef std::unique_ptr<VariableType> VariableTypeUPtr;
  typedef std::shared_ptr<VariableType> VariableTypeSPtr;
}// end namespace Core

namespace Core {
  
  class CORE_VARIABLE_EXPORT_CLASS VariableType {
  public:
    VariableType(VariableTypeClass aVarTypeClass);
    
    virtual ~VariableType();

    inline VariableTypeClass getTypeClass() const
    {
      return pVariableTypeClass;
    }
    
    /// Returns true if this is a primitive data type,
    /// e.g., VariableBoolean, VariableInteger, etc.
    /// False otherwise, e.g., VariableMatrix2D
    bool isPrimitiveType() const;
    
  private:
    /// Semantic class this variables belongs to
    VariableTypeClass pVariableTypeClass;
  };
  
} // end namespace Core
