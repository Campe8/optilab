//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/09/2016
//

#pragma once

// Export definitions
#include "VariableExportDefs.hpp"

// Types and macros
#include "VariableDefs.hpp"

// Base class
#include "CoreObject.hpp"

// Variable type
#include "VariableType.hpp"

// Variable semantic
#include "VariableSemantic.hpp"

#include <vector>
#include <memory>

// UUID generators
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>

// Forward declarations
namespace Core {
  class Domain;
  class DomainEvent;
  class DomainElement;
  
  typedef std::vector<std::shared_ptr<Domain>> DomainList;
  typedef DomainList* DomainListPtr;
  
  class Variable;
  class VariableArray;
  class VariableMatrix2D;
  typedef Variable* VariablePtr;
  typedef std::unique_ptr<Variable> VariableUPtr;
  typedef std::shared_ptr<Variable> VariableSPtr;
  
  class VariableFactory;
  
  class Constraint;
  class BranchingConstraint;
}// end namespace Core

namespace Search {
  class SearchHeuristic;
}// end namespace Search

namespace Core {
  
  /**
   * Class Variable is the base class for the variables in the model.
   * The architectural structure of variables is as follows:
   *
   *                   Variable      <--- has_a ---      VariableSemantic
   *                   /     \                                      |
   *             is_a /       \ is_a                                | is_a
   *                 /         \                                    |
   *       - VariableBoolean  VariableArray (Composite)  - VariableSemanticSupport
   *       - VariableInteger  VariableMatrix (Composite) - VariableSemanticDecision
   *       - VariableReal                                - VariableSemanticObjective
   */
  
  class CORE_VARIABLE_EXPORT_CLASS Variable : public CoreObject {
  public:
    virtual ~Variable();
    
    /// Returns true if the Variable has a primitive data type,
    /// e.g., VariableBoolean, VariableInteger, etc.
    /// False otherwise, e.g., VariableMatrix2D
    inline bool hasPrimitiveType() const
    {
      assert(pVariableType);
      return pVariableType->isPrimitiveType();
    }
    
    /// Returns degree, i.e., the number of subscribed constraints
    std::size_t getDegree() const;
    
    /// Returns the pointer to the semantic the variable
    /// was instantiated upon, i.e., the semantic before any upload
    VariableSemantic* variableBaseSemantic() const
    {
      // Use lazy initialization pattern
      return pBaseVariableSemantic ? pBaseVariableSemantic.get() : pVariableSemantic.get();
    }
    
    /// Returns the pointer to the semantic of the variable
    VariableSemantic* variableSemantic() const
    {
      return pVariableSemantic.get();
    }
    
    /// Uploads "aSemantic" as current variable semantic.
    /// @note base properties (e.g., name) of the semantic to upload
    /// must be equal to the original semantic.
    /// This method is virtual since subclasses of the class variable,
    /// e.g., matrix variables, may want to set the semantic for each
    /// sub-variable composing the matrix
    virtual void uploadSemantic(const VariableSemanticSPtr& aSemantic);
    
    /// Returns the variable's name identifier
    inline std::string getVariableNameId() const
    {
      return pVariableSemantic->getVariableNameIdentifier();
    }
    
    inline void setType(VariableType* aVarType)
    {
      pVariableType = aVarType;
    }
    
    /// Returns the type of this variable
    inline VariableType* getType() const
    {
      return pVariableType;
    }
    
    /// Returns true if the variables is assigned, false otherwise
    virtual bool isAssigned() const = 0;
    
    /// Returns the list of domains paired with this variable
    virtual const DomainListPtr domainList() const = 0;
    
    /// Returns size of domainlist
    inline std::size_t domainListSize() const
    {
      return domainList()->size();
    }
    
    /// Return indexed domain from domain list.
    /// @note throws if index is out of bounds.
    /// @note returns first domain by default
    inline const std::shared_ptr<Domain> domain(std::size_t idx = 0)
    {
      return domainList()->at(idx);
    }
    
  protected:
    Variable(VariableSemanticUPtr aVariableSemantic);
    
    Variable(const Variable& aOther);
    
    Variable(Variable&& aOther);
    
    Variable& operator= (const Variable& aOther);
    
    Variable& operator= (Variable&& aOther);
    
  private:
    /// Semantic class this variables belongs to
    VariableSemanticUPtr pVariableSemantic;
    
    /// Copy of the semantic to preserve the original
    /// semantic before any update
    VariableSemanticUPtr pBaseVariableSemantic;
    
    VariableType* pVariableType;
  };
  
} // end namespace Core
