//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/09/2016
//

#pragma once

// Export definitions
#include "VariableExportDefs.hpp"

// Types and macros
#include "VariableDefs.hpp"

// Base class
#include "Variable.hpp"

#include <memory>
#include <unordered_set>

// Forward declarations
namespace Core {
  class DomainInteger;
  class DomainElementInt64;
}

namespace Core {
  
  class CORE_VARIABLE_EXPORT_CLASS VariableInteger : public Variable {
  public:
    /// Copy constructor
    VariableInteger(const VariableInteger& aOther);
    
    /// Move constructor
    VariableInteger(VariableInteger&& aOther);
    
    virtual ~VariableInteger();
    
    /// Assignment operator
    VariableInteger& operator= (const VariableInteger& aOther);
    
    /// Move assignment operator
    VariableInteger& operator= (VariableInteger&& aOther);
    
    static bool isa(const Variable* aVariable)
    {
      assert(aVariable);
      assert(aVariable->getType());
      
      return aVariable->getType()->getTypeClass() == VariableTypeClass::VAR_TYPE_INTEGER;
    }
    
    static VariableInteger* cast(const Variable* aVariable)
    {
      if(!isa(aVariable))
      {
        return nullptr;
      }
  
      return static_cast<VariableInteger*>(const_cast<Variable*>(aVariable));
    }
    
    bool isAssigned() const override;
    
    inline const DomainListPtr domainList() const override
    {
      return &pDomainList;
    }
    
  protected:
    friend class VariableFactory;
    
    /// Create a variable integer with domain ["aLowerBound".."aUpperBound"]
    VariableInteger(DomainElementInt64 *aLowerBound,
                    DomainElementInt64 *aUpperBound,
                    VariableSemanticUPtr aVariableSemantic);
    
    /// Create a variable integer with domain { x | x in "aSetOfElements" }
    VariableInteger(const std::unordered_set<DomainElementInt64 *>& aSetOfElements,
                    VariableSemanticUPtr aVariableSemantic);
    
  private:
    std::shared_ptr<DomainInteger> pDomain;
    
    mutable DomainList pDomainList;
  };
  
} // end namespace Core
