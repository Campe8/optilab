//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/09/2016
//

#pragma once

namespace Core {
  
  enum VariableTypeClass : int {
      VAR_TYPE_BOOLEAN = 0
    , VAR_TYPE_INTEGER
    , VAR_TYPE_REAL
    , VAR_TYPE_ARRAY
    , VAR_TYPE_MATRIX
    , VAR_TYPE_MATRIX2D
    , VAR_TYPE_UNDEF
  };
  
  enum VariableSemanticClassType : int {
      VAR_SEMANTIC_CLASS_DECISION = 0
    , VAR_SEMANTIC_CLASS_SUPPORT
    , VAR_SEMANTIC_CLASS_OBJECTIVE
    , VAR_SEMANTIC_CLASS_UNDEF
  };
  
} // end namespace Core
