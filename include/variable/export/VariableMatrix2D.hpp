//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/16/2016
//
// Variable 2D Matrix, i.e. a (static) 2D matrix of Variables.
// Variables must be of base types:
// - VariableBoolean
// - VariableInteger
//

#pragma once

// Export definitions
#include "VariableExportDefs.hpp"

// Types and macros
#include "VariableDefs.hpp"

// Base class
#include "VariableMatrix.hpp"

// Boost multi-array data structure for matrices
#include "boost/multi_array.hpp"

namespace Core {
  
  class CORE_VARIABLE_EXPORT_CLASS VariableMatrix2D : public VariableMatrix {
  public:
    virtual ~VariableMatrix2D() = default;
    
    static bool isa(const Variable* aVariable);
    static VariableMatrix2D* cast(const Variable* aVariable);
    
    /// Return the number of dimensions of the matrix
    inline std::size_t numberOfDimensions() const override { return pVariableMatrix.num_dimensions(); }
    
    inline std::size_t numRows() const { return pNumRows; }
    inline std::size_t numCols() const { return pNumCols; }
    
    /// Returns the (total) number of elements contained in the matrix
    inline std::size_t size() const override { return pVariableMatrix.num_elements(); }
    
    ///  Assigns "aVarToAssign" to the matrix cell ["aRowIdx", "aColIdx"].
    /// @note "aVarToAssign" must be either a nullptr or a variable of primitive type, otherwise throws logic_error.
    /// @note throws out of range exception if "aRowIdx" or "aColIdx" do not match matrix dimensions
    void assignVariableToCell(std::size_t aRowIdx, std::size_t aColIdx, VariableSPtr aVarToAssign);
    
    /// Returns the pointer to the variable stored in ["aRowIdx", "aColIdx"]
    VariableSPtr at(std::size_t aRowIdx, std::size_t aColIdx) const;
    
    /// Returns a reference to the "idx"^th column
    inline boost::multi_array<VariableSPtr, 1> operator[](std::size_t idx) const { return pVariableMatrix[idx]; }
    
    /// Returns true if all the variables in the matrix are assigned,
    /// false otherwise
    bool isAssigned() const override;
    
    const DomainListPtr domainList() const override;
    
  protected:
    friend class VariableFactory;
    
    /// Constructor: creates an empty matrix of size
    /// "aNumRows" x "aNumCols"
    VariableMatrix2D(std::size_t aNumRows, std::size_t aNumCols,
                     VariableSemanticUPtr aVariableSemantic);
    
    /// Upload the semantic on each element of the variable
    void uploadSemanticToDimensions(const VariableSemanticSPtr& aSemantic) override;
    
  private:
    typedef boost::multi_array<VariableSPtr, 2> matrix2Dtype;
    
    /// Number of rows
    std::size_t pNumRows;
    
    /// Number of columns
    std::size_t pNumCols;
    
    /// Actual data structure
    matrix2Dtype pVariableMatrix;
    
    /// Initialize matrix with nullptr
    void initMatrix();
    
    /// Flag set everytime list of domains is changed
    mutable bool pDomainListMod;
    /// Temporary domain list used to store current domains
    mutable DomainList pDomainList;
  };
  
} // end namespace Core
