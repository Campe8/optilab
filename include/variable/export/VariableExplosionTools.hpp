//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 01/25/2016
//
// Utility to explode variables that don't have primitive type.
//

#pragma once

#include "Variable.hpp"

#include <vector>

namespace Core { namespace Tools {
  
  /// Explode all variables in "aVarSet" and returns the list of exploded variables
  std::vector<Core::VariableSPtr> explodeVariables(const std::vector<Core::VariableSPtr>& aVarSet);
  
  /// Explode a single variable
  std::vector<Core::VariableSPtr> explodeVariable(const VariableSPtr& aVar);
  
}} // end namespace Tools end namespace Core
