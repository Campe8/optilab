//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/15/2016
//

#pragma once

// Export definitions
#include "VariableExportDefs.hpp"

// Types and macros
#include "VariableDefs.hpp"

// Base class
#include "VariableSemantic.hpp"

#include <cassert>
#include <string>
#include <memory>

namespace Core {
  
  class CORE_VARIABLE_EXPORT_CLASS VariableSemanticObjective : public VariableSemantic {
  public:
    enum ObjectiveExtremum : int
    {
        OBJ_EXT_MINIMIZE = 0
      , OBJ_EXT_MAXIMIZE
      , OBJ_EXT_UNDEF
    };
    
    /// Constructor: if aVariableName is empty, sets a random identifier
    /// with prefix 'O'
    VariableSemanticObjective(ObjectiveExtremum aObjExtremum, const std::string& aVariableName="");
    
    VariableSemantic* clone() override;
    
    static bool isa(const VariableSemantic* aSemantic)
    {
      assert(aSemantic);
      return aSemantic->getSemanticClassType() == VariableSemanticClassType::VAR_SEMANTIC_CLASS_OBJECTIVE;
    }
    
    static VariableSemanticObjective* cast(const VariableSemantic* aSemantic)
    {
      if(!isa(aSemantic))
      {
        return nullptr;
      }
      return static_cast<VariableSemanticObjective*>(const_cast<VariableSemantic*>(aSemantic));
    }
    
    /// Returns the variable's name identifier
    inline std::string getVariableNameIdentifier() const override
    {
      return pVariableStringNameIdentifier;
    }
    
    /// Sets variable solution element flag
    inline void setAsSolutionElement(bool aSol)
    {
      pSolutionElement = aSol;
    }
    
    //// Solution element.
    /// @note for objective variables the default is true
    inline bool isSolutionElement() const override
    {
      return pSolutionElement;
    }
    
    /// Returns objective extremum
    inline ObjectiveExtremum getObjectiveExtremum() const
    {
      return pObjExtremum;
    }
  
  protected:
    /// Returns given branching policy, default false
    inline bool getBranchingPolicy(bool aBranchingSemanticPolicy) const override
    {
      return aBranchingSemanticPolicy;
    }
    
  private:
    /// Solution element flag
    bool pSolutionElement;
    
    /// Variable's string name identifier
    mutable std::string pVariableStringNameIdentifier;
    
    /// Semantic extremum to optimize
    ObjectiveExtremum pObjExtremum;
  };
  
} // end namespace Core
