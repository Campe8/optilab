//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/13/2016
//

#pragma once

// Export definitions
#include "VariableExportDefs.hpp"

// Types and macros
#include "VariableDefs.hpp"

// Base class
#include "VariableSemantic.hpp"

#include <cassert>
#include <string>
#include <memory>

namespace Core {
  
  class CORE_VARIABLE_EXPORT_CLASS VariableSemanticSupport : public VariableSemantic {
  public:
    /// Constructor
    /// @note set '_' as prefix if not present or create a new random identifier
    /// if "aVariableName" is empty
    VariableSemanticSupport(const std::string& aVariableName = "");
    
    VariableSemantic* clone() override;
    
    static bool isa(const VariableSemantic* aSemantic)
    {
      assert(aSemantic);
      return aSemantic->getSemanticClassType() == VariableSemanticClassType::VAR_SEMANTIC_CLASS_SUPPORT;
    }
    
    static VariableSemanticSupport* cast(const VariableSemantic* aSemantic)
    {
      if(!isa(aSemantic))
      {
        return nullptr;
      }
      return static_cast<VariableSemanticSupport*>(const_cast<VariableSemantic*>(aSemantic));
    }
    
    /// Returns the variable's name identifier
    inline std::string getVariableNameIdentifier() const override
    {
      return pVariableStringNameIdentifier;
    }
    
    /// Support variables are never part of the current solution nor
    /// they need to be ground
    inline bool isSolutionElement() const override
    {
      return false;
    }
    
  protected:
    /// Never branch on support variables
    inline bool getBranchingPolicy(bool aBranchingSemantic) const override
    {
      (void) aBranchingSemantic;
      return false;
    }
    
  private:
    /// Set '_' as prefix if not present or create a new random identifier
    /// if "aVariableName" is empty
    void setVariableNameIdentifier(const std::string& aVariableName);
    
    /// Variable's string name identifier
    mutable std::string pVariableStringNameIdentifier;
  };
  
} // end namespace Core
