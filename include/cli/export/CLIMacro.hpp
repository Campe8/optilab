//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/05/2017
//
// Declaration in the format "tag: value(s)":
// - 1: declaration type, e.g., variable, constraint, etc;
// - 2: unique identifier, i.e., an unsigned integer that uniquely
//      identifies the CLI message in a sequence.
//      This field is optional.
// - 8: list of parameters <id, value> separated by STX character.
//      id and value are space separated.
//

#pragma once

#include "CLIMacroVariable.hpp"
#include "CLIMacroConstraint.hpp"
#include "CLIMacroBranch.hpp"
#include "CLIMacroSearch.hpp"
#include "CLIMacroFunctionCall.hpp"
#include "CLIMacroMatrix.hpp"
#include "CLIMacroError.hpp"

// Data types
#define CLI_INT long
#define CLI_STR char*

#define CLI_MDL_EXPR	          class CLI::ModelExpr*
#define CLI_MDL_FCN_ARG_LIST	  class CLI::FunctionArgList*
#define CLI_MDL_PARAM_LIST	    class CLI::ParameterList*
#define CLI_MDL_DOM_DECL		    class CLI::DomainDeclaration*
#define CLI_MDL_DOM_DECL_SET	  class CLI::DomainDeclarationSet*
#define CLI_MDL_MATRIX_DECL     class CLI::MatrixDeclaration*
#define CLI_MDL_MATRIX_ROW_DECL class CLI::MatrixRowDeclaration*

#define CLI_MDL_NODE            class CLI::ModelNode*
#define CLI_MDL_NODE_VAR        class CLI::ModelNodeVariable*
#define CLI_MDL_NODE_CON        class CLI::ModelNodeConstraint*
#define CLI_MDL_NODE_FCN_CALL   class CLI::ModelNodeFunctionCall*
#define CLI_MDL_NODE_MATRIX     class CLI::ModelNodeMatrix*
#define CLI_MDL_NODE_ERR        class CLI::ModelNodeError*

// Macros for CLI tokens
#define CLI_INF std::numeric_limits<CLI_INT>::max();
#define CLI_TRUE  1
#define CLI_FALSE 0

#define CLI_MSG_ASCII_SOH    1
#define CLI_MSG_ASCII_STX    2
#define CLI_MSG_ASCII_US     31
#define CLI_MSG_ASCII_SPACE  32

// Separator between pairs (tag, value).
// For example: (tag, value) CLI_MSG_FIELD_DELIM (tag, value)
#define CLI_MSG_FIELD_DELIM CLI_MSG_ASCII_SOH

// Separator between tag-value pairs.
// For example: 1 CLI_MSG_TAG_VAL_DELIM "variable"
#define CLI_MSG_TAG_VAL_DELIM CLI_MSG_ASCII_US

// CLI message tag: field in cli msg for tag type
#define CLI_MSG_TAG_TYPE        1   // Type of message
#define CLI_MSG_TAG_UID         2   // unique id
#define CLI_MSG_TAG_EVENT       3   // event, for example, overlflow
#define CLI_MSG_TAG_PARAM_LIST  8   // parameter list

// Events happening during CLI parsing
#define CLI_MSG_VAL_EVENT_SATURATION "saturation"

// CLI message macro function
#define CLI_MSG_TAG_INT_TO_CHAR(value) static_cast<char>(value)
