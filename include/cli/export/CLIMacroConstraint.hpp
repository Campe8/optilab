//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/23/2017
//
// Macros for CLI messages for constraint declaration.
//
// Constraint declaration in the format "tag: value(s)":
// - 10: constraint declaration type
//       a) expr
//       b) fcn_call
// - 11: expr type, valid if 10 is (a)
//       a) c -> constant, e.g., 5
//       b) i -> identifier, e.g., x
//       c) e -> complex expression, e.g., 2 + x[3]
// - 12: preorder visit of expr, valid if 10 is (a)
// - 13: constraint name, valid if 10 is (b)
// - 14: constraint parameters -> x y 10
// - 15: constraint propagation type:
//       a) bounds
//       b) domain
// - 16: constraint key used to uniquely identify
//       a constraint, this field is optional
//

#pragma once

// Use AST macro
#include "ModelASTMacro.hpp"

// CLI MSG type value
#define CLI_MSG_TYPE_VALUE_CON_DECL "constraint"

// Tags for CLI message constraint declaration
#define CLI_MSG_TAG_CON_DECL_TYPE       10
#define CLI_MSG_TAG_CON_EXPR_TYPE       11
#define CLI_MSG_TAG_CON_EXPR_PREORDER   12
#define CLI_MSG_TAG_CON_NAME            13
#define CLI_MSG_TAG_CON_PARAMS          14
#define CLI_MSG_TAG_CON_PROP_TYPE				15
#define CLI_MSG_TAG_CON_KEY     				16

// Propagation type
#define CLI_MSG_VAL_CON_PROP_TYPE_DOM  MDL_AST_LEAF_PROPAGATION_TYPE_DOMAIN
#define CLI_MSG_VAL_CON_PROP_TYPE_BND  MDL_AST_LEAF_PROPAGATION_TYPE_BOUND

// Declaration type
#define CLI_MSG_VAL_CON_DECL_TYPE_EXPR  MDL_AST_LEAF_DECLARATION_TYPE_EXPR
#define CLI_MSG_VAL_CON_DECL_TYPE_CALL  MDL_AST_LEAF_DECLARATION_TYPE_FCN_CALL

// Expression declaration
#define CLI_MSG_VAL_CON_EXPR_TYPE_C "c" // constant
#define CLI_MSG_VAL_CON_EXPR_TYPE_I "i" // identifier
#define CLI_MSG_VAL_CON_EXPR_TYPE_E "e" // complex expression
