//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 08/05/2017
//
// Macros for CLI messages for function call statement.
//
// Function call declaration in the format "tag: value(s)":
// - 10: function name;
// - 11: number of inputs;
// - 12: input list,
//       for example: x y z
//

#pragma once

// Use JSON macro 
#include "ModelASTMacro.hpp"

// CLI MSG type value
#define CLI_MSG_TYPE_VALUE_FCN_CALL_DECL "fcn_call"

// Tags for CLI message variable declaration
#define CLI_MSG_TAG_FCN_CALL_ID         10
#define CLI_MSG_TAG_FCN_CALL_NUM_ARGS   11
#define CLI_MSG_TAG_FCN_CALL_ARGS       12

// Separator for function arguments
#define CLI_MSG_VAL_FCN_CALL_ARGS_SEP   " "
