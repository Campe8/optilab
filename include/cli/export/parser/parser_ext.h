#pragma once

#include "CLIExportDefs.hpp"

// Parse inStr and return the syntax tree string in outStr
CLI_EXPORT_EXTERN_C int parseString(char *inStr, char *outStr);

// Parser function
CLI_EXPORT_EXTERN_C int yyparse(void);
