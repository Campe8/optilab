//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/11/2017
//
// Macros for CLI messages for variable declaration.
//
// Variable declaration in the format "tag: value(s)":
// - 10: variable id;
// - 11: variable type
//       a) bool
//       b) integer
// - 12: variable semantic type
//       a) decision
//       b) objective
//       c) support
// - 13: output variable
//       a) 1
//       b) 0
// - 14: domain declaration format
//       a) expr  -> 2 + 3
//       b) range -> 1 10
//       c) list  -> 1 20 300
// - 15: domain declaration in plain text
// - 16: branching semantic
//       a) 1
//       b) 0
// - 17: optimization extremum
//       a) min
//       b) max
// - 18: input order
// - 19: number of rows (for matrix declaration)
// - 20: number of columns per row (for matrix declaration)
//

#pragma once

// Use JSON macro 
#include "ModelASTMacro.hpp"

// CLI MSG type value
#define CLI_MSG_TYPE_VALUE_VAR_DECL "variable"

// Tags for CLI message variable declaration
#define CLI_MSG_TAG_VAR_ID              10
#define CLI_MSG_TAG_VAR_TYPE            11
#define CLI_MSG_TAG_VAR_SEMANTIC_TYPE   12
#define CLI_MSG_TAG_VAR_OUTPUT          13
#define CLI_MSG_TAG_VAR_DOM_DEC_FORMAT  14
#define CLI_MSG_TAG_VAR_DOM_DEC_TEXT    15
#define CLI_MSG_TAG_VAR_BRANCHING       16
#define CLI_MSG_TAG_VAR_OBJ_EXTREMUM    17
#define CLI_MSG_TAG_VAR_INPUT_ORDER			18
#define CLI_MSG_TAG_VAR_NUM_ROWS        19
#define CLI_MSG_TAG_VAR_NUM_COLS        20

// Values for CLI message variable declaration
#define CLI_MSG_VAL_VAR_TYPE_BOOL                 MDL_AST_LEAF_VAR_TYPE_BOOL
#define CLI_MSG_VAL_VAR_TYPE_INT                  MDL_AST_LEAF_VAR_TYPE_INT

#define CLI_MSG_VAL_VAR_SEMANTIC_TYPE_DECISION    MDL_AST_LEAF_VAR_SEMANTIC_TYPE_DECISION
#define CLI_MSG_VAL_VAR_SEMANTIC_TYPE_OBJECTIVE   MDL_AST_LEAF_VAR_SEMANTIC_TYPE_OBJECTIVE
#define CLI_MSG_VAL_VAR_SEMANTIC_TYPE_SUPPORT     MDL_AST_LEAF_VAR_SEMANTIC_TYPE_SUPPORT

#define CLI_MSG_VAL_VAR_DOM_DEC_FORMAT_EXP        "expr"
#define CLI_MSG_VAL_VAR_DOM_DEC_FORMAT_RNG        "range"
#define CLI_MSG_VAL_VAR_DOM_DEC_FORMAT_LST        "list"

// Separator for domain elements
#define CLI_MSG_VAL_VAR_DOM_DEC_ELEM_SEP          " "

// Optional values for parameter list
#define CLI_MSG_VAL_VAR_OUTPUT                    MDL_AST_LEAF_VAR_OUTPUT
#define CLI_MSG_VAL_VAR_SEMANTIC_TYPE             MDL_AST_LEAF_VAR_SEMANTIC
#define CLI_MSG_VAL_VAR_BRANCHING                 MDL_AST_LEAF_VAR_BRANCHING
#define CLI_MSG_VAL_VAR_OBJ_EXTREMUM              MDL_AST_LEAF_VAR_SEMANTIC_OPT_EXTREMUM
