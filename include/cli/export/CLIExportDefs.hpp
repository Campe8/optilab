#pragma once

#include "dll_export.h"

/* This header is being included by files inside this module */

#define CLI_EXPORT_CLASS      DLL_EXPORT_SYM
#define CLI_EXPORT_STRUCT     DLL_EXPORT_SYM
#define CLI_EXPORT_FRIEND     DLL_EXPORT_SYM
#define CLI_EXPORT_FCN        DLL_EXPORT_SYM
#define CLI_EXPORT_TEMPLATE   DLL_EXPORT_TEMPLATE
#define CLI_EXPORT_EXTERN_C   extern "C" DLL_EXPORT_SYM
