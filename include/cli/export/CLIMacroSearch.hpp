//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/20/2017
//
// Macros for CLI messages for search declaration.
//
// Search declaration in the format "tag: value(s)":
// - 10: search type
//			 a) dfs 
// - 11: variable choice type
//       a) input_order
//       b) largest
//			 c) ...
// - 12: value choice type
//       a) indomain_max
//       b) indomain_min
//       c) ...
// - 13: branching variables
// - 14: number of solutions
//

#pragma once

// Use JSON macro 
#include "ModelASTMacro.hpp"

// CLI MSG type value
#define CLI_MSG_TYPE_VALUE_SRC_DECL "search"

// Tags for CLI message variable declaration
#define CLI_MSG_TAG_SRC_TYPE            10
#define CLI_MSG_TAG_SRC_VAR_CHOICE_TYPE	11
#define CLI_MSG_TAG_SRC_VAL_CHOICE_TYPE 12
#define CLI_MSG_TAG_SRC_BRANCHING_VAR		13
#define CLI_MSG_TAG_SRC_NUM_SOLUTIONS		14

// Values for CLI message search type
#define CLI_MSG_VAL_SRC_TYPE_DFS		MDL_AST_LEAF_SEARCH_TYPE_ALGO_DFS

// Values for CLI message search variable choice type
#define CLI_MSG_VAL_SRC_VAR_DEFAULT           MDL_AST_LEAF_SEARCH_SEMANTIC_VAR_DEFAULT
#define CLI_MSG_VAL_SRC_VAR_INPUT_ORDER				MDL_AST_LEAF_SEARCH_SEMANTIC_VAR_INPUT_ORDER
#define CLI_MSG_VAL_SRC_VAR_LARGEST						MDL_AST_LEAF_SEARCH_SEMANTIC_VAR_LARGEST
#define CLI_MSG_VAL_SRC_VAR_SMALLEST					MDL_AST_LEAF_SEARCH_SEMANTIC_VAR_SMALLEST
#define CLI_MSG_VAL_SRC_VAR_FIRST_FAIL				MDL_AST_LEAF_SEARCH_SEMANTIC_VAR_FIRST_FAIL
#define CLI_MSG_VAL_SRC_VAR_ANTI_FIRST_FAIL		MDL_AST_LEAF_SEARCH_SEMANTIC_VAR_ANTI_FIRST_FAIL

// Values for CLI message search value choice type
#define CLI_MSG_VAL_SRC_VAL_DEFAULT         MDL_AST_LEAF_SEARCH_SEMANTIC_VAL_DEFAULT
#define CLI_MSG_VAL_SRC_VAL_INDOMAIN_MAX		MDL_AST_LEAF_SEARCH_SEMANTIC_VAL_INDOMAIN_MAX
#define CLI_MSG_VAL_SRC_VAL_INDOMAIN_MIN		MDL_AST_LEAF_SEARCH_SEMANTIC_VAL_INDOMAIN_MIN
#define CLI_MSG_VAL_SRC_VAL_INDOMAIN_RND		MDL_AST_LEAF_SEARCH_SEMANTIC_VAL_INDOMAIN_RANDOM
