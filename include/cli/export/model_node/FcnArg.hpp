//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/16/2017
//

#pragma once

#include "CLIExportDefs.hpp"

#include <string>
#include <vector>
#include <cassert>

namespace CLI {

  class CLI_EXPORT_CLASS FunctionArg {
  public:
    FunctionArg(const std::string& aArg);

    ~FunctionArg() = default;

    inline std::string getArgument() const
    {
      return pArgument;
    }
    
    inline bool isBoolean() const
    {
      return pBoolean;
    }
    
    inline void setBoolean()
    {
      pBoolean = true;
    }
    
  private:
    /// Function argument
    std::string pArgument;
    
    /// Boolean flag argument
    bool pBoolean;
  };
  
  class CLI_EXPORT_CLASS FunctionArgList {
  public:
    FunctionArgList() = default;
    ~FunctionArgList() = default;
    
    /// Adds an argument array,
    /// given "aArg" stores it as "[aArg]"
    void addArgArray(const FunctionArg& aArg);
    
    void addArg(const FunctionArg& aArg)
    {
      pArgList.push_back(aArg);
    }
    
    inline std::size_t numArgs() const
    {
      return pArgList.size();
    }
    
    inline FunctionArg& getArg(std::size_t aIdx)
    {
      assert(aIdx < numArgs());
      return pArgList[aIdx];
    }
    
  private:
    std::vector<FunctionArg> pArgList;
  };
  
}// end namespace CLI
