//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/16/2017
//

#pragma once

#include "CLIExportDefs.hpp"
#include "CLIMacroVariable.hpp"

#include <string>
#include <memory>

namespace CLI {

  class CLI_EXPORT_CLASS DomainDeclaration {
  public:
    enum DomainDeclarationType : int {
        DOM_DEC_EXP = 0
      , DOM_DEC_RNG
      , DOM_DEC_SET
      , DOM_DEC_UNDEF
    };

    virtual ~DomainDeclaration() = default;
    
    inline DomainDeclarationType domainDeclarationType() const
    {
      return pDomainType;
    }
    
    /// Returns the string declaration for this domain
    inline std::string domainDeclaration() const
    {
      return pDomain;
    }

    /// Return the type of domain, e.g., bool, integer, etc.
    virtual std::string domainType();
    
    inline bool hasOverflow() const
    {
      return pOverflow;
    }
    
    inline void setOverflow()
    {
      pOverflow = true;
    }
    
    inline void setBoolean()
    {
      pBoolean = true;
    }
    
    inline bool isBoolean() const
    {
      return pBoolean;
    }
    
  protected:
    /**
     * Domain declaration for ModelNodeVariable object(s).
     * It contains the type of domain as per CLI declaration statement
     * and the string encoding the domain.
     */
    DomainDeclaration(DomainDeclarationType aDecType, std::string aDomDec);
    
  private:
    /// Type of this domain declaration
    DomainDeclarationType pDomainType;

    /// Domain declaration in plain text
    std::string pDomain;
    
    /// Overflow on domain elements
    bool pOverflow;
    
    /// Boolean domain flag
    bool pBoolean;
  };

  class CLI_EXPORT_CLASS DomainDeclarationExpression : public DomainDeclaration {
  public:
    DomainDeclarationExpression(std::string aDomDec);

    static bool isa(const DomainDeclaration* aDomainDecl);

    static DomainDeclarationExpression* cast(DomainDeclaration* aDomainDecl);
    static const DomainDeclarationExpression* cast(const DomainDeclaration* aDomainDecl);
  };

  class CLI_EXPORT_CLASS DomainDeclarationRange : public DomainDeclaration {
  public:
    DomainDeclarationRange(std::string aDomDec);

    static bool isa(const DomainDeclaration* aDomainDecl);

    static DomainDeclarationRange* cast(DomainDeclaration* aDomainDecl);
    static const DomainDeclarationRange* cast(const DomainDeclaration* aDomainDecl);
  };

  class CLI_EXPORT_CLASS DomainDeclarationSet : public DomainDeclaration {
  public:
    DomainDeclarationSet(std::string aDomDec);

    static bool isa(const DomainDeclaration* aDomainDecl);

    static DomainDeclarationSet* cast(DomainDeclaration* aDomainDecl);
    static const DomainDeclarationSet* cast(const DomainDeclaration* aDomainDecl);
  };
}// end namespace CLI
