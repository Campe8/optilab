//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 11/18/2017
//

#pragma once

#include "CLIExportDefs.hpp"
#include "ModelExpr.hpp"

#include <string>
#include <vector>

namespace CLI {

  class CLI_EXPORT_CLASS MatrixRowDeclaration {
  public:
    using Row = std::vector<std::string>;
    
  public:
    MatrixRowDeclaration();
    
    void addElement(ModelExpr* aExpr);
    
    /// Sets flag to indicate that this expression is a Boolean expression
    inline void setBoolean()
    {
      pBoolean = true;
    }
    
    /// Returns Boolean flag
    inline bool isBoolean() const
    {
      return pBoolean;
    }
    
    /// Sets flag to indicate that this expression has an overflow
    inline void setOverflow()
    {
      pOverflow = true;
    }
    
    /// Returns overflow flag
    inline bool hasOverflow() const
    {
      return pOverflow;
    }
    
    inline const Row& getRow() const
    {
      return pRow;
    }
    
  private:
    /// Boolean flag
    bool pBoolean;
    
    /// Overflow flag
    bool pOverflow;
    
    /// Matrix row
    Row pRow;
  };
  
  class CLI_EXPORT_CLASS MatrixDeclaration {
  public:
    using MatrixRow = MatrixRowDeclaration::Row;
    
  public:
    MatrixDeclaration();
    
    /// Sets flag to indicate that this expression is a Boolean expression
    inline void setBoolean()
    {
      pBoolean = true;
    }
    
    /// Returns Boolean flag
    inline bool isBoolean() const
    {
      return pBoolean;
    }
    
    /// Sets flag to indicate that this expression has an overflow
    inline void setOverflow()
    {
      pOverflow = true;
    }
    
    /// Returns overflow flag
    inline bool hasOverflow() const
    {
      return pOverflow;
    }
    
    inline std::size_t numRows() const
    {
      return pMatrix.size();
    }
    
    inline std::size_t numCols() const
    {
      if(numRows() == 0) { return  0; }
      return pMatrix[0].size();
    }
    
    /// Adds a row to the matrix, size must be consistent.
    /// First row determines the number of columns
    void addRow(MatrixRowDeclaration* aRow);
    
    MatrixRow& getRow(std::size_t aRowIdx);
    
    inline const std::vector<MatrixRow>& getMatrix() const
    {
      return pMatrix;
    }
    
  private:
    /// Boolean flag
    bool pBoolean;
    
    /// Overflow flag
    bool pOverflow;
    
    /// Matrix
    std::vector<MatrixRow> pMatrix;
  };
  
}// end namespace CLI
