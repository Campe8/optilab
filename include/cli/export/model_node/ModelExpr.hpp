//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/23/2017
//

#pragma once

#include "CLIExportDefs.hpp"
#include "CLIMacro.hpp"

#include <cstring>
#include <string>
#include <unordered_map>
#include <memory>

namespace CLI {
  
  class CLI_EXPORT_CLASS ModelExpr {
  public:
    // Operators for expressions
    enum ExprOp : int {
        EOP_EQUALS = 0      // '='
      , EOP_UNARY_PLUS      // '+'
      , EOP_UNARY_MINUS     // '-'
      , EOP_PLUS            // '+'
      , EOP_MINUS           // '-'
      , EOP_MULT            // '*'
      , EOP_DIV             // '/'
      , EOP_LT              // '<'
      , EOP_GT              // '>'
      , EOP_GE              // ">="
      , EOP_LE              // "<="
      , EOP_NE              // "!="
      , EOP_EQ              // "=="
      , EOP_AND             // "&&"
      , EOP_OR              // "||"
      , EOP_REIF            // "<->"
      , EOP_HREIF_L         // "<-"
      , EOP_HREIF_R         // "->"
      , EOP_SUBSCRIPT       // "[]"
    };
    
    /// C'tor for scalar INT
    ModelExpr(CLI_INT aScalar);
    
    /// C'tor for Strings
    ModelExpr(CLI_STR aId);
    
    /// C'tor for unary expressions
    ModelExpr(ModelExpr::ExprOp aExprOp, const std::shared_ptr<ModelExpr>& aExpr);
    
    /// C'tor for binary expressions
    ModelExpr(ModelExpr::ExprOp aExprOp, const std::shared_ptr<ModelExpr>& aExpr1, const std::shared_ptr<ModelExpr>& aExpr2);
    
    /// Sets flag to indicate that this expression is a Boolean expression
    inline void setBoolean()
    {
      pBoolean = true;
    }
    
    /// Returns Boolean flag
    inline bool isBoolean() const
    {
      return pBoolean;
    }
    
    /// Sets flag to indicate that this expression has an overflow
    inline void setOverflow()
    {
      pOverflow = true;
    }
    
    /// Returns overflow flag
    inline bool hasOverflow() const
    {
      return pOverflow;
    }
    
    /// Returns true if this expression encodes
    /// a scalar number, false otherwise
    inline bool isScalar() const
    {
      return pExprType == ExprType::ET_SCALAR_INT;
    }
    
    /// Returns true if this expression encodes
    /// an identifier, false otherwise
    inline bool isId() const
    {
      return pExprType == ExprType::ET_ID;
    }
    
    /// Returns true if this expression represents a scalar OR
    /// an identifier, false otherwise
    inline bool isScalarOrId() const
    {
      return isScalar() || isId();
    }
    
    /// Returns the expression as string
    inline std::string expr() const
    {
      return pExpr;
    }
    
  private:
    enum ExprType : int {
        ET_SCALAR_INT = 0
      , ET_ID
      , ET_CEXPR
    };
    
    /// Expression type
    ExprType pExprType;
    
    /// Expression as a string
    std::string pExpr;
    
    /// Flag for overflow event
    bool pOverflow;
    
    /// Flag for Boolean expression
    bool pBoolean;
    
    /// Evaluate the "pExpr1 aExprOp pExpr2" where
    /// "pExpr1" and "pExpr2" are ModelExpr representing scalar values.
    /// Returns the evaluated expression as a string
    std::string evaluateScalarExpr(ModelExpr::ExprOp aExprOp, const std::shared_ptr<ModelExpr>& aExpr1, const std::shared_ptr<ModelExpr>& aExpr2);
    
    /// Returns the string operator "aOp"
    std::string getOperatorSymb(ModelExpr::ExprOp aOp);
  };
  
}// end namespace CLI
