//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/16/2017
//

#pragma once

#include "CLIExportDefs.hpp"

#include <string>
#include <utility>
#include <unordered_map>

namespace CLI {

  class CLI_EXPORT_CLASS ParameterList {
  public:
    /// A parameter is a pair:
    /// <parameter name, parameter value>
    typedef std::pair<std::string, std::string> Parameter;

    ParameterList() = default;

    ~ParameterList() = default;

    /// Adds a parameter to the list
    inline void addParameter(const Parameter& aParameter)
    {
      pParameters[aParameter.first] = aParameter.second;
    }

    /// Returns the number of parameters in the list
    inline std::size_t numParameters() const
    {
      return pParameters.size();
    }

    /// Returns the parameter in the list at position "aIndexParam"
    Parameter getParameter(std::size_t aIndexParam);

    /// Returns true if "aParameterName" is in the list of parameters,
    /// false otherwise
    inline bool containsParameter(const std::string& aParameterName) const
    {
      return pParameters.find(aParameterName) != pParameters.end();
    }

    /// Returns the value for the given parameter name
    inline std::string getParameterValue(const std::string& aParameterName) const
    {
      return pParameters.at(aParameterName);
    }

  private:
    /// List of parameters
    std::unordered_map<std::string, std::string> pParameters;
  };

}// end namespace CLI
