//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 11/19/2017
//
// Macros for CLI messages for matrix declaration.
//
// Matrix declaration in the format "id: [value(s)]":
// - 10: matrix id;
// - 11: matrix type
//       a) bool
//       b) integer
// - 12: number of rows
// - 13: number of columns
// - 14: rows defining the matrix
//       separated by the '#' char
//       a1, b1, ..., c1#a2, b2, ..., c2
//

#pragma once

// Use JSON macro 
#include "ModelASTMacro.hpp"

// CLI MSG type value
#define CLI_MSG_TYPE_VALUE_MAT_DECL "matrix"

// Tags for CLI message variable declaration
#define CLI_MSG_TAG_MAT_ID              10
#define CLI_MSG_TAG_MAT_TYPE            11
#define CLI_MSG_TAG_MAT_NUM_ROWS        12
#define CLI_MSG_TAG_MAT_NUM_COLS        13
#define CLI_MSG_TAG_MAT_ROWS            14

// Values for CLI message variable declaration
#define CLI_MSG_VAL_MAT_TYPE_BOOL                 MDL_AST_LEAF_VAR_TYPE_BOOL
#define CLI_MSG_VAL_MAT_TYPE_INT                  MDL_AST_LEAF_VAR_TYPE_INT

// Values for CLI message variable declaration
#define CLI_MSG_TAG_MAT_ROWS_SEP  "#"
