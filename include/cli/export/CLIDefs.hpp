//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/05/2017
//

#pragma once

#include <map>

namespace CLI {
  typedef std::string CLIString;
  
  /// Map of pairs <tag, value> for a CLI messages:
  /// - tag: 1, 2, 3, etc. according to the CLI message protocol
  /// - value: string containing the value of the tag
  typedef std::map<int, std::string> CLIStmt;
  
  enum CLIMsgType : int {
      CLI_MSG_VAR_DECL = 0
    , CLI_MSG_CON_DECL
    , CLI_MSG_BRC_DECL
    , CLI_MSG_SRC_DECL
    , CLI_MSG_OBJ_ASSIGN
    , CLI_MSG_FCN_CALL
    , CLI_MSG_MATRIX_DECL
    , CLI_MSG_ERR
    , CLI_MSG_UNDEF
  };
  
  /// Event on CLI msg
  enum CLIMsgEvent : int {
      CLI_MSG_EVENT_SATURATION = 0
    , CLI_MSG_EVENT_NONE
  };
  
}// end namespace CLI

