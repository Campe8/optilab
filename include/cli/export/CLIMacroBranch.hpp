//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 01/12/2018
//
// Macros for CLI messages for branch statement.
//
// Branching declaration in the format "tag: value(s)":
// - 10: variable choice type
//       a) input_order
//       b) largest
//			 c) ...
// - 11: value choice type
//       a) indomain_max
//       b) indomain_min
//       c) ...
// - 12: branching variables
// - 13: branching key used to uniquely identify
//       a branching statement, this field is optional
//
 
#pragma once

// Use JSON macro 
#include "ModelASTMacro.hpp"

// CLI MSG type value
#define CLI_MSG_TYPE_VALUE_BRC_DECL "branch"

// Tags for CLI message branching declaration
#define CLI_MSG_TAG_BRC_VAR_CHOICE_TYPE	10
#define CLI_MSG_TAG_BRC_VAL_CHOICE_TYPE 11
#define CLI_MSG_TAG_BRC_BRANCHING_VAR		12
#define CLI_MSG_TAG_BRC_KEY     				13

// Values for CLI message search variable choice type
#define CLI_MSG_VAL_BRC_VAR_DEFAULT           MDL_AST_LEAF_BRC_VAR_DEFAULT
#define CLI_MSG_VAL_BRC_VAR_INPUT_ORDER				MDL_AST_LEAF_BRC_VAR_INPUT_ORDER
#define CLI_MSG_VAL_BRC_VAR_LARGEST						MDL_AST_LEAF_BRC_VAR_LARGEST
#define CLI_MSG_VAL_BRC_VAR_SMALLEST					MDL_AST_LEAF_BRC_VAR_SMALLEST
#define CLI_MSG_VAL_BRC_VAR_FIRST_FAIL				MDL_AST_LEAF_BRC_VAR_FIRST_FAIL
#define CLI_MSG_VAL_BRC_VAR_ANTI_FIRST_FAIL		MDL_AST_LEAF_BRC_VAR_ANTI_FIRST_FAIL

// Values for CLI message search value choice type
#define CLI_MSG_VAL_BRC_VAL_DEFAULT         MDL_AST_LEAF_BRC_VAL_DEFAULT
#define CLI_MSG_VAL_BRC_VAL_INDOMAIN_MAX		MDL_AST_LEAF_BRC_VAL_INDOMAIN_MAX
#define CLI_MSG_VAL_BRC_VAL_INDOMAIN_MIN		MDL_AST_LEAF_BRC_VAL_INDOMAIN_MIN
#define CLI_MSG_VAL_BRC_VAL_INDOMAIN_RND		MDL_AST_LEAF_BRC_VAL_INDOMAIN_RANDOM
