//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/18/2017
//
// Macros for CLI messages for error messages.
//
// Error declaration in the format "tag: value(s)":
// - 10: error message
//

#pragma once

// Use AST macro 
#include "ModelASTMacro.hpp"

// CLI MSG type value
#define CLI_MSG_TYPE_VALUE_ERR_DECL "error"

// Tags for CLI message variable declaration
#define CLI_MSG_TAG_ERR_MSG 10
