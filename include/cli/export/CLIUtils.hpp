//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/04/2017
//

#pragma once

#include "CLIExportDefs.hpp"

#include "CLIMacro.hpp"
#include "CLIDefs.hpp"

#include "GlobalsSysLib.hpp"
#include "GlobalsSysMacro.hpp"
#include "GlobalsType.hpp"

#include <boost/optional.hpp>

#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <limits>

namespace CLI {
  namespace Utils {
    
    /// Adds a <tag, value> pair to the given CLI statement
    CLI_EXPORT_FCN void addCLIStmtVal(CLIStmt& aCLIStmt, const std::pair<int, std::string>& aTagVal);
    
    /// Returns the value of tag "aTag" in "aCLIStmt"
    CLI_EXPORT_FCN boost::optional<std::string> getCLIStmtVal(const CLIStmt& aCLIStmt, int aTag);
    
    /// Parses a CLI message such as
    /// 1 "variable" | 10 "d" | etc.
    /// and returns a CLIStmt
    CLI_EXPORT_FCN CLIStmt getCLIStmtFromString(const std::string& aCLIMsg);
    
    /// Parses a CLIStmt and returns
    /// a CLI message string like the following
    /// 1 "variable" | 10 "d" | etc.
    CLI_EXPORT_FCN std::string getStringFromCLIStmt(const CLIStmt& aCLIStmt);
    
    /// Returns a pretty print of "aStructuredMsgString" which must be constructed from a structured message
    CLI_EXPORT_FCN std::string prettyPrintStringFromCLIStmt(const std::string& aCLIStmtString);
    
    /// Returns a structure message string print of "aPrettyPrinttring" which must be constructed from a structured message
    CLI_EXPORT_FCN std::string cliStmtStringFromPrettyPrint(const std::string& aPrettyPrintString);
    
    /// Returns the type of "aStructuredMsg"
    CLI_EXPORT_FCN CLIMsgType getMsgType(const CLIStmt& aStructuredMsg);
    
    /// Returns the event on "aStructuredMsg"
    CLI_EXPORT_FCN CLIMsgEvent getMsgEvent(const CLIStmt& aStructuredMsg);
    
    /// Trims left end of "aString"
    CLI_EXPORT_FCN void ltrim(std::string& aString);
    CLI_EXPORT_FCN std::string ltrimmed(std::string aString);
    
    /// Trims right end of "aString"S
    CLI_EXPORT_FCN void rtrim(std::string& aString);
    CLI_EXPORT_FCN std::string rtrimmed(std::string aString);
    
    /// Trims "aString" on both ends
    CLI_EXPORT_FCN void trim(std::string& aString);
    CLI_EXPORT_FCN std::string trimmed(std::string aString);
    
    /// Returns the number of occurences of "aSubStr" in "aStr"
    CLI_EXPORT_FCN std::size_t numSubStrInString(const std::string& aStr, const std::string& aSubStr);
    
    /// Returns true if "aString" encodes an Integer number (sign is optional),
    /// false otherwise
    CLI_EXPORT_FCN bool isInteger(const std::string& aString);
    
    /// Returns true if "aString" encodes a quoted string,
    /// false otherwise.
    /// For example,
    /// \"myQuotedString\"
    /// returns true
    CLI_EXPORT_FCN bool isQuotedString(const std::string& aString);
    
    /// Returns the un-quoted string "aString"
    CLI_EXPORT_FCN std::string removeQuotesFromQuotedString(const std::string& aString);
    
    /// Converts "aString" into an integer.
    /// Throws if conversion is not possible
    CLI_EXPORT_FCN INT_64_T stoi(const std::string& aString);
    
    /// Tokenizes "aString" based on delimiter character "aChar".
    /// For example, if aString = 1,2,3 and aChar = ',', returns the
    /// vector ["1", "2", "3"]
    CLI_EXPORT_FCN std::vector<std::string> tokenizeStringOnSymbol(const std::string& aString, char aChar=' ');
    
    /// Given a string containing the
    /// arguments of the fcn call, returns a vector
    /// containing each argument separately.
    /// For example:
    /// x, t[x[1], m, 2], string_arg
    /// returns the following 3 arguments
    /// 1) x
    /// 2) t[x[1], m, 2]
    /// 3) string_arg
    CLI_EXPORT_FCN std::vector<std::string> splitArguments(const std::string& aArgs);
    
    /// Saturates on min/max of type T or returns value casted to T
    template<typename T>
    CLI_EXPORT_FCN T evalAndSaturate(double aVal)
    {
      if (aVal >= static_cast<double>(std::numeric_limits<T>::max()))
      {
        return std::numeric_limits<T>::max();
      }
      else if (aVal <= static_cast<double>(std::numeric_limits<T>::lowest()))
      {
        return std::numeric_limits<T>::lowest();
      }
      else if (aVal <= static_cast<double>(std::numeric_limits<T>::min()))
      {
        return static_cast<T>(0);
      }
      return static_cast<T>(aVal);
    }
    
    /// Converts "aVal" into string.
    /// @note "aVal" must be a C++ primitive type
    template<class T> CLI_EXPORT_FCN std::string toString(const T& aVal)
    {
      std::stringstream ss;
      ss << aVal;
      return ss.str();
    }
    template<> CLI_EXPORT_FCN std::string toString<BOOL_T>(const BOOL_T& aVal);
    
    /// Returns the global path to the root folder
    CLI_EXPORT_FCN std::string getRootPath();
    
    /// Returns a string representing the current date in the format year<sep>month<sep>day
    CLI_EXPORT_FCN std::string getCurrentDate(char aSep='_');
  }// end namespace Utils
  
}// end namespace CLI
