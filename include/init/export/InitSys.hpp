//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 11/08/2018.
//
// Initialize OptiLab system.
//

#pragma once

#include "InitExportDefs.hpp"

#include "ProgramOptions.hpp"

#include "MVCModel.hpp"
#include "MVCViewCLI.hpp"
#include "MVCViewFile.hpp"
#include "MVCControllerIO.hpp"

#include <memory>

namespace Init {
  
  class INIT_EXPORT_CLASS InitSystem {
  public:
    /**
     * RunMode refers to how Optilab can be run.
     *
     * STATELESS:   OptiLab loads and run a ".opt" file;
     *
     * INTERACTIVE: the interactive OptiLab console is opened. This is the default mode.
     *              Note that the interactive console can run with/without smartcli support;
     *
     * GUI:         OptiLab runs inside the graphical user interface.
     *              Note that this mode is conflicting with STATELESS, i.e., only one mode
     *              can be selected at a time;
     *
     * TEST:        run OptiLab tests.
     *              Note that TEST is an exclusive mode;
     *
     * NONE:        OptiLab doesn't run at all (used for printing
     *              only commands like --help)
     */
    enum RunMode : int {
        STATELESS = 0
      , INTERACTIVE
      , GUI
      , TEST
      , NONE //here of handling print-only options like --help
    };
    
  public:
    InitSystem(int argc, char* argv[]);
    
    virtual ~InitSystem() = default;
    
    // Run the OptiLab system
    virtual int runSystem();
    
    /// Returns the run mode specified by the command line options
    inline RunMode getRunMode() const { return pRunMode; }
    
    /// Returns true if OptiLab should use a smart cli, false otherwise
    inline bool useSmartCLI() const { return pSmartCLI; }
    
    /// Returns the path to the file specified on CLI (used for STATELESS RunMode).
    /// @note returns an empty string if no path is specified
    inline std::string getFilePath() const { return pFilePath; };
    
  protected:
    virtual void setupCallingArguments(int argc, char* argv[]);
    
    /// Executes OptiLab
    virtual int exeSystem();
    
    /// Returns the instance of the program options
    inline Base::ProgramOptions& getProgramOptions() const { return *pProgramOptions; }
    
  private:
    /// Input number of arguments
    int pArgc;
    
    /// Input arguments
    char** pArgv;
    
    /// Stateless options
    char pStatelessSOpt;
    std::string pStatelessOpt;
    
    /// Smart CLI options
    char pSmartCLISOpt;
    std::string pSmartCLIOpt;
    
    /// Run unit and system test option
    char pRunSTest;
    std::string pRunTest;
    
    /// Use GUI options
    char pGUISOpt;
    std::string pGUIOpt;
    
    /// Print help message options
    char pHelpSOpt;
    std::string pHelpOpt;
    
    /// Flag indicating if a "smart" CLI is used.
    /// A "smart" CLI is any CLI that has features like history, up arrow search, etc.
    /// For more information, see readline or editline.
    /// @note by default smart cli usage is disabled
    bool pSmartCLI;
    
    /// Path to file for STATELESS run mode
    std::string pFilePath;
    
    /// Run mode cli selection.
    /// @note the default run mode is UNDEF
    RunMode pRunMode;
    
    /// Parser for CLI options
    std::shared_ptr<Base::ProgramOptions> pProgramOptions;
    
    /// View
    std::shared_ptr<MVC::MVCView> pView;
    
    /// Model
    std::shared_ptr<MVC::MVCModel> pModel;
    
    /// Controller
    std::shared_ptr<MVC::MVCControllerIO> pController;
    
    /// Utility function: register the CLI options
    void registerCLIOptions();
  };
  
} // end namespace Init
