//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/26/2016
//
// Base class for propagators.
//

#pragma once

// Export definitions
#include "ConstraintExportDefs.hpp"

// Types and macros
#include "ConstraintDefs.hpp"

// Propagation cost and priorities
#include "PropagationCost.hpp"

// Domain events
#include "DomainEventInc.hpp"

// Filters
#include "FilteringAlgorithm.hpp"

// Macros
#include "ConstraintMacro.hpp"

#include <iostream>
#include <cassert>
#include <memory>
#include <unordered_set>
#include <unordered_map>

// Forward decls.
namespace Core {
  class Propagator;
  typedef std::unique_ptr<Propagator> PropagatorUPtr;
  typedef std::shared_ptr<Propagator> PropagatorSPtr;
  typedef std::unordered_set<std::shared_ptr<DomainEvent>> DomainEventSet;
}// end namespace Core

namespace Core {
  
  /**
   * Semantics for the propagator.
   * Struct containing information about
   * strategy type, and other features of
   * the propagator.
   */
  struct PropagatorSemantics
  {
    PropagatorSemantics(std::size_t aContextSize,
                        PropagatorStrategyType aStrategyType,
                        PropagationPriority aPriority,
                        PropagatorSemanticType aSemanticType,
                        PropagatorSemanticClass aSemanticClass
                        )
    : ContextSize(aContextSize)
    , PropStrategyType(aStrategyType)
    , PropPriority(aPriority)
    , PropSemanticType(aSemanticType)
    , PropSemanticClass(aSemanticClass)
    {
    }
    
    ~PropagatorSemantics() = default;
    
    /// Context size, i.e., number of Domains
    /// involved in the propagator having this semantics
    std::size_t ContextSize;
    
    /// Strategy type, e.g., bound or domain
    PropagatorStrategyType PropStrategyType;
    
    /// Priority of the propagator, e.g., PROP_PRIORITY_BINARY
    PropagationPriority PropPriority;
    
    /// Propagator type
    PropagatorSemanticType PropSemanticType;
    
    /// Propagator semantic class
    PropagatorSemanticClass PropSemanticClass;
  };
  
  class Propagator {
  public:
    virtual ~Propagator() = default;
    
    /// Returns the minimum value for a fitness function
    static double getMinFitness();
    
    /// Returns the value for an unspecified fitness, for example, when not all
    /// domains in the propagator are singleton
    static double getUnspecFitness();
    
    /// Returns the value for invalid fitness, for example, when one of the domains
    /// in the propagator is empty
    static double getInvalidFitness();
    
    /// Filter domains according to the propagation semantic of this propagator.
    /// Returns the propagation event caused by propagation.
    /// @note if this propagator is disabled, returns
    /// PropagationEvent::PROP_EVENT_RUN_UNSPEC
    PropagationEvent propagate();
    
    /// Returns the priority/cost of this propagator.
    inline PropagationCost getCost() const
    {
      return pPropagatorCost;
    }
    
    /// Enable current propagator (default)
    inline void enablePropagator()
    {
      pPropagatorEnabled = true;
    }
    
    /// Disable current propagator
    inline void disablePropagator()
    {
      pPropagatorEnabled = false;
    }
    
    /// Returns true if this propagator is currently disabled
    bool isPropagatorDisabled() const
    {
      return !pPropagatorEnabled;
    }
    
    inline PropagatorStrategyType getPropagatorStrategyType() const
    {
      return pPropagatorSemantics.PropStrategyType;
    }
    
    inline PropagatorSemanticType getPropagatorTypeId() const
    {
      return pPropagatorSemantics.PropSemanticType;
    }
    
    inline PropagatorSemanticClass getPropagatorTypeClass() const
    {
      return pPropagatorSemantics.PropSemanticClass;
    }
    
    /// Returns the set of DomainEvent(s) triggering reevaluation
    /// of this propagator
    virtual DomainEventSet getReevaluationEventSet() const = 0;
    
    /// Returns the fitness of this propagatr w.r.t. the domains in its scope.
    /// If a domain is not singleton, it should return a negative value.
    /// The lower bound for the fitness function is 0
    virtual double getFitness() { return 0; }
    
    /// Post this propagator.
    /// Returns the propagation event caused by posting this propagator
    virtual PropagationEvent post() = 0;
    
  protected:
    Propagator(const PropagatorSemantics& aPropagatorSemantics);
    
    /// Run propagation on output domains according to the
    /// semantics of this propagator.
    /// It is guaranteed that no domain in the context is empty.
    virtual PropagationEvent runPropagation() = 0;
    
    /// Create and register a new instance (if not already present) of a
    /// filtering algorithm of type "aType"
    void registerFilteringStrategy(FilteringAlgorithmSemanticType aType);
    
    inline std::size_t getContextSize() const
    {
      return pContext.size();
    }
    
    /// Register a propagator instance to be used as a "sub-propagator"
    /// for the implementation of this propagator
    void registerPropagator(PropagatorSPtr aPropagator);
    
    /// Returns a pointer to a registered propagator given
    /// its semantic type "aPropagatorSemanticType".
    /// If no propagator is registered with that type, returns nullptr
    PropagatorSPtr getRegisteredPropagator(PropagatorSemanticType aPropagatorSemanticType);
    
    /// Posts internal propagator given its type "aPropagatorSemanticType".
    /// @note "aPropagatorSemanticType" must be registered, throws otherwise
    inline PropagationEvent postInternalPropagator(PropagatorSemanticType aPropagatorSemanticType)
    {
      assert(getRegisteredPropagator(aPropagatorSemanticType));
      return getRegisteredPropagator(aPropagatorSemanticType)->post();
    }//postInternalPropagator
    
    /// Runs "runPropagation" on internal propagator given its type "aPropagatorSemanticType".
    /// @note "aPropagatorSemanticType" must be registered, throws otherwise
    inline PropagationEvent runInternalPropagator(PropagatorSemanticType aPropagatorSemanticType)
    {
      assert(getRegisteredPropagator(aPropagatorSemanticType));
      return getRegisteredPropagator(aPropagatorSemanticType)->runPropagation();
    }//runInternalPropagator
    
    /// Adds a domain to the current context.
    /// The domain in the context will be identified by "aContextDomainIdx"
    /// where
    /// 0 <= aContextDomainIdx < getContextSize()
    inline void addContextDomain(std::size_t aContextDomainIdx, Domain *aDomainPtr)
    {
      assert(aContextDomainIdx < getContextSize());
      pContext[aContextDomainIdx] = aDomainPtr;
    }
    
    /// Returns Domain from context according to its Context idx
    /// Throws is "aContextDomainIdx" >= getContextSize()
    inline Domain *getContextDomain(std::size_t aContextDomainIdx) const
    {
      assert(aContextDomainIdx < getContextSize());
      return pContext.at(aContextDomainIdx);
    }
    
    /// Returns the number of singleton domains in the
    /// current context
    std::size_t numSingletonContextDomains() const;
    
    /// Sets the filtering algorithm to use when
    /// calling applyFilteringAlgorithm.
    /// @note "aFilterStrategy" must be registered (throws otherwise)
    inline void setFilterStrategy(FilteringAlgorithmSemanticType aFilterStrategy)
    {
      pFilterToUse = pFilterAlgorithmRegister.at(static_cast<int>(aFilterStrategy));
    }
    
    /// Returns the current filtering algorithm int use
    inline FilteringAlgorithmSPtr getCurrentFilterStrategy()
    {
      return pFilterToUse;
    }
    
    /// Sets output context domain as parameter for filtering algorithm.
    /// @note Throws is "aContextDomainIdx" >= getContextSize() or
    /// the context domain has not been added to the current context.
    inline void setFilterOutput(std::size_t aContextDomainIdx)
    {
      assert(aContextDomainIdx < getContextSize());
      Domain * domOut = getContextDomain(aContextDomainIdx);
      
      assert(domOut);
      pFilterParams.setOutputDomain(domOut);
    }
    
    /// Sets output context domain as parameter for filtering algorithm
    inline void setFilterOutputPtr(Domain *aDomainOutput)
    {
      assert(aDomainOutput);
      pFilterParams.setOutputDomain(aDomainOutput);
    }
    
    /// Sets input context domain as parameter for filtering algorithm.
    /// @note "aInputIdx" is the ordinal position of the input parameter
    /// for the filtering algorithm, as per FilteringParameters spec.
    /// @note Throws is "aContextDomainIdx" >= getContextSize() or
    /// the context domain has not been added to the current context.
    inline void setFilterInput(std::size_t aInputIdx, Domain* aInputDomain)
    {
      assert(aInputIdx < pFilterParams.getInputDomainArray()->size());
      pFilterParams.getInputDomainArray()->assignElementToCell(aInputIdx, aInputDomain);
    }
    
    /// Sets input domain element as parameter for filtering algorithm.
    /// @note "aInputIdx" is the ordinal position of the input parameter
    /// for the filtering algorithm, as per FilteringParameters spec.
    inline void setFilterInput(std::size_t aInputIdx, DomainElement* aInputDomainElement)
    {
      assert(aInputIdx < pFilterParams.getDomainElementArray()->size());
      pFilterParams.getDomainElementArray()->assignElementToCell(aInputIdx, aInputDomainElement);
    }
    
    /// Sets input domain array as parameter for filtering algorithm
    inline void setFilterInput(DomainArraySPtr aArray)
    {
      pFilterParams.setInputDomainArray(aArray);
    }
    
    /// Sets input domain element array as parameter for filtering algorithm
    inline void setFilterInput(DomainElementArraySPtr aArray)
    {
      pFilterParams.setDomainElementArray(aArray);
    }
    
    /// Apply the filtering algorithm specified by
    /// setFilterStrategy on the filtering parameters.
    /// Returns the output domain post filtering
    inline Domain* applyFilteringStrategy()
    {
      assert(pFilterToUse);
      return pFilterToUse->filter(pFilterParams);
    }
    
  private:
    /// Propagator enabling flag
    bool pPropagatorEnabled;
    
    /// Propagator semantics
    PropagatorSemantics pPropagatorSemantics;
    
    /// Propagator cost
    PropagationCost pPropagatorCost;
    
    /// Filtering algorithm in use
    FilteringAlgorithmSPtr pFilterToUse;
    
    /// Filtering parameters to use with pFilterToUse
    /// when applying the filtering algorithm
    FilteringParameters pFilterParams;
    
    /// Map of filter algorithms to use: <(int)FilteringAlgorithmSemanticType, FilteringAlgorithm ptr>
    std::unordered_map<int, FilteringAlgorithmSPtr> pFilterAlgorithmRegister;
    
    /// Context of this propagator, i.e.,
    /// the set of domains this propagator propagates on
    std::vector<Domain*> pContext;
    
    /// Map of filter algorithms to use: <(int)PropagatorSemanticType, FilteringAlgorithm ptr>
    std::unordered_map<int, PropagatorSPtr> pPropagatorRegister;
    
    /// Returns true if the filtering algorithm of type "aType"
    /// is already registered, false otherwise.
    inline bool isFilteringAlgorithmRegistered(FilteringAlgorithmSemanticType aType) const
    {
      return pFilterAlgorithmRegister.find(static_cast<int>(aType)) != pFilterAlgorithmRegister.end();
    }
    
    /// Returns true if there is an empty domain in
    /// the current context, false otherwise
    bool isAnyContextDomainEmpty() const;
    
    /// Returns true if there is at least one
    /// registered context domain., false otherwise.
    bool isContextRegistered() const;
  };
  
} // end namespace Core
