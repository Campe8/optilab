//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 12/10/2016
//
// Propagator for linear in-equations
//

#pragma once

// Base class
#include "PropagatorLinear.hpp"

namespace Core {
  
  class PropagatorLinearLq : public PropagatorLinear {
  public:
    PropagatorLinearLq(DomainElementArray& aCoeffArray,
                       DomainArray&        aDomainArray,
                       DomainElement*      aConstant,
                       bool aUseGCDOnNormalization = true);
    
    ~PropagatorLinearLq() override = default;
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;
    
  private:
    /// Returns "alpha" value:
    /// floor((constant - Sum_POS coeff * LB + Sum_NEG coeff * UB)/coeff)
    DomainElement* getAlpha(DomainElement* aCoeff, Domain* aDomain,
                            DomainElement* aPosLowerBound, DomainElement* aNegUpperBound);
    
    /// Returns "beta" value:
    /// ceil((-constant + Sum_POS coeff * LB - Sum_NEG coeff * UB)/coeff)
    DomainElement* getBeta(DomainElement* aCoeff, Domain* aDomain,
                           DomainElement* aPosLowerBound, DomainElement* aNegUpperBound);
  };
  
} // end namespace Core
