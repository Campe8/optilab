//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 12/29/2016
//
// Propagator for reif linear equations
//

#pragma once

// Base class
#include "PropagatorLinearReif.hpp"

namespace Core {
  
  class PropagatorLinearLqReif : public PropagatorLinearReif {
  public:
    PropagatorLinearLqReif(DomainElementArray& aCoeffArray,
                           DomainArray&        aDomainArray,
                           DomainElement*      aConstant,
                           DomainBoolean*      aDomBool,
                           PropagatorSemanticType aReifType = PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_LQ_REIF,
                           bool aUseGCDOnNormalization = true);
    
    ~PropagatorLinearLqReif() override = default;
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;

    /// Run inverse Lq propagation, i.e., Lq negation
    PropagationEvent runInverseLqPropagation();

  private:
    bool pUseGCDOnNormalization;
  };
  
  class PropagatorHLinLqReif : public PropagatorLinearLqReif {
  public:
    ~PropagatorHLinLqReif() override = default;

    PropagationEvent post() override;

  protected:
    PropagatorHLinLqReif(DomainElementArray& aCoeffArray,
      DomainArray&        aDomainArray,
      DomainElement*      aConstant,
      DomainBoolean*      aDomBool,
      PropagatorSemanticType aReifType,
      bool aUseGCDOnNormalization = true);

    PropagationEvent runPropagation() override;

  private:
    /// Half reification type, i.e., left or right
    PropagatorSemanticType pReifType;

    /// Returns true if half reification left
    inline bool isHalfReifLeft() const
    {
      return pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_LQ_HREIF_L;
    }

    /// Returns true if half reification right
    inline bool isHalfReifRight() const
    {
      return pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_LQ_HREIF_R;
    }
  };

  class PropagatorHLinLqReifL : public PropagatorHLinLqReif {
  public:
    PropagatorHLinLqReifL(DomainElementArray& aCoeffArray,
      DomainArray&        aDomainArray,
      DomainElement*      aConstant,
      DomainBoolean*      aDomBool,
      bool aUseGCDOnNormalization = true);

    ~PropagatorHLinLqReifL() override = default;
    
    double getFitness() override;
  };

  class PropagatorHLinLqReifR : public PropagatorHLinLqReif {
  public:
    PropagatorHLinLqReifR(DomainElementArray& aCoeffArray,
      DomainArray&        aDomainArray,
      DomainElement*      aConstant,
      DomainBoolean*      aDomBool,
      bool aUseGCDOnNormalization = true);

    ~PropagatorHLinLqReifR() override = default;
    
    double getFitness() override;
  };
} // end namespace Core
