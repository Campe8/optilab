//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 12/10/2016
//
// Propagator for linear strict in-equations
//

#pragma once

// Base class
#include "PropagatorLinear.hpp"

namespace Core {
  
  class PropagatorLinearLt : public PropagatorLinear {
  public:
    PropagatorLinearLt(DomainElementArray& aCoeffArray,
                       DomainArray&        aDomainArray,
                       DomainElement*      aConstant,
                       bool aUseGCDOnNormalization = true);
    
    ~PropagatorLinearLt() override = default;
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;

  private:
    PropagatorSemanticType pInternalPropagatorType;
  };
  
} // end namespace Core
