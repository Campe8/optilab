//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 12/16/2016
//
// Base class for binary propagators, i.e., propagators
// working on a pair of domains.
//

#pragma once

// Base class
#include "Propagator.hpp"

#include "Domain.hpp"
#include "DomainElement.hpp"

#include <utility>
#include <memory>

namespace Core {
  
  class PropagatorLinear : public Propagator {
  public:
    virtual ~PropagatorLinear();
    
  protected:
    /// Constructor for linear propagator:
    /// "aCoeffArray" * "aDomainArray" "aLinearOperatorType" "aConstant".
    /// @note "aCoeffArray.size()" == "aDomainArray.size()"
    /// @note "aCoeffArray[i]" is the coefficient of "aDomainArray[i]"
    /// for 0 <= i < "aCoeffArray.size()"
    /// @note "aDomainArray" MUST contain all different (pointers to) domains
    PropagatorLinear(DomainElementArray& aCoeffArray,
                     DomainArray&        aDomainArray,
                     DomainElement*      aConstant,
                     PropagatorStrategyType aStrategyType,
                     PropagatorSemanticType aSemanticType,
                     PropagatorSemanticClass aSemanticClass,
                     PropagationPriority aPriority = PropagationPriority::PROP_PRIORITY_LINEAR,
                     bool aUseGCDOnNormalization = true);
    
    inline DomainElement *getLinearConstant() const { return pConstant; }
    
    inline DomainElementArraySPtr getPosCoefficients() const { return pDomElemPos; }
    
    inline DomainElementArraySPtr getNegCoefficients() const { return pDomElemNeg; }
    
    inline DomainArraySPtr getPosDomains() const { return pDomPos; }
    
    inline DomainArraySPtr getNegDomains() const { return pDomNeg; }
    
    /// Returns the pair
    /// <sum(coeff * LB), sum(coeff * UB)>
    /// for positive domains
    std::pair<DomainElement*, DomainElement*> calculatePositiveBounds();
    
    /// Returns the pair
    /// <sum(coeff * LB), sum(coeff * UB)>
    /// for negative domains
    std::pair<DomainElement*, DomainElement*> calculateNegativeBounds();
    
  private:
    /// Linear constant
    DomainElement* pConstant;
    
    /// Array of positive domain elements
    DomainElementArraySPtr pDomElemPos;
    
    /// Array of negative domain elements
    DomainElementArraySPtr pDomElemNeg;
    
    /// Array of positive domains
    /// @note pDomElemPos.size() == pDomPos.size()
    /// @note pDomElemPos[i] is the coefficient of pDomPos[i]
    /// for 0 <= i < pDomElemPos.size()
    DomainArraySPtr pDomPos;
    /// Array of negative domains
    /// @note pDomElemNeg.size() == pDomNeg.size()
    /// @note pDomElemNeg[i] is the coefficient of pDomNeg[i]
    /// for 0 <= i < pDomElemNeg.size()
    DomainArraySPtr pDomNeg;
    
    /// Normalizes linear expression by:
    /// 1) eliminating zero coefficients/variables;
    /// 2) splitting coefficients into negative/positive;
    /// 3) dividing by the GCD
    /// Fills array of coefficients and domain elements.
    /// @note if all domains are singletons, this method will
    /// initialize only pConstant as follows
    /// pConstant = "aConstant" - <sum left hand expression>
    void normalizeLinearExpression(DomainElementArray& aCoeffArray,
                                   DomainArray& aDomainArray,
                                   DomainElement* aConstant,
                                   bool aUseGCDOnNormalization);
    
    /// Returns the GCD of the elements in "aCoeffArray" array
    DomainElement *findGCDOnArray(DomainElementArray& aCoeffArray);
    
    /// Returns the GCD of the two elements "aElem1" and "aElem2"
    DomainElement *findGCD(DomainElement* aElem1, DomainElement* aElem2);
    
    /// Returns "aElem1" % "aElem2"
    DomainElement *mod(DomainElement* aElem1, DomainElement* aElem2);
    
    /// Divide every value in "aDomainElementArray" by "aDomainElement" and
    /// replaces it in "aDomainElementArray
    void divideArrayByDomainElement(DomainElementArray& aDomainElementArray, DomainElement* aDomainElement);
  };
  
} // end namespace Core
