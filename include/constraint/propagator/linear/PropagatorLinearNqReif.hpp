//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 12/30/2016
//
// Propagator for reif linear equations
//

#pragma once

// Base class
#include "PropagatorLinearReif.hpp"

namespace Core {
  
  class PropagatorLinearNqReif : public PropagatorLinearReif {
  public:
    PropagatorLinearNqReif(DomainElementArray& aCoeffArray,
                           DomainArray&        aDomainArray,
                           DomainElement*      aConstant,
                           DomainBoolean*      aDomBool,
                           PropagatorSemanticType aReifType = PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_EQ_REIF,
                           bool aUseGCDOnNormalization = true);
    
    ~PropagatorLinearNqReif() override = default;
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;
  };
  
  class PropagatorHLinNqReif : public PropagatorLinearNqReif {
  public:
    ~PropagatorHLinNqReif() override = default;

    PropagationEvent post() override;

  protected:
    PropagatorHLinNqReif(DomainElementArray& aCoeffArray,
      DomainArray&        aDomainArray,
      DomainElement*      aConstant,
      DomainBoolean*      aDomBool,
      PropagatorSemanticType aReifType,
      bool aUseGCDOnNormalization = true);

    PropagationEvent runPropagation() override;

  private:
    /// Half reification type, i.e., left or right
    PropagatorSemanticType pReifType;

    /// Returns true if half reification left
    inline bool isHalfReifLeft() const
    {
      return pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_NQ_HREIF_L;
    }

    /// Returns true if half reification right
    inline bool isHalfReifRight() const
    {
      return pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_NQ_HREIF_R;
    }
  };

  class PropagatorHLinNqReifL : public PropagatorHLinNqReif {
  public:
    PropagatorHLinNqReifL(DomainElementArray& aCoeffArray,
      DomainArray&        aDomainArray,
      DomainElement*      aConstant,
      DomainBoolean*      aDomBool,
      bool aUseGCDOnNormalization = true);

    ~PropagatorHLinNqReifL() override = default;
    
    double getFitness() override;
  };

  class PropagatorHLinNqReifR : public PropagatorHLinNqReif {
  public:
    PropagatorHLinNqReifR(DomainElementArray& aCoeffArray,
      DomainArray&        aDomainArray,
      DomainElement*      aConstant,
      DomainBoolean*      aDomBool,
      bool aUseGCDOnNormalization = true);

    ~PropagatorHLinNqReifR() override = default;
    
    double getFitness() override;
  };
} // end namespace Core
