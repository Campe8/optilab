//
// Copyright OptiLab 2016-2017. All rights reserved.
//
// Created by Federico Campeotto on 12/10/2016
//
// Propagator for linear dis-equations
//

#pragma once

// Base class
#include "PropagatorLinear.hpp"

namespace Core {
  
  class PropagatorLinearNq : public PropagatorLinear {
  public:
    PropagatorLinearNq(DomainElementArray& aCoeffArray,
                       DomainArray&        aDomainArray,
                       DomainElement*      aConstant,
                       bool aUseGCDOnNormalization = true);
    
    virtual ~PropagatorLinearNq() = default;
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>()
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;
  };
  
} // end namespace Core
