//
// Copyright OptiLab 2016-2017. All rights reserved.
//
// Created by Federico Campeotto on 12/17/2016
//
// Propagator for linear equations
//

#pragma once

// Base class
#include "PropagatorLinear.hpp"

namespace Core {
  
  class PropagatorLinearEq : public PropagatorLinear {
  public:
    PropagatorLinearEq(DomainElementArray& aCoeffArray,
                       DomainArray&        aDomainArray,
                       DomainElement*      aConstant,
                       bool aUseGCDOnNormalization = true);
    
    virtual ~PropagatorLinearEq() = default;
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;
    
  private:
    /// Returns "alpha" value:
    /// floor((constant - Sum_POS coeff * LB + Sum_NEG coeff * UB)/coeff)
    DomainElement* getAlpha(DomainElement* aCoeff, Domain* aDomain,
                            DomainElement* aPosLowerBound, DomainElement* aNegUpperBound);
    
    /// Returns "beta" value:
    /// ceil((-constant + Sum_POS coeff * LB - Sum_NEG coeff * UB)/coeff)
    DomainElement* getBeta(DomainElement* aCoeff, Domain* aDomain,
                           DomainElement* aPosLowerBound, DomainElement* aNegUpperBound);
    
    /// Returns "gamma" value:
    /// ceil((constant - Sum_POS coeff * UB + Sum_NEG coeff * LB)/coeff)
    DomainElement* getGamma(DomainElement* aCoeff, Domain* aDomain,
                            DomainElement* aPosUpperBound, DomainElement* aNegLowerBound);
    
    /// Returns "delta" value:
    /// floor((-constant + Sum_POS coeff * UB - Sum_NEG coeff * LB)/coeff)
    DomainElement* getDelta(DomainElement* aCoeff, Domain* aDomain,
                            DomainElement* aPosUpperBound, DomainElement* aNegLowerBound);
  };
  
} // end namespace Core
