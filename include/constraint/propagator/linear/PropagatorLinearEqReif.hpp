//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 12/29/2016
//
// Propagator for reif linear equations
//

#pragma once

// Base class
#include "PropagatorLinearReif.hpp"

namespace Core {
  
  class PropagatorLinearEqReif : public PropagatorLinearReif {
  public:
    PropagatorLinearEqReif(DomainElementArray& aCoeffArray,
                           DomainArray&        aDomainArray,
                           DomainElement*      aConstant,
                           DomainBoolean*      aDomBool,
                           PropagatorSemanticType aReifType = PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_EQ_REIF,
                           bool aUseGCDOnNormalization = true);
    
    ~PropagatorLinearEqReif() override = default;
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;
  };
  
  class PropagatorHLinEqReif : public PropagatorLinearEqReif {
  public:
    ~PropagatorHLinEqReif() override = default;

    PropagationEvent post() override;

  protected:
    PropagatorHLinEqReif(DomainElementArray& aCoeffArray,
      DomainArray&        aDomainArray,
      DomainElement*      aConstant,
      DomainBoolean*      aDomBool,
      PropagatorSemanticType aReifType,
      bool aUseGCDOnNormalization = true);
    
    PropagationEvent runPropagation() override;

  private:
    /// Half reification type, i.e., left or right
    PropagatorSemanticType pReifType;

    /// Returns true if half reification left
    inline bool isHalfReifLeft() const
    {
      return pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_EQ_HREIF_L;
    }

    /// Returns true if half reification right
    inline bool isHalfReifRight() const
    {
      return pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_EQ_HREIF_R;
    }
  };

  class PropagatorHLinEqReifL : public PropagatorHLinEqReif {
  public:
    PropagatorHLinEqReifL(DomainElementArray& aCoeffArray,
      DomainArray&        aDomainArray,
      DomainElement*      aConstant,
      DomainBoolean*      aDomBool,
      bool aUseGCDOnNormalization = true);

    ~PropagatorHLinEqReifL() override = default;
    
    double getFitness() override;
  };

  class PropagatorHLinEqReifR : public PropagatorHLinEqReif {
  public:
    PropagatorHLinEqReifR(DomainElementArray& aCoeffArray,
      DomainArray&        aDomainArray,
      DomainElement*      aConstant,
      DomainBoolean*      aDomBool,
      bool aUseGCDOnNormalization = true);

    ~PropagatorHLinEqReifR() override = default;
    
    double getFitness() override;
  };
} // end namespace Core
