//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 12/20/2016
//
// Abs (i.e., absolute value) propagator:
// |D0| D1
//

#pragma once

// Base class
#include "PropagatorBinary.hpp"

#include "Domain.hpp"

// Forward declaration
namespace Core {
  class DomainSpecular;
}// end namespace Core

namespace Core {
  
  class PropagatorAbs : public PropagatorBinary {
  public:
    virtual ~PropagatorAbs();
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
  protected:
    PropagatorAbs(Domain *aDom0, Domain *aDom1,
      PropagatorStrategyType aStrategyType,
      PropagationPriority aPriority = PropagationPriority::PROP_PRIORITY_BINARY);

    PropagationEvent runPropagation() override;

  private:
    /// Use EQ propagation on positive domains
    bool pUsePositivePropagation;

    /// Internal propagator type to be used for
    /// positive domains
    PropagatorSemanticType pInternalPropagatorType;

    /// Strategy to be used for internal propagator
    PropagatorStrategyType pPropagatorStrategy;

    /// Specular domain used for D0 to be used when D0
    /// is a negative domain
    std::unique_ptr<DomainSpecular> pDomainSpecular;
  };
  
  class PropagatorAbsBound : public PropagatorAbs {
  public:
    PropagatorAbsBound(Domain *aDom0, Domain *aDom1);

    virtual ~PropagatorAbsBound();
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
  };

  class PropagatorAbsDomain : public PropagatorAbs {
  public:
    PropagatorAbsDomain(Domain *aDom0, Domain *aDom1);

    virtual ~PropagatorAbsDomain();
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>(),
        std::make_shared<DomainEventChange>()
      };
      return eventSet;
    }
    
  };

} // end namespace Core
