//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 12/21/2016
//
// Division (i.e., prod) propagator:
// D0 / D1 = D2
//

#pragma once

// Base class
#include "PropagatorTernary.hpp"

#include "Domain.hpp"

namespace Core {
  
  class PropagatorDiv : public PropagatorTernary {
  public:
    PropagatorDiv(Domain *aDom0, Domain *aDom1, Domain *aDom2);
    
    virtual ~PropagatorDiv();
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;
    
  private:
    /// Run times propagation when 2 or 3 domains
    /// in the current context are singletons
    PropagationEvent runEasyPropagation(std::size_t aNumSingletons);
    
    /// Apply TIMES filtering algorithm s.t.
    /// aD2 = aD0 * aD1
    /// where aD0 and aD1 must be indexes of singleton context domains.
    /// @note aD0, aD1, and aD2 must be in {D0, D1, D2}
    Domain* filterTIMESFromSingleton(std::size_t aD2, std::size_t aD0, std::size_t aD1);
    
    /// Apply DIVIDE filtering algorithm s.t.
    /// aD2 = aD0 / aD1
    /// where aD0 and aD1 must be indexes of singleton context domains.
    /// @note aD0, aD1, and aD2 must be in {D0, D1, D2}
    Domain* filterDIVIDEFromSingleton(std::size_t aD2, std::size_t aD0, std::size_t aD1);
    
    /// Post times propagator on given domains
    /// @note "aDom0", "aDom1", and "aDom2" must be positive domains
    PropagationEvent postOnPositiveDomains(Domain* aDom0, Domain* aDom1, Domain* aDom2);

    /// Run Times propagator for
    /// "aDom2" = "aDom0" * "aDom1"
    /// where
    /// 1 - all domains are NOT singleton
    /// 2 - all domains are positive
    PropagationEvent runPropagatorDivOnPositiveDomains(Domain* aDom0, Domain* aDom1, Domain* aDom2);
  };

} // end namespace Core
