//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 12/23/2016
//
// Division (i.e., prod) propagator:
// max(D0, D1) = D2
//

#pragma once

// Base class
#include "PropagatorTernary.hpp"

#include "Domain.hpp"

namespace Core {
  
  class PropagatorMax : public PropagatorTernary {
  public:
    virtual ~PropagatorMax();
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
  protected:    
    PropagatorMax(Domain *aDom0, Domain *aDom1, Domain *aDom2,
      PropagatorStrategyType aStrategyType,
      PropagationPriority aPropagatorPriority);

    inline bool useInternalPropagator() const
    {
      return pUseInternalPropagator;
    }

    inline PropagatorSemanticType getInternalPropagatorType() const
    {
      return pInternalPropagatorType;
    }

  private:
    /// Flags for internal propagator use
    bool pUseInternalPropagator;
    /// Internal propagator type to use
    PropagatorSemanticType pInternalPropagatorType;
  };
  
  class PropagatorMaxBound : public PropagatorMax {
  public:
    PropagatorMaxBound(Domain *aDom0, Domain *aDom1, Domain *aDom2);

    virtual ~PropagatorMaxBound();

    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;
  };

  class PropagatorMaxDomain : public PropagatorMax {
  public:
    PropagatorMaxDomain(Domain *aDom0, Domain *aDom1, Domain *aDom2);

    virtual ~PropagatorMaxDomain();

    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>(),
        std::make_shared<DomainEventChange>()
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;
  };

} // end namespace Core
