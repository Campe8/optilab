//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/29/2016
//
// Times (i.e., prod) propagator:
// D0 x D1 = D2
//

#pragma once

// Base class
#include "PropagatorTernary.hpp"

#include "Domain.hpp"

namespace Core {
  
  class PropagatorTimes : public PropagatorTernary {
  public:
    virtual ~PropagatorTimes();
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
  protected:
    PropagatorTimes(Domain *aDom0, Domain *aDom1, Domain *aDom2,
                    PropagatorStrategyType aStrategyType,
                    PropagationPriority aPriority=PropagationPriority::PROP_PRIORITY_TERNARY);
    
    PropagationEvent runPropagation() override;
    
    /// Run Times propagator for
    /// "aDom2" = "aDom0" * "aDom1"
    /// where
    /// 1 - all domains are NOT singleton
    /// 2 - all domains are positive
    virtual PropagationEvent runPropagatorTimesOnPositiveDomains(Domain* aDom0, Domain* aDom1, Domain* aDom2) = 0;
    
  private:
    /// Run times propagation when 2 or 3 domains
    /// in the current context are singletons
    PropagationEvent runEasyPropagation(std::size_t aNumSingletons);
    
    /// Apply TIMES filtering algorithm s.t.
    /// aD2 = aD0 * aD1
    /// where aD0 and aD1 must be indexes of singleton context domains.
    /// @note aD0, aD1, and aD2 must be in {D0, D1, D2}
    Domain* filterTIMESFromSingleton(std::size_t aD2, std::size_t aD0, std::size_t aD1);
    
    /// Apply DIVIDE filtering algorithm s.t.
    /// aD2 = aD0 / aD1
    /// where aD0 and aD1 must be indexes of singleton context domains.
    /// @note aD0, aD1, and aD2 must be in {D0, D1, D2}
    Domain* filterDIVIDEFromSingleton(std::size_t aD2, std::size_t aD0, std::size_t aD1);
    
    /// Post times propagator on given domains
    /// @note "aDom0", "aDom1", and "aDom2" must be positive domains
    PropagationEvent postOnPositiveDomains(Domain* aDom0, Domain* aDom1, Domain* aDom2);
  };
  
  class PropagatorTimesBound : public PropagatorTimes {
  public:
    PropagatorTimesBound(Domain *aDom0, Domain *aDom1, Domain *aDom2);
    
    virtual ~PropagatorTimesBound();
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagatorTimesOnPositiveDomains(Domain* aDom0, Domain* aDom1, Domain* aDom2) override;
  };
  
  class PropagatorTimesDomain : public PropagatorTimes {
  public:
    PropagatorTimesDomain(Domain *aDom0, Domain *aDom1, Domain *aDom2);
    
    virtual ~PropagatorTimesDomain();
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>(),
        std::make_shared<DomainEventChange>()
      };      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagatorTimesOnPositiveDomains(Domain* aDom0, Domain* aDom1, Domain* aDom2) override;
  };
  
} // end namespace Core
