//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 12/23/2016
//
// Division (i.e., prod) propagator:
// min(D0, D1) = D2
//

#pragma once

// Base class
#include "PropagatorTernary.hpp"

#include "Domain.hpp"

namespace Core {
  
  class PropagatorMin : public PropagatorTernary {
  public:
    virtual ~PropagatorMin();
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
  protected:    
    PropagatorMin(Domain *aDom0, Domain *aDom1, Domain *aDom2,
      PropagatorStrategyType aStrategyType,
      PropagationPriority aPropagatorPriority);

    inline bool useInternalPropagator() const
    {
      return pUseInternalPropagator;
    }

    inline PropagatorSemanticType getInternalPropagatorType() const
    {
      return pInternalPropagatorType;
    }

  private:
    /// Flags for internal propagator use
    bool pUseInternalPropagator;
    /// Internal propagator type to use
    PropagatorSemanticType pInternalPropagatorType;
  };
  
  class PropagatorMinBound : public PropagatorMin {
  public:
    PropagatorMinBound(Domain *aDom0, Domain *aDom1, Domain *aDom2);

    virtual ~PropagatorMinBound();
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;
  };

  class PropagatorMinDomain : public PropagatorMin {
  public:
    PropagatorMinDomain(Domain *aDom0, Domain *aDom1, Domain *aDom2);

    virtual ~PropagatorMinDomain();
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>(),
        std::make_shared<DomainEventChange>()
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;
  };

} // end namespace Core
