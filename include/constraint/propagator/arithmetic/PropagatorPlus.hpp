//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/29/2016
//
// Plus propagator:
// D0 + D1 = D2
//

#pragma once

// Base class
#include "PropagatorTernary.hpp"

#include "Domain.hpp"

namespace Core {
  
  class PropagatorPlus : public PropagatorTernary {
  public:
    double getFitness() override;
    
    PropagationEvent post() override;
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
  protected:
    PropagatorPlus(Domain *aDom0, Domain *aDom1, Domain *aDom2,
                   PropagatorStrategyType aStrategyType,
                   PropagationPriority aPriority=PropagationPriority::PROP_PRIORITY_TERNARY);
    
    PropagationEvent runPropagation() override;
    
    /// Run Times propagator for
    /// "aDom2" = "aDom0" + "aDom1"
    /// where
    /// 1) there is at most one singleton domain
    virtual PropagationEvent runPropagatorPlus(Domain* aDom0, Domain* aDom1, Domain* aDom2) = 0;
    
  private:
    /// Run times propagation when 2 or 3 domains
    /// in the current context are singletons
    PropagationEvent runEasyPropagation(std::size_t aNumSingletons);
  };
  
  class PropagatorPlusBound : public PropagatorPlus {
  public:
    PropagatorPlusBound(Domain *aDom0, Domain *aDom1, Domain *aDom2);
    
    virtual ~PropagatorPlusBound();
    
  protected:
    PropagationEvent runPropagatorPlus(Domain* aDom0, Domain* aDom1, Domain* aDom2) override;
  };
  
  class PropagatorPlusDomain : public PropagatorPlus {
  public:
    PropagatorPlusDomain(Domain *aDom0, Domain *aDom1, Domain *aDom2);
    
    virtual ~PropagatorPlusDomain();
    
  protected:
    PropagationEvent runPropagatorPlus(Domain* aDom0, Domain* aDom1, Domain* aDom2) override;
  };
  
} // end namespace Core
