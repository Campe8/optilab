//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/28/2016
//
// Base class for ternary propagators, i.e., propagators
// working on a triple of domains.
//

#pragma once

// Base class
#include "Propagator.hpp"
#include "Domain.hpp"
#include <memory>

namespace Core {
  
  class PropagatorTernary : public Propagator {
  protected:
    enum TernaryDomainSignConfig : int {
        TDSC_PPP = 0 // D0 > 0 /\ D1 > 0 /\ D2 > 0
      , TDSC_PPN     // D0 > 0 /\ D1 > 0 /\ D2 < 0
      , TDSC_PPX     // D0 > 0 /\ D1 > 0 /\ D2.LB <= 0 /\ D2.UB >= 0
      , TDSC_PNP     // D0 > 0 /\ D1 < 0 /\ D2 > 0
      , TDSC_PNN     // D0 > 0 /\ D1 < 0 /\ D2 < 0
      , TDSC_PNX     // D0 > 0 /\ D1 < 0 /\ D2.LB <= 0 /\ D2.UB >= 0
      , TDSC_PXP     // D0 > 0 /\ D1.LB <= 0 /\ D1.UB >= 0 /\ D2 > 0
      , TDSC_PXN     // D0 > 0 /\ D1.LB <= 0 /\ D1.UB >= 0 /\ D2 < 0
      , TDSC_PXX     // D0 > 0 /\ D1.LB <= 0 /\ D1.UB >= 0/\ D2.LB <= 0 /\ D2.UB >= 0
      , TDSC_NPP     // D0 < 0 /\ D1 > 0 /\ D2 > 0
      , TDSC_NPN     // D0 < 0 /\ D1 > 0 /\ D2 < 0
      , TDSC_NPX     // D0 < 0 /\ D1 > 0 /\ D2.LB <= 0 /\ D2.UB >= 0
      , TDSC_NNP     // D0 < 0 /\ D1 < 0 /\ D2 > 0
      , TDSC_NNN     // D0 < 0 /\ D1 < 0 /\ D2 < 0
      , TDSC_NNX     // D0 < 0 /\ D1 < 0 /\ D2.LB <= 0 /\ D2.UB >= 0
      , TDSC_NXP     // D0 < 0 /\ D1.LB <= 0 /\ D1.UB >= 0 /\ D2 > 0
      , TDSC_NXN     // D0 < 0 /\ D1.LB <= 0 /\ D1.UB >= 0 /\ D2 < 0
      , TDSC_NXX     // D0 < 0 /\ D1.LB <= 0 /\ D1.UB >= 0/\ D2.LB <= 0 /\ D2.UB >= 0
      , TDSC_XPP     // D0.LB <= 0 /\ D0.UB >= 0 /\ D1 > 0 /\ D2 > 0
      , TDSC_XPN     // D0.LB <= 0 /\ D0.UB >= 0 /\ D1 > 0 /\ D2 < 0
      , TDSC_XPX     // D0.LB <= 0 /\ D0.UB >= 0 /\ D1 > 0 /\ D2.LB <= 0 /\ D2.UB >= 0
      , TDSC_XNP     // D0.LB <= 0 /\ D0.UB >= 0 /\ D1 < 0 /\ D2 > 0
      , TDSC_XNN     // D0.LB <= 0 /\ D0.UB >= 0 /\ D1 < 0 /\ D2 < 0
      , TDSC_XNX     // D0.LB <= 0 /\ D0.UB >= 0 /\ D1 < 0 /\ D2.LB <= 0 /\ D2.UB >= 0
      , TDSC_XXP     // D0.LB <= 0 /\ D0.UB >= 0 /\ D1.LB <= 0 /\ D1.UB >= 0 /\ D2 > 0
      , TDSC_XXN     // D0.LB <= 0 /\ D0.UB >= 0 /\ D1.LB <= 0 /\ D1.UB >= 0 /\ D2 < 0
      , TDSC_XXX     // D0.LB <= 0 /\ D0.UB >= 0 /\ D1.LB <= 0 /\ D1.UB >= 0/\ D2.LB <= 0 /\ D2.UB >= 0
    };
    
  public:
    virtual ~PropagatorTernary();
  
  protected:
    PropagatorTernary(Domain *aDom0, Domain *aDom1, Domain *aDom2,
                      PropagatorStrategyType aStrategyType,
                      PropagatorSemanticType aSemanticType,
                      PropagatorSemanticClass aSemanticClass,
                      PropagationPriority aPriority = PropagationPriority::PROP_PRIORITY_TERNARY);
  
    /// Get sign configuration for the domains in this propagator
    TernaryDomainSignConfig getTernaryDomainSignConfig() const;
  };
  
} // end namespace Core
