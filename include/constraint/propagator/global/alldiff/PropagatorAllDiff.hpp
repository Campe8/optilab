//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 1/25/2018
//
// Propagator for bound consistency of the alldifferent constraint.
// This propagator is based on
// A. Lopez-Ortiz, C.-G. Quimper, J. Tromp, and P.  van Beek.
// A fast and simple algorithm for bounds consistency of the
// alldifferent constraint. IJCAI-2003.
//

#pragma once

// Base class
#include "PropagatorArray.hpp"

#include "DomainElementInt64.hpp"

namespace Core {
  
  class PropagatorAllDiff : public PropagatorArray {
  public:
    PropagatorAllDiff(DomainArray& aDomArray);
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
          std::make_shared<DomainEventSingleton>()
        , std::make_shared<DomainEventLwb>()
        , std::make_shared<DomainEventUpb>()
        , std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;
  
  private:
    using DomainIdxPair = std::pair<Core::Domain*, std::size_t>;
    
    // stores (pointers to) the domains sorted in ascending order
    // w.r.t. lower bounds
    std::vector<DomainIdxPair> minSortedDomains;
    
    // stores (pointers to) the domains sorted in ascending order
    // w.r.t. upper bounds
    std::vector<DomainIdxPair> maxSortedDomains;
    
    // sort comparing lower bounds
    static bool compareLowerBounds(const DomainIdxPair& aPair1, const DomainIdxPair& aPair2)
    {
      return DomainElement::lessThan(aPair1.first->lowerBound(), aPair2.first->lowerBound());
    }
    
    // sort comparing upper bounds
    static bool compareUpperBounds(const DomainIdxPair& aPair1, const DomainIdxPair& aPair2)
    {
      return DomainElement::lessThan(aPair1.first->upperBound(), aPair2.first->upperBound());
    }
    
    // stores information about Hall intervals
    struct Hall {
      DomainElement* bounds;
      int crit;                    // index of the critical capacity value
      DomainElement* diff;         // difference between subsequent critical capacities
      int index;                   // index of the Hall interval
    };
    
    struct Rank {
      int min;
      int max;
    };
    
    // the following methods are used for path compression
    void pathset_crit(std::vector<Hall>& hall, int start, int end, int to)
    {
      int k, l;
      for(l=start; (k=l) != end; hall[k].crit=to)
        l = hall[k].crit;
    }
    
    void pathset_index(std::vector<Hall>& hall, int start, int end, int to)
    {
      int k, l;
      for(l=start; (k=l) != end; hall[k].index=to)
        l = hall[k].index;
    }
    
    int pathmax_index(std::vector<Hall>& hall, int i)
    {
      while(hall[i].index > i)
        i = hall[i].index;
      return i;
    }
    
    int pathmax_crit(std::vector<Hall>& hall, int i)
    {
      while(hall[i].crit > i)
        i = hall[i].crit;
      return i;
    }
    
    int pathmin_index(std::vector<Hall>& hall, int i) {
      while (hall[i].index < i)
        i = hall[i].index;
      return i;
    }

    int pathmin_crit(std::vector<Hall>& hall, int i) {
      while (hall[i].crit < i)
        i = hall[i].crit;
      return i;
    }
  };
  
} // end namespace Core
