//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Luca Foschiani on 4/26/2018
//
// Domain propagator:
// LB <= a[i] <= UP   for every i
// This is the propagator for the "domain" 
// global constraint as described in
// http://sofdem.github.io/gccat/gccat/Cdomain.html#uid20109.
// Example:
// X in [0..2], Y in [3..3]
// domain([X,Y],1,9)
// gives the following solutions:
// 1) X = 1, Y = 3;
// 1) X = 2, Y = 3.
//

#pragma once

// Base class

#include "Propagator.hpp"
#include "PropagatorArray.hpp"

#include "Domain.hpp"
#include "DomainElement.hpp"

namespace Core {
  
  class PropagatorDomain : public PropagatorArray {
  public:
    PropagatorDomain(DomainArray& aDomArray,
                     DomainElement* aLowerBound,
                     DomainElement* aUpperBound);
    
    ~PropagatorDomain() override = default;
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
    // this shouldn't be needed, this constraint is always subsumed (or fails) after 1 propagation
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;
    
  private:
    inline DomainElement* getLB() const
    {
      return pLB;
    }

    inline DomainElement* getUB() const
    {
      return pUB;
    }
    
    DomainElement* pLB;
    DomainElement* pUB;
  };
  
} // end namespace Core
