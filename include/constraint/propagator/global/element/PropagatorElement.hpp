//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 3/2/2017
//
// Element propagator:
// b in 1..n /\ as[b] = c
// where
// n is the length of "as".
// This is the propagator for the "element" 
// global constraint as described in
// http://sofdem.github.io/gccat/gccat/Celement.html#uid20367.
// Please, refer to the above page for further information.
// @note the array is 1-based.
// Example:
// I in [3, 6], V in [1, 9], 
// element(I,[4, 8, 1, 0, 3, 3, 4, 3], V)
// gives the following solutions:
// 1) I = 3, V = 1;
// 2) I = 5, V = 3;
// 3) I = 6, V = 3.
//

#pragma once

// Base class
#include "PropagatorArray.hpp"

#include "Domain.hpp"
#include "DArrayMatrix.hpp"
#include "DomainElement.hpp"

namespace Core {
  
  class PropagatorElement : public PropagatorArray {
  public:
    /// Constructor for "aDomArray[aDomIdx] = aDom",
    /// where "aDomArray" is an array of domains of the same type of "aDom"
    PropagatorElement(Domain *aDomIdx, DomainArray& aDomArray, Domain *aDom);
    
    /// Constructor for "aDomElemArray[aDomIdx] = aDom",
    /// where "aDomElemArray" is an array of domain elements of the same type of
    /// the elements in "aDom"
    PropagatorElement(Domain *aDomIdx, DomainElementArray& aDomElemArray, Domain *aDom);
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;

  private:
    /// Returns domain used for indexing the array
    inline Domain* getIndexingDomain() const
    {
      return getContextDomain(D0);
    }

    /// Returns domain being assigned
    inline Domain* getAssignmentDomain() const
    {
      return getContextDomain(D1);
    }

    /// Filters indexing domain according to the SINGLETON value
    /// or lower/upper bounds of the assignment domain
    PropagationEvent filterIndexingDomain();

    /// Filters assignment domain to be within the min(max) of
    /// all the lower(upper) bounds in the array
    PropagationEvent filterAssignmentDomain();
    
    /// Filters the table domains whose index is present in the
    /// indexing domain to be within the bounds of the assignment
    /// domain
    PropagationEvent filterTableDomains();
  };
  
} // end namespace Core
