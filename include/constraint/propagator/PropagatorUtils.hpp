//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/29/2016
//
// Utility function for propagators.
//

#pragma once

// Export definitions
#include "ConstraintExportDefs.hpp"

#include "DArrayMatrix.hpp"

// Forward declarations
namespace Core {
  class Domain;
  class DomainElement;
}

namespace Core { namespace PropagatorUtils {
  
  /// Returns b, used for std algorithm functions
  /// such as "count_if"
  inline bool isTrue (bool b) { return b; }
  
  /// Returns !b, used for std algorithm functions
  /// such as "count_if"
  inline bool isFalse (bool b) { return !b; }
  
  /// Returns true if the given domain is empty,
  /// false otherwise
  bool isDomainEmpty(const Domain *aDomain);
  
  /// Returns true if the given domain is singleton,
  /// false otherwise
  bool isDomainSingleton(const Domain *aDomain);
  
  /// Returns true if the given domain has the lower bound
  /// which is greater than the zero element,
  /// false otherwise
  bool isDomainPositive(const Domain *aDomain);
  
  /// Returns true if the given domain has the upper bound
  /// which is less than the zero element,
  /// false otherwise
  bool isDomainNegative(const Domain *aDomain);
  
  /// Returns true if the given domain has the lower bound
  /// which is less than the zero element, and the upper bound
  /// greater than zero, false otherwise
  bool isDomainNegativePositive(const Domain *aDomain);
  
  /// Returns the DomainElement if the domain is
  /// singleton, DomainElementVoid otherwise
  DomainElement* getSingleton(const Domain *aDomain);
  
  /// Returns a DomainElementArraySPtr containing
  /// the elements in "aDomainElementArray"
  DomainElementArraySPtr createDomainElementArraySPtr(std::vector<DomainElement*>&& aDomainElementArray);
  
  /// Returns true if "aDomain1" == "aDomain2" w.r.t. bounds,
  /// false otherwise
  bool sameBounds(const Domain *aDomain1, const Domain* aDomain2);
  
  /// Returns true if "aDomain1" == "aDomain2" w.r.t. domain elements,
  /// false otherwise.
  /// @note this is an expensive operation and it should be used with caution
  bool sameDomains(const Domain *aDomain1, const Domain* aDomain2);
  
  /// Returns an array union of "aDomain1" and "aDomain2".
  /// @note this is an expensive operation and it should be used with caution
  DomainElementArraySPtr getDomainUnionArraySPtr(const Domain* aDomain1, const Domain* aDomain2);
  
  /// Returns the fitness value corresponding to the given domain element.
  /// @note assert if "aElement" is nullptr
  double toFitness(DomainElement* aElement);
  
}} // end namespace Core::PropagatorUtils
