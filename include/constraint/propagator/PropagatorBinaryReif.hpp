//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 12/15/2016
//
// Base class for binary propagators, i.e., propagators
// working on a pair of domains.
//

#pragma once

// Base class
#include "PropagatorBinary.hpp"
#include "PropagatorReif.hpp"

#include "DomainBoolean.hpp"

namespace Core {
  
  class PropagatorBinaryReif : public PropagatorBinary, public PropagatorReif {
  public:
    virtual ~PropagatorBinaryReif();
    
  protected:
    PropagatorBinaryReif(Domain *aDom0,
                         Domain *aDom1,
                         DomainBoolean *aDomBool,
                         PropagatorStrategyType aStrategyType,
                         PropagatorSemanticType aSemanticType,
                         PropagatorSemanticClass aSemanticClass,
                         PropagationPriority aPriority = PropagationPriority::PROP_PRIORITY_TERNARY);
  };
  
} // end namespace Core
