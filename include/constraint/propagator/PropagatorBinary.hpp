//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 12/05/2016
//
// Base class for binary propagators, i.e., propagators
// working on a pair of domains.
//

#pragma once

// Base class
#include "Propagator.hpp"

#include "Domain.hpp"

#include <memory>

namespace Core {
  
  class PropagatorBinary : public Propagator {
  protected:
    enum BinaryDomainSignConfig : int {
        BDSC_PP = 0 // D0 > 0 /\ D1 > 0
      , BDSC_PN     // D0 > 0 /\ D1 < 0
      , BDSC_PX     // D0 > 0 /\ D1.LB <= 0 /\ D1.UB >= 0
      , BDSC_NP     // D0 < 0 /\ D1 < 0
      , BDSC_NN     // D0 < 0 /\ D1 < 0 /\ D2 < 0
      , BDSC_NX     // D0 < 0 /\ D1.LB <= 0 /\ D1.UB >= 0
      , BDSC_XP     // D0.LB <= 0 /\ D0.UB >= 0 /\ D1 > 0
      , BDSC_XN     // D0.LB <= 0 /\ D0.UB >= 0 /\ D1 < 0
      , BDSC_XX     // D0.LB <= 0 /\ D0.UB >= 0 /\ D1.LB <= 0 /\ D1.UB >= 0
    };
    
  public:
    virtual ~PropagatorBinary();
    
  protected:
    PropagatorBinary(Domain *aDom0, Domain *aDom1,
                     PropagatorStrategyType aStrategyType,
                     PropagatorSemanticType aSemanticType,
                     PropagatorSemanticClass aSemanticClass,
                     PropagationPriority aPriority = PropagationPriority::PROP_PRIORITY_BINARY);
  
    /// Get sign configuration for the domains in this propagator
    BinaryDomainSignConfig getBinaryDomainSignConfig() const;
  };
  
} // end namespace Core
