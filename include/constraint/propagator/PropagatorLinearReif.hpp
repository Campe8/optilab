//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 12/29/2016
//
// Base class for binary propagators, i.e., propagators
// working on a pair of domains.
//

#pragma once

// Base class
#include "PropagatorLinear.hpp"
#include "PropagatorReif.hpp"

#include "DomainBoolean.hpp"

namespace Core {

    class PropagatorLinearReif : public PropagatorLinear, public PropagatorReif {
    public:
      virtual ~PropagatorLinearReif();

    protected:
      PropagatorLinearReif(DomainElementArray& aCoeffArray,
                           DomainArray&        aDomainArray,
                           DomainElement*      aConstant,
                           DomainBoolean*      aDomBool,
                           PropagatorStrategyType aStrategyType,
                           PropagatorSemanticType aSemanticType,
                           PropagatorSemanticClass aSemanticClass, 
                           PropagationPriority aPriority = PropagationPriority::PROP_PRIORITY_LINEAR,
                           bool aUseGCDOnNormalization = true);

      /// Returns true if all domains in the linear propagator are singleton,
      /// false otherwise
      bool areAllDomainsSingleton();

      /// Returns the domain element corresponding to 
      /// the left side of the expression calculated 
      /// by summing up positive and negative lower bounds
      /// of domains multiplied by their correspondent coefficients
      DomainElement* getLeftSideExpressionValue();
    };

} // end namespace Core
