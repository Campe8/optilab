//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 3/3/2017
//
// Bool Eq (i.e., equaivalence) propagator:
// D0 == D1
// and 
// Bool Eq (half) reif:
// (D0 == D1) <-> r
// Helper class using eq propagator.
//

#pragma once

// Base class
#include "PropagatorBinary.hpp"

#include "DomainBoolean.hpp"

namespace Core {
  
  class PropagatorBoolEq : public PropagatorBinary {
  public:
    PropagatorBoolEq(DomainBoolean *aDom0, DomainBoolean *aDom1);
    
    ~PropagatorBoolEq() override = default;
    
    double getFitness() override;
    
    inline PropagationEvent post() override
    {
      return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ);
    }
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
  protected:
    inline PropagationEvent runPropagation() override
    {
      return runInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ);
    }
  };
  
  class PropagatorBoolEqReif : public PropagatorBinary {
  public:
    PropagatorBoolEqReif(DomainBoolean *aDom0, DomainBoolean *aDom1, DomainBoolean *aReif);

    ~PropagatorBoolEqReif() override = default;
    
    double getFitness() override;
    
    inline PropagationEvent post() override
    {
      return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ_REIF);
    }

    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet{
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }

  protected:
    inline PropagationEvent runPropagation() override
    {
      return runInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ_REIF);
    }
  };
  
  class PropagatorBoolEqHReifL : public PropagatorBinary {
  public:
    PropagatorBoolEqHReifL(DomainBoolean *aDom0, DomainBoolean *aDom1, DomainBoolean *aReif);

    ~PropagatorBoolEqHReifL() override = default;

    double getFitness() override;
    
    inline PropagationEvent post() override
    {
      return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ_HREIF_L);
    }

    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet{
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }

  protected:
    inline PropagationEvent runPropagation() override
    {
      return runInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ_HREIF_L);
    }
  };

  class PropagatorBoolEqHReifR : public PropagatorBinary {
  public:
    PropagatorBoolEqHReifR(DomainBoolean *aDom0, DomainBoolean *aDom1, DomainBoolean *aReif);

    ~PropagatorBoolEqHReifR() override = default;

    double getFitness() override;
    
    inline PropagationEvent post() override
    {
      return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ_HREIF_R);
    }

    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet{
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }

  protected:
    inline PropagationEvent runPropagation() override
    {
      return runInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ_HREIF_R);
    }
  };

} // end namespace Core
