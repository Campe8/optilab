//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 3/2/2017
//
// Bool Lq (i.e., inequality) propagator:
// !D0 \/ D1
//

#pragma once

// Base class
#include "PropagatorBinary.hpp"

#include "DomainBoolean.hpp"

namespace Core {
  
  class PropagatorBoolLq : public PropagatorBinary {
  public:
    PropagatorBoolLq(DomainBoolean *aDom0, DomainBoolean *aDom1);
    
    ~PropagatorBoolLq() override = default;
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;
  };
  
} // end namespace Core
