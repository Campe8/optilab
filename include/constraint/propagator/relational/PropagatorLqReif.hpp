//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 12/29/2016
//
// Lq reif (i.e., disequality reif) propagator:
// (D0 <= D1) <-> b
//

#pragma once

// Base class
#include "PropagatorBinaryReif.hpp"

#include "Domain.hpp"

namespace Core {
  
  class PropagatorLqReif : public PropagatorBinaryReif {
  public:
    PropagatorLqReif(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif, PropagatorSemanticType aSemanticType = PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ_REIF);

    virtual ~PropagatorLqReif();
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;
  };

  class PropagatorHLqReif : public PropagatorLqReif {
  public:
    virtual ~PropagatorHLqReif();

    double getFitness() override;

    PropagationEvent post() override;

  protected:
    PropagatorHLqReif(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif, PropagatorSemanticType aReifType);

    PropagationEvent runPropagation() override;

  private:
    /// Half reification type, i.e., left or right
    PropagatorSemanticType pReifType;

    /// Returns true if half reification left
    inline bool isHalfReifLeft() const
    {
      return pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ_HREIF_L;
    }

    /// Returns true if half reification right
    inline bool isHalfReifRight() const
    {
      return pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ_HREIF_R;
    }
  };

  class PropagatorLqHReifL : public PropagatorHLqReif {
  public:
    PropagatorLqHReifL(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif);

    virtual ~PropagatorLqHReifL();
  };

  class PropagatorLqHReifR : public PropagatorHLqReif {
  public:
    PropagatorLqHReifR(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif);

    virtual ~PropagatorLqHReifR();
  };

} // end namespace Core
