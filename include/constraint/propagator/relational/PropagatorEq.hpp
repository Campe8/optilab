//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 12/07/2016
//
// Eq (i.e., equality) propagator:
// D0 == D1
//

#pragma once

// Base class
#include "PropagatorBinary.hpp"

#include "Domain.hpp"

namespace Core {
  
  class PropagatorEq : public PropagatorBinary {
  public:
    virtual ~PropagatorEq() override = default;
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
  protected:
    PropagatorEq(Domain *aDom0, Domain *aDom1,
                 PropagatorStrategyType aStrategyType,
                 PropagationPriority aPriority = PropagationPriority::PROP_PRIORITY_BINARY);
  };
  
  class PropagatorEqBound : public PropagatorEq {
  public:
    PropagatorEqBound(Domain *aDom0, Domain *aDom1);
    
    virtual ~PropagatorEqBound() = default;
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;
  };
  
  class PropagatorEqDomain : public PropagatorEq {
  public:
    PropagatorEqDomain(Domain *aDom0, Domain *aDom1);
    
    virtual ~PropagatorEqDomain() = default;
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>(),
        std::make_shared<DomainEventChange>()
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;
  };
  
} // end namespace Core
