//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 3/3/2017
//
// Bool Not (i.e., not) propagator:
// !D0 == D1
//

#pragma once

// Base class
#include "PropagatorBinary.hpp"

#include "DomainBoolean.hpp"

namespace Core {
  
  class PropagatorBoolNot : public PropagatorBinary {
  public:
    PropagatorBoolNot(DomainBoolean *aDom0, DomainBoolean *aDom1);
    
    ~PropagatorBoolNot() override = default;
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;
  };
  
} // end namespace Core
