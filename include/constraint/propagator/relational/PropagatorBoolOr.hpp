//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 3/2/2017
//
// Bool Or propagator:
// D0 \/ D1 binary version
// (D0 \/ D1) <-> b ternary version
//

#pragma once

// Base class
#include "PropagatorBinary.hpp"
#include "PropagatorBinaryReif.hpp"

namespace Core {
  
  class PropagatorBoolOrBin : public PropagatorBinary {
  public:
    PropagatorBoolOrBin(DomainBoolean *aDom0, DomainBoolean *aDom1);

    ~PropagatorBoolOrBin() override = default;
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;
  };
  
  class PropagatorBoolOrTer : public PropagatorBinaryReif {
  public:
    PropagatorBoolOrTer(DomainBoolean *aDom0, DomainBoolean *aDom1, DomainBoolean *aDom2);

    ~PropagatorBoolOrTer() override = default;

    double getFitness() override;
    
    PropagationEvent post() override;

    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet{
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }

  protected:
    PropagationEvent runPropagation() override;
  };
} // end namespace Core
