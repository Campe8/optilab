//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 12/22/2016
//
// Lq (i.e., inequality) propagator:
// D0 <= D1
//

#pragma once

// Base class
#include "PropagatorBinary.hpp"

#include "Domain.hpp"

namespace Core {
  
  class PropagatorLq : public PropagatorBinary {
  public:
    PropagatorLq(Domain *aDom0, Domain *aDom1);
    
    virtual ~PropagatorLq() = default;
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;
  };
  
} // end namespace Core
