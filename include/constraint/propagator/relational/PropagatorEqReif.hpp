//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 12/07/2016
//
// Eq reif (i.e., equality reif) propagator:
// (D0 == D1) <-> b
//

#pragma once

// Base class
#include "PropagatorBinaryReif.hpp"

#include "Domain.hpp"

namespace Core {
  
  class PropagatorEqReif : public PropagatorBinaryReif {
  public:
    virtual ~PropagatorEqReif();
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>(),
        std::make_shared<DomainEventChange>()
      };
      return eventSet;
    }
    
  protected:
    PropagatorEqReif(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif,
                     PropagatorStrategyType aStrategyType,
                     PropagatorSemanticType aReifType = PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ_REIF,
                     PropagationPriority aPriority = PropagationPriority::PROP_PRIORITY_TERNARY);

    PropagationEvent runPropagation() override;
  };

  class PropagatorEqReifBound : public PropagatorEqReif {
  public:
    PropagatorEqReifBound(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif);
    
    virtual ~PropagatorEqReifBound();
  };
  
  class PropagatorEqReifDomain : public PropagatorEqReif {
  public:
    PropagatorEqReifDomain(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif);
    
    virtual ~PropagatorEqReifDomain();
  };
  
  class PropagatorHEqReif : public PropagatorEqReif {
  public:
    virtual ~PropagatorHEqReif();
    
    double getFitness() override;
    
    PropagationEvent post() override;

  protected:
    PropagatorHEqReif(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif,
      PropagatorStrategyType aStrategyType,
      PropagatorSemanticType aReifType,
      PropagationPriority aPriority = PropagationPriority::PROP_PRIORITY_TERNARY);

    PropagationEvent runPropagation() override;

  private:
    /// Half reification type, i.e., left or right
    PropagatorSemanticType pReifType;

    /// Returns true if half reification left
    inline bool isHalfReifLeft() const
    {
      return pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ_HREIF_L;
    }

    /// Returns true if half reification right
    inline bool isHalfReifRight() const
    {
      return pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ_HREIF_R;
    }
  };

  class PropagatorEqHReifLBound : public PropagatorHEqReif {
  public:
    PropagatorEqHReifLBound(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif);

    virtual ~PropagatorEqHReifLBound();
  };

  class PropagatorEqHReifLDomain : public PropagatorHEqReif {
  public:
    PropagatorEqHReifLDomain(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif);

    virtual ~PropagatorEqHReifLDomain();
  };

  class PropagatorEqHReifRBound : public PropagatorHEqReif {
  public:
    PropagatorEqHReifRBound(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif);

    virtual ~PropagatorEqHReifRBound();
  };

  class PropagatorEqHReifRDomain : public PropagatorHEqReif {
  public:
    PropagatorEqHReifRDomain(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif);

    virtual ~PropagatorEqHReifRDomain();
  };

} // end namespace Core
