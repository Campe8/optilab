//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 12/30/2016
//
// Lq reif (i.e., disequality reif) propagator:
// (D0 < D1) <-> b
//

#pragma once

// Base class
#include "PropagatorBinaryReif.hpp"

#include "Domain.hpp"

namespace Core {
  
  class PropagatorLtReif : public PropagatorBinaryReif {
  public:
    PropagatorLtReif(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif, PropagatorSemanticType aSemanticType = PropagatorSemanticType::PROP_SEMANTIC_TYPE_LT_REIF);

    virtual ~PropagatorLtReif();
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;
  };

  class PropagatorHLtReif : public PropagatorLtReif {
  public:
    virtual ~PropagatorHLtReif();

    double getFitness() override;

    PropagationEvent post() override;

  protected:
    PropagatorHLtReif(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif, PropagatorSemanticType aReifType);

    PropagationEvent runPropagation() override;

  private:
    /// Half reification type, i.e., left or right
    PropagatorSemanticType pReifType;

    /// Returns true if half reification left
    inline bool isHalfReifLeft() const
    {
      return pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_LT_HREIF_L;
    }

    /// Returns true if half reification right
    inline bool isHalfReifRight() const
    {
      return pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_LT_HREIF_R;
    }
  };

  class PropagatorLtHReifL : public PropagatorHLtReif {
  public:
    PropagatorLtHReifL(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif);

    virtual ~PropagatorLtHReifL();
  };

  class PropagatorLtHReifR : public PropagatorHLtReif {
  public:
    PropagatorLtHReifR(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif);

    virtual ~PropagatorLtHReifR();
  };

} // end namespace Core
