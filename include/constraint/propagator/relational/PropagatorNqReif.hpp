//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 12/30/2016
//
// Nq reif (i.e., inequality reif) propagator:
// (D0 != D1) <-> b
//

#pragma once

// Base class
#include "PropagatorBinaryReif.hpp"

#include "Domain.hpp"

namespace Core {
  
  class PropagatorNqReif : public PropagatorBinaryReif {
  public:
    PropagatorNqReif(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif, PropagatorSemanticType aSemanticType = PropagatorSemanticType::PROP_SEMANTIC_TYPE_NQ_REIF);

    virtual ~PropagatorNqReif();
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;
  };

  class PropagatorHNqReif : public PropagatorNqReif {
  public:
    virtual ~PropagatorHNqReif();
    
    double getFitness() override;
    
    PropagationEvent post() override;

  protected:
    PropagatorHNqReif(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif, PropagatorSemanticType aReifType);

    PropagationEvent runPropagation() override;

  private:
    /// Half reification type, i.e., left or right
    PropagatorSemanticType pReifType;

    /// Returns true if half reification left
    inline bool isHalfReifLeft() const
    {
      return pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_NQ_HREIF_L;
    }

    /// Returns true if half reification right
    inline bool isHalfReifRight() const
    {
      return pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_NQ_HREIF_R;
    }
  };

  class PropagatorNqHReifL : public PropagatorHNqReif {
  public:
    PropagatorNqHReifL(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif);

    virtual ~PropagatorNqHReifL();
  };

  class PropagatorNqHReifR : public PropagatorHNqReif {
  public:
    PropagatorNqHReifR(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif);

    virtual ~PropagatorNqHReifR();
  };

} // end namespace Core
