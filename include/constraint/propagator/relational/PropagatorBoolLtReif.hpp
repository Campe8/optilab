//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 3/2/2017
//
// Bool Lq (i.e., inequality) propagator:
// (!D0 /\ D1) <-> r
//
// @note propagator is fully implemented for
// full reification for better performance.
// For half reification Lt reif left/right
// propagators are used.
//

#pragma once

// Base class
#include "PropagatorBinaryReif.hpp"

#include "DomainBoolean.hpp"

namespace Core {
  
  class PropagatorBoolLtReif : public PropagatorBinaryReif {
  public:
    PropagatorBoolLtReif(DomainBoolean *aDom0, DomainBoolean *aDom1, DomainBoolean *aReif);
    
    ~PropagatorBoolLtReif() override = default;
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;
  };
  
  class PropagatorBoolLtHReifL : public PropagatorBinary {
  public:
    PropagatorBoolLtHReifL(DomainBoolean *aDom0, DomainBoolean *aDom1, DomainBoolean *aReif);

    ~PropagatorBoolLtHReifL() override = default;

    double getFitness() override;
    
    inline PropagationEvent post() override
    {
      return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LT_HREIF_L);
    }

    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet{
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }

  protected:
    inline PropagationEvent runPropagation() override
    {
      return runInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LT_HREIF_L);
    }
  };

  class PropagatorBoolLtHReifR : public PropagatorBinary {
  public:
    PropagatorBoolLtHReifR(DomainBoolean *aDom0, DomainBoolean *aDom1, DomainBoolean *aReif);

    ~PropagatorBoolLtHReifR() override = default;

    double getFitness() override;
    
    inline PropagationEvent post() override
    {
      return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LT_HREIF_R);
    }

    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet{
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }

  protected:
    inline PropagationEvent runPropagation() override
    {
      return runInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LT_HREIF_R);
    }
  };

} // end namespace Core
