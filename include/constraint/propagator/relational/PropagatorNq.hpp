//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 12/05/2016
//
// Nq (i.e., disequality) propagator:
// D0 != D1
//

#pragma once

// Base class
#include "PropagatorBinary.hpp"

#include "Domain.hpp"

namespace Core {
  
  class PropagatorNq : public PropagatorBinary {
  public:
    PropagatorNq(Domain *aDom0, Domain *aDom1);
    
    virtual ~PropagatorNq() = default;
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>()
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;
  };
  
} // end namespace Core
