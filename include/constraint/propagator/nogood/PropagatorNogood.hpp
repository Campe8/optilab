//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 11/02/2018
//
// Propagator for nogood.
//

#pragma once

// Base class
#include "Propagator.hpp"

#include "Domain.hpp"
#include "DomainElement.hpp"

namespace Core {
  
  class PropagatorNogood : public Propagator {
  public:
    PropagatorNogood(const DomainArraySPtr& aDomainArray, const DomainElementArraySPtr& aDomainElementArray);
    
    virtual ~PropagatorNogood() = default;
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>()
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;
    
  private:
    /// Array of domains
    DomainArraySPtr pDomainArray;
    
    /// Array of domain elements mapped 1 to 1 to the array of domains
    DomainElementArraySPtr pDomainElementArray;
  };
  
} // end namespace Core
