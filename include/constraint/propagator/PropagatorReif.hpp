//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 12/15/2016
//
// Base class for binary propagators, i.e., propagators
// working on a pair of domains.
//

#pragma once


// Export definitions
#include "ConstraintExportDefs.hpp"

// Types and macros
#include "ConstraintDefs.hpp"

// Propagation cost and priorities
#include "PropagationCost.hpp"

// Domain boolean for reif propagation
#include "DomainBoolean.hpp"

// Utils
#include "PropagatorUtils.hpp"

// Filters
#include "FilteringAlgorithm.hpp"

#include <iostream>
#include <cassert>
#include <unordered_map>

namespace Core {
  
  class PropagatorReif {
  public:
    PropagatorReif(DomainBoolean *aDomBool);
    
    virtual ~PropagatorReif();
    
  protected:
    /// Sets the reification Boolean domain
    inline void setContextReifDomain(DomainBoolean *aDomainBool)
    {
      assert(aDomainBool);
      pDomainBool = aDomainBool;
    }
    
    /// Returns the reification Boolean domain
    inline DomainBoolean* GetContextReifDomain() const
    {
      return pDomainBool;
    }
    
    /// Returns True if the context domain is singleton,
    /// False otherwise
    inline bool isContextReifSingleton()
    {
      return PropagatorUtils::isDomainSingleton(GetContextReifDomain());
    }
    
    /// Returns True if the reif domain is singleton and True,
    /// False otherwise
    bool isContextReifTrue();
    
    /// Returns True if the reif domain is singleton and False,
    /// False otherwise
    bool isContextReifFalse();
    
    /// Propagates on the reif context domain and
    /// set it to True
    PropagationEvent setContextReifTrue();
    
    /// Propagates on the reif context domain and
    /// set it to False
    PropagationEvent setContextReifFalse();
    
  private:
    // Boolean domain for reification
    DomainBoolean* pDomainBool;
    
    /// Filtering parameters to use with pFilterToUse
    /// when applying the filtering algorithm
    FilteringParameters pFilterParams;
    
    /// Filtering algorithm in use
    FilteringAlgorithmSPtr pFilterToUse;
    
    /// Map of filter algorithms to use: <(int)FilteringAlgorithmSemanticType, FilteringAlgorithm ptr>
    std::unordered_map<int, FilteringAlgorithmSPtr> pFilterAlgorithmRegister;
    
    /// Sets the filtering algorithm to use when
    /// calling applyFilteringAlgorithm.
    /// @note "aFilterStrategy" must be registered (throws otherwise)
    inline void setFilterReifStrategy(FilteringAlgorithmSemanticType aFilterStrategy)
    {
      pFilterToUse = pFilterAlgorithmRegister.at(static_cast<int>(aFilterStrategy));
    }
  };
  
} // end namespace Core
