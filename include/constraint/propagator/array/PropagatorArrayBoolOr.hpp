//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 3/8/2017
//
// Array Boolean OR propagator:
// (Exists an i in [0..n-1]: as[i]) <-> r
// where
// n is the length of "as".
//

#pragma once

// Base class
#include "PropagatorArray.hpp"

#include "DArrayMatrix.hpp"
#include "DomainBoolean.hpp"

namespace Core {
  
  class PropagatorArrayBoolOr : public PropagatorArray {
  public:
    /// Constructor for "aDomArray[...] = True iff aBoolDom",
    /// where "aDomArray" is an array of Boolean domains and "aBoolDom"
    /// is a Boolean domain
    PropagatorArrayBoolOr(DomainArray& aDomArray, DomainBoolean *aBoolDom);

    virtual ~PropagatorArrayBoolOr();
    
    double getFitness() override;
    
    PropagationEvent post() override;
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
      };
      return eventSet;
    }
    
  protected:
    PropagationEvent runPropagation() override;
    
  private:
    /// Returns domain setting the array to True or False
    inline DomainBoolean* getBoolDomain() const
    {
      return DomainBoolean::cast(getContextDomain(D0));
    }
  };
  
} // end namespace Core
