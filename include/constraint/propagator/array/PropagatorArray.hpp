//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 3/8/2017
//
// Base class for propagators on arrays,
// e.g., Element, BoolAnd, etc.
//

#pragma once

// Base class
#include "Propagator.hpp"

#include "DArrayMatrix.hpp"
#include "DomainElement.hpp"

namespace Core {
  
  class PropagatorArray : public Propagator {
  protected:
    /// Constructor for array of Domains
    PropagatorArray(const PropagatorSemantics& aPropagatorSemantics, DomainArray& aDomArray);

    /// Constructor for array of Domain Elements
    PropagatorArray(const PropagatorSemantics& aPropagatorSemantics, DomainElementArray& aDomElemArray);

    /// Returns True if the propagator is defined on 
    /// an array of Domains, False otherwise
    inline bool hasDomainArray() const
    {
      return pDomArray != nullptr;
    }

    /// Returns True if the propagator is defined on 
    /// an array of Domain Elements, False otherwise
    inline bool hasDomainElementArray() const
    {
      return pDomElemArray != nullptr;
    }

    inline DomainElement* arraySize() const
    {
      return pArraySize;
    }

    inline INT_64 arraySizeINT64() const
    {
      return hasDomainArray() ? static_cast<INT_64>(pDomArray->size()) :
      static_cast<INT_64>(pDomElemArray->size());
    }

    /// Returns True if all the elements in the array are singletons,
    /// False otherwise
    bool isArraySingleton();

    /// Returns the domain stored in "pDomArray" at position "aDomainElementIdx".
    /// @note returns nullptr if "pDomArray" is empty.
    /// @note throws if position is not consistent
    Domain* domainArrayAt(DomainElement* aDomainElementIdx);

    /// Returns the domain stored in "pDomArray" at position "aIdx".
    /// @note returns nullptr if "pDomArray" is empty.
    /// @note throws if position is not consistent
    Domain* domainArrayAtINT64(INT_64 aIdx);

    /// Returns the domain element stored in "pDomElemArray"
    /// at position "aDomainElementIdx".
    /// @note returns nullptr if "pDomElemArray" is empty.
    /// @note throws if position is not consistent
    DomainElement* domainElementArrayAt(DomainElement* aDomainElementIdx);

    /// Returns the domain element stored in "pDomElemArray"
    /// at position "aIdx".
    /// @note returns nullptr if "pDomElemArray" is empty.
    /// @note throws if position is not consistent
    DomainElement* domainElementArrayAtINT64(INT_64 aIdx);

  private:
    DomainElement* pArraySize;

    /// Array of domain elements
    DomainElementArrayUPtr pDomElemArray;

    /// Array of domains
    DomainArrayUPtr pDomArray;
  };
  
} // end namespace Core
