//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/24/2016
//
// Filtering algorithm for "minus" arithmetic operator.
// Requires two input domain element (in domain element array)
//
// Output domain = input domain 1 - input domain 2
//

#pragma once

// Base class
#include "FilteringAlgorithm.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS FilteringAlgorithmMINUS : public FilteringAlgorithm {
  public:
    FilteringAlgorithmMINUS();
    
    virtual ~FilteringAlgorithmMINUS();
    
    Domain *filter(const FilteringParameters& aFilteringParams) const override;
    
    static inline bool isa(FilteringAlgorithm *aFilterAlgo)
    {
      assert(aFilterAlgo);
      return aFilterAlgo->getFilteringAlgorithmSemanticType() ==
      FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_MINUS;
    }
    
    static inline FilteringAlgorithmMINUS* cast(FilteringAlgorithm *aFilterAlgo)
    {
      if(!isa(aFilterAlgo))
      {
        return nullptr;
      }
      return static_cast<FilteringAlgorithmMINUS*>(aFilterAlgo);
    }
    
  };
  
} // end namespace Core
