//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/29/2016
//
//

#pragma once

// Export definitions
#include "ConstraintExportDefs.hpp"

// Types and macros
#include "ConstraintDefs.hpp"

// FilteringAlgorithms
#include "FilteringAlgorithmInc.hpp"

#include <cassert>

namespace Core {
  
  class FilteringAlgorithmFactory {
  public:
    static FilteringAlgorithmSPtr getFilteringAlgorithmInstance(FilteringAlgorithmSemanticType aSemanticType)
    {
      switch (aSemanticType)
      {
        case Core::FILTERING_ALGO_SEM_TYPE_LQ:
          return std::make_shared<FilteringAlgorithmLQ>();
        case Core::FILTERING_ALGO_SEM_TYPE_LT:
          return std::make_shared<FilteringAlgorithmLT>();
        case Core::FILTERING_ALGO_SEM_TYPE_GQ:
          return std::make_shared<FilteringAlgorithmGQ>();
        case Core::FILTERING_ALGO_SEM_TYPE_GT:
          return std::make_shared<FilteringAlgorithmGT>();
        case Core::FILTERING_ALGO_SEM_TYPE_EQ:
          return std::make_shared<FilteringAlgorithmEQ>();
        case Core::FILTERING_ALGO_SEM_TYPE_NQ:
          return std::make_shared<FilteringAlgorithmNQ>();
        case Core::FILTERING_ALGO_SEM_TYPE_TRUE:
          return std::make_shared<FilteringAlgorithmTRUE>();
        case Core::FILTERING_ALGO_SEM_TYPE_FALSE:
          return std::make_shared<FilteringAlgorithmFALSE>();
        case Core::FILTERING_ALGO_SEM_TYPE_ABS:
          return std::make_shared<FilteringAlgorithmABS>();
        case Core::FILTERING_ALGO_SEM_TYPE_PLUS:
          return std::make_shared<FilteringAlgorithmPLUS>();
        case Core::FILTERING_ALGO_SEM_TYPE_MINUS:
          return std::make_shared<FilteringAlgorithmMINUS>();
        case Core::FILTERING_ALGO_SEM_TYPE_TIMES:
          return std::make_shared<FilteringAlgorithmTIMES>();
        case Core::FILTERING_ALGO_SEM_TYPE_DIVIDE:
          return std::make_shared<FilteringAlgorithmDIVIDE>();
        case Core::FILTERING_ALGO_SEM_TYPE_INTERSECT:
          return std::make_shared<FilteringAlgorithmINTERSECT>();
        case Core::FILTERING_ALGO_SEM_TYPE_SUBTRACT:
          return std::make_shared<FilteringAlgorithmSUBTRACT>();
        default:
          assert(aSemanticType == Core::FILTERING_ALGO_SEM_TYPE_ELEMENT);
          return nullptr;
      }
    }//getFilteringAlgorithmInstance
    
  };
  
} // end namespace Core
