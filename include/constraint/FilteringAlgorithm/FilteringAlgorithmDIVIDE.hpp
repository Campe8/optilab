//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/24/2016
//
// Filtering algorithm for "divide" arithmetic operator.
// Requires two input domain element (in domain element array)
//
// Output domain = input domain 0 / input domain 1
// i.e.,
// Output domain = { DomainElementArray[0] / DomainElementArray[1] }
// according to the specified rounding mode:
// - DIV_ROUNDING_CEILING
// - DIV_ROUNDING_FLOOR
// - DIV_ROUNDING_UNSPEC
// where DIV_ROUNDING_UNSPEC is the default and it is rounding
// as specified by standard C.
//

#pragma once

// Base class
#include "FilteringAlgorithm.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS FilteringAlgorithmDIVIDE : public FilteringAlgorithm {
  public:
    enum DivisionRoundingMode {
        DIV_ROUNDING_CEILING = 0
      , DIV_ROUNDING_FLOOR
      , DIV_ROUNDING_UNSPEC
    };
    
    FilteringAlgorithmDIVIDE();
    
    virtual ~FilteringAlgorithmDIVIDE();
    
    Domain *filter(const FilteringParameters& aFilteringParams) const override;
    
    inline void setDivisionRoundingMode(DivisionRoundingMode aRoundingMode)
    {
      pRoundingMode = aRoundingMode;
    }
    
    inline DivisionRoundingMode getDivisionRoundingMode() const
    {
      return pRoundingMode;
    }
    
    static inline bool isa(FilteringAlgorithm *aFilterAlgo)
    {
      assert(aFilterAlgo);
      return aFilterAlgo->getFilteringAlgorithmSemanticType() ==
      FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_DIVIDE;
    }
    
    static inline FilteringAlgorithmDIVIDE* cast(FilteringAlgorithm *aFilterAlgo)
    {
      if(!isa(aFilterAlgo))
      {
        return nullptr;
      }
      return static_cast<FilteringAlgorithmDIVIDE*>(aFilterAlgo);
    }
    
  private:
    /// Rounding mode for filtering
    DivisionRoundingMode pRoundingMode;
    
  };
  
} // end namespace Core
