//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/24/2016
//
// Filtering algorithm for "subtract" set operator.
// Requires an array of domain elements OR a Domain BUT NOT both.
//
// Output domain = Output domain - {x \in array of domain elements}
//

#pragma once

// Base class
#include "FilteringAlgorithm.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS FilteringAlgorithmSUBTRACT : public FilteringAlgorithm {
  public:
    // Specify what to subtract from
    enum SubtractSetType {
        SS_DOMAIN                 // Subtract Domain
      , SS_DOMAIN_ELEMENT_ARRAY   // Subtract an array of DomainElement
    };
    
    // Constructor, default intersection set is SS_DOMAIN
    FilteringAlgorithmSUBTRACT();
    
    virtual ~FilteringAlgorithmSUBTRACT();
    
    Domain *filter(const FilteringParameters& aFilteringParams) const override;
    
    inline void setSubtractSetType(SubtractSetType aSubtracSet)
    {
      pSubtractSet = aSubtracSet;
    }
    
    inline SubtractSetType getSubtractSetType()
    {
      return pSubtractSet;
    }
    
    static inline bool isa(FilteringAlgorithm *aFilterAlgo)
    {
      assert(aFilterAlgo);
      return aFilterAlgo->getFilteringAlgorithmSemanticType() ==
      FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_SUBTRACT;
    }
    
    static inline FilteringAlgorithmSUBTRACT* cast(FilteringAlgorithm *aFilterAlgo)
    {
      if(!isa(aFilterAlgo))
      {
        return nullptr;
      }
      return static_cast<FilteringAlgorithmSUBTRACT*>(aFilterAlgo);
    }
    
  private:
    SubtractSetType pSubtractSet;
    
  };
  
} // end namespace Core
