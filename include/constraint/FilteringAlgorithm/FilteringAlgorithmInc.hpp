//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/29/2016
//

#pragma once

#include "FilteringAlgorithmABS.hpp"
#include "FilteringAlgorithmDIVIDE.hpp"
#include "FilteringAlgorithmELEMENT.hpp"
#include "FilteringAlgorithmEQ.hpp"
#include "FilteringAlgorithmFALSE.hpp"
#include "FilteringAlgorithmGQ.hpp"
#include "FilteringAlgorithmGT.hpp"
#include "FilteringAlgorithmINTERSECT.hpp"
#include "FilteringAlgorithmLQ.hpp"
#include "FilteringAlgorithmLT.hpp"
#include "FilteringAlgorithmMINUS.hpp"
#include "FilteringAlgorithmNQ.hpp"
#include "FilteringAlgorithmPLUS.hpp"
#include "FilteringAlgorithmSUBTRACT.hpp"
#include "FilteringAlgorithmTIMES.hpp"
#include "FilteringAlgorithmTRUE.hpp"
