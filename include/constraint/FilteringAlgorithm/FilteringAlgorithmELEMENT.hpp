//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/24/2016
//
// Filtering algorithm for "element" array element filtering.
// Requires an array of domain elements AND a domain element of type INT64
// which MUST BE the first element in DomainElementArray.
//
// Output domain = {c} s.t.,
// c = as[i]
// where
// as is the array of domain elements
// and
// i is the domain element 0 <= i < as.size()
//

#pragma once

// Base class
#include "FilteringAlgorithm.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS FilteringAlgorithmELEMENT : public FilteringAlgorithm {
  public:
    FilteringAlgorithmELEMENT();
    
    virtual ~FilteringAlgorithmELEMENT();
    
    Domain *filter(const FilteringParameters& aFilteringParams) const override;
    
    static inline bool isa(FilteringAlgorithm *aFilterAlgo)
    {
      assert(aFilterAlgo);
      return aFilterAlgo->getFilteringAlgorithmSemanticType() ==
      FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_ELEMENT;
    }
    
    static inline FilteringAlgorithmELEMENT* cast(FilteringAlgorithm *aFilterAlgo)
    {
      if(!isa(aFilterAlgo))
      {
        return nullptr;
      }
      return static_cast<FilteringAlgorithmELEMENT*>(aFilterAlgo);
    }
    
  };
  
} // end namespace Core
