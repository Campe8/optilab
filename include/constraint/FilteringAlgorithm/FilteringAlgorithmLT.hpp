//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/22/2016
//
// Filtering algorithm for < relational operator.
// Requires one input domain element.
//
// Output domain < DomainElement
//

#pragma once

// Base class
#include "FilteringAlgorithm.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS FilteringAlgorithmLT : public FilteringAlgorithm {
  public:
    FilteringAlgorithmLT();
    
    virtual ~FilteringAlgorithmLT();
    
    Domain *filter(const FilteringParameters& aFilteringParams) const override;
    
    static inline bool isa(FilteringAlgorithm *aFilterAlgo)
    {
      assert(aFilterAlgo);
      return aFilterAlgo->getFilteringAlgorithmSemanticType() ==
      FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LT;
    }
    
    static inline FilteringAlgorithmLT* cast(FilteringAlgorithm *aFilterAlgo)
    {
      if(!isa(aFilterAlgo))
      {
        return nullptr;
      }
      return static_cast<FilteringAlgorithmLT*>(aFilterAlgo);
    }
    
  };
  
} // end namespace Core
