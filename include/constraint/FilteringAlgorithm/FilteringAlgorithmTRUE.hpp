//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/25/2016
//
// Filtering algorithm for TRUE value on Boolean domain.
// Requires no input.
//
// Output domain = { TRUE }
//

#pragma once

// Base class
#include "FilteringAlgorithm.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS FilteringAlgorithmTRUE : public FilteringAlgorithm {
  public:
    FilteringAlgorithmTRUE();
    
    virtual ~FilteringAlgorithmTRUE();
    
    Domain *filter(const FilteringParameters& aFilteringParams) const override;
    
    static inline bool isa(FilteringAlgorithm *aFilterAlgo)
    {
      assert(aFilterAlgo);
      return aFilterAlgo->getFilteringAlgorithmSemanticType() ==
      FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_TRUE;
    }
    
    static inline FilteringAlgorithmTRUE* cast(FilteringAlgorithm *aFilterAlgo)
    {
      if(!isa(aFilterAlgo))
      {
        return nullptr;
      }
      return static_cast<FilteringAlgorithmTRUE*>(aFilterAlgo);
    }
    
  };
  
} // end namespace Core
