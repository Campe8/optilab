//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/21/2016
//
// Filtering algorithm for >= relational operator.
// Requires one input domain element.
//
// Output domain >= DomainElement
//

#pragma once

// Base class
#include "FilteringAlgorithm.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS FilteringAlgorithmGQ : public FilteringAlgorithm {
  public:
    FilteringAlgorithmGQ();
    
    virtual ~FilteringAlgorithmGQ();
    
    Domain *filter(const FilteringParameters& aFilteringParams) const override;
    
    static inline bool isa(FilteringAlgorithm *aFilterAlgo)
    {
      assert(aFilterAlgo);
      return aFilterAlgo->getFilteringAlgorithmSemanticType() ==
      FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ;
    }
    
    static inline FilteringAlgorithmGQ* cast(FilteringAlgorithm *aFilterAlgo)
    {
      if(!isa(aFilterAlgo))
      {
        return nullptr;
      }
      return static_cast<FilteringAlgorithmGQ*>(aFilterAlgo);
    }
    
  };
  
} // end namespace Core
