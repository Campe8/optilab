//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/25/2016
//
// Filtering algorithm for FALSE value on Boolean domain.
// Requires no input.
//
// Output domain = { FALSE }
//

#pragma once

// Base class
#include "FilteringAlgorithm.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS FilteringAlgorithmFALSE : public FilteringAlgorithm {
  public:
    FilteringAlgorithmFALSE();
    
    virtual ~FilteringAlgorithmFALSE();
    
    Domain *filter(const FilteringParameters& aFilteringParams) const override;
    
    static inline bool isa(FilteringAlgorithm *aFilterAlgo)
    {
      assert(aFilterAlgo);
      return aFilterAlgo->getFilteringAlgorithmSemanticType() ==
      FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_FALSE;
    }
    
    static inline FilteringAlgorithmFALSE* cast(FilteringAlgorithm *aFilterAlgo)
    {
      if(!isa(aFilterAlgo))
      {
        return nullptr;
      }
      return static_cast<FilteringAlgorithmFALSE*>(aFilterAlgo);
    }
    
  };
  
} // end namespace Core
