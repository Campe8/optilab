//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/24/2016
//
// Filtering algorithm for "times" arithmetic operator.
// Requires two input domain element (in domain element array)
//
// Output domain = input domain 1 * input domain 2
//

#pragma once

// Base class
#include "FilteringAlgorithm.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS FilteringAlgorithmTIMES : public FilteringAlgorithm {
  public:
    FilteringAlgorithmTIMES();
    
    virtual ~FilteringAlgorithmTIMES();
    
    Domain *filter(const FilteringParameters& aFilteringParams) const override;
    
    static inline bool isa(FilteringAlgorithm *aFilterAlgo)
    {
      assert(aFilterAlgo);
      return aFilterAlgo->getFilteringAlgorithmSemanticType() ==
      FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_TIMES;
    }
    
    static inline FilteringAlgorithmTIMES* cast(FilteringAlgorithm *aFilterAlgo)
    {
      if(!isa(aFilterAlgo))
      {
        return nullptr;
      }
      return static_cast<FilteringAlgorithmTIMES*>(aFilterAlgo);
    }
    
  };
  
} // end namespace Core
