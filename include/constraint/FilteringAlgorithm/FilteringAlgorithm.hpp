//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/17/2016
//
// Base class for filtering algorithm strategy pattern.
// @note a filtering algorithm is an actual algorithm which is
// used by propagators to filter domains.
// In other words, it is a function from Domain to Domain:
//         Domain -> Domain
// that filters the given domain according to some filtering
// strategy based on the input parameters.
// @note filtering algorithms are "building blocks" for propagators,
// e.g., they should not carry over states or domain events.
//

#pragma once

// Export definitions
#include "ConstraintExportDefs.hpp"

// Types and macros
#include "ConstraintDefs.hpp"

// Parameters
#include "FilteringParameters.hpp"

#include <memory>
#include <cassert>

namespace Core {
  class FilteringAlgorithm;
}

namespace Core {
  
  typedef std::unique_ptr<FilteringAlgorithm> FilteringAlgorithmUPtr;
  typedef std::shared_ptr<FilteringAlgorithm> FilteringAlgorithmSPtr;
  
  class FilteringAlgorithm {
  public:
    virtual ~FilteringAlgorithm();
    
    /// Actual strategy to implement by each derived class.
    /// It takes filtering parameters and returns the filtered domain
    /// according to the input parameters.
    virtual Domain *filter(const FilteringParameters& aFilteringParams) const = 0;
    
    /// Get semantic type of this filter
    inline FilteringAlgorithmSemanticType getFilteringAlgorithmSemanticType() const
    {
      return pFilteringAlgorithmSemanticType;
    }
    
  protected:
    FilteringAlgorithm(FilteringAlgorithmClass aFilteringAlgorithmClass,
                       FilteringAlgorithmSemanticType aFilteringAlgorithmSemanticType);
    
    inline FilteringAlgorithmClass getFilteringAlgorithmClass() const
    {
      return pFilteringAlgorithmClass;
    }
    
  private:
    /// Class of this filtering algorithm
    FilteringAlgorithmClass pFilteringAlgorithmClass;
    /// Type of filtering algorithm
    FilteringAlgorithmSemanticType pFilteringAlgorithmSemanticType;
  };
  
} // end namespace Core
