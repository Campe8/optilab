//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/22/2016
//
// Filtering algorithm for "abs" arithmetic operator.
// Requires one input domain element.
//
// Output domain = {abs(DomainElement)}
//

#pragma once

// Base class
#include "FilteringAlgorithm.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS FilteringAlgorithmABS : public FilteringAlgorithm {
  public:
    FilteringAlgorithmABS();
    
    virtual ~FilteringAlgorithmABS();
    
    Domain *filter(const FilteringParameters& aFilteringParams) const override;
    
    static inline bool isa(FilteringAlgorithm *aFilterAlgo)
    {
      assert(aFilterAlgo);
      return aFilterAlgo->getFilteringAlgorithmSemanticType() ==
      FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_ABS;
    }
    
    static inline FilteringAlgorithmABS* cast(FilteringAlgorithm *aFilterAlgo)
    {
      if(!isa(aFilterAlgo))
      {
        return nullptr;
      }
      return static_cast<FilteringAlgorithmABS*>(aFilterAlgo);
    }
    
  };
  
} // end namespace Core
