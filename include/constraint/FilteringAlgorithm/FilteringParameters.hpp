//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/18/2016
//
// Parameters for filtering algorithms.
//

#pragma once

// Export definitions
#include "ConstraintExportDefs.hpp"

// Types and macros
#include "ConstraintDefs.hpp"

#include "Domain.hpp"
#include "DomainElement.hpp"
#include "DArrayMatrix.hpp"

#include <vector>
#include <cassert>
#include <memory>

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS FilteringParameters {
  public:
    FilteringParameters();
    
    virtual ~FilteringParameters();
    
    /// Stores the output domain
    inline void setOutputDomain(Domain *aDomain)
    {
      assert(aDomain);
      pOutputDomain = aDomain;
    }
    
    inline void setInputDomainArray(DomainArraySPtr domainArray)
    {
      assert(domainArray);
      pInputDomainArray = domainArray;
    }
    
    inline void setInputDomainMatrix2D(DomainMatrix2DSPtr domainMatrix2D)
    {
      assert(domainMatrix2D);
      pInputDomainMatrix2D = domainMatrix2D;
    }
    
    inline void setDomainElementArray(DomainElementArraySPtr domainElementArray)
    {
      assert(domainElementArray);
      pDomainElementArray = domainElementArray;
    }
    
    inline void setDomainElementMatrix2D(DomainElementMatrix2DSPtr domainElementMatrix2D)
    {
      assert(domainElementMatrix2D);
      pDomainElementMatrix2D = domainElementMatrix2D;
    }
    
    inline bool hasOutputDomain() const
    {
      return pOutputDomain != nullptr;
    }
    
    inline bool hasInputDomainArray() const
    {
      return pInputDomainArray != nullptr;
    }
    
    inline bool hasInputDomainMatrix2D() const
    {
      return pInputDomainMatrix2D != nullptr;
    }
    
    inline bool hasDomainElementArray() const
    {
      return pDomainElementArray != nullptr;
    }
    
    inline bool hasDomainElementMatrix2D() const
    {
      return pDomainElementMatrix2D != nullptr;
    }
    
    inline Domain * getOutputDomain() const
    {
      return pOutputDomain;
    }
    
    inline DomainArraySPtr getInputDomainArray() const
    {
      return pInputDomainArray;
    }
    
    inline DomainMatrix2DSPtr getInputDomainMatrix2D() const
    {
      return pInputDomainMatrix2D;
    }
    
    inline DomainElementArraySPtr getDomainElementArray() const
    {
      return pDomainElementArray;
    }
    
    inline DomainElementMatrix2DSPtr getDomainElementMatrix2D() const
    {
      return pDomainElementMatrix2D;
    }
    
  private:
    Domain *pOutputDomain;
    
    /// Array data structure for domains
    DomainArraySPtr pInputDomainArray;
    
    /// Matrix data structure for domains
    DomainMatrix2DSPtr pInputDomainMatrix2D;
    
    /// Array data structure for domain elements
    DomainElementArraySPtr pDomainElementArray;
    
    /// Matrix data structure for domain elements
    DomainElementMatrix2DSPtr pDomainElementMatrix2D;
  };
  
} // end namespace Core
