//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/22/2016
//
// Filtering algorithm for "intersect" set operator.
// Requires an array of domain elements OR a Domain BUT NOT both.
//
// Output domain = {x : x \in Output domain /\ x \in array of domain elements}
//

#pragma once

// Base class
#include "FilteringAlgorithm.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS FilteringAlgorithmINTERSECT : public FilteringAlgorithm {
  public:
    // Specify what to intersect with
    enum IntersectionSetType {
        IS_DOMAIN                 // Intersect with a Domain
      , IS_DOMAIN_ELEMENT_ARRAY   // Intersect with an array of DomainElement
    };
    
    // Constructor, default intersection set is IS_DOMAIN
    FilteringAlgorithmINTERSECT();
    
    virtual ~FilteringAlgorithmINTERSECT();
    
    Domain *filter(const FilteringParameters& aFilteringParams) const override;
    
    inline void setIntersectionSetType(IntersectionSetType aIntersectionSet)
    {
      pIntersectionSet = aIntersectionSet;
    }
    
    inline IntersectionSetType getIntersectionSetType()
    {
      return pIntersectionSet;
    }
    
    static inline bool isa(FilteringAlgorithm *aFilterAlgo)
    {
      assert(aFilterAlgo);
      return aFilterAlgo->getFilteringAlgorithmSemanticType() ==
      FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_INTERSECT;
    }
    
    static inline FilteringAlgorithmINTERSECT* cast(FilteringAlgorithm *aFilterAlgo)
    {
      if(!isa(aFilterAlgo))
      {
        return nullptr;
      }
      return static_cast<FilteringAlgorithmINTERSECT*>(aFilterAlgo);
    }
    
  private:
    IntersectionSetType pIntersectionSet;
    
  };
  
} // end namespace Core
