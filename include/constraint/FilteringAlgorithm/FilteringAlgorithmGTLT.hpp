//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 12/08/2016
//
// Filtering algorithm for > and < relational operator.
// Requires two input domain element.
//
// DomainElement1 < Output domain < DomainElement2
//

#pragma once

// Base class
#include "FilteringAlgorithm.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS FilteringAlgorithmGTLT : public FilteringAlgorithm {
  public:
    FilteringAlgorithmGTLT();
    
    virtual ~FilteringAlgorithmGTLT();
    
    static inline bool isa(FilteringAlgorithm *aFilterAlgo)
    {
      assert(aFilterAlgo);
      return aFilterAlgo->getFilteringAlgorithmSemanticType() ==
      FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GTLT;
    }
    
    static inline FilteringAlgorithmGTLT* cast(FilteringAlgorithm *aFilterAlgo)
    {
      if (!isa(aFilterAlgo))
      {
        return nullptr;
      }
      return static_cast<FilteringAlgorithmGTLT*>(aFilterAlgo);
    }
    
    Domain *filter(const FilteringParameters& aFilteringParams) const override;
  };
  
} // end namespace Core
