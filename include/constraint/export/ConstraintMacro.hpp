//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 12/08/2016
//

#pragma once

#include "ConstraintParameterVariable.hpp"
#include "ConstraintParameterDomain.hpp"

#define D0 0
#define D1 1
#define D2 2

#define DOMAIN_FAILURE_CHECK(aDomain) do {      \
if (PropagatorUtils::isDomainEmpty(aDomain))    \
return PropagationEvent::PROP_EVENT_FAIL;       \
} while (0)

// Verify and returns whether given parameter has a primitive type
#define VERIFY_PRIMITIVE_TYPE(aParam) (\
ConstraintParameterDomain::isa(aParam) || \
(ConstraintParameterVariable::isa(aParam) && ConstraintParameterVariable::cast(aParam)->getVariable()->hasPrimitiveType()) \
)

// Verify and returns whether given parameter has an array type
#define VERIFY_ARRAY_TYPE(aParam) (\
ConstraintParameterDomainArray::isa(aParam) \
)

// Notify constraint observers on init propagation event
#define NOTIFY_CONSTRAINT_OBSERVERS_PROP_INIT(aPropEven) do {  \
pConstraintObserverEvent->setConstraintPropagationStatus(ConstraintObserverEvent::ConstraintObserverPropagationStatus::CON_EVT_PROPAGATION_INIT);  \
pConstraintObserverEvent->setConstraintPropagationEvent(aPropEven);  \
notifyRegisteredDomainsOnEvent();  \
} while (0)

// Notify constraint observers on term propagation event
#define NOTIFY_CONSTRAINT_OBSERVERS_PROP_TERM(aPropEven) do {  \
pConstraintObserverEvent->setConstraintPropagationStatus(ConstraintObserverEvent::ConstraintObserverPropagationStatus::CON_EVT_PROPAGATION_TERM);  \
pConstraintObserverEvent->setConstraintPropagationEvent(aPropEven);  \
notifyRegisteredDomainsOnEvent();  \
} while (0)

