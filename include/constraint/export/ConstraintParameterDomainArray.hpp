//
// Copyright OptiLab 2016-2017. All rights reserved.
//
// Created by Federico Campeotto on 03/16/2017
//

#pragma once

// Base class
#include "ConstraintParameter.hpp"

#include "DMatrix2D.hpp"

// Forward declarations
namespace Core {
  class ConstraintParameterFactory;
}

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintParameterDomainArray : public ConstraintParameter {
  public:
    static bool isa(const ConstraintParameter* aParameter);
    static ConstraintParameterDomainArray* cast(ConstraintParameter* aParameter);
    static const ConstraintParameterDomainArray* cast(const ConstraintParameter* aParameter);
    
    inline DomainSPtrArraySPtr getDomainArray() const
    {
      return pDomParameter;
    }
    
    /// Returns the size of the (contained) parameter array
    std::size_t size() const;
    
    /// Returns true if the element in the array is a subject parameter,
    /// false otherwise
    bool isSubject(std::size_t aIdx);
    
    /// Sets all the parameters contained in this array as subjects
    /// for observers. This means than whenever one of these parameters
    /// changes, it will notify the constraint(s) owning this parameter
    void setAllAsSubject();
    
    /// Sets the element in position 1 - "aIdx" as a subject element
    void setAsSubjectGivenIndex(std::size_t aIdx);
    
  protected:
    friend class ConstraintParameterFactory;
    
    /// Parameter for array of domains
    ConstraintParameterDomainArray(const DomainSPtrArraySPtr& aDomainArray);
    
    /// Parameter for array of domain elements.
    /// @note "aDomainClass" is the container class for each domain element in "aDomainElementArray"
    ConstraintParameterDomainArray(const DomainElementArraySPtr& aDomainElementArray, DomainClass aDomainClass);
    
  private:
    /// This vector has the size of "pDomParameter"
    /// and describes which parameter in the array is a subject parameter
    std::vector<bool> pSubjectArray;
    
    /// Array of domains
    DomainSPtrArraySPtr pDomParameter;
  };
  
} // end namespace Core

