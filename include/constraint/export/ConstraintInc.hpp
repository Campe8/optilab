//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 04/06/2017
//
// Factory class for constraints.
//

#pragma once

// Export definitions
#include "ConstraintExportDefs.hpp"

// Types and macros
#include "ConstraintDefs.hpp"

// Relational
#include "ConstraintEq.hpp"
#include "ConstraintEqReif.hpp"
#include "ConstraintNq.hpp"
#include "ConstraintNqReif.hpp"
#include "ConstraintLq.hpp"
#include "ConstraintLqReif.hpp"
#include "ConstraintLt.hpp"
#include "ConstraintLtReif.hpp"

// Arithmetic
#include "ConstraintPlus.hpp"
#include "ConstraintTimes.hpp"
#include "ConstraintDiv.hpp"
#include "ConstraintAbs.hpp"
#include "ConstraintMax.hpp"
#include "ConstraintMin.hpp"

// Linear
#include "ConstraintLinearEq.hpp"
#include "ConstraintLinearEqReif.hpp"
#include "ConstraintLinearNq.hpp"
#include "ConstraintLinearNqReif.hpp"
#include "ConstraintLinearLq.hpp"
#include "ConstraintLinearLqReif.hpp"
#include "ConstraintLinearLt.hpp"

// Global
#include "ConstraintAllDiff.hpp"
#include "ConstraintCircuit.hpp"
#include "ConstraintDomain.hpp"
#include "ConstraintElement.hpp"
