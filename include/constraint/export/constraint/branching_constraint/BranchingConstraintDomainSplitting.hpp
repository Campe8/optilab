//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 01/29/2017
//
// Domain splitting branching constraint:
// given var x and domain element d
// this constraints forces the following:
// x <= d and x > d
//

#pragma once

// Base class
#include "BranchingConstraint.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS BranchingConstraintDomainSplitting : public BranchingConstraint {
  public:
    ~BranchingConstraintDomainSplitting() = default;
    
  protected:
    /// Protected constructor to be used only by the
    /// branching constraint factory
    friend class BranchingConstraintFactory;
    
    BranchingConstraintDomainSplitting(const VariableSPtr& aBranchingVar, const DomainSPtr& aDomain,
                                       BranchingConstraintId aBranchingConstraintId);
  };
  
} // end namespace Core
