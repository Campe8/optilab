//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 01/28/2017
//

#pragma once

namespace Core {
  
  enum BranchingConstraintId : int {
      BRC_CON_ID_BINARY_CHOICE_EQ = 0
    , BRC_CON_ID_BINARY_CHOICE_NQ
    , BRC_CON_ID_DOMAIN_SPLIT_LQ
    , BRC_CON_ID_DOMAIN_SPLIT_GT
  };
  
} // end namespace Core

