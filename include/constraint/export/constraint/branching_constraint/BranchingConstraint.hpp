//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 01/28/2017
//
// Base class for branching constraints.
// A branching constraint is a (usually) unary constraint
// which defines the branches of a node in a search tree.
//

#pragma once

// Base class
#include "Constraint.hpp"

// Definitions
#include "BranchingConstraintDefs.hpp"

// Forward declarations
namespace Core {
  class BranchingConstraint;
  
  typedef BranchingConstraint* BranchingConstraintPtr;
  typedef std::unique_ptr<BranchingConstraint> BranchingConstraintUPtr;
  typedef std::shared_ptr<BranchingConstraint> BranchingConstraintSPtr;
} // end namespace Core

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS BranchingConstraint : public Constraint {
  public:
    virtual ~BranchingConstraint() = default;
    
    inline BranchingConstraintId getBranchingConstraintId() const
    {
      return pBranchingConstraintId;
    }
    
    /// Returns the variable to branch on
    inline VariableSPtr getBranchingVariable() const { return pBranchingVariable; }
    
    /// Returns the domain the branching variable branches on
    inline DomainSPtr getBranchingDomain() const { return pBranchingDomain; }
    
    /// Pretty prints the constraint for debugging purposes
    std::string prettyPrintConstraint() const;
    
  protected:
    BranchingConstraint(const VariableSPtr& aBranchingVar, const DomainSPtr& aDomain,
                        BranchingConstraintId aBranchingConstraintId);
    
    /// Always return unary cost, i.e.,
    /// assign high priority to branching constraints
    PropagationCost reducedCost(const PropagationCost& aPropagationCost) override;
    
    /// Register propagator according to its type and the variable/domain
    /// registered in this constraint.
    /// By default, if REL is the semantic relation between the variable and the domain
    /// representing this branching constraint (e.g., var == d -> REL is "=="),
    /// this method registers the propagator as:
    /// variable REL domain
    /// to inverse the relation use "aInverseRelation = true".
    /// Using "aInverseRelation = true" the registered propagator is:
    /// domain REL variable
    virtual void registerBranchingPropagator(PropagatorSemanticType aPropagatorType,
                                             bool aInverseRelation=false);
    
  private:
    /// Branching constraint id
    BranchingConstraintId pBranchingConstraintId;
    
    /// Branching domain
    DomainSPtr pBranchingDomain;
    
    /// Variable to branch on
    VariableSPtr pBranchingVariable;
  };
  
} // end namespace Core
