//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 01/28/2017
//

#pragma once

#include "BranchingConstraint.hpp"
#include "BranchingConstraintDefs.hpp"

// Domain factory
#include "DomainFactory.hpp"

// Forward declarations
namespace Core {
  class BranchingConstraintFactory;
  typedef BranchingConstraintFactory* BranchingConstraintFactoryPtr;
  typedef std::unique_ptr<BranchingConstraintFactory> BranchingConstraintFactoryUPtr;
  typedef std::shared_ptr<BranchingConstraintFactory> BranchingConstraintFactorySPtr;
} // end namespace Core

namespace Core {
  
  class BranchingConstraintFactory {
  public:
    BranchingConstraintFactory();
    
    ~BranchingConstraintFactory();
    
    /// Returns a branching constraint posted on the given variable
    /// and domain element (unary constraint).
    /// @note this method DOES NOT check whether "aBranchingDomainElement" is actually
    /// a domain element of the domain of "aBranchingVar" or not
    BranchingConstraintSPtr branchingConstraint(const VariableSPtr& aBranchingVar,
                                                DomainElement* aBranchingDomainElement,
                                                BranchingConstraintId aBranchingConstraintId);
    
  private:
    DomainFactoryUPtr pDomainFactory;
  };
  
} // end namespace Core
