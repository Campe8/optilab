//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 01/28/2017
//
// Binary choice branching constraint:
// given var x and domain element d
// this constraints forces the following:
// x = d and x != d
//

#pragma once

// Base class
#include "BranchingConstraint.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS BranchingConstraintBinaryChoice : public BranchingConstraint {
  public:
    ~BranchingConstraintBinaryChoice();
    
  protected:
    /// Protected constructor to be used only by the
    /// branching constraint factory
    friend class BranchingConstraintFactory;
    
    BranchingConstraintBinaryChoice(const VariableSPtr& aBranchingVar, const DomainSPtr& aDomain,
                                    BranchingConstraintId aBranchingConstraintId);
  };
  
} // end namespace Core
