//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 11/02/2018
//
// Nogood constraint for nogood learning done during
// backtrack search.
//

#pragma once

// Base class
#include "Constraint.hpp"

#include <vector>

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS NogoodConstraint : public Constraint {
  public:
    /// Constructor given {v1, v2, v3} and {d1, d2, d3} registers
    /// the nogood constraint as
    /// v1 != d1 \/ v2 != d2 \/ v3 != d3
    NogoodConstraint(const std::vector<VariableSPtr>& aNogoodVars, const std::vector<DomainSPtr>& aDomains);
    
    virtual ~NogoodConstraint() = default;
    
    /// Life-time management of nogood constraints
    /// is different from standard constraints.
    /// In particular, a nogood constraint is stored in the constraint store
    /// and it remains stored until the nogood set is cleared.
    void subscribeToConstraintStore(const std::shared_ptr<Semantics::ConstraintStore>& aConstraintStore) override;
    
    /// Unsubscribe this constraint from the constraint store
    void unsubscribeFromConstraintStore() override;
    
  protected:
    /// Always return unary cost, i.e.,
    /// assign high priority to nogood constraints
    PropagationCost reducedCost(const PropagationCost& aPropagationCost) override;

  private:
    /// Utility function: register nogood propagator
    void registerNogoodPropagator(const std::vector<VariableSPtr>& aNogoodVars, const std::vector<DomainSPtr>& aDomains);
  };
  
} // end namespace Core
