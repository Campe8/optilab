//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/31/2017
//
// Base class for binary reification constraints.
//

#pragma once

// Base class
#include "ConstraintBinary.hpp"

#include "DomainBoolean.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintBinaryReif : public ConstraintBinary {
	protected:
		ConstraintBinaryReif(ConstraintId aConstraintId,
												 PropagatorStrategyType aStrategyType,
                         const ConstraintParameterSPtr& aP1,
                         const ConstraintParameterSPtr& aP2,
			                   const ConstraintParameterSPtr& aReif);
    

	protected:
		/// Returns the reification domain
		inline DomainBoolean* getReifDomain()
		{
			return pReifDomain.get();
		}

	private:
		/// Reification domain
		std::shared_ptr<DomainBoolean> pReifDomain;

		/// Utility function: returns the reif domain in "aReif"
    std::shared_ptr<DomainBoolean> getReifDomain(const ConstraintParameterSPtr& aReif);
  };
  
} // end namespace Core
