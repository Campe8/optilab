//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/28/2017
//

#pragma once

// Base class
#include "ConstraintTernary.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintTimes : public ConstraintTernary {
  public:
    /// Constructor default propagation is bound
      ConstraintTimes(const ConstraintParameterSPtr& aParam1,
                      const ConstraintParameterSPtr& aParam2,
                      const ConstraintParameterSPtr& aParam3,
                      PropagatorStrategyType aStrategyType = PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND);
  };
  
} // end namespace Core
