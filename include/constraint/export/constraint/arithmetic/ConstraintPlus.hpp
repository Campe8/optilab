//
// Copyright OptiLab 2016-2017. All rights reserved.
//
// Created by Federico Campeotto on 01/19/2017
//
// Base class for constraints
//

#pragma once

// Base class
#include "ConstraintTernary.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintPlus : public ConstraintTernary {
  public:
    /// Constructor default propagation is bound
      ConstraintPlus(const ConstraintParameterSPtr& aParam1,
                     const ConstraintParameterSPtr& aParam2,
                     const ConstraintParameterSPtr& aParam3,
                     PropagatorStrategyType aStrategyType = PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND);
  };
  
} // end namespace Core
