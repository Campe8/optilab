//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/29/2017
//

#pragma once

// Base class
#include "ConstraintTernary.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintMin : public ConstraintTernary {
  public:
      ConstraintMin(const ConstraintParameterSPtr& aParam1,
                    const ConstraintParameterSPtr& aParam2,
                    const ConstraintParameterSPtr& aParam3,
                    PropagatorStrategyType aStrategyType = PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND);
  };
  
} // end namespace Core
