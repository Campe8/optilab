//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/28/2017
//
// Constraint Abs:
// X == |Y|
//

#pragma once

// Base class
#include "ConstraintBinary.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintAbs : public ConstraintBinary {
  private:
    friend class ConstraintFactory;
    
    /// Constructor default propagation is bound
    ConstraintAbs(const ConstraintParameterSPtr& aParam1, const ConstraintParameterSPtr& aParam2,
                  PropagatorStrategyType aStrategyType = PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND);
  };
  
} // end namespace Core
