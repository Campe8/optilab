//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/30/2017
//
// Base class for linear reification constraints.
//

#pragma once

// Base class
#include "ConstraintLinear.hpp"

#include "DomainBoolean.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintLinearReif : public ConstraintLinear {
	protected:
		ConstraintLinearReif(ConstraintId aConstraintId,
												 PropagatorStrategyType aStrategyType,
                         const ConstraintParameterSPtr& aCoeff,
                         const ConstraintParameterSPtr& aDomVar,
                         const ConstraintParameterSPtr& aConst,
			                   const ConstraintParameterSPtr& aReif);
    

	protected:
		/// Returns the reification domain
		inline DomainBoolean* getReifDomain()
		{
			return pReifDomain.get();
		}

	private:
		/// Reification domain
		std::shared_ptr<DomainBoolean> pReifDomain;

		/// Utility function: returns the reif domain in "aReif"
    std::shared_ptr<DomainBoolean> getReifDomain(const ConstraintParameterSPtr& aReif);
  };
  
} // end namespace Core
