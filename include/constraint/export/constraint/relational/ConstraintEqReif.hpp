//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/31/2017
//

#pragma once

// Base class
#include "ConstraintBinaryReif.hpp"

namespace Core {

	class CORE_CONSTRAINT_EXPORT_CLASS ConstraintEqReif : public ConstraintBinaryReif {
	public:
		ConstraintEqReif(const ConstraintParameterSPtr& aP1,
										 const ConstraintParameterSPtr& aP2,
										 const ConstraintParameterSPtr& aReif,
			               PropagatorStrategyType aStrategyType);
	};

	class CORE_CONSTRAINT_EXPORT_CLASS ConstraintEqHalfReifLeft : public ConstraintBinaryReif {
	public:
		ConstraintEqHalfReifLeft(const ConstraintParameterSPtr& aP1,
			                       const ConstraintParameterSPtr& aP2,
			                       const ConstraintParameterSPtr& aReif,
			                       PropagatorStrategyType aStrategyType);
	};

	class CORE_CONSTRAINT_EXPORT_CLASS ConstraintEqHalfReifRight : public ConstraintBinaryReif {
	public:
		ConstraintEqHalfReifRight(const ConstraintParameterSPtr& aP1,
			                        const ConstraintParameterSPtr& aP2,
			                        const ConstraintParameterSPtr& aReif,
			                        PropagatorStrategyType aStrategyType);
	};

} // end namespace Core
