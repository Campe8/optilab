//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 11/01/2017
//

#pragma once

// Base class
#include "ConstraintBinaryReif.hpp"

namespace Core {

	class CORE_CONSTRAINT_EXPORT_CLASS ConstraintLqReif : public ConstraintBinaryReif {
	public:
		ConstraintLqReif(const ConstraintParameterSPtr& aP1,
										 const ConstraintParameterSPtr& aP2,
										 const ConstraintParameterSPtr& aReif);
	};

	class CORE_CONSTRAINT_EXPORT_CLASS ConstraintLqHalfReifLeft : public ConstraintBinaryReif {
	public:
		ConstraintLqHalfReifLeft(const ConstraintParameterSPtr& aP1,
			                       const ConstraintParameterSPtr& aP2,
			                       const ConstraintParameterSPtr& aReif);
	};

	class CORE_CONSTRAINT_EXPORT_CLASS ConstraintLqHalfReifRight : public ConstraintBinaryReif {
	public:
		ConstraintLqHalfReifRight(const ConstraintParameterSPtr& aP1,
			                        const ConstraintParameterSPtr& aP2,
			                        const ConstraintParameterSPtr& aReif);
	};

} // end namespace Core
