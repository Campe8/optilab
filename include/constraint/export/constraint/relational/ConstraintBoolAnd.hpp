//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 03/23/2017
//

#pragma once

// Base class
#include "ConstraintBinary.hpp"
#include "ConstraintTernary.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintBoolAndBin : public ConstraintBinary {
  public:
    // X /\ Y
    ConstraintBoolAndBin(const ConstraintParameterSPtr& aParam1, const ConstraintParameterSPtr& aParam2);
    
    virtual ~ConstraintBoolAndBin();
  };
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintBoolAndTer : public ConstraintTernary {
  public:
    // (X /\ Y) <=> R
    ConstraintBoolAndTer(const ConstraintParameterSPtr& aParam1,
                         const ConstraintParameterSPtr& aParam2,
                         const ConstraintParameterSPtr& aParam3);
    
    virtual ~ConstraintBoolAndTer();
  };
  
} // end namespace Core
