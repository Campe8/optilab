//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 03/24/2017
//

#pragma once

// Base class
#include "ConstraintBinary.hpp"
#include "ConstraintTernary.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintBoolEq : public ConstraintBinary {
  public:
    // X == Y
    ConstraintBoolEq(const ConstraintParameterSPtr& aParam1, const ConstraintParameterSPtr& aParam2);
    
    virtual ~ConstraintBoolEq();
  };
  
} // end namespace Core
