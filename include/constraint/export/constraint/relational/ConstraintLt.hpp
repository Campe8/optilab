//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Luca Foschiani on 02/27/2017
//
// Base class for constraints
//

#pragma once

// Base class
#include "ConstraintBinary.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintLt : public ConstraintBinary {
  public:
    /// Constructor default propagation is bound
    ConstraintLt(const ConstraintParameterSPtr& aParam1, const ConstraintParameterSPtr& aParam2);
    
    virtual ~ConstraintLt();
    
  private:
    /// Register Lt propagator according to the
    /// type of the variables involved in this constraint
    void registerLtPropagator();
  };
  
} // end namespace Core
