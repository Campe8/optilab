//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Luca Foschiani on 02/27/2017
//
// Constraint Not Equal:
// X != Y
//

#pragma once

// Base class
#include "ConstraintBinary.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintNq : public ConstraintBinary {
  public:
    virtual ~ConstraintNq() = default;
    
  private:
    friend class ConstraintFactory;
    
    /// Constructor default propagation is bound
    ConstraintNq(const ConstraintParameterSPtr& aParam1, const ConstraintParameterSPtr& aParam2);
    
    /// Register Nq propagator according to the
    /// type of the variables involved in this constraint
    void registerNqPropagator();
  };
  
} // end namespace Core
