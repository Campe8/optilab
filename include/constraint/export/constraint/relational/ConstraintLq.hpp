//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Luca Foschiani on 02/25/2017
//
// Base class for constraints
//

#pragma once

// Base class
#include "ConstraintBinary.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintLq : public ConstraintBinary {
  public:
    /// Constructor default propagation is bound
    ConstraintLq(const ConstraintParameterSPtr& aParam1, const ConstraintParameterSPtr& aParam2);
    
    virtual ~ConstraintLq();
    
  private:
    /// Register Lq propagator according to the
    /// type of the variables involved in this constraint
    void registerLqPropagator();
  };
  
} // end namespace Core
