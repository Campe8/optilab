//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 11/01/2017
//

#pragma once

// Base class
#include "ConstraintBinaryReif.hpp"

namespace Core {

	class CORE_CONSTRAINT_EXPORT_CLASS ConstraintLtReif : public ConstraintBinaryReif {
	public:
		ConstraintLtReif(const ConstraintParameterSPtr& aP1,
										 const ConstraintParameterSPtr& aP2,
										 const ConstraintParameterSPtr& aReif);
	};

	class CORE_CONSTRAINT_EXPORT_CLASS ConstraintLtHalfReifLeft : public ConstraintBinaryReif {
	public:
		ConstraintLtHalfReifLeft(const ConstraintParameterSPtr& aP1,
			                       const ConstraintParameterSPtr& aP2,
			                       const ConstraintParameterSPtr& aReif);
	};

	class CORE_CONSTRAINT_EXPORT_CLASS ConstraintLtHalfReifRight : public ConstraintBinaryReif {
	public:
		ConstraintLtHalfReifRight(const ConstraintParameterSPtr& aP1,
			                        const ConstraintParameterSPtr& aP2,
			                        const ConstraintParameterSPtr& aReif);
	};

} // end namespace Core
