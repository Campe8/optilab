//
// Copyright OptiLab 2016-2017. All rights reserved.
//
// Created by Federico Campeotto on 01/04/2017
//
// Base class for constraints
//

#pragma once

// Base class
#include "ConstraintBinary.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintEq : public ConstraintBinary {
  public:
    virtual ~ConstraintEq();
    
  private:
    friend class ConstraintFactory;
    
    /// Constructor default propagation is bound
    ConstraintEq(const ConstraintParameterSPtr& aParam1, const ConstraintParameterSPtr& aParam2,
                 PropagatorStrategyType aStrategyType = PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND);
    
    /// Register Eq propagator according to the
    /// type of the variables involved in this constraint
    void registerEqPropagator(PropagatorStrategyType aStrategyType);
  };
  
} // end namespace Core
