//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 11/01/2017
//

#pragma once

// Base class
#include "ConstraintBinaryReif.hpp"

namespace Core {

	class CORE_CONSTRAINT_EXPORT_CLASS ConstraintNqReif : public ConstraintBinaryReif {
	public:
		ConstraintNqReif(const ConstraintParameterSPtr& aP1,
										 const ConstraintParameterSPtr& aP2,
										 const ConstraintParameterSPtr& aReif);
	};

	class CORE_CONSTRAINT_EXPORT_CLASS ConstraintNqHalfReifLeft : public ConstraintBinaryReif {
	public:
		ConstraintNqHalfReifLeft(const ConstraintParameterSPtr& aP1,
			                       const ConstraintParameterSPtr& aP2,
			                       const ConstraintParameterSPtr& aReif);
	};

	class CORE_CONSTRAINT_EXPORT_CLASS ConstraintNqHalfReifRight : public ConstraintBinaryReif {
	public:
		ConstraintNqHalfReifRight(const ConstraintParameterSPtr& aP1,
			                        const ConstraintParameterSPtr& aP2,
			                        const ConstraintParameterSPtr& aReif);
	};

} // end namespace Core
