//
// Copyright OptiLab 2016-2017. All rights reserved.
//
// Created by Federico Campeotto on 01/04/2017
//
// Base class for constraints
//

#pragma once

// Base class
#include "Constraint.hpp"

#ifndef P0
#define P0 0
#endif // !P0

#ifndef P1
#define P1 1
#endif // !P1

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintBinary : public Constraint {
  public:
    ConstraintBinary(ConstraintId aConstraintId,
                     PropagatorStrategyType aStrategyType,
                     const ConstraintParameterSPtr& aParam1,
                     const ConstraintParameterSPtr& aParam2);
    
    virtual ~ConstraintBinary();
    
  protected:
    PropagationCost reducedCost(const PropagationCost& aPropagationCost) override;
  };
  
} // end namespace Core
