//
// Copyright OptiLab 2016-2018. All rights reserved.
//
// Created by Federico Campeotto on 01/03/2017
//
// Base class for constraints
//

#pragma once

// Export definitions
#include "ConstraintExportDefs.hpp"

// Types and macros
#include "ConstraintDefs.hpp"

// Parameters
#include "ConstraintParameter.hpp"

// Base class
#include "CoreObject.hpp"

// Propagators
#include "Propagator.hpp"

#include <memory>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "DomainEvent.hpp"

// Forward declarations
namespace Core {
  class Variable;
  class ConstraintParameterVariable;
  class ConstraintParameterDomain;
  class ConstraintParameterDomainArray;
  
  typedef Variable* VariablePtr;
  typedef std::unique_ptr<Variable> VariableUPtr;
  typedef std::shared_ptr<Variable> VariableSPtr;
  
  class Constraint;
  using ConstraintSPtr = std::shared_ptr<Constraint>;
  using Fitness = double;
} // end namespace Core

namespace Semantics {
  class ConstraintStore;
}// end namespace Semantics

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS Constraint : public CoreObject, public Semantics::ConstraintObserver {
  public:
    
    /// Undefined value for fitness
    static const Fitness FitnessUndef;
    
    /// Minimum fitness value
    static const Fitness FitnessMin;
    
    /// Invalid fitness value
    static const Fitness FitnessInvalid;
    
    /// @note unsubscribe and deregister this constraint from each subject
    virtual ~Constraint();
    
    /// Attaches this observer to all subjects.
    /// Subjects (i.e., domains) will be able to notify this observer (i.e., this constraints)
    void attachToSubjects();
    
    /// Detaches this observer from all subjects.
    /// Subjects (i.e., domains) won't be able to notify this observer
    /// (i.e., this constraints) anymore
    void detachFromSubjects();
    
    /// Returns the scope size of this constraint
    inline std::size_t scopeSize() const { return pScope.size(); }
    
    /// Returns the UUID of the variable in the scope of this constraint
    /// given its cardinal position within the scope.
    /// @note the position is zero-based.
    /// @note throws if "aScopeCardinalPosition" does not match any registered
    /// variable at the given position
    inline Core::UUIDCoreObj getScopeVariableId(std::size_t aScopeCardinalPosition)
    {
      return pScope[getUUIDFromScopePosition(aScopeCardinalPosition)]->getUUID();
    }
    
    /// Returns the name ID of the variable in the scope of this constraint
    /// given its cardinal position within the scope.
    inline std::string getScopeVariableNameId(std::size_t aScopeCardinalPosition)
    {
      return pScope[getUUIDFromScopePosition(aScopeCardinalPosition)]->getVariableNameId();
    }
    
    inline ConstraintId getConstraintId() const { return pConstraintId; }
    
    inline PropagatorStrategyType getStrategyType() const { return pStrategyType; }
    
    /// Sets this constraint as an optimization constraint
    inline void setOptimizationConstraint() { pOptimizationConstraint = true; }
    
    /// Returns true if this is an optimization constraint, false otherwise.
    /// @note an "optimization constraint" is a constraint that has at least one optimization
    /// variable among the variables in its scope
    inline bool isOptimizationConstraint() const { return pOptimizationConstraint; }
    
    /// Returns true if all variables in the scope are ground variables,
    /// false otherwise.
    /// @note Returns true if the scope does not contain any variables
    bool isScopeGround() const;
    
    /// Subscribes this constraint to "aConstraintStore",
    /// i.e., register this constraint within "aConstraintStore".
    /// @note unsubscribes from current subscribed constraint store if any
    virtual void subscribeToConstraintStore(const std::shared_ptr<Semantics::ConstraintStore>& aConstraintStore);
    
    /// Unsubscribes this constraint from the constraint store this constraint
    /// is subscribed to if any
    virtual void unsubscribeFromConstraintStore();
    
    /// Forces the re-evaluation of this constraint on the constraint store this constraint
    /// is registered on (if any)
    virtual void forceReevaluation();
    
    /// Returns the cost of this constraint.
    /// @note returns undefined cost if no propagator is registered
    PropagationCost getCost();
    
    /// Returns the fitness value of this constraint.
    /// The fitness is an indicator of "how much the constraint is satisfied".
    /// For example, given the constraint
    /// X = Y
    /// its fitness value can be calculated as
    /// | X - Y |
    /// @note that different functions can be used to calculate the fitness of a constraint.
    /// For example, for the constraint above, another way to calculate the fitness
    /// may be given following function:
    /// f(X, Y) = X == Y ? 0 : 1;
    /// @note the lower the better.
    /// @note the lower bound should is 0.
    /// @note returns -1.0 if the fitness cannot be calculated (e.g., variables in the scope
    /// are not ground)
    virtual Fitness getFitness();
    
    /// Posts the registered propagators and returns PropagationEvent where:
    /// - PROP_EVENT_SUBSUMED: if all registered propagators are subsumed
    /// - PROP_EVENT_FAIL: if at least one registered propagator failed
    /// - PROP_EVENT_RUN_UNSPEC: all other cases
    /// @note returns PROP_EVENT_SUBSUMED if no propagator is registered
    virtual PropagationEvent post();
    
    /// Propagate this constraint by propagating all registered internal propagators.
    /// Returns PropagationEvent where:
    /// - PROP_EVENT_SUBSUMED: if all registered propagators are subsumed
    /// - PROP_EVENT_FIXPOINT: if all registered propagators reached the fix point
    /// - PROP_EVENT_FAIL: if at least one registered propagator failed
    /// - PROP_EVENT_NO_FIXPOINT: if at least one registered propagator did not reach the fix point
    ///                           or all other cases not considered above
    /// @note returns PROP_EVENT_SUBSUMED if no propagator is registered
    virtual PropagationEvent propagate();
    
    /// Resets internal state as this constraint has been just instantiated
    /// and subscribed to constraint store.
    /// For example, it enables all subsumed propagators
    void resetState();
    
    /// Checks if "aConstraintObserverEvent" needs to be notified to this ConstraintObserver.
    /// Returns true if it does, false otherwise.
    bool requiresNotification(const Core::ConstraintObserverEvent* aConstraintObserverEvent) override;
    
    /// Update internal status or notify observers
    /// according to "aDomainEvent".
    /// @note this will trigger a reevaluation of this constraint
    /// in the registered constraint store if any
    void updateOnDomainEvent(DomainEvent* aDomainEvent) override;
    
  protected:
    Constraint(ConstraintId aConstraintId, PropagatorStrategyType aStrategyType);
    
    /// Registers "aConstraintParameter" as a new parameter of this constraint
    void registerConstraintParameter(const ConstraintParameterSPtr& aConstraintParameter);
    void registerParameterVariable(ConstraintParameterVariable* aPVar);
    void registerParameterDomain(ConstraintParameterDomain* aPDom);
    void registerParameterArray(ConstraintParameterDomainArray* aPArr);
    
    /// Returns the number of registered constraint parameters
    inline std::size_t numConstraintParameters() const
    {
      return pRegisteredDomains.size();
    }
    
    /// Returns the Domain instance corresponding to the given
    /// parameter index "aParameterIndex".
    /// @note throws if there is no parameter registered for
    /// the index "aParameterIndex"
    inline DomainSPtr getParameterDomain(std::size_t aParameterIndex)
    {
      return pRegisteredDomains.at(aParameterIndex).first;
    }
    
    /// Register "aPropagator" in the internal register-map.
    /// If a propagator of the same type of "aPropagator" has already been registered,
    /// this method add "aPropagator" to the list of propagators of
    /// type of "aPropagator".
    /// @note "aPropagator" should be instantiated with the variables and domains
    /// previously registered through "registerConstraintParameter".
    /// It is possible to register a propagator defined on local variables but
    /// those variables won't be attached to any subject and won't receive
    /// any notification, i.e., they will remain local to the constraint
    void registerPropagator(const PropagatorSPtr& aPropagator);
    
    /// Returns a propagator of type "aPropagatorSemanticType" if it was previously registered,
    /// otherwise throws.
    /// @note by default returns the first registered propagator of type "aPropagatorSemanticType".
    /// Use "propIdx" zero-based index to specify which propagator to return
    inline PropagatorSPtr getRegisteredPropagator(PropagatorSemanticType aPropagatorSemanticType, std::size_t aPropIdx = 0)
    {
      assert(aPropIdx < pPropagatorRegister.at(static_cast<int>(aPropagatorSemanticType)).size());
      return pPropagatorRegister.at(static_cast<int>(aPropagatorSemanticType))[aPropIdx];
    }
    
    /// Returns the number of registered propagators of type "aPropagatorSemanticType"
    inline std::size_t numRegisteredPropagators(PropagatorSemanticType aPropagatorSemanticType) const
    {
      if (pPropagatorRegister.find(static_cast<int>(aPropagatorSemanticType)) == pPropagatorRegister.end())
      {
        return 0;
      }
      return pPropagatorRegister.at(static_cast<int>(aPropagatorSemanticType)).size();
    }
    
    /// Returns true if the registered propagator of type "aPropagatorSemanticType"
    /// and index "propIdx" (default 0) is currently disabled, false otherwise.
    /// @note returns true if the propagator is not registered
    inline bool isPropagatorDisabled(PropagatorSemanticType aPropagatorSemanticType, std::size_t aPropIdx = 0)
    {
      try
      {
        return getRegisteredPropagator(aPropagatorSemanticType, aPropIdx)->isPropagatorDisabled();
      }
      catch(...)
      {
        return true;
      }
    }
    
    /// Disables the registered propagator of type "aPropagatorSemanticType"
    /// and index "propIdx" (default 0).
    /// @note throws if the propagator is not registered
    inline void disablePropagator(PropagatorSemanticType aPropagatorSemanticType, std::size_t aPropIdx = 0)
    {
      getRegisteredPropagator(aPropagatorSemanticType, aPropIdx)->disablePropagator();
    }
    
    /// Enables the registered propagator of type "aPropagatorSemanticType"
    /// and index "propIdx" (default 0).
    /// @note throws if the propagator is not registered
    inline void enablePropagator(PropagatorSemanticType aPropagatorSemanticType, std::size_t aPropIdx = 0)
    {
      getRegisteredPropagator(aPropagatorSemanticType, aPropIdx)->enablePropagator();
    }
    
    /// Returns the reduced cost given "aPropagationCost", i.e.,
    /// checks if cost is <= LINEAR and if so it returns the cost
    /// considering the size of the scope.
    /// @default returns "aPropagationCost"
    virtual PropagationCost reducedCost(const PropagationCost& aPropagationCost);
    
    /// Removes this constraint from the stored constraints in the constraint store
    void removeStoredConstraintFromConstraintStore();
    
  private:
    using ConstraintStoreSPtr = std::shared_ptr<Semantics::ConstraintStore>;

  private:
    /// Constraint ID
    ConstraintId pConstraintId;
    
    /// Strategy type used by this constraint
    PropagatorStrategyType pStrategyType;
    
    /// Optimization constraint flag
    bool pOptimizationConstraint;
    
    /// Constraint store this constraint is subscribed to
    ConstraintStoreSPtr pConstraintStore;
    
    /// Map between cardinal positions of variables in
    /// the scope of this constraint and their UUID tag.
    /// The cardinal position is zero-based
    std::unordered_map<std::size_t, UUIDCoreObj> pCardinalTagMap;
    
    /// Scope of this constraint
    std::unordered_map<UUIDCoreObj, VariableSPtr> pScope;
    
    /// Map of propagators implementing this constraint:
    /// <(int)PropagatorSemanticType, vector of FilteringAlgorithm ptr>
    std::unordered_map<int, std::vector<PropagatorSPtr>> pPropagatorRegister;
    
    /// Internal ds mapping a unique int identifier to a pair
    /// <PropagatorSemanticType, std::size_t propagator index>
    /// used by the pPropagatorRegister map
    std::unordered_map<int, std::pair<int, std::size_t>> pInternalPropagatorMap;
    
    /// Set of subsumed propagators identified by their index
    /// as stored in the internal map
    std::unordered_set<int> pSubsumedPropagatorSet;
    
    /// Domains registered in this Constraint (part of its scope).
    /// This vector contains pairs
    /// <DomainSPtr, bool>
    /// containing a pointer to the domain and a flag indicating
    /// if the domains is a subject of this observer
    std::vector<std::pair<DomainSPtr, bool>> pRegisteredDomains;
    
    /// Array of domain events that trigger reevaluation of this constraint
    std::vector<std::shared_ptr<DomainEvent>> reevaluationEventMap;
    
    /// Constraint observer event
    std::unique_ptr<ConstraintObserverEvent> pConstraintObserverEvent;
    
    /// Returns true if "aDomainEvent" triggers reevaluation of this constraint,
    /// false otherwise
    bool isEventConsideredForReevaluation(DomainEvent* aDomainEvent) const;
    
    /// Notify all registered domains about "aConstraintEvent"
    void notifyRegisteredDomainsOnEvent();
    
    /// Registers "aVariable" within the scope of this constraint using the
    /// given cardinal position "aScopeCardinalPosition".
    /// @note this method does not check the size of the scope. "aScopeCardinalPosition" can be
    /// completely unrelated to the size of the scope.
    /// @note this method overrides any variable which has been previously registered
    /// at position "aScopeCardinalPosition"
    void registerScopeVariable(const VariableSPtr& aVariable);
    
    /// Returns the pointer to the variable in the scope of this constraint
    /// given its cardinal position within the scope.
    /// @note the position is zero-based.
    /// @note throws if "aScopeCardinalPosition" does not match any registered
    /// variable at the given position
    inline VariableSPtr getScopeVariable(std::size_t aScopeCardinalPosition)
    {
      return pScope[getUUIDFromScopePosition(aScopeCardinalPosition)];
    }
    
    inline bool isPropagatorSubsumed(int aPropIdx) const
    {
      return pSubsumedPropagatorSet.find(aPropIdx) != pSubsumedPropagatorSet.end();
    }
    
    inline UUIDCoreObj getUUIDFromScopePosition(std::size_t aScopePosition)
    {
      return pCardinalTagMap.at(aScopePosition);
    }
    
    inline PropagatorSPtr getPropagatorFromInternalMap(int aPropagatorIdx)
    {
      return pPropagatorRegister[pInternalPropagatorMap.at(aPropagatorIdx).first][pInternalPropagatorMap.at(aPropagatorIdx).second];
    }
    
  };
  
} // end namespace Core
