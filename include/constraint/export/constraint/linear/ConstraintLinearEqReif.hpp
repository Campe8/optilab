//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/31/2017
//

#pragma once

// Base class
#include "ConstraintLinearReif.hpp"

namespace Core {

	class CORE_CONSTRAINT_EXPORT_CLASS ConstraintLinearEqReif : public ConstraintLinearReif {
	public:
		ConstraintLinearEqReif(const ConstraintParameterSPtr& aCoeff,
													 const ConstraintParameterSPtr& aDomVar,
											     const ConstraintParameterSPtr& aConst,
													 const ConstraintParameterSPtr& aReif);
	};

	class CORE_CONSTRAINT_EXPORT_CLASS ConstraintLinearEqHalfReifLeft : public ConstraintLinearReif {
	public:
		ConstraintLinearEqHalfReifLeft(const ConstraintParameterSPtr& aCoeff,
			                             const ConstraintParameterSPtr& aDomVar,
			                             const ConstraintParameterSPtr& aConst,
			                             const ConstraintParameterSPtr& aReif);
	};

	class CORE_CONSTRAINT_EXPORT_CLASS ConstraintLinearEqHalfReifRight : public ConstraintLinearReif {
	public:
		ConstraintLinearEqHalfReifRight(const ConstraintParameterSPtr& aCoeff,
			                              const ConstraintParameterSPtr& aDomVar,
			                              const ConstraintParameterSPtr& aConst,
			                              const ConstraintParameterSPtr& aReif);
	};

} // end namespace Core
