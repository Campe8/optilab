//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/27/2017
//

#pragma once

// Base class
#include "ConstraintLinear.hpp"

namespace Core {

	class CORE_CONSTRAINT_EXPORT_CLASS ConstraintLinearNq : public ConstraintLinear {
	public:
		/// Constructor default propagation is bound
		ConstraintLinearNq(const ConstraintParameterSPtr& aCoeff,
											 const ConstraintParameterSPtr& aDomVar,
											 const ConstraintParameterSPtr& aConst);
	};

} // end namespace Core
