//
// Copyright OptiLab 2016-2017. All rights reserved.
//
// Created by Federico Campeotto on 10/26/2017
//

#pragma once

// Base class
#include "ConstraintLinear.hpp"

namespace Core {

	class CORE_CONSTRAINT_EXPORT_CLASS ConstraintLinearEq : public ConstraintLinear {
	public:
		/// Constructor default propagation is bound
		ConstraintLinearEq(const ConstraintParameterSPtr& aCoeff,
											 const ConstraintParameterSPtr& aDomVar,
											 const ConstraintParameterSPtr& aConst);
	};

} // end namespace Core
