//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/31/2017
//

#pragma once

// Base class
#include "ConstraintLinearReif.hpp"

namespace Core {

	class CORE_CONSTRAINT_EXPORT_CLASS ConstraintLinearLqReif : public ConstraintLinearReif {
	public:
		ConstraintLinearLqReif(const ConstraintParameterSPtr& aCoeff,
													 const ConstraintParameterSPtr& aDomVar,
											     const ConstraintParameterSPtr& aConst,
													 const ConstraintParameterSPtr& aReif);
	};

	class CORE_CONSTRAINT_EXPORT_CLASS ConstraintLinearLqHalfReifLeft : public ConstraintLinearReif {
	public:
		ConstraintLinearLqHalfReifLeft(const ConstraintParameterSPtr& aCoeff,
			                             const ConstraintParameterSPtr& aDomVar,
			                             const ConstraintParameterSPtr& aConst,
			                             const ConstraintParameterSPtr& aReif);
	};

	class CORE_CONSTRAINT_EXPORT_CLASS ConstraintLinearLqHalfReifRight : public ConstraintLinearReif {
	public:
		ConstraintLinearLqHalfReifRight(const ConstraintParameterSPtr& aCoeff,
			                              const ConstraintParameterSPtr& aDomVar,
			                              const ConstraintParameterSPtr& aConst,
			                              const ConstraintParameterSPtr& aReif);
	};

} // end namespace Core
