//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/31/2017
//

#pragma once

// Base class
#include "ConstraintLinearReif.hpp"

namespace Core {

	class CORE_CONSTRAINT_EXPORT_CLASS ConstraintLinearNqReif : public ConstraintLinearReif {
	public:
		ConstraintLinearNqReif(const ConstraintParameterSPtr& aCoeff,
													 const ConstraintParameterSPtr& aDomVar,
											     const ConstraintParameterSPtr& aConst,
													 const ConstraintParameterSPtr& aReif);
	};

	class CORE_CONSTRAINT_EXPORT_CLASS ConstraintLinearNqHalfReifLeft : public ConstraintLinearReif {
	public:
		ConstraintLinearNqHalfReifLeft(const ConstraintParameterSPtr& aCoeff,
			                             const ConstraintParameterSPtr& aDomVar,
			                             const ConstraintParameterSPtr& aConst,
			                             const ConstraintParameterSPtr& aReif);
	};

	class CORE_CONSTRAINT_EXPORT_CLASS ConstraintLinearNqHalfReifRight : public ConstraintLinearReif {
	public:
		ConstraintLinearNqHalfReifRight(const ConstraintParameterSPtr& aCoeff,
			                              const ConstraintParameterSPtr& aDomVar,
			                              const ConstraintParameterSPtr& aConst,
			                              const ConstraintParameterSPtr& aReif);
	};

} // end namespace Core
