//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/28/2017
//

#pragma once

// Base class
#include "ConstraintLinear.hpp"

namespace Core {

	class CORE_CONSTRAINT_EXPORT_CLASS ConstraintLinearLt : public ConstraintLinear {
	public:
		/// Constructor default propagation is bound
		ConstraintLinearLt(const ConstraintParameterSPtr& aCoeff,
											 const ConstraintParameterSPtr& aDomVar,
											 const ConstraintParameterSPtr& aConst);
	};

} // end namespace Core
