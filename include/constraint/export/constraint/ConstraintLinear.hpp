//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/26/2017
//
// Base class for linear constraints
//

#pragma once

// Base class
#include "Constraint.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintLinear : public Constraint {
	protected:
		ConstraintLinear(ConstraintId aConstraintId,
                     PropagatorStrategyType aStrategyType,
                     const ConstraintParameterSPtr& aCoeff,
                     const ConstraintParameterSPtr& aDomVar,
                     const ConstraintParameterSPtr& aConst);
    
    PropagationCost reducedCost(const PropagationCost& aPropagationCost) override;

	protected:
		/// Returns the array of coefficients
		inline DomainElementArray& getCoeffArray()
		{
			return *pCoeffArray;
		}

		/// Returns the array of domains
		inline DomainArray& getDomainArray()
		{
			return *pDomArray;
		}

		/// Returns the constant
		inline DomainElement* getConstant()
		{
			return pConstant;
		}

	private:
		/// Size of the arrays of the linear constraint
		std::size_t pLinearSize;

		/// Array of coefficients
		DomainElementArraySPtr pCoeffArray;

		/// Array of domains
		DomainArraySPtr pDomArray;

		/// Constant
		DomainElement* pConstant;

		/// Returns the array of domain elements representing the
		/// array of coefficients of the linear constraint.
		/// @note "aCoeff" must be a constraint parameter domain array
		DomainElementArraySPtr getCoeffArray(const ConstraintParameterSPtr& aCoeff);

		/// Returns the array of domains representing the
		/// array variables in the linear constraint.
		/// @note "aDom" must be a constraint parameter domain array
		DomainArraySPtr getDomainArray(const ConstraintParameterSPtr& aDom);

		/// Returns the domain element corresponding to the constant
		/// constraint parameter.
		/// @note "aConst" must be a singleton constraint parameter domain
		DomainElement* getConstant(const ConstraintParameterSPtr& aConst);
  };
  
} // end namespace Core
