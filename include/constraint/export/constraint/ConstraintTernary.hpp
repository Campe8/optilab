//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 01/19/2017
//
// Base class for ternary constraints
//

#pragma once

// Base class
#include "Constraint.hpp"

#ifndef P0
#define P0 0
#endif // !P0

#ifndef P1
#define P1 1
#endif // !P1

#ifndef P2
#define P2 2
#endif // !P3

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintTernary : public Constraint {
  public:
      ConstraintTernary(ConstraintId aConstraintId,
                        PropagatorStrategyType aStrategyType,
                        const ConstraintParameterSPtr& aParam1,
                        const ConstraintParameterSPtr& aParam2,
                        const ConstraintParameterSPtr& aParam3);
    
      virtual ~ConstraintTernary();
    
  protected:
    PropagationCost reducedCost(const PropagationCost& aPropagationCost) override;
  };
  
} // end namespace Core
