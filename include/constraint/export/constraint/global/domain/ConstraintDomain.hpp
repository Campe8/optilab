//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Luca Foschiani on 04/26/2018
//
// Constraint Domain
//

#pragma once

#include "Constraint.hpp"
#include "PropagatorDomain.hpp"
#include "ConstraintDomain.hpp"

#include "ConstraintLinear.hpp"
#include "ConstraintParameterVariable.hpp"
#include "ConstraintParameterDomain.hpp"
#include "ConstraintParameterDomainArray.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintDomain : public Constraint {
  private:
    friend class ConstraintFactory;
    ConstraintDomain(const ConstraintParameterSPtr& aDomainArray,
                     const ConstraintParameterSPtr& aLowerBound,
                     const ConstraintParameterSPtr& aUpperBound);
    
  private:
    /// Array of domains to propagate on
    DomainArraySPtr pDomArray;
    
    DomainElement* pLB;
    DomainElement* pUB;
  };
  
} // end namespace Core
