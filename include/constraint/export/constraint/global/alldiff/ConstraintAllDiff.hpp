//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 1/25/2018
//
// alldifferent constraint.
//

#pragma once

// Base class
#include "Constraint.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintAllDiff : public Constraint {
  private:
    friend class ConstraintFactory;
    ConstraintAllDiff(const ConstraintParameterSPtr& aDomainList);
    
  private:
    /// Array of domains to propagate on
    DomainArraySPtr pDomArray;
  };
  
} // end namespace Core
