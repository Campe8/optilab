//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Luca Foschiani on 5/15/2018
//
// circuit constraint.
//

#pragma once

// Base class
#include "Constraint.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintCircuit : public Constraint {
  private:
    friend class ConstraintFactory;
    ConstraintCircuit(const ConstraintParameterSPtr& aDomainList);
    
  private:
    /// Array of input domains
    DomainArraySPtr pDomArray;
    
    /// Array of "successor" domains
    DomainArraySPtr succDomArray;
  };
  
} // end namespace Core
