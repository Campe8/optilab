//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 03/13/2017
//
// Constraint Element
//

#pragma once

// Base class
#include "Constraint.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintElement : public Constraint {
  public:
    /// Constructor for "aParamArray[aParamIdx] = aParamAssign".
    /// @note "aParamArray" must be of type VariableArray of primitive type variables all of the same type.
    /// @note "aParamIdx" must be a VariableInteger.
    /// @note "aParamAssign" must have the same type as the primitive type variables in "aParamArray"
    ConstraintElement(const ConstraintParameterSPtr& aParamIdx,
                      const ConstraintParameterSPtr& aParamArray,
                      const ConstraintParameterSPtr& aParamAssign);
    
    /// Constructor for "aParamArray[aParamIdx] = aParamAssign".
    /// @note "aElementArray" must contain domain elements all of the same type as the type of "aParamAssign".
    /// @note "aParamIdx" must be a VariableInteger.
    ConstraintElement(ConstraintParameter& aParamIdx, DomainElementArray& aElementArray, ConstraintParameter& aParamAssign);

    virtual ~ConstraintElement();
  
  private:
    /// Pointer to the element array
    DomainSPtrArraySPtr pDomainArray;
    
    /// Registers propagator element based on content of "pDomainArray"
    void registerPropagatorElement();
    
    /// Returns true if "pDomainArray" contains only singleton elements, false otherwise
    bool isArraySingleton() const;
  };
  
} // end namespace Core
