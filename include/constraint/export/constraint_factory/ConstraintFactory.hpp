//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 03/16/2017
//
// Factory class for constraints.
//

#pragma once

#include "Constraint.hpp"

#include "ConstraintExportDefs.hpp"
#include "ConstraintParameter.hpp"

#include <vector>

// Forward declarations
namespace Core {
  class DomainElementInt64;
  
  class ConstraintFactory;
  typedef ConstraintFactory* ConstraintFactoryPtr;
  typedef std::unique_ptr<ConstraintFactory> ConstraintFactoryUPtr;
  typedef std::shared_ptr<ConstraintFactory> ConstraintFactorySPtr;
} // end namespace Core

namespace Core {
  
  class CORE_VARIABLE_EXPORT_CLASS ConstraintFactory {
  public:
    using ParamVec = std::vector<ConstraintParameterSPtr>;
    
  public:
    /// Returns a pointer to a new instance of a Constraint of type "aConstraintId" with given parameters and strategy type.
    /// @note default strategy type is bound.
    /// @note this is an helper method that generalizes constraint instantiation
    Constraint* constraint(ConstraintId aCID, const ParamVec& aCP,
                           PropagatorStrategyType aStrategyType = PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND);
  };
  
} // end namespace Core
