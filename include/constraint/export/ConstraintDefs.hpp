//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/09/2016
//

#pragma once

namespace Core {
  
  //
  // Filtering algorithms
  //
  
  enum FilteringAlgorithmClass : int {
      FILTERING_ALGO_CLASS_RELATIONAL = 0
    , FILTERING_ALGO_CLASS_ARITHMETIC
    , FILTERING_ALGO_CLASS_SETOP
    , FILTERING_ALGO_CLASS_ELEMENT
    , FILTERING_ALGO_CLASS_BOOLEAN
    , FILTERING_ALGO_CLASS_UNDEF           // must be last
  };
  
  enum FilteringAlgorithmSemanticType : int {
      FILTERING_ALGO_SEM_TYPE_LQ = 0    // <=
    , FILTERING_ALGO_SEM_TYPE_LT        // <
    , FILTERING_ALGO_SEM_TYPE_GQ        // >=
    , FILTERING_ALGO_SEM_TYPE_GT        // >
    , FILTERING_ALGO_SEM_TYPE_GQLQ      // shrink to bounds included
    , FILTERING_ALGO_SEM_TYPE_GTLT      // shrink to bounds excluded
    , FILTERING_ALGO_SEM_TYPE_EQ        // ==
    , FILTERING_ALGO_SEM_TYPE_NQ        // !=
    , FILTERING_ALGO_SEM_TYPE_TRUE      // Boolean True
    , FILTERING_ALGO_SEM_TYPE_FALSE     // Boolean False
    , FILTERING_ALGO_SEM_TYPE_ABS       // abs()
    , FILTERING_ALGO_SEM_TYPE_PLUS      // plus()
    , FILTERING_ALGO_SEM_TYPE_MINUS     // minus()
    , FILTERING_ALGO_SEM_TYPE_TIMES     // times()
    , FILTERING_ALGO_SEM_TYPE_DIVIDE    // divide()
    , FILTERING_ALGO_SEM_TYPE_INTERSECT // intersect()
    , FILTERING_ALGO_SEM_TYPE_SUBTRACT  // subtract()
    , FILTERING_ALGO_SEM_TYPE_ELEMENT   // subtract()
    , FILTERING_ALGO_SEM_TYPE_UNDEF     // must be last
  };
  
  //
  // Propagator
  //
  
  enum PropagatorStrategyType : int {
      PROPAGATOR_STRATEGY_TYPE_BOUND = 0
    , PROPAGATOR_STRATEGY_TYPE_DOMAIN
  };
  
  /*
   * Events occured during/after propagation.
   */
  enum PropagationEvent : int {
      PROP_EVENT_FAIL = 0      // Propagation failed
    , PROP_EVENT_FIXPOINT    // Propagation reached the fix point
    , PROP_EVENT_NO_FIXPOINT // Propagation has not reached the fix point
    , PROP_EVENT_SUBSUMED    // Propagation is subsumed
    , PROP_EVENT_RUN_UNSPEC  // Propagation has been performed without any specific event
    , PROP_EVENT_UNDEF
  };
  
  /*
   * Propagation priorities.
   * See "Efficient Constraint Propagation Engines"
   * by C. Schulte and P. J. Stuckey, p. 6.2.
   */
  enum PropagationPriority : int {
      PROP_PRIORITY_UNARY = 0   // One variable involved
    , PROP_PRIORITY_BINARY      // Two variables involved
    , PROP_PRIORITY_TERNARY     // Three variables involved
    , PROP_PRIORITY_LINEAR      // Linear complexity
    , PROP_PRIORITY_QUADRATIC   // Quadratic complexity
    , PROP_PRIORITY_CUBIC       // Cubic complexity
    , PROP_PRIORITY_VERY_SLOW   // Exponential complexity
    , PROP_PRIORITY_UNDEF
  };
  
  /*
   * Propagator ID (semantic related)
   */
  enum PropagatorSemanticType : int {
      PROP_SEMANTIC_TYPE_LQ = 0           // d0 <= d1
    , PROP_SEMANTIC_TYPE_NQ               // d0 != d1
    , PROP_SEMANTIC_TYPE_EQ               // d0 == d1
    , PROP_SEMANTIC_TYPE_LIN_EQ           // Sum_i(coeff_i * D_i) == c
    , PROP_SEMANTIC_TYPE_LIN_NQ           // Sum_i(coeff_i * D_i) != c
    , PROP_SEMANTIC_TYPE_LIN_LQ           // Sum_i(coeff_i * D_i) <= c
    , PROP_SEMANTIC_TYPE_LIN_LT           // Sum_i(coeff_i * D_i) < c
    , PROP_SEMANTIC_TYPE_TIMES            // d0 * d1 = d2
    , PROP_SEMANTIC_TYPE_ABS              // |d0| = d1
    , PROP_SEMANTIC_TYPE_DIV              // d0/d1 = d2
    , PROP_SEMANTIC_TYPE_LT               // d0 < d1
    , PROP_SEMANTIC_TYPE_MAX              // max(d0, d1) = d2
    , PROP_SEMANTIC_TYPE_MIN              // min(d0, d1) = d2
    , PROP_SEMANTIC_TYPE_PLUS             // d0 + d1 = d2
    , PROP_SEMANTIC_TYPE_EQ_REIF          // (d0 == d1) <-> b
    , PROP_SEMANTIC_TYPE_EQ_HREIF_R       // (d0 == d1)  -> b
    , PROP_SEMANTIC_TYPE_EQ_HREIF_L       // (d0 == d1) <-  b
    , PROP_SEMANTIC_TYPE_LQ_REIF          // (d0 <= d1) <-> b
    , PROP_SEMANTIC_TYPE_LQ_HREIF_R       // (d0 <= d1)  -> b
    , PROP_SEMANTIC_TYPE_LQ_HREIF_L       // (d0 <= d1) <-  b
    , PROP_SEMANTIC_TYPE_LIN_EQ_REIF      // (Sum_i(coeff_i * D_i) == c) <-> b
    , PROP_SEMANTIC_TYPE_LIN_EQ_HREIF_R   // (Sum_i(coeff_i * D_i) == c)  -> b
    , PROP_SEMANTIC_TYPE_LIN_EQ_HREIF_L   // (Sum_i(coeff_i * D_i) == c) <-  b
    , PROP_SEMANTIC_TYPE_LIN_LQ_REIF      // (Sum_i(coeff_i * D_i) <= c) <-> b
    , PROP_SEMANTIC_TYPE_LIN_LQ_HREIF_R   // (Sum_i(coeff_i * D_i) <= c)  -> b
    , PROP_SEMANTIC_TYPE_LIN_LQ_HREIF_L   // (Sum_i(coeff_i * D_i) <= c) <-  b
    , PROP_SEMANTIC_TYPE_LIN_NQ_REIF      // (Sum_i(coeff_i * D_i) != c) <-> b
    , PROP_SEMANTIC_TYPE_LIN_NQ_HREIF_R   // (Sum_i(coeff_i * D_i) != c)  -> b
    , PROP_SEMANTIC_TYPE_LIN_NQ_HREIF_L   // (Sum_i(coeff_i * D_i) != c) <-  b
    , PROP_SEMANTIC_TYPE_LT_REIF          // (d0 < d1) <-> b
    , PROP_SEMANTIC_TYPE_LT_HREIF_R       // (d0 < d1)  -> b
    , PROP_SEMANTIC_TYPE_LT_HREIF_L       // (d0 < d1) <-  b
    , PROP_SEMANTIC_TYPE_NQ_REIF          // (d0 != d1) <-> b
    , PROP_SEMANTIC_TYPE_NQ_HREIF_R       // (d0 != d1)  -> b
    , PROP_SEMANTIC_TYPE_NQ_HREIF_L       // (d0 != d1) <-  b
    , PROP_SEMANTIC_TYPE_BOOL_LQ          // !d0 \/ d1
    , PROP_SEMANTIC_TYPE_BOOL_LT          // !d0 /\ d1
    , PROP_SEMANTIC_TYPE_BOOL_OR_BIN      //  d0 \/ d1
    , PROP_SEMANTIC_TYPE_BOOL_OR_TER      // (d0 \/ d1) <-> b
    , PROP_SEMANTIC_TYPE_BOOL_XOR_BIN     //  d0 != d1
    , PROP_SEMANTIC_TYPE_BOOL_XOR_TER     // (d0 != d1) <-> b
    , PROP_SEMANTIC_TYPE_BOOL_NOT         // !d0 == d1
    , PROP_SEMANTIC_TYPE_BOOL_AND_BIN     //  d0 /\ d1
    , PROP_SEMANTIC_TYPE_BOOL_AND_TER     // (d0 /\ d1) <-> b
    , PROP_SEMANTIC_TYPE_BOOL_EQ          // d0 == d1
    , PROP_SEMANTIC_TYPE_BOOL_EQ_REIF     // (d0 == d1) <-> b
    , PROP_SEMANTIC_TYPE_BOOL_EQ_HREIF_R  // (d0 == d1)  -> b
    , PROP_SEMANTIC_TYPE_BOOL_EQ_HREIF_L  // (d0 == d1) <-  b
    , PROP_SEMANTIC_TYPE_BOOL_LQ_REIF     // (!d0 \/ d1) <-> b
    , PROP_SEMANTIC_TYPE_BOOL_LQ_HREIF_L  // (!d0 \/ d1) <-  b
    , PROP_SEMANTIC_TYPE_BOOL_LQ_HREIF_R  // (!d0 \/ d1)  -> b
    , PROP_SEMANTIC_TYPE_BOOL_LT_REIF     // (!d0 /\ d1) <-> b
    , PROP_SEMANTIC_TYPE_BOOL_LT_HREIF_L  // (!d0 /\ d1) <-  b
    , PROP_SEMANTIC_TYPE_BOOL_LT_HREIF_R  // (!d0 /\ d1)  -> b
    , PROP_SEMANTIC_TYPE_ARRAY_BOOL_AND   // (for all i in [0..n-1]: as[i]) <-> r
    , PROP_SEMANTIC_TYPE_ARRAY_BOOL_OR    // (exists an i in [0..n-1]: as[i]) <-> r
    
    // System
    , PROP_SEMANTIC_TYPE_NOGOOD           // d0 != d0' \/ d1 != d1' \/ ... \/ dn != dn'
    
    // Todo
    , PROP_SEMANTIC_BOOL_CLAUSE           // (exists and i in [0..n-1]: as[i]) \/ (exists and i in [0..m-1]: !bs[i])
    , PROP_SEMANTIC_TYPE_MOD              // d0 % d1 = d2
    
    // Global
    , PROP_SEMANTIC_TYPE_ALLDIFF          // di != dj \/ i, j in 1..n
    , PROP_SEMANTIC_TYPE_CIRCUIT          //
    , PROP_SEMANTIC_TYPE_DOMAIN           //
    , PROP_SEMANTIC_TYPE_ELEMENT          // array[d0] = d1
    
    , PROP_SEMANTIC_TYPE_UNDEF            // must be last
  };
  
  /*
   * Propagator semantic class
   */
  enum PropagatorSemanticClass : int {
      PROP_SEMANTIC_CLASS_RELATIONAL = 0
    , PROP_SEMANTIC_CLASS_ARITHMETIC
    , PROP_SEMANTIC_CLASS_LINEAR
    , PROP_SEMANTIC_CLASS_ARRAY
    , PROP_SEMANTIC_CLASS_GLOBAL
    , PROP_SEMANTIC_CLASS_UNDEF  // must be last
  };
  
  //
  // Constraint
  //
  
  /*
   * Constraint ID
   */
  enum ConstraintId: int {
      CON_ID_LQ = 0           // V0 <= V1
    , CON_ID_GQ               // V0 >= V1
    , CON_ID_NQ               // V0 != V1
    , CON_ID_EQ               // V0 == V1
    , CON_ID_LIN_EQ           // Sum_i(coeff_i * V_i) == c
    , CON_ID_LIN_NQ           // Sum_i(coeff_i * V_i) != c
    , CON_ID_LIN_LQ           // Sum_i(coeff_i * V_i) <= c
    , CON_ID_LIN_LT           // Sum_i(coeff_i * V_i) < c
    , CON_ID_TIMES            // V0 * V1 = V2
    , CON_ID_ABS              // |V0| = V1
    , CON_ID_DIV              // V0/V1 = V2
    , CON_ID_LT               // V0 < V1
    , CON_ID_GT               // V0 > V1
    , CON_ID_MAX              // max(V0, V1) = V2
    , CON_ID_MIN              // min(V0, V1) = V2
    , CON_ID_PLUS             // V0 + V1 = V2
    , CON_ID_MINUS            // V0 - V1 = V2
    , CON_ID_EQ_REIF          // (V0 == V1) <-> b
    , CON_ID_EQ_HREIF_R       // (V0 == V1)  -> b
    , CON_ID_EQ_HREIF_L       // (V0 == V1) <-  b
    , CON_ID_LQ_REIF          // (V0 <= V1) <-> b
    , CON_ID_LQ_HREIF_R       // (V0 <= V1)  -> b
    , CON_ID_LQ_HREIF_L       // (V0 <= V1) <-  b
    , CON_ID_GQ_REIF          // (V0 >= V1) <-> b
    , CON_ID_GQ_HREIF_L       // (V0 >= V1)  <- b
    , CON_ID_GQ_HREIF_R       // (V0 >= V1)  -> b
    , CON_ID_LIN_EQ_REIF      // (Sum_i(coeff_i * V_i) == c) <-> b
    , CON_ID_LIN_EQ_HREIF_R   // (Sum_i(coeff_i * V_i) == c)  -> b
    , CON_ID_LIN_EQ_HREIF_L   // (Sum_i(coeff_i * V_i) == c) <-  b
    , CON_ID_LIN_LQ_REIF      // (Sum_i(coeff_i * V_i) <= c) <-> b
    , CON_ID_LIN_LQ_HREIF_R   // (Sum_i(coeff_i * V_i) <= c)  -> b
    , CON_ID_LIN_LQ_HREIF_L   // (Sum_i(coeff_i * V_i) <= c) <-  b
    , CON_ID_LIN_NQ_REIF      // (Sum_i(coeff_i * V_i) != c) <-> b
    , CON_ID_LIN_NQ_HREIF_R   // (Sum_i(coeff_i * V_i) != c)  -> b
    , CON_ID_LIN_NQ_HREIF_L   // (Sum_i(coeff_i * V_i) != c) <-  b
    , CON_ID_LT_REIF          // (V0 < V1) <-> b
    , CON_ID_LT_HREIF_R       // (V0 < V1)  -> b
    , CON_ID_LT_HREIF_L       // (V0 < V1) <-  b
    , CON_ID_GT_REIF          // (V0 > V1) <-> b
    , CON_ID_GT_HREIF_L       // (V0 > V1) <- b
    , CON_ID_GT_HREIF_R       // (V0 > V1) -> b
    , CON_ID_NQ_REIF          // (V0 != V1) <-> b
    , CON_ID_NQ_HREIF_R       // (V0 != V1)  -> b
    , CON_ID_NQ_HREIF_L       // (V0 != V1) <-  b
    , CON_ID_BRANCHING        // branching constraint
    , CON_ID_NOGOOD           // nogood constraint
    , CON_ID_BOOL_AND         // d0 /\ d1 OR (d0 /\ d1) <-> b
    , CON_ID_BOOL_EQ          // d0 == d1
    
    // Todo
    , CON_ID_MOD              // d0 % d1 = d2
    
    // Global
    , CON_ID_ALLDIFF          // alldiff(V_i) \/ i in 1..n;
    , CON_ID_CIRCUIT          // circuit constraint
    , CON_ID_DOMAIN           // domain constraint
    , CON_ID_ELEMENT          // element constraint
    
    , CON_ID_UNDEF            // must be last
  };
  
} // end namespace Core

