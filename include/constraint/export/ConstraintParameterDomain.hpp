//
// Copyright OptiLab 2016-2017. All rights reserved.
//
// Created by Federico Campeotto on 03/16/2017
//

#pragma once

// Base class
#include "ConstraintParameter.hpp"

// Forward declarations
namespace Core {
  class ConstraintParameterFactory;
}

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintParameterDomain : public ConstraintParameter {
  public:
    
    static bool isa(const ConstraintParameter* aParameter);
    static const ConstraintParameterDomain* cast(const ConstraintParameter* aParameter);
    static ConstraintParameterDomain* cast(ConstraintParameter* aParameter);
    
    inline DomainSPtr getDomain() const
    {
      return pDomParameter;
    }
    
  protected:
    friend class ConstraintParameterFactory;
    
    ConstraintParameterDomain(const DomainSPtr& aDomain);
    
  private:
    DomainSPtr pDomParameter;
  };
  
} // end namespace Core
