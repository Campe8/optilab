//
// Copyright OptiLab 2016-2017. All rights reserved.
//
// Created by Federico Campeotto on 03/16/2017
//

#pragma once

// Base class
#include "ConstraintParameter.hpp"

// Forward declarations
namespace Core {
  class ConstraintParameterFactory;
}

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintParameterVariable : public ConstraintParameter {
  public:
    
    static bool isa(const ConstraintParameter* aParameter);
    static const ConstraintParameterVariable* cast(const ConstraintParameter* aParameter);
    static ConstraintParameterVariable* cast(ConstraintParameter* aParameter);
    
    inline VariableSPtr getVariable() const
    {
      return pVarParameter;
    }
    
  protected:
    friend class ConstraintParameterFactory;
    
    ConstraintParameterVariable(const VariableSPtr& aVariable);
    
  private:
    VariableSPtr pVarParameter;
  };
  
} // end namespace Core
