//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/27/2016
//
// Represents the propagator cost and associated priority
//

#pragma once

// Export definitions
#include "ConstraintExportDefs.hpp"

// Types and macros
#include "ConstraintDefs.hpp"

#include <iostream>

namespace Core {
  class PropagationCost;
}

namespace Core {

  bool PropagationCostSort(const PropagationCost &lhs, const PropagationCost &rhs);
  
  class CORE_CONSTRAINT_EXPORT_CLASS PropagationCost {
  public:
    PropagationCost(PropagationPriority aPriority);
    
    PropagationCost(const PropagationCost& aOther);
    
    PropagationCost(PropagationCost&& aOther);
    
    ~PropagationCost();
    
    PropagationCost& operator=(const PropagationCost& aOther);
    
    PropagationCost& operator=(PropagationCost&& aOther);
    
    bool operator<(const PropagationCost& aOtherCost) const
    {
      return static_cast<int>(pPriority) < static_cast<int>(aOtherCost.pPriority);
    }
    
    bool operator==(const PropagationCost& aOtherCost) const
    {
      return static_cast<int>(pPriority) == static_cast<int>(aOtherCost.pPriority);
    }
    
    bool operator!=(const PropagationCost& aOtherCost) const
    {
      return static_cast<int>(pPriority) != static_cast<int>(aOtherCost.pPriority);
    }
    
    inline PropagationPriority getCost() const
    {
      return pPriority;
    }
    
  private:
    PropagationPriority pPriority;
  };
  
} // end namespace Core

// Specialize template hash function for PropagationCost
namespace std
{
  template <>
  struct hash<Core::PropagationCost>
  {
    std::size_t operator()(const Core::PropagationCost& aPropagationCost) const
    {
      using std::size_t;
      using std::hash;
      
      using std::hash;
      using std::size_t;
      return hash<int>()(static_cast<int>(aPropagationCost.getCost()));
    }
  };
}// end namespace std

