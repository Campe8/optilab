//
// Copyright OptiLab 2016-2017. All rights reserved.
//
// Created by Federico Campeotto on 03/16/2017
//
// Constraint parameter base class.
//

#pragma once

// Export definitions
#include "ConstraintExportDefs.hpp"

// Types and macros
#include "ConstraintDefs.hpp"

#include "Variable.hpp"
#include "Domain.hpp"

#include <boost/core/noncopyable.hpp>

// Forward declarations
namespace Core {
  class ConstraintParameter;
  
  typedef ConstraintParameter* ConstraintParameterPtr;
  typedef std::unique_ptr<ConstraintParameter> ConstraintParameterUPtr;
  typedef std::shared_ptr<ConstraintParameter> ConstraintParameterSPtr;
}

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintParameter : private boost::noncopyable {
  public:
    /// Class holding information and instance
    /// of an input or output parameter of a constraint
    
    enum ConstraintParameterClass : int {
        CON_PARAM_CLASS_VARIABLE = 0
      , CON_PARAM_CLASS_DOMAIN
      , CON_PARAM_CLASS_DOMAIN_ARRAY
      , CON_PARAM_CLASS_DOMAIN_MATRIX_2D
    };
    
    inline ConstraintParameterClass getParameterClass() const
    {
      return pParameterClass;
    }
    
    /// Returns true if this parameter acts as a subject notifying observers
    inline bool isSubject() const
    {
      return pActAsSubject;
    }
    
  protected:
    ConstraintParameter(ConstraintParameterClass aParamClass);
    
    /// Sets this parameter as subject,
    /// i.e., it will be attached to an observer
    /// and the constraint owning this parameter
    /// will be notified whenever the object owned by
    /// this parameter changes
    inline void setAsSubject(bool aIsSubject)
    {
      pActAsSubject = aIsSubject;
    }
    
  private:
    ConstraintParameterClass pParameterClass;
    
    /// Notification flag, if true, this parameter
    /// becomes a subject that notifies the observer
    /// represented by the constraint this parameter is for
    bool pActAsSubject;
  };
  
} // end namespace Core
