//
// Copyright OptiLab 2016-2017. All rights reserved.
//
// Created by Federico Campeotto on 03/16/2017
//
// Constraint parameter base class.
//

#pragma once

// Export definitions
#include "ConstraintExportDefs.hpp"

// Types and macros
#include "ConstraintDefs.hpp"

#include "VariableFactory.hpp"
#include "DomainFactory.hpp"

#include "ConstraintParameterVariable.hpp"
#include "ConstraintParameterDomain.hpp"

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintParameterFactory {
  public:
    /// Class holding information and instance
    /// of an input or output parameter of a constraint
    ConstraintParameterFactory();
    
    virtual ~ConstraintParameterFactory() = default;
    
    inline std::shared_ptr<DomainFactory> domainFactory() const
    {
      return pDomainFactory;
    }
    
    /// Returns a constraint parameter for "aVariable"
    inline ConstraintParameterUPtr constraintParameterVariable(const VariableSPtr& aVariable)
    {
      return ConstraintParameterUPtr(new ConstraintParameterVariable(aVariable));
    }
    
    /// Returns a constraint parameter containing a VariableArray with element of "aVariableArray".
    /// @note the semantic of the returned variable is "Support"
    ConstraintParameterUPtr constraintParameterVariable(const std::vector<VariableSPtr>& aVariableArray);
    
    /// Returns a constraint parameter for "aDomain"
    inline ConstraintParameterUPtr constraintParameterDomain(const DomainSPtr& aDomain)
    {
      return ConstraintParameterUPtr(new ConstraintParameterDomain(aDomain));
    }
    
    /// Returns a constraint parameter containing a DomainSPtrArraySPtr with element of "aDomainArray".
    ConstraintParameterUPtr constraintParameterDomain(const std::vector<DomainSPtr>& aDomainArray);
    
    /// Returns a constraint parameter containing a DomainSPtrArraySPtr with element of "aDomainArray".
    /// @note aDomainClass should be congruent with the class of the domains in "aDomainElementArray"
    ConstraintParameterUPtr constraintParameterDomain(const std::vector<DomainElement*>& aDomainElementArray,
                                                      DomainClass aDomainClass);
    
  private:
    /// Variable factory
    std::unique_ptr<VariableFactory> pVariableFactory;
    
    /// Domain factory
    std::shared_ptr<DomainFactory> pDomainFactory;
  };
  
} // end namespace Core
