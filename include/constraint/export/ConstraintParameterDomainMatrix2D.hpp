//
// Copyright OptiLab 2016-2017. All rights reserved.
//
// Created by Federico Campeotto on 03/16/2017
//

#pragma once

// Base class
#include "ConstraintParameter.hpp"

#include "DMatrix2D.hpp"

// Forward declarations
namespace Core {
  class ConstraintParameterFactory;
}

namespace Core {
  
  class CORE_CONSTRAINT_EXPORT_CLASS ConstraintParameterDomainMatrix2D : public ConstraintParameter {
  public:
    static bool isa(const ConstraintParameter* aParameter)
    {
      return aParameter &&
      aParameter->getParameterClass() == ConstraintParameter::ConstraintParameterClass::CON_PARAM_CLASS_DOMAIN_MATRIX_2D;
    }
    
    static ConstraintParameterDomainMatrix2D* cast(ConstraintParameter* aParameter)
    {
      if (!isa(aParameter))
      {
        return nullptr;
      }
      return static_cast<ConstraintParameterDomainMatrix2D*>(aParameter);
    }
    
    static const ConstraintParameterDomainMatrix2D* cast(const ConstraintParameter* aParameter)
    {
      if (!isa(aParameter))
      {
        return nullptr;
      }
      return static_cast<const ConstraintParameterDomainMatrix2D*>(aParameter);
    }
    
    inline DomainMatrix2DSPtr getDomainMatrix2D() const
    {
      return pDomParameter;
    }
    
  protected:
    friend class ConstraintParameterFactory;
    
    ConstraintParameterDomainMatrix2D(const DomainMatrix2DSPtr& aDomainMatrix2D);
    
  private:
    DomainMatrix2DSPtr pDomParameter;
  };
  
} // end namespace Core
