#pragma once

#include "dll_export.h"

/* This header is being included by files inside this module */

#define GHOST_EXPORT_CLASS      DLL_EXPORT_SYM
#define GHOST_EXPORT_STRUCT     DLL_EXPORT_SYM
#define GHOST_EXPORT_FRIEND     DLL_EXPORT_SYM
#define GHOST_EXPORT_TEMPLATE   DLL_EXPORT_TEMPLATE
#define GHOST_EXPORT_EXTERN_C   extern "C" DLL_EXPORT_SYM

