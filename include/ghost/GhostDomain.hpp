/*
 * GHOST (General meta-Heuristic Optimization Solving Tool) is a C++ library 
 * designed to help developers to model and implement optimization problem 
 * solving. It contains a meta-heuristic solver aiming to solve any kind of 
 * combinatorial and optimization real-time problems represented by a CSP/COP. 
 *
 * GHOST has been first developped to help making AI for the RTS game
 * StarCraft: Brood war, but can be used for any kind of applications where 
 * solving combinatorial and optimization problems within some tenth of 
 * milliseconds is needed. It is a generalization of the Wall-in project.
 * Please visit https://github.com/richoux/GHOST for further information.
 * 
 * Copyright (C) 2014-2017 Florian Richoux
 *
 * This file is part of GHOST.
 * GHOST is free software: you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * GHOST is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with GHOST. If not, see http://www.gnu.org/licenses/.
 */

#pragma once

#include "GhostExportDefs.hpp"

#include <algorithm>  // for std::swap
#include <cstddef>    // fot std::size_t
#include <exception>
#include <iostream>
#include <iterator>
#include <vector>

#include "BaseTools.hpp"  // Random number generator
#include "DomainTypeDefs.hpp"

namespace ghost
{
  /// This class encodes domains of your CSP/COP variables.
  /// You cannot inherits your own class from Domain.
  /*! 
   * Domain is the class implementing variable domains, ie, the set of possible values a variable can take.
   * In GHOST, such values must be integers, possibly positive, negative or both. 
   */
  class GHOST_EXPORT_CLASS GhostDomain final
  {
  public:
    /*!
     * Constructor taking a vector of integer values.
     * Values in this vector will constitute the domain values.
     * For instance, the code as follows
     * 
     * std::vector<int> v{ 7, -1, 3 };
     * Domain d( v );
     * 
     * will create a domain with three values: 7, -1 and 3, in that order.
     *
     * \param domain A vector of int corresponding to the variable domain.
     */
    GhostDomain(const std::vector<INT_64>& domain);

    /*!
     * Constructor taking a starting value 'startValue' and the domain of size 'size', 
     * thus creating a domain with all integers in [startValue, startValue + size].
     *
     * \param startValue An integer specifying what is the first value in the domain.
     * \param size A size_t specifying the number of elements in the domain.
     */
    GhostDomain(INT_64 startValue, std::size_t size);

    //! Default destructor.
    ~GhostDomain() = default;
    
    /*!
     * Unique copy constructor.
     *
     * \param other A const reference to a Domain object.
     */
    GhostDomain(const GhostDomain& other);

    /// Move copy constructor
    GhostDomain(GhostDomain&& other);
    
    /*!
     * Copy assignment operator follwing the copy-and-swap idiom.
     * 
     * \param other A Domain object.
     */
    GhostDomain& operator=(GhostDomain other);
    
    /*!
     * Inline function returning a random value from the domain,
     * following a near-uniform distribution.
     * 
     * \sa Random
     */
    inline INT_64 random_value() const
    {
      return _domain[_random.getRandomNumber(static_cast<int>(_size))];
    }

    /*!
     * Return the number of values currently composing the domain.
     * 
     * \return A size_t corresponding to the size of the domain.
     */
    inline std::size_t get_size() const
    {
      return _size;
    }

    /*!
     * Inline function to get the full domain.
     *
     * \return A const reference to the vector of integer within Domain representing the domain.
     */
    inline const std::vector<INT_64>& get_domain() const
    {
      return _domain;
    }

    /*!
     * Get the value at the given index. 
     * 
     * \param index is the index of the desired value.
     * \return The value at the given index if this one is within the domain range,
     *         or raises an indexException otherwise.
     */    
    INT_64 get_value(INT_64 index) const;

    /*!
     * Get the index of a given value.
     * 
     * \return If the given value is in the domain, returns its index;
     *         raises a valueException otherwise.
     */ 
    INT_64 index_of(INT_64 value) const;

    /// To have a nicer stream of Domain.
    friend std::ostream& operator<<(std::ostream& os, const GhostDomain& domain)
    {
      os << "Size: " <<  domain._size << "\nDomain: ";
      std::copy(std::begin(domain._domain ), std::end( domain._domain ),
                std::ostream_iterator<int>(os, " "));
      return os << "\n";
    }
    
  private:
    /// Vector of integers containing the current values of the domain
    std::vector<INT_64> _domain;
    
    /// Vector of integers containing indexes of current values of the domain
    std::vector<INT_64> _indexes;
    
    /// Min value, used for indexes
    INT_64 _minValue;
    
    /// Max value
    INT_64 _maxValue;
    
    /// Size of _domain, i.e., number of elements it contains
    std::size_t _size;
    
    /// A random generator used by the function randomValue
    Base::Tools::SimpleRandomGenerator _random;
    
    /*
     Why having both domain and indexes vectors?
     
     The domain vector contains integers modelling possible values of a variable.
     Such values can be {7, -1, 3}.
     Then, your domain can be not canonically ordered and have 'holes',
     i.e., non-contiguous integers.
     Thus, it can be more convient for Variable objects to handle the index of their value
     in the domain rather than their value itself.
     Indeed, taking the next value in the domain is just incrementing the current index,
     rather than searching for the next possible value in the domain.
     However, it is necessary to know which index is associated to a value,
     for instance for seting a new value (in fact, a new index) to a variable.
     We had then two choices: searching for such an index in the domain vector
     or storing all indexes in another vector. For speed sake, we chose this second option.
     */
    struct indexException : std::exception
    {
      const char* what() const noexcept { return "Wrong index passed to Domain::get_value.\n"; }
    };
    
    struct valueException : std::exception
    {
      const char* what() const noexcept { return "Wrong value passed to Domain::index_of.\n"; }
    };
    
    /// For the copy-and-swap idiom
    void swap(GhostDomain &other);
  };
  
}// namespace ghost
