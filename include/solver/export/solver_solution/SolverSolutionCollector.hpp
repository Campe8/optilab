//
// Copyright OptiLab 2019. All rights reserved.
//
// Created by Federico Campeotto on 05/07/2019
//
// Collector of engine solutions.
//

#pragma once

#include "SolverExportDefs.hpp"

#include <memory>   // for std::shared_ptr
#include <mutex>
#include <string>
#include <utility>  // for std::pair
#include <vector>

#include "DomainElement.hpp"
#include "Model.hpp"
#include "SolutionManager.hpp"

namespace Solver {
  
  class SOLVER_EXPORT_CLASS SolverSolutionCollector {
  public:
    using SPtr = std::shared_ptr<SolverSolutionCollector>;
    
    /// Domain component of a solution as list of bounds [[LB_1, UB_1], ..., [LB_n, UB_n]]
    using DomainBounds = std::pair<Core::DomainElement*, Core::DomainElement*>;
    using DomainList = std::vector<DomainBounds>;
    
    /// A solution element is a pair
    /// <variable_id, [[LB_1, UB_1], ..., [LB_n, UB_n]]>
    using SolutionElement = std::pair<std::string, DomainList>;
    
    /// A solution is a vector of solution elements,
    /// one for each variable in the solution set
    using Solution = std::vector<SolutionElement>;
    
    /// Collection of solutions
    using SolutionCollection = std::vector<Solution>;
    
  public:
    /// Constructor, creates a new instance of a solution collector on the given
    /// map of model variables.
    /// @throw std::invalid_argument if the given map pointer is empty
    SolverSolutionCollector(const Model::ModelVarMapSPtr& varMap);
    
    /// Reads the solution from the given solution manager and sets it internally
    /// according to the given search status.
    /// @throw std::invalid_argument if the given solution manager pointer is empty
    void readSolution(Search::SearchStatus searchStatus,
                      const Search::SolutionManager::SPtr& solutionManager);
    
    /// Returns the status of the solution
    inline Search::SearchStatus searchStatus() const { return pSearchStatus; }
    
    /// Returns the total number of collected solutions
    std::size_t numSolutions() const;
    
    /// Returns the solution identified by the given 0-based index.
    /// @throw std::out_of_range if the given index is greater or equal
    /// to the number of collected solutions
    const Solution& getSolution(std::size_t idx) const;
    
    /// Returns the full collection of recorded solutions
    const SolutionCollection& getSolutionCollection() const;

    /// Helper function: returns the domain pair <is_singleton, [LB, UB]> of a solution element.
    /// @note domain elements are represented as strings.
    /// @note this method assumes that the DomainElementSolution in SolutionElement are sorted
    /// w.r.t. the primitive components of the variable in "aSolElem", i.e., visiting rows sequentially
    /// if the variable in "aSolElem" is a matrix
    static std::pair<bool, std::vector<std::vector<std::string>>>
    getVariableDomain(const DomainList& domainList);
    
  private:
    /// Lock type on the internal mutex
    using LockGuard = std::lock_guard<std::mutex>;
    
  private:
    /// Status of this solution
    Search::SearchStatus pSearchStatus;
    
    /// Map of pairs <ID, Variable>
    Model::ModelVarMapSPtr pIDVarMap;
    
    /// Register of solutions
    SolutionCollection pSolutionCollection;
    
    /// Mutex synchronizing on the internal collection of solutions
    mutable std::mutex pMutex;
    
    /// Utility function: returns reference to the ID-Var map
    inline Model::ModelVarMap& modelVarMap() { return *pIDVarMap; }
  };
  
} // end namespace Solver
