//
// Copyright OptiLab 2017-2019. All rights reserved.
//
// Created by Federico Campeotto on 04/10/2017
//
// Output stream for solver solutions.
//

#pragma once

#include "SolverExportDefs.hpp"

#include <iostream>

#include "Model.hpp"
#include "ModelCallbackHandler.hpp"
#include "SolverSolutionCollector.hpp"

// Forward declarations
namespace Core {
  class DomainElement;
}

namespace Solver {
  
  class SOLVER_EXPORT_CLASS OutputConverterSolverSolution {
  public:
    /// Returns true if the given solution is an output solution,
    /// i.e., a solution that can be sent to the output stream.
    /// Returns false otherwise
    bool isOutputSolution(const Model::ModelSPtr& model,
                          const ModelCallbackHandler::Solution& solution) const;
    
    /// Returns the output stream containing "aSolution" in the format:
    /// "variable id":'\t'domain(\n)
    /// where '\n' is not added to the last line
    std::ostream& ostreamSolution(std::ostream& OStream, const Model::ModelSPtr& model,
                                  const ModelCallbackHandler::Solution& solution);
  };
  
} // end namespace Solver
