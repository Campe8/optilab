//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 04/09/2017
//
// Solutions returned by a solver engine.
//

#pragma once

#include "SolverExportDefs.hpp"

#include "SearchDefs.hpp"

// Solution manager
#include "SolutionManager.hpp"

#include "Variable.hpp"

#include <vector>
#include <utility>
#include <memory>
#include <set>
#include <unordered_map>

// Forward declarations
namespace Core {
  class DomainElement;
}// end namespace Core

namespace Solver {
  
  class SOLVER_EXPORT_CLASS SolverSolution {
  public:
    using SPtr = std::shared_ptr<SolverSolution>;
    
    // Domain component of a solution [LB, UB]
    using DomainBounds = Search::SolutionManager::BoundsPair;
    using DomainBoundsList = std::vector<DomainBounds>;
    
    // A solution element is a pair
    // <variable, [LB, UB]->[LB, UB]-->...->[LB, UB]>
    // one for each variable primitive component
    using SolutionElement = std::pair<Core::VariableSPtr, DomainBoundsList>;
    
    // A solution is a vector of solution elements, one for each variable in the solution set
    using Solution = std::vector<SolutionElement>;
    
  public:
    /// Constructor taking a search status and a solution manager
    SolverSolution(const Model::ModelVarMapSPtr& aVarMap);
    
    /// Reads the solution from the given solution manager
    void readSolution(Search::SearchStatus aSearchStatus,
                      const std::shared_ptr<Search::SolutionManager>& aSManager);
    
    /// Returns the status of the solution
    inline Search::SearchStatus searchStatus() const { return pSearchStatus; }
    
    /// Returns the total number of solutions
    inline std::size_t numSolutions() const { return pSolutionRegister.size(); }
    
    /// Returns the solution identified by "aIdx".
    /// @note "aIdx" must be consistent with the recorded number of solutions.
    /// @note "aIdx" is zero-based
    inline const Solution& getSolution(std::size_t aIdx) const
    {
      assert(aIdx < numSolutions());
      return pSolutionRegister[aIdx];
    }
    
    /// Helper function: returns the ID of the variable in "aSElem"
    static std::string getVariableID(const SolutionElement& aSElem);
    
    /// Helper function: returns the domain pair [LB, UB] of a solution element.
    /// @note domain elements are represented as strings.
    /// @note this method assumes that the DomainElementSolution in SolutionElement are sorted
    /// w.r.t. the primitive components of the variable in "aSolElem", i.e., visiting rows sequentially
    /// if the variable in "aSolElem" is a matrix
    static std::vector<std::vector<std::string>> getVariableDomain(const SolutionElement& aSolElem);
    
    /// Returns true if the given domain (from "getVariableDomain") is singleton, false otherwise
    static bool isVariableDomainSingleton(const std::vector<std::vector<std::string>>& aDomain);
    
  private:
    /// Status of this solution
    Search::SearchStatus pSearchStatus;
    
    /// Map of pairs <ID, Variable>
    Model::ModelVarMapSPtr pIDVarMap;
    
    /// Register of solutions
    std::vector<Solution> pSolutionRegister;
    
    /// Utility function: returns reference to the ID-Var map
    inline Model::ModelVarMap& modelVarMap()
    {
      return *pIDVarMap;
    }
    
  };
  
} // end namespace Solver
