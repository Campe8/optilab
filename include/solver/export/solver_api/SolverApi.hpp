//
// Copyright OptiLab 2019. All rights reserved.
//
// Created by Federico Campeotto on 05/05/2019
//
// The API for interacting with a solver.
//

#pragma once

#include "SolverExportDefs.hpp"

#include <cstddef>  // for std::size_t
#include <memory>   // for std::shared_ptr
#include <mutex>
#include <string>
#include <unordered_map>

#include "Engine.hpp"
#include "EngineCallbackHandler.hpp"
#include "EngineEvent.hpp"
#include "Model.hpp"

namespace Solver {
  
  class SOLVER_EXPORT_CLASS SolverApi {
  public:
    using SPtr = std::shared_ptr<SolverApi>;
    
    enum SolverError {
        kNoError = 0
      , kError
    };
    
  public:
    /// Creates and registers new instance of an engine with given identifier.
    /// Returns a SolverError type.
    /// @note this method is thread-safe.
    /// @note this method returns an error if the same engine is registered twice.
    /// @note this method returns an error is there is no handler previously registered.
    /// @note this method never throws
    SolverError createEngine(const std::string& aEngineId) noexcept;
    
    /// Deletes the engine instance "engineId" from the internal map.
    /// Returns a SolverError type.
    /// @note this method is thread-safe.
    /// @note returns an error on deleting an engine that is not registered.
    /// @note this method never throws
    SolverError deleteEngine(const std::string& aEngineId) noexcept;
    
    /// Loads the model "aModel" into the engine with given identifier.
    /// @note this method is thread-safe.
    /// @note returns SolverError::kError if the engine with given identifier is not registered.
    /// @note returns SolverError::kError on empty model
    SolverError loadModel(const std::string& aEngineId, const Model::ModelSPtr& aModel) noexcept;
    
    /// Runs the engine "aEngineId" on its loaded model and returns asap.
    /// @note this method DOES NOT wait for the engine to complete running.
    /// @note this method is thread-safe.
    /// @note returns SolverError::kError if the engine with given identifier is not registered
    /// or on any other exception thrown by the engine (e.g., no model is loaded)
    SolverError runModel(const std::string& aEngineId) noexcept;
    
    /// Collects "aNumSol" solutions produced by the engine with given identifier.
    /// @note if "aNumSol" is greater than the actual number of solutions, return the
    /// actual number of solutions.
    /// @note this method DOES NOT wait for the engine to complete running.
    /// Use "engineWait(...)" method to wait for the engine to finish running.
    /// @note this method is thread-safe.
    /// @note returns SolverError::kError if the engine with given identifier is not registered
    SolverError collectSolutions(const std::string& aEngineId, std::size_t aNumSol) noexcept;
    
    /// Waits for the engine with specified identifier to complete
    /// all the tasks in its queue of tasks.
    /// If "aTimeoutSec" is -1, wait for all the queue to be empty, otherwise wait
    /// for "aTimeoutSec" seconds.
    /// @note returns SolverError::kError if the engine with given identifier is not registered
    SolverError engineWait(const std::string& aEngineId, int aTimeoutSec) noexcept;
    
    /// Sets the given handler as engine handler.
    /// @note returns error if the given handler is empty
    SolverError setEngineHandler(const EngineCallbackHandler::SPtr& aHandler) noexcept;
    
  private:
    /// Map of engines used by this solver Api
    using EngineMap = std::unordered_map<std::string, Engine::SPtr>;
    
    /// Lock type on the internal mutex
    using LockGuard = std::lock_guard<std::recursive_mutex>;
    
  private:
    /// Map storing the instances of the engines
    EngineMap pEngineMap;
    
    /// Mutex synchronizing on the internal map
    std::recursive_mutex pMutex;
    
    /// Callback handler used to send back messages to the caller of this API
    EngineCallbackHandler::SPtr pEngineHandler;
    
    /// Utility function: returns the instance of the engine registered in the map
    /// with given identifier. Returns an empty instance if no such engine is found.
    /// @note this method is thread-safe
    Engine::SPtr getEngine(const std::string& aEngineId);
  };
  
} // end namespace Solver
