//
// Copyright OptiLab 2019. All rights reserved.
//
// Created by Federico Campeotto on 04/27/2019
//
// Class for an asynchronous Constraint Programming engine.
//

#pragma once

#include "Engine.hpp"

#include <atomic>
#include <cstddef>  // for std::size_t
#include <memory>   // for std::shared_ptr
#include <string>

#include "AsyncEngineProcessor.hpp"
#include "EngineCallbackHandler.hpp"

namespace Solver {
  
  class SOLVER_EXPORT_CLASS CPEngine : public Engine {
  public:
    using SPtr = std::shared_ptr<CPEngine>;
    
  public:
    /// Constructor.
    /// @throw std::invalid_argument exception on empty identifier or empty handler
    CPEngine(const std::string& aEngineId, const EngineCallbackHandler::SPtr& aHandler);
    
    /// Returns the identifier of this engine
    inline std::string getEngineIdentifier() const { return pEngineId; }
    
    /// Registers the given model.
    /// @note this method does not run the model.
    /// @throw std::runtime_error if this model is called while the engine is running.
    /// @throw std::runtime_error if this method is called more than once with a valid model
    void registerModel(const Model::ModelSPtr& aModel) override;
    
    /// Notifies the engine on a given EngineEvent.
    /// @note if the engine is run more than once, it simply returns.
    /// @throw std::runtime_error if no model is registered
    void notifyEngine(const EngineEvent& aEvent) override;
    
    /// Wait for the current jobs in the queue to finish.
    /// @note if "aTimeoutSec" is -1, waits for all jobs in the queue to complete
    void engineWait(int aTimeoutSec);
    
  private:
    /// Identifier of this engine
    std::string pEngineId;
    
    /// Flag indicating whether this engine already registered a model
    std::atomic<bool> pRegisteredModel{false};
    
    /// Flag indicating whether this engine already started running
    std::atomic<bool> pActiveEngine{false};
    
    /// Asynchronous engine processor
    AsyncEngineProcessor::SPtr pEngineProcessor;
    
    /// Handler for callbacks to send back results to caller methods
    EngineCallbackHandler::SPtr pCallbackHandler;
    
    /// Runs the registered model
    void processRunModelEvent();
    
    /// Sends back using the handler all the solutions collected so far
    void processCollectSolutionsEvent(std::size_t aNumSolutions);
  };
  
} // end namespace Solver
