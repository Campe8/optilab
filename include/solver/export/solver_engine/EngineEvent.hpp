//
// Copyright OptiLab 2019. All rights reserved.
//
// Created by Federico Campeotto on 04/26/2019
//
// Engine event class used to notify the engine on events.
//

#pragma once

#include <cstddef>  // for std::size_t

#include "Model.hpp"
#include "SolverExportDefs.hpp"

namespace Solver {
  
  class SOLVER_EXPORT_CLASS EngineEvent {
  public:
    enum EventType {
      /// Starts the solving process
        kRunModel = 0
      /// Kills any ongoing computation and return asap
      , kKill
      , kCollectSolutions
      , kUndefEvent
    };
    
  public:
    EngineEvent(EventType aType, std::size_t aNumSolutions = 0);
    
    /// Returns the type of this event
    inline EventType getType() const { return pEventType; }
    
    /// Returns the number of solutions for "kCollectSolutions" event types
    inline std::size_t getNumSolutions() const { return pNumSolutions; }
    
  private:
    /// Type of this event
    EventType pEventType;
    
    /// Number of solutions for "kCollectSolutions" events
    std::size_t pNumSolutions;
  };
  
} // end namespace Solver
