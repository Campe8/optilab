//
// Copyright OptiLab 2019. All rights reserved.
//
// Created by Federico Campeotto on 04/26/2019
//
// Base class for an optimization engine.
//

#pragma once

#include "SolverExportDefs.hpp"

#include <memory>  // for std::shared_ptr

#include <boost/core/noncopyable.hpp>

#include "EngineEvent.hpp"
#include "Model.hpp"

namespace Solver {
  
  class SOLVER_EXPORT_CLASS Engine : private boost::noncopyable {
  public:
    using SPtr = std::shared_ptr<Engine>;
    
  public:
    virtual ~Engine() = default;
    
    /// Registers the given model.
    /// @note this method does not run the model.
    /// @throw std::runtime_error if this model is called while the engine is running
    virtual void registerModel(const Model::ModelSPtr& aModel) = 0;
    
    /// Notifies the engine on a given EngineEvent
    virtual void notifyEngine(const EngineEvent& aEvent) = 0;
  };
  
} // end namespace Solver
