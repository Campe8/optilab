//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 04/09/2017
//
// Engine for the solver, runs a model on a
// search engine to compute solutions.
//

#pragma once

#include <boost/core/noncopyable.hpp>

#include "SolverExportDefs.hpp"

// Model
#include "Model.hpp"

// Engine
#include "SearchEngine.hpp"
#include "BaseSearch.hpp"

// Solution
#include "SolverSolution.hpp"

#include <memory>
#include <set>
#include <unordered_map>
#include <memory>

// Forward declarations
namespace Core {
  class Variable;
}// end namespace Core

namespace Base {
  namespace Tools {
    class MetricsRegister;
  }// end namespace Tools
}// end namespace Base

namespace Solver {
  
  class SOLVER_EXPORT_CLASS SolverEngine : private boost::noncopyable {
  public:
    /// Pair <environment_id, metrics>
    using EnvMetrics = std::pair<std::string, std::shared_ptr<Base::Tools::MetricsRegister>>;
    
  public:
    /// Unique key used for each registered model
    typedef std::size_t ModelKey;
    
    SolverEngine();
    
    virtual ~SolverEngine() = default;
    
    /// Registers a new model with this solver engine and
    /// returns its unique ModelKey.
    /// @note the returned key is unique for THIS instance
    /// of solver engine
    ModelKey registerModel(Model::ModelSPtr aModel);
    
    /// Initializes internal data structures and run
    /// the model "aKey".
    /// Informations about the results of the run are
    /// internally collected.
    /// @note throws if "aKey" cannot be found in the internal register.
    void runModel(ModelKey aKey);
    
    /// Returns the solver solution for the model "aModelKey".
    /// @note throws if "aModelKey" is not a key of a registered model
    std::shared_ptr<SolverSolution> getSolution(ModelKey aModelKey) const;
    
    /// Returns the number of metrics for a given model
    inline std::size_t getNumMetricsForModel(ModelKey aModelKey) const
    {
      return (pModelRegister.find(aModelKey) == pModelRegister.end()) ? 0 :
      pMetricsRegister.at(aModelKey).size();
    }
    
    /// Returns a EnvMetrics pair for given model and metrics index.
    /// @note throws out_of_range exception if no model is registered or the index is
    /// not consistent w.r.t. the number of metrics for the given model
    const EnvMetrics& getMetrics(ModelKey aModelKey, std::size_t aMetricIdx) const;
    
  private:
    /// List of EnvMetrics
    using MetricsList = std::vector<EnvMetrics>;
    
    using EngineResult = std::pair<std::shared_ptr<Search::SolutionManager>, Search::SearchStatus>;
    
    using SearchSchemeList = std::vector<std::shared_ptr<Search::BaseSearchScheme>>;
    
  private:
    ModelKey pModelKeySeed;
    
    /// Register of model objects
    std::unordered_map<ModelKey, Model::ModelSPtr> pModelRegister;
    
    /// Solution register, one for each model
    std::unordered_map<ModelKey, std::shared_ptr<SolverSolution>> pSolutionRegister;
    
    /// Register of metrics
    std::unordered_map<ModelKey, MetricsList> pMetricsRegister;
    
    /// Returns the set of variables that are part of the solution
    std::set<Core::VariableSPtr> getSolutionSet(Model::Model* aModel);
    
    /// Creates and runs an engine on the given list of search schemes.
    /// Returns a pair of solution manager and the search status
    EngineResult runEngine(const SearchSchemeList& aListScheme, int aSchemeIdx,
                           Model::Model* aModel, std::size_t aModelKey);
  };
  
} // end namespace Solver
