//
// Copyright OptiLab 2019. All rights reserved.
//
// Created by Federico Campeotto on 05/09/2019
//
// Standard output handler.
//

#pragma once

#include "EngineCallbackHandler.hpp"

#include <iostream>
#include <memory>  // for std::shared_ptr

namespace Solver {
  
  class SOLVER_EXPORT_CLASS StdOutCallbackHandler : public EngineCallbackHandler {
  public:
    using SPtr = std::shared_ptr<StdOutCallbackHandler>;
    
  public:
    void solutionCallback(const std::string& engineId, const std::string& json) override
    {
      std::cout << json << std::endl;
    }
    
    void metricsCallback(const std::string& engineId, const std::string& json) override
    {
      std::cout << json << std::endl;
    }
    
  };
  
} // end namespace Solver
