//
// Copyright OptiLab 2019. All rights reserved.
//
// Created by Federico Campeotto on 04/26/2019
//
// Utilities for solvers
//

#pragma once

#include <vector>

#include "JSON.hpp"

#include "SolverSolutionCollector.hpp"

namespace Solver { namespace Constants {
  
  namespace Solution {
    extern const char* SOLUTION;
    extern const char* SOLUTION_COLLECTION;
    extern const char* SOLUTION_STATUS;
    extern const char* VAR_DOM;
    extern const char* VAR_ID;
  }// end namespace Solution

  namespace Metrics {
    extern const char* METRICS_COLLECTION;
    extern const char* SEARCH_ENVIRONMENT;
  }// end namespace Metrics
  
}}// end namespace Solver
