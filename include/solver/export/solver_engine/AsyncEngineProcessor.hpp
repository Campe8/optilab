//
// Copyright OptiLab 2019. All rights reserved.
//
// Created by Federico Campeotto on 04/27/2019
//
// Asynchronous engine processor.
// It receives a sequence of tasks and perform those tasks in parallel on a
// FIFO bases using a thread pool.
//

#pragma once

#include <functional>  // for std::function
#include <memory>      // for std::unique_ptr
#include <utility>     // for std::pair
#include <vector>

#include "ctpl.hpp"
#include "MetricsRegister.hpp"
#include "Model.hpp"
#include "SolverExportDefs.hpp"
#include "SolverSolutionCollector.hpp"

namespace Solver {
  
  class SOLVER_EXPORT_CLASS AsyncEngineProcessor {
  public:
    using SPtr = std::shared_ptr<AsyncEngineProcessor>;
    
    /// SearchMetrics is a pair <id, metrics> where "id" represents the identifier
    /// of the search environment corresponding to the metrics collected from
    using SearchMetrics = std::pair<std::string, Base::Tools::MetricsRegister::SPtr>;
    
    /// List of search metrics
    using SearchMetricsList = std::vector<SearchMetrics>;
    
  public:
    AsyncEngineProcessor(int aThreadPoolSize);
    
    virtual ~AsyncEngineProcessor();
    
    /// Registers the given model.
    /// @note this method does not run the model. It is an asynchronous call which sets the
    /// task of registering "aModel" in the internal pipeline.
    /// @throw std::runtime_error if the given model is empty
    void registerModel(const Model::ModelSPtr& aModel);
    
    /// Starts the engine processor to run on the registered model
    void runModel();
    
    /// Returns the solution collected from running the registered model.
    /// @note returns an empty pointer if there is no model registered
    inline SolverSolutionCollector::SPtr getModelSolutionCollector() const
    {
      return pSolverSolutionCollector;
    }
    
    /// Returns the pointer to the metrics list
    inline std::shared_ptr<SearchMetricsList> getMetricsList() const
    {
      return pMetricsList;
    }
    
    /// Blocks the calling thread for "aTimeoutSec" seconds or, if -1, until
    /// the engine is done running the current task from the pipeline of tasks
    void processorWait(int aTimeoutSec);
    
  protected:
    /// Sets up the model to be ready for the engine which will run it
    virtual void setupModel(const Model::ModelSPtr& aModel) = 0;
    
    /// Triggers the start of the engine
    virtual void runEngine() = 0;
    
  private:
    /// Pool of threads that run tasks from a FIFO queue
    std::unique_ptr<DataStructure::ctpl::thread_pool> pThreadPool;
    
    /// Pointer to the solution collector
    SolverSolutionCollector::SPtr pSolverSolutionCollector;
    
    /// Pointer to the list of metrics
    std::shared_ptr<SearchMetricsList> pMetricsList;
    
    /// Function object for setting up a model
    std::function<void (const Model::ModelSPtr&)> setupModelFcn;
    
    /// Function object for starting the egine
    std::function<void (void)> runEngineFcn;
  };
  
} // end namespace Solver
