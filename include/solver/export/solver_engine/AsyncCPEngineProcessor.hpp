//
// Copyright OptiLab 2019. All rights reserved.
//
// Created by Federico Campeotto on 04/27/2019
//
// Asynchronous engine processor for Constraint Programming engines.
//

#pragma once

#include "AsyncEngineProcessor.hpp"

#include <string>
#include <unordered_map>
#include <utility>  // for std::pair
#include <vector>

#include "BaseSearchScheme.hpp"
#include "Model.hpp"
#include "SearchDefs.hpp"
#include "SolverExportDefs.hpp"

namespace {
  /// Default Number of threads to be used in the pool
  constexpr int kPoolNumThreads{2};
}

// Forward declarations
namespace Search {
  class SearchEngine;
  class SearchEnvironment;
}// namespace Search

namespace Model {
  class SearchDescriptor;
  class ModelSearch;
}// namespace Model

namespace Solver {
  
  class SOLVER_EXPORT_CLASS AsyncCPEngineProcessor : public AsyncEngineProcessor {
  public:
    using SPtr = std::shared_ptr<AsyncCPEngineProcessor>;
    
  public:
    AsyncCPEngineProcessor(int aThreadPoolSize = kPoolNumThreads);
    
  protected:
    /// Sets up the model to be ready for the engine which will run it
    void setupModel(const Model::ModelSPtr& aModel) override;
    
    /// Triggers the start of the engine
    void runEngine() override;
    
  private:
    /// Struct used to collect search information
    struct SearchInfo {
      /// Unique identifier of this search information object
      std::string searchInfoId;
      
      /// Unique identifier of the search strategy this informantion object is describing
      int searchStrategyId;
      
      /// Search environment associated with this search information object
      Search::SearchEnvironment* searchEnvironment;
    };
    
    /// Unit for search environment, a pair of <identifier, SearchEnvironment>
    using SrcEnvUnit = std::pair<std::string, Search::SearchEnvironment*>;
    
    /// List of search environment units
    using SrcEnvList = std::vector<SrcEnvUnit>;
    
    /// Map of list of search environment units
    using SrcEnvListMap = std::unordered_map<int, SrcEnvList>;
    
    /// List of search information objects
    using SrcInfoList = std::vector<SearchInfo>;
    
  private:
    /// Pointer to the model to solve
    Model::ModelSPtr pModel;
    
    /// Map collecting all the list of search environment units
    SrcEnvListMap pSearchEnvironmentMap;
    
    /// Utility function: initializes the search environment map with all the search environments
    /// read from the registered model
    void initSearchEnvironmentMap();
    
    /// Utility function: returns a list of search information objects obtained by reading the
    /// registered model
    SrcInfoList getEnvironmentDescriptorList();
    
    /// Utility function: creates a SearchInfo object from the givel search objects
    /// and adds it to the given list at given position.
    /// @throw exception if the index is greater than the list size
    void addSearchInfoToList(const Model::ModelSearch* aSrcObj, SearchInfo* aSrcInfo);
    
    /// Runs the given SearchEngine which will create a search tree according to the given algorithm.
    /// @note the engine must be created on the same algorithm instance
    Search::SearchStatus performSearch(Search::SearchEngine* aEngine,
                                       Search::BaseSearchScheme* aAlgorithm);
    
    /// Utility function: creates and returns a new search engine initialized to run
    /// the given algorithm
    Search::SearchEngine* createSearchEngine(const Search::BaseSearchScheme::SPtr& aAlgorithm);
  };
  
} // end namespace Solver
