//
// Copyright OptiLab 2019. All rights reserved.
//
// Created by Federico Campeotto on 05/14/2019
//
// Callback handler for the MVC infrastructure.
//

#pragma once

#include "EngineCallbackHandler.hpp"

#include <cstdio>  // for std::size_t
#include <memory>  // for std::shared_ptr
#include <string>
#include <unordered_map>
#include <utility> // for std::pair
#include <vector>

#include "MetricsRegister.hpp"
#include "SearchDefs.hpp"
#include "SolverSolutionCollector.hpp"

namespace Solver {
  
  class SOLVER_EXPORT_CLASS ModelCallbackHandler : public EngineCallbackHandler {
  public:
    using SPtr = std::shared_ptr<ModelCallbackHandler>;
    using MetricsMap = std::unordered_map<std::string, Base::Tools::MetricsRegister::SPtr>;
    
    /// Domain component of a solution as list of bounds [[LB_1, UB_1], ..., [LB_n, UB_n]]
    using DomainBounds = std::pair<std::string, std::string>;
    using DomainList = std::vector<DomainBounds>;
    
    /// A solution element is a pair
    /// <variable_id, [[LB_1, UB_1], ..., [LB_n, UB_n]]>
    using SolutionElement = std::pair<std::string, DomainList>;
    
    /// A solution is a vector of solution elements,
    /// one for each variable in the solution set
    using Solution = std::vector<SolutionElement>;
    
  public:
    void solutionCallback(const std::string& engineId, const std::string& json) override;
    
    void metricsCallback(const std::string& engineId, const std::string& json) override;

    /// Returns true if the handler collected solutions and the model is satified.
    /// Returns false otherwise.
    /// @note a model with 0 solutions can be satisfied,
    /// for example, the model 2 != 3 is always satisfied.
    bool satisfied() const;
    
    /// Returns the number of collected solutions
    inline std::size_t numSolutions() const { return pSolutionList.size(); }
    
    /// Returns the solution at given index (0-based).
    /// @throw std::out_of_range exception on invalid indices
    const Solution& getSolution(std::size_t solIdx) const;
    
    /// Returns the collection of metrics as a map of pairs:
    /// <search_id, metrics>.
    /// @note a model can have multiple search statement, each one providing different metrics
    inline const MetricsMap& getMetrics() const { return pMetricsMap; }
    
  private:
    /// Map of metrics
    MetricsMap pMetricsMap;
    
    /// Status of the search
    Search::SearchStatus pSearchStatus;
    
    /// Vector of solutions
    std::vector<Solution> pSolutionList;
  };
  
} // end namespace Solver
