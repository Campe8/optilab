//
// Copyright OptiLab 2019. All rights reserved.
//
// Created by Federico Campeotto on 05/06/2019
//
// Callback handler used to pass messages from
// engine to the caller.
//

#pragma once

#include "SolverExportDefs.hpp"

#include <memory>   // for std::shared_ptr
#include <string>


namespace Solver {
  
  class SOLVER_EXPORT_CLASS EngineCallbackHandler {
  public:
    using SPtr = std::shared_ptr<EngineCallbackHandler>;
    
  public:
    /// Callback for sending back solutions to the client
    virtual void solutionCallback(const std::string& engineId, const std::string& json) = 0;
    
    /// Callback for sending back metrics to the client
    virtual void metricsCallback(const std::string& engineId, const std::string& json) = 0;
  };
  
} // end namespace Solver
