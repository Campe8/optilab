//
// Copyright OptiLab 2019. All rights reserved.
//
// Created by Federico Campeotto on 04/26/2019
//
// Utilities for solvers
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <vector>

#include "JSON.hpp"

#include "AsyncEngineProcessor.hpp"
#include "MetricsRegister.hpp"
#include "SolverSolutionCollector.hpp"

namespace Solver { namespace Utilities {
  
  /// Returns the JSON object correspondent to the given solution collection
  DataStructure::JSON::SPtr convertSolutionToJson(const SolverSolutionCollector::Solution& solution);
  
  /// Returns the JSON object representing the collection of solutions given in input as vector
  /// of single JSON solution objects
  DataStructure::JSON::SPtr
  createJSONSolutionCollection(const std::vector<DataStructure::JSON::SPtr>& jsonSolutionCollection,
                               Search::SearchStatus searchStatus);
  
  /// Returns the JSON object representing the collection of metrics
  DataStructure::JSON::SPtr
  createJSONMetricsCollection(const AsyncEngineProcessor::SearchMetricsList& metrics);
  
}// end namespace Utilities
}// end namespace Solver
