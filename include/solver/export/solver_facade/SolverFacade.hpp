//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 04/18/2017
//
// Facade class for solver.
// It allows clients to provide a model as an input stream,
// creates a new instance of a model from the generated
// Model Abstract Syntax Tree and runs the model on a solver engine.
// Workflow is as follows:
//
//               JSON Model "m"
//                    |
//                 PARSER
//        Reads "m" and generates
//        Model AST "t"
//                    |
//             MODEL GENERATOR
//  Reads "t" and generates Model Objects "o".
//  Adds "o" object to new instance of model "M".
//                    |
//              SOLVER ENGINE
//  Reads "M" and runs a Search Engine on it.
//

#pragma once

#include <boost/core/noncopyable.hpp>

#include "SolverExportDefs.hpp"

#include "SolverApi.hpp"

// Engine
#include "SolverEngine.hpp"

// Solver Solution
#include "SolverSolution.hpp"

// Metrics
#include "MetricsRegister.hpp"

#include <istream>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <memory>

namespace Model {
  class LeafConstraintParameter;
}// end namespace model

namespace Solver {
  
  class SOLVER_EXPORT_CLASS SolverFacade : private boost::noncopyable {
  public:
    SolverFacade();
    
    virtual ~SolverFacade() = default;
    
    /// Registers the input model and returns its internal regiter key
    SolverEngine::ModelKey registerModel(const Model::ModelSPtr& aModel);
    
    /// Runs the registered model "aModelKey"
    void runModel(SolverEngine::ModelKey aModelKey);
    
    /// Returns true if the model "aModelKey" has run and
    /// is a satisfied model, false otherwise.
    /// @note a model with 0 solutions can be satisfied,
    /// for example, the model 2 != 3 is always satisfied.
    /// @note this method returns false for queries on non-run models
    bool satisfied(SolverEngine::ModelKey aModelKey) const;
    
    /// Returns the number of solutions found for registered model "aModelKey".
    /// @note if "aModelKey" does not correspond to any registered model. returns 0
    std::size_t numSolutions(SolverEngine::ModelKey aModelKey) const;
    
    /// Returns the solution "aSolutionIdx" for model "aModelKey".
    /// @note "aSolutionIdx" is 0-based and throws if correspondent solution is not present
    SolverSolution::Solution getSolution(SolverEngine::ModelKey aModelKey, std::size_t aIdx);
    
    /// Returns the metrics for the given registered model as a map of pairs:
    /// <search_id, metrics>.
    /// @note a model can have multiple search statement, each one providing different metrics
    std::unordered_map<std::string, Base::Tools::MetricsRegister::SPtr>
    getMetrics(SolverEngine::ModelKey aModelKey) const;
    
  private:
    /// Register of models
    std::unordered_set<SolverEngine::ModelKey> pModelRegister;
    
    /// Engine for models
    std::unique_ptr<SolverEngine> pSolverEngine;
  };
  
} // end namespace Solver
