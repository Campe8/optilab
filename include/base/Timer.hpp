//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 12/03/2018
//
// Timer class.
//

#pragma once

#include "dll_export.h"

#include <chrono>
#include <cstdint>
#include <memory>

// Forward declarations
namespace Base { namespace Tools {
  class Timer;
  using TimerSPtr = std::shared_ptr<Timer>;
}}//end namespace Tools/Base

namespace Base { namespace Tools {
  
  class DLL_EXPORT_SYM Timer {
  public:
    /// Constructor, if "aStart" is true (default value), starts the timer
    explicit Timer(bool aStart=true);
    
    ~Timer() = default;
    
    /// Starts the timer
    void start();
    
    /// Stops the timer
    void stop();
    
    /// Resets the timer and stops it
    void reset();
    
    /// Returns true if this timer is running, false otherwise
    inline bool isRunning() const { return pRunning; }
    
    /// Returns the elapsed time (msec) between the start of the timer and now.
    /// @note this value is the cumulative running time, i.e., it does not count the
    /// time when the timer wasn't running.
    /// @note time is expressed in milliseconds
    uint64_t getWallClockTime();
    
    /// Utility method: returns the wallclock time in seconds
    inline uint64_t getWallClockTimeSec() { return getWallClockTime() / 1000; }
    
  private:
    using Clock = std::chrono::high_resolution_clock;
    using TimePoint = Clock::time_point;
    using MSec = std::chrono::milliseconds;
    
  private:
    /// Flag indicating whether the timer is running
    bool pRunning;
    
    /// Counter of elapsed time between start and update
    MSec pElapsedTime;
    
    /// Point in time when the timer started
    TimePoint pTimePointStart;
    
    /// Current point in time
    TimePoint pTimePointCurrent;
    
    /// Updates the counter for the elapsed time
    void updateTimeCounter();
    
  };
  
}}//end namespace Tools/Base
