//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 07/13/2017
//
// Global libraries system dependent.
//


#pragma once

# if defined _WIN32 && ( \
defined _MSC_VER || \
defined __ICL    || \
defined __DMC__  || \
defined __BORLANDC__ || \
defined __MINGW32__)

// Max macro to undef on windows.h.
// See https://stackoverflow.com/questions/6884093/warning-c4003-not-enough-actual-parameters-for-macro-max-visual-studio-2010
#define NOMINMAX
    #include <stdint.h>
    #include <windows.h>
    #include <time.h>
    #include <direct.h>
    #include <io.h>
# else
    #include <sys/time.h>
    #include <sys/wait.h>
    #include <unistd.h>
# endif

/* Common dependencies */
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <cstddef>
#include <type_traits>
