#pragma once

#undef D0
#undef D1
#undef D2

#undef P0
#undef P1
#undef P2

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/functional/hash.hpp>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <boost/optional.hpp>

#include <boost/current_function.hpp>

#define P0 0
#define P1 1
#define P2 2

#define D0 0
#define D1 1
#define D2 2
