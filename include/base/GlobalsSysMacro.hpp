//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 07/13/2017
//
// Global macros system dependent.
//


#pragma once

# if defined _WIN32 && ( \
defined _MSC_VER || \
defined __ICL    || \
defined __DMC__  || \
defined __BORLANDC__ || \
defined __MINGW32__)

#define CLI_STR_CPY(aDst, aSrc) do {    \
std::size_t strLen = strlen(aSrc);      \
aDst = new char[strLen + 1];            \
strncpy_s(aDst, strLen + 1, aSrc, strLen); \
aDst[strLen] = '\0';                       \
} while (0)

#else

#define CLI_STR_CPY(aDst, aSrc) do {  \
std::size_t strLen = strlen(aSrc);    \
aDst = new char[strLen + 1];          \
strncpy(aDst, aSrc, strLen);        \
aDst[strLen] = '\0';                \
} while (0)

# endif


# if defined _WIN32 && ( \
defined _MSC_VER || \
defined __ICL    || \
defined __DMC__  || \
defined __BORLANDC__ || \
defined __MINGW32__)

#define ISSATTY _isatty

#else

#define ISSATTY isatty

# endif

# if defined _WIN32 && ( \
defined _MSC_VER || \
defined __ICL    || \
defined __DMC__  || \
defined __BORLANDC__ || \
defined __MINGW32__)
    #define STDIN_FILENO 0
# endif
