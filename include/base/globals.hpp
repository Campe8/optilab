//
//  globals.hpp
//  OptiLab
//
//  Created by Federico Campeotto on 26/06/14.
//  Copyright (c) 2014-2016 Federico Campeotto. All rights reserved.
//

#ifndef OPTILAB_globals_hpp
#define OPTILAB_globals_hpp

/* Common dependencies */
#if WINDOWS_REL
	#include <windows.h>
	#include <time.h>
  #include <direct.h>
  #define GetCurrentDir _getcwd
#else
	#include <sys/time.h>
	#include <sys/wait.h>
	#include <unistd.h>
  #define GetCurrentDir getcwd
#endif

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <cstddef>
#include <type_traits>
 
/* Input/Output */
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <exception>

/* Arithmetic and Assertions */
#include <cassert>
#include <cmath>
#include <climits>

/* STL dependencies */
#include <functional> 
#include <cctype>
#include <locale>
#include <algorithm>
#include <iterator>
#include <bitset>
#include <random>
#include <map>
#include <unordered_map>
#include <list>
#include <queue>
#include <stack>
#include <set>
#include <unordered_set>
#include <string>
#include <regex>
#include <vector>
#include <utility>
#include <memory>

#endif
