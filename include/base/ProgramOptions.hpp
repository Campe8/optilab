//
// Copyright OptiLab 2019. All rights reserved.
//
// Created by Federico Campeotto on 1/19/2019
//
#pragma once

#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>

#include <boost/program_options.hpp>

namespace Base {
  
class ProgramOptions {
public:
  ProgramOptions();
  
  ~ProgramOptions() = default;
  
  /// Parse the cli options
  void parseOptions(int argc, char* argv[]);
  
  /// Registers the option "aName".
  /// @note "aRequired" specifies whether the option requires an argument or not.
  /// @note "aName" must be non empty and "aShortName" must be an alpha char.
  /// @note "aOptInfoMessage" is the message to print about "aName" on "toStream()" method
  void registerOption(const std::string& aName, char aShortName, bool aRequired,
                      const std::string& aOptInfoMessage="");
  
  /// Returns true if "aName" is registered as an option, false otherwise
  inline bool isOptionRegistered(const std::string& aName) const
  {
    return pOptionsWithArgumentLookup.find(aName) != pOptionsWithArgumentLookup.end();
  }
  
  /// Returns true if "aName" is registered as an option, false otherwise
  inline bool isOptionRegistered(char aName) const
  {
    if (pShortToNameOptLookup.find(aName) == pShortToNameOptLookup.end()) return false;
    return isOptionRegistered(pShortToNameOptLookup.at(aName));
  }
  
  /// Returns true if the option "aName" was selected after parsing the cli input.
  /// Returns false otherwise
  inline bool isOptionSelected(const std::string& aName) const
  {
    return pOptionValuesMap.find(aName) != pOptionValuesMap.end();
  }
  
  /// Returns true if the option "aName" was selected after parsing the cli input.
  /// Returns false otherwise
  inline bool isOptionSelected(char aName) const
  {
    if (pShortToNameOptLookup.find(aName) == pShortToNameOptLookup.end()) return false;
    return isOptionSelected(pShortToNameOptLookup.at(aName));
  }
  
  /// Returns true if the option "aName" requires an argument, false otherwise
  inline bool isArgumentRequired(const std::string& aName) const
  {
    if (!isOptionRegistered(aName)) return false;
    return pOptionsWithArgumentLookup.at(aName);
  }
  
  /// Returns true if the option "aName" requires an argument, false otherwise
  inline bool isArgumentRequired(char aName) const
  {
    if (!isOptionRegistered(aName)) return false;
    return pOptionsWithArgumentLookup.at(pShortToNameOptLookup.at(aName));
  }
  
  /// Returns the values for the option "aName" which requires arguments.
  /// Returns an empty vector if the option doesn't require arguments.
  /// See also "isArgumentRequired()" method
  std::vector<std::string> getOptionValues(const std::string& aName) const;
  
  /// Returns the values for the option "aName" which requires arguments.
  /// Returns an empty vector if the option doesn't require arguments.
  /// See also "isArgumentRequired()" method
  inline std::vector<std::string> getOptionValues(char aName) const
  {
    if (!isOptionRegistered(aName)) return std::vector<std::string>();
    return getOptionValues(pShortToNameOptLookup.at(aName));
  }
  
  /// Returns the first value for the option "aName" which requires arguments.
  /// Returns an empty string if the option doesn't require arguments.
  /// See also "isArgumentRequired()" method
  inline std::string getOptionValue(const std::string& aName) const
  {
    auto args = getOptionValues(aName);
    return args.empty() ? "" : args[0];
  }
  
  /// Returns the first value for the option "aName" which requires arguments.
  /// Returns an empty vector if the option doesn't require arguments.
  /// See also "isArgumentRequired()" method
  inline std::string getOptionValue(char aName) const
  {
    if (!isOptionRegistered(aName)) return "";
    return getOptionValue(pShortToNameOptLookup.at(aName));
  }
  
  /// Prints and returns on the given output stream this ProgramOptions's help message
  std::ostream& toStream(std::ostream& aStream);
  
private:
  using OptionsDescription = boost::program_options::options_description;
  using OptionsDescriptionSPtr = std::shared_ptr<OptionsDescription>;
  
private:
  /// Pointer to the opton description instance
  OptionsDescriptionSPtr pProgramOptions;
  
  /// Map to store command line options evaluated by the parser
  boost::program_options::variables_map pOptionValuesMap;
  
  /// Lookup table mapping short option names to their correspondent long option name
  std::unordered_map<char, std::string> pShortToNameOptLookup;
  
  /// Lookup table mapping option names and required (true/false) arguments
  std::unordered_map<std::string, bool> pOptionsWithArgumentLookup;
  
  /// Utility function: returns the instance of options_description
  inline OptionsDescription& getOptionDescription() { return *pProgramOptions; }
};

}// end namespace Base
