#pragma once

#include <iostream>

#include "BoostInc.hpp"

#define OPT_TERMINATE(MSG)  \
terminate((MSG), __FILE__, __LINE__, BOOST_CURRENT_FUNCTION)

#define ASSERT_ALWAYS(MSG) (OPT_TERMINATE(MSG))

typedef boost::uuids::uuid UUIDTag;

// Generates UUIDTag
UUIDTag generateUUIDTag();

// Generates randomString
std::string generateRandomString();

// Specialization of std::hash for boost::uuid
namespace std
{
  
  template<>
  struct hash <UUIDTag>
  {
    size_t operator () (const UUIDTag& uid) const
    {
      return boost::hash<UUIDTag>()(uid);
    }
  };
  
}// end namespace std

// Terminate function printing error message
void terminate(const char* aMsg, const char* aFileName, const std::size_t aLineNumber, const char* aCurrFun);
