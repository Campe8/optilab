//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 12/08/2018
//
// Class for managing metrics and metrics registers.
//

#pragma once

#include "MetricsRegister.hpp"

#include <boost/core/noncopyable.hpp>

#include <assert.h>
#include <iostream>
#include <map>
#include <string>
#include <unordered_map>
#include <cassert>
#include <utility>


// Forward declarations
namespace Interpreter {
  class BaseObject;
}// end namespace Interpreter

namespace Base { namespace Tools {
  
  class DLL_EXPORT_SYM MetricsManager : private boost::noncopyable {
  public:
    enum MetricsCategory : int {
        TIME_METRICS = 0
      , SEARCH_METRICS
      , LAST_METRICS_CATEGORY // must be last
    };
    
  public:
    /// Singleton class managing all the metrics in the system.
    /// @note by default all metric categories are disabled
    static MetricsManager& getInstance();
    
    ~MetricsManager() = default;
    
    /// Clear all registered metrics
    void clearMetrics();
    
    /// Enable/disable metric category
    void enableMetricCategory(MetricsCategory aCategory, bool aEnabled);
    
    inline bool isCategoryEnabled(MetricsCategory aCategory) const
    {
      assert(aCategory != MetricsCategory::LAST_METRICS_CATEGORY);
      return pMetricsCollection.at(static_cast<int>(aCategory)).IsActive;
    }
    
    /// Adds a metric register on a given category with given identifier
    /// @note this method will add the metrics only if the category is enabled.
    /// @note if there is another register with the same identifier in the same category,
    /// the register will be replaced with "aMReg"
    void addMetricsOnCategory(MetricsCategory aCategory, const std::string& aMId,
                              const MetricsRegister::SPtr& aMReg);
    
    /// Creates and returns a BaseObject that has a property per metric and the
    /// correspondent value initialized to the metric value.
    /// @note only enabled metrics are used to instantiate the object
    Interpreter::BaseObject* toObject() const;
    
  private:
    struct MetricsCollection {
      /// Flag for active metric collection
      bool IsActive;
      
      /// Register of metrics
      std::map<std::string, MetricsRegister::SPtr> Collection;
    };
    
  private:
    MetricsManager();
    
    /// Unordered map storing the collection of ActiveRegister(s).
    /// Each category can have multiple registers with different ids
    std::unordered_map<int, MetricsCollection> pMetricsCollection;
  };
  
}}//end namespace Tools/Base


