//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 07/23/2017
//
// Global types.
//


#pragma once

#define BOOL_T    char
#define INT_32_T  int
#define INT_64_T  long
#define UINT_64_T unsigned long long
#define SINGLE_T  float
#define REAL_T    double
#define DOUBLE_T  double
