//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 12/08/2018
//
// Register for metrics.
//

#pragma once

#include "dll_export.h"

#include <cstdint>    // for int64_t
#include <string>
#include <map>
#include <memory>

namespace Base { namespace Tools {
  
  class DLL_EXPORT_SYM MetricsRegister {
  public:
    using MetricId = std::string;
    using MetricValue = int64_t;
    using MetricIter = std::map<MetricId, MetricValue>::iterator;
    using MetricConstIter = std::map<MetricId, MetricValue>::const_iterator;
    using SPtr = std::shared_ptr<MetricsRegister>;
    
  public:
    /// Adds a new metric into the register.
    /// @note default value for the metric is 0.
    /// @note throws invalid_argument exception if the metric is already registered
    void addMetric(const MetricId& aMetricId, MetricValue aValue=0);
    
    /// Updates the given metric with the given value.
    /// @note throws invalid_argument exception if the metric is not registered
    void updateMetric(const MetricId& aMetricId, MetricValue aValue);
    
    inline bool isMetricRegistered(const MetricId& aId) const
    {
      return pRegister.find(aId) != pRegister.end();
    }
    
    /// Returns the value of the given metric.
    /// @note throws invalid_argument exception if the given metric is not registered
    MetricValue getMetric(const MetricId& aMetricId);
    
    /// Iterators over the metrics register
    MetricIter begin() { return pRegister.begin(); }
    MetricIter end() { return pRegister.end(); }
    MetricConstIter cbegin() const { return pRegister.cbegin(); }
    MetricConstIter cend() const { return pRegister.cend(); }
    
  private:
    /// Map of pairs <metric_id, metric_value>.
    /// @note using a map instead of an unordered map to
    /// have an ordering on the metric ids
    std::map<MetricId, MetricValue> pRegister;
  };
  
}}//end namespace Tools/Base
