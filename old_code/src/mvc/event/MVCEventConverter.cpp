// Self first
#include "MVCEventConverter.hpp"

//MVC
#include "MVCEventEvalStmt.hpp"
#include "MVCModelContext.hpp"
#include "FunctionCtx.hpp"

// CLI
#include "CLIUtils.hpp"
#include "CLIMacro.hpp"

// Model
#include "ModelUtils.hpp"

#include <cassert>

namespace MVC {
  
  static bool isFcnConDecl(const CLI::CLIStmt& aStmt)
  {
    auto fcnName = CLI::Utils::getCLIStmtVal(aStmt, CLI_MSG_TAG_FCN_CALL_ID);
    assert(fcnName);
    
    return Model::Utils::isConstraintFcnName(*fcnName);
  }//isFcnConDecl
  
  static bool isFcnBranchDecl(const CLI::CLIStmt& aStmt)
  {
    auto fcnName = CLI::Utils::getCLIStmtVal(aStmt, CLI_MSG_TAG_FCN_CALL_ID);
    assert(fcnName);
    
    return *fcnName == CLI_MSG_TYPE_VALUE_BRC_DECL;
  }//isFcnBranchDecl
  
  static bool isIDExprDecl(const CLI::CLIStmt& aStmt)
  {
    auto dType = CLI::Utils::getCLIStmtVal(aStmt, CLI_MSG_TAG_CON_DECL_TYPE);
    auto eType = CLI::Utils::getCLIStmtVal(aStmt, CLI_MSG_TAG_CON_EXPR_TYPE);
    assert(dType);
    assert(eType);
    
    if (*dType == CLI_MSG_VAL_CON_DECL_TYPE_EXPR && *eType == CLI_MSG_VAL_CON_EXPR_TYPE_I)
    {
      return true;
    }
    
    return false;
  }//isIDExprDecl
  
  static bool isConstantExprDecl(const CLI::CLIStmt& aStmt)
  {
    auto dType = CLI::Utils::getCLIStmtVal(aStmt, CLI_MSG_TAG_CON_DECL_TYPE);
    auto eType = CLI::Utils::getCLIStmtVal(aStmt, CLI_MSG_TAG_CON_EXPR_TYPE);
    assert(dType);
    assert(eType);
    
    if (*dType == CLI_MSG_VAL_CON_DECL_TYPE_EXPR && *eType == CLI_MSG_VAL_CON_EXPR_TYPE_C)
    {
      return true;
    }
    
    return false;
  }//isConstantExprDecl
  
  static std::shared_ptr<MVCEvent> convertFcnDeclToConDeclEvent(const std::shared_ptr<MVCEventEvalStmt>& aEvent, MVCEventFactory* aEF)
  {
    auto stmt = aEvent->getEvalStmt();
    auto fcnName = CLI::Utils::getCLIStmtVal(stmt, CLI_MSG_TAG_FCN_CALL_ID);
    auto args = CLI::Utils::getCLIStmtVal(stmt, CLI_MSG_TAG_FCN_CALL_ARGS);
    
    auto src = aEvent->getEventSource();
    auto dst = aEvent->getEventDestination();
    auto sd = MVCEventFactory::actorsToSrcDst(src, dst);
    
    CLI::CLIStmt ctxStmt;
    CLI::Utils::addCLIStmtVal(ctxStmt, { CLI_MSG_TAG_TYPE , CLI_MSG_TYPE_VALUE_CON_DECL});
    CLI::Utils::addCLIStmtVal(ctxStmt, { CLI_MSG_TAG_CON_DECL_TYPE , CLI_MSG_VAL_CON_DECL_TYPE_CALL });
    CLI::Utils::addCLIStmtVal(ctxStmt, { CLI_MSG_TAG_CON_NAME , *fcnName });
    CLI::Utils::addCLIStmtVal(ctxStmt, { CLI_MSG_TAG_CON_PARAMS , *args });

    auto e = aEF->mvcEventEvalStmt(CLI::Utils::getStringFromCLIStmt(ctxStmt), sd);
    assert(e);
    
    return std::shared_ptr<MVCEvent>(e);
  }//convertFcnDeclToConDeclEvent
  
  static std::shared_ptr<MVCEvent> convertFcnDeclToBranchDeclEvent(const std::shared_ptr<MVCEventEvalStmt>& aEvent, MVCEventFactory* aEF)
  {
    auto stmt = aEvent->getEvalStmt();
    auto fcnName = CLI::Utils::getCLIStmtVal(stmt, CLI_MSG_TAG_FCN_CALL_ID);
    
    // Create the base template for the statements
    CLI::CLIStmt brcStmt;
    CLI::Utils::addCLIStmtVal(brcStmt, { CLI_MSG_TAG_TYPE , CLI_MSG_TYPE_VALUE_BRC_DECL });
    
    // Get the function arguments
    auto args = CLI::Utils::getCLIStmtVal(stmt, CLI_MSG_TAG_FCN_CALL_ARGS);
    if (args)
    {
      auto argList = CLI::Utils::splitArguments(*args);
      
      CLI::Utils::addCLIStmtVal(brcStmt, { CLI_MSG_TAG_BRC_BRANCHING_VAR , argList[0]});
      if (argList.size() == 1)
      {
        CLI::Utils::addCLIStmtVal(brcStmt, { CLI_MSG_TAG_BRC_VAR_CHOICE_TYPE , CLI_MSG_VAL_BRC_VAR_DEFAULT });
        CLI::Utils::addCLIStmtVal(brcStmt, { CLI_MSG_TAG_BRC_VAL_CHOICE_TYPE , CLI_MSG_VAL_BRC_VAL_DEFAULT });
      }
      else if (argList.size() == 2)
      {
        CLI::Utils::addCLIStmtVal(brcStmt, { CLI_MSG_TAG_BRC_VAR_CHOICE_TYPE , argList[1] });
        CLI::Utils::addCLIStmtVal(brcStmt, { CLI_MSG_TAG_BRC_VAL_CHOICE_TYPE , CLI_MSG_VAL_BRC_VAL_DEFAULT });
      }
      else if (argList.size() == 3)
      {
        CLI::Utils::addCLIStmtVal(brcStmt, { CLI_MSG_TAG_BRC_VAR_CHOICE_TYPE , argList[1] });
        CLI::Utils::addCLIStmtVal(brcStmt, { CLI_MSG_TAG_BRC_VAL_CHOICE_TYPE , argList[2] });
      }
    }
    
    // Create the event on the new statement
    auto src = aEvent->getEventSource();
    auto dst = aEvent->getEventDestination();
    auto sd = MVCEventFactory::actorsToSrcDst(src, dst);
    auto e = aEF->mvcEventEvalStmt(CLI::Utils::getStringFromCLIStmt(brcStmt), sd);
    assert(e);
    
    return std::shared_ptr<MVCEvent>(e);
  }//convertFcnDeclToBranchDeclEvent
  
  static std::shared_ptr<MVCEvent> convertIDDeclToCtxFcnEvent(const std::shared_ptr<MVCEventEvalStmt>& aEvent, MVCEventFactory* aEF)
  {
    auto stmt = aEvent->getEvalStmt();
    auto val = CLI::Utils::getCLIStmtVal(stmt, CLI_MSG_TAG_CON_EXPR_PREORDER);
    assert(val);
    
    auto src = aEvent->getEventSource();
    auto dst = aEvent->getEventDestination();
    auto sd = MVCEventFactory::actorsToSrcDst(src, dst);
    
    FunctionCtx fcnCtx;
    CLI::CLIStmt ctxStmt;
    CLI::Utils::addCLIStmtVal(ctxStmt, { CLI_MSG_TAG_TYPE , CLI_MSG_TYPE_VALUE_FCN_CALL_DECL });
    CLI::Utils::addCLIStmtVal(ctxStmt, { CLI_MSG_TAG_FCN_CALL_ID , fcnCtx.name() });
    CLI::Utils::addCLIStmtVal(ctxStmt, { CLI_MSG_TAG_FCN_CALL_NUM_ARGS , "1" });
    CLI::Utils::addCLIStmtVal(ctxStmt, { CLI_MSG_TAG_FCN_CALL_ARGS , *val });
    
    auto e = aEF->mvcEventEvalStmt(CLI::Utils::getStringFromCLIStmt(ctxStmt), sd);
    assert(e);
    
    return std::shared_ptr<MVCEvent>(e);
  }//convertIDDeclToCtxFcnEvent
  
  static std::shared_ptr<MVCEvent> convertConstDeclToVarDeclEvent(const std::shared_ptr<MVCEventEvalStmt>& aEvent, MVCEventFactory* aEF)
  {
    auto stmt = aEvent->getEvalStmt();
    auto val = CLI::Utils::getCLIStmtVal(stmt, CLI_MSG_TAG_CON_EXPR_PREORDER);
    assert(val);
    assert(CLI::Utils::isInteger(*val));
    
    auto src = aEvent->getEventSource();
    auto dst = aEvent->getEventDestination();
    auto sd = MVCEventFactory::actorsToSrcDst(src, dst);
    
    std::string domRange = *val;
    domRange += " " + *val;
    CLI::CLIStmt ctxStmt;
    CLI::Utils::addCLIStmtVal(ctxStmt, { CLI_MSG_TAG_TYPE , CLI_MSG_TYPE_VALUE_VAR_DECL });
    CLI::Utils::addCLIStmtVal(ctxStmt, { CLI_MSG_TAG_VAR_ID , Model::Utils::getUndefVariableName() });
    CLI::Utils::addCLIStmtVal(ctxStmt, { CLI_MSG_TAG_VAR_TYPE , "integer" });
    CLI::Utils::addCLIStmtVal(ctxStmt, { CLI_MSG_TAG_VAR_SEMANTIC_TYPE , "decision" });
    CLI::Utils::addCLIStmtVal(ctxStmt, { CLI_MSG_TAG_VAR_DOM_DEC_FORMAT , "range" });
    CLI::Utils::addCLIStmtVal(ctxStmt, { CLI_MSG_TAG_VAR_DOM_DEC_TEXT , domRange });
    
    auto e = aEF->mvcEventEvalStmt(CLI::Utils::getStringFromCLIStmt(ctxStmt), sd);
    assert(e);
    
    return std::shared_ptr<MVCEvent>(e);
  }//convertConstDeclToVarDeclEvent
  
  static std::shared_ptr<MVCEvent> convertConDecl(const std::shared_ptr<MVCEventEvalStmt>& aEvent, MVCEventFactory* aEF)
  {
    assert(CLI::Utils::getMsgType(aEvent->getEvalStmt()) == CLI::CLIMsgType::CLI_MSG_CON_DECL);
    auto stmt = aEvent->getEvalStmt();
    
    if (isIDExprDecl(stmt))
    {
      // If a constraint declaration is an identifier expr,
      // return a ctx function.
      // For example:
      // a;
      // =>
      // ctx('a');
      return convertIDDeclToCtxFcnEvent(aEvent, aEF);
    }
    else if (isConstantExprDecl(stmt))
    {
      // If a constraint declaration is a constant expr,
      // return a variable declaration where the variable is the context variable.
      // For example:
      // 3;
      // =>
      // ctx = 3;
      return convertConstDeclToVarDeclEvent(aEvent, aEF);
    }
    return aEvent;
  }//convertConDecl
  
  static std::shared_ptr<MVCEvent> convertFcnCall(const std::shared_ptr<MVCEventEvalStmt>& aEvent, MVCEventFactory* aEF)
  {
    assert(CLI::Utils::getMsgType(aEvent->getEvalStmt()) == CLI::CLIMsgType::CLI_MSG_FCN_CALL);
    auto stmt = aEvent->getEvalStmt();
    
    // Look for post constraint function calls
    if (isFcnConDecl(stmt))
    {
      // If a fcn call is a constraint declaration as fcn call,
      // convert message type from fcn call to con decl
      return convertFcnDeclToConDeclEvent(aEvent, aEF);
    }
    
    // Look for post branch function calls
    if (isFcnBranchDecl(stmt))
    {
      // If a fcn call is a branching variable declaration as fcn call,
      // convert message type from fcn call to branch decl
      return convertFcnDeclToBranchDeclEvent(aEvent, aEF);
    }
    return aEvent;
  }//convertFcnCall
  
  static std::shared_ptr<MVCEvent> convertEvalStmt(const std::shared_ptr<MVCEventEvalStmt>& aEvent, MVCEventFactory* aEF)
  {
    assert(aEF);
    
    auto msgType = CLI::Utils::getMsgType(aEvent->getEvalStmt());
    switch (msgType)
    {
      case CLI::CLIMsgType::CLI_MSG_CON_DECL:
        return convertConDecl(aEvent, aEF);
      case CLI::CLIMsgType::CLI_MSG_FCN_CALL:
        return convertFcnCall(aEvent, aEF);
      case CLI::CLIMsgType::CLI_MSG_VAR_DECL:
      default:
        return aEvent;
    }//switch
    
  }//convertEvalStmt
  
  MVCEventConverter::MVCEventConverter()
  : pEventFactory(new MVCEventFactory())
  {
  }
  
  std::shared_ptr<MVCEvent> MVCEventConverter::convert(const std::shared_ptr<MVCEvent>& aEvent)
  {
    assert(aEvent);
    auto etype = aEvent->getEventType();
    switch (etype)
    {
      case MVC::MVC_EVT_EVAL_STMT:
        return convertEvalStmt(std::static_pointer_cast<MVCEventEvalStmt>(aEvent), pEventFactory.get());
        break;
      case MVC::MVC_EVT_SOLUTION:
      case MVC::MVC_EVT_WRITE_OUTPUT:
      case MVC::MVC_EVT_ERROR:
      default:
        return aEvent;
    }
  }//convert
  
}//end namespace MVC
