// Base class
#include "CommandFcnCall.hpp"

#include <sstream>
#include <exception>

namespace MVC {
  
  CommandFcnCall::CommandFcnCall()
  : pFcnRegister(new FunctionRegister())
  {
  }
  
  void CommandFcnCall::setCallbackFcn(const std::string& aFcn, const std::vector<std::string>& aArgs)
  {
    pFcnName = aFcn;
    pArgs = aArgs;
  }//setCallbackFcn
  
  void CommandFcnCall::execute()
  {
    // Set MVC components into the function register which,
    // in turn, will set them into the invoked functions.
    // @note the controller is the actual function caller since
    // it owns this command which is executed by the controller
    pFcnRegister->setView(getView());
    pFcnRegister->setModel(getModel());
    pFcnRegister->setController(getController());
    
    auto callback = pFcnRegister->getCallbackFunction(pFcnName);
    if(!callback)
    {
      // Notify controller about function not recognized through model
      std::string err = "Undefined function " + pFcnName;
      notifyInvokerOnError(err.c_str(), MVCEvent::MVCEventActor::MVC_ACTOR_CONTROLLER);
      
      return;
    }
    
    // Execute the callback function
    try
    {
      //(*callback)(pArgs);
    }
    catch (std::exception const &e)
    {
      notifyInvokerOnError(e.what(), MVCEvent::MVCEventActor::MVC_ACTOR_CONTROLLER);
    }
  }//execute
  
}//end namespace MVC
