// Base class
#include "CommandEvalStmt.hpp"

#include "MVCModel.hpp"

#include <sstream>
#include <cassert>

namespace MVC {
  
  void CommandEvalStmt::execute()
  {
    assert(!pStmt.empty());
    try
    {
      //getModel()->addStmtToContext(pStmt);
    }
    catch (std::exception const &e)
    {
      // The model couldn't perform adding a statement to the context.
      // Notify the invoker of the command, i.e., the controller,
      // that the requested action carried by the command cannot be performed
      notifyInvokerOnError(e.what(), MVCEvent::MVCEventActor::MVC_ACTOR_MODEL);
    }
  }//execute
  
}//end namespace MVC
