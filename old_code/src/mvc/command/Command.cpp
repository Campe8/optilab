// Base class
#include "Command.hpp"

#include "MVCController.hpp"

namespace MVC {
  
  Command::Command()
  : pEventFactory(new MVCEventFactory())
  {
  }
  
  void Command::notifyInvokerOnError(const char* aMsg, MVCEvent::MVCEventActor aSrc)
  {
    std::string msg;
    if(aMsg)
    {
      msg = std::string(aMsg);
    }
    getController()->notifyOnError(aMsg, aSrc);
  }//notifyInvokerOnError
  
}//end namespace MVC
