// Base class
#include "CommandFactory.hpp"

#include "CLIUtils.hpp"

#include <cassert>

namespace cutils = CLI::Utils;

namespace MVC {
  
  CommandFactory::CommandFactory()
  {
    registerCommands();
  }
  
  void CommandFactory::registerCommands()
  {
    CommandIdType cmdId = 0;
    for(; cmdId < static_cast<CommandIdType>(CommandId::CMD_UNDEF); ++cmdId)
    {
      switch(static_cast<CommandId>(cmdId))
      {
        case CommandId::CMD_EVAL_STMT:
          pCmdRegister[cmdId] = std::shared_ptr<Command>(new CommandEvalStmt());
          break;
        case CommandId::CMD_FCN_CALL:
          pCmdRegister[cmdId] = std::shared_ptr<Command>(new CommandFcnCall());
          break;
        default:
          assert(false);
          break;
      }
    }
  }//registerCommands
  
  std::shared_ptr<Command> CommandFactory::getCommand(CommandId aCmdId)
  {
    auto id = static_cast<CommandIdType>(aCmdId);
    if(pCmdRegister.find(id) == pCmdRegister.end())
    {
      return std::shared_ptr<Command>(nullptr);
    }
    
    // Get the command instance from its ID and
    // and setup its view and model
    auto cmd = pCmdRegister[id];
    if(!cmd->hasArguments())
    {
      cmd->setArguments(pCommandArgSet);
    }
    
    return cmd;
  }//getCommand
  
  std::shared_ptr<Command> CommandFactory::command(CommandId aCmdId, const CLI::CLIStmt& aStmt)
  {
    switch(aCmdId)
    {
      case CommandId::CMD_EVAL_STMT:
        return commandEvalStmt(aStmt);
      default:
        assert(aCmdId == CommandId::CMD_FCN_CALL);
        return commandFcnCall(aStmt);
    }// switch
  }//command
  
  std::shared_ptr<CommandEvalStmt> CommandFactory::commandEvalStmt(const CLI::CLIStmt& aStmt)
  {
    auto cmd = getCommand(MVC::CommandId::CMD_EVAL_STMT);
    assert(cmd);
    
    std::shared_ptr<CommandEvalStmt> eStmt = std::static_pointer_cast<CommandEvalStmt>(cmd);
    eStmt->setStmt(aStmt);
    
    return eStmt;
  }//commandEvalStmt
  
  std::shared_ptr<CommandFcnCall> CommandFactory::commandFcnCall(const CLI::CLIStmt& aStmt)
  {
    assert(!aStmt.empty());
    
    auto fcnName = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_FCN_CALL_ID);
    assert(fcnName);
    assert(!(*fcnName).empty());
    
    auto nargs = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_FCN_CALL_NUM_ARGS);
    auto args = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_FCN_CALL_ARGS);
    assert(nargs);
    assert(!(*nargs).empty());
    
    std::vector<std::string> argVec;
    auto n = cutils::stoi(*nargs);
    if(n > 0)
    {
      assert(args);
      argVec = CLI::Utils::splitArguments(*args);
    }
    assert(argVec.size() == static_cast<std::size_t>(n));

    auto cmd = getCommand(MVC::CommandId::CMD_FCN_CALL);
    assert(cmd);
    
    std::shared_ptr<CommandFcnCall> fcnCall = std::static_pointer_cast<CommandFcnCall>(cmd);
    fcnCall->setCallbackFcn(*fcnName, argVec);
    
    return fcnCall;
  }//commandEvalStmt
  
}//end namespace MVC
