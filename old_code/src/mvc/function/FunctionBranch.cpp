// Self first
#include "FunctionBranch.hpp"
#include "FunctionMacro.hpp"
#include "MVCUtils.hpp"
#include "MVCMacro.hpp"
#include "ObjectHelper.hpp"
#include "SearchOptionsTools.hpp"
#include "CLIMacroSearch.hpp"

namespace otools = Interpreter::ObjectTools;
namespace sotools = Interpreter::SearchOptionsTools;

namespace MVC {
  
  static bool isArrayOfIdentifiers(const Interpreter::DataObject& aDO)
  {
    if(Interpreter::isClassObjectType(aDO))
    {
      auto obj = aDO.getClassObject().get();
      if(otools::isVariableObject(obj)) return true;
      else if(otools::isListObject(obj))
      {
        for(const auto& listObj : otools::getListValues(obj))
        {
          if(!isArrayOfIdentifiers(listObj)) return false;
        }
        return true;
      }
    }
    return false;
  }//isArrayOfIdentifiers
  
  FunctionBranch::FunctionBranch()
  : Function(FunctionId::FCN_BRANCH)
  {
  }
  
  bool FunctionBranch::requiresFcnObjectAsArgument() const
  {
    // Post branch function requires the function data object
    // itself as an input parameter and as the object that
    // will be stored in the model and, in case, re-interpreted
    return true;
  }//requiresFcnObjectAsArgument
  
  void FunctionBranch::checkFunctionArguments(const Interpreter::DataObject& aDO)
  {
    // Branch function should have the following arguments:
    // aArgs[0][0]: list of variables to branch on
    // aArgs[0][1]:
    //    - variable choice heuristic (old)
    //    - BranchOptions object
    // aArgs[0][2]: value choice heuristic (old)
    // @note both aArgs[0][1] and aArgs[0][2] are optional arguments
    
    // Check first input to be a list of identifiers for variables to branch on
    assertAndNotify(aDO.isComposite() && aDO.composeSize() < 5, ERR_MSG_NUM_ARGS);
    assertAndNotify(isArrayOfIdentifiers(aDO[0]), ERR_MSG_INPUT_ARG);
    
    // Returns if there are no arguments besides the list of identifiers
    if(aDO.composeSize() == 1) return;
    
    bool consistentInput_1 = aDO[1].dataObjectType() ==
                             Interpreter::DataObject::DataObjectType::DOT_STRING;
    if(isClassObjectType(aDO[1]))
    {
      // If it is a class object, check that it is a SearchOptions object
      consistentInput_1 = consistentInput_1 ||
      sotools::isSearchOptionsObject(aDO[1].getClassObject().get());
    }
    assertAndNotify(consistentInput_1, ERR_MSG_INPUT_ARG_TYPE);
    
    // Returns if there is only one argument besides the list of variables
    if(aDO.composeSize() == 2) return;
    
    bool consistentInput_2 = aDO[2].dataObjectType() ==
    Interpreter::DataObject::DataObjectType::DOT_STRING;
    assertAndNotify(consistentInput_2, ERR_MSG_INPUT_ARG_TYPE);
  }//checkFunctionArguments
  
  Interpreter::DataObject FunctionBranch::callback(const std::vector<Interpreter::DataObject>& aArgs)
  {
    // The arguments should contain only the function object
    assert(aArgs.size() == 1 && aArgs[0].isFcnObject());
    
    // Check arguments consistency
    checkFunctionArguments(aArgs[0]);
    try
    {
      getModel()->addObjectToContext(aArgs[0]);
    }
    catch(MVCException& mvce)
    {
      assertAndNotify(false, mvce.what());
    }
    catch (...)
    {
      assertAndNotify(false, "Post branch failed");
    }
    
    return Interpreter::DataObject();
  }//callback
  
  std::string FunctionBranch::name() const
  {
    return "branch";
  }//name
  
}//end namespace MVC
