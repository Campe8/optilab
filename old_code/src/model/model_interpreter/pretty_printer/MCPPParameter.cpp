// Self first
#include "MCPPParameter.hpp"

#include "ModelASTMacro.hpp"
#include "CLIUtils.hpp"
#include "ObjectHelper.hpp"
#include "ObjectTools.hpp"

#include <string>
#include <utility>
#include <cassert>

namespace cutils = CLI::Utils;
namespace otools = Interpreter::ObjectTools;

namespace Model {

	void MCPPParameter::prettyPrintDimensions(Interpreter::BaseObject* aPar, std::ostream& aOut)
	{
    outTag("dims", aOut);
    std::string dims{"["};
    for(auto dim : otools::getParameterDimensions(aPar))
    {
      dims += CLI::Utils::toString<int>(dim) + ", ";
    }
    dims.resize(dims.size() - 2);
    dims += "]";
    aOut << dims;
	}//prettyPrintNCols
  
	void MCPPParameter::prettyPrintValues(Interpreter::BaseObject* aPar, std::ostream& aOut)
	{
		outTag(MDL_AST_LEAF_MAT_VALS, aOut);
    aOut << objToString(otools::getParameterValues(aPar));
	}//prettyPrintValues

	void MCPPParameter::prettyPrint(const std::shared_ptr<ContextObject>& aObj, std::ostream& aOut,
                                  bool aFullPrettyPrint)
	{
    assert(aObj && (aObj->payload()).isClassObject());
    printIDAndType(aObj, aOut, aFullPrettyPrint);

    auto par = (aObj->payload()).getClassObject().get();
    assert(otools::isParameterObject(par));

    // Pretty print ID
    prettyPrintObjectID(par, aOut);
    aOut << '\n';
    
		if (aFullPrettyPrint)
		{
			prettyPrintDimensions(par, aOut);
			aOut << '\n';
      
      prettyPrintFullyInterpreted(par, aOut);
      aOut << '\n';
		}
		prettyPrintValues(par, aOut);
	}//prettyPrint

}// end namespace Model

