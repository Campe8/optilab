// Self first
#include "MCPPBranch.hpp"

#include "ModelASTMacro.hpp"
#include "BranchContextObject.hpp"

#include <string>
#include <unordered_set>
#include <utility>
#include <cassert>

namespace Model {

  void MCPPBranch::prettyPrintKey(const std::string aID, std::ostream& aOut)
  {
    outTag(MDL_AST_LEAF_CON_ID, aOut);
    aOut << aID;
  }//prettyPrintKey
  
  void MCPPBranch::prettyPrintBranchingSet(const Interpreter::DataObject& aObj, std::ostream& aOut)
  {
    assert(aObj.isComposite());
    
    // Get branching set
    outTag(MDL_AST_LEAF_BRC_BRANCHING_SET, aOut);
    aOut << '[' << objToString(aObj[0]) << ']';
  }//prettyPrintBranchingSet
  
  void MCPPBranch::prettyPrintHeuristic(const ContextObject* aObj, std::ostream& aOut)
  {
    auto varChoice = static_cast<const BranchContextObject*>(aObj)->getVariableChoice();
    auto valChoice = static_cast<const BranchContextObject*>(aObj)->getValueChoice();
    
    outTag(MDL_AST_LEAF_BRC_VAR_CHOICE, aOut);
    switch (varChoice)
    {
      case BranchContextObject::VariableChoice::VARC_FIRST_FAIL:
        aOut << MDL_AST_LEAF_BRC_VAR_FIRST_FAIL;
        break;
      case BranchContextObject::VariableChoice::VARC_ANTI_FIRST_FAIL:
        aOut << MDL_AST_LEAF_BRC_VAR_ANTI_FIRST_FAIL;
        break;
      case BranchContextObject::VariableChoice::VARC_SMALLEST:
        aOut << MDL_AST_LEAF_BRC_VAR_SMALLEST;
        break;
      case BranchContextObject::VariableChoice::VARC_LARGEST:
        aOut << MDL_AST_LEAF_BRC_VAR_LARGEST;
        break;
      default:
        assert(varChoice == BranchContextObject::VariableChoice::VARC_INPUT_ORDER);
        aOut << MDL_AST_LEAF_BRC_VAR_INPUT_ORDER;
        break;
    }
    
    aOut << '\n';
    outTag(MDL_AST_LEAF_BRC_VAL_CHOICE, aOut);
    switch (valChoice)
    {
      case BranchContextObject::ValueChoice::VALC_INDOMAIN_MAX:
        aOut << MDL_AST_LEAF_BRC_VAL_INDOMAIN_MAX;
        break;
      case BranchContextObject::ValueChoice::VALC_INDOMAIN_MIN:
        aOut << MDL_AST_LEAF_BRC_VAL_INDOMAIN_MIN;
        break;
      default:
        assert(valChoice == BranchContextObject::ValueChoice::VALC_INDOMAIN_RANDOM);
        aOut << MDL_AST_LEAF_BRC_VAL_INDOMAIN_RANDOM;
        break;
    }
  }//prettyPrintHeuristic
  
	void MCPPBranch::prettyPrint(const std::shared_ptr<ContextObject>& aObj, std::ostream& aOut, bool aFullPrettyPrint)
	{
    assert(aObj);
    printIDAndType(aObj, aOut, aFullPrettyPrint);
    
    const auto& payload = aObj->payload();
    assert(payload.isFcnObject());
    
    // Print branching set
    prettyPrintBranchingSet(payload, aOut);
    aOut << '\n';
    
    if (aFullPrettyPrint)
    {
      auto branchSpec = static_cast<BranchContextObject*>(aObj.get());
      // Print heuristics if any
      prettyPrintHeuristic(branchSpec, aOut);
      aOut << '\n';
      prettyPrintFullyInterpreted(payload.isFullyInterpreted(), aOut);
      aOut << '\n';
    }
	}//prettyPrint

}// end namespace Model

