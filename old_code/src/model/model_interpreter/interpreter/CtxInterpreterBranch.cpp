// Self first
#include "CtxInterpreterBranch.hpp"

// Macros
#include "ModelASTMacro.hpp"
#include "ModelMacro.hpp"

#include "ViewEntryArray.hpp"

// Utilities
#include "InterpreterUtils.hpp"
#include "CLIUtils.hpp"

#include <string>
#include <utility>
#include <memory>
#include <cassert>

namespace iutils = Model::Utils::Interpreter;
namespace cutils = CLI::Utils;

namespace Model {

	CtxInterpreterBranch::CtxInterpreterBranch()
  : Model::CtxInterpreter(ModelInterpreter::SymbolType::ST_BRANCH_DECL)
  {
    registerInterpreters();
  }
  
  void CtxInterpreterBranch::registerInterpreters()
  {
    registerInterpreterFcn("interpret_branching_set",
                           std::bind(&CtxInterpreterBranch::interpretBranchingSet, this, std::placeholders::_1, std::placeholders::_2));
  }//registerInterpreters

  std::pair<bool, bool> CtxInterpreterBranch::interpretBranchingSet(const ModelContext& aMdlCtx, ModelContext& aCtx)
  {
		// Get the branching set
		auto branchingSet = aCtx.context().getValueList<std::string>({ MDL_AST_LEAF_BRC_BRANCHING_SET });

		// Interpret each scope element
		bool fullyInterpreted{ true };

		// Optimization: flag indicating that at least one
		// element of the set changed during interpretation
		bool changed{ false };
    std::vector<std::string> interpretedSet;
    std::unordered_set<std::string> varSet;
		for (auto& vexpr : branchingSet)
		{
			// "Cleanup" expression
			cutils::trim(vexpr);

			// If the branching expression is an alias for a variable,
			// there is no need to further interpret it
			if (aMdlCtx.view().findAlias(vexpr))
			{
        if(varSet.find(vexpr) != varSet.end()) continue;
        varSet.insert(vexpr);
        
        interpretedSet.push_back(vexpr);
				continue;
      }
			
			// Interpret the expression
			if (auto isexpr = iutils::interpretExpr(vexpr, aMdlCtx.view()))
			{
				// At least one value is interpreted
				changed = true;
				vexpr = *isexpr;
        
        auto eType = iutils::getExprType(vexpr);
        model_assert_msg(eType == iutils::ExprType::AT_ID, "Invalid branching element: " + vexpr);
        
        if (aMdlCtx.view().findAlias(vexpr))
        {
          if(varSet.find(vexpr) != varSet.end()) continue;
          varSet.insert(vexpr);
          
          interpretedSet.push_back(vexpr);
        }
        else if(eType == iutils::ExprType::AT_ID && aMdlCtx.view().lookup(vexpr))
        {
          // Check if the ID corresponds to a matrix of aliases which is fully interpreted.
          // If so, use the aliases as branching set
          auto ventry = aMdlCtx.view().lookup(vexpr);
          if(ViewEntryArray::isa(ventry) && ventry->isFullyInterpreted())
          {
            for(auto& elem : ViewEntryArray::cast(ventry)->getArray())
            {
              model_assert_msg(iutils::getExprType(elem) == iutils::ExprType::AT_ID, "Invalid branching element: " + elem);
              if(varSet.find(elem) != varSet.end()) continue;
              varSet.insert(elem);
              
              interpretedSet.push_back(elem);
            }
          }
        }
        else
        {
          // Argument cannot be interpreted
          interpretedSet.push_back(vexpr);
          fullyInterpreted = false;
        }
			}
			else
			{
				// Set interpretation flag as "not fully interpreted"
				fullyInterpreted = false;
        interpretedSet.push_back(vexpr);
			}
		}//for

		if (!changed)
		{
			return { changed, fullyInterpreted };
		}

		// At least one value changed during interpretation
		ModelContext::Path p = { MDL_AST_LEAF_BRC_BRANCHING_SET };
		aCtx.context().deleteSubContext(p);
		for (auto& vexpr : interpretedSet)
		{
			aCtx.context().addValueList<std::string>(p, vexpr);
		}

		return { changed, fullyInterpreted };
  }//interpretValues
  
  void CtxInterpreterBranch::doInterpretation()
  {
    runInterpreterFcn("interpret_branching_set");
  }//doInterpretation
  
}// end namespace Model
