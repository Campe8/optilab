// Self first
#include "CtxInterpreter.hpp"

#include "MVCModelContext.hpp"
#include "ModelMacro.hpp"

#include <cassert>

namespace cutils = CLI::Utils;

namespace Model {
  
  CtxInterpreter::CtxInterpreter(SymbolType aSymbType)
  : Model::ModelInterpreter(aSymbType)
  , pTouchedCtx(false)
  {
  }
  
  void CtxInterpreter::registerInterpreterFcn(const std::string& aFcnName, const InterpreterFcn& aIFcn)
  {
    assert(!aFcnName.empty());
    pIFcnNameMap[aFcnName] = pIFcnRegister.size();
    pIFcnRegister.push_back(aIFcn);
  }//registerInterpreterFcn
  
  boost::optional<ModelContext> CtxInterpreter::interpretCtx(const ModelContext& aMdlCtx, const Context& aCtx)
  {
    // Copy of the given context to be interpreted.
    // @note the scope of the context is within this method.
    // The context is then copied back on the caller side
    ModelContext mctx = ModelContext(aCtx);
    setInterpContex(&mctx);
    
    // Set current model context
    setModelContex(&aMdlCtx);

    // Set touched flag to false before interpretation
    pTouchedCtx = false;
    
    // Run interpreters
    runInterpreter();
    
    if(pTouchedCtx)
    {
      return mctx;
    }
    
    return boost::none;
  }//interpretStmt
  
  void CtxInterpreter::runInterpreterFcn()
  {
    model_assert(getModelContext());
    model_assert(getInterpContext());
    for(auto& fcn : pIFcnRegister)
    {
      auto istate = fcn(*getModelContext(), *getInterpContext());
      andFullyInterpretedState(istate.second);
      pTouchedCtx = pTouchedCtx || istate.first;
    }
  }//runInterpreterFcn
  
  void CtxInterpreter::runInterpreterFcn(const std::string& aFcnName)
  {
    model_assert(pIFcnNameMap.find(aFcnName) != pIFcnNameMap.end());
    auto istate = pIFcnRegister[pIFcnNameMap[aFcnName]](*getModelContext(), *getInterpContext());
    andFullyInterpretedState(istate.second);
    pTouchedCtx = pTouchedCtx || istate.first;
  }//runInterpreterFcn
  
  void CtxInterpreter::doInterpretation()
  {
    runInterpreterFcn();
  }//doInterpretation

}// end namespace Model
