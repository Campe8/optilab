// Self first
#include "CtxInterpreterSearch.hpp"

// Macros
#include "ModelASTMacro.hpp"
#include "ModelMacro.hpp"

// Utilities
#include "InterpreterUtils.hpp"
#include "CLIUtils.hpp"

#include <string>
#include <utility>
#include <memory>
#include <cassert>

namespace iutils = Model::Utils::Interpreter;
namespace cutils = CLI::Utils;

namespace Model {

	CtxInterpreterSearch::CtxInterpreterSearch()
  : Model::CtxInterpreter(ModelInterpreter::SymbolType::ST_SEARCH_DECL)
  {
    registerInterpreters();
  }
  
  void CtxInterpreterSearch::registerInterpreters()
  {
    registerInterpreterFcn("interpret_branching_set",
                           std::bind(&CtxInterpreterSearch::interpretBranchingSet, this, std::placeholders::_1, std::placeholders::_2));
    registerInterpreterFcn("interpret_solution_number",
                           std::bind(&CtxInterpreterSearch::interpretSolutionNumber, this, std::placeholders::_1, std::placeholders::_2));
  }//registerInterpreters

  std::pair<bool, bool> CtxInterpreterSearch::interpretSolutionNumber(const ModelContext& aMdlCtx, ModelContext& aCtx)
  {
    // Get number of solutions
    ModelContext::Path p = { MDL_AST_LEAF_SEARCH_BRANCHING_SET };
    auto nsol = aCtx.context().getValue<std::string>(p);
    
    if(cutils::isInteger(nsol))
    {
      return { false, true };
    }
    
    // Interpret the expression
    if (auto isexpr = iutils::interpretExpr(nsol, aMdlCtx.view()))
    {
      // Interpreted value
      model_assert(cutils::isInteger(*isexpr));
      aCtx.context().deleteSubContext(p);
      aCtx.context().addValue<std::size_t>(p, static_cast<std::size_t>(cutils::stoi(*isexpr)));
      
      return { true, true };
    }
    
    return { false, false };
  }//interpretSolutionNumber
  
  std::pair<bool, bool> CtxInterpreterSearch::interpretBranchingSet(const ModelContext& aMdlCtx, ModelContext& aCtx)
  {
		// Get the branching set
		auto branchingSet = aCtx.context().getValueList<std::string>({ MDL_AST_LEAF_SEARCH_BRANCHING_SET });

		// Interpret each scope element
		bool fullyInterpreted{ true };

		// Optimization: flag indicating that at least one
		// element of the set changed during interpretation
		bool changed{ false };
		for (auto& vexpr : branchingSet)
		{
			// "Cleanup" expression
			cutils::trim(vexpr);

			// If the branching expression is an alias for a variable,
			// there is no need to further interpret it
			if (aMdlCtx.view().findAlias(vexpr))
			{
				continue;
			}
			
			// Interpret the expression
			if (auto isexpr = iutils::interpretExpr(vexpr, aMdlCtx.view()))
			{
				// At least one value is interpreted
				changed = true;
				vexpr = *isexpr;
			}
			else
			{
				// Set interpretation flag as "not fully interpreted"
				fullyInterpreted = false;
			}
		}//for

		if (!changed)
		{
			model_assert(!fullyInterpreted);
			return { changed, fullyInterpreted };
		}

		// At least one value changed during interpretation
		ModelContext::Path p = { MDL_AST_LEAF_SEARCH_BRANCHING_SET };
		aCtx.context().deleteSubContext(p);
		for (auto& vexpr : branchingSet)
		{
			aCtx.context().addValueList<std::string>(p, vexpr);
		}

		return { changed, fullyInterpreted };
  }//interpretValues
  
  void CtxInterpreterSearch::doInterpretation()
  {
    runInterpreterFcn("interpret_branching_set");
    runInterpreterFcn("interpret_solution_number");
  }//doInterpretation
  
}// end namespace Model
