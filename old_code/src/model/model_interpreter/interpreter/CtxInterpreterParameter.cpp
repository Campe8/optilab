// Base class
#include "CtxInterpreterParameter.hpp"

// Macros
#include "ModelASTMacro.hpp"
#include "ModelMacro.hpp"

#include "StmtInterpreterParameter.hpp"

// View
#include "ViewEntryArray.hpp"

// Utilities
#include "InterpreterUtils.hpp"
#include "CLIUtils.hpp"

#include <string>
#include <utility>
#include <unordered_set>
#include <memory>
#include <cassert>

namespace iutils = Model::Utils::Interpreter;
namespace cutils = CLI::Utils;

namespace Model {
  
  bool interpretExpr(std::string& aExpr, std::unordered_map<std::string, std::unordered_set<std::size_t>>& aCircDepMap, const ContextView& aView);
  
  CtxInterpreterParameter::CtxInterpreterParameter()
  : Model::CtxInterpreter(ModelInterpreter::SymbolType::ST_MATRIX_DECL)
  {
    registerInterpreters();
  }
  
  void CtxInterpreterParameter::registerInterpreters()
  {
    registerInterpreterFcn("interpret_values",
                           std::bind(&CtxInterpreterParameter::interpretValues, this, std::placeholders::_1, std::placeholders::_2));
  }//registerInterpreters
  
  // Interprets the value "aMatID[aIdx] = aExpr"
  static bool interpretValueArray(const std::string& aMatID, std::size_t aIdx, std::string& aExpr, const ContextView& aView)
  {
    // Create the set of pairs <id, [idx]> needed
    // to interpret matrices and subscriptions in order
    // to determine circular dependencies
    iutils::ReferenceMap refMap;
    refMap[aMatID].insert(aIdx);
    
    if (auto iExpr = iutils::interpretExpr(aExpr, aView, &refMap))
    {
      aExpr = *iExpr;
      return true;
    }
    
    return false;
  }//interpretValueArray
  
  std::pair<bool, bool> CtxInterpreterParameter::interpretValues(const ModelContext& aMdlCtx, ModelContext& aCtx)
  {
    // Get the vector of the rows
    auto rows = aCtx.context().getValueList<std::string>({ MDL_AST_LEAF_MAT_VALS });
    
    // Interpret each element of each row
    bool fullyInterpreted{ true };
    std::vector<std::string> vectorizedMatrix;
    
    // Get the matrix identifier
    auto matID = aCtx.context().getValue<std::string>({ MDL_AST_LEAF_MAT_ID });
    
    // Get the correspondent view entry if any
    auto matEntry = aCtx.view().lookup(matID);
    
    // Optimization: flag indicating that at least one
    // element of the matrix changed during interpretation
    bool changed{ false };
    std::size_t elemIdx{ 0 };
    for (auto& row : rows)
    {
      // Interpreted row
      std::string interpRow;
      auto rowElems = cutils::tokenizeStringOnSymbol(row, ',');
      for (auto& val : rowElems)
      {
        // "Cleanup" values
        cutils::trim(val);
        
        // Interpret a single element.
        // We consider four cases:
        // 1 - val does not need interpretation;
        //     -> Continue but at least one value needs interpretation
        //        otherwise this code would need to run.
        // 2 - val was interpreted successfully;
        //     -> This is an "okay" case, save the value to later
        //        replace it in the parameter.
        // 3 - val wasn't interpreted successfully;
        //     -> Set context flag as not fully interpreted.
        // 4 - val needs interpretation but it is already interpreted
        //     in the view since some other context may have changed the view.
        //     -> Change val to that interpreted value
        if (matEntry)
        {
          // Case (4)
          model_assert(ViewEntryArray::isa(matEntry));
          if (matEntry->isFullyInterpreted() || !(ViewEntryArray::cast(matEntry)->needsInterpretation(elemIdx)))
          {
            
            // The matrix or the value are already fully interpreted
            auto ival = (ViewEntryArray::cast(matEntry)->getArray())[elemIdx];
            if(ival != val)
            {
              // At least one value is interpreted
              changed = true;
            }
            
            // Save the interpreted value
            interpRow += ival + ", ";
            
            // Continue with next element
            elemIdx++;
            continue;
          }
        }
        
        // Case (1), (2), and (3)
        if (!interpretValueArray(matID, elemIdx, val, aCtx.view()))
        {
          // Case (3)
          // set interpretation flag as "not fully interpreted"
          fullyInterpreted = false;
        }
        else
        {
          // At least one value is interpreted
          changed = true;
          
          // Change it in the view if not already updated
          if(ViewEntryArray::cast(matEntry)->needsInterpretation(elemIdx))
          {
            ViewEntryArray::cast(matEntry)->setInterpretedValue(elemIdx, val);
          }
        }
        
        // Save the interpreted value
        interpRow += val + ", ";
        
        // Continue with next element
        elemIdx++;
      }
      
      if (!rowElems.empty())
      {
        interpRow.pop_back();
        interpRow.pop_back();
      }
      // Replace the row with its interpreted version
      row = interpRow;
    }
    
    if (!changed)
    {
      model_assert(!fullyInterpreted);
      return {changed, fullyInterpreted};
    }
    
    // At least one value changed during interpretation.
    // Replace values: delete old sub-context with values
    aCtx.context().deleteSubContext({ MDL_AST_LEAF_MAT_VALS });
    
    // Set interpreted context
    auto nrows = getNumberOfParamRows(aCtx);
    if (nrows == 1)
    {
      aCtx.context().addValueList<std::string>({ MDL_AST_LEAF_MAT_VALS }, rows[0]);
    }
    else
    {
      aCtx.context().addValueList<std::string>({ MDL_AST_LEAF_MAT_VALS }, rows);
    }
    
    return {changed, fullyInterpreted};
  }//interpretValues
  
  void CtxInterpreterParameter::doInterpretation()
  {
    runInterpreterFcn("interpret_values");
  }//doInterpretation
  
}// end namespace Model
