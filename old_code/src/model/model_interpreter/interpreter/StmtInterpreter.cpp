// Base class
#include "StmtInterpreter.hpp"

#include "MVCModelContext.hpp"
#include "ModelMacro.hpp"

namespace cutils = CLI::Utils;

namespace Model {
  
  static std::string getObjTypeFromStmt(const CLI::CLIStmt& aStmt)
  {
    auto msgType = CLI::Utils::getMsgType(aStmt);
    switch (msgType)
    {
      case CLI::CLI_MSG_VAR_DECL:
        return MDL_AST_OBJ_TYPE_VAR;
      case CLI::CLI_MSG_CON_DECL:
        return MDL_AST_OBJ_TYPE_CON;
      case CLI::CLI_MSG_MATRIX_DECL:
        return MDL_AST_OBJ_TYPE_MAT;
      case CLI::CLI_MSG_BRC_DECL:
        return MDL_AST_OBJ_TYPE_BRC;
      default:
        assert(msgType == CLI::CLI_MSG_SRC_DECL);
        return MDL_AST_OBJ_TYPE_SRC;
    }
  }//getObjTypeFromStmt
  
  StmtInterpreter::StmtInterpreter(SymbolType aSymbType)
  : Model::ModelInterpreter(aSymbType)
  , pStmt(nullptr)
  {
  }
  
  void StmtInterpreter::registerInterpreterFcn(const std::string& aFcnName, const InterpreterFcn& aIFcn)
  {
    model_assert(!aFcnName.empty());
    pIFcnNameMap[aFcnName] = pIFcnRegister.size();
    pIFcnRegister.push_back(aIFcn);
  }//registerInterpreterFcn
  
  void StmtInterpreter::finalizeInterpreter()
  {
    model_assert(pStmt);
    model_assert(getInterpContext());
    
    // Add auxiliary information to the context
    auto uid = cutils::getCLIStmtVal(*pStmt, CLI_MSG_TAG_UID);
    if (uid)
    {
      std::stringstream ss;
      ss << *uid;
      std::size_t key{ 0 };
      ss >> key;
      (*getInterpContext()).context().addValue<MVC::MVCModelContext::MdlCtxObjKey>({ MDL_AST_OBJ_KEY }, key);
    }
    
    // Add unique type of the object based on the stmt declaration
    auto objType = getObjTypeFromStmt(*pStmt);
    (*getInterpContext()).context().addValue<std::string>({ MDL_AST_OBJ_TYPE }, objType);
    
    // Call base class finalize method
    ModelInterpreter::finalizeInterpreter();
  }//finalizeInterpreter
  
  ModelContext StmtInterpreter::interpretStmt(const ModelContext& aModelContext, const CLI::CLIStmt& aStmt)
  {
    // Empty context to be interpreted.
    // @note the scope of the model context is within this method.
    // The model context is then copied back on the caller side
    ModelContext mctx = createLocalModelContext();
    setInterpContex(&mctx);
    
    // Set current model context to be interpreted
    setModelContex(&aModelContext);
    
    // Set current statement to be interpreted
    pStmt = &aStmt;
    
    // Run interpreters
    runInterpreter();
    
    return mctx;
  }//interpretStmt
  
  void StmtInterpreter::runInterpreterFcn()
  {
    model_assert(pStmt);
    model_assert(getModelContext());
    model_assert(getInterpContext());
    for(auto& fcn : pIFcnRegister)
    {
      auto istate = fcn(*getModelContext(), *pStmt, *getInterpContext());
      andFullyInterpretedState(istate);
    }
  }//runInterpreterFcn
  
  void StmtInterpreter::runInterpreterFcn(const std::string& aFcnName)
  {
    model_assert(pIFcnNameMap.find(aFcnName) != pIFcnNameMap.end());
    auto istate = pIFcnRegister[pIFcnNameMap[aFcnName]](*getModelContext(), *pStmt, *getInterpContext());
    andFullyInterpretedState(istate);
  }//runInterpreterFcn
  
  void StmtInterpreter::doInterpretation()
  {
    runInterpreterFcn();
  }//doInterpretation
  
}// end namespace Model
