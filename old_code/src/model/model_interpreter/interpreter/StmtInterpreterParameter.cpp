// Base class
#include "StmtInterpreterParameter.hpp"

#include "ModelASTMacro.hpp"
#include "ModelMacro.hpp"

#include "InterpreterUtils.hpp"
#include "CLIUtils.hpp"

#include "ViewEntryArray.hpp"

#include <string>
#include <utility>
#include <memory>
#include <cassert>

namespace iutils = Model::Utils::Interpreter;
namespace cutils = CLI::Utils;

namespace Model {
  
  StmtInterpreterParameter::StmtInterpreterParameter()
  : Model::StmtInterpreter(ModelInterpreter::SymbolType::ST_MATRIX_DECL)
  {
    registerInterpreters();
  }
  
  void StmtInterpreterParameter::registerInterpreters()
  {
    registerInterpreterFcn("interpret_ID",
                           std::bind(&StmtInterpreterParameter::interpretID, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    registerInterpreterFcn("interpret_type",
                           std::bind(&StmtInterpreterParameter::interpretType, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    registerInterpreterFcn("interpret_num_rows",
                           std::bind(&StmtInterpreterParameter::interpretNumRows, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    registerInterpreterFcn("interpret_num_cols",
                           std::bind(&StmtInterpreterParameter::interpretNumCols, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    registerInterpreterFcn("interpret_values",
                           std::bind(&StmtInterpreterParameter::interpretValues, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
  }//registerInterpreters
  
  bool StmtInterpreterParameter::interpretID(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx)
  {
    auto id = iutils::getIDFromStmt(aStmt);
    if (!id || (*id).empty())
    {
      return false;
    }
    
    // Set variable id
    aIMCtx.context().addValue<std::string>({ MDL_AST_LEAF_MAT_ID }, *id);
    
    return true;
  }//interpretMatId
  
  bool StmtInterpreterParameter::interpretType(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx)
  {
    auto tp = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_MAT_TYPE);
    if (!tp || (*tp).empty())
    {
      return false;
    }
    
    // Set variable type
    std::string matType;
    if (*tp == CLI_MSG_VAL_MAT_TYPE_BOOL)
    {
      matType = CLI_MSG_VAL_MAT_TYPE_BOOL;
    }
    else
    {
      assert(*tp == CLI_MSG_VAL_MAT_TYPE_INT);
      matType = CLI_MSG_VAL_MAT_TYPE_INT;
    }
    
    aIMCtx.context().addValue<std::string>({ MDL_AST_LEAF_MAT_TYPE }, matType);
    return true;
  }//interpretType
  
  bool StmtInterpreterParameter::interpretNumRows(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx)
  {
    auto nr = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_MAT_NUM_ROWS);
    if (!nr || (*nr).empty())
    {
      return false;
    }
    
    // Set number of rows
    aIMCtx.context().addValue<std::string>({ MDL_AST_LEAF_MAT_NROWS }, *nr);
    
    return true;
  }//interpretNumRows
  
  bool StmtInterpreterParameter::interpretNumCols(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx)
  {
    auto nc = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_MAT_NUM_COLS);
    if (!nc || (*nc).empty())
    {
      return false;
    }
    
    // Set number of columns
    aIMCtx.context().addValue<std::string>({ MDL_AST_LEAF_MAT_NCOLS }, *nc);
    
    return true;
  }//interpretNumCols
  
  bool StmtInterpreterParameter::interpretValues(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx)
  {
    auto vals = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_MAT_ROWS);
    model_assert(vals && !(*vals).empty());
    
    // Get the vector of matrix rows
    auto tokenSymb = CLI_MSG_TAG_MAT_ROWS_SEP;
    auto matrixRows = CLI::Utils::tokenizeStringOnSymbol(*vals, tokenSymb[0]);
    
    // Get number of rows
    auto nrows = getNumberOfParamRows(aIMCtx);
    model_assert(nrows > 0);
    model_assert(matrixRows.size() == getNumberOfParamRows(aIMCtx));
    
    // For each value, check if it needs to be interpreted,
    // if so, try to interpret it
    bool fullyInterpreted{ true };
    auto matID = aIMCtx.context().getValue<std::string>({ MDL_AST_LEAF_MAT_ID });
    std::vector<ViewEntry::EntryValue> vectorizedMatrix;
    for (auto& row : matrixRows)
    {
      // Interpreted row
      std::string interpRow;
      auto rowElems = cutils::tokenizeStringOnSymbol(row, ',');
      for (auto& val : rowElems)
      {
        // Try to interpret as much as possible
        cutils::trim(val);
        if (auto valInterp = iutils::interpretExpr(val, aIMCtx.view()))
        {
          val = *valInterp;
          if(iutils::getExprType(val) == Utils::Interpreter::ExprType::AT_ID && !aIMCtx.view().findAlias(val))
          {
            model_assert_msg(matID != val, "Circular dependency");
            fullyInterpreted = false;
            vectorizedMatrix.push_back({ val, false });
          }
          else
          {
            vectorizedMatrix.push_back({ val, true });
          }
        }
        else
        {
          // Value couldn't been interpreted,
          // Set flag as needs further interpretation
          fullyInterpreted = false;
          vectorizedMatrix.push_back({ val, false });
        }
        interpRow += val + ", ";
      }
      if (!rowElems.empty())
      {
        interpRow.pop_back();
        interpRow.pop_back();
      }
      // Replace the row with its interpreted version
      row = interpRow;
    }
    
    // Add interpreted array as entry to the view
    auto paramID = aIMCtx.context().getValue<std::string>({ MDL_AST_LEAF_MAT_ID });
    aIMCtx.view().addEntry(std::make_shared<ViewEntryArray>(paramID, vectorizedMatrix));
    
    // Set matrix values
    if (nrows == 1)
    {
      aIMCtx.context().addValueList<std::string>({ MDL_AST_LEAF_MAT_VALS }, matrixRows[0]);
    }
    else
    {
      aIMCtx.context().addValueList<std::string>({ MDL_AST_LEAF_MAT_VALS }, matrixRows);
    }
    
    return fullyInterpreted;
  }//interpretValues
  
  void StmtInterpreterParameter::doInterpretation()
  {
    runInterpreterFcn("interpret_ID");
    runInterpreterFcn("interpret_type");
    runInterpreterFcn("interpret_num_rows");
    runInterpreterFcn("interpret_num_cols");
    runInterpreterFcn("interpret_values");
  }//doInterpretation
  
  std::size_t getNumberOfParamRows(const ModelContext& aCtx)
  {
    return getNumberOfParamRows(aCtx.context());
  }//getNumberOfParamRows
  
  std::size_t getNumberOfParamRows(const Context& aCtx)
  {
    auto nrows = aCtx.getValue<std::string>({ MDL_AST_LEAF_MAT_NROWS });
    model_assert(cutils::isInteger(nrows));
    
    return static_cast<std::size_t>(cutils::stoi(nrows));
  }//getNumberOfParamRows
  
  std::size_t getNumberOfParamCols(const ModelContext& aCtx)
  {
    return getNumberOfParamCols(aCtx.context());
  }//getNumberOfParamCols
  
  std::size_t getNumberOfParamCols(const Context& aCtx)
  {
    auto ncols = aCtx.getValue<std::string>({ MDL_AST_LEAF_MAT_NCOLS });
    model_assert(cutils::isInteger(ncols));
    
    return static_cast<std::size_t>(cutils::stoi(ncols));
  }//getNumberOfParamCols
  
  std::vector<std::vector<std::string>> getParameterRows(const ModelContext& aCtx)
  {
    return getParameterRows(aCtx.context());
  }//getParameterRows
  
  std::vector<std::vector<std::string>> getParameterRows(const Context& aCtx)
  {
    std::vector<std::vector<std::string>> rows;
    auto numRows = getNumberOfParamRows(aCtx);
    
    if (numRows == 1)
    {
      auto vals = aCtx.getValueList<std::string>({ MDL_AST_LEAF_MAT_VALS });
      rows.push_back(vals);
    }
    else
    {
      rows = aCtx.getValueMatrix<std::string>({ MDL_AST_LEAF_MAT_VALS });
    }
    return  rows;
  }//getParameterRows
  
}// end namespace Model

