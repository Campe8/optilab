// Base class
#include "StmtInterpreterConstraint.hpp"

#include "ModelASTMacro.hpp"

#include "InterpreterUtils.hpp"
#include "CLIUtils.hpp"

#include "ModelMacro.hpp"

#include <string>
#include <utility>

#include <cassert>

namespace cutils = CLI::Utils;
namespace iutils = Model::Utils::Interpreter;

namespace Model {
  
  // Recursively check "aExpr" if it is fully interpreted w.r.t. the given view.
  // For example:
  // 10, a, [a, b, 10, [3, 4, a]] with view = (a, b) is fully interpretd
  // 10, b, with view = (a) is not fully interpreted
  static bool isExprFullyInterpretedOnView(const std::string& aExpr, const ContextView& aView)
  {
    auto etype = iutils::getExprType(aExpr);
    if(etype == Utils::Interpreter::ExprType::AT_INT)
    {
      return true;
    }
    else if(etype == Utils::Interpreter::ExprType::AT_ID && aView.findAlias(aExpr))
    {
      return true;
    }
    else if (etype == Utils::Interpreter::ExprType::AT_ARRAY)
    {
      // Interpreted expression is an array check if each element is either an alias
      // or a number, if not this expression is not fully interpreted
      for(const auto& e : iutils::getArrayElements(aExpr))
      {
        if(!isExprFullyInterpretedOnView(e, aView)) return false;
      }
      return true;
    }
    
    return false;
  }//isExprFullyInterpretedOnView
  
  StmtInterpreterConstraint::StmtInterpreterConstraint()
  : Model::StmtInterpreter(StmtInterpreter::SymbolType::ST_CONSTRAINT_DECL)
  {
    registerInterpreters();
  }
  
  void StmtInterpreterConstraint::registerInterpreters()
  {
    registerInterpreterFcn("interpret_key",
                           std::bind(&StmtInterpreterConstraint::interpretKey, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    registerInterpreterFcn("interpret_ID",
                           std::bind(&StmtInterpreterConstraint::interpretID, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    registerInterpreterFcn("interpret_prop_type",
                           std::bind(&StmtInterpreterConstraint::interpretPropType, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    registerInterpreterFcn("interpret_scope",
                           std::bind(&StmtInterpreterConstraint::interpretScope, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
  }//registerInterpreters
  
  bool StmtInterpreterConstraint::interpretKey(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx)
  {
    auto key = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_CON_KEY);
    if (key)
    {
      std::string keyval = MDL_AST_LEAF_CON_KEY_PREFIX;
      keyval += *key;
      
      // Set constraint key
      aIMCtx.context().addValue<std::string>({ MDL_AST_LEAF_CON_KEY }, keyval);
    }
    return true;
  }//interpretKey
  
  bool StmtInterpreterConstraint::interpretID(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx)
  {
    // Check that the constraint declaration type is a function call
    auto decl = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_CON_DECL_TYPE);
    model_assert(decl && !(*decl).empty() && ((*decl) == CLI_MSG_VAL_CON_DECL_TYPE_CALL));
    
    auto id = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_CON_NAME);
    model_assert(id && !(*id).empty());
    
    // Set constraint id
    aIMCtx.context().addValue<std::string>({ MDL_AST_LEAF_CON_ID }, *id);
    
    return true;
  }//interpretID
  
  bool StmtInterpreterConstraint::interpretPropType(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx)
  {
    auto tp = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_CON_PROP_TYPE);
    if (!tp || (*tp).empty())
    {
      // Default propagation type is bound
      aIMCtx.context().addValue<std::string>({ MDL_AST_LEAF_PROPAGATION_TYPE }, MDL_AST_LEAF_PROPAGATION_TYPE_BOUND);
      return true;
    }
    
    // Set constraint propagation type
    std::string propType = *tp;
    model_assert(propType == MDL_AST_LEAF_PROPAGATION_TYPE_BOUND || MDL_AST_LEAF_PROPAGATION_TYPE_DOMAIN);
    
    aIMCtx.context().addValue<std::string>({ MDL_AST_LEAF_PROPAGATION_TYPE }, propType);
    return true;
  }//interpretPropType
  
  bool StmtInterpreterConstraint::interpretScope(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx)
  {
    // Get the scope
    auto scope = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_CON_PARAMS);
    model_assert(scope && !(*scope).empty());
    
    auto arguments = CLI::Utils::splitArguments(*scope);
    model_assert(!arguments.empty());
    
    // Add constraint scope
    bool fullyInterpreted{true};
    ModelContext::Path p = { MDL_AST_LEAF_CON_SCOPE };
    for (auto& arg : arguments)
    {
      if(isExprFullyInterpretedOnView(arg, aIMCtx.view()))
      {
        // The argument is already fully interpreted w.r.t. the current view
        aIMCtx.context().addValueList<std::string>(p, arg);
      }
      else if (auto ival = iutils::interpretExpr(arg, aIMCtx.view()))
      {
        // The argument can be interpreted in the current view
        arg = *ival;
        aIMCtx.context().addValueList<std::string>(p, arg);
        
        if(!isExprFullyInterpretedOnView(arg, aIMCtx.view()))
        {
          // After interpretation, the argument is still not fully interpreted
          fullyInterpreted = false;
        }
      }
      else
      {
        // The argument cannot be interpreted, set flag and add it as is
        aIMCtx.context().addValueList<std::string>(p, arg);
        fullyInterpreted = false;
      }
    }
    
    return fullyInterpreted;
  }//interpretScope
  
}// end namespace Model
