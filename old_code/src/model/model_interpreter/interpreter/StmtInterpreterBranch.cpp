// Self first
#include "StmtInterpreterBranch.hpp"

#include "ModelASTMacro.hpp"
#include "ModelMacro.hpp"

#include "ViewEntryArray.hpp"
#include "InterpreterUtils.hpp"
#include "CLIUtils.hpp"

#include <string>
#include <unordered_set>
#include <utility>
#include <cassert>

namespace iutils = Model::Utils::Interpreter;
namespace cutils = CLI::Utils;

namespace Model {
  
  static bool isValidVarChoice(const std::string& aStr)
  {
    static std::unordered_set<std::string> varChoice =
    {
        MDL_AST_LEAF_BRC_VAR_INPUT_ORDER
      , MDL_AST_LEAF_BRC_VAR_LARGEST
      , MDL_AST_LEAF_BRC_VAR_SMALLEST
      , MDL_AST_LEAF_BRC_VAR_FIRST_FAIL
      , MDL_AST_LEAF_BRC_VAR_ANTI_FIRST_FAIL
      , MDL_AST_LEAF_BRC_VAR_DEFAULT
    };
    
    return varChoice.find(aStr) != varChoice.end();
  }//isValidVarChoice
  
  static bool isValidValChoice(const std::string& aStr)
  {
    static std::unordered_set<std::string> valChoice =
    {
        MDL_AST_LEAF_BRC_VAL_INDOMAIN_MAX
      , MDL_AST_LEAF_BRC_VAL_INDOMAIN_MIN
      , MDL_AST_LEAF_BRC_VAL_INDOMAIN_RANDOM
      , MDL_AST_LEAF_BRC_VAL_DEFAULT
    };
    
    return valChoice.find(aStr) != valChoice.end();
  }//isValidVarChoice
  
  bool StmtInterpreterBranch::interpretBranchingSet(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx)
  {
    auto branchingList = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_BRC_BRANCHING_VAR);
    if (!branchingList) return true;
    
    // Branching set should not be empty
    auto brcList = *branchingList;
    model_assert(!brcList.empty());
    
    // Branching can be performed on a single variable or on a set of variables
    std::vector<std::string> brcVars;
    auto brcListType = iutils::getExprType(brcList);
    if(brcListType == iutils::ExprType::AT_ARRAY)
    {
      brcVars = iutils::getArrayElements(brcList);
    }
    else
    {
      brcVars.push_back(brcList);
    }
    
    // Add branching variables
    bool fullyInterpreted{true};
    ModelContext::Path p = { MDL_AST_LEAF_BRC_BRANCHING_SET };
    
    std::unordered_set<std::string> varSet;
    for (auto& var : brcVars)
    {
      if (aMCtx.view().findAlias(var))
      {
        if(varSet.find(var) != varSet.end()) continue;
        varSet.insert(var);
        
        // The branching variable is an alias for a variable
        aIMCtx.context().addValueList<std::string>(p, var);
      }
      else if (auto ival = iutils::interpretExpr(var, aIMCtx.view()))
      {
        var = *ival;

        brcListType = iutils::getExprType(var);
        model_assert_msg(brcListType == iutils::ExprType::AT_ID ||
                         brcListType == iutils::ExprType::AT_SUBSCRIPT, "Invalid branching element: " + var);
        
        if (aIMCtx.view().findAlias(var))
        {
          if(varSet.find(var) != varSet.end()) continue;
          varSet.insert(var);
          
          aIMCtx.context().addValueList<std::string>(p, *ival);
        }
        else if(brcListType == iutils::ExprType::AT_ID && aIMCtx.view().lookup(var))
        {
          // Check if the ID corresponds to a matrix of aliases which is fully interpreted.
          // If so, use the aliases as branching set
          auto ventry = aIMCtx.view().lookup(var);
          if(ViewEntryArray::isa(ventry) && ventry->isFullyInterpreted())
          {
            for(auto& elem : ViewEntryArray::cast(ventry)->getArray())
            {
              if(varSet.find(elem) != varSet.end()) continue;
              varSet.insert(elem);
              
              model_assert_msg(iutils::getExprType(elem) == iutils::ExprType::AT_ID, "Invalid branching element: " + var);
              aIMCtx.context().addValueList<std::string>(p, elem);
            }
          }
        }
        else
        {
          // Argument cannot be interpreted
          aIMCtx.context().addValueList<std::string>(p, *ival);
          fullyInterpreted = false;
        }
      }
      else
      {
        brcListType = iutils::getExprType(var);
        model_assert_msg(brcListType == iutils::ExprType::AT_ID ||
                         brcListType == iutils::ExprType::AT_SUBSCRIPT, "Invalid branching element: " + var);
        
        if(varSet.find(var) != varSet.end()) continue;
        varSet.insert(var);
        
        // The constraint argument cannot be interpreted
        aIMCtx.context().addValueList<std::string>(p, var);
        fullyInterpreted = false;
      }
    }

    return fullyInterpreted;
  }//interpretBranchingSet
  
  bool StmtInterpreterBranch::interpretHeuristics(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx)
  {
    // Create sub-context for heuristics
    Context heuristics;
    
    auto varChoice = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_BRC_VAR_CHOICE_TYPE);
    auto valChoice = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_BRC_VAL_CHOICE_TYPE);
    
    // Variable choice
    Context::Path pHVar = { MDL_AST_LEAF_BRC_VAR_CHOICE };
    if (!varChoice || (*varChoice).empty() || *varChoice == MDL_AST_LEAF_BRC_VAR_DEFAULT)
    {
      // Set default var choice if not specified
      aIMCtx.context().addValue<std::string>(pHVar, MDL_AST_LEAF_BRC_VAR_DEFAULT_VAL);
    }
    else
    {
      // Var choice is specified but it must be a valid choice
      auto varH = *varChoice;
      model_assert_msg(isValidVarChoice(varH), "Unknow variable choice: " + varH);
      aIMCtx.context().addValue<std::string>(pHVar, varH);
    }
    
    // Valkue choice
    Context::Path pHVal = { MDL_AST_LEAF_BRC_VAL_CHOICE };
    if (!valChoice || (*valChoice).empty() || *valChoice == MDL_AST_LEAF_BRC_VAL_DEFAULT)
    {
      // Set default var choice if not specified
      aIMCtx.context().addValue<std::string>(pHVal, MDL_AST_LEAF_BRC_VAL_DEFAULT_VAL);
    }
    else
    {
      // Var choice is specified but it must be a valid choice
      auto valH = *valChoice;
      model_assert_msg(isValidValChoice(valH), "Unknow value choice: " + valH);
      aIMCtx.context().addValue<std::string>(pHVal, valH);
    }

    return true;
  }//interpretHeuristics
  
  bool StmtInterpreterBranch::interpretKey(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx)
  {
    auto key = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_BRC_KEY);
    if (key)
    {
      std::string keyval = MDL_AST_LEAF_BRC_KEY_PREFIX;
      keyval += *key;
      
      // Set constraint key
      aIMCtx.context().addValue<std::string>({ MDL_AST_LEAF_BRC_KEY }, keyval);
    }
    return true;
  }//interpretKey
  
  StmtInterpreterBranch::StmtInterpreterBranch()
  : Model::StmtInterpreter(StmtInterpreter::SymbolType::ST_BRANCH_DECL)
  {
    registerInterpreters();
  }
  
  void StmtInterpreterBranch::registerInterpreters()
  {
    registerInterpreterFcn("interpret_key",
                           std::bind(&StmtInterpreterBranch::interpretKey, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    registerInterpreterFcn("interpret_branching_set",
                           std::bind(&StmtInterpreterBranch::interpretBranchingSet, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    registerInterpreterFcn("interpret_heuristics",
                           std::bind(&StmtInterpreterBranch::interpretHeuristics, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
  }//registerInterpreters
  
}// end namespace Model
