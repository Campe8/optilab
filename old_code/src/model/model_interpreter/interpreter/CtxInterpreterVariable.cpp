// Self first
#include "CtxInterpreterVariable.hpp"

// Macros
#include "ModelASTMacro.hpp"
#include "ModelMacro.hpp"

#include "StmtInterpreterVariable.hpp"
#include "Context.hpp"

// View
#include "ViewEntryArray.hpp"

// Utilities
#include "InterpreterUtils.hpp"
#include "CLIUtils.hpp"

#include <string>
#include <utility>
#include <unordered_set>
#include <memory>
#include <cassert>

namespace iutils = Model::Utils::Interpreter;
namespace cutils = CLI::Utils;

namespace Model {
  
  CtxInterpreterVariable::CtxInterpreterVariable()
  : Model::CtxInterpreter(ModelInterpreter::SymbolType::ST_VARIABLE_DECL)
  {
    registerInterpreters();
  }
  
  void CtxInterpreterVariable::registerInterpreters()
  {
    registerInterpreterFcn("interpret_domain",
                           std::bind(&CtxInterpreterVariable::interpretDomain, this, std::placeholders::_1, std::placeholders::_2));
    registerInterpreterFcn("interpret_dimensions",
                           std::bind(&CtxInterpreterVariable::interpretDimensions, this, std::placeholders::_1, std::placeholders::_2));
  }//registerInterpreters
  
  std::pair<bool, bool> CtxInterpreterVariable::interpretDomain(const ModelContext& aMdlCtx, ModelContext& aCtx)
  {
    // Get the vector of domain elements
    auto domain = getVectorizedDomain(aCtx);
    model_assert(!domain.empty());
    
    // Interpret each domain element of, possibly,
    // each domain for a matrix variable
    bool fullyInterpreted{ true };
    
    // Optimization: flag indicating that at least one
    // element of the matrix changed during interpretation
    bool changed{ false };
    
    // Just need to parse the first row since all the elements of
    // a variable matrix are the same
    for (auto& val : domain[0])
    {
      // "Cleanup" values
      cutils::trim(val);
      if (auto ival = iutils::interpretExpr(val, aMdlCtx.view()))
      {
        // At least one value is interpreted
        changed = true;
        val = *ival;
        model_assert_msg(iutils::getExprType(val) != Utils::Interpreter::ExprType::AT_ID,
                         "Invalid domain declaration");
      }
      else
      {
        // Set interpretation flag as "not fully interpreted"
        fullyInterpreted = false;
      }
    }//for
    
    if (!changed)
    {
      return { changed, fullyInterpreted };
    }
    
    // At least one value changed during interpretation
    ModelContext::Path p = { MDL_AST_LEAF_VAR_DOMAIN, MDL_AST_LEAF_VAR_DOM_VALS };
    aCtx.context().deleteSubContext(p);
    
    // Replace values: delete old sub-context with values
    std::size_t dim{1};
    auto dims = aCtx.context().getValueList<std::string>({ MDL_AST_LEAF_VAR_DIMENSIONS });
    if(!dims.empty() && cutils::isInteger(dims[0]) && cutils::isInteger(dims[1]))
    {
      dim = getVectorizedDomainDimension(aCtx);
      dim = dim == 0 ? 1 : dim;
    }
    if (fullyInterpreted)
    {
      // Convert value to Integer values if the domain is fully interpreted
      std::vector<INT_64_T> intArray(domain[0].size());
      std::transform(domain[0].begin(), domain[0].end(), intArray.begin(), cutils::stoi);
      
      // Sets the context "p" with ["aVals"] "dim" times
      addValueListToCtx<INT_64_T>(aCtx.context(), p, intArray, dim);
    }
    else
    {
      // Sets the context "p" with ["aVals"] "dim" times
      addValueListToCtx<std::string>(aCtx.context(), p, domain[0], dim);
    }
    
    return { changed, fullyInterpreted };
  }//interpretValues
  
  std::pair<bool, bool> CtxInterpreterVariable::CtxInterpreterVariable::interpretDimensions(const ModelContext& aMdlCtx, ModelContext& aCtx)
  {
    if (!containsDomainDimensionsField(aCtx)) { return { false, true }; }
    
    // Get dimensions values
    auto dim = aCtx.context().getValueList<std::string>({ MDL_AST_LEAF_VAR_DIMENSIONS });
    assert(!dim.empty());
    
    if (cutils::isInteger(dim[0]) && cutils::isInteger(dim[1]))
    {
      return {false, true};
    }
    
    // Interpret each dimension
    bool fullyInterpreted{ true };
    
    // Optimization: flag indicating that at least one
    // dimension changed during interpretation
    bool changed{ false };
    
    for (auto& val : dim)
    {
      // "Cleanup" values
      cutils::trim(val);
      if (auto ival = iutils::interpretExpr(val, aMdlCtx.view()))
      {
        // At least one value is interpreted
        changed = true;
        val = *ival;
        model_assert_msg(iutils::getExprType(val) != Utils::Interpreter::ExprType::AT_ID,
                         "Invalid domain declaration");
      }
      else
      {
        // Set interpretation flag as "not fully interpreted"
        fullyInterpreted = false;
      }
    }//for
    
    ModelContext::Path p = { MDL_AST_LEAF_VAR_DIMENSIONS };
    
    aCtx.context().deleteSubContext(p);
    if (cutils::isInteger(dim[0]))
    {
      aCtx.context().addValueList<INT_64_T>(p, cutils::stoi(dim[0]));
    }
    else
    {
      aCtx.context().addValueList<std::string>(p, dim[0]);
    }
    
    if (cutils::isInteger(dim[1]))
    {
      auto nrowsInt = cutils::stoi(dim[1]);
      nrowsInt = nrowsInt > 1 ? nrowsInt : 1;
      
      // No columns means no dimensions
      if(nrowsInt == 0)
      {
        aCtx.context().deleteSubContext(p);
        return  { changed, true};
      }
      
      aCtx.context().addValueList<INT_64_T>(p, nrowsInt);
    }
    else
    {
      aCtx.context().addValueList<std::string>(p, dim[1]);
    }
    
    return { changed, fullyInterpreted };
  }//interpretDimensions
  
  void CtxInterpreterVariable::doInterpretation()
  {
    runInterpreterFcn("interpret_dimensions");
    runInterpreterFcn("interpret_domain");
  }//doInterpretation
  
}// end namespace Model

