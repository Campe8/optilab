// Self first
#include "BranchContextObject.hpp"

#include "ModelMacro.hpp"
#include "MVCUtils.hpp"

#include "ObjectHelper.hpp"

#include <algorithm>

namespace Model {
  
  BranchContextObject::BranchContextObject(const std::string& aID, const Interpreter::DataObject& aDO)
  : ContextObject(aID, aDO)
  , pVarChoice(VariableChoice::VARC_INPUT_ORDER)
  , pValChoice(ValueChoice::VALC_INDOMAIN_MIN)
  {
    assert(aDO.composeSize() > 0);
    
    setType(Type::CTX_OBJ_BRC);
    if(aDO.isFullyInterpreted())
    {
      removeDuplicates(payload());
    
      if(payload().composeSize() > 1)
      {
        setHeuristics(payload());
      }
    }
  }
  
  void BranchContextObject::setHeuristics(const Interpreter::DataObject& aDO)
  {
    std::string varChoice, valChoice;
    if(aDO.composeSize() > 1)
    {
      varChoice = aDO[1].getDataValue<std::string>();
      if(aDO.composeSize() > 2)
      {
        valChoice = aDO[2].getDataValue<std::string>();
      }
    }
    if(varChoice == MDL_AST_LEAF_BRC_VAR_DEFAULT) pVarChoice = getDefaultVariableChoice();
    if(varChoice == MDL_AST_LEAF_BRC_VAR_INPUT_ORDER) pVarChoice = VariableChoice::VARC_INPUT_ORDER;
    if(varChoice == MDL_AST_LEAF_BRC_VAR_FIRST_FAIL) pVarChoice = VariableChoice::VARC_FIRST_FAIL;
    if(varChoice == MDL_AST_LEAF_BRC_VAR_LARGEST) pVarChoice = VariableChoice::VARC_LARGEST;
    if(varChoice == MDL_AST_LEAF_BRC_VAR_SMALLEST) pVarChoice = VariableChoice::VARC_SMALLEST;
    if(varChoice == MDL_AST_LEAF_BRC_VAR_ANTI_FIRST_FAIL) pVarChoice = VariableChoice::VARC_ANTI_FIRST_FAIL;
    
    if(valChoice == MDL_AST_LEAF_BRC_VAL_DEFAULT) pValChoice = getDefaultValueChoice();
    if(valChoice == MDL_AST_LEAF_BRC_VAL_INDOMAIN_MAX) pValChoice = ValueChoice::VALC_INDOMAIN_MAX;
    if(valChoice == MDL_AST_LEAF_BRC_VAL_INDOMAIN_MIN) pValChoice = ValueChoice::VALC_INDOMAIN_MIN;
    if(valChoice == MDL_AST_LEAF_BRC_VAL_INDOMAIN_RANDOM) pValChoice = ValueChoice::VALC_INDOMAIN_RANDOM;
  }//setHeuristics
  
  void BranchContextObject::removeDuplicates(Interpreter::DataObject& aDO)
  {
    std::vector<std::string> branchingVarIds;
    std::vector<Interpreter::DataObject> uniqueBranchingVars;
    
    // For single variables, there is nothing to remove, return
    if(aDO[0].isClassObject() &&
       Interpreter::ObjectTools::isVariableObject(aDO[0].getClassObject().get())) return;
    
    assert(aDO[0].isClassObject() &&
           (Interpreter::ObjectTools::isMatrixParameterObject(aDO[0].getClassObject().get()) ||
           Interpreter::ObjectTools::isListObject(aDO[0].getClassObject().get())));
    for (const auto& var : aDO[0])
    {
      assert(var.isClassObject() &&
             (Interpreter::ObjectTools::isVariableObject(var.getClassObject().get()) ||
              !var.isFullyInterpreted()));

      std::string ID;
      if(var.isFullyInterpreted())
      {
        // Do not allow subscriptions
        assert(!MVC::Utils::isVarMatrixSubscr(var));
        ID = MVC::Utils::getLinkedVariableIDFromDomainDataObject(var);
      }
      else
      {
        assert(var.dataObjectType() == Interpreter::DataObject::DataObjectType::DOT_STRING);
        ID = var.getDataValue<std::string>();
      }
      
      if(std::find(branchingVarIds.begin(), branchingVarIds.end(), ID) == branchingVarIds.end())
      {
        branchingVarIds.push_back(ID);
        uniqueBranchingVars.push_back(var);
      }
    }//for
    
    // Remplace branching variables with the unique set of branching variables
    aDO[0].compose(uniqueBranchingVars);
  }//removeDuplicates
  
}//end namespace Model
