// Base class
#include "StmtInterpreterSearch.hpp"

#include "ModelASTMacro.hpp"
#include "ModelMacro.hpp"

#include "InterpreterUtils.hpp"
#include "CLIUtils.hpp"

#include <string>
#include <unordered_set>
#include <utility>
#include <cassert>

namespace iutils = Model::Utils::Interpreter;
namespace cutils = CLI::Utils;

namespace Model {
  
  static bool isValidVarChoice(const std::string& aStr)
  {
    static std::unordered_set<std::string> varChoice =
    {
        MDL_AST_LEAF_SEARCH_SEMANTIC_VAR_INPUT_ORDER
      , MDL_AST_LEAF_SEARCH_SEMANTIC_VAR_LARGEST
      , MDL_AST_LEAF_SEARCH_SEMANTIC_VAR_SMALLEST
      , MDL_AST_LEAF_SEARCH_SEMANTIC_VAR_FIRST_FAIL
      , MDL_AST_LEAF_SEARCH_SEMANTIC_VAR_ANTI_FIRST_FAIL
    };
    
    return varChoice.find(aStr) != varChoice.end();
  }//isValidVarChoice
  
  static bool isValidValChoice(const std::string& aStr)
  {
    static std::unordered_set<std::string> valChoice =
    {
        MDL_AST_LEAF_SEARCH_SEMANTIC_VAL_INDOMAIN_MAX
      , MDL_AST_LEAF_SEARCH_SEMANTIC_VAL_INDOMAIN_MIN
      , MDL_AST_LEAF_SEARCH_SEMANTIC_VAL_INDOMAIN_RANDOM
    };
    
    return valChoice.find(aStr) != valChoice.end();
  }//isValidVarChoice
  
  bool StmtInterpreterSearch::interpretType(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx)
  {
    auto typeSrc = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_SRC_TYPE);
    if(!typeSrc || (*typeSrc).empty())
    {
      return false;
    }
    
    // Set variable id
    aIMCtx.context().addValue<std::string>({ MDL_AST_LEAF_SEARCH_TYPE }, *typeSrc);
    
    return true;
  }//interpretType
  
  bool StmtInterpreterSearch::interpretBranchingSet(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx)
  {
    auto branchingList = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_SRC_BRANCHING_VAR);
    if (!branchingList)
    {
      return true;
    }
    model_assert(!(*branchingList).empty());
    
    auto varList = cutils::tokenizeStringOnSymbol(*branchingList, ' ');
    
    Context::Path p = { MDL_AST_LEAF_SEARCH_BRANCHING_SET };
    
    std::unordered_set<std::string> varSet;
    for (auto& var : varList)
    {
      if(varSet.find(var) != varSet.end()) continue;
      varSet.insert(var);
      aIMCtx.context().addValueList <std::string>(p, var);
    }
    
    return true;
  }//interpretBranchingSet
  
  // Sets "aCtx" with the semantic of the search from "aStmt".
  // Returns true on success, false otherwise
  bool StmtInterpreterSearch::interpretSemantic(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx)
  {
    // Create sub-context for semantic
    Context semantic;
    
    auto varChoice = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_SRC_VAR_CHOICE_TYPE);
    if (!varChoice || (*varChoice).empty() || !isValidVarChoice(*varChoice))
    {
      return false;
    }
    semantic.addValue<std::string>({ MDL_AST_LEAF_SEARCH_SEMANTIC_VAR_CHOICE }, *varChoice);
    
    auto valChoice = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_SRC_VAL_CHOICE_TYPE);
    if (!valChoice || (*valChoice).empty() || !isValidValChoice(*valChoice))
    {
      return false;
    }
    semantic.addValue<std::string>({ MDL_AST_LEAF_SEARCH_SEMANTIC_VAL_CHOICE }, *valChoice);
    
    // Add semantic context
    aIMCtx.context().addSubContext({ MDL_AST_LEAF_VAR_SEMANTIC }, semantic);
    
    return true;
  }//interpretSemantic
  
  bool StmtInterpreterSearch::interpretNumSol(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx)
  {
    auto nsol = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_SRC_NUM_SOLUTIONS);
    if (!nsol)
    {
      aIMCtx.context().addValue<std::size_t>({ MDL_AST_LEAF_SEARCH_NUM_SOLUTIONS }, 1);
      return true;
    }
    
    auto val = *nsol;
    model_assert(!val.empty());
    
    cutils::trim(val);
    if (auto ival = iutils::interpretExpr(val, aIMCtx.view()))
    {
      model_assert(CLI::Utils::isInteger(*ival));
      auto num = CLI::Utils::stoi(*ival);
      
      if(num < 0) num = 0;
      aIMCtx.context().addValue<std::size_t>({ MDL_AST_LEAF_SEARCH_NUM_SOLUTIONS }, static_cast<std::size_t>(num));
      
      // Fully interpreted value
      return true;
    }
    
    // Value couldn't been interpreted,
    // return needs further interpretation
    aIMCtx.context().addValue<std::string>({ MDL_AST_LEAF_SEARCH_NUM_SOLUTIONS }, val);
    return false;
  }//interpretNumSol
  
  StmtInterpreterSearch::StmtInterpreterSearch()
  : Model::StmtInterpreter(StmtInterpreter::SymbolType::ST_SEARCH_DECL)
  {
    registerInterpreters();
  }
  
  void StmtInterpreterSearch::registerInterpreters()
  {
    registerInterpreterFcn("interpret_type",
                           std::bind(&StmtInterpreterSearch::interpretType, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    registerInterpreterFcn("interpret_branching_set",
                           std::bind(&StmtInterpreterSearch::interpretBranchingSet, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    registerInterpreterFcn("interpret_semantic",
                           std::bind(&StmtInterpreterSearch::interpretSemantic, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    registerInterpreterFcn("interpret_num_sol",
                           std::bind(&StmtInterpreterSearch::interpretNumSol, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
  }//registerInterpreters
  
}// end namespace Model
