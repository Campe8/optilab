// Base class
#include "ModelInterpreter.hpp"
#include "ModelMacro.hpp"

namespace Model {
  
  ModelInterpreter::ModelInterpreter(SymbolType aSymbType)
  : pInterpretedSymb(aSymbType)
  , pInterpreted(false)
  , pICtx(nullptr)
  , pMCtx(nullptr)
  {
  }
  
  void ModelInterpreter::initInterpreter()
  {
    model_assert(pICtx);
    model_assert(pMCtx);
    
    // Set interpreted state as true to be in "and" with the each interpreting function
    setFullyInterpretedState(true);
    
    // Set the view as the view hold by the current model context.
    // This view is shared between all interpreters if the model context
    // remains the same between calls to the interpreters
    pICtx->setView(pMCtx->getView());
  }//initInterpreter
  
  void ModelInterpreter::runInterpreter()
  {
    initInterpreter();
    
    // Run the interpreter.
    // This call the derived instances which, in turn, will call their interpreting functions
    doInterpretation();
    model_assert(!(*pICtx).context().isEmpty());
    
    finalizeInterpreter();
  }//runInterpreter
  
  void ModelInterpreter::finalizeInterpreter()
  {
    model_assert(pICtx);
    
    // Change fully interpreted flag
    (*pICtx).context().addValue<int>({ MDL_AST_OBJ_FINTERP }, pInterpreted ? 1 : 0);
  }//finalizeInterpreter
  
  void ModelInterpreter::assertAndThrow(bool aTest, const std::string& aMsg)
  {
    // On false assert the statement is not being interpreted
    if (!aTest)
    {
      setFullyInterpretedState(false);
    }
    
    // Assert on "aTest"
    MODEL_ASSERT_MSG(aTest, aMsg.c_str());
  }//assertAndThrow
  
}// end namespace Model
