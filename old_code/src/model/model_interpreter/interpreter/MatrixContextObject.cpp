// Self first
#include "MatrixContextObject.hpp"

namespace Model {
  
  MatrixContextObject::MatrixContextObject(const std::string& aID, const Interpreter::DataObject& aDO)
  : ContextObject(aID, aDO)
  {
    setType(Type::CTX_OBJ_MAT);
  }
  
}//end namespace Model

