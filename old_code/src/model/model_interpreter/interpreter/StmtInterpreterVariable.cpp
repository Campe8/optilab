// Base class
#include "StmtInterpreterVariable.hpp"

#include "ModelASTMacro.hpp"
#include "InterpreterUtils.hpp"
#include "CLIUtils.hpp"

#include "ModelMacro.hpp"

#include <string>
#include <algorithm>
#include <utility>

#include <cassert>

namespace cutils = CLI::Utils;
namespace iutils = Model::Utils::Interpreter;

namespace Model {
  
  // Sets the domain (set of values) "aVals" in the context "aDomain".
  // "aFullyInterpreted" refers to the vector of values
  static void setDomainCtx(ModelContext& aCtx, Context& aDomain, std::vector<std::string>& aVals, const std::string& aType, bool aFullyInterpreted)
  {
    auto dims = aCtx.context().getValueList<std::string>({ MDL_AST_LEAF_VAR_DIMENSIONS });
    
    std::size_t dim{1};
    if(!dims.empty() && cutils::isInteger(dims[0]) && cutils::isInteger(dims[1]))
    {
      dim = getVectorizedDomainDimension(aCtx);
      dim = dim == 0 ? 1 : dim;
    }
    
    ModelContext::Path p = { MDL_AST_LEAF_VAR_DOM_VALS };
    
    // Convert values to Boolean values
    if (aType == MDL_AST_LEAF_VAR_TYPE_BOOL)
    {
      std::for_each(aVals.begin(), aVals.end(), [](std::string& aVal)
                    {
                      if (cutils::isInteger(aVal) && aVal != "0") { aVal = "1"; }
                    });
    }
    
    // Convert value to Integer values
    if (aFullyInterpreted)
    {
      std::vector<INT_64_T> intArray(aVals.size());
      std::transform(aVals.begin(), aVals.end(), intArray.begin(), cutils::stoi);
      
      // Sets the context "p" with ["aVals"] "dim" times
      addValueListToCtx<INT_64_T>(aDomain, p, intArray, dim);
      return;
    }
    
    // Sets the context "p" with ["aVals"] "dim" times
    addValueListToCtx<std::string>(aDomain, p, aVals, dim);
  }//setDomainCtx
  
  // Sets "aCtx" with the domain spec described by lists such as "d1 d2 d3" in "aDomText"
  static bool interpretVarDomainList(ModelContext& aCtx, const std::string& aDom, const std::string& aVarType)
  {
    model_assert(!aDom.empty());
    
    // Create sub-context for domain
    Context domain;
    
    // Set extensional type
    domain.addValue<std::string>({ MDL_AST_LEAF_VAR_DOM_TYPE }, MDL_AST_LEAF_VAR_DOM_TYPE_EXTENSIONAL);
    
    // Domain list is "d1 d2 d3" where d1, d2, and d3 are numbers (Integers, Floats, etc.)
    auto domainList = CLI::Utils::tokenizeStringOnSymbol(aDom);
    model_assert(!domainList.empty());
    
    // Add extensional domain
    ModelContext::Path p = { MDL_AST_LEAF_VAR_DOM_VALS };
    
    bool fullyInterpreted{ true };
    for (auto& val : domainList)
    {
      cutils::trim(val);
      if (auto valInterp = iutils::interpretExpr(val, aCtx.view()))
      {
        val = *valInterp;
        model_assert_msg(iutils::getExprType(val) != Utils::Interpreter::ExprType::AT_ID,
                         "Invalid domain declaration");
      }
      else
      {
        fullyInterpreted = false;
      }
    }
    
    // Set values in domain
    setDomainCtx(aCtx, domain, domainList, aVarType, fullyInterpreted);
    
    // Add sub-context domain to context
    aCtx.context().addSubContext({ MDL_AST_LEAF_VAR_DOMAIN }, domain);
    
    return fullyInterpreted;
  }//interpretVarDomainList
  
  // Sets "aCtx" with the domain spec described by ranges such as "d1 d2" in "aDomText"
  static bool interpretVarDomainRange(ModelContext& aCtx, const std::string& aDom, const std::string& aVarType)
  {
    // Variable type should be already set in the context
    model_assert(!aVarType.empty() && (aVarType == MDL_AST_LEAF_VAR_TYPE_INT || aVarType == MDL_AST_LEAF_VAR_TYPE_BOOL));
    model_assert(!aDom.empty());
    
    // Create sub-context for domain
    Context domain;
    
    // Set bounds type
    domain.addValue<std::string>({ MDL_AST_LEAF_VAR_DOM_TYPE }, MDL_AST_LEAF_VAR_DOM_TYPE_BOUNDS);
    
    // Domain range is "d1 d2" where d1 and d2 are numbers (Integers, Floats, etc.)
    auto bounds = CLI::Utils::tokenizeStringOnSymbol(aDom);
    model_assert(bounds.size() == 2);
    
    bool fullyInterpreted{ true };
    for (auto& val : bounds)
    {
      cutils::trim(val);
      if (auto valInterp = iutils::interpretExpr(val, aCtx.view()))
      {
        val = *valInterp;
        model_assert_msg(iutils::getExprType(val) != Utils::Interpreter::ExprType::AT_ID,
                         "Invalid domain declaration");
      }
      else
      {
        fullyInterpreted = false;
      }
    }
    
    // Set values in domain
    setDomainCtx(aCtx, domain, bounds, aVarType, fullyInterpreted);
    
    // Add sub-context domain to context
    aCtx.context().addSubContext({ MDL_AST_LEAF_VAR_DOMAIN }, domain);
    return fullyInterpreted;
  }//interpretVarDomainRange
  
  // Sets "aCtx" with the domain spec described by expressions such as "y [] 5+(x)" or "5*((t)+3)" or "20" in "aDom"
  static bool interpretVarDomainExpression(ModelContext& aCtx, const std::string& aDom, const std::string& aVarType)
  {
    // Variable type should be already set in the context
    model_assert(aVarType == MDL_AST_LEAF_VAR_TYPE_INT || aVarType == MDL_AST_LEAF_VAR_TYPE_BOOL);
    
    // Create sub-context for domain
    Context domain;
    
    // Set bounds type (singleton)
    domain.addValue<std::string>({ MDL_AST_LEAF_VAR_DOM_TYPE }, MDL_AST_LEAF_VAR_DOM_TYPE_BOUNDS);
    
    ModelContext::Path p = { MDL_AST_LEAF_VAR_DOM_VALS };
    
    bool fullyInterpreted{ true };
    auto domExpr = cutils::trimmed(aDom);
    if (auto valInterp = iutils::interpretExpr(domExpr, aCtx.view()))
    {
      domExpr = *valInterp;
      model_assert_msg(iutils::getExprType(domExpr) != Utils::Interpreter::ExprType::AT_ID,
                       "Invalid domain declaration");
    }
    else
    {
      fullyInterpreted = false;
    }
    
    // Set values in domain
    std::vector<std::string> singleton{ domExpr };
    setDomainCtx(aCtx, domain, singleton, aVarType, fullyInterpreted);
    
    // Add sub-context domain to context
    aCtx.context().addSubContext({ MDL_AST_LEAF_VAR_DOMAIN }, domain);
    
    return fullyInterpreted;
  }//interpretVarDomainExpression
  
  bool StmtInterpreterVariable::interpretID(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx)
  {
    auto id = iutils::getIDFromStmt(aStmt);
    model_assert(id && !(*id).empty());
    
    // Add alias to the view
    aIMCtx.view().addAlias(*id);
    
    // Set variable ID
    aIMCtx.context().addValue<std::string>({ MDL_AST_LEAF_VAR_ID }, *id);
    return true;
  }//interpretID
  
  bool StmtInterpreterVariable::interpretBranching(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx)
  {
    auto br = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_VAR_BRANCHING);
    if (!br || (*br).empty())
    {
      aIMCtx.context().addValue<std::string>({ MDL_AST_LEAF_VAR_BRANCHING }, MDL_AST_FALSE);
      return true;
    }
    
    // Set branching policy
    auto brVal = *br != "0" ? MDL_AST_TRUE : MDL_AST_FALSE;
    aIMCtx.context().addValue<std::string>({ MDL_AST_LEAF_VAR_BRANCHING }, brVal);
    
    return true;
  }//interpertBranching
  
  bool StmtInterpreterVariable::interpretType(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx)
  {
    auto tp = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_VAR_TYPE);
    model_assert(tp && !(*tp).empty());
    
    // Set variable type
    std::string varType{ "" };
    if (*tp == CLI_MSG_VAL_VAR_TYPE_BOOL)
    {
      varType = MDL_AST_LEAF_VAR_TYPE_BOOL;
    }
    else
    {
      assert(*tp == CLI_MSG_VAL_VAR_TYPE_INT);
      varType = MDL_AST_LEAF_VAR_TYPE_INT;
    }
    
    aIMCtx.context().addValue<std::string>({ MDL_AST_LEAF_VAR_TYPE }, varType);
    return true;
  }//interpretType
  
  bool StmtInterpreterVariable::interpretOutput(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx)
  {
    auto out = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_VAR_OUTPUT);
    if (!out || (*out).empty())
    {
      aIMCtx.context().addValue<std::string>({ MDL_AST_LEAF_VAR_OUTPUT }, MDL_AST_FALSE);
      return true;
    }
    
    assert(CLI::Utils::isInteger(*out));
    std::string outValStr = std::stoi(*out) != 0 ? MDL_AST_TRUE : MDL_AST_FALSE;
    aIMCtx.context().addValue<std::string>({ MDL_AST_LEAF_VAR_OUTPUT }, outValStr);
    
    return true;
  }//interpretOutput
  
  bool StmtInterpreterVariable::interpretInputOrder(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx)
  {
    auto io = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_VAR_INPUT_ORDER);
    if (!io || (*io).empty())
    {
      aIMCtx.context().addValue<std::size_t>({ MDL_AST_LEAF_VAR_INPUT_ORDER }, 0);
      return true;
    }
    assert(CLI::Utils::isInteger(*io));
    aIMCtx.context().addValue<std::size_t>({ MDL_AST_LEAF_VAR_INPUT_ORDER }, static_cast<std::size_t>(CLI::Utils::stoi(*io)));
    return true;
  }//interpretInputOrder
  
  bool StmtInterpreterVariable::interpretSemantic(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx)
  {
    // Create sub-context for semantic
    Context semantic;
    ModelContext::Path p = { MDL_AST_LEAF_VAR_SEMANTIC_TYPE };
    
    auto st = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_VAR_SEMANTIC_TYPE);
    if (!st || (*st).empty() || ((*st) == CLI_MSG_VAL_VAR_SEMANTIC_TYPE_DECISION))
    {
      semantic.addValue<std::string>(p, MDL_AST_LEAF_VAR_SEMANTIC_TYPE_DECISION);
    }
    else if ((*st) == CLI_MSG_VAL_VAR_SEMANTIC_TYPE_OBJECTIVE)
    {
      semantic.addValue<std::string>(p, MDL_AST_LEAF_VAR_SEMANTIC_TYPE_OBJECTIVE);
      
      // Set extremum
      auto ext = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_VAR_OBJ_EXTREMUM);
      if (!ext || (*ext).empty() || ((*ext) == MDL_AST_LEAF_VAR_SEMANTIC_OPT_EXTREMUM_MIN))
      {
        semantic.addValue<std::string>({ MDL_AST_LEAF_VAR_SEMANTIC_OPT_EXTREMUM }, MDL_AST_LEAF_VAR_SEMANTIC_OPT_EXTREMUM_MIN);
      }
      else
      {
        assert((*ext) == MDL_AST_LEAF_VAR_SEMANTIC_OPT_EXTREMUM_MAX);
        semantic.addValue<std::string>({ MDL_AST_LEAF_VAR_SEMANTIC_OPT_EXTREMUM }, MDL_AST_LEAF_VAR_SEMANTIC_OPT_EXTREMUM_MAX);
      }
    }
    else
    {
      assert((*st) == CLI_MSG_VAL_VAR_SEMANTIC_TYPE_SUPPORT);
      semantic.addValue<std::string>(p, MDL_AST_LEAF_VAR_SEMANTIC_TYPE_SUPPORT);
    }
    
    // Add semantic context
    aIMCtx.context().addSubContext({ MDL_AST_LEAF_VAR_SEMANTIC }, semantic);
    
    return true;
  }//interpretSemantic
  
  bool StmtInterpreterVariable::interpretDimension(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx)
  {
    auto nrows = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_VAR_NUM_ROWS);
    auto ncols = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_VAR_NUM_COLS);
    
    if (!nrows || !ncols || (*nrows).empty() || (*ncols).empty())
    {
      return true;
    }
    
    // If both dimensions are integer values, stored them and continue
    ModelContext::Path p = { MDL_AST_LEAF_VAR_DIMENSIONS };
    if (cutils::isInteger(*nrows) && cutils::isInteger(*ncols))
    {
      auto nrowsInt = cutils::stoi(*nrows);
      auto ncolsInt = cutils::stoi(*ncols);
      
      // 0 columns means no dimensions
      if (ncolsInt == 0) { return true; }
      
      nrowsInt = nrowsInt > 1 ? nrowsInt : 1;
      aIMCtx.context().addValueList<INT_64_T>(p, nrowsInt);
      aIMCtx.context().addValueList<INT_64_T>(p, ncolsInt);
      return true;
    }
    
    // At least one dimension is not an integer and must be interpreted
    bool fullyInterpreted{ true };
    if (auto irows = iutils::interpretExpr(*nrows, aIMCtx.view()))
    {
      model_assert(cutils::isInteger(*irows));
      aIMCtx.context().addValueList<INT_64_T>(p, cutils::stoi(*irows));
    }
    else
    {
      aIMCtx.context().addValueList<std::string>(p, *nrows);
      fullyInterpreted = false;
    }
    
    if (auto icols = iutils::interpretExpr(*ncols, aIMCtx.view()))
    {
      model_assert(cutils::isInteger(*icols));
      
      auto intCols = cutils::stoi(*icols);
      if (intCols == 0) { return true; }
      
      intCols = intCols > 1 ? intCols : 1;
      aIMCtx.context().addValueList<INT_64_T>(p, intCols);
    }
    else
    {
      aIMCtx.context().addValueList<std::string>(p, *ncols);
      fullyInterpreted = false;
    }
    
    return fullyInterpreted;
  }//interpretDimension
  
  // Sets "aCtx" with the domain as describe in "aStrMsg"
  bool StmtInterpreterVariable::interpretDomain(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx)
  {
    auto domf = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_VAR_DOM_DEC_FORMAT);
    auto domt = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_VAR_DOM_DEC_TEXT);
    model_assert(domf && domt && !(*domf).empty() && !(*domt).empty());
    
    auto tp = cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_VAR_TYPE);
    model_assert(tp && !(*tp).empty());
    
    // Set domain based on format
    if ((*domf) == CLI_MSG_VAL_VAR_DOM_DEC_FORMAT_EXP) { return interpretVarDomainExpression(aIMCtx, *domt, *tp); }
    if ((*domf) == CLI_MSG_VAL_VAR_DOM_DEC_FORMAT_RNG) { return interpretVarDomainRange(aIMCtx, *domt, *tp); }
    
    model_assert((*domf) == CLI_MSG_VAL_VAR_DOM_DEC_FORMAT_LST);
    return interpretVarDomainList(aIMCtx, *domt, *tp);
  }//interpretDomain
  
  StmtInterpreterVariable::StmtInterpreterVariable()
  : Model::StmtInterpreter(StmtInterpreter::SymbolType::ST_VARIABLE_DECL)
  {
    registerInterpreters();
  }
  
  void StmtInterpreterVariable::registerInterpreters()
  {
    registerInterpreterFcn("interpret_ID",
                           std::bind(&StmtInterpreterVariable::interpretID, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    registerInterpreterFcn("interpret_branching",
                           std::bind(&StmtInterpreterVariable::interpretBranching, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    registerInterpreterFcn("interpret_type",
                           std::bind(&StmtInterpreterVariable::interpretType, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    registerInterpreterFcn("interpret_output",
                           std::bind(&StmtInterpreterVariable::interpretOutput, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    registerInterpreterFcn("interpret_input_order",
                           std::bind(&StmtInterpreterVariable::interpretInputOrder, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    registerInterpreterFcn("interpret_semantic",
                           std::bind(&StmtInterpreterVariable::interpretSemantic, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    registerInterpreterFcn("interpret_dimensions",
                           std::bind(&StmtInterpreterVariable::interpretDimension, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    registerInterpreterFcn("interpret_domain",
                           std::bind(&StmtInterpreterVariable::interpretDomain, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
  }//registerInterpreters
  
  void StmtInterpreterVariable::doInterpretation()
  {
    runInterpreterFcn("interpret_ID");
    runInterpreterFcn("interpret_branching");
    runInterpreterFcn("interpret_type");
    runInterpreterFcn("interpret_output");
    runInterpreterFcn("interpret_input_order");
    runInterpreterFcn("interpret_semantic");
    runInterpreterFcn("interpret_dimensions");
    runInterpreterFcn("interpret_domain");
  }//doInterpretation
  
  bool containsDomainDimensionsField(const ModelContext& aCtx)
  {
    return containsDomainDimensionsField(aCtx.context());
  }//containsDomainDimensionsField
  
  bool containsDomainDimensionsField(const Context& aCtx)
  {
    return !(aCtx.getValueList<std::string>({ MDL_AST_LEAF_VAR_DIMENSIONS })).empty();
  }//containsDomainDimensionsField
  
  std::size_t getVectorizedDomainDimension(const ModelContext& aCtx)
  {
    return getVectorizedDomainDimension(aCtx.context());
  }//getVectorizedDomainDimension
  
  std::size_t getVectorizedDomainDimension(const Context& aCtx)
  {
    if (!containsDomainDimensionsField(aCtx)) { return 0; }
    
    auto dims = aCtx.getValueList<INT_64_T>({ MDL_AST_LEAF_VAR_DIMENSIONS });
    if (dims.size() == 1) { return static_cast<std::size_t>(dims[0] <= 0 ? 1 : dims[0]); }
    dims[0] = dims[0] <= 0 ? 1 : dims[0];
    dims[1] = dims[1] <= 0 ? 1 : dims[1];
    return static_cast<std::size_t>(dims[0] * dims[1]);
  }//getVectorizedDomainDimension
  
  std::vector<std::vector<std::string>> getVectorizedDomain(const ModelContext& aCtx)
  {
    return getVectorizedDomain(aCtx.context());
  }//getVectorizedDomain
  
  std::vector<std::vector<std::string>> getVectorizedDomain(const Context& aCtx)
  {
    std::vector<std::vector<std::string>> matrix;
    
    auto dc = aCtx.getSubContext({ MDL_AST_LEAF_VAR_DOMAIN });
    if (dc.isEmpty()) { return matrix; }
    
    // Get domain values
    auto dims = aCtx.getValueList<std::string>({ MDL_AST_LEAF_VAR_DIMENSIONS });
    auto isMatrix = !dims.empty();
    if(isMatrix)
    {
      // Check if dimensions are interpreted
      if(!cutils::isInteger(dims[1]) || !cutils::isInteger(dims[2]))
      {
        isMatrix = false;
      }
    }
    if (!isMatrix)
    {
      auto vals = dc.getValueList<std::string>({ MDL_AST_LEAF_VAR_DOM_VALS });
      matrix.push_back(vals);
    }
    else
    {
      matrix = dc.getValueMatrix<std::string>({ MDL_AST_LEAF_VAR_DOM_VALS });
    }
    return  matrix;
  }//getVectorizedDomain
  
}// end namespace Model
