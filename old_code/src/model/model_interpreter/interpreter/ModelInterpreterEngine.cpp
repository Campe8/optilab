// Base class
#include "ModelInterpreterEngine.hpp"

#include "CLIUtils.hpp"
#include "CLIDefs.hpp"

#include "ModelMacro.hpp"

// Interpreters
#include "StmtInterpreterVariable.hpp"
#include "StmtInterpreterConstraint.hpp"
#include "StmtInterpreterBranch.hpp"
#include "StmtInterpreterSearch.hpp"
#include "StmtInterpreterParameter.hpp"
#include "CtxInterpreterVariable.hpp"
#include "CtxInterpreterConstraint.hpp"
#include "CtxInterpreterParameter.hpp"
#include "CtxInterpreterBranch.hpp"
#include "CtxInterpreterSearch.hpp"

#include <iostream>
#include <string>

#include <cassert>

namespace Model {
  
  ModelInterpreterEngine::ModelInterpreterEngine()
  {
    for (std::size_t symbolType = 0; symbolType < static_cast<std::size_t>(ModelInterpreter::SymbolType::ST_UNDEF); ++symbolType)
    {
      pInterpreterRegister.push_back({ nullptr, nullptr });
    }
  }
  
  void ModelInterpreterEngine::registerInterpreters()
  {
    assert(pInterpreterRegister.size() == static_cast<std::size_t>(ModelInterpreter::SymbolType::ST_UNDEF));
    for (std::size_t symbolType = 0; symbolType < static_cast<std::size_t>(ModelInterpreter::SymbolType::ST_UNDEF); ++symbolType)
    {
      if (pInterpreterRegister[symbolType].first)
      {
        assert(pInterpreterRegister[symbolType].second);
        
        // Continue if already registered
        continue;
      }
      
      auto sType = static_cast<ModelInterpreter::SymbolType>(symbolType);
      pInterpreterRegister[symbolType].first = getStmtIntrepreterInstance(sType);
      pInterpreterRegister[symbolType].second = getCtxIntrepreterInstance(sType);
    }
  }//registerInterpreters
  
  ModelContext ModelInterpreterEngine::createBaseModelContext(const std::string& aCtxId)
  {
    // Register interpreters for all symbols
    registerInterpreters();
    
    // Create a global context
    auto mctx = createGlobalModelContext();
    
    // Set context id
    mctx.context().addValue<std::string>({ MDL_AST_ROOT_NAME }, aCtxId);
    
    // Set CSP default framework type
    mctx.context().addValue<std::string>({ MDL_AST_ROOT_FRAMEWORK_TYPE }, MDL_AST_ROOT_FRAMEWORK_TYPE_CSP);
    
    // Set default number of solution as 1
    mctx.context().addValue<int>({ MDL_AST_ROOT_SOLUTION_LIMIT }, 1);
    
    return mctx;
  }//createBaseModelContext
  
  ModelInterpreter::SymbolType ModelInterpreterEngine::getSymbolType(const CLI::CLIStmt& aStmt)
  {
    assert(!aStmt.empty());
    
    auto msgType = CLI::Utils::getMsgType(aStmt);
    switch (msgType)
    {
      case CLI::CLI_MSG_VAR_DECL:
        return ModelInterpreter::SymbolType::ST_VARIABLE_DECL;
      case CLI::CLI_MSG_CON_DECL:
        return ModelInterpreter::SymbolType::ST_CONSTRAINT_DECL;
      case CLI::CLI_MSG_BRC_DECL:
        return ModelInterpreter::SymbolType::ST_BRANCH_DECL;
      case CLI::CLI_MSG_SRC_DECL:
        return ModelInterpreter::SymbolType::ST_SEARCH_DECL;
      case CLI::CLI_MSG_MATRIX_DECL:
        return ModelInterpreter::SymbolType::ST_MATRIX_DECL;
      case CLI::CLI_MSG_ERR:
        return ModelInterpreter::SymbolType::ST_ERROR;
      case CLI::CLI_MSG_OBJ_ASSIGN:
      case CLI::CLI_MSG_FCN_CALL:
      case CLI::CLI_MSG_UNDEF:
      default:
        return ModelInterpreter::SymbolType::ST_UNDEF;
    }
  }//getSymbolType
  
  std::shared_ptr<StmtInterpreter> ModelInterpreterEngine::getStmtIntrepreterInstance(ModelInterpreter::SymbolType aSymbolType)
  {
    switch (aSymbolType)
    {
      case ModelInterpreter::SymbolType::ST_VARIABLE_DECL:
        return std::make_shared<StmtInterpreterVariable>();
      case ModelInterpreter::SymbolType::ST_CONSTRAINT_DECL:
        return std::make_shared<StmtInterpreterConstraint>();
      case ModelInterpreter::SymbolType::ST_MATRIX_DECL:
        return std::make_shared<StmtInterpreterParameter>();
      case ModelInterpreter::SymbolType::ST_BRANCH_DECL:
        return std::make_shared<StmtInterpreterBranch>();
      case ModelInterpreter::SymbolType::ST_SEARCH_DECL:
        return std::make_shared<StmtInterpreterSearch>();
      default:
        return nullptr;
    }
  }//getIntrepreterInstance
  
  std::shared_ptr<CtxInterpreter> ModelInterpreterEngine::getCtxIntrepreterInstance(ModelInterpreter::SymbolType aSymbolType)
  {
    switch (aSymbolType)
    {
      case ModelInterpreter::SymbolType::ST_MATRIX_DECL:
        return std::make_shared<CtxInterpreterParameter>();
      case ModelInterpreter::SymbolType::ST_VARIABLE_DECL:
        return std::make_shared<CtxInterpreterVariable>();
      case ModelInterpreter::SymbolType::ST_CONSTRAINT_DECL:
        return std::make_shared<CtxInterpreterConstraint>();
      case ModelInterpreter::SymbolType::ST_BRANCH_DECL:
        return std::make_shared<CtxInterpreterBranch>();
      case ModelInterpreter::SymbolType::ST_SEARCH_DECL:
        return std::make_shared<CtxInterpreterSearch>();
      default:
        return std::make_shared<CtxInterpreterParameter>();
        //return nullptr;
    }
  }//getCtxIntrepreterInstance
  
  void ModelInterpreterEngine::expandModelContext(ModelContext& aModelContext, const ModelContext& aMdlCtx, ModelInterpreter::SymbolType aSType)
  {
    // Expand context with the interpreted context w.r.t. its type
    Context::Path path;
    switch (aSType)
    {
      case ModelInterpreter::SymbolType::ST_VARIABLE_DECL:
        path = { MDL_AST_SUBTREE_VAR, MDL_AST_LEAF_VAR };
        break;
      case ModelInterpreter::SymbolType::ST_BRANCH_DECL:
        path = { MDL_AST_SUBTREE_SEARCH, MDL_AST_LEAF_BRC };
        break;
      case ModelInterpreter::SymbolType::ST_MATRIX_DECL:
        path = { MDL_AST_SUBTREE_PAR, MDL_AST_LEAF_MAT };
        break;
      case ModelInterpreter::SymbolType::ST_SEARCH_DECL:
        path = { MDL_AST_SUBTREE_SEARCH };
        break;
      default:
        assert(aSType == ModelInterpreter::SymbolType::ST_CONSTRAINT_DECL);
        path = { MDL_AST_SUBTREE_CON, MDL_AST_LEAF_CON };
        break;
    }
    
    // Expand the context with the interpreted context as sub-context
    aModelContext.context().expandSubContext(path, aMdlCtx.context());
  }//expandModelContext
  
  StmtInterpreter* ModelInterpreterEngine::getStmtInterpreter(ModelInterpreter::SymbolType aSymbolType)
  {
    assert(aSymbolType != ModelInterpreter::SymbolType::ST_UNDEF);
    assert(static_cast<std::size_t>(aSymbolType) < pInterpreterRegister.size());
    return pInterpreterRegister[static_cast<std::size_t>(aSymbolType)].first.get();
  }//getStmtInterpreter
  
  CtxInterpreter* ModelInterpreterEngine::getCtxInterpreter(ModelInterpreter::SymbolType aSymbolType)
  {
    assert(aSymbolType != ModelInterpreter::SymbolType::ST_UNDEF);
    assert(static_cast<std::size_t>(aSymbolType) < pInterpreterRegister.size());
    return pInterpreterRegister[static_cast<std::size_t>(aSymbolType)].second.get();
  }//getStmtInterpreter
  
  ModelInterpreterEngine::SIContext ModelInterpreterEngine::interpretStmt(const ModelContext& aMdlCxt, const CLI::CLIStmt& aStmt)
  {
    // Get the symbol type encoded in the message and
    // dispatch to the correct interpreter
    auto symbolType = getSymbolType(aStmt);
    assert(symbolType != ModelInterpreter::SymbolType::ST_UNDEF);
    
    // Get stmt interpreter for symbol type
    auto interpreter = getStmtInterpreter(symbolType);
    assert(interpreter);
    
    return std::make_pair(symbolType, interpreter->interpretStmt(aMdlCxt, aStmt));
  }//interpretStmt
  
  ModelInterpreterEngine::OIContext ModelInterpreterEngine::interpretContext(const ModelContext& aMdlCxt, ModelInterpreter::SymbolType aCtxType, const Context& aCtx)
  {
    assert(aCtxType != ModelInterpreter::SymbolType::ST_UNDEF);
    
    // Get ctx interpreter for symbol type
    auto interpreter = getCtxInterpreter(aCtxType);
    assert(interpreter);
    
    return interpreter->interpretCtx(aMdlCxt, aCtx);
  }//interpretContext
  
}// end namespace Model
