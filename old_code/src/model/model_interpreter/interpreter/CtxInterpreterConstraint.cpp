// Self first
#include "CtxInterpreterConstraint.hpp"

// Macros
#include "ModelASTMacro.hpp"
#include "ModelMacro.hpp"

// View
#include "ViewEntryArray.hpp"

// Utilities
#include "InterpreterUtils.hpp"
#include "CLIUtils.hpp"

#include <string>
#include <utility>
#include <unordered_set>
#include <memory>
#include <cassert>

namespace iutils = Model::Utils::Interpreter;
namespace cutils = CLI::Utils;

namespace Model {

  // Recursively check "aExpr" if it is fully interpreted w.r.t. the given view.
  // For example:
  // 10, a, [a, b, 10, [3, 4, a]] with view = (a, b) is fully interpretd
  // 10, b, with view = (a) is not fully interpreted
  static bool isExprFullyInterpretedOnView(const std::string& aExpr, const ContextView& aView)
  {
    auto etype = iutils::getExprType(aExpr);
    if(etype == Utils::Interpreter::ExprType::AT_INT)
    {
      return true;
    }
    else if(etype == Utils::Interpreter::ExprType::AT_ID && aView.findAlias(aExpr))
    {
      return true;
    }
    else if (etype == Utils::Interpreter::ExprType::AT_ARRAY)
    {
      // Interpreted expression is an array check if each element is either an alias
      // or a number, if not this expression is not fully interpreted
      for(const auto& e : iutils::getArrayElements(aExpr))
      {
        if(!isExprFullyInterpretedOnView(e, aView)) return false;
      }
      return true;
    }
    
    return false;
  }//isExprFullyInterpretedOnView
  
	CtxInterpreterConstraint::CtxInterpreterConstraint()
  : Model::CtxInterpreter(ModelInterpreter::SymbolType::ST_CONSTRAINT_DECL)
  {
    registerInterpreters();
  }
  
  void CtxInterpreterConstraint::registerInterpreters()
  {
    registerInterpreterFcn("interpret_scope",
                           std::bind(&CtxInterpreterConstraint::interpretScope, this, std::placeholders::_1, std::placeholders::_2));
  }//registerInterpreters

  std::pair<bool, bool> CtxInterpreterConstraint::interpretScope(const ModelContext& aMdlCtx, ModelContext& aCtx)
  {
		// Get the constraint's scope
		auto scope = aCtx.context().getValueList<std::string>({ MDL_AST_LEAF_CON_SCOPE });

		// Interpret each scope element
		bool fullyInterpreted{ true };

		// Optimization: flag indicating that at least one
		// element of the scope changed during interpretation
		bool changed{ false };
    for (auto& sexpr : scope)
    {
      // "Cleanup" expression
      cutils::trim(sexpr);
      if(isExprFullyInterpretedOnView(sexpr, aMdlCtx.view()))
      {
        continue;
      }
      if (auto isexpr = iutils::interpretExpr(sexpr, aMdlCtx.view()))
      {
        // At least one value is interpreted
        changed = true;
        sexpr = *isexpr;

        if(!isExprFullyInterpretedOnView(sexpr, aMdlCtx.view()))
        {
          // After interpretation, the argument is still not fully interpreted
          fullyInterpreted = false;
        }
      }
      else
      {
        // Set interpretation flag as "not fully interpreted"
        fullyInterpreted = false;
      }
    }//for

		if (!changed)
		{
      // Nothing changed but now the constraint may be fully interpreted.
      // For example, the constraint can have only variables in its scope:
      // nq(x, y)
      // which, at first, tag it as non-interpreted.
      // Those variables may be declared before the call to this interpreter.
      // The scope of this constraint doesn't change even if now the constraint
      // is fully interpreted
			return { changed, fullyInterpreted };
		}

		// At least one value changed during interpretation
		ModelContext::Path p = { MDL_AST_LEAF_CON_SCOPE };
		aCtx.context().deleteSubContext(p);

		for (auto& val : scope)
		{
			if (cutils::isInteger(val))
			{
				aCtx.context().addValueList<INT_64_T>(p, cutils::stoi(val));
			}
			else
			{
				aCtx.context().addValueList<std::string>(p, val);
			}
		}
		
		return { changed, fullyInterpreted };
  }//interpretValues
  
  void CtxInterpreterConstraint::doInterpretation()
  {
    runInterpreterFcn("interpret_scope");
  }//doInterpretation
  
}// end namespace Model
