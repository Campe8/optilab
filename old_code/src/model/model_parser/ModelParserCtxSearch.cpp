// Base class
#include "ModelParserCtxSearch.hpp"

// AST Macros
#include "ModelMacro.hpp"

// Utilities
#include "InterpreterUtils.hpp"

#include <cassert>

namespace iutils = Model::Utils::Interpreter;

namespace Model {
  
#ifdef OLD_CODE
  static IR::SymbolTable::SymbolTableEntrySPtr lookupSymbolEntryOnModules(const std::vector<IR::IRModuleSPtr>& aMods, const IR::IRSymbol::SymbolName& aSymb)
  {
    IR::SymbolTable::SymbolTableEntrySPtr symbEntry(nullptr);
    for (auto& m : aMods)
    {
      symbEntry = lookupSymbol(m, aSymb);
      if (symbEntry)
      {
        return symbEntry;
      }
    }
    return symbEntry;
  }//getSymbolEntryFromModule
  
  // Returns the var expression for "aVarID" from the variable module "aMod".
  // Returns nullptr if no variable expression is present
  static std::shared_ptr<IR::IRExprVar> getVarExprFromID(const std::string aVarID, const IR::IRModuleSPtr& aMod)
  {
    // Look in variable module for each "aVarID"
    // get all the leaves since their symbol tables are brothers and they do not
    // share symbols although they share the parents.
    auto modSet = getLeaves(aMod);
    assert(!modSet.empty());
    
    // Lookup for module containing the given symbol
    auto symbEntry = lookupSymbolEntryOnModules(modSet, aVarID);
    if (!symbEntry) { return nullptr; }
    
    // Get the (unique) expr containing the symbol
    assert((symbEntry->getLinkSet()).size() == 1);
    auto varExpr = *((symbEntry->getLinkSet()).begin());
    
    assert(IR::IRExprVar::isa(varExpr.get()));
    return std::static_pointer_cast<IR::IRExprVar>(varExpr);
  }//getVarExprFromID
  
  // Returns the variable in the array indexed by the given subscription
  static std::shared_ptr<IR::IRExprVar> getVarExprFromSubcript(const std::string aSubscript, const IR::IRModuleSPtr& aMod)
  {
    auto subscriptPair = iutils::getSubscriptMatrixAndIndex(aSubscript);
    assert(!subscriptPair.first.empty());
    assert(!subscriptPair.second.empty());
    
    // Get matrix expression, it should be:
    // - a reference to a matrix which will be subscripted
    // - a variable of type array
    auto matrixExpr = getVarExprFromID(subscriptPair.first, aMod);
    assert(IR::IRTypeArray::isa((matrixExpr->getType()).get()));
    
    // Get matrix subscription should be an integer or another matrix subscription
    //auto matrixSub = aArg.substr(openPar+1, closePar - openPar - 1);
    //auto subscriptExpr = getExprFromArgument(matrixSub, aMod, aCF);
    //assert(IR::IRExprConst::isa(subscriptExpr.get()) || IR::IRExprMatrixSub::isa(subscriptExpr.get()));
    
    //return aCF.exprMatrixSub(matrixExpr, subscriptExpr);
    return nullptr;
  }//getVarExprFromSubcript
  
  static std::vector<IR::IRExprVarSPtr> getBranchingList(const ModelContext& aCtx, const IR::IRModuleSPtr& aMod)
  {
    using IRExprSPtr = IR::IRExprVarSPtr;
    std::vector<IR::IRExprVarSPtr> branchingVars;
    
    // Reference to a symbol, get the expression containing it
    auto ctx = aMod->getContext();
    auto mdlName = IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_VARIABLE);
    auto varModule = ctx->globalModule()->getSubModule(mdlName);
    assert(varModule);
    
    // For each branching variable, get its (IR) expression
    auto branchingList = aCtx.context().getValueList<std::string>({ MDL_AST_LEAF_SEARCH_BRANCHING_SET });
    for (auto& var : branchingList)
    {
      IRExprSPtr branchingVar(nullptr);
      
      auto varType = iutils::getExprType(var);
      switch (varType)
      {
        case iutils::ExprType::AT_SUBSCRIPT:
        {
          branchingVar = getVarExprFromSubcript(var, varModule);
        }
          break;
        case iutils::ExprType::AT_ARRAY:
        {
          //exprArg = getIRConstExprArray(aArg, aMod, aCF);
          //assert(exprArg);
        }
        default:
        {
          assert(varType == iutils::ExprType::AT_ID);
          branchingVar = getVarExprFromID(var, varModule);
        }
      }// switch
      
      assert(branchingVar);
      branchingVars.push_back(branchingVar);
    }// for
    
    return branchingVars;
  }//getBranchingList
  
  static void parseSearchType(const ModelContext& aCtx, const IR::IRModuleSPtr& aMod, const IR::IRSrcDeclSPtr& aSrcDecl)
  {
    assert(aSrcDecl);
    assert(!aCtx.isEmpty());
    
    // Get variable ID and create the corresponding symbol variable
    std::string type = aCtx.context().getValue<std::string>({ MDL_AST_LEAF_SEARCH_TYPE });
    if (type == MDL_AST_LEAF_SEARCH_TYPE_ALGO_DFS)
    {
      aSrcDecl->setSearchType(IR::IRSrcDecl::SearchType::ST_DFS);
    }
    else
    {
      aSrcDecl->setSearchType(IR::IRSrcDecl::SearchType::ST_UNDEF);
    }
  }//parseSearchType
  
  static void parseBranchingVarSet(const ModelContext& aCtx, const IR::IRModuleSPtr& aMod, const IR::IRSrcDeclSPtr& aSrcDecl)
  {
    assert(aSrcDecl);
    assert(!aCtx.isEmpty());
    
    // Get branching list if any
    auto branchingList = getBranchingList(aCtx, aMod);
    if (!branchingList.empty())
    {
      aSrcDecl->setBranchingVarList(branchingList);
    }
  }//parseBranchingVarSet
  
  static void parseSearchSemantic(const ModelContext& aCtx, const IR::IRModuleSPtr& aMod, const IR::IRSrcDeclSPtr& aSrcDecl)
  {
    assert(aSrcDecl);
    assert(!aCtx.isEmpty());
    
    // Parse semantics
    auto smCtx = aCtx.context().getSubContext({ MDL_AST_LEAF_SEARCH_SEMANTIC });
    assert(!smCtx.isEmpty());
    
    IR::IRSrcDecl::VarChoice vrchoice = IR::IRSrcDecl::VarChoice::VRC_UNDEF;
    auto varChoice = smCtx.getValue<std::string>({ MDL_AST_LEAF_SEARCH_SEMANTIC_VAR_CHOICE });
    if (varChoice == MDL_AST_LEAF_SEARCH_SEMANTIC_VAR_INPUT_ORDER) { vrchoice = IR::IRSrcDecl::VarChoice::VRC_INPUT_ORDER; }
    else if (varChoice == MDL_AST_LEAF_SEARCH_SEMANTIC_VAR_FIRST_FAIL) { vrchoice = IR::IRSrcDecl::VarChoice::VRC_FIRST_FAIL; }
    else if (varChoice == MDL_AST_LEAF_SEARCH_SEMANTIC_VAR_LARGEST) { vrchoice = IR::IRSrcDecl::VarChoice::VRC_LARGEST; }
    else if (varChoice == MDL_AST_LEAF_SEARCH_SEMANTIC_VAR_SMALLEST) { vrchoice = IR::IRSrcDecl::VarChoice::VRC_SMALLEST; }
    else if (varChoice == MDL_AST_LEAF_SEARCH_SEMANTIC_VAR_ANTI_FIRST_FAIL) { vrchoice = IR::IRSrcDecl::VarChoice::VRC_ANTI_FIRST_FAIL; }
    
    aSrcDecl->setVariableChoice(vrchoice);
    
    IR::IRSrcDecl::ValChoice vlchoice = IR::IRSrcDecl::ValChoice::VLC_UNDEF;
    auto valChoice = smCtx.getValue<std::string>({ MDL_AST_LEAF_SEARCH_SEMANTIC_VAL_CHOICE });
    if (valChoice == MDL_AST_LEAF_SEARCH_SEMANTIC_VAL_INDOMAIN_MAX) { vlchoice = IR::IRSrcDecl::ValChoice::VLC_INDOMAIN_MAX; }
    else if (valChoice == MDL_AST_LEAF_SEARCH_SEMANTIC_VAL_INDOMAIN_MIN) { vlchoice = IR::IRSrcDecl::ValChoice::VLC_INDOMAIN_MIN; }
    else if (valChoice == MDL_AST_LEAF_SEARCH_SEMANTIC_VAL_INDOMAIN_RANDOM) { vlchoice = IR::IRSrcDecl::ValChoice::VLC_INDOMAIN_RND; }
    
    aSrcDecl->setValueChoice(vlchoice);
  }//parseSearchSemantic
  
  static void parseNumSolutions(const ModelContext& aCtx, const IR::IRModuleSPtr& aMod)
  {
    assert(!aCtx.isEmpty());
    
    // Get number of solutions
    auto nums = aCtx.context().getValue<std::size_t>({ MDL_AST_LEAF_SEARCH_NUM_SOLUTIONS });
    
    // Set number of solutions
    assert(aMod->getContext());
    assert(aMod->getContext()->getContextSpec());
    aMod->getContext()->getContextSpec()->setSolutionLimit(nums);
  }//parseNumSolutions
  
#endif
  
  ModelParserCtxSearch::ModelParserCtxSearch()
  : ModelParserCtx(ModelParserCtxType::MDLPRS_SEARCH)
  {
  }
  
  void ModelParserCtxSearch::parseModelContext(const ModelContext& aCtx, const IR::IRModuleSPtr& aMod)
  {
    /*
    assert(aMod);
    
    // Parse variable sub-context
    auto srcCtx = aCtx.context().getSubContext({ MDL_AST_SUBTREE_SEARCH });
    if (srcCtx.isEmpty())
    {
      MODEL_ABSTRACT_SYNTAX_TREE_ERROR("ModelParser::missing search declaration");
    }

    // Get the leaf variable declaration
    auto srcLeaf = srcCtx.getSubContextList({ MDL_AST_LEAF_SEARCH });
    if (srcLeaf.empty())
    {
      MODEL_ABSTRACT_SYNTAX_TREE_ERROR("ModelParser::missing search declaration - leaf");
    }
    
    // Get search declaration module
    auto srcMdl = aMod->getSubModule(IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_SEARCH));
    assert(srcMdl);
    
    // Parse each leaf node and add a variable declaration node into IR
    for (auto& vCtx : srcLeaf)
    {
      addIRSrcDeclNode(vCtx, srcMdl);
    }*/
  }//parseModelContext
  /*
  void ModelParserCtxSearch::addIRSrcDeclNode(const ModelContext& aCtx, const IR::IRModuleSPtr& aMod)
  {
    assert(aMod);
    assert(!aMod->isGlobalModule());
    
    auto f = getConstructionFacade();
    auto oldBlk = f->setBlock(aMod->getBlock());
    
    auto srcDecl = f->srcDecl();
    assert(srcDecl);
    
    // Set search type
    parseSearchType(aCtx, aMod, srcDecl);
    
    // Set branching var set
    parseBranchingVarSet(aCtx, aMod, srcDecl);
    
    // Set search semantic
    parseSearchSemantic(aCtx, aMod, srcDecl);
    
    // Set number of solutions.
    // @note the solve context carries information about
    // the solution limit and it is set in the IR context
    parseNumSolutions(aCtx, aMod);
    
    aMod->getBlock()->appendStmt(f->stmt(srcDecl));
    
    f->setBlock(oldBlk);
  }//addIRSrcDeclNode
  */
}// end namespace Model

