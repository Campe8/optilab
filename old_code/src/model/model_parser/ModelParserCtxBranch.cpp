// Base class
#include "ModelParserCtxBranch.hpp"

#include "BranchContextObject.hpp"
#include "MVCUtils.hpp"

// AST Macros
#include "ModelMacro.hpp"

// Search specific information
#include "DFSInformation.hpp"

// Utilities
#include "ParserUtils.hpp"
#include "InterpreterUtils.hpp"
#include "ObjectHelper.hpp"

#include <cassert>

namespace iutils = Model::Utils::Interpreter;
namespace otools = Interpreter::ObjectTools;

namespace Model {
  
  static IR::SymbolTable::SymbolTableEntrySPtr lookupSymbolEntryOnModules(const std::vector<IR::IRModuleSPtr>& aMods,
                                                                          const IR::IRSymbol::SymbolName& aSymb)
  {
    IR::SymbolTable::SymbolTableEntrySPtr symbEntry(nullptr);
    for (auto& m : aMods)
    {
      symbEntry = lookupSymbol(m, aSymb);
      if (symbEntry)
      {
        return symbEntry;
      }
    }
    return symbEntry;
  }//getSymbolEntryFromModule
  
  // Returns the var expression for "aVarID" from the variable module "aMod".
  // Returns nullptr if no variable expression is present
  static std::shared_ptr<IR::IRExprVar> getVarExprFromID(const std::string aVarID, const IR::IRModuleSPtr& aMod)
  {
    // Look in variable module for each "aVarID"
    // get all the leaves since their symbol tables are brothers and they do not
    // share symbols although they share the parents.
    auto modSet = getLeaves(aMod);
    assert(!modSet.empty());
    
    // Lookup for module containing the given symbol
    auto symbEntry = lookupSymbolEntryOnModules(modSet, aVarID);
    if (!symbEntry) { return nullptr; }
    
    // Get the (unique) expr containing the symbol
    assert((symbEntry->getLinkSet()).size() == 1);
    auto varExpr = *((symbEntry->getLinkSet()).begin());
    
    assert(IR::IRExprVar::isa(varExpr.get()));
    return std::static_pointer_cast<IR::IRExprVar>(varExpr);
  }//getVarExprFromID
  
  static std::vector<IR::IRExprVarSPtr> getBranchingList(const BranchContextObject* aBrc, const IR::IRModuleSPtr& aMod)
  {
    using IRExprSPtr = IR::IRExprVarSPtr;
    std::vector<IR::IRExprVarSPtr> branchingVars;
    
    // Reference to a symbol, get the expression containing it
    auto ctx = aMod->getContext();
    auto mdlName = IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_VARIABLE);
    auto varModule = ctx->globalModule()->getSubModule(mdlName);
    assert(varModule);
    
    // For each branching variable, get its (IR) expression
    const auto& dataObject = aBrc->payload();
    assert(dataObject.composeSize() == 1);
    
    const auto& brcObj = dataObject[0];
    assert(Interpreter::isClassObjectType(brcObj));
    
    auto brcObjPtr = brcObj.getClassObject().get();
    if(otools::isVariableObject(brcObjPtr))
    {
      // Add IR variable ID into the set of branching variables
      branchingVars.push_back(getVarExprFromID(otools::getVarID(brcObjPtr), varModule));
    }
    else
    {
      // The other type argument of the branching constraint must be a list
      assert(otools::isListObject(brcObjPtr));
      for(const auto& var : otools::getListValues(brcObjPtr))
      {
        assert(Interpreter::isClassObjectType(var) && otools::isVariableObject(var.getClassObject().get()));
        branchingVars.push_back(getVarExprFromID(otools::getVarID(var.getClassObject().get()), varModule));
      }
    }
    
    return branchingVars;
  }//getBranchingList
  
  static void parseBranchingSet(const BranchContextObject* aBrc,
                                const IR::IRModuleSPtr& aMod, const IR::IRSrcDeclSPtr& aSrcDecl)
  {
    assert(aBrc && aSrcDecl);
    
    // Get branching list if any
    auto branchingList = getBranchingList(aBrc, aMod);
    if (!branchingList.empty())
    {
      aSrcDecl->setBranchingVarList(branchingList);
    }
  }//parseBranchingVarSet
  
  ModelParserCtxBranch::ModelParserCtxBranch()
  : ModelParserCtx(ModelParserCtxType::MDLPRS_BRANCH)
  {
  }
  
  void ModelParserCtxBranch::parseModelContext(const ModelContext& aCtx, const IR::IRModuleSPtr& aMod)
  {
    assert(aMod);
    
    // Get branch declaration module
    auto brcMdl = aMod->getSubModule(IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_SEARCH));
    assert(brcMdl);
    
    // Parse each leaf node and add a branch declaration node into IR
    if(aCtx.getContextObjectVectorByType(ContextObject::Type::CTX_OBJ_BRC).empty())
    {
      // No branch object, create a default one
      addDefaultIRBranchingDeclNode(brcMdl);
    }
    else
    {
      for (auto& brcObj : aCtx.getContextObjectVectorByType(ContextObject::Type::CTX_OBJ_BRC))
      {
        addIRBranchingDeclNode(brcObj, brcMdl);
      }
    }
  }//parseModelContext
  
  void ModelParserCtxBranch::addDefaultIRBranchingDeclNode(const IR::IRModuleSPtr& aMod)
  {
    assert(aMod && !aMod->isGlobalModule());
    
    auto f = getConstructionFacade();
    auto oldBlk = f->setBlock(aMod->getBlock());
    
    std::string ID;
    ID + MDL_AST_LEAF_BRC_KEY_PREFIX + "1";
    
    auto srcDecl = f->srcDecl(ID);
    assert(srcDecl);
    
    // Set search type: default is DFS
    srcDecl->setSearchType(IR::IRSrcDecl::SearchType::ST_DFS);
    
    // Create a new search information instance with default DFS values
    auto srcInfo = std::make_shared<DFSInformation>();
    
    srcInfo->setVariableChoice(DFSInformation::VariableSelectionChoiceList[0]);
    srcInfo->setValueChoice(DFSInformation::ValueSelectionChoiceList[0]);
    srcDecl->setSearchInformation(srcInfo);
    
    aMod->getBlock()->appendStmt(f->stmt(srcDecl));
    
    f->setBlock(oldBlk);
  }//addIRBranchingDeclNode
  
  void ModelParserCtxBranch::addIRBranchingDeclNode(const ModelContext::CtxObjSPtr& aBrc, const IR::IRModuleSPtr& aMod)
  {
    assert(aMod && !aMod->isGlobalModule());
    
    auto f = getConstructionFacade();
    auto oldBlk = f->setBlock(aMod->getBlock());
    
    assert(aBrc && aBrc->getType() == ContextObject::Type::CTX_OBJ_BRC);
    auto brc = static_cast<const BranchContextObject*>(aBrc.get());
    
    auto ID = brc->getID();
    assert(!ID.empty());
    
    auto srcDecl = f->srcDecl(ID);
    assert(srcDecl);
    
    // Set search type: default is DFS
    srcDecl->setSearchType(IR::IRSrcDecl::SearchType::ST_DFS);
    
    // Set branching var set
    parseBranchingSet(brc, aMod, srcDecl);
    
    // Set search heuristic
    srcDecl->setSearchType(IR::IRSrcDecl::SearchType::ST_DFS);
    
    auto dfsInfo = std::make_shared<Model::DFSInformation>();
    dfsInfo->setVariableChoice(DFSInformation::VariableSelectionChoiceList[0]);
    dfsInfo->setValueChoice(DFSInformation::ValueSelectionChoiceList[0]);
    srcDecl->setSearchInformation(dfsInfo);
    
    aMod->getBlock()->appendStmt(f->stmt(srcDecl));
    
    f->setBlock(oldBlk);
  }//addIRBranchingDeclNode
  
}// end namespace Model
