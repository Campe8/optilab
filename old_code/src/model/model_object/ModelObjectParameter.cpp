// Self first
#include "ModelObjectParameter.hpp"

#include "ConstraintParameterDescriptor.hpp"
#include "ConstraintParameterDescriptorLeaf.hpp"

#include "Model.hpp"
#include "VariableArray.hpp"
#include "ConstraintParameterVariable.hpp"
#include "ConstraintParameterDomain.hpp"
#include "ConstraintParameterDomainArray.hpp"

#include <cassert>

namespace Model {
  
  static std::vector<Core::DomainSPtr> getDomainFromParamDescVar(const ParamLeaf* aLeafDescriptor, const std::shared_ptr<Model>& aMdl)
  {
    auto vID = static_cast<const ParamVarID*>(aLeafDescriptor)->getID();
    auto vobj = getModelObjectBySID(aMdl.get(), vID);
    assert(ModelObjectVariable::isa(vobj));
    
    std::vector<Core::DomainSPtr> domList;
    for(auto& dom : *(ModelObjectVariable::cast(vobj)->variable()->domainList()))
    {
      domList.push_back(dom);
    }
    return domList;
  }//getDomainFromParamDescVar
  
  static std::vector<Core::DomainSPtr> getDomainFromParamDescBool(const ParamLeaf* aLeafDescriptor, const Core::ConstraintParameterFactory& aCPF)
  {
    auto val = static_cast<const ParamBool*>(aLeafDescriptor)->getValue();
    
    return { std::shared_ptr<Core::Domain>(aCPF.domainFactory()->domainBoolean(val)) };
  }//getDomainFromParamDescBool
  
  static std::vector<Core::DomainSPtr> getDomainFromParamDescRefID(const ParamLeaf* aLeafDescriptor, const std::shared_ptr<Model>& aMdl)
  {
    auto vID = static_cast<const ParamRefID*>(aLeafDescriptor)->getID();
    auto pobj = getModelObjectBySID(aMdl.get(), vID);
    assert(ModelObjectParameter::isa(pobj));
    
    // Switch on the reference parameter and retrieve its domains
    auto mdlObjParam = ModelObjectParameter::cast(pobj);
    auto conParam = mdlObjParam->parameter();
    auto conParamType = conParam->getParameterClass();
    
    std::vector<Core::DomainSPtr> domArray;
    switch (conParamType)
    {
      case Core::ConstraintParameter::ConstraintParameterClass::CON_PARAM_CLASS_VARIABLE:
      {
        for(auto& dom : *(Core::ConstraintParameterVariable::cast(conParam.get())->getVariable()->domainList()))
        {
          domArray.push_back(dom);
        }
      }
        break;
      case Core::ConstraintParameter::ConstraintParameterClass::CON_PARAM_CLASS_DOMAIN:
      {
        domArray.push_back(Core::ConstraintParameterDomain::cast(conParam.get())->getDomain());
      }
        break;
      default:
        assert(conParamType == Core::ConstraintParameter::ConstraintParameterClass::CON_PARAM_CLASS_DOMAIN_ARRAY);
      {
        auto dArray = Core::ConstraintParameterDomainArray::cast(conParam.get());
        const auto& domains = *(dArray->getDomainArray().get());
        for(std::size_t idx{0}; idx < domains.size(); ++idx)
        {
          domArray.push_back(domains[idx]);
        }
      }
        break;
    }
    
    return domArray;
  }//getDomainFromParamDescRefID
  
  static std::vector<Core::DomainSPtr> getDomainFromParamDescSubscript(const ParamLeaf* aLeafDescriptor,  const std::shared_ptr<Model>& aMdl)
  {
    auto vID = static_cast<const ParamVarSubscript*>(aLeafDescriptor)->getVariableID();
    auto sub = static_cast<const ParamVarSubscript*>(aLeafDescriptor)->getSubscript();
    
    auto vobj = getModelObjectBySID(aMdl.get(), vID);
    assert(ModelObjectVariable::isa(vobj));
    
    auto var = (ModelObjectVariable::cast(vobj)->variable()).get();
    assert(Core::VariableArray::isa(var));
    assert(sub < Core::VariableArray::cast(var)->size());
    
    std::vector<Core::DomainSPtr> domList;
    for(auto& dom : *(Core::VariableArray::cast(var)->at(sub)->domainList()))
    {
      domList.push_back(dom);
    }
    return domList;
  }//getDomainFromParamDescSubscript
  
  static std::vector<Core::DomainSPtr> getDomainFromParamDescInt64(const ParamLeaf* aLeafDescriptor, const Core::ConstraintParameterFactory& aCPF)
  {
    auto val = static_cast<const ParamInt64*>(aLeafDescriptor)->getValue();
    return { std::shared_ptr<Core::Domain>(aCPF.domainFactory()->domainInteger(val)) };
  }//getDomainFromParamDescBool
  
  static std::vector<Core::DomainSPtr> getDomainFromDescriptor(const ParamLeaf* aLeafDescriptor,
                                                               const std::shared_ptr<Model>& aMdl,
                                                               const Core::ConstraintParameterFactory& aCPFactory)
  {
    assert(aLeafDescriptor);
    auto type = aLeafDescriptor->getParamType();
    switch (type)
    {
      case ConstraintParameterDescriptor::ParameterType::PT_VAR_ID:
        return getDomainFromParamDescVar(aLeafDescriptor, aMdl);
      case ConstraintParameterDescriptor::ParameterType::PT_BOOL:
        return getDomainFromParamDescBool(aLeafDescriptor, aCPFactory);
      case ConstraintParameterDescriptor::ParameterType::PT_REF_ID:
        return getDomainFromParamDescRefID(aLeafDescriptor, aMdl);
      case ConstraintParameterDescriptor::ParameterType::PT_VAR_SUBSCRIPT:
        return getDomainFromParamDescSubscript(aLeafDescriptor, aMdl);
      default:
        assert(type == ConstraintParameterDescriptor::ParameterType::PT_INT64);
        return getDomainFromParamDescInt64(aLeafDescriptor, aCPFactory);
    }//switch
  }//getDomainFromDescriptor
  
  ModelObjectParameter::ModelObjectParameter(const ModelObjectDescriptorParameterSPtr& aDP,
                                             const std::shared_ptr<Model>& aMdl,
                                             const std::shared_ptr<Core::ConstraintParameterFactory>& aCPFactory)
  : ModelObject(ModelObjectClass::MODEL_OBJ_PARAMETER)
  {
    assert(aDP);
    assert(aMdl);
    assert(aCPFactory);
    pID = aDP->getID();
    pParameter = createParameter(aDP, aMdl, aCPFactory);
  }
  
  bool ModelObjectParameter::isa(const ModelObject* aModelObject)
  {
    return aModelObject &&
    aModelObject->getModelObjectClass() == ModelObjectClass::MODEL_OBJ_PARAMETER;
  }//isa
  
  ModelObjectParameter* ModelObjectParameter::cast(ModelObject* aModelObject)
  {
    if (!isa(aModelObject))
    {
      return nullptr;
    }
    
    return static_cast<ModelObjectParameter*>(aModelObject);
  }//cast
  
  const ModelObjectParameter* ModelObjectParameter::cast(const ModelObject* aModelObject)
  {
    if (!isa(aModelObject))
    {
      return nullptr;
    }
    
    return static_cast<const ModelObjectParameter*>(aModelObject);
  }//cast
  
  ModelObjectParameter::ParameterSPtr ModelObjectParameter::createScalarParameter(ScalarParameterDescriptor* aScalar,
                                                                                  const std::shared_ptr<Model>& aMdl,
                                                                                  const std::shared_ptr<Core::ConstraintParameterFactory>& aCPFactory)
  {
    assert(aScalar);
    
    const auto& value = aScalar->getValue();
    assert(!value->isComposite());
    
    // Get the constraint leaf descriptor
    auto leafParamDescriptor = static_cast<const ParamLeaf*>(value.get());
    
    /// Get the domain representing the leaf descriptor
    auto domain = getDomainFromDescriptor(leafParamDescriptor, aMdl, *(aCPFactory.get()));
    
    /// Returns the constraint parameter domain
    return aCPFactory->constraintParameterDomain(domain);
  }//createScalarParameter
  
  ModelObjectParameter::ParameterSPtr ModelObjectParameter::createMatrixParameter(MatrixParameterDescriptor* aMatrix,
                                                                                  const std::shared_ptr<Model>& aMdl,
                                                                                  const std::shared_ptr<Core::ConstraintParameterFactory>& aCPFactory)
  {
    assert(aMatrix);
    
    // Iterate over all parameters descriptors and generate the correspondent domain object
    assert(aMatrix->numRows() == 1);
    
    // Array of domains used to create the parameter descriptor
    std::vector<Core::DomainSPtr> domainArray;
    
    // Array of boolean flags indicating whether the correspondent
    // domain in "domainArray" is a subject or not
    std::vector<bool> subjectArray;
    
    // Reserve size for the vectors
    domainArray.reserve(aMatrix->numCols());
    subjectArray.reserve(aMatrix->numCols());
    
    // Iterate over the matrix and get the domains that will generate
    // the constraint parameter
    for(std::size_t colIdx{0}; colIdx < aMatrix->numCols(); ++colIdx)
    {
      // Composite parametes not allowed
      const auto& paramDescriptor = aMatrix->getElement(0, colIdx);
      assert(!paramDescriptor->isComposite());
      
      // Get the leaf parameter
      auto leafParamDescriptor = static_cast<const ParamLeaf*>(paramDescriptor.get());
      
      // If the leaf parameter is a variable, set it as subject
      if (leafParamDescriptor->getParamType() == ConstraintParameterDescriptor::ParameterType::PT_VAR_ID)
      {
        subjectArray.push_back(true);
      }
      else
      {
        subjectArray.push_back(false);
      }
      
      for(auto& dom : getDomainFromDescriptor(leafParamDescriptor, aMdl, *(aCPFactory.get())))
      {
        domainArray.push_back( dom );
      }
    }//for
    
    // Create the constraint parameter from the array
    auto constraintParameter = aCPFactory->constraintParameterDomain(domainArray);
    
    // Set the subjects as specified in the array of subject flags
    auto arrayParameter = Core::ConstraintParameterDomainArray::cast(constraintParameter.get());
    assert(arrayParameter->size() == subjectArray.size());
    
    for(std::size_t idx{0}; idx < arrayParameter->size(); ++idx)
    {
      if(subjectArray[idx]) arrayParameter->setAsSubjectGivenIndex(idx);
    }
    
    return constraintParameter;
  }//createMatrixParameter
  
  ModelObjectParameter::ParameterSPtr ModelObjectParameter::createParameter(const ModelObjectDescriptorParameterSPtr& aDP,
                                                                            const std::shared_ptr<Model>& aMdl,
                                                                            const std::shared_ptr<Core::ConstraintParameterFactory>& aCPFactory)
  {
    auto paramType = aDP->getParameterType();
    switch (paramType) {
      case ModelObjectDescriptorParameter::ParameterType::PT_MATRIX:
        return createMatrixParameter(static_cast<MatrixParameterDescriptor*>(aDP.get()), aMdl, aCPFactory);
      default:
        assert(paramType == ModelObjectDescriptorParameter::ParameterType::PT_SCALAR);
        return createScalarParameter(static_cast<ScalarParameterDescriptor*>(aDP.get()), aMdl, aCPFactory);
    }
  }//createParameter
  
}// end namespace Search
