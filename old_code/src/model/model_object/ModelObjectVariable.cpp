// Base class
#include "ModelObjectVariable.hpp"

// Domain descriptors
#include"ModelObjectDescriptorVariableDomainInteger.hpp"
#include"ModelObjectDescriptorVariableDomainBoolean.hpp"

#include <unordered_set>
#include <cassert>

namespace Model {
  
  ModelObjectVariable::ModelObjectVariable(const ModelObjectDescriptorVariableSPtr& aDescriptorVariable, Core::VariableFactoryPtr aVariableFactory)
  : ModelObject(ModelObjectClass::MODEL_OBJ_VARIABLE)
  {
    pVariable = createVariableOnDescriptor(aDescriptorVariable.get(), aVariableFactory);
  }
  
  Core::VariableSPtr ModelObjectVariable::createVariableOnDescriptor(ModelObjectDescriptorVariable* aVar, Core::VariableFactoryPtr aVF)
  {
    assert(aVar);
    assert(aVF);
    if(aVar->isComposite())
    {
      return createCompositeTypeVariable(aVar, aVF);
    }
    return createPrimitiveTypeVariable(aVar, aVF);
  }//createVariableOnDescriptor
  
  Core::VariableSPtr ModelObjectVariable::createPrimitiveVariableInteger(ModelObjectDescriptorVariableDomain* aModelObjectDomainDescriptor,
                                                                         Core::VariableSemanticUPtr aVariableSemantic,
                                                                         Core::VariableFactoryPtr aVariableFactory)
  {
    assert(aModelObjectDomainDescriptor);
    assert(aVariableSemantic);
    
    assert(ModelObjectDescriptorVariableDomainInteger::isa(aModelObjectDomainDescriptor));
    ModelObjectDescriptorVariableDomainInteger* domDescInt = ModelObjectDescriptorVariableDomainInteger::cast(aModelObjectDomainDescriptor);
    
    assert(domDescInt);
    if (domDescInt->isSingleton())
    {
      assert(domDescInt->domainConfiguration().size() == 1);
      assert(domDescInt->descriptorVariableDomainConfig() == ModelObjectDescriptorVariableDomain::DescriptorVariableDomainConfig::DOM_CONFIG_SINGLETON);
      return Core::VariableSPtr(aVariableFactory->variableInteger((domDescInt->domainConfiguration())[0], std::move(aVariableSemantic)));
    }
    else if(domDescInt->descriptorVariableDomainConfig() == ModelObjectDescriptorVariableDomain::DescriptorVariableDomainConfig::DOM_CONFIG_BOUNDS)
    {
      assert(domDescInt->domainConfiguration().size() == 2);
      return Core::VariableSPtr(aVariableFactory->variableInteger((domDescInt->domainConfiguration())[0], (domDescInt->domainConfiguration())[1], std::move(aVariableSemantic)));
    }
    
    assert(domDescInt->descriptorVariableDomainConfig() == ModelObjectDescriptorVariableDomain::DescriptorVariableDomainConfig::DOM_CONFIG_SET);
    std::unordered_set<Core::DomainElementInt64 *> setOfElements((domDescInt->domainConfiguration()).begin(), (domDescInt->domainConfiguration()).end());
    
    return Core::VariableSPtr(aVariableFactory->variableInteger(setOfElements, std::move(aVariableSemantic)));
  }//createPrimitiveVariableInteger
  
  Core::VariableSPtr ModelObjectVariable::createPrimitiveVariableBoolean(ModelObjectDescriptorVariableDomain* aModelObjectDomainDescriptor,
                                                                         Core::VariableSemanticUPtr aVariableSemantic,
                                                                         Core::VariableFactoryPtr aVariableFactory)
  {
    assert(aModelObjectDomainDescriptor);
    assert(aVariableSemantic);
    
    assert(ModelObjectDescriptorVariableDomainBoolean::isa(aModelObjectDomainDescriptor));
    ModelObjectDescriptorVariableDomainBoolean* domDescBool = ModelObjectDescriptorVariableDomainBoolean::cast(aModelObjectDomainDescriptor);
    
    assert(domDescBool);
    if (!domDescBool->isSingleton())
    {
      return Core::VariableSPtr(aVariableFactory->variableBoolean(std::move(aVariableSemantic)));
    }
    else
    {
      return Core::VariableSPtr(aVariableFactory->variableBoolean(domDescBool->getBooleanOnSingleton(), std::move(aVariableSemantic)));
    }
  }//createPrimitiveVariableBoolean
  
  Core::VariableSPtr ModelObjectVariable::createPrimitiveTypeVariable(ModelObjectDescriptorVariable* aDescriptorVariable, Core::VariableFactoryPtr aVariableFactory)
  {
    assert(aVariableFactory);
    assert(aDescriptorVariable);
    assert(!aDescriptorVariable->isComposite());
    
    // Get descriptor for primitive variable
    ModelObjectDescriptorPrimitiveVariable* primVarDesc = static_cast<ModelObjectDescriptorPrimitiveVariable*>(aDescriptorVariable);
    
    auto typeClass = primVarDesc->domainDescriptor()->type()->variableTypeClass();
    switch (typeClass)
    {
      case Core::VariableTypeClass::VAR_TYPE_INTEGER:
        return createPrimitiveVariableInteger(primVarDesc->domainDescriptor(), aDescriptorVariable->releaseSemantic(), aVariableFactory);
      default:
        // Boolean type variable -> create a new Boolean variable (singleton or not)
        assert(typeClass == Core::VariableTypeClass::VAR_TYPE_BOOLEAN);
        return createPrimitiveVariableBoolean(primVarDesc->domainDescriptor(), aDescriptorVariable->releaseSemantic(), aVariableFactory);
    }
  }//createPrimitiveTypeVariable
  
  Core::VariableSPtr ModelObjectVariable::createCompositeTypeVariable(ModelObjectDescriptorVariable* aDescriptorVariable, Core::VariableFactoryPtr aVariableFactory)
  {
    assert(aVariableFactory);
    assert(aDescriptorVariable);
    assert(aDescriptorVariable->isComposite());
    
    // Get descriptor for composite variable
    ModelObjectDescriptorCompositeVariable* compVarDesc = static_cast<ModelObjectDescriptorCompositeVariable*>(aDescriptorVariable);
    const Core::DMatrix2D<ModelObjectDescriptorVariable*>* compDesc = compVarDesc->getCompositeDescriptor();
    
    // Create an array or matrix variable
    auto numRows = compDesc->numRows();
    auto numCols = compDesc->numCols();
    assert(numRows > 0);
    
    std::vector<Core::VariableSPtr> arrayVars;
    for(std::size_t row{0}; row < numRows; ++row)
    {
      for(std::size_t col{0}; col < numCols; ++col)
      {
        arrayVars.push_back(createVariableOnDescriptor((*compDesc)[row][col], aVariableFactory));
      }//col
    }//row
    
    // @note the following applies to matrices:
    // std::vector<std::vector<Core::VariableSPtr>> matrixVars;
    // Core::VariableSPtr(aVariableFactory->variableMatrix2D(matrixVars, aDescriptorVariable->releaseSemantic()));
    // For the time being we consider vectorized matrices
    return Core::VariableSPtr(aVariableFactory->variableArray(arrayVars, aDescriptorVariable->releaseSemantic()));
  }//createCompositeTypeVariable
  
}// end namespace Search
