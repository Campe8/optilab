// Base class
#include "ModelObjectSearchSemantic.hpp"

#include <cassert>

namespace Model {
  
  ModelObjectSearchSemantic::ModelObjectSearchSemantic(const ModelObjectDescriptorSearchSemanticSPtr& aDescriptorSearchSemantic)
  : ModelObject(ModelObjectClass::MODEL_OBJ_SEARCH_SEMANTIC)
  {
    assert(aDescriptorSearchSemantic);
    
    pID = aDescriptorSearchSemantic->getSearchSemanticID();
    pSearchStrategtType = aDescriptorSearchSemantic->searchStrategyType();
    pHeuristicSemantic.first = aDescriptorSearchSemantic->variableChoiceMetricType();
    pHeuristicSemantic.second = aDescriptorSearchSemantic->valueChoiceMetricType();
  }
  
  bool ModelObjectSearchSemantic::isa(const ModelObject* aModelObject)
  {
    return aModelObject &&
    aModelObject->getModelObjectClass() == ModelObjectClass::MODEL_OBJ_SEARCH_SEMANTIC;
  }
  
  ModelObjectSearchSemantic* ModelObjectSearchSemantic::cast(ModelObject* aModelObject)
  {
    if (!isa(aModelObject))
    {
      return nullptr;
    }
    
    return static_cast<ModelObjectSearchSemantic*>(aModelObject);
  }
  
  const ModelObjectSearchSemantic* ModelObjectSearchSemantic::cast(const ModelObject* aModelObject)
  {
    if (!isa(aModelObject))
    {
      return nullptr;
    }
    
    return static_cast<const ModelObjectSearchSemantic*>(aModelObject);
  }
  
}// end namespace Search
