// Base class
#include "ModelObjectConstraint.hpp"

#include "ConstraintFactory.hpp"

#include <cassert>

namespace Model {
  
  ModelObjectConstraint::ModelObjectConstraint(const ModelObjectDescriptorConstraintSPtr& aDesc, Core::ConstraintFactoryPtr aCF)
  : ModelObject(ModelObjectClass::MODEL_OBJ_CONSTRAINT)
  , pConstraint(nullptr)
  , pConstraintFactory(aCF)
  , pID(aDesc->constraintID())
  , pCtxID(aDesc->constraintCtxID())
  , pStrategyType(aDesc->strategyType())
  {
  }
  
  ModelObjectConstraint::ConstraintSPtr ModelObjectConstraint::constraint() const
  {
    /// Lazy initialization
    if(!pConstraint)
    {
      pConstraint = getConstraint();
      
      // Free unused memory
      cleanupConstraintCreationResources();
    }
    
    return pConstraint;
  }//constraint
  
  ModelObjectConstraint::ConstraintSPtr ModelObjectConstraint::getConstraint() const
  {
    auto c = pConstraintFactory->constraint(pID, pScope, pStrategyType);
    assert(c);
    
    return std::shared_ptr<Core::Constraint>(c);
  }// createConstraintOnDescriptor
  
  void ModelObjectConstraint::cleanupConstraintCreationResources() const
  {
    pScope.clear();
  }//cleanupConstraintCreationResources
  
}// end namespace Search
