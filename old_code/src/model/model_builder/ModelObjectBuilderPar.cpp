// Self first
#include "ModelObjectBuilderPar.hpp"
#include "ModelObjectParameter.hpp"

#include <cassert>

namespace Model {
  
  ModelObjectBuilderPar::ModelObjectBuilderPar()
  : ModelObjectBuilder(BuilderPriority::BP_PAR)
  , pParamFactory(nullptr)
  {
    pParamFactory = std::make_shared<Core::ConstraintParameterFactory>();
  }
  
  void ModelObjectBuilderPar::expandModel(const ModelSPtr& aMdl, const std::shared_ptr<ModelGeneratorContext>& aCtx)
  {
    for(auto& d : getDescriptors(aCtx, ModelObjectDescriptorClass::MODEL_OBJ_DESCRIPTOR_PARAMETER))
    {
      aMdl->addModelObject(buildObjPar(std::dynamic_pointer_cast<ModelObjectDescriptorParameter>(d), aMdl));
    }
    
  }//expandModel

  ModelObjectUPtr ModelObjectBuilderPar::buildObjPar(const std::shared_ptr<ModelObjectDescriptorParameter>& aPDesc, const ModelSPtr& aMdl)
  {
    return ModelObjectUPtr(new ModelObjectParameter(aPDesc, aMdl, pParamFactory));
  }//buildObjPar
  
}// end namespace Model
