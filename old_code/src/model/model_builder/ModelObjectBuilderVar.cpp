// Self first
#include "ModelObjectBuilderVar.hpp"
#include "ModelObjectVariable.hpp"

#include <cassert>

namespace Model {
  
  ModelObjectBuilderVar::ModelObjectBuilderVar()
  : ModelObjectBuilder(BuilderPriority::BP_VAR)
  , pVarFactory(new Core::VariableFactory())
  {
  }
  
  void ModelObjectBuilderVar::expandModel(const ModelSPtr& aMdl, const std::shared_ptr<ModelGeneratorContext>& aCtx)
  {
    for(auto& d : getDescriptors(aCtx, ModelObjectDescriptorClass::MODEL_OBJ_DESCRIPTOR_VARIABLE))
    {
      aMdl->addModelObject(buildObjVar(std::dynamic_pointer_cast<ModelObjectDescriptorVariable>(d)));
    }
    
  }//expandModel

  ModelObjectUPtr ModelObjectBuilderVar::buildObjVar(const ModelObjectDescriptorVariableSPtr& aDVar)
  {
    return ModelObjectUPtr(new ModelObjectVariable(aDVar, pVarFactory.get()));
  }//modelObjectVar
  
}// end namespace Model
