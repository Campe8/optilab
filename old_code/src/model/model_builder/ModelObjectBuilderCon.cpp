// Self first
#include "ModelObjectBuilderCon.hpp"
#include "ModelObjectConstraint.hpp"

#include <cassert>

namespace Model {
  
  ModelObjectBuilderCon::ModelObjectBuilderCon()
  : ModelObjectBuilder(BuilderPriority::BP_CON)
  , pConFactory(nullptr)
  {
    pConFactory = std::make_shared<Core::ConstraintFactory>();
  }
  
  void ModelObjectBuilderCon::expandModel(const ModelSPtr& aMdl, const std::shared_ptr<ModelGeneratorContext>& aCtx)
  {
    for(auto& d : getDescriptors(aCtx, ModelObjectDescriptorClass::MODEL_OBJ_DESCRIPTOR_CONSTRAINT))
    {
      aMdl->addModelObject(buildObjCon(std::dynamic_pointer_cast<ModelObjectDescriptorConstraint>(d), aMdl));
    }
    
  }//expandModel

  ModelObjectUPtr ModelObjectBuilderCon::buildObjCon(const ModelObjectDescriptorConstraintSPtr& aDCon, const ModelSPtr& aMdl)
  {
    assert(aMdl);
    
    auto obj = new ModelObjectConstraint(aDCon, pConFactory.get());
    
    // Add constraint parameters
    for(const auto& param : aDCon->getScope(aMdl))
    {
      obj->addConstraintParameter(param);
    }
    
    return ModelObjectUPtr(obj);
  }//buildObjCon
  
}// end namespace Model
