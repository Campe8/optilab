// Self first
#include "ModelObjectBuilder.hpp"

#include <cassert>

namespace Model {
  
  ModelObjectBuilder::ModelObjectBuilder(BuilderPriority aPriority)
  : pPriority(aPriority)
  {
  }
  
  ModelObjectBuilder::DescSet ModelObjectBuilder::getDescriptors(const DescCtxSPtr& aCtx, DescClass aClass)
  {
    return aCtx->getModelObjectDescriptorSet(aClass);
  }//getDescriptors
  
}// end namespace Model
