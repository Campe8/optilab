// Self first
#include "ModelObjectBuilderSrc.hpp"

#include "ModelObjectSearchSemantic.hpp"

#include <cassert>

namespace Model {
  
  ModelObjectBuilderSrc::ModelObjectBuilderSrc()
  : ModelObjectBuilder(BuilderPriority::BP_SRC)
  {
  }
  
  void ModelObjectBuilderSrc::expandModel(const ModelSPtr& aMdl, const std::shared_ptr<ModelGeneratorContext>& aCtx)
  {
    for(auto& d : getDescriptors(aCtx, ModelObjectDescriptorClass::MODEL_OBJ_DESCRIPTOR_SEARCH_SEMANTIC))
    {
      aMdl->addModelObject(buildObjSrc(aCtx, std::dynamic_pointer_cast<ModelObjectDescriptorSearchSemantic>(d)));
    }
    
  }//expandModel

  ModelObjectUPtr ModelObjectBuilderSrc::buildObjSrc(const std::shared_ptr<ModelGeneratorContext>& aCtx,
                                                     const ModelObjectDescriptorSearchSemanticSPtr& aDSrc)
  {
    ModelObjectSearchSemantic::SemanticList semanticList;
    for(auto& vID : aDSrc->getSemanticallyDependentVarList())
    {
      auto vdesc = std::dynamic_pointer_cast<ModelObjectDescriptorVariable>(aCtx->getObjectDescriptorByID(vID));
      assert(vdesc);

      for(auto& semantic : vdesc->getSemanticList())
      {
        if(semantic.first == aDSrc->getSearchSemanticID())
        {
          semanticList.push_back({vID, semantic.second});
          break;
        }
      }
    }
    auto objss = new ModelObjectSearchSemantic(aDSrc);
    objss->setSemanticList(semanticList);
    
    return ModelObjectUPtr(objss);
  }//buildObjSrc
  
}// end namespace Model
