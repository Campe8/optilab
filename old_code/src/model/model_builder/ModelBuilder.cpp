// Self first
#include "ModelBuilder.hpp"

// Builders
#include "ModelObjectBuilderVar.hpp"
#include "ModelObjectBuilderCon.hpp"
#include "ModelObjectBuilderPar.hpp"
#include "ModelObjectBuilderSrc.hpp"

#include <cassert>

namespace Model {
  
  static bool allBuildersRegistered(const std::map<int, std::shared_ptr<ModelObjectBuilder>>& aMap)
  {
    for(int bld = 0; bld < static_cast<int>(BuilderPriority::BP_LAST); ++bld)
    {
      if(aMap.find(bld) == aMap.end())
      {
        return false;
      }
    }
    return true;
  }//allBuildersRegistered
  
  ModelBuilder::ModelBuilder()
  : pModelGenerator(new ModelGenerator())
  {
    registerBuilders();
    
    // Verify that all builders have been registered
    auto reg = allBuildersRegistered(pBuilderMap);
    assert(reg);
  }
  
  void ModelBuilder::registerBuilders()
  {
    Builder bvar = std::make_shared<ModelObjectBuilderVar>();
    pBuilderMap[bvar->getPriority()] = bvar;
    
    Builder bpar = std::make_shared<ModelObjectBuilderPar>();
    pBuilderMap[bpar->getPriority()] = bpar;
    
    Builder bcon = std::make_shared<ModelObjectBuilderCon>();
    pBuilderMap[bcon->getPriority()] = bcon;
    
    Builder bsrc = std::make_shared<ModelObjectBuilderSrc>();
    pBuilderMap[bsrc->getPriority()] = bsrc;
  }//registerBuilders
  
	ModelSPtr ModelBuilder::buildModel(const IR::IRContextSPtr& aIRCtx)
	{
		assert(aIRCtx);

		// Create meta information to generate the model
    auto modelMetaInfo = std::make_shared<ModelMetaInfo>(aIRCtx->getContextSpec()->getModelName(),
                                                         aIRCtx->getContextSpec()->getSolutionLimit());

		// Create a model generator context
		auto genCtx = pModelGenerator->generateContext(modelMetaInfo, aIRCtx);
		assert(genCtx);

		auto model = genCtx->getModel();
    assert(model);
    
    // Generate a model object for each model descriptor.
    // Each object is added to the model.
    // Model objects have dependencies on each other so
    // they must be generated in a topological order:
    // 1) object variables
    // 2) object parameters
    // 2) object constraints
    // 3) object search semantic
    // The above order must be preserved by the builder priorities
    for(auto it = pBuilderMap.begin(); it != pBuilderMap.end(); ++it)
    {
      it->second->expandModel(model, genCtx);
    }
    
    return model;
	}//buildModel
  
}// end namespace Model
