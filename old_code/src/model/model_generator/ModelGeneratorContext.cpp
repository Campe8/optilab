// Self first
#include "ModelGeneratorContext.hpp"

#include "ModelDefs.hpp"

namespace Model {
  
  void ModelGeneratorContext::createModel(const ModelMetaInfoSPtr& aModelMetaInfo)
  {
    pModel = std::shared_ptr<Model>(new Model(aModelMetaInfo));
  }//setModel
  
  void ModelGeneratorContext::addModelObjectDescriptor(const ModelObjectDescriptorSPtr& aObjDesc)
  {
    assert(aObjDesc);
    auto objClass = aObjDesc->getModelObjectDescriptorClass();
    switch (objClass)
    {
      case ModelObjectDescriptorClass::MODEL_OBJ_DESCRIPTOR_VARIABLE:
        addObjDescVar(std::static_pointer_cast<ModelObjectDescriptorVariable>(aObjDesc));
        break;
      case ModelObjectDescriptorClass::MODEL_OBJ_DESCRIPTOR_PARAMETER:
        addObjDescPar(std::static_pointer_cast<ModelObjectDescriptorParameter>(aObjDesc));
        break;
      case ModelObjectDescriptorClass::MODEL_OBJ_DESCRIPTOR_CONSTRAINT:
        addObjDescCon(std::static_pointer_cast<ModelObjectDescriptorConstraint>(aObjDesc));
        break;
      default:
        assert(objClass == ModelObjectDescriptorClass::MODEL_OBJ_DESCRIPTOR_SEARCH_SEMANTIC);
        addObjDescSrc(std::static_pointer_cast<ModelObjectDescriptorSearchSemantic>(aObjDesc));
        break;
    }
  }//addModelObjectDescriptor
  
  void ModelGeneratorContext::addObjDescVar(const std::shared_ptr<ModelObjectDescriptorVariable>& aObjDesc)
  {
    assert(aObjDesc);
    assert(aObjDesc->getDefaultSemantic());
    pObjDescMap[modelObjectDescriptorClassToInt(ModelObjectDescriptorClass::MODEL_OBJ_DESCRIPTOR_VARIABLE)].insert(aObjDesc);
    pIDToObjDescMap[aObjDesc->getDefaultSemantic()->getVariableNameIdentifier()] = aObjDesc;
  }//addObjDescVar
  
  void ModelGeneratorContext::addObjDescPar(const std::shared_ptr<ModelObjectDescriptorParameter>& aObjDesc)
  {
    assert(aObjDesc);
    pObjDescMap[modelObjectDescriptorClassToInt(ModelObjectDescriptorClass::MODEL_OBJ_DESCRIPTOR_PARAMETER)].insert(aObjDesc);
    pIDToObjDescMap[aObjDesc->getID()] = aObjDesc;
  }//addObjDescPar
  
  void ModelGeneratorContext::addObjDescCon(const std::shared_ptr<ModelObjectDescriptorConstraint>& aObjDesc)
  {
    assert(aObjDesc);
    pObjDescMap[modelObjectDescriptorClassToInt(ModelObjectDescriptorClass::MODEL_OBJ_DESCRIPTOR_CONSTRAINT)].insert(aObjDesc);
  }//addObjDescCon
  
  void ModelGeneratorContext::addObjDescSrc(const std::shared_ptr<ModelObjectDescriptorSearchSemantic>& aObjDesc)
  {
    assert(aObjDesc);
    pObjDescMap[modelObjectDescriptorClassToInt(ModelObjectDescriptorClass::MODEL_OBJ_DESCRIPTOR_SEARCH_SEMANTIC)].insert(aObjDesc);
    if(!aObjDesc->getSearchSemanticID().empty())
    {
      pIDToObjDescMap[aObjDesc->getSearchSemanticID()] = aObjDesc;
    }
  }//addObjDescSrc
  
  ModelObjectDescriptorSPtr ModelGeneratorContext::getObjectDescriptorByID(const std::string& aID) const
  {
    if(pIDToObjDescMap.find(aID) == pIDToObjDescMap.end()) return nullptr;
    return pIDToObjDescMap.at(aID);
  }//getObjectDescriptorByID
  
  ModelGeneratorContext::ObjDescSet ModelGeneratorContext::getModelObjectDescriptorSet(ModelObjectDescriptorClass aClass) const
  {
    auto mdclass = modelObjectDescriptorClassToInt(aClass);
    if(pObjDescMap.find(mdclass) == pObjDescMap.end())
    {
      return ObjDescSet();
    }
    
    return pObjDescMap.at(mdclass);
  }//getModelObjectDescriptorSet
  
}// end namespace Model
