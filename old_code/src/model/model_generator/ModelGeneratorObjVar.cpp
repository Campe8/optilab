// Self first
#include "ModelGeneratorObjVar.hpp"

#include "ModelObjectDescriptorVariable.hpp"
#include "ModelObjectDescriptorVariableDomainBoolean.hpp"
#include "ModelObjectDescriptorVariableDomainInteger.hpp"

// IR
#include "IRVarDecl.hpp"
#include "IRTypeInteger.hpp"
#include "IRTypeBoolean.hpp"
#include "IRTypeArray.hpp"
#include "IRDomDeclBounds.hpp"
#include "IRDomDeclExt.hpp"
#include "IRSrcDecl.hpp"

// Semantics
#include "VariableSemanticDecision.hpp"
#include "VariableSemanticSupport.hpp"
#include "VariableSemanticObjective.hpp"

// Domain
#include "ModelObjectDescriptorVariableDomain.hpp"

// Utilities
#include "CLIUtils.hpp"

#include <cassert>

namespace Model {

  static Core::VariableSemantic* getBaseSemantic(const std::shared_ptr<IR::IRExprVar>& aVar)
  {
    Core::VariableSemantic* vs{ nullptr };

    auto id = aVar->getVar()->getName();
    auto sType = aVar->getSemantic();
    switch (sType)
    {
      case IR::IRExprVar::VS_DECISION:
        vs = new Core::VariableSemanticDecision(id);
        break;
      case IR::IRExprVar::VS_SUPPORT:
        vs = new Core::VariableSemanticSupport(id);
        break;
      case IR::IRExprVar::VS_OBJECTIVE_MAX:
        vs = new Core::VariableSemanticObjective(Core::VariableSemanticObjective::ObjectiveExtremum::OBJ_EXT_MAXIMIZE, id);
        break;
      default:
        assert(sType == IR::IRExprVar::VS_OBJECTIVE_MIN);
        vs = new Core::VariableSemanticObjective(Core::VariableSemanticObjective::ObjectiveExtremum::OBJ_EXT_MINIMIZE, id);
        break;
    }
    
    // Set force output true or false according to the expr var semantics.
    // If neither OFF nor ON is set, use default output semantics
    if(aVar->getOutput() == IR::IRExprVar::VarOutput::VO_FORCED_OFF)
    {
      vs->forceOutput(false);
    }
    else if(aVar->getOutput() == IR::IRExprVar::VarOutput::VO_FORCED_ON)
    {
      vs->forceOutput(true);
    }
    
    return vs;
  }//getBaseSemantic
  
  static ModelObjectDescriptorVariable::SemanticList getSemanticList(IR::IRVarDecl* aVDecl)
	{
		auto eVar = aVDecl->lhs();
		assert(eVar);
    
    ModelObjectDescriptorVariable::SemanticList semanticList;
    for(auto& ss : *eVar)
    {
      // Create new semantic
      auto semantic = std::shared_ptr<Core::VariableSemantic>(getBaseSemantic(eVar));
      
      // Set search semantic
      semantic->setInputOrder(ss->getInputOrder());
      semantic->setBranchingPolicy(ss->getBranchingPolicy());
      semantic->setReachability(ss->getReachability());
      
      semanticList.push_back({ss->getSearchDeclaration()->getID(), semantic});
    }
    
		return semanticList;
	}//getSemanticList

	static ModelObjectDescriptorVariableDomain* getDomainDeclBounds(IR::IRDomDeclBounds* aDom)
	{
		assert(aDom);
		assert(aDom->getLowerBound());
		assert(aDom->getUpperBound());

		auto lb = aDom->getLowerBound()->getConst();
		auto ub = aDom->getUpperBound()->getConst();
		assert(lb);
		assert(ub);

		if (aDom->isBoolean())
		{
			if (lb->isEqual(ub.get()))
			{
				bool bval  = lb->getName() == "0" ? false : true;
				return new ModelObjectDescriptorVariableDomainBoolean(bval);
			}
			return new ModelObjectDescriptorVariableDomainBoolean();
		}

		assert(CLI::Utils::isInteger(lb->getName()));
		assert(CLI::Utils::isInteger(ub->getName()));
		auto vlb = CLI::Utils::stoi(lb->getName());
		auto vub = CLI::Utils::stoi(ub->getName());

		if (lb->isEqual(ub.get()))
		{
			return new ModelObjectDescriptorVariableDomainInteger(static_cast<INT_64>(vlb));
		}
		return new ModelObjectDescriptorVariableDomainInteger(static_cast<INT_64>(vlb), static_cast<INT_64>(vub));
	}//getDomainDeclBounds

	static ModelObjectDescriptorVariableDomain* getDomainDeclExt(IR::IRDomDeclExt* aDom)
	{
		assert(aDom);

		std::unordered_set<INT_64> dset;
		for (auto& c : aDom->getExtensionalDomain())
		{
			assert(c->getConst());
			assert(CLI::Utils::isInteger(c->getConst()->getName()));
			auto val = CLI::Utils::stoi(c->getConst()->getName());
			dset.insert(static_cast<INT_64>(val));
		}

		return new ModelObjectDescriptorVariableDomainInteger(dset);
	}//getDomainDeclExt

  static std::vector<ModelObjectDescriptorVariableDomain*> getDomain(IR::IRVarDecl* aVDecl)
	{
    std::vector<ModelObjectDescriptorVariableDomain*> domainDescriptor;
    for(std::size_t idx{0}; idx < aVDecl->getNumRhs(); ++idx)
    {
      auto eDom = aVDecl->rhs(idx);
      assert(eDom);
      
      auto declType = eDom->getDeclarationType();
      switch (declType)
      {
        case IR::IRDomDecl::DOM_DECL_BOUNDS:
          domainDescriptor.push_back(getDomainDeclBounds(IR::IRDomDeclBounds::cast(eDom.get())));
          break;
        default:
          assert(declType == IR::IRDomDecl::DOM_DECL_EXTENSIONAL);
          domainDescriptor.push_back(getDomainDeclExt(IR::IRDomDeclExt::cast(eDom.get())));
          break;
      }
    }
    return domainDescriptor;
	}//getDomain
  
  static std::vector<std::size_t> getDimensions(IR::IRVarDecl* aVDecl)
  {
    auto eVar = aVDecl->lhs();
    assert(eVar);
    
    auto vtype = (eVar->getType()).get();
    
    std::vector<std::size_t> dims;
    if(!IR::IRTypeArray::isa(vtype)) { return dims; }
    while(IR::IRTypeArray::isa(vtype))
    {
      dims.push_back(IR::IRTypeArray::cast(vtype)->getNumElements());
      vtype = (IR::IRTypeArray::cast(vtype)->getBaseType()).get();
    }
    
    if(dims.size() == 1)
    {
      // If only cols, set number of rows to be 1
      dims.insert(dims.begin(), 1);
    }
    
    return dims;
  }//getDimensions
  
	ModelObjectDescriptorSPtr ModelGeneratorObjVar::generateObjectDescriptor(IR::IRNode* aNode)
	{
    using Semantic = std::unique_ptr<Core::VariableSemantic>;
    using Domain   = std::unique_ptr<ModelObjectDescriptorVariableDomain>;
    using PrimVar  = ModelObjectDescriptorPrimitiveVariable;
    
		assert(IR::IRVarDecl::isa(aNode));
		auto vdecl = IR::IRVarDecl::cast(aNode);		
		
		// Get the list of the semantics of the variable
    // @note the semantic list can be empty, for example for non reachable variables.
    // We need here at least the base semantic
    auto sbase = getBaseSemantic(vdecl->lhs());
		auto slist = getSemanticList(vdecl);

		// Get domain descriptor
		auto vdom = getDomain(vdecl);
		assert(!vdom.empty());
    
    // Get dimensions of the variable
    auto dims = getDimensions(vdecl);
    if(dims.empty())
    {
      // Dims empty: single variable
      assert(vdom.size() == 1);
      auto desc = std::make_shared<PrimVar>(Domain(vdom[0]), Semantic(sbase));
      if(!slist.empty())
      {
        desc->setSemanticList(slist);
      }
      return desc;
    }
    
    // Generate matrix
    assert(dims.size() > 1);
    assert(dims[1] > 0);
    dims[0] = dims[0] > 0 ? dims[0] : 1;
    
    // Set the same semantic for each matrix element
    auto semantic = Semantic(sbase->clone());
    auto vmat = std::make_shared<ModelObjectDescriptorCompositeVariable>(dims[0], dims[1], Semantic(sbase));
    if(!slist.empty())
    {
      vmat->setSemanticList(slist);
    }
    
    assert(vdom.size() == dims[0] * dims[1]);
    for(std::size_t nrow{0}; nrow < dims[0]; ++nrow)
    {
      for(std::size_t ncol{0}; ncol < dims[1]; ++ncol)
      {
        auto domPtr = vdom[dims[1] * nrow + ncol];
        auto semCpy = semantic->clone();
        
        // Create a primitive variable for each element in the matrix
        // and set the base semantic.
        // The proper semantic will be set later from the semantic list according
        // to the running branch statement
        PrimVar* var = new PrimVar(Domain(domPtr), Semantic(semCpy));
        vmat->addDescriptorVariable(std::unique_ptr<PrimVar>(var), nrow, ncol);
      }
    }
  
    return vmat;
	}//generateObjectDescriptor

}// end namespace Model
