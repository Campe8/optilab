// Self first
#include "ModelGeneratorObjSrc.hpp"

// Object descriptor
#include "ModelObjectDescriptorSearchSemantic.hpp"

// IR
#include "IRBlock.hpp"
#include "IRModule.hpp"
#include "IRContext.hpp"
#include "IRSrcDecl.hpp"
#include "IRVarDecl.hpp"

// Utilities
#include "CLIUtils.hpp"
#include "ModelUtils.hpp"

#include <cassert>

namespace Model {
  
  static Search::ValueChoiceMetricType getValueChoice(IR::IRSrcDecl* aDecl)
  {
    assert(aDecl);
    auto vtype = aDecl->getValueChoice();
    switch (vtype)
    {
      case IR::IRSrcDecl::ValChoice::VLC_INDOMAIN_MAX:
        return Search::ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MAX;
      case IR::IRSrcDecl::ValChoice::VLC_INDOMAIN_MIN:
        return Search::ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MIN;
      default:
        assert(vtype == IR::IRSrcDecl::ValChoice::VLC_INDOMAIN_RND);
        return Search::ValueChoiceMetricType::VAL_METRIC_INDOMAIN_RANDOM;
    }
  }//getVariableChoice
  
  static Search::VariableChoiceMetricType getVariableChoice(IR::IRSrcDecl* aDecl)
  {
    assert(aDecl);
    auto vtype = aDecl->getVariableChoice();
    switch (vtype)
    {
      case IR::IRSrcDecl::VarChoice::VRC_FIRST_FAIL:
        return Search::VariableChoiceMetricType::VAR_CM_FIRST_FAIL;
      case IR::IRSrcDecl::VarChoice::VRC_INPUT_ORDER:
        return Search::VariableChoiceMetricType::VAR_CM_INPUT_ORDER;
      case IR::IRSrcDecl::VarChoice::VRC_SMALLEST:
        return Search::VariableChoiceMetricType::VAR_CM_SMALLEST;
      case IR::IRSrcDecl::VarChoice::VRC_LARGEST:
        return Search::VariableChoiceMetricType::VAR_CM_LARGEST;
      default:
        assert(vtype == IR::IRSrcDecl::VarChoice::VRC_ANTI_FIRST_FAIL);
        return Search::VariableChoiceMetricType::VAR_CM_ANTI_FIRST_FAIL;
    }
  }//getVariableChoice

  static Search::SearchEngineStrategyType getSearchType(IR::IRSrcDecl* aDecl)
  {
    assert(aDecl);
    auto stype = aDecl->getSearchType();
    switch (stype)
    {
      default:
        assert(stype == IR::IRSrcDecl::SearchType::ST_DFS);
        return Search::SearchEngineStrategyType::SE_DEPTH_FIRST_SEARCH;
        break;
    }
  }//getSearchType

  ModelObjectDescriptorSPtr ModelGeneratorObjSrc::generateObjectDescriptor(IR::IRNode* aNode)
  {
    assert(IR::IRSrcDecl::isa(aNode));
    auto srcdecl = IR::IRSrcDecl::cast(aNode);
    assert(srcdecl);
    
    // Get search type
    auto searchType = getSearchType(srcdecl);
    
    // Get variable choice heuristic
    auto varChoice = getVariableChoice(srcdecl);
    
    // Get value choice heuristic
    auto valChoice = getValueChoice(srcdecl);
    
    // Create an object descriptor
    auto ssDesc = std::make_shared<ModelObjectDescriptorSearchSemantic>(searchType, varChoice, valChoice);
    
    // Set the ids of the variables which are semantically dependent on the search semantic
    ssDesc->setSemanticallyDependentVarList(srcdecl->getSemanticDependentVarList());

    // Set search semantic ID
    ssDesc->setSearchSemanticID(srcdecl->getID());
    
    return ssDesc;
  }//generateObjectDescriptor
  
}// end namespace Model
