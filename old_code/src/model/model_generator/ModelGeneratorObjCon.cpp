// Self first
#include "ModelGeneratorObjCon.hpp"

// Object descriptor
#include "ModelObjectDescriptorConstraint.hpp"
#include "ConstraintParameterDescriptorLeaf.hpp"
#include "ConstraintParameterDescriptorComposite.hpp"

// IR
#include "IRConDecl.hpp"
#include "IRExprVar.hpp"
#include "IRExprConst.hpp"
#include "IRExprRef.hpp"
#include "IRExprMatrixSub.hpp"
#include "IRTypeArray.hpp"
#include "IRSymbolConstArray.hpp"
#include "IRSymbolConstInteger.hpp"
#include "IRSymbolConstBoolean.hpp"
#include "IRSymbolVar.hpp"
#include "IRSymbolFactory.hpp"

// Utilities
#include "CLIUtils.hpp"
#include "ModelUtils.hpp"

#include <cassert>

namespace Model {
  // Returns the parameter descriptor corresponding for the given IR expression
  static ParamComp::ParamDescSPtr getConstraintParameterDescriptor(const IR::IRExprFcn::IRExprSPtr& aExpr);
  
  static Core::ConstraintId getConstraintType(IR::IRConDecl* aDecl)
  {
    assert(aDecl->fcnCon());
    assert(aDecl->fcnCon()->getFcn());
    auto cID = aDecl->fcnCon()->getFcn()->getName();
    return Utils::ConstraintHelper::getInstance().stringIDToCoreID(cID);
  }//getConstraintType
  
  static Core::PropagatorStrategyType getPropagatorType(IR::IRConDecl* aDecl)
  {
    auto pType = aDecl->getPropagationType();
    switch (pType)
    {
      case IR::IRConDecl::PropagationType::PT_BOUNDS:
        return Core::PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND;
      default:
        assert(pType == IR::IRConDecl::PropagationType::PT_DOMAIN);
        return Core::PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN;
    }
  }//getPropagatorType
  
  static ParamComp::ParamDescSPtr getIntParamDesc(const IR::IRSymbolConstInteger* aSymb)
  {
    assert(aSymb);
    return std::make_shared<ParamInt64>(aSymb->getInt());
  }//getIntParamDesc
  
  static ParamComp::ParamDescSPtr getBoolParamDesc(const IR::IRSymbolConstBoolean* aSymb)
  {
    assert(aSymb);
    return std::make_shared<ParamBool>(aSymb->getBool());
  }//getBoolParamDesc
  
  MODEL_EXPORT_FUNCTION std::shared_ptr<ConstraintParameterDescriptor> getDescriptorFromSymbol(const IR::IRSymbol* aSymb)
  {
    assert(aSymb);
    if (IR::IRSymbolConstInteger::isa(aSymb))
    {
      return getIntParamDesc(IR::IRSymbolConstInteger::cast(aSymb));
    }
    else if (IR::IRSymbolConstBoolean::isa(aSymb))
    {
      return getBoolParamDesc(IR::IRSymbolConstBoolean::cast(aSymb));
    }
    else
    {
      assert(IR::IRSymbolVar::isa(aSymb));
      assert(aSymb->getModule()->getSymbolTable());
      
      // Retrieve the IR expression linked to this symbol
      const auto& entry = aSymb->getModule()->getSymbolTable()->lookup(aSymb->getName());
      
      assert(entry && (entry->getLinkSet()).size() == 1);
      const auto& expr = *((entry->getLinkSet()).begin());
      
      // Recursively create the parameter descriptor
      return getConstraintParameterDescriptor(expr);
    }
  }//getDescriptorFromSymbol
  
  // Utility function: returns a parameter array
  static ParamComp::ParamDescSPtr getParamArrayDesc(IR::IRSymbolConstArray* aArray)
  {
    assert(aArray);
    if(aArray->size() == 0) return nullptr;
    
    // The input array contains IR symbols of variables or subscript variables.
    // This must be taken into account while creating the parameter
    std::vector<ParamComp::ParamDescSPtr> paramArray;
    for (std::size_t idx{ 0 }; idx < aArray->size(); ++idx)
    {
      auto symb = (aArray->getSymbol(idx)).get();
      paramArray.push_back( getDescriptorFromSymbol(symb) );
    }
    
    return std::make_shared<ParamArray>(paramArray);
  }//getParamArrayDesc
  
  static ParamComp::ParamDescSPtr getParameterDescriptorFromSymbol(const IR::IRExprConst::SymbConstSPtr& aSymb)
  {
    assert(aSymb);
    auto sType = aSymb->getType();
    auto sTypeID = sType->getTypeID();
    
    ParamComp::ParamDescSPtr pDesc{ nullptr };
    switch (sTypeID)
    {
      case IR::IRTypeID::IR_TYPE_INTEGER:
        pDesc = getIntParamDesc( IR::IRSymbolConstInteger::cast(aSymb.get()) );
        break;
      case IR::IRTypeID::IR_TYPE_BOOLEAN:
        pDesc = getBoolParamDesc( IR::IRSymbolConstBoolean::cast(aSymb.get()) );
        break;
      default:
        assert(sTypeID == IR::IRTypeID::IR_TYPE_ARRAY);
        pDesc = getParamArrayDesc( IR::IRSymbolConstArray::cast(aSymb.get()) );
        break;
    }
    assert(pDesc);
    
    return pDesc;
  }//getParameterDescriptorFromSymbol
  
  static ParamComp::ParamDescSPtr getParameterDescriptorFromSubscript(IR::IRExprMatrixSub* aSubExpr)
  {
    // Get matrix name
    auto mat = aSubExpr->getMatrix();
    assert(IR::IRExprVar::isa(mat.get()));
    auto var = IR::IRExprVar::cast(mat.get())->getVar()->getName();
    
    // Get subscript
    auto sub = aSubExpr->getSubscript();
    assert(IR::IRExprConst::isa(sub.get()));
    assert(IR::IRSymbolConstInteger::isa((IR::IRExprConst::cast(sub.get())->getConst()).get()));
    auto subint = IR::IRSymbolConstInteger::cast((IR::IRExprConst::cast(sub.get())->getConst()).get())->getInt();
    return std::make_shared<ParamVarSubscript>(var, static_cast<std::size_t>(subint));
  }//getParameterDescriptorFromSubscript
  
  ParamComp::ParamDescSPtr getConstraintParameterDescriptor(const IR::IRExprFcn::IRExprSPtr& aExpr)
  {
    assert(aExpr);
    
    ParamComp::ParamDescSPtr desc(nullptr);
    if (IR::IRExprVar::isa(aExpr.get()))
    {
      // If the expression is a variable expression,
      // create a constraint varID parameter
      auto e = IR::IRExprVar::cast(aExpr.get());
      assert(e && e->getVar());
      
      desc = std::make_shared<ParamVarID>(e->getVar()->getName());
    }
    else if (IR::IRExprRef::isa(aExpr.get()))
    {
      // Reference expressions create a parameter expression
      auto refSymb = getReferencedSymbol(IR::IRExprRef::cast(aExpr.get()));
      assert(refSymb);
      
      desc = std::make_shared<ParamRefID>(refSymb->getName());
    }
    else if (IR::IRExprMatrixSub::isa(aExpr.get()))
    {
      auto sExpr = IR::IRExprMatrixSub::cast(aExpr.get());
      desc = getParameterDescriptorFromSubscript(sExpr);
    }
    else
    {
      // All the other parameters are constant parameters,
      // switch the generation according to the const symbol
      assert(IR::IRExprConst::isa(aExpr.get()));
      auto symb = IR::IRExprConst::cast(aExpr.get())->getConst();
      desc = getParameterDescriptorFromSymbol(symb);
    }
    
    assert(desc);
    return desc;
  }//getConstraintParameterDescriptor
  
  static ModelObjectDescriptorConstraint::ScopeDesc getParameterDescriptors(IR::IRConDecl* aDecl)
  {
    ModelObjectDescriptorConstraint::ScopeDesc sDesc;
    auto fcn = aDecl->fcnCon();
    assert(fcn);
    
    for (std::size_t idx{ 0 }; idx < fcn->numArgs(); ++idx)
    {
      auto argExpr = fcn->getArg(idx);
      assert(argExpr);
      
      // Instantiate a parameter descriptor
      // w.r.t. the IRExpr argument of the function
      auto pDesc = getConstraintParameterDescriptor(argExpr);
      sDesc.push_back(pDesc);
    }//for
    
    return sDesc;
  }//getParameterDescriptors
  
  ModelGeneratorObjCon::ModelGeneratorObjCon()
  : pParamFactory(nullptr)
  {
    pParamFactory = std::make_shared<Core::ConstraintParameterFactory>();
  }
  
  ModelObjectDescriptorSPtr ModelGeneratorObjCon::generateObjectDescriptor(IR::IRNode* aNode)
  {
    assert(IR::IRConDecl::isa(aNode));
    auto condecl = IR::IRConDecl::cast(aNode);
    assert(condecl);
    
    // Get constraint type
    auto cType = getConstraintType(condecl);
    
    // Get propagator strategy type
    auto pType = getPropagatorType(condecl);
    
    // Get descriptors for constraint parameters (i.e., the arguments of the constraint)
    auto parDescriptors = getParameterDescriptors(condecl);
    
    // Create an object descriptor
    auto cdesc = std::make_shared<ModelObjectDescriptorConstraint>(cType, pType, parDescriptors, pParamFactory);
    
    // Set the context ID which is carried by the constraint declaration node
    // on its ID if non empty
    auto ctxID = condecl->getID();
    if(!ctxID.empty())
    {
      cdesc->setConstraintCtxID(ctxID);
    }
    
    return cdesc;
  }//generateObjectDescriptor
  
}// end namespace Model
