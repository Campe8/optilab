// Base class
#include "ModelGenerator.hpp"

// Definitions
#include "ModelDefs.hpp"

// IR
#include "IRVarDecl.hpp"
#include "IRConDecl.hpp"
#include "IRSrcDecl.hpp"
#include "IRExprAssign.hpp"

// Model generators
#include "ModelGeneratorObjVar.hpp"
#include "ModelGeneratorObjCon.hpp"
#include "ModelGeneratorObjPar.hpp"
#include "ModelGeneratorObjSrc.hpp"

// Model objects
#include "ModelObjectVariable.hpp"
#include "ModelObjectConstraint.hpp"
#include "ModelObjectSearchSemantic.hpp"

#include <cassert>

namespace Model {
  
  // Returns true if "aMod" is
  // 1 - a global module;
  // 2 - a low-lever IR representation.
  static bool isLowIR(const IR::IRModuleSPtr& aMod)
  {
    assert(aMod);
    if (!aMod->isGlobalModule()) return false;
    if (aMod->numChildren() == 4)
    {
      auto m1 = aMod->getSubModule(IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_VARIABLE));
      auto m2 = aMod->getSubModule(IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_CONSTRAINT));
      auto m3 = aMod->getSubModule(IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_PARAMETER));
      auto m4 = aMod->getSubModule(IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_SEARCH));
      if (m1 && m2 && m3 && m4) return true;
    }
    return false;
  }//isLowIR
  
  ModelGenerator::ModelGenerator()
  {
    fillModelGeneratorReg();
  }
  
  std::shared_ptr<ModelGeneratorObj> ModelGenerator::getGenerator(ModelGeneratorClass aGenClass) const
  {
    auto gclass = modelGeneratorClassToInt(aGenClass);
    return pModelGeneratorReg.at(gclass);
  }//getGenerator
  
  template<typename T>
  void ModelGenerator::generateDescriptorObjects(const IR::IRModuleSPtr& aMod,
                                                 const ModelGeneratorContextSPtr& aCtx,
                                                 ModelGeneratorClass aGenClass)
  {
    assert(aMod && aCtx && aMod->numChildren() == 0);
    auto blk = aMod->getBlock();
    
    assert(blk && !blk->isEmpty());
    for (auto& n : blk->preOrderVisit<T>())
    {
      assert(T::isa(n.get()));
      auto decl = T::cast(n.get());
      aCtx->addModelObjectDescriptor(getGenerator(aGenClass)->generateObjectDescriptor(decl));
    }
  }//generateDescriptorObjects
  
  void ModelGenerator::fillModelGeneratorReg()
  {
    pModelGeneratorReg[modelGeneratorClassToInt(ModelGeneratorClass::MG_VARIABLE)] = std::shared_ptr<ModelGeneratorObjVar>(new ModelGeneratorObjVar());
    pModelGeneratorReg[modelGeneratorClassToInt(ModelGeneratorClass::MG_CONSTRAINT)] = std::shared_ptr<ModelGeneratorObjCon>(new ModelGeneratorObjCon());
    pModelGeneratorReg[modelGeneratorClassToInt(ModelGeneratorClass::MG_PARAMETER)] = std::shared_ptr<ModelGeneratorObjPar>(new ModelGeneratorObjPar());
    pModelGeneratorReg[modelGeneratorClassToInt(ModelGeneratorClass::MG_SEARCH)] = std::shared_ptr<ModelGeneratorObjSrc>(new ModelGeneratorObjSrc());
  }//fillModelGeneratorReg
  
  ModelGenerator::ModelGeneratorContextSPtr ModelGenerator::createModelGeneratorContext(const ModelMetaInfoSPtr& aModelMetaInfo)
  {
    auto ctx = std::shared_ptr<ModelGeneratorContext>(new ModelGeneratorContext());
    ctx->createModel(aModelMetaInfo);
    
    return ctx;
  }//createModelGeneratorContext
  
  ModelGenerator::ModelGeneratorContextSPtr ModelGenerator::generateContext(const ModelMetaInfoSPtr& aModelMetaInfo, const IR::IRContextSPtr& aCtx)
  {
    assert(aModelMetaInfo);
    assert(aCtx);
    
    // Create a model generator context
    auto ct = createModelGeneratorContext(aModelMetaInfo);
    
    // Walk the global module and create model object descriptors
    auto glbModule = aCtx->globalModule();
    assert(glbModule);
    assert(isLowIR(glbModule));
    
    // Create variable descriptor objects.
    // @note generate variable descriptor objects if there are variable declaration stmts
    //       in the module.
    //       Otherwise proceed with next sub-module
    auto vMod = glbModule->getSubModule(IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_VARIABLE));
    if(!IR::hasEmptyBlock(vMod))
    {
      generateDescriptorObjects<IR::IRVarDecl>(vMod, ct, MG_VARIABLE);
    }
    
    // Create constraint descriptor objects.
    // @note generate constraint descriptor objects if there are constraint declaration stmts
    //       in the module.
    //       Otherwise proceed with next sub-module
    auto cMod = glbModule->getSubModule(IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_CONSTRAINT));
    if (!IR::hasEmptyBlock(cMod))
    {
      generateDescriptorObjects<IR::IRConDecl>(cMod, ct, MG_CONSTRAINT);
    }
    
    // Create parameter descriptor objects.
    // @note generate parameter descriptor objects if there are parameter declaration stmts
    //       in the module.
    //       Otherwise proceed with next sub-module
    auto pMod = glbModule->getSubModule(IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_PARAMETER));
    if(!IR::hasEmptyBlock(pMod))
    {
      // All parameters should be assignment expressions between a lhs ID of the parameter
      // and the correspondent rhs expression
      generateDescriptorObjects<IR::IRExprAssign>(pMod, ct, MG_PARAMETER);
    }
    
    // Creare search descriptor objects
    auto sMod = glbModule->getSubModule(IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_SEARCH));
    generateDescriptorObjects<IR::IRSrcDecl>(sMod, ct, MG_SEARCH);
    
    return ct;
  }//generateContext
  
}// end namespace Model

template void Model::ModelGenerator::generateDescriptorObjects<IR::IRVarDecl>(const IR::IRModuleSPtr&,
                                                                              const ModelGeneratorContextSPtr&,
                                                                              ModelGeneratorClass);
template void Model::ModelGenerator::generateDescriptorObjects<IR::IRConDecl>(const IR::IRModuleSPtr&,
                                                                              const ModelGeneratorContextSPtr&,
                                                                              ModelGeneratorClass);
template void Model::ModelGenerator::generateDescriptorObjects<IR::IRExprConst>(const IR::IRModuleSPtr&,
                                                                                const ModelGeneratorContextSPtr&,
                                                                                ModelGeneratorClass);
template void Model::ModelGenerator::generateDescriptorObjects<IR::IRSrcDecl>(const IR::IRModuleSPtr&,
                                                                              const ModelGeneratorContextSPtr&,
                                                                              ModelGeneratorClass);

