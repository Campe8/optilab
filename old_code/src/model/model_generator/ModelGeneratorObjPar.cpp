// Self first
#include "ModelGeneratorObjPar.hpp"

// Object descriptor
#include "ScalarParameterDescriptor.hpp"
#include "MatrixParameterDescriptor.hpp"
#include "ConstraintParameterDescriptorLeaf.hpp"
#include "ModelGeneratorObjCon.hpp"

// IR
#include "IRExprAssign.hpp"
#include "IRExprStdVar.hpp"
#include "IRExprConst.hpp"
#include "IRSymbolConstArray.hpp"
#include "IRSymbolConstInteger.hpp"
#include "IRSymbolConstBoolean.hpp"
#include "IRDefs.hpp"

#include <cassert>

namespace Model {
  
  static ModelObjectDescriptorParameter::BaseType getParameterType(const std::shared_ptr<IR::IRType>& aIRType)
  {
    assert(aIRType);
    auto typeID = aIRType->getTypeID();
    switch (typeID)
    {
      case IR::IRTypeID::IR_TYPE_INTEGER:
        return ModelObjectDescriptorParameter::BaseType::PT_INT64;
      case IR::IRTypeID::IR_TYPE_BOOLEAN:
        return ModelObjectDescriptorParameter::BaseType::PT_BOOL;
      default:
        assert(typeID == IR::IRTypeID::IR_TYPE_VOID);
        return ModelObjectDescriptorParameter::BaseType::PT_VOID;
    }
  }//getParameterType
  
  static MatrixParameterDescriptor::MatrixRow getParameterArray(IR::IRSymbolConstArray* aSymb)
  {
    assert(aSymb);
    
    MatrixParameterDescriptor::MatrixRow row;
    for(std::size_t idx{0}; idx < aSymb->size(); ++idx)
    {
      assert(aSymb->getSymbol(idx));
      auto symb = (aSymb->getSymbol(idx)).get();
      row.push_back( getDescriptorFromSymbol(symb) );
    }
    return row;
  }//getParameterArray
  
  static ScalarParameterDescriptor::ValueDescSPtr getParameterScalar(IR::IRSymbolConstScalar* aSymb)
  {
    assert(aSymb);
    return getDescriptorFromSymbol(aSymb);
  }//getParameterArray
  
  static ModelObjectDescriptorSPtr getArrayDescriptor(const std::string& aID, IR::IRSymbolConstArray* aArray)
  {
    assert(aArray);
    
    // Get parameter base type
    auto pBaseType = getParameterType(aArray->getBaseType());
    
    // Create an object parameter descriptor
    auto pDesc = std::make_shared<MatrixParameterDescriptor>(aID, pBaseType);
    
    // Add rows to the matrix
    pDesc->addRow(getParameterArray(aArray));
    
    return pDesc;
  }//getArrayDescriptor
  
  static ModelObjectDescriptorSPtr getScalarDescriptor(const std::string& aID, IR::IRSymbolConstScalar* aScalar)
  {
    assert(aScalar);
    
    // Get parameter base type
    auto pBaseType = getParameterType(aScalar->getType());
    
    // Create an object parameter descriptor
    auto pDesc = std::make_shared<ScalarParameterDescriptor>(aID, pBaseType);
    
    // Add scalar value to the descriptor
    pDesc->setValue(getParameterScalar(aScalar));
    
    return pDesc;
  }//getScalarDescriptor
  
  static ModelObjectDescriptorSPtr getParameterDescriptor(IR::IRSymbol* lhs, IR::IRSymbol* rhs)
  {
    assert(lhs && IR::IRSymbolVar::isa(lhs));
    assert(rhs && IR::IRSymbolConst::isa(rhs));
    
    auto paramID = IR::IRSymbolVar::cast(lhs)->getName();
    if(IR::IRSymbolConstArray::isa(rhs))
    {
      return getArrayDescriptor(paramID, IR::IRSymbolConstArray::cast(rhs));
    }
    else
    {
      assert(IR::IRSymbolConstScalar::isa(rhs));
      return getScalarDescriptor(paramID, IR::IRSymbolConstScalar::cast(rhs));
    }
  }//getParameterDescriptor
  
  ModelObjectDescriptorSPtr ModelGeneratorObjPar::generateObjectDescriptor(IR::IRNode* aNode)
  {
    assert(IR::IRExprAssign::isa(aNode));
    
    auto assignExpr = IR::IRExprAssign::cast(aNode);
    assert(IR::IRExprStdVar::isa((assignExpr->lhs()).get()));
    assert(IR::IRExprConst::isa((assignExpr->rhs()).get()));
    
    auto lhsSymb = (IR::IRExprStdVar::cast((assignExpr->lhs()).get())->getVar()).get();
    auto rhsSymb = (IR::IRExprConst::cast((assignExpr->rhs()).get())->getConst()).get();
    
    return getParameterDescriptor(lhsSymb, rhsSymb);
  }//generateObjectDescriptor
  
}// end namespace Model
