// Base class
#include "ScalarParameterDescriptor.hpp"

namespace Model {
  
  ScalarParameterDescriptor::ScalarParameterDescriptor(const std::string& aID, BaseType aBaseType)
  : ModelObjectDescriptorParameter(ModelObjectDescriptorParameter::ParameterType::PT_SCALAR, aID, aBaseType)
  {
  }
  
}// end namespace Search

