// Base class
#include "ModelObjectDescriptorParameter.hpp"

namespace Model {
  
  ModelObjectDescriptorParameter::ModelObjectDescriptorParameter(ParameterType aPType, const std::string& aID, BaseType aBaseType)
  : ModelObjectDescriptor(ModelObjectDescriptorClass::MODEL_OBJ_DESCRIPTOR_PARAMETER)
  , pType(aPType)
  , pID(aID)
  , pBaseType(aBaseType)
  {
  }

}// end namespace Search

