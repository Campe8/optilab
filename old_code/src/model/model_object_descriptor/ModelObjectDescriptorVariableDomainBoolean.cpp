// Base class
#include "ModelObjectDescriptorVariableDomainBoolean.hpp"

namespace Model {

  ModelObjectDescriptorVariableDomainBoolean::ModelObjectDescriptorVariableDomainBoolean()
    : ModelObjectDescriptorVariableDomain(DescriptorVariableDomainConfig::DOM_CONFIG_BOUNDS, Core::VariableTypeClass::VAR_TYPE_BOOLEAN)
    , pBooleanSingleton(boost::logic::indeterminate)
  {
  }

  ModelObjectDescriptorVariableDomainBoolean::ModelObjectDescriptorVariableDomainBoolean(bool aSingleton)
    : ModelObjectDescriptorVariableDomain(DescriptorVariableDomainConfig::DOM_CONFIG_SINGLETON, Core::VariableTypeClass::VAR_TYPE_BOOLEAN)
    , pBooleanSingleton(aSingleton)
  {
  }
  
  ModelObjectDescriptorVariableDomain* ModelObjectDescriptorVariableDomainBoolean::clone()
  {
    return new ModelObjectDescriptorVariableDomainBoolean(*this);
  }//clone

}// end namespace Search

