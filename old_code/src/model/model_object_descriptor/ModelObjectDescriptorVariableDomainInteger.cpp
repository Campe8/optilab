// Base class
#include "ModelObjectDescriptorVariableDomainInteger.hpp"

// Domain element manager
#include "DomainElementManager.hpp"

#include <algorithm>

namespace Model {

  ModelObjectDescriptorVariableDomainInteger::ModelObjectDescriptorVariableDomainInteger(INT_64 aSingleton)
    : ModelObjectDescriptorVariableDomain(DescriptorVariableDomainConfig::DOM_CONFIG_SINGLETON, Core::VariableTypeClass::VAR_TYPE_INTEGER)
  {
    pDomainConfiguration.push_back(Core::DomainElementManager::getInstance().createDomainElementInt64(aSingleton));
  }

  ModelObjectDescriptorVariableDomainInteger::ModelObjectDescriptorVariableDomainInteger(INT_64 aLowerBound, INT_64 aUpperBound)
    : ModelObjectDescriptorVariableDomain(DescriptorVariableDomainConfig::DOM_CONFIG_BOUNDS, Core::VariableTypeClass::VAR_TYPE_INTEGER)
  {
    // Handle case where lower bound == upper bound
    if(aLowerBound == aUpperBound)
    {
      resetDescriptorVariableDomainConfig(DescriptorVariableDomainConfig::DOM_CONFIG_SINGLETON);
      pDomainConfiguration.push_back(Core::DomainElementManager::getInstance().createDomainElementInt64(aLowerBound));
    }
    else
    {
      // Switch bounds if upper is less than lower
      if (aUpperBound < aLowerBound)
      {
        std::swap(aLowerBound, aUpperBound);
      }
      
      pDomainConfiguration.push_back(Core::DomainElementManager::getInstance().createDomainElementInt64(aLowerBound));
      pDomainConfiguration.push_back(Core::DomainElementManager::getInstance().createDomainElementInt64(aUpperBound));
    }
  }

  ModelObjectDescriptorVariableDomainInteger::ModelObjectDescriptorVariableDomainInteger(const std::unordered_set<INT_64>& aSetOfElements)
    : ModelObjectDescriptorVariableDomain(DescriptorVariableDomainConfig::DOM_CONFIG_SET, Core::VariableTypeClass::VAR_TYPE_INTEGER)
  {
    assert(!aSetOfElements.empty());
    
    // Push elements
    for (auto elem : aSetOfElements)
    {
      pDomainConfiguration.push_back(Core::DomainElementManager::getInstance().createDomainElementInt64(elem));
    }
    
    if(aSetOfElements.size() == 1)
    {
      resetDescriptorVariableDomainConfig(DescriptorVariableDomainConfig::DOM_CONFIG_SINGLETON);
    }
    else if(aSetOfElements.size() == 2)
    {
      resetDescriptorVariableDomainConfig(DescriptorVariableDomainConfig::DOM_CONFIG_BOUNDS);
      if(pDomainConfiguration[1]->isLessThan(pDomainConfiguration[0]))
      {
        std::iter_swap(pDomainConfiguration.begin(), pDomainConfiguration.begin() + 1);
      }
    }
  }

  ModelObjectDescriptorVariableDomain* ModelObjectDescriptorVariableDomainInteger::clone()
  {
    return new ModelObjectDescriptorVariableDomainInteger(*this);
  }//clone
  
}// end namespace Search

