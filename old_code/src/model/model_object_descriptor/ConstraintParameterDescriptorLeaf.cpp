// Base class
#include "ConstraintParameterDescriptorLeaf.hpp"

#include <cassert>

namespace Model {

	ParamInt64::ParamInt64(INT_64 aScalarInt)
  : ParamLeaf(ConstraintParameterDescriptor::ParameterType::PT_INT64)
  , pValue(aScalarInt)
  {
  }
  
  ParamBool::ParamBool(BOOL_T aBool)
  : ParamLeaf(ConstraintParameterDescriptor::ParameterType::PT_BOOL)
  , pValue(aBool)
  {
  }
  
  ParamID::ParamID(const std::string& aID, ConstraintParameterDescriptor::ParameterType aParamType)
  : ParamLeaf(aParamType)
  , pID(aID)
  {
  }
  
	ParamVarID::ParamVarID(const std::string& aID)
  : ParamID(aID, ConstraintParameterDescriptor::ParameterType::PT_VAR_ID)
  {
  }
  
  ParamRefID::ParamRefID(const std::string& aID)
  : ParamID(aID, ConstraintParameterDescriptor::ParameterType::PT_REF_ID)
  {
  }
  
  ParamVarSubscript::ParamVarSubscript(const std::string& aVarID, std::size_t aSubscript)
  : ParamLeaf(ConstraintParameterDescriptor::ParameterType::PT_VAR_SUBSCRIPT)
  , pVarID(aVarID)
  , pSubscript(aSubscript)
  {
  }
  
}// end namespace Model
