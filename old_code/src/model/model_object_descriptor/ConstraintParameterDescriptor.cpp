// Self first
#include "ConstraintParameterDescriptor.hpp"

#include <cassert>

namespace Model {
  
	ParamLeaf::ParamLeaf(ConstraintParameterDescriptor::ParameterType aType)
	: pParamType(aType)
	{
	}

  ParamComp::ParamComp(ParamCompType aPType)
  : pParamType(aPType)
  {
  }
  
	ParamComp::ChildrenParamIter ParamComp::begin()
	{
		return pChildren.begin();
	}

	ParamComp::ChildrenParamCIter ParamComp::begin() const
	{
		return pChildren.cbegin();
	}

	ParamComp::ChildrenParamIter ParamComp::end()
	{
		return pChildren.end();
	}

	ParamComp::ChildrenParamCIter ParamComp::end() const
	{
		return pChildren.cend();
	}
  
}// end namespace Model
