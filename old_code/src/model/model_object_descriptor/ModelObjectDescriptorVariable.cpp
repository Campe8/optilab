// Base class
#include "ModelObjectDescriptorVariable.hpp"

namespace Model {
  
  ModelObjectDescriptorVariable::ModelObjectDescriptorVariable(Semantic aSemantic)
  : ModelObjectDescriptor(ModelObjectDescriptorClass::MODEL_OBJ_DESCRIPTOR_VARIABLE)
  , pVariableSemantic(std::move(aSemantic))
  {
  }
  
  ModelObjectDescriptorPrimitiveVariable::ModelObjectDescriptorPrimitiveVariable(Domain aDomain, Semantic aSemantic)
  : ModelObjectDescriptorVariable(std::move(aSemantic))
  , pDomain(std::move(aDomain))
  {
  }
  
  ModelObjectDescriptorCompositeVariable::ModelObjectDescriptorCompositeVariable(std::size_t aRows, std::size_t aCols, Core::VariableSemanticUPtr aVariableSemantic)
  : ModelObjectDescriptorVariable(std::move(aVariableSemantic))
  , pCompositeVariable(aRows, aCols)
  {
  }
  
  ModelObjectDescriptorCompositeVariable::~ModelObjectDescriptorCompositeVariable()
  {
    for (std::size_t idxRow{0}; idxRow < pCompositeVariable.numRows(); ++idxRow)
    {
      for (std::size_t idxCol{0}; idxCol < pCompositeVariable.numCols(); ++idxCol)
      {
        delete pCompositeVariable[idxRow][idxCol];
      }
    }
    pCompositeVariable.clear();
  }
  
  void ModelObjectDescriptorCompositeVariable::addDescriptorVariable(ModelObjectDescriptorVariableUPtr aVariable, std::size_t aRow, std::size_t aCol)
  {
    assert(aVariable);
    pCompositeVariable.assignElementToCell(aRow, aCol, aVariable.release());
  }//addDescriptorVariable
  
}// end namespace Search
