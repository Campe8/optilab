// Base class
#include "ConstraintParameterDescriptorComposite.hpp"

// Leaf class
#include "ConstraintParameterDescriptorLeaf.hpp"

#include <cassert>

namespace Model {
  
  ParamArray::ParamArray(const std::vector<INT_64_T>& aVec)
  : ParamComp(ParamComp::ParamCompType::PCT_ARRAY)
  {
    addParametersToCompositeObject<INT_64_T, ParamInt64>(aVec);
  }
  
  ParamArray::ParamArray(const std::vector<BOOL_T>& aVec)
  : ParamComp(ParamComp::ParamCompType::PCT_ARRAY)
  {
    addParametersToCompositeObject<BOOL_T, ParamBool>(aVec);
  }
  
  ParamArray::ParamArray(const std::vector<std::string>& aVec)
  : ParamComp(ParamComp::ParamCompType::PCT_ARRAY)
  {
    addParametersToCompositeObject<std::string, ParamVarID>(aVec);
  }
  
  ParamArray::ParamArray(const std::vector<ParamDescSPtr>& aVec)
  : ParamComp(ParamComp::ParamCompType::PCT_ARRAY)
  {
    for (auto& param : aVec)
    {
      assert(param);
      addParamDescriptor(param);
    }
  }//ParamArray
  
}// end namespace Model

