// Base class
#include "MatrixParameterDescriptor.hpp"

namespace Model {
  
  MatrixParameterDescriptor::MatrixParameterDescriptor(const std::string& aID, BaseType aBaseType)
  : ModelObjectDescriptorParameter(ModelObjectDescriptorParameter::ParameterType::PT_MATRIX, aID, aBaseType)
  {
  }
  
  void MatrixParameterDescriptor::addRow(const MatrixRow& aRow)
  {
    if(pRows.empty())
    {
      pRows.push_back(aRow);
      return;
    }
    assert(pRows[0].size() == aRow.size());
    pRows.push_back(aRow);
  }//addRow
  
}// end namespace Search

