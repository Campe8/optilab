// Base class
#include "ModelObjectDescriptorVariableDomain.hpp"

namespace Model {
  
  ModelObjectDescriptorVariableType::ModelObjectDescriptorVariableType(Core::VariableTypeClass aVariableTypeClass)
  : pVarDescriptorTypeClass(aVariableTypeClass)
  {
  }
  
  bool ModelObjectDescriptorVariableType::isComposite() const
  {
    switch (pVarDescriptorTypeClass)
    {
      case Core::VariableTypeClass::VAR_TYPE_ARRAY:
        return true;
      case Core::VariableTypeClass::VAR_TYPE_BOOLEAN:
        return false;
      case Core::VariableTypeClass::VAR_TYPE_INTEGER:
        return false;
      case Core::VariableTypeClass::VAR_TYPE_REAL:
        return false;
      case Core::VariableTypeClass::VAR_TYPE_MATRIX:
        return true;
      default:
        assert(pVarDescriptorTypeClass == Core::VariableTypeClass::VAR_TYPE_MATRIX2D);
        return true;
    }
  }//isComposite
  
  ModelObjectDescriptorVariableDomain::ModelObjectDescriptorVariableDomain(DescriptorVariableDomainConfig aDomainConfig, Core::VariableTypeClass aClass)
  : pDomainConfig(aDomainConfig)
  , pVarType(new ModelObjectDescriptorVariableType(aClass))
  {
  }
  
}// end namespace Search

