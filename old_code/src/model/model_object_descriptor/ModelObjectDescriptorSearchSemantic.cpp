// Base class
#include "ModelObjectDescriptorSearchSemantic.hpp"

namespace Model {
  
  ModelObjectDescriptorSearchSemantic::ModelObjectDescriptorSearchSemantic(Search::SearchEngineStrategyType aStrategyType,
                                                                           Search::VariableChoiceMetricType aVarMetricType,
                                                                           Search::ValueChoiceMetricType aValeMetricType)
  : ModelObjectDescriptor(ModelObjectDescriptorClass::MODEL_OBJ_DESCRIPTOR_SEARCH_SEMANTIC)
  , pID("")
  , pSearchStrategtType(aStrategyType)
  , pVarChoiceMetric(aVarMetricType)
  , pValChoiceMetric(aValeMetricType)
  {
  }
  
}// end namespace Search
