// Base class
#include "ModelObjectDescriptorConstraint.hpp"

#include "Model.hpp"
#include "ModelObjectParameter.hpp"

#include "VariableArray.hpp"
#include "DomainElementManager.hpp"
#include "DomainElementInt64.hpp"

#include "ConstraintParameterVariable.hpp"

#include "ConstraintParameterDescriptorLeaf.hpp"
#include "ConstraintParameterDescriptorComposite.hpp"

#include "ConstraintParameterDomainArray.hpp"

namespace Model {

  using MODC = ModelObjectDescriptorConstraint;
  
  MODC::ModelObjectDescriptorConstraint(Core::ConstraintId aConType,
                                        Core::PropagatorStrategyType aStrType,
                                        const ScopeDesc& aScopeDesc,
                                        const std::shared_ptr<Core::ConstraintParameterFactory>& aParamFactory)
  : ModelObjectDescriptor(ModelObjectDescriptorClass::MODEL_OBJ_DESCRIPTOR_CONSTRAINT)
  , pConstraintID(aConType)
  , pConstraintCtxID("")
  , pPropagatorStrategyType(aStrType)
  , pScopeDescriptor(aScopeDesc)
  , pParamFactory(aParamFactory)
  {
    assert(pParamFactory);
  }
  
  MODC::TypeAndParameter MODC::getLeafParam(const ConParamDescSPtr& aDesc, const std::shared_ptr<Model>& aModel)
  {
    assert(!aDesc->isComposite());
    
    ConParamSPtr cparam{nullptr};
    auto type = std::static_pointer_cast<ParamLeaf>(aDesc)->getParamType();
    switch (type)
    {
      case ConstraintParameterDescriptor::ParameterType::PT_VAR_ID:
        cparam = paramDescVarID(aDesc, aModel);
        break;
      case ConstraintParameterDescriptor::ParameterType::PT_BOOL:
        cparam = paramDescBool(aDesc);
        break;
      case ConstraintParameterDescriptor::ParameterType::PT_REF_ID:
        cparam = paramDescRefID(aDesc, aModel);
        break;
      case ConstraintParameterDescriptor::ParameterType::PT_VAR_SUBSCRIPT:
        cparam = paramDescSubscript(aDesc, aModel);
        break;
      default:
        assert(type == ConstraintParameterDescriptor::ParameterType::PT_INT64);
        cparam = paramDescInt64(aDesc);
        break;
    }//switch
    assert(cparam);
    
    return {cparam->getParameterClass(), cparam};
  }//getLeafParam
  
  MODC::ConParamSPtr MODC::createCompositeParamFromParamArray(const std::vector<TypeAndParameter>& aParamArry,
                                                              const std::shared_ptr<Model>& aModel)
  {
    using PType = Core::ConstraintParameter::ConstraintParameterClass;
    /*
     * Type of leaf parameters:
     * CON_PARAM_CLASS_VARIABLE
     * CON_PARAM_CLASS_DOMAIN
     * CON_PARAM_CLASS_DOMAIN_ARRAY
     * CON_PARAM_CLASS_DOMAIN_MATRIX_2D
     * The input vector should contain only leaf parameters, i.e.,
     * i.e., only domain or variable parameters.
     */
    
    // Create the domain array that will be the parameter to return
    std::vector<Core::DomainSPtr> domainArray;
    
    // Array of indeces of the domains to set as subject domains
    std::vector<std::size_t> subjectDomains;
    
    // Index for subject domains
    std::size_t idx{0};
    for (auto& typeAndParam : aParamArry)
    {
      if (typeAndParam.first == PType::CON_PARAM_CLASS_VARIABLE)
      {
        // Get the variable from the constraint parameter and, from the variable,
        // retrieve its domain
        assert(Core::ConstraintParameterVariable::isa(typeAndParam.second.get()));
        auto paramVar = Core::ConstraintParameterVariable::cast(typeAndParam.second.get());
        for(auto& dom : *(paramVar->getVariable()->domainList()))
        {
          domainArray.push_back(dom);
          subjectDomains.push_back(idx++);
        }
      }
      else
      {
        assert(typeAndParam.first == PType::CON_PARAM_CLASS_DOMAIN);
        auto paramDom = Core::ConstraintParameterDomain::cast(typeAndParam.second.get());
        domainArray.push_back(paramDom->getDomain());
        idx++;
      }
    }
    
    // Create the constraint parameter on the array of domains
    auto constraintParameterArray = pParamFactory->constraintParameterDomain(domainArray);
    
    // Set the variable domains as subjects for the constraint owning this parameter
    assert(Core::ConstraintParameterDomainArray::isa(constraintParameterArray.get()));
    auto cpda = Core::ConstraintParameterDomainArray::cast(constraintParameterArray.get());
    for(auto domIdx : subjectDomains)
    {
      assert(domIdx < cpda->size());
      cpda->setAsSubjectGivenIndex(domIdx);
    }
    
    return constraintParameterArray;
  }//createCompositeParamFromParamArray
  
  MODC::ConParamSPtr MODC::getCompositeParam(const ConParamDescSPtr& aDesc, const std::shared_ptr<Model>& aModel)
  {
    assert(aDesc->isComposite());
    auto parameterArray = static_cast<ParamComp*>(aDesc.get());
    
    assert(parameterArray->getParamCompType() == ParamComp::ParamCompType::PCT_ARRAY);
    assert(parameterArray->numChildren() > 0);
    
    // For each child in the array, get the correspondent object descriptor
    std::vector<TypeAndParameter> leafParameters;
    for(std::size_t childIdx{0}; childIdx < parameterArray->numChildren(); ++childIdx)
    {
      // Composite children not allowed
      const auto& parameterChild = parameterArray->getChild(childIdx);
      assert(!parameterChild->isComposite());
      
      // Add the leaf parameter to the array of leaves
      leafParameters.push_back(getLeafParam(parameterChild, aModel));
    }
    
    // Create the composite parameter from the array of types and leaf parameters
    return createCompositeParamFromParamArray(leafParameters, aModel);
  }//getCompositeParam
  
  MODC::Scope MODC::getScope(const ModelSPtr& aModel)
  {
    assert(aModel);
    Scope scope;
    
    for (const auto& sd : pScopeDescriptor)
    {
      //who creates this composite parameter descriptor?
      if (sd->isComposite())
      {
        scope.push_back(getCompositeParam(sd, aModel));
      }
      else
      {
        scope.push_back(getLeafParam(sd, aModel).second);
      }
    }//for
    
    return scope;
  }//getScope
  
  MODC::ConParamSPtr MODC::paramDescVarID(const ConParamDescSPtr& aParamDesc, const ModelSPtr& aModel)
  {
    auto vID = static_cast<ParamVarID*>(aParamDesc.get())->getID();
    auto vobj = getModelObjectBySID(aModel.get(), vID);
    assert(ModelObjectVariable::isa(vobj));
    
    return pParamFactory->constraintParameterVariable(ModelObjectVariable::cast(vobj)->variable());
  }//paramDescVarID
  
  MODC::ConParamSPtr MODC::paramDescRefID(const ConParamDescSPtr& aParamDesc, const ModelSPtr& aModel)
  {
    auto refID = static_cast<ParamRefID*>(aParamDesc.get())->getID();
    auto pobj = getModelObjectBySID(aModel.get(), refID);
    assert(ModelObjectParameter::isa(pobj));
    
    // Return the constraint parameter contained in the model object parameter
    return ModelObjectParameter::cast(pobj)->parameter();
  }//paramDescRefID
  
  MODC::ConParamSPtr MODC::paramDescSubscript(const ConParamDescSPtr& aParamDesc, const std::shared_ptr<Model>& aModel)
  {
    auto vID = static_cast<ParamVarSubscript*>(aParamDesc.get())->getVariableID();
    auto sub = static_cast<ParamVarSubscript*>(aParamDesc.get())->getSubscript();
    
    auto vobj = getModelObjectBySID(aModel.get(), vID);
    assert(ModelObjectVariable::isa(vobj));
    
    auto var = (ModelObjectVariable::cast(vobj)->variable()).get();
    assert(Core::VariableArray::isa(var));
    assert(sub < Core::VariableArray::cast(var)->size());
    
    return pParamFactory->constraintParameterVariable(Core::VariableArray::cast(var)->at(sub));
  }//paramDescSubscript
  
  MODC::ConParamSPtr MODC::paramDescInt64(const ConParamDescSPtr& aParamDesc)
  {
    auto val = static_cast<ParamInt64*>(aParamDesc.get())->getValue();
    
    // Create new constraint parameter
    std::shared_ptr<Core::ConstraintParameter> param =
    pParamFactory->constraintParameterDomain(std::shared_ptr<Core::Domain>(pParamFactory->domainFactory()->domainInteger(val)));
    return param;
  }//paramDescInt64
  
  MODC::ConParamSPtr MODC::paramDescBool(const ConParamDescSPtr& aParamDesc)
  {
    auto val = static_cast<ParamBool*>(aParamDesc.get())->getValue();
    
    // Create new constraint parameter
    std::shared_ptr<Core::ConstraintParameter> param =
    pParamFactory->constraintParameterDomain(std::shared_ptr<Core::Domain>(pParamFactory->domainFactory()->domainBoolean(val)));
    return param;
  }//paramDescBool
  
}// end namespace Search

