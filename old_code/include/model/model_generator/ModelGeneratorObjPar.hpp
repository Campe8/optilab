//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 11/24/2017
//

#pragma once

#include "ModelGeneratorObj.hpp"

namespace Model {

	class MODEL_EXPORT_CLASS ModelGeneratorObjPar : public ModelGeneratorObj {
	public:
		ModelObjectDescriptorSPtr generateObjectDescriptor(IR::IRNode* aNode) override;

	protected:
		friend class ModelGenerator;

		/// Protected constructor visible only by ModelGenerator
    ModelGeneratorObjPar() = default;
	};

}// end namespace Model
