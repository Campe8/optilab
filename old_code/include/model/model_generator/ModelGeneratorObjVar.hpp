//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/06/2017
//

#pragma once

#include "ModelGeneratorObj.hpp"

namespace Model {

	class MODEL_EXPORT_CLASS ModelGeneratorObjVar : public ModelGeneratorObj {
	public:
		virtual ~ModelGeneratorObjVar() = default;

		ModelObjectDescriptorSPtr generateObjectDescriptor(IR::IRNode* aNode) override;

	protected:
		friend class ModelGenerator;

		/// Protected constructor visible only by ModelGenerator
		ModelGeneratorObjVar() = default;
	};

}// end namespace Model
