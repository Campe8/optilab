//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/20/2017
//

#pragma once

#include "ModelGeneratorObj.hpp"

namespace Model {

	class MODEL_EXPORT_CLASS ModelGeneratorObjSrc : public ModelGeneratorObj {
	public:
		virtual ~ModelGeneratorObjSrc() = default;

		ModelObjectDescriptorSPtr generateObjectDescriptor(IR::IRNode* aNode) override;

	protected:
		friend class ModelGenerator;

		/// Protected constructor visible only by ModelGenerator
		ModelGeneratorObjSrc() = default;
	};

}// end namespace Model
