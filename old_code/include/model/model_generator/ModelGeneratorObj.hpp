//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/06/2017
//

#pragma once

#include "ModelExportDefs.hpp"

#include "ModelObjectDescriptor.hpp"

// IR
#include "IRNode.hpp"

namespace Model {

	class MODEL_EXPORT_CLASS ModelGeneratorObj {
	public:
		virtual ~ModelGeneratorObj() = default;

		virtual ModelObjectDescriptorSPtr generateObjectDescriptor(IR::IRNode* aNode) = 0;

	protected:
		ModelGeneratorObj() = default;
	};

}// end namespace Model
