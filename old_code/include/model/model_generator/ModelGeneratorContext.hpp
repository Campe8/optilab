//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/05/2017
//
// ModelGeneratorContext is the context used by
// model generator and contains the information
// required to generate model and model objects.
//

#pragma once

#include "ModelExportDefs.hpp"

// Model object descriptors
#include "ModelObjectDescriptorVariable.hpp"
#include "ModelObjectDescriptorParameter.hpp"
#include "ModelObjectDescriptorConstraint.hpp"
#include "ModelObjectDescriptorSearchSemantic.hpp"

#include "Model.hpp"

#include <set>
#include <map>

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelGeneratorContext {
  public:
    using ObjDescSet = std::set<ModelObjectDescriptorSPtr>;
    
  public:
    ModelGeneratorContext() = default;
    
    /// Returns the set of object descriptors of given class
    ObjDescSet getModelObjectDescriptorSet(ModelObjectDescriptorClass aClass) const;
    
    /// Returns the object descriptor of the object which is identified by ID.
    /// For example, an object descriptor for a variable is represented by the variable's ID.
    /// If no such object descriptor can be found, returns nullptr
    ModelObjectDescriptorSPtr getObjectDescriptorByID(const std::string& aID) const;
    
    /// Returns the model hold by this context
    inline ModelSPtr getModel() const
    {
      return pModel;
    }
    
  protected:
    friend class ModelGenerator;
    
    /// Creates a new model based on the information hold by "aInfo"
    void createModel(const ModelMetaInfoSPtr& aModelMetaInfo);
    
    /// Adds an object descriptor to this context
    void addModelObjectDescriptor(const ModelObjectDescriptorSPtr& aObjDesc);
    
  private:
    /// Pointer to the model hold by this context
    ModelSPtr pModel;
    
    /// Map of object descriptors representing the target model
    std::map<int, ObjDescSet> pObjDescMap;
    
    /// Map id to object descriptor
    std::unordered_map<std::string, ModelObjectDescriptorSPtr> pIDToObjDescMap;
    
    /// Utility function: converts descriptor classes into int
    const int modelObjectDescriptorClassToInt(ModelObjectDescriptorClass aClass) const
    {
      return static_cast<int>(aClass);
    }
    
    /// Adds an object descriptor to the set of descriptors
    void addObjDescVar(const std::shared_ptr<ModelObjectDescriptorVariable>& aObjDesc);
    void addObjDescPar(const std::shared_ptr<ModelObjectDescriptorParameter>& aObjDesc);
    void addObjDescCon(const std::shared_ptr<ModelObjectDescriptorConstraint>& aObjDesc);
    void addObjDescSrc(const std::shared_ptr<ModelObjectDescriptorSearchSemantic>& aObjDesc);
  };
  
}// end namespace Model
