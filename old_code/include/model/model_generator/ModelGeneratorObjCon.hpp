//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/15/2017
//

#pragma once

#include "ModelGeneratorObj.hpp"

#include "IRSymbol.hpp"
#include "ConstraintParameterDescriptor.hpp"
#include "ConstraintParameterFactory.hpp"

namespace Model {

	class MODEL_EXPORT_CLASS ModelGeneratorObjCon : public ModelGeneratorObj {
	public:
		ModelObjectDescriptorSPtr generateObjectDescriptor(IR::IRNode* aNode) override;

	protected:
		friend class ModelGenerator;

		/// Protected constructor visible only by ModelGenerator
		ModelGeneratorObjCon();
    
    /// Factory for constraints parameters
    std::shared_ptr<Core::ConstraintParameterFactory> pParamFactory;
	};

  /// Utility function: returns the parameter descriptor for the given IRSymbol
  MODEL_EXPORT_FUNCTION std::shared_ptr<ConstraintParameterDescriptor> getDescriptorFromSymbol(const IR::IRSymbol* aSymb);
  
}// end namespace Model
