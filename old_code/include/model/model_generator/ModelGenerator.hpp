//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 04/11/2017
//
// ModelGenerator generates models and model objects
// given ModelObjectDescriptor(s).
// It represents the link between the output of the parser and
// the solver engine.
//

#pragma once

#include "ModelExportDefs.hpp"

// Generator context
#include "ModelGeneratorContext.hpp"

// IR
#include "IRContext.hpp"

// Model generators
#include "ModelGeneratorObj.hpp"

#include <map>

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelGenerator {
  public:
    typedef std::shared_ptr<ModelGeneratorContext> ModelGeneratorContextSPtr;
    
    virtual ~ModelGenerator() = default;
    
    /// Returns the generator context, i.e., the context containing the object descriptors
    /// used to generate the target model
    ModelGeneratorContextSPtr generateContext(const ModelMetaInfoSPtr& aModelMetaInfo, const IR::IRContextSPtr& aCtx);
    
  protected:
    friend class ModelBuilder;
    
    /// Protected constructor visible only by the ModelBuilder
    ModelGenerator();
    
  private:
    enum ModelGeneratorClass : int {
        MG_VARIABLE = 0
      , MG_CONSTRAINT
      , MG_PARAMETER
      , MG_SEARCH
    };
    
    /// Register of model generator objects
    std::map<int, std::shared_ptr<ModelGeneratorObj>> pModelGeneratorReg;
    
    /// Fills the register of model generator objects
    void fillModelGeneratorReg();
    
    inline int modelGeneratorClassToInt(ModelGeneratorClass aClass) const
    {
      return static_cast<int>(aClass);
    }
    
    /// Returns a new instance of an empty model which meta-information
    /// is described by "aModelMetaInfo"
    ModelGeneratorContextSPtr createModelGeneratorContext(const ModelMetaInfoSPtr& aModelMetaInfo);
    
    /// Returns the generator of class "aGenClass"
    std::shared_ptr<ModelGeneratorObj> getGenerator(ModelGeneratorClass aGenClass) const;
    
    /// Generates the descriptor objects for the type T declaration
    template<typename T>
    void generateDescriptorObjects(const IR::IRModuleSPtr& aMod, const ModelGeneratorContextSPtr& aCtx, ModelGeneratorClass aGenClass);
  };
  
}// end namespace Model
