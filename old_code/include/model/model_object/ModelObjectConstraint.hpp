//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 03/27/2017
//
//

#pragma once

// Base class
#include "ModelObject.hpp"

// Descriptor
#include "ModelObjectDescriptorConstraint.hpp"

#include <vector>

// Forward declarations
namespace Core {
  class Constraint;
  class ConstraintFactory;
}// end namespace Core

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelObjectConstraint : public ModelObject {
  public:
    using ConstraintSPtr = std::shared_ptr<Core::Constraint>;
  
  private:
    using ConstraintFactorySPtr = std::shared_ptr<Core::ConstraintFactory>;
  
  public:
    virtual ~ModelObjectConstraint() = default;
    
    static bool isa(const ModelObject* aModelObject)
    {
      assert(aModelObject);
      return aModelObject->getModelObjectClass() == ModelObjectClass::MODEL_OBJ_CONSTRAINT;
    }
    
    static ModelObjectConstraint* cast(ModelObject* aModelObject)
    {
      if (!isa(aModelObject))
      {
        return nullptr;
      }
      
      return static_cast<ModelObjectConstraint*>(aModelObject);
    }
    
    static const ModelObjectConstraint* cast(const ModelObject* aModelObject)
    {
      if (!isa(aModelObject))
      {
        return nullptr;
      }
      
      return static_cast<const ModelObjectConstraint*>(aModelObject);
    }
    
    /// Adds a constraint parameter to the object
    inline void addConstraintParameter(const std::shared_ptr<Core::ConstraintParameter>& aParam)
    {
      pScope.push_back(aParam);
    }
    
    inline std::string getCtxID() const
    {
      return pCtxID;
    }
    
    std::string getID() const override
    {
      return "";
    }//getID
    
    /// Returns the constraint object encapsulated by this model object
    ConstraintSPtr constraint() const;
    
  protected:
    friend class ModelObjectBuilderCon;
    
    ModelObjectConstraint(const ModelObjectDescriptorConstraintSPtr& aDesc, Core::ConstraintFactory* aCF);
    
  private:
    /// Constraint instance
    mutable ConstraintSPtr pConstraint;
    
    /// Factory for late constraint generation
    Core::ConstraintFactory* pConstraintFactory;
    
    /// Constraint ID
    Core::ConstraintId pID;
    
    /// Constraint context ID is the ID given by the context
    /// this constraint has been generated from
    std::string pCtxID;
    
    /// Constraint propagation strategy
    Core::PropagatorStrategyType pStrategyType;
    
    /// Constraint scope
    mutable std::vector<std::shared_ptr<Core::ConstraintParameter>> pScope;
    
    /// Returns the constraint created according to the given descriptor
    ConstraintSPtr getConstraint() const;
    
    /// Free memory
    void cleanupConstraintCreationResources() const;
  };
  
}// end namespace Model
