//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 03/27/2017
//
// Model object representing a modeling Variable object.
// Using composite pattern to create tree-structure variable object,
// for example, for matrices and arrays.
//

#pragma once

// Base class
#include "ModelObject.hpp"

// Descriptor
#include "ModelObjectDescriptorVariable.hpp"

// Factory for variables
#include "VariableFactory.hpp"

#include <memory>
#include <vector>

// Forward declarations
namespace Core {
  class Variable;
}// end namespace Core

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelObjectVariable : public ModelObject {
  public:
    static bool isa(const ModelObject* aModelObject)
    {
      assert(aModelObject);
      return aModelObject->getModelObjectClass() == ModelObjectClass::MODEL_OBJ_VARIABLE;
    }
    
    static ModelObjectVariable* cast(ModelObject* aModelObject)
    {
      if (!isa(aModelObject))
      {
        return nullptr;
      }
      
      return static_cast<ModelObjectVariable*>(aModelObject);
    }
    
    static const ModelObjectVariable* cast(const ModelObject* aModelObject)
    {
      if (!isa(aModelObject))
      {
        return nullptr;
      }
      
      return static_cast<const ModelObjectVariable*>(aModelObject);
    }
    
    std::string getID() const override
    {
      return "";
    }//getID
    
    /// Returns the variable object encapsulated by this model object
    inline std::shared_ptr<Core::Variable> variable() const
    {
      return pVariable;
    }
    
  protected:
    friend class ModelObjectBuilderVar;
    
    ModelObjectVariable(const ModelObjectDescriptorVariableSPtr& aDescriptorVariable, Core::VariableFactoryPtr aVariableFactory);
    
  private:
    std::shared_ptr<Core::Variable> pVariable;
    
    /// Returns the variable created according to the given descriptor
    Core::VariableSPtr createVariableOnDescriptor(ModelObjectDescriptorVariable* aDescriptorVariable, Core::VariableFactoryPtr aVariableFactory);
    
    /// Returns a new primitive type variable created according to the semantic of descriptor "aDescriptorVariable"
    Core::VariableSPtr createPrimitiveTypeVariable(ModelObjectDescriptorVariable* aDescriptorVariable, Core::VariableFactoryPtr aVariableFactory);
    
    /// Returns a new composite type variable created according to the semantic of descriptor "aDescriptorVariable"
    Core::VariableSPtr createCompositeTypeVariable(ModelObjectDescriptorVariable* aDescriptorVariable, Core::VariableFactoryPtr aVariableFactory);
    
    /// Returns a new variable object for variable Integer
    Core::VariableSPtr createPrimitiveVariableInteger(ModelObjectDescriptorVariableDomain* aModelObjectDomainDescriptor,
                                                      Core::VariableSemanticUPtr aVariableSemantic, Core::VariableFactoryPtr aVariableFactory);
    
    /// Returns a new variable object for variable Boolean
    Core::VariableSPtr createPrimitiveVariableBoolean(ModelObjectDescriptorVariableDomain* aModelObjectDomainDescriptor,
                                                      Core::VariableSemanticUPtr aVariableSemantic, Core::VariableFactoryPtr aVariableFactory);
  };
  
}// end namespace Model
