//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 11/25/2017
//
// Model object representing a modeling Parameter object.
//

#pragma once

// Base class
#include "ModelObject.hpp"

// Descriptor
#include "ScalarParameterDescriptor.hpp"
#include "MatrixParameterDescriptor.hpp"

// Parameter
#include "ConstraintParameter.hpp"
#include "ConstraintParameterFactory.hpp"

#include <memory>

// Forward declarations
namespace Model {
  class Model;
}//end namespace model

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelObjectParameter : public ModelObject {
  public:
    /// Model parameters such as x:[1,2,3] are used to specify
    /// parameters in the constraint scope.
    using ParameterSPtr = std::shared_ptr<Core::ConstraintParameter>;
    using ModelObjectDescriptorParameterSPtr = std::shared_ptr<ModelObjectDescriptorParameter>;
    
  public:
    
    static bool isa(const ModelObject* aModelObject);
    static ModelObjectParameter* cast(ModelObject* aModelObject);
    static const ModelObjectParameter* cast(const ModelObject* aModelObject);
    
    /// Returns the ID of this parameter
    inline std::string getID() const override
    {
      return pID;
    }
    
    /// Returns the parameter object encapsulated by this model object
    inline ParameterSPtr parameter() const
    {
      return pParameter;
    }
    
  protected:
    friend class ModelObjectBuilderPar;
    
    ModelObjectParameter(const ModelObjectDescriptorParameterSPtr& aDP,
                         const std::shared_ptr<Model>& aMdl,
                         const std::shared_ptr<Core::ConstraintParameterFactory>& aCPFactory);
    
  private:
    /// Parameter ID
    std::string pID;
    
    /// Parameter instance
    ParameterSPtr pParameter;
    
    /// Creates an instance of Parameter w.r.t. its descriptor
    ParameterSPtr createParameter(const ModelObjectDescriptorParameterSPtr& aDP,
                                  const std::shared_ptr<Model>& aMdl,
                                  const std::shared_ptr<Core::ConstraintParameterFactory>& aCPFactory);
    
    /// Creates an instance of a Matrix Parameter w.r.t. its descriptor
    ParameterSPtr createScalarParameter(ScalarParameterDescriptor* aMatrix,
                                        const std::shared_ptr<Model>& aMdl,
                                        const std::shared_ptr<Core::ConstraintParameterFactory>& aCPFactory);
    
    /// Creates an instance of a Matrix Parameter w.r.t. its descriptor
    ParameterSPtr createMatrixParameter(MatrixParameterDescriptor* aMatrix,
                                        const std::shared_ptr<Model>& aMdl,
                                        const std::shared_ptr<Core::ConstraintParameterFactory>& aCPFactory);
  };
  
}// end namespace Model
