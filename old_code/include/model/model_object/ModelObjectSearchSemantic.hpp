//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 04/09/2017
//
// Model object representing semnatics for the search engine.
//

#pragma once

// Base class
#include "ModelObject.hpp"

#include "ModelObjectDescriptorSearchSemantic.hpp"
#include "VariableSemantic.hpp"

#include <string>
#include <utility>
#include <vector>
#include <memory>

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelObjectSearchSemantic : public ModelObject {
  public:
    using HeuristicChoice = std::pair<Search::VariableChoiceMetricType, Search::ValueChoiceMetricType>;
    using VarSemPair = std::pair<std::string, Core::VariableSemanticSPtr>;
    using SemanticList = std::vector<VarSemPair>;
    
  public:
    static bool isa(const ModelObject* aModelObject);
    static ModelObjectSearchSemantic* cast(ModelObject* aModelObject);
    static const ModelObjectSearchSemantic* cast(const ModelObject* aModelObject);
    
    inline std::string getSearchSemanticID() const
    {
      return pID;
    }
    
    std::string getID() const override
    {
      return "";
    }//getID
    
    /// Returns the strategy type of this search semantic
    inline Search::SearchEngineStrategyType searchEngineStrategyType() const
    {
      return pSearchStrategtType;
    }
    
    /// Returns the heuristic choice of of this search semantic
    inline const HeuristicChoice& heuristicChoice() const
    {
      return pHeuristicSemantic;
    }
    
    inline void setSemanticList(const SemanticList& aList)
    {
      pSemanticList = aList;
    }
    
    inline SemanticList getSemanticList() const
    {
      return pSemanticList;
    }
    
  protected:
    friend class ModelObjectBuilderSrc;
    
    ModelObjectSearchSemantic(const ModelObjectDescriptorSearchSemanticSPtr& aDescriptorSearchSemantic);
    
  private:
    /// Search semantic identifies
    std::string pID;
    
    /// Search strategy type
    Search::SearchEngineStrategyType pSearchStrategtType;
    
    /// Heuristic semantic
    HeuristicChoice pHeuristicSemantic;
    
    /// List of the variable IDs and their semantics
    SemanticList pSemanticList;
  };
  
}// end namespace Model
