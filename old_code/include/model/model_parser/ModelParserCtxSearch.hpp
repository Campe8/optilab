//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/20/2017
//
// Model context-specific parser: search context.
//

#pragma once

// Base class
#include "ModelParserCtx.hpp"

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelParserCtxSearch : public ModelParserCtx {
  public:
    virtual ~ModelParserCtxSearch() = default;

		/// Parses the model context "aMdlCtx" and generates the corresponding IR module in "aMod".
		/// For example:
		/// Model Context Variable --> IR Module Variable
		void parseModelContext(const ModelContext& aCtx, const IR::IRModuleSPtr& aMod) override;

	protected:
		friend class ModelParser;

		ModelParserCtxSearch();

	private:
		/// Adds the src declaration node corresponding to "aCtx" into the IR module
		/// for search declaration
		//void addIRSrcDeclNode(const ModelContext& aCtx, const IR::IRModuleSPtr& aMod);
  };
  
}// end namespace Model
