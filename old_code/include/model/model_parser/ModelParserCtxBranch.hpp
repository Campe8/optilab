//
// Copyright OptiLab 2017-2018 All rights reserved.
//
// Created by Federico Campeotto on 01/13/2018
//
// Model context-specific parser: branch context.
//
// Update 10/8/2018
// This parser is deprecated.
// It has been replaced with ModelParserCtxSearch.
//

#pragma once

// Base class
#include "ModelParserCtx.hpp"

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelParserCtxBranch : public ModelParserCtx {
  public:
    virtual ~ModelParserCtxBranch() = default;

		/// Parses the model context "aMdlCtx" and generates the corresponding IR module in "aMod".
		/// For example:
		/// Model Context Variable --> IR Module Variable
		void parseModelContext(const ModelContext& aCtx, const IR::IRModuleSPtr& aMod) override;

	protected:
		friend class ModelParser;

		ModelParserCtxBranch();

	private:
    /// Adds a default branch declaration node into the IR "search" module
		void addDefaultIRBranchingDeclNode(const IR::IRModuleSPtr& aMod);
    
    /// Adds a branch declaration node into the IR "search" module
    /// for search declaration
    void addIRBranchingDeclNode(const ModelContext::CtxObjSPtr& aBrc, const IR::IRModuleSPtr& aMod);
  };
  
}// end namespace Model
