//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 12/21/2017
//
// Interpreter class for context matrix.
//

#pragma once

// Base class
#include "CtxInterpreter.hpp"

#include <vector>
#include <unordered_map>

namespace Model {
  
  class MODEL_EXPORT_CLASS CtxInterpreterParameter : public CtxInterpreter {
  public:
		CtxInterpreterParameter();
    
  protected:
    void doInterpretation() override;
    
  private:
    /// Register the interpreting functions
    void registerInterpreters();
    
    //== Interpreters for parameter context ==//
    /// Parameter values
    std::pair<bool, bool> interpretValues(const ModelContext& aMdlCtx, ModelContext& aCtx);
  };
  
}// end namespace Model
