//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/20/2017
//
// Interpreter class for CLI search declarations.
//
// Search context has the following structure:
//  {
//  "type": "dfs",
//  "branching_set": [var1, var2, ...],
//  "semantic": {
//    "variable_choice": "first_fail"/...,
//    "value_choice": "indomain_min"/...
//   }
//  }
//

#pragma once

// Base class
#include "StmtInterpreter.hpp"

namespace Model {
  
  class MODEL_EXPORT_CLASS StmtInterpreterSearch : public StmtInterpreter {
  public:
    StmtInterpreterSearch();
    
  private:
    /// Register the interpreting functions
    void registerInterpreters();
    
    //== Interpreters for search statements ==//
    /// Search type, e.g., dfs
    bool interpretType(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx);
    /// Branching set
    bool interpretBranchingSet(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx);
    /// Semantic
    bool interpretSemantic(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx);
    /// Number of solutions
    bool interpretNumSol(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx);
  };
  
}// end namespace Model
