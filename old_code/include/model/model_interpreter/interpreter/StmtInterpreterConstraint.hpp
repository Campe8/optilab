//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/16/2017
//
// Interpreter class for CLI constraint declarations.
//
// Constraint context has the following structure:
//  {
//  "id": [a-z][A-Z]*,
//  "type": "domain"/"bounds",
//  "scope": [ {[a-z][A-Z]*, 0-9+}, ...]
//  }
//

#pragma once

// Base class
#include "StmtInterpreter.hpp"

namespace Model {
  
  class MODEL_EXPORT_CLASS StmtInterpreterConstraint : public StmtInterpreter {
  public:
    StmtInterpreterConstraint();
    
  private:
    /// Register the interpreting functions
    void registerInterpreters();
    
    //== Interpreters for constraint statements ==//
    /// Constraint key, uniquely identifies a constraint
    bool interpretKey(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx);
    /// Constraint ID
    bool interpretID(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx);
    /// Propagation type
    bool interpretPropType(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx);
    /// Constraint scope
    bool interpretScope(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx);
  };
  
}// end namespace Model

