//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 2/28/2018
//

#pragma once

// Base class
#include "ContextObject.hpp"

namespace Model {
  
  class MODEL_EXPORT_CLASS MatrixContextObject : public ContextObject {
  public:
    MatrixContextObject(const std::string& aID, const Interpreter::DataObject& aDO);
  };
  
}//end namespace Model
