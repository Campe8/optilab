//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 12/27/2017
//
// Interpreter class for context constraint.
//

#pragma once

// Base class
#include "CtxInterpreter.hpp"

#include <vector>
#include <unordered_map>

namespace Model {

	class MODEL_EXPORT_CLASS CtxInterpreterConstraint : public CtxInterpreter {
	public:
		CtxInterpreterConstraint();

	protected:
		void doInterpretation() override;

	private:
		/// Register the interpreting functions
		void registerInterpreters();

		//== Interpreters for constraint context ==//
		std::pair<bool, bool> interpretScope(const ModelContext& aMdlCtx, ModelContext& aCtx);
	};

}// end namespace Model
