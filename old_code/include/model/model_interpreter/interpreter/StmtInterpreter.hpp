//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/10/2017
//
// Interpreter class for statements (CLI) messages.
//

#pragma once

// Base class
#include "ModelInterpreter.hpp"

// CLI includes
#include "CLIUtils.hpp"
#include "CLIDefs.hpp"

#include <ostream>
#include <map>
#include <functional>

namespace Model {
  
  class MODEL_EXPORT_CLASS StmtInterpreter : public ModelInterpreter {
  public:
    virtual ~StmtInterpreter() = default;
    
    /// Interprets the given stmt based on the current context "aModelContext".
    /// Returns a new ModelContext that describes the interpreted statement
    ModelContext interpretStmt(const ModelContext& aModelContext, const CLI::CLIStmt& aStmt);
    
  protected:
    /// Interpreter function, given a context and a statement,
    /// modifies the input context (third parameter) by adding the interpreted information.
    /// Returns true if the given statement has been fully interpreted, false otherwise
    using InterpreterFcn = std::function<bool(const ModelContext& aMdlCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx)>;
    
  protected:
    /// Runs interpreters
    void doInterpretation() override;
    
    /// Finalizes the interpreting process
    void finalizeInterpreter() override;
    
  protected:
    /// Constructor sets the symbol type of this interpreter
    StmtInterpreter(SymbolType aSymbType);
    
    /// Adds "aIFcn" to the internal register.
    /// When "runInterpreterFcns" is called, it runs the functions in their registration order.
    /// To call each single function, use the function name
    void registerInterpreterFcn(const std::string& aFcnName, const InterpreterFcn& aIFcn);
    
    /// Runs all the registered interpreter functions by their execution order
    void runInterpreterFcn();
    
    /// Runs the registered interpreter function with name "aFcnName"
    void runInterpreterFcn(const std::string& aFcnName);
    
    /// Returns current stmt
    inline const CLI::CLIStmt* getStmt() const
    {
      return pStmt;
    }
    
  private:
    /// Current stmt to be interpreted
    const CLI::CLIStmt* pStmt;
    
    /// Register of interpreter functions
    std::vector<InterpreterFcn> pIFcnRegister;
    
    /// Mapping between interpreter function names and their keys
    std::map<std::string, std::size_t> pIFcnNameMap;
  };
  
}// end namespace Model
