//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/04/2017
//
// Interpreter class for model instances.
// It specifies how to evaluate sentences in OptiLab language.
// It converts input streams representing models into a JSON model
// read by the model parser.
//

#pragma once

#include "ModelExportDefs.hpp"

// Model context
#include "ModelContext.hpp"

#include "CLIDefs.hpp"

#include <istream>

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelInterpreter {
  public:
    /// Type of symbol encoding the message to interpret
    enum SymbolType : unsigned int {
        ST_VARIABLE_DECL = 0
      , ST_CONSTRAINT_DECL
      , ST_BRANCH_DECL
      , ST_SEARCH_DECL
      , ST_MATRIX_DECL
      , ST_ERROR
      , ST_UNDEF // must be last
    };
    
    virtual ~ModelInterpreter() = default;
    
    inline SymbolType getInterpretedSymbolType() const
    {
      return pInterpretedSymb;
    }
    
  protected:
    /// Runs the interpreter by calling the interpreting functions
    /// according to their order defined by each derived class.
    /// @note override this method calling "runInterpreterFcn"
    /// with the function name to change the execution order
    /// of interpreting functions
    virtual void doInterpretation() = 0;
    
  protected:
    ModelInterpreter(SymbolType aSymbType);
    
    /// Sets fully interpreted state
    inline void setFullyInterpretedState(bool aInterp)
    {
      pInterpreted = aInterp;
    }
    
    /// Performs logical "and" between "aInterp" and the
    /// internal state
    inline void andFullyInterpretedState(bool aInterp)
    {
      pInterpreted &= aInterp;
    }
    
    /// Returns fully interpreted state
    inline bool getFullyInterpretedState() const
    {
      return pInterpreted;
    }
    
    inline void setModelContex(const ModelContext* aMCtx)
    {
      pMCtx = aMCtx;
    }
    inline const ModelContext* getModelContext() const
    {
      return pMCtx;
    }
    
    inline void setInterpContex(ModelContext* aICtx)
    {
      pICtx = aICtx;
    }
    inline ModelContext* getInterpContext() const
    {
      return pICtx;
    }
    
    /// Initializes the internal state before interpreting.
    /// @note sets the interpretate state and view.
    /// @note it should be called before "runInterpreter"
    virtual void initInterpreter();
    
    /// Calls initialization, interpret and finalize
    /// methods, this call runs the interpreting functions
    virtual void runInterpreter();
    
    /// Finalizes the internal state once the interpretation is done.
    /// @note sets the interpreter field in the context
    /// @note it should be called after "runInterpreter"
    virtual void finalizeInterpreter();
    
    /// Checks input Boolean value and, if not true,
    /// throws with notification "aMsg"
    void assertAndThrow(bool aTest, const std::string& aMsg);
    
  private:
    /// Symbol type this instance is interpreting
    SymbolType pInterpretedSymb;
    
    /// Flag for fully interpreted statements
    bool pInterpreted;
    
    /// Model context to be interpreted
    ModelContext* pICtx;
    
    /// Model context used to interpret
    const ModelContext* pMCtx;
  };
  
}// end namespace Model

