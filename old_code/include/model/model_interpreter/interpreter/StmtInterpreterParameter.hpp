//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 22/11/2017
//
// Interpreter class for CLI matrix declarations.
//
// Matrix context has the following structure:
//  {
//  "id": [a-z][A-Z]*,
//  "type": "integer"/"boolean",
//  "nrows": [0-9+],
//  "ncols": [0-9+],
//  "values": [ [0-9+, 0-9+, ...]* | [0-9+, 0-9+, ...]*]
//  }
//

#pragma once

// Base class
#include "StmtInterpreter.hpp"

namespace Model {
  
  class MODEL_EXPORT_CLASS StmtInterpreterParameter : public StmtInterpreter {
  public:
    StmtInterpreterParameter();
    
  protected:
    void doInterpretation() override;
    
  private:
    /// Register the interpreting functions
    void registerInterpreters();
    
    //== Interpreters for parameter statements ==//
    /// Parameter ID
    bool interpretID(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx);
    /// Parameter type
    bool interpretType(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx);
    /// Number of rows
    bool interpretNumRows(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx);
    /// Number of columns
    bool interpretNumCols(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx);
    /// Values
    bool interpretValues(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx);
  };
  
  /// Returns the number of rows of the parameter
  MODEL_EXPORT_FUNCTION std::size_t getNumberOfParamRows(const ModelContext& aCtx);
  MODEL_EXPORT_FUNCTION std::size_t getNumberOfParamRows(const Context& aCtx);
  
  /// Returns the number of columns of the parameter
  MODEL_EXPORT_FUNCTION std::size_t getNumberOfParamCols(const ModelContext& aCtx);
  MODEL_EXPORT_FUNCTION std::size_t getNumberOfParamCols(const Context& aCtx);
  
  /// Returns the vector of parameter's rows
  MODEL_EXPORT_FUNCTION std::vector<std::vector<std::string>>getParameterRows(const ModelContext& aCtx);
  MODEL_EXPORT_FUNCTION std::vector<std::vector<std::string>> getParameterRows(const Context& aCtx);
  
}// end namespace Model
