//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/04/2017
//
// Engine interpreter: provides an interface for running
// interpreters and expanding the context.
//

#pragma once

#include "ModelExportDefs.hpp"

#include "ModelMacro.hpp"
#include "ModelDefs.hpp"

#include "BoostInc.hpp"

// OptiLab language interpreter
#include "StmtInterpreter.hpp"
// Context interpreter
#include "CtxInterpreter.hpp"

// CLI stmt
#include "CLIDefs.hpp"

#include <boost/optional.hpp>

#include <vector>
#include <utility>
#include <memory>

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelInterpreterEngine {
  public:
    /// Context interpreted on a SymbolType statement
    using SIContext = std::pair<ModelInterpreter::SymbolType, ModelContext>;
    /// Optional Interpreted Context
    using OIContext = boost::optional<ModelContext>;
    
    ModelInterpreterEngine();
    
    /// Creates a new model context identified by "aCtxID".
    /// The context can be expanded by calling "expandModelContext" on an
    /// interpreted context
    ModelContext createBaseModelContext(const std::string& aCtxID);
    
    /// Expands the context "aModelContext" with the interpreted context "aInterpContext"
    void expandModelContext(ModelContext& aModelContext, const ModelContext& aMdlCtx, ModelInterpreter::SymbolType aSType);
    
    /// Interprets "aStmt" on the given model context "aMdlCxt".
    /// Returns the interpreted context and its type
    SIContext interpretStmt(const ModelContext& aMdlCxt, const CLI::CLIStmt& aStmt);
    
    /// Interprets "aContext" on the given model context "aMdlCxt".
    /// Returns the interpreted context if it was touched, i.e., if "aCtx" was
    OIContext interpretContext(const ModelContext& aMdlCxt, ModelInterpreter::SymbolType aCtxType, const Context& aCtx);
    
    /// Returns the symbol type of "aStmt"
    ModelInterpreter::SymbolType getSymbolType(const CLI::CLIStmt& aStmt);
    
  protected:
    /// Returns a new interpreter for a stmt encoding a content of type "aSymbolType".
    /// @note it should return an instance of interpreter for each type of symbols SymbolType,
    /// nullptr otherwise
    std::shared_ptr<StmtInterpreter> getStmtIntrepreterInstance(ModelInterpreter::SymbolType aSymbolType);
    
    /// Returns a new interpreter for a context encoding a content of type "aSymbolType".
    /// @note it should return an instance of interpreter for each type of symbols SymbolType,
    /// nullptr otherwise
    std::shared_ptr<CtxInterpreter> getCtxIntrepreterInstance(ModelInterpreter::SymbolType aSymbolType);
    
  private:
    using StmtInterpPtr = std::shared_ptr<StmtInterpreter>;
    using CtxInterpPtr = std::shared_ptr<CtxInterpreter>;
    using InterPtrPair = std::pair<StmtInterpPtr, CtxInterpPtr>;
    
  private:
    /// Register of interpreters for each symbol in the language
    std::vector<InterPtrPair> pInterpreterRegister;
    
    /// Returns the instance of the stmt interpreter for the symbol "aSymbolType".
    /// @note returns nullptr if no such interpreter is registered
    StmtInterpreter* getStmtInterpreter(ModelInterpreter::SymbolType aSymbolType);
    
    /// Returns the instance of the ctx interpreter for the symbol "aSymbolType".
    /// @note returns nullptr if no such interpreter is registered
    CtxInterpreter* getCtxInterpreter(ModelInterpreter::SymbolType aSymbolType);
    
    /// Instantiates new interpreters and registers them into the register.
    /// There should be an interpreter for each symbol in the language
    void registerInterpreters();
  };
  
}// end namespace Model

