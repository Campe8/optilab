//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/10/2017
//
// Interpreter class for CLI variable declarations.
//
// Variable context has the following structure:
//  {
//  "id": [a-z][A-Z]*,
//  "type": "integer"/"boolean",
//  "output": "true"/"false"
//  "branching": "true"/"false",
//  "dimensions": [ 0-9+ | 0-9+, ]
//  "semantic": {
//    "type": "decision"/"support"/"objective",
//    "extremum": "minimize"/"minimize"
//   },
//  "domain": {
//    "type": "extensional"/"bounds",
//    "values": [ 0-9+ | 0-9+, ]
//  }
//  }
//

#pragma once

// Base class
#include "StmtInterpreter.hpp"

namespace Model {
  
  class MODEL_EXPORT_CLASS StmtInterpreterVariable : public StmtInterpreter {
  public:
    StmtInterpreterVariable();
    
  protected:
    void doInterpretation() override;
    
  private:
    /// Register the interpreting functions
    void registerInterpreters();
    
    //== Interpreters for variables ==//
    /// Variable ID
    bool interpretID(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx);
    /// Branching option
    bool interpretBranching(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx);
    /// Variable type, e.g., integer
    bool interpretType(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx);
    /// Output variable
    bool interpretOutput(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx);
    /// Input order
    bool interpretInputOrder(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx);
    /// Semantic type, e.g., decision
    bool interpretSemantic(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx);
    /// Variable dimensions
    bool interpretDimension(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx);
    /// Domain
    bool interpretDomain(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx);
  };
  
  /// Returns true if "aCtx" contains the domain dimensions field, false otherwise
  MODEL_EXPORT_FUNCTION bool containsDomainDimensionsField(const ModelContext& aCtx);
  MODEL_EXPORT_FUNCTION bool containsDomainDimensionsField(const Context& aCtx);
  
  /// Returns the vectorized size of the dimensions in the "dimensions" field of a domain context.
  /// If there is no such field, it returns 0.
  /// For example, if dimensions are [2, 3], it returns 6.
  /// The value 0 for a dimension leads to undefined behavior
  MODEL_EXPORT_FUNCTION std::size_t getVectorizedDomainDimension(const ModelContext& aCtx);
  MODEL_EXPORT_FUNCTION std::size_t getVectorizedDomainDimension(const Context& aCtx);
  
  /// Returns the scalarized matrix of domains from the "domain" field of "aCtx".
  /// If there is only one single domain declaration (i.e., no dimensions field),
  /// returns the single domain
  MODEL_EXPORT_FUNCTION std::vector<std::vector<std::string>> getVectorizedDomain(const ModelContext& aCtx);
  MODEL_EXPORT_FUNCTION std::vector<std::vector<std::string>> getVectorizedDomain(const Context& aCtx);
  
}// end namespace Model
