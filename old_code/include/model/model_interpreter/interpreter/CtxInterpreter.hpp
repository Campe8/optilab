//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 12/20/2017
//
// Interpreter class for context.
//

#pragma once

// Base class
#include "ModelInterpreter.hpp"

// CLI includes
#include "CLIUtils.hpp"
#include "CLIDefs.hpp"

#include <boost/optional.hpp>

#include <ostream>
#include <map>
#include <utility>
#include <functional>

namespace Model {
  
  class MODEL_EXPORT_CLASS CtxInterpreter : public ModelInterpreter {
  public:
    virtual ~CtxInterpreter() = default;

    /// Interprets the given context based on the current model context "aMdlCtx".
    /// Returns the interpreted model context or none if no interpretation was possible
    /// in the current (given) model context.
    /// @note the interpreted view is w.r.t. the given context, i.e., its entries
    /// contain only information about "aCtx"
    boost::optional<ModelContext> interpretCtx(const ModelContext& aMdlCtx, const Context& aCtx);
    
  protected:
    /// Interpreter function, given a model context and a context,
    /// modifies the input context (second parameter) by re-interpreting it w.r.t.
    /// the information contained in "aMdlCtx".
    /// Returns a pair <a, b> of Boolean values where:
    /// (a) is true if "aCtx" changed during interpretation, false otherwise.
    ///     For example, (a) can be false if there wasn't enough information to interpret anything in "aCtx"
    /// (b) is true if the given context is fully interpreted, false otherwise.
    /// @note "aCtx" can still require interpretation but it can be changed by this interpretation,
    /// i.e., an InterpretFcn can return the pair <true, false>
    using InterpreterFcn = std::function<std::pair<bool, bool>(const ModelContext& aMdlCtx, ModelContext& aCtx)>;
    
  protected:
    /// Runs interpreters
    void doInterpretation() override;
    
  protected:
    /// Constructor sets the symbol type of this interpreter
    CtxInterpreter(SymbolType aSymbType);
    
    /// Adds "aIFcn" to the internal register.
    /// When "runInterpreterFcns" is called, it runs the functions in their registration order.
    /// To call each single function, use the function name
    void registerInterpreterFcn(const std::string& aFcnName, const InterpreterFcn& aIFcn);
    
    /// Runs all the registered interpreter functions by their execution order
    void runInterpreterFcn();
    
    /// Runs the registered interpreter function with name "aFcnName"
    void runInterpreterFcn(const std::string& aFcnName);
    
  private:
    /// Flag indicating whether the interpreting context is touched or not
    bool pTouchedCtx;
    
    /// Register of interpreter functions
    std::vector<InterpreterFcn> pIFcnRegister;
    
    /// Mapping between interpreter function names and their keys
    std::map<std::string, std::size_t> pIFcnNameMap;
  };
  
}// end namespace Model
