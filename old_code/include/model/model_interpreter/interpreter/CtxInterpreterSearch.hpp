//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 12/27/2017
//
// Interpreter class for context search.
//

#pragma once

// Base class
#include "CtxInterpreter.hpp"

#include <vector>
#include <unordered_map>

namespace Model {

	class MODEL_EXPORT_CLASS CtxInterpreterSearch : public CtxInterpreter {
	public:
		CtxInterpreterSearch();

	protected:
		void doInterpretation() override;

	private:
		/// Register the interpreting functions
		void registerInterpreters();

		//== Interpreters for search context ==//
		// Interpret branching set
    std::pair<bool, bool> interpretBranchingSet(const ModelContext& aMdlCtx, ModelContext& aCtx);
    // Interpret number of solutions
    std::pair<bool, bool> interpretSolutionNumber(const ModelContext& aMdlCtx, ModelContext& aCtx);
	};

}// end namespace Model
