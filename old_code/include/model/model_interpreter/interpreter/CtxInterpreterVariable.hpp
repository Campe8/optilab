//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 12/26/2017
//
// Interpreter class for context variable.
//

#pragma once

// Base class
#include "CtxInterpreter.hpp"

#include <vector>
#include <unordered_map>

namespace Model {
  
  class MODEL_EXPORT_CLASS CtxInterpreterVariable : public CtxInterpreter {
  public:
    CtxInterpreterVariable();
    
  protected:
    void doInterpretation() override;
    
  private:
    /// Register the interpreting functions
    void registerInterpreters();
    
    //== Interpreters for variable context ==//
    std::pair<bool, bool> interpretDomain(const ModelContext& aMdlCtx, ModelContext& aCtx);
    std::pair<bool, bool> interpretDimensions(const ModelContext& aMdlCtx, ModelContext& aCtx);
  };
  
}// end namespace Model

