//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 01/13/2018
//
// Interpreter class for context branch.
//

#pragma once

// Base class
#include "CtxInterpreter.hpp"

#include <vector>
#include <unordered_map>

namespace Model {

	class MODEL_EXPORT_CLASS CtxInterpreterBranch : public CtxInterpreter {
	public:
		CtxInterpreterBranch();

	protected:
		void doInterpretation() override;

	private:
		/// Register the interpreting functions
		void registerInterpreters();

		//== Interpreters for branch context ==//
		// Interpret branching set
    std::pair<bool, bool> interpretBranchingSet(const ModelContext& aMdlCtx, ModelContext& aCtx);
	};

}// end namespace Model
