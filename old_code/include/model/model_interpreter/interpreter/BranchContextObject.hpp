//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 2/21/2018
//
// Update 10/8/2018
// This context object is deprecated.
// It has been replaced with SearchContextObject.
//

#pragma once

// Base class
#include "ContextObject.hpp"

namespace Model {
  
  class MODEL_EXPORT_CLASS BranchContextObject : public ContextObject {
  public:
    enum VariableChoice : char {
        VARC_INPUT_ORDER = 0
      , VARC_FIRST_FAIL
      , VARC_LARGEST
      , VARC_SMALLEST
      , VARC_ANTI_FIRST_FAIL
    };
    
    enum ValueChoice : char {
        VALC_INDOMAIN_MAX = 0
      , VALC_INDOMAIN_MIN
      , VALC_INDOMAIN_RANDOM
    };
    
  public:
    BranchContextObject(const std::string& aID, const Interpreter::DataObject& aDO);
    
    static VariableChoice getDefaultVariableChoice() { return VariableChoice::VARC_INPUT_ORDER; }
    static ValueChoice getDefaultValueChoice()    { return ValueChoice::VALC_INDOMAIN_MIN; }
    
    /// Set/get variable choice
    inline void setVariableChoice(VariableChoice aVarChoice) { pVarChoice = aVarChoice; }
    inline VariableChoice getVariableChoice() const { return pVarChoice; }
    
    /// Set/get value choice
    inline void setValueChoice(ValueChoice aValChoice) { pValChoice = aValChoice; }
    inline ValueChoice getValueChoice() const { return pValChoice; }
    
  private:
    /// Variable choice heuristic
    VariableChoice pVarChoice;
    
    /// Value choice heuristic
    ValueChoice pValChoice;
    
    /// Set the variable/value heuristics as specified in "aDO"
    void setHeuristics(const Interpreter::DataObject& aDO);
    
    /// Remove duplicate variables from the branching variable list
    void removeDuplicates(Interpreter::DataObject& aDO);
  };
  
}//end namespace Model
