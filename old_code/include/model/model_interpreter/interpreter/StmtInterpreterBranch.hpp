//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 01/13/2018
//
// Interpreter class for CLI branch declarations.
//
// Branch context has the following structure:
//  {
//    "branching_set": [var1, var2, ...],
//    "variable_choice": "first_fail"/...,
//    "value_choice": "indomain_min"/...
//  }
//

#pragma once

// Base class
#include "StmtInterpreter.hpp"

namespace Model {
  
  class MODEL_EXPORT_CLASS StmtInterpreterBranch : public StmtInterpreter {
  public:
    StmtInterpreterBranch();
    
  private:
    /// Register the interpreting functions
    void registerInterpreters();
    
    //== Interpreters for branch statements ==//
    /// Constraint key, uniquely identifies a branch statement
    bool interpretKey(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx);
    /// Branching set
    bool interpretBranchingSet(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx);
    /// Semantic
    bool interpretHeuristics(const ModelContext& aMCtx, const CLI::CLIStmt& aStmt, ModelContext& aIMCtx);
  };
  
}// end namespace Model
