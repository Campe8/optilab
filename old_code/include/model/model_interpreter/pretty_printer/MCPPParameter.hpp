//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 12/15/2017
//
// This is a pretty printer for all the BaseObjects that are
// deriving from CtxObj.
//

#pragma once

// Base class
#include "ModelContextPrettyPrinter.hpp"

namespace Model {
  
	class MODEL_EXPORT_CLASS MCPPParameter : public ModelContextPrettyPrinter {
	public:
		void prettyPrint(const std::shared_ptr<ContextObject>& aObj, std::ostream& aOut, bool aFullPrettyPrint) override;

	private:
		/// Pretty prints dimensions
		void prettyPrintDimensions(Interpreter::BaseObject* aPar, std::ostream& aOut);

		/// Pretty prints the values of the paramerter
		void prettyPrintValues(Interpreter::BaseObject* aPar, std::ostream& aOut);
	};

}// end namespace Model
