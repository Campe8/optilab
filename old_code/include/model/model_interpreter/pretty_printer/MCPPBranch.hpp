//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 01/13/2018
//

#pragma once

// Base class
#include "ModelContextPrettyPrinter.hpp"

namespace Model {
  
	class MODEL_EXPORT_CLASS MCPPBranch : public ModelContextPrettyPrinter {
	public:
		void prettyPrint(const std::shared_ptr<ContextObject>& aObj, std::ostream& aOut, bool aFullPrettyPrint) override;

	private:
    /// Pretty prints beanching key
    void prettyPrintKey(const std::string aID, std::ostream& aOut);
    
		/// Pretty prints the branching set
		void prettyPrintBranchingSet(const Interpreter::DataObject& aObj, std::ostream& aOut);
    
		/// Pretty prints branch heuristic
		void prettyPrintHeuristic(const ContextObject* aObj, std::ostream& aOut);
	};

}// end namespace Model
