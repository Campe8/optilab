//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/16/2017
//

#pragma once

#include "ModelObjectBuilder.hpp"

#include "ModelObjectDescriptorVariable.hpp"
#include "VariableFactory.hpp"

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelObjectBuilderVar : public ModelObjectBuilder {
  public:
    ModelObjectBuilderVar();
    
    virtual ~ModelObjectBuilderVar() = default;
    
    /// Expands "aMdl" by adding the model objects built from "aCtx"
    void expandModel(const ModelSPtr& aMdl, const std::shared_ptr<ModelGeneratorContext>& aCtx) override;
    
  private:
    /// Factory for variables
    std::unique_ptr<Core::VariableFactory> pVarFactory;
    
    /// Returns a model object variable
    ModelObjectUPtr buildObjVar(const ModelObjectDescriptorVariableSPtr& aDesc);
  };
  
}// end namespace Model
