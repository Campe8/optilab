//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 11/26/2017
//

#pragma once

#include "ModelObjectBuilder.hpp"
#include "ModelObjectDescriptorParameter.hpp"
#include "ConstraintParameterFactory.hpp"

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelObjectBuilderPar : public ModelObjectBuilder {
  public:
    ModelObjectBuilderPar();
    
    /// Expands "aMdl" by adding the model objects built from "aCtx"
    void expandModel(const ModelSPtr& aMdl, const std::shared_ptr<ModelGeneratorContext>& aCtx) override;
    
  private:
    /// Factory for constraints parameters
    std::shared_ptr<Core::ConstraintParameterFactory> pParamFactory;
    
    /// Returns a model object parameter
    ModelObjectUPtr buildObjPar(const std::shared_ptr<ModelObjectDescriptorParameter>& aPDesc, const ModelSPtr& aMdl);
  };
  
}// end namespace Model
