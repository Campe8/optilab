//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/17/2017
//

#pragma once

#include "ModelObjectBuilder.hpp"
#include "ModelObjectDescriptorConstraint.hpp"

#include "ConstraintFactory.hpp"

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelObjectBuilderCon : public ModelObjectBuilder {
  public:
    ModelObjectBuilderCon();
    
    virtual ~ModelObjectBuilderCon() = default;
    
    /// Expands "aMdl" by adding the model objects built from "aCtx"
    void expandModel(const ModelSPtr& aMdl, const std::shared_ptr<ModelGeneratorContext>& aCtx) override;
    
  private:
    /// Factory for variables
    std::unique_ptr<Core::VariableFactory> pVarFactory;
    
    /// Returns a model object constraint
    ModelObjectUPtr buildObjCon(const ModelObjectDescriptorConstraintSPtr& aDesc, const ModelSPtr& aMdl);
    
  private:
    /// Factory for constraints
    std::shared_ptr<Core::ConstraintFactory> pConFactory;
  };
  
}// end namespace Model
