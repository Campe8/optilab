//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 09/17/2017
//

#pragma once

#include "ModelObjectBuilder.hpp"
#include "ModelObjectDescriptorSearchSemantic.hpp"

#include "ConstraintFactory.hpp"

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelObjectBuilderSrc : public ModelObjectBuilder {
  public:
    ModelObjectBuilderSrc();
    
    /// Expands "aMdl" by adding the model objects built from "aCtx"
    void expandModel(const ModelSPtr& aMdl, const std::shared_ptr<ModelGeneratorContext>& aCtx) override;
    
  private:
    /// Returns a model object search
    ModelObjectUPtr buildObjSrc(const std::shared_ptr<ModelGeneratorContext>& aCtx,
                                const ModelObjectDescriptorSearchSemanticSPtr& aDSrc);
  };
  
}// end namespace Model
