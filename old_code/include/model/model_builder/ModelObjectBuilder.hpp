//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/15/2017
//

#pragma once

#include "ModelExportDefs.hpp"

#include "Model.hpp"
#include "ModelGeneratorContext.hpp"

#include "ModelDefs.hpp"

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelObjectBuilder {
  public:
    virtual ~ModelObjectBuilder() = default;
    
    /// Expands "aMdl" by adding the model objects built from "aCtx"
    virtual void expandModel(const ModelSPtr& aMdl, const std::shared_ptr<ModelGeneratorContext>& aCtx) = 0;
    
    /// Returns the priority of this builder
    inline int getPriority() const
    {
      return static_cast<int>(pPriority);
    }
    
  protected:
    using DescSet = ModelGeneratorContext::ObjDescSet;
    using DescClass = ModelObjectDescriptorClass;
    using DescCtxSPtr = std::shared_ptr<ModelGeneratorContext>;
    
  protected:
    ModelObjectBuilder(BuilderPriority aPriority);
    
    /// Utility function: returns the descriptors of a given class
    /// fromt eh model generator context
    DescSet getDescriptors(const DescCtxSPtr& aCtx, DescClass aClass);
    
  private:
    BuilderPriority pPriority;
  };
  
}// end namespace Model
