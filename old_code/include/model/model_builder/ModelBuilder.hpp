//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/8/2017
//
// Model builder: back-end building the target model.
//

#pragma once

#include "ModelExportDefs.hpp"

// IR
#include "IRContext.hpp"

// Generator
#include "ModelGenerator.hpp"

// Builder
#include "ModelObjectBuilder.hpp"

// Model object descriptors
#include "ModelObjectDescriptorVariable.hpp"
#include "ModelObjectDescriptorConstraint.hpp"
#include "ModelObjectDescriptorSearchSemantic.hpp"

#include <map>
#include <memory>

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelBuilder {
  public:
		ModelBuilder();
    
    virtual ~ModelBuilder() = default;
    
		/// Builds the target model from the given IR context
		ModelSPtr buildModel(const IR::IRContextSPtr& aIRCtx);
    
  private:
    using Builder = std::shared_ptr<ModelObjectBuilder>;
    using BuilderMap = std::map<int, Builder>;
    
  private:
    /// Model generator for converting abstract syntax tree to
    /// the model read by the engine
    std::unique_ptr<ModelGenerator> pModelGenerator;
    
    /// Map of builders
    BuilderMap pBuilderMap;
    
    /// Registers the model builders
    void registerBuilders();
  };
  
}// end namespace Model
