//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 09/15/2017
//
//

#pragma once

#include "ConstraintParameterDescriptor.hpp"

namespace Model {

  class MODEL_EXPORT_CLASS ParamInt64 : public ParamLeaf {
  public:
		ParamInt64(INT_64 aScalarInt);
    
    /// Override internal value and set it to +INF
    inline void overrideValueWithPosInf()
    {
      pValue = Core::Limits::int64Max();
    }
    
    /// Override internal value and set it to -INF
    inline void overrideValueWithNegInf()
    {
      pValue = Core::Limits::int64Min();
    }
    
    inline INT_64 getValue() const
    {
      return pValue;
    }
    
  private:
    INT_64 pValue;
  };
  
  class MODEL_EXPORT_CLASS ParamBool : public ParamLeaf {
  public:
    ParamBool(BOOL_T aBool);
    
    inline BOOL_T getValue() const
    {
      return pValue;
    }
    
  private:
    BOOL_T pValue;
  };
  
  class MODEL_EXPORT_CLASS ParamID : public ParamLeaf {
  public:
    inline std::string getID() const
    {
      return pID;
    }
    
  protected:
    ParamID(const std::string& aID, ConstraintParameterDescriptor::ParameterType aParamType);
    
  private:
    std::string pID;
  };
  
  class MODEL_EXPORT_CLASS ParamVarID : public ParamID {
  public:
    /// Variable identifier
    ParamVarID(const std::string& aVarID);
  };
  
  class MODEL_EXPORT_CLASS ParamRefID : public ParamID {
  public:
    /// Reference identifier
    ParamRefID(const std::string& aVarID);
  };
  
  class MODEL_EXPORT_CLASS ParamVarSubscript : public ParamLeaf {
  public:
    /// Variable, subscript identifier
    ParamVarSubscript(const std::string& aVarID, std::size_t aSubscript);
    
    /// Returns matrix variable name
    inline std::string getVariableID() const
    {
      return pVarID;
    }
    
    /// Returns subscript
    inline std::size_t getSubscript() const
    {
      return pSubscript;
    }
    
  private:
    /// Matrix variable name
    std::string pVarID;
    
    /// Indexing value
    std::size_t pSubscript;
  };
  
}// end namespace Model
