//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 03/29/2017
//
// Model object descriptors represent meta-information
// given by the parser in order to create model objects
// describing domains of variables.
//

#pragma once

// Base class
#include "ModelObjectDescriptorVariableDomain.hpp"

#include "VariableDefs.hpp"

#include "DomainElementInt64.hpp"

#include <vector>
#include <unordered_set>
#include <memory>
#include <cassert>

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelObjectDescriptorVariableDomainInteger : public ModelObjectDescriptorVariableDomain {
  public:
    ModelObjectDescriptorVariableDomainInteger(INT_64 aSingleton);
    
    ModelObjectDescriptorVariableDomainInteger(INT_64 aLowerBound, INT_64 aUpperBound);
    
    ModelObjectDescriptorVariableDomainInteger(const std::unordered_set<INT_64>& aSetOfElements);
    
    ModelObjectDescriptorVariableDomain* clone() override;
    
    static bool isa(ModelObjectDescriptorVariableDomain* aDescriptorDomain)
    {
      assert(aDescriptorDomain);
      return aDescriptorDomain->type()->variableTypeClass() == Core::VariableTypeClass::VAR_TYPE_INTEGER;
    }
    
    static ModelObjectDescriptorVariableDomainInteger* cast(ModelObjectDescriptorVariableDomain* aDescriptorDomain)
    {
      if (!isa(aDescriptorDomain))
      {
        return nullptr;
      }
      return static_cast<ModelObjectDescriptorVariableDomainInteger*>(aDescriptorDomain);
    }
    
    inline const std::vector<Core::DomainElementInt64*>& domainConfiguration() const
    {
      return pDomainConfiguration;
    }
    
  private:
    /// Vector of domain elements representing the configuration of this domain
    std::vector<Core::DomainElementInt64*> pDomainConfiguration;
  };
  
}// end namespace Model
