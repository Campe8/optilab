//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 04/05/2017
//
// Model object descriptors represent meta-information
// given by the parser in order to create model objects
// describing variables.
//

#pragma once

// Base class
#include "ModelObjectDescriptor.hpp"

#include "ConstraintDefs.hpp"
#include "ConstraintParameter.hpp"

// Constraint parameter descriptor
#include "ConstraintParameterDescriptor.hpp"
#include "ConstraintParameterFactory.hpp"

#include <vector>
#include <utility>

// Forward declarations
namespace Model {
  class Model;
  
  class ModelObjectDescriptorConstraint;
  typedef std::shared_ptr<ModelObjectDescriptorConstraint> ModelObjectDescriptorConstraintSPtr;
}// end namespace Search

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelObjectDescriptorConstraint : public ModelObjectDescriptor {
  public:
    using ConParamDescSPtr = std::shared_ptr<ConstraintParameterDescriptor>;
    using ScopeDesc = std::vector<ConParamDescSPtr>;
    using ConParamSPtr = std::shared_ptr<Core::ConstraintParameter>;
    using Scope = std::vector<ConParamSPtr>;
    
  public:
    ModelObjectDescriptorConstraint(Core::ConstraintId aConType,
                                    Core::PropagatorStrategyType aStrType,
                                    const ScopeDesc& aScopeDesc,
                                    const std::shared_ptr<Core::ConstraintParameterFactory>& aParamFactory);
    
    virtual ~ModelObjectDescriptorConstraint() = default;
    
    /// Returns the constraint id for the constraints described by this descriptor
    inline Core::ConstraintId constraintID() const
    {
      return pConstraintID;
    }
    
    /// Sets the constraint context ID
    inline void setConstraintCtxID(const std::string& aCtxID)
    {
      pConstraintCtxID = aCtxID;
    }
    
    /// Returns the constraint context ID
    inline std::string constraintCtxID() const
    {
      return pConstraintCtxID;
    }
    
    /// Returns the strategy type to be used by "constraintId"
    inline Core::PropagatorStrategyType strategyType() const
    {
      return pPropagatorStrategyType;
    }
    
    /// Generates the scope from the parameters descriptors
    /// which is defined over "aModel"
    Scope getScope(const std::shared_ptr<Model>& aModel);
    
  private:
    using TypeAndParameter = std::pair<Core::ConstraintParameter::ConstraintParameterClass, ConParamSPtr>;
    
  private:
    /// Constraint id - type
    Core::ConstraintId pConstraintID;
    
    /// Constraint context ID
    std::string pConstraintCtxID;
    
    /// Constraint propagation strategy type
    Core::PropagatorStrategyType pPropagatorStrategyType;
    
    /// Scope descriptor
    ScopeDesc pScopeDescriptor;
    
    /// Factory for constraint parameters
    std::shared_ptr<Core::ConstraintParameterFactory> pParamFactory;
    
    /// Returns the type and parameter constraint parameter descriptor
    TypeAndParameter getLeafParam(const ConParamDescSPtr& aDesc, const std::shared_ptr<Model>& aModel);
    
    /// Returns a scope composite parameter
    ConParamSPtr getCompositeParam(const ConParamDescSPtr& aDesc, const std::shared_ptr<Model>& aModel);
    
    // Utility function for creating composite parameters from leaf parameters
    ConParamSPtr createCompositeParamFromParamArray(const std::vector<TypeAndParameter>& aParamArry, const std::shared_ptr<Model>& aModel);
    
    /// Utility functions: parameter descriptor generators
    ConParamSPtr paramDescVarID(const ConParamDescSPtr& aParamDesc, const std::shared_ptr<Model>& aModel);
    ConParamSPtr paramDescRefID(const ConParamDescSPtr& aParamDesc, const std::shared_ptr<Model>& aModel);
    ConParamSPtr paramDescSubscript(const ConParamDescSPtr& aParamDesc, const std::shared_ptr<Model>& aModel);
    ConParamSPtr paramDescInt64(const ConParamDescSPtr& aParamDesc);
    ConParamSPtr paramDescBool(const ConParamDescSPtr& aParamDesc);
  };
  
}// end namespace Model

