//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 5/10/2018
// Descriptor for scalar parameters.
//

#pragma once

// Base class
#include "ModelObjectDescriptorParameter.hpp"

// Constraint parameter descriptor
#include "ConstraintParameterDescriptor.hpp"

namespace Model {
  
  class MODEL_EXPORT_CLASS ScalarParameterDescriptor : public ModelObjectDescriptorParameter {
  public:
    /// Value represented sas a constraint parameter descriptor
    using ValueDescSPtr = std::shared_ptr<ConstraintParameterDescriptor>;
    
  public:
    ScalarParameterDescriptor(const std::string& aID, BaseType aBaseType);
    
    /// Sets the descriptor of the scalar value
    void setValue(const ValueDescSPtr& aScalar) { pScalar = aScalar; };
    
    /// Returns the scalar value
    inline ValueDescSPtr getValue() const { return pScalar; }
    
  private:
    /// Descriptor of the scalar value
    ValueDescSPtr pScalar;
  };
  
}// end namespace Model

