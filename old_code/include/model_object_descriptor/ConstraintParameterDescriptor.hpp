//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/15/2017
//
//

#pragma once

#include "ModelExportDefs.hpp"

// Scalar type defs
#include "DomainTypeDefs.hpp"
#include "DomainDefs.hpp"

#include <string>
#include <vector>
#include <memory>
#include <boost/logic/tribool.hpp>

namespace Model {
  
  /*
	 * Constraint parameter descriptor describes any
	 * parameter of a constraint relation.
	 * Class diagram:
	 *       +--------------------------------+
	 *       |                                |
	 *       | ConstraintParameterDescriptor  |
	 *       |                                |
	 *       +--------------------------------+
	 *             ^                   ^
	 *             |                   |
	 *       +-----------+       +-----------+
	 *       |           |       |           |
	 *       | ParamLeaf |-----<>| ParamComp |
	 *       |           |       |           |
	 *       +-----------+       +-----------+
	 *         ^   ^   ^
	 *         |   |   |
	 *         |   |   +-----------------------------+ 
	 *         |   +--------------------+            |
	 *         |                        |            |
	 *       +------------+       +------------+    ...
	 *       |            |       |            |
	 *       | ParamInt64 |       | ParamVarID |
	 *       |            |       |            |
	 *       +------------+       +------------+
	 */
	 
  
  class MODEL_EXPORT_CLASS ConstraintParameterDescriptor {
  public:
    // Types of constraint parameters 
    enum ParameterType : int {
        PT_VAR_ID = 0
      , PT_VAR_SUBSCRIPT
      , PT_REF_ID
      , PT_INT64
      , PT_BOOL
      , PT_VOID
    };
    
    virtual ~ConstraintParameterDescriptor() = default;
    
		/// Returns true if the parameter is composed by sub-parameters,
		/// false otherwise
    virtual bool isComposite() const = 0;
    
  protected:
		ConstraintParameterDescriptor() = default;
  };
  
  class MODEL_EXPORT_CLASS ParamLeaf : public ConstraintParameterDescriptor {
  public:
    virtual ~ParamLeaf() = default;
    
    inline ParameterType getParamType() const
    {
      return pParamType;
    }
    
    inline bool isComposite() const override
    {
      return false;
    }
    
  protected:
		ParamLeaf(ConstraintParameterDescriptor::ParameterType aType);
    
  private:
		ParameterType pParamType;
  };
  
  class MODEL_EXPORT_CLASS ParamComp : public ConstraintParameterDescriptor {
	public:
		using ParamDescSPtr = std::shared_ptr<ConstraintParameterDescriptor>;
		using ChildrenParam = std::vector<ParamDescSPtr>;
		using ChildrenParamIter = ChildrenParam::iterator;
		using ChildrenParamCIter = ChildrenParam::const_iterator;

    enum ParamCompType : short {
      PCT_ARRAY = 0
    };

  public:
    inline ParamCompType getParamCompType() const
    {
      return pParamType;
    }
    
    inline void addParamDescriptor(const ParamDescSPtr& aParamDesc)
    {
			pChildren.push_back(aParamDesc);
    }
    
    inline std::size_t numChildren() const
    {
      return pChildren.size();
    }
    
		inline ParamDescSPtr getChild(std::size_t aIdx) const
    {
      return pChildren.at(aIdx);
    }
    
    inline bool isComposite() const override
    {
      return true;
    }

		/// Range-based for-loop begin
		ChildrenParamIter begin();
		ChildrenParamCIter begin() const;

		/// Range-based for-loop end
		ChildrenParamIter end();
		ChildrenParamCIter end() const;

  protected:
    ParamComp(ParamCompType aPType);
    
  private:
    /// Composite parameter type
    ParamCompType pParamType;
    
    /// Children of this composite parameter
    ChildrenParam pChildren;
  };
  
}// end namespace Model
