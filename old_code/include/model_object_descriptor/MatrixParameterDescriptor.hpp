//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 11/24/2017
// Descriptor for matrix parameters.
// It describes a matrix parameter object in terms
// of the elements of the matrix.
// The elements are constraint parameter descriptors.
// In practice, it describes a matrix lvalue
// that can be used by any constraint.
// For example:
// x:[1, 2, 3]
// y:[var1, var2, var3]
// The above are lvalues.
// Note that this are used as reference as arguments
// of a constraints.
// For example:
// x:[1, -1]
// v:[q1, q2]
// lin_eq(x, v, 1)
// which is different from rvalues defined directly
// as a constraint argument:
// lin_eq([1, -1], v, 1)
// where [1, -1] is an rvalue and it is not described by
// a MatrixParameterDescriptor.
//

#pragma once

// Base class
#include "ModelObjectDescriptorParameter.hpp"

// Constraint parameter descriptor
#include "ConstraintParameterDescriptor.hpp"

namespace Model {
  
  class MODEL_EXPORT_CLASS MatrixParameterDescriptor : public ModelObjectDescriptorParameter {
  public:
    using ConParamDescSPtr = std::shared_ptr<ConstraintParameterDescriptor>;
    using MatrixRow = std::vector<ConParamDescSPtr>;
    
  public:
    MatrixParameterDescriptor(const std::string& aID, BaseType aBaseType);
    
    // Adds a new row to the matrix.
    // @note dimensions must match
    void addRow(const MatrixRow& aRow);

    inline std::size_t numCols() const
    {
      return pRows.empty() ? 0 : pRows[0].size();
    }
    
    inline std::size_t numRows() const
    {
      return pRows.size();
    }
    
    inline ConParamDescSPtr getElement(std::size_t aRow, std::size_t aCol) const
    {
      return pRows.at(aRow).at(aCol);
    }
    
  private:
    // Matrix of constraint parameter descriptors
    std::vector<MatrixRow> pRows;
  };
  
}// end namespace Model

