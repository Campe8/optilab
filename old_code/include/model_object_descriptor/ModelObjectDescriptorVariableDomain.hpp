//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 03/27/2017
//
// Model object descriptors represent meta-information
// given by the parser in order to create model objects
// describing variables.
//

#pragma once

// Base class
#include "ModelObjectDescriptor.hpp"

#include "VariableDefs.hpp"

#include <memory>

// Forward declarations
namespace Model {
  
  class ModelObjectDescriptorVariable;
  class ModelObjectDescriptorVariableDomain;
  
}// end namespace Model

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelObjectDescriptorVariableType {
  public:
    /// Type encoded by the variable described by ModelObjectDescriptorVariable
    bool isComposite() const;
    
    virtual ~ModelObjectDescriptorVariableType() = default;
    
    inline Core::VariableTypeClass variableTypeClass() const
    {
      return pVarDescriptorTypeClass;
    }
    
  protected:
    friend class ModelObjectDescriptorVariableDomain;
    
    ModelObjectDescriptorVariableType(Core::VariableTypeClass aVariableTypeClass);
    
  private:
    /// Variable type class this descriptor type is encoding for
    Core::VariableTypeClass pVarDescriptorTypeClass;
  };
  
  class MODEL_EXPORT_CLASS ModelObjectDescriptorVariableDomain {
  public:
    // Domain information encoded by the variable described by ModelObjectDescriptorVariable
    enum DescriptorVariableDomainConfig {
        DOM_CONFIG_SINGLETON
      , DOM_CONFIG_BOUNDS
      , DOM_CONFIG_SET
    };
    
    virtual ~ModelObjectDescriptorVariableDomain() = default;
    
    /// Returns a clone of this object
    virtual ModelObjectDescriptorVariableDomain* clone() = 0;
    
    inline bool isSingleton() const
    {
      return descriptorVariableDomainConfig() == DescriptorVariableDomainConfig::DOM_CONFIG_SINGLETON;
    }
    
    inline DescriptorVariableDomainConfig descriptorVariableDomainConfig() const
    {
      return pDomainConfig;
    }
    
    ModelObjectDescriptorVariableType* type() const
    {
      return pVarType.get();
    }
    
  protected:
    ModelObjectDescriptorVariableDomain(DescriptorVariableDomainConfig aDomainConfig, Core::VariableTypeClass aClass);
    
    /// Resets domain config type
    inline void resetDescriptorVariableDomainConfig(DescriptorVariableDomainConfig aDomainConfig)
    {
      pDomainConfig = aDomainConfig;
    }
    
  private:
    /// Domain description configuration
    DescriptorVariableDomainConfig pDomainConfig;
    
    /// Type of this domain
    std::shared_ptr<ModelObjectDescriptorVariableType> pVarType;
  };
  
}// end namespace Model
