//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 03/27/2017
//
// Model object descriptors represent meta-information
// given by the parser in order to create model objects
// describing variables.
//

#pragma once

// Base class
#include "ModelObjectDescriptor.hpp"

#include "ModelObjectDescriptorVariableDomain.hpp"

// Data structure for composite descriptor variable
#include "DMatrix2D.hpp"

#include "VariableDefs.hpp"
#include "VariableSemantic.hpp"
#include "DomainElement.hpp"

#include <utility>

// Forward declarations
namespace Model {
  class ModelObjectDescriptorVariable;
  
  typedef std::unique_ptr<ModelObjectDescriptorVariable> ModelObjectDescriptorVariableUPtr;
  typedef std::shared_ptr<ModelObjectDescriptorVariable> ModelObjectDescriptorVariableSPtr;
}// end namespace Search

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelObjectDescriptorVariable : public ModelObjectDescriptor {
  public :
    using Semantic = Core::VariableSemanticUPtr;
    using SemanticList = std::vector<std::pair<std::string, Core::VariableSemanticSPtr>>;
    using Domain = std::unique_ptr<ModelObjectDescriptorVariableDomain>;
    
  public:
    virtual ~ModelObjectDescriptorVariable() = default;
    
    /// Returns true if this descriptor is for composite variable,
    /// false otherwise
    virtual bool isComposite() const = 0;
    
    /// Release default semantic
    inline Semantic releaseSemantic()
    {
      return std::move(pVariableSemantic);
    }
    
    inline const Core::VariableSemantic* getDefaultSemantic() const
    {
      if(pVariableSemantic) return pVariableSemantic.get();
      return nullptr;
    }
    
    /// Sets the list of the semantics this variable can assume
    /// w.r.t. the running search task.
    /// @note there can be different semantics for the same variable
    /// depending on the search tree the variable is in.
    /// In turn, the search tree is defined by the current search semantic
    /// which is identified by its ID
    inline void setSemanticList(const SemanticList& aSList)
    {
      pSemanticList = aSList;
    }
    
    inline SemanticList getSemanticList() const
    {
      return pSemanticList;
    }
    
  protected:
    /// Constructor takes a variable semantic which represents the default
    /// semantic of the variable.
    /// The same variable can change its semantic over time
    /// w.r.t. to the running search semantic
    ModelObjectDescriptorVariable(Semantic aSemantic);
    
  private:
    /// Default semantic used for this variable
    Semantic pVariableSemantic;
    
    /// List of the semantics this variable can assume
    SemanticList pSemanticList;
  };
  
  class MODEL_EXPORT_CLASS ModelObjectDescriptorPrimitiveVariable : public ModelObjectDescriptorVariable {
  public:
    ModelObjectDescriptorPrimitiveVariable();
    ModelObjectDescriptorPrimitiveVariable(Domain aDomain, Semantic aSemantic);
    
    inline bool isComposite() const override
    {
      return false;
    }
    
    inline ModelObjectDescriptorVariableDomain* domainDescriptor() const
    {
      return pDomain.get();
    }
    
  private:
    /// Descriptor domain for primitive variables
    std::shared_ptr<ModelObjectDescriptorVariableDomain>  pDomain;
  };
  
  class MODEL_EXPORT_CLASS ModelObjectDescriptorCompositeVariable : public ModelObjectDescriptorVariable {
  public:
    ModelObjectDescriptorCompositeVariable(std::size_t aRows, std::size_t aCols, Semantic aVariableSemantic);
    
    virtual ~ModelObjectDescriptorCompositeVariable();
    
    inline bool isComposite() const override
    {
      return true;
    }
    
    /// Adds a sub-component to this composite descriptor variable at position ["row", "col"].
    /// "aDescriptorComponentVariable" can be either a ModelObjectDescriptorPrimitiveVariable
    /// or another ModelObjectDescriptorCompositeVariable.
    /// @note "row" and "col" must be consistent with the sizes specified in the constructor
    void addDescriptorVariable(ModelObjectDescriptorVariableUPtr aVariable, std::size_t aRow, std::size_t aCol);
    
    inline const Core::DMatrix2D<ModelObjectDescriptorVariable*>* getCompositeDescriptor() const
    {
      return &pCompositeVariable;
    }
    
  private:
    Core::DMatrix2D<ModelObjectDescriptorVariable*> pCompositeVariable;
  };
  
}// end namespace Model
