//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 10/23/2017
//
//

#pragma once

#include "ConstraintParameterDescriptor.hpp"

#include <vector>

namespace Model {

  class MODEL_EXPORT_CLASS ParamArray : public ParamComp {
  public:
    // Constructors for type-specific parameters
		ParamArray(const std::vector<INT_64_T>& aVec);
    ParamArray(const std::vector<BOOL_T>& aVec);
		ParamArray(const std::vector<std::string>& aVec);
    
    // Constructor for arrays of generic parameters
    ParamArray(const std::vector<ParamDescSPtr>& aVec);
    
  private:
    /// Creates a parameter from each element of the vector of objects of type T
    /// and adds it to the composite object
    template<class T, class U>
    void addParametersToCompositeObject(const std::vector<T>& aParamVector)
    {
      for (auto& param : aParamVector)
      {
        addParamDescriptor(std::make_shared<U>(param));
      }
    }
    
  };
  
}// end namespace Model
