//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 04/09/2017
//
// Model object descriptors represent meta-information
// given by the parser in order to create model objects
// describing search engines.
//

#pragma once

// Base class
#include "ModelObjectDescriptor.hpp"

#include "SearchDefs.hpp"
#include "SearchHeuristicDefs.hpp"

#include <string>
#include <vector>

// Forward declarations
namespace Model {
  class ModelObjectDescriptorSearchSemantic;
  
  typedef std::unique_ptr<ModelObjectDescriptorSearchSemantic> ModelObjectDescriptorSearchSemanticUPtr;
  typedef std::shared_ptr<ModelObjectDescriptorSearchSemantic> ModelObjectDescriptorSearchSemanticSPtr;
}// end namespace Search

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelObjectDescriptorSearchSemantic : public ModelObjectDescriptor {
  public:
    ModelObjectDescriptorSearchSemantic(Search::SearchEngineStrategyType aStrategyType,
                                        Search::VariableChoiceMetricType aVarMetricType,
                                        Search::ValueChoiceMetricType aValeMetricType);
    
    /// Sets an ID for this search semantic
    inline void setSearchSemanticID(const std::string& aID)
    {
      pID = aID;
    }
    
    /// Returns the ID of this search semantic
    inline std::string getSearchSemanticID() const
    {
      return pID;
    }
    
    /// Sets the list of IDs of variables that are semantically dependent on this search semantic
    inline void setSemanticallyDependentVarList(const std::vector<std::string>& aList)
    {
      pVarIDList = aList;
    }
    
    /// Returns the list of variables semantically dependent on this search semantic
    inline std::vector<std::string> getSemanticallyDependentVarList() const
    {
      return pVarIDList;
    }
    
    inline Search::SearchEngineStrategyType searchStrategyType() const
    {
      return pSearchStrategtType;
    }
    
    inline Search::VariableChoiceMetricType variableChoiceMetricType() const
    {
      return pVarChoiceMetric;
    }
    
    inline Search::ValueChoiceMetricType valueChoiceMetricType() const
    {
      return pValChoiceMetric;
    }
    
  private:
    /// Search semantic ID
    std::string pID;
    
    /// Search strategy type
    Search::SearchEngineStrategyType pSearchStrategtType;
    
    /// Choice metrics: variable
    Search::VariableChoiceMetricType pVarChoiceMetric;
    
    /// Choice metrics: value
    Search::ValueChoiceMetricType pValChoiceMetric;
    
    /// List of IDs of the variables semantically dependent on this search semantic
    std::vector<std::string> pVarIDList;
  };
  
}// end namespace Model
