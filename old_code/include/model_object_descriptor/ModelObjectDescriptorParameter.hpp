//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 11/24/2017
//
// Model object descriptors represent meta-information
// given by the parser in order to create model objects
// describing model parameters, e.g., arrays of numbers.
//

#pragma once

// Base class
#include "ModelObjectDescriptor.hpp"
#include "ConstraintParameterDescriptor.hpp"

#include <string>

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelObjectDescriptorParameter : public ModelObjectDescriptor {
  public:
    /// Base parameter type, for example, PT_INT64 in x:[1,2,3].
    /// The base type is a "ParameterType" of constraint parameter descriptors.
    /// Model parameters such as x:[1,2,3] are eventually used to specify
    /// parameters in the constraint scope.
    /// Other parameters usage is handled during IR transformations.
    using BaseType = ConstraintParameterDescriptor::ParameterType;
    
    enum ParameterType : int {
        PT_SCALAR = 0
      , PT_MATRIX
    };
    
  public:
    inline ParameterType getParameterType() const
    {
      return pType;
    }
    
    inline std::string getID() const
    {
      return pID;
    }
    
    inline BaseType getBaseType() const
    {
      return pBaseType;
    }
    
  protected:
    ModelObjectDescriptorParameter(ParameterType aPType, const std::string& aID, BaseType aBaseType);
    
  private:
    /// Descriptor parameter type
    ParameterType pType;
    
    /// Parameter ID
    std::string pID;
    
    /// Base parameter type
    BaseType pBaseType;
  };
  
}// end namespace Model

