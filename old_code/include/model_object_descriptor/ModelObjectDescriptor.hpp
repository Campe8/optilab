//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 03/27/2017
//
// Model object descriptors represent meta-information
// given by the parser in order to create model objects.
//

#pragma once

#include "ModelExportDefs.hpp"

#include "ModelDefs.hpp"

#include <memory>
#include <cassert>

// Forward declarations
namespace Model {
  class ModelObjectDescriptor;
  typedef std::shared_ptr<ModelObjectDescriptor> ModelObjectDescriptorSPtr;
}// end namespace Search

namespace Model {
  
  class MODEL_EXPORT_CLASS ModelObjectDescriptor {
  public:
    virtual ~ModelObjectDescriptor() = default;
    
    inline ModelObjectDescriptorClass getModelObjectDescriptorClass() const
    {
      return pModelObjectDescriptorClass;
    }
    
  protected:
    ModelObjectDescriptor(ModelObjectDescriptorClass aModelDescriptorClass)
    : pModelObjectDescriptorClass(aModelDescriptorClass)
    {
    }
    
  private:
    ModelObjectDescriptorClass pModelObjectDescriptorClass;
  };
  
}// end namespace Search
