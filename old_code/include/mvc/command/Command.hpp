//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 06/15/2017
//
// Command base class.
// Commands are owned by the controller in the MVC pattern.
//

#pragma once

#include "MVCExportDefs.hpp"
#include "MVCMacro.hpp"

#include "CommandDefs.hpp"

#include "MVCEventFactory.hpp"

#include <tuple>
#include <memory>

#define ARG_MODEL 0
#define ARG_VIEW 1
#define ARG_CONTROLLER 2

// Forward declarations
namespace MVC {
  class MVCModel;
  class MVCView;
  class MVCController;
}// end namespace MVC

namespace MVC {
  
  class MVC_EXPORT_CLASS Command {
  public:
    using ModelPtr = std::shared_ptr<MVCModel>;
    using ViewPtr =  std::shared_ptr<MVCView>;
    using ControllerPtr = MVCController*;
    using EventFactoryPtr = MVCEventFactory*;
    
    // Set of invokers and receivers.
    // @note the MVCController is the owner of the commands.
    // It passes its instace as a raw pointer to the command
    using CommandArgumentSet = std::tuple<ModelPtr, ViewPtr, ControllerPtr>;
    
  public:
    virtual ~Command() = default;
    
    /// Executes this command
    virtual void execute() = 0;
    
  protected:
    friend class CommandFactory;
    
    Command();
    
    /// Returns true if all command arguments are set,
    /// false otherwise
    inline bool hasArguments() const
    {
      return getView() && getModel() && getController();
    }
    
    /// Sets the arguments of the command as invokers and receivers
    inline void setArguments(const CommandArgumentSet& aArgSet)
    {
      pArguments = aArgSet;
    }
    
    /// Returns the view receiver of the command
    inline ViewPtr getView() const
    {
      return std::get<ARG_VIEW>(pArguments);
    }
    
    /// Returns the model receiver of the command
    inline ModelPtr getModel() const
    {
      return std::get<ARG_MODEL>(pArguments);
    }
    
    /// Returns the model controller invoker of the command
    inline ControllerPtr getController() const
    {
      return std::get<ARG_CONTROLLER>(pArguments);
    }
    
    inline EventFactoryPtr eventFactory() const
    {
      return pEventFactory.get();
    }
    
    /// Notifies the controller, i.e., the invoker of the command,
    /// about an error that happened during execution of the command.
    /// The notification carries the message "aMsg" that has been generated
    /// by the event actor "aSrc"
    void notifyInvokerOnError(const char* aMsg, MVCEvent::MVCEventActor aSrc);
    
  private:
    /// Command arguments
    CommandArgumentSet pArguments;
    
    /// Event factory to communicate to model
    std::unique_ptr<MVCEventFactory> pEventFactory;
  };
  
}//end namespace MVC
