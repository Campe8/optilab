//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 06/16/2017
//
// Factory for commands
//

#pragma once

#include "MVCExportDefs.hpp"
#include "MVCMacro.hpp"

// Commands
#include "CommandInc.hpp"
#include "CommandDefs.hpp"

#include <unordered_map>
#include <memory>

namespace MVC {
  
  class MVC_EXPORT_CLASS CommandFactory {
  public:
    CommandFactory();
    
    /// Sets the arguments - invoker and receiver - of the commands
    inline void setCommandArguments(const Command::CommandArgumentSet& aCommArgs)
    {
      pCommandArgSet = aCommArgs;
    }
    
    /// Returns a command of type "aCmdId" that executes the statment "aStmt"
    std::shared_ptr<Command> command(CommandId aCmdId, const CLI::CLIStmt& aStmt);
    
  private:
    /// Instance of the mvc entities the commands will work with.
    /// @note some commands need the model to execute their functionality,
    /// for example, fcn call command performs the action of executing the
    /// associated fcn which, in turn, may need the view to be called on.
    /// In this situation, the receiver is the view
    Command::CommandArgumentSet pCommandArgSet;
    
    /// Register of commands
    std::unordered_map<CommandIdType, std::shared_ptr<Command>> pCmdRegister;
    
    /// Registers all commands
    void registerCommands();
    
    /// Returns a registered instance of a command of type "aCmdId".
    /// @note returns nullptr if no such command has been registered
    std::shared_ptr<Command> getCommand(CommandId aCmdId);
    
    /// Returns a command evaluate statement
    std::shared_ptr<CommandEvalStmt> commandEvalStmt(const CLI::CLIStmt& aStmt);
    
    /// Returns a command function call
    std::shared_ptr<CommandFcnCall> commandFcnCall(const CLI::CLIStmt& aStmt);
  };
  
}//end namespace MVC

