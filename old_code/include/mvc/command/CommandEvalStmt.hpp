//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 06/15/2017
//
// Command evaluate statement.
//

#pragma once

// Base class
#include "Command.hpp"

#include <string>
#include <utility>
#include <memory>

namespace MVC {
  
  class MVC_EXPORT_CLASS CommandEvalStmt : public Command {
  public:
    virtual ~CommandEvalStmt() = default;
    
    /// Sets the stmt to evaluate
    inline void setStmt(const CLI::CLIStmt& aStmt)
    {
      pStmt = aStmt;
    }
    
    /// Executes this command
    void execute() override;
    
  protected:
    friend class CommandFactory;
    
    /// Constructor: visible by CommandFactory
    CommandEvalStmt() = default;
    
  private:
    /// Model name
    std::string pStmtId;
    
    /// Statement to execute by MVC model
    CLI::CLIStmt pStmt;
  };

}//end namespace MVC
