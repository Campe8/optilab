//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 06/15/2017
//

#pragma once

namespace MVC {
  typedef unsigned int CommandIdType;
  
  enum CommandId : CommandIdType {
      CMD_EVAL_STMT = 0
    , CMD_FCN_CALL
    , CMD_UNDEF // must be last
  };
  
} // end namespace MVC
