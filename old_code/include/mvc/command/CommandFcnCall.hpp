//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 08/05/2017
//
// Command function call.
//

#pragma once

// Base class
#include "Command.hpp"

#include "FunctionRegister.hpp"

#include "MVCEventFactory.hpp"

#include <string>
#include <vector>

namespace MVC {
  
  class MVC_EXPORT_CLASS CommandFcnCall : public Command {
  public:
    virtual ~CommandFcnCall() = default;
    
    /// Sets the callback function to execute
    /// @input "aFcn": name of the function to callback
    /// @input "aArgs": sorted vector or (string) arguments for the function to callback
    void setCallbackFcn(const std::string& aFcn, const std::vector<std::string>& aArgs);
    
    /// Executes this command
    void execute() override;
    
  protected:
    friend class CommandFactory;
    
    /// Constructor: visible by CommandFactory
    CommandFcnCall();
    
  private:
    /// Callback function name
    std::string pFcnName;
    
    /// Sorted vector of arguments to the function
    std::vector<std::string> pArgs;
    
    /// Function register
    std::unique_ptr<FunctionRegister> pFcnRegister;
  };
  
}//end namespace MVC
