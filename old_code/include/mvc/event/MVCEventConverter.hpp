//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/11/2017
//
// Converter of MVC events.
//

#pragma once

#include "MVCExportDefs.hpp"
#include "MVCDefs.hpp"

#include "MVCEvent.hpp"
#include "MVCEventFactory.hpp"

#include <memory>

namespace MVC {
  
  class MVC_EXPORT_CLASS MVCEventConverter {
  public:
		MVCEventConverter();
    
    /// Converts "aEvent" such that it is a "standard" event according
		/// to the semantic of the MVC
		std::shared_ptr<MVCEvent> convert(const std::shared_ptr<MVCEvent>& aEvent);

	private:
		/// Event factory
		std::unique_ptr<MVCEventFactory> pEventFactory;
  };
  
}//end namespace MVC
