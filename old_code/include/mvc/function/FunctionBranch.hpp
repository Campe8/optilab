//
// Copyright OptiLab 2017-2018. All rights reserved.
//
// Created by Federico Campeotto on 2/18/2018
//
// "branch" function: post branching relations
// on the model defining the search strategy.
//

#pragma once

// Base class
#include "Function.hpp"

namespace MVC {
  
  class FunctionBranch : public Function {
  public:
    FunctionBranch();
    
    bool requiresFcnObjectAsArgument() const override;
    
    Interpreter::DataObject callback(const std::vector<Interpreter::DataObject>& aArgs) override;
    
    std::string name() const override;
    
  private:
    /// Checks consistency of the arguments given to the branch function
    void checkFunctionArguments(const Interpreter::DataObject& aDO);
  };
  
} // end namespace MVC
