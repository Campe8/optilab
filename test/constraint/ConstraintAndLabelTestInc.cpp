#include "ConstraintAndLabelTestInc.hpp"

#include "SearchStrategy.hpp"
#include "VariableSemanticDecision.hpp"
#include "DomainElementInt64.hpp"

using namespace Core;
using namespace Search;
using namespace Semantics;

ConstraintAndLabelTest::ConstraintAndLabelTest()
: pVarFactory(new Core::VariableFactory())
, pInputOrderVar(0)
, pEngine(nullptr)
{
}

VariableSPtr ConstraintAndLabelTest::getVariable(const std::pair<INT_64, INT_64>& aBounds)
{
  VariableSemanticUPtr varSem(new VariableSemanticDecision());
  varSem->setInputOrder(pInputOrderVar++);
  return VariableSPtr(pVarFactory->variableInteger(aBounds.first, aBounds.second, std::move(varSem)));
}//getVariable

void ConstraintAndLabelTest::setEngine(const std::set<Core::VariableSPtr>& aBranchingVariableSet)
{
  // Set branching variables and create environment
  pBranchingVariableSet = aBranchingVariableSet;
  std::vector<SearchEnvironment::VarSem> varList;
  for(auto& var : pBranchingVariableSet)
  {
    varList.push_back({var, VariableSemanticSPtr(var->variableSemantic()->clone())});
  }
  
  // Create environment
  auto env = new SearchEnvironment(varList, VariableChoiceMetricType::VAR_CM_INPUT_ORDER, ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MIN);
  Search::DepthFirstSearch::EnvironmentList envPtrList(1);
  envPtrList[0].reset(env);
  
  // Create semantics
  std::unique_ptr<SearchSemantics> searchSemantics(new SearchSemantics());
  
  auto dfs = std::make_shared<Search::DepthFirstSearch>(std::move(searchSemantics), envPtrList);
  
  // Create search strategy wrapper around the base search
  auto ss = std::unique_ptr<Search::SearchStrategy>(new Search::SearchStrategy(pDFS));
  
  // Create search engine on the search strategy
  pEngine.reset(new Search::SearchEngine(std::move(ss)));
}//getEngine

void ConstraintAndLabelTest::setConstraint(std::shared_ptr<Constraint> aConstraint)
{
  assert(aConstraint && pDFS);
  pDFS->registerConstraint(aConstraint);
}// setConstraint

SearchStatus ConstraintAndLabelTest::runEngine()
{
  assert(pEngine && pDFS);
  assert(pDFS->getNumRootNodes() == 1);
  auto rootNode = pDFS->getRootNode(0);
  return pEngine->label(rootNode);
}

std::vector<std::pair<INT_64, INT_64>> ConstraintAndLabelTest::getSolution(std::size_t aSolutionIdx)
{
  assert(pDFS && aSolutionIdx <= numOfSolutions());
  auto solution = pDFS->solutionManager()->getSolution(aSolutionIdx);
  
  std::vector<std::pair<INT_64, INT_64>> intSolution;
  for(auto& var : pBranchingVariableSet)
  {
    std::pair<INT_64, INT_64> varBounds;
    varBounds.first  = DomainElementInt64::cast(solution.at(var->getVariableNameId())[0].first)->getValue();
    varBounds.second = DomainElementInt64::cast(solution.at(var->getVariableNameId())[0].second)->getValue();
    intSolution.push_back(varBounds);
  }
  return intSolution;
}//getSolution
