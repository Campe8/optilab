#include "PropagatorTest.hpp"

namespace Core {
  
  PropagatorTestClass::PropagatorTestClass(std::size_t aContextSize,
                                           PropagatorStrategyType aStrategyType,
                                           PropagationPriority aPriority,
                                           PropagatorSemanticType aSemanticType,
                                           PropagatorSemanticClass aSemanticClass)
  : Propagator(PropagatorSemantics(aContextSize, aStrategyType, aPriority, aSemanticType, aSemanticClass))
  {
  }
  
  PropagatorTestClass::~PropagatorTestClass()
  {
  }
  
}// end namespace core
