
#include "utest_globals.hpp"

#include "FilteringAlgorithmELEMENT.hpp"

#include "ConstraintTestInc.hpp"

OPTILAB_TEST(FilteringAlgorithmELEMENT, ELEMENT)
{
  using namespace Core;
  
  FilteringAlgorithmELEMENT filter;
  DomainIntPtr domainOutput = getDomainInt(-10, +10);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  
  // vec[2] = 4
  std::vector<INT_64> vec = {2, 1, 2, 4, 6, 8, 10};
  ArrayPtr array = getArray(vec);
  params.setDomainElementArray(array);
  
  auto domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), getElem(+4).get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), getElem(+4).get()));
}//ELEMENT
