
#include "utest_globals.hpp"

#include "FilteringAlgorithmEQ.hpp"

#include "ConstraintTestInc.hpp"

OPTILAB_TEST(FilteringAlgorithmEQ, EQ1)
{
  using namespace Core;
  
  FilteringAlgorithmEQ filter;
  DomainIntPtr domainOutput = getDomainInt(-10, +10);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  
  ElemPtr elemPos5 = getElem(+5);
  auto array = getArray(+5, +5);
  params.setDomainElementArray(array);
  
  auto domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemPos5.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos5.get()));
  
  DomainIntPtr domainOutput2 = getDomainInt(-10, +10);
  params.setOutputDomain(domainOutput2.get());
  
  ElemPtr elemNeg2 = getElem(-2);
  params.getDomainElementArray()->assignElementToCell(0, elemNeg2.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg2.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemNeg2.get()));
  
  DomainIntPtr domainOutput3 = getDomainInt(-10, +10);
  params.setOutputDomain(domainOutput3.get());
  ElemPtr elemNeg11 = getElem(-11);
  params.getDomainElementArray()->assignElementToCell(0, elemNeg11.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 0);
}//EQ1

OPTILAB_TEST(FilteringAlgorithmEQ, EQ2)
{
  using namespace Core;
  
  FilteringAlgorithmEQ filter;
  DomainIntPtr domainOutput = getDomainInt(-1000, +1000);
  
  FilteringParameters params;
  ElemPtr elemPos5 = getElem(+5);
  params.setOutputDomain(domainOutput.get());
  auto array = getArray(+5, +5);
  params.setDomainElementArray(array);
  
  auto domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemPos5.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos5.get()));
}//EQ2

OPTILAB_TEST(FilteringAlgorithmEQ, EQ3)
{
  using namespace Core;
  
  FilteringAlgorithmEQ filter;
  
  std::vector<INT_64> set;
  for(INT_64 elem = -10; elem <= 10; elem += 2)
  {
    set.push_back(elem);
  }
  DomainIntPtr domainOutput = getDomainInt(set);
  
  FilteringParameters params;
  ElemPtr elemPos6 = getElem(+6);
  params.setOutputDomain(domainOutput.get());
  auto array = getArray(+6, +6);
  params.setDomainElementArray(array);
  
  auto domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemPos6.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos6.get()));
  
  DomainIntPtr domainOutput2 = getDomainInt(set);
  ElemPtr elemPos5 = getElem(+5);
  params.setOutputDomain(domainOutput2.get());
  params.getDomainElementArray()->assignElementToCell(0, elemPos5.get());
  
  domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 0);
}//EQ3

