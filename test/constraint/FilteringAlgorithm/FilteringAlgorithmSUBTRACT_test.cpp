
#include "utest_globals.hpp"

#include "FilteringAlgorithmSUBTRACT.hpp"

#include "ConstraintTestInc.hpp"

OPTILAB_TEST(FilteringAlgorithmSUBTRACT, SUBTRACTInputDomain)
{
  using namespace Core;
  
  FilteringAlgorithmSUBTRACT filter;
  filter.setSubtractSetType(FilteringAlgorithmSUBTRACT::SubtractSetType::SS_DOMAIN);
  
  DomainIntPtr domainOutput    = getDomainInt(-10, +10);
  DomainIntPtr domainIntersect = getDomainInt(+8, +12);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  
  DomainArrayPtr array = std::make_shared<DomainArray>(1);
  params.setInputDomainArray(array);
  
  array->assignElementToCell(0, domainIntersect.get());
  
  auto domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 18);
  
  ElemPtr elemPos7 = getElem(+7);
  ElemPtr elemNeg10 = getElem(-10);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg10.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos7.get()));
  
  // Subtract bigger domain should empty current domain
  DomainIntPtr domainOutput2    = getDomainInt(-10, +10);
  DomainIntPtr domainIntersect2 = getDomainInt(-12, +12);
  params.setOutputDomain(domainOutput2.get());
  array->assignElementToCell(0, domainIntersect2.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 0);
  
  // Intersect domain with no elements in common should not change current domain
  DomainIntPtr domainOutput3    = getDomainInt(-10, +10);
  DomainIntPtr domainIntersect3 = getDomainInt(+11, +12);
  params.setOutputDomain(domainOutput3.get());
  array->assignElementToCell(0, domainIntersect3.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 21);
  ElemPtr elemPos10 = getElem(+10);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg10.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos10.get()));
  
  // Subtract smaller domain
  DomainIntPtr domainOutput4    = getDomainInt(-10, +10);
  DomainIntPtr domainIntersect4 = getDomainInt(-1, +1);
  params.setOutputDomain(domainOutput4.get());
  array->assignElementToCell(0, domainIntersect4.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 18);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg10.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos10.get()));
  ASSERT_FALSE(domOut->contains(getElem(-1).get()));
  ASSERT_FALSE(domOut->contains(getElem( 0).get()));
  ASSERT_FALSE(domOut->contains(getElem(+1).get()));
}//SUBTRACTInputDomain

OPTILAB_TEST(FilteringAlgorithmSUBTRACT, SUBTRACTInputDomain2)
{
  using namespace Core;
  
  FilteringAlgorithmSUBTRACT filter;
  filter.setSubtractSetType(FilteringAlgorithmSUBTRACT::SubtractSetType::SS_DOMAIN);
  
  DomainIntPtr domainOutput    = getDomainInt(-1000, +1000);
  DomainIntPtr domainSubtract = getDomainInt(+800, +1200);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  DomainArrayPtr array = std::make_shared<DomainArray>(1);
  params.setInputDomainArray(array);
  
  array->assignElementToCell(0, domainSubtract.get());
  
  auto domOut = filter.filter(params);
  
  ElemPtr elemPos799 = getElem(+799);
  ElemPtr elemNeg1000 = getElem(-1000);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg1000.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos799.get()));
  
  // Intersect bigger domain should empty current domain
  DomainIntPtr domainOutput2    = getDomainInt(-1000, +1000);
  DomainIntPtr domainSubtract2 = getDomainInt(-1200, +1200);
  params.setOutputDomain(domainOutput2.get());
  array->assignElementToCell(0, domainSubtract2.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 0);
  
  // Intersect domain with no elements in common should not change current domain
  DomainIntPtr domainOutput3    = getDomainInt(-1000, +1000);
  DomainIntPtr domainSubtract3 = getDomainInt(+1001, +1200);
  params.setOutputDomain(domainOutput3.get());
  array->assignElementToCell(0, domainSubtract3.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 2001);
  ElemPtr elemPos1000 = getElem(+1000);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg1000.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos1000.get()));
  
  // Intersect smaller domain should not change current domain for bounds domain
  DomainIntPtr domainOutput4    = getDomainInt(-1000, +1000);
  DomainIntPtr domainSubtract4 = getDomainInt(-100, +100);
  params.setOutputDomain(domainOutput4.get());
  array->assignElementToCell(0, domainSubtract4.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 2001);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg1000.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos1000.get()));
}//SUBTRACTInputDomain2

OPTILAB_TEST(FilteringAlgorithmSUBTRACT, SUBTRACTInputDomain3)
{
  using namespace Core;
  
  FilteringAlgorithmSUBTRACT filter;
  filter.setSubtractSetType(FilteringAlgorithmSUBTRACT::SubtractSetType::SS_DOMAIN);
  
  std::vector<INT_64> set;
  for(INT_64 elem = -500; elem <= 500; elem += 2)
  {
    set.push_back(elem);
  }
  DomainIntPtr domainOutput = getDomainInt(set);
  DomainIntPtr domainSubtract = getDomainInt(+494, +502);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  
  DomainArrayPtr array = std::make_shared<DomainArray>(1);
  params.setInputDomainArray(array);
  
  array->assignElementToCell(0, domainSubtract.get());
  
  auto domOut = filter.filter(params);
  
  ElemPtr elemPos492 = getElem(+492);
  ElemPtr elemNeg500 = getElem(-500);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg500.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos492.get()));
  
  // Subtract bigger domain should empty current domain
  
  DomainIntPtr domainOutput2    = getDomainInt(set);
  DomainIntPtr domainSubtract2 = getDomainInt(-501, 501);
  params.setOutputDomain(domainOutput2.get());
  array->assignElementToCell(0, domainSubtract2.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 0);
  
  // Intersect domain with no elements in common should not change current domain
  DomainIntPtr domainOutput3    = getDomainInt(set);
  DomainIntPtr domainSubtract3 = getDomainInt(+502, +505);
  params.setOutputDomain(domainOutput3.get());
  array->assignElementToCell(0, domainSubtract3.get());
  
  domOut = filter.filter(params);
  ElemPtr elemPos500 = getElem(+500);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg500.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos500.get()));
}//SUBTRACTInputDomain3


OPTILAB_TEST(FilteringAlgorithmSUBTRACT, SUBTRACTDomainElementArray)
{
  using namespace Core;
  
  FilteringAlgorithmSUBTRACT filter;
  filter.setSubtractSetType(FilteringAlgorithmSUBTRACT::SubtractSetType::SS_DOMAIN_ELEMENT_ARRAY);
  
  DomainIntPtr domainOutput = getDomainInt(-10, +10);
  ArrayPtr domainIntersect  = getArray(+8, +12);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  params.setDomainElementArray(domainIntersect);
  
  auto domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 18);
  
  ElemPtr elemPos7 = getElem(+7);
  ElemPtr elemNeg10 = getElem(-10);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg10.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos7.get()));
  
  // Subtract bigger domain should empty current domain
  DomainIntPtr domainOutput2    = getDomainInt(-10, +10);
  ArrayPtr domainIntersect2 = getArray(-12, +12);
  params.setOutputDomain(domainOutput2.get());
  params.setDomainElementArray(domainIntersect2);
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 0);
  
  // Intersect domain with no elements in common should not change current domain
  DomainIntPtr domainOutput3    = getDomainInt(-10, +10);
  ArrayPtr domainIntersect3 = getArray(+11, +12);
  params.setOutputDomain(domainOutput3.get());
  params.setDomainElementArray(domainIntersect3);
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 21);
  ElemPtr elemPos10 = getElem(+10);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg10.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos10.get()));
  
  // Subtract smaller domain
  DomainIntPtr domainOutput4    = getDomainInt(-10, +10);
  ArrayPtr domainIntersect4 = getArray(-1, +1);
  params.setOutputDomain(domainOutput4.get());
  params.setDomainElementArray(domainIntersect4);
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 18);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg10.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos10.get()));
  ASSERT_FALSE(domOut->contains(getElem(-1).get()));
  ASSERT_FALSE(domOut->contains(getElem( 0).get()));
  ASSERT_FALSE(domOut->contains(getElem(+1).get()));
}//SUBTRACTDomainElementArray

OPTILAB_TEST(FilteringAlgorithmSUBTRACT, SUBTRACTDomainElementArray2)
{
  using namespace Core;
  
  FilteringAlgorithmSUBTRACT filter;
  filter.setSubtractSetType(FilteringAlgorithmSUBTRACT::SubtractSetType::SS_DOMAIN_ELEMENT_ARRAY);
  
  DomainIntPtr domainOutput    = getDomainInt(-1000, +1000);
  ArrayPtr domainIntersect = getArray(+800, +1200);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  params.setDomainElementArray(domainIntersect);
  
  auto domOut = filter.filter(params);
  
  ElemPtr elemPos799 = getElem(+799);
  ElemPtr elemNeg1000 = getElem(-1000);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg1000.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos799.get()));
  
  // Intersect bigger domain should empty current domain
  DomainIntPtr domainOutput2    = getDomainInt(-1000, +1000);
  ArrayPtr domainIntersect2 = getArray(-1200, +1200);
  params.setOutputDomain(domainOutput2.get());
  params.setDomainElementArray(domainIntersect2);
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 0);
  
  // Intersect domain with no elements in common should not change current domain
  DomainIntPtr domainOutput3    = getDomainInt(-1000, +1000);
  ArrayPtr domainIntersect3 = getArray(+1001, +1200);
  params.setOutputDomain(domainOutput3.get());
  params.setDomainElementArray(domainIntersect3);
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 2001);
  ElemPtr elemPos1000 = getElem(+1000);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg1000.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos1000.get()));
  
  // Intersect smaller domain should not change current domain for bounds domain
  DomainIntPtr domainOutput4    = getDomainInt(-1000, +1000);
  ArrayPtr domainIntersect4 = getArray(-100, +100);
  params.setOutputDomain(domainOutput4.get());
  params.setDomainElementArray(domainIntersect4);
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 2001);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg1000.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos1000.get()));
}//SUBTRACTDomainElementArray2

OPTILAB_TEST(FilteringAlgorithmSUBTRACT, SUBTRACTDomainElementArray3)
{
  using namespace Core;
  
  FilteringAlgorithmSUBTRACT filter;
  filter.setSubtractSetType(FilteringAlgorithmSUBTRACT::SubtractSetType::SS_DOMAIN_ELEMENT_ARRAY);
  
  std::vector<INT_64> set;
  for(INT_64 elem = -500; elem <= 500; elem += 2)
  {
    set.push_back(elem);
  }
  DomainIntPtr domainOutput = getDomainInt(set);
  ArrayPtr domainIntersect  = getArray(+494, +502);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  params.setDomainElementArray(domainIntersect);
  
  auto domOut = filter.filter(params);
  
  ElemPtr elemPos492 = getElem(+492);
  ElemPtr elemNeg500 = getElem(-500);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg500.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos492.get()));
  
  // Subtract bigger domain should empty current domain
  
  DomainIntPtr domainOutput2 = getDomainInt(set);
  ArrayPtr domainIntersect2  = getArray(-501, 501);
  params.setOutputDomain(domainOutput2.get());
  params.setDomainElementArray(domainIntersect2);
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 0);
  
  // Intersect domain with no elements in common should not change current domain
  DomainIntPtr domainOutput3 = getDomainInt(set);
  ArrayPtr domainIntersect3  = getArray(+502, +505);
  params.setOutputDomain(domainOutput3.get());
  params.setDomainElementArray(domainIntersect3);
  
  domOut = filter.filter(params);
  ElemPtr elemPos500 = getElem(+500);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg500.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos500.get()));
}//SUBTRACTDomainElementArray3
