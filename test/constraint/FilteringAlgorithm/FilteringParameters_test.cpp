
#include "utest_globals.hpp"

#include "FilteringParameters.hpp"

#include "DomainInteger.hpp"
#include "DomainElementInt64.hpp"

OPTILAB_TEST(FilteringParameters, Basics)
{
  using namespace Core;
  
  FilteringParameters params;
  
  // Output domain
  ASSERT_FALSE(params.hasOutputDomain());
  ASSERT_FALSE(params.getOutputDomain());
  
  // Input domain and matrix
  ASSERT_FALSE(params.hasInputDomainArray());
  ASSERT_FALSE(params.hasInputDomainMatrix2D());

  // Input element array and matrix
  ASSERT_FALSE(params.hasDomainElementArray());
  ASSERT_FALSE(params.hasDomainElementMatrix2D());
}//Basics

OPTILAB_TEST(FilteringParameters, OutputDomain)
{
  using namespace Core;
  
  FilteringParameters params;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  
  DomainElementInt64 *lowerBoundOut = elementManager.createDomainElementInt64(-10);
  DomainElementInt64 *upperBoundOut = elementManager.createDomainElementInt64(+10);
  DomainInteger domainOutput(lowerBoundOut, upperBoundOut);
  
  params.setOutputDomain(&domainOutput);
  Domain *domOut = params.getOutputDomain();
  ASSERT_TRUE(params.hasOutputDomain());
  ASSERT_EQ(domOut, &domainOutput);
}//OutputDomain

OPTILAB_TEST(FilteringParameters, inputOutputDomainArrayMatrix)
{
  using namespace Core;
  
  FilteringParameters params;
  DomainArraySPtr array = std::make_shared<DomainArray>(3);
  
  params.setInputDomainArray(array);
  ASSERT_TRUE(params.hasInputDomainArray());
  
  ASSERT_EQ(params.getInputDomainArray()->size(), 3);
  ASSERT_EQ(params.getInputDomainArray().get(), array.get());
  
  DomainMatrix2DSPtr matrix = std::make_shared<DomainMatrix2D>(3, 2);
  params.setInputDomainMatrix2D(matrix);
  ASSERT_TRUE(params.hasInputDomainMatrix2D());
  
  ASSERT_EQ(params.getInputDomainMatrix2D()->size(), 6);
  ASSERT_EQ(params.getInputDomainMatrix2D().get(), matrix.get());
}//inputOutputDomainArrayMatrix

OPTILAB_TEST(FilteringParameters, inputOutputDomainElementArrayMatrix)
{
  using namespace Core;
  
  FilteringParameters params;
  DomainElementArraySPtr array = std::make_shared<DomainElementArray>(3);
  
  params.setDomainElementArray(array);
  ASSERT_TRUE(params.hasDomainElementArray());
  
  ASSERT_EQ(params.getDomainElementArray()->size(), 3);
  ASSERT_EQ(params.getDomainElementArray().get(), array.get());
  
  DomainElementMatrix2DSPtr matrix = std::make_shared<DomainElementMatrix2D>(3, 2);
  params.setDomainElementMatrix2D(matrix);
  ASSERT_TRUE(params.hasDomainElementMatrix2D());
  
  ASSERT_EQ(params.getDomainElementMatrix2D()->size(), 6);
  ASSERT_EQ(params.getDomainElementMatrix2D().get(), matrix.get());
}//inputOutputDomainElementArrayMatrix
