
#include "utest_globals.hpp"

#include "FilteringAlgorithmGTLT.hpp"

#include "ConstraintTestInc.hpp"

OPTILAB_TEST(FilteringAlgorithmGTLT, DomainSmall)
{
  using namespace Core;
  
  FilteringAlgorithmGTLT filterGTLT;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  DomainElementInt64 *lowerBoundOut = elementManager.createDomainElementInt64(-10);
  DomainElementInt64 *upperBoundOut = elementManager.createDomainElementInt64(+10);
  DomainIntPtr domainOutput = getDomainInt(-10, +10);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  
  auto array = getArray(0, 1);
  params.setDomainElementArray(array);
  params.getDomainElementArray()->assignElementToCell(0, lowerBoundOut);
  params.getDomainElementArray()->assignElementToCell(1, upperBoundOut);
  
  Domain *out = filterGTLT.filter(params);
  ASSERT_TRUE(DomainElement::isEqual(out->lowerBound(), elementManager.createDomainElementInt64(-9)));
  ASSERT_TRUE(DomainElement::isEqual(out->upperBound(), elementManager.createDomainElementInt64(+9)));
  
  upperBoundOut = elementManager.createDomainElementInt64(5);
  params.getDomainElementArray()->assignElementToCell(1, upperBoundOut);
  
  out = filterGTLT.filter(params);
  ASSERT_TRUE(DomainElement::isEqual(out->lowerBound(), elementManager.createDomainElementInt64(-9)));
  ASSERT_TRUE(DomainElement::isEqual(out->upperBound(), elementManager.createDomainElementInt64(+4)));
  
  lowerBoundOut = elementManager.createDomainElementInt64(-5);
  params.getDomainElementArray()->assignElementToCell(0, lowerBoundOut);
  
  out = filterGTLT.filter(params);
  ASSERT_TRUE(DomainElement::isEqual(out->lowerBound(), elementManager.createDomainElementInt64(-4)));
  ASSERT_TRUE(DomainElement::isEqual(out->upperBound(), elementManager.createDomainElementInt64(+4)));
  
  upperBoundOut = elementManager.createDomainElementInt64(-15);
  params.getDomainElementArray()->assignElementToCell(1, upperBoundOut);
  out = filterGTLT.filter(params);
  ASSERT_TRUE(DomainEventFail::isa(out->getEvent()));
}//DomainSmall

OPTILAB_TEST(FilteringAlgorithmGTLT, DomainLarge)
{
  using namespace Core;
  
  FilteringAlgorithmGTLT filterGTLT;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  DomainElementInt64 *lowerBoundOut = elementManager.createDomainElementInt64(-10000);
  DomainElementInt64 *upperBoundOut = elementManager.createDomainElementInt64(+10000);
  DomainInteger domainOutput(lowerBoundOut, upperBoundOut);
  
  FilteringParameters params;
  params.setOutputDomain(&domainOutput);
  
  auto array = getArray(0, 1);
  params.setDomainElementArray(array);
  params.getDomainElementArray()->assignElementToCell(0, lowerBoundOut);
  params.getDomainElementArray()->assignElementToCell(1, upperBoundOut);
  
  Domain *out = filterGTLT.filter(params);
  ASSERT_TRUE(DomainElement::isEqual(out->lowerBound(), elementManager.createDomainElementInt64(-9999)));
  ASSERT_TRUE(DomainElement::isEqual(out->upperBound(), elementManager.createDomainElementInt64(+9999)));
  
  upperBoundOut = elementManager.createDomainElementInt64(500);
  params.getDomainElementArray()->assignElementToCell(1, upperBoundOut);
  
  out = filterGTLT.filter(params);
  ASSERT_TRUE(DomainElement::isEqual(out->lowerBound(), elementManager.createDomainElementInt64(-9999)));
  ASSERT_TRUE(DomainElement::isEqual(out->upperBound(), elementManager.createDomainElementInt64(+499)));
  
  lowerBoundOut = elementManager.createDomainElementInt64(-1000);
  params.getDomainElementArray()->assignElementToCell(0, lowerBoundOut);
  
  out = filterGTLT.filter(params);
  ASSERT_TRUE(DomainElement::isEqual(out->lowerBound(), elementManager.createDomainElementInt64(-999)));
  ASSERT_TRUE(DomainElement::isEqual(out->upperBound(), elementManager.createDomainElementInt64(+499)));
  
  upperBoundOut = elementManager.createDomainElementInt64(-20000);
  params.getDomainElementArray()->assignElementToCell(1, upperBoundOut);
  out = filterGTLT.filter(params);
  ASSERT_TRUE(DomainEventFail::isa(out->getEvent()));
}//DomainLarge

OPTILAB_TEST(FilteringAlgorithmGTLT, DomainSet)
{
  using namespace Core;
  
  FilteringAlgorithmGTLT filterGQLQ;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  DomainElementInt64 *lowerBound = elementManager.createDomainElementInt64(-10000);
  DomainElementInt64 *elem1 = elementManager.createDomainElementInt64(0);
  DomainElementInt64 *elem2 = elementManager.createDomainElementInt64(500);
  DomainElementInt64 *upperBound = elementManager.createDomainElementInt64(+10000);
  
  std::unordered_set<DomainElementInt64 *> set;
  set.insert(lowerBound);
  set.insert(elem1);
  set.insert(elem2);
  set.insert(upperBound);
  DomainInteger domainOutput(set);
  
  FilteringParameters params;
  params.setOutputDomain(&domainOutput);
  auto array = getArray(0, 1);
  params.setDomainElementArray(array);
  params.getDomainElementArray()->assignElementToCell(0, lowerBound);
  params.getDomainElementArray()->assignElementToCell(1, upperBound);
  
  Domain *out = filterGQLQ.filter(params);
  ASSERT_TRUE(DomainElement::isEqual(out->lowerBound(), elem1));
  ASSERT_TRUE(DomainElement::isEqual(out->upperBound(), elem2));
  
  upperBound = elementManager.createDomainElementInt64(600);
  params.getDomainElementArray()->assignElementToCell(1, upperBound);
  
  out = filterGQLQ.filter(params);
  ASSERT_TRUE(DomainElement::isEqual(out->lowerBound(), elem1));
  ASSERT_TRUE(DomainElement::isEqual(out->upperBound(), elem2));
  
  params.getDomainElementArray()->assignElementToCell(0, elem1);
  
  out = filterGQLQ.filter(params);
  ASSERT_TRUE(DomainElement::isEqual(out->lowerBound(), elem2));
  ASSERT_TRUE(DomainElement::isEqual(out->upperBound(), elem2));
  
  upperBound = elementManager.createDomainElementInt64(-20000);
  params.getDomainElementArray()->assignElementToCell(1, upperBound);
  out = filterGQLQ.filter(params);
  ASSERT_TRUE(DomainEventFail::isa(out->getEvent()));
}//DomainSet
