
#include "utest_globals.hpp"

#include "FilteringAlgorithmMINUS.hpp"

#include "ConstraintTestInc.hpp"

OPTILAB_TEST(FilteringAlgorithmMINUS, MINUS1)
{
  using namespace Core;
  
  FilteringAlgorithmMINUS filter;
  DomainIntPtr domainOutput = getDomainInt(-10, +10);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  
  // 8 - 6 = 2
  std::vector<INT_64> vec = {8, 6};
  ArrayPtr array = getArray(vec);
  params.setDomainElementArray(array);
  
  auto domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), getElem(+2).get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), getElem(+2).get()));
  
  DomainIntPtr domainOutput2 = getDomainInt(-10, +10);
  params.setOutputDomain(domainOutput2.get());
  
  // 5 - 8 = -3
  std::vector<INT_64> vec2 = {5, 8};
  ArrayPtr array2 = getArray(vec2);
  params.setDomainElementArray(array2);
  
  domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), getElem(-3).get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), getElem(-3).get()));
  
  DomainIntPtr domainOutput3 = getDomainInt(-10, +10);
  params.setOutputDomain(domainOutput2.get());
  
  // 4 - 4 = 0
  DomainIntPtr domainOutput4 = getDomainInt(-10, +10);
  params.setOutputDomain(domainOutput4.get());
  
  std::vector<INT_64> vec3 = {4, 4};
  ArrayPtr array3 = getArray(vec3);
  params.setDomainElementArray(array3);
  
  domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), getElem(0).get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), getElem(0).get()));
  
  // 4 - 20 = -16 out of bounds should empty domain
  DomainIntPtr domainOutput5 = getDomainInt(-10, +10);
  params.setOutputDomain(domainOutput5.get());
  
  std::vector<INT_64> vec4 = {4, 20};
  ArrayPtr array4 = getArray(vec4);
  params.setDomainElementArray(array4);
  
  domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 0);
}//MINUS1

OPTILAB_TEST(FilteringAlgorithmMINUS, MINUS2)
{
  using namespace Core;
  
  FilteringAlgorithmMINUS filter;
  DomainIntPtr domainOutput = getDomainInt(-1000, +1000);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  
  // 800 - 300 = 500
  std::vector<INT_64> vec = {800, 300};
  ArrayPtr array = getArray(vec);
  params.setDomainElementArray(array);
  
  auto domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), getElem(+500).get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), getElem(+500).get()));
  
  DomainIntPtr domainOutput2 = getDomainInt(-1000, +1000);
  params.setOutputDomain(domainOutput2.get());
  
  // 500 - 800 = -300
  std::vector<INT_64> vec2 = {500, 800};
  ArrayPtr array2 = getArray(vec2);
  params.setDomainElementArray(array2);
  
  domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), getElem(-300).get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), getElem(-300).get()));
  
  DomainIntPtr domainOutput3 = getDomainInt(-1000, +1000);
  params.setOutputDomain(domainOutput3.get());
  
  // 500 - 499 = 1
  std::vector<INT_64> vec3 = {500, 499};
  ArrayPtr array3 = getArray(vec3);
  params.setDomainElementArray(array3);
  
  domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), getElem(+1).get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), getElem(+1).get()));
}//MINUS2

OPTILAB_TEST(FilteringAlgorithmMINUS, MINUS3)
{
  using namespace Core;
  
  FilteringAlgorithmMINUS filter;
  
  std::vector<INT_64> set;
  for(INT_64 elem = -10; elem <= 10; elem += 2)
  {
    set.push_back(elem);
  }
  DomainIntPtr domainOutput = getDomainInt(set);
  
  // 10 - 4 = 6
  std::vector<INT_64> vec = {10, 4};
  ArrayPtr array = getArray(vec);
  FilteringParameters params;
  params.setDomainElementArray(array);
  params.setOutputDomain(domainOutput.get());
  
  auto domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), getElem(+6).get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), getElem(+6).get()));
  
  DomainIntPtr domainOutput2 = getDomainInt(set);
  
  // 6 - 1 = 5 not in set
  std::vector<INT_64> vec2 = {6, 5};
  ArrayPtr array2 = getArray(vec2);
  params.setDomainElementArray(array2);
  params.setOutputDomain(domainOutput.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 0);
}//MINUS3

