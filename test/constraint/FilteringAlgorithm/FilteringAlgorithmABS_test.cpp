
#include "utest_globals.hpp"

#include "FilteringAlgorithmABS.hpp"

#include "ConstraintTestInc.hpp"

OPTILAB_TEST(FilteringAlgorithmABS, ABS1)
{
  using namespace Core;
  
  FilteringAlgorithmABS filter;
  DomainIntPtr domainOutput = getDomainInt(-10, +10);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  auto array = getArray(+5, +5);
  params.setDomainElementArray(array);
  
  auto domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), getElem(+5).get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), getElem(+5).get()));
  
  DomainIntPtr domainOutput2 = getDomainInt(-10, +10);
  params.setOutputDomain(domainOutput2.get());
  ElemPtr elemNeg2 = getElem(-2);
  params.getDomainElementArray()->assignElementToCell(0, elemNeg2.get());
  
  domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), getElem(+2).get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), getElem(+2).get()));
  
  DomainIntPtr domainOutput3 = getDomainInt(-10, +10);
  params.setOutputDomain(domainOutput3.get());
  ElemPtr elemPos20 = getElem(20);
  params.getDomainElementArray()->assignElementToCell(0, elemPos20.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 0);
}//ABS1

OPTILAB_TEST(FilteringAlgorithmABS, ABS2)
{
  using namespace Core;
  
  FilteringAlgorithmABS filter;
  DomainIntPtr domainOutput = getDomainInt(-1000, +1000);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  auto array = getArray(+5, +5);
  params.setDomainElementArray(array);
  
  auto domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), getElem(+5).get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), getElem(+5).get()));
  
  DomainIntPtr domainOutput2 = getDomainInt(-1000, +1000);
  params.setOutputDomain(domainOutput2.get());
  ElemPtr elemNeg2 = getElem(-2);
  params.getDomainElementArray()->assignElementToCell(0, elemNeg2.get());
  
  domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), getElem(+2).get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), getElem(+2).get()));
  
  DomainIntPtr domainOutput3 = getDomainInt(-1000, +1000);
  params.setOutputDomain(domainOutput3.get());
  ElemPtr elemPos2000 = getElem(2000);
  params.getDomainElementArray()->assignElementToCell(0, elemPos2000.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 0);
}//ABS2

OPTILAB_TEST(FilteringAlgorithmABS, ABS3)
{
  using namespace Core;
  
  FilteringAlgorithmABS filter;
  
  std::vector<INT_64> set;
  for(INT_64 elem = -10; elem <= 10; elem += 2)
  {
    set.push_back(elem);
  }
  DomainIntPtr domainOutput = getDomainInt(set);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  auto array = getArray(+5, +5);
  params.setDomainElementArray(array);
  
  auto domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 0);
  
  DomainIntPtr domainOutput2 = getDomainInt(set);
  params.setOutputDomain(domainOutput2.get());
  ElemPtr elemNeg2 = getElem(-2);
  params.getDomainElementArray()->assignElementToCell(0, elemNeg2.get());
  
  domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), getElem(+2).get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), getElem(+2).get()));
}//ABS3

