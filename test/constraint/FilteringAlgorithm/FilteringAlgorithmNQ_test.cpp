
#include "utest_globals.hpp"

#include "FilteringAlgorithmNQ.hpp"

#include "ConstraintTestInc.hpp"

OPTILAB_TEST(FilteringAlgorithmNQ, NQ1)
{
  using namespace Core;
  
  FilteringAlgorithmNQ filter;
  DomainIntPtr domainOutput = getDomainInt(-10, +10);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  
  ElemPtr elemPos10 = getElem(+10);
  
  auto array = getArray(+10, +10);
  params.setDomainElementArray(array);
  
  auto domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 20);
  
  ElemPtr elemPos9  = getElem(+9);
  ElemPtr elemNeg10 = getElem(-10);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg10.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos9.get()));
  
  DomainIntPtr domainOutput2 = getDomainInt(-10, +10);
  params.setOutputDomain(domainOutput2.get());
  params.getDomainElementArray()->assignElementToCell(0, elemNeg10.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 20);
  ElemPtr elemNeg9  = getElem(-9);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg9.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos10.get()));
  
  DomainIntPtr domainOutput3 = getDomainInt(-10, +10);
  params.setOutputDomain(domainOutput3.get());
  ElemPtr elemNeg11 = getElem(-11);
  params.getDomainElementArray()->assignElementToCell(0, elemNeg11.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 21);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg10.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos10.get()));
  
  DomainIntPtr domainOutput4 = getDomainInt(-10, +10);
  params.setOutputDomain(domainOutput4.get());
  ElemPtr elemNeg2 = getElem(-2);
  params.getDomainElementArray()->assignElementToCell(0, elemNeg2.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 20);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg10.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos10.get()));
  ASSERT_FALSE(domOut->contains(elemNeg2.get()));
}//NQ1

OPTILAB_TEST(FilteringAlgorithmNQ, NQ2)
{
  using namespace Core;
  
  FilteringAlgorithmNQ filter;
  DomainIntPtr domainOutput = getDomainInt(-1000, +1000);
  
  FilteringParameters params;
  ElemPtr elemPos1000 = getElem(+1000);
  params.setOutputDomain(domainOutput.get());
  
  auto array = getArray(+1000, +1000);
  params.setDomainElementArray(array);
  
  auto domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 2000);
  ElemPtr elemNeg1000 = getElem(-1000);
  ElemPtr elemPos999  = getElem(+999);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg1000.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos999.get()));
  
  DomainIntPtr domainOutput2 = getDomainInt(-1000, +1000);
  params.setOutputDomain(domainOutput2.get());
  params.getDomainElementArray()->assignElementToCell(0, elemNeg1000.get());
  
  domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 2000);
  ElemPtr elemNeg999  = getElem(-999);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg999.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos1000.get()));
  
  // Big domains work only on boundaries
  DomainIntPtr domainOutput3 = getDomainInt(-1000, +1000);
  params.setOutputDomain(domainOutput3.get());
  ElemPtr elemNeg500  = getElem(-500);
  params.getDomainElementArray()->assignElementToCell(0, elemNeg500.get());
  
  domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 2001);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg1000.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos1000.get()));
}//NQ2

OPTILAB_TEST(FilteringAlgorithmNQ, NQ3)
{
  using namespace Core;
  
  FilteringAlgorithmNQ filter;
  
  std::vector<INT_64> set;
  for(INT_64 elem = -10; elem <= 10; elem += 2)
  {
    set.push_back(elem);
  }
  DomainIntPtr domainOutput = getDomainInt(set);
  
  FilteringParameters params;
  ElemPtr elemPos10 = getElem(+10);
  params.setOutputDomain(domainOutput.get());

  auto array = getArray(+10, +10);
  params.setDomainElementArray(array);
  
  auto domOut = filter.filter(params);
  ElemPtr elemNeg10 = getElem(-10);
  ElemPtr elemPos8 = getElem(+8);
  ASSERT_EQ(domOut->getSize(), 10);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg10.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos8.get()));
  
  // Element not in domain should not change current domain
  DomainIntPtr domainOutput2 = getDomainInt(set);
  ElemPtr elemPos5 = getElem(+5);
  params.setOutputDomain(domainOutput2.get());
  params.getDomainElementArray()->assignElementToCell(0, elemPos5.get());
  
  domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 11);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg10.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos10.get()));
}//NQ3
