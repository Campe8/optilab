
#include "utest_globals.hpp"

#include "FilteringAlgorithmTRUE.hpp"

#include "ConstraintTestInc.hpp"

OPTILAB_TEST(FilteringAlgorithmTRUE, BTRUE)
{
  using namespace Core;
  
  FilteringAlgorithmTRUE filter;
  DomainBoolPtr domainOutput = getDomainBool();
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  
  auto domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ElemPtr elemPos1 = getElem(1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemPos1.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos1.get()));
  
  DomainBoolPtr domainOutput2 = getDomainBool();
  domainOutput2->setFalse();
  params.setOutputDomain(domainOutput2.get());
  
  domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 0);
  
  DomainBoolPtr domainOutput3 = getDomainBool();
  domainOutput3->setTrue();
  params.setOutputDomain(domainOutput3.get());
  
  domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemPos1.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos1.get()));
}//BFALSE
