
#include "utest_globals.hpp"

#include "FilteringAlgorithmLT.hpp"

#include "ConstraintTestInc.hpp"

OPTILAB_TEST(FilteringAlgorithmLT, LT1)
{
  using namespace Core;
  
  FilteringAlgorithmLT filter;
  DomainIntPtr domainOutput = getDomainInt(-10, +10);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  
  ElemPtr elemPos5 = getElem(+5);
  ElemPtr elemPos4 = getElem(+4);
  ElemPtr elemNeg10 = getElem(-10);
  
  auto array = getArray(+5, +5);
  params.setDomainElementArray(array);
  
  auto domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 15);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg10.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos4.get()));
  
  // Less than out of bound domain should empty current domain
  DomainIntPtr domainOutput2 = getDomainInt(-10, +10);
  params.setOutputDomain(domainOutput2.get());
  
  ElemPtr elemNeg11 = getElem(-11);
  params.getDomainElementArray()->assignElementToCell(0, elemNeg11.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 0);
  
  // Less than current upper bound shouldn't change current domain
  DomainIntPtr domainOutput3 = getDomainInt(-10, +10);
  params.setOutputDomain(domainOutput3.get());
  ElemPtr elemPos11 = getElem(+11);
  params.getDomainElementArray()->assignElementToCell(0, elemPos11.get());
  
  domOut = filter.filter(params);
  ElemPtr elemPos10 = getElem(+10);
  ASSERT_EQ(domOut->getSize(), 21);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg10.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos10.get()));
  
  // Less than current lower bound should empty current domain
  DomainIntPtr domainOutput4 = getDomainInt(-10, +10);
  params.setOutputDomain(domainOutput4.get());
  params.getDomainElementArray()->assignElementToCell(0, elemNeg10.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 0);
}//LT1

OPTILAB_TEST(FilteringAlgorithmLT, LT2)
{
  using namespace Core;
  
  FilteringAlgorithmLT filter;
  DomainIntPtr domainOutput = getDomainInt(-1000, +1000);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  
  ElemPtr elemNeg1000 = getElem(-1000);
  auto array = getArray(-1000, -1000);
  params.setDomainElementArray(array);

  auto domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 0);
  
  DomainIntPtr domainOutput2 = getDomainInt(-1000, +1000);
  params.setOutputDomain(domainOutput2.get());
  ElemPtr elemPos500 = getElem(+500);
  ElemPtr elemPos501 = getElem(+501);
  params.getDomainElementArray()->assignElementToCell(0, elemPos501.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 1501);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg1000.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos500.get()));
  
  DomainIntPtr domainOutput3 = getDomainInt(-1000, +1000);
  params.setOutputDomain(domainOutput3.get());
  ElemPtr elemPos1000 = getElem(+1000);
  params.getDomainElementArray()->assignElementToCell(0, elemPos1000.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 2000);
  ElemPtr elemPos999 = getElem(+999);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg1000.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos999.get()));
  
  DomainIntPtr domainOutput4 = getDomainInt(-1000, +1000);
  params.setOutputDomain(domainOutput4.get());
  ElemPtr elemPos2000 = getElem(+2000);
  params.getDomainElementArray()->assignElementToCell(0, elemPos2000.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 2001);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg1000.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos1000.get()));
}//LT2

OPTILAB_TEST(FilteringAlgorithmLT, LT3)
{
  using namespace Core;
  
  FilteringAlgorithmLT filter;
  
  std::vector<INT_64> set;
  for(INT_64 elem = -10; elem <= 10; elem += 2)
  {
    set.push_back(elem);
  }
  DomainIntPtr domainOutput = getDomainInt(set);
  
  FilteringParameters params;
  ElemPtr elemPos7 = getElem(+7);
  params.setOutputDomain(domainOutput.get());
  auto array = getArray(+7, +7);
  params.setDomainElementArray(array);
  
  auto domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 9);
  ElemPtr elemPos6 = getElem(+6);
  ElemPtr elemNeg10 = getElem(-10);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg10.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos6.get()));
  
  DomainIntPtr domainOutput1 = getDomainInt(set);
  params.setOutputDomain(domainOutput1.get());
  ElemPtr elemNeg8 = getElem(-8);
  params.getDomainElementArray()->assignElementToCell(0, elemNeg8.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg10.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemNeg10.get()));
  
  DomainIntPtr domainOutput2 = getDomainInt(set);
  params.setOutputDomain(domainOutput2.get());
  
  ElemPtr elemNeg11 = getElem(-11);
  params.getDomainElementArray()->assignElementToCell(0, elemNeg11.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 0);
  
  DomainIntPtr domainOutput3 = getDomainInt(set);
  params.setOutputDomain(domainOutput3.get());
  
  ElemPtr elemPos12 = getElem(+12);
  params.getDomainElementArray()->assignElementToCell(0, elemPos12.get());
  
  domOut = filter.filter(params);
  ElemPtr elemPos10 = getElem(+10);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg10.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos10.get()));
}//LT3

