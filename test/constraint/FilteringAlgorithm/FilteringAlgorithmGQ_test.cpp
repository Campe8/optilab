
#include "utest_globals.hpp"

#include "FilteringAlgorithmGQ.hpp"

#include "ConstraintTestInc.hpp"

OPTILAB_TEST(FilteringAlgorithmGQ, GQ1)
{
  using namespace Core;
  
  FilteringAlgorithmGQ filter;
  DomainIntPtr domainOutput = getDomainInt(-10, +10);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  
  ElemPtr elemPos5 = getElem(+5);
  ElemPtr elemPos10 = getElem(+10);
  
  auto array = getArray(+5, +5);
  params.setDomainElementArray(array);
  
  auto domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 6);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemPos5.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos10.get()));
  
  DomainIntPtr domainOutput2 = getDomainInt(-10, +10);
  params.setOutputDomain(domainOutput2.get());
  
  ElemPtr elemPos11 = getElem(+11);
  params.getDomainElementArray()->assignElementToCell(0, elemPos11.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 0);
  
  DomainIntPtr domainOutput3 = getDomainInt(-10, +10);
  params.setOutputDomain(domainOutput3.get());
  ElemPtr elemNeg11 = getElem(-11);
  params.getDomainElementArray()->assignElementToCell(0, elemNeg11.get());
  
  domOut = filter.filter(params);
  ElemPtr elemNeg10 = getElem(-10);
  ASSERT_EQ(domOut->getSize(), 21);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg10.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos10.get()));
  
  DomainIntPtr domainOutput4 = getDomainInt(-10, +10);
  params.setOutputDomain(domainOutput4.get());
  params.getDomainElementArray()->assignElementToCell(0, elemPos10.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemPos10.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos10.get()));
}//GQ1

OPTILAB_TEST(FilteringAlgorithmGQ, GQ2)
{
  using namespace Core;
  
  FilteringAlgorithmGQ filter;
  DomainIntPtr domainOutput = getDomainInt(-1000, +1000);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  
  ElemPtr elemPos1000 = getElem(+1000);
  
  auto array = getArray(+1000, +1000);
  params.setDomainElementArray(array);
  
  auto domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemPos1000.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos1000.get()));
  
  DomainIntPtr domainOutput2 = getDomainInt(-1000, +1000);
  params.setOutputDomain(domainOutput2.get());
  ElemPtr elemPos500 = getElem(+500);
  params.getDomainElementArray()->assignElementToCell(0, elemPos500.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 501);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemPos500.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos1000.get()));
  
  DomainIntPtr domainOutput3 = getDomainInt(-1000, +1000);
  params.setOutputDomain(domainOutput3.get());
  ElemPtr elemNeg1000 = getElem(-1000);
  params.getDomainElementArray()->assignElementToCell(0, elemNeg1000.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 2001);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg1000.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos1000.get()));
  
  DomainIntPtr domainOutput4 = getDomainInt(-1000, +1000);
  params.setOutputDomain(domainOutput4.get());
  ElemPtr elemNeg2000 = getElem(-2000);
  params.getDomainElementArray()->assignElementToCell(0, elemNeg2000.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 2001);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg1000.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos1000.get()));
}//GQ2

OPTILAB_TEST(FilteringAlgorithmGQ, GQ3)
{
  using namespace Core;
  
  FilteringAlgorithmGQ filter;
  
  std::vector<INT_64> set;
  for(INT_64 elem = -10; elem <= 10; elem += 2)
  {
    set.push_back(elem);
  }
  DomainIntPtr domainOutput = getDomainInt(set);
  
  FilteringParameters params;
  ElemPtr elemPos7 = getElem(+7);
  params.setOutputDomain(domainOutput.get());

  auto array = getArray(+7, +7);
  params.setDomainElementArray(array);
  
  auto domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 2);
  ElemPtr elemPos8 = getElem(+8);
  ElemPtr elemPos10 = getElem(+10);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemPos8.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos10.get()));
  
  DomainIntPtr domainOutput1 = getDomainInt(set);
  params.setOutputDomain(domainOutput1.get());
  params.getDomainElementArray()->assignElementToCell(0, elemPos8.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 2);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemPos8.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos10.get()));
  
  DomainIntPtr domainOutput2 = getDomainInt(set);
  params.setOutputDomain(domainOutput2.get());
  
  ElemPtr elemPos11 = getElem(+11);
  params.getDomainElementArray()->assignElementToCell(0, elemPos11.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 0);
  
  DomainIntPtr domainOutput3 = getDomainInt(set);
  params.setOutputDomain(domainOutput3.get());
  
  ElemPtr elemNeg12 = getElem(-12);
  params.getDomainElementArray()->assignElementToCell(0, elemNeg12.get());
  
  domOut = filter.filter(params);
  ElemPtr elemNeg10 = getElem(-10);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg10.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos10.get()));
}//GQ3

