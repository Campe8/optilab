
#include "utest_globals.hpp"

#include "FilteringAlgorithmLQ.hpp"

#include "ConstraintTestInc.hpp"

OPTILAB_TEST(FilteringAlgorithmLQ, DomainSmall)
{
  using namespace Core;
  
  FilteringAlgorithmLQ filterLQ;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  DomainElementInt64 *lowerBoundOut = elementManager.createDomainElementInt64(-10);
  DomainElementInt64 *upperBoundOut = elementManager.createDomainElementInt64(+10);
  DomainIntPtr domainOutput = getDomainInt(-10, +10);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  
  auto array = getArray(10, 10);
  params.setDomainElementArray(array);
  
  Domain *out = filterLQ.filter(params);
  ASSERT_TRUE(DomainElement::isEqual(out->lowerBound(), lowerBoundOut));
  ASSERT_TRUE(DomainElement::isEqual(out->upperBound(), upperBoundOut));
  
  upperBoundOut = elementManager.createDomainElementInt64(15);
  params.getDomainElementArray()->assignElementToCell(0, upperBoundOut);
  ASSERT_TRUE(DomainElement::isEqual(out->lowerBound(), lowerBoundOut));
  ASSERT_TRUE(DomainElement::isEqual(out->upperBound(), elementManager.createDomainElementInt64(+10)));
  
  upperBoundOut = elementManager.createDomainElementInt64(5);
  params.getDomainElementArray()->assignElementToCell(0, upperBoundOut);
  
  out = filterLQ.filter(params);
  ASSERT_TRUE(DomainElement::isEqual(out->lowerBound(), lowerBoundOut));
  ASSERT_TRUE(DomainElement::isEqual(out->upperBound(), upperBoundOut));
  
  upperBoundOut = elementManager.createDomainElementInt64(-10);
  params.getDomainElementArray()->assignElementToCell(0, upperBoundOut);
  
  out = filterLQ.filter(params);
  ASSERT_TRUE(DomainElement::isEqual(out->lowerBound(), lowerBoundOut));
  ASSERT_TRUE(DomainElement::isEqual(out->upperBound(), upperBoundOut));
  
  upperBoundOut = elementManager.createDomainElementInt64(-15);
  params.getDomainElementArray()->assignElementToCell(0, upperBoundOut);
  out = filterLQ.filter(params);
  ASSERT_TRUE(DomainEventFail::isa(out->getEvent()));
}//DomainSmall

OPTILAB_TEST(FilteringAlgorithmLQ, DomainLarge)
{
  using namespace Core;
  
  FilteringAlgorithmLQ filterLQ;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  DomainElementInt64 *lowerBoundOut = elementManager.createDomainElementInt64(-10000);
  DomainElementInt64 *upperBoundOut = elementManager.createDomainElementInt64(+10000);
  DomainInteger domainOutput(lowerBoundOut, upperBoundOut);
  
  FilteringParameters params;
  params.setOutputDomain(&domainOutput);
  auto array = getArray(+10000, +10000);
  params.setDomainElementArray(array);
  
  Domain *out = filterLQ.filter(params);
  ASSERT_TRUE(DomainElement::isEqual(out->lowerBound(), lowerBoundOut));
  ASSERT_TRUE(DomainElement::isEqual(out->upperBound(), upperBoundOut));
  
  upperBoundOut = elementManager.createDomainElementInt64(500);
  params.getDomainElementArray()->assignElementToCell(0, upperBoundOut);
  
  out = filterLQ.filter(params);
  ASSERT_TRUE(DomainElement::isEqual(out->lowerBound(), lowerBoundOut));
  ASSERT_TRUE(DomainElement::isEqual(out->upperBound(), upperBoundOut));
  
  upperBoundOut = elementManager.createDomainElementInt64(-1000);
  params.getDomainElementArray()->assignElementToCell(0, upperBoundOut);
  
  out = filterLQ.filter(params);
  ASSERT_TRUE(DomainElement::isEqual(out->lowerBound(), lowerBoundOut));
  ASSERT_TRUE(DomainElement::isEqual(out->upperBound(), upperBoundOut));
  
  upperBoundOut = elementManager.createDomainElementInt64(-20000);
  params.getDomainElementArray()->assignElementToCell(0, upperBoundOut);
  out = filterLQ.filter(params);
  ASSERT_TRUE(DomainEventFail::isa(out->getEvent()));
}//DomainLarge

OPTILAB_TEST(FilteringAlgorithmLQ, DomainSet)
{
  using namespace Core;
  
  FilteringAlgorithmLQ filterLQ;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  DomainElementInt64 *lowerBound = elementManager.createDomainElementInt64(-10000);
  DomainElementInt64 *elem1 = elementManager.createDomainElementInt64(0);
  DomainElementInt64 *elem2 = elementManager.createDomainElementInt64(500);
  DomainElementInt64 *upperBound = elementManager.createDomainElementInt64(+10000);
  
  std::unordered_set<DomainElementInt64 *> set;
  set.insert(lowerBound);
  set.insert(elem1);
  set.insert(elem2);
  set.insert(upperBound);
  DomainInteger domainOutput(set);
  
  FilteringParameters params;
  params.setOutputDomain(&domainOutput);
  auto array = getArray(+10000, +10000);
  params.setDomainElementArray(array);
  
  Domain *out = filterLQ.filter(params);
  ASSERT_TRUE(DomainElement::isEqual(out->lowerBound(), lowerBound));
  ASSERT_TRUE(DomainElement::isEqual(out->upperBound(), upperBound));
  
  upperBound = elementManager.createDomainElementInt64(600);
  params.getDomainElementArray()->assignElementToCell(0, upperBound);
  
  out = filterLQ.filter(params);
  ASSERT_TRUE(DomainElement::isEqual(out->lowerBound(), lowerBound));
  ASSERT_TRUE(DomainElement::isEqual(out->upperBound(), elem2));
  
  upperBound = elementManager.createDomainElementInt64(-1000);
  params.getDomainElementArray()->assignElementToCell(0, upperBound);
  
  out = filterLQ.filter(params);
  ASSERT_TRUE(DomainElement::isEqual(out->lowerBound(), lowerBound));
  ASSERT_TRUE(DomainElement::isEqual(out->upperBound(), lowerBound));
  
  upperBound = elementManager.createDomainElementInt64(-20000);
  params.getDomainElementArray()->assignElementToCell(0, upperBound);
  out = filterLQ.filter(params);
  ASSERT_TRUE(DomainEventFail::isa(out->getEvent()));
}//DomainSet
