
#include "utest_globals.hpp"

#include "FilteringAlgorithmDIVIDE.hpp"

#include "ConstraintTestInc.hpp"

OPTILAB_TEST(FilteringAlgorithmDIVIDE, DIVIDE1)
{
  using namespace Core;
  
  FilteringAlgorithmDIVIDE filter;
  DomainIntPtr domainOutput = getDomainInt(-10, +10);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  
  // 8 / 4 = 2
  std::vector<INT_64> vec = {8, 4};
  ArrayPtr array = getArray(vec);
  params.setDomainElementArray(array);
  
  auto domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), getElem(+2).get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), getElem(+2).get()));
  
  DomainIntPtr domainOutput2 = getDomainInt(-10, +10);
  params.setOutputDomain(domainOutput2.get());
  
  // 4 / 8 = 0
  std::vector<INT_64> vec2 = {4, 8};
  ArrayPtr array2 = getArray(vec2);
  params.setDomainElementArray(array2);
  
  domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), getElem(0).get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), getElem(0).get()));
  
  DomainIntPtr domainOutput3 = getDomainInt(-10, +10);
  params.setOutputDomain(domainOutput2.get());
  
  // 4 / 0 = should throw
  std::vector<INT_64> vec3 = {4, 0};
  ArrayPtr array3 = getArray(vec3);
  params.setDomainElementArray(array3);
  //ASSERT_ANY_THROW(domOut = filter.filter(params));
}//DIVIDE1

OPTILAB_TEST(FilteringAlgorithmDIVIDE, DIVIDE2)
{
  using namespace Core;
  
  FilteringAlgorithmDIVIDE filter;
  DomainIntPtr domainOutput = getDomainInt(-1000, +1000);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  
  // 800 / 400 = 2
  std::vector<INT_64> vec = {800, 400};
  ArrayPtr array = getArray(vec);
  params.setDomainElementArray(array);
  
  auto domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), getElem(+2).get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), getElem(+2).get()));
  
  DomainIntPtr domainOutput2 = getDomainInt(-1000, +1000);
  params.setOutputDomain(domainOutput2.get());
  
  // 400 / 800 = 0
  std::vector<INT_64> vec2 = {400, 800};
  ArrayPtr array2 = getArray(vec2);
  params.setDomainElementArray(array2);
  
  domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), getElem(0).get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), getElem(0).get()));
}//DIVIDE2

OPTILAB_TEST(FilteringAlgorithmDIVIDE, DIVIDE3)
{
  using namespace Core;
  
  FilteringAlgorithmDIVIDE filter;
  
  std::vector<INT_64> set;
  for(INT_64 elem = -10; elem <= 10; elem += 2)
  {
    set.push_back(elem);
  }
  DomainIntPtr domainOutput = getDomainInt(set);
  
  std::vector<INT_64> vec = {10, 5};
  ArrayPtr array = getArray(vec);
  FilteringParameters params;
  params.setDomainElementArray(array);
  params.setOutputDomain(domainOutput.get());
  
  auto domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), getElem(+2).get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), getElem(+2).get()));
  
  DomainIntPtr domainOutput2 = getDomainInt(set);
  
  // 6 / 2 = 3 not in set
  std::vector<INT_64> vec2 = {6, 2};
  ArrayPtr array2 = getArray(vec2);
  params.setDomainElementArray(array2);
  params.setOutputDomain(domainOutput.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 0);
}//DIVIDE3


