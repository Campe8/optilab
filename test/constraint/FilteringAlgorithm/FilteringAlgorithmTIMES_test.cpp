
#include "utest_globals.hpp"

#include "FilteringAlgorithmTIMES.hpp"

#include "ConstraintTestInc.hpp"

OPTILAB_TEST(FilteringAlgorithmTIMES, TIMES1)
{
  using namespace Core;
  
  FilteringAlgorithmTIMES filter;
  DomainIntPtr domainOutput = getDomainInt(-10, +10);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  
  // 2 * 4 = 8
  std::vector<INT_64> vec = {2, 4};
  ArrayPtr array = getArray(vec);
  params.setDomainElementArray(array);
  
  auto domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), getElem(+8).get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), getElem(+8).get()));
  
  DomainIntPtr domainOutput2 = getDomainInt(-10, +10);
  params.setOutputDomain(domainOutput2.get());
  
  // 2 * 0 = 0
  std::vector<INT_64> vec2 = {2, 0};
  ArrayPtr array2 = getArray(vec2);
  params.setDomainElementArray(array2);
  
  domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), getElem(0).get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), getElem(0).get()));
  
  DomainIntPtr domainOutput3 = getDomainInt(-10, +10);
  params.setOutputDomain(domainOutput2.get());
  
  // 4 * 12 = out of bound, current domain should be empty
  std::vector<INT_64> vec3 = {4, 12};
  ArrayPtr array3 = getArray(vec3);
  params.setDomainElementArray(array3);
  domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 0);
}//TIMES1

OPTILAB_TEST(FilteringAlgorithmTIMES, TIMES2)
{
  using namespace Core;
  
  FilteringAlgorithmTIMES filter;
  DomainIntPtr domainOutput = getDomainInt(-1000, +1000);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  
  // 2 * 400 = 800
  std::vector<INT_64> vec = {2, 400};
  ArrayPtr array = getArray(vec);
  params.setDomainElementArray(array);
  
  auto domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), getElem(800).get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), getElem(800).get()));
  
  DomainIntPtr domainOutput2 = getDomainInt(-1000, +1000);
  params.setOutputDomain(domainOutput2.get());
  
  // 400 * 0 = 0
  std::vector<INT_64> vec2 = {400, 0};
  ArrayPtr array2 = getArray(vec2);
  params.setDomainElementArray(array2);
  
  domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), getElem(0).get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), getElem(0).get()));
  
  // 400 * 800 = out of bound current domain should be empty
  DomainIntPtr domainOutput3 = getDomainInt(-1000, +1000);
  params.setOutputDomain(domainOutput3.get());
  
  std::vector<INT_64> vec3 = {400, 800};
  ArrayPtr array3 = getArray(vec3);
  params.setDomainElementArray(array3);
  
  domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 0);
}//TIMES2

OPTILAB_TEST(FilteringAlgorithmTIMES, TIMES3)
{
  using namespace Core;
  
  FilteringAlgorithmTIMES filter;
  
  std::vector<INT_64> set;
  for(INT_64 elem = -10; elem <= 10; elem += 2)
  {
    set.push_back(elem);
  }
  DomainIntPtr domainOutput = getDomainInt(set);
  
  // 2 * 5 = 10
  std::vector<INT_64> vec = {2, 5};
  ArrayPtr array = getArray(vec);
  FilteringParameters params;
  params.setDomainElementArray(array);
  params.setOutputDomain(domainOutput.get());
  
  auto domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), getElem(+10).get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), getElem(+10).get()));
  
  DomainIntPtr domainOutput2 = getDomainInt(set);
  
  // 3 * 3 = 9 not in set
  std::vector<INT_64> vec2 = {3, 3};
  ArrayPtr array2 = getArray(vec2);
  params.setDomainElementArray(array2);
  params.setOutputDomain(domainOutput.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 0);
}//TIMES3


