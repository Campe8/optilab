
#include "utest_globals.hpp"

#include "FilteringAlgorithmINTERSECT.hpp"

#include "ConstraintTestInc.hpp"

OPTILAB_TEST(FilteringAlgorithmINTERSECT, INTERSECTInputDomain)
{
  using namespace Core;
  
  FilteringAlgorithmINTERSECT filter;
  
  filter.setIntersectionSetType(FilteringAlgorithmINTERSECT::IntersectionSetType::IS_DOMAIN);
  DomainIntPtr domainOutput    = getDomainInt(-10, +10);
  DomainIntPtr domainIntersect = getDomainInt(+8, +12);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  
  DomainArrayPtr array = std::make_shared<DomainArray>(1);
  params.setInputDomainArray(array);
  
  array->assignElementToCell(0, domainIntersect.get());
  
  auto domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 3);
  
  ElemPtr elemPos8 = getElem(+8);
  ElemPtr elemPos10 = getElem(+10);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemPos8.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos10.get()));
  
  // Intersect bigger domain should not change current domain
  DomainIntPtr domainOutput2    = getDomainInt(-10, +10);
  DomainIntPtr domainIntersect2 = getDomainInt(-12, +12);
  params.setOutputDomain(domainOutput2.get());
  array->assignElementToCell(0, domainIntersect2.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 21);
  ElemPtr elemNeg10 = getElem(-10);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg10.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos10.get()));
  
  // Intersect domain with no elements in common should empty current domain
  DomainIntPtr domainOutput3    = getDomainInt(-10, +10);
  DomainIntPtr domainIntersect3 = getDomainInt(+11, +12);
  params.setOutputDomain(domainOutput3.get());
  array->assignElementToCell(0, domainIntersect3.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 0);
  
  // Intersect smaller domain should change current domain to smaller domain
  DomainIntPtr domainOutput4    = getDomainInt(-10, +10);
  DomainIntPtr domainIntersect4 = getDomainInt(-1, +1);
  params.setOutputDomain(domainOutput4.get());
  array->assignElementToCell(0, domainIntersect4.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 3);
  ElemPtr elemNeg1 = getElem(-1);
  ElemPtr elemPos1 = getElem(+1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg1.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos1.get()));
}//INTERSECTInputDomain

OPTILAB_TEST(FilteringAlgorithmINTERSECT, INTERSECTInputDomain2)
{
  using namespace Core;
  
  FilteringAlgorithmINTERSECT filter;
  
  filter.setIntersectionSetType(FilteringAlgorithmINTERSECT::IntersectionSetType::IS_DOMAIN);
  DomainIntPtr domainOutput    = getDomainInt(-1000, +1000);
  DomainIntPtr domainIntersect = getDomainInt(+800, +1200);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  
  DomainArrayPtr array = std::make_shared<DomainArray>(1);
  params.setInputDomainArray(array);
  
  array->assignElementToCell(0, domainIntersect.get());
  
  auto domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 201);
  
  ElemPtr elemPos800 = getElem(+800);
  ElemPtr elemPos1000 = getElem(+1000);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemPos800.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos1000.get()));
  
  // Intersect bigger domain should not change current domain
  DomainIntPtr domainOutput2    = getDomainInt(-1000, +1000);
  DomainIntPtr domainIntersect2 = getDomainInt(-1200, +1200);
  params.setOutputDomain(domainOutput2.get());
  array->assignElementToCell(0, domainIntersect2.get());
  
  domOut = filter.filter(params);
  ElemPtr elemNeg1000 = getElem(-1000);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg1000.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos1000.get()));
  
  // Intersect domain with no elements in common should empty current domain
  DomainIntPtr domainOutput3    = getDomainInt(-1000, +1000);
  DomainIntPtr domainIntersect3 = getDomainInt(+1001, +1200);
  params.setOutputDomain(domainOutput3.get());
  array->assignElementToCell(0, domainIntersect3.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 0);
  
  // Intersect smaller domain should change current domain to smaller domain
  DomainIntPtr domainOutput4    = getDomainInt(-1000, +1000);
  DomainIntPtr domainIntersect4 = getDomainInt(-100, +100);
  params.setOutputDomain(domainOutput4.get());
  array->assignElementToCell(0, domainIntersect4.get());
  
  domOut = filter.filter(params);
  ElemPtr elemNeg100 = getElem(-100);
  ElemPtr elemPos100 = getElem(+100);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg100.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos100.get()));
}//INTERSECTInputDomain2

OPTILAB_TEST(FilteringAlgorithmINTERSECT, INTERSECTInputDomain3)
{
  using namespace Core;
  
  FilteringAlgorithmINTERSECT filter;
  
  filter.setIntersectionSetType(FilteringAlgorithmINTERSECT::IntersectionSetType::IS_DOMAIN);
  std::vector<INT_64> set;
  for(INT_64 elem = -500; elem <= 500; elem += 2)
  {
    set.push_back(elem);
  }
  DomainIntPtr domainOutput = getDomainInt(set);
  DomainIntPtr domainIntersect = getDomainInt(+8, +12);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  
  DomainArrayPtr array = std::make_shared<DomainArray>(1);
  params.setInputDomainArray(array);
  
  array->assignElementToCell(0, domainIntersect.get());
  
  auto domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 3);
  
  ElemPtr elemPos8 = getElem(+8);
  ElemPtr elemPos12 = getElem(+12);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemPos8.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos12.get()));
  
  // Intersect bigger domain should not change current domain
  
  DomainIntPtr domainOutput2    = getDomainInt(set);
  DomainIntPtr domainIntersect2 = getDomainInt(-501, 501);
  params.setOutputDomain(domainOutput2.get());
  array->assignElementToCell(0, domainIntersect2.get());
  
  domOut = filter.filter(params);
  ElemPtr elemNeg500 = getElem(-500);
  ElemPtr elemPos500 = getElem(+500);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg500.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos500.get()));
  
  // Intersect domain with no elements in common should empty current domain
  DomainIntPtr domainOutput3    = getDomainInt(set);
  DomainIntPtr domainIntersect3 = getDomainInt(+502, +505);
  params.setOutputDomain(domainOutput3.get());
  array->assignElementToCell(0, domainIntersect3.get());
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 0);
  
  // Intersect smaller domain should change current domain to smaller domain
  DomainIntPtr domainOutput4    = getDomainInt(set);
  DomainIntPtr domainIntersect4 = getDomainInt(-10, +10);
  params.setOutputDomain(domainOutput4.get());
  array->assignElementToCell(0, domainIntersect4.get());
  
  domOut = filter.filter(params);
  ElemPtr elemNeg10 = getElem(-10);
  ElemPtr elemPos10 = getElem(+10);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg10.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos10.get()));
}//INTERSECTInputDomain3

OPTILAB_TEST(FilteringAlgorithmINTERSECT, INTERSECTDomainElementArray)
{
  using namespace Core;
  
  FilteringAlgorithmINTERSECT filter;
  filter.setIntersectionSetType(FilteringAlgorithmINTERSECT::IntersectionSetType::IS_DOMAIN_ELEMENT_ARRAY);

  DomainIntPtr domainOutput = getDomainInt(-10, +10);
  ArrayPtr domainIntersect = getArray(+8, +12);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  params.setDomainElementArray(domainIntersect);
  
  auto domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 3);
  
  ElemPtr elemPos8 = getElem(+8);
  ElemPtr elemPos10 = getElem(+10);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemPos8.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos10.get()));
  
  // Intersect bigger domain should not change current domain
  DomainIntPtr domainOutput2    = getDomainInt(-10, +10);
  ArrayPtr domainIntersect2 = getArray(-12, +12);
  params.setOutputDomain(domainOutput2.get());
  params.setDomainElementArray(domainIntersect2);
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 21);
  ElemPtr elemNeg10 = getElem(-10);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg10.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos10.get()));
  
  // Intersect domain with no elements in common should empty current domain
  DomainIntPtr domainOutput3    = getDomainInt(-10, +10);
  ArrayPtr domainIntersect3 = getArray(+11, +12);
  params.setOutputDomain(domainOutput3.get());
  params.setDomainElementArray(domainIntersect3);
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 0);
  
  // Intersect smaller domain should change current domain to smaller domain
  DomainIntPtr domainOutput4    = getDomainInt(-10, +10);
  ArrayPtr domainIntersect4 = getArray(-1, +1);
  params.setOutputDomain(domainOutput4.get());
  params.setDomainElementArray(domainIntersect4);
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 3);
  ElemPtr elemNeg1 = getElem(-1);
  ElemPtr elemPos1 = getElem(+1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg1.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos1.get()));
}//INTERSECTDomainElementArray

OPTILAB_TEST(FilteringAlgorithmINTERSECT, INTERSECTDomainElementArray2)
{
  using namespace Core;
  
  FilteringAlgorithmINTERSECT filter;
  filter.setIntersectionSetType(FilteringAlgorithmINTERSECT::IntersectionSetType::IS_DOMAIN_ELEMENT_ARRAY);
  
  DomainIntPtr domainOutput    = getDomainInt(-1000, +1000);
  ArrayPtr domainIntersect = getArray(+8, +12);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  params.setDomainElementArray(domainIntersect);
  
  auto domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 5);
  
  ElemPtr elemPos8 = getElem(+8);
  ElemPtr elemPos12 = getElem(+12);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemPos8.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos12.get()));
  
  // Intersect domain with no elements in common should empty current domain
  DomainIntPtr domainOutput3    = getDomainInt(-1000, +1000);
  ArrayPtr domainIntersect3 = getArray(+1001, +1002);
  params.setOutputDomain(domainOutput3.get());
  params.setDomainElementArray(domainIntersect3);
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 0);
  
  // Intersect smaller domain should change current domain to smaller domain
  DomainIntPtr domainOutput4    = getDomainInt(-1000, +1000);
  ArrayPtr domainIntersect4 = getArray(-1, +1);
  params.setOutputDomain(domainOutput4.get());
  params.setDomainElementArray(domainIntersect4);
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 3);
  ElemPtr elemNeg1 = getElem(-1);
  ElemPtr elemPos1 = getElem(+1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg1.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos1.get()));
}//INTERSECTDomainElementArray2

OPTILAB_TEST(FilteringAlgorithmINTERSECT, INTERSECTDomainElementArray3)
{
  using namespace Core;
  
  FilteringAlgorithmINTERSECT filter;
  filter.setIntersectionSetType(FilteringAlgorithmINTERSECT::IntersectionSetType::IS_DOMAIN_ELEMENT_ARRAY);
  
  std::vector<INT_64> set;
  for(INT_64 elem = -500; elem <= 500; elem += 2)
  {
    set.push_back(elem);
  }
  DomainIntPtr domainOutput = getDomainInt(set);
  ArrayPtr domainIntersect = getArray(+8, +12);
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  params.setDomainElementArray(domainIntersect);
  
  auto domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 3);
  
  ElemPtr elemPos8 = getElem(+8);
  ElemPtr elemPos12 = getElem(+12);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemPos8.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos12.get()));
  
  // Intersect bigger domain should not change current domain
  
  DomainIntPtr domainOutput2    = getDomainInt(set);
  ArrayPtr domainIntersect2 = getArray(-501, 501);
  params.setOutputDomain(domainOutput2.get());
  params.setDomainElementArray(domainIntersect2);
  
  domOut = filter.filter(params);
  ElemPtr elemNeg500 = getElem(-500);
  ElemPtr elemPos500 = getElem(+500);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg500.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos500.get()));
  
  // Intersect domain with no elements in common should empty current domain
  DomainIntPtr domainOutput3    = getDomainInt(set);
  ArrayPtr domainIntersect3 = getArray(+502, +505);
  params.setOutputDomain(domainOutput3.get());
  params.setDomainElementArray(domainIntersect3);
  
  domOut = filter.filter(params);
  ASSERT_EQ(domOut->getSize(), 0);
  
  // Intersect smaller domain should change current domain to smaller domain
  DomainIntPtr domainOutput4    = getDomainInt(set);
  ArrayPtr domainIntersect4 = getArray(-10, +10);
  params.setOutputDomain(domainOutput4.get());
  params.setDomainElementArray(domainIntersect4);
  
  domOut = filter.filter(params);
  ElemPtr elemNeg10 = getElem(-10);
  ElemPtr elemPos10 = getElem(+10);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemNeg10.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos10.get()));
}//INTERSECTDomainElementArray3
