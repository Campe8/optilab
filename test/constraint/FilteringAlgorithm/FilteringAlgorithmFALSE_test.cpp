
#include "utest_globals.hpp"

#include "FilteringAlgorithmFALSE.hpp"

#include "ConstraintTestInc.hpp"

OPTILAB_TEST(FilteringAlgorithmFALSE, BFALSE)
{
  using namespace Core;
  
  FilteringAlgorithmFALSE filter;
  DomainBoolPtr domainOutput = getDomainBool();
  
  FilteringParameters params;
  params.setOutputDomain(domainOutput.get());
  
  auto domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ElemPtr elemPos0 = getElem(0);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemPos0.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos0.get()));
  
  DomainBoolPtr domainOutput2 = getDomainBool();
  domainOutput2->setTrue();
  params.setOutputDomain(domainOutput2.get());
  
  domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 0);
  
  DomainBoolPtr domainOutput3 = getDomainBool();
  domainOutput3->setFalse();
  params.setOutputDomain(domainOutput3.get());
  
  domOut = filter.filter(params);
  
  ASSERT_EQ(domOut->getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domOut->lowerBound(), elemPos0.get()));
  ASSERT_TRUE(DomainElement::isEqual(domOut->upperBound(), elemPos0.get()));
}//BFALSE
