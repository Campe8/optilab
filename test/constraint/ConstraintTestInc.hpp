#pragma once

#include "DomainInteger.hpp"
#include "DomainBoolean.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"

#include "Constraint.hpp"

#include "DepthFirstSearch.hpp"

#include <utility>

#define USE_BOUND_PROP true
#define USE_DOMAIN_PROP false

typedef std::shared_ptr<Core::DomainElementInt64> ElemPtr;
typedef std::shared_ptr<Core::DomainElementArray> ArrayPtr;
typedef std::shared_ptr<Core::DomainInteger>  DomainIntPtr;
typedef std::shared_ptr<Core::DomainBoolean>  DomainBoolPtr;
typedef std::shared_ptr<Core::DomainArray>  DomainArrayPtr;

DomainBoolPtr getDomainBool();

DomainIntPtr getDomainInt(INT_64 aLower, INT_64 aUpper);

DomainIntPtr getDomainInt(const std::vector<INT_64>& aElems);

/// Returns element
ElemPtr getElem(INT_64 aVal);

/// Returns DomainElementArraySPtr
ArrayPtr getArray(const std::vector<INT_64>& aArray);

/// Returns DomainElementArraySPtr
ArrayPtr getArray(INT_64 aMin, INT_64 aMax);

/// Returns DomainArraySPtr
DomainArrayPtr getDomainArray(const std::vector<std::shared_ptr<Core::Domain>>& aDomains);

namespace Core {
  class ConstraintTest : public Constraint
  {
  public:
    ConstraintTest()
    : Constraint(ConstraintId::CON_ID_UNDEF, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND)
    , pValueTest(0)
    {
    }
    
    void updateOnDomainEvent(DomainEvent* aDomainEvent) override
    {
      (void) aDomainEvent;
      pValueTest = 1000;
    }
    
    inline int getValueTest() const
    {
      return pValueTest;
    }
    
    inline void resetInternalValue(int aVal)
    {
      pValueTest = aVal;
    }
    
    void registerConstraintParameter(const ConstraintParameterSPtr& aConstraintParameter)
    {
      Constraint::registerConstraintParameter(aConstraintParameter);
    }
    
    inline std::size_t numConstraintParameters() const
    {
      return Constraint::numConstraintParameters();
    }
    
    inline DomainSPtr getParameterDomain(std::size_t aParameterIndex)
    {
      return Constraint::getParameterDomain(aParameterIndex);
    }
    
    void registerPropagator(PropagatorSPtr aPropagator)
    {
      Constraint::registerPropagator(aPropagator);
    }
    
    PropagatorSPtr getRegisteredPropagator(PropagatorSemanticType aPropagatorSemanticType, std::size_t aPropIdx = 0)
    {
      return Constraint::getRegisteredPropagator(aPropagatorSemanticType, aPropIdx);
    }
    
    std::size_t numRegisteredPropagators(PropagatorSemanticType aPropagatorSemanticType) const
    {
      return Constraint::numRegisteredPropagators(aPropagatorSemanticType);
    }
    
    bool isPropagatorDisabled(PropagatorSemanticType aPropagatorSemanticType, std::size_t aPropIdx = 0)
    {
      return Constraint::isPropagatorDisabled(aPropagatorSemanticType, aPropIdx);
    }
    
    void disablePropagator(PropagatorSemanticType aPropagatorSemanticType, std::size_t aPropIdx = 0)
    {
      Constraint::disablePropagator(aPropagatorSemanticType, aPropIdx);
    }
    
    void enablePropagator(PropagatorSemanticType aPropagatorSemanticType, std::size_t aPropIdx = 0)
    {
      Constraint::enablePropagator(aPropagatorSemanticType, aPropIdx);
    }
    
  private:
    int pValueTest;
  };
  
} // end namespace Core
