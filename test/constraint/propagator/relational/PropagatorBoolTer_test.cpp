
#include "utest_globals.hpp"

#include "PropagatorTest.hpp"

/*********************** Test ***********************/

OPTILAB_TEST(PropagatorBoolTerTest, PostTerAnd)
{
  using namespace Core;
  
  PropagatorBoolTerTest andTer(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_AND_TER);
  
  // T /\ T <-> T -> subsumed
  andTer.setDomains(true, true, true, true, true, true);
  andTer.postPropagator(true, true, true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F /\ T <-> T -> fail
  andTer.setDomains(false, false, true, true, true, true);
  andTer.postPropagator(false, false, true, true, true, true, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T /\ F <-> T -> fail
  andTer.setDomains(true, true, false, false, true, true);
  andTer.postPropagator(true, true, false, false, true, true, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T /\ T <-> F -> fail
  andTer.setDomains(true, true, true, true, false, false);
  andTer.postPropagator(true, true, true, true, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F /\ F <-> T -> fail
  andTer.setDomains(false, false, false, false, true, true);
  andTer.postPropagator(false, false, false, false, true, true, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F /\ T <-> F -> subsumed
  andTer.setDomains(false, false, true, true, false, false);
  andTer.postPropagator(false, false, true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T /\ F <-> F -> subsumed
  andTer.setDomains(true, true, false, false, false, false);
  andTer.postPropagator(true, true, false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F /\ F <-> F -> subsumed
  andTer.setDomains(false, false, false, false, false, false);
  andTer.postPropagator(false, false, false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] /\ F <-> F -> subsumed
  andTer.setDomains(false, true, false, false, false, false);
  andTer.postPropagator(false, true, false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] /\ T <-> F -> subsumed
  andTer.setDomains(false, true, true, true, false, false);
  andTer.postPropagator(false, false, true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] /\ T <-> T -> subsumed
  andTer.setDomains(false, true, true, true, true, true);
  andTer.postPropagator(true, true, true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] /\ T <-> F -> subsumed
  andTer.setDomains(false, true, true, true, false, false);
  andTer.postPropagator(false, false, true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T /\ T <-> [F, T] -> subsumed
  andTer.setDomains(true, true, true, true, false, true);
  andTer.postPropagator(true, true, true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T /\ F <-> [F, T] -> subsumed
  andTer.setDomains(true, true, false, false, false, true);
  andTer.postPropagator(true, true, false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F /\ F <-> [F, T] -> subsumed
  andTer.setDomains(false, false, false, false, false, true);
  andTer.postPropagator(false, false, false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] /\ [F, T] <-> T -> subsumed
  andTer.setDomains(false, true, false, true, true, true);
  andTer.postPropagator(true, true, true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] /\ [F, T] <-> F -> undef
  andTer.setDomains(false, true, false, true, false, false);
  andTer.postPropagator(false, true, false, true, false, false, Core::PropagationEvent::PROP_EVENT_UNDEF);
  
  // [F, T] /\ T <-> [F, T] -> unspec
  andTer.setDomains(false, true, true, true, false, true);
  andTer.postPropagator(false, true, true, true, false, true, Core::PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // F /\ [F, T] <-> [F, T] -> subsumed
  andTer.setDomains(false, false, false, true, false, true);
  andTer.postPropagator(false, false, false, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);  
  
  // [F, T] /\ [F, T] <-> [F, T] -> unspec
  andTer.setDomains(false, true, false, true, false, true);
  andTer.postPropagator(false, true, false, true, false, true, Core::PropagationEvent::PROP_EVENT_RUN_UNSPEC);
}//PostTerAnd

OPTILAB_TEST(PropagatorBoolTerTest, RunTerAnd)
{
  using namespace Core;
  
  PropagatorBoolTerTest andTer(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_AND_TER);
  
  // T /\ T <-> T -> subsumed
  andTer.setDomains(true, true, true, true, true, true);
  andTer.runPropagationAndCheck(true, true, true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F /\ T <-> T -> fail
  andTer.setDomains(false, false, true, true, true, true);
  andTer.runPropagationAndCheck(false, false, true, true, true, true, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T /\ F <-> T -> fail
  andTer.setDomains(true, true, false, false, true, true);
  andTer.runPropagationAndCheck(true, true, false, false, true, true, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T /\ T <-> F -> fail
  andTer.setDomains(true, true, true, true, false, false);
  andTer.runPropagationAndCheck(true, true, true, true, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F /\ F <-> T -> fail
  andTer.setDomains(false, false, false, false, true, true);
  andTer.runPropagationAndCheck(false, false, false, false, true, true, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F /\ T <-> F -> subsumed
  andTer.setDomains(false, false, true, true, false, false);
  andTer.runPropagationAndCheck(false, false, true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T /\ F <-> F -> subsumed
  andTer.setDomains(true, true, false, false, false, false);
  andTer.runPropagationAndCheck(true, true, false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F /\ F <-> F -> subsumed
  andTer.setDomains(false, false, false, false, false, false);
  andTer.runPropagationAndCheck(false, false, false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] /\ F <-> F -> subsumed
  andTer.setDomains(false, true, false, false, false, false);
  andTer.runPropagationAndCheck(false, true, false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] /\ T <-> F -> subsumed
  andTer.setDomains(false, true, true, true, false, false);
  andTer.runPropagationAndCheck(false, false, true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] /\ T <-> T -> subsumed
  andTer.setDomains(false, true, true, true, true, true);
  andTer.runPropagationAndCheck(true, true, true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] /\ T <-> F -> subsumed
  andTer.setDomains(false, true, true, true, false, false);
  andTer.runPropagationAndCheck(false, false, true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T /\ T <-> [F, T] -> subsumed
  andTer.setDomains(true, true, true, true, false, true);
  andTer.runPropagationAndCheck(true, true, true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T /\ F <-> [F, T] -> subsumed
  andTer.setDomains(true, true, false, false, false, true);
  andTer.runPropagationAndCheck(true, true, false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F /\ F <-> [F, T] -> subsumed
  andTer.setDomains(false, false, false, false, false, true);
  andTer.runPropagationAndCheck(false, false, false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] /\ [F, T] <-> T -> subsumed
  andTer.setDomains(false, true, false, true, true, true);
  andTer.runPropagationAndCheck(true, true, true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] /\ [F, T] <-> F -> undef
  andTer.setDomains(false, true, false, true, false, false);
  andTer.runPropagationAndCheck(false, true, false, true, false, false, Core::PropagationEvent::PROP_EVENT_UNDEF);
  
  // [F, T] /\ T <-> [F, T] -> fixpoint
  andTer.setDomains(false, true, true, true, false, true);
  andTer.runPropagationAndCheck(false, true, true, true, false, true, Core::PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // F /\ [F, T] <-> [F, T] -> subsumed
  andTer.setDomains(false, false, false, true, false, true);
  andTer.runPropagationAndCheck(false, false, false, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);  
  
  // [F, T] /\ [F, T] <-> [F, T] -> fixpoint
  andTer.setDomains(false, true, false, true, false, true);
  andTer.runPropagationAndCheck(false, true, false, true, false, true, Core::PropagationEvent::PROP_EVENT_FIXPOINT);
}//RunTerAnd

OPTILAB_TEST(PropagatorBoolTerTest, PostTerOr)
{
  using namespace Core;
  
  PropagatorBoolTerTest orTer(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_OR_TER);
  
  // T \/ T <-> T -> subsumed
  orTer.setDomains(true, true, true, true, true, true);
  orTer.postPropagator(true, true, true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F \/ T <-> T -> subsumed
  orTer.setDomains(false, false, true, true, true, true);
  orTer.postPropagator(false, false, true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T \/ F <-> T -> subsumed
  orTer.setDomains(true, true, false, false, true, true);
  orTer.postPropagator(true, true, false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T \/ T <-> F -> fail
  orTer.setDomains(true, true, true, true, false, false);
  orTer.postPropagator(true, true, true, true, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F \/ F <-> T -> fail
  orTer.setDomains(false, false, false, false, true, true);
  orTer.postPropagator(false, false, false, false, true, true, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F \/ T <-> F -> fail
  orTer.setDomains(false, false, true, true, false, false);
  orTer.postPropagator(false, false, true, true, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T \/ F <-> F -> fail
  orTer.setDomains(true, true, false, false, false, false);
  orTer.postPropagator(true, true, false, false, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F \/ F <-> F -> subsumed
  orTer.setDomains(false, false, false, false, false, false);
  orTer.postPropagator(false, false, false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] \/ F <-> F -> subsumed
  orTer.setDomains(false, true, false, false, false, false);
  orTer.postPropagator(false, false, false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] \/ T <-> F -> fail
  orTer.setDomains(false, true, true, true, false, false);
  orTer.postPropagator(false, true, true, true, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // [F, T] \/ T <-> T -> subsumed
  orTer.setDomains(false, true, true, true, true, true);
  orTer.postPropagator(false, true, true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] \/ T <-> F -> fail
  orTer.setDomains(false, true, true, true, false, false);
  orTer.postPropagator(false, true, true, true, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T \/ T <-> [F, T] -> subsumed
  orTer.setDomains(true, true, true, true, false, true);
  orTer.postPropagator(true, true, true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T \/ F <-> [F, T] -> subsumed
  orTer.setDomains(true, true, false, false, false, true);
  orTer.postPropagator(true, true, false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F \/ F <-> [F, T] -> subsumed
  orTer.setDomains(false, false, false, false, false, true);
  orTer.postPropagator(false, false, false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] \/ [F, T] <-> T -> unspec
  orTer.setDomains(false, true, false, true, true, true);
  orTer.postPropagator(false, true, false, true, true, true, Core::PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // [F, T] \/ [F, T] <-> F -> subsumed
  orTer.setDomains(false, true, false, true, false, false);
  orTer.postPropagator(false, false, false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] \/ T <-> [F, T] -> subsumed
  orTer.setDomains(false, true, true, true, false, true);
  orTer.postPropagator(false, true, true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F \/ [F, T] <-> [F, T] -> unspec
  orTer.setDomains(false, false, false, true, false, true);
  orTer.postPropagator(false, false, false, true, false, true, Core::PropagationEvent::PROP_EVENT_RUN_UNSPEC);  
  
  // [F, T] \/ [F, T] <-> [F, T] -> unspec
  orTer.setDomains(false, true, false, true, false, true);
  orTer.postPropagator(false, true, false, true, false, true, Core::PropagationEvent::PROP_EVENT_RUN_UNSPEC);
}//PostTerOr

OPTILAB_TEST(PropagatorBoolTerTest, RunTerOr)
{
  using namespace Core;
  
  PropagatorBoolTerTest orTer(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_OR_TER);
  
  // T \/ T <-> T -> subsumed
  orTer.setDomains(true, true, true, true, true, true);
  orTer.runPropagationAndCheck(true, true, true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F \/ T <-> T -> subsumed
  orTer.setDomains(false, false, true, true, true, true);
  orTer.runPropagationAndCheck(false, false, true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T \/ F <-> T -> subsumed
  orTer.setDomains(true, true, false, false, true, true);
  orTer.runPropagationAndCheck(true, true, false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T \/ T <-> F -> fail
  orTer.setDomains(true, true, true, true, false, false);
  orTer.runPropagationAndCheck(true, true, true, true, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F \/ F <-> T -> fail
  orTer.setDomains(false, false, false, false, true, true);
  orTer.runPropagationAndCheck(false, false, false, false, true, true, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F \/ T <-> F -> fail
  orTer.setDomains(false, false, true, true, false, false);
  orTer.runPropagationAndCheck(false, false, true, true, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T \/ F <-> F -> fail
  orTer.setDomains(true, true, false, false, false, false);
  orTer.runPropagationAndCheck(true, true, false, false, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F \/ F <-> F -> subsumed
  orTer.setDomains(false, false, false, false, false, false);
  orTer.runPropagationAndCheck(false, false, false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] \/ F <-> F -> subsumed
  orTer.setDomains(false, true, false, false, false, false);
  orTer.runPropagationAndCheck(false, false, false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] \/ T <-> F -> fail
  orTer.setDomains(false, true, true, true, false, false);
  orTer.runPropagationAndCheck(false, true, true, true, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // [F, T] \/ T <-> T -> subsumed
  orTer.setDomains(false, true, true, true, true, true);
  orTer.runPropagationAndCheck(false, true, true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] \/ T <-> F -> fail
  orTer.setDomains(false, true, true, true, false, false);
  orTer.runPropagationAndCheck(false, true, true, true, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T \/ T <-> [F, T] -> subsumed
  orTer.setDomains(true, true, true, true, false, true);
  orTer.runPropagationAndCheck(true, true, true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T \/ F <-> [F, T] -> subsumed
  orTer.setDomains(true, true, false, false, false, true);
  orTer.runPropagationAndCheck(true, true, false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F \/ F <-> [F, T] -> subsumed
  orTer.setDomains(false, false, false, false, false, true);
  orTer.runPropagationAndCheck(false, false, false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] \/ [F, T] <-> T -> fixpoint
  orTer.setDomains(false, true, false, true, true, true);
  orTer.runPropagationAndCheck(false, true, false, true, true, true, Core::PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [F, T] \/ [F, T] <-> F -> subsumed
  orTer.setDomains(false, true, false, true, false, false);
  orTer.runPropagationAndCheck(false, false, false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] \/ T <-> [F, T] -> subsumed
  orTer.setDomains(false, true, true, true, false, true);
  orTer.runPropagationAndCheck(false, true, true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F \/ [F, T] <-> [F, T] -> fixpoint
  orTer.setDomains(false, false, false, true, false, true);
  orTer.runPropagationAndCheck(false, false, false, true, false, true, Core::PropagationEvent::PROP_EVENT_FIXPOINT);  
  
  // [F, T] \/ [F, T] <-> [F, T] -> fixpoint
  orTer.setDomains(false, true, false, true, false, true);
  orTer.runPropagationAndCheck(false, true, false, true, false, true, Core::PropagationEvent::PROP_EVENT_FIXPOINT);
}//RunTerOr

OPTILAB_TEST(PropagatxorBoolTerTest, PostTerXor)
{
  using namespace Core;
  
  PropagatorBoolTerTest xorTer(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_XOR_TER);
  
  // T != T <-> T -> fail
  xorTer.setDomains(true, true, true, true, true, true);
  xorTer.postPropagator(true, true, true, true, true, true, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F != T <-> T -> subsumed
  xorTer.setDomains(false, false, true, true, true, true);
  xorTer.postPropagator(false, false, true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T != F <-> T -> subsumed
  xorTer.setDomains(true, true, false, false, true, true);
  xorTer.postPropagator(true, true, false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T != T <-> F -> subsumed
  xorTer.setDomains(true, true, true, true, false, false);
  xorTer.postPropagator(true, true, true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F != F <-> T -> fail
  xorTer.setDomains(false, false, false, false, true, true);
  xorTer.postPropagator(false, false, false, false, true, true, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F != T <-> F -> fail
  xorTer.setDomains(false, false, true, true, false, false);
  xorTer.postPropagator(false, false, true, true, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T != F <-> F -> fail
  xorTer.setDomains(true, true, false, false, false, false);
  xorTer.postPropagator(true, true, false, false, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F != F <-> F -> subsumed
  xorTer.setDomains(false, false, false, false, false, false);
  xorTer.postPropagator(false, false, false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] != F <-> F -> subsumed
  xorTer.setDomains(false, true, false, false, false, false);
  xorTer.postPropagator(false, false, false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] != T <-> F -> subsumed
  xorTer.setDomains(false, true, true, true, false, false);
  xorTer.postPropagator(true, true, true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] != T <-> T -> unspec
  xorTer.setDomains(false, true, true, true, true, true);
  xorTer.postPropagator(false, false, true, true, true, true, Core::PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // [F, T] != T <-> F -> subsumed
  xorTer.setDomains(false, true, true, true, false, false);
  xorTer.postPropagator(true, true, true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T != T <-> [F, T] -> subsumed
  xorTer.setDomains(true, true, true, true, false, true);
  xorTer.postPropagator(true, true, true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T != F <-> [F, T] -> subsumed
  xorTer.setDomains(true, true, false, false, false, true);
  xorTer.postPropagator(true, true, false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F != F <-> [F, T] -> subsumed
  xorTer.setDomains(false, false, false, false, false, true);
  xorTer.postPropagator(false, false, false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] != [F, T] <-> T -> unspec
  xorTer.setDomains(false, true, false, true, true, true);
  xorTer.postPropagator(false, true, false, true, true, true, Core::PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // [F, T] != [F, T] <-> F -> unspec
  xorTer.setDomains(false, true, false, true, false, false);
  xorTer.postPropagator(false, true, false, true, false, false, Core::PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // [F, T] != T <-> [F, T] -> unspec
  xorTer.setDomains(false, true, true, true, false, true);
  xorTer.postPropagator(false, true, true, true, false, true, Core::PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // F != [F, T] <-> [F, T] -> unspec
  xorTer.setDomains(false, false, false, true, false, true);
  xorTer.postPropagator(false, false, false, true, false, true, Core::PropagationEvent::PROP_EVENT_RUN_UNSPEC);  
  
  // [F, T] != [F, T] <-> [F, T] -> unspec
  xorTer.setDomains(false, true, false, true, false, true);
  xorTer.postPropagator(false, true, false, true, false, true, Core::PropagationEvent::PROP_EVENT_RUN_UNSPEC);
}//PostTerXor

OPTILAB_TEST(PropagatxorBoolTerTest, RunTerXor)
{
  using namespace Core;
  
  PropagatorBoolTerTest xorTer(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_XOR_TER);
  
  // T != T <-> T -> fail
  xorTer.setDomains(true, true, true, true, true, true);
  xorTer.runPropagationAndCheck(true, true, true, true, true, true, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F != T <-> T -> subsumed
  xorTer.setDomains(false, false, true, true, true, true);
  xorTer.runPropagationAndCheck(false, false, true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T != F <-> T -> subsumed
  xorTer.setDomains(true, true, false, false, true, true);
  xorTer.runPropagationAndCheck(true, true, false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T != T <-> F -> subsumed
  xorTer.setDomains(true, true, true, true, false, false);
  xorTer.runPropagationAndCheck(true, true, true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F != F <-> T -> fail
  xorTer.setDomains(false, false, false, false, true, true);
  xorTer.runPropagationAndCheck(false, false, false, false, true, true, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F != T <-> F -> fail
  xorTer.setDomains(false, false, true, true, false, false);
  xorTer.runPropagationAndCheck(false, false, true, true, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T != F <-> F -> fail
  xorTer.setDomains(true, true, false, false, false, false);
  xorTer.runPropagationAndCheck(true, true, false, false, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F != F <-> F -> subsumed
  xorTer.setDomains(false, false, false, false, false, false);
  xorTer.runPropagationAndCheck(false, false, false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] != F <-> F -> subsumed
  xorTer.setDomains(false, true, false, false, false, false);
  xorTer.runPropagationAndCheck(false, false, false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] != T <-> F -> subsumed
  xorTer.setDomains(false, true, true, true, false, false);
  xorTer.runPropagationAndCheck(true, true, true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] != T <-> T -> fixpoint
  xorTer.setDomains(false, true, true, true, true, true);
  xorTer.runPropagationAndCheck(false, false, true, true, true, true, Core::PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [F, T] != T <-> F -> subsumed
  xorTer.setDomains(false, true, true, true, false, false);
  xorTer.runPropagationAndCheck(true, true, true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T != T <-> [F, T] -> subsumed
  xorTer.setDomains(true, true, true, true, false, true);
  xorTer.runPropagationAndCheck(true, true, true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T != F <-> [F, T] -> subsumed
  xorTer.setDomains(true, true, false, false, false, true);
  xorTer.runPropagationAndCheck(true, true, false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F != F <-> [F, T] -> subsumed
  xorTer.setDomains(false, false, false, false, false, true);
  xorTer.runPropagationAndCheck(false, false, false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] != [F, T] <-> T -> fixpoint
  xorTer.setDomains(false, true, false, true, true, true);
  xorTer.runPropagationAndCheck(false, true, false, true, true, true, Core::PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [F, T] != [F, T] <-> F -> fixpoint
  xorTer.setDomains(false, true, false, true, false, false);
  xorTer.runPropagationAndCheck(false, true, false, true, false, false, Core::PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [F, T] != T <-> [F, T] -> fixpoint
  xorTer.setDomains(false, true, true, true, false, true);
  xorTer.runPropagationAndCheck(false, true, true, true, false, true, Core::PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // F != [F, T] <-> [F, T] -> fixpoint
  xorTer.setDomains(false, false, false, true, false, true);
  xorTer.runPropagationAndCheck(false, false, false, true, false, true, Core::PropagationEvent::PROP_EVENT_FIXPOINT);  
  
  // [F, T] != [F, T] <-> [F, T] -> fixpoint
  xorTer.setDomains(false, true, false, true, false, true);
  xorTer.runPropagationAndCheck(false, true, false, true, false, true, Core::PropagationEvent::PROP_EVENT_FIXPOINT);
}//RunTerXor

OPTILAB_TEST(PropagatorBoolTerTest, GetFitnessBoolAndTer)
{
  using namespace Core;
  
  PropagatorBoolTerTest andTer(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_AND_TER);
  
  // T /\ T <-> T -> 0
  andTer.setDomains(true, true, true, true, true, true);
  EXPECT_EQ(Propagator::getMinFitness(), andTer.getFitness());
  
  // F /\ T <-> F -> 0
  andTer.setDomains(false, false, true, true, false, false);
  EXPECT_EQ(Propagator::getMinFitness(), andTer.getFitness());
  
  // T /\ F <-> F -> 0
  andTer.setDomains(true, true, false, false, false, false);
  EXPECT_EQ(Propagator::getMinFitness(), andTer.getFitness());
  
  // F /\ T <-> T -> 1
  andTer.setDomains(false, false, true, true, true, true);
  EXPECT_EQ(1, andTer.getFitness());
  
  // T /\ F <-> T -> 1
  andTer.setDomains(true, true, false, false, true, true);
  EXPECT_EQ(1, andTer.getFitness());
  
  // T /\ T <-> F -> 1
  andTer.setDomains(true, true, true, true, false, false);
  EXPECT_EQ(1, andTer.getFitness());
  
  // [F, T] /\ T <-> T -> -1
  andTer.setDomains(false, true, true, true, true, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), andTer.getFitness());
  
  // T /\ [F, T] <-> T -> -1
  andTer.setDomains(true, true, false, true, true, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), andTer.getFitness());
  
  // T /\ T <-> [F, T] -> -1
  andTer.setDomains(true, true, true, true, false, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), andTer.getFitness());
}//GetFitnessBoolAndTer

OPTILAB_TEST(PropagatorBoolTerTest, GetFitnessBoolLqReifTer)
{
  using namespace Core;
  
  PropagatorBoolTerTest lqReif(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_LQ_REIF);
  
  // T <= T <-> T -> 0
  lqReif.setDomains(true, true, true, true, true, true);
  EXPECT_EQ(Propagator::getMinFitness(), lqReif.getFitness());
  
  // T <= F <-> F -> 0
  lqReif.setDomains(true, true, false, false, false, false);
  EXPECT_EQ(Propagator::getMinFitness(), lqReif.getFitness());
  
  // F <= F <-> T -> 0
  lqReif.setDomains(false, false, false, false, true, true);
  EXPECT_EQ(Propagator::getMinFitness(), lqReif.getFitness());
  
  // F <= T <-> T -> 0
  lqReif.setDomains(false, false, true, true, true, true);
  EXPECT_EQ(Propagator::getMinFitness(), lqReif.getFitness());
  
  // F <= F <-> F -> 1
  lqReif.setDomains(false, false, false, false, false, false);
  EXPECT_EQ(1, lqReif.getFitness());
  
  // F <= T <-> F -> 1
  lqReif.setDomains(false, false, true, true, false, false);
  EXPECT_EQ(1, lqReif.getFitness());
  
  // T <= F <-> T -> 1
  lqReif.setDomains(true, true, false, false, true, true);
  EXPECT_EQ(1, lqReif.getFitness());
  
  // [F, T] <= F <-> T -> -1
  lqReif.setDomains(false, true, false, false, true, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), lqReif.getFitness());
  
  // F <= [F, T] <-> T -> -1
  lqReif.setDomains(false, false, false, true, true, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), lqReif.getFitness());
  
  // F <= T <-> [F, T] -> -1
  lqReif.setDomains(false, false, true, true, false, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), lqReif.getFitness());
}//GetFitnessBoolLqReifTer

OPTILAB_TEST(PropagatorBoolTerTest, GetFitnessBoolLtReifTer)
{
  using namespace Core;
  
  PropagatorBoolTerTest ltReif(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_LT_REIF);
  
  // T < T <-> T -> 1
  ltReif.setDomains(true, true, true, true, true, true);
  EXPECT_EQ(1, ltReif.getFitness());
  
  // T < F <-> F -> 0
  ltReif.setDomains(true, true, false, false, false, false);
  EXPECT_EQ(Propagator::getMinFitness(), ltReif.getFitness());
  
  // F < F <-> T -> 1
  ltReif.setDomains(false, false, false, false, true, true);
  EXPECT_EQ(1, ltReif.getFitness());
  
  // F < T <-> T -> 0
  ltReif.setDomains(false, false, true, true, true, true);
  EXPECT_EQ(Propagator::getMinFitness(), ltReif.getFitness());
  
  // F < F <-> F -> 1
  ltReif.setDomains(false, false, false, false, false, false);
  EXPECT_EQ(1, ltReif.getFitness());
  
  // F < T <-> F -> 1
  ltReif.setDomains(false, false, true, true, false, false);
  EXPECT_EQ(1, ltReif.getFitness());
  
  // T < F <-> T -> 1
  ltReif.setDomains(true, true, false, false, true, true);
  EXPECT_EQ(1, ltReif.getFitness());
  
  // [F, T] < F <-> T -> -1
  ltReif.setDomains(false, true, false, false, true, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), ltReif.getFitness());
  
  // F < [F, T] <-> T -> -1
  ltReif.setDomains(false, false, false, true, true, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), ltReif.getFitness());
  
  // F < T <-> [F, T] -> -1
  ltReif.setDomains(false, false, true, true, false, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), ltReif.getFitness());
}//GetFitnessBoolLtReifTer

OPTILAB_TEST(PropagatorBoolTerTest, GetFitnessBoolOrReifTer)
{
  using namespace Core;
  
  PropagatorBoolTerTest orTer(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_OR_TER);
  
  // T \/ T <-> T -> 0
  orTer.setDomains(true, true, true, true, true, true);
  EXPECT_EQ(Propagator::getMinFitness(), orTer.getFitness());
  
  // T \/ F <-> T -> 0
  orTer.setDomains(true, true, false, false, true, true);
  EXPECT_EQ(Propagator::getMinFitness(), orTer.getFitness());
  
  // F \/ T <-> T -> 0
  orTer.setDomains(false, false, true, true, true, true);
  EXPECT_EQ(Propagator::getMinFitness(), orTer.getFitness());
  
  // F \/ F <-> F -> 0
  orTer.setDomains(false, false, false, false, false, false);
  EXPECT_EQ(Propagator::getMinFitness(), orTer.getFitness());
  
  // F \/ F <-> T -> 1
  orTer.setDomains(false, false, false, false, true, true);
  EXPECT_EQ(1, orTer.getFitness());
  
  // T \/ T <-> F -> 1
  orTer.setDomains(true, true, true, true, false, false);
  EXPECT_EQ(1, orTer.getFitness());
  
  // T \/ F <-> F -> 1
  orTer.setDomains(true, true, false, false, false, false);
  EXPECT_EQ(1, orTer.getFitness());
  
  // F \/ T <-> F -> 1
  orTer.setDomains(false, false, true, true, false, false);
  EXPECT_EQ(1, orTer.getFitness());
  
  // [F, T] \/ T <-> T -> -1
  orTer.setDomains(false, true, true, true, true, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), orTer.getFitness());
  
  // T \/ [F, T] <-> T -> -1
  orTer.setDomains(true, true, false, true, true, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), orTer.getFitness());
  
  // T \/ T <-> [F, T] -> -1
  orTer.setDomains(true, true, true, true, false, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), orTer.getFitness());
}//GetFitnessBoolOrReifTer

OPTILAB_TEST(PropagatxorBoolTerTest, GetFitnessBoolXOrReifTer)
{
  using namespace Core;
  
  PropagatorBoolTerTest xorTer(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_XOR_TER);
  
  // T ^ T <-> T -> 1
  xorTer.setDomains(true, true, true, true, true, true);
  EXPECT_EQ(1, xorTer.getFitness());
  
  // F ^ F <-> T -> 1
  xorTer.setDomains(false, false, false, false, true, true);
  EXPECT_EQ(1, xorTer.getFitness());
  
  // F ^ T <-> F -> 1
  xorTer.setDomains(false, false, true, true, false, false);
  EXPECT_EQ(1, xorTer.getFitness());
  
  // T ^ F <-> F -> 1
  xorTer.setDomains(true, true, false, false, false, false);
  EXPECT_EQ(1, xorTer.getFitness());
  
  // T ^ T <-> F -> 0
  xorTer.setDomains(true, true, true, true, false, false);
  EXPECT_EQ(Propagator::getMinFitness(), xorTer.getFitness());
  
  // F ^ F <-> F -> 0
  xorTer.setDomains(false, false, false, false, false, false);
  EXPECT_EQ(Propagator::getMinFitness(), xorTer.getFitness());
  
  // F ^ T <-> T -> 0
  xorTer.setDomains(false, false, true, true, true, true);
  EXPECT_EQ(Propagator::getMinFitness(), xorTer.getFitness());
  
  // T ^ F <-> T -> 0
  xorTer.setDomains(true, true, false, false, true, true);
  EXPECT_EQ(Propagator::getMinFitness(), xorTer.getFitness());
  
  // [F, T] ^ F <-> T -> -1
  xorTer.setDomains(false, true, false, false, true, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), xorTer.getFitness());
  
  // F ^ [F, T] <-> T -> -1
  xorTer.setDomains(false, false, false, true, true, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), xorTer.getFitness());
  
  // T ^ F <-> [F, T] -> -1
  xorTer.setDomains(true, true, false, false, false, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), xorTer.getFitness());
}//GetFitnessBoolXOrReifTer
