
#include "utest_globals.hpp"

#include "DomainDefs.hpp"
#include "PropagatorNq.hpp"
#include "ConstraintTestInc.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"

#include <vector>
#include <memory>

namespace {
  class PropagatorNqTest : public ::testing::Test {
  public:
    void setDomains(INT_64 aD0LB, INT_64 aD0UB,
                    INT_64 aD1LB, INT_64 aD1UB)
    {
      pDom0 = getDomainInt(aD0LB, aD0UB);
      pDom1 = getDomainInt(aD1LB, aD1UB);
    }//setDomains
    
    void setDomains(std::vector<INT_64>&& aD0,
                    std::vector<INT_64>&& aD1)
    {
      pDom0 = getDomainInt(aD0);
      pDom1 = getDomainInt(aD1);
    }//setDomains
    
    void postPropagator(INT_64 aD0LB, INT_64 aD0UB,
                        INT_64 aD1LB, INT_64 aD1UB,
                        Core::PropagationEvent aEvent)
    {
      std::unique_ptr<Core::PropagatorNq> propagator(nullptr);
      propagator.reset(new Core::PropagatorNq(pDom0.get(), pDom1.get()));
      
      Core::PropagationEvent event = propagator->post();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        ASSERT_TRUE(pDom0->lowerBound()->isEqual(getElem(aD0LB).get()));
        ASSERT_TRUE(pDom0->upperBound()->isEqual(getElem(aD0UB).get()));
        
        ASSERT_TRUE(pDom1->lowerBound()->isEqual(getElem(aD1LB).get()));
        ASSERT_TRUE(pDom1->upperBound()->isEqual(getElem(aD1UB).get()));
      }
    }//postPropagator
    
    double getFitness(INT_64 aD0LB, INT_64 aD0UB, INT_64 aD1LB, INT_64 aD1UB)
    {
      std::unique_ptr<Core::PropagatorNq> propagator(nullptr);
      propagator.reset(new Core::PropagatorNq(pDom0.get(), pDom1.get()));
      return propagator->getFitness();
    }//getFitness
    
    void runPropagationAndCheck(INT_64 aD0LB, INT_64 aD0UB,
                                INT_64 aD1LB, INT_64 aD1UB,
                                Core::PropagationEvent aEvent)
    {
      // Perform propagation
      std::unique_ptr<Core::PropagatorNq> propagator(nullptr);
      propagator.reset(new Core::PropagatorNq(pDom0.get(), pDom1.get()));
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        ASSERT_TRUE(pDom0->lowerBound()->isEqual(getElem(aD0LB).get()));
        ASSERT_TRUE(pDom0->upperBound()->isEqual(getElem(aD0UB).get()));
      
        ASSERT_TRUE(pDom1->lowerBound()->isEqual(getElem(aD1LB).get()));
        ASSERT_TRUE(pDom1->upperBound()->isEqual(getElem(aD1UB).get()));
      }
    }//runPropagationAndCheck
    
    void runPropagationAndCheckDomains(std::vector<INT_64>&& aD0,
                                       std::vector<INT_64>&& aD1,
                                       Core::PropagationEvent aEvent)
    {
      // Perform propagation
      std::unique_ptr<Core::PropagatorNq> propagator(nullptr);
      propagator.reset(new Core::PropagatorNq(pDom0.get(), pDom1.get()));
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        for(auto& elem : aD0)
        {
          ASSERT_TRUE(pDom0->contains(getElem(elem).get()));
        }
        ASSERT_EQ(pDom0->getSize(), aD0.size());
        
        for(auto& elem : aD1)
        {
          ASSERT_TRUE(pDom1->contains(getElem(elem).get()));
        }
        ASSERT_EQ(pDom1->getSize(), aD1.size());
      }
    }//runPropagationAndCheckDomains
    
    virtual void SetUp()
    {
    }
    
    virtual void TearDown()
    {
    }
    
  protected:
    // Domain instances
    DomainIntPtr pDom0;
    DomainIntPtr pDom1;
    DomainIntPtr pDom2;
  };
}// end namespace


/*********************** Test ***********************/

OPTILAB_TEST_F(PropagatorNqTest, PropagateSingletonEdges)
{
  using namespace Core;
  
  // +INF != 2
  setDomains(Limits::int64Max(), Limits::int64Max(), +2, +2);
  runPropagationAndCheck(Limits::int64Max(), Limits::int64Max(), +2, +2, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -INF != 2
  setDomains(Limits::int64Min(), Limits::int64Min(), +2, +2);
  runPropagationAndCheck(Limits::int64Min(), Limits::int64Min(), +2, +2, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 2 != +INF
  setDomains(+2, +2, Limits::int64Max(), Limits::int64Max());
  runPropagationAndCheck(+2, +2, Limits::int64Max(), Limits::int64Max(), PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 2 != -INF
  setDomains(+2, +2, Limits::int64Min(), Limits::int64Min());
  runPropagationAndCheck(+2, +2, Limits::int64Min(), Limits::int64Min(), PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -INF != -INF
  setDomains(Limits::int64Min(), Limits::int64Min(), Limits::int64Min(), Limits::int64Min());
  runPropagationAndCheck(0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // -INF != +INF
  setDomains(Limits::int64Min(), Limits::int64Min(), Limits::int64Max(), Limits::int64Max());
  runPropagationAndCheck(Limits::int64Min(), Limits::int64Min(),
                         Limits::int64Max(), Limits::int64Max(), PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // +INF != +INF
  setDomains(Limits::int64Max(), Limits::int64Max(), Limits::int64Max(), Limits::int64Max());
  runPropagationAndCheck(0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
}//PropagateSingletonEdges

OPTILAB_TEST_F(PropagatorNqTest, PropagateSingleton1)
{
  using namespace Core;
  
  // 5 != 2
  setDomains(+5, +5, +2, +2);
  runPropagationAndCheck(+5, +5, +2, +2, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 5 != -2
  setDomains(+5, +5, -2, -2);
  runPropagationAndCheck(+5, +5, -2, -2, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -500 != -200
  setDomains(-500, -500, -200, -200);
  runPropagationAndCheck(-500, -500, -200, -200, PropagationEvent::PROP_EVENT_SUBSUMED);
}//PropagateSingleton1

OPTILAB_TEST_F(PropagatorNqTest, PropagateSingletonPost1)
{
  using namespace Core;
  
  // 5 != 2
  setDomains(+5, +5, +2, +2);
  postPropagator(+5, +5, +2, +2, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 5 != -2
  setDomains(+5, +5, -2, -2);
  postPropagator(+5, +5, -2, -2, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -500 != -200
  setDomains(-500, -500, -200, -200);
  postPropagator(-500, -500, -200, -200, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -5 != -5
  setDomains(-5, -5, -5, -5);
  postPropagator(0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
}//PropagateSingletonPost1

OPTILAB_TEST_F(PropagatorNqTest, PropagateSingleton2)
{
  using namespace Core;
  
  // 5 != [-10, 10]
  setDomains(+5, +5, -10, +10);
  runPropagationAndCheck(+5, +5, -10, +10, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 5 != [-10, 5] => D1 = [-10, 4]
  setDomains(+5, +5, -10, +5);
  runPropagationAndCheck(+5, +5, -10, +4, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -5 != [-5, +10] => D1 = [-4, +10]
  setDomains(-5, -5, -5, +10);
  runPropagationAndCheck(-5, -5, -4, +10, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [-10, 10] != 2
  setDomains(-10, +10, +2, +2);
  runPropagationAndCheck(-10, +10, +2, +2, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [-10, 2] != 2 => D0 = [-10, +1]
  setDomains(-10, +2, +2, +2);
  runPropagationAndCheck(-10, +1, +2, +2, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [-10, 10] != -10  => D0 = [-9, +1]
  setDomains(-10, +10, -10, -10);
  runPropagationAndCheck(-9, +10, -10, -10, PropagationEvent::PROP_EVENT_SUBSUMED);
}//PropagateSingleton2

OPTILAB_TEST_F(PropagatorNqTest, PropagateSingletonPost2)
{
  using namespace Core;
  
  // 5 != [-10, 10]
  setDomains(+5, +5, -10, +10);
  postPropagator(+5, +5, -10, +10, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // 5 != [-10, 5] => D1 = [-10, 4]
  setDomains(+5, +5, -10, +5);
  postPropagator(+5, +5, -10, +4, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // -5 != [-5, +10] => D1 = [-4, +10]
  setDomains(-5, -5, -5, +10);
  postPropagator(-5, -5, -4, +10, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // [-10, 10] != 2
  setDomains(-10, +10, +2, +2);
  postPropagator(-10, +10, +2, +2, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // [-10, 2] != 2 => D0 = [-10, +1]
  setDomains(-10, +2, +2, +2);
  postPropagator(-10, +1, +2, +2, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // [-10, 10] != -10  => D0 = [-9, +1]
  setDomains(-10, +10, -10, -10);
  postPropagator(-9, +10, -10, -10, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
}//PropagateSingletonPost2

OPTILAB_TEST_F(PropagatorNqTest, Propagate)
{
  using namespace Core;
  
  // [5, 10] != [2, 4]
  setDomains(+5, +10, +2, +4);
  runPropagationAndCheck(+5, +10, +2, +4, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-5, 10] != [-5, 50]
  setDomains(-5, +10, -5, +50);
  runPropagationAndCheck(-5, +10, -5, +50, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [3, 3] != [1, 4]
  setDomains({3}, {1,2,3,4});
  runPropagationAndCheckDomains({3}, {1,2,4}, PropagationEvent::PROP_EVENT_SUBSUMED);
}//Propagate

OPTILAB_TEST_F(PropagatorNqTest, getFitness)
{
  // Test fitness function
  using namespace Core;
  
  // Satisfied constraint
  setDomains(+5, +5, +2, +2);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness(+5, +5, +2, +2));
  
  // Unsatisfied constraint
  setDomains(+5, +5, +5, +5);
  EXPECT_EQ(1, getFitness(+5, +5, +5, +5));
  
  // Invalid fitness
  setDomains(+5, +6, +5, +6);
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness(+5, +6, +5, +6));
}//getFitness
