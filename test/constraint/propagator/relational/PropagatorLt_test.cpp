
#include "utest_globals.hpp"

#include "DomainDefs.hpp"
#include "PropagatorLt.hpp"
#include "ConstraintTestInc.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"

#include <vector>
#include <memory>

namespace {
  class PropagatorLtTest : public ::testing::Test {
  public:
    void setDomains(INT_64 aD0LB, INT_64 aD0UB,
                    INT_64 aD1LB, INT_64 aD1UB)
    {
      pDom0 = getDomainInt(aD0LB, aD0UB);
      pDom1 = getDomainInt(aD1LB, aD1UB);
    }//setDomains
    
    void setDomains(std::vector<INT_64>&& aD0,
                    std::vector<INT_64>&& aD1)
    {
      pDom0 = getDomainInt(aD0);
      pDom1 = getDomainInt(aD1);
    }//setDomains
    
    double getFitness(INT_64 aD0LB, INT_64 aD0UB, INT_64 aD1LB, INT_64 aD1UB)
    {
      std::unique_ptr<Core::PropagatorLt> propagator(nullptr);
      propagator.reset(new Core::PropagatorLt(pDom0.get(), pDom1.get()));
      return propagator->getFitness();
    }//getFitness
    
    
    void postPropagator(INT_64 aD0LB, INT_64 aD0UB,
                        INT_64 aD1LB, INT_64 aD1UB,
                        Core::PropagationEvent aEvent)
    {
      std::unique_ptr<Core::PropagatorLt> propagator(nullptr);
      propagator.reset(new Core::PropagatorLt(pDom0.get(), pDom1.get()));
      
      Core::PropagationEvent event = propagator->post();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        ASSERT_TRUE(pDom0->lowerBound()->isEqual(getElem(aD0LB).get()));
        ASSERT_TRUE(pDom0->upperBound()->isEqual(getElem(aD0UB).get()));
        
        ASSERT_TRUE(pDom1->lowerBound()->isEqual(getElem(aD1LB).get()));
        ASSERT_TRUE(pDom1->upperBound()->isEqual(getElem(aD1UB).get()));
      }
    }//postPropagator
    
    void runPropagationAndCheck(INT_64 aD0LB, INT_64 aD0UB,
                                INT_64 aD1LB, INT_64 aD1UB,
                                Core::PropagationEvent aEvent)
    {
      // Perform propagation
      std::unique_ptr<Core::PropagatorLt> propagator(nullptr);
      propagator.reset(new Core::PropagatorLt(pDom0.get(), pDom1.get()));
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        ASSERT_TRUE(pDom0->lowerBound()->isEqual(getElem(aD0LB).get()));
        ASSERT_TRUE(pDom0->upperBound()->isEqual(getElem(aD0UB).get()));
      
        ASSERT_TRUE(pDom1->lowerBound()->isEqual(getElem(aD1LB).get()));
        ASSERT_TRUE(pDom1->upperBound()->isEqual(getElem(aD1UB).get()));
      }
    }//runPropagationAndCheck
    
    void runPropagationAndCheckDomains(std::vector<INT_64>&& aD0,
                                       std::vector<INT_64>&& aD1,
                                       Core::PropagationEvent aEvent)
    {
      // Perform propagation
      std::unique_ptr<Core::PropagatorLt> propagator(nullptr);
      propagator.reset(new Core::PropagatorLt(pDom0.get(), pDom1.get()));
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        for(auto& elem : aD0)
        {
          ASSERT_TRUE(pDom0->contains(getElem(elem).get()));
        }
        ASSERT_EQ(pDom0->getSize(), aD0.size());
        
        for(auto& elem : aD1)
        {
          ASSERT_TRUE(pDom1->contains(getElem(elem).get()));
        }
        ASSERT_EQ(pDom1->getSize(), aD1.size());
      }
    }//runPropagationAndCheckDomains
    
    virtual void SetUp()
    {
    }
    
    virtual void TearDown()
    {
    }
    
  protected:
    // Domain instances
    DomainIntPtr pDom0;
    DomainIntPtr pDom1;
  };
}// end namespace


/*********************** Test ***********************/

OPTILAB_TEST_F(PropagatorLtTest, PropagateSingletonEdges)
{
  using namespace Core;
  
  // 2 < INF
  setDomains(+2, +2, Limits::int64Max(), Limits::int64Max());
  runPropagationAndCheck(+2, +2, Limits::int64Max(), Limits::int64Max(), PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -INF < 2
  setDomains(Limits::int64Min(), Limits::int64Min(), +2, +2);
  runPropagationAndCheck(Limits::int64Min(), Limits::int64Min(), +2, +2, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 2 < -INF => fail
  setDomains(+2, +2, Limits::int64Min(), Limits::int64Min());
  runPropagationAndCheck(+2, +2, Limits::int64Min(), Limits::int64Min(), PropagationEvent::PROP_EVENT_FAIL);
  
  // -INF < -INF => fail
  setDomains(Limits::int64Min(), Limits::int64Min(), Limits::int64Min(), Limits::int64Min());
  runPropagationAndCheck(+2, +2, Limits::int64Min(), Limits::int64Min(), PropagationEvent::PROP_EVENT_FAIL);
  
  // -INF < +INF
  setDomains(Limits::int64Min(), Limits::int64Min(), Limits::int64Max(), Limits::int64Max());
  runPropagationAndCheck(Limits::int64Min(), Limits::int64Min(),
                         Limits::int64Max(), Limits::int64Max(), PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // +INF < +INF => fail
  setDomains(Limits::int64Max(), Limits::int64Max(), Limits::int64Max(), Limits::int64Max());
  runPropagationAndCheck(+2, +2, Limits::int64Min(), Limits::int64Min(), PropagationEvent::PROP_EVENT_FAIL);
  
}//PropagateSingletonEdges

OPTILAB_TEST_F(PropagatorLtTest, PropagateSingleton1)
{
  using namespace Core;
  
  // 5 < 5 => fail
  setDomains(+5, +5, +5, +5);
  runPropagationAndCheck(+5, +5, +5, +5, PropagationEvent::PROP_EVENT_FAIL);
  
  // -2 <= 5
  setDomains(-2, -2, 5, 5);
  runPropagationAndCheck(-2, -2, 5, 5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -500 <= -200
  setDomains(-500, -500, -200, -200);
  runPropagationAndCheck(-500, -500, -200, -200, PropagationEvent::PROP_EVENT_SUBSUMED);
}//PropagateSingleton1

OPTILAB_TEST_F(PropagatorLtTest, PropagateSingletonPost1)
{
  using namespace Core;
  
  // 5 < 5 => fail
  setDomains(+5, +5, +5, +5);
  postPropagator(+5, +5, +5, +5, PropagationEvent::PROP_EVENT_FAIL);
  
  // -2 <= 5
  setDomains(-2, -2, 5, 5);
  postPropagator(-2, -2, 5, 5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -500 <= -200
  setDomains(-500, -500, -200, -200);
  postPropagator(-500, -500, -200, -200, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -500 <= -800
  setDomains(-500, -500, -800, -800);
  postPropagator(-500, -500, -800, -800, PropagationEvent::PROP_EVENT_FAIL);
}//PropagateSingletonPost1

OPTILAB_TEST_F(PropagatorLtTest, PropagateSingleton2)
{
  using namespace Core;
  
  // 5 < [-10, 10] => [6, 10]
  setDomains(+5, +5, -10, +10);
  runPropagationAndCheck(+5, +5, +6, +10, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 5 < [-10, 5] => fail
  setDomains(+5, +5, -10, +5);
  runPropagationAndCheck(+5, +5, +5, +5, PropagationEvent::PROP_EVENT_FAIL);
  
  // [-10, 10] < 2 => [-10, +1]
  setDomains(-10, +10, +2, +2);
  runPropagationAndCheck(-10, +1, +2, +2, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [2, 3] < 2 => D0 = fail
  setDomains(+2, +3, +2, +2);
  runPropagationAndCheck(+2, +2, +2, +2, PropagationEvent::PROP_EVENT_FAIL);
}//PropagateSingleton2

OPTILAB_TEST_F(PropagatorLtTest, PropagateSingletonPost2)
{
  using namespace Core;
  
  // 5 < [-10, 10] => [6, 10]
  setDomains(+5, +5, -10, +10);
  postPropagator(+5, +5, +6, +10, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // 5 < [-10, 5] => fail
  setDomains(+5, +5, -10, +5);
  postPropagator(+5, +5, +5, +5, PropagationEvent::PROP_EVENT_FAIL);
  
  // [-10, 10] < 2 => [-10, +1]
  setDomains(-10, +10, +2, +2);
  postPropagator(-10, +1, +2, +2, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // [2, 3] < 2 => D0 = fail
  setDomains(+2, +3, +2, +2);
  runPropagationAndCheck(+2, +2, +2, +2, PropagationEvent::PROP_EVENT_FAIL);
}//PropagateSingletonPost2

OPTILAB_TEST_F(PropagatorLtTest, Propagate)
{
  using namespace Core;
  
  // [5, 10] < [2, 5] => fail
  setDomains(+5, +10, +2, +5);
  runPropagationAndCheck(+5, +10, +2, +5, PropagationEvent::PROP_EVENT_FAIL);
  
  // [5, 10] < [-5, 50] => [6, 50]
  setDomains(+5, +10, -5, +50);
  runPropagationAndCheck(+5, +10, 6, 50, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [3, 3] < [1, 4] => [4, 4]
  setDomains({3}, {1,2,3,4});
  runPropagationAndCheckDomains({3}, {4}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [-5, 10] < [2, 8] => D0 = [-5, 7]
  setDomains(-5, +10, 2, 8);
  runPropagationAndCheck(-5, +7, 2, 8, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-5, 10] < [-10, 12] => D1 = [-4, 12]
  setDomains(-5, +10, -10, 12);
  runPropagationAndCheck(-5, 10, -4, 12, PropagationEvent::PROP_EVENT_FIXPOINT);
}//Propagate

OPTILAB_TEST_F(PropagatorLtTest, getFitness)
{
  // Test fitness function
  using namespace Core;
  
  // Unsatisfied constraint, should return a fitness of (5 - 2 + 1) = 4
  setDomains(+5, +5, +2, +2);
  EXPECT_EQ(4, getFitness(+5, +5, +2, +2));
  
  // Satisfied constraint
  setDomains(+2, +2, +5, +5);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness(+2, +2, +5, +5));
  
  // Invalid fitness
  setDomains(+5, +6, +5, +6);
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness(+5, +6, +5, +6));
}//getFitness
