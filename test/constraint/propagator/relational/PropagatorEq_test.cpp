
#include "utest_globals.hpp"

#include "DomainDefs.hpp"
#include "PropagatorEq.hpp"
#include "ConstraintTestInc.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"

#include <vector>
#include <memory>

namespace {
  class PropagatorEqTest : public ::testing::Test {
  public:
    PropagatorEqTest()
    {
    }
    
    ~PropagatorEqTest()
    {
    }
    
    void setDomains(INT_64 aD0LB, INT_64 aD0UB,
                    INT_64 aD1LB, INT_64 aD1UB)
    {
      pDom0 = getDomainInt(aD0LB, aD0UB);
      pDom1 = getDomainInt(aD1LB, aD1UB);
    }//setDomains
    
    void setDomains(std::vector<INT_64>&& aD0,
                    std::vector<INT_64>&& aD1)
    {
      pDom0 = getDomainInt(aD0);
      pDom1 = getDomainInt(aD1);
    }//setDomains
    
    void postPropagator(INT_64 aD0LB, INT_64 aD0UB,
                        INT_64 aD1LB, INT_64 aD1UB,
                        Core::PropagationEvent aEvent)
    {
      std::unique_ptr<Core::PropagatorEqBound> propagator(nullptr);
      propagator.reset(new Core::PropagatorEqBound(pDom0.get(), pDom1.get()));
      
      Core::PropagationEvent event = propagator->post();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        ASSERT_TRUE(pDom0->lowerBound()->isEqual(getElem(aD0LB).get()));
        ASSERT_TRUE(pDom0->upperBound()->isEqual(getElem(aD0UB).get()));
        
        ASSERT_TRUE(pDom1->lowerBound()->isEqual(getElem(aD1LB).get()));
        ASSERT_TRUE(pDom1->upperBound()->isEqual(getElem(aD1UB).get()));
      }
    }//postPropagator
    
    double getFitness(INT_64 aD0LB, INT_64 aD0UB, INT_64 aD1LB, INT_64 aD1UB)
    {
      std::unique_ptr<Core::PropagatorEqBound> propagator(nullptr);
      propagator.reset(new Core::PropagatorEqBound(pDom0.get(), pDom1.get()));
      return propagator->getFitness();
    }//getFitness
    
    void runPropagationAndCheck(INT_64 aD0LB, INT_64 aD0UB,
                                INT_64 aD1LB, INT_64 aD1UB,
                                Core::PropagationEvent aEvent,
                                bool aUseBound=USE_BOUND_PROP)
    {
      // Perform propagation
      std::unique_ptr<Core::PropagatorEq> propagator(nullptr);
      if(aUseBound == USE_BOUND_PROP)
      {
        propagator.reset(new Core::PropagatorEqBound(pDom0.get(), pDom1.get()));
      }
      else
      {
        propagator.reset(new Core::PropagatorEqDomain(pDom0.get(), pDom1.get()));
      }
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        ASSERT_TRUE(pDom0->lowerBound()->isEqual(getElem(aD0LB).get()));
        ASSERT_TRUE(pDom0->upperBound()->isEqual(getElem(aD0UB).get()));
      
        ASSERT_TRUE(pDom1->lowerBound()->isEqual(getElem(aD1LB).get()));
        ASSERT_TRUE(pDom1->upperBound()->isEqual(getElem(aD1UB).get()));
      }
    }//runPropagationAndCheck
    
    void runPropagationAndCheckDomains(std::vector<INT_64>&& aD0,
                                       std::vector<INT_64>&& aD1,
                                       Core::PropagationEvent aEvent,
                                       bool aUseBound=USE_DOMAIN_PROP)
    {
      // Perform propagation
      std::unique_ptr<Core::PropagatorEq> propagator(nullptr);
      if(aUseBound == USE_BOUND_PROP)
      {
        propagator.reset(new Core::PropagatorEqBound(pDom0.get(), pDom1.get()));
      }
      else
      {
        propagator.reset(new Core::PropagatorEqDomain(pDom0.get(), pDom1.get()));
      }
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        for(auto& elem : aD0)
        {
          ASSERT_TRUE(pDom0->contains(getElem(elem).get()));
        }
        ASSERT_EQ(pDom0->getSize(), aD0.size());
        
        for(auto& elem : aD1)
        {
          ASSERT_TRUE(pDom1->contains(getElem(elem).get()));
        }
        ASSERT_EQ(pDom1->getSize(), aD1.size());
      }
    }//runPropagationAndCheckDomains
    
  protected:
    // Domain instances
    DomainIntPtr pDom0;
    DomainIntPtr pDom1;
    DomainIntPtr pDom2;
  };
}// end namespace


/*********************** Test ***********************/

OPTILAB_TEST_F(PropagatorEqTest, PropagateSingletonEdges)
{
  using namespace Core;
  
  // +INF == 2
  setDomains(Limits::int64Max(), Limits::int64Max(), +2, +2);
  runPropagationAndCheck(0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // -INF == 2
  setDomains(Limits::int64Min(), Limits::int64Min(), +2, +2);
  runPropagationAndCheck(0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // 2 == +INF
  setDomains(+2, +2, Limits::int64Max(), Limits::int64Max());
  runPropagationAndCheck(0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // 2 == -INF
  setDomains(+2, +2, Limits::int64Min(), Limits::int64Min());
  runPropagationAndCheck(0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // -INF == -INF
  setDomains(Limits::int64Min(), Limits::int64Min(), Limits::int64Min(), Limits::int64Min());
  runPropagationAndCheck(Limits::int64Min(), Limits::int64Min(),
                         Limits::int64Min(), Limits::int64Min(), PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -INF == +INF
  setDomains(Limits::int64Min(), Limits::int64Min(), Limits::int64Max(), Limits::int64Max());
  runPropagationAndCheck(0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // +INF == +INF
  setDomains(Limits::int64Max(), Limits::int64Max(), Limits::int64Max(), Limits::int64Max());
  runPropagationAndCheck(Limits::int64Max(), Limits::int64Max(),
                         Limits::int64Max(), Limits::int64Max(), PropagationEvent::PROP_EVENT_SUBSUMED);
}//PropagateSingletonEdges

OPTILAB_TEST_F(PropagatorEqTest, PropagateSingleton1)
{
  using namespace Core;
  
  // 5 == 2
  setDomains(+5, +5, +2, +2);
  runPropagationAndCheck(0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // 5 == 5
  setDomains(+5, +5, +5, +5);
  runPropagationAndCheck(+5, +5, +5, +5, PropagationEvent::PROP_EVENT_SUBSUMED);
}//PropagateSingleton1

OPTILAB_TEST_F(PropagatorEqTest, PropagateSingletonPost1)
{
  using namespace Core;
  
  // 5 == 2
  setDomains(+5, +5, +2, +2);
  postPropagator(+5, +5, +2, +2, PropagationEvent::PROP_EVENT_FAIL);
  
  // 5 == 5
  setDomains(+5, +5, +5, +5);
  postPropagator(+5, +5, +5, +5, PropagationEvent::PROP_EVENT_SUBSUMED);
}//PropagateSingletonPost1

OPTILAB_TEST_F(PropagatorEqTest, PropagateSingleton2)
{
  using namespace Core;
  
  // 5 == [-10, 10] => D1 = [5, 5]
  setDomains(+5, +5, -10, +10);
  runPropagationAndCheck(+5, +5, +5, +5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 5 == [-10, 4] => D1 = []
  setDomains(+5, +5, -10, +4);
  runPropagationAndCheck(0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // [-10, 10] == 2 => D0 = [2, 2]
  setDomains(-10, +10, +2, +2);
  runPropagationAndCheck(+2, +2, +2, +2, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [-10, 2] == 3 => D0 = []
  setDomains(-10, +2, +3, +3);
  runPropagationAndCheck(0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // [3, 3] == [1, 4] => D1 = [3, 3]
  setDomains({3}, {1,2,3,4});
  runPropagationAndCheckDomains({3}, {3}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [3, 3] == [1, 4] - [3] => D1 = []
  setDomains({3}, {1,2,4});
  runPropagationAndCheckDomains({0}, {0}, PropagationEvent::PROP_EVENT_FAIL);
}//PropagateSingleton2

OPTILAB_TEST_F(PropagatorEqTest, PropagatePost2)
{
  using namespace Core;
  
  // 5 == [-10, 10] => D1 = [5, 5]
  setDomains(+5, +5, -10, +10);
  postPropagator(+5, +5, +5, +5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 5 == [-10, 4] => D1 = []
  setDomains(+5, +5, -10, +4);
  postPropagator(0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // [-10, 10] == 2 => D0 = [2, 2]
  setDomains(-10, +10, +2, +2);
  postPropagator(+2, +2, +2, +2, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [-10, 2] == 3 => D0 = []
  setDomains(-10, +2, +3, +3);
  postPropagator(0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
}//PropagatePost2

OPTILAB_TEST_F(PropagatorEqTest, Propagate)
{
  using namespace Core;
  
  // [-11, +11] == [-10, 10] => D0 = [-10, 10]
  setDomains(-11, +11, -10, +10);
  runPropagationAndCheck(-10, +10, -10, +10, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-10, +10] == [-11, +11] => D1 = [-10, 10]
  setDomains(-10, +10, -11, +11);
  runPropagationAndCheck(-10, +10, -10, +10, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-10, +10] == [-11, +9] => D0 = [-10, 9] /\ D1 = [-10, 9]
  setDomains(-10, +10, -11, +9);
  runPropagationAndCheck(-10, +9, -10, +9, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-INF, 3] == [1, +INF] => D0 = [1, 3] /\ D1 = [1, 3]
  setDomains(Limits::int64Min(), +3, +1, Limits::int64Max());
  runPropagationAndCheck(+1, +3, +1, +3, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-10, +10] == [+11, +12] => D0 = []
  setDomains(-10, +10, +11, +12);
  postPropagator(0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // [-3, +3] == [-4, +4] => intersection
  setDomains({-3, -1, 0, 2, 3}, {-4, -1, 1, 3, 4});
  runPropagationAndCheckDomains({-1, 3}, {-1, 3}, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-3, +3] == [-4, +4] => intersection
  setDomains({-3, -1, 0, 2, 3}, {-4, -2, 1, 4});
  runPropagationAndCheckDomains({0}, {0}, PropagationEvent::PROP_EVENT_FAIL);
}//Propagate

OPTILAB_TEST_F(PropagatorEqTest, Post)
{
  using namespace Core;
  
  // [-11, +11] == [-10, 10] => D0 = [-10, 10]
  setDomains(-11, +11, -10, +10);
  postPropagator(-10, +10, -10, +10, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // [-10, +10] == [-11, +11] => D1 = [-10, 10]
  setDomains(-10, +10, -11, +11);
  postPropagator(-10, +10, -10, +10, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // [-10, +10] == [-11, +9] => D0 = [-10, 9] /\ D1 = [-10, 9]
  setDomains(-10, +10, -11, +9);
  postPropagator(-10, +9, -10, +9, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // [-INF, 3] == [1, +INF] => D0 = [1, 3] /\ D1 = [1, 3]
  setDomains(Limits::int64Min(), +3, +1, Limits::int64Max());
  postPropagator(+1, +3, +1, +3, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // [-10, +10] == [+11, +12] => D0 = []
  setDomains(-10, +10, +11, +12);
  postPropagator(0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
}//Post

OPTILAB_TEST_F(PropagatorEqTest, getFitness)
{
  // Test fitness function
  using namespace Core;
  
  // Unsatisfied constraint (should return |5 - 2| = 3)
  setDomains(+5, +5, +2, +2);
  EXPECT_EQ(3, getFitness(+5, +5, +2, +2));

  // Unsatisfied constraint (should return |2 - 5| = 3)
  setDomains(+2, +2, +5, +5);
  EXPECT_EQ(3, getFitness(+2, +2, +5, +5));
  
  // Satisfied constraint
  setDomains(+5, +5, +5, +5);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness(+5, +5, +5, +5));
  
  // Invalid fitness
  setDomains(+5, +6, +5, +6);
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness(+5, +6, +5, +6));
}//getFitness
