
#include "utest_globals.hpp"

#include "PropagatorTest.hpp"

/*********************** Test ***********************/

OPTILAB_TEST(PropagatorBoolBinTest, PostBinAnd)
{
  using namespace Core;
  
  PropagatorBoolBinTest andBin(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_AND_BIN);
  
  // F /\ T -> fail
  andBin.setDomains(false, false, true, true);
  andBin.postPropagator(false, false, true, true, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T /\ F -> fail
  andBin.setDomains(true, true, false, false);
  andBin.postPropagator(true, true, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F /\ F -> fail
  andBin.setDomains(false, false, false, false);
  andBin.postPropagator(false, false, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T /\ T -> subsumed
  andBin.setDomains(true, true, true, true);
  andBin.postPropagator(true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F /\ [F, T] -> fail
  andBin.setDomains(false, false, false, true);
  andBin.postPropagator(false, false, false, true, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T /\ [F, T] -> T, subsumed
  andBin.setDomains(true, true, false, true);
  andBin.postPropagator(true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] /\ F -> fail
  andBin.setDomains(false, true, false, false);
  andBin.postPropagator(false, true, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // [F, T] /\ T -> T, subsumed
  andBin.setDomains(false, true, true, true);
  andBin.postPropagator(true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] /\ [F, T] -> subsumed
  andBin.setDomains(false, true, false, true);
  andBin.postPropagator(true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
}//PostBinAnd

OPTILAB_TEST(PropagatorBoolBinTest, RunBinAnd)
{
  using namespace Core;
  
  PropagatorBoolBinTest andBin(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_AND_BIN);
  
  // F /\ T -> fail
  andBin.setDomains(false, false, true, true);
  andBin.runPropagationAndCheck(false, false, true, true, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T /\ F -> fail
  andBin.setDomains(true, true, false, false);
  andBin.runPropagationAndCheck(true, true, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F /\ F -> fail
  andBin.setDomains(false, false, false, false);
  andBin.runPropagationAndCheck(false, false, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T /\ T -> subsumed
  andBin.setDomains(true, true, true, true);
  andBin.runPropagationAndCheck(true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F /\ [F, T] -> fail
  andBin.setDomains(false, false, false, true);
  andBin.runPropagationAndCheck(false, false, false, true, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T /\ [F, T] -> T, subsumed
  andBin.setDomains(true, true, false, true);
  andBin.runPropagationAndCheck(true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] /\ F -> fail
  andBin.setDomains(false, true, false, false);
  andBin.runPropagationAndCheck(false, true, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // [F, T] /\ T -> T, subsumed
  andBin.setDomains(false, true, true, true);
  andBin.runPropagationAndCheck(true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] /\ [F, T] -> subsumed
  andBin.setDomains(false, true, false, true);
  andBin.runPropagationAndCheck(true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
}//RunBinAnd

OPTILAB_TEST(PropagatorBoolBinTest, PostBinEq)
{
  using namespace Core;
  
  PropagatorBoolBinTest eqBin(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_EQ);
  
  // F == T -> fail
  eqBin.setDomains(false, false, true, true);
  eqBin.postPropagator(false, false, true, true, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T == F -> fail
  eqBin.setDomains(true, true, false, false);
  eqBin.postPropagator(true, true, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F == F -> subsumed
  eqBin.setDomains(false, false, false, false);
  eqBin.postPropagator(false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T == T -> subsumed
  eqBin.setDomains(true, true, true, true);
  eqBin.postPropagator(true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F == [F, T] -> F, subsumed
  eqBin.setDomains(false, false, false, true);
  eqBin.postPropagator(false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T == [F, T] -> T, subsumed
  eqBin.setDomains(true, true, false, true);
  eqBin.postPropagator(true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] == F -> F, subsumed
  eqBin.setDomains(false, true, false, false);
  eqBin.postPropagator(false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] == T -> T, subsumed
  eqBin.setDomains(false, true, true, true);
  eqBin.postPropagator(true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] == [F, T] -> unspec
  eqBin.setDomains(false, true, false, true);
  eqBin.postPropagator(false, true, false, true, Core::PropagationEvent::PROP_EVENT_RUN_UNSPEC);
}//PostBinEq

OPTILAB_TEST(PropagatorBoolBinTest, RunBinEq)
{
  using namespace Core;
  
  PropagatorBoolBinTest eqBin(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_EQ);
  
  // F == T -> fail
  eqBin.setDomains(false, false, true, true);
  eqBin.runPropagationAndCheck(false, false, true, true, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T == F -> fail
  eqBin.setDomains(true, true, false, false);
  eqBin.runPropagationAndCheck(true, true, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F == F -> subsumed
  eqBin.setDomains(false, false, false, false);
  eqBin.runPropagationAndCheck(false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T == T -> subsumed
  eqBin.setDomains(true, true, true, true);
  eqBin.runPropagationAndCheck(true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F == [F, T] -> F, subsumed
  eqBin.setDomains(false, false, false, true);
  eqBin.runPropagationAndCheck(false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T == [F, T] -> T, subsumed
  eqBin.setDomains(true, true, false, true);
  eqBin.runPropagationAndCheck(true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] == F -> F, subsumed
  eqBin.setDomains(false, true, false, false);
  eqBin.runPropagationAndCheck(false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] == T -> T, subsumed
  eqBin.setDomains(false, true, true, true);
  eqBin.runPropagationAndCheck(true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] == [F, T] -> unspec
  eqBin.setDomains(false, true, false, true);
  eqBin.runPropagationAndCheck(false, true, false, true, Core::PropagationEvent::PROP_EVENT_FIXPOINT);
}//RunBinEq

OPTILAB_TEST(PropagatorBoolBinTest, PostBinLq)
{
  using namespace Core;
  
  PropagatorBoolBinTest lqBin(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_LQ);
  
  // F <= T -> subsumed
  lqBin.setDomains(false, false, true, true);
  lqBin.postPropagator(false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);

  // T <= F -> fail
  lqBin.setDomains(true, true, false, false);
  lqBin.postPropagator(true, true, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F <= F -> subsumed
  lqBin.setDomains(false, false, false, false);
  lqBin.postPropagator(false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T <= T -> subsumed
  lqBin.setDomains(true, true, true, true);
  lqBin.postPropagator(true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F <= [F, T] -> always satisfied, subsumed
  lqBin.setDomains(false, false, false, true);
  lqBin.postPropagator(false, false, false, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T <= [F, T] -> T, subsumed
  lqBin.setDomains(true, true, false, true);
  lqBin.postPropagator(true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] <= F -> F, subsumed
  lqBin.setDomains(false, true, false, false);
  lqBin.postPropagator(false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] <= T -> always satisfied
  lqBin.setDomains(false, true, true, true);
  lqBin.postPropagator(false, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] <= [F, T] -> unspec
  lqBin.setDomains(false, true, false, true);
  lqBin.postPropagator(false, true, false, true, Core::PropagationEvent::PROP_EVENT_RUN_UNSPEC);
}//PostBinLq

OPTILAB_TEST(PropagatorBoolBinTest, RunBinLq)
{
  using namespace Core;
  
  PropagatorBoolBinTest lqBin(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_LQ);
  
  // F <= T -> subsumed
  lqBin.setDomains(false, false, true, true);
  lqBin.runPropagationAndCheck(false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T <= F -> fail
  lqBin.setDomains(true, true, false, false);
  lqBin.runPropagationAndCheck(true, true, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F <= F -> subsumed
  lqBin.setDomains(false, false, false, false);
  lqBin.runPropagationAndCheck(false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T <= T -> subsumed
  lqBin.setDomains(true, true, true, true);
  lqBin.runPropagationAndCheck(true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F <= [F, T] -> always satisfied, subsumed
  lqBin.setDomains(false, false, false, true);
  lqBin.runPropagationAndCheck(false, false, false, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T <= [F, T] -> T, subsumed
  lqBin.setDomains(true, true, false, true);
  lqBin.runPropagationAndCheck(true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] <= F -> F, subsumed
  lqBin.setDomains(false, true, false, false);
  lqBin.runPropagationAndCheck(false, false, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] <= T -> always satisfied
  lqBin.setDomains(false, true, true, true);
  lqBin.runPropagationAndCheck(false, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] <= [F, T] -> unspec
  lqBin.setDomains(false, true, false, true);
  lqBin.runPropagationAndCheck(false, true, false, true, Core::PropagationEvent::PROP_EVENT_FIXPOINT);
}//RunBinLq

OPTILAB_TEST(PropagatorBoolBinTest, PostBinLt)
{
  using namespace Core;
  
  PropagatorBoolBinTest ltBin(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_LT);
  
  // F < T -> subsumed
  ltBin.setDomains(false, false, true, true);
  ltBin.postPropagator(false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);

  // T < F -> fail
  ltBin.setDomains(true, true, false, false);
  ltBin.postPropagator(true, true, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F < F -> fail
  ltBin.setDomains(false, false, false, false);
  ltBin.postPropagator(false, false, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T < T -> fail
  ltBin.setDomains(true, true, true, true);
  ltBin.postPropagator(true, true, true, true, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F < [F, T] -> T, subsumed
  ltBin.setDomains(false, false, false, true);
  ltBin.postPropagator(false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] < F -> fail
  ltBin.setDomains(false, true, false, false);
  ltBin.postPropagator(false, true, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // [F, T] < [F, T] -> subsumed
  ltBin.setDomains(false, true, false, true);
  ltBin.postPropagator(false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
}//PostBinLt

OPTILAB_TEST(PropagatorBoolBinTest, RunBinLt)
{
  using namespace Core;
  
  PropagatorBoolBinTest ltBin(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_LT);
  
  // F < T -> subsumed
  ltBin.setDomains(false, false, true, true);
  ltBin.runPropagationAndCheck(false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T < F -> fail
  ltBin.setDomains(true, true, false, false);
  ltBin.runPropagationAndCheck(true, true, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F < F -> fail
  ltBin.setDomains(false, false, false, false);
  ltBin.runPropagationAndCheck(false, false, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T < T -> fail
  ltBin.setDomains(true, true, true, true);
  ltBin.runPropagationAndCheck(true, true, true, true, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F < [F, T] -> T, subsumed
  ltBin.setDomains(false, false, false, true);
  ltBin.runPropagationAndCheck(false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] < T -> F, subsumed
  ltBin.setDomains(false, true, true, true);
  ltBin.runPropagationAndCheck(false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] < F -> fail
  ltBin.setDomains(false, true, false, false);
  ltBin.runPropagationAndCheck(false, true, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // [F, T] < [F, T] -> subsumed
  ltBin.setDomains(false, true, false, true);
  ltBin.runPropagationAndCheck(false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
}//RunBinLt

OPTILAB_TEST(PropagatorBoolBinTest, PostBinNot)
{
  using namespace Core;
  
  PropagatorBoolBinTest notBin(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_NOT);
  
  // F != T -> subsumed
  notBin.setDomains(false, false, true, true);
  notBin.postPropagator(false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T != F -> subsumed
  notBin.setDomains(true, true, false, false);
  notBin.postPropagator(true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F != F -> fail
  notBin.setDomains(false, false, false, false);
  notBin.postPropagator(false, false, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T != T -> fail
  notBin.setDomains(true, true, true, true);
  notBin.postPropagator(true, true, true, true, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F != [F, T] -> T, subsumed
  notBin.setDomains(false, false, false, true);
  notBin.postPropagator(false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T != [F, T] -> F, subsumed
  notBin.setDomains(true, true, false, true);
  notBin.postPropagator(true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] != F -> T, subsumed
  notBin.setDomains(false, true, false, false);
  notBin.postPropagator(true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] != T -> F, subsumed
  notBin.setDomains(false, true, true, true);
  notBin.postPropagator(false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] != [F, T] -> unspec
  notBin.setDomains(false, true, false, true);
  notBin.postPropagator(false, true, false, true, Core::PropagationEvent::PROP_EVENT_RUN_UNSPEC);
}//PostBinNot

OPTILAB_TEST(PropagatorBoolBinTest, RunBinNot)
{
  using namespace Core;
  
  PropagatorBoolBinTest notBin(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_NOT);
  
  // F != T -> subsumed
  notBin.setDomains(false, false, true, true);
  notBin.runPropagationAndCheck(false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T != F -> subsumed
  notBin.setDomains(true, true, false, false);
  notBin.runPropagationAndCheck(true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F != F -> fail
  notBin.setDomains(false, false, false, false);
  notBin.runPropagationAndCheck(false, false, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T != T -> fail
  notBin.setDomains(true, true, true, true);
  notBin.runPropagationAndCheck(true, true, true, true, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F != [F, T] -> T, subsumed
  notBin.setDomains(false, false, false, true);
  notBin.runPropagationAndCheck(false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T != [F, T] -> F, subsumed
  notBin.setDomains(true, true, false, true);
  notBin.runPropagationAndCheck(true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] != F -> T, subsumed
  notBin.setDomains(false, true, false, false);
  notBin.runPropagationAndCheck(true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] != T -> F, subsumed
  notBin.setDomains(false, true, true, true);
  notBin.runPropagationAndCheck(false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] != [F, T] -> fixpoint
  notBin.setDomains(false, true, false, true);
  notBin.runPropagationAndCheck(false, true, false, true, Core::PropagationEvent::PROP_EVENT_FIXPOINT);
}//RunBinNot

OPTILAB_TEST(PropagatorBoolBinTest, PostBinOr)
{
  using namespace Core;
  
  PropagatorBoolBinTest orBin(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_OR_BIN);
  
  // F \/ T -> subsumed
  orBin.setDomains(false, false, true, true);
  orBin.postPropagator(false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T \/ F -> subsumed
  orBin.setDomains(true, true, false, false);
  orBin.postPropagator(true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F \/ F -> fail
  orBin.setDomains(false, false, false, false);
  orBin.postPropagator(false, false, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T \/ T -> subsumed
  orBin.setDomains(true, true, true, true);
  orBin.postPropagator(true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F \/ [F, T] -> T, subsumed
  orBin.setDomains(false, false, false, true);
  orBin.postPropagator(false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T \/ [F, T] -> subsumed
  orBin.setDomains(true, true, false, true);
  orBin.postPropagator(true, true, false, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] \/ F -> T, subsumed
  orBin.setDomains(false, true, false, false);
  orBin.postPropagator(true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] \/ T -> subsumed
  orBin.setDomains(false, true, true, true);
  orBin.postPropagator(false, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] \/ [F, T] -> unspec
  orBin.setDomains(false, true, false, true);
  orBin.postPropagator(false, true, false, true, Core::PropagationEvent::PROP_EVENT_RUN_UNSPEC);
}//PostBinOr

OPTILAB_TEST(PropagatorBoolBinTest, RunBinOr)
{
  using namespace Core;
  
  PropagatorBoolBinTest orBin(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_OR_BIN);
  
  // F \/ T -> subsumed
  orBin.setDomains(false, false, true, true);
  orBin.runPropagationAndCheck(false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T \/ F -> subsumed
  orBin.setDomains(true, true, false, false);
  orBin.runPropagationAndCheck(true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F \/ F -> fail
  orBin.setDomains(false, false, false, false);
  orBin.runPropagationAndCheck(false, false, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T \/ T -> subsumed
  orBin.setDomains(true, true, true, true);
  orBin.runPropagationAndCheck(true, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F \/ [F, T] -> T, subsumed
  orBin.setDomains(false, false, false, true);
  orBin.runPropagationAndCheck(false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T \/ [F, T] -> subsumed
  orBin.setDomains(true, true, false, true);
  orBin.runPropagationAndCheck(true, true, false, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] \/ F -> T, subsumed
  orBin.setDomains(false, true, false, false);
  orBin.runPropagationAndCheck(true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] \/ T -> subsumed
  orBin.setDomains(false, true, true, true);
  orBin.runPropagationAndCheck(false, true, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] \/ [F, T] -> fixpoint
  orBin.setDomains(false, true, false, true);
  orBin.runPropagationAndCheck(false, true, false, true, Core::PropagationEvent::PROP_EVENT_FIXPOINT);
}//RunBinOr

OPTILAB_TEST(PropagatorBoolBinTest, PostBinXor)
{
  using namespace Core;
  
  PropagatorBoolBinTest xorBin(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_XOR_BIN);
  
  // F != T -> subsumed
  xorBin.setDomains(false, false, true, true);
  xorBin.postPropagator(false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T != F -> subsumed
  xorBin.setDomains(true, true, false, false);
  xorBin.postPropagator(true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F != F -> fail
  xorBin.setDomains(false, false, false, false);
  xorBin.postPropagator(false, false, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T != T -> fail
  xorBin.setDomains(true, true, true, true);
  xorBin.postPropagator(true, true, true, true, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F != [F, T] -> T, subsumed
  xorBin.setDomains(false, false, false, true);
  xorBin.postPropagator(false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T != [F, T] -> F, subsumed
  xorBin.setDomains(true, true, false, true);
  xorBin.postPropagator(true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] != F -> T, subsumed
  xorBin.setDomains(false, true, false, false);
  xorBin.postPropagator(true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] != T -> F, subsumed
  xorBin.setDomains(false, true, true, true);
  xorBin.postPropagator(false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] != [F, T] -> unspec
  xorBin.setDomains(false, true, false, true);
  xorBin.postPropagator(false, true, false, true, Core::PropagationEvent::PROP_EVENT_RUN_UNSPEC);
}//PostBinXor

OPTILAB_TEST(PropagatorBoolBinTest, RunBinXor)
{
  using namespace Core;
  
  PropagatorBoolBinTest xorBin(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_XOR_BIN);
  
  // F != T -> subsumed
  xorBin.setDomains(false, false, true, true);
  xorBin.runPropagationAndCheck(false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T != F -> subsumed
  xorBin.setDomains(true, true, false, false);
  xorBin.runPropagationAndCheck(true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // F != F -> fail
  xorBin.setDomains(false, false, false, false);
  xorBin.runPropagationAndCheck(false, false, false, false, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // T != T -> fail
  xorBin.setDomains(true, true, true, true);
  xorBin.runPropagationAndCheck(true, true, true, true, Core::PropagationEvent::PROP_EVENT_FAIL);
  
  // F != [F, T] -> T, subsumed
  xorBin.setDomains(false, false, false, true);
  xorBin.runPropagationAndCheck(false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // T != [F, T] -> F, subsumed
  xorBin.setDomains(true, true, false, true);
  xorBin.runPropagationAndCheck(true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] != F -> T, subsumed
  xorBin.setDomains(false, true, false, false);
  xorBin.runPropagationAndCheck(true, true, false, false, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] != T -> F, subsumed
  xorBin.setDomains(false, true, true, true);
  xorBin.runPropagationAndCheck(false, false, true, true, Core::PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] != [F, T] -> fixpoint
  xorBin.setDomains(false, true, false, true);
  xorBin.runPropagationAndCheck(false, true, false, true, Core::PropagationEvent::PROP_EVENT_FIXPOINT);
}//RunBinBiXor

OPTILAB_TEST(PropagatorBoolBinTest, GetFitnessBoolAndBin)
{
  using namespace Core;
  
  PropagatorBoolBinTest andBin(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_AND_BIN);
  
  // T /\ T -> 0
  andBin.setDomains(true, true, true, true);
  EXPECT_EQ(Propagator::getMinFitness(), andBin.getFitness());
  
  // T /\ F -> 1
  andBin.setDomains(true, true, false, false);
  EXPECT_EQ(1, andBin.getFitness());
  
  // F /\ T -> 1
  andBin.setDomains(false, false, true, true);
  EXPECT_EQ(1, andBin.getFitness());
  
  // F /\ F -> 1
  andBin.setDomains(false, false, false, false);
  EXPECT_EQ(1, andBin.getFitness());
  
  // [F, T] /\ T -> -1
  andBin.setDomains(false, true, true, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), andBin.getFitness());
  
  // T /\ [F, T] -> -1
  andBin.setDomains(true, true, false, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), andBin.getFitness());
}//GetFitnessBoolAndBin

OPTILAB_TEST(PropagatorBoolBinTest, GetFitnessBoolEqBin)
{
  using namespace Core;
  
  PropagatorBoolBinTest eqBin(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_EQ);
  
  // F == T -> 1
  eqBin.setDomains(false, false, true, true);
  EXPECT_EQ(1, eqBin.getFitness());
  
  // T == F -> 1
  eqBin.setDomains(true, true, false, false);
  EXPECT_EQ(1, eqBin.getFitness());
  
  // T == T -> 0
  eqBin.setDomains(true, true, true, true);
  EXPECT_EQ(Propagator::getMinFitness(), eqBin.getFitness());
  
  // F == F -> 0
  eqBin.setDomains(false, false, false, false);
  EXPECT_EQ(Propagator::getMinFitness(), eqBin.getFitness());
  
  // [F, T] == T -> -1
  eqBin.setDomains(false, true, true, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), eqBin.getFitness());
  
  // T == [F, T] -> -1
  eqBin.setDomains(true, true, false, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), eqBin.getFitness());
}//GetFitnessBoolEqBin

OPTILAB_TEST(PropagatorBoolBinTest, GetFitnessBoolLqBin)
{
  using namespace Core;
  
  PropagatorBoolBinTest lqBin(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_LQ);
  
  // F <= T -> 0
  lqBin.setDomains(false, false, true, true);
  EXPECT_EQ(Propagator::getMinFitness(), lqBin.getFitness());
  
  // T <= T -> 0
  lqBin.setDomains(true, true, true, true);
  EXPECT_EQ(Propagator::getMinFitness(), lqBin.getFitness());
  
  // F <= F -> 0
  lqBin.setDomains(false, false, false, false);
  EXPECT_EQ(Propagator::getMinFitness(), lqBin.getFitness());
  
  // T <= F -> 1
  lqBin.setDomains(true, true, false, false);
  EXPECT_EQ(1, lqBin.getFitness());
  
  // [F, T] <= T -> -1
  lqBin.setDomains(false, true, true, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), lqBin.getFitness());
  
  // F <= [F, T] -> -1
  lqBin.setDomains(false, false, false, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), lqBin.getFitness());
}//GetFitnessBoolLqBin

OPTILAB_TEST(PropagatorBoolBinTest, GetFitnessBoolLt)
{
  using namespace Core;
  
  PropagatorBoolBinTest ltBin(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_LT);
  
  // F < T -> 0
  ltBin.setDomains(false, false, true, true);
  EXPECT_EQ(Propagator::getMinFitness(), ltBin.getFitness());
  
  // F < F -> 1
  ltBin.setDomains(false, false, false, false);
  EXPECT_EQ(1, ltBin.getFitness());
  
  // T < T -> 1
  ltBin.setDomains(true, true, true, true);
  EXPECT_EQ(1, ltBin.getFitness());
  
  // T < F -> 1
  ltBin.setDomains(true, true, false, false);
  EXPECT_EQ(1, ltBin.getFitness());
  
  // [F, T] < T -> -1
  ltBin.setDomains(false, true, true, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), ltBin.getFitness());
  
  // F < [F, T] -> -1
  ltBin.setDomains(false, false, false, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), ltBin.getFitness());
}//GetFitnessBoolLt

OPTILAB_TEST(PropagatorBoolBinTest, GetFitnessBoolNot)
{
  using namespace Core;
  
  PropagatorBoolBinTest notBin(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_NOT);
  
  // F != T -> 0
  notBin.setDomains(false, false, true, true);
  EXPECT_EQ(Propagator::getMinFitness(), notBin.getFitness());
  
  // T != F -> 0
  notBin.setDomains(true, true, false, false);
  EXPECT_EQ(Propagator::getMinFitness(), notBin.getFitness());
  
  // T != T -> 1
  notBin.setDomains(true, true, true, true);
  EXPECT_EQ(1, notBin.getFitness());
  
  // F != F -> 1
  notBin.setDomains(false, false, false, false);
  EXPECT_EQ(1, notBin.getFitness());
  
  // [F, T] != T -> -1
  notBin.setDomains(false, true, true, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), notBin.getFitness());
  
  // F != [F, T] -> -1
  notBin.setDomains(false, false, false, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), notBin.getFitness());
}//GetFitnessBoolNot

OPTILAB_TEST(PropagatorBoolBinTest, GetFitnessBoolOr)
{
  using namespace Core;
  
  PropagatorBoolBinTest orBin(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_OR_BIN);
  
  // F \/ T -> 0
  orBin.setDomains(false, false, true, true);
  EXPECT_EQ(Propagator::getMinFitness(), orBin.getFitness());
  
  // T \/ F -> 0
  orBin.setDomains(true, true, false, false);
  EXPECT_EQ(Propagator::getMinFitness(), orBin.getFitness());
  
  // T \/ T -> 0
  orBin.setDomains(true, true, true, true);
  EXPECT_EQ(Propagator::getMinFitness(), orBin.getFitness());
  
  // F \/ F -> 1
  orBin.setDomains(false, false, false, false);
  EXPECT_EQ(1, orBin.getFitness());
  
  // [F, T] \/ T -> -1
  orBin.setDomains(false, true, true, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), orBin.getFitness());
  
  // T \/ [F, T] -> -1
  orBin.setDomains(true, true, false, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), orBin.getFitness());
}//GetFitnessBoolOr

OPTILAB_TEST(PropagatorBoolBinTest, GetFitnessBoolXor)
{
  using namespace Core;
  
  PropagatorBoolBinTest xorBin(PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_XOR_BIN);
  
  // F ^ T -> 0
  xorBin.setDomains(false, false, true, true);
  EXPECT_EQ(Propagator::getMinFitness(), xorBin.getFitness());
  
  // T ^ F -> 0
  xorBin.setDomains(true, true, false, false);
  EXPECT_EQ(Propagator::getMinFitness(), xorBin.getFitness());
  
  // T ^ T -> 1
  xorBin.setDomains(true, true, true, true);
  EXPECT_EQ(1, xorBin.getFitness());
  
  // F ^ F -> 1
  xorBin.setDomains(false, false, false, false);
  EXPECT_EQ(1, xorBin.getFitness());
  
  // [F, T] ^ F -> -1
  xorBin.setDomains(false, true, false, false);
  EXPECT_EQ(Propagator::getUnspecFitness(), xorBin.getFitness());
  
  // F ^ [F, T] -> -1
  xorBin.setDomains(false, false, false, true);
  EXPECT_EQ(Propagator::getUnspecFitness(), xorBin.getFitness());
}//GetFitnessBoolXor
