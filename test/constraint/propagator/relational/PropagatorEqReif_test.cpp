
#include "utest_globals.hpp"

#include "PropagatorEqReif.hpp"
#include "ConstraintTestInc.hpp"
#include "DomainElementInt64.hpp"

#define HREIF_L 0
#define HREIF_R 1

namespace {
  class PropagatorEqReifTest : public ::testing::Test {
  public:
    PropagatorEqReifTest()
    {
    }
    
    ~PropagatorEqReifTest()
    {
    }
    
    void setDomains(INT_64 aD0LB, INT_64 aD0UB,
                    INT_64 aD1LB, INT_64 aD1UB)
    {
      pDom0 = getDomainInt(aD0LB, aD0UB);
      pDom1 = getDomainInt(aD1LB, aD1UB);
      pDomainReif = std::make_shared<Core::DomainBoolean>();
    }//setDomains
    
    void setReifDomain(bool aTrueValue)
    {
      if(aTrueValue)
      {
        pDomainReif->setTrue();
      }
      else
      {
        pDomainReif->setFalse();
      }
    }//setReifDomain
    
    DomainBoolPtr getReifDomain()
    {
      return pDomainReif;
    }
    
    void postPropagator(INT_64 aD0LB, INT_64 aD0UB,
                        INT_64 aD1LB, INT_64 aD1UB,
                        Core::PropagationEvent aEvent,
                        int aHalfReif = -1)
    {
      std::unique_ptr<Core::PropagatorEqReif> propagator(nullptr);
      if(aHalfReif == -1)
      {
        propagator.reset(new Core::PropagatorEqReifBound(pDom0.get(),
                                                         pDom1.get(),
                                                         pDomainReif.get()));
      }
      else if(aHalfReif == HREIF_L)
      {
        propagator.reset(new Core::PropagatorEqHReifLBound(pDom0.get(),
                                                           pDom1.get(),
                                                           pDomainReif.get()));
      }
      else
      {
        assert(aHalfReif == HREIF_R);
        propagator.reset(new Core::PropagatorEqHReifRBound(pDom0.get(),
                                                           pDom1.get(),
                                                           pDomainReif.get()));
      }
      
      Core::PropagationEvent event = propagator->post();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        ASSERT_TRUE(pDom0->lowerBound()->isEqual(getElem(aD0LB).get()));
        ASSERT_TRUE(pDom0->upperBound()->isEqual(getElem(aD0UB).get()));
        
        ASSERT_TRUE(pDom1->lowerBound()->isEqual(getElem(aD1LB).get()));
        ASSERT_TRUE(pDom1->upperBound()->isEqual(getElem(aD1UB).get()));
      }
    }//postPropagator
    
    double getFitness(int aHalfReif = -1)
    {
      std::unique_ptr<Core::PropagatorEqReif> propagator(nullptr);
      if(aHalfReif == -1)
        {
          propagator.reset(new Core::PropagatorEqReifBound(pDom0.get(),
                                                           pDom1.get(),
                                                           pDomainReif.get()));
        }
        else if(aHalfReif == HREIF_L)
        {
          propagator.reset(new Core::PropagatorEqHReifLBound(pDom0.get(),
                                                             pDom1.get(),
                                                             pDomainReif.get()));
        }
        else
        {
          assert(aHalfReif == HREIF_R);
          propagator.reset(new Core::PropagatorEqHReifRBound(pDom0.get(),
                                                             pDom1.get(),
                                                             pDomainReif.get()));
        }
      return propagator->getFitness();
    }//getFitness
    
    void runPropagationAndCheck(INT_64 aD0LB, INT_64 aD0UB,
                                INT_64 aD1LB, INT_64 aD1UB,
                                Core::PropagationEvent aEvent,
                                bool aUseBound=USE_BOUND_PROP,
                                int aHalfReif = -1)
    {
      // Perform propagation
      std::unique_ptr<Core::PropagatorEqReif> propagator(nullptr);
      if(aUseBound == USE_BOUND_PROP)
      {
        if(aHalfReif == -1)
        {
          propagator.reset(new Core::PropagatorEqReifBound(pDom0.get(),
                                                           pDom1.get(),
                                                           pDomainReif.get()));
        }
        else if(aHalfReif == HREIF_L)
        {
          propagator.reset(new Core::PropagatorEqHReifLBound(pDom0.get(),
                                                             pDom1.get(),
                                                             pDomainReif.get()));
        }
        else
        {
          assert(aHalfReif == HREIF_R);
          propagator.reset(new Core::PropagatorEqHReifRBound(pDom0.get(),
                                                             pDom1.get(),
                                                             pDomainReif.get()));
        }
      }
      else
      {
        if(aHalfReif == -1)
        {
          propagator.reset(new Core::PropagatorEqReifDomain(pDom0.get(),
                                                            pDom1.get(),
                                                            pDomainReif.get()));
        }
        else if(aHalfReif == HREIF_L)
        {
          propagator.reset(new Core::PropagatorEqHReifLDomain(pDom0.get(),
                                                              pDom1.get(),
                                                              pDomainReif.get()));
        }
        else
        {
          assert(aHalfReif == HREIF_R);
          propagator.reset(new Core::PropagatorEqHReifRDomain(pDom0.get(),
                                                              pDom1.get(),
                                                              pDomainReif.get()));
        }
      }
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        ASSERT_TRUE(pDom0->lowerBound()->isEqual(getElem(aD0LB).get()));
        ASSERT_TRUE(pDom0->upperBound()->isEqual(getElem(aD0UB).get()));
      
        ASSERT_TRUE(pDom1->lowerBound()->isEqual(getElem(aD1LB).get()));
        ASSERT_TRUE(pDom1->upperBound()->isEqual(getElem(aD1UB).get()));
      }
    }//runPropagationAndCheck
    
    virtual void SetUp()
    {
    }
    
    virtual void TearDown()
    {
    }
    
  protected:
    // Domain instances
    DomainIntPtr pDom0;
    DomainIntPtr pDom1;
    // Domain boolean
    std::shared_ptr<Core::DomainBoolean> pDomainReif;
  };
}// end namespace


/*********************** Test ***********************/

OPTILAB_TEST_F(PropagatorEqReifTest, FullReifPost)
{
  using namespace Core;
  
  // 6 == 6 <-> b => b = true => subsumed
  setDomains(6, 6, 6, 6);
  postPropagator(6, 6, 6, 6, PropagationEvent::PROP_EVENT_SUBSUMED);
  ASSERT_TRUE(getReifDomain()->isTrue());
  
  // 6 == 7 <-> b => b = true => subsumed
  setDomains(6, 6, 7, 7);
  postPropagator(6,6,7,7, PropagationEvent::PROP_EVENT_SUBSUMED);
  ASSERT_TRUE(getReifDomain()->isFalse());
  
  // [0, 5] == [-1, 4] <-> b
  setDomains(0, 5, -1, 4);
  postPropagator(0, 5, -1, 4, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  ASSERT_FALSE(getReifDomain()->isTrue());
  ASSERT_FALSE(getReifDomain()->isFalse());
  
  // 6 == 6 <-> true => subsumed
  setDomains(6,6,6,6);
  setReifDomain(true);
  postPropagator(6,6,6,6, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 6 == 6 <-> false => fail
  setDomains(6,6,6,6);
  setReifDomain(false);
  postPropagator(6,6,6,6, PropagationEvent::PROP_EVENT_FAIL);
  
  // [0, 9] == [-10, -8] <-> true => fail
  setDomains(0, 9, -10, -8);
  setReifDomain(true);
  postPropagator(0, 9, -10, -8, PropagationEvent::PROP_EVENT_FAIL);
  
  // [0, 9] == [1, 8] <-> false
  setDomains(0, 9, 1, 8);
  setReifDomain(false);
  postPropagator(0, 9, 1, 8, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
}//FullReifPost

OPTILAB_TEST_F(PropagatorEqReifTest, HalfReifPost)
{
  using namespace Core;
  
  // 6 == 6 <- b => unspec
  setDomains(6,6,6,6);
  postPropagator(6,6,6,6, PropagationEvent::PROP_EVENT_RUN_UNSPEC, HREIF_L);
  ASSERT_FALSE(getReifDomain()->isTrue());
  ASSERT_FALSE(getReifDomain()->isFalse());
  
  // 6 == 6 -> b => b = true => subsumed
  setDomains(6,6,6,6);
  postPropagator(6,6,6,6, PropagationEvent::PROP_EVENT_SUBSUMED, HREIF_R);
  ASSERT_TRUE(getReifDomain()->isTrue());
  
  // [0, 9] == [1, 8] <- false
  setDomains(0,9,1,8);
  setReifDomain(false);
  postPropagator(0,9,1,8, PropagationEvent::PROP_EVENT_RUN_UNSPEC, HREIF_L);
  
  // [0, 9] == [1, 8] -> false
  setDomains(0, 9, 1, 8);
  setReifDomain(false);
  postPropagator(0, 9, 1, 8, PropagationEvent::PROP_EVENT_RUN_UNSPEC, HREIF_R);
  
  // 6 == 6 <- true => subsumed
  setDomains(6,6,6,6);
  setReifDomain(true);
  postPropagator(6,6,6,6, PropagationEvent::PROP_EVENT_SUBSUMED, HREIF_L);
  
  // 6 == 6 -> false => fail
  setDomains(6,6,6,6);
  setReifDomain(false);
  postPropagator(6,6,6,6, PropagationEvent::PROP_EVENT_FAIL, HREIF_R);
}//HalfReifPost

OPTILAB_TEST_F(PropagatorEqReifTest, simpleCases)
{
  using namespace Core;
  
  // 6 == 6 -> [T, F] => T, subsumed
  setDomains(6,6,6,6);
  runPropagationAndCheck(6,6,6,6, PropagationEvent::PROP_EVENT_SUBSUMED, true);
  
  // 6 == 6 -> [F] => fail
  setDomains(6,6,6,6);
  setReifDomain(false);
  runPropagationAndCheck(6,6,6,6,PropagationEvent::PROP_EVENT_FAIL, true);
  
  // 6 == 6 -> [T] => subsumed
  setDomains(6,6,6,6);
  setReifDomain(true);
  runPropagationAndCheck(6,6,6,6, PropagationEvent::PROP_EVENT_SUBSUMED, true);
  
  // 6 == 7 -> [T, F] => F, subsumed
  setDomains(6,6,7,7);
  runPropagationAndCheck(6,6,7,7, PropagationEvent::PROP_EVENT_SUBSUMED, true);
  
  // 6 == 7 -> [F] => subsumed
  setDomains(6,6,7,7);
  setReifDomain(false);
  runPropagationAndCheck(6,6,7,7, PropagationEvent::PROP_EVENT_SUBSUMED, true);
  
  // 6 == 7 -> [T] => fail
  setDomains(6,6,7,7);
  setReifDomain(true);
  runPropagationAndCheck(6,6,7,7, PropagationEvent::PROP_EVENT_FAIL, true);
  
  // [0, 9] == [1, 8]
  setDomains(0,9,1,8);
  runPropagationAndCheck(0,9,1,8, PropagationEvent::PROP_EVENT_FIXPOINT, true);
}//simpleCases

OPTILAB_TEST_F(PropagatorEqReifTest, simpleCasesHL)
{
  using namespace Core;
  
  // 6 == 6 <- [T, F] => fixpoint
  setDomains(6,6,6,6);
  runPropagationAndCheck(6,6,6,6, PropagationEvent::PROP_EVENT_FIXPOINT, true, HREIF_L);
  
  // 6 == 6 <- [F] => fixpoint
  setDomains(6,6,6,6);
  setReifDomain(false);
  runPropagationAndCheck(6,6,6,6, PropagationEvent::PROP_EVENT_FIXPOINT, true, HREIF_L);
  
  // 6 == 6 <- [T] => fixpoint
  setDomains(6,6,6,6);
  setReifDomain(true);
  runPropagationAndCheck(6,6,6,6, PropagationEvent::PROP_EVENT_SUBSUMED, true, HREIF_L);
  
  // 6 == 7 <- [T, F] => subsumed
  setDomains(6,6,7,7);
  runPropagationAndCheck(6,6,7,7, PropagationEvent::PROP_EVENT_SUBSUMED, true, HREIF_L);
  
  // 6 == 7 <- [F] => subsumed
  setDomains(6,6,7,7);
  setReifDomain(false);
  runPropagationAndCheck(6,6,7,7, PropagationEvent::PROP_EVENT_SUBSUMED, true, HREIF_L);
  
  // 6 == 7 <- [T] => fail
  setDomains(6,6,7,7);
  setReifDomain(true);
  runPropagationAndCheck(6,6,7,7, PropagationEvent::PROP_EVENT_FAIL, true, HREIF_L);
  
  // [0, 9] == [1, 8]
  setDomains(0, 9, 1, 8);
  runPropagationAndCheck(0, 9, 1, 8, PropagationEvent::PROP_EVENT_FIXPOINT, true, HREIF_L);
}//simpleCasesHL

OPTILAB_TEST_F(PropagatorEqReifTest, simpleCasesHR)
{
  using namespace Core;
  
  // 6 == 6 -> [T, F] => subsumed
  setDomains(6,6,6,6);
  runPropagationAndCheck(6,6,6,6, PropagationEvent::PROP_EVENT_SUBSUMED, true, HREIF_R);
  
  // 6 == 6 -> [F] => fail
  setDomains(6,6,6,6);
  setReifDomain(false);
  runPropagationAndCheck(6,6,6,6, PropagationEvent::PROP_EVENT_FAIL, true, HREIF_R);
  
  // 6 == 6 -> [T] => subsumed
  setDomains(6,6,6,6);
  setReifDomain(true);
  runPropagationAndCheck(6,6,6,6, PropagationEvent::PROP_EVENT_SUBSUMED, true, HREIF_R);
  
  // 6 == 7 -> [T, F] => fixpoint
  setDomains(6,6,7,7);
  runPropagationAndCheck(6,6,7,7, PropagationEvent::PROP_EVENT_FIXPOINT, true, HREIF_R);
  
  // 6 == 7 -> [F] => subsumed
  setDomains(6,6,7,7);
  setReifDomain(false);
  runPropagationAndCheck(6,6,7,7, PropagationEvent::PROP_EVENT_SUBSUMED, true, HREIF_R);
  
  // 6 == 7 -> [T] => fixpoint
  setDomains(6,6,7,7);
  setReifDomain(true);
  runPropagationAndCheck(6,6,7,7, PropagationEvent::PROP_EVENT_FIXPOINT, true, HREIF_R);
  
  // [0, 9] == [1, 8]
  setDomains(0, 9, 1, 8);
  runPropagationAndCheck(0, 9, 1, 8, PropagationEvent::PROP_EVENT_FIXPOINT, true, HREIF_R);
}//simpleCasesHR

OPTILAB_TEST_F(PropagatorEqReifTest, getFitness)
{
  // Test fitness function
  using namespace Core;
  
  /*
    full reif
  */
  setDomains(+5, +5, +2, +2);
  setReifDomain(true);
  EXPECT_EQ(3, getFitness());
  
  setDomains(+1, +1, +5, +5);
  setReifDomain(true);
  EXPECT_EQ(4, getFitness());
  
  setDomains(+5, +5, +5, +5);
  setReifDomain(true);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());
  
  setDomains(+5, +6, +5, +6);
  setReifDomain(true);
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness());

  setDomains(+5, +5, +2, +2);
  setReifDomain(false);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());
  
  setDomains(+1, +1, +5, +5);
  setReifDomain(false);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());
  
  setDomains(+5, +5, +5, +5);
  setReifDomain(false);
  EXPECT_EQ(1, getFitness());
  
  setDomains(+5, +6, +5, +6);
  setReifDomain(false);
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness());

  /*
    half reif left
  */
  setDomains(+5, +5, +2, +2);
  setReifDomain(true);
  EXPECT_EQ(3, getFitness(HREIF_L));
  
  setDomains(+1, +1, +5, +5);
  setReifDomain(true);
  EXPECT_EQ(4, getFitness(HREIF_L));
  
  setDomains(+5, +5, +5, +5);
  setReifDomain(true);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness(HREIF_L));
  
  setDomains(+5, +6, +5, +6);
  setReifDomain(true);
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness(HREIF_L));

  setDomains(+5, +5, +2, +2);
  setReifDomain(false);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness(HREIF_L));
  
  setDomains(+1, +1, +5, +5);
  setReifDomain(false);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness(HREIF_L));
  
  setDomains(+5, +5, +5, +5);
  setReifDomain(false);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness(HREIF_L));
  
  setDomains(+5, +6, +5, +6);
  setReifDomain(false);
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness(HREIF_L));

  /*
    half reif right
  */
  setDomains(+5, +5, +2, +2);
  setReifDomain(true);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness(HREIF_R));
  
  setDomains(+1, +1, +5, +5);
  setReifDomain(true);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness(HREIF_R));
  
  setDomains(+5, +5, +5, +5);
  setReifDomain(true);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness(HREIF_R));
  
  setDomains(+5, +6, +5, +6);
  setReifDomain(true);
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness(HREIF_R));

  setDomains(+5, +5, +2, +2);
  setReifDomain(false);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness(HREIF_R));
  
  setDomains(+1, +1, +5, +5);
  setReifDomain(false);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness(HREIF_R));
  
  setDomains(+5, +5, +5, +5);
  setReifDomain(false);
  EXPECT_EQ(1, getFitness(HREIF_R));
  
  setDomains(+5, +6, +5, +6);
  setReifDomain(false);
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness(HREIF_R));
}//getFitness
