
#include "utest_globals.hpp"

#include "PropagatorBinary.hpp"
#include "PropagatorNq.hpp"
#include "ConstraintTestInc.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"

namespace Core {
  
  class PropagatorBinaryTest : public PropagatorNq {
  public:
    enum BinaryDomainSignConfigTest : int {
        BDSC_PP = 0 // D0 > 0 /\ D1 > 0
      , BDSC_PN     // D0 > 0 /\ D1 < 0
      , BDSC_PX     // D0 > 0 /\ D1.LB <= 0 /\ D1.UB >= 0
      , BDSC_NP     // D0 < 0 /\ D1 < 0
      , BDSC_NN     // D0 < 0 /\ D1 < 0 /\ D2 < 0
      , BDSC_NX     // D0 < 0 /\ D1.LB <= 0 /\ D1.UB >= 0
      , BDSC_XP     // D0.LB <= 0 /\ D0.UB >= 0 /\ D1 > 0
      , BDSC_XN     // D0.LB <= 0 /\ D0.UB >= 0 /\ D1 < 0
      , BDSC_XX     // D0.LB <= 0 /\ D0.UB >= 0 /\ D1.LB <= 0 /\ D1.UB >= 0
    };
    
    PropagatorBinaryTest(Domain *aDom0, Domain *aDom1)
    : PropagatorNq(aDom0, aDom1)
    {
    }
    
    ~PropagatorBinaryTest()
    {
    }
    
    /// Expose method for testing
    BinaryDomainSignConfigTest getBinaryDomainSignConfig() const
    {
      return static_cast<BinaryDomainSignConfigTest>(PropagatorNq::getBinaryDomainSignConfig());
    }
  };
}// end namespace Core

/*********************** Test ***********************/

OPTILAB_TEST(PropagatorBinary, getBinaryDomainSignConfigEdge)
{
  using namespace Core;
  
  DomainIntPtr DP1 = getDomainInt(+0, +1);
  DomainIntPtr DP2 = getDomainInt(-1, +0);
  DomainIntPtr DP3 = getDomainInt(+0, +0);
  
  PropagatorBinaryTest ptXX1(DP1.get(), DP1.get());
  PropagatorBinaryTest ptXX2(DP2.get(), DP2.get());
  PropagatorBinaryTest ptXX3(DP3.get(), DP3.get());
  
  ASSERT_EQ(ptXX1.getBinaryDomainSignConfig(), Core::PropagatorBinaryTest::BinaryDomainSignConfigTest::BDSC_XX);
  ASSERT_EQ(ptXX2.getBinaryDomainSignConfig(), Core::PropagatorBinaryTest::BinaryDomainSignConfigTest::BDSC_XX);
  ASSERT_EQ(ptXX3.getBinaryDomainSignConfig(), Core::PropagatorBinaryTest::BinaryDomainSignConfigTest::BDSC_XX);
}//getBinaryDomainSignConfigEdge

OPTILAB_TEST(PropagatorBinary, getBinaryDomainSignConfig)
{
  using namespace Core;
  
  DomainIntPtr D0P = getDomainInt(+10, +20);
  DomainIntPtr D1P = getDomainInt(+10, +20);
  
  DomainIntPtr D0N = getDomainInt(-20, -10);
  DomainIntPtr D1N = getDomainInt(-20, -10);
  
  DomainIntPtr D0X = getDomainInt(-20, +10);
  DomainIntPtr D1X = getDomainInt(-20, +10);
  
  // Create propagators according to DomainSignConfig
  PropagatorBinaryTest ptPP(D0P.get(), D1P.get());
  PropagatorBinaryTest ptPN(D0P.get(), D1N.get());
  PropagatorBinaryTest ptPX(D0P.get(), D1X.get());
  PropagatorBinaryTest ptNP(D0N.get(), D1P.get());
  PropagatorBinaryTest ptNN(D0N.get(), D1N.get());
  PropagatorBinaryTest ptNX(D0N.get(), D1X.get());
  PropagatorBinaryTest ptXP(D0X.get(), D1P.get());
  PropagatorBinaryTest ptXN(D0X.get(), D1N.get());
  PropagatorBinaryTest ptXX(D0X.get(), D1X.get());

  ASSERT_EQ(ptPP.getBinaryDomainSignConfig(), Core::PropagatorBinaryTest::BinaryDomainSignConfigTest::BDSC_PP);
  ASSERT_EQ(ptPN.getBinaryDomainSignConfig(), Core::PropagatorBinaryTest::BinaryDomainSignConfigTest::BDSC_PN);
  ASSERT_EQ(ptPX.getBinaryDomainSignConfig(), Core::PropagatorBinaryTest::BinaryDomainSignConfigTest::BDSC_PX);
  ASSERT_EQ(ptNP.getBinaryDomainSignConfig(), Core::PropagatorBinaryTest::BinaryDomainSignConfigTest::BDSC_NP);
  ASSERT_EQ(ptNN.getBinaryDomainSignConfig(), Core::PropagatorBinaryTest::BinaryDomainSignConfigTest::BDSC_NN);
  ASSERT_EQ(ptNX.getBinaryDomainSignConfig(), Core::PropagatorBinaryTest::BinaryDomainSignConfigTest::BDSC_NX);
  ASSERT_EQ(ptXP.getBinaryDomainSignConfig(), Core::PropagatorBinaryTest::BinaryDomainSignConfigTest::BDSC_XP);
  ASSERT_EQ(ptXN.getBinaryDomainSignConfig(), Core::PropagatorBinaryTest::BinaryDomainSignConfigTest::BDSC_XN);
  ASSERT_EQ(ptXX.getBinaryDomainSignConfig(), Core::PropagatorBinaryTest::BinaryDomainSignConfigTest::BDSC_XX);
}//getBinaryDomainSignConfig

