
#include "utest_globals.hpp"

#include "PropagatorLinearReif.hpp"
#include "ConstraintTestInc.hpp"

namespace Core {
  
  class PropagatorLinearReifTest : public PropagatorLinearReif {
  public:
    PropagatorLinearReifTest(DomainElementArray& aCoeffArray,
                             DomainArray&        aDomainArray,
                             DomainElement*      aConstant,
                             DomainBoolean*      aDomBool)
    : PropagatorLinearReif(aCoeffArray,
                           aDomainArray,
                           aConstant,
                           aDomBool,
                           PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
                           PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ,
                           PropagatorSemanticClass::PROP_SEMANTIC_CLASS_LINEAR)
    {
    }
    
    ~PropagatorLinearReifTest()
    {
    }
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
    PropagationEvent post() override
    {
      return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    }
    
    PropagationEvent runPropagation() override
    {
      return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    }
    
    /// Expose methods for testing
    bool allSingleton()
    {
      return PropagatorLinearReif::areAllDomainsSingleton();
    }
    
    DomainElement& getLeftVal()
    {
      return *PropagatorLinearReif::getLeftSideExpressionValue();
    }
  };
}// end namespace Core

/*********************** Test ***********************/

OPTILAB_TEST(PropagatorLinearReif, basics)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  DomainElementArray coeff1(2);
  DomainArray domains1(2);
  DomainBoolPtr domBool1 = getDomainBool();
  
  // 2 * [1, 3] + 3 * [2, 4] = 0
  coeff1.assignElementToCell(0, elementManager.createDomainElementInt64(2));
  coeff1.assignElementToCell(1, elementManager.createDomainElementInt64(3));
  DomainIntPtr domainA = getDomainInt(1, 3);
  DomainIntPtr domainB = getDomainInt(2, 4);
  domains1.assignElementToCell(0, domainA.get());
  domains1.assignElementToCell(1, domainB.get());
  
  PropagatorLinearReifTest ptXX1(coeff1, domains1, elementManager.createDomainElementInt64(0), domBool1.get());
  
  ASSERT_FALSE(ptXX1.allSingleton());
  ASSERT_TRUE(ptXX1.getLeftVal().isEqual(elementManager.createDomainElementInt64(8)));
  
  elementManager.clear();
  
  DomainElementArray coeff2(2);
  DomainArray domains2(2);
  DomainBoolPtr domBool2 = getDomainBool();
  
  // 2 * [3, 3] + 3 * [4, 4] = 0 <-> true
  coeff2.assignElementToCell(0, elementManager.createDomainElementInt64(2));
  coeff2.assignElementToCell(1, elementManager.createDomainElementInt64(3));
  DomainIntPtr domainC = getDomainInt(3, 3);
  DomainIntPtr domainD = getDomainInt(4, 4);
  domains2.assignElementToCell(0, domainC.get());
  domains2.assignElementToCell(1, domainD.get());
  domBool2->setTrue();
  
  PropagatorLinearReifTest ptXX2(coeff2, domains2, elementManager.createDomainElementInt64(0), domBool2.get());
  
  ASSERT_TRUE(ptXX2.allSingleton());
  ASSERT_TRUE(ptXX2.getLeftVal().isEqual(elementManager.createDomainElementInt64(0)));
}//basics
