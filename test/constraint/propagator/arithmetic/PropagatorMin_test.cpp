
#include "utest_globals.hpp"

#include "DomainDefs.hpp"
#include "PropagatorMin.hpp"
#include "ConstraintTestInc.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"

#include <vector>
#include <memory>

namespace {
  class PropagatorMinTest : public ::testing::Test {
  public:
    PropagatorMinTest()
    {
    }
    
    ~PropagatorMinTest()
    {
    }
    
    void setDomains(INT_64 aD0LB, INT_64 aD0UB,
                    INT_64 aD1LB, INT_64 aD1UB,
                    INT_64 aD2LB, INT_64 aD2UB)
    {
      pDom0 = getDomainInt(aD0LB, aD0UB);
      pDom1 = getDomainInt(aD1LB, aD1UB);
      pDom2 = getDomainInt(aD2LB, aD2UB);
    }//setDomains
    
    void setDomains(std::vector<INT_64>&& aD0,
                    std::vector<INT_64>&& aD1,
                    std::vector<INT_64>&& aD2)
    {
      pDom0 = getDomainInt(aD0);
      pDom1 = getDomainInt(aD1);
      pDom2 = getDomainInt(aD2);
    }//setDomains
    
    double getFitness()
    {
      std::unique_ptr<Core::PropagatorMin> propagator(nullptr);
      propagator.reset(new Core::PropagatorMinBound(pDom0.get(),
                                                     pDom1.get(),
                                                     pDom2.get()));
      return propagator->getFitness();
    }//getFitness
    
    void postPropagator(INT_64 aD0LB, INT_64 aD0UB,
                        INT_64 aD1LB, INT_64 aD1UB,
                        INT_64 aD2LB, INT_64 aD2UB,
                        Core::PropagationEvent aEvent)
    {
      std::unique_ptr<Core::PropagatorMinBound> propagator(nullptr);
      propagator.reset(new Core::PropagatorMinBound(pDom0.get(), pDom1.get(), pDom2.get()));
      
      Core::PropagationEvent event = propagator->post();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        ASSERT_TRUE(pDom0->lowerBound()->isEqual(getElem(aD0LB).get()));
        ASSERT_TRUE(pDom0->upperBound()->isEqual(getElem(aD0UB).get()));
        
        ASSERT_TRUE(pDom1->lowerBound()->isEqual(getElem(aD1LB).get()));
        ASSERT_TRUE(pDom1->upperBound()->isEqual(getElem(aD1UB).get()));
        
        ASSERT_TRUE(pDom2->lowerBound()->isEqual(getElem(aD2LB).get()));
        ASSERT_TRUE(pDom2->upperBound()->isEqual(getElem(aD2UB).get()));
      }
    }//postPropagator
    
    void runPropagationAndCheck(INT_64 aD0LB, INT_64 aD0UB,
                                INT_64 aD1LB, INT_64 aD1UB,
                                INT_64 aD2LB, INT_64 aD2UB,
                                Core::PropagationEvent aEvent,
                                bool aUseBound=USE_BOUND_PROP)
    {
      // Perform propagation
      std::unique_ptr<Core::PropagatorMin> propagator(nullptr);
      if(aUseBound == USE_BOUND_PROP)
      {
        propagator.reset(new Core::PropagatorMinBound(pDom0.get(), pDom1.get(), pDom2.get()));
      }
      else
      {
        propagator.reset(new Core::PropagatorMinDomain(pDom0.get(), pDom1.get(), pDom2.get()));
      }
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        ASSERT_TRUE(pDom0->lowerBound()->isEqual(getElem(aD0LB).get()));
        ASSERT_TRUE(pDom0->upperBound()->isEqual(getElem(aD0UB).get()));
      
        ASSERT_TRUE(pDom1->lowerBound()->isEqual(getElem(aD1LB).get()));
        ASSERT_TRUE(pDom1->upperBound()->isEqual(getElem(aD1UB).get()));
      
        ASSERT_TRUE(pDom2->lowerBound()->isEqual(getElem(aD2LB).get()));
        ASSERT_TRUE(pDom2->upperBound()->isEqual(getElem(aD2UB).get()));
      }
    }//runPropagationAndCheck
    
    void runPropagationAndCheckDomains(std::vector<INT_64>&& aD0,
                                       std::vector<INT_64>&& aD1,
                                       std::vector<INT_64>&& aD2,
                                       Core::PropagationEvent aEvent,
                                       bool aUseBound=USE_DOMAIN_PROP)
    {
      // Perform propagation
      std::unique_ptr<Core::PropagatorMin> propagator(nullptr);
      if(aUseBound == USE_BOUND_PROP)
      {
        propagator.reset(new Core::PropagatorMinBound(pDom0.get(), pDom1.get(), pDom2.get()));
      }
      else
      {
        propagator.reset(new Core::PropagatorMinDomain(pDom0.get(), pDom1.get(), pDom2.get()));
      }
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        for(auto& elem : aD0)
        {
          ASSERT_TRUE(pDom0->contains(getElem(elem).get()));
        }
        ASSERT_EQ(pDom0->getSize(), aD0.size());
        
        for(auto& elem : aD1)
        {
          ASSERT_TRUE(pDom1->contains(getElem(elem).get()));
        }
        ASSERT_EQ(pDom1->getSize(), aD1.size());
        
        for(auto& elem : aD2)
        {
          ASSERT_TRUE(pDom2->contains(getElem(elem).get()));
        }
        ASSERT_EQ(pDom2->getSize(), aD2.size());
      }
    }//runPropagationAndCheckDomains
    
    virtual void SetUp()
    {
    }
    
    virtual void TearDown()
    {
    }
    
  protected:
    // Domain instances
    DomainIntPtr pDom0;
    DomainIntPtr pDom1;
    DomainIntPtr pDom2;
  };
}// end namespace


/*********************** Test ***********************/

OPTILAB_TEST_F(PropagatorMinTest, PropagateSingletonEdges)
{
  using namespace Core;
  
  // @note -INF = Limits::int64Min() and +INF = Limits::int64Max()
  
  // INF, 2 = [-INF, +INF] => 2
  setDomains(Limits::int64Max(), Limits::int64Max(), +2, +2, Limits::int64Min(), Limits::int64Max());
  runPropagationAndCheck(Limits::int64Max(), Limits::int64Max(),
                         +2, +2,
                         2, 2,
                         PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -INF, 2 = [-INF, INF] => -INF
  setDomains(Limits::int64Min(), Limits::int64Min(), +2, +2, Limits::int64Min(), Limits::int64Max());
  runPropagationAndCheck(Limits::int64Min(), Limits::int64Min(),
                         +2, +2,
                         Limits::int64Min(), Limits::int64Min(),
                         PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -INF, -INF = [-INF, INF] => -INF
  setDomains(Limits::int64Min(), Limits::int64Min(),
             Limits::int64Min(), Limits::int64Min(),
             Limits::int64Min(), Limits::int64Max());
  runPropagationAndCheck(Limits::int64Min(), Limits::int64Min(),
                         Limits::int64Min(), Limits::int64Min(),
                         Limits::int64Min(), Limits::int64Min(),
                         PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -INF, +INF = [-INF, INF] => -INF
  setDomains(Limits::int64Min(), Limits::int64Min(),
             Limits::int64Max(), Limits::int64Max(),
             Limits::int64Min(), Limits::int64Max());
  runPropagationAndCheck(Limits::int64Min(), Limits::int64Min(),
                         Limits::int64Max(), Limits::int64Max(),
                         Limits::int64Min(), Limits::int64Min(),
                         PropagationEvent::PROP_EVENT_SUBSUMED);
}//PropagateSingletonEdges

OPTILAB_TEST_F(PropagatorMinTest, PropagateSingleton1)
{
  using namespace Core;
  
  // 10, 2 = 2
  setDomains(+10, +10, +2, +2, 2, 2);
  runPropagationAndCheck(+10, +10, +2, +2, 2, 2, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 10, -2 = -2
  setDomains(+10, +10, -2, -2, -2, -2);
  runPropagationAndCheck(+10, +10, -2, -2, -2, -2, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -500, -200 = -500
  setDomains(-500, -500, -200, -200, -500 , -500);
  runPropagationAndCheck(-500, -500, -200, -200, -500, -500, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -5, -2 != 20
  setDomains(-5, -5, -2, -2, +20, +20);
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
}//PropagateSingleton1

OPTILAB_TEST_F(PropagatorMinTest, PropagateSingletonPost1)
{
  using namespace Core;
  
  // 10, 2 = 2
  setDomains(+10, +10, +2, +2, 2, 2);
  postPropagator(+10, +10, +2, +2, 2, 2, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 10, -2 = -2
  setDomains(+10, +10, -2, -2, -2, -2);
  postPropagator(+10, +10, -2, -2, -2, -2, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -500, -200 = -500
  setDomains(-500, -500, -200, -200, -500 , -500);
  postPropagator(-500, -500, -200, -200, -500, -500, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -5, -2 != 20
  setDomains(-5, -5, -2, -2, +20, +20);
  postPropagator(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
}//PropagateSingletonPost1

OPTILAB_TEST_F(PropagatorMinTest, PropagateSingleton2)
{
  using namespace Core;
  
  // 3, 2 = [-10, 10] => 2
  setDomains(3, 3, +2, +2, -10, +10);
  runPropagationAndCheck(3, 3, +2, +2, 2, 2, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -3, -2 = [-10, 10] => -3
  setDomains(-3, -3, -2, -2, -10, +10);
  runPropagationAndCheck(-3, -3, -2, -2, -3, -3, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 1, [-10, 10] = 2 => fail (2 > 1)
  setDomains(+1, +1, -10, +10, +2, +2);
  runPropagationAndCheck(+1, +1, 2, 10, +2, +2, PropagationEvent::PROP_EVENT_FAIL);
  
  // 1, [-10, 10] = -1 => [-1, 10]
  setDomains(+1, +1, -10, +10, -1, -1);
  runPropagationAndCheck(+1, +1, -1, 10, -1, -1, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-10, 10], 2 = 1 => [1, 10]
  setDomains(-10, +10, +2, +2, +1, +1);
  runPropagationAndCheck(1, 10, +2, +2, +1, +1, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-10, 10], 2 = 11 => fail
  setDomains(-10, +10, +2, +2, +11, +11);
  runPropagationAndCheck(-10, +5, +2, +2, +5, +5, PropagationEvent::PROP_EVENT_FAIL);
}//PropagateSingleton2

OPTILAB_TEST_F(PropagatorMinTest, PropagateSingletonPost2)
{
  using namespace Core;
  
  // 3, 2 = [-10, 10] => 2
  setDomains(3, 3, +2, +2, -10, +10);
  postPropagator(3, 3, +2, +2, 2, 2, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -3, -2 = [-10, 10] => -3
  setDomains(-3, -3, -2, -2, -10, +10);
  postPropagator(-3, -3, -2, -2, -3, -3, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 1, [-10, 10] = 0 => no changes
  setDomains(+1, +1, -10, +10, 0, 0);
  postPropagator(+1, +1, -10, +10, 0, 0, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // [-10, 10], 2 = 1 => no changes
  setDomains(-10, +10, +2, +2, 1, 1);
  postPropagator(-10, +10, +2, +2, 1, 1, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // [-10, 10], 2 = 11 => fail
  setDomains(-10, +10, +2, +2, +11, +11);
  postPropagator(-10, +5, +2, +2, +5, +5, PropagationEvent::PROP_EVENT_FAIL);
}//PropagateSingletonPost2

OPTILAB_TEST_F(PropagatorMinTest, Propagate)
{
  using namespace Core;
  
  // [+20, +50], [2, 4] = [-100, -100] => [2, 4]
  setDomains(+20, +50, +2, +4, -100, +100);
  runPropagationAndCheck(+20, +50, +2, +4, 2, 4, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [+20, +50], [2, 60] = [-100, -100] => [2, 50]
  setDomains(+20, +50, +2, +60, -100, +100);
  runPropagationAndCheck(+20, +50, +2, +60, 2, 50, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [+3, +50], [2, 100] = [-1, 10] => D2 = [2, 10]
  setDomains(+3, +50, +2, +100, -1, +10);
  runPropagationAndCheck(+3, +50, +2, +100, 2, 10, PropagationEvent::PROP_EVENT_FIXPOINT);
}//Propagate

OPTILAB_TEST_F(PropagatorMinTest, PropagateDomain)
{
  using namespace Core;

  setDomains({+1, +3, +5}, {+2, +4, +6}, {1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
  runPropagationAndCheckDomains({1, 3, 5}, {+2, +4, 6}, {1, 2, 3, 4, 5}, PropagationEvent::PROP_EVENT_FIXPOINT, USE_DOMAIN_PROP);
  
  setDomains({+2, +4, +6}, {+1, +3, +5}, {1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
  runPropagationAndCheckDomains({2, 4, 6}, {1, 3, 5}, {1, 2, 3, 4, 5}, PropagationEvent::PROP_EVENT_FIXPOINT, USE_DOMAIN_PROP);
}//PropagateDomain

OPTILAB_TEST_F(PropagatorMinTest, getFitness)
{
  // Test fitness function
  using namespace Core;
  
  setDomains(+5, +5, +2, +2, +2, +2);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());

  setDomains(+0, +0, +2, +2, +2, +2);
  EXPECT_EQ(2, getFitness());

  setDomains(-5, -5, +2, +2, +2, +2);
  EXPECT_EQ(7, getFitness());

  setDomains(+5, +5, -2, -2, +2, +2);
  EXPECT_EQ(4, getFitness());

  setDomains(+2, +2, +2, +2, +1, +1);
  EXPECT_EQ(1, getFitness());

  setDomains(+9, +9, +2, +2, -9, -9);
  EXPECT_EQ(11, getFitness());

  setDomains(-1, -1, -2, -2, -2, -2);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());
}//getFitness
