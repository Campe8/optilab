
#include "utest_globals.hpp"

#include "DomainDefs.hpp"
#include "PropagatorPlus.hpp"
#include "ConstraintTestInc.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"

#include <vector>
#include <memory>

namespace {
  class PropagatorPlusTest : public ::testing::Test {
  public:
    PropagatorPlusTest()
    {
    }
    
    ~PropagatorPlusTest()
    {
    }
    
    void setDomains(INT_64 aD0LB, INT_64 aD0UB,
                    INT_64 aD1LB, INT_64 aD1UB,
                    INT_64 aD2LB, INT_64 aD2UB)
    {
      pDom0 = getDomainInt(aD0LB, aD0UB);
      pDom1 = getDomainInt(aD1LB, aD1UB);
      pDom2 = getDomainInt(aD2LB, aD2UB);
    }//setDomains
    
    void setDomains(std::vector<INT_64>&& aD0,
                    std::vector<INT_64>&& aD1,
                    std::vector<INT_64>&& aD2)
    {
      pDom0 = getDomainInt(aD0);
      pDom1 = getDomainInt(aD1);
      pDom2 = getDomainInt(aD2);
    }//setDomains
    
    double getFitness()
    {
      std::unique_ptr<Core::PropagatorPlus> propagator(nullptr);
      propagator.reset(new Core::PropagatorPlusBound(pDom0.get(),
                                                     pDom1.get(),
                                                     pDom2.get()));
      return propagator->getFitness();
    }//getFitness
    
    void postPropagator(INT_64 aD0LB, INT_64 aD0UB,
                        INT_64 aD1LB, INT_64 aD1UB,
                        INT_64 aD2LB, INT_64 aD2UB,
                        Core::PropagationEvent aEvent)
    {
      std::unique_ptr<Core::PropagatorPlus> propagator(nullptr);
      propagator.reset(new Core::PropagatorPlusBound(pDom0.get(), pDom1.get(), pDom2.get()));
      
      Core::PropagationEvent event = propagator->post();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        ASSERT_TRUE(pDom0->lowerBound()->isEqual(getElem(aD0LB).get()));
        ASSERT_TRUE(pDom0->upperBound()->isEqual(getElem(aD0UB).get()));
        
        ASSERT_TRUE(pDom1->lowerBound()->isEqual(getElem(aD1LB).get()));
        ASSERT_TRUE(pDom1->upperBound()->isEqual(getElem(aD1UB).get()));
        
        ASSERT_TRUE(pDom2->lowerBound()->isEqual(getElem(aD2LB).get()));
        ASSERT_TRUE(pDom2->upperBound()->isEqual(getElem(aD2UB).get()));
      }
    }//postPropagator
    
    void runPropagationAndCheck(INT_64 aD0LB, INT_64 aD0UB,
                                INT_64 aD1LB, INT_64 aD1UB,
                                INT_64 aD2LB, INT_64 aD2UB,
                                Core::PropagationEvent aEvent,
                                bool aUseBound=USE_BOUND_PROP)
    {
      // Perform propagation
      std::unique_ptr<Core::PropagatorPlus> propagator(nullptr);
      if(aUseBound == USE_BOUND_PROP)
      {
        propagator.reset(new Core::PropagatorPlusBound(pDom0.get(), pDom1.get(), pDom2.get()));
      }
      else
      {
        propagator.reset(new Core::PropagatorPlusDomain(pDom0.get(), pDom1.get(), pDom2.get()));
      }
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        ASSERT_TRUE(pDom0->lowerBound()->isEqual(getElem(aD0LB).get()));
        ASSERT_TRUE(pDom0->upperBound()->isEqual(getElem(aD0UB).get()));
      
        ASSERT_TRUE(pDom1->lowerBound()->isEqual(getElem(aD1LB).get()));
        ASSERT_TRUE(pDom1->upperBound()->isEqual(getElem(aD1UB).get()));
      
        ASSERT_TRUE(pDom2->lowerBound()->isEqual(getElem(aD2LB).get()));
        ASSERT_TRUE(pDom2->upperBound()->isEqual(getElem(aD2UB).get()));
      }
    }//runPropagationAndCheck
    
    void runPropagationAndCheckDomains(std::vector<INT_64>&& aD0,
                                       std::vector<INT_64>&& aD1,
                                       std::vector<INT_64>&& aD2,
                                       Core::PropagationEvent aEvent,
                                       bool aUseBound=USE_DOMAIN_PROP)
    {
      // Perform propagation
      std::unique_ptr<Core::PropagatorPlus> propagator(nullptr);
      if(aUseBound == USE_BOUND_PROP)
      {
        propagator.reset(new Core::PropagatorPlusBound(pDom0.get(), pDom1.get(), pDom2.get()));
      }
      else
      {
        propagator.reset(new Core::PropagatorPlusDomain(pDom0.get(), pDom1.get(), pDom2.get()));
      }
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        for(auto& elem : aD0)
        {
          ASSERT_TRUE(pDom0->contains(getElem(elem).get()));
        }
        ASSERT_EQ(pDom0->getSize(), aD0.size());
        
        for(auto& elem : aD1)
        {
          ASSERT_TRUE(pDom1->contains(getElem(elem).get()));
        }
        ASSERT_EQ(pDom1->getSize(), aD1.size());
        
        for(auto& elem : aD2)
        {
          ASSERT_TRUE(pDom2->contains(getElem(elem).get()));
        }
        ASSERT_EQ(pDom2->getSize(), aD2.size());
      }
    }//runPropagationAndCheckDomains
    
    virtual void SetUp()
    {
    }
    
    virtual void TearDown()
    {
    }
    
  protected:
    // Domain instances
    DomainIntPtr pDom0;
    DomainIntPtr pDom1;
    DomainIntPtr pDom2;
  };
}// end namespace


/*********************** Test ***********************/

OPTILAB_TEST_F(PropagatorPlusTest, PropagateSingletonEdges)
{
  using namespace Core;
  
  // @note -INF = Limits::int64Min() and +INF = Limits::int64Max()
  
  // INF + 2 = [-INF, +INF] =>INF
  setDomains(Limits::int64Max(), Limits::int64Max(), +2, +2, Limits::int64Min(), Limits::int64Max());
  runPropagationAndCheck(Limits::int64Max(), Limits::int64Max(),
                         +2, +2,
                         Limits::int64Max(), Limits::int64Max(),
                         PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -INF + 2 = [-INF, INF] => [-INF + 2, -INF + 2]
  setDomains(Limits::int64Min(), Limits::int64Min(), +2, +2, Limits::int64Min(), Limits::int64Max());
  runPropagationAndCheck(Limits::int64Min(), Limits::int64Min(),
                         +2, +2,
                         Limits::int64Min() + 2, Limits::int64Min() + 2,
                         PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -INF + -INF = [-INF, INF] => -INF
  setDomains(Limits::int64Min(), Limits::int64Min(),
             Limits::int64Min(), Limits::int64Min(),
             Limits::int64Min(), Limits::int64Max());
  runPropagationAndCheck(Limits::int64Min(), Limits::int64Min(),
                         Limits::int64Min(), Limits::int64Min(),
                         Limits::int64Min(), Limits::int64Min(),
                         PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -INF + +INF = [-INF, INF] => 0
  setDomains(Limits::int64Min(), Limits::int64Min(),
             Limits::int64Max(), Limits::int64Max(),
             Limits::int64Min(), Limits::int64Max());
  runPropagationAndCheck(Limits::int64Min(), Limits::int64Min(),
                         Limits::int64Max(), Limits::int64Max(),
                         0, 0,
                         PropagationEvent::PROP_EVENT_SUBSUMED);
}//PropagateSingletonEdges

OPTILAB_TEST_F(PropagatorPlusTest, ReevaluationSet)
{
  using namespace Core;
  
  auto dom = getDomainInt(1, 2);
  PropagatorPlusBound prop(dom.get(), dom.get(), dom.get());
  
  auto singleton = std::make_shared<DomainEventSingleton>();
  auto lwb = std::make_shared<DomainEventLwb>();
  auto upb = std::make_shared<DomainEventUpb>();
  auto bound = std::make_shared<DomainEventBound>();
  
  DomainEventSet eventSet = prop.getReevaluationEventSet();
  for(auto& event : eventSet)
  {
    ASSERT_TRUE(*event == *singleton ||
                *event == *lwb       ||
                *event == *upb       ||
                *event == *bound);
  }
}//ReevaluationSet

OPTILAB_TEST_F(PropagatorPlusTest, PropagateSingleton1)
{
  using namespace Core;
  
  // 10 + 2 = 12
  setDomains(+10, +10, +2, +2, +12, +12);
  runPropagationAndCheck(+10, +10, +2, +2, +12, +12, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 10 + -2 = 8
  setDomains(+10, +10, -2, -2, 8, 8);
  runPropagationAndCheck(+10, +10, -2, -2, 8, 8, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -500 + -200 = -700
  setDomains(-500, -500, -200, -200, -700 , -700);
  runPropagationAndCheck(-500, -500, -200, -200, -700, -700, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -5 + -2 != 20
  setDomains(-5, -5, -2, -2, +20, +20);
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
}//PropagateSingleton1

OPTILAB_TEST_F(PropagatorPlusTest, PropagateSingletonPost1)
{
  using namespace Core;
  
  // Post propagates on D2
  
  // 10 + 2 = 12
  setDomains(+10, +10, +2, +2, +12, +12);
  postPropagator(+10, +10, +2, +2, +12, +12, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 10 + -2 = 8
  setDomains(+10, +10, -2, -2, 8, 8);
  postPropagator(+10, +10, -2, -2, 8, 8, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -500 + -200 = -700
  setDomains(-500, -500, -200, -200, -700 , -700);
  postPropagator(-500, -500, -200, -200, -700, -700, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -5 + -2 != 20
  setDomains(-5, -5, -2, -2, +20, +20);
  postPropagator(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
}//PropagateSingletonPost1

OPTILAB_TEST_F(PropagatorPlusTest, PropagateSingleton2)
{
  using namespace Core;
  
  // 3 + 2 = [-10, 10] => 5
  setDomains(3, 3, +2, +2, -10, +10);
  runPropagationAndCheck(3, 3, +2, +2, +5, +5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -3 + -2 = [-10, 10] => -5
  setDomains(-3, -3, -2, -2, -10, +10);
  runPropagationAndCheck(-3, -3, -2, -2, -5, -5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 10 + [-10, 10] = 2 => -8
  // @note 10 / 4 = 2.5 -> 2 using INT_64
  setDomains(+10, +10, -10, +10, +2, +2);
  runPropagationAndCheck(+10, +10, -8, -8, +2, +2, PropagationEvent::PROP_EVENT_SUBSUMED);

  // 3 * [-10, 10] = 5 => 2
  // @note 10 / 4 = 2.5 -> 2 using INT_64
  setDomains(3, 3, -10, +10, 5, 5);
  runPropagationAndCheck(3, 3, 2, 2, 5, 5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [-10, 10] + 2 = 5 => 3
  setDomains(-10, +10, +2, +2, +5, +5);
  runPropagationAndCheck(3, 3, +2, +2, +5, +5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [-10, 10] / -2 = -5 => -3
  setDomains(-10, +10, -2, -2, -5, -5);
  runPropagationAndCheck(-3, -3, -2, -2, -5, -5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 10 + 2 = [6, 10] => FAIL
  setDomains(+10, +10, +2, +2, 6, 10);
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
}//PropagateSingleton2

OPTILAB_TEST_F(PropagatorPlusTest, PropagateSingletonPost2)
{
  using namespace Core;
  
  // 3 + 2 = [-10, 10] => 5
  setDomains(3, 3, +2, +2, -10, +10);
  postPropagator(3, 3, +2, +2, +5, +5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -3 + -2 = [-10, 10] => -5
  setDomains(-3, -3, -2, -2, -10, +10);
  postPropagator(-3, -3, -2, -2, -5, -5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 10 + [-10, 10] = 2 => -8
  // @note 10 / 4 = 2.5 -> 2 using INT_64
  setDomains(+10, +10, -10, +10, +2, +2);
  postPropagator(+10, +10, -8, -8, +2, +2, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 3 * [-10, 10] = 5 => 2
  // @note 10 / 4 = 2.5 -> 2 using INT_64
  setDomains(3, 3, -10, +10, 5, 5);
  postPropagator(3, 3, 2, 2, 5, 5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [-10, 10] + 2 = 5 => 3
  setDomains(-10, +10, +2, +2, +5, +5);
  postPropagator(3, 3, +2, +2, +5, +5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [-10, 10] / -2 = -5 => -3
  setDomains(-10, +10, -2, -2, -5, -5);
  postPropagator(-3, -3, -2, -2, -5, -5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 10 + 2 = [6, 10] => FAIL
  setDomains(+10, +10, +2, +2, 6, 10);
  postPropagator(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
}//PropagateSingletonPost2

OPTILAB_TEST_F(PropagatorPlusTest, PropagatePPN_NNP)
{
  using namespace Core;
  
  // [+20, +50] + [2, 4] = [-100, -1] => fail
  setDomains(+20, +50, +2, +4, -100, -1);
  runPropagationAndCheck(+20, +50, +2, +4, -100, -1, PropagationEvent::PROP_EVENT_FAIL);
  
  // [-50, -20] / [-50, -1] = [1, 100] => fail
  setDomains(-50, -20, -50, -1, +1, +100);
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
}//PropagatePPN_NNP

OPTILAB_TEST_F(PropagatorPlusTest, Propagate)
{
  using namespace Core;

  // [20, 50] + [2, 4] = [1, 100] => D2 = [22 54]
  setDomains(+20, +50, +2, +4, +1, +100);
  runPropagationAndCheck(+20, +50, +2, +4, +22, +54, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-1, 50] + [2, 4] = [2, 5] => D0 = [-1, 3]
  setDomains(-1, +50, +2, +4, +2, +5);
  runPropagationAndCheck(-1, 3, 2, +4, +2, +5, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-1, 50] + [-2, 4] = [2, 5] => D0 = [-1, 7]
  setDomains(-1, +50, -2, +4, +2, +5);
  runPropagationAndCheck(-1, 7, -2, +4, +2, +5, PropagationEvent::PROP_EVENT_FIXPOINT);
}//Propagate

OPTILAB_TEST_F(PropagatorPlusTest, PropagateDomain)
{
  using namespace Core;
  
  // [20, 50] + [2, 4] = [1, 100] => D2 = [22 54]
  setDomains({1, 3}, {+2, +4}, {1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
  runPropagationAndCheckDomains({1, 3}, {+2, +4}, {3, 5, 7}, PropagationEvent::PROP_EVENT_FIXPOINT, USE_DOMAIN_PROP);
}//PropagateDomain

OPTILAB_TEST_F(PropagatorPlusTest, getFitness)
{
  // Test fitness function
  using namespace Core;
  
  setDomains(+5, +5, +2, +2, +7, +7);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());

  setDomains(+0, +0, +2, +2, +2, +2);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());

  setDomains(-5, -5, +2, +2, +2, +2);
  EXPECT_EQ(5, getFitness());

  setDomains(+5, +5, -2, -2, +2, +2);
  EXPECT_EQ(1, getFitness());

  setDomains(+2, +2, +2, +2, +1, +1);
  EXPECT_EQ(3, getFitness());

  setDomains(+9, +9, +2, +2, -9, -9);
  EXPECT_EQ(20, getFitness());

  setDomains(-4, -4, -2, -2, -2, -2);
  EXPECT_EQ(4, getFitness());
}//getFitness
