
#include "utest_globals.hpp"

#include "DomainDefs.hpp"
#include "PropagatorAbs.hpp"
#include "ConstraintTestInc.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"

#include <vector>
#include <memory>

namespace {
  class PropagatorAbsTest : public ::testing::Test {
  public:
    PropagatorAbsTest()
    {
    }
    
    ~PropagatorAbsTest()
    {
    }
    
    void setDomains(INT_64 aD0LB, INT_64 aD0UB,
                    INT_64 aD1LB, INT_64 aD1UB)
    {
      pDom0 = getDomainInt(aD0LB, aD0UB);
      pDom1 = getDomainInt(aD1LB, aD1UB);
    }//setDomains
    
    void setDomains(std::vector<INT_64>&& aD0,
                    std::vector<INT_64>&& aD1)
    {
      pDom0 = getDomainInt(aD0);
      pDom1 = getDomainInt(aD1);
    }//setDomains
    
    double getFitness()
    {
      std::unique_ptr<Core::PropagatorAbs> propagator(nullptr);
      propagator.reset(new Core::PropagatorAbsBound(pDom0.get(),
                                                     pDom1.get()));
      return propagator->getFitness();
    }//getFitness
    
    void postPropagator(INT_64 aD0LB, INT_64 aD0UB,
                        INT_64 aD1LB, INT_64 aD1UB,
                        Core::PropagationEvent aEvent)
    {
      std::unique_ptr<Core::PropagatorAbsBound> propagator(nullptr);
      propagator.reset(new Core::PropagatorAbsBound(pDom0.get(), pDom1.get()));
      
      Core::PropagationEvent event = propagator->post();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        ASSERT_TRUE(pDom0->lowerBound()->isEqual(getElem(aD0LB).get()));
        ASSERT_TRUE(pDom0->upperBound()->isEqual(getElem(aD0UB).get()));
        
        ASSERT_TRUE(pDom1->lowerBound()->isEqual(getElem(aD1LB).get()));
        ASSERT_TRUE(pDom1->upperBound()->isEqual(getElem(aD1UB).get()));
      }
    }//postPropagator
    
    void runPropagationAndCheck(INT_64 aD0LB, INT_64 aD0UB,
                                INT_64 aD1LB, INT_64 aD1UB,
                                Core::PropagationEvent aEvent,
                                bool aUseBound=USE_BOUND_PROP)
    {
      // Perform propagation
      std::unique_ptr<Core::PropagatorAbs> propagator(nullptr);
      if(aUseBound == USE_BOUND_PROP)
      {
        propagator.reset(new Core::PropagatorAbsBound(pDom0.get(), pDom1.get()));
      }
      else
      {
        propagator.reset(new Core::PropagatorAbsDomain(pDom0.get(), pDom1.get()));
      }
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        ASSERT_TRUE(pDom0->lowerBound()->isEqual(getElem(aD0LB).get()));
        ASSERT_TRUE(pDom0->upperBound()->isEqual(getElem(aD0UB).get()));
      
        ASSERT_TRUE(pDom1->lowerBound()->isEqual(getElem(aD1LB).get()));
        ASSERT_TRUE(pDom1->upperBound()->isEqual(getElem(aD1UB).get()));
      }
    }//runPropagationAndCheck
    
    void runPropagationAndCheckDomains(std::vector<INT_64>&& aD0,
                                       std::vector<INT_64>&& aD1,
                                       Core::PropagationEvent aEvent,
                                       bool aUseBound=USE_DOMAIN_PROP)
    {
      // Perform propagation
      std::unique_ptr<Core::PropagatorAbs> propagator(nullptr);
      if(aUseBound == USE_BOUND_PROP)
      {
        propagator.reset(new Core::PropagatorAbsBound(pDom0.get(), pDom1.get()));
      }
      else
      {
        propagator.reset(new Core::PropagatorAbsDomain(pDom0.get(), pDom1.get()));
      }
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        for(auto& elem : aD0)
        {
          ASSERT_TRUE(pDom0->contains(getElem(elem).get()));
        }
        ASSERT_EQ(pDom0->getSize(), aD0.size());
        
        for(auto& elem : aD1)
        {
          ASSERT_TRUE(pDom1->contains(getElem(elem).get()));
        }
        ASSERT_EQ(pDom1->getSize(), aD1.size());
      }
    }//runPropagationAndCheckDomains
    
    virtual void SetUp()
    {
    }
    
    virtual void TearDown()
    {
    }
    
  protected:
    // Domain instances
    DomainIntPtr pDom0;
    DomainIntPtr pDom1;
    DomainIntPtr pDom2;
  };
}// end namespace


/*********************** Test ***********************/

OPTILAB_TEST_F(PropagatorAbsTest, PropagateSingletonEdges)
{
  using namespace Core;
  
  // +INF == 2 => fail
  setDomains(Limits::int64Max(), Limits::int64Max(), +2, +2);
  runPropagationAndCheck(0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // -INF == 2 => fail
  setDomains(Limits::int64Min(), Limits::int64Min(), +2, +2);
  runPropagationAndCheck(0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);

  // 2 == +INF => fail
  setDomains(+2, +2, Limits::int64Max(), Limits::int64Max());
  runPropagationAndCheck(0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // 2 == -INF => fail
  setDomains(+2, +2, Limits::int64Min(), Limits::int64Min());
  runPropagationAndCheck(0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // -INF == +INF
  setDomains(Limits::int64Min(), Limits::int64Min(), Limits::int64Min(), Limits::int64Max());
  runPropagationAndCheck(Limits::int64Min(), Limits::int64Min(),
                         Limits::int64Max(), Limits::int64Max(), PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // +INF == +INF
  setDomains(Limits::int64Max(), Limits::int64Max(), Limits::int64Min(), Limits::int64Max());
  runPropagationAndCheck(Limits::int64Max(), Limits::int64Max(),
                         Limits::int64Max(), Limits::int64Max(), PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // +INF == -2
  // no check since the propagator needs to be posted before propagation
}//PropagateSingletonEdges

OPTILAB_TEST_F(PropagatorAbsTest, PropagateSingleton1)
{
  using namespace Core;
  
  // 5 == 2
  setDomains(+5, +5, +2, +2);
  runPropagationAndCheck(0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // 5 == 5
  setDomains(+5, +5, +5, +5);
  runPropagationAndCheck(+5, +5, +5, +5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -5 == 5
  setDomains(-5, -5, +5, +5);
  runPropagationAndCheck(-5, -5, +5, +5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -5 == 5
  setDomains(-5, -5, -5, +5);
  runPropagationAndCheck(-5, -5, +5, +5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -5 == [-5, +4] => fail
  setDomains(-5, -5, -5, +4);
  runPropagationAndCheck(0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // -5 == [+4, +4] => fail
  setDomains(-5, -5, +4, +4);
  runPropagationAndCheck(0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // -5 == [+5, +7] => [5, 5]
  setDomains(-5, -5, +5, +7);
  runPropagationAndCheck(-5, -5, +5, +5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -5 == [+6, +7] => fail
  setDomains(-5, -5, +6, +7);
  runPropagationAndCheck(0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // -5 == {1, 2, 3, 4, 6} => fail
  setDomains({-5, -5}, {+1, +2, +3, +4, +6});
  runPropagationAndCheck(0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
}//PropagateSingleton1

OPTILAB_TEST_F(PropagatorAbsTest, PropagateSingletonPost1)
{
  using namespace Core;
  
  // 5 == 2
  setDomains(+5, +5, +2, +2);
  postPropagator(+5, +5, +2, +2, PropagationEvent::PROP_EVENT_FAIL);
  
  // 5 == -5
  setDomains(+5, +5, -5, -5);
  postPropagator(+5, +5, +5, +5, PropagationEvent::PROP_EVENT_FAIL);
  
  // 5 == 5
  setDomains(+5, +5, +5, +5);
  postPropagator(+5, +5, +5, +5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -5 == 2
  setDomains(-5, -5, +2, +2);
  postPropagator(+5, +5, +2, +2, PropagationEvent::PROP_EVENT_FAIL);
  
  // -5 == -5
  setDomains(-5, -5, -5, -5);
  postPropagator(+5, +5, +5, +5, PropagationEvent::PROP_EVENT_FAIL);
  
  // -5 == 5
  setDomains(-5, -5, +5, +5);
  postPropagator(-5, -5, +5, +5, PropagationEvent::PROP_EVENT_SUBSUMED);
}//PropagateSingletonPost1

OPTILAB_TEST_F(PropagatorAbsTest, postSingleton2)
{
  using namespace Core;
  
  // 5 == 2
  setDomains(+5, +5, -2, +2);
  postPropagator(+5, +5, +2, +2, PropagationEvent::PROP_EVENT_FAIL);
  
  // 5 == -5
  setDomains(+5, +5, -4, -1);
  postPropagator(+5, +5, +5, +5, PropagationEvent::PROP_EVENT_FAIL);
  
  // 5 == 5
  setDomains(+5, +5, -5, +5);
  postPropagator(+5, +5, +5, +5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -5 == 2
  setDomains(-5, -5, -2, +2);
  postPropagator(+5, +5, +2, +2, PropagationEvent::PROP_EVENT_FAIL);
  
  // -5 == -5
  setDomains(-5, -5, -6, -1);
  postPropagator(+5, +5, +5, +5, PropagationEvent::PROP_EVENT_FAIL);
  
  // -5 == 5
  setDomains(-5, -5, -10, +10);
  postPropagator(-5, -5, +5, +5, PropagationEvent::PROP_EVENT_SUBSUMED);
}//postSingleton2

OPTILAB_TEST_F(PropagatorAbsTest, postGeneral)
{
  using namespace Core;
  
  setDomains(-4, +5, +2, +2);
  postPropagator(-2, +2, +2, +2, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // Post does not modify the domain of D0
  setDomains(-4, +5, -2, +2);
  postPropagator(-4, +5, +0, +2, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // Post does modify the domain of D1
  setDomains(-4, +3, -10, +10);
  postPropagator(-4, +3, +0, +4, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
}//postGeneral

OPTILAB_TEST_F(PropagatorAbsTest, Propagate)
{
  using namespace Core;
  
  // Propagates only on upper bound of D1 -> call post first
  setDomains(-4, +3, -10, +10);
  runPropagationAndCheck(-4, +3, -10, +4, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  setDomains(-4, +3, -3, +2);
  runPropagationAndCheck(-2, +2, -3, +2, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  setDomains(-4, -1, -3, +2);
  runPropagationAndCheck(-2, -1, -3, +2, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // |[-4, -2]| = [-3, +2] =>
  //                          [-4, -2] = [0, +2] =>
  //                          [-2, -2] = [0, +2] =>
  //                          [-2, -2] = [+2, +2]
  setDomains(-4, -2, -3, +2);
  runPropagationAndCheck(-2, -2, +2, +2, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  setDomains(-4, -2, -3, -1);
  runPropagationAndCheck(-2, -2, +2, +2, PropagationEvent::PROP_EVENT_FAIL);
}//Propagate

OPTILAB_TEST_F(PropagatorAbsTest, getFitness)
{
  // Test fitness function
  using namespace Core;
  
  setDomains(+5, +5, +2, +2);
  EXPECT_EQ(3, getFitness());

  setDomains(+2, +2, +5, +5);
  EXPECT_EQ(3, getFitness());
  
  setDomains(+5, +5, +5, +5);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());
  
  setDomains(+5, +6, +5, +6);
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness());

  setDomains(-5, -5, +2, +2);
  EXPECT_EQ(3, getFitness());

  setDomains(-2, -2, +5, +5);
  EXPECT_EQ(3, getFitness());
  
  setDomains(+5, +5, -5, -5);
  EXPECT_EQ(10, getFitness());
  
  setDomains(-6, -5, +5, +6);
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness());
}//getFitness
