
#include "utest_globals.hpp"

#include "DomainDefs.hpp"
#include "PropagatorTimes.hpp"
#include "ConstraintTestInc.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"

#include <vector>
#include <memory>

namespace Core {
  
  class PropagatorTimesBoundTest : public PropagatorTimesBound {
  public:
    PropagatorTimesBoundTest(Domain *aDom0, Domain *aDom1, Domain *aDom2)
    : PropagatorTimesBound(aDom0, aDom1, aDom2)
    {
    }
    
    ~PropagatorTimesBoundTest()
    {
    }
    
    /// Expose method for testing
    PropagationEvent runPropagatorTimesOnPositiveDomains(Domain* aDom0, Domain* aDom1, Domain* aDom2)
    {
      return PropagatorTimesBound::runPropagatorTimesOnPositiveDomains(aDom0, aDom1, aDom2);
    }
  };
  
  class PropagatorTimesDomainTest : public PropagatorTimesDomain {
  public:
    PropagatorTimesDomainTest(Domain *aDom0, Domain *aDom1, Domain *aDom2)
    : PropagatorTimesDomain(aDom0, aDom1, aDom2)
    {
    }
    
    ~PropagatorTimesDomainTest()
    {
    }
    
    /// Expose method for testing
    TernaryDomainSignConfig getTernaryDomainSignConfig() const
    {
      return PropagatorTimesDomain::getTernaryDomainSignConfig();
    }
    
    /// Expose method for testing
    PropagationEvent runPropagatorTimesOnPositiveDomains(Domain* aDom0, Domain* aDom1, Domain* aDom2)
    {
      return PropagatorTimesDomain::runPropagatorTimesOnPositiveDomains(aDom0, aDom1, aDom2);
    }
  };
  
}// end namespace Core

namespace {
  class PropagatorTimesTest : public ::testing::Test {
  public:
    PropagatorTimesTest()
    {
    }
    
    ~PropagatorTimesTest()
    {
    }
    
    void setDomains(INT_64 aD0LB, INT_64 aD0UB,
                    INT_64 aD1LB, INT_64 aD1UB,
                    INT_64 aD2LB, INT_64 aD2UB)
    {
      pDom0 = getDomainInt(aD0LB, aD0UB);
      pDom1 = getDomainInt(aD1LB, aD1UB);
      pDom2 = getDomainInt(aD2LB, aD2UB);
    }//setDomains
    
    void setDomains(std::vector<INT_64>&& aD0,
                    std::vector<INT_64>&& aD1,
                    std::vector<INT_64>&& aD2)
    {
      pDom0 = getDomainInt(aD0);
      pDom1 = getDomainInt(aD1);
      pDom2 = getDomainInt(aD2);
    }//setDomains
    
    double getFitness()
    {
      std::unique_ptr<Core::PropagatorTimes> propagator(nullptr);
      propagator.reset(new Core::PropagatorTimesBound(pDom0.get(),
                                                     pDom1.get(),
                                                     pDom2.get()));
      return propagator->getFitness();
    }//getFitness
    
    void postPropagator(INT_64 aD0LB, INT_64 aD0UB,
                        INT_64 aD1LB, INT_64 aD1UB,
                        INT_64 aD2LB, INT_64 aD2UB,
                        Core::PropagationEvent aEvent)
    {
      std::unique_ptr<Core::PropagatorTimes> propagator(nullptr);
      propagator.reset(new Core::PropagatorTimesBoundTest(pDom0.get(), pDom1.get(), pDom2.get()));
      
      Core::PropagationEvent event = propagator->post();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        ASSERT_TRUE(pDom0->lowerBound()->isEqual(getElem(aD0LB).get()));
        ASSERT_TRUE(pDom0->upperBound()->isEqual(getElem(aD0UB).get()));
        
        ASSERT_TRUE(pDom1->lowerBound()->isEqual(getElem(aD1LB).get()));
        ASSERT_TRUE(pDom1->upperBound()->isEqual(getElem(aD1UB).get()));
        
        ASSERT_TRUE(pDom2->lowerBound()->isEqual(getElem(aD2LB).get()));
        ASSERT_TRUE(pDom2->upperBound()->isEqual(getElem(aD2UB).get()));
      }
    }//postPropagator
    
    void runPropagationAndCheck(INT_64 aD0LB, INT_64 aD0UB,
                                INT_64 aD1LB, INT_64 aD1UB,
                                INT_64 aD2LB, INT_64 aD2UB,
                                Core::PropagationEvent aEvent,
                                bool aUseBound=USE_BOUND_PROP)
    {
      // Perform propagation
      std::unique_ptr<Core::PropagatorTimes> propagator(nullptr);
      if(aUseBound == USE_BOUND_PROP)
      {
        propagator.reset(new Core::PropagatorTimesBoundTest(pDom0.get(), pDom1.get(), pDom2.get()));
      }
      else
      {
        propagator.reset(new Core::PropagatorTimesDomainTest(pDom0.get(), pDom1.get(), pDom2.get()));
      }
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        ASSERT_TRUE(pDom0->lowerBound()->isEqual(getElem(aD0LB).get()));
        ASSERT_TRUE(pDom0->upperBound()->isEqual(getElem(aD0UB).get()));
      
        ASSERT_TRUE(pDom1->lowerBound()->isEqual(getElem(aD1LB).get()));
        ASSERT_TRUE(pDom1->upperBound()->isEqual(getElem(aD1UB).get()));
      
        ASSERT_TRUE(pDom2->lowerBound()->isEqual(getElem(aD2LB).get()));
        ASSERT_TRUE(pDom2->upperBound()->isEqual(getElem(aD2UB).get()));
      }
    }//runPropagationAndCheck
    
    void runPropagationAndCheckDomains(std::vector<INT_64>&& aD0,
                                       std::vector<INT_64>&& aD1,
                                       std::vector<INT_64>&& aD2,
                                       Core::PropagationEvent aEvent,
                                       bool aUseBound=USE_DOMAIN_PROP)
    {
      // Perform propagation
      std::unique_ptr<Core::PropagatorTimes> propagator(nullptr);
      if(aUseBound == USE_BOUND_PROP)
      {
        propagator.reset(new Core::PropagatorTimesBoundTest(pDom0.get(), pDom1.get(), pDom2.get()));
      }
      else
      {
        propagator.reset(new Core::PropagatorTimesDomainTest(pDom0.get(), pDom1.get(), pDom2.get()));
      }
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        for(auto& elem : aD0)
        {
          ASSERT_TRUE(pDom0->contains(getElem(elem).get()));
        }
        ASSERT_EQ(pDom0->getSize(), aD0.size());
        
        for(auto& elem : aD1)
        {
          ASSERT_TRUE(pDom1->contains(getElem(elem).get()));
        }
        ASSERT_EQ(pDom1->getSize(), aD1.size());
        
        for(auto& elem : aD2)
        {
          ASSERT_TRUE(pDom2->contains(getElem(elem).get()));
        }
        ASSERT_EQ(pDom2->getSize(), aD2.size());
      }
    }//runPropagationAndCheckDomains
    
    virtual void SetUp()
    {
    }
    
    virtual void TearDown()
    {
    }
    
  protected:
    // Domain instances
    DomainIntPtr pDom0;
    DomainIntPtr pDom1;
    DomainIntPtr pDom2;
  };
}// end namespace


/*********************** Test ***********************/

OPTILAB_TEST_F(PropagatorTimesTest, PropagateSingletonEdges)
{
  using namespace Core;
  
  // INF * 2 = [-INF, INF] => INF should saturate on INF
  setDomains(Limits::int64Max(), Limits::int64Max(), +2, +2, Limits::int64Min(), Limits::int64Max());
  runPropagationAndCheck(Limits::int64Max(), Limits::int64Max(),
                         +2, +2,
                         Limits::int64Max(), Limits::int64Max(),
                         PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -INF * 2 = [-INF, INF] => -INF should saturate on -INF
  setDomains(Limits::int64Min(), Limits::int64Min(), +2, +2, Limits::int64Min(), Limits::int64Max());
  runPropagationAndCheck(Limits::int64Min(), Limits::int64Min(),
                         +2, +2,
                         Limits::int64Min(), Limits::int64Min(),
                         PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 2 * INF = [-INF, INF] => INF should saturate on INF
  setDomains(+2, +2, Limits::int64Max(), Limits::int64Max(), Limits::int64Min(), Limits::int64Max());
  runPropagationAndCheck(+2, +2,
                         Limits::int64Max(), Limits::int64Max(),
                         Limits::int64Max(), Limits::int64Max(),
                         PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 2 * -INF = [-INF, INF] => -INF should saturate on -INF
  setDomains(+2, +2, Limits::int64Min(), Limits::int64Min(), Limits::int64Min(), Limits::int64Max());
  runPropagationAndCheck(+2, +2,
                         Limits::int64Min(), Limits::int64Min(),
                         Limits::int64Min(), Limits::int64Min(),
                         PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -INF * -INF = [-INF, INF] => +INF should saturate on -INF
  setDomains(Limits::int64Min(), Limits::int64Min(),
             Limits::int64Min(), Limits::int64Min(),
             Limits::int64Min(), Limits::int64Max());
  runPropagationAndCheck(Limits::int64Min(), Limits::int64Min(),
                         Limits::int64Min(), Limits::int64Min(),
                         Limits::int64Max(), Limits::int64Max(),
                         PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -INF * +INF = [-INF, INF] => -INF should saturate on -INF
  setDomains(Limits::int64Min(), Limits::int64Min(),
             Limits::int64Max(), Limits::int64Max(),
             Limits::int64Min(), Limits::int64Max());
  runPropagationAndCheck(Limits::int64Min(), Limits::int64Min(),
                         Limits::int64Max(), Limits::int64Max(),
                         Limits::int64Min(), Limits::int64Min(),
                         PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [-INF, INF] * 2 = INF => INF/2
  setDomains(Limits::int64Min(), Limits::int64Max(), +2, +2, Limits::int64Max(), Limits::int64Max());
  runPropagationAndCheck(Limits::int64Max()/2, Limits::int64Max()/2,
                         +2, +2,
                         Limits::int64Max(), Limits::int64Max(),
                         PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [-INF, INF] * -2 = INF => -INF/2
  setDomains(Limits::int64Min(), Limits::int64Max(), -2, -2, Limits::int64Max(), Limits::int64Max());
  runPropagationAndCheck(Limits::int64Min()/2, Limits::int64Min()/2,
                         -2, -2,
                         Limits::int64Max(), Limits::int64Max(),
                         PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 2 * [-INF, INF] = INF => INF/2
  setDomains(+2, +2, Limits::int64Min(), Limits::int64Max(), Limits::int64Max(), Limits::int64Max());
  runPropagationAndCheck(+2, +2,
                         Limits::int64Max()/2, Limits::int64Max()/2,
                         Limits::int64Max(), Limits::int64Max(),
                         PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -2 * [-INF, INF] = INF => -INF/2
  setDomains(-2, -2, Limits::int64Min(), Limits::int64Max(), Limits::int64Max(), Limits::int64Max());
  runPropagationAndCheck(-2, -2,
                         Limits::int64Min()/2, Limits::int64Min()/2,
                         Limits::int64Max(), Limits::int64Max(),
                         PropagationEvent::PROP_EVENT_SUBSUMED);
}//PropagateSingletonEdges

OPTILAB_TEST_F(PropagatorTimesTest, PropagateSingleton1)
{
  using namespace Core;
  
  // 5 * 2 = 10
  setDomains(+5, +5, +2, +2, +10, +10);
  runPropagationAndCheck(+5, +5, +2, +2, +10, +10, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 5 * -2 = -10
  setDomains(+5, +5, -2, -2, -10, -10);
  runPropagationAndCheck(+5, +5, -2, -2, -10, -10, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -500 * -200 = 100000
  setDomains(-500, -500, -200, -200, +100000, +100000);
  runPropagationAndCheck(-500, -500, -200, -200, +100000, +100000, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -5 * -2 != 20
  setDomains(-5, -5, -2, -2, +20, +20);
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
}//PropagateSingleton1

OPTILAB_TEST_F(PropagatorTimesTest, PropagateSingletonPost1)
{
  using namespace Core;
  
  // 5 * 2 = 10
  setDomains(+5, +5, +2, +2, +10, +10);
  postPropagator(+5, +5, +2, +2, +10, +10, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 5 * -2 = -10
  setDomains(+5, +5, -2, -2, -10, -10);
  postPropagator(+5, +5, -2, -2, -10, -10, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -500 * -200 = 100000
  setDomains(-500, -500, -200, -200, +100000, +100000);
  postPropagator(-500, -500, -200, -200, +100000, +100000, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -5 * -2 != 20
  setDomains(-5, -5, -2, -2, +20, +20);
  postPropagator(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
}//PropagateSingletonPost1

OPTILAB_TEST_F(PropagatorTimesTest, PropagateSingleton2)
{
  using namespace Core;
  
  // 5 * 2 = [-10, 10] => 10
  setDomains(+5, +5, +2, +2, -10, +10);
  runPropagationAndCheck(+5, +5, +2, +2, +10, +10, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 5 * -2 = [-10, 10] => -10
  setDomains(+5, +5, -2, -2, -10, +10);
  runPropagationAndCheck(+5, +5, -2, -2, -10, -10, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -5 * -2 = [-10, 10] => 10
  setDomains(-5, -5, -2, -2, -10, +10);
  runPropagationAndCheck(-5, -5, -2, -2, +10, +10, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 5 * [-10, 10] = 10 => 2
  setDomains(+5, +5, -10, +10, +10, +10);
  runPropagationAndCheck(+5, +5, +2, +2, +10, +10, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 5 * [-10, 10] = -10 => -2
  setDomains(+5, +5, -10, +10, -10, -10);
  runPropagationAndCheck(+5, +5, -2, -2, -10, -10, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -5 * [-10, 10] = 10 => -2
  setDomains(-5, -5, -10, +10, +10, +10);
  runPropagationAndCheck(-5, -5, -2, -2, +10, +10, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [-10, 10] * 2 = 10 => 5
  setDomains(-10, +10, +2, +2, +10, +10);
  runPropagationAndCheck(+5, +5, +2, +2, +10, +10, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [-10, 10] * 2 = -10 => 5
  setDomains(-10, +10, -2, -2, -10, -10);
  runPropagationAndCheck(+5, +5, -2, -2, -10, -10, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [-10, 10] * 2 = 10 => -5
  setDomains(-10, +10, -2, -2, +10, +10);
  runPropagationAndCheck(-5, -5, -2, -2, +10, +10, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 5 * 2 = [20, 30] => FAIL
  setDomains(+5, +5, +2, +2, 20, 30);
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // 5 * [20, 30] = 10 => FAIL
  setDomains(+5, +5, +20, +30, 10, 10);
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // [20, 30] * 2 = 10 => FAIL
  setDomains(+20, +30, +2, +2, 10, 10);
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
}//PropagateSingleton2

OPTILAB_TEST_F(PropagatorTimesTest, PropagateSingletonPost2)
{
  using namespace Core;
  
  // 5 * 2 = [-10, 10] => 10
  setDomains(+5, +5, +2, +2, -10, +10);
  postPropagator(+5, +5, +2, +2, +10, +10, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 5 * -2 = [-10, 10] => -10
  setDomains(+5, +5, -2, -2, -10, +10);
  postPropagator(+5, +5, -2, -2, -10, -10, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -5 * -2 = [-10, 10] => 10
  setDomains(-5, -5, -2, -2, -10, +10);
  postPropagator(-5, -5, -2, -2, +10, +10, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 5 * [-10, 10] = 10 => 2
  setDomains(+5, +5, -10, +10, +10, +10);
  postPropagator(+5, +5, +2, +2, +10, +10, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 5 * [-10, 10] = -10 => -2
  setDomains(+5, +5, -10, +10, -10, -10);
  postPropagator(+5, +5, -2, -2, -10, -10, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -5 * [-10, 10] = 10 => -2
  setDomains(-5, -5, -10, +10, +10, +10);
  postPropagator(-5, -5, -2, -2, +10, +10, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [-10, 10] * 2 = 10 => 5
  setDomains(-10, +10, +2, +2, +10, +10);
  postPropagator(+5, +5, +2, +2, +10, +10, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [-10, 10] * 2 = -10 => 5
  setDomains(-10, +10, -2, -2, -10, -10);
  postPropagator(+5, +5, -2, -2, -10, -10, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [-10, 10] * 2 = 10 => -5
  setDomains(-10, +10, -2, -2, +10, +10);
  postPropagator(-5, -5, -2, -2, +10, +10, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 5 * 2 = [20, 30] => FAIL
  setDomains(+5, +5, +2, +2, 20, 30);
  postPropagator(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // 5 * [20, 30] = 10 => FAIL
  setDomains(+5, +5, +20, +30, 10, 10);
  postPropagator(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // [20, 30] * 2 = 10 => FAIL
  setDomains(+20, +30, +2, +2, 10, 10);
  postPropagator(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
}//PropagateSingletonPost2


OPTILAB_TEST_F(PropagatorTimesTest, PropagatePPP)
{
  using namespace Core;
  
  // [5, 10] * [2, 4] = [1, 50] => D2 = [10, 40]
  setDomains(+5, +10, +2, +4, 1, +50);
  runPropagationAndCheck(+5, +10, +2, +4, +10, +40, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [5, 10] * [1, 50] = [10, 40] => D1 = [1, 8]
  setDomains(+5, +10, 1, +50, +10, +40);
  runPropagationAndCheck(+5, +10, +1, +8, +10, +40, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [1, 50] * [2, 4] = [10, 40] => D0 = [3, 20]
  setDomains(1, +50, +2, +4, +10, +40);
  runPropagationAndCheck(+3, +20, +2, +4, +10, +40, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [1, 3] * [1, 3] = [1, +10] => D2 = [1, 9]
  setDomains({1,2,3}, {1,2,3}, {1,2,3,4,5,6,7,8,9,10});
  runPropagationAndCheck(+1, +3, +1, +3, +1, +9, PropagationEvent::PROP_EVENT_FIXPOINT);
}//PropagatePPP

OPTILAB_TEST_F(PropagatorTimesTest, PostPPP)
{
  using namespace Core;
  
  // @note post operates only on D2
  
  // [5, 10] * [2, 4] = [1, 50] => D2 = [10, 40]
  setDomains(+5, +10, +2, +4, 1, +50);
  postPropagator(+5, +10, +2, +4, +10, +40, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // [5, 10] * [1, 50] = [10, 40]
  setDomains(+5, +10, 1, +50, +10, +40);
  postPropagator(+5, +10, +1, +50, +10, +40, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // [1, 50] * [2, 4] = [10, 40]
  setDomains(1, +50, +2, +4, +10, +40);
  postPropagator(+1, +50, +2, +4, +10, +40, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // [1, 3] * [1, 3] = [1, +10] => D2 = [1, 9]
  setDomains({1,2,3}, {1,2,3}, {1,2,3,4,5,6,7,8,9,10});
  postPropagator(+1, +3, +1, +3, +1, +9, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
}//PostPPP

OPTILAB_TEST_F(PropagatorTimesTest, PostPNN)
{
  using namespace Core;
  
  // @note post operates only on D2
  
  // [5, 10] * [-4, -2] = [-50, -1] => D2 = [-40, -10]
  setDomains(+5, +10, -4, -2, -50, -1);
  postPropagator(+5, +10, -4, -2, -40, -10, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
}//PostPNN

OPTILAB_TEST_F(PropagatorTimesTest, PropagatePPN_PNP_NPP_NNN)
{
  using namespace Core;
  
  // [5, 10] * [2, 4] = [-50, -10] => D2 = []
  setDomains(+5, +10, +2, +4, -50, -10);
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // [1, 3] * [1, 3] = [-3, -1] => D2 = []
  setDomains({1,2,3}, {1,2,3}, {-3, -2, -1});
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // [5, 10] * [-10, -4] = [1, 10] => D1 = []
  setDomains(+5, +10, -10, -4, +1, +10);
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // [1, 3] * [-3, -1] = [1, 3] => D1 = []
  setDomains({1,2,3}, {-3,-2,-1}, {1, 2, 3});
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // [-5, -1] * [4, 10] = [1, 50] => D2 = []
  setDomains(-5, -1, +4, +10, +1, +50);
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // [-3, -1] * [1, 3] = [1, 3] => D2 = []
  setDomains({-1,-2,-3}, {3,2,1}, {1, 2, 3});
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // [-5, -1] * [-10, -4] = [-100, -1] => D2 = []
  setDomains(-5, -1, -10, -4, -100, -1);
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // [-2, -1] * [-2, -1] = [-5, -1] => D2 = []
  setDomains({-1,-2}, {-2, -1}, {-1, -2, -3, -4, -5});
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
}//PropagatePPN_PNP_NPP_NNN

OPTILAB_TEST_F(PropagatorTimesTest, PropagatePPX_PXP_XPP)
{
  using namespace Core;
  
  // [5, 10] * [2, 4] = [-50, 50] => D2 = [10, 40]
  setDomains(+5, +10, +2, +4, -50, +50);
  runPropagationAndCheck(+5, +10, +2, +4, +10, +40, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [1, 3] * [1, 3] = [-1, +10] => D2 = [1, 9]
  setDomains({1,2,3}, {1,2,3}, {-1,0,1,2,3,4,5,6,7,8,9,10});
  runPropagationAndCheck(+1, +3, +1, +3, +1, +9, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [5, 10] * [-50, 50] = [10, 40] => D1 = [1, 8]
  setDomains(+5, +10, -50, +50, +10, +40);
  runPropagationAndCheck(+5, +10, +1, +8, +10, +40, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-50, 50] * [2, 4] = [10, 40] => D0 = [3, 20]
  setDomains(-50, +50, +2, +4, +10, +40);
  runPropagationAndCheck(+3, +20, +2, +4, +10, +40, PropagationEvent::PROP_EVENT_FIXPOINT);
}//PropagatePPX_PXP_XPP

OPTILAB_TEST_F(PropagatorTimesTest, PropagatePNN_PNX_PXN)
{
  using namespace Core;
  
  // [5, 10] * [-4, -2] = [-50, -1] => D2 = [-40, -10]
  setDomains(+5, +10, -4, -2, -50, -1);
  runPropagationAndCheck(+5, +10, -4, -2, -40, -10, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [5, 10] * [-4, -2] = [-50, 50] => D2 = [-40, -10]
  setDomains(+5, +10, -4, -2, -50, +50);
  runPropagationAndCheck(+5, +10, -4, -2, -40, -10, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [5, 10] * [-4, 4] = [-50, -1] => [-4, -1], D2 = [-40, -5]
  setDomains(+5, +10, -4, +4, -50, -1);
  runPropagationAndCheck(+5, +10, -4, -1, -40, -5, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [1, 3] * [-3, -1] = [-10, -1] => D2 = [-9, -1]
  setDomains({1,2,3}, {-1,-2,-3}, {-1,-2,-3,-4,-5,-6,-7,-8,-9,-10});
  runPropagationAndCheck(+1, +3, -3, -1, -9, -1, PropagationEvent::PROP_EVENT_FIXPOINT);
}//PropagatePNN_PNX_PXN

OPTILAB_TEST_F(PropagatorTimesTest, PropagatePXX)
{
  using namespace Core;
  
  // [5, 10] * [-4, +4] = [-40, +40] => D2 = [-40, -10]
  setDomains(+5, +10, -4, +4, -50, 50);
  runPropagationAndCheck(+5, +10, -4, +4, -40, +40, PropagationEvent::PROP_EVENT_NO_FIXPOINT);
}//PropagatePXX

OPTILAB_TEST_F(PropagatorTimesTest, PropagateNPN_NPX)
{
  using namespace Core;
  
  // [-10, -5] * [1, 4] = [-100, -1] => D2 = [-40, -5]
  setDomains(-10, -5, +1, +4, -100, -1);
  runPropagationAndCheck(-10, -5, +1, +4, -40, -5, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-10, -5] * [1, 4] = [-100, +100] => D2 = [-40, -5]
  setDomains(-10, -5, +1, +4, -100, +100);
  runPropagationAndCheck(-10, -5, +1, +4, -40, -5, PropagationEvent::PROP_EVENT_FIXPOINT);
}//PropagateNPN_NPX

OPTILAB_TEST_F(PropagatorTimesTest, PropagateNNP_NNX)
{
  using namespace Core;
  
  // [-10, -5] * [-4, -1] = [+1, +100] => D2 = [+5, +40]
  setDomains(-10, -5, -4, -1, +1, +100);
  runPropagationAndCheck(-10, -5, -4, -1, +5, +40, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-3, -1] * [-3, -1] = [+1, +10] => D2 = [+1, +9]
  setDomains({-1,-2,-3}, {-1,-2,-3}, {+1,+2,+3,+4,+5,+6,+7,+8,+9,+10});
  runPropagationAndCheck(-3, -1, -3, -1, +1, +9, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-10, -5] * [-4, -1] = [-100, +100] => D2 = [+5, +40]
  setDomains(-10, -5, -4, -1, -100, +100);
  runPropagationAndCheck(-10, -5, -4, -1, +5, +40, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-3, -1] * [-3, -1] = [-1, +10] => D2 = [+1, +9]
  setDomains({-1,-2,-3}, {-1,-2,-3}, {-1, 0, +1,+2,+3,+4,+5,+6,+7,+8,+9,+10});
  runPropagationAndCheck(-3, -1, -3, -1, +1, +9, PropagationEvent::PROP_EVENT_FIXPOINT);
}//PropagateNNP_NNX

OPTILAB_TEST_F(PropagatorTimesTest, PropagateNXP_NXN_NXX)
{
  using namespace Core;
  
  // [-10, -5] * [-4, 4] = [+1, +100] => D1 = [-4, -1] /\ D2 = [+20, +40]
  setDomains(-10, -5, -4, +4, +1, +100);
  runPropagationAndCheck(-10, -5, -4, -1, +5, +40, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-10, -5] * [-4, 4] = [-100, -1] => D1 = [1, 4] /\ D2 = [-40, -5]
  setDomains(-10, -5, -4, +4, -100, -1);
  runPropagationAndCheck(-10, -5, +1, +4, -40, -5, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-10, -5] * [-4, 4] = [-100, +100] => D2 = [-40, +40]
  setDomains(-10, -5, -4, +4, -100, +100);
  runPropagationAndCheck(-10, -5, -4, +4, -40, +40, PropagationEvent::PROP_EVENT_NO_FIXPOINT);
}//PropagateNXP_NXN_NXX

OPTILAB_TEST_F(PropagatorTimesTest, PropagateXPP_XPN_XPX)
{
  using namespace Core;
  
  // [-10, 10] * [1, 4] = [+1, +100] => D0 = [1, 10] /\ D2 = [1, +40]
  setDomains(-10, +10, +1, +4, +1, +100);
  runPropagationAndCheck(+1, +10, +1, +4, +1, +40, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-10, 10] * [1, 4] = [-100, -1] => D0 = [-10, -1] /\ D2 = [-40, -1]
  setDomains(-10, +10, +1, +4, -100, -1);
  runPropagationAndCheck(-10, -1, +1, +4, -40, -1, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-10, 10] * [1, 4] = [-100, +100] => D2 = [-40, +40]
  setDomains(-10, +10, +1, +4, -100, +100);
  runPropagationAndCheck(-10, +10, +1, +4, -40, +40, PropagationEvent::PROP_EVENT_NO_FIXPOINT);
}//PropagateXPP_XPN_XPX

OPTILAB_TEST_F(PropagatorTimesTest, PropagateXNP_XNN_XNX)
{
  using namespace Core;
  
  // [-10, 10] * [-4, -1] = [+1, +100] => D0 = [-10, -1] /\ D2 = [1, +40]
  setDomains(-10, +10, -4, -1, +1, +100);
  runPropagationAndCheck(-10, -1, -4, -1, +1, +40, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-10, 10] * [-4, -1] = [-100, -1] => D0 = [+1, +10] /\ D2 = [-40, -1]
  setDomains(-10, +10, -4, -1, -100, -1);
  runPropagationAndCheck(+1, +10, -4, -1, -40, -1, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-10, 10] * [-4, -1] = [-100, +100] => D2 = [-40, +40]
  setDomains(-10, +10, -4, -1, -100, +100);
  runPropagationAndCheck(-10, +10, -4, -1, -40, +40, PropagationEvent::PROP_EVENT_NO_FIXPOINT);
}//PropagateXNP_XNN_XNX

OPTILAB_TEST_F(PropagatorTimesTest, PropagateXXP_XXN_XXX)
{
  using namespace Core;
  
  // [-10, 10] * [-4, +4] = [+1, +100] => D2 = [+1, +40]
  setDomains(-10, +10, -4, +4, +1, +100);
  runPropagationAndCheck(-10, +10, -4, +4, +1, +40, PropagationEvent::PROP_EVENT_NO_FIXPOINT);
  
  // [-10, 10] * [-4, +4] = [-100, -1] => D2 = [-40, -1]
  setDomains(-10, +10, -4, +4, -100, -1);
  runPropagationAndCheck(-10, +10, -4, +4, -40, -1, PropagationEvent::PROP_EVENT_NO_FIXPOINT);
  
  // [-10, 10] * [-4, +4] = [-100, +100] => D2 = [-40, +40]
  setDomains(-10, +10, -4, +4, -100, +100);
  runPropagationAndCheck(-10, +10, -4, +4, -40, +40, PropagationEvent::PROP_EVENT_NO_FIXPOINT);
}//PropagateXXP_XXN_XXX

OPTILAB_TEST_F(PropagatorTimesTest, PropagateDomainPPP)
{
  using namespace Core;
  
  // [5, 10] * [2, 4] = [1, 50] => D2 = [10, 40]
  setDomains(+5, +10, +2, +4, +1, +50);
  runPropagationAndCheck(+5, +10, +2, +4, +10, +40, PropagationEvent::PROP_EVENT_FIXPOINT, USE_DOMAIN_PROP);
  
  // [5, 10] * [1, 50] = [10, 40] => D1 = [1, 8]
  setDomains(+5, +10, 1, +50, +10, +40);
  runPropagationAndCheck(+5, +10, +1, +8, +10, +40, PropagationEvent::PROP_EVENT_FIXPOINT, USE_DOMAIN_PROP);
  
  // [1, 50] * [2, 4] = [10, 40] => D0 = [3, 20]
  setDomains(1, +50, +2, +4, +10, +40);
  runPropagationAndCheck(+3, +20, +2, +4, +10, +40, PropagationEvent::PROP_EVENT_FIXPOINT, USE_DOMAIN_PROP);
  
  // [1, 3] * [1, 3] = [1, +10] => D2 = [1, 9]
  setDomains({1,3}, {1,3}, {1,2,3,4,5,6,7,8,9,10});
  runPropagationAndCheckDomains({1,3}, {1,3}, {1,3,9}, PropagationEvent::PROP_EVENT_FIXPOINT, USE_DOMAIN_PROP);
  
  // [1, 3] * [1, 4] = [1, 9] => D1 = [1, 3]
  setDomains({1,3}, {1,3,4}, {1,3,9});
  runPropagationAndCheckDomains({1,3}, {1,3}, {1,3,9}, PropagationEvent::PROP_EVENT_FIXPOINT, USE_DOMAIN_PROP);
  
  // [1, 4] * [1, 3] = [1, 9] => D0 = [1, 3]
  setDomains({1,3,4}, {1,3}, {1,3,9});
  runPropagationAndCheckDomains({1,3}, {1,3}, {1,3,9}, PropagationEvent::PROP_EVENT_FIXPOINT, USE_DOMAIN_PROP);
  
  // [1, 4] * [1, 4] = [1, 9] => D0 = [1, 3] /\ D1 = [1, 3]
  setDomains({1,3,4}, {1,3,4}, {1,3,9});
  runPropagationAndCheckDomains({1,3}, {1,3}, {1,3,9}, PropagationEvent::PROP_EVENT_FIXPOINT, USE_DOMAIN_PROP);
}//PropagateDomainPPP

OPTILAB_TEST_F(PropagatorTimesTest, getFitness)
{
  // Test fitness function
  using namespace Core;
  
  setDomains(+5, +5, +2, +2, +10, +10);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());

  setDomains(+0, +0, +2, +2, 0, 0);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());

  setDomains(-5, -5, +2, +2, +2, +2);
  EXPECT_EQ(12, getFitness());

  setDomains(+5, +5, -2, -2, +2, +2);
  EXPECT_EQ(12, getFitness());

  setDomains(+2, +2, +2, +2, +1, +1);
  EXPECT_EQ(3, getFitness());

  setDomains(+9, +9, +2, +2, -9, -9);
  EXPECT_EQ(27, getFitness());

  setDomains(-4, -4, -2, -2, -2, -2);
  EXPECT_EQ(10, getFitness());
}//getFitness
