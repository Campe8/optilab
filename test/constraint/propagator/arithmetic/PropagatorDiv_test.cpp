
#include "utest_globals.hpp"

#include "DomainDefs.hpp"
#include "PropagatorDiv.hpp"
#include "ConstraintTestInc.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"

#include <vector>
#include <memory>

namespace {
  class PropagatorDivTest : public ::testing::Test {
  public:
    PropagatorDivTest()
    {
    }
    
    ~PropagatorDivTest()
    {
    }
    
    void setDomains(INT_64 aD0LB, INT_64 aD0UB,
                    INT_64 aD1LB, INT_64 aD1UB,
                    INT_64 aD2LB, INT_64 aD2UB)
    {
      pDom0 = getDomainInt(aD0LB, aD0UB);
      pDom1 = getDomainInt(aD1LB, aD1UB);
      pDom2 = getDomainInt(aD2LB, aD2UB);
    }//setDomains
    
    void setDomains(std::vector<INT_64>&& aD0,
                    std::vector<INT_64>&& aD1,
                    std::vector<INT_64>&& aD2)
    {
      pDom0 = getDomainInt(aD0);
      pDom1 = getDomainInt(aD1);
      pDom2 = getDomainInt(aD2);
    }//setDomains
    
    double getFitness()
    {
      std::unique_ptr<Core::PropagatorDiv> propagator(nullptr);
      propagator.reset(new Core::PropagatorDiv(pDom0.get(),
                                               pDom1.get(),
                                               pDom2.get()));
      return propagator->getFitness();
    }//getFitness
    
    void postPropagator(INT_64 aD0LB, INT_64 aD0UB,
                        INT_64 aD1LB, INT_64 aD1UB,
                        INT_64 aD2LB, INT_64 aD2UB,
                        Core::PropagationEvent aEvent)
    {
      std::unique_ptr<Core::PropagatorDiv> propagator(nullptr);
      propagator.reset(new Core::PropagatorDiv(pDom0.get(), pDom1.get(), pDom2.get()));
      
      Core::PropagationEvent event = propagator->post();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        ASSERT_TRUE(pDom0->lowerBound()->isEqual(getElem(aD0LB).get()));
        ASSERT_TRUE(pDom0->upperBound()->isEqual(getElem(aD0UB).get()));
        
        ASSERT_TRUE(pDom1->lowerBound()->isEqual(getElem(aD1LB).get()));
        ASSERT_TRUE(pDom1->upperBound()->isEqual(getElem(aD1UB).get()));
        
        ASSERT_TRUE(pDom2->lowerBound()->isEqual(getElem(aD2LB).get()));
        ASSERT_TRUE(pDom2->upperBound()->isEqual(getElem(aD2UB).get()));
      }
    }//postPropagator
    
    void runPropagationAndCheck(INT_64 aD0LB, INT_64 aD0UB,
                                INT_64 aD1LB, INT_64 aD1UB,
                                INT_64 aD2LB, INT_64 aD2UB,
                                Core::PropagationEvent aEvent)
    {
      // Perform propagation
      std::unique_ptr<Core::PropagatorDiv> propagator(nullptr);
      propagator.reset(new Core::PropagatorDiv(pDom0.get(), pDom1.get(), pDom2.get()));
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        ASSERT_TRUE(pDom0->lowerBound()->isEqual(getElem(aD0LB).get()));
        ASSERT_TRUE(pDom0->upperBound()->isEqual(getElem(aD0UB).get()));
      
        ASSERT_TRUE(pDom1->lowerBound()->isEqual(getElem(aD1LB).get()));
        ASSERT_TRUE(pDom1->upperBound()->isEqual(getElem(aD1UB).get()));
      
        ASSERT_TRUE(pDom2->lowerBound()->isEqual(getElem(aD2LB).get()));
        ASSERT_TRUE(pDom2->upperBound()->isEqual(getElem(aD2UB).get()));
      }
    }//runPropagationAndCheck
    
    void runPropagationAndCheckDomains(std::vector<INT_64>&& aD0,
                                       std::vector<INT_64>&& aD1,
                                       std::vector<INT_64>&& aD2,
                                       Core::PropagationEvent aEvent)
    {
      // Perform propagation
      std::unique_ptr<Core::PropagatorDiv> propagator(nullptr);
      propagator.reset(new Core::PropagatorDiv(pDom0.get(), pDom1.get(), pDom2.get()));
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        for(auto& elem : aD0)
        {
          ASSERT_TRUE(pDom0->contains(getElem(elem).get()));
        }
        ASSERT_EQ(pDom0->getSize(), aD0.size());
        
        for(auto& elem : aD1)
        {
          ASSERT_TRUE(pDom1->contains(getElem(elem).get()));
        }
        ASSERT_EQ(pDom1->getSize(), aD1.size());
        
        for(auto& elem : aD2)
        {
          ASSERT_TRUE(pDom2->contains(getElem(elem).get()));
        }
        ASSERT_EQ(pDom2->getSize(), aD2.size());
      }
    }//runPropagationAndCheckDomains
    
    virtual void SetUp()
    {
    }
    
    virtual void TearDown()
    {
    }
    
  protected:
    // Domain instances
    DomainIntPtr pDom0;
    DomainIntPtr pDom1;
    DomainIntPtr pDom2;
  };
}// end namespace


/*********************** Test ***********************/

OPTILAB_TEST_F(PropagatorDivTest, PropagateSingletonEdges)
{
  using namespace Core;
  
  // @note -INF = Limits::int64Min() and +INF = Limits::int64Max()
  
  // INF / 2 = [-INF, +INF] => [INF/2, INF/2]
  setDomains(Limits::int64Max(), Limits::int64Max(), +2, +2, Limits::int64Min(), Limits::int64Max());
  runPropagationAndCheck(Limits::int64Max(), Limits::int64Max(),
                         +2, +2,
                         Limits::int64Max()/2, Limits::int64Max()/2,
                         PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -INF / 2 = [-INF, INF] => [-INF/2, -INF/2]
  setDomains(Limits::int64Min(), Limits::int64Min(), +2, +2, Limits::int64Min(), Limits::int64Max());
  runPropagationAndCheck(Limits::int64Min(), Limits::int64Min(),
                         +2, +2,
                         Limits::int64Min()/2, Limits::int64Min()/2,
                         PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 2 / INF = [-INF, INF] => 0
  setDomains(+2, +2, Limits::int64Max(), Limits::int64Max(), Limits::int64Min(), Limits::int64Max());
  runPropagationAndCheck(+2, +2,
                         Limits::int64Max(), Limits::int64Max(),
                         0, 0,
                         PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 2 / -INF = [-INF, INF] => 0
  setDomains(+2, +2, Limits::int64Min(), Limits::int64Min(), Limits::int64Min(), Limits::int64Max());
  runPropagationAndCheck(+2, +2,
                         Limits::int64Min(), Limits::int64Min(),
                         0, 0,
                         PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -INF / -INF = [-INF, INF] => +1
  setDomains(Limits::int64Min(), Limits::int64Min(),
             Limits::int64Min(), Limits::int64Min(),
             Limits::int64Min(), Limits::int64Max());
  runPropagationAndCheck(Limits::int64Min(), Limits::int64Min(),
                         Limits::int64Min(), Limits::int64Min(),
                         1, 1,
                         PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -INF / +INF = [-INF, INF] => -1
  setDomains(Limits::int64Min(), Limits::int64Min(),
             Limits::int64Max(), Limits::int64Max(),
             Limits::int64Min(), Limits::int64Max());
  runPropagationAndCheck(Limits::int64Min(), Limits::int64Min(),
                         Limits::int64Max(), Limits::int64Max(),
                         -1, -1,
                         PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [-INF, INF] / 2 = [+INF, +INF] => fail
  setDomains(Limits::int64Min(), Limits::int64Max(), +2, +2, Limits::int64Max(), Limits::int64Max());
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // [-INF, INF] / -2 = INF => fail
  setDomains(Limits::int64Min(), Limits::int64Max(), -2, -2, Limits::int64Max(), Limits::int64Max());
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  
  // 2 / [-INF, INF] = INF => fail
  setDomains(+2, +2, Limits::int64Min(), Limits::int64Max(), Limits::int64Max(), Limits::int64Max());
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // -2 / [-INF, INF] = INF => fail
  setDomains(-2, -2, Limits::int64Min(), Limits::int64Max(), Limits::int64Max(), Limits::int64Max());
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
}//PropagateSingletonEdges

OPTILAB_TEST_F(PropagatorDivTest, PropagateSingleton1)
{
  using namespace Core;
  
  // 10 / 2 = 5
  setDomains(+10, +10, +2, +2, +5, +5);
  runPropagationAndCheck(+10, +10, +2, +2, +5, +5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 10 * -2 = -5
  setDomains(+10, +10, -2, -2, -5, -5);
  runPropagationAndCheck(+10, +10, -2, -2, -5, -5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -500 / -200 = 2
  setDomains(-500, -500, -200, -200, +2, +2);
  runPropagationAndCheck(-500, -500, -200, -200, +2, +2, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -5 / -2 != 20
  setDomains(-5, -5, -2, -2, +20, +20);
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
}//PropagateSingleton1

OPTILAB_TEST_F(PropagatorDivTest, PropagateSingletonPost1)
{
  using namespace Core;
  
  // 10 / 2 = 5
  setDomains(+10, +10, +2, +2, 5, 5);
  postPropagator(+10, +10, +2, +2, 5, 5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 10 / -2 = -5
  setDomains(+10, +10, -2, -2, -5, -5);
  postPropagator(10, 10, -2, -2, -5, -5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -500 / -200 = 2
  setDomains(-500, -500, -200, -200, +2, +2);
  postPropagator(-500, -500, -200, -200, +2, +2, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -5 / -2 != 20
  setDomains(-5, -5, -2, -2, +20, +20);
  postPropagator(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
}//PropagateSingletonPost1

OPTILAB_TEST_F(PropagatorDivTest, PropagateSingleton2)
{
  using namespace Core;
  
  // 10 / 2 = [-10, 10] => 5
  setDomains(+10, +10, +2, +2, -10, +10);
  runPropagationAndCheck(+10, +10, +2, +2, +5, +5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 10 * -2 = [-10, 10] => -5
  setDomains(+10, +10, -2, -2, -10, +10);
  runPropagationAndCheck(+10, +10, -2, -2, -5, -5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -10 / -2 = [-10, 10] => 5
  setDomains(-10, -10, -2, -2, -10, +10);
  runPropagationAndCheck(-10, -10, -2, -2, +5, +5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 10 / [-10, 10] = 2 => [4, 5]
  // @note 10 / 4 = 2.5 -> 2 using INT_64
  setDomains(+10, +10, -10, +10, +2, +2);
  runPropagationAndCheck(+10, +10, +4, +5, +2, +2, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // 10 / [-10, 10] = -2 => [-5, -4]
  // @note 10 / 4 = 2.5 -> 2 using INT_64
  setDomains(+10, +10, -10, +10, -2, -2);
  runPropagationAndCheck(+10, +10, -5, -4, -2, -2, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // -10 * [-10, 10] = 2 => [-5, -4]
  // @note 10 / 4 = 2.5 -> 2 using INT_64
  setDomains(-10, -10, -10, +10, 2, 2);
  runPropagationAndCheck(-10, -10, -5, -4, +2, +2, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-10, 10] / 2 = 5 => 10
  setDomains(-10, +10, +2, +2, +5, +5);
  runPropagationAndCheck(+10, +10, +2, +2, +5, +5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [-10, 10] / -2 = -5 => 10
  setDomains(-10, +10, -2, -2, -5, -5);
  runPropagationAndCheck(+10, +10, -2, -2, -5, -5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 10 / 2 = [6, 10] => FAIL
  setDomains(+10, +10, +2, +2, 6, 10);
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // 10 / [2, 3] = 10 => FAIL
  setDomains(+10, +10, +2, +3, 10, 10);
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // [1, 9] / 2 = 5 => FAIL
  setDomains(+1, +9, +2, +2, 5, 5);
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
}//PropagateSingleton2

OPTILAB_TEST_F(PropagatorDivTest, PropagateSingletonPost2)
{
  using namespace Core;
  
  // 10 / 2 = [-10, 10] => 5
  setDomains(+10, +10, +2, +2, -10, +10);
  postPropagator(+10, +10, +2, +2, +5, +5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 10 * -2 = [-10, 10] => -5
  setDomains(+10, +10, -2, -2, -10, +10);
  postPropagator(+10, +10, -2, -2, -5, -5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -10 / -2 = [-10, 10] => 5
  setDomains(-10, -10, -2, -2, -10, +10);
  postPropagator(-10, -10, -2, -2, +5, +5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 10 / [-10, 10] = 2 => 5
  // @note 10 / 4 = 2.5 -> 2 using INT_64 but on post D1 = 10 / 2 => 5
  setDomains(+10, +10, -10, +10, +2, +2);
  postPropagator(+10, +10, +5, +5, +2, +2, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 10 / [-10, 10] = -2 => -5
  // @note 10 / 4 = 2.5 -> 2 using INT_64 but on post D1 = 10 / 2 => 5
  setDomains(+10, +10, -10, +10, -2, -2);
  postPropagator(+10, +10, -5, -5, -2, -2, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -10 * [-10, 10] = 2 => -5
  // @note 10 / 4 = 2.5 -> 2 using INT_64 but on post D1 = 10 / 2 => 5
  setDomains(-10, -10, -10, +10, 2, 2);
  postPropagator(-10, -10, -5, -5, +2, +2, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [-10, 10] / 2 = 5 => 10
  setDomains(-10, +10, +2, +2, +5, +5);
  postPropagator(+10, +10, +2, +2, +5, +5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [-10, 10] / -2 = -5 => 10
  setDomains(-10, +10, -2, -2, -5, -5);
  postPropagator(+10, +10, -2, -2, -5, -5, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 10 / 2 = [6, 10] => FAIL
  setDomains(+10, +10, +2, +2, 6, 10);
  postPropagator(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // 10 / [2, 3] = 10 => FAIL
  setDomains(+10, +10, +2, +3, 10, 10);
  postPropagator(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // [1, 9] / 2 = 5 => FAIL
  setDomains(+1, +9, +2, +2, 5, 5);
  postPropagator(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
}//PropagateSingletonPost2

OPTILAB_TEST_F(PropagatorDivTest, PropagatePPP)
{
  using namespace Core;
  
  // [20, 50] / [2, 4] = [1, 50] => D2 = [5, 25]
  setDomains(+20, +50, +2, +4, +1, +50);
  runPropagationAndCheck(+20, +50, +2, +4, +5, +25, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [20, 50] / [1, 50] = [5, 25] => D1 = [1, 10]
  setDomains(+20, +50, 1, +50, +5, +25);
  runPropagationAndCheck(+20, +50, 1, +10, +5, +25, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [1, 50] / [1, 50] = [5, 25] => D0 = [5, 50], D1 = [1, 10]
  setDomains(1, +50, +1, +50, +5, +25);
  runPropagationAndCheck(+5, +50, +1, +10, +5, +25, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [1, 3] / [1, 3] = [1, +10] => D2 = [1, 3]
  setDomains({1,2,3}, {1,2,3}, {1,2,3,4,5,6,7,8,9,10});
  runPropagationAndCheck(+1, +3, +1, +3, +1, +3, PropagationEvent::PROP_EVENT_FIXPOINT);
}//PropagatePPP

OPTILAB_TEST_F(PropagatorDivTest, PostPPP)
{
  using namespace Core;
  
  //@note post propagates only on D2.LB
  // D2 >= D0.LB/D1.UB
  
  // [20, 50] / [2, 4] = [1, 50] => D2 = [5, 50]
  setDomains(+20, +50, +2, +4, +1, +50);
  postPropagator(+20, +50, +2, +4, +5, +50, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // [20, 50] / [1, 50] = [5, 25] => D2 = [5, 25]
  setDomains(+20, +50, 1, +50, +5, +25);
  postPropagator(+20, +50, 1, +50, +5, +25, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // [1, 3] / [1, 3] = [1, +10] => D2 = [1, 3]
  setDomains({1,2,3}, {1,2,3}, {1,2,3,4,5,6,7,8,9,10});
  postPropagator(+1, +3, +1, +3, +1, +10, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
}//PostPPP

OPTILAB_TEST_F(PropagatorDivTest, PostPNN)
{
  using namespace Core;
  
  //@note post propagates only on D2.LB
  // D2 >= D0.LB/D1.UB
  
  // [20, 50] / [-4, -2] = [-50, -1] => D2 = [-50, -5]
  setDomains(+20, +50, -4, -2, -50, -1);
  postPropagator(+20, +50, -4, -2, -50, -5, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
}//PostPNN

OPTILAB_TEST_F(PropagatorDivTest, PropagatePPN_PNP_NPP_NNN)
{
  using namespace Core;
  
  // [5, 10] / [2, 4] = [-50, -10] => D2 = []
  setDomains(+5, +10, +2, +4, -50, -10);
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // [1, 3] / [1, 3] = [-3, -1] => D2 = []
  setDomains({1,2,3}, {1,2,3}, {-3, -2, -1});
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // [5, 10] / [-10, -4] = [1, 10] => D1 = []
  setDomains(+5, +10, -10, -4, +1, +10);
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // [1, 3] / [-3, -1] = [1, 3] => D1 = []
  setDomains({1,2,3}, {-3,-2,-1}, {1, 2, 3});
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // [-5, -1] / [4, 10] = [1, 50] => D2 = []
  setDomains(-5, -1, +4, +10, +1, +50);
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // [-3, -1] / [1, 3] = [1, 3] => D2 = []
  setDomains({-1,-2,-3}, {3,2,1}, {1, 2, 3});
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // [-5, -1] / [-10, -4] = [-100, -1] => D2 = []
  setDomains(-5, -1, -10, -4, -100, -1);
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
  
  // [-2, -1] / [-2, -1] = [-5, -1] => D2 = []
  setDomains({-1,-2}, {-2, -1}, {-1, -2, -3, -4, -5});
  runPropagationAndCheck(0, 0, 0, 0, 0, 0, PropagationEvent::PROP_EVENT_FAIL);
}//PropagatePPN_PNP_NPP_NNN

OPTILAB_TEST_F(PropagatorDivTest, PropagatePPX_PXP_XPP)
{
  using namespace Core;
  
  // [20, 50] / [2, 4] = [-50, 50] => D2 = [5, 25]
  setDomains(+20, +50, +2, +4, -50, +50);
  runPropagationAndCheck(+20, +50, +2, +4, +5, +25, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [1, 3] / [1, 3] = [-1, +10] => D2 = [0, 3]
  setDomains({1,2,3}, {1,2,3}, {-1,0,1,2,3,4,5,6,7,8,9,10});
  runPropagationAndCheck(+1, +3, +1, +3, +0, +3, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [20, 50] / [-50, 50] = [5, 40] => D1 = [1, 10]
  setDomains(+20, +50, -50, +50, +5, +40);
  runPropagationAndCheck(+20, +50, +1, +10, +5, +40, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-50, 50] / [2, 4] = [10, 40] => D0 = [20, 50] and D2 = [10, 25]
  setDomains(-50, +50, +2, +4, +10, +40);
  runPropagationAndCheck(+20, +50, +2, +4, +10, +25, PropagationEvent::PROP_EVENT_FIXPOINT);
}//PropagatePPX_PXP_XPP

OPTILAB_TEST_F(PropagatorDivTest, PropagatePNN_PNX_PXN)
{
  using namespace Core;
  
  // [20, 50] / [-4, -2] = [-50, -1] => D2 = [-25, -5]
  setDomains(+20, +50, -4, -2, -50, -1);
  runPropagationAndCheck(+20, +50, -4, -2, -25, -5, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [20, 50] / [-4, -2] = [-50, 50] => D2 = [-25, -5]
  setDomains(+20, +50, -4, -2, -50, +50);
  runPropagationAndCheck(+20, +50, -4, -2, -25, -5, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [1, 3] / [-3, -1] = [-10, -1] => D2 = [-3, -1]
  setDomains({1,2,3}, {-1,-2,-3}, {-1,-2,-3,-4,-5,-6,-7,-8,-9,-10});
  runPropagationAndCheck(+1, +3, -3, -1, -3, -1, PropagationEvent::PROP_EVENT_FIXPOINT);
}//PropagatePNN_PNX_PXN

OPTILAB_TEST_F(PropagatorDivTest, PropagatePXX)
{
  using namespace Core;
  
  // [20, 50] / [-4, +4] = [-50, +50]
  setDomains(+20, +50, -4, +4, -50, +50);
  runPropagationAndCheck(+20, +50, -4, +4, -50, +50, PropagationEvent::PROP_EVENT_NO_FIXPOINT);
  
  // [20, 50] / [-4, +4] = [-10, +10] => D0 = [-20, +43]
  setDomains(+20, +50, -4, +4, -10, +10);
  runPropagationAndCheck(+20, +43, -4, +4, -10, +10, PropagationEvent::PROP_EVENT_NO_FIXPOINT);
}//PropagatePXX

OPTILAB_TEST_F(PropagatorDivTest, PropagateNPN_NPX)
{
  using namespace Core;
  
  // [-50, -5] / [1, 4] = [-100, -1] => D2 = [-50, -1]
  setDomains(-50, -5, +1, +4, -100, -1);
  runPropagationAndCheck(-50, -5, +1, +4, -50, -1, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-50, -5] / [1, 4] = [-100, +100] => D2 = [-50, -1]
  setDomains(-50, -5, +1, +4, -100, +100);
  runPropagationAndCheck(-50, -5, +1, +4, -50, -1, PropagationEvent::PROP_EVENT_FIXPOINT);
}//PropagateNPN_NPX

OPTILAB_TEST_F(PropagatorDivTest, PropagateNNP_NNX)
{
  using namespace Core;
  
  // [-10, -5] / [-4, -1] = [+1, +100] => D2 = [+1, +50]
  setDomains(-50, -5, -4, -1, +1, +100);
  runPropagationAndCheck(-50, -5, -4, -1, +1, +50, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-3, -1] / [-3, -1] = [+1, +10] => D2 = [+1, +9]
  setDomains({-1,-2,-3}, {-1,-2,-3}, {+1,+2,+3,+4,+5,+6,+7,+8,+9,+10});
  runPropagationAndCheck(-3, -1, -3, -1, +1, +3, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-10, -5] / [-4, -1] = [-100, +100] => D2 = [+1, +50]
  setDomains(-50, -5, -4, -1, -100, +100);
  runPropagationAndCheck(-50, -5, -4, -1, +1, +50, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-3, -1] / [-3, -1] = [-1, +10] => D2 = [+0, +3]
  setDomains({-1,-2,-3}, {-1,-2,-3}, {-1, 0, +1,+2,+3,+4,+5,+6,+7,+8,+9,+10});
  runPropagationAndCheck(-3, -1, -3, -1, +0, +3, PropagationEvent::PROP_EVENT_FIXPOINT);
}//PropagateNNP_NNX

OPTILAB_TEST_F(PropagatorDivTest, PropagateNXP_NXN_NXX)
{
  using namespace Core;
  
  // [-10, -5] / [-4, 4] = [+1, +100] => D2 = [1, 10]
  setDomains(-10, -5, -4, +4, +1, +100);
  runPropagationAndCheck(-10, -5, -4, -1, +1, +10, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-10, -5] / [-4, 4] = [-100, -1] => D2 = [-10, -1]
  setDomains(-10, -5, -4, +4, -100, -1);
  runPropagationAndCheck(-10, -5, +1, +4, -10, -1, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-10, -5] / [-4, 4] = [-100, +100] => no fix point
  setDomains(-10, -5, -4, +4, -100, +100);
  runPropagationAndCheck(-10, -5, -4, +4, -100, +100, PropagationEvent::PROP_EVENT_NO_FIXPOINT);
}//PropagateNXP_NXN_NXX

OPTILAB_TEST_F(PropagatorDivTest, PropagateXPP_XPN_XPX)
{
  using namespace Core;
  
  // [-50, 50] / [1, 4] = [+1, +100] => D0 = [1, 50] /\ D2 = [1, +50]
  setDomains(-50, +50, +1, +4, +1, +100);
  runPropagationAndCheck(+1, +50, +1, +4, +1, +50, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-50, 50] / [1, 4] = [-100, -1] => D0 = [-50, -1] /\ D2 = [-50, -1]
  setDomains(-50, +10, +1, +4, -100, -1);
  runPropagationAndCheck(-50, -1, +1, +4, -50, -1, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-50, 50] / [1, 4] = [-100, +100] => D2 = [-50, +50]
  setDomains(-50, +50, +1, +4, -100, +100);
  runPropagationAndCheck(-50, +50, +1, +4, -50, +50, PropagationEvent::PROP_EVENT_NO_FIXPOINT);
}//PropagateXPP_XPN_XPX

OPTILAB_TEST_F(PropagatorDivTest, PropagateXNP_XNN_XNX)
{
  using namespace Core;
  
  // [-10, 10] / [-4, -1] = [+1, +100] => D0 = [-10, -1] /\ D2 = [1, +10]
  setDomains(-10, +10, -4, -1, +1, +100);
  runPropagationAndCheck(-10, -1, -4, -1, +1, +10, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-10, 10] / [-4, -1] = [-100, -1] => D0 = [+1, +10] /\ D2 = [-10, -1]
  setDomains(-10, +10, -4, -1, -100, -1);
  runPropagationAndCheck(+1, +10, -4, -1, -10, -1, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [-10, 10] / [-4, -1] = [-100, +100] => D2 = [-10, +10]
  setDomains(-10, +10, -4, -1, -100, +100);
  runPropagationAndCheck(-10, +10, -4, -1, -10, +10, PropagationEvent::PROP_EVENT_NO_FIXPOINT);
}//PropagateXNP_XNN_XNX

OPTILAB_TEST_F(PropagatorDivTest, PropagateXXP_XXN_XXX)
{
  using namespace Core;
  
  // [-10, 10] / [-4, +4] = [+1, +100] => no fix point
  setDomains(-10, +10, -4, +4, +1, +100);
  runPropagationAndCheck(-10, +10, -4, +4, +1, +100, PropagationEvent::PROP_EVENT_NO_FIXPOINT);
  
  // [-10, 10] / [-4, +4] = [-100, -1] => no fix point
  setDomains(-10, +10, -4, +4, -100, -1);
  runPropagationAndCheck(-10, +10, -4, +4, -100, -1, PropagationEvent::PROP_EVENT_NO_FIXPOINT);
  
  // [-10, 10] / [-4, +4] = [-100, +100] => no fix point
  setDomains(-10, +10, -4, +4, -100, +100);
  runPropagationAndCheck(-10, +10, -4, +4, -100, +100, PropagationEvent::PROP_EVENT_NO_FIXPOINT);
}//PropagateXXP_XXN_XXX

OPTILAB_TEST_F(PropagatorDivTest, getFitness)
{
  // Test fitness function
  using namespace Core;
  
  setDomains(+5, +5, +2, +2, +2, +2);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());

  setDomains(+0, +0, +2, +2, +2, +2);
  EXPECT_EQ(2, getFitness());

  setDomains(-5, -5, +2, +2, +2, +2);
  EXPECT_EQ(5, getFitness());

  setDomains(+5, +5, -2, -2, +2, +2);
  EXPECT_EQ(5, getFitness());

  setDomains(+2, +2, +2, +2, +1, +1);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());

  setDomains(+9, +9, +2, +2, -9, -9);
  EXPECT_EQ(13, getFitness());

  setDomains(-4, -4, -2, -2, -2, -2);
  EXPECT_EQ(4, getFitness());
}//getFitness
