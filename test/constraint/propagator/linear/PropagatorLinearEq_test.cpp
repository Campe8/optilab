
#include "utest_globals.hpp"

#include "DomainDefs.hpp"
#include "PropagatorLinearEq.hpp"
#include "ConstraintTestInc.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"
#include "DArrayMatrix.hpp"

#include <vector>
#include <memory>
#include <cassert>

namespace {
  class PropagatorLinearEqTest : public ::testing::Test {
  public:
    PropagatorLinearEqTest() = default;
    ~PropagatorLinearEqTest() = default;
    
    void setLinearExpr(std::vector<INT_64>&& aCoeff,
                       std::vector<INT_64>&& aDomMinMax,
                       INT_64 aLinearConstant)
    {
      assert(aCoeff.size() * 2 == aDomMinMax.size());
      pCoeff = std::make_shared<Core::DomainElementArray>(aCoeff.size());
      pDomains = std::make_shared<Core::DomainArray>(aDomMinMax.size()/2);
      
      // Coefficients
      std::size_t idx{0};
      pTempCoeffArray.clear();
      for(auto& coeff : aCoeff)
      {
        pTempCoeffArray.push_back(getElem(coeff));
        pCoeff->assignElementToCell(idx++, pTempCoeffArray.back().get());
      }
      
      // Domains
      idx = 0;
      pTempDomainArray.clear();
      for(std::size_t domIdx = 0; domIdx < aDomMinMax.size(); ++domIdx)
      {
        if(domIdx > 0 && (domIdx % 2 != 0))
        {
          pTempDomainArray.push_back(getDomainInt(aDomMinMax[domIdx-1], aDomMinMax[domIdx]));
          pDomains->assignElementToCell(idx++, pTempDomainArray.back().get());
        }
      }
      
      // Constant
      pConstant = getElem(aLinearConstant);
    }//setLinearExpr
    
    void postPropagator(std::vector<INT_64>&& aDomMinMax,
                        Core::PropagationEvent aEvent)
    {
      std::unique_ptr<Core::PropagatorLinearEq> propagator(new Core::PropagatorLinearEq(*(pCoeff.get()),
                                                                                        *(pDomains.get()),
                                                                                        pConstant.get()));
      Core::PropagationEvent event = propagator->post();
      ASSERT_EQ(event, aEvent);
      
      std::size_t domIdxToCompare{0};
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        for(std::size_t domIdx = 0; domIdx < aDomMinMax.size(); ++domIdx)
        {
          if(domIdx > 0 && (domIdx % 2 != 0))
          {
            DomainIntPtr domain = getDomainInt(aDomMinMax[domIdx-1], aDomMinMax[domIdx]);
            ASSERT_TRUE(pDomains->operator[](domIdxToCompare)->lowerBound()->isEqual(domain->lowerBound()));
            ASSERT_TRUE(pDomains->operator[](domIdxToCompare)->upperBound()->isEqual(domain->upperBound()));
            domIdxToCompare++;
          }
        }
      }
    }//postPropagator
    
    void runPropagationAndCheck(std::vector<INT_64>&& aDomMinMax,
                                Core::PropagationEvent aEvent)
    {
      // Perform propagation
      std::unique_ptr<Core::PropagatorLinearEq> propagator(new Core::PropagatorLinearEq(*(pCoeff.get()),
                                                                                        *(pDomains.get()),
                                                                                        pConstant.get()));
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      
      std::size_t domIdxToCompare{0};
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        for(std::size_t domIdx = 0; domIdx < aDomMinMax.size(); ++domIdx)
        {
          if(domIdx > 0 && (domIdx % 2 != 0))
          {
            DomainIntPtr domain = getDomainInt(aDomMinMax[domIdx-1], aDomMinMax[domIdx]);
            ASSERT_TRUE(pDomains->operator[](domIdxToCompare)->lowerBound()->isEqual(domain->lowerBound()));
            ASSERT_TRUE(pDomains->operator[](domIdxToCompare)->upperBound()->isEqual(domain->upperBound()));
            domIdxToCompare++;
          }
        }
      }
    }//runPropagationAndCheck
    
    double getFitness()
    {
      std::unique_ptr<Core::PropagatorLinearEq> propagator(new Core::PropagatorLinearEq(*(pCoeff.get()),
                                                                                        *(pDomains.get()),
                                                                                        pConstant.get()));
      return propagator->getFitness();
    }
    
  protected:
    // Coefficients array
    Core::DomainElementArraySPtr pCoeff;
    // Domain array
    Core::DomainArraySPtr pDomains;
    // Linear Constat
    ElemPtr pConstant;
    
    // Array of shared ptr used to preserve coefficients
    std::vector<ElemPtr> pTempCoeffArray;
    
    // Array of shared ptr used to preserve domains
    std::vector<DomainIntPtr> pTempDomainArray;
  };
}// end namespace


/*********************** Test ***********************/

OPTILAB_TEST_F(PropagatorLinearEqTest, post)
{
  using namespace Core;
  
  // @note Post for linear Eq propagates only on singletons
  
  // 1 * 2 + 2 * 2 = 6 => subsumed
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  postPropagator({2,2,2,2},PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 1 * 2 + 2 * 2 = 7 => fail
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 7);
  postPropagator({2,2,2,2},PropagationEvent::PROP_EVENT_FAIL);
  
  // 3 * [0, 9] - 5 * [1, 8] = 4
  setLinearExpr({ 3, -5 }, { 0, 9, 1, 8 }, 4);
  postPropagator({ 0, 9, 1, 8 }, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
}//post

OPTILAB_TEST_F(PropagatorLinearEqTest, simpleCases)
{
  using namespace Core;
  
  // 1 * 2 + 2 * 2 = 6
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  runPropagationAndCheck({2,2,2,2},PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 3 * [0, 9] - 5 * [1, 8] = 4 => 3 * [3, 8] - 5 * [1, 4] = 4
  setLinearExpr({ 3, -5 }, { 0, 9, 1, 8 }, 4);
  runPropagationAndCheck({ 3, 8, 1, 4 }, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // 4 * [0, 100] = 19 => []
  setLinearExpr({ 4 }, { 0, 100 }, 19);
  runPropagationAndCheck({ 0, 0 }, PropagationEvent::PROP_EVENT_FAIL);
  
  // 4 * [0, 100] = 20 => [5, 5]
  setLinearExpr({ 4 }, { 0, 100 }, 20);
  runPropagationAndCheck({ 5, 5 }, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 4 * [-100, 100] = -20 => [-5, -5]
  setLinearExpr({ 4 }, { -100, 100 }, -20);
  runPropagationAndCheck({ -5, -5 }, PropagationEvent::PROP_EVENT_SUBSUMED);
}//simpleCases

OPTILAB_TEST_F(PropagatorLinearEqTest, getFitnessMin)
{
  using namespace Core;
  
  // Test getFitness() method on min fitness
  
  // 1 * 2 + 2 * 2 = 6
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  EXPECT_EQ(0, getFitness());
}//getFitnessMin

OPTILAB_TEST_F(PropagatorLinearEqTest, getFitnessUndef)
{
  using namespace Core;
  
  // Test getFitness() method on undef fitness
  setLinearExpr({ 3, -5 }, { 0, 9, 1, 8 }, 4);
  EXPECT_EQ(Constraint::FitnessUndef, getFitness());
}//getFitnessUndef

OPTILAB_TEST_F(PropagatorLinearEqTest, getFitness)
{
  using namespace Core;
  
  // Test getFitness() method on non min fitness
  
  // 1 * 2 + 2 * 2 = 6 != 10 -> 10 - 6 = 4
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 10);
  EXPECT_EQ(4.0, getFitness());
}//getFitness
