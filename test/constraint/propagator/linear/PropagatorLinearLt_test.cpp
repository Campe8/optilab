
#include "utest_globals.hpp"

#include "DomainDefs.hpp"
#include "PropagatorLinearLt.hpp"
#include "ConstraintTestInc.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"
#include "DArrayMatrix.hpp"

#include <vector>
#include <memory>
#include <cassert>

namespace {
  using PropPtr = std::unique_ptr<Core::PropagatorLinearLt>;
  
  class PropagatorLinearLtTest : public ::testing::Test {
  public:
    void setLinearExpr(std::vector<INT_64>&& aCoeff,
                       std::vector<INT_64>&& aDomMinMax,
                       INT_64 aLinearConstant)
    {
      assert(aCoeff.size() * 2 == aDomMinMax.size());
      pCoeff = std::make_shared<Core::DomainElementArray>(aCoeff.size());
      pDomains = std::make_shared<Core::DomainArray>(aDomMinMax.size()/2);
      
      // Coefficients
      std::size_t idx{0};
      pTempCoeffArray.clear();
      for(auto& coeff : aCoeff)
      {
        pTempCoeffArray.push_back(getElem(coeff));
        pCoeff->assignElementToCell(idx++, pTempCoeffArray.back().get());
      }
      
      // Domains
      idx = 0;
      pTempDomainArray.clear();
      for(std::size_t domIdx = 0; domIdx < aDomMinMax.size(); ++domIdx)
      {
        if(domIdx > 0 && (domIdx % 2 != 0))
        {
          pTempDomainArray.push_back(getDomainInt(aDomMinMax[domIdx-1], aDomMinMax[domIdx]));
          pDomains->assignElementToCell(idx++, pTempDomainArray.back().get());
        }
      }
      
      // Constant
      pConstant = getElem(aLinearConstant);
    }//setLinearExpr
    
    void setLinearExpr(std::vector<INT_64>&& aCoeff,
                       std::vector<std::vector<INT_64>>&& aDomains,
                       INT_64 aLinearConstant)
    {
      assert(aCoeff.size() == aDomains.size());
      pCoeff = std::make_shared<Core::DomainElementArray>(aCoeff.size());
      pDomains = std::make_shared<Core::DomainArray>(aDomains.size());
      
      // Coefficients
      std::size_t idx{0};
      pTempCoeffArray.clear();
      for(auto& coeff : aCoeff)
      {
        pTempCoeffArray.push_back(getElem(coeff));
        pCoeff->assignElementToCell(idx++, pTempCoeffArray.back().get());
      }
      
      // Domains
      idx = 0;
      pTempDomainArray.clear();
      for(auto& vec : aDomains)
      {
        pTempDomainArray.push_back(getDomainInt(vec));
        pDomains->assignElementToCell(idx++, pTempDomainArray.back().get());
      }
      
      // Constant
      pConstant = getElem(aLinearConstant);
    }//setLinearExpr
    
    void postPropagator(std::vector<INT_64>&& aDomMinMax,
                        Core::PropagationEvent aEvent)
    {
      PropPtr propagator(new Core::PropagatorLinearLt(*(pCoeff.get()), *(pDomains.get()),
                                                      pConstant.get()));
      
      Core::PropagationEvent event = propagator->post();
      ASSERT_EQ(event, aEvent);
      
      std::size_t domIdxToCompare{0};
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        for(std::size_t domIdx = 0; domIdx < aDomMinMax.size(); ++domIdx)
        {
          if(domIdx > 0 && (domIdx % 2 != 0))
          {
            DomainIntPtr domain = getDomainInt(aDomMinMax[domIdx-1], aDomMinMax[domIdx]);
            ASSERT_TRUE(pDomains->operator[](domIdxToCompare)->lowerBound()->isEqual(domain->lowerBound()));
            ASSERT_TRUE(pDomains->operator[](domIdxToCompare)->upperBound()->isEqual(domain->upperBound()));
            domIdxToCompare++;
          }
        }
      }
    }//postPropagator
    
    void runPropagationAndCheck(std::vector<INT_64>&& aDomMinMax,
                                Core::PropagationEvent aEvent)
    {
      // Perform propagation
      PropPtr propagator(new Core::PropagatorLinearLt(*(pCoeff.get()), *(pDomains.get()),
                                                      pConstant.get()));
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      
      std::size_t domIdxToCompare{0};
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        for(std::size_t domIdx = 0; domIdx < aDomMinMax.size(); ++domIdx)
        {
          if(domIdx > 0 && (domIdx % 2 != 0))
          {
            DomainIntPtr domain = getDomainInt(aDomMinMax[domIdx-1], aDomMinMax[domIdx]);
            ASSERT_TRUE(pDomains->operator[](domIdxToCompare)->lowerBound()->isEqual(domain->lowerBound()));
            ASSERT_TRUE(pDomains->operator[](domIdxToCompare)->upperBound()->isEqual(domain->upperBound()));
            domIdxToCompare++;
          }
        }
      }
    }//runPropagationAndCheck
    
    double getFitness()
    {
      PropPtr propagator(new Core::PropagatorLinearLt(*(pCoeff.get()), *(pDomains.get()),
                                                      pConstant.get()));
      return propagator->getFitness();
    }//getFitness
    
  protected:
    // Coefficients array
    Core::DomainElementArraySPtr pCoeff;
    // Domain array
    Core::DomainArraySPtr pDomains;
    // Linear Constat
    ElemPtr pConstant;
    
    // Array of shared ptr used to preserve coefficients
    std::vector<ElemPtr> pTempCoeffArray;
    
    // Array of shared ptr used to preserve domains
    std::vector<DomainIntPtr> pTempDomainArray;
  };
}// end namespace


/*********************** Test ***********************/

OPTILAB_TEST_F(PropagatorLinearLtTest, post)
{
  using namespace Core;
  
  // @note Post for linear Eq propagates only on singletons
  
  // 1 * 2 + 2 * 2 < 6 => fail
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  postPropagator({2,2,2,2},PropagationEvent::PROP_EVENT_FAIL);
  
  // 1 * 2 + 2 * 2 < 7 => subsumed
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 7);
  postPropagator({2,2,2,2},PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 1 * 2 + 2 * 2 < 3 => fail
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 3);
  postPropagator({2,2,2,2},PropagationEvent::PROP_EVENT_FAIL);
  
  // 3 * [0, 9] - 5 * [1, 8] < 4
  setLinearExpr({ 3, -5 }, { 0, 9, 1, 8 }, 4);
  postPropagator({ 0, 9, 1, 8 }, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
}//post

OPTILAB_TEST_F(PropagatorLinearLtTest, simpleCases)
{
  using namespace Core;
  
  // 1 * 2 + 2 * 2 < 7
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 7);
  runPropagationAndCheck({2,2,2,2},PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 3 * [0, 9] - 5 * [1, 8] <=5
  setLinearExpr({ 3, -5 }, { 0, 9, 1, 8 }, 5);
  runPropagationAndCheck({ 0, 9, 1, 8 }, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // 3 * [1, 2] + 2 * [2, 5] < 12 => [1, 2] /\ [2, 4]
  setLinearExpr({ 3, 2 }, { 1, 2, 2, 5 }, 12);
  runPropagationAndCheck({ 1, 2, 2, 4 }, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // 4 * [0, 100] < -1 => []
  setLinearExpr({ 4 }, { 0, 100 }, -1);
  runPropagationAndCheck({ 0, 0 }, PropagationEvent::PROP_EVENT_FAIL);
}//simpleCases

OPTILAB_TEST_F(PropagatorLinearLtTest, setCases)
{
  using namespace Core;
  
  setLinearExpr({ 4, 2 }, { {-3, -2, -1}, {-2, -1, 0} }, -20);
  runPropagationAndCheck({ 0, 0, 0, 0 }, PropagationEvent::PROP_EVENT_FAIL);
  
  // 3 * [1, 2] + 2 * [2, 5] < 12 => [1, 2] /\ [2, 4]
  setLinearExpr({ 3, 2 }, { {1, 2}, {2, 3, 4, 5} }, 12);
  runPropagationAndCheck({ 1, 2, 2, 4 }, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // 3 * [1, 2] + 2 * [2, 5] < 12 => [1, 2] /\ [2, 3]
  setLinearExpr({ 3, 2 }, { {1, 2}, {2, 3, 5} }, 12);
  runPropagationAndCheck({ 1, 2, 2, 3 }, PropagationEvent::PROP_EVENT_FIXPOINT);
}//setCases

OPTILAB_TEST_F(PropagatorLinearLtTest, getFitness)
{
  using namespace Core;
  
  // 1 * 2 + 2 * 2 < 6 => 1
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  EXPECT_EQ(1, getFitness());
  
  // 1 * 2 + 2 * 2 < 7 => 0
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 7);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());
  
  // 1 * 2 + 2 * 2 < 3 => 4
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 3);
  EXPECT_EQ(4, getFitness());
  
  // 3 * [0, 9] - 5 * [1, 8] <= 4 => -1
  setLinearExpr({ 3, -5 }, { 0, 9, 1, 8 }, 4);
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness());
}//getFitness
