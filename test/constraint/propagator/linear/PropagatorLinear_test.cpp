
#include "utest_globals.hpp"

#include "PropagatorLinear.hpp"
#include "ConstraintTestInc.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"

namespace Core {
  
  class PropagatorLinearTest : public PropagatorLinear {
  public:
    PropagatorLinearTest(DomainElementArray& aCoeffArray,
                         DomainArray&        aDomainArray,
                         DomainElement*      aConstant,
                         bool aUseGCDOnNormalization = true)
    : PropagatorLinear(aCoeffArray, aDomainArray, aConstant,
                       PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
                       PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ,
                       PropagatorSemanticClass::PROP_SEMANTIC_CLASS_LINEAR,
                       PropagationPriority::PROP_PRIORITY_LINEAR,
                       aUseGCDOnNormalization)
    {
    }
    
    ~PropagatorLinearTest()
    {
    }
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
    PropagationEvent post() override
    {
      return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    }
    
    /// Expose method for testing
    inline DomainElement *getLinearConstant() const
    {
      return PropagatorLinear::getLinearConstant();
    }
    
    inline std::pair<DomainElement*, DomainElement*> calculatePositiveBounds()
    {
      return PropagatorLinear::calculatePositiveBounds();
    }
    
    inline std::pair<DomainElement*, DomainElement*> calculateNegativeBounds()
    {
      return PropagatorLinear::calculateNegativeBounds();
    }
    
  protected:
    PropagationEvent runPropagation() override
    {
      return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    }
    
  };
}// end namespace Core

/*********************** Test ***********************/

OPTILAB_TEST(PropagatorLinearTest, CalculateBounds)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  DomainElementArray coeff(2);
  DomainArray domains(2);
  
  // 2 * [1, 3] + 3 * [2, 4] = 0 => [8, 18]
  coeff.assignElementToCell(0, elementManager.createDomainElementInt64(2));
  coeff.assignElementToCell(1, elementManager.createDomainElementInt64(3));
  
  DomainIntPtr domain0 = getDomainInt(1, 3);
  DomainIntPtr domain1 = getDomainInt(2, 4);
  domains.assignElementToCell(0, domain0.get());
  domains.assignElementToCell(1, domain1.get());
  
  PropagatorLinearTest propagator1(coeff, domains, elementManager.createDomainElementInt64(0));
  
  auto negativeBounds = propagator1.calculateNegativeBounds();
  auto positiveBounds = propagator1.calculatePositiveBounds();
  
  // No negative bounds
  ASSERT_EQ(negativeBounds.first, nullptr);
  ASSERT_EQ(negativeBounds.second, nullptr);
  
  // Positive bounds
  ASSERT_TRUE(positiveBounds.first->isEqual(elementManager.createDomainElementInt64(8)));
  ASSERT_TRUE(positiveBounds.second->isEqual(elementManager.createDomainElementInt64(18)));
  
  // Linear constant
  ASSERT_TRUE(propagator1.getLinearConstant()->isEqual(elementManager.createDomainElementInt64(0)));
  
  // 3 * [1, 3] + 6 * [2, 4] = 12 => 1 * [1, 3] + 2 * [2, 4] = 4 => [5, 11]
  coeff.assignElementToCell(0, elementManager.createDomainElementInt64(3));
  coeff.assignElementToCell(1, elementManager.createDomainElementInt64(6));
  
  PropagatorLinearTest propagator2(coeff, domains, elementManager.createDomainElementInt64(12));
  
  negativeBounds = propagator2.calculateNegativeBounds();
  positiveBounds = propagator2.calculatePositiveBounds();
  
  // No negative bounds
  ASSERT_EQ(negativeBounds.first, nullptr);
  ASSERT_EQ(negativeBounds.second, nullptr);
  
  // Positive bounds
  ASSERT_TRUE(positiveBounds.first->isEqual(elementManager.createDomainElementInt64(5)));
  ASSERT_TRUE(positiveBounds.second->isEqual(elementManager.createDomainElementInt64(11)));
  
  // Linear constant
  ASSERT_TRUE(propagator2.getLinearConstant()->isEqual(elementManager.createDomainElementInt64(4)));
  
  // -3 * [1, 3] + 6 * [2, 4] = 12 => -1 * [1, 3] + 2 * [2, 4] = 4 => [1, 3] /\ [4, 8]
  coeff.assignElementToCell(0, elementManager.createDomainElementInt64(-3));
  coeff.assignElementToCell(1, elementManager.createDomainElementInt64(6));
  
  PropagatorLinearTest propagator3(coeff, domains, elementManager.createDomainElementInt64(12));
  
  negativeBounds = propagator3.calculateNegativeBounds();
  positiveBounds = propagator3.calculatePositiveBounds();
  
  // No negative bounds
  ASSERT_TRUE(negativeBounds.first->isEqual(elementManager.createDomainElementInt64(1)));
  ASSERT_TRUE(negativeBounds.second->isEqual(elementManager.createDomainElementInt64(3)));
  
  // Positive bounds
  ASSERT_TRUE(positiveBounds.first->isEqual(elementManager.createDomainElementInt64(4)));
  ASSERT_TRUE(positiveBounds.second->isEqual(elementManager.createDomainElementInt64(8)));
  
  // Linear constant
  ASSERT_TRUE(propagator3.getLinearConstant()->isEqual(elementManager.createDomainElementInt64(4)));
  
  // -3 * [1, 3] + 6 * [2, 4] + 0 * [1, 5] = 12 => -1 * [1, 3] + 2 * [2, 4] = 4 => [1, 3] /\ [4, 8]
  DomainElementArray coeff2(3);
  DomainArray domains2(3);
  coeff2.assignElementToCell(0, elementManager.createDomainElementInt64(-3));
  coeff2.assignElementToCell(1, elementManager.createDomainElementInt64(6));
  coeff2.assignElementToCell(2, elementManager.createDomainElementInt64(0));
  
  DomainIntPtr domain2 = getDomainInt(1, 5);
  domains2.assignElementToCell(0, domain0.get());
  domains2.assignElementToCell(1, domain1.get());
  domains2.assignElementToCell(2, domain2.get());
  
  PropagatorLinearTest propagator4(coeff2, domains2, elementManager.createDomainElementInt64(12));
  
  negativeBounds = propagator4.calculateNegativeBounds();
  positiveBounds = propagator4.calculatePositiveBounds();
  
  // No negative bounds
  ASSERT_TRUE(negativeBounds.first->isEqual(elementManager.createDomainElementInt64(1)));
  ASSERT_TRUE(negativeBounds.second->isEqual(elementManager.createDomainElementInt64(3)));
  
  // Positive bounds
  ASSERT_TRUE(positiveBounds.first->isEqual(elementManager.createDomainElementInt64(4)));
  ASSERT_TRUE(positiveBounds.second->isEqual(elementManager.createDomainElementInt64(8)));
  
  // Linear constant
  ASSERT_TRUE(propagator4.getLinearConstant()->isEqual(elementManager.createDomainElementInt64(4)));
  
  // 3 * [1, 3] + 6 * [2, 4] = 12 => [15, 33]
  coeff.assignElementToCell(0, elementManager.createDomainElementInt64(3));
  coeff.assignElementToCell(1, elementManager.createDomainElementInt64(6));
  
  PropagatorLinearTest propagator5(coeff, domains, elementManager.createDomainElementInt64(12), false);
  
  negativeBounds = propagator5.calculateNegativeBounds();
  positiveBounds = propagator5.calculatePositiveBounds();
  
  // No negative bounds
  ASSERT_EQ(negativeBounds.first, nullptr);
  ASSERT_EQ(negativeBounds.second, nullptr);
  
  // Positive bounds
  ASSERT_TRUE(positiveBounds.first->isEqual(elementManager.createDomainElementInt64(15)));
  ASSERT_TRUE(positiveBounds.second->isEqual(elementManager.createDomainElementInt64(33)));
  
  // Linear constant
  ASSERT_TRUE(propagator5.getLinearConstant()->isEqual(elementManager.createDomainElementInt64(12)));
}//CalculateBounds

