
#include "utest_globals.hpp"

#include "PropagatorLinearLqReif.hpp"
#include "ConstraintTestInc.hpp"

#define HREIF_L 0
#define HREIF_R 1

namespace {
  class PropagatorLinearLqReifTest : public ::testing::Test {
  public:
    void setLinearExpr(std::vector<INT_64>&& aCoeff,
                       std::vector<INT_64>&& aDomMinMax,
                       INT_64 aLinearConstant)
    {
      assert(aCoeff.size() * 2 == aDomMinMax.size());
      pCoeff = std::make_shared<Core::DomainElementArray>(aCoeff.size());
      pDomains = std::make_shared<Core::DomainArray>(aDomMinMax.size()/2);
      pDomainReif = std::make_shared<Core::DomainBoolean>();
      
      // Coefficients
      std::size_t idx{0};
      pTempCoeffArray.clear();
      for(auto& coeff : aCoeff)
      {
        pTempCoeffArray.push_back(getElem(coeff));
        pCoeff->assignElementToCell(idx++, pTempCoeffArray.back().get());
      }
      
      // Domains
      idx = 0;
      pTempDomainArray.clear();
      for(std::size_t domIdx = 0; domIdx < aDomMinMax.size(); ++domIdx)
      {
        if(domIdx > 0 && (domIdx % 2 != 0))
        {
          pTempDomainArray.push_back(getDomainInt(aDomMinMax[domIdx-1], aDomMinMax[domIdx]));
          pDomains->assignElementToCell(idx++, pTempDomainArray.back().get());
        }
      }
      
      // Constant
      pConstant = getElem(aLinearConstant);
    }//setLinearExpr
    
    void setReifDomain(bool aTrueValue)
    {
      if(aTrueValue)
      {
        pDomainReif->setTrue();
      }
      else
      {
        pDomainReif->setFalse();
      }
    }//setReifDomain
    
    DomainBoolPtr getReifDomain()
    {
      return pDomainReif;
    }
    
    double getFitness(int aHalfReif = -1)
    {
      std::unique_ptr<Core::PropagatorLinearLqReif> propagator;
      if(aHalfReif == -1)
      {
        propagator.reset(new Core::PropagatorLinearLqReif(*(pCoeff.get()),
                                                          *(pDomains.get()),
                                                          pConstant.get(),
                                                          pDomainReif.get()));
      }
      else if(aHalfReif == HREIF_L)
      {
        propagator.reset(new Core::PropagatorHLinLqReifL(*(pCoeff.get()),
                                                         *(pDomains.get()),
                                                         pConstant.get(),
                                                         pDomainReif.get()));
      }
      else
      {
        assert(aHalfReif == HREIF_R);
        propagator.reset(new Core::PropagatorHLinLqReifR(*(pCoeff.get()),
                                                         *(pDomains.get()),
                                                         pConstant.get(),
                                                         pDomainReif.get()));
      }
      
      return propagator->getFitness();
    }//getFitness
    
    void postPropagator(std::vector<INT_64>&& aDomMinMax,
                        Core::PropagationEvent aEvent,
                        int aHalfReif = -1)
    {
      std::unique_ptr<Core::PropagatorLinearLqReif> propagator;
      if(aHalfReif == -1)
      {
        propagator.reset(new Core::PropagatorLinearLqReif(*(pCoeff.get()),
                                                          *(pDomains.get()),
                                                          pConstant.get(),
                                                          pDomainReif.get()));
      }
      else if(aHalfReif == HREIF_L)
      {
        propagator.reset(new Core::PropagatorHLinLqReifL(*(pCoeff.get()),
                                                         *(pDomains.get()),
                                                         pConstant.get(),
                                                         pDomainReif.get()));
      }
      else
      {
        assert(aHalfReif == HREIF_R);
        propagator.reset(new Core::PropagatorHLinLqReifR(*(pCoeff.get()),
                                                         *(pDomains.get()),
                                                         pConstant.get(),
                                                         pDomainReif.get()));
      }
      
      Core::PropagationEvent event = propagator->post();
      ASSERT_EQ(event, aEvent);
      
      std::size_t domIdxToCompare{0};
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        for(std::size_t domIdx = 0; domIdx < aDomMinMax.size(); ++domIdx)
        {
          if(domIdx > 0 && (domIdx % 2 != 0))
          {
            DomainIntPtr domain = getDomainInt(aDomMinMax[domIdx-1], aDomMinMax[domIdx]);
            ASSERT_TRUE(pDomains->operator[](domIdxToCompare)->lowerBound()->isEqual(domain->lowerBound()));
            ASSERT_TRUE(pDomains->operator[](domIdxToCompare)->upperBound()->isEqual(domain->upperBound()));
            domIdxToCompare++;
          }
        }
      }
    }//postPropagator
    
    void runPropagationAndCheck(std::vector<INT_64>&& aDomMinMax,
                                bool aReifValMin, bool aReifVal2Max,
                                Core::PropagationEvent aEvent,
                                int aHalfReif = -1)
    {
      // Perform propagation
      std::unique_ptr<Core::PropagatorLinearLqReif> propagator;
      if(aHalfReif == -1)
      {
        propagator.reset(new Core::PropagatorLinearLqReif(*(pCoeff.get()),
                                                          *(pDomains.get()),
                                                          pConstant.get(),
                                                          pDomainReif.get()));
      }
      else if(aHalfReif == HREIF_L)
      {
        propagator.reset(new Core::PropagatorHLinLqReifL(*(pCoeff.get()),
                                                         *(pDomains.get()),
                                                         pConstant.get(),
                                                         pDomainReif.get()));
      }
      else
      {
        assert(aHalfReif == HREIF_R);
        propagator.reset(new Core::PropagatorHLinLqReifR(*(pCoeff.get()),
                                                         *(pDomains.get()),
                                                         pConstant.get(),
                                                         pDomainReif.get()));
      }
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      
      std::size_t domIdxToCompare{0};
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        if(aReifValMin == aReifVal2Max)
        {
          if(aReifValMin)
          {
            ASSERT_TRUE(pDomainReif->isTrue());
          }
          else
          {
            ASSERT_FALSE(pDomainReif->isTrue());
          }
        }
        else
        {
          ASSERT_FALSE(pDomainReif->isTrue());
          ASSERT_FALSE(pDomainReif->isFalse());
        }
        
        for(std::size_t domIdx = 0; domIdx < aDomMinMax.size(); ++domIdx)
        {
          if(domIdx > 0 && (domIdx % 2 != 0))
          {
            DomainIntPtr domain = getDomainInt(aDomMinMax[domIdx-1], aDomMinMax[domIdx]);
            ASSERT_TRUE(pDomains->operator[](domIdxToCompare)->lowerBound()->isEqual(domain->lowerBound()));
            ASSERT_TRUE(pDomains->operator[](domIdxToCompare)->upperBound()->isEqual(domain->upperBound()));
            domIdxToCompare++;
          }
        }
      }
    }//runPropagationAndCheck
    
    virtual void SetUp()
    {
    }
    
    virtual void TearDown()
    {
    }
    
  protected:
    // Coefficients array
    Core::DomainElementArraySPtr pCoeff;
    // Domain array
    Core::DomainArraySPtr pDomains;
    // Domain boolean
    std::shared_ptr<Core::DomainBoolean> pDomainReif;
    // Linear Constat
    ElemPtr pConstant;
    
    // Array of shared ptr used to preserve coefficients
    std::vector<ElemPtr> pTempCoeffArray;
    
    // Array of shared ptr used to preserve domains
    std::vector<DomainIntPtr> pTempDomainArray;
  };
}// end namespace


/*********************** Test ***********************/

OPTILAB_TEST_F(PropagatorLinearLqReifTest, FullReifPost)
{
  using namespace Core;
  
  // 1 * 2 + 2 * 2 <= 6 <-> b => b = true => subsumed
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  postPropagator({2,2,2,2}, PropagationEvent::PROP_EVENT_SUBSUMED);
  ASSERT_TRUE(getReifDomain()->isTrue());
  
  // 1 * 2 + 2 * 2 <= 5 <-> b => b = true => subsumed
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 5);
  postPropagator({2,2,2,2}, PropagationEvent::PROP_EVENT_SUBSUMED);
  ASSERT_TRUE(getReifDomain()->isFalse());
  
  // 3 * [0, 9] - 5 * [1, 8] <= 4 <-> b
  setLinearExpr({ 3, -5 }, { 0, 9, 1, 8 }, 4);
  postPropagator({ 0, 9, 1, 8 }, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  ASSERT_FALSE(getReifDomain()->isTrue());
  ASSERT_FALSE(getReifDomain()->isFalse());
  
  // 1 * 2 + 2 * 2 <= 6 <-> true => subsumed
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  setReifDomain(true);
  postPropagator({2,2,2,2}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 1 * 2 + 2 * 2 <= 6 <-> false => fail
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  setReifDomain(false);
  postPropagator({2,2,2,2}, PropagationEvent::PROP_EVENT_FAIL);
  
  // 3 * [0, 9] - 5 * [1, 8] <= 4 <-> true
  setLinearExpr({ 3, -5 }, { 0, 9, 1, 8 }, 4);
  setReifDomain(true);
  postPropagator({ 0, 9, 1, 8 }, PropagationEvent::PROP_EVENT_RUN_UNSPEC);

  // 3 * [0, 9] - 5 * [1, 8] <= 4 <-> false
  setLinearExpr({ 3, -5 }, { 0, 9, 1, 8 }, 4);
  setReifDomain(false);
  postPropagator({ 0, 9, 1, 8 }, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
}//FullReifPost

OPTILAB_TEST_F(PropagatorLinearLqReifTest, HalfReifPost)
{
  using namespace Core;
  
  // 1 * 2 + 2 * 2 <= 6 <- b => unspec
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  postPropagator({2,2,2,2}, PropagationEvent::PROP_EVENT_RUN_UNSPEC, HREIF_L);
  ASSERT_FALSE(getReifDomain()->isTrue());
  ASSERT_FALSE(getReifDomain()->isFalse());
  
  // 1 * 2 + 2 * 2 <= 6 -> b => b = true => subsumed
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 10);
  postPropagator({2,2,2,2}, PropagationEvent::PROP_EVENT_SUBSUMED, HREIF_R);
  ASSERT_TRUE(getReifDomain()->isTrue());
  
  // 3 * [0, 9] - 5 * [1, 8] <= 4 <- true post linLq
  setLinearExpr({ 3, -5 }, { 0, 9, 1, 8 }, 4);
  setReifDomain(true);
  postPropagator({ 0, 9, 1, 8 }, PropagationEvent::PROP_EVENT_RUN_UNSPEC, HREIF_L);
  
  // 3 * [0, 9] - 5 * [1, 8] <= -20 -> false post linNq
  setLinearExpr({ 3, -5 }, { 0, 9, 1, 8 }, -20);
  setReifDomain(false);
  postPropagator({ 0, 9, 1, 8 }, PropagationEvent::PROP_EVENT_RUN_UNSPEC, HREIF_R);
  
  // 1 * 2 + 2 * 2 <= 6 <- true => subsumed
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  setReifDomain(true);
  postPropagator({2,2,2,2}, PropagationEvent::PROP_EVENT_SUBSUMED, HREIF_L);
  
  // 1 * 2 + 2 * 2 <= 6 -> false => fail
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  setReifDomain(false);
  postPropagator({2,2,2,2}, PropagationEvent::PROP_EVENT_FAIL, HREIF_R);
}//HalfReifPost

OPTILAB_TEST_F(PropagatorLinearLqReifTest, simpleCases)
{
  using namespace Core;
  
  // 1 * 2 + 2 * 2 <= 6 -> [T, F] => T, subsumed
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  runPropagationAndCheck({2,2,2,2}, true, true, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 1 * 2 + 2 * 2 <= 6 -> [T] => subsumed
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  setReifDomain(true);
  runPropagationAndCheck({2,2,2,2}, true, true, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 1 * 2 + 2 * 2 <= 3 -> [T, F] => F, subsumed
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 3);
  runPropagationAndCheck({2,2,2,2}, false, false, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 1 * 2 + 2 * 2 <= 3 -> [T] => fail
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 3);
  setReifDomain(true);
  runPropagationAndCheck({2,2,2,2}, true, true, PropagationEvent::PROP_EVENT_FAIL);
  
  // 3 * [0, 9] - 5 * [1, 8] = 4 => 3 * [3, 8] - 5 * [1, 4] <= 4
  setLinearExpr({ 3, -5 }, { 0, 9, 1, 8 }, 4);
  runPropagationAndCheck({ 0, 9, 1, 8 }, true, false, PropagationEvent::PROP_EVENT_FIXPOINT);
}//simpleCases

OPTILAB_TEST_F(PropagatorLinearLqReifTest, simpleCasesHL)
{
  using namespace Core;
  
  // 1 * 2 + 2 * 2 <= 6 <- [T, F] => fixpoint
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  runPropagationAndCheck({2,2,2,2}, false, false, PropagationEvent::PROP_EVENT_FIXPOINT, HREIF_L);
  
  // 1 * 2 + 2 * 2 <= 6 <- [F] => fixpoint
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  setReifDomain(false);
  runPropagationAndCheck({2,2,2,2}, false, false, PropagationEvent::PROP_EVENT_FIXPOINT, HREIF_L);
  
  // 1 * 2 + 2 * 2 <= 6 <- [T] => fixpoint
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  setReifDomain(true);
  runPropagationAndCheck({2,2,2,2}, true, true, PropagationEvent::PROP_EVENT_SUBSUMED, HREIF_L);
  
  // 1 * 2 + 2 * 2 <= 3 <- [T, F] => subsumed
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 3);
  runPropagationAndCheck({2,2,2,2}, false, false, PropagationEvent::PROP_EVENT_SUBSUMED, HREIF_L);
  
  // 1 * 2 + 2 * 2 <= 3 <- [F] => subsumed
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 3);
  setReifDomain(false);
  runPropagationAndCheck({2,2,2,2}, false, false, PropagationEvent::PROP_EVENT_SUBSUMED, HREIF_L);
  
  // 1 * 2 + 2 * 2 <= 3 <- [T] => fail
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 3);
  setReifDomain(true);
  runPropagationAndCheck({2,2,2,2}, true, true, PropagationEvent::PROP_EVENT_FAIL, HREIF_L);
  
  // 3 * [0, 9] - 5 * [1, 8] = 4 => 3 * [3, 8] - 5 * [1, 4] <= 4
  setLinearExpr({ 3, -5 }, { 0, 9, 1, 8 }, 4);
  runPropagationAndCheck({ 0, 9, 1, 8 }, true, false, PropagationEvent::PROP_EVENT_FIXPOINT, HREIF_L);
}//simpleCasesHL

OPTILAB_TEST_F(PropagatorLinearLqReifTest, simpleCasesHR)
{
  using namespace Core;
  
  // 1 * 2 + 2 * 2 == 6 -> [T, F] => subsumed
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  runPropagationAndCheck({2,2,2,2}, true, true, PropagationEvent::PROP_EVENT_SUBSUMED, HREIF_R);
  
  // 1 * 2 + 2 * 2 == 6 -> [T] => subsumed
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  setReifDomain(true);
  runPropagationAndCheck({2,2,2,2}, true, true, PropagationEvent::PROP_EVENT_SUBSUMED, HREIF_R);
  
  // 1 * 2 + 2 * 2 <= 3 -> [T, F] => fixpoint
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 3);
  runPropagationAndCheck({2,2,2,2}, true, false, PropagationEvent::PROP_EVENT_FIXPOINT, HREIF_R);
  
  // 1 * 2 + 2 * 2 <= 3 -> [T] => fixpoint
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 3);
  setReifDomain(true);
  runPropagationAndCheck({2,2,2,2}, true, true, PropagationEvent::PROP_EVENT_FIXPOINT, HREIF_R);
  
  // 3 * [0, 9] - 5 * [1, 8] = 4 => 3 * [3, 8] - 5 * [1, 4] <= 4
  setLinearExpr({ 3, -5 }, { 0, 9, 1, 8 }, 4);
  runPropagationAndCheck({ 0, 9, 1, 8 }, true, false, PropagationEvent::PROP_EVENT_FIXPOINT, HREIF_R);
}//simpleCasesHR

OPTILAB_TEST_F(PropagatorLinearLqReifTest, getFitnessReif)
{
  using namespace Core;
  
  // 1 * 2 + 2 * 2 <= 6 <-> [F, T] => -1
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness());
  
  // 1 * 2 + 2 * 2 <= 6 <-> T => 0
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  setReifDomain(true);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());
  
  // 1 * 2 + 1 * 2 <= 6 <-> T => 0
  setLinearExpr({1, 1}, {2, 2, 2, 2}, 6);
  setReifDomain(true);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());
  
  // 1 * 2 + 2 * 2 <= 6 <-> F => 1
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  setReifDomain(false);
  EXPECT_EQ(1, getFitness());
  
  // 1 * 2 + 2 * [2, 3] <= 6 <-> T => -1
  setLinearExpr({1, 2}, {2, 2, 2, 3}, 6);
  setReifDomain(true);
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness());
  
  // 1 * 2 + 3 * 2 <= 6 <-> T => 2
  setLinearExpr({1, 3}, {2, 2, 2, 2}, 6);
  setReifDomain(true);
  EXPECT_EQ(2, getFitness());
}//getFitnessReif

OPTILAB_TEST_F(PropagatorLinearLqReifTest, getFitnessReifLeft)
{
  using namespace Core;
  
  // 1 * 2 + 2 * 2 <= 6 <- [F, T] => -1
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness(HREIF_L));
  
  // 1 * 2 + 2 * 2 <= 6 <- T => 0
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  setReifDomain(true);
  EXPECT_EQ(0, getFitness(HREIF_L));
  
  // 1 * 2 + 2 * 2 <= 6 <- F => 0
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  setReifDomain(false);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness(HREIF_L));
  
  // 1 * 2 + 3 * 2 <= 6 <- F => 0
  setLinearExpr({1, 3}, {2, 2, 2, 2}, 6);
  setReifDomain(false);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness(HREIF_L));
  
  // 1 * 2 + 2 * [2, 3] <= 6 <- T => -1
  setLinearExpr({1, 2}, {2, 2, 2, 3}, 6);
  setReifDomain(true);
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness(HREIF_L));
  
  // 1 * 2 + 3 * 2 <= 6 <- T => 2
  setLinearExpr({1, 3}, {2, 2, 2, 2}, 6);
  setReifDomain(true);
  EXPECT_EQ(2, getFitness(HREIF_L));
}//getFitnessReifLeft

OPTILAB_TEST_F(PropagatorLinearLqReifTest, getFitnessReifRight)
{
  using namespace Core;
  
  // 1 * 2 + 2 * 2 <= 6 -> [F, T] => -1
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness(HREIF_R));
  
  // 1 * 2 + 2 * 2 <= 6 -> T => 0
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  setReifDomain(true);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness(HREIF_R));
  
  // 1 * 2 + 2 * 2 <= 6 -> F => 1
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  setReifDomain(false);
  EXPECT_EQ(1, getFitness(HREIF_R));
  
  // 1 * 2 + 3 * 2 <= 6 -> F => 0
  setLinearExpr({1, 3}, {2, 2, 2, 2}, 6);
  setReifDomain(false);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness(HREIF_R));
  
  // 1 * 2 + 3 * 2 <= 6 -> T => 0
  setLinearExpr({1, 3}, {2, 2, 2, 2}, 6);
  setReifDomain(true);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness(HREIF_R));
  
  // 1 * 2 + 2 * [2, 3] <= 6 -> T => -1
  setLinearExpr({1, 2}, {2, 2, 2, 3}, 6);
  setReifDomain(true);
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness(HREIF_R));
}//getFitnessReifRight

