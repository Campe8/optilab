
#include "utest_globals.hpp"

#include "DomainDefs.hpp"
#include "PropagatorLinearNq.hpp"
#include "ConstraintTestInc.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"
#include "DArrayMatrix.hpp"

#include <vector>
#include <memory>
#include <cassert>

namespace {
  class PropagatorLinearNqTest : public ::testing::Test {
  public:
    void setLinearExpr(std::vector<INT_64>&& aCoeff,
                       std::vector<INT_64>&& aDomMinMax,
                       INT_64 aLinearConstant)
    {
      assert(aCoeff.size() * 2 == aDomMinMax.size());
      pCoeff = std::make_shared<Core::DomainElementArray>(aCoeff.size());
      pDomains = std::make_shared<Core::DomainArray>(aDomMinMax.size()/2);
      
      // Coefficients
      std::size_t idx{0};
      pTempCoeffArray.clear();
      for(auto& coeff : aCoeff)
      {
        pTempCoeffArray.push_back(getElem(coeff));
        pCoeff->assignElementToCell(idx++, pTempCoeffArray.back().get());
      }
      
      // Domains
      idx = 0;
      pTempDomainArray.clear();
      for(std::size_t domIdx = 0; domIdx < aDomMinMax.size(); ++domIdx)
      {
        if(domIdx > 0 && (domIdx % 2 != 0))
        {
          pTempDomainArray.push_back(getDomainInt(aDomMinMax[domIdx-1], aDomMinMax[domIdx]));
          pDomains->assignElementToCell(idx++, pTempDomainArray.back().get());
        }
      }
      
      // Constant
      pConstant = getElem(aLinearConstant);
    }//setLinearExpr
    
    void postPropagator(std::vector<INT_64>&& aDomMinMax, Core::PropagationEvent aEvent)
    {
      std::unique_ptr<Core::PropagatorLinearNq> propagator(new Core::PropagatorLinearNq(*(pCoeff.get()),
                                                                                        *(pDomains.get()),
                                                                                        pConstant.get()));
      
      Core::PropagationEvent event = propagator->post();
      ASSERT_EQ(event, aEvent);
      
      std::size_t domIdxToCompare{0};
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        for(std::size_t domIdx = 0; domIdx < aDomMinMax.size(); ++domIdx)
        {
          if(domIdx > 0 && (domIdx % 2 != 0))
          {
            DomainIntPtr domain = getDomainInt(aDomMinMax[domIdx-1], aDomMinMax[domIdx]);
            ASSERT_TRUE(pDomains->operator[](domIdxToCompare)->lowerBound()->isEqual(domain->lowerBound()));
            ASSERT_TRUE(pDomains->operator[](domIdxToCompare)->upperBound()->isEqual(domain->upperBound()));
            domIdxToCompare++;
          }
        }
      }
    }//postPropagator
    
    double getFitness(std::vector<INT_64>&& aDomMinMax)
    {
      std::unique_ptr<Core::PropagatorLinearNq> propagator(new Core::PropagatorLinearNq(*(pCoeff.get()),
                                                                                        *(pDomains.get()),
                                                                                        pConstant.get()));
      return propagator->getFitness();
    }//getFitness
    
    void runPropagationAndCheck(std::vector<INT_64>&& aDomMinMax,
                                Core::PropagationEvent aEvent)
    {
      // Perform propagation
      std::unique_ptr<Core::PropagatorLinearNq> propagator(new Core::PropagatorLinearNq(*(pCoeff.get()),
                                                                                        *(pDomains.get()),
                                                                                        pConstant.get()));
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      
      std::size_t domIdxToCompare{0};
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        for(std::size_t domIdx = 0; domIdx < aDomMinMax.size(); ++domIdx)
        {
          if(domIdx > 0 && (domIdx % 2 != 0))
          {
            DomainIntPtr domain = getDomainInt(aDomMinMax[domIdx-1], aDomMinMax[domIdx]);
            ASSERT_TRUE(pDomains->operator[](domIdxToCompare)->lowerBound()->isEqual(domain->lowerBound()));
            ASSERT_TRUE(pDomains->operator[](domIdxToCompare)->upperBound()->isEqual(domain->upperBound()));
            domIdxToCompare++;
          }
        }
      }
    }//runPropagationAndCheck
        
  protected:
    // Coefficients array
    Core::DomainElementArraySPtr pCoeff;
    // Domain array
    Core::DomainArraySPtr pDomains;
    // Linear Constat
    ElemPtr pConstant;
    
    // Array of shared ptr used to preserve coefficients
    std::vector<ElemPtr> pTempCoeffArray;
    
    // Array of shared ptr used to preserve domains
    std::vector<DomainIntPtr> pTempDomainArray;
  };
}// end namespace


/*********************** Test ***********************/

OPTILAB_TEST_F(PropagatorLinearNqTest, post)
{
  using namespace Core;
  
  // 1 * 2 + 2 * 2 != 6 => fail
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  postPropagator({2,2,2,2},PropagationEvent::PROP_EVENT_FAIL);
  
  // 1 * 2 + 2 * 2 != 7 => subsumed
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 7);
  postPropagator({2,2,2,2},PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 3 * [0, 9] - 5 * [1, 8] != 4
  setLinearExpr({ 3, -5 }, { 0, 9, 1, 8 }, 4);
  postPropagator({ 0, 9, 1, 8 }, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
}//post

OPTILAB_TEST_F(PropagatorLinearNqTest, simpleCases)
{
  using namespace Core;
  
  // 1 * 2 + 2 * 2 != 6
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  runPropagationAndCheck({2,2,2,2},PropagationEvent::PROP_EVENT_FAIL);
  
  // 1 * 2 + 2 * 2 != 7
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 7);
  runPropagationAndCheck({2,2,2,2},PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 3 * [0, 9] - 5 * [1, 8] = 4 => 3 * [3, 8] - 5 * [1, 4] != 4
  setLinearExpr({ 3, -5 }, { 0, 9, 1, 8 }, 4);
  runPropagationAndCheck({ 0, 9, 1, 8 }, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // 10 * [0, 9] + 15 * [1, 8] != -1
  setLinearExpr({ 10, 15 }, { 0, 9, 1, 8 }, -1);
  runPropagationAndCheck({ 0, 9, 1, 8 }, PropagationEvent::PROP_EVENT_SUBSUMED);
}//simpleCases

OPTILAB_TEST_F(PropagatorLinearNqTest, oneNonSingleton)
{
  using namespace Core;
  
  // 1 * 3 + -1 * [2, 3] != 1
  setLinearExpr({1, -1}, {3, 3, 2, 3}, 1);
  runPropagationAndCheck({3,3,3,3},PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 1 * 3 + -1 * [2, 3] != -10
  setLinearExpr({1, -1}, {3, 3, 2, 3}, -10);
  runPropagationAndCheck({3,3,2,3},PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 1 * 3 - 1 * [2, 3] + 2 * (-5) != -10
  setLinearExpr({1, -1, 2}, {3, 3, 2, 3, -5, -5}, -10);
  runPropagationAndCheck({3,3,2,2,-5,-5},PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // -1 * 3 + 1 * [2, 3] + 2 * 5 != 10
  setLinearExpr({-1, 1, 2}, {3, 3, 2, 3, 5, 5}, 10);
  runPropagationAndCheck({3,3,2,2,5,5},PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // 1 * 3 - 1 * [2, 4] + 2 * (-5) != -10
  setLinearExpr({1, -1, 2}, {3, 3, 2, 4, -5, -5}, -10);
  runPropagationAndCheck({3,3,2,4,-5,-5},PropagationEvent::PROP_EVENT_SUBSUMED);
}//oneNonSingleton

OPTILAB_TEST_F(PropagatorLinearNqTest, getFitness)
{
  // Test fitness function
  using namespace Core;
  
  // Satisfied constraint
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 6);
  EXPECT_EQ(1, getFitness({2, 2, 2, 2}));
  
  // Unsatisfied constraint
  setLinearExpr({1, 2}, {2, 2, 2, 2}, 7);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness({2, 2, 2, 2}));
  
  // Invalid fitness
  setLinearExpr({1, 2}, {2, 3, 2, 2}, 7);
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness({2, 2, 2, 2}));
}//getFitness
