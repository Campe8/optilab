
#include "utest_globals.hpp"

#include "PropagatorNogood.hpp"
#include "ConstraintTestInc.hpp"
#include "DomainElementManager.hpp"
#include "DomainElementInt64.hpp"

namespace Core {
  
  class PropagatorNogoodTest : public PropagatorNogood {
  public:
    PropagatorNogoodTest(const DomainArraySPtr& aDomainArray, const DomainElementArraySPtr& aDomainElementArray)
    : PropagatorNogood(aDomainArray, aDomainElementArray)
    {
    }
    
    PropagationEvent runPropagation()
    {
      return PropagatorNogood::runPropagation();
    }
  };
  
}// end namespace Core

OPTILAB_TEST(PropagatorNogood, ReevaluationEventSet)
{
  // Test re-evaluation event set for nogood constraint
  using namespace Core;
  
  DomainIntPtr domain = getDomainInt(1, 2);
  auto domainArray = getDomainArray({domain});
  auto elementArray = getArray({1});
  
  // The re-evaluation set should have size of one
  PropagatorNogood nogood(domainArray, elementArray);
  ASSERT_EQ(1, nogood.getReevaluationEventSet().size());
  
  // The ecvent should be singleton
  auto reevalSet = nogood.getReevaluationEventSet();
  auto event = *reevalSet.begin();
  ASSERT_EQ(DomainEventType::SINGLETON_EVENT, event->getEventType());
}//ReevaluationEventSet

OPTILAB_TEST(PropagatorNogood, postPropagatorSubsumed)
{
  // Test posting nogood with subsumed event
  using namespace Core;
  
  DomainIntPtr domain1 = getDomainInt(1, 1);
  DomainIntPtr domain2 = getDomainInt(2, 2);
  auto domainArray = getDomainArray({domain1, domain2});
  auto elementArray = getArray({2, 1});
  
  // Post a nogood constraint on {1, 1} != {2} \/ {2, 2} != {1}
  PropagatorNogood nogood(domainArray, elementArray);
  ASSERT_EQ(PropagationEvent::PROP_EVENT_SUBSUMED, nogood.post());
}//postPropagatorSubsumed

OPTILAB_TEST(PropagatorNogood, postPropagatorUnspecified)
{
  // Test posting nogood with unspecified event since one of the domains is not singleton
  using namespace Core;
  
  DomainIntPtr domain1 = getDomainInt(1, 1);
  DomainIntPtr domain2 = getDomainInt(2, 3);
  auto domainArray = getDomainArray({domain1, domain2});
  auto elementArray = getArray({1, 1});
  
  // Post a nogood constraint on {1, 1} != {1} \/ {2, 3} != {1}.
  // @note the first domain is equal to its corresponding domain element
  PropagatorNogood nogood(domainArray, elementArray);
  ASSERT_EQ(PropagationEvent::PROP_EVENT_RUN_UNSPEC, nogood.post());
}//postPropagatorUnspecified

OPTILAB_TEST(PropagatorNogood, postPropagatorFail)
{
  // Test posting nogood with fail event since all domains are equal
  // to corresponding domain elements
  using namespace Core;
  
  DomainIntPtr domain1 = getDomainInt(1, 1);
  DomainIntPtr domain2 = getDomainInt(2, 2);
  auto domainArray = getDomainArray({domain1, domain2});
  auto elementArray = getArray({1, 2});
  
  // Post a nogood constraint on {1, 1} != {1} \/ {2, 2} != {2}.
  PropagatorNogood nogood(domainArray, elementArray);
  ASSERT_EQ(PropagationEvent::PROP_EVENT_FAIL, nogood.post());
}//postPropagatorFail

OPTILAB_TEST(PropagatorNogood, runPropagatorSubsumed)
{
  // Test running nogood with subsumed event
  using namespace Core;
  
  DomainIntPtr domain1 = getDomainInt(1, 1);
  DomainIntPtr domain2 = getDomainInt(2, 2);
  auto domainArray = getDomainArray({domain1, domain2});
  auto elementArray = getArray({2, 1});
  
  // Post a nogood constraint on {1, 1} != {2} \/ {2, 2} != {1}
  PropagatorNogoodTest nogood(domainArray, elementArray);
  ASSERT_EQ(PropagationEvent::PROP_EVENT_SUBSUMED, nogood.runPropagation());
}//runPropagatorSubsumed

OPTILAB_TEST(PropagatorNogood, runPropagatorFail)
{
  // Test running nogood with fail event since all domains are equal
  // to corresponding domain elements
  using namespace Core;
  
  DomainIntPtr domain1 = getDomainInt(1, 1);
  DomainIntPtr domain2 = getDomainInt(2, 2);
  auto domainArray = getDomainArray({domain1, domain2});
  auto elementArray = getArray({1, 2});
  
  // Propagate a nogood constraint on {1, 1} != {1} \/ {2, 2} != {2}.
  PropagatorNogoodTest nogood(domainArray, elementArray);
  ASSERT_EQ(PropagationEvent::PROP_EVENT_FAIL, nogood.runPropagation());
}//postPropagatorFail

OPTILAB_TEST(PropagatorNogood, runPropagatorFixpoint)
{
  // Test running nogood with fixpoint event when more than one domain is not ground
  using namespace Core;
  
  DomainIntPtr domain1 = getDomainInt(1, 2);
  DomainIntPtr domain2 = getDomainInt(2, 3);
  auto domainArray = getDomainArray({domain1, domain2});
  auto elementArray = getArray({1, 2});
  
  PropagatorNogoodTest nogood(domainArray, elementArray);
  ASSERT_EQ(PropagationEvent::PROP_EVENT_FIXPOINT, nogood.runPropagation());
}//runPropagatorFixpoint

OPTILAB_TEST(PropagatorNogood, runPropagatorPropagate)
{
  // Test running nogood with subsumed, propagating on the only
  // domain which is not singleton
  using namespace Core;
  
  DomainIntPtr domain1 = getDomainInt(1, 1);
  DomainIntPtr domain2 = getDomainInt(2, 3);
  auto domainArray = getDomainArray({domain1, domain2});
  auto elementArray = getArray({1, 2});
  
  PropagatorNogoodTest nogood(domainArray, elementArray);
  ASSERT_EQ(PropagationEvent::PROP_EVENT_SUBSUMED, nogood.runPropagation());
  
  // Check the domain2 which should be singleton and equal to {3}
  ASSERT_EQ(1, domain2->getSize());
  ASSERT_TRUE(domain2->lowerBound()->isEqual(DomainElementManager::getInstance().createDomainElementInt64(3)));
}//runPropagatorPropagate

OPTILAB_TEST(PropagatorNogood, getFitnessMin)
{
  // Test running nogood fitness function on with subsumed propagator
  using namespace Core;
  
  DomainIntPtr domain1 = getDomainInt(1, 1);
  DomainIntPtr domain2 = getDomainInt(2, 2);
  auto domainArray = getDomainArray({domain1, domain2});
  auto elementArray = getArray({2, 1});
  
  // Post a nogood constraint on {1, 1} != {2} \/ {2, 2} != {1}
  PropagatorNogoodTest nogood(domainArray, elementArray);
  ASSERT_EQ(Propagator::getMinFitness(), nogood.getFitness());
}//getFitnessMin

OPTILAB_TEST(PropagatorNogood, getFitnessMax)
{
  // Test running nogood fitness function on with failed propagator
  using namespace Core;
  
  DomainIntPtr domain1 = getDomainInt(1, 1);
  DomainIntPtr domain2 = getDomainInt(2, 2);
  auto domainArray = getDomainArray({domain1, domain2});
  auto elementArray = getArray({1, 2});
  
  // Propagate a nogood constraint on {1, 1} != {1} \/ {2, 2} != {2}.
  PropagatorNogoodTest nogood(domainArray, elementArray);
  ASSERT_EQ(1.0, nogood.getFitness());
}//getFitnessMax

OPTILAB_TEST(PropagatorNogood, getFitnessUnspec)
{
  // Test running nogood fitness function on non ground variables
  using namespace Core;
  
  DomainIntPtr domain1 = getDomainInt(1, 5);
  DomainIntPtr domain2 = getDomainInt(2, 3);
  auto domainArray = getDomainArray({domain1, domain2});
  auto elementArray = getArray({1, 2});
  
  // Propagate a nogood constraint on {1, 1} != {1} \/ {2, 2} != {2}.
  PropagatorNogoodTest nogood(domainArray, elementArray);
  ASSERT_EQ(Propagator::getUnspecFitness(), nogood.getFitness());
}//getFitnessUnspec
