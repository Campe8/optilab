
#include "utest_globals.hpp"

#include "PropagatorBinaryReif.hpp"
#include "PropagatorReif.hpp"
#include "ConstraintTestInc.hpp"

namespace Core {
  
  class PropagatorBinaryReifTest : public PropagatorReif {
  public:
    PropagatorBinaryReifTest(DomainBoolean *aDomBool)
    : PropagatorReif(aDomBool)
    {
    }
    
    ~PropagatorBinaryReifTest()
    {
    }
    
    /// Expose method for testing
    DomainBoolean& getBoolDomainConfig() const
    {
      return *PropagatorReif::GetContextReifDomain();
    }
  };
}// end namespace Core

/*********************** Test ***********************/

OPTILAB_TEST(PropagatorBinaryReif, getBoolDomainConfig)
{
  using namespace Core;
  
  DomainBoolPtr DP1 = getDomainBool();
  
  PropagatorBinaryReifTest ptXX1(DP1.get());
  
  ASSERT_EQ(ptXX1.getBoolDomainConfig().getDomainType(), DomainClass::DOM_BOOL);
}//getBoolDomainConfig
