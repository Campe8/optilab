
#include "utest_globals.hpp"

#include "PropagatorReif.hpp"
#include "ConstraintTestInc.hpp"

namespace Core {
  
  class PropagatorReifTest : public PropagatorReif {
  public:
    PropagatorReifTest(DomainBoolean *aDomBool)
    : PropagatorReif(aDomBool)
    {
    }
    
    ~PropagatorReifTest()
    {
    }
    
    /// Expose methods for testing
    void setReifDomain(DomainBoolean *aDomainBool)
    {
      PropagatorReif::setContextReifDomain(aDomainBool);
    }
    
    DomainBoolean& getBoolDomain() const
    {
      return *PropagatorReif::GetContextReifDomain();
    }
    
    bool isSingleton()
    {
      return PropagatorReif::isContextReifSingleton();
    }
    
    bool isTrue()
    {
      return PropagatorReif::isContextReifTrue();
    }
    
    bool isFalse()
    {
      return PropagatorReif::isContextReifFalse();
    }
  };
}// end namespace core

OPTILAB_TEST(PropagatorReif, basics)
{
  using namespace Core;
  
  DomainBoolPtr DP1 = getDomainBool();
  
  PropagatorReifTest ptXX1(DP1.get());
  
  ASSERT_EQ(ptXX1.getBoolDomain().getDomainType(), DomainClass::DOM_BOOL);
  
  DomainBoolean pDomainReif;
  pDomainReif.setTrue();
  ptXX1.setReifDomain(&pDomainReif);
  ASSERT_TRUE(ptXX1.isSingleton());
  ASSERT_TRUE(ptXX1.isTrue());
  ASSERT_FALSE(ptXX1.isFalse());
  
}//basics
