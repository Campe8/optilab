
#include "utest_globals.hpp"

#include "PropagatorDomain.hpp"
#include "ConstraintTestInc.hpp"

#include <vector>
#include <memory>
#include <cassert>

namespace {
  using PropagatorPtr = std::unique_ptr<Core::PropagatorDomain>;
  
  class PropagatorDomainTest : public ::testing::Test {
  public:
    void setLinearExpr(std::vector<INT_64>&& aDomMinMax,
                       INT_64 aLowerBound,
                       INT_64 aUpperBound)
    {
      pDomains = std::make_shared<Core::DomainArray>(aDomMinMax.size()/2);
      
      // Domains
      size_t idx = 0;
      pTempDomainArray.clear();
      for(std::size_t domIdx = 0; domIdx < aDomMinMax.size(); ++domIdx)
      {
        if(domIdx > 0 && (domIdx % 2 != 0))
        {
          pTempDomainArray.push_back(getDomainInt(aDomMinMax[domIdx-1], aDomMinMax[domIdx]));
          pDomains->assignElementToCell(idx++, pTempDomainArray.back().get());
        }
      }
      
      // Constants
      pLowerBound = getElem(aLowerBound);
      pUpperBound = getElem(aUpperBound);
    }//setLinearExpr
    
    double getFitness()
    {
      PropagatorPtr propagator(new Core::PropagatorDomain(*(pDomains.get()), pLowerBound.get(),
                                                          pUpperBound.get()));
      return propagator->getFitness();
    }//getFitness
    
    void postPropagator(std::vector<INT_64>&& aDomMinMax,
                        Core::PropagationEvent aEvent)
    {
      PropagatorPtr propagator(new Core::PropagatorDomain(*(pDomains.get()), pLowerBound.get(),
                                                          pUpperBound.get()));
      
      Core::PropagationEvent event = propagator->post();
      ASSERT_EQ(event, aEvent);
      
      std::size_t domIdxToCompare{0};
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        for(std::size_t domIdx = 0; domIdx < aDomMinMax.size(); ++domIdx)
        {
          if(domIdx > 0 && (domIdx % 2 != 0))
          {
            DomainIntPtr domain = getDomainInt(aDomMinMax[domIdx-1], aDomMinMax[domIdx]);
            ASSERT_TRUE(pDomains->operator[](domIdxToCompare)->lowerBound()->isEqual(domain->lowerBound()));
            ASSERT_TRUE(pDomains->operator[](domIdxToCompare)->upperBound()->isEqual(domain->upperBound()));
            domIdxToCompare++;
          }
        }
      }
    }//postPropagator
    
    void runPropagationAndCheck(std::vector<INT_64>&& aDomMinMax,
                                Core::PropagationEvent aEvent)
    {
      // Perform propagation
      PropagatorPtr propagator(new Core::PropagatorDomain(*(pDomains.get()), pLowerBound.get(),
                                                          pUpperBound.get()));
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      
      std::size_t domIdxToCompare{0};
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        for(std::size_t domIdx = 0; domIdx < aDomMinMax.size(); ++domIdx)
        {
          if(domIdx > 0 && (domIdx % 2 != 0))
          {
            DomainIntPtr domain = getDomainInt(aDomMinMax[domIdx-1], aDomMinMax[domIdx]);
            ASSERT_TRUE(pDomains->operator[](domIdxToCompare)->lowerBound()->isEqual(domain->lowerBound()));
            ASSERT_TRUE(pDomains->operator[](domIdxToCompare)->upperBound()->isEqual(domain->upperBound()));
            domIdxToCompare++;
          }
        }
      }
    }//runPropagationAndCheck
        
  protected:
    // Domain array
    Core::DomainArraySPtr pDomains;
    
    // Constants
    ElemPtr pLowerBound;
    ElemPtr pUpperBound;
    
    // Array of shared ptr used to preserve domains
    std::vector<DomainIntPtr> pTempDomainArray;
  };
}// end namespace


/*********************** Test ***********************/

OPTILAB_TEST_F(PropagatorDomainTest, post)
{
  using namespace Core;
  
  // ([2,2],[2,2]) 1 6 -> subsumed
  setLinearExpr({2, 2, 2, 2}, 1, 6);
  postPropagator({2,2,2,2}, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // ([2,2],[2,2]) 3 6 -> fail
  setLinearExpr({2, 2, 2, 2}, 3, 6);
  runPropagationAndCheck({2,2,2,2}, PropagationEvent::PROP_EVENT_FAIL);
}//post

OPTILAB_TEST_F(PropagatorDomainTest, simpleCases)
{
  using namespace Core;
  
  // ([2,2],[2,2]) 1 6 -> subsumed
  setLinearExpr({2, 2, 2, 2}, 1, 6);
  runPropagationAndCheck({2,2,2,2},PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // ([2,2],[2,2]) 3 6 -> fail
  setLinearExpr({2, 2, 2, 2}, 3, 6);
  runPropagationAndCheck({2,2,2,2},PropagationEvent::PROP_EVENT_FAIL);
  
  // ([3,10],[-9,-3]) -3 3 -> ([3,3],[-3,-3]) subsumed
  setLinearExpr({3, 10, -9, -3}, -3, 3);
  runPropagationAndCheck({3,3,-3,-3},PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // ([2,10],[-10,90]) 1 6 -> ([2,6],[1,6]) subsumed
  setLinearExpr({2, 10, -10, 90}, 1, 6);
  runPropagationAndCheck({2,6,1,6},PropagationEvent::PROP_EVENT_SUBSUMED);
}//simpleCases

OPTILAB_TEST_F(PropagatorDomainTest, getFitness)
{
  using namespace Core;
  
  // (2, 2) 1 6 -> 0
  setLinearExpr({2, 2, 2, 2}, 1, 6);
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());
  
  // (2, 2) 3 6 -> 1 + 1 = 2
  setLinearExpr({2, 2, 2, 2}, 3, 6);
  EXPECT_EQ(2, getFitness());
  
  // ([3,10], [-9,-3]) -3 3 -> -1
  setLinearExpr({3, 10, -9, -3}, -3, 3);
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness());
  
  // (7, 8) 3 6 -> 1 + 2 = 3
  setLinearExpr({7, 7, 8, 8}, 3, 6);
  EXPECT_EQ(3, getFitness());
  
  // (2, 7) 3 6 -> 1 + 1 = 2
  setLinearExpr({2, 2, 7, 7}, 3, 6);
  EXPECT_EQ(2, getFitness());
}//getFitness
