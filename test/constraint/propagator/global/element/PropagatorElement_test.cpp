#include "utest_globals.hpp"

#include "PropagatorElement.hpp"
#include "ConstraintTestInc.hpp"

namespace {
  class PropagatorElementTest : public ::testing::Test {
  public:
    PropagatorElementTest()
    {
    }
    
    ~PropagatorElementTest()
    {
    }
    
    void setDomains(std::array<INT_64, 2>&& aDomIdx,
                    std::vector<INT_64>&& aDomArray,
                    std::array<INT_64, 2>&& aDom)
    {
      pDomIdx = getDomainInt(aDomIdx[0], aDomIdx[1]);
      pDomArray = std::make_shared<Core::DomainArray>(aDomArray.size()/2);
      pDom = getDomainInt(aDom[0], aDom[1]);
      
      // pDomArray
      std::size_t idx{0};
      pTempDomainArray.clear();
      for(std::size_t domIdx = 0; domIdx < aDomArray.size(); ++domIdx)
      {
        if(domIdx > 0 && (domIdx % 2 != 0))
        {
          pTempDomainArray.push_back(getDomainInt(aDomArray[domIdx-1], aDomArray[domIdx]));
          pDomArray->assignElementToCell(idx++, pTempDomainArray.back().get());
        }
      }
    }//setDomains
    
    void setDomainsElem(std::array<INT_64, 2>&& aDomIdx,
                        std::vector<INT_64>&& aDomElemArray,
                        std::array<INT_64, 2>&& aDom)
    {
      pDomIdx = getDomainInt(aDomIdx[0], aDomIdx[1]);
      pDomElemArray = std::make_shared<Core::DomainElementArray>(aDomElemArray.size());
      pDom = getDomainInt(aDom[0], aDom[1]);
      
      // pDomElemArray
      std::size_t idx{0};
      pTempDomainElemArray.clear();
      for(auto& elem : aDomElemArray)
      {
        pTempDomainElemArray.push_back(getElem(elem));
        pDomElemArray->assignElementToCell(idx++, pTempDomainElemArray.back().get());
      }
    }//setDomainsElem
    
    double getFitness()
    {
      std::unique_ptr<Core::PropagatorElement> propagator(nullptr);
      propagator.reset(new Core::PropagatorElement(pDomIdx.get(),
                                                   *(pDomArray.get()),
                                                   pDom.get()));
      return propagator->getFitness();
    }//getFitness
    
    void postPropagatorDomains(std::array<INT_64, 2>&& aDomIdx,
                               std::vector<INT_64>&& aDomArray,
                               std::array<INT_64, 2>&& aDom,
                               Core::PropagationEvent aEvent)
    {
      std::unique_ptr<Core::PropagatorElement> propagator(new Core::PropagatorElement(pDomIdx.get(),
                                                                                      *(pDomArray.get()),
                                                                                      pDom.get()));
      
      Core::PropagationEvent event = propagator->post();
      ASSERT_EQ(event, aEvent);
      if(event == Core::PropagationEvent::PROP_EVENT_FAIL) return;
      
      DomainIntPtr domain;
      
      // check pDomIdx
      domain = getDomainInt(aDomIdx[0], aDomIdx[1]);
      ASSERT_TRUE(pDomIdx->lowerBound()->isEqual(domain->lowerBound()));
      ASSERT_TRUE(pDomIdx->upperBound()->isEqual(domain->upperBound()));
      
      // check pDomArray
      std::size_t domIdxToCompare{0};
      for(std::size_t domIdx = 1; domIdx < aDomArray.size(); ++domIdx)
      {
        if(domIdx > 0 && (domIdx % 2 != 0))
        {
          domain = getDomainInt(aDomArray[domIdx-1], aDomArray[domIdx]);
          ASSERT_TRUE(pDomArray->operator[](domIdxToCompare)->lowerBound()->isEqual(domain->lowerBound()));
          ASSERT_TRUE(pDomArray->operator[](domIdxToCompare)->upperBound()->isEqual(domain->upperBound()));
          domIdxToCompare++;
        }
      }
      
      // check pDom
      domain = getDomainInt(aDom[0], aDom[1]);
      ASSERT_TRUE(pDom->lowerBound()->isEqual(domain->lowerBound()));
      ASSERT_TRUE(pDom->upperBound()->isEqual(domain->upperBound()));
    }//postPropagatorDomain
    
    void postPropagatorDomainsElem(std::array<INT_64, 2>&& aDomIdx,
                                   std::array<INT_64, 2>&& aDom,
                                   Core::PropagationEvent aEvent)
    {
      std::unique_ptr<Core::PropagatorElement> propagator(new Core::PropagatorElement(pDomIdx.get(),
                                                                                      *(pDomElemArray.get()),
                                                                                      pDom.get()));
      
      Core::PropagationEvent event = propagator->post();
      ASSERT_EQ(event, aEvent);
      if(event == Core::PropagationEvent::PROP_EVENT_FAIL) return;
      
      DomainIntPtr domain;
      
      // check pDomIdx
      domain = getDomainInt(aDomIdx[0], aDomIdx[1]);
      ASSERT_TRUE(pDomIdx->lowerBound()->isEqual(domain->lowerBound()));
      ASSERT_TRUE(pDomIdx->upperBound()->isEqual(domain->upperBound()));
      
      // check pDom
      domain = getDomainInt(aDom[0], aDom[1]);
      ASSERT_TRUE(pDom->lowerBound()->isEqual(domain->lowerBound()));
      ASSERT_TRUE(pDom->upperBound()->isEqual(domain->upperBound()));
    }//postPropagatorDomainsElem
    
    void runPropagationAndCheckDomains(std::array<INT_64, 2>&& aDomIdx,
                                std::vector<INT_64>&& aDomArray,
                                std::array<INT_64, 2>&& aDom,
                                Core::PropagationEvent aEvent)
    {
      // Perform propagation
      std::unique_ptr<Core::PropagatorElement> propagator(new Core::PropagatorElement(pDomIdx.get(),
                                                                                      *(pDomArray.get()),
                                                                                      pDom.get()));
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      if(event == Core::PropagationEvent::PROP_EVENT_FAIL) return;
      
      DomainIntPtr domain;
      
      // check pDomIdx
      domain = getDomainInt(aDomIdx[0], aDomIdx[1]);
      ASSERT_TRUE(pDomIdx->lowerBound()->isEqual(domain->lowerBound()));
      ASSERT_TRUE(pDomIdx->upperBound()->isEqual(domain->upperBound()));
      
      // check pDomArray
      std::size_t domIdxToCompare{0};
      for(std::size_t domIdx = 1; domIdx < aDomArray.size(); ++domIdx)
      {
        if(domIdx > 0 && (domIdx % 2 != 0))
        {
          domain = getDomainInt(aDomArray[domIdx-1], aDomArray[domIdx]);
          ASSERT_TRUE(pDomArray->operator[](domIdxToCompare)->lowerBound()->isEqual(domain->lowerBound()));
          ASSERT_TRUE(pDomArray->operator[](domIdxToCompare)->upperBound()->isEqual(domain->upperBound()));
          domIdxToCompare++;
        }
      }
      
      // check pDom
      domain = getDomainInt(aDom[0], aDom[1]);
      ASSERT_TRUE(pDom->lowerBound()->isEqual(domain->lowerBound()));
      ASSERT_TRUE(pDom->upperBound()->isEqual(domain->upperBound()));
    }//runPropagationAndCheckDomains
    
    void runPropagationAndCheckDomainsElem(std::array<INT_64, 2>&& aDomIdx,
                                           std::array<INT_64, 2>&& aDom,
                                           Core::PropagationEvent aEvent)
    {
      std::unique_ptr<Core::PropagatorElement> propagator(new Core::PropagatorElement(pDomIdx.get(),
                                                                                      *(pDomElemArray.get()),
                                                                                      pDom.get()));
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      if(event == Core::PropagationEvent::PROP_EVENT_FAIL) return;
      
      DomainIntPtr domain;
      
      // check pDomIdx
      domain = getDomainInt(aDomIdx[0], aDomIdx[1]);
      ASSERT_TRUE(pDomIdx->lowerBound()->isEqual(domain->lowerBound()));
      ASSERT_TRUE(pDomIdx->upperBound()->isEqual(domain->upperBound()));
      
      // check pDom
      domain = getDomainInt(aDom[0], aDom[1]);
      ASSERT_TRUE(pDom->lowerBound()->isEqual(domain->lowerBound()));
      ASSERT_TRUE(pDom->upperBound()->isEqual(domain->upperBound()));
    }//runPropagationAndCheckDomainsElem

  protected:
    // integer domains
    DomainIntPtr pDomIdx, pDom;
    // Domain array
    Core::DomainArraySPtr pDomArray;
    // Domain element array
    Core::DomainElementArraySPtr pDomElemArray;
    
    // Array of shared ptr used to preserve domains
    std::vector<DomainIntPtr> pTempDomainArray;
    // Array of shared ptr used to preserve domain elements
    std::vector<ElemPtr> pTempDomainElemArray;
  };
}// end namespace

OPTILAB_TEST_F(PropagatorElementTest, postDomains)
{
  using namespace Core;
  
  // @note indexing 1-based
  
  // (2, [8, 3, 4], 3) => (2, [8, 3, 4], 3) unspec
  setDomains({2, 2}, {8, 8, 3, 3, 4, 4}, {3, 3});
  postPropagatorDomains({2, 2}, {8, 8, 3, 3, 4, 4}, {3, 3}, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // (2, [{8,11}, 3, 4], 3) => (2, [{8,11}, 3, 4], 3) unspec
  setDomains({2, 2}, {8, 11, 3, 3, 4, 4}, {3, 3});
  postPropagatorDomains({2, 2}, {8, 11, 3, 3, 4, 4}, {3, 3}, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // ({2,3}, [8, 3, 4], 4) => (3, [8, 3, 4], 4) unspec
  setDomains({2, 3}, {8, 8, 3, 3, 4, 4}, {4, 4});
  postPropagatorDomains({3, 3}, {8, 8, 3, 3, 4, 4}, {4, 4}, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // (2, [8, 3, 4], {0,3}) => (2, [8, 3, 4], 3) unspec
  setDomains({2, 2}, {8, 8, 3, 3, 4, 4}, {0, 3});
  postPropagatorDomains({2, 2}, {8, 8, 3, 3, 4, 4}, {3, 3}, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // ({1,3}, [8, 3, 4], {-10,2}) => ({1,3}, [8, 3, 4], {-10,2}) unspec
  setDomains({1, 3}, {8, 8, 3, 3, 4, 4}, {-10, 2});
  postPropagatorDomains({1, 3}, {8, 8, 3, 3, 4, 4}, {-10, 2}, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // (1, [8, 3, 4], 3) => (1, [8, 3, 4], 3) fail
  setDomains({1, 1}, {8, 8, 3, 3, 4, 4}, {3, 3});
  postPropagatorDomains({1, 1}, {8, 8, 3, 3, 4, 4}, {3, 3}, PropagationEvent::PROP_EVENT_FAIL);
  
  // ({1,3}, [8, 3, 4], 1) => ({1,3}, [8, 3, 4], 1) fail
  setDomains({1, 3}, {8, 8, 3, 3, 4, 4}, {1, 1});
  postPropagatorDomains({1, 3}, {8, 8, 3, 3, 4, 4}, {1, 1}, PropagationEvent::PROP_EVENT_FAIL);
  
  // (1, [8, 3, 4], {200,300) => (1, [8, 3, 4], 3) fail
  setDomains({1, 1}, {8, 8, 3, 3, 4, 4}, {3, 3});
  postPropagatorDomains({1, 1}, {8, 8, 3, 3, 4, 4}, {3, 3}, PropagationEvent::PROP_EVENT_FAIL);
  
  // (1, [8, 3, {9,13}], {200,300}) => (1, [8, 3, {9,13}], {200,300}) fail
  setDomains({1, 1}, {8, 8, 3, 3, 9, 13}, {3, 3});
  postPropagatorDomains({1, 1}, {8, 8, 3, 3, 9, 13}, {3, 3}, PropagationEvent::PROP_EVENT_FAIL);
}//postDomains

OPTILAB_TEST_F(PropagatorElementTest, postDomainsElem)
{
  using namespace Core;
  
  // @note indexing 1-based
  
  // (2, [8, 3, 4], 3) => (2, [8, 3, 4], 3) unspec
  setDomainsElem({2, 2}, {8, 3, 4}, {3, 3});
  postPropagatorDomainsElem({2, 2}, {3, 3}, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // (2, [8, 9, 10, 11, 3, 4], 3) => (2, [8, 9, 10, 11, 3, 4], 3) fail
  setDomainsElem({2, 2}, {8, 9, 10, 11, 3, 4}, {3, 3});
  postPropagatorDomainsElem({2, 2}, {3, 3}, PropagationEvent::PROP_EVENT_FAIL);
  
  // ({2,3}, [8, 3, 4], 4) => (3, [8, 3, 4], 4) unspec
  setDomainsElem({2, 3}, {8, 3, 4}, {4, 4});
  postPropagatorDomainsElem({3, 3}, {4, 4}, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // (2, [8, 3, 4], {0,3}) => (2, [8, 3, 4], 3) unspec
  setDomainsElem({2, 2}, {8, 3, 4}, {0, 3});
  postPropagatorDomainsElem({2, 2}, {3, 3}, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // ({1,3}, [8, 3, 4], {-10,2}) => ({1,3}, [8, 3, 4], {-10,2}) unspec
  setDomainsElem({1, 3}, {8, 3, 4}, {-10, 2});
  postPropagatorDomainsElem({1, 3}, {-10, 2}, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // (1, [8, 3, 4], 3) => (1, [8, 3, 4], 3) fail
  setDomainsElem({1, 1}, {8, 3, 4}, {3, 3});
  postPropagatorDomainsElem({1, 1}, {3, 3}, PropagationEvent::PROP_EVENT_FAIL);
  
  // ({1,3}, [8, 3, 4], 1) => ({1,3}, [8, 3, 4], 1) fail
  setDomainsElem({1, 3}, {8, 3, 4}, {1, 1});
  postPropagatorDomainsElem({1, 3}, {1, 1}, PropagationEvent::PROP_EVENT_FAIL);
  
  // (1, [8, 3, 4], {200,300) => (1, [8, 3, 4], 3) fail
  setDomainsElem({1, 1}, {8, 3, 4}, {3, 3});
  postPropagatorDomainsElem({1, 1}, {3, 3}, PropagationEvent::PROP_EVENT_FAIL);
  
  // (1, [8, 3, 9, 10, 11, 12, 13], 3) => (1, [8, 3, 9, 10, 11, 12, 13], 3) fail
  setDomainsElem({1, 1}, {8, 3, 9, 10, 11, 12, 13}, {3, 3});
  postPropagatorDomainsElem({1, 1}, {3, 3}, PropagationEvent::PROP_EVENT_FAIL);
}//postDomainsElem

OPTILAB_TEST_F(PropagatorElementTest, simpleCasesDomains)
{
  using namespace Core;
  
  // @note indexing 1-based
  
  // (2, [8, 3, 4], 3) => (2, [8, 3, 4], 3) subsumed
  setDomains({2, 2}, {8, 8, 3, 3, 4, 4}, {3, 3});
  runPropagationAndCheckDomains({2, 2}, {8, 8, 3, 3, 4, 4}, {3, 3}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // (2, [{8,11}, 3, 4], 3) => (2, [{8,11}, 3, 4], 3) subsumed
  setDomains({2, 2}, {8, 11, 3, 3, 4, 4}, {3, 3});
  runPropagationAndCheckDomains({2, 2}, {8, 11, 3, 3, 4, 4}, {3, 3}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // ({2,3}, [8, 3, 4], 4) => (3, [8, 3, 4], 4) subsumed
  setDomains({2, 3}, {8, 8, 3, 3, 4, 4}, {4, 4});
  runPropagationAndCheckDomains({3, 3}, {8, 8, 3, 3, 4, 4}, {4, 4}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // (2, [8, 3, 4], {0,3}) => (2, [8, 3, 4], 3) subsumed
  setDomains({2, 2}, {8, 8, 3, 3, 4, 4}, {0, 3});
  runPropagationAndCheckDomains({2, 2}, {8, 8, 3, 3, 4, 4}, {3, 3}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // ({1,3}, [8, 3, 4], {-10,2}) => ({1,3}, [8, 3, 4], {-10,2}) fail
  setDomains({1, 3}, {8, 8, 3, 3, 4, 4}, {-10, 2});
  runPropagationAndCheckDomains({1, 3}, {8, 8, 3, 3, 4, 4}, {-10, 2}, PropagationEvent::PROP_EVENT_FAIL);
  
  // (1, [8, 3, 4], 3) => (1, [8, 3, 4], 3) fail
  setDomains({1, 1}, {8, 8, 3, 3, 4, 4}, {3, 3});
  runPropagationAndCheckDomains({1, 1}, {8, 8, 3, 3, 4, 4}, {3, 3}, PropagationEvent::PROP_EVENT_FAIL);
  
  // ({1,3}, [8, 3, 4], 1) => ({1,3}, [8, 3, 4], 1) fail
  setDomains({1, 3}, {8, 8, 3, 3, 4, 4}, {1, 1});
  runPropagationAndCheckDomains({1, 3}, {8, 8, 3, 3, 4, 4}, {1, 1}, PropagationEvent::PROP_EVENT_FAIL);
  
  // (1, [8, 3, 4], 3) => (1, [8, 3, 4], 3) fail
  setDomains({1, 1}, {8, 8, 3, 3, 4, 4}, {3, 3});
  runPropagationAndCheckDomains({1, 1}, {8, 8, 3, 3, 4, 4}, {3, 3}, PropagationEvent::PROP_EVENT_FAIL);
  
  // (1, [8, 3, {9,13}], 3) => (1, [8, 3, {9,13}], 3) fail
  setDomains({1, 1}, {8, 8, 3, 3, 9, 13}, {3, 3});
  runPropagationAndCheckDomains({1, 1}, {8, 8, 3, 3, 9, 13}, {3, 3}, PropagationEvent::PROP_EVENT_FAIL);
  
  // ({1,5}, [8, 3, {9,13}, -100, 2], {-101,2}) => ({4,5}, [8, 3, {9,13}, -100, 2], {-101, 2}) fixpoint
  setDomains({1, 5}, {8, 8, 3, 3, 9, 13, -100, -100, 2, 2}, {-101, 2});
  runPropagationAndCheckDomains({4, 5}, {8, 8, 3, 3, 9, 13, -100, -100, 2, 2}, {-100, 2}, PropagationEvent::PROP_EVENT_FIXPOINT);
}//simpleCasesDomains

OPTILAB_TEST_F(PropagatorElementTest, simpleCasesDomainsElem)
{
  using namespace Core;
  
  // @note indexing 1-based
  
  // (2, [8, 3, 4], 3) => (2, [8, 3, 4], 3) subsumed
  setDomainsElem({2, 2}, {8, 3, 4}, {3, 3});
  runPropagationAndCheckDomainsElem({2, 2}, {3, 3}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // (2, [8, 9, 10, 11, 3, 4], 3) => (2, [8, 9, 10, 11, 3, 4], 3) fail
  setDomainsElem({2, 2}, {8, 9, 10, 11, 3, 4}, {3, 3});
  runPropagationAndCheckDomainsElem({2, 2}, {3, 3}, PropagationEvent::PROP_EVENT_FAIL);
  
  // ({2,3}, [8, 3, 4], 4) => (3, [8, 3, 4], 4) subsumed
  setDomainsElem({2, 3}, {8, 3, 4}, {4, 4});
  runPropagationAndCheckDomainsElem({3, 3}, {4, 4}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // (2, [8, 3, 4], {0,3}) => (2, [8, 3, 4], 3) subsumed
  setDomainsElem({2, 2}, {8, 3, 4}, {0, 3});
  runPropagationAndCheckDomainsElem({2, 2}, {3, 3}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // ({1,3}, [8, 3, 4], {-10,2}) => ({1,3}, [8, 3, 4], {-10,2}) fail
  setDomainsElem({1, 3}, {8, 3, 4}, {-10, 2});
  runPropagationAndCheckDomainsElem({1, 3}, {-10, 2}, PropagationEvent::PROP_EVENT_FAIL);
  
  // (1, [8, 3, 4], 3) => (1, [8, 3, 4], 3) fail
  setDomainsElem({1, 1}, {8, 3, 4}, {3, 3});
  runPropagationAndCheckDomainsElem({1, 1}, {3, 3}, PropagationEvent::PROP_EVENT_FAIL);
  
  // ({1,3}, [8, 3, 4], 1) => ({1,3}, [8, 3, 4], 1) fail
  setDomainsElem({1, 3}, {8, 3, 4}, {1, 1});
  runPropagationAndCheckDomainsElem({1, 3}, {1, 1}, PropagationEvent::PROP_EVENT_FAIL);
  
  // (1, [8, 3, 4], 3) => (1, [8, 3, 4], 3) fail
  setDomainsElem({1, 1}, {8, 3, 4}, {3, 3});
  runPropagationAndCheckDomainsElem({1, 1}, {3, 3}, PropagationEvent::PROP_EVENT_FAIL);
  
  // (1, [8, 3, 9, 10, 11, 12, 13], 3) => (1, [8, 3, 9, 10, 11, 12, 13], 3) fail
  setDomainsElem({1, 1}, {8, 3, 9, 10, 11, 12, 13}, {3, 3});
  runPropagationAndCheckDomainsElem({1, 1}, {3, 3}, PropagationEvent::PROP_EVENT_FAIL);
  
  // ({1,9}, [8, 3, 9, 10, 11, 12, 13, -100, 2], {-101,2}) => ({8,9}, [8, 3, 9, 10, 11, 12, 13, -100, 2], {-101, 2}) fixpoint
  setDomainsElem({1, 9}, {8, 3, 9, 10, 11, 12, 13, -100, 2}, {-101, 2});
  runPropagationAndCheckDomainsElem({8,9}, {-100, 2}, PropagationEvent::PROP_EVENT_FIXPOINT);
}//simpleCasesDomainsElem

OPTILAB_TEST_F(PropagatorElementTest, propOnTable)
{
  using namespace Core;
  
  // @note indexing 1-based
  
  // (1, [{0,30}], {2,3) => (1, [{2,3}], {2,3}) fixpoint
  setDomains({1, 1}, {0, 30}, {2, 3});
  runPropagationAndCheckDomains({1, 1}, {2, 3}, {2, 3}, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // (2, [8, {-100,90}, 4], 3) => (2, [8, 3, 4], 3) subsumed
  setDomains({2, 2}, {8, 8, -100, 90, 4, 4}, {3, 3});
  runPropagationAndCheckDomains({2, 2}, {8, 8, 3, 3, 4, 4}, {3, 3}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // (2, [8, {-100,-90}, 4], 3) => (2, [8, {-100,-90}, 4], 3) fail
  setDomains({2, 2}, {8, 8, -100, -90, 4, 4}, {3, 3});
  runPropagationAndCheckDomains({2, 2}, {8, 8, -100, -90, 4, 4}, {3, 3}, PropagationEvent::PROP_EVENT_FAIL);
  
  // ({1,3}, [{0,8}, {-100,3}, {3,4}], 3) => ({1,3}, [3, 3, 3], 3) subsumed
  setDomains({1, 3}, {0, 8, -100, 3, 3, 4}, {3, 3});
  runPropagationAndCheckDomains({1, 3}, {3, 3, 3, 3, 3, 3}, {3, 3}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // ({1,3}, [{0,8}, {-100,3}, {3,4}], 10) => ({1,3}, [{0,8}, {-100,3}, {3,4}], 10) fail
  setDomains({1, 3}, {0, 8, -100, 3, 3, 4}, {10, 10});
  runPropagationAndCheckDomains({1, 3}, {0, 8, -100, 3, 3, 4}, {10, 10}, PropagationEvent::PROP_EVENT_FAIL);
  
  // ({1,3}, [{0,8}, {-100,3}, {3,4}], {1,2}) => ({1,3}, [{1,2}, {1,2}, {1,2}], {1,2}) fixpoint
  setDomains({1, 3}, {0, 8, -100, 3, -3, 4}, {1, 2});
  runPropagationAndCheckDomains({1, 3}, {1, 2, 1, 2, 1, 2}, {1, 2}, PropagationEvent::PROP_EVENT_FIXPOINT);

  // ({1,3}, [{1,100}, {50,101}, {-5,-4}], {20,99}) => ({1,2}, [{20,99}, {50,99}, {-5,-4}, {20,99}) fixpoint
  setDomains({1, 3}, {1, 100, 50, 101, -5, -4}, {20, 99});
  runPropagationAndCheckDomains({1, 2}, {20, 99, 50, 99, -5, -4}, {20, 99}, PropagationEvent::PROP_EVENT_FIXPOINT);
}//propOnTable

// from constraint description page:
// http://sofdem.github.io/gccat/gccat/Celement.html
OPTILAB_TEST_F(PropagatorElementTest, webPageCases)
{
  using namespace Core;
  
  // @note indexing 1-based
  
  // exercise 2
  setDomainsElem({2, 6}, {0, 2, 9, 5, 9, 2, 3, 9}, {0, 5});
  runPropagationAndCheckDomainsElem({2, 6}, {2, 5}, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  //exercise 3
  setDomains({2, 3}, {5, 5, 3, 5, 0, 3}, {1, 2});
  runPropagationAndCheckDomains({3, 3}, {5, 5, 3, 5, 1, 2}, {1, 2}, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  //exercise 4
  setDomains({2, 3}, {2, 4, 0, 0, 0, 4, 6, 6}, {3, 5});
  runPropagationAndCheckDomains({3, 3}, {2, 4, 0, 0, 3, 4, 6, 6}, {3, 4}, PropagationEvent::PROP_EVENT_FIXPOINT);
}//webPageCases


OPTILAB_TEST_F(PropagatorElementTest, getFitness)
{
  using namespace Core;
  
  // @note indexing 1-based
  

  // cases in which index I is I>=1 /\ I<=arraySize

  setDomains({1, 1}, {4, 4}, {4, 4});
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());
  
  setDomainsElem({1, 1}, {4}, {4, 4});
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());

  setDomains({1, 1}, {4, 4, 5, 5, 9, 9}, {4, 4});
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());
  
  setDomainsElem({1, 1}, {4, 5, 9}, {4, 4});
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());
  
  setDomains({2, 2}, {4, 4, 5, 5, 9, 9}, {4, 4});
  EXPECT_EQ(1, getFitness());
  
  setDomainsElem({2, 2}, {4, 5, 9}, {4, 4});
  EXPECT_EQ(1, getFitness());
  
  setDomains({2, 2}, {4, 4, 5, 5, 9, 9}, {-4, -4});
  EXPECT_EQ(9, getFitness());
  setDomainsElem({2, 2}, {4, 5, 9}, {-4, -4});
  EXPECT_EQ(9, getFitness());
  
  setDomains({-1, 1}, {4, 4, 5, 5, 9, 9}, {4, 4});
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness());
  
  setDomainsElem({-1, 1}, {4, 5, 9}, {4, 4});
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness());
  
  setDomains({1, 1}, {4, 4, -5, 5, 9, 9}, {4, 4});
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness());
  
  setDomains({1, 1}, {4, 4, 5, 5, 9, 9}, {-4, 4});
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness());
  
  setDomainsElem({1, 1}, {4, 5, 9}, {-4, 4});
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness());
  
  
  // cases in which index I is I<1 \/ I>arraySize

  setDomains({0, 0}, {4, 4}, {4, 4});
  EXPECT_EQ(1, getFitness());
  setDomainsElem({0, 0}, {4}, {4, 4});
  EXPECT_EQ(1, getFitness());

  setDomains({0, 0}, {4, 4, 5, 5, 9, 9}, {9, 9});
  EXPECT_EQ(3, getFitness());
  setDomainsElem({0, 0}, {4, 5, 9}, {9, 9});
  EXPECT_EQ(3, getFitness());
  
  setDomains({-12, -12}, {4, 4, 5, 5, 9, 9}, {9, 9});
  EXPECT_EQ(15, getFitness());
  setDomainsElem({-12, -12}, {4, 5, 9}, {9, 9});
  EXPECT_EQ(15, getFitness());
  
  setDomains({0, 0}, {4, 4, 5, 5, -9, -9}, {9, 9});
  EXPECT_EQ(6, getFitness());
  setDomainsElem({0, 0}, {4, 5, -9}, {9, 9});
  EXPECT_EQ(6, getFitness());
  
  setDomains({10, 10}, {4, 4, 5, 5, 9, 9}, {9, 9});
  EXPECT_EQ(7, getFitness());
  setDomainsElem({10, 10}, {4, 5, 9}, {9, 9});
  EXPECT_EQ(7, getFitness());
  
  setDomains({10, 10}, {40, 40, 5, 5, 90, 90}, {20, 20});
  EXPECT_EQ(23, getFitness());
  setDomainsElem({10, 10}, {40, 5, 90}, {20, 20});
  EXPECT_EQ(23, getFitness());
  
  setDomains({10, 10}, {40, 40, 5, 5, -90, -90}, {-20, -20});
  EXPECT_EQ(33, getFitness());
  setDomainsElem({10, 10}, {40, 5, -90}, {-20, -20});
  EXPECT_EQ(33, getFitness());
  
  setDomains({-10, -9}, {40, 40, 5, 5, 90, 90}, {20, 20});
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness());
  
  setDomains({10, 10}, {-40, 40, 5, 5, 90, 90}, {20, 20});
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness());
  
  setDomains({10, 10}, {40, 40, 5, 5, 90, 90}, {-20, 0});
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness());
}//getFitness
