#include "utest_globals.hpp"

#include "PropagatorAllDiff.hpp"
#include "ConstraintTestInc.hpp"

namespace {
  class PropagatorAllDiffTest : public ::testing::Test {
  public:
    PropagatorAllDiffTest()
    {
    }
    
    ~PropagatorAllDiffTest()
    {
    }
    
    void setDomains(std::vector<int>&& aDomArray)
    {
      pDomArray = std::make_shared<Core::DomainArray>(aDomArray.size()/2);
      
      // pDomArray
      std::size_t idx{0};
      pTempDomainArray.clear();
      for(std::size_t domIdx = 0; domIdx < aDomArray.size(); ++domIdx)
      {
        if(domIdx > 0 && (domIdx % 2 != 0))
        {
          pTempDomainArray.push_back(getDomainInt(aDomArray[domIdx-1], aDomArray[domIdx]));
          pDomArray->assignElementToCell(idx++, pTempDomainArray.back().get());
        }
      }
    }//setDomains
    
    void setDomains(std::vector<int>& aDomArray)
    {
      pDomArray = std::make_shared<Core::DomainArray>(aDomArray.size()/2);
      
      // pDomArray
      std::size_t idx{0};
      pTempDomainArray.clear();
      for(std::size_t domIdx = 0; domIdx < aDomArray.size(); ++domIdx)
      {
        if(domIdx > 0 && (domIdx % 2 != 0))
        {
          pTempDomainArray.push_back(getDomainInt(aDomArray[domIdx-1], aDomArray[domIdx]));
          pDomArray->assignElementToCell(idx++, pTempDomainArray.back().get());
        }
      }
    }//setDomains
    
    void postPropagator(std::vector<int>&& aDomArray,
                        Core::PropagationEvent aEvent)
    {
      std::unique_ptr<Core::PropagatorAllDiff> propagator(new Core::PropagatorAllDiff(*(pDomArray.get())));
      
      Core::PropagationEvent event = propagator->post();
      ASSERT_EQ(event, aEvent);
      if(event == Core::PropagationEvent::PROP_EVENT_FAIL) return;
      
      // check pDomArray
      std::size_t domIdxToCompare{0};
      DomainIntPtr domain;
      for(std::size_t domIdx = 1; domIdx < aDomArray.size(); ++domIdx)
      {
        if(domIdx > 0 && (domIdx % 2 != 0))
        {
          domain = getDomainInt(aDomArray[domIdx-1], aDomArray[domIdx]);
          ASSERT_TRUE(pDomArray->operator[](domIdxToCompare)->lowerBound()->isEqual(domain->lowerBound()));
          ASSERT_TRUE(pDomArray->operator[](domIdxToCompare)->upperBound()->isEqual(domain->upperBound()));
          domIdxToCompare++;
        }
      }
    }//postPropagator
    
    void runPropagationAndCheck(std::vector<int>&& aDomArray,
                                Core::PropagationEvent aEvent)
    {
      // Perform propagation
      std::unique_ptr<Core::PropagatorAllDiff> propagator(new Core::PropagatorAllDiff(*(pDomArray.get())));
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      if(event == Core::PropagationEvent::PROP_EVENT_FAIL) return;
      
      // check pDomArray
      std::size_t domIdxToCompare{0};
      DomainIntPtr domain;
      for(std::size_t domIdx = 1; domIdx < aDomArray.size(); ++domIdx)
      {
        if(domIdx > 0 && (domIdx % 2 != 0))
        {
          domain = getDomainInt(aDomArray[domIdx-1], aDomArray[domIdx]);
          ASSERT_TRUE(pDomArray->operator[](domIdxToCompare)->lowerBound()->isEqual(domain->lowerBound()));
          ASSERT_TRUE(pDomArray->operator[](domIdxToCompare)->upperBound()->isEqual(domain->upperBound()));
          domIdxToCompare++;
        }
      }
    }//runPropagationAndCheck
    
    double runFitness(std::vector<int>&& aDomArray)
    {
      // Perform propagation
      std::unique_ptr<Core::PropagatorAllDiff> propagator(new Core::PropagatorAllDiff(*(pDomArray.get())));
      return propagator->getFitness();
    }//runFitness
    
    // This method creates two instances of size = size/2, executes propagation
    // and prints times.
    // The first instance has smaller domains while the second one
    // has larger domains.
    void runPropagationAndPrintTimes(size_t size)
    {
      std::vector<int> doms(size);
      size_t idx = 0;
      while( idx<size )
      {
        doms[idx] = rand() % (size/3);
        ++idx;
        doms[idx] = doms[idx-1] + ( rand() % ( size/3 - doms[idx-1] ) );
        ++idx;
      }
      setDomains(doms);
      
      // "small domains" meaning the upper and lower bounds of the domains are
      // in the interval [0..size/3]
      std::cout << "testing an instance of size " << pDomArray->size() << " with small domains\n";
      
      // Perform propagation and measure time
      std::unique_ptr<Core::PropagatorAllDiff> propagator1(new Core::PropagatorAllDiff(*(pDomArray.get())));
      clock_t t;
      t = clock();
      Core::PropagationEvent event = propagator1->propagate();
      t = clock() - t;
      
      std::cout << "time: " << static_cast<float>(t)/CLOCKS_PER_SEC << " seconds\n";
      
      
      idx = 0;
      while( idx<size )
      {
        doms[idx] = rand() % (size);
        ++idx;
        doms[idx] = doms[idx-1] + ( rand() % ( size - doms[idx-1] ) );
        ++idx;
      }
      setDomains(doms);
      
      // "large domains" meaning the upper and lower bounds of the domains are
      // in the interval [0..size]
      std::cout << "testing an instance of size " << pDomArray->size() << " with large domains\n";
      
      // Perform propagation and measure time
      std::unique_ptr<Core::PropagatorAllDiff> propagator2(new Core::PropagatorAllDiff(*(pDomArray.get())));
      t = clock();
      event = propagator2->propagate();
      t = clock() - t;
      
      std::cout << "time: " << static_cast<float>(t)/CLOCKS_PER_SEC << " seconds\n\n";
    }//runPropagationAndPrintTimes
    
  protected:
    // Domain array
    Core::DomainArraySPtr pDomArray;
    
    // Array of shared ptr used to preserve domains
    std::vector<DomainIntPtr> pTempDomainArray;
  };
}// end namespace

OPTILAB_TEST_F(PropagatorAllDiffTest, post)
{
  using namespace Core;
  
}//post

OPTILAB_TEST_F(PropagatorAllDiffTest, propagator)
{
  using namespace Core;
  
  setDomains            ({1, 1, 2, 2, 3, 3});
  runPropagationAndCheck({1, 1, 2, 2, 3, 3}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  setDomains            ({1, 1, 2, 2, 2, 2});
  runPropagationAndCheck({1, 1, 2, 2, 2, 2}, PropagationEvent::PROP_EVENT_FAIL);
  
  setDomains            ({1, 1, 1, 1, 2, 2});
  runPropagationAndCheck({1, 1, 1, 1, 2, 2}, PropagationEvent::PROP_EVENT_FAIL);
  
  setDomains            ({1, 2, 1, 1});
  runPropagationAndCheck({2, 2, 1, 1}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  setDomains            ({-3, 0,-2, 0, 0, 0});
  runPropagationAndCheck({-3,-1,-2,-1, 0, 0}, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  setDomains            ({0, 2, 0, 1,  99, 100, 99, 99});
  runPropagationAndCheck({0, 2, 0, 1, 100, 100, 99, 99}, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  setDomains            ({0, 2, 0, 2, 0, 2, 0, 2});
  runPropagationAndCheck({0, 2, 0, 2, 0, 2, 0, 2}, PropagationEvent::PROP_EVENT_FAIL);
  
  setDomains            ({1, 2, 1, 3, 1, 3, 1, 3});
  runPropagationAndCheck({1, 2, 1, 3, 1, 3, 1, 3}, PropagationEvent::PROP_EVENT_FAIL);
  
  setDomains            ({2, 3, 1, 3, 1, 3, 1, 3});
  runPropagationAndCheck({2, 3, 1, 3, 1, 3, 1, 3}, PropagationEvent::PROP_EVENT_FAIL);
  
  setDomains            ({0, 3, 2, 2, 0, 3, 0, 3, 0, 3});
  runPropagationAndCheck({0, 3, 2, 2, 0, 3, 0, 3, 0, 3}, PropagationEvent::PROP_EVENT_FAIL);
  
  setDomains            ({-8,-6, 12, 14, 12, 20, 12, 12, 14, 14,-6,-5,-5,-5});
  runPropagationAndCheck({-8,-7, 13, 13, 15, 20, 12, 12, 14, 14,-6,-6,-5,-5},  PropagationEvent::PROP_EVENT_FIXPOINT);
  
  setDomains            ({-8,-5, 18, 20, 10, 11, 10, 10,-6,-5,-5,-5});
  runPropagationAndCheck({-8,-7, 18, 20, 11, 11, 10, 10,-6,-6,-5,-5},  PropagationEvent::PROP_EVENT_FIXPOINT);
  
  setDomains            ({0, 100, 1, 100, 2, 100, 50, 100, 30, 100, 20, 100});
  runPropagationAndCheck({0, 100, 1, 100, 2, 100, 50, 100, 30, 100, 20, 100}, PropagationEvent::PROP_EVENT_FIXPOINT);
}//propagator

OPTILAB_TEST_F(PropagatorAllDiffTest, getFitnessOnSat)
{
  // Test alldiff fitness function on satisfied constraint
  using namespace Core;
  
  setDomains            ({1, 1, 2, 2, 3, 3});
  ASSERT_EQ(Propagator::getMinFitness(), runFitness({1, 1, 2, 2, 3, 3}));
}//getFitnessOnSat

OPTILAB_TEST_F(PropagatorAllDiffTest, getFitnessOnUnsat)
{
  // Test alldiff fitness function on an unsatisfied constraint
  using namespace Core;
  
  setDomains            ({1, 1, 2, 2, 2, 2});
  ASSERT_EQ(1, runFitness({1, 1, 2, 2, 2, 2}));
  
  setDomains            ({2, 2, 2, 2, 2, 2});
#ifdef FAST_FITNESS
  ASSERT_EQ(2, runFitness({2, 2, 2, 2, 2, 2}));
#else
  ASSERT_EQ(3, runFitness({2, 2, 2, 2, 2, 2}));
#endif
}//getFitnessOnUnsat

OPTILAB_TEST_F(PropagatorAllDiffTest, getFitnessOnNonGround)
{
  // Test alldiff fitness function on satisfied constraint
  using namespace Core;
  
  setDomains            ({1, 2, 2, 3, 2, 4});
  ASSERT_EQ(Propagator::getUnspecFitness(), runFitness({1, 2, 2, 3, 2, 4}));
}//getFitnessOnNonGround

// from constraint description page:
// http://sofdem.github.io/gccat/gccat/Calldifferent.html
OPTILAB_TEST_F(PropagatorAllDiffTest, webPageCases)
{
  using namespace Core;
  
  setDomains            ({3, 4, 2, 4, 3, 4, 2, 5, 3, 6, 1, 6});
  runPropagationAndCheck({3, 4, 2, 2, 3, 4, 5, 5, 6, 6, 1, 1}, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  setDomains            ({2, 4, 2, 3, 1, 6, 2, 5, 2, 3, 1, 6});
  runPropagationAndCheck({4, 4, 2, 3, 1, 6, 5, 5, 2, 3, 1, 6}, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  setDomains            ({5, 5, 1, 1, 4, 4, 8, 8, 1, 1});
  runPropagationAndCheck({5, 5, 1, 1, 4, 4, 8, 8, 1, 1}, PropagationEvent::PROP_EVENT_FAIL);
  
  setDomains            ({8, 8, 2, 2, 4, 4, 3, 3});
  runPropagationAndCheck({8, 8, 2, 2, 4, 4, 3, 3}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  setDomains            ({0, 0});
  runPropagationAndCheck({0, 0}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  setDomains            ({3, 5, 3, 4, 2, 7, 6, 6, 3, 4, 2, 7});
  runPropagationAndCheck({5, 5, 3, 4, 2, 7, 6, 6, 3, 4, 2, 7}, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  setDomains            ({4, 6, 1, 3, 1, 4, 1, 2, 4, 7, 4, 6, 1, 2});
  runPropagationAndCheck({5, 6, 3, 3, 4, 4, 1, 2, 7, 7, 5, 6, 1, 2}, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  setDomains            ({1, 3, 1, 4, 1, 5, 1, 6, 3, 5});
  runPropagationAndCheck({1, 3, 1, 4, 1, 5, 1, 6, 3, 5}, PropagationEvent::PROP_EVENT_FIXPOINT);
}//webPageCases


//#define EXE_TIME_ALLDIFF
#ifdef EXE_TIME_ALLDIFF
OPTILAB_TEST_F(PropagatorAllDiffTest, time)
{
  using namespace Core;
  
  // We set deterministically the initial seed on purpose,
  // we use this test only to check propagation times on
  // different implementations.
  srand(1);
  
  // @note: instance size will be 2000/2
  runPropagationAndPrintTimes(2000);
  /* 
   * on i7-6700HQ @ 2.60GHz × 4 and SSD hard drive (Ubuntu):
   * 0.005201 seconds
   * 0.006955 seconds
   */
  
  runPropagationAndPrintTimes(2000);
  /* 
   * on i7-6700HQ @ 2.60GHz × 4 and SSD hard drive (Ubuntu):
   * 0.004062 seconds
   * 0.007995 seconds
   */
  
  runPropagationAndPrintTimes(20000);
  /* 
   * on i7-6700HQ @ 2.60GHz × 4 and SSD hard drive (Ubuntu):
   * 0.058955 seconds
   * 0.115701 seconds
   */
  
  runPropagationAndPrintTimes(20000);
  /* 
   * on i7-6700HQ @ 2.60GHz × 4 and SSD hard drive (Ubuntu):
   * 0.053382 seconds
   * 0.075358 seconds
   */
  
  runPropagationAndPrintTimes(100000);
  /* 
   * on i7-6700HQ @ 2.60GHz × 4 and SSD hard drive (Ubuntu):
   * 0.446694 seconds
   * 0.75644 seconds
   */
  
  runPropagationAndPrintTimes(100000);
  /* 
   * on i7-6700HQ @ 2.60GHz × 4 and SSD hard drive (Ubuntu):
   * 0.419644 seconds
   * 0.748248 seconds
   */
  
  runPropagationAndPrintTimes(200000);
  /* 
   * on i7-6700HQ @ 2.60GHz × 4 and SSD hard drive (Ubuntu):
   * 0.954698 seconds
   * 1.61981 seconds
   */
  
  runPropagationAndPrintTimes(200000);
  /* 
   * on i7-6700HQ @ 2.60GHz × 4 and SSD hard drive (Ubuntu):
   * 0.993853 seconds
   * 1.27769 seconds
   */
  
  runPropagationAndPrintTimes(1000000);
  /* 
   * on i7-6700HQ @ 2.60GHz × 4 and SSD hard drive (Ubuntu):
   * 5.84949 seconds
   * 8.11867 seconds
   */
  
  runPropagationAndPrintTimes(1000000);
  /* 
   * on i7-6700HQ @ 2.60GHz × 4 and SSD hard drive (Ubuntu):
   * 6.73185 seconds
   * 10.4955 seconds
   */
  
  runPropagationAndPrintTimes(2000000);
  /* 
   * on i7-6700HQ @ 2.60GHz × 4 and SSD hard drive (Ubuntu):
   * 13.0176 seconds
   * 22.0637 seconds
   */
  
  runPropagationAndPrintTimes(2000000);
  /* 
   * on i7-6700HQ @ 2.60GHz × 4 and SSD hard drive (Ubuntu):
   * 13.6936 seconds
   * 21.9188 seconds
   */
  
  std::cout << "\ntests finished";
  getchar();
}//time
#endif
