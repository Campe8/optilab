
#include "utest_globals.hpp"

#include "PropagatorUtils.hpp"

#include "DomainInteger.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementReal.hpp"
#include "DomainElementVoid.hpp"
#include "DomainElementManager.hpp"

OPTILAB_TEST(PropagatorUtils, isTrueFalse)
{
  using namespace Core::PropagatorUtils;

  ASSERT_TRUE(isTrue(true));
  ASSERT_FALSE(isTrue(false));
  
  ASSERT_TRUE(isFalse(false));
  ASSERT_FALSE(isFalse(true));
}//isTrueFalse

OPTILAB_TEST(PropagatorUtils, isDomainEmpty)
{
  using namespace Core;
  using namespace Core::PropagatorUtils;
  DomainInteger domain(DomainElementManager::getInstance().createDomainElementInt64(-1),
                       DomainElementManager::getInstance().createDomainElementInt64(+1));
  
  ASSERT_FALSE(isDomainEmpty(&domain));
  
  // Empty domain
  domain.shrinkOnLowerBound(DomainElementInt64::cast(DomainElementManager::getInstance().createDomainElementInt64(+2)));
  ASSERT_EQ(domain.getSize(), 0);
  
  ASSERT_TRUE(isDomainEmpty(&domain));
}//isDomainEmpty

OPTILAB_TEST(PropagatorUtils, sameBounds)
{
  using namespace Core;
  using namespace Core::PropagatorUtils;
  
  DomainInteger domain1(DomainElementManager::getInstance().createDomainElementInt64(-10),
                       DomainElementManager::getInstance().createDomainElementInt64(+10));
  DomainInteger domain2(DomainElementManager::getInstance().createDomainElementInt64(-10),
                       DomainElementManager::getInstance().createDomainElementInt64(+10));
  DomainInteger domain3(DomainElementManager::getInstance().createDomainElementInt64(-9),
                       DomainElementManager::getInstance().createDomainElementInt64(+10));
  
  ASSERT_TRUE(sameBounds(&domain1,  &domain1));
  ASSERT_TRUE(sameBounds(&domain1,  &domain2));
  ASSERT_FALSE(sameBounds(&domain1, &domain3));
}//sameBounds

OPTILAB_TEST(PropagatorUtils, sameDomains)
{
  using namespace Core;
  using namespace Core::PropagatorUtils;
  
  DomainInteger domain1(DomainElementManager::getInstance().createDomainElementInt64(-10),
                        DomainElementManager::getInstance().createDomainElementInt64(+10));
  DomainInteger domain2(DomainElementManager::getInstance().createDomainElementInt64(-10),
                        DomainElementManager::getInstance().createDomainElementInt64(+10));
  DomainInteger domain3(DomainElementManager::getInstance().createDomainElementInt64(-9),
                        DomainElementManager::getInstance().createDomainElementInt64(+10));
  
  ASSERT_TRUE(sameDomains(&domain1,  &domain1));
  ASSERT_TRUE(sameDomains(&domain1,  &domain2));
  ASSERT_FALSE(sameDomains(&domain1, &domain3));
  
  domain2.subtract(DomainElementManager::getInstance().createDomainElementInt64(1));
  ASSERT_FALSE(sameDomains(&domain1,  &domain2));
}//sameDomains

OPTILAB_TEST(PropagatorUtils, getDomainUnionArraySPtr)
{
  using namespace Core;
  using namespace Core::PropagatorUtils;
  
  DomainInteger domain1(DomainElementManager::getInstance().createDomainElementInt64(-10),
                        DomainElementManager::getInstance().createDomainElementInt64(+10));
  DomainInteger domain2(DomainElementManager::getInstance().createDomainElementInt64(-10),
                        DomainElementManager::getInstance().createDomainElementInt64(+10));
  DomainInteger domain3(DomainElementManager::getInstance().createDomainElementInt64(+11),
                        DomainElementManager::getInstance().createDomainElementInt64(+15));
  
  auto array1 = getDomainUnionArraySPtr(&domain1,  &domain2);
  auto array2 = getDomainUnionArraySPtr(&domain1,  &domain3);
  
  INT_64 elemVal = -10;
  for(std::size_t idx = 0; idx < array1->size(); ++idx)
  {
    ASSERT_TRUE(array1->operator[](idx)->isEqual(DomainElementManager::getInstance().createDomainElementInt64(elemVal++)));
  }
  
  elemVal = -10;
  for(std::size_t idx = 0; idx < array2->size(); ++idx)
  {
    ASSERT_TRUE(array2->operator[](idx)->isEqual(DomainElementManager::getInstance().createDomainElementInt64(elemVal++)));
  }
  ASSERT_EQ(elemVal - 1, 15);
}//getDomainUnionArraySPtr

OPTILAB_TEST(PropagatorUtils, isDomainSingleton)
{
  using namespace Core;
  using namespace Core::PropagatorUtils;
  DomainInteger domain(DomainElementManager::getInstance().createDomainElementInt64(-1),
                       DomainElementManager::getInstance().createDomainElementInt64(+1));
  
  ASSERT_FALSE(isDomainSingleton(&domain));
  
  // Reduce to singleton domain
  domain.shrinkOnLowerBound(DomainElementInt64::cast(DomainElementManager::getInstance().createDomainElementInt64(+1)));
  ASSERT_EQ(domain.getSize(), 1);
  
  ASSERT_TRUE(isDomainSingleton(&domain));
}//isDomainSingleton

OPTILAB_TEST(PropagatorUtils, isDomainPositive)
{
  using namespace Core;
  using namespace Core::PropagatorUtils;
  DomainInteger domain1(DomainElementManager::getInstance().createDomainElementInt64(-1),
                       DomainElementManager::getInstance().createDomainElementInt64(+1));
  
  ASSERT_FALSE(isDomainPositive(&domain1));
  
  DomainInteger domain2(DomainElementManager::getInstance().createDomainElementInt64(0),
                        DomainElementManager::getInstance().createDomainElementInt64(+1));
  
  ASSERT_FALSE(isDomainPositive(&domain2));
  
  DomainInteger domain3(DomainElementManager::getInstance().createDomainElementInt64(+1),
                        DomainElementManager::getInstance().createDomainElementInt64(+2));
  
  ASSERT_TRUE(isDomainPositive(&domain3));
}//isDomainPositive

OPTILAB_TEST(PropagatorUtils, isDomainNegative)
{
  using namespace Core;
  using namespace Core::PropagatorUtils;
  DomainInteger domain1(DomainElementManager::getInstance().createDomainElementInt64(-1),
                        DomainElementManager::getInstance().createDomainElementInt64(+1));
  
  ASSERT_FALSE(isDomainNegative(&domain1));
  
  DomainInteger domain2(DomainElementManager::getInstance().createDomainElementInt64(-1),
                        DomainElementManager::getInstance().createDomainElementInt64(0));
  
  ASSERT_FALSE(isDomainNegative(&domain2));
  
  DomainInteger domain3(DomainElementManager::getInstance().createDomainElementInt64(-2),
                        DomainElementManager::getInstance().createDomainElementInt64(-1));
  
  ASSERT_TRUE(isDomainNegative(&domain3));
}//isDomainNegative

OPTILAB_TEST(PropagatorUtils, isDomainNegativePositive)
{
  using namespace Core;
  using namespace Core::PropagatorUtils;
  DomainInteger domain1(DomainElementManager::getInstance().createDomainElementInt64(-1),
                        DomainElementManager::getInstance().createDomainElementInt64(+1));
  
  ASSERT_TRUE(isDomainNegativePositive(&domain1));
  
  DomainInteger domain2(DomainElementManager::getInstance().createDomainElementInt64(-1),
                        DomainElementManager::getInstance().createDomainElementInt64(0));
  
  ASSERT_TRUE(isDomainNegativePositive(&domain2));
  
  DomainInteger domain3(DomainElementManager::getInstance().createDomainElementInt64(0),
                        DomainElementManager::getInstance().createDomainElementInt64(+1));
  
  ASSERT_TRUE(isDomainNegativePositive(&domain3));
  
  DomainInteger domain4(DomainElementManager::getInstance().createDomainElementInt64(0),
                        DomainElementManager::getInstance().createDomainElementInt64(0));
  
  ASSERT_TRUE(isDomainNegativePositive(&domain4));
  
  DomainInteger domain5(DomainElementManager::getInstance().createDomainElementInt64(-2),
                        DomainElementManager::getInstance().createDomainElementInt64(-1));
  
  ASSERT_FALSE(isDomainNegativePositive(&domain5));
  
  DomainInteger domain6(DomainElementManager::getInstance().createDomainElementInt64(+1),
                        DomainElementManager::getInstance().createDomainElementInt64(+2));
  
  ASSERT_FALSE(isDomainNegativePositive(&domain6));
}//isDomainNegativePositive

OPTILAB_TEST(PropagatorUtils, getSingleton)
{
  using namespace Core;
  using namespace Core::PropagatorUtils;
  DomainInteger domain(DomainElementManager::getInstance().createDomainElementInt64(-1),
                        DomainElementManager::getInstance().createDomainElementInt64(+1));
  
  ASSERT_TRUE(DomainElementVoid::isa(getSingleton(&domain)));
  
  // Reduce to singleton domain
  domain.shrinkOnLowerBound(DomainElementInt64::cast(DomainElementManager::getInstance().createDomainElementInt64(+1)));
  ASSERT_EQ(domain.getSize(), 1);
  
  ASSERT_TRUE(DomainElement::isEqual(getSingleton(&domain), DomainElementManager::getInstance().createDomainElementInt64(+1)));
  
  // Empty domain
  domain.shrinkOnLowerBound(DomainElementInt64::cast(DomainElementManager::getInstance().createDomainElementInt64(+2)));
  ASSERT_EQ(domain.getSize(), 0);
  
  ASSERT_TRUE(DomainElementVoid::isa(getSingleton(&domain)));
}//getSingleton

OPTILAB_TEST(PropagatorUtils, toFitness)
{
  // Test "toFitness" function
  using namespace Core;
  using namespace Core::PropagatorUtils;
  
  INT_64 intVal = 10;
  auto intToDbl = toFitness(DomainElementManager::getInstance().createDomainElementInt64(intVal));
  EXPECT_EQ(intVal, intToDbl);
  
  REAL realVal = 20.56;
  auto dblToDbl = toFitness(DomainElementManager::getInstance().createDomainElementReal(realVal));
  EXPECT_EQ(realVal, dblToDbl);
}//toFitness
