
#include "utest_globals.hpp"

#include "PropagatorTimes.hpp"
#include "ConstraintTestInc.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"

namespace Core {
  
  class PropagatorTernaryTest : public PropagatorTimesBound {
  public:
    enum TernaryDomainSignConfigTest : int {
      TDSC_PPP = 0 // D0 > 0 /\ D1 > 0 /\ D2 > 0
      , TDSC_PPN     // D0 > 0 /\ D1 > 0 /\ D2 < 0
      , TDSC_PPX     // D0 > 0 /\ D1 > 0 /\ D2.LB <= 0 /\ D2.UB >= 0
      , TDSC_PNP     // D0 > 0 /\ D1 < 0 /\ D2 > 0
      , TDSC_PNN     // D0 > 0 /\ D1 < 0 /\ D2 < 0
      , TDSC_PNX     // D0 > 0 /\ D1 < 0 /\ D2.LB <= 0 /\ D2.UB >= 0
      , TDSC_PXP     // D0 > 0 /\ D1.LB <= 0 /\ D1.UB >= 0 /\ D2 > 0
      , TDSC_PXN     // D0 > 0 /\ D1.LB <= 0 /\ D1.UB >= 0 /\ D2 < 0
      , TDSC_PXX     // D0 > 0 /\ D1.LB <= 0 /\ D1.UB >= 0/\ D2.LB <= 0 /\ D2.UB >= 0
      , TDSC_NPP     // D0 < 0 /\ D1 > 0 /\ D2 > 0
      , TDSC_NPN     // D0 < 0 /\ D1 > 0 /\ D2 < 0
      , TDSC_NPX     // D0 < 0 /\ D1 > 0 /\ D2.LB <= 0 /\ D2.UB >= 0
      , TDSC_NNP     // D0 < 0 /\ D1 < 0 /\ D2 > 0
      , TDSC_NNN     // D0 < 0 /\ D1 < 0 /\ D2 < 0
      , TDSC_NNX     // D0 < 0 /\ D1 < 0 /\ D2.LB <= 0 /\ D2.UB >= 0
      , TDSC_NXP     // D0 < 0 /\ D1.LB <= 0 /\ D1.UB >= 0 /\ D2 > 0
      , TDSC_NXN     // D0 < 0 /\ D1.LB <= 0 /\ D1.UB >= 0 /\ D2 < 0
      , TDSC_NXX     // D0 < 0 /\ D1.LB <= 0 /\ D1.UB >= 0/\ D2.LB <= 0 /\ D2.UB >= 0
      , TDSC_XPP     // D0.LB <= 0 /\ D0.UB >= 0 /\ D1 > 0 /\ D2 > 0
      , TDSC_XPN     // D0.LB <= 0 /\ D0.UB >= 0 /\ D1 > 0 /\ D2 < 0
      , TDSC_XPX     // D0.LB <= 0 /\ D0.UB >= 0 /\ D1 > 0 /\ D2.LB <= 0 /\ D2.UB >= 0
      , TDSC_XNP     // D0.LB <= 0 /\ D0.UB >= 0 /\ D1 < 0 /\ D2 > 0
      , TDSC_XNN     // D0.LB <= 0 /\ D0.UB >= 0 /\ D1 < 0 /\ D2 < 0
      , TDSC_XNX     // D0.LB <= 0 /\ D0.UB >= 0 /\ D1 < 0 /\ D2.LB <= 0 /\ D2.UB >= 0
      , TDSC_XXP     // D0.LB <= 0 /\ D0.UB >= 0 /\ D1.LB <= 0 /\ D1.UB >= 0 /\ D2 > 0
      , TDSC_XXN     // D0.LB <= 0 /\ D0.UB >= 0 /\ D1.LB <= 0 /\ D1.UB >= 0 /\ D2 < 0
      , TDSC_XXX     // D0.LB <= 0 /\ D0.UB >= 0 /\ D1.LB <= 0 /\ D1.UB >= 0/\ D2.LB <= 0 /\ D2.UB >= 0
    };
    
    PropagatorTernaryTest(Domain *aDom0, Domain *aDom1, Domain *aDom2)
    : PropagatorTimesBound(aDom0, aDom1, aDom2)
    {
    }
    
    ~PropagatorTernaryTest()
    {
    }
    
    /// Expose method for testing
    TernaryDomainSignConfig getTernaryDomainSignConfig() const
    {
      return PropagatorTimesBound::getTernaryDomainSignConfig();
    }
    
    
  };
}// end namespace Core

/*********************** Test ***********************/

OPTILAB_TEST(PropagatorTernary, getTernaryDomainSignConfigEdge)
{
  using namespace Core;
  
  DomainIntPtr DP1 = getDomainInt(+0, +1);
  DomainIntPtr DP2 = getDomainInt(-1, +0);
  DomainIntPtr DP3 = getDomainInt(+0, +0);
  
  PropagatorTernaryTest ptXXX1(DP1.get(), DP1.get(), DP1.get());
  PropagatorTernaryTest ptXXX2(DP2.get(), DP2.get(), DP2.get());
  PropagatorTernaryTest ptXXX3(DP3.get(), DP3.get(), DP3.get());
  
  ASSERT_EQ(static_cast<int>(ptXXX1.getTernaryDomainSignConfig()),
            static_cast<int>(PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_XXX));
  ASSERT_EQ(static_cast<int>(ptXXX2.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_XXX));
  ASSERT_EQ(static_cast<int>(ptXXX3.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_XXX));
}//getTernaryDomainSignConfigEdge

OPTILAB_TEST(PropagatorTernary, getTernaryDomainSignConfig)
{
  using namespace Core;
  
  DomainIntPtr D0P = getDomainInt(+10, +20);
  DomainIntPtr D1P = getDomainInt(+10, +20);
  DomainIntPtr D2P = getDomainInt(+10, +20);
  
  DomainIntPtr D0N = getDomainInt(-20, -10);
  DomainIntPtr D1N = getDomainInt(-20, -10);
  DomainIntPtr D2N = getDomainInt(-20, -10);
  
  DomainIntPtr D0X = getDomainInt(-20, +10);
  DomainIntPtr D1X = getDomainInt(-20, +10);
  DomainIntPtr D2X = getDomainInt(-20, +10);
  
  // Create propagators according to DomainSignConfig
  PropagatorTernaryTest ptPPP(D0P.get(), D1P.get(), D2P.get());
  PropagatorTernaryTest ptPPN(D0P.get(), D1P.get(), D2N.get());
  PropagatorTernaryTest ptPPX(D0P.get(), D1P.get(), D2X.get());
  PropagatorTernaryTest ptPNP(D0P.get(), D1N.get(), D2P.get());
  PropagatorTernaryTest ptPNN(D0P.get(), D1N.get(), D2N.get());
  PropagatorTernaryTest ptPNX(D0P.get(), D1N.get(), D2X.get());
  PropagatorTernaryTest ptPXP(D0P.get(), D1X.get(), D2P.get());
  PropagatorTernaryTest ptPXN(D0P.get(), D1X.get(), D2N.get());
  PropagatorTernaryTest ptPXX(D0P.get(), D1X.get(), D2X.get());
  
  PropagatorTernaryTest ptNPP(D0N.get(), D1P.get(), D2P.get());
  PropagatorTernaryTest ptNPN(D0N.get(), D1P.get(), D2N.get());
  PropagatorTernaryTest ptNPX(D0N.get(), D1P.get(), D2X.get());
  PropagatorTernaryTest ptNNP(D0N.get(), D1N.get(), D2P.get());
  PropagatorTernaryTest ptNNN(D0N.get(), D1N.get(), D2N.get());
  PropagatorTernaryTest ptNNX(D0N.get(), D1N.get(), D2X.get());
  PropagatorTernaryTest ptNXP(D0N.get(), D1X.get(), D2P.get());
  PropagatorTernaryTest ptNXN(D0N.get(), D1X.get(), D2N.get());
  PropagatorTernaryTest ptNXX(D0N.get(), D1X.get(), D2X.get());
  
  PropagatorTernaryTest ptXPP(D0X.get(), D1P.get(), D2P.get());
  PropagatorTernaryTest ptXPN(D0X.get(), D1P.get(), D2N.get());
  PropagatorTernaryTest ptXPX(D0X.get(), D1P.get(), D2X.get());
  PropagatorTernaryTest ptXNP(D0X.get(), D1N.get(), D2P.get());
  PropagatorTernaryTest ptXNN(D0X.get(), D1N.get(), D2N.get());
  PropagatorTernaryTest ptXNX(D0X.get(), D1N.get(), D2X.get());
  PropagatorTernaryTest ptXXP(D0X.get(), D1X.get(), D2P.get());
  PropagatorTernaryTest ptXXN(D0X.get(), D1X.get(), D2N.get());
  PropagatorTernaryTest ptXXX(D0X.get(), D1X.get(), D2X.get());
  
  ASSERT_EQ(static_cast<int>(ptPPP.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_PPP));
  ASSERT_EQ(static_cast<int>(ptPPN.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_PPN));
  ASSERT_EQ(static_cast<int>(ptPPX.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_PPX));
  ASSERT_EQ(static_cast<int>(ptPNP.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_PNP));
  ASSERT_EQ(static_cast<int>(ptPNN.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_PNN));
  ASSERT_EQ(static_cast<int>(ptPNX.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_PNX));
  ASSERT_EQ(static_cast<int>(ptPXP.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_PXP));
  ASSERT_EQ(static_cast<int>(ptPXN.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_PXN));
  ASSERT_EQ(static_cast<int>(ptPXX.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_PXX));
  
  ASSERT_EQ(static_cast<int>(ptNPP.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_NPP));
  ASSERT_EQ(static_cast<int>(ptNPN.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_NPN));
  ASSERT_EQ(static_cast<int>(ptNPX.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_NPX));
  ASSERT_EQ(static_cast<int>(ptNNP.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_NNP));
  ASSERT_EQ(static_cast<int>(ptNNN.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_NNN));
  ASSERT_EQ(static_cast<int>(ptNNX.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_NNX));
  ASSERT_EQ(static_cast<int>(ptNXP.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_NXP));
  ASSERT_EQ(static_cast<int>(ptNXN.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_NXN));
  ASSERT_EQ(static_cast<int>(ptNXX.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_NXX));
  
  ASSERT_EQ(static_cast<int>(ptXPP.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_XPP));
  ASSERT_EQ(static_cast<int>(ptXPN.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_XPN));
  ASSERT_EQ(static_cast<int>(ptXPX.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_XPX));
  ASSERT_EQ(static_cast<int>(ptXNP.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_XNP));
  ASSERT_EQ(static_cast<int>(ptXNN.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_XNN));
  ASSERT_EQ(static_cast<int>(ptXNX.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_XNX));
  ASSERT_EQ(static_cast<int>(ptXXP.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_XXP));
  ASSERT_EQ(static_cast<int>(ptXXN.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_XXN));
  ASSERT_EQ(static_cast<int>(ptXXX.getTernaryDomainSignConfig()),
            static_cast<int>(Core::PropagatorTernaryTest::TernaryDomainSignConfigTest::TDSC_XXX));
}//getTernaryDomainSignConfig

