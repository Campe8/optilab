#include "utest_globals.hpp"

#include "PropagatorArrayBoolOr.hpp"
#include "ConstraintTestInc.hpp"

namespace {
  class PropagatorArrayBoolOrTest : public ::testing::Test {
  public:
    PropagatorArrayBoolOrTest()
    {
    }
    
    ~PropagatorArrayBoolOrTest()
    {
    }
    
    void setDomains(std::vector<UBOOL>&& aDomains,
                    std::array<UBOOL, 2>&& aDomain)
    {
      pDomains = std::make_shared<Core::DomainArray>(aDomains.size()/2);
      pDomain = std::make_shared<Core::DomainBoolean>();
      
      // pDomains
      std::size_t idx{0};
      pTempDomainArray.clear();
      for(std::size_t domIdx = 0; domIdx < aDomains.size(); ++domIdx)
      {
        if(domIdx > 0 && (domIdx % 2 != 0))
        {
          pTempDomainArray.push_back(getDomainBool());
          // domain is [true]
          if(aDomains[domIdx-1])
          {
            pTempDomainArray.back()->setTrue();
          }
          // domain is [false]
          else if(!aDomains[domIdx])
          {
            pTempDomainArray.back()->setFalse();
          }
          pDomains->assignElementToCell(idx++, pTempDomainArray.back().get());
        }
      }
      
      // pDomain
      pDomain = getDomainBool();
      // domain is [true]
      if(aDomain[0])
      {
        pDomain->setTrue();
      }
      // domain is [false]
      else if(!aDomain[1])
      {
        pDomain->setFalse();
      }
    }//setDomains
    
    double getFitness()
    {
      std::unique_ptr<Core::PropagatorArrayBoolOr> propagator(new Core::PropagatorArrayBoolOr(*(pDomains.get()),
                                                                                              pDomain.get()));
      return propagator->getFitness();
    }//getFitness

    void postPropagator(std::vector<UBOOL>&& aDomains,
                        std::array<UBOOL, 2>&& aDomain,
                        Core::PropagationEvent aEvent)
    {
      std::unique_ptr<Core::PropagatorArrayBoolOr> propagator(new Core::PropagatorArrayBoolOr(*(pDomains.get()),
                                                                                              pDomain.get()));
      
      Core::PropagationEvent event = propagator->post();
      ASSERT_EQ(event, aEvent);
      
      std::size_t domIdxToCompare{0};
      DomainIntPtr domain;
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        for(std::size_t domIdx = 1; domIdx < aDomains.size(); ++domIdx)
        {
          if(domIdx > 0 && (domIdx % 2 != 0))
          {
            domain = getDomainInt(aDomains[domIdx-1], aDomains[domIdx]);
            ASSERT_TRUE(pDomains->operator[](domIdxToCompare)->lowerBound()->isEqual(domain->lowerBound()));
            ASSERT_TRUE(pDomains->operator[](domIdxToCompare)->upperBound()->isEqual(domain->upperBound()));
            domIdxToCompare++;
          }
        }
      }
      domain = getDomainInt(aDomain[0], aDomain[1]);
      ASSERT_TRUE(pDomain->lowerBound()->isEqual(domain->lowerBound()));
      ASSERT_TRUE(pDomain->upperBound()->isEqual(domain->upperBound()));
    }//postPropagator
    
    void runPropagationAndCheck(std::vector<UBOOL>&& aDomains,
                                std::array<UBOOL, 2>&& aDomain,
                                Core::PropagationEvent aEvent)
    {
      // Perform propagation
      std::unique_ptr<Core::PropagatorArrayBoolOr> propagator(new Core::PropagatorArrayBoolOr(*(pDomains.get()),
                                                                                                pDomain.get()));
      
      Core::PropagationEvent event = propagator->propagate();
      ASSERT_EQ(event, aEvent);
      
      std::size_t domIdxToCompare{0};
      DomainIntPtr domain;
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        for(std::size_t domIdx = 1; domIdx < aDomains.size(); ++domIdx)
        {
          if(domIdx > 0 && (domIdx % 2 != 0))
          {
            domain = getDomainInt(aDomains[domIdx-1], aDomains[domIdx]);
            ASSERT_TRUE(pDomains->operator[](domIdxToCompare)->lowerBound()->isEqual(domain->lowerBound()));
            ASSERT_TRUE(pDomains->operator[](domIdxToCompare)->upperBound()->isEqual(domain->upperBound()));
            domIdxToCompare++;
          }
        }
      }
      domain = getDomainInt(aDomain[0], aDomain[1]);
      ASSERT_TRUE(pDomain->lowerBound()->isEqual(domain->lowerBound()));
      ASSERT_TRUE(pDomain->upperBound()->isEqual(domain->upperBound()));
    }//runPropagationAndCheck
    
    
  protected:
    // Domain array
    Core::DomainArraySPtr pDomains;
    // bool domain
    DomainBoolPtr pDomain;
    
    // Array of shared ptr used to preserve domains
    std::vector<DomainBoolPtr> pTempDomainArray;
  };
}// end namespace

OPTILAB_TEST_F(PropagatorArrayBoolOrTest, post)
{
  using namespace Core;
  
  // [F, F] \/ [T, T] <=> [F, T] => [F, F] \/ [T, T] <=> [T, T] subsumed
  setDomains({false, false, true, true}, {false, true});
  postPropagator({false, false, true, true}, {true, true}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [T, T] \/ [T, T] <=> [F, T] => [T, T] \/ [T, T] <=> [T, T] subsumed
  setDomains({true, true, true, true}, {false, true});
  postPropagator({true, true, true, true}, {true, true}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] \/ [T, T] <=> [T, T] => [F, T] \/ [T, T] <=> [T, T] subsumed
  setDomains({false, true, true, true}, {true, true});
  postPropagator({false, true, true, true}, {true, true}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, F] \/ [F, F] <=> [T, T] => [F, F] \/ [F, F] <=> [T, T] fail
  setDomains({false, false, false, false}, {true, true});
  postPropagator({false, false, false, false}, {true, true}, PropagationEvent::PROP_EVENT_FAIL);
  
  // [F, F] \/ [T, T] <=> [T, T] => [F, F] \/ [T, T] <=> [T, T] subsumed
  setDomains({false, false, true, true}, {true, true});
  postPropagator({false, false, true, true}, {true, true}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [T, T] \/ [T, T] <=> [F, F] => [T, T] \/ [T, T] <=> [F, F] fail
  setDomains({true, true, true, true}, {false, false});
  postPropagator({true, true, true, true}, {false, false}, PropagationEvent::PROP_EVENT_FAIL);
  
  // [F, T] \/ [T, T] <=> [F, T] => [F, T] \/ [T, T] <=> [T, T] subsumed
  setDomains({false, true, true, true}, {false, true});
  postPropagator({false, true, true, true}, {true, true}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] \/ [F, T] <=> [F, F] => [F, F] \/ [F, F] <=> [F, F] subsumed
  setDomains({false, true, false, true}, {false, false});
  postPropagator({false, false, false, false}, {false, false}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] \/ [F, T] <=> [F, T] => [F, T] \/ [F, T] <=> [F, T] unspec
  setDomains({false, true, false, true}, {false, true});
  postPropagator({false, true, false, true}, {false, true}, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
}//post

OPTILAB_TEST_F(PropagatorArrayBoolOrTest, simpleCases)
{
  using namespace Core;
  
  // [F, F] \/ [T, T] <=> [F, T] => [F, F] \/ [T, T] <=> [T, T] subsumed
  setDomains({false, false, true, true}, {false, true});
  runPropagationAndCheck({false, false, true, true}, {true, true}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [T, T] \/ [T, T] <=> [F, T] => [T, T] \/ [T, T] <=> [T, T] subsumed
  setDomains({true, true, true, true}, {false, true});
  runPropagationAndCheck({true, true, true, true}, {true, true}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] \/ [T, T] <=> [T, T] => [F, T] \/ [T, T] <=> [T, T] subsumed
  setDomains({false, true, true, true}, {true, true});
  runPropagationAndCheck({false, true, true, true}, {true, true}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] \/ [F, F] <=> [F, T] => [F, T] \/ [F, F] <=> [F, T] fixpoint
  setDomains({false, true, false, false}, {false, true});
  runPropagationAndCheck({false, true, false, false}, {false, true}, PropagationEvent::PROP_EVENT_FIXPOINT);
  
  // [F, F] \/ [F, F] <=> [T, T] => [F, F] \/ [F, F] <=> [T, T] fail
  setDomains({false, false, false, false}, {true, true});
  runPropagationAndCheck({false, false, false, false}, {true, true}, PropagationEvent::PROP_EVENT_FAIL);
  
  // [F, F] \/ [T, T] <=> [T, T] => [F, F] \/ [T, T] <=> [T, T] subsumed
  setDomains({false, false, true, true}, {true, true});
  runPropagationAndCheck({false, false, true, true}, {true, true}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [T, T] \/ [T, T] <=> [F, F] => [T, T] \/ [T, T] <=> [F, F] fail
  setDomains({true, true, true, true}, {false, false});
  runPropagationAndCheck({true, true, true, true}, {false, false}, PropagationEvent::PROP_EVENT_FAIL);
  
  // [F, T] \/ [T, T] <=> [F, T] => [F, T] \/ [T, T] <=> [T, T] subsumed
  setDomains({false, true, true, true}, {false, true});
  runPropagationAndCheck({false, true, true, true}, {true, true}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] \/ [F, T] <=> [F, F] => [F, F] \/ [F, F] <=> [F, F] subsumed
  setDomains({false, true, false, true}, {false, false});
  runPropagationAndCheck({false, false, false, false}, {false, false}, PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // [F, T] \/ [F, T] <=> [F, T] => [F, T] \/ [F, T] <=> [F, T] fixpoint
  setDomains({false, true, false, true}, {false, true});
  runPropagationAndCheck({false, true, false, true}, {false, true}, PropagationEvent::PROP_EVENT_FIXPOINT);
}//runPropagationAndCheck

OPTILAB_TEST_F(PropagatorArrayBoolOrTest, getFitness)
{
  using namespace Core;
  
  setDomains({false, false}, {false, false});
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());

  setDomains({true, true}, {true, true});
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());

  setDomains({false, true}, {false, false});
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness());

  setDomains({false, false}, {false, true});
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness());

  setDomains({false, false, false, false, false, false}, {false, false});
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());

  setDomains({false, false, false, false, true, true}, {false, false});
  EXPECT_EQ(1, getFitness());

  setDomains({true, true, false, false, true, true}, {false, false});
  EXPECT_EQ(2, getFitness());

  setDomains({true, true, true, true, true, true}, {false, false});
  EXPECT_EQ(3, getFitness());

  setDomains({false, false, false, false, false, false}, {true, true});
  EXPECT_EQ(1, getFitness());

  setDomains({false, false, false, false, true, true}, {true, true});
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());

  setDomains({true, true, false, false, true, true}, {true, true});
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());

  setDomains({true, true, true, true, true, true}, {true, true});
  EXPECT_EQ(Propagator::getMinFitness(), getFitness());

  setDomains({true, true, false, false, false, true}, {true, true});
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness());

  setDomains({true, true, false, false, true, true}, {false, true});
  EXPECT_EQ(Propagator::getUnspecFitness(), getFitness());
}
