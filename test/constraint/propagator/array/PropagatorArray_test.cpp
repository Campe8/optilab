
#include "utest_globals.hpp"

#include "PropagatorArray.hpp"
#include "ConstraintTestInc.hpp"

namespace Core {
  
  class PropagatorArrayTest : public PropagatorArray {
  public:
    
    PropagatorArrayTest(std::size_t aContextSize, DomainArray& aDomArray)
    : PropagatorArray(PropagatorSemantics(aContextSize,
                                          PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
                                          PropagationPriority::PROP_PRIORITY_LINEAR,
                                          PropagatorSemanticType::PROP_SEMANTIC_TYPE_ARRAY_BOOL_AND,
                                          PropagatorSemanticClass::PROP_SEMANTIC_CLASS_ARRAY),
                      aDomArray)
    {
    }
    
    PropagatorArrayTest(std::size_t aContextSize, DomainElementArray& aDomElemArray)
    : PropagatorArray(PropagatorSemantics(aContextSize,
                                          PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
                                          PropagationPriority::PROP_PRIORITY_LINEAR,
                                          PropagatorSemanticType::PROP_SEMANTIC_TYPE_ARRAY_BOOL_AND,
                                          PropagatorSemanticClass::PROP_SEMANTIC_CLASS_ARRAY),
                      aDomElemArray)
    {
    }
    
    ~PropagatorArrayTest()
    {
    }
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
    PropagationEvent post() override
    {
      return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    }
    
    /// Expose methods for testing
    inline bool hasDomainArray() const
    {
      return PropagatorArray::hasDomainArray();
    }
    
    inline bool hasDomainElementArray() const
    {
      return PropagatorArray::hasDomainElementArray();
    }
    
    inline DomainElement* arraySize() const
    {
      return PropagatorArray::arraySize();
    }
    
    inline INT_64 arraySizeINT64() const
    {
      return PropagatorArray::arraySizeINT64();
    }
    
    bool isArraySingleton()
    {
      return PropagatorArray::isArraySingleton();
    }
    
    Domain* domainArrayAt(DomainElement* aDomainElementIdx)
    {
      return PropagatorArray::domainArrayAt(aDomainElementIdx);
    }
    
    Domain* domainArrayAtINT64(INT_64 aIdx)
    {
      return PropagatorArray::domainArrayAtINT64(aIdx);
    }
    
    DomainElement* domainElementArrayAt(DomainElement* aDomainElementIdx)
    {
      return PropagatorArray::domainElementArrayAt(aDomainElementIdx);
    }
    
    DomainElement* domainElementArrayAtINT64(INT_64 aIdx)
    {
      return PropagatorArray::domainElementArrayAtINT64(aIdx);
    }
    
  
  protected:
    PropagationEvent runPropagation() override
    {
      return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    }
  
  };
}// end namespace Core

/*********************** Test ***********************/

OPTILAB_TEST(PropagatorArrayTest, HasDomain)
{
  using namespace Core;

  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  
  DomainArray domains1(3);
  
  DomainBoolPtr domain0 = getDomainBool();
  domains1.assignElementToCell(0, domain0.get());
  domains1.assignElementToCell(1, domain0.get());
  domains1.assignElementToCell(2, domain0.get());
  
  // array of domains
  PropagatorArrayTest propagator1(3, domains1);
  
  ASSERT_TRUE(propagator1.hasDomainArray());
  ASSERT_FALSE(propagator1.hasDomainElementArray());
  
  
  DomainElementArray domains2(4);
  domains2.assignElementToCell(0, elementManager.createDomainElementInt64(0));
  domains2.assignElementToCell(1, elementManager.createDomainElementInt64(0));
  domains2.assignElementToCell(2, elementManager.createDomainElementInt64(0));
  domains2.assignElementToCell(3, elementManager.createDomainElementInt64(1));
  
  // array of domain elements
  PropagatorArrayTest propagator2(4, domains2);
  
  ASSERT_FALSE(propagator2.hasDomainArray());
  ASSERT_TRUE(propagator2.hasDomainElementArray());
}

OPTILAB_TEST(PropagatorArrayTest, Size)
{
  using namespace Core;

  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  
  DomainArray domains1(3);
  
  DomainBoolPtr domain0 = getDomainBool();
  domains1.assignElementToCell(0, domain0.get());
  domains1.assignElementToCell(1, domain0.get());
  domains1.assignElementToCell(2, domain0.get());
  
  // array of domains
  PropagatorArrayTest propagator1(3, domains1);
  
  ASSERT_EQ(propagator1.arraySize(), elementManager.createDomainElementInt64(3));
  ASSERT_EQ(propagator1.arraySizeINT64(), 3);
  
  
  DomainElementArray domains2(4);
  domains2.assignElementToCell(0, elementManager.createDomainElementInt64(0));
  domains2.assignElementToCell(1, elementManager.createDomainElementInt64(0));
  domains2.assignElementToCell(2, elementManager.createDomainElementInt64(0));
  domains2.assignElementToCell(3, elementManager.createDomainElementInt64(1));
  
  // array of domain elements
  PropagatorArrayTest propagator2(4, domains2);
  
  ASSERT_EQ(propagator2.arraySize(), elementManager.createDomainElementInt64(4));
  ASSERT_EQ(propagator2.arraySizeINT64(), 4);
}

OPTILAB_TEST(PropagatorArrayTest, IsArraySingleton)
{
  using namespace Core;

  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  DomainArray domains(2);
  
  DomainBoolPtr domain0 = getDomainBool();
  DomainBoolPtr domain1 = getDomainBool();
  
  domains.assignElementToCell(0, domain0.get());
  domains.assignElementToCell(1, domain1.get());
  PropagatorArrayTest propagator1(2, domains);
  ASSERT_FALSE(propagator1.isArraySingleton());
  
  domain0->setTrue();
  PropagatorArrayTest propagator2(2, domains);
  ASSERT_FALSE(propagator2.isArraySingleton());
  
  domain1->setFalse();
  PropagatorArrayTest propagator3(2, domains);
  ASSERT_TRUE(propagator3.isArraySingleton());
}

OPTILAB_TEST(PropagatorArrayTest, DomainAt)
{
  using namespace Core;

  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  DomainArray domains1(2);
  
  DomainBoolPtr domain0 = getDomainBool();
  DomainBoolPtr domain1 = getDomainBool();
  domain1->setFalse();
  
  domains1.assignElementToCell(0, domain0.get());
  domains1.assignElementToCell(1, domain1.get());
  
  // array of domains
  PropagatorArrayTest propagator1(2, domains1);
  
  // domainArrayAt
  ASSERT_EQ(propagator1.domainArrayAt(elementManager.createDomainElementInt64(0)), domain0.get());
  ASSERT_NE(propagator1.domainArrayAt(elementManager.createDomainElementInt64(0)), domain1.get());
  ASSERT_NE(propagator1.domainArrayAt(elementManager.createDomainElementInt64(1)), domain0.get());
  ASSERT_EQ(propagator1.domainArrayAt(elementManager.createDomainElementInt64(1)), domain1.get());
  
  // domainArrayAtINT64
  ASSERT_EQ(propagator1.domainArrayAtINT64(0), domain0.get());
  ASSERT_NE(propagator1.domainArrayAtINT64(0), domain1.get());
  ASSERT_NE(propagator1.domainArrayAtINT64(1), domain0.get());
  ASSERT_EQ(propagator1.domainArrayAtINT64(1), domain1.get());
  
  // domainElementArrayAt
  ASSERT_FALSE(propagator1.domainElementArrayAt(elementManager.createDomainElementInt64(0)));
  
  // domainElementArrayAtINT64
  ASSERT_FALSE(propagator1.domainElementArrayAtINT64(0));
  
  
  DomainElementArray domains2(2);
  domains2.assignElementToCell(0, elementManager.createDomainElementInt64(0));
  domains2.assignElementToCell(1, elementManager.createDomainElementInt64(1));
  
  // array of domain elements
  PropagatorArrayTest propagator2(2, domains2);
  
  // domainArrayAt
  ASSERT_FALSE(propagator2.domainArrayAt(elementManager.createDomainElementInt64(0)));
  
  // domainArrayAtINT64
  ASSERT_FALSE(propagator2.domainArrayAtINT64(0));
  
  // domainElementArrayAt
  ASSERT_EQ(propagator2.domainElementArrayAt(elementManager.createDomainElementInt64(0)), elementManager.createDomainElementInt64(0));
  ASSERT_NE(propagator2.domainElementArrayAt(elementManager.createDomainElementInt64(0)), elementManager.createDomainElementInt64(1));
  ASSERT_NE(propagator2.domainElementArrayAt(elementManager.createDomainElementInt64(1)), elementManager.createDomainElementInt64(0));
  ASSERT_EQ(propagator2.domainElementArrayAt(elementManager.createDomainElementInt64(1)), elementManager.createDomainElementInt64(1));
  
  // domainElementArrayAtINT64
  ASSERT_EQ(propagator2.domainElementArrayAtINT64(0), elementManager.createDomainElementInt64(0));
  ASSERT_NE(propagator2.domainElementArrayAtINT64(0), elementManager.createDomainElementInt64(1));
  ASSERT_NE(propagator2.domainElementArrayAtINT64(1), elementManager.createDomainElementInt64(0));
  ASSERT_EQ(propagator2.domainElementArrayAtINT64(1), elementManager.createDomainElementInt64(1));
}
