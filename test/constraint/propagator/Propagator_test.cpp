
#include "utest_globals.hpp"

#include "PropagatorTest.hpp"
#include "ConstraintTestInc.hpp"
#include "FilteringAlgorithmLQ.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"

OPTILAB_TEST_F(PropagatorTest, basics)
{
  ASSERT_FALSE(ptcTest1.isPropagatorDisabled());
  ptcTest1.disablePropagator();
  ASSERT_TRUE(ptcTest1.isPropagatorDisabled());
  ptcTest1.enablePropagator();
  ASSERT_FALSE(ptcTest1.isPropagatorDisabled());
  
  // Propagator type class
  ASSERT_EQ(ptcTest2.getPropagatorTypeClass(), PropagatorSemanticClass::PROP_SEMANTIC_CLASS_ARITHMETIC);
  // Propagator type id
  ASSERT_EQ(ptcTest2.getPropagatorTypeId(), PropagatorSemanticType::PROP_SEMANTIC_TYPE_TIMES);
  // Strategy type
  ASSERT_EQ(ptcTest2.getPropagatorStrategyType(), PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND);
  // Cost function
  ASSERT_EQ(ptcTest2.getCost().getCost(), PropagationPriority::PROP_PRIORITY_BINARY);
  
  // Run propagation with disabled propagator
  ptcTest2.disablePropagator();
  ASSERT_TRUE(ptcTest2.isPropagatorDisabled());
  auto propagationEvent = ptcTest2.propagate();
  ASSERT_EQ(propagationEvent, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // Run propagation with enabled propagator
  ptcTest2.enablePropagator();
  ASSERT_FALSE(ptcTest2.isPropagatorDisabled());
  propagationEvent = ptcTest2.propagate();
  ASSERT_EQ(propagationEvent, PropagationEvent::PROP_EVENT_RUN_UNSPEC);
  
  // Context size
  ASSERT_EQ(ptcTest1.getContextSize(), 0);
  ASSERT_EQ(ptcTest2.getContextSize(), 2);
}//isTrueFalse

OPTILAB_TEST_F(PropagatorTest, filterStrategy)
{
  using namespace Core;
  
  // Register filter strategy
  ptcTest2.registerFilteringStrategy(Core::FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
  // Set current filter strategy
  ptcTest2.setFilterStrategy(Core::FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
  // Verify that the current filter strategy is ABS
  ASSERT_TRUE(FilteringAlgorithmLQ::isa(ptcTest2.getCurrentFilterStrategy().get()));
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  DomainElementInt64 *upperBound = elementManager.createDomainElementInt64(+8);
  DomainIntPtr domainOutput1 = getDomainInt(-10, +10);
  DomainIntPtr domainOutput2 = getDomainInt(+9, +10);
  
  // Set domain output
  ptcTest2.addContextDomain(0, domainOutput1.get());
  ptcTest2.addContextDomain(1, domainOutput2.get());
  ptcTest2.setFilterOutput(0);
  // Set filter input
  ptcTest2.setFilterInput(0, upperBound);
  
  // Apply filter strategy
  Domain* domOut= ptcTest2.applyFilteringStrategy();
  
  // Check that the domain in output is the domain set for output
  ASSERT_EQ(domOut, domainOutput1.get());
  ASSERT_TRUE(domOut->upperBound()->isEqual(upperBound));
}//filterStrategy
