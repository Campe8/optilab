#pragma once

#include "ConstraintAndLabelTestInc.hpp"

#include "VariableFactory.hpp"

#include "SearchEngine.hpp"
#include "DepthFirstSearch.hpp"
#include "SolutionManager.hpp"

#include "Constraint.hpp"

#include <vector>
#include <memory>
#include <utility>
#include <set>

using namespace Core;
using namespace Search;
using namespace Semantics;

class ConstraintAndLabelTest {
public:
  ConstraintAndLabelTest();
  
  void setEngine(const std::set<Core::VariableSPtr>& aBranchingVariableSet);
  
  void setConstraint(std::shared_ptr<Constraint> aConstraint);
  
  VariableSPtr getVariable(const std::pair<INT_64, INT_64>& aBounds);
  
  inline std::size_t numOfSolutions()
  {
    assert(pDFS);
    return pDFS->solutionManager()->getNumberOfRecordedSolutions();
  }
  
  SearchStatus runEngine();
  
  std::vector<std::pair<INT_64, INT_64>> getSolution(std::size_t aSolutionIdx);
  
private:
  std::unique_ptr<VariableFactory> pVarFactory;
  
  std::size_t pInputOrderVar;
  
  std::unique_ptr<SearchEngine> pEngine;
  std::shared_ptr<DepthFirstSearch> pDFS;
  
  std::set<Core::VariableSPtr> pBranchingVariableSet;
};
