//
// Copyright OptiLab 2016. All rights reserved.
//
// Created by Federico Campeotto on 11/27/2016
//
// Propagator test class
//

#pragma once

#include "utest_globals.hpp"

#include "Propagator.hpp"
#include "PropagatorUtils.hpp"

#include "ConstraintTestInc.hpp"

#include "DomainDefs.hpp"
#include "DomainInteger.hpp"
#include "DomainBoolean.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementVoid.hpp"
#include "DomainElementManager.hpp"


#include "PropagatorBoolLq.hpp"
#include "PropagatorBoolLqReif.hpp"
#include "PropagatorBoolLt.hpp"
#include "PropagatorBoolLtReif.hpp"
#include "PropagatorBoolOr.hpp"
#include "PropagatorBoolXor.hpp"
#include "PropagatorBoolNot.hpp"
#include "PropagatorBoolEq.hpp"
#include "PropagatorBoolAnd.hpp"

namespace Core {
  /// Propagator test class that exposes all the
  /// member of Propagator for testing purposes
  class PropagatorTestClass :public Propagator {
  public:
    PropagatorTestClass(std::size_t aContextSize,
                        PropagatorStrategyType aStrategyType,
                        PropagationPriority aPriority,
                        PropagatorSemanticType aSemanticType,
                        PropagatorSemanticClass aSemanticClass);
    
    ~PropagatorTestClass();
    
    inline DomainEventSet getReevaluationEventSet() const override
    {
      DomainEventSet eventSet {
        std::make_shared<DomainEventSingleton>(),
        std::make_shared<DomainEventLwb>(),
        std::make_shared<DomainEventUpb>(),
        std::make_shared<DomainEventBound>()
      };
      return eventSet;
    }
    
    PropagationEvent post() override
    {
      return runPropagation();
    }
    
    inline void registerFilteringStrategy(FilteringAlgorithmSemanticType aType)
    {
      Propagator::registerFilteringStrategy(aType);
    }
    
    inline std::size_t getContextSize() const
    {
      return Propagator::getContextSize();
    }
    
    inline void addContextDomain(std::size_t aContextDomainIdx, Domain *aDomainPtr)
    {
      Propagator::addContextDomain(aContextDomainIdx, aDomainPtr);
    }
    
    inline Domain* getContextDomain(std::size_t aContextDomainIdx) const
    {
      return Propagator::getContextDomain(aContextDomainIdx);
    }
    
    inline std::size_t numSingletonContextDomains() const
    {
      return Propagator::numSingletonContextDomains();
    }
    
    inline void setFilterStrategy(FilteringAlgorithmSemanticType aFilterStrategy)
    {
      Propagator::setFilterStrategy(aFilterStrategy);
    }
    
    inline FilteringAlgorithmSPtr getCurrentFilterStrategy()
    {
      return Propagator::getCurrentFilterStrategy();
    }
    
    inline void setFilterOutput(std::size_t aContextDomainIdx)
    {
      Propagator::setFilterOutput(aContextDomainIdx);
    }
    
    inline void setFilterOutputPtr(Domain *aDomainOutput)
    {
      Propagator::setFilterOutputPtr(aDomainOutput);
    }
    
    inline void setFilterInput(std::size_t aInputIdx, Domain* aInputDomain)
    {
      Propagator::setFilterInput(aInputIdx, aInputDomain);
    }
    
    inline void setFilterInput(std::size_t aInputIdx, DomainElement* aInputDomainElement)
    {
      Propagator::setFilterInput(aInputIdx, aInputDomainElement);
    }
    
    inline void setFilterInput(DomainArraySPtr aArray)
    {
      Propagator::setFilterInput(aArray);
    }
    
    inline void setFilterInput(DomainElementArraySPtr aArray)
    {
      Propagator::setFilterInput(aArray);
    }
    
    inline Domain* applyFilteringStrategy()
    {
      return Propagator::applyFilteringStrategy();
    }
    
  protected:
    PropagationEvent runPropagation() override
    {
      return PropagationEvent::PROP_EVENT_FIXPOINT;
    }
  };
  
  class PropagatorBoolBinTest {
  public:
    PropagatorBoolBinTest(PropagatorSemanticType aBoolSemanticType)
    : pPropagatorSemanticType(aBoolSemanticType)
    {
    }
    
    ~PropagatorBoolBinTest() = default;
    
    void setDomains(bool aLBD0, bool aUBD0, bool aLBD1, bool aUBD1)
    {
      pDom0 = getDomainBool();
      pDom1 = getDomainBool();
      if(aLBD0 == aUBD0)
      {
        if(aLBD0)
        {
          pDom0->setTrue();
        }
        else
        {
          pDom0->setFalse();
        }
      }
      
      if(aLBD1 == aUBD1)
      {
        if(aLBD1)
        {
          pDom1->setTrue();
        }
        else
        {
          pDom1->setFalse();
        }
      }
    }//setDomains
    
    double getFitness()
    {
      switch (pPropagatorSemanticType) {
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_AND_BIN:
        {
          std::unique_ptr<Core::PropagatorBoolAndBin> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolAndBin(pDom0.get(), pDom1.get()));
          return propagator->getFitness();
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_EQ:
        {
          std::unique_ptr<Core::PropagatorBoolEq> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolEq(pDom0.get(), pDom1.get()));
          return propagator->getFitness();
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_OR_BIN:
        {
          std::unique_ptr<Core::PropagatorBoolOrBin> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolOrBin(pDom0.get(), pDom1.get()));
          return propagator->getFitness();
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_XOR_BIN:
        {
          std::unique_ptr<Core::PropagatorBoolXorBin> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolXorBin(pDom0.get(), pDom1.get()));
          return propagator->getFitness();
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_NOT:
        {
          std::unique_ptr<Core::PropagatorBoolNot> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolNot(pDom0.get(), pDom1.get()));
          return propagator->getFitness();
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_LT:
        {
          std::unique_ptr<Core::PropagatorBoolLt> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolLt(pDom0.get(), pDom1.get()));
          return propagator->getFitness();
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_LQ:
        {
          std::unique_ptr<Core::PropagatorBoolLq> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolLq(pDom0.get(), pDom1.get()));
          return propagator->getFitness();
        }
        default:
          return -1;
      }
    }//getFitness
    
    void postPropagator(bool aD0LB, bool aD0UB,
                        bool aD1LB, bool aD1UB,
                        Core::PropagationEvent aEvent)
    {
      Core::PropagationEvent event = Core::PropagationEvent::PROP_EVENT_FAIL;
      switch (pPropagatorSemanticType) {
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_AND_BIN:
        {
          std::unique_ptr<Core::PropagatorBoolAndBin> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolAndBin(pDom0.get(), pDom1.get()));
          event = propagator->post();
          break;
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_EQ:
        {
          std::unique_ptr<Core::PropagatorBoolEq> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolEq(pDom0.get(), pDom1.get()));
          event = propagator->post();
          break;
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_OR_BIN:
        {
          std::unique_ptr<Core::PropagatorBoolOrBin> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolOrBin(pDom0.get(), pDom1.get()));
          event = propagator->post();
          break;
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_XOR_BIN:
        {
          std::unique_ptr<Core::PropagatorBoolXorBin> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolXorBin(pDom0.get(), pDom1.get()));
          event = propagator->post();
          break;
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_NOT:
        {
          std::unique_ptr<Core::PropagatorBoolNot> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolNot(pDom0.get(), pDom1.get()));
          event = propagator->post();
          break;
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_LT:
        {
          std::unique_ptr<Core::PropagatorBoolLt> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolLt(pDom0.get(), pDom1.get()));
          event = propagator->post();
          break;
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_LQ:
        {
          std::unique_ptr<Core::PropagatorBoolLq> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolLq(pDom0.get(), pDom1.get()));
          event = propagator->post();
          break;
        }
        default:
          break;
      }
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        ASSERT_TRUE(pDom0->lowerBound()->isEqual(getElem(aD0LB ? 1 : 0).get()));
        ASSERT_TRUE(pDom0->upperBound()->isEqual(getElem(aD0UB ? 1 : 0).get()));
        
        ASSERT_TRUE(pDom1->lowerBound()->isEqual(getElem(aD1LB ? 1 : 0).get()));
        ASSERT_TRUE(pDom1->upperBound()->isEqual(getElem(aD1UB ? 1 : 0).get()));
      }
    }//postPropagator
    
    void runPropagationAndCheck(bool aD0LB, bool aD0UB,
                                bool aD1LB, bool aD1UB,
                                Core::PropagationEvent aEvent)
    {
      // Perform propagation
      Core::PropagationEvent event = Core::PropagationEvent::PROP_EVENT_FAIL;
      
      switch (pPropagatorSemanticType) {
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_AND_BIN:
        {
          std::unique_ptr<Core::PropagatorBoolAndBin> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolAndBin(pDom0.get(), pDom1.get()));
          event = propagator->propagate();
          break;
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_EQ:
        {
          std::unique_ptr<Core::PropagatorBoolEq> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolEq(pDom0.get(), pDom1.get()));
          event = propagator->propagate();
          break;
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_OR_BIN:
        {
          std::unique_ptr<Core::PropagatorBoolOrBin> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolOrBin(pDom0.get(), pDom1.get()));
          event = propagator->propagate();
          break;
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_XOR_BIN:
        {
          std::unique_ptr<Core::PropagatorBoolXorBin> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolXorBin(pDom0.get(), pDom1.get()));
          event = propagator->propagate();
          break;
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_NOT:
        {
          std::unique_ptr<Core::PropagatorBoolNot> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolNot(pDom0.get(), pDom1.get()));
          event = propagator->propagate();
          break;
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_LT:
        {
          std::unique_ptr<Core::PropagatorBoolLt> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolLt(pDom0.get(), pDom1.get()));
          event = propagator->propagate();
          break;
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_LQ:
        {
          std::unique_ptr<Core::PropagatorBoolLq> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolLq(pDom0.get(), pDom1.get()));
          event = propagator->propagate();
          break;
        }
        default:
          break;
      }
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        ASSERT_TRUE(pDom0->lowerBound()->isEqual(getElem(aD0LB ? 1 : 0).get()));
        ASSERT_TRUE(pDom0->upperBound()->isEqual(getElem(aD0UB ? 1 : 0).get()));
        
        ASSERT_TRUE(pDom1->lowerBound()->isEqual(getElem(aD1LB ? 1 : 0).get()));
        ASSERT_TRUE(pDom1->upperBound()->isEqual(getElem(aD1UB ? 1 : 0).get()));
      }
    }//runPropagationAndCheck

  protected:
    // Propagator type
    PropagatorSemanticType pPropagatorSemanticType;
    
    // Domain instances
    DomainBoolPtr pDom0;
    DomainBoolPtr pDom1;
  };
  
  class PropagatorBoolTerTest {
  public:
    PropagatorBoolTerTest(PropagatorSemanticType aBoolSemanticType)
    : pPropagatorSemanticType(aBoolSemanticType)
    {
    }
    
    ~PropagatorBoolTerTest()
    {
    }
    
    void setDomains(bool aLBD0, bool aUBD0, bool aLBD1, bool aUBD1, bool aLBD2, bool aUBD2)
    {
      pDom0 = getDomainBool();
      pDom1 = getDomainBool();
      pDom2 = getDomainBool();
      if(aLBD0 == aUBD0)
      {
        if(aLBD0)
        {
          pDom0->setTrue();
        }
        else
        {
          pDom0->setFalse();
        }
      }
      
      if(aLBD1 == aUBD1)
      {
        if(aLBD1)
        {
          pDom1->setTrue();
        }
        else
        {
          pDom1->setFalse();
        }
      }
      
      if(aLBD2 == aUBD2)
      {
        if(aLBD2)
        {
          pDom2->setTrue();
        }
        else
        {
          pDom2->setFalse();
        }
      }
    }//setDomains
    
    double getFitness()
    {
      switch (pPropagatorSemanticType) {
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_AND_TER:
        {
          std::unique_ptr<Core::PropagatorBoolAndTer> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolAndTer(pDom0.get(), pDom1.get(), pDom2.get()));
          return propagator->getFitness();
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_OR_TER:
        {
          std::unique_ptr<Core::PropagatorBoolOrTer> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolOrTer(pDom0.get(), pDom1.get(), pDom2.get()));
          return propagator->getFitness();
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_XOR_TER:
        {
          std::unique_ptr<Core::PropagatorBoolXorTer> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolXorTer(pDom0.get(), pDom1.get(), pDom2.get()));
          return propagator->getFitness();
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_LQ_REIF:
        {
          std::unique_ptr<Core::PropagatorBoolLqReif> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolLqReif(pDom0.get(), pDom1.get(), pDom2.get()));
          return propagator->getFitness();
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_LT_REIF:
        {
          std::unique_ptr<Core::PropagatorBoolLtReif> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolLtReif(pDom0.get(), pDom1.get(), pDom2.get()));
          return propagator->getFitness();
        }
        default:
          return -1;
      }
    }//getFitness
    
    void postPropagator(bool aD0LB, bool aD0UB,
                        bool aD1LB, bool aD1UB,
                        bool aD2LB, bool aD2UB,
                        Core::PropagationEvent aEvent)
    {
      Core::PropagationEvent event = Core::PropagationEvent::PROP_EVENT_FAIL;
      switch (pPropagatorSemanticType) {
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_AND_TER:
        {
          std::unique_ptr<Core::PropagatorBoolAndTer> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolAndTer(pDom0.get(), pDom1.get(), pDom2.get()));
          event = propagator->post();
          break;
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_OR_TER:
        {
          std::unique_ptr<Core::PropagatorBoolOrTer> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolOrTer(pDom0.get(), pDom1.get(), pDom2.get()));
          event = propagator->post();
          break;
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_XOR_TER:
        {
          std::unique_ptr<Core::PropagatorBoolXorTer> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolXorTer(pDom0.get(), pDom1.get(), pDom2.get()));
          event = propagator->post();
          break;
        }
        default:
          break;
      }
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        ASSERT_TRUE(pDom0->lowerBound()->isEqual(getElem(aD0LB ? 1 : 0).get()));
        ASSERT_TRUE(pDom0->upperBound()->isEqual(getElem(aD0UB ? 1 : 0).get()));
        
        ASSERT_TRUE(pDom1->lowerBound()->isEqual(getElem(aD1LB ? 1 : 0).get()));
        ASSERT_TRUE(pDom1->upperBound()->isEqual(getElem(aD1UB ? 1 : 0).get()));
        
        ASSERT_TRUE(pDom2->lowerBound()->isEqual(getElem(aD2LB ? 1 : 0).get()));
        ASSERT_TRUE(pDom2->upperBound()->isEqual(getElem(aD2UB ? 1 : 0).get()));
      }
    }//postPropagator
    
    void runPropagationAndCheck(bool aD0LB, bool aD0UB,
                                bool aD1LB, bool aD1UB,
                                bool aD2LB, bool aD2UB,
                                Core::PropagationEvent aEvent)
    {
      // Perform propagation
      Core::PropagationEvent event = Core::PropagationEvent::PROP_EVENT_FAIL;
      
      switch (pPropagatorSemanticType) {
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_AND_TER:
        {
          std::unique_ptr<Core::PropagatorBoolAndTer> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolAndTer(pDom0.get(), pDom1.get(), pDom2.get()));
          event = propagator->propagate();
          break;
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_OR_TER:
        {
          std::unique_ptr<Core::PropagatorBoolOrTer> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolOrTer(pDom0.get(), pDom1.get(), pDom2.get()));
          event = propagator->propagate();
          break;
        }
        case PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_XOR_TER:
        {
          std::unique_ptr<Core::PropagatorBoolXorTer> propagator(nullptr);
          propagator.reset(new Core::PropagatorBoolXorTer(pDom0.get(), pDom1.get(), pDom2.get()));
          event = propagator->propagate();
          break;
        }
        default:
          break;
      }
      ASSERT_EQ(event, aEvent);
      
      if(event != Core::PropagationEvent::PROP_EVENT_FAIL)
      {
        ASSERT_TRUE(pDom0->lowerBound()->isEqual(getElem(aD0LB ? 1 : 0).get()));
        ASSERT_TRUE(pDom0->upperBound()->isEqual(getElem(aD0UB ? 1 : 0).get()));
        
        ASSERT_TRUE(pDom1->lowerBound()->isEqual(getElem(aD1LB ? 1 : 0).get()));
        ASSERT_TRUE(pDom1->upperBound()->isEqual(getElem(aD1UB ? 1 : 0).get()));
        
        ASSERT_TRUE(pDom2->lowerBound()->isEqual(getElem(aD2LB ? 1 : 0).get()));
        ASSERT_TRUE(pDom2->upperBound()->isEqual(getElem(aD2UB ? 1 : 0).get()));
      }
    }//runPropagationAndCheck

  protected:
    // Propagator type
    PropagatorSemanticType pPropagatorSemanticType;
    
    // Domain instances
    DomainBoolPtr pDom0;
    DomainBoolPtr pDom1;
    DomainBoolPtr pDom2;
  };
  
}// end namespace core

using namespace Core;

class PropagatorTest : public ::testing::Test {
public:
  PropagatorTest()
  : ptcTest1(0,
              PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
              PropagationPriority::PROP_PRIORITY_UNARY,
              PropagatorSemanticType::PROP_SEMANTIC_TYPE_UNDEF,
              PropagatorSemanticClass::PROP_SEMANTIC_CLASS_UNDEF)
  , ptcTest2(2,
             PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
             PropagationPriority::PROP_PRIORITY_BINARY,
             PropagatorSemanticType::PROP_SEMANTIC_TYPE_TIMES,
             PropagatorSemanticClass::PROP_SEMANTIC_CLASS_ARITHMETIC)
  {
  }
  
  ~PropagatorTest()
  {
  }
  
  virtual void SetUp()
  {
  }
  
  virtual void TearDown()
  {
  }
  
protected:
  // Propagator test instances
  PropagatorTestClass ptcTest1;
  PropagatorTestClass ptcTest2;
};
