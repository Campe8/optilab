
#include "utest_globals.hpp"

#include "Constraint.hpp"
#include "ConstraintEq.hpp"
#include "VariableInteger.hpp"
#include "VariableSemanticDecision.hpp"
#include "PropagatorTest.hpp"
#include "PropagatorLq.hpp"
#include "ConstraintTestInc.hpp"
#include "DomainTestInc.hpp"
#include "VariableFactory.hpp"
#include "DomainFactory.hpp"
#include "VariableArray.hpp"
#include "ConstraintParameterFactory.hpp"
#include "ConstraintParameterDomainArray.hpp"

OPTILAB_TEST(ConstraintParameterTest, ConstraintParameterVariable)
{
  using namespace Core;
  
  VariableFactory varFactory;
  ConstraintParameterFactory cpf;
  
  
  VariableSPtr varInt(varFactory.variableInteger(-10, +10, VariableSemanticUPtr(new VariableSemanticDecision())));
  ConstraintParameterUPtr parameterVar = cpf.constraintParameterVariable(varInt);
  
  ASSERT_TRUE(parameterVar->isSubject());
  ASSERT_EQ(parameterVar->getParameterClass(), ConstraintParameter::ConstraintParameterClass::CON_PARAM_CLASS_VARIABLE);
  
  ASSERT_TRUE(ConstraintParameterVariable::isa(parameterVar.get()));
  ASSERT_EQ(ConstraintParameterVariable::cast(parameterVar.get())->getVariable(), varInt);
}//ConstraintParameterVariable

OPTILAB_TEST(ConstraintParameterTest, ConstraintParameterVariableArray)
{
  using namespace Core;
  
  VariableFactory varFactory;
  ConstraintParameterFactory cpf;
  
  
  VariableSPtr varInt1(varFactory.variableInteger(-10, +10, VariableSemanticUPtr(new VariableSemanticDecision())));
  VariableSPtr varInt2(varFactory.variableInteger(+10, +20, VariableSemanticUPtr(new VariableSemanticDecision())));
  VariableSPtr varInt3(varFactory.variableInteger(+20, +30, VariableSemanticUPtr(new VariableSemanticDecision())));
  ConstraintParameterUPtr parameterVar = cpf.constraintParameterVariable({varInt1, varInt2, varInt3});
  
  ASSERT_TRUE(parameterVar->isSubject());
  ASSERT_EQ(parameterVar->getParameterClass(), ConstraintParameter::ConstraintParameterClass::CON_PARAM_CLASS_VARIABLE);
  
  ASSERT_TRUE(ConstraintParameterVariable::isa(parameterVar.get()));
  ASSERT_TRUE(VariableArray::isa((ConstraintParameterVariable::cast(parameterVar.get())->getVariable()).get()));
  
  VariableArray* varArray = VariableArray::cast((ConstraintParameterVariable::cast(parameterVar.get())->getVariable()).get());
  ASSERT_EQ(varArray->size(), 3);
  ASSERT_EQ(varArray->variableSemantic()->getSemanticClassType(), VariableSemanticClassType::VAR_SEMANTIC_CLASS_SUPPORT);
  ASSERT_EQ(varArray->operator[](0), varInt1);
  ASSERT_EQ(varArray->operator[](1), varInt2);
  ASSERT_EQ(varArray->operator[](2), varInt3);
}//ConstraintParameterVariableArray

OPTILAB_TEST(ConstraintParameterTest, ConstraintParameterDomain)
{
  using namespace Core;
  
  DomainFactory domFactory;
  ConstraintParameterFactory cpf;
  
  
  DomainSPtr domPtr(domFactory.domainBoolean(true));
  ConstraintParameterUPtr parameterDom = cpf.constraintParameterDomain(domPtr);
  
  ASSERT_FALSE(parameterDom->isSubject());
  ASSERT_EQ(parameterDom->getParameterClass(), ConstraintParameter::ConstraintParameterClass::CON_PARAM_CLASS_DOMAIN);
  
  ASSERT_TRUE(ConstraintParameterDomain::isa(parameterDom.get()));
  ASSERT_EQ(ConstraintParameterDomain::cast(parameterDom.get())->getDomain(), domPtr);
}//ConstraintParameterDomain

OPTILAB_TEST(ConstraintParameterTest, ConstraintParameterDomainArray)
{
  using namespace Core;
  
  DomainFactory domFactory;
  ConstraintParameterFactory cpf;
  
  
  DomainSPtr domPtr1(domFactory.domainBoolean(true));
  DomainSPtr domPtr2(domFactory.domainBoolean(true));
  DomainSPtr domPtr3(domFactory.domainBoolean(true));
  ConstraintParameterUPtr parameterDom = cpf.constraintParameterDomain({domPtr1, domPtr2, domPtr3});
  
  ASSERT_FALSE(parameterDom->isSubject());
  ASSERT_EQ(parameterDom->getParameterClass(), ConstraintParameter::ConstraintParameterClass::CON_PARAM_CLASS_DOMAIN_ARRAY);
  
  ASSERT_TRUE(ConstraintParameterDomainArray::isa(parameterDom.get()));
  
  auto domArray = ConstraintParameterDomainArray::cast(parameterDom.get())->getDomainArray();
  ASSERT_EQ(domArray->size(), 3);
  ASSERT_EQ(domArray->operator[](0), domPtr1);
  ASSERT_EQ(domArray->operator[](1), domPtr2);
  ASSERT_EQ(domArray->operator[](2), domPtr3);
}//ConstraintParameterDomain

OPTILAB_TEST(ConstraintParameterTest, ConstraintParameterDomainElementArray)
{
  using namespace Core;

  ConstraintParameterFactory cpf;

  ConstraintParameterUPtr parameterDom = cpf.constraintParameterDomain({
    DomainElementManager::getInstance().createDomainElementInt64(10),
    DomainElementManager::getInstance().createDomainElementInt64(11),
    DomainElementManager::getInstance().createDomainElementInt64(12)}, DomainClass::DOM_INT64);
  
  ASSERT_FALSE(parameterDom->isSubject());
  ASSERT_EQ(parameterDom->getParameterClass(), ConstraintParameter::ConstraintParameterClass::CON_PARAM_CLASS_DOMAIN_ARRAY);
  
  ASSERT_TRUE(ConstraintParameterDomainArray::isa(parameterDom.get()));
  
  auto domArray = ConstraintParameterDomainArray::cast(parameterDom.get())->getDomainArray();
  ASSERT_EQ(domArray->size(), 3);
  ASSERT_EQ(domArray->operator[](0)->getSize(), 1);
  ASSERT_EQ(domArray->operator[](1)->getSize(), 1);
  ASSERT_EQ(domArray->operator[](2)->getSize(), 1);
  
  ASSERT_TRUE(domArray->operator[](0)->lowerBound()->isEqual(DomainElementManager::getInstance().createDomainElementInt64(10)));
  ASSERT_TRUE(domArray->operator[](1)->lowerBound()->isEqual(DomainElementManager::getInstance().createDomainElementInt64(11)));
  ASSERT_TRUE(domArray->operator[](2)->lowerBound()->isEqual(DomainElementManager::getInstance().createDomainElementInt64(12)));
}//ConstraintParameterDomainElementArray
