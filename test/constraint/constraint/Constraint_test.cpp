
#include "utest_globals.hpp"

#include "Constraint.hpp"
#include "ConstraintEq.hpp"
#include "VariableInteger.hpp"
#include "VariableSemanticDecision.hpp"
#include "PropagatorTest.hpp"
#include "PropagatorLq.hpp"
#include "ConstraintTestInc.hpp"
#include "DomainTestInc.hpp"
#include "VariableFactory.hpp"
#include "ConstraintFactory.hpp"
#include "ConstraintParameterFactory.hpp"

OPTILAB_TEST(ConstraintTest, ConstraintDefault)
{
  ConstraintTest constraint;
  ASSERT_EQ(constraint.scopeSize(), 0);
  ASSERT_EQ(constraint.getConstraintId(), ConstraintId::CON_ID_UNDEF);
  ASSERT_EQ(constraint.getStrategyType(), PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND);
  ASSERT_TRUE(constraint.getCost() == PropagationCost(PropagationPriority::PROP_PRIORITY_UNDEF));
  ASSERT_EQ(constraint.post(), PropagationEvent::PROP_EVENT_SUBSUMED);
  ASSERT_EQ(constraint.propagate(), PropagationEvent::PROP_EVENT_SUBSUMED);
  ASSERT_EQ(constraint.numConstraintParameters(), 0);
  ASSERT_EQ(constraint.numRegisteredPropagators(PropagatorSemanticType::PROP_SEMANTIC_TYPE_ABS), 0);
  ASSERT_TRUE(constraint.isPropagatorDisabled(PropagatorSemanticType::PROP_SEMANTIC_TYPE_ABS));
  ASSERT_TRUE(constraint.isPropagatorDisabled(PropagatorSemanticType::PROP_SEMANTIC_TYPE_ABS, 1));
}//ConstraintDefault

OPTILAB_TEST(ConstraintTest, Constraint)
{
  using namespace Core;
  std::string varName{"VarTest"};
  VariableSemanticUPtr semanticDecision(new VariableSemanticDecision(varName));
  
  auto lowerBound = DomainElementManager::getInstance().createDomainElementInt64(-10);
  auto upperBound = DomainElementManager::getInstance().createDomainElementInt64(+10);
  VariableFactoryUPtr varFactory(new VariableFactory());
  VariableSPtr varInt(varFactory->variableInteger(-10, +10, std::move(semanticDecision)));
  
  // Create new constraint parameter with variable
  ConstraintParameterFactory cpf;
  ConstraintParameterUPtr parameterVar1 = cpf.constraintParameterVariable(varInt);
  ConstraintParameterUPtr parameterVar2 = cpf.constraintParameterVariable(varInt);
  
  ConstraintTest constraint;
  
  // By default a constraint shouldn't be an optimization constraint
  EXPECT_FALSE(constraint.isOptimizationConstraint());
  
  // Set the constraint as an optimization constraint
  constraint.setOptimizationConstraint();
  EXPECT_TRUE(constraint.isOptimizationConstraint());
  
  constraint.registerConstraintParameter(std::move(parameterVar1));
  ASSERT_EQ(constraint.numConstraintParameters(), 1);
  
  // There are two constraint parameters even if they are actually the same variable
  constraint.registerConstraintParameter(std::move(parameterVar2));
  ASSERT_EQ(constraint.numConstraintParameters(), 2);
  
  // Scope size is 1 since there is only 1 variable registered
  ASSERT_EQ(constraint.scopeSize(), 1);
  
  ASSERT_TRUE(constraint.getParameterDomain(0)->lowerBound()->isEqual(lowerBound));
  ASSERT_TRUE(constraint.getParameterDomain(0)->upperBound()->isEqual(upperBound));
  ASSERT_TRUE(constraint.getParameterDomain(1)->lowerBound()->isEqual(lowerBound));
  ASSERT_TRUE(constraint.getParameterDomain(1)->upperBound()->isEqual(upperBound));
  
  // Register 0 <= 1
  DomainSPtr domain1 = std::make_shared<DomainTest>();
  DomainSPtr domain2 = std::make_shared<DomainTest>();
  std::shared_ptr<Core::PropagatorLq> propagator = std::make_shared<PropagatorLq>(domain1.get(), domain2.get());
  constraint.registerPropagator(propagator);
  
  ASSERT_EQ(constraint.numRegisteredPropagators(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ), 1);
  ASSERT_EQ(constraint.getRegisteredPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ), propagator);
  ASSERT_FALSE(constraint.isPropagatorDisabled(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ));
  
  constraint.disablePropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ);
  ASSERT_TRUE(constraint.isPropagatorDisabled(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ));
  
  constraint.enablePropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ);
  ASSERT_FALSE(constraint.isPropagatorDisabled(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ));
  
  // Reset constraint
  constraint.disablePropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ);
  
  // Disable propagator: running propagate should return subsumed since there is no propagator running
  ASSERT_EQ(constraint.propagate(), PropagationEvent::PROP_EVENT_SUBSUMED);
  
  // Reset state, enable all propagators
  constraint.resetState();
  ASSERT_FALSE(constraint.isPropagatorDisabled(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ));
  ASSERT_EQ(constraint.propagate(), PropagationEvent::PROP_EVENT_FIXPOINT);
}//Constraint

OPTILAB_TEST(ConstraintTest, ConstraintCost)
{
  using namespace Core;
  std::string varName{"VarTest"};
  VariableSemanticUPtr semanticDecision1(new VariableSemanticDecision(varName));
  VariableSemanticUPtr semanticDecision2(new VariableSemanticDecision(varName));
  
  
  VariableFactoryUPtr varFactory(new VariableFactory());
  VariableSPtr varInt1(varFactory->variableInteger(-10, +10, std::move(semanticDecision1)));
  VariableSPtr varInt2(varFactory->variableInteger(-10, +10, std::move(semanticDecision2)));
  
  // Create new constraint parameter with variable
  ConstraintParameterFactory cpf;
  
  PropagationCost binaryCost(PropagationPriority::PROP_PRIORITY_BINARY);
  
  ConstraintFactory cf;
  
  auto ceq = cf.constraint(ConstraintId::CON_ID_EQ,
  {cpf.constraintParameterVariable(varInt1), cpf.constraintParameterVariable(varInt2)});
  std::unique_ptr<ConstraintEq> eq1(static_cast<ConstraintEq*>(ceq));
  
  ASSERT_TRUE(eq1->getCost() == binaryCost);
  
  // Linear cost for DOMAIN type propagation
  auto ceq2 = cf.constraint(ConstraintId::CON_ID_EQ,
  {cpf.constraintParameterVariable(varInt1), cpf.constraintParameterVariable(varInt2)}, PROPAGATOR_STRATEGY_TYPE_DOMAIN);
  std::unique_ptr<ConstraintEq> eq2(static_cast<ConstraintEq*>(ceq2));
  PropagationCost linearCost(PropagationPriority::PROP_PRIORITY_LINEAR);
  ASSERT_TRUE(eq2->getCost() == linearCost);
  
  // Test reduced cost
  auto ceq3 = cf.constraint(ConstraintId::CON_ID_EQ,
  { cpf.constraintParameterVariable(varInt1),
                             cpf.constraintParameterDomain(std::make_shared<DomainTest>()) });
  
  PropagationCost unaryCost(PropagationPriority::PROP_PRIORITY_UNARY);
  std::unique_ptr<ConstraintEq> eq3(static_cast<ConstraintEq*>(ceq3));
  ASSERT_TRUE(eq3->getCost() == unaryCost);
}//ConstraintCost
