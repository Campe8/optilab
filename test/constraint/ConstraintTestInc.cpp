#include "ConstraintTestInc.hpp"

DomainBoolPtr getDomainBool()
{
  return std::make_shared<Core::DomainBoolean>();
}//getDomainBool

DomainIntPtr getDomainInt(INT_64 aLower, INT_64 aUpper)
{
  return
  std::make_shared<Core::DomainInteger>(Core::DomainElementManager::getInstance().createDomainElementInt64(aLower),
                                        Core::DomainElementManager::getInstance().createDomainElementInt64(aUpper));
}//getDomainInt

DomainIntPtr getDomainInt(const std::vector<INT_64>& aElems)
{
  std::unordered_set<Core::DomainElementInt64 *> set;
  for(auto elem : aElems)
  {
    set.insert(Core::DomainElementManager::getInstance().createDomainElementInt64(elem));
  }
  
  return std::make_shared<Core::DomainInteger>(set);
}//getDomain

ElemPtr getElem(INT_64 aVal)
{
  return std::make_shared<Core::DomainElementInt64>(aVal);
}//getElem

ArrayPtr getArray(const std::vector<INT_64>& aArray)
{
  ArrayPtr arrayPtr = std::make_shared<Core::DomainElementArray>(aArray.size());
  
  std::size_t idx{0};
  for(auto& elem : aArray)
  {
    arrayPtr->assignElementToCell(idx++, Core::DomainElementManager::getInstance().createDomainElementInt64(elem));
  }
  return arrayPtr;
}//getArray

ArrayPtr getArray(INT_64 aMin, INT_64 aMax)
{
  std::vector<INT_64> array;
  for(; aMin <= aMax; ++aMin)
  {
    array.push_back(aMin);
  }
  
  return getArray(array);
}//getArray

DomainArrayPtr getDomainArray(const std::vector<std::shared_ptr<Core::Domain>>& aDomains)
{
  auto domainArray = std::make_shared<Core::DomainArray>(aDomains.size());
  
  for(std::size_t idx{0}; idx < aDomains.size(); ++idx)
  {
    domainArray->assignElementToCell(idx, aDomains[idx].get());
  }
  
  return domainArray;
}//getDomainArray

