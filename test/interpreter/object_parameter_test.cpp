
#include "utest_globals.hpp"

#include "BaseObject.hpp"
#include "ObjectTools.hpp"
#include "ObjectHelper.hpp"
#include "OPCode.hpp"

OPTILAB_TEST(ObjectParameter, ListObjectBase)
{
  // Test base methods for checking parameters
  using namespace Interpreter;
  using namespace Interpreter::ObjectTools;
  std::unique_ptr<BaseObject> matrix(createMatrixObject({1}));
  ASSERT_TRUE(isParameterObject(matrix.get()));
}//ListObjectBase

OPTILAB_TEST(ObjectParameter, Dimensions)
{
  // Test dimensions query
  using namespace Interpreter;
  using namespace Interpreter::ObjectTools;
  
  // A vector should return one dimension corresponding to the number of elements in the vector
  std::unique_ptr<BaseObject> vector(createMatrixObject({ DataObject(1), DataObject(2) }));
  
  // Assert that dims property of an object parameter is a list object
  ASSERT_TRUE(vector->lookupProperty("dims").isClassObject());
  ASSERT_TRUE(isListObject(vector->lookupProperty("dims").getClassObject().get()));
  ASSERT_EQ(getParameterDimensionsList(vector.get()),
            vector->lookupProperty("dims").getClassObject().get());
  
  // Check the size of the vector
  ASSERT_EQ(2, getParameterDimensions(vector.get()).size());
  ASSERT_EQ(1, getParameterDimensions(vector.get())[0]);
  ASSERT_EQ(2, getParameterDimensions(vector.get())[1]);
  
  // Create a 3 x 1 x 2 matrix
  std::shared_ptr<BaseObject> row1(createMatrixObject( { DataObject(1), DataObject(2) } ));
  std::shared_ptr<BaseObject> row2(createMatrixObject( { DataObject(1), DataObject(2) } ));
  std::shared_ptr<BaseObject> row3(createMatrixObject( { DataObject(1), DataObject(2) } ));
  DataObject doRow1;
  DataObject doRow2;
  DataObject doRow3;
  doRow1.setClassObject(row1);
  doRow2.setClassObject(row2);
  doRow3.setClassObject(row3);
  std::unique_ptr<BaseObject> matrix(createMatrixObject( {doRow1, doRow2, doRow3} ));
  
  auto dimsMatrix = getParameterDimensions(matrix.get());
  ASSERT_EQ(3, dimsMatrix.size());
  ASSERT_EQ(3, dimsMatrix[0]);
  ASSERT_EQ(1, dimsMatrix[1]);
  ASSERT_EQ(2, dimsMatrix[2]);
}//Dimensions

OPTILAB_TEST(ObjectParameter, concatParameters)
{
  // Test concatenation of parameters
  using namespace Interpreter;
  using namespace Interpreter::ObjectTools;
  
  // List concatenation
  
  // Test that creating a parameter with empty domain returns a data object
  // with composite size of zero
  std::unique_ptr<BaseObject> par1(createListObject({}));
  std::unique_ptr<BaseObject> par2(createListObject({}));
  std::unique_ptr<BaseObject> parConcat12(concatParameters(par1.get(), par2.get()));
  
  // Empty list concat into another empty list
  EXPECT_EQ(1, getParameterDimensions(parConcat12.get()).size());
  
  std::unique_ptr<BaseObject> par3(createListObject({ DataObject(1) }));
  std::unique_ptr<BaseObject> parConcat13(concatParameters(par1.get(), par3.get()));
  std::unique_ptr<BaseObject> parConcat31(concatParameters(par3.get(), par1.get()));
  
  // Empty list concat to scalar and vice-versa cretes  list of size 1
  ASSERT_TRUE(isListObject(parConcat13.get()));
  ASSERT_TRUE(isListObject(parConcat31.get()));
  EXPECT_EQ(1, getParameterDimensions(parConcat13.get()).size());
  EXPECT_EQ(1, getParameterDimensions(parConcat31.get()).size());
  
  std::unique_ptr<BaseObject> par4(createListObject({ DataObject(2) }));
  std::unique_ptr<BaseObject> parConcat34(concatParameters(par3.get(), par4.get()));
  
  // Vectors concatenation
  EXPECT_EQ(1, getParameterDimensions(parConcat34.get()).size());
  EXPECT_EQ(2, getParameterDimensions(parConcat34.get())[0]);
  
  // Matrix concatenation
  
  // Create matrix: vector of matrix objects
  Interpreter::DataObject matA1;
  Interpreter::DataObject matB1;
  
  Interpreter::DataObject a1_d1;
  Interpreter::DataObject a1_d2;
  a1_d1.compose({Interpreter::DataObject(0), Interpreter::DataObject(1)});
  a1_d2.compose({Interpreter::DataObject(2), Interpreter::DataObject(3)});
  
  Interpreter::DataObject b1_d1;
  Interpreter::DataObject b1_d2;
  Interpreter::DataObject b1_d3;
  b1_d1.compose({Interpreter::DataObject(4), Interpreter::DataObject(5)});
  b1_d2.compose({Interpreter::DataObject(6), Interpreter::DataObject(7)});
  b1_d3.compose({Interpreter::DataObject(8), Interpreter::DataObject(9)});
  
  // Create
  // matA1 = [0, 1;
  //          2, 3]
  auto matA1Obj =
  std::shared_ptr<Interpreter::BaseObject>(createMatrixObject({a1_d1, a1_d2}));
  matA1.setClassObject(matA1Obj);
  
  // Create
  // matB1 = [4, 5;
  //          6, 7;
  //          8, 9]
  auto matB1Obj =
  std::shared_ptr<Interpreter::BaseObject>(createMatrixObject({b1_d1, b1_d2, b1_d3}));
  matB1.setClassObject(matB1Obj);
  
  // Create the composed matrix
  // [matA1, matB1] = [ 0, 1;
  //                    2, 3;
  //                    4, 5;
  //                    6, 7;
  //                    8, 9]
  // It should create a [5 x 2] matrix
  auto matrix =
  std::unique_ptr<Interpreter::BaseObject>((concatParameters(matA1.getClassObject().get(),
                                                             matB1.getClassObject().get())));
  // It should be a 5 x 2 matrix
  auto concatDims = getParameterDimensions(matrix.get());
  ASSERT_EQ(2, concatDims.size());
  EXPECT_EQ(5, concatDims[0]);
  EXPECT_EQ(2, concatDims[1]);
  
  // Check that indexing is correct: get element matrix(3, 0) = 6
  Interpreter::DataObject indexing;
  indexing.compose({Interpreter::DataObject(3), Interpreter::DataObject(0)});
  const auto& element = getMatrixElement(matrix.get(), indexing);
  
  ASSERT_TRUE(isPrimitiveType(element));
  EXPECT_EQ(6, element.getDataValue<int>());
}//concatParameters

OPTILAB_TEST(ObjectParameter, ParameterLookup)
{
  // Test subscript function on a parameter
  using namespace Interpreter;
  using namespace Interpreter::ObjectTools;
  
  BaseObjectSPtr param(createMatrixObject({ DataObject(2), DataObject(3) }));
  
  DataObject dobj;
  dobj.compose({ DataObject(1) });
  auto element = getParameterElement(param.get(), dobj);
  ASSERT_EQ(DataObject::DataObjectType::DOT_INT, element.dataObjectType());
  ASSERT_EQ(3, element.getDataValue<int>());
  
  
  BaseObjectSPtr vec1(createMatrixObject({ DataObject(1), DataObject(2) }));
  BaseObjectSPtr vec2(createMatrixObject({ DataObject(3), DataObject(4) }));
  
  DataObject doVec1, doVec2;
  doVec1.setClassObject(vec1);
  doVec2.setClassObject(vec2);
  
  // Create a 2 x 1 x 2 matrix
  BaseObjectSPtr param2(createMatrixObject({ doVec1, doVec2 }));
  
  // Create index [1, 0, 0]
  DataObject dobjIdx;
  dobjIdx.compose({ DataObject(1), DataObject(0), DataObject(0) });
  auto elementMatrix = getParameterElement(param2.get(), dobjIdx);
  ASSERT_EQ(DataObject::DataObjectType::DOT_INT, elementMatrix.dataObjectType());
  ASSERT_EQ(3, elementMatrix.getDataValue<int>());
}//ParameterLookup

OPTILAB_TEST(ObjectParameter, ParameterScalar)
{
  // Test subscript function on a parameter
  using namespace Interpreter;
  using namespace Interpreter::ObjectTools;
  
  BaseObjectSPtr scalar(createScalarObject(1));
  ASSERT_TRUE(isParameterObject(scalar.get()));
  ASSERT_TRUE(isScalarObject(scalar.get()));
  
  auto& val1 = getScalarParameterValue(scalar.get());
  ASSERT_TRUE(isPrimitiveType(val1));
  ASSERT_TRUE(DataObject::DataObjectType::DOT_INT == val1.dataObjectType());
  ASSERT_EQ(1, val1.getDataValue<int>());
  
  // Change the value of the scalar object
  std::string str{"string"};
  val1 = DataObject(str.c_str());
  
  // Getting again the scalar should reflect the new scalar value
  auto& val2 = getScalarParameterValue(scalar.get());
  ASSERT_TRUE(isPrimitiveType(val1));
  ASSERT_EQ(DataObject::DataObjectType::DOT_STRING, val2.dataObjectType());
  ASSERT_TRUE(val2.getDataValue<std::string>() == str);
}//ParameterScalar
