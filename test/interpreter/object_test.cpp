
#include "utest_globals.hpp"

#include "BaseObject.hpp"
#include "ObjectTools.hpp"
#include "OPCode.hpp"

namespace Interpreter {
  class BaseObjectTest : public BaseObject {
  public:
    BaseObject* clone()
    {
      return BaseObject::clone();
    }
  };
  
  DataObject setPropFcnTest(BaseObject* aObj, const std::vector<DataObject>& aArray)
  {
    assert(aArray.size() == 1);
    auto val = aArray[0].getDataValue<int>();
    assert(aObj->hasProperty("prop"));
    aObj->lookupProperty("prop") = DataObject(val + 1);
    return DataObject();
  }//setPropFcnTest
  
  DataObject getPropFcnTest(BaseObject* aObj, const std::vector<DataObject>& aArray)
  {
    assert(aObj->hasProperty("prop"));
    return (aObj->lookupProperty("prop")).getDataValue<int>() +1;
  }//setPropFcnTest
  
}// end namespace Interpreter

OPTILAB_TEST(Object, BaseObject)
{
  // Test base methods and default properties
  using namespace Interpreter;
  
  std::string prop{"prop"};
  
  BaseObjectTest bot;
  ASSERT_FALSE(bot.hasProperty(prop));
  ASSERT_FALSE(bot.isPropertyVisibile(prop));
  ASSERT_TRUE(bot.toString().empty());
  
  // Check that bot2 is an instance of bot object
  BaseObjectTest bot2;
  ASSERT_TRUE(bot2.isInstanceOf(&bot));
  
  // Add empty property
  bot.addProperty(prop);
  ASSERT_TRUE(bot.hasProperty(prop));
  
  // Check that bot2 is not an instance of bot anymore (missing the property)
  ASSERT_FALSE(bot2.isInstanceOf(&bot));
  
  // Add the property to bot2 and check that it is an instance of bot
  bot2.addProperty(prop);
  ASSERT_TRUE(bot2.isInstanceOf(&bot));
  
  // Check visibility
  ASSERT_TRUE(bot.isPropertyVisibile(prop));
  
  bot.setPropertyVisibility(prop, false);
  ASSERT_FALSE(bot.isPropertyVisibile(prop));
  
  bot.setPropertyVisibility(prop, true);
  ASSERT_TRUE(bot.isPropertyVisibile(prop));
  
  // Get property
  auto dobj = bot.getProperty(prop);
  ASSERT_EQ(DataObject::DataObjectType::DOT_VOID, dobj.dataObjectType());
  
  // Set non empty property
  std::string prop2{ "prop2" };
  bot.addProperty(prop2, DataObject(5));
  
  // Get property and check its value
  auto dobj2_1 = bot.getProperty(prop2);
  ASSERT_EQ(DataObject::DataObjectType::DOT_INT, dobj2_1.dataObjectType());
  ASSERT_EQ(5, dobj2_1.getDataValue<int>());
  
  // Lookup property and check its value
  auto dobj2_2 = bot.lookupProperty(prop2);
  ASSERT_EQ(DataObject::DataObjectType::DOT_INT, dobj2_2.dataObjectType());
  ASSERT_EQ(5, dobj2_2.getDataValue<int>());
  
  // Lookup returns a reference to the value
  bot.lookupProperty(prop2) = DataObject(6);
  auto dobj2_3 = bot.getProperty(prop2);
  ASSERT_EQ(DataObject::DataObjectType::DOT_INT, dobj2_3.dataObjectType());
  ASSERT_EQ(6, dobj2_3.getDataValue<int>());
  
  // Remove property
  std::string prop3{ "prop3" };
  bot.addProperty(prop3, DataObject(5));
  bot.removeProperty(prop3);
  ASSERT_FALSE(bot.hasProperty(prop3));
}//BaseObject

OPTILAB_TEST(Object, Clone)
{
  // Test clone method
  using namespace Interpreter;
  
  // Create a base object
  BaseObjectTest bot;
  std::string prop{ "prop" };
  bot.addProperty(prop, DataObject(5));
  
  // Clone from the first object
  std::unique_ptr<BaseObject> botCpy(bot.clone());
  ASSERT_TRUE(botCpy);
  ASSERT_TRUE(botCpy->hasProperty(prop));
  
  auto dobjCpy = botCpy->getProperty(prop);
  ASSERT_EQ(DataObject::DataObjectType::DOT_INT, dobjCpy.dataObjectType());
  
  // Check that the clone does not share any ancestor
  std::string prop3{ "another_prop" };
  bot.addProperty(prop3);
  ASSERT_FALSE(botCpy->hasProperty(prop3));
}//Clone

OPTILAB_TEST(Object, Derive)
{
  // Test derive method
  using namespace Interpreter;
  
  // Create a base object
  BaseObjectTest bot;
  
  // Set a non-empty property
  std::string prop{ "prop" };
  bot.addProperty(prop, DataObject(5));
  
  std::string propAux{ "prop2" };
  bot.addProperty(propAux, DataObject(10));
  
  // Clone the base object and use the clone copy
  std::unique_ptr<BaseObject> botCpy(bot.clone());
  
  // Create a third base object and add a property
  std::unique_ptr<BaseObject> newBase(ObjectTools::createObject("type"));
  ASSERT_EQ(1, (newBase->getKeys()).size());
  
  std::string prop2{ "another_prop" };
  newBase->addProperty(prop2);
  newBase->addProperty(prop, DataObject(6));
  ASSERT_EQ(3, (newBase->getKeys()).size());
  
  // Derive the object from the copy of the first object.
  // @note the child has the same property of the parent
  // but it is not counted twice.
  // Moreover, the value of the property should be the child's value
  newBase->deriveFrom(botCpy.get());
  ASSERT_EQ(4, (newBase->getKeys()).size());
  ASSERT_EQ(10, ObjectTools::getObjectPropertyValue<int>(newBase.get(), propAux));
  ASSERT_EQ(6, ObjectTools::getObjectPropertyValue<int>(newBase.get(), prop));
  
  // Check that newBase is an instance of the parent
  newBase->isInstanceOf(botCpy.get());
  
  // Check that the derived object has all its original properties
  ASSERT_TRUE(newBase->hasProperty(prop2));
  
  // Check that the derived object has the properties of the parent
  ASSERT_TRUE(newBase->hasProperty(prop));
  auto newProp = newBase->getProperty(prop);
  ASSERT_EQ(DataObject::DataObjectType::DOT_INT, newProp.dataObjectType());
  
  // Check that the derived object has a deep copy of the parent object
  // by modifying the parent and verifying that the derived object does not
  // have any modification
  std::string prop3{ "parent_prop" };
  botCpy->addProperty(prop3);
  ASSERT_FALSE(newBase->hasProperty(prop3));
}//Derive

OPTILAB_TEST(Object, GetVisibleKeys)
{
  // Test get visibile keys method
  using namespace Interpreter;
  
  BaseObjectTest bot;
  std::string prop1{ "prop1" };
  std::string prop2{ "prop2" };
  bot.addProperty(prop1);
  bot.addProperty(prop2);
  
  // Get properties
  auto props = bot.getKeys();
  ASSERT_EQ(2, props.size());
  ASSERT_TRUE(std::find(props.begin(), props.end(), prop1) != props.end());
  ASSERT_TRUE(std::find(props.begin(), props.end(), prop2) != props.end());
  
  bot.setPropertyVisibility(prop1, false);
  auto visibleProps = bot.getVisibleKeys();
  ASSERT_EQ(1, visibleProps.size());
  ASSERT_TRUE(std::find(visibleProps.begin(), visibleProps.end(), prop2) != visibleProps.end());
}//GetVisibleKeys

OPTILAB_TEST(Object, ConstantProperties)
{
  // Test constant properties (setConstProperty and isConstProperty)
  using namespace Interpreter;
  
  BaseObjectTest bot;
  std::string prop1{ "prop10" };
  std::string prop2{ "prop11" };
  bot.addProperty(prop1, 10);
  bot.addProperty(prop2, 11);
  ASSERT_FALSE(bot.isConstProperty(prop1));
  ASSERT_FALSE(bot.isConstProperty(prop2));
  
  bot.setConstProperty(prop1, true);
  bot.setConstProperty(prop2, false);
  ASSERT_TRUE(bot.isConstProperty(prop1));
  ASSERT_FALSE(bot.isConstProperty(prop2));
  
  bot.setConstProperty(prop1, false);
  bot.setConstProperty(prop2, true);
  ASSERT_FALSE(bot.isConstProperty(prop1));
  ASSERT_TRUE(bot.isConstProperty(prop2));
  
  std::unique_ptr<BaseObject> clone(bot.clone());
  ASSERT_TRUE(clone->hasProperty(prop1));
  ASSERT_TRUE(clone->hasProperty(prop2));
  ASSERT_FALSE(clone->isConstProperty(prop1));
  ASSERT_TRUE(clone->isConstProperty(prop2));
  
  BaseObjectTest bot2;
  bot2.deriveFrom(clone.get());
  ASSERT_TRUE(bot2.hasProperty(prop1));
  ASSERT_TRUE(bot2.hasProperty(prop2));
  ASSERT_FALSE(bot2.isConstProperty(prop1));
  ASSERT_TRUE(bot2.isConstProperty(prop2));
  
  // Set the values of the property, only prop1 (non-const)
  // should be modified
  bot2.setProperty(prop1, 20);
  bot2.setProperty(prop2, 30);
  ASSERT_EQ(20, bot2.getProperty(prop1).getDataValue<int>());
  ASSERT_EQ(11, bot2.getProperty(prop2).getDataValue<int>());
}//ConstantProperties

OPTILAB_TEST(Object, ConstantDerivedProperties)
{
  // Test constant properties from derived objects
  using namespace Interpreter;
  
  auto bot = std::make_shared<BaseObjectTest>();
  std::string prop10{ "prop10" };
  std::string prop11{ "prop11" };
  std::string prop12{ "prop12" };
  bot->addProperty(prop10, 10);
  bot->addProperty(prop11, 11);
  bot->addProperty(prop12, 12);
  
  // Set all properties as constant properties
  bot->setConstProperty(prop10, true);
  bot->setConstProperty(prop11, true);
  bot->setConstProperty(prop12, true);
  
  auto botDerived = std::make_shared<BaseObjectTest>();
  botDerived->addProperty(prop10, 100);
  botDerived->setConstProperty(prop10, true);
  
  botDerived->addProperty(prop12, 120);
  botDerived->setConstProperty(prop12, false);
  
  // Derive from bot
  botDerived->deriveFrom(bot.get());
  
  // prop10 should be set to 100 and it should be still const
  ASSERT_EQ(100, botDerived->getProperty(prop10).getDataValue<int>());
  ASSERT_TRUE(botDerived->isConstProperty(prop10));
  
  // prop11 should be const
  ASSERT_TRUE(botDerived->isConstProperty(prop11));
  
  // prop12 should be 120 and non const
  ASSERT_EQ(120, botDerived->getProperty(prop12).getDataValue<int>());
  ASSERT_FALSE(botDerived->isConstProperty(prop12));
}//ConstantDerivedProperties

OPTILAB_TEST(Object, SetPropertyFunction)
{
  // Test setProperty applying a function
  using namespace Interpreter;
  
  BaseObjectTest bot;
  std::string prop{ "prop" };
  
  // Add the property
  bot.addProperty(prop);
  
  // Add a function "set" for the property
  auto setFcn = ObjectTools::createObjectFcn(prop, setPropFcnTest, false);
  bot.attachFcnToSetProperty(prop, setFcn);

  // Set property should call the function set
  bot.setProperty(prop, 1);
  
  // Get the value using the property name
  auto val = bot.getProperty(prop);
  
  // The value should be the successor
  ASSERT_EQ(val.getDataValue<int>(), 2);
}//SetPropertyFunction

OPTILAB_TEST(Object, GetPropertyFunction)
{
  // Test getProperty applying a function
  using namespace Interpreter;
  
  BaseObjectTest bot;
  std::string prop{ "prop" };
  
  // Add the property
  bot.addProperty(prop);
  
  // Add a function "get" for the property
  auto getFcn = ObjectTools::createObjectFcn(prop, getPropFcnTest, false);
  bot.attachFcnToGetProperty(prop, getFcn);

  // Set property should call the function set
  bot.setProperty(prop, 1);
  
  // Get the value using the property name
  auto val = bot.getProperty(prop);
  
  // The value should be the successor
  ASSERT_EQ(val.getDataValue<int>(), 2);
}//SetPropertyFunction
