
#include "utest_globals.hpp"

#include "InterpUtils.hpp"
#include "ObjectTools.hpp"
#include "ObjectHelper.hpp"

namespace otools = Interpreter::ObjectTools;

OPTILAB_TEST(interpUtil, byteToIntToByte)
{
  // Test bytetoInt and intToLSByte functions
  using namespace Interpreter;
  
  for(int num{0}; num < 256; ++num)
  {
    byte_T bt = num & 0xFF;
    ASSERT_EQ(num, Utils::byteToInt(bt));
    ASSERT_EQ(bt, Utils::intToLSByte(num));
  }
}//byteToIntToByte

OPTILAB_TEST(interpUtil, intToByte)
{
  // Test intToByte function
  using namespace Interpreter;
  
  for(int num{0}; num < 1024; ++num)
  {
    auto btArray = Utils::intToByte(num);
    byte_T zero   = (num >> 0)  & 0xFF;
    byte_T one    = (num >> 8)  & 0xFF;
    byte_T two    = (num >> 16) & 0xFF;
    byte_T three  = (num >> 24) & 0xFF;
    ASSERT_EQ(zero,  btArray[0]);
    ASSERT_EQ(one,   btArray[1]);
    ASSERT_EQ(two,   btArray[2]);
    ASSERT_EQ(three, btArray[3]);
  }
}//byteToIntToByte

OPTILAB_TEST(interpUtil, opAddOnCompositeObjects)
{
  // Test add operator on composite objects
  Interpreter::DataObject do1;
  Interpreter::DataObject do2;
  do1.compose({Interpreter::DataObject(0), Interpreter::DataObject(1)});
  do2.compose({Interpreter::DataObject(2), Interpreter::DataObject(3)});
  auto add = Interpreter::Utils::opAddOnCompositeObjects(do1, do2);
  
  ASSERT_TRUE(Interpreter::isCompositeType(add));
  ASSERT_EQ(4, add.composeSize());
  for (int idx{0}; idx < 4; ++idx)
  {
    EXPECT_EQ(idx, add[idx].getDataValue<int>());
  }
}//opAddOnCompositeObjects

OPTILAB_TEST(interpUtil, opAddOnListObjectsWithList)
{
  // Test add operator on list plus list objects
  Interpreter::DataObject do1;
  Interpreter::DataObject do2;
  do1.setClassObject(otools::createListObjectSPtr({Interpreter::DataObject(0),
    Interpreter::DataObject(1)}));
  do2.setClassObject(otools::createListObjectSPtr({Interpreter::DataObject(2),
    Interpreter::DataObject(3)}));
  auto add = Interpreter::Utils::opAddOnListObjects(do1, do2);
  
  ASSERT_TRUE(Interpreter::isClassObjectType(add));
  ASSERT_TRUE(otools::isListObject(add.getClassObject().get()));
  ASSERT_EQ(4, otools::getListSize(add.getClassObject().get()));
  auto listVals = otools::getListValues(add.getClassObject().get());
  for (int idx{0}; idx < 4; ++idx)
  {
    EXPECT_EQ(idx, listVals[idx].getDataValue<int>());
  }
}//opAddOnListObjectsWithList

OPTILAB_TEST(interpUtil, opAddOnListObjectsWithPrimitive)
{
  // Test add operator on list plus primitive objects
  Interpreter::DataObject do1;
  do1.setClassObject(otools::createListObjectSPtr({Interpreter::DataObject(0),
    Interpreter::DataObject(1)}));
  auto add = Interpreter::Utils::opAddOnListObjects(do1, Interpreter::DataObject(2));
  
  ASSERT_TRUE(Interpreter::isClassObjectType(add));
  ASSERT_TRUE(otools::isListObject(add.getClassObject().get()));
  ASSERT_EQ(3, otools::getListSize(add.getClassObject().get()));
  auto listVals = otools::getListValues(add.getClassObject().get());
  for (int idx{0}; idx < 3; ++idx)
  {
    EXPECT_EQ(idx, listVals[idx].getDataValue<int>());
  }
}//opAddOnListObjectsWithPrimitive

OPTILAB_TEST(interpUtil, opAddOnListObjectsWithScalar)
{
  // Test add operator on list plus primitive objects
  Interpreter::DataObject do1;
  Interpreter::DataObject do2;
  do1.setClassObject(otools::createListObjectSPtr({Interpreter::DataObject(0),
    Interpreter::DataObject(1)}));
  do2.setClassObject(std::shared_ptr<Interpreter::BaseObject>
                     (otools::createScalarObject(Interpreter::DataObject(2))));
  auto add = Interpreter::Utils::opAddOnListObjects(do1, do2);
  
  ASSERT_TRUE(Interpreter::isClassObjectType(add));
  ASSERT_TRUE(otools::isListObject(add.getClassObject().get()));
  ASSERT_EQ(3, otools::getListSize(add.getClassObject().get()));
  auto listVals = otools::getListValues(add.getClassObject().get());
  for (int idx{0}; idx < 3; ++idx)
  {
    EXPECT_EQ(idx, listVals[idx].getDataValue<int>());
  }
}//opAddOnListObjectsWithScalar

OPTILAB_TEST(interpUtil, opAddOnListObjectsWithComposite)
{
  // Test add operator on list plus composite objects
  Interpreter::DataObject do1;
  Interpreter::DataObject do2;
  do1.setClassObject(otools::createListObjectSPtr({Interpreter::DataObject(0),
    Interpreter::DataObject(1)}));
  do2.compose({Interpreter::DataObject(2), Interpreter::DataObject(3)});
  auto add = Interpreter::Utils::opAddOnListObjects(do1, do2);
  
  ASSERT_TRUE(Interpreter::isClassObjectType(add));
  ASSERT_TRUE(otools::isListObject(add.getClassObject().get()));
  ASSERT_EQ(4, otools::getListSize(add.getClassObject().get()));
  auto listVals = otools::getListValues(add.getClassObject().get());
  for (int idx{0}; idx < 4; ++idx)
  {
    EXPECT_EQ(idx, listVals[idx].getDataValue<int>());
  }
}//opAddOnListObjectsWithComposite

OPTILAB_TEST(interpUtil, opAddOnParameterObjectsScalarAddScalar)
{
  // Test add operator on scalar plus scalar objects
  Interpreter::DataObject do1;
  Interpreter::DataObject do2;
  do1.setClassObject(std::shared_ptr<Interpreter::BaseObject>
                     (otools::createScalarObject(Interpreter::DataObject(0))));
  do2.setClassObject(std::shared_ptr<Interpreter::BaseObject>
                     (otools::createScalarObject(Interpreter::DataObject(1))));
  auto add = Interpreter::Utils::opAddOnParameterObjects(do1, do2);
  
  ASSERT_TRUE(Interpreter::isClassObjectType(add));
  ASSERT_TRUE(otools::isListObject(add.getClassObject().get()));
  ASSERT_EQ(2, otools::getListSize(add.getClassObject().get()));
  auto listVals = otools::getListValues(add.getClassObject().get());
  for (int idx{0}; idx < 2; ++idx)
  {
    EXPECT_EQ(idx, listVals[idx].getDataValue<int>());
  }
}//opAddOnParameterObjectsScalarAddScalar

OPTILAB_TEST(interpUtil, opAddOnParameterObjectsScalarAddList)
{
  // Test add operator on scalar plus list objects
  Interpreter::DataObject do1;
  Interpreter::DataObject do2;
  do1.setClassObject(std::shared_ptr<Interpreter::BaseObject>
                     (otools::createScalarObject(Interpreter::DataObject(0))));
  do2.setClassObject(otools::createListObjectSPtr({Interpreter::DataObject(1),
    Interpreter::DataObject(2)}));
  auto add = Interpreter::Utils::opAddOnParameterObjects(do1, do2);
  
  ASSERT_TRUE(Interpreter::isClassObjectType(add));
  ASSERT_TRUE(otools::isListObject(add.getClassObject().get()));
  ASSERT_EQ(3, otools::getListSize(add.getClassObject().get()));
  auto listVals = otools::getListValues(add.getClassObject().get());
  for (int idx{0}; idx < 3; ++idx)
  {
    EXPECT_EQ(idx, listVals[idx].getDataValue<int>());
  }
}//opAddOnParameterObjectsScalarAddList
