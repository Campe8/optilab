
#include "utest_globals.hpp"

#include "ObjectToolsAux.hpp"

#include <stdexcept>  // for std::logic_error

#define BOPtr(x) std::shared_ptr<Interpreter::BaseObject>(x)

namespace otools = Interpreter::ObjectTools;

OPTILAB_TEST(ObjectToolsAux, getPropertyNames)
{
  // Test property names
  using namespace Interpreter;
  
  ASSERT_EQ(std::string("timeMetrics"), otools::getMetricsTimeOptionsName());
  ASSERT_EQ(std::string("searchMetrics"), otools::getMetricsSearchOptionsName());
}//getObjectPropertyValue

OPTILAB_TEST(ObjectToolsAux, metricsOptionObjectDefault)
{
  // Test metrics option object creation (default values)
  using namespace Interpreter;
  
  auto optObj = BOPtr(otools::getMetricsOptionsObject());
  ASSERT_TRUE(optObj->hasProperty(otools::getMetricsTimeOptionsName()));
  ASSERT_TRUE(optObj->hasProperty(otools::getMetricsSearchOptionsName()));
  
  ASSERT_EQ(DataObject::DataObjectType::DOT_BOOL,
            optObj->getProperty(otools::getMetricsTimeOptionsName()).dataObjectType());
  
  ASSERT_EQ(DataObject::DataObjectType::DOT_BOOL,
            optObj->getProperty(otools::getMetricsSearchOptionsName()).dataObjectType());
  
  EXPECT_TRUE(optObj->getProperty(otools::getMetricsTimeOptionsName()).getDataValue<bool>());
  EXPECT_FALSE(optObj->getProperty(otools::getMetricsSearchOptionsName()).getDataValue<bool>());
}//metricsOptionObjectDefault

OPTILAB_TEST(ObjectToolsAux, metricsOptionObject)
{
  // Test metrics option object creation (non default values)
  using namespace Interpreter;
  
  auto optObj = BOPtr(otools::getMetricsOptionsObject(false, true));
  ASSERT_TRUE(optObj->hasProperty(otools::getMetricsTimeOptionsName()));
  ASSERT_TRUE(optObj->hasProperty(otools::getMetricsSearchOptionsName()));
  
  ASSERT_EQ(DataObject::DataObjectType::DOT_BOOL,
            optObj->getProperty(otools::getMetricsTimeOptionsName()).dataObjectType());
  
  ASSERT_EQ(DataObject::DataObjectType::DOT_BOOL,
            optObj->getProperty(otools::getMetricsSearchOptionsName()).dataObjectType());
  
  EXPECT_FALSE(optObj->getProperty(otools::getMetricsTimeOptionsName()).getDataValue<bool>());
  EXPECT_TRUE(optObj->getProperty(otools::getMetricsSearchOptionsName()).getDataValue<bool>());
}//metricsOptionObject
