
#include "utest_globals.hpp"

#include "ObjectFcn.hpp"

#include <memory>

namespace {
  using namespace Interpreter;
  
  DataObject constantFunction(BaseObject*, const std::vector<DataObject>&)
  {
    return DataObject(10);
  }
  
} // end namespace

OPTILAB_TEST(ObjectFcn, interfaceMethods)
{
  // Test get/set methods, clone, etc.
  using namespace Interpreter;
  
  ObjectFcn fcn("test");
  ASSERT_EQ(std::string("test"), fcn.getFcnName());
  ASSERT_FALSE(fcn.hasOutput());
  
  fcn.setAsOutputFcn();
  ASSERT_TRUE(fcn.hasOutput());
  ASSERT_EQ(DataObjectFcn::FcnType::FCN_OBJECT, fcn.getFcnType());
  
  // Test clone method
  auto cloneFcn = std::shared_ptr<DataObjectFcn>(fcn.clone());
  ASSERT_EQ(std::string("test"), cloneFcn->getFcnName());
  ASSERT_TRUE(cloneFcn->hasOutput());
}//interfaceMethods

OPTILAB_TEST(ObjectFcn, callback)
{
  // Test callback
  using namespace Interpreter;
  
  ObjectFcn fcn("test");
  fcn.setCallbackFcn(constantFunction);
  
  auto res = fcn(nullptr, {});
  ASSERT_EQ(DataObject::DataObjectType::DOT_INT, res.dataObjectType());
  ASSERT_EQ(10, res.getDataValue<int>());
}//callback

