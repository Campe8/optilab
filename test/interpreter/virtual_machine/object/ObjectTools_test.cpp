
#include "utest_globals.hpp"

#include "ObjectTools.hpp"
#include "ObjectHelper.hpp"

#include <stdexcept>  // for std::logic_error

#define BOPtr(x) std::shared_ptr<Interpreter::BaseObject>(x)

namespace otools = Interpreter::ObjectTools;

OPTILAB_TEST(ObjectTools, getObjectPropertyValue)
{
  // Test getObjectPropertyValue function
  using namespace Interpreter;
  
  // nullptr should return the default type-value
  ASSERT_EQ(int(), otools::getObjectPropertyValue<int>(nullptr, "test"));
  
  // If the object doesn't have that property, it should return the default type-value
  auto obj = std::shared_ptr<BaseObject>(otools::createObject("test"));
  ASSERT_EQ(int(), otools::getObjectPropertyValue<int>(obj.get(), "test"));
  
  // If the object does have that property, it should return its value
  obj->addProperty("test", 10);
  ASSERT_EQ(10, otools::getObjectPropertyValue<int>(obj.get(), "test"));
}//getObjectPropertyValue

OPTILAB_TEST(ObjectTools, setObjectPropertyValue)
{
  // Test setObjectPropertyValue functions
  using namespace Interpreter;
  
  // nullptr shouldn't set any property
  ASSERT_NO_THROW(otools::setObjectPropertyValue<std::string>(nullptr, "test", "prop"));
  ASSERT_NO_THROW(otools::setObjectPropertyValue<int>(nullptr, "test", 10));
  
  // If the object doesn't have that property is should not throw and "just" return
  auto obj = std::shared_ptr<BaseObject>(otools::createObject("test"));
  ASSERT_NO_THROW(otools::setObjectPropertyValue<std::string>(obj.get(), "test", "prop"));
  ASSERT_NO_THROW(otools::setObjectPropertyValue<int>(obj.get(), "test", 10));
  
  // Set the property
  obj->addProperty("prop1");
  obj->addProperty("prop2");
  otools::setObjectPropertyValue<std::string>(obj.get(), "prop1", "prop");
  otools::setObjectPropertyValue<int>(obj.get(), "prop2", 10);
  ASSERT_THAT("prop",
              ::testing::StrEq(otools::getObjectPropertyValue<std::string>(obj.get(), "prop1")));
  ASSERT_EQ(10, otools::getObjectPropertyValue<int>(obj.get(), "prop2"));
}//setObjectPropertyValue

OPTILAB_TEST(ObjectTools, createScalarObject)
{
  // Test that scalar base objects are created correctly
  auto intScalar = BOPtr(otools::createScalarObject(Interpreter::DataObject(static_cast<int>(1))));
  auto boolScalar =
  BOPtr(otools::createScalarObject(Interpreter::DataObject(static_cast<bool>(true))));
  auto doubleScalar =
  BOPtr(otools::createScalarObject(Interpreter::DataObject(static_cast<double>(2.0))));
  
  std::size_t sizetValue{3};
  auto size_tScalar = otools::createScalarObject(Interpreter::DataObject(sizetValue));
  auto stringScalar = otools::createScalarObject(Interpreter::DataObject("str"));
  
  EXPECT_TRUE(otools::isScalarObject(intScalar.get()));
  EXPECT_TRUE(otools::isScalarObject(boolScalar.get()));
  EXPECT_TRUE(otools::isScalarObject(doubleScalar.get()));
  EXPECT_TRUE(otools::isScalarObject(size_tScalar));
  EXPECT_TRUE(otools::isScalarObject(stringScalar));
  
  // Check property "data" of scalar object
  EXPECT_TRUE(intScalar->hasProperty("data"));
  EXPECT_TRUE(boolScalar->hasProperty("data"));
  EXPECT_TRUE(doubleScalar->hasProperty("data"));
  EXPECT_TRUE(size_tScalar->hasProperty("data"));
  EXPECT_TRUE(stringScalar->hasProperty("data"));
  
  const auto& intData = intScalar->getProperty("data");
  const auto& boolData = boolScalar->getProperty("data");
  const auto& doubleData = doubleScalar->getProperty("data");
  const auto& size_tData = size_tScalar->getProperty("data");
  const auto& stringData = stringScalar->getProperty("data");
  EXPECT_EQ(Interpreter::DataObject::DataObjectType::DOT_INT, intData.dataObjectType());
  EXPECT_EQ(Interpreter::DataObject::DataObjectType::DOT_BOOL, boolData.dataObjectType());
  EXPECT_EQ(Interpreter::DataObject::DataObjectType::DOT_DOUBLE, doubleData.dataObjectType());
  EXPECT_EQ(Interpreter::DataObject::DataObjectType::DOT_SIZE_T, size_tData.dataObjectType());
  EXPECT_EQ(Interpreter::DataObject::DataObjectType::DOT_STRING, stringData.dataObjectType());
  
  EXPECT_EQ(1, intData.getDataValue<int>());
  EXPECT_EQ(true, boolData.getDataValue<bool>());
  EXPECT_EQ(2.0, doubleData.getDataValue<double>());
  EXPECT_EQ(3, size_tData.getDataValue<std::size_t>());
  EXPECT_EQ("str", stringData.getDataValue<std::string>());
}//createScalarObject

OPTILAB_TEST(ObjectTools, createMatrixObjectWithWrongDimensions)
{
  // Test that a matrix with inconsistent rows throws
  Interpreter::DataObject rowA1;
  Interpreter::DataObject rowA2;
  rowA1.compose({Interpreter::DataObject(0), Interpreter::DataObject(1)});
  rowA2.compose({Interpreter::DataObject(0)});
  
  // Check that the function throws when the row dimensions are not consistent
  EXPECT_THROW(otools::createMatrixObject({rowA1, rowA2}), std::logic_error);
}//createMatrixObjectWithWrongDimensions

OPTILAB_TEST(ObjectTools, createEmptyMatrixObject)
{
  // Test creation of an empty matrix
  auto emptyMatrix = BOPtr(otools::createMatrixObject({}));
  ASSERT_TRUE(otools::isMatrixObject(emptyMatrix.get()));
  EXPECT_TRUE(emptyMatrix->hasProperty("dims"));
  EXPECT_TRUE(emptyMatrix->hasProperty("data"));
  
  const auto emptyDims = emptyMatrix->getProperty("dims");
  const auto emptyData = emptyMatrix->getProperty("data");
  EXPECT_TRUE(Interpreter::isClassObjectType(emptyDims));
  EXPECT_TRUE(otools::isListObject(emptyDims.getClassObject().get()));
  EXPECT_EQ(0, otools::getListSize(emptyDims.getClassObject().get()));
  EXPECT_TRUE(Interpreter::isClassObjectType(emptyData));
  EXPECT_TRUE(otools::isListObject(emptyData.getClassObject().get()));
  EXPECT_EQ(0, otools::getListSize(emptyData.getClassObject().get()));
}//createEmptyMatrixObject

OPTILAB_TEST(ObjectTools, createMatrixObjectFromPrimitiveObjects)
{
  // Test creation of a row vector matrix
  auto rowVector =
  BOPtr(otools::createMatrixObject({Interpreter::DataObject(0), Interpreter::DataObject(1)}));
  ASSERT_TRUE(otools::isMatrixObject(rowVector.get()));
  EXPECT_TRUE(rowVector->hasProperty("dims"));
  EXPECT_TRUE(rowVector->hasProperty("data"));
  
  // Dimensions should be [1 x 2]
  const auto& rowDims = rowVector->getProperty("dims");
  const auto& rowData = rowVector->getProperty("data");
  EXPECT_TRUE(Interpreter::isClassObjectType(rowDims));
  EXPECT_TRUE(otools::isListObject(rowDims.getClassObject().get()));
  EXPECT_EQ(2, otools::getListSize(rowDims.getClassObject().get()));
  
  auto dimVals = otools::getListValues(rowDims.getClassObject().get());
  ASSERT_TRUE(Interpreter::isCompositeType(dimVals));
  EXPECT_EQ(1, dimVals[0].getDataValue<int>());
  EXPECT_EQ(2, dimVals[1].getDataValue<int>());
  
  // Data should be a list of lists
  ASSERT_TRUE(Interpreter::isClassObjectType(rowData));
  ASSERT_TRUE(otools::isListObject(rowData.getClassObject().get()));
  
  // Get the list of lists, it should contain only one list, i.e., the row
  auto rowVals = otools::getListValues(rowData.getClassObject().get());
  ASSERT_TRUE(Interpreter::isCompositeType(rowVals));
  ASSERT_EQ(1, rowVals.composeSize());
  ASSERT_TRUE(Interpreter::isClassObjectType(rowVals[0]));
  ASSERT_TRUE(otools::isListObject(rowVals[0].getClassObject().get()));
  
  auto data = otools::getListValues(rowVals[0].getClassObject().get());
  ASSERT_TRUE(Interpreter::isCompositeType(data));
  ASSERT_EQ(2, data.composeSize());
  for (std::size_t idx{0}; idx < 2; idx++)
  {
    EXPECT_EQ(static_cast<int>(idx), data[idx].getDataValue<int>());
  }
}//createMatrixObjectFromPrimitiveObjects

OPTILAB_TEST(ObjectTools, createMatrixObjectFromScalarObjects)
{
  // Test creation of a row vector matrix
  Interpreter::DataObject d0;
  Interpreter::DataObject d1;
  auto d0obj =
  std::shared_ptr<Interpreter::BaseObject>(otools::createScalarObject(Interpreter::DataObject(0)));
  auto d1obj =
  std::shared_ptr<Interpreter::BaseObject>(otools::createScalarObject(Interpreter::DataObject(1)));
  d0.setClassObject(d0obj);
  d1.setClassObject(d1obj);
  
  auto rowVector =
  BOPtr(otools::createMatrixObject({d0, d1}));
  ASSERT_TRUE(otools::isMatrixObject(rowVector.get()));
  EXPECT_TRUE(rowVector->hasProperty("dims"));
  EXPECT_TRUE(rowVector->hasProperty("data"));
  
  // Dimensions should be [1 x 2]
  const auto& rowDims = rowVector->getProperty("dims");
  const auto& rowData = rowVector->getProperty("data");
  EXPECT_TRUE(Interpreter::isClassObjectType(rowDims));
  EXPECT_TRUE(otools::isListObject(rowDims.getClassObject().get()));
  EXPECT_EQ(2, otools::getListSize(rowDims.getClassObject().get()));
  
  auto dimVals = otools::getListValues(rowDims.getClassObject().get());
  ASSERT_TRUE(Interpreter::isCompositeType(dimVals));
  EXPECT_EQ(1, dimVals[0].getDataValue<int>());
  EXPECT_EQ(2, dimVals[1].getDataValue<int>());
  
  // Data should be a list of lists
  ASSERT_TRUE(Interpreter::isClassObjectType(rowData));
  ASSERT_TRUE(otools::isListObject(rowData.getClassObject().get()));
  
  // Get the list of lists, it should contain only one list, i.e., the row
  auto rowVals = otools::getListValues(rowData.getClassObject().get());
  ASSERT_TRUE(Interpreter::isCompositeType(rowVals));
  ASSERT_EQ(1, rowVals.composeSize());
  ASSERT_TRUE(Interpreter::isClassObjectType(rowVals[0]));
  ASSERT_TRUE(otools::isListObject(rowVals[0].getClassObject().get()));
  
  auto data = otools::getListValues(rowVals[0].getClassObject().get());
  ASSERT_TRUE(Interpreter::isCompositeType(data));
  ASSERT_EQ(2, data.composeSize());
  for (std::size_t idx{0}; idx < 2; idx++)
  {
    ASSERT_TRUE(Interpreter::isClassObjectType(data[idx]));
    ASSERT_TRUE(otools::isScalarObject(data[idx].getClassObject().get()));
    EXPECT_EQ(static_cast<int>(idx), otools::getScalarValue(data[idx].getClassObject().get())
              .getDataValue<int>());
  }
}//createMatrixObjectFromScalarObjects

OPTILAB_TEST(ObjectTools, createMatrixObjectFromCompositeObjects)
{
  // Test that matrix base objects are created correctly from composite objects
  
  // Create matrix: vector of composite objects
  Interpreter::DataObject rowA1;
  Interpreter::DataObject rowB1;
  rowA1.compose({Interpreter::DataObject(0), Interpreter::DataObject(1)});
  rowB1.compose({Interpreter::DataObject(2), Interpreter::DataObject(3)});
  
  // Check dimensions and values for a well-constructed matrix
  auto matrix = BOPtr(otools::createMatrixObject({rowA1, rowB1}));
  ASSERT_TRUE(otools::isMatrixObject(matrix.get()));
  EXPECT_TRUE(matrix->hasProperty("dims"));
  EXPECT_TRUE(matrix->hasProperty("data"));
  
  const auto& matDims = matrix->getProperty("dims");
  const auto& matData = matrix->getProperty("data");
  EXPECT_TRUE(Interpreter::isClassObjectType(matDims));
  EXPECT_TRUE(otools::isListObject(matDims.getClassObject().get()));
  EXPECT_EQ(2, otools::getListSize(matDims.getClassObject().get()));
  
  // Check dimensions, it should be a [2 x 2] matrix
  auto matDimVals = otools::getListValues(matDims.getClassObject().get());
  ASSERT_TRUE(Interpreter::isCompositeType(matDimVals));
  EXPECT_EQ(2, matDimVals[0].getDataValue<int>());
  EXPECT_EQ(2, matDimVals[1].getDataValue<int>());
  
  // Data should be a list of lists
  ASSERT_TRUE(Interpreter::isClassObjectType(matData));
  ASSERT_TRUE(otools::isListObject(matData.getClassObject().get()));
  
  // Get the list of lists, it should be a list of 2 lists, i.e., the two rows
  auto rowVals = otools::getListValues(matData.getClassObject().get());
  ASSERT_TRUE(Interpreter::isCompositeType(rowVals));
  ASSERT_EQ(2, rowVals.composeSize());
  ASSERT_TRUE(Interpreter::isClassObjectType(rowVals[0]));
  ASSERT_TRUE(Interpreter::isClassObjectType(rowVals[1]));
  ASSERT_TRUE(otools::isListObject(rowVals[0].getClassObject().get()));
  ASSERT_TRUE(otools::isListObject(rowVals[1].getClassObject().get()));
  
  auto dataRow1 = otools::getListValues(rowVals[0].getClassObject().get());
  auto dataRow2 = otools::getListValues(rowVals[1].getClassObject().get());
  ASSERT_TRUE(Interpreter::isCompositeType(dataRow1));
  ASSERT_TRUE(Interpreter::isCompositeType(dataRow2));
  ASSERT_EQ(2, dataRow1.composeSize());
  ASSERT_EQ(2, dataRow2.composeSize());
  
  EXPECT_EQ(static_cast<int>(0), dataRow1[0].getDataValue<int>());
  EXPECT_EQ(static_cast<int>(1), dataRow1[1].getDataValue<int>());
  EXPECT_EQ(static_cast<int>(2), dataRow2[0].getDataValue<int>());
  EXPECT_EQ(static_cast<int>(3), dataRow2[1].getDataValue<int>());
}//createMatrixObjectFromCompositeObjects

OPTILAB_TEST(ObjectTools, createMatrixObjectFromListObjects)
{
  // Test that matrix base objects are created correctly from list objects
  
  // Create matrix: vector of list objects
  Interpreter::DataObject rowA1;
  Interpreter::DataObject rowB1;
  rowA1.setClassObject(otools::createListObjectSPtr(
  {Interpreter::DataObject(0), Interpreter::DataObject(1)}));
  rowB1.setClassObject(otools::createListObjectSPtr(
  {Interpreter::DataObject(2), Interpreter::DataObject(3)}));
  
  // Check dimensions and values for a well-constructed matrix
  auto matrix = BOPtr(otools::createMatrixObject({rowA1, rowB1}));
  ASSERT_TRUE(otools::isMatrixObject(matrix.get()));
  EXPECT_TRUE(matrix->hasProperty("dims"));
  EXPECT_TRUE(matrix->hasProperty("data"));
  
  const auto& matDims = matrix->getProperty("dims");
  const auto& matData = matrix->getProperty("data");
  EXPECT_TRUE(Interpreter::isClassObjectType(matDims));
  EXPECT_TRUE(otools::isListObject(matDims.getClassObject().get()));
  EXPECT_EQ(2, otools::getListSize(matDims.getClassObject().get()));
  
  // Check dimensions, it should be a [2 x 2] matrix
  auto matDimVals = otools::getListValues(matDims.getClassObject().get());
  ASSERT_TRUE(Interpreter::isCompositeType(matDimVals));
  EXPECT_EQ(2, matDimVals[0].getDataValue<int>());
  EXPECT_EQ(2, matDimVals[1].getDataValue<int>());
  
  // Data should be a list of lists
  ASSERT_TRUE(Interpreter::isClassObjectType(matData));
  ASSERT_TRUE(otools::isListObject(matData.getClassObject().get()));
  
  // Get the list of lists, it should be a list of 2 lists, i.e., the two rows
  auto rowVals = otools::getListValues(matData.getClassObject().get());
  ASSERT_TRUE(Interpreter::isCompositeType(rowVals));
  ASSERT_EQ(2, rowVals.composeSize());
  ASSERT_TRUE(Interpreter::isClassObjectType(rowVals[0]));
  ASSERT_TRUE(Interpreter::isClassObjectType(rowVals[1]));
  ASSERT_TRUE(otools::isListObject(rowVals[0].getClassObject().get()));
  ASSERT_TRUE(otools::isListObject(rowVals[1].getClassObject().get()));
  
  auto dataRow1 = otools::getListValues(rowVals[0].getClassObject().get());
  auto dataRow2 = otools::getListValues(rowVals[1].getClassObject().get());
  ASSERT_TRUE(Interpreter::isCompositeType(dataRow1));
  ASSERT_TRUE(Interpreter::isCompositeType(dataRow2));
  ASSERT_EQ(2, dataRow1.composeSize());
  ASSERT_EQ(2, dataRow2.composeSize());
  
  EXPECT_EQ(static_cast<int>(0), dataRow1[0].getDataValue<int>());
  EXPECT_EQ(static_cast<int>(1), dataRow1[1].getDataValue<int>());
  EXPECT_EQ(static_cast<int>(2), dataRow2[0].getDataValue<int>());
  EXPECT_EQ(static_cast<int>(3), dataRow2[1].getDataValue<int>());
}//createMatrixObjectFromListObjects

OPTILAB_TEST(ObjectTools, createMatrixObjectFromMatrixObjects)
{
  // Test that matrix base objects are created correctly from matrix objects
  
  // Create matrix: vector of matrix objects
  Interpreter::DataObject matA1;
  Interpreter::DataObject matB1;
  Interpreter::DataObject matB2;
  
  Interpreter::DataObject a1_d1;
  Interpreter::DataObject a1_d2;
  a1_d1.compose({Interpreter::DataObject(0), Interpreter::DataObject(1)});
  a1_d2.compose({Interpreter::DataObject(2), Interpreter::DataObject(3)});
  
  Interpreter::DataObject b1_d1;
  Interpreter::DataObject b1_d2;
  Interpreter::DataObject b1_d3;
  b1_d1.compose({Interpreter::DataObject(4), Interpreter::DataObject(5)});
  b1_d2.compose({Interpreter::DataObject(6), Interpreter::DataObject(7)});
  b1_d3.compose({Interpreter::DataObject(8), Interpreter::DataObject(9)});
  
  // Create
  // matA1 = [0, 1;
  //          2, 3]
  auto matA1Obj =
  std::shared_ptr<Interpreter::BaseObject>(otools::createMatrixObject({a1_d1, a1_d2}));
  matA1.setClassObject(matA1Obj);
  
  // Create
  // matB1 = [4, 5;
  //          6, 7;
  //          8, 9]
  auto matB1Obj =
  std::shared_ptr<Interpreter::BaseObject>(otools::createMatrixObject({b1_d1, b1_d2, b1_d3}));
  matB1.setClassObject(matB1Obj);
  
  // Create
  // matB2 = [4, 5;
  //          6, 7]
  auto matB2Obj =
  std::shared_ptr<Interpreter::BaseObject>(otools::createMatrixObject({b1_d1, b1_d2}));
  matB2.setClassObject(matB2Obj);
  
  // Calling createMatrixObject() on matA1 and matB1 should throw since the two sub-matrices
  // don't have the same dimensions
  ASSERT_THROW(otools::createMatrixObject({matA1, matB1}), std::logic_error);
  
  // Create the composed matrix
  // [matA1, matB2] = [[0, 1;
  //                    2, 3],
  //                   [4, 5
  //                    6, 7]]
  // It should create a [2 x 2 x 2] matrix
  auto matrix = BOPtr(otools::createMatrixObject({matA1, matB2}));
  ASSERT_TRUE(otools::isMatrixObject(matrix.get()));
  EXPECT_TRUE(matrix->hasProperty("dims"));
  EXPECT_TRUE(matrix->hasProperty("data"));
  
  const auto& matDims = matrix->getProperty("dims");
  const auto& matData = matrix->getProperty("data");
  EXPECT_TRUE(Interpreter::isClassObjectType(matDims));
  EXPECT_TRUE(otools::isListObject(matDims.getClassObject().get()));
  EXPECT_EQ(3, otools::getListSize(matDims.getClassObject().get()));
  
  // Check dimensions, it should be a [2 x 2 x 2] matrix
  auto matDimVals = otools::getListValues(matDims.getClassObject().get());
  ASSERT_TRUE(Interpreter::isCompositeType(matDimVals));
  EXPECT_EQ(2, matDimVals[0].getDataValue<int>());
  EXPECT_EQ(2, matDimVals[1].getDataValue<int>());
  EXPECT_EQ(2, matDimVals[2].getDataValue<int>());
  
  // Data should be a list of lists
  ASSERT_TRUE(Interpreter::isClassObjectType(matData));
  ASSERT_TRUE(otools::isListObject(matData.getClassObject().get()));
  
  // Get the list of lists, it should be a list of 2 lists, i.e., the 2 sub-matrices
  auto rowVals = otools::getListValues(matData.getClassObject().get());
  ASSERT_TRUE(Interpreter::isCompositeType(rowVals));
  ASSERT_EQ(2, rowVals.composeSize());
  
  ASSERT_TRUE(Interpreter::isClassObjectType(rowVals[0]));
  ASSERT_TRUE(Interpreter::isClassObjectType(rowVals[1]));
  ASSERT_TRUE(otools::isListObject(rowVals[0].getClassObject().get()));
  ASSERT_TRUE(otools::isListObject(rowVals[1].getClassObject().get()));
  
  int scalarVal{0};
  for (std::size_t idx{0}; idx < 2; ++idx)
  {
    ASSERT_TRUE(Interpreter::isClassObjectType(rowVals[idx]));
    ASSERT_TRUE(otools::isListObject(rowVals[idx].getClassObject().get()));
    
    const auto& subMatrix = otools::getListValues(rowVals[idx].getClassObject().get());
    ASSERT_TRUE(Interpreter::isCompositeType(subMatrix));
    ASSERT_EQ(2, subMatrix.composeSize());
    for (std::size_t jdx{0}; jdx < 2; ++jdx)
    {
      ASSERT_TRUE(Interpreter::isClassObjectType(subMatrix[jdx]));
      ASSERT_TRUE(otools::isListObject(subMatrix[jdx].getClassObject().get()));
      
      const auto& row = otools::getListValues(subMatrix[jdx].getClassObject().get());
      ASSERT_TRUE(Interpreter::isCompositeType(row));
      ASSERT_EQ(2, row.composeSize());
      for (std::size_t zdx{0}; zdx < 2; ++zdx)
      {
        ASSERT_TRUE(Interpreter::isPrimitiveType(row[zdx]));
        EXPECT_EQ(scalarVal++, row[zdx].getDataValue<int>());
      }
    }
  }
}//createMatrixObjectFromMatrixObjects

OPTILAB_TEST(ObjectTools, createInvalidSearchObject)
{
  // Test that search base objects throw on invalid arguments
  
  // Throw for non-list or non-composite arguments
  EXPECT_THROW(otools::createObjectSearch(Interpreter::DataObject(2)), std::exception);
  
  // Throw for non search options arguments
  Interpreter::DataObject scope;
  Interpreter::DataObject list;
  scope.compose({Interpreter::DataObject(0)});
  list.setClassObject(otools::createListObjectSPtr({Interpreter::DataObject(0)}));
  EXPECT_THROW(otools::createObjectSearch(scope, list), std::exception);
}//createInvalidSearchObject

OPTILAB_TEST(ObjectTools, createSearchObjectFromCompositeObject)
{
  // Test that search base objects are creted correctly from composite objects
  
  Interpreter::DataObject scope;
  scope.compose({Interpreter::DataObject(0)});
  auto searchObject = BOPtr(otools::createObjectSearch(scope));
  ASSERT_TRUE(otools::isSearchObject(searchObject.get()));
  
  auto scopeList = otools::getSearchScope(searchObject.get());
  ASSERT_TRUE(Interpreter::isClassObjectType(scopeList));
  ASSERT_TRUE(otools::isListObject(scopeList.getClassObject().get()));
  EXPECT_EQ(1, otools::getListSize(scopeList.getClassObject().get()));
}//createSearchObjectFromCompositeObject

OPTILAB_TEST(ObjectTools, createSearchObjectFromListObject)
{
  // Test that search base objects are creted correctly from list objects
  
  Interpreter::DataObject scope;
  scope.setClassObject(otools::createListObjectSPtr({Interpreter::DataObject(0)}));
  auto searchObject = BOPtr(otools::createObjectSearch(scope));
  ASSERT_TRUE(otools::isSearchObject(searchObject.get()));
  
  auto scopeList = otools::getSearchScope(searchObject.get());
  ASSERT_TRUE(Interpreter::isClassObjectType(scopeList));
  ASSERT_TRUE(otools::isListObject(scopeList.getClassObject().get()));
  EXPECT_EQ(1, otools::getListSize(scopeList.getClassObject().get()));
}//createSearchObjectFromCompositeObject

OPTILAB_TEST(ObjectTools, createConstraintObject)
{
  // Test that constraint base objects are creted correctly
  
  Interpreter::DataObject dobj("callbackFcn");
  dobj.setFcnObject(new Interpreter::ObjectFcn("fcn"));
  
  Interpreter::BaseObjectSPtr constraint(otools::createObjectConstraint(dobj));
  ASSERT_TRUE(otools::isConstraintObject(constraint.get()));
}//createConstraintObject
