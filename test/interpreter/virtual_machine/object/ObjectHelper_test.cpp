
#include "utest_globals.hpp"

#include "ObjectTools.hpp"
#include "ObjectHelper.hpp"
#include "SearchOptionsTools.hpp"

#define BOPtr(x) std::shared_ptr<Interpreter::BaseObject>(x)

namespace otools = Interpreter::ObjectTools;

OPTILAB_TEST(ObjectHelper, functions)
{
  // Test helper functions
  using namespace Interpreter;
  
  const auto& helper = otools::ObjectHelper::getInstance();
  ASSERT_TRUE(helper.isObjectClassRegistered("List"));
}//functions

OPTILAB_TEST(ObjectHelper, scalarObjectsHelper)
{
  // Test helper functions for scalar objects
  EXPECT_EQ("Scalar", otools::getScalarClassName());
  
  auto list = otools::createListObject({});
  auto scalar = otools::createScalarObject(Interpreter::DataObject(3));
  EXPECT_FALSE(otools::isScalarObject(list));
  EXPECT_TRUE(otools::isScalarObject(scalar));
  
  EXPECT_EQ(3, otools::getScalarValue(scalar).getDataValue<int>());
}//scalarObjectsHelper

OPTILAB_TEST(ObjectHelper, matrixObjectsHelper)
{
  // Test helper functions for matrix objects
  EXPECT_EQ("Matrix", otools::getMatrixClassName());
  
  auto list = otools::createListObject({});
  auto matrix = otools::createMatrixObject({Interpreter::DataObject(3)});
  EXPECT_FALSE(otools::isMatrixObject(list));
  EXPECT_TRUE(otools::isMatrixObject(matrix));
}//matrixObjectsHelper

OPTILAB_TEST(ObjectHelper, getMatrixElementOnVectorWithPrimitiveIndex)
{
  // Test helper functions "getElement" for matrix objects.
  // This test point checks that the indexing works on matrix "vectors" using a primitive index
  auto vector =
  BOPtr(otools::createMatrixObject({Interpreter::DataObject(10), Interpreter::DataObject(11)}));
  
  const auto& val = otools::getMatrixElement(vector.get(), Interpreter::DataObject(0));
  ASSERT_TRUE(Interpreter::isPrimitiveType(val));
  EXPECT_EQ(Interpreter::DataObject::DataObjectType::DOT_INT, val.dataObjectType());
  EXPECT_EQ(10, val.getDataValue<int>());
  
  // The same element, in case of a vector, can be retrieved also with indexing [0, 0]
  Interpreter::DataObject indexing;
  indexing.compose({Interpreter::DataObject(0), Interpreter::DataObject(0)});
  const auto& val2 = otools::getMatrixElement(vector.get(), indexing);
  ASSERT_TRUE(Interpreter::isPrimitiveType(val2));
  EXPECT_EQ(Interpreter::DataObject::DataObjectType::DOT_INT, val2.dataObjectType());
  EXPECT_EQ(10, val.getDataValue<int>());
  
  // The value should be a reference into the matrix
  otools::getMatrixElement(vector.get(), indexing) = Interpreter::DataObject(20);
  const auto& val3 = otools::getMatrixElement(vector.get(), Interpreter::DataObject(0));
  ASSERT_TRUE(Interpreter::isPrimitiveType(val3));
  EXPECT_EQ(Interpreter::DataObject::DataObjectType::DOT_INT, val3.dataObjectType());
  EXPECT_EQ(20, val3.getDataValue<int>());
}//getMatrixElementOnVectorWithPrimitiveIndex

OPTILAB_TEST(ObjectHelper, getMatrixElementOn3DMatrix)
{
  // Test helper functions "getElement" on complex matrix indexing
  Interpreter::DataObject matA1;
  Interpreter::DataObject matB1;
  
  Interpreter::DataObject a1_d1;
  Interpreter::DataObject a1_d2;
  a1_d1.compose({Interpreter::DataObject(0), Interpreter::DataObject(1)});
  a1_d2.compose({Interpreter::DataObject(2), Interpreter::DataObject(3)});
  
  Interpreter::DataObject b1_d1;
  Interpreter::DataObject b1_d2;
  b1_d1.compose({Interpreter::DataObject(4), Interpreter::DataObject(5)});
  b1_d2.compose({Interpreter::DataObject(6), Interpreter::DataObject(7)});
  
  // Create
  // matA1 = [[0, 1];
  //          [2, 3]]
  auto matA1Obj =
  std::shared_ptr<Interpreter::BaseObject>(otools::createMatrixObject({a1_d1, a1_d2}));
  matA1.setClassObject(matA1Obj);
  
  // Create
  // matB2 = [[4, 5];
  //          [6, 7]]
  auto matB1Obj =
  std::shared_ptr<Interpreter::BaseObject>(otools::createMatrixObject({b1_d1, b1_d2}));
  matB1.setClassObject(matB1Obj);
  
  // Create the composed matrix
  // [matA1, matB1] = [[[0, 1];
  //                    [2, 3]];
  //                   [[4, 5];
  //                    [6, 7]]]
  // It should create a [2 x 2 x 2] matrix
  auto matrix = BOPtr(otools::createMatrixObject({matA1, matB1}));
  
  // Get the sub-matrix
  // [[4, 5];
  //  [6, 7]]
  Interpreter::DataObject index1;
  index1.compose({Interpreter::DataObject(1)});
  const auto& subMat = otools::getMatrixElement(matrix.get(), index1);
  
  // subMat should be a list of two lists [4, 5] and [6, 7]
  ASSERT_TRUE(Interpreter::isClassObjectType(subMat));
  ASSERT_TRUE(otools::isListObject(subMat.getClassObject().get()));
  
  auto listOfList = otools::getListValues(subMat.getClassObject().get());
  ASSERT_TRUE(Interpreter::isCompositeType(listOfList));
  EXPECT_EQ(2, listOfList.composeSize());
  EXPECT_TRUE(Interpreter::isClassObjectType(listOfList[0]));
  EXPECT_TRUE(otools::isListObject(listOfList[0].getClassObject().get()));
  
  auto list = otools::getListValues(listOfList[0].getClassObject().get());
  ASSERT_TRUE(Interpreter::isCompositeType(list));
  EXPECT_EQ(2, list.composeSize());
  EXPECT_TRUE(Interpreter::isPrimitiveType(list[0]));
  EXPECT_EQ(4, list[0].getDataValue<int>());
  
  // Get element matrix[0, 1, 0] = 2
  Interpreter::DataObject index2;
  index2.compose({ Interpreter::DataObject(0), Interpreter::DataObject(1),
    Interpreter::DataObject(0)});
  const auto& element = otools::getMatrixElement(matrix.get(), index2);
  ASSERT_TRUE(Interpreter::isPrimitiveType(element));
  EXPECT_EQ(2, element.getDataValue<int>());
  
  // Check that the values are references
  otools::getMatrixElement(matrix.get(), index2) = Interpreter::DataObject(100);
  const auto& element2 = otools::getMatrixElement(matrix.get(), index2);
  ASSERT_TRUE(Interpreter::isPrimitiveType(element2));
  EXPECT_EQ(100, element2.getDataValue<int>());
}//getMatrixElementOn3DMatrix

OPTILAB_TEST(ObjectHelper, searchObjectsHelper)
{
  // Test helper functions for search objects
  EXPECT_EQ("Search", otools::getSearchClassName());
  
  auto list = otools::createListObject({});
  
  Interpreter::DataObject scope;
  auto listScope = std::shared_ptr<Interpreter::BaseObject>(
                                        otools::createListObject({Interpreter::DataObject("var")}));
  scope.setClassObject(listScope);
  auto search = otools::createObjectSearch(scope);
  EXPECT_FALSE(otools::isSearchObject(list));
  EXPECT_TRUE(otools::isSearchObject(search));
  
  // Get search scope
  const auto& srcScope = otools::getSearchScope(search);
  EXPECT_TRUE(Interpreter::isClassObjectType(srcScope));
  EXPECT_TRUE(otools::isListObject(srcScope.getClassObject().get()));
  EXPECT_EQ(1, otools::getListValues(srcScope.getClassObject().get()).composeSize());
  EXPECT_EQ(Interpreter::DataObject::DataObjectType::DOT_STRING,
            otools::getListValues(srcScope.getClassObject().get())[0].dataObjectType());
  EXPECT_EQ("var",
            otools::getListValues(srcScope.getClassObject().get())[0].getDataValue<std::string>());
  
  // Gewt search options
  const auto& srcOptions = otools::getSearchOptions(search);
  EXPECT_TRUE(Interpreter::isClassObjectType(srcOptions));
  
  auto options = srcOptions.getClassObject().get();
  EXPECT_TRUE(Interpreter::SearchOptionsTools::isSearchOptionsObject(options));
}//searchObjectsHelper

OPTILAB_TEST(ObjectHelper, constraintObjectsHelper)
{
  // Test helper functions for constraint objects
  EXPECT_EQ("Constraint", otools::getConstraintClassName());
  
  auto list = otools::createListObject({});
  
  Interpreter::ObjectFcn* doFcn = new Interpreter::ObjectFcn("fcn");
  Interpreter::DataObject dobj("callbackFcn");
  dobj.setFcnObject(doFcn);
  auto constraint = otools::createObjectConstraint(dobj);
  
  EXPECT_FALSE(otools::isConstraintObject(list));
  EXPECT_TRUE(otools::isConstraintObject(constraint));
  
  const auto postFcn = otools::getConstraintPostFcn(constraint);
  EXPECT_TRUE(Interpreter::isFunctionType(postFcn));
  EXPECT_EQ("fcn", postFcn.getFcn().getFcnName());
}//constraintObjectsHelper

OPTILAB_TEST(ObjectHelper, parameterObjectsHelper)
{
  auto scalar = otools::createScalarObject(Interpreter::DataObject(3));
  auto list = otools::createListObject({});
  auto matrix = otools::createMatrixObject({Interpreter::DataObject(3)});
  
  EXPECT_TRUE(otools::isParameterObject(scalar));
  EXPECT_TRUE(otools::isScalarParameterObject(scalar));
  EXPECT_FALSE(otools::isListParameterObject(scalar));
  EXPECT_FALSE(otools::isMatrixParameterObject(scalar));
  
  EXPECT_TRUE(otools::isParameterObject(list));
  EXPECT_TRUE(otools::isListParameterObject(list));
  EXPECT_FALSE(otools::isScalarParameterObject(list));
  EXPECT_FALSE(otools::isMatrixParameterObject(list));
  
  EXPECT_TRUE(otools::isParameterObject(matrix));
  EXPECT_TRUE(otools::isMatrixParameterObject(matrix));
  EXPECT_FALSE(otools::isScalarParameterObject((matrix)));
  EXPECT_FALSE(otools::isListParameterObject((matrix)));
}//parameterObjectsHelper

OPTILAB_TEST(ObjectHelper, getVectorizedMatrixData)
{
  // Test helper functions "getVectorizedMatrixData" on complex matrix
  Interpreter::DataObject matA1;
  Interpreter::DataObject matB1;
  
  Interpreter::DataObject a1_d1;
  Interpreter::DataObject a1_d2;
  a1_d1.compose({Interpreter::DataObject(0), Interpreter::DataObject(1)});
  a1_d2.compose({Interpreter::DataObject(2), Interpreter::DataObject(3)});
  
  Interpreter::DataObject b1_d1;
  Interpreter::DataObject b1_d2;
  b1_d1.compose({Interpreter::DataObject(4), Interpreter::DataObject(5)});
  b1_d2.compose({Interpreter::DataObject(6), Interpreter::DataObject(7)});
  
  // Create
  // matA1 = [[0, 1];
  //          [2, 3]]
  auto matA1Obj =
  std::shared_ptr<Interpreter::BaseObject>(otools::createMatrixObject({a1_d1, a1_d2}));
  matA1.setClassObject(matA1Obj);
  
  // Create
  // matB2 = [[4, 5];
  //          [6, 7]]
  auto matB1Obj =
  std::shared_ptr<Interpreter::BaseObject>(otools::createMatrixObject({b1_d1, b1_d2}));
  matB1.setClassObject(matB1Obj);
  
  // Create the composed matrix
  // [matA1, matB1] = [[[0, 1];
  //                    [2, 3]];
  //                   [[4, 5];
  //                    [6, 7]]]
  // It should create a [2 x 2 x 2] matrix
  auto matrix = BOPtr(otools::createMatrixObject({matA1, matB1}));
  auto vector = otools::getVectorizedMatrixData(matrix.get());
  ASSERT_EQ(8, vector.size());
  
  for (int idx{0}; idx < 8; ++idx)
  {
    EXPECT_EQ(idx, vector[idx].getDataValue<int>());
  }
}// getVectorizedMatrixData
