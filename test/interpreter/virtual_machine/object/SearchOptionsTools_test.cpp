
#include "utest_globals.hpp"

#include "SearchOptionsTools.hpp"

#include "DFSInformation.hpp"
#include "GeneticInformation.hpp"

OPTILAB_TEST(SearchOptionsTools, keyValues)
{
  // Test key values
  using namespace Interpreter::SearchOptionsTools;
  
  ASSERT_EQ(std::string("SearchOptions"), getSearchOptionsTypeName());
  ASSERT_EQ(std::string("CBLSSearchOptions"), getCBLSSearchOptionsTypeName());
  ASSERT_EQ(std::string("DFSSearchOptions"), getDFSSearchOptionsTypeName());
  ASSERT_EQ(std::string("GeneticSearchOptions"), getGeneticSearchOptionsTypeName());
  ASSERT_EQ(std::string("classType"), getClassTypePropName());
  ASSERT_EQ(std::string("strategy"), getStrategyPropName());
  ASSERT_EQ(std::string("timeout"), getTimeoutPropName());
}//keyValues

OPTILAB_TEST(SearchOptionsTools, dfsSearchOptions)
{
  // Test constructor for dfs search options
  using namespace Interpreter::SearchOptionsTools;
  using namespace Interpreter::ObjectTools;
  auto obj = getDFSSearchOptionsObject();
  
  ASSERT_TRUE(obj);
  ASSERT_TRUE(isSearchOptionsObject(obj));
  
  // Timeout, default is -1
  ASSERT_TRUE(obj->hasProperty(getTimeoutPropName()));
  EXPECT_EQ(-1, getObjectPropertyValue<int>(obj, getTimeoutPropName()));
  
  // getTypePropName
  ASSERT_TRUE(obj->hasProperty(getTypePropName()));
  ASSERT_EQ(getDFSSearchOptionsTypeName(),
            getObjectPropertyValue<std::string>(obj, getTypePropName()));
  ASSERT_FALSE(obj->isPropertyVisibile(getTypePropName()));
  
  ASSERT_EQ(static_cast<char>(Model::SearchInformation::SearchInformationType::SI_DFS),
            getClassType(obj));
  
  // Check options specific parameters
  ASSERT_TRUE(obj->hasProperty(getStrategyPropName()));
  ASSERT_TRUE(obj->isConstProperty(getStrategyPropName()));
  ASSERT_EQ(Model::DFSInformation::StrategyName,
            getObjectPropertyValue<std::string>(obj, getStrategyPropName()));
  
  ASSERT_TRUE(obj->hasProperty(Model::DFSInformation::VariableSelectionKey));
  ASSERT_EQ(Model::DFSInformation::getDefaultVariableChoice(),
            getObjectPropertyValue<std::string>(obj, Model::DFSInformation::VariableSelectionKey));
  
  ASSERT_TRUE(obj->hasProperty(Model::DFSInformation::ValueSelectionKey));
  ASSERT_EQ(Model::DFSInformation::getDefaultValueChoice(),
            getObjectPropertyValue<std::string>(obj, Model::DFSInformation::ValueSelectionKey));
}//dfsSearchOptions

OPTILAB_TEST(SearchOptionsTools, geneticSearchOptions)
{
  // Test constructor for genetic search options
  using namespace Interpreter::SearchOptionsTools;
  using namespace Interpreter::ObjectTools;
  auto obj = getGeneticSearchOptionsObject();
  
  ASSERT_TRUE(obj);
  ASSERT_TRUE(isSearchOptionsObject(obj));
  
  // Timeout, default is -1
  ASSERT_TRUE(obj->hasProperty(getTimeoutPropName()));
  EXPECT_EQ(-1, getObjectPropertyValue<int>(obj, getTimeoutPropName()));
  
  // getTypePropName
  ASSERT_TRUE(obj->hasProperty(getTypePropName()));
  ASSERT_EQ(getGeneticSearchOptionsTypeName(),
            getObjectPropertyValue<std::string>(obj, getTypePropName()));
  ASSERT_FALSE(obj->isPropertyVisibile(getTypePropName()));
  
  ASSERT_EQ(static_cast<char>(Model::SearchInformation::SearchInformationType::SI_GENETIC),
            getClassType(obj));
  
  // Check options specific parameters
  ASSERT_TRUE(obj->hasProperty(getStrategyPropName()));
  EXPECT_EQ(Model::GeneticInformation::StrategyName,
            getObjectPropertyValue<std::string>(obj, getStrategyPropName()));
  
  ASSERT_TRUE(obj->hasProperty(Model::SearchInformation::TimeoutKey));
  EXPECT_EQ(-1, getObjectPropertyValue<int>(obj, Model::SearchInformation::TimeoutKey));
  
  ASSERT_TRUE(obj->hasProperty(Model::CBLSInformation::RandomSeedKey));
  EXPECT_EQ(0, getObjectPropertyValue<int>(obj, Model::CBLSInformation::RandomSeedKey));
  
  ASSERT_TRUE(obj->hasProperty(Model::CBLSInformation::WarmStartKey));
  EXPECT_TRUE(getObjectPropertyValue<bool>(obj, Model::CBLSInformation::WarmStartKey));
  
  ASSERT_TRUE(obj->hasProperty(Model::CBLSInformation::NeighborhoodSizeKey));
  EXPECT_EQ(-1, getObjectPropertyValue<int>(obj, Model::CBLSInformation::NeighborhoodSizeKey));

  ASSERT_TRUE(obj->hasProperty(Model::CBLSInformation::NumNeighborhoodsKey));
  EXPECT_EQ(1, getObjectPropertyValue<int>(obj, Model::CBLSInformation::NumNeighborhoodsKey));
  
  ASSERT_TRUE(obj->hasProperty(Model::CBLSInformation::AggressiveCBLSKey));
  EXPECT_FALSE(getObjectPropertyValue<bool>(obj, Model::CBLSInformation::AggressiveCBLSKey));
  
  ASSERT_TRUE(obj->hasProperty(Model::GeneticInformation::PopulationSizeKey));
  EXPECT_EQ(100, getObjectPropertyValue<std::size_t>(obj,
                                                   Model::GeneticInformation::PopulationSizeKey));
  
  ASSERT_TRUE(obj->hasProperty(Model::GeneticInformation::NumGenerationsKey));
  EXPECT_EQ(500, getObjectPropertyValue<std::size_t>(obj,
                                                   Model::GeneticInformation::NumGenerationsKey));
  
  ASSERT_TRUE(obj->hasProperty(Model::GeneticInformation::MutationChanceKey));
  EXPECT_EQ(70, getObjectPropertyValue<std::size_t>(obj,
                                                   Model::GeneticInformation::MutationChanceKey));
  
  ASSERT_TRUE(obj->hasProperty(Model::GeneticInformation::MutationSizeKey));
  EXPECT_EQ(1, getObjectPropertyValue<std::size_t>(obj,
                                                   Model::GeneticInformation::MutationSizeKey));
}//geneticSearchOptions
