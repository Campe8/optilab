
#include "utest_globals.hpp"

#include "DataObject.hpp"
#include "DataObjectFcn.hpp"
#include "BaseObject.hpp"

namespace Interpreter {
  class DataObjectFcnTest : public DataObjectFcn {
  public:
    DataObjectFcnTest()
    : DataObjectFcn("TestFcn", DataObjectFcn::FcnType::FCN_INLINE)
    {
    }
    
    DataObjectFcn* clone() override { return nullptr; }
    
  };

}//end namespace Interpreter

OPTILAB_TEST(DataObject, isPrimitiveType)
{
  // Test for primitive type primitive types
  std::string testStr{"test"};
  ASSERT_TRUE(Interpreter::isPrimitiveType(Interpreter::DataObject(true)));
  ASSERT_TRUE(Interpreter::isPrimitiveType(Interpreter::DataObject(static_cast<int>(1))));
  ASSERT_TRUE(Interpreter::isPrimitiveType(Interpreter::DataObject(static_cast<std::size_t>(1))));
  ASSERT_TRUE(Interpreter::isPrimitiveType(Interpreter::DataObject(1.0)));
  ASSERT_TRUE(Interpreter::isPrimitiveType(Interpreter::DataObject(testStr.c_str())));
  
  // Test non primitive objects
  // VOID type
  ASSERT_TRUE(Interpreter::isVoidObjectType(Interpreter::DataObject()));
  ASSERT_FALSE(Interpreter::isPrimitiveType(Interpreter::DataObject()));
  
  // Composite type
  Interpreter::DataObject objInner(1);
  Interpreter::DataObject objComposite(1);
  objComposite.compose({objInner});
  ASSERT_FALSE(Interpreter::isPrimitiveType(objComposite));
  
  // Function type
  // @note the setFcnObject takes ownership of the function
  Interpreter::DataObject objFcn(1);
  auto fcn = new Interpreter::DataObjectFcnTest();
  objFcn.setFcnObject(fcn);
  ASSERT_FALSE(Interpreter::isPrimitiveType(objFcn));
  
  // Class object
  Interpreter::DataObject objClass(1);
  auto baseObj = std::shared_ptr<Interpreter::BaseObject>(Interpreter::newObject());
  objClass.setClassObject(baseObj);
  
  ASSERT_FALSE(Interpreter::isPrimitiveType(objClass));
}//isPrimitiveType

OPTILAB_TEST(DataObject, castDataValue)
{
  // Test cast data object value method
  Interpreter::DataObject doInt(static_cast<int>(1));
  Interpreter::DataObject doDouble(static_cast<double>(2.5));
  Interpreter::DataObject doBool(static_cast<bool>(true));
  Interpreter::DataObject doSizeT(static_cast<std::size_t>(100));
  Interpreter::DataObject doString("string");
  
  // Int to other types
  EXPECT_EQ(1, doInt.castDataValue<int>());
  EXPECT_EQ(true, doInt.castDataValue<bool>());
  EXPECT_EQ(1.0,  doInt.castDataValue<double>());
  EXPECT_EQ(1,  doInt.castDataValue<std::size_t>());
  EXPECT_EQ("1",  doInt.castDataValue<std::string>());
  
  // Double to other types
  EXPECT_EQ(2, doDouble.castDataValue<int>());
  EXPECT_EQ(true, doDouble.castDataValue<bool>());
  EXPECT_EQ(2.5,  doDouble.castDataValue<double>());
  EXPECT_EQ(2,  doDouble.castDataValue<std::size_t>());
  EXPECT_EQ("2.5",  doDouble.castDataValue<std::string>());
  
  // Bool to other types
  EXPECT_EQ(1, doBool.castDataValue<int>());
  EXPECT_EQ(true, doBool.castDataValue<bool>());
  EXPECT_EQ(1.0,  doBool.castDataValue<double>());
  EXPECT_EQ(1,  doBool.castDataValue<std::size_t>());
  EXPECT_EQ("true",  doBool.castDataValue<std::string>());
  
  // Size_t to other types
  EXPECT_EQ(100, doSizeT.castDataValue<int>());
  EXPECT_EQ(true, doSizeT.castDataValue<bool>());
  EXPECT_EQ(100.0,  doSizeT.castDataValue<double>());
  EXPECT_EQ(100,  doSizeT.castDataValue<std::size_t>());
  EXPECT_EQ("100",  doSizeT.castDataValue<std::string>());
  
  // String to other types
  EXPECT_THROW(doString.castDataValue<int>(), std::logic_error);
  EXPECT_THROW(doString.castDataValue<bool>(), std::logic_error);
  EXPECT_THROW(doString.castDataValue<double>(), std::logic_error);
  EXPECT_THROW(doString.castDataValue<std::size_t>(), std::logic_error);
  EXPECT_EQ("string",  doString.castDataValue<std::string>());
}//castDataValue

OPTILAB_TEST(DataObject, isAnyObject)
{
  // Test is any object
  EXPECT_EQ(std::string("_"), Interpreter::getAnyObjectData());
  
  auto anyObj = Interpreter::getAnyObject();
  EXPECT_TRUE(Interpreter::isAnyObjectType(anyObj));
  
  // Check any object
  EXPECT_TRUE(Interpreter::isPrimitiveType(anyObj));
  EXPECT_EQ(anyObj.dataObjectType(), Interpreter::DataObject::DataObjectType::DOT_STRING);
  EXPECT_EQ(std::string("_"), anyObj.getDataValue<std::string>());
}//isAnyObject
