
#include "utest_globals.hpp"

#include "BaseObject.hpp"
#include "ObjectTools.hpp"
#include "OPCode.hpp"

OPTILAB_TEST(Object, ModelCtxObject)
{
  // Test function to create model context objects
  using namespace Interpreter;
  using namespace Interpreter::ObjectTools;
  
  std::unique_ptr<BaseObject> ctxObj(createModelContextObject(MDL_OBJ_TYPE_DOM));
  ASSERT_TRUE(ctxObj->hasProperty(getObjectTypePropName()));
  ASSERT_TRUE(ctxObj->hasProperty(getObjectInterpretedPropName()));
  
  auto objType = ctxObj->getProperty(getObjectTypePropName());
  auto objInterp = ctxObj->getProperty(getObjectInterpretedPropName());
  ASSERT_EQ(DataObject::DataObjectType::DOT_INT, objType.dataObjectType());
  ASSERT_EQ(DataObject::DataObjectType::DOT_BOOL, objInterp.dataObjectType());
  ASSERT_EQ(MDL_OBJ_TYPE_DOM, objType.getDataValue<int>());
  ASSERT_TRUE(objInterp.getDataValue<bool>());
}//ModelCtxObject
