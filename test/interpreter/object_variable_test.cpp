
#include "utest_globals.hpp"

#include "BaseObject.hpp"
#include "ObjectTools.hpp"
#include "ObjectHelper.hpp"
#include "OPCode.hpp"

OPTILAB_TEST(ObjectVariable, VariableObject)
{
  // Test instantiation of a new object variable
  using namespace Interpreter;
  using namespace Interpreter::ObjectTools;
  
  std::unique_ptr<BaseObject> var(createObjectVariable("var", DOM_SINGLETON, { DataObject(5) }));
  ASSERT_TRUE((var->getProperty(getTypePropName())).getDataValue<std::string>() == "Variable");
  
  ASSERT_TRUE(var->hasProperty("inputOrder"));
  
  auto propID = var->getProperty("id");
  ASSERT_EQ(DataObject::DataObjectType::DOT_STRING, propID.dataObjectType());
  ASSERT_TRUE(propID.getDataValue<std::string>() == "var");
  
  auto propDomType = var->getProperty("domainType");
  ASSERT_EQ(DataObject::DataObjectType::DOT_INT, propDomType.dataObjectType());
  ASSERT_EQ(DOM_SINGLETON, propDomType.getDataValue<int>());
  
  auto propDom = var->getProperty("domain");
  ASSERT_TRUE(propDom.isClassObject() && isListObject(propDom.getClassObject().get()));
  
  auto listDomain = propDom.getClassObject().get();
  ASSERT_EQ(listDomain, getVarDomainList(var.get()));
  ASSERT_EQ(1, getListSize(listDomain));
  ASSERT_EQ(DataObject::DataObjectType::DOT_INT, getListValues(listDomain)[0].dataObjectType());
  ASSERT_EQ(5, getListValues(listDomain)[0].getDataValue<int>());
  
  auto propDomDims = var->getProperty("domainDimensions");
  ASSERT_TRUE(propDomDims.isClassObject() && isListObject(propDomDims.getClassObject().get()));
  
  auto domDimsList = propDomDims.getClassObject().get();
  ASSERT_EQ(domDimsList, getVarDomainDimensionsList(var.get()));
  ASSERT_EQ(0, getListSize(domDimsList));
  
  auto propDomArray = var->getProperty("domainArray");
  ASSERT_TRUE(propDomArray.isClassObject() && isListObject(propDomArray.getClassObject().get()));
  
  auto domArray = propDomArray.getClassObject().get();
  ASSERT_EQ(domArray, getVarDomainArrayList(var.get()));
  ASSERT_EQ(0, getListSize(domArray));
  
  auto propDomSubScript = var->getProperty("domainSubscript");
  ASSERT_TRUE(propDomSubScript.isClassObject() && isListObject(propDomSubScript.getClassObject().get()));
  
  auto domSubScript = propDomSubScript.getClassObject().get();
  ASSERT_EQ(domSubScript, getVarDomainSubscriptIndicesList(var.get()));
  ASSERT_EQ(0, getListSize(domSubScript));
  
  auto propOpt = var->getProperty("semantic");
  ASSERT_EQ(DataObject::DataObjectType::DOT_INT, propOpt.dataObjectType());
  ASSERT_EQ(VAR_SPEC_NONE, propOpt.getDataValue<int>());
}//VariableObject

OPTILAB_TEST(ObjectVariable, VariableObjectBoundDomain)
{
  // Test sorting of bound domains
  using namespace Interpreter;
  using namespace Interpreter::ObjectTools;
  
  std::unique_ptr<BaseObject> var1(createObjectVariable("var", DOM_BOUNDS, { DataObject(0), DataObject(1) }));
  auto domain1 = getListValues(getVarDomainList(var1.get()));
  ASSERT_TRUE(domain1.isComposite());
  ASSERT_EQ(2, domain1.composeSize());
  ASSERT_EQ(0, domain1[0].getDataValue<int>());
  ASSERT_EQ(1, domain1[1].getDataValue<int>());
  
  // Bound domain should be sorted
  std::unique_ptr<BaseObject> var2(createObjectVariable("var", DOM_BOUNDS, { DataObject(1), DataObject(0) }));
  auto domain2 = getListValues(getVarDomainList(var2.get()));
  ASSERT_TRUE(domain2.isComposite());
  ASSERT_EQ(2, domain2.composeSize());
  ASSERT_EQ(0, domain2[0].getDataValue<int>());
  ASSERT_EQ(1, domain2[1].getDataValue<int>());
}//VariableObjectBoundDomain

OPTILAB_TEST(Object, MatrixVariableObject)
{
  // Test instantiation of a new object variable matrix
  using namespace Interpreter;
  using namespace Interpreter::ObjectTools;
  
  BaseObjectSPtr dom1(createListObject({DataObject(2)}));
  DataObject do1;
  do1.setClassObject(dom1);
  std::unique_ptr<BaseObject> var(createObjectMatrixVariable("var", DOM_SINGLETON, { DataObject(5) }, { DataObject(2) }, { do1 }, VAR_SPEC_OPT_MIN));
  
  auto propID = var->getProperty("id");
  ASSERT_EQ(DataObject::DataObjectType::DOT_STRING, propID.dataObjectType());
  ASSERT_TRUE(propID.getDataValue<std::string>() == "var");
  
  auto propDomType = var->getProperty("domainType");
  ASSERT_EQ(DataObject::DataObjectType::DOT_INT, propDomType.dataObjectType());
  ASSERT_EQ(DOM_SINGLETON, propDomType.getDataValue<int>());
  
  auto propDom = getListValues(getVarDomainList(var.get()));
  ASSERT_TRUE(propDom.isComposite());
  ASSERT_EQ(1, propDom.composeSize());
  ASSERT_EQ(DataObject::DataObjectType::DOT_INT, propDom[0].dataObjectType());
  ASSERT_EQ(5, propDom[0].getDataValue<int>());
  
  auto propDomDims = getListValues(getVarDomainDimensionsList(var.get()));
  ASSERT_EQ(1, propDomDims.composeSize());
  ASSERT_EQ(DataObject::DataObjectType::DOT_INT, propDomDims[0].dataObjectType());
  ASSERT_EQ(2, propDomDims[0].getDataValue<int>());
  
  auto propDomArray = getListValues(getVarDomainArrayList(var.get()));
  ASSERT_EQ(1, propDomArray.composeSize());
  
  // The element is a domain, i.e., a list of DataObjects
  ASSERT_TRUE(propDomArray[0].isClassObject() && isListObject(propDomArray[0].getClassObject().get()));
  auto vals = getListValues(propDomArray[0].getClassObject().get());
  ASSERT_EQ(1, vals.composeSize());
  ASSERT_EQ(2, vals[0].getDataValue<int>());
  
  auto propDomSubscript = getListValues(getVarDomainSubscriptIndicesList(var.get()));
  ASSERT_EQ(0, propDomSubscript.composeSize());
  ASSERT_FALSE(isVarSubscript(var.get()));
  
  auto propOpt = var->getProperty("semantic");
  ASSERT_EQ(DataObject::DataObjectType::DOT_INT, propOpt.dataObjectType());
  ASSERT_EQ(VAR_SPEC_OPT_MIN, propOpt.getDataValue<int>());
}//MatrixVariableObject

OPTILAB_TEST(Object, MatrixVariableObjectTools)
{
  // Test tools on object variable matrix
  using namespace Interpreter;
  using namespace Interpreter::ObjectTools;
  
  BaseObjectSPtr dom1(createListObject({DataObject(2)}));
  DataObject do1;
  do1.setClassObject(dom1);
  std::unique_ptr<BaseObject> var(createObjectMatrixVariable("var", DOM_SINGLETON, { DataObject(5) }, { DataObject(1) }, { do1 }, VAR_SPEC_OPT_MIN));
  
  ASSERT_EQ(1, getVarDomainNumDimensions(var.get()));
  
  auto dimVals = getVarDomainDimensionsVals(var.get());
  ASSERT_EQ(1, dimVals.first);
  ASSERT_EQ(0, dimVals.second);
  ASSERT_EQ(1, getVarDomainDimensionsSize(var.get()));
}//MatrixVariableObject

OPTILAB_TEST(Object, MatrixVariableSubscript)
{
  // Test creation of a subscript variable
  using namespace Interpreter;
  using namespace Interpreter::ObjectTools;
  
  // Create a variable vector with singleton domain: x with domain [[2], [3]]
  BaseObjectSPtr dom1(createListObject({DataObject(2)}));
  BaseObjectSPtr dom2(createListObject({DataObject(3)}));
  DataObject do1, do2;
  do1.setClassObject(dom1);
  do2.setClassObject(dom2);
  std::unique_ptr<BaseObject> var(createObjectMatrixVariable("x", DOM_SINGLETON, { DataObject(5) }, { DataObject(2) }, { do1, do2 }, VAR_SPEC_OPT_MIN));
  
  // Dimensions number should be 1
  ASSERT_EQ(1, getVarDomainNumDimensions(var.get()));
  
  // Create a subscript variable x[1]
  DataObject indices;
  indices.compose({DataObject(1)});
  std::unique_ptr<BaseObject> varSubscr(createObjectMatrixVariableSubscr(var.get(), indices));
  
  // ID of the variable should be "x"
  ASSERT_TRUE("x" == getVarID(varSubscr.get()));
  ASSERT_TRUE(isVarSubscript(varSubscr.get()));
  
  auto idx = getVarDomainSubscriptIndices(varSubscr.get());
  ASSERT_EQ(1, getListSize(idx.getClassObject().get()));
  
  // Get the domain of the subscript variable, it should be [3]
  auto domSubscript = getVarDomainList(varSubscr.get());
  ASSERT_EQ(1, getListSize(domSubscript));
  ASSERT_EQ(3, getListElement(domSubscript, 0).getDataValue<int>());
  
  // Check that the domain dimensions are the same as the original matrix
  auto subscriptDims = varSubscr->getProperty("domainDimensions");
  ASSERT_TRUE(subscriptDims.isClassObject() && isListObject(subscriptDims.getClassObject().get()));
  ASSERT_EQ(1, getListSize(subscriptDims.getClassObject().get()));
  ASSERT_EQ(2, getListElement(subscriptDims.getClassObject().get(), 0).getDataValue<int>());
  
  // Domain array should be empty
  ASSERT_EQ(0, getListValues(getVarDomainArrayList(varSubscr.get())).composeSize());
}//MatrixVariableSubscript
