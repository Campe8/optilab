
#include "utest_globals.hpp"

#include "BaseObject.hpp"
#include "ObjectTools.hpp"
#include "ObjectHelper.hpp"
#include "OPCode.hpp"
#include "InterpreterDefs.hpp"

OPTILAB_TEST(ObjectList, ListObjectBase)
{
  using namespace Interpreter;
  using namespace Interpreter::ObjectTools;
  ASSERT_TRUE(getListClassName() == "List");
}//ListObjectBase

OPTILAB_TEST(ObjectList, isListObject)
{
  // Test isListObject function
  using namespace Interpreter;
  using namespace Interpreter::ObjectTools;
  
  std::unique_ptr<BaseObject> ctx(createModelContextObject(1));
  ASSERT_FALSE(isListObject(ctx.get()));
  
  std::unique_ptr<BaseObject> list(createListObject( {DataObject(1), DataObject(2), DataObject(3)} ));
  ASSERT_TRUE(isListObject(list.get()));
}//IsListObject

OPTILAB_TEST(ObjectList, getList)
{
  // Test getList function
  using namespace Interpreter;
  using namespace Interpreter::ObjectTools;
  
  std::unique_ptr<BaseObject> list(createListObject( {DataObject(1), DataObject(2), DataObject(3)} ));
  const auto& listFromFcn = getListValues(list.get());
  ASSERT_EQ(3, listFromFcn.composeSize());
  ASSERT_EQ(1, listFromFcn[0].getDataValue<int>());
  ASSERT_EQ(2, listFromFcn[1].getDataValue<int>());
  ASSERT_EQ(3, listFromFcn[2].getDataValue<int>());
}//getList

OPTILAB_TEST(ObjectList, getSize)
{
  // Test get/set size functions
  using namespace Interpreter;
  using namespace Interpreter::ObjectTools;
  
  std::unique_ptr<BaseObject> list(createListObject( {DataObject(1), DataObject(2), DataObject(3)} ));
  ASSERT_EQ(3, getListSize(list.get()));
}//getSize

OPTILAB_TEST(ObjectList, setSize)
{
  // Test get/set size functions
  using namespace Interpreter;
  using namespace Interpreter::ObjectTools;
  
  std::unique_ptr<BaseObject> list(createListObject( {DataObject(1), DataObject(2), DataObject(3)} ));
  setListSize(list.get(), 2);
  ASSERT_EQ(2, getListSize(list.get()));

  // Set larger size
  setListSize(list.get(), 4);
  ASSERT_EQ(4, getListSize(list.get()));
  
  // Get the list, the elements 2 and 3 should be void
  const auto& listFromFcn = getListValues(list.get());
  ASSERT_EQ(DataObject::DataObjectType::DOT_VOID, listFromFcn[2].dataObjectType());
  ASSERT_EQ(DataObject::DataObjectType::DOT_VOID, listFromFcn[3].dataObjectType());
}//setSize

OPTILAB_TEST(ObjectList, concatLists)
{
  // Test get/set size functions
  using namespace Interpreter;
  using namespace Interpreter::ObjectTools;
  
  std::unique_ptr<BaseObject> list1(createListObject( {DataObject(1), DataObject(2)} ));
  std::unique_ptr<BaseObject> list2(createListObject( {DataObject(3), DataObject(4)} ));
  std::unique_ptr<BaseObject> list(concatLists(list1.get(), list2.get()));
  
  ASSERT_EQ(4, getListSize(list.get()));

  // Get the concatenated values
  const auto& listVals = getListValues(list.get());
  ASSERT_EQ(1, listVals[0].getDataValue<int>());
  ASSERT_EQ(2, listVals[1].getDataValue<int>());
  ASSERT_EQ(3, listVals[2].getDataValue<int>());
  ASSERT_EQ(4, listVals[3].getDataValue<int>());
}//concatLists

OPTILAB_TEST(ObjectList, getSetListElement)
{
  // Test get/set list element function
  using namespace Interpreter;
  using namespace Interpreter::ObjectTools;
  
  std::unique_ptr<BaseObject> list(createListObject( {DataObject(1), DataObject(2)} ));
  
  DataObject elem;
  ASSERT_NO_THROW(elem = getListElement(list.get(), DataObject(0)));
  EXPECT_EQ(DataObject::DataObjectType::DOT_INT, elem.dataObjectType());
  EXPECT_EQ(1, elem.castDataValue<int>());
  
  ASSERT_NO_THROW(elem = getListElement(list.get(), DataObject(1)));
  EXPECT_EQ(DataObject::DataObjectType::DOT_INT, elem.dataObjectType());
  EXPECT_EQ(2, elem.castDataValue<int>());
  
  EXPECT_THROW(getListElement(list.get(), DataObject(2)), Interpreter::InterpreterException);
  
  const int elemVal = 10;
  setListElement(list.get(), DataObject(0), DataObject(elemVal));
  ASSERT_NO_THROW(elem = getListElement(list.get(), DataObject(0)));
  EXPECT_EQ(DataObject::DataObjectType::DOT_INT, elem.dataObjectType());
  EXPECT_EQ(elemVal, elem.castDataValue<int>());
}//getSetListElement
