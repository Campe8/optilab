#include "utest_inc.hpp"

bool checkEqStringNoWhiteChars(const std::string& aStr1, const std::string& aStr2)
{
  auto str1Cpy = aStr1;
  auto str2Cpy = aStr2;
  
  str1Cpy.erase(std::remove(str1Cpy.begin(), str1Cpy.end(), '\t'), str1Cpy.end());
  str1Cpy.erase(std::remove(str1Cpy.begin(), str1Cpy.end(), ' '), str1Cpy.end());
  
  str2Cpy.erase(std::remove(str2Cpy.begin(), str2Cpy.end(), '\t'), str2Cpy.end());
  str2Cpy.erase(std::remove(str2Cpy.begin(), str2Cpy.end(), ' '), str2Cpy.end());
  
  return str1Cpy == str2Cpy;
}//checkEqStringNoWhiteChars
