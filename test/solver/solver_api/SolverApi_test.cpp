
#include "utest_globals.hpp"

#include "SolverApi.hpp"
#include "SolverTestInc.hpp"
#include "StdOutCallbackHandler.hpp"

namespace {
  
  const char* kEngineId = "engineId";
  
}// namespace

class SolverApiFixture : public ::testing::Test {
protected:
  void SetUp() override
  {
    Solver::StdOutCallbackHandler::SPtr handler = std::make_shared<Solver::StdOutCallbackHandler>();
    pSolverApi.setEngineHandler(handler);
  }
  
  inline Solver::SolverApi& getApi() { return pSolverApi; }
  
private:
  Solver::SolverApi pSolverApi;
};

OPTILAB_TEST_F(SolverApiFixture, registerEngine)
{
  // Test registering an engine
  
  // Registering and engine without setting the engine handler should throw
  Solver::SolverApi api;
  Solver::SolverApi::SolverError err = Solver::SolverApi::SolverError::kNoError;
  ASSERT_NO_THROW(err = api.createEngine(kEngineId));
  EXPECT_EQ(Solver::SolverApi::SolverError::kError, err);
  
  // Registering an engine should not throw
  ASSERT_NO_THROW(err = getApi().createEngine(kEngineId));
  EXPECT_EQ(Solver::SolverApi::SolverError::kNoError, err);
  
  // Registering the same engine twice should not throw but return an error
  ASSERT_NO_THROW(err = getApi().createEngine(kEngineId));
  EXPECT_EQ(Solver::SolverApi::SolverError::kError, err);
  
  // Registering another engine with a different id should not throw
  const std::string engineId = "anotherId";
  ASSERT_NO_THROW(err = getApi().createEngine(engineId));
  EXPECT_EQ(Solver::SolverApi::SolverError::kNoError, err);
}// registerEngine

OPTILAB_TEST_F(SolverApiFixture, deleteEngine)
{
  // Test registering an engine
  
  // Registering and engine without setting the engine handler should throw
  Solver::SolverApi::SolverError err = Solver::SolverApi::SolverError::kNoError;
  ASSERT_NO_THROW(err = getApi().createEngine(kEngineId));
  EXPECT_EQ(Solver::SolverApi::SolverError::kNoError, err);
  
  // Delete the same detector should not throw
  ASSERT_NO_THROW(err = getApi().deleteEngine(kEngineId));
  EXPECT_EQ(Solver::SolverApi::SolverError::kNoError, err);
  
  // Deleting the same detector twice should not throw but return an error
  ASSERT_NO_THROW(err = getApi().deleteEngine(kEngineId));
  EXPECT_EQ(Solver::SolverApi::SolverError::kError, err);
}// deleteEngine
