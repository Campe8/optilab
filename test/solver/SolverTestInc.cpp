#include "SolverTestInc.hpp"

std::string getSimpleJSONModel() {
  static std::string jsonModel =
  R"(
  {
    "instance": {
      "name": "Test",
      "type": "CSP",
      "solution" :  -1,
      "variables": {
        "var": [
                {
                "id": "x",
                "branching": true,
                "input_order": 1,
                "type": "integer",
                "semantic": {
                "type": "decision"
                },
                "domain": {
                "type": "bounds",
                "values": [ 1, 2 ]
                }
                }
                ]
      },
      "constraints": {
        "con": [
        ]
      },
      "search": {
        "type": "dfs",
        "semantic": {
          "variable_choice": "input_order",
          "value_choice":    "indomain_min"
        }
      }
    }
  }
  )";
  
  return jsonModel;
}//getSimpleJSONModel

std::string getSimpleJSONModel2() {
  static std::string jsonModel =
  R"(
  {
    "instance": {
      "name": "Test",
      "type": "CSP",
      "solution" :  1,
      "variables": {
        "var": [
                {
                "id": "x",
                "branching": true,
                "input_order": 1,
                "type": "integer",
                "semantic": {
                "type": "decision"
                },
                "domain": {
                "type": "bounds",
                "values": [ 1, 2 ]
                }
                },
                {
                "id": "y",
                "type": "integer",
                "branching": false,
                "semantic": {
                "type": "objective",
                "extremum": "minimize"
                },
                "domain": {
                "type": "extensional",
                "values": [ 1, 2, 3 ]
                }
                },
                {
                "id": "z",
                "type": "boolean",
                "branching": false,
                "semantic": {
                "type": "support"
                },
                "domain": {
                "type": "bounds",
                "values": [ 1, 1 ]
                }
                }
                ]
      },
      "constraints": {
        "con": [
        ]
      },
      "search": {
        "type": "dfs",
        "semantic": {
          "variable_choice": "input_order",
          "value_choice":  "indomain_min"
        }
      }
    }
  }
  )";
  
  return jsonModel;
}//getSimpleJSONModel2

std::string getSimpleJSONModel3() {
  static std::string jsonModel =
  R"(
  {
    "instance": {
      "name": "Test",
      "type": "CSP",
      "solution" :  1,
      "variables": {
        "var": [
                {
                "id": "x",
                "branching": true,
                "input_order": 1,
                "type": "integer",
                "semantic": {
                "type": "decision"
                },
                "domain": {
                "type": "bounds",
                "values": [ 1, 2 ]
                }
                },
                {
                "id": "y",
                "type": "integer",
                "branching": true,
                "semantic": {
                "type": "decision"
                },
                "domain": {
                "type": "bounds",
                "values": [ 1, 1 ]
                }
                }
                ]
      },
      "constraints": {
        "con": [
        {
        "id": "nq",
        "type": "bounds",
        "scope_type": ["var_id", "var_id"],
        "scope": ["x", "y"]
        }
        ]
      },
      "search": {
        "type": "dfs",
        "semantic": {
          "variable_choice": "input_order",
          "value_choice":  "indomain_min"
        }
      }
    }
  }
  )";
  
  return jsonModel;
}//getSimpleJSONModel3

Core::VariableSPtr getIntVar(const std::string& aVarName, INT_64 aLb, INT_64 aUb)
{
  using namespace Core;
  
  VariableFactory varFactory;
  VariableSemanticUPtr semanticDecision(new VariableSemanticDecision(aVarName));
  
  return VariableSPtr(varFactory.variableInteger(aLb, aUb, std::move(semanticDecision)));
}//getIntVar
