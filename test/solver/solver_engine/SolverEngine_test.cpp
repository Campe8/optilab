
#include "utest_globals.hpp"

#include "SolverTestInc.hpp"

#include "SolverEngine.hpp"
#include "ModelGenerator.hpp"
#include "ModelParser.hpp"

#ifdef RUNTEST

OPTILAB_TEST(SolverEngine, defaultValues)
{
  using namespace Solver;
  using namespace Model;
  using namespace Core;
  
  SolverEngine se;
  ModelBuilder mb;
  ModelParser mp;
  
  // Read model in a string stream
  std::stringstream modelStream;
  modelStream << getSimpleJSONModel();

  auto ast = mp.parse(modelStream);
  auto model = mb
  auto modelKey = se.registerModel(model);
  
  // Solution without running the model should be nullptr
  auto sol = se.getSolution(modelKey);
  ASSERT_TRUE(sol == nullptr);
}//defaultValues

OPTILAB_TEST(SolverEngine, runModel)
{
  using namespace Solver;
  using namespace Model;
  using namespace Core;
  
  SolverEngine se;
  ModelBuilder mb;
  ModelParser mp;
  
  // Read model in a string stream
  std::stringstream modelStream;
  modelStream << getSimpleJSONModel();
  
  auto ast = mp.parse(modelStream);
  auto model = mb.generateModelFromAbstracSyntaxTree(ast.get());
  auto modelKey = se.registerModel(model);
  
  // Run model
  se.runModel(modelKey);
  
  // Solution without running the model should be nullptr
  auto sol = se.getSolution(modelKey);
  ASSERT_TRUE(sol != nullptr);
  
  // Solutions for model retrieved by "getSimpleJSONModel"
  ASSERT_EQ(sol->numSolutions(), 2);
  
  auto sol1 = sol->getSolution(0);
  auto sol2 = sol->getSolution(1);
  
  ASSERT_EQ(sol1.size(), 1);
  ASSERT_EQ(sol2.size(), 1);
  
  // See "getSimpleJSONModel"
  ASSERT_EQ(sol1[0].first->getVariableNameId(), "x");
  ASSERT_EQ(sol2[0].first->getVariableNameId(), "x");
  
  ASSERT_TRUE(sol1[0].second.first->isEqual(sol1[0].second.second));
  ASSERT_TRUE(sol2[0].second.first->isEqual(sol2[0].second.second));
  
  ASSERT_TRUE(sol1[0].second.first->isEqual(Core::DomainElementManager::getInstance().createDomainElementInt64(1)));
  ASSERT_TRUE(sol2[0].second.first->isEqual(Core::DomainElementManager::getInstance().createDomainElementInt64(2)));
}//runModel

#endif
