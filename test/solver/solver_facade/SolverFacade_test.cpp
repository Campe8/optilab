
#include "utest_globals.hpp"

#include "SolverFacade.hpp"

#include "SolverTestInc.hpp"

#ifdef RUNTEST

namespace Solver {
  
  class SolverFacadeTest : public SolverFacade {
  public:
    SolverEngine::ModelKey registerModel(std::istream& aInputStream)
    {
      using namespace Model;
      
      ModelParser modelParser;
      ModelFacade modelFacade;
      
      // Create model abstract syntax tree representing the model aInputStream
      ModelAbstractSyntaxTreeUPtr ast = modelParser.parse(aInputStream);
      
      // Generate a new model reflecting the abstract syntax tree
      ModelSPtr model = modelFacade.generateModelFromAbstracSyntaxTree(ast.get());
      assert(model);
      
      return SolverFacade::registerModel(model);
    }
  };
  
}

OPTILAB_TEST(SolverFacade, registerModel)
{
  using namespace Solver;
  using namespace Model;
  
  SolverFacadeTest sf;
  
  // Read model in a string stream
  std::stringstream modelStream;
  modelStream << getSimpleJSONModel();
  
  // Register model
  auto modelId = sf.registerModel(modelStream);
  
  ASSERT_EQ(sf.numSolutions(modelId), 0);
}//registerModel

OPTILAB_TEST(SolverFacade, runModel)
{
  using namespace Solver;
  using namespace Model;
  
  SolverFacadeTest sf;
  
  // Read model in a string stream
  std::stringstream modelStream;
  modelStream << getSimpleJSONModel();
  
  // Register model
  auto modelId = sf.registerModel(modelStream);
  
  sf.runModel(modelId);
  
  ASSERT_EQ(sf.numSolutions(modelId), 2);
  
  auto sol1 = sf.getSolution(modelId, 0);
  auto sol2 = sf.getSolution(modelId, 1);
  
  ASSERT_EQ(sol1.size(), 1);
  ASSERT_EQ(sol2.size(), 1);
  
  // See "getSimpleJSONModel"
  ASSERT_EQ(sol1[0].first->getVariableNameId(), "x");
  ASSERT_EQ(sol2[0].first->getVariableNameId(), "x");
  
  ASSERT_TRUE(sol1[0].second.first->isEqual(sol1[0].second.second));
  ASSERT_TRUE(sol2[0].second.first->isEqual(sol2[0].second.second));
  
  ASSERT_TRUE(sol1[0].second.first->isEqual(Core::DomainElementManager::getInstance().createDomainElementInt64(1)));
  ASSERT_TRUE(sol2[0].second.first->isEqual(Core::DomainElementManager::getInstance().createDomainElementInt64(2)));
}//runModel

OPTILAB_TEST(SolverFacade, registerMultipleModels)
{
  using namespace Solver;
  using namespace Model;
  
  SolverFacadeTest sf;
  
  // Read model in a string stream
  std::stringstream modelStream1;
  modelStream1 << getSimpleJSONModel();
  
  std::stringstream modelStream2;
  modelStream2 << getSimpleJSONModel();
  
  // Register models
  auto modelId1 = sf.registerModel(modelStream1);
  auto modelId2 = sf.registerModel(modelStream2);
  
  ASSERT_NE(modelId1, modelId2);
  
  sf.runModel(modelId1);
  sf.runModel(modelId2);
  
  ASSERT_EQ(sf.numSolutions(modelId1), 2);
  ASSERT_EQ(sf.numSolutions(modelId2), 2);
}//registerMultipleModels

#endif
