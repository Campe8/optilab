#pragma once

#include "SolverSolution.hpp"
#include "SolutionManager.hpp"

#include "VariableFactory.hpp"
#include "VariableInteger.hpp"
#include "VariableBoolean.hpp"
#include "VariableSemanticDecision.hpp"
#include "Model.hpp"

#include "DomainElementManager.hpp"
#include "DomainElementInt64.hpp"

#include <fstream>
#include <sstream>

/// Returns a simple JSON CSP model
/// Variables:
/// - x: [1, 2]
/// Constraints:
/// - none
/// Search:
/// - DFS, input_order, indomain_min
std::string getSimpleJSONModel();

/// Returns a simple JSON CSP model
/// Variables:
/// - x: [1, 2],    branching: true,  semantic: decision
/// - y: [1, 2, 3], branching: false, semantic: objective, extremumn: minimize
/// - z: [1, 1],    branching: false, semantic: support
/// Constraints:
/// - none
/// Search:
/// - DFS, input_order, indomain_min
std::string getSimpleJSONModel2();

/// Returns a simple JSON CSP model
/// Variables:
/// - x: [1, 2], branching: true, semantic: decision
/// - y: [1, 1], branching: true, semantic: decision
/// Constraints:
/// - nq(x, y),  prop_type: bounds
/// Search:
/// - DFS, input_order, indomain_min
std::string getSimpleJSONModel3();

/// Returns a new varible (decision semantic) with name "aVarName"
/// and domain [aLb..aUb]
Core::VariableSPtr getIntVar(const std::string& aVarName, INT_64 aLb, INT_64 aUb);
