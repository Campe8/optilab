
#include "utest_globals.hpp"

#include "SolverTestInc.hpp"

#include "OutputConverterSolverSolution.hpp"

#include <sstream>

#ifdef TESTALL

OPTILAB_TEST(SolverSolution, emptySolution)
{
  // Test for empty solutions
  
  using namespace Solver;
  using namespace Core;
  OutputConverterSolverSolution ocss;
  
  SolverSolution::Solution solution;
  
  // Empty solution is not an output solution
  ASSERT_FALSE(ocss.isOutputSolution(solution));
  
  std::stringstream ss;
  ocss.ostreamSolution(ss, solution);
  ASSERT_TRUE(ss.str().empty());
}//emptySolution

OPTILAB_TEST(SolverSolution, isOutputSolution)
{
  using namespace Solver;
  using namespace Core;
  OutputConverterSolverSolution ocss;
  
  SolverSolution::Solution solution;
  auto var1 = getIntVar("test1", 1, 1);
  var1->variableSemantic()->setInputOrder(0);
  var1->variableSemantic()->forceOutput(false);
  
  auto var2 = getIntVar("test2", 2, 2);
  var2->variableSemantic()->setInputOrder(1);
  var2->variableSemantic()->forceOutput(false);
  
  solution.push_back({ var1, { {var1->domain()->lowerBound(), var1->domain()->upperBound()} } });
  solution.push_back({ var2, { {var2->domain()->lowerBound(), var2->domain()->upperBound()} } });
  
  // Solution with no output variables is not an output solution
  ASSERT_FALSE(ocss.isOutputSolution(solution));
  
  // Force the output on one variable, it should return true
  // on querying if it is an output solution
  var2->variableSemantic()->forceOutput(true);
  ASSERT_TRUE(ocss.isOutputSolution(solution));
}//isOutputSolution

OPTILAB_TEST(SolverSolution, ostreamSolution)
{
  using namespace Solver;
  using namespace Core;
  OutputConverterSolverSolution ocss;
  
  SolverSolution::Solution solution;
  auto var1 = getIntVar("test1", 1, 1);
  var1->variableSemantic()->setInputOrder(0);
  var1->variableSemantic()->forceOutput(false);
  
  auto var2 = getIntVar("test2", 2, 2);
  var2->variableSemantic()->setInputOrder(1);
  var2->variableSemantic()->forceOutput(false);
  
  solution.push_back({ var1, { {var1->domain()->lowerBound(), var1->domain()->upperBound()} } });
  solution.push_back({ var2, { {var2->domain()->lowerBound(), var2->domain()->upperBound()} } });
  
  // Solution with no output variables should not push anything into the stream
  std::stringstream ss1;
  ocss.ostreamSolution(ss1, solution);
  ASSERT_TRUE(ss1.str().empty());
  
  // Force the output on one variable, it should return
  // a stream with with the output variable in it
  var2->variableSemantic()->forceOutput(true);
  
  std::stringstream ss2;
  ocss.ostreamSolution(ss2, solution);
  
  auto solStr = ss2.str();
  ASSERT_FALSE(solStr.empty());
  ASSERT_TRUE(solStr == "test2:\t2");
}//isOutputSolution
#endif
