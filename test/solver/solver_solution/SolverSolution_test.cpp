
#include "utest_globals.hpp"

#include "SolverTestInc.hpp"
#include "SearchHeuristicDefs.hpp"
#include "SearchEnvironment.hpp"
#include "Variable.hpp"
#include "VariableSemantic.hpp"

OPTILAB_TEST(SolverSolution, staticMethods)
{
  using namespace Solver;
  std::string varName{"test"};
  auto intVar = getIntVar(varName, 0, 10);
  SolverSolution::SolutionElement se = { intVar, { { intVar->domain()->lowerBound(), intVar->domain()->upperBound() } } };
  
  auto ID = SolverSolution::getVariableID(se);
  ASSERT_TRUE(ID == varName);
  
  auto varDom = SolverSolution::getVariableDomain(se);
  ASSERT_EQ(varDom.size(), 1);
  ASSERT_EQ(varDom[0].size(), 2);
  ASSERT_TRUE(varDom[0][0] == "0");
  ASSERT_TRUE(varDom[0][1] == "10");
}//staticMethods

OPTILAB_TEST(SolverSolution, defaultValues)
{
  using namespace Solver;
  using namespace Core;
  using namespace Search;
  
  std::string varName{"test"};
  SolutionManager::VarMapSPtr vm = std::make_shared<Model::ModelVarMap>();
  (*vm)[varName] = getIntVar(varName, 0, 10);
  
  // Create a solver solution on the ModelVarMap
  SolverSolution solSolution(vm);
  
  // Create a solution manager
  auto solManager = std::make_shared<SolutionManager>(vm);
  
  // Reads the solution on success.
  // Since the solution manager has not been notified about the solution
  // the solution cannot be read
  solSolution.readSolution(Search::SearchStatus::SEARCH_SUCCESS, solManager);
  ASSERT_EQ(solSolution.numSolutions(), 0);
}//defaultValues

OPTILAB_TEST(SolverSolution, getSolution)
{
  using namespace Solver;
  using namespace Core;
  using namespace Search;
  
  std::string varName{"test"};
  auto var = getIntVar(varName, 1, 1);
  var->variableSemantic()->setInputOrder(0);
  var->variableSemantic()->setBranchingPolicy(true);
  SolutionManager::VarMapSPtr vm = std::make_shared<Model::ModelVarMap>();
  (*vm)[varName] = var;
  
  // Create a solver solution on the ModelVarMap
  auto solSolution = std::make_shared<SolverSolution>(vm);
  
  // Create a solution manager, add the search environment and notify it about a solution
  auto solManager = std::make_shared<SolutionManager>(vm);
  
  SearchEnvironment::VarSem vs{ var, var->variableSemantic()->clone() };
  std::vector<SearchEnvironment::VarSem> vvs = { vs };
  auto searchEnv = std::make_shared<SearchEnvironment>(vvs,
                                                       VariableChoiceMetricType::VAR_CM_FIRST_FAIL,
                                                       ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MIN);
  solManager->setSearchEnvironment(searchEnv.get());
  solManager->notify();
  
  // Read the solution from the solution manager
  solSolution->readSolution(Search::SearchStatus::SEARCH_SUCCESS, solManager);
  ASSERT_EQ(solSolution->numSolutions(), 1);
  
  auto solution = solSolution->getSolution(0);
  ASSERT_EQ(solution.size(), 1);
  ASSERT_EQ(solution[0].first.get(), var.get());
  ASSERT_EQ(solution[0].first->getVariableNameId(), varName);
}//getSolution
