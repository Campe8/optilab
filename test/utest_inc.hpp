//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 06/25/2017
//
// Helper functions and methods for unit test.
//

#pragma once

#include <string>
#include <algorithm>

#define ASSERT_EQ_STR(X, Y) ASSERT_TRUE(checkEqStringNoWhiteChars((X), (Y)))

/// Returns true if "aStr1" is equal to "aStr2" modulo tabs and white spaces.
/// Returns false otherwise
bool checkEqStringNoWhiteChars(const std::string& aStr1, const std::string& aStr2);
