
#include "utest_globals.hpp"

#include "VariableFactory.hpp"
#include "DomainFactory.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"
#include "VariableChoiceMetricInc.hpp"
#include "ValueChoiceMetricInc.hpp"
#include "VariableSemanticDecision.hpp"
#include "BaseTools.hpp"

OPTILAB_TEST(ChoiceMetric, ValueChoiceMetricIndomainMax)
{
  using namespace Core;
  using namespace Search;
  ValueChoiceMetricIndomainMax vcm;
  
  DomainFactory df;
  DomainSPtr dom(df.domainInteger(0, 10));
  
  // Max value most promising
  auto domElem = vcm.getValue(dom.get());
  ASSERT_TRUE(domElem->isEqual(DomainElementManager::getInstance().createDomainElementInt64(10)));
}//ValueChoiceMetricIndomainMax

OPTILAB_TEST(ChoiceMetric, ValueChoiceMetricIndomainMin)
{
  using namespace Core;
  using namespace Search;
  ValueChoiceMetricIndomainMin vcm;
  
  DomainFactory df;
  DomainSPtr dom(df.domainInteger(0, 10));
  
  // Min value most promising
  auto domElem = vcm.getValue(dom.get());
  ASSERT_TRUE(domElem->isEqual(DomainElementManager::getInstance().createDomainElementInt64(0)));
}//ValueChoiceMetricIndomainMin

OPTILAB_TEST(ChoiceMetric, ValueChoiceMetricIndomainRandom)
{
  using namespace Core;
  using namespace Search;
  ValueChoiceMetricIndomainRandom vcm;
  
  DomainFactory df;
  DomainSPtr dom(df.domainInteger(0, 10));
  
  // Random value most promising
  auto domElem = vcm.getValue(dom.get());
  ASSERT_TRUE(dom->contains(domElem));
}//ValueChoiceMetricIndomainRandom

OPTILAB_TEST(ChoiceMetric, ValueChoiceMetricIndomainRandomWithSeed)
{
  // Test value choice metric random with fixed seed
  using namespace Core;
  using namespace Search;
  
  ValueChoiceMetricIndomainRandom vcm;
  auto rndGen = std::make_shared<Base::Tools::RandomGenerator>(0);
  vcm.setRandomGenerator(rndGen.get());
  
  DomainFactory df;
  DomainSPtr dom(df.domainInteger(0, 100));
  
  std::vector<DomainElement*> domElemVec;
  for (int idx{0}; idx < 10; ++idx)
  {
    domElemVec.push_back(vcm.getValue(dom.get()));
    ASSERT_TRUE(domElemVec.back());
    EXPECT_TRUE(dom->contains(domElemVec.back()));
  }
  
  // Reset a new random generator with same seed.
  // Check the the elements returned by the metric are the same
  rndGen = std::make_shared<Base::Tools::RandomGenerator>(0);
  vcm.setRandomGenerator(rndGen.get());
  for (int idx{0}; idx < 10; ++idx)
  {
    auto domVal = vcm.getValue(dom.get());
    ASSERT_TRUE(domVal);
    EXPECT_TRUE(domVal->isEqual(domElemVec[idx]));
  }
}//ValueChoiceMetricIndomainRandom

OPTILAB_TEST(ChoiceMetric, VariableChoiceMetricAntiFirstFail)
{
  using namespace Core;
  using namespace Search;
  VariableChoiceMetricAntiFirstFail vcm;
  
  VariableFactory vf;
  auto var1 = vf.variableInteger(0, 3, VariableSemanticUPtr(new VariableSemanticDecision()));
  auto var2 = vf.variableInteger(0, 4, VariableSemanticUPtr(new VariableSemanticDecision()));
  ASSERT_EQ(vcm.compare(var1, var2), VariableMetricEval::VAR_METRIC_LT);
  ASSERT_EQ(vcm.compare(var1, var1), VariableMetricEval::VAR_METRIC_EQ);
  ASSERT_EQ(vcm.compare(var2, var1), VariableMetricEval::VAR_METRIC_GT);
}//VariableChoiceMetricAntiFirstFail

OPTILAB_TEST(ChoiceMetric, VariableChoiceMetricFirstFail)
{
  using namespace Core;
  using namespace Search;
  VariableChoiceMetricFirstFail vcm;
  
  VariableFactory vf;
  auto var1 = vf.variableInteger(0, 3, VariableSemanticUPtr(new VariableSemanticDecision()));
  auto var2 = vf.variableInteger(0, 4, VariableSemanticUPtr(new VariableSemanticDecision()));
  ASSERT_EQ(vcm.compare(var1, var2), VariableMetricEval::VAR_METRIC_GT);
  ASSERT_EQ(vcm.compare(var1, var1), VariableMetricEval::VAR_METRIC_EQ);
  ASSERT_EQ(vcm.compare(var2, var1), VariableMetricEval::VAR_METRIC_LT);
}//VariableChoiceMetricFirstFail

OPTILAB_TEST(ChoiceMetric, VariableChoiceMetricLargest)
{
  using namespace Core;
  using namespace Search;
  VariableChoiceMetricLargest vcm;
  
  VariableFactory vf;
  auto var1 = vf.variableInteger(0, 50, VariableSemanticUPtr(new VariableSemanticDecision()));
  auto var2 = vf.variableInteger(49, 51, VariableSemanticUPtr(new VariableSemanticDecision()));
  ASSERT_EQ(vcm.compare(var1, var2), VariableMetricEval::VAR_METRIC_LT);
  ASSERT_EQ(vcm.compare(var1, var1), VariableMetricEval::VAR_METRIC_EQ);
  ASSERT_EQ(vcm.compare(var2, var1), VariableMetricEval::VAR_METRIC_GT);
}//VariableChoiceMetricLargest

OPTILAB_TEST(ChoiceMetric, VariableChoiceMetricSmallest)
{
  using namespace Core;
  using namespace Search;
  VariableChoiceMetricSmallest vcm;
  
  VariableFactory vf;
  auto var1 = vf.variableInteger(0, 50, VariableSemanticUPtr(new VariableSemanticDecision()));
  auto var2 = vf.variableInteger(49, 51, VariableSemanticUPtr(new VariableSemanticDecision()));
  ASSERT_EQ(vcm.compare(var1, var2), VariableMetricEval::VAR_METRIC_GT);
  ASSERT_EQ(vcm.compare(var1, var1), VariableMetricEval::VAR_METRIC_EQ);
  ASSERT_EQ(vcm.compare(var2, var1), VariableMetricEval::VAR_METRIC_LT);
}//VariableChoiceMetricSmallest

OPTILAB_TEST(ChoiceMetric, VariableChoiceMetricInputOrder)
{
  using namespace Core;
  using namespace Search;
  VariableChoiceMetricSmallest vcm;
  
  VariableFactory vf;
  auto sem1 = VariableSemanticUPtr(new VariableSemanticDecision());
  auto sem2 = VariableSemanticUPtr(new VariableSemanticDecision());
  
  sem1->setInputOrder(0);
  sem2->setInputOrder(1);
  
  auto var1 = vf.variableInteger(0, 50, std::move(sem1));
  auto var2 = vf.variableInteger(49, 51, std::move(sem2));
  ASSERT_EQ(vcm.compare(var1, var2), VariableMetricEval::VAR_METRIC_GT);
  ASSERT_EQ(vcm.compare(var1, var1), VariableMetricEval::VAR_METRIC_EQ);
  ASSERT_EQ(vcm.compare(var2, var1), VariableMetricEval::VAR_METRIC_LT);
}//VariableChoiceMetricInputOrder
