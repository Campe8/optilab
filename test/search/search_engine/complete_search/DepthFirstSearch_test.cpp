
#include "utest_globals.hpp"

#include "SearchEngine.hpp"
#include "SearchStrategy.hpp"
#include "DepthFirstSearch.hpp"
#include "Node.hpp"

#include "BranchingStrategyRegister.hpp"
#include "BranchingStrategyBinaryChoicePoint.hpp"

#include "SearchHeuristic.hpp"

#include "VariableFactory.hpp"
#include "VariableInteger.hpp"
#include "VariableSemanticDecision.hpp"
#include "VariableSemanticObjective.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementVoid.hpp"
#include "DomainElementManager.hpp"

#include "SearchMacro.hpp"
#include "SolutionManager.hpp"

namespace {
  
  using namespace Search;
  
  class DepthFirstSearchTest : public ::testing::Test {
  public:
    using IntPair = std::pair<INT_64, INT_64>;
    using IntPairList = std::vector<IntPair>;
    using ObjExtr = Core::VariableSemanticObjective::ObjectiveExtremum;
    using ObjExtrList = std::vector<ObjExtr>;
    
  public:
    DepthFirstSearchTest()  {};
    ~DepthFirstSearchTest() {};
    
    /// Sets up and returns a DFS on the variables identified by given domain bounds
    std::shared_ptr<SearchEngine> getDepthFirstSearch(const IntPairList& aBounds, const IntPairList& aBoundsOpt,
                                                      const ObjExtrList& aOptimizationExtremum,
                                                      bool aBranchOnObj=false)
    {
      using namespace Core;
      using namespace Search;
      using namespace Semantics;
      
      assert(aOptimizationExtremum.size() == aBoundsOpt.size());
      
      std::set<Core::VariableSPtr> brcVars;
      std::unique_ptr<VariableFactory> varFactory(new VariableFactory());
      
      pEnvironmentVars.clear();
      
      // Fill branching variable set
      std::string varName{"x"};
      std::size_t inputOrder{0};
      for(auto& bounds : aBounds)
      {
        std::string vname = varName;
        std::stringstream ss;
        ss << inputOrder;
        vname += ss.str();
        VariableSemanticUPtr varSem(new VariableSemanticDecision(vname));
        varSem->setInputOrder(inputOrder++);
        varSem->setBranchingPolicy(true);
        VariableSPtr var(varFactory->variableInteger(bounds.first, bounds.second, std::move(varSem)));
        
        pEnvironmentVars.push_back(var);
        brcVars.insert(var);
      }
      
      std::size_t extremumCtr{0};
      for(auto& bounds : aBoundsOpt)
      {
        VariableSemanticUPtr varOpt(new VariableSemanticObjective(aOptimizationExtremum[extremumCtr++]));
        varOpt->setBranchingPolicy(aBranchOnObj);
        
        varOpt->setInputOrder(inputOrder++);
        VariableSPtr var(varFactory->variableInteger(bounds.first, bounds.second, std::move(varOpt)));
        
        pOptimizationVars.push_back(var);
        brcVars.insert(var);
      }
      
      // Create the search environment
      std::vector<SearchEnvironment::VarSem> venv;
      for(auto& var : brcVars)
      {
        auto sem = VariableSemanticSPtr(var->variableSemantic()->clone());
        venv.push_back({var, sem});
      }
      auto env = new SearchEnvironment(venv, VariableChoiceMetricType::VAR_CM_INPUT_ORDER,
                                       ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MIN);
      std::vector<std::unique_ptr<SearchEnvironment>> searchEnv(1);
      searchEnv[0].reset(env);
      
      // Create ID var map
      auto idVarMap = std::make_shared<Model::ModelVarMap>();
      for(auto& var : pEnvironmentVars)
      {
        (*idVarMap)[var->getVariableNameId()] = var;
      }
      for(auto& var : pOptimizationVars)
      {
        (*idVarMap)[var->getVariableNameId()] = var;
      }
      
      // Create the search semantic
      std::unique_ptr<SearchSemantics> searchSem(new SearchSemantics());
      
      // Create new DFS
      pDFS = std::make_shared<DepthFirstSearch>(std::move(searchSem), searchEnv);
      
      auto solManager = std::make_shared<Search::SolutionManager>(idVarMap);
      pDFS->setSolutionManager(solManager);
      
      // Create search strategy wrapper around the base search
      auto ss = std::unique_ptr<Search::SearchStrategy>(new Search::SearchStrategy(pDFS));
      
      // Create search engine on the search strategy
      return std::unique_ptr<Search::SearchEngine>(new Search::SearchEngine(std::move(ss)));
    }// getDepthFirstSearch
    
    Core::VariableSPtr getDecVar(std::size_t aVarIdx)
    {
      if(aVarIdx < pEnvironmentVars.size())
      {
        return pEnvironmentVars[aVarIdx];
      }
      return nullptr;
    }
    
    Core::VariableSPtr getOptVar(std::size_t aVarIdx)
    {
      if(aVarIdx < pOptimizationVars.size())
      {
        return pOptimizationVars[aVarIdx];
      }
      return nullptr;
    }
    
    SolutionManager* getSolutionManager()
    {
      assert(pDFS);
      return pDFS->solutionManager();
    }
    
    CombinatorNode* getRootNode()
    {
      assert(pDFS);
      return pDFS->getRootNode(0);
    }
    
  private:
    std::shared_ptr<DepthFirstSearch> pDFS;
    
    /// Vector of decision variables
    std::vector<Core::VariableSPtr> pEnvironmentVars;
    
    /// Vector of optimization variables
    std::vector<Core::VariableSPtr> pOptimizationVars;
  };
  
}// end namespace

OPTILAB_TEST_F(DepthFirstSearchTest, Label0Var)
{
  // Labeling on empty environment: should return success
  using namespace Search;
  using namespace Core;
  
  auto dfs = getDepthFirstSearch({}, {}, {});
  
  // Set all solutions
  auto solutionManager = getSolutionManager();
  solutionManager->setSolutionLimit(SM_SOLUTION_LIMIT_NONE);
  
  // Run DFS should return SEARCH_SUCCESS since all solutions are found
  auto searchStatus = dfs->label(getRootNode());
  ASSERT_EQ(searchStatus, SearchStatus::SEARCH_SUCCESS);
  
  // Solution manager should have recorded 0 solutions
  ASSERT_EQ(solutionManager->getNumberOfRecordedSolutions(), 0);
}//Label0Var

OPTILAB_TEST_F(DepthFirstSearchTest, Label1Var)
{
  // Label 1 var: should explore entire domain
  
  using namespace Search;
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  
  auto dfs = getDepthFirstSearch({{1, 5}}, {}, {});
  
  // Set all solutions
  auto solutionManager = getSolutionManager();
  solutionManager->setSolutionLimit(-1);
  
  Core::VariableSPtr var1 = getDecVar(0);
  
  // Run DFS should return SEARCH_SUCCESS since all solutions are found
  auto searchStatus = dfs->label(getRootNode());
  ASSERT_EQ(searchStatus, SearchStatus::SEARCH_SUCCESS);
  
  // Solution manager should have recorded [1, 5] different solutions
  ASSERT_EQ(solutionManager->getNumberOfRecordedSolutions(), 5);
  
  // Verify solutions as combination of domain elements
  std::size_t solutionIdx{1};
  for(INT_64 valV1 = 1; valV1 < 6; ++valV1)
  {
    auto domainElementVar1 = elementManager.createDomainElementInt64(valV1);
    auto solution = solutionManager->getSolution(solutionIdx++);
    ASSERT_TRUE(solution.find(var1->getVariableNameId()) != solution.end());
    
    ASSERT_TRUE(solution[var1->getVariableNameId()][0].first->isEqual(domainElementVar1));
    ASSERT_TRUE(solution[var1->getVariableNameId()][0].second->isEqual(domainElementVar1));
  }
  
  // Domain of the variable must be as initialization
  auto domainVar = var1->domain();
  ASSERT_TRUE(domainVar->lowerBound()->isEqual(elementManager.createDomainElementInt64(1)));
  ASSERT_TRUE(domainVar->upperBound()->isEqual(elementManager.createDomainElementInt64(5)));
}//Label1Var

OPTILAB_TEST_F(DepthFirstSearchTest, Label1VarSingleton)
{
  // Label 1 singleton var: should explore 1 solution
  
  using namespace Search;
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  
  auto dfs = getDepthFirstSearch({{5, 5}}, {}, {});
  
  // Set all solutions
  auto solutionManager = getSolutionManager();
  solutionManager->setSolutionLimit(-1);
  
  Core::VariableSPtr var1 = getDecVar(0);
  
  // Run DFS should return SEARCH_SUCCESS since all solutions are found
  auto searchStatus = dfs->label(getRootNode());
  ASSERT_EQ(searchStatus, SearchStatus::SEARCH_SUCCESS);
  
  // Solution manager should have recorded [5, 5] different solutions
  ASSERT_EQ(solutionManager->getNumberOfRecordedSolutions(), 1);
  
  // Verify solutions as combination of domain elements
  std::size_t solutionIdx{1};
  for(INT_64 valV1 = 5; valV1 < 6; ++valV1)
  {
    auto domainElementVar1 = elementManager.createDomainElementInt64(valV1);
    SolutionManager::Solution solution = solutionManager->getSolution(solutionIdx++);
    ASSERT_TRUE(solution.find(var1->getVariableNameId()) != solution.end());
    
    ASSERT_TRUE(solution[var1->getVariableNameId()][0].first->isEqual(domainElementVar1));
    ASSERT_TRUE(solution[var1->getVariableNameId()][0].second->isEqual(domainElementVar1));
  }
  
  // Domain of the variable must be as initialization
  auto domainVar = var1->domain();
  ASSERT_TRUE(domainVar->lowerBound()->isEqual(elementManager.createDomainElementInt64(5)));
  ASSERT_TRUE(domainVar->upperBound()->isEqual(elementManager.createDomainElementInt64(5)));
}//Label1VarSingleton

OPTILAB_TEST_F(DepthFirstSearchTest, Label2Vars)
{
  // Label 2 var2: should explore cartesian product of domains
  
  using namespace Search;
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  
  auto dfs = getDepthFirstSearch({{1, 5}, {1, 3}}, {}, {});
  
  // Set all solutions
  auto solutionManager = getSolutionManager();
  solutionManager->setSolutionLimit(-1);
  
  Core::VariableSPtr var1 = getDecVar(0);
  Core::VariableSPtr var2 = getDecVar(1);
  
  // Run DFS should return SEARCH_SUCCESS since all solutions are found
  auto searchStatus = dfs->label(getRootNode());
  ASSERT_EQ(searchStatus, SearchStatus::SEARCH_SUCCESS);
  
  // Solution manager should have recorded [1, 5] x [1, 3] different solutions
  ASSERT_EQ(solutionManager->getNumberOfRecordedSolutions(), 5 * 3);
  
  // Verify solutions as combination of domain elements
  std::size_t solutionIdx{1};
  for(INT_64 valV1 = 1; valV1 < 6; ++valV1)
  {
    auto domainElementVar1 = elementManager.createDomainElementInt64(valV1);
    for(INT_64 valV2 = 1; valV2 < 4; ++valV2)
    {
      auto domainElementVar2 = elementManager.createDomainElementInt64(valV2);
      
      SolutionManager::Solution solution = solutionManager->getSolution(solutionIdx++);
      ASSERT_TRUE(solution.find(var1->getVariableNameId()) != solution.end());
      ASSERT_TRUE(solution.find(var2->getVariableNameId()) != solution.end());
      
      ASSERT_TRUE(solution[var1->getVariableNameId()][0].first->isEqual(domainElementVar1));
      ASSERT_TRUE(solution[var1->getVariableNameId()][0].second->isEqual(domainElementVar1));
      
      ASSERT_TRUE(solution[var2->getVariableNameId()][0].first->isEqual(domainElementVar2));
      ASSERT_TRUE(solution[var2->getVariableNameId()][0].second->isEqual(domainElementVar2));
    }
  }
  
  // Domains of the variable smust be as initialization
  auto domainVar1 = var1->domain();
  auto domainVar2 = var2->domain();
  ASSERT_TRUE(domainVar1->lowerBound()->isEqual(elementManager.createDomainElementInt64(1)));
  ASSERT_TRUE(domainVar1->upperBound()->isEqual(elementManager.createDomainElementInt64(5)));
  ASSERT_TRUE(domainVar2->lowerBound()->isEqual(elementManager.createDomainElementInt64(1)));
  ASSERT_TRUE(domainVar2->upperBound()->isEqual(elementManager.createDomainElementInt64(3)));
}//Label2Vars

OPTILAB_TEST_F(DepthFirstSearchTest, Label2VarsIncreasingSolutions)
{
  // Label 2 var2: should explore cartesian product of domains
  
  using namespace Search;
  using namespace Core;
  
  auto dfs = getDepthFirstSearch({{1, 5}, {1, 3}}, {}, {});
  
  // Set all solutions
  auto solutionManager = getSolutionManager();
  solutionManager->setSolutionLimit(-1);
  
  Core::VariableSPtr var1 = getDecVar(0);
  Core::VariableSPtr var2 = getDecVar(1);
  
  // Run DFS should return SEARCH_SUCCESS since all solutions are found
  auto searchStatus = dfs->label(getRootNode());
  ASSERT_EQ(searchStatus, SearchStatus::SEARCH_SUCCESS);
  
  // Solution manager should have recorded [1, 5] x [1, 3] different solutions
  ASSERT_EQ(solutionManager->getNumberOfRecordedSolutions(), 5 * 3);
  
  solutionManager->setSolutionLimit(0);
  searchStatus = dfs->label(getRootNode());
  ASSERT_EQ(searchStatus, SearchStatus::SEARCH_SUCCESS);
  ASSERT_EQ(solutionManager->getNumberOfRecordedSolutions(), 0);
  
  solutionManager->setSolutionLimit(3);
  searchStatus = dfs->label(getRootNode());
  ASSERT_EQ(searchStatus, SearchStatus::SEARCH_SUCCESS);
  ASSERT_EQ(solutionManager->getNumberOfRecordedSolutions(), 3);
}//Label2VarsIncreasingSolutions

OPTILAB_TEST_F(DepthFirstSearchTest, LabelOptVarsMinimize)
{
  // Label 2 var2: should explore cartesian product of domains
  
  using namespace Search;
  using namespace Core;
  
  auto dfs = getDepthFirstSearch(
  {{1, 3}}, // Decision var
  {{1, 5}}, // Optimization var
  {Core::VariableSemanticObjective::ObjectiveExtremum::OBJ_EXT_MINIMIZE});
  
  // Set all solutions
  auto solutionManager = getSolutionManager();

  auto searchStatus = dfs->label(getRootNode());
  ASSERT_EQ(searchStatus, SearchStatus::SEARCH_SUCCESS);
  
  /*
   * It should contain only 1 solution, i.e., <{1}, {1, 5}> since it is not
   * possible to further minimize for the objective variable {1, 5}.
   * @note the objective variables is not constrained and, by default,
   * it has not a branching semantic.
   * @note the decision variable is {1} and not {3} since the search engine finds
   * the first solution ({1}) and then constraints the objective variable to be 
   * better than {1, 5}, i.e., lower than {1} but this cannot lead to other
   * admissible solutions.
   */
  ASSERT_EQ(solutionManager->getNumberOfRecordedSolutions(), 1);
  
  SolutionManager::Solution solution = solutionManager->getSolution(1);
  ASSERT_TRUE(solution.find(getDecVar(0)->getVariableNameId()) != solution.end());
  ASSERT_TRUE(solution.find(getOptVar(0)->getVariableNameId()) != solution.end());
  
  ASSERT_TRUE(solution[getDecVar(0)->getVariableNameId()][0].first->isEqual(DomainElementManager::getInstance().createDomainElementInt64(1)));
  ASSERT_TRUE(solution[getDecVar(0)->getVariableNameId()][0].second->isEqual(DomainElementManager::getInstance().createDomainElementInt64(1)));
  
  ASSERT_TRUE(solution[getOptVar(0)->getVariableNameId()][0].first->isEqual(DomainElementManager::getInstance().createDomainElementInt64(1)));
  ASSERT_TRUE(solution[getOptVar(0)->getVariableNameId()][0].second->isEqual(DomainElementManager::getInstance().createDomainElementInt64(5)));
}//LabelOptVarsMinimize

OPTILAB_TEST_F(DepthFirstSearchTest, LabelOptVarsMaximizeNoBranchingOnOptVar)
{
  /*
   * Label 2 var2: should explore cartesian product of domains.
   * Use maximization variable objective.
   * Test optimization variable NO BRANCHING semantic.
   */
  
  using namespace Search;
  using namespace Core;
  
  auto dfs = getDepthFirstSearch({{1, 3}}, // Decision var
                                 {{1, 5}}, // Optimization var
                                 {Core::VariableSemanticObjective::ObjectiveExtremum::OBJ_EXT_MAXIMIZE});
  
  // Set all solutions
  auto solutionManager = getSolutionManager();
  solutionManager->setSolutionLimit(4);
  
  auto searchStatus = dfs->label(getRootNode());
  ASSERT_EQ(searchStatus, SearchStatus::SEARCH_SUCCESS);
  
  /*
   * It should contain 3 solution, i.e.,
   * <{1}, {1, 5}>
   * <{2}, {1, 5}>
   * <{3}, {1, 5}>
   * @note the objective variables is not constrained - reachable and,
   * by default, it doesn't have a branching semantic.
   */
  ASSERT_EQ(solutionManager->getNumberOfRecordedSolutions(), 3);
  
  SolutionManager::Solution solution = solutionManager->getSolution(1);
  ASSERT_TRUE(solution.find(getDecVar(0)->getVariableNameId()) != solution.end());
  ASSERT_TRUE(solution.find(getOptVar(0)->getVariableNameId()) != solution.end());
  
  ASSERT_TRUE(solution[getDecVar(0)->getVariableNameId()][0].first->isEqual(DomainElementManager::getInstance().createDomainElementInt64(1)));
  ASSERT_TRUE(solution[getDecVar(0)->getVariableNameId()][0].second->isEqual(DomainElementManager::getInstance().createDomainElementInt64(1)));
  
  ASSERT_TRUE(solution[getOptVar(0)->getVariableNameId()][0].first->isEqual(DomainElementManager::getInstance().createDomainElementInt64(1)));
  ASSERT_TRUE(solution[getOptVar(0)->getVariableNameId()][0].second->isEqual(DomainElementManager::getInstance().createDomainElementInt64(5)));
  
  solution = solutionManager->getSolution(2);
  ASSERT_TRUE(solution.find(getDecVar(0)->getVariableNameId()) != solution.end());
  ASSERT_TRUE(solution.find(getOptVar(0)->getVariableNameId()) != solution.end());
  
  ASSERT_TRUE(solution[getDecVar(0)->getVariableNameId()][0].first->isEqual(DomainElementManager::getInstance().createDomainElementInt64(2)));
  ASSERT_TRUE(solution[getDecVar(0)->getVariableNameId()][0].second->isEqual(DomainElementManager::getInstance().createDomainElementInt64(2)));
  
  ASSERT_TRUE(solution[getOptVar(0)->getVariableNameId()][0].first->isEqual(DomainElementManager::getInstance().createDomainElementInt64(1)));
  ASSERT_TRUE(solution[getOptVar(0)->getVariableNameId()][0].second->isEqual(DomainElementManager::getInstance().createDomainElementInt64(5)));
  
  solution = solutionManager->getSolution(3);
  ASSERT_TRUE(solution.find(getDecVar(0)->getVariableNameId()) != solution.end());
  ASSERT_TRUE(solution.find(getOptVar(0)->getVariableNameId()) != solution.end());
  
  ASSERT_TRUE(solution[getDecVar(0)->getVariableNameId()][0].first->isEqual(DomainElementManager::getInstance().createDomainElementInt64(3)));
  ASSERT_TRUE(solution[getDecVar(0)->getVariableNameId()][0].second->isEqual(DomainElementManager::getInstance().createDomainElementInt64(3)));
  
  ASSERT_TRUE(solution[getOptVar(0)->getVariableNameId()][0].first->isEqual(DomainElementManager::getInstance().createDomainElementInt64(1)));
  ASSERT_TRUE(solution[getOptVar(0)->getVariableNameId()][0].second->isEqual(DomainElementManager::getInstance().createDomainElementInt64(5)));
}//LabelOptVarsMaximizeNoBranchingOnOptVar

OPTILAB_TEST_F(DepthFirstSearchTest, LabelOptVarsMaximizeBranchingOnOptVar)
{
  /*
   * Label 2 var2: should explore cartesian product of domains.
   * Use maximization variable objective.
   * Test optimization variable BRANCHING semantic.
   */
  
  using namespace Search;
  using namespace Core;
  
  auto dfs = getDepthFirstSearch({{1, 3}}, // Decision var
                                 {{1, 5}}, // Optimization var
                                 {Core::VariableSemanticObjective::ObjectiveExtremum::OBJ_EXT_MAXIMIZE},
                                 true);
  ASSERT_TRUE(getOptVar(0)->variableSemantic()->isBranching());
  
  // Set all solutions
  auto solutionManager = getSolutionManager();
  solutionManager->setSolutionLimit(SM_SOLUTION_LIMIT_NONE);
  
  auto searchStatus = dfs->label(getRootNode());
  ASSERT_EQ(searchStatus, SearchStatus::SEARCH_SUCCESS);
  
  /*
   * It should contain 5 solution, i.e.,
   * <{1}, {1, 1}>
   * <{1}, {2, 2}>
   * <{1}, {3, 3}>
   * <{1}, {4, 4}>
   * <{1}, {5, 5}>
   */
  ASSERT_EQ(solutionManager->getNumberOfRecordedSolutions(), 5);
  
  SolutionManager::Solution solution = solutionManager->getSolution(1);
  ASSERT_TRUE(solution.find(getDecVar(0)->getVariableNameId()) != solution.end());
  ASSERT_TRUE(solution.find(getOptVar(0)->getVariableNameId()) != solution.end());
  
  ASSERT_TRUE(solution[getDecVar(0)->getVariableNameId()][0].first->isEqual(DomainElementManager::getInstance().createDomainElementInt64(1)));
  ASSERT_TRUE(solution[getDecVar(0)->getVariableNameId()][0].second->isEqual(DomainElementManager::getInstance().createDomainElementInt64(1)));
  
  ASSERT_TRUE(solution[getOptVar(0)->getVariableNameId()][0].first->isEqual(DomainElementManager::getInstance().createDomainElementInt64(1)));
  ASSERT_TRUE(solution[getOptVar(0)->getVariableNameId()][0].second->isEqual(DomainElementManager::getInstance().createDomainElementInt64(1)));
  
  solution = solutionManager->getSolution(2);
  ASSERT_TRUE(solution.find(getDecVar(0)->getVariableNameId()) != solution.end());
  ASSERT_TRUE(solution.find(getOptVar(0)->getVariableNameId()) != solution.end());
  
  ASSERT_TRUE(solution[getDecVar(0)->getVariableNameId()][0].first->isEqual(DomainElementManager::getInstance().createDomainElementInt64(1)));
  ASSERT_TRUE(solution[getDecVar(0)->getVariableNameId()][0].second->isEqual(DomainElementManager::getInstance().createDomainElementInt64(1)));
  
  ASSERT_TRUE(solution[getOptVar(0)->getVariableNameId()][0].first->isEqual(DomainElementManager::getInstance().createDomainElementInt64(2)));
  ASSERT_TRUE(solution[getOptVar(0)->getVariableNameId()][0].second->isEqual(DomainElementManager::getInstance().createDomainElementInt64(2)));
  
  solution = solutionManager->getSolution(3);
  ASSERT_TRUE(solution.find(getDecVar(0)->getVariableNameId()) != solution.end());
  ASSERT_TRUE(solution.find(getOptVar(0)->getVariableNameId()) != solution.end());
  
  ASSERT_TRUE(solution[getDecVar(0)->getVariableNameId()][0].first->isEqual(DomainElementManager::getInstance().createDomainElementInt64(1)));
  ASSERT_TRUE(solution[getDecVar(0)->getVariableNameId()][0].second->isEqual(DomainElementManager::getInstance().createDomainElementInt64(1)));
  
  ASSERT_TRUE(solution[getOptVar(0)->getVariableNameId()][0].first->isEqual(DomainElementManager::getInstance().createDomainElementInt64(3)));
  ASSERT_TRUE(solution[getOptVar(0)->getVariableNameId()][0].second->isEqual(DomainElementManager::getInstance().createDomainElementInt64(3)));
  
  solution = solutionManager->getSolution(4);
  ASSERT_TRUE(solution.find(getDecVar(0)->getVariableNameId()) != solution.end());
  ASSERT_TRUE(solution.find(getOptVar(0)->getVariableNameId()) != solution.end());
  
  ASSERT_TRUE(solution[getDecVar(0)->getVariableNameId()][0].first->isEqual(DomainElementManager::getInstance().createDomainElementInt64(1)));
  ASSERT_TRUE(solution[getDecVar(0)->getVariableNameId()][0].second->isEqual(DomainElementManager::getInstance().createDomainElementInt64(1)));
  
  ASSERT_TRUE(solution[getOptVar(0)->getVariableNameId()][0].first->isEqual(DomainElementManager::getInstance().createDomainElementInt64(4)));
  ASSERT_TRUE(solution[getOptVar(0)->getVariableNameId()][0].second->isEqual(DomainElementManager::getInstance().createDomainElementInt64(4)));
  
  solution = solutionManager->getSolution(5);
  ASSERT_TRUE(solution.find(getDecVar(0)->getVariableNameId()) != solution.end());
  ASSERT_TRUE(solution.find(getOptVar(0)->getVariableNameId()) != solution.end());
  
  ASSERT_TRUE(solution[getDecVar(0)->getVariableNameId()][0].first->isEqual(DomainElementManager::getInstance().createDomainElementInt64(1)));
  ASSERT_TRUE(solution[getDecVar(0)->getVariableNameId()][0].second->isEqual(DomainElementManager::getInstance().createDomainElementInt64(1)));
  
  ASSERT_TRUE(solution[getOptVar(0)->getVariableNameId()][0].first->isEqual(DomainElementManager::getInstance().createDomainElementInt64(5)));
  ASSERT_TRUE(solution[getOptVar(0)->getVariableNameId()][0].second->isEqual(DomainElementManager::getInstance().createDomainElementInt64(5)));
}//LabelOptVarsMaximizeBranchingOnOptVar
