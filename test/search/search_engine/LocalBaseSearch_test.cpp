
#include "utest_globals.hpp"

#include "SearchEngineTestInc.hpp"
#include "MetricsRegister.hpp"

namespace Search {

  class LocalBaseSearchMock: public LocalBaseSearch {
  public:
    LocalBaseSearchMock(SemanticPtr aSemantics,
                        EnvironmentList& aEnvironmentList,
                        SearchEngineStrategyType aSearchEngineType,
                        int aSearchOption)
    : LocalBaseSearch(std::move(aSemantics), aEnvironmentList, aSearchEngineType)
    , pSearchOption(aSearchOption)
    {
    }
    
    // === BaseSearchScheme protected methods === //
    
    inline SearchSemantics* searchSemantics() const { return LocalBaseSearch::searchSemantics(); }
    
    inline SearchEnvironment* searchEnvironment() const { return LocalBaseSearch::searchEnvironment(); }
    
    inline void setGlobalSearchStatus(SearchStatus aStatus) { LocalBaseSearch::setGlobalSearchStatus(aStatus); }
    
    inline SearchStatus getGlobalSearchStatus() const { return LocalBaseSearch::getGlobalSearchStatus(); }
    
    inline std::size_t getActiveEnvironmentIndex() const {  return LocalBaseSearch::getActiveEnvironmentIndex(); }
    
    inline std::size_t getNumEnvironments() const {  return LocalBaseSearch::getNumEnvironments(); }
    
    std::shared_ptr<Base::Tools::MetricsRegister> getMetrics() override
    {
      return std::make_shared<Base::Tools::MetricsRegister>();
    }
    
    // === LocalBaseSearch protected methods === //
    
    SearchStatus exploreNeighborhood(Neighborhood* aRootNeighborhood)
    {
      return LocalBaseSearch::exploreNeighborhood(aRootNeighborhood);
    }
    
    void setupNeighborhoodSearch() override
    {
      LocalBaseSearch::setupNeighborhoodSearch();
    }
    
    void resetNeighborhoodSearchState(bool) override
    {
      // Do nothing
    }
    
    void cleanupNeighborhoodSearch() override
    {
      LocalBaseSearch::cleanupNeighborhoodSearch();
    }
    
    NeighborhoodCost getNeighborhoodCost(Neighborhood* aNeighborhood) override
    {
      return LocalBaseSearch::getNeighborhoodCost(aNeighborhood);
    }
    
    CombinatorNode* createRootCombinatorNode(SearchEnvironment* aEnv, SearchSemantics* aSem) override
    {
      return LocalBaseSearch::createRootCombinatorNode(aEnv, aSem);
    }
    
    bool isSearchAtGlobalMinimum() const
    {
      return LocalBaseSearch::isSearchAtGlobalMinimum();
    }
    
    void resetBestNeighborhoodCost()
    {
      LocalBaseSearch::resetBestNeighborhoodCost();
    }

    inline int getNeighborhoodSize() const { return LocalBaseSearch::getNeighborhoodSize(); };
    
    void updateBestNeighborhoodCost(NeighborhoodCost aCost, Neighborhood* aNeighborhood)
    {
      LocalBaseSearch::updateBestNeighborhoodCost(aCost, aNeighborhood);
    }
    
    SearchStatus runNeighborhoodSearch(Neighborhood* aRootNeighborhood)
    {
      return neighborhoodSearch(aRootNeighborhood);
    }//runNeighborhoodSearch
    
  protected:
    void initLocalSearchEnvironment(LocalSearchEnvironment* aEnv) override {}
    
    SearchStatus neighborhoodSearch(Neighborhood* aRootNeighborhood) override
    {
      switch (pSearchOption) {
        case 0:
          return SearchStatus::SEARCH_FAIL;
        case 1:
        {
          // Label all the variables in the neighborhood
          // with their lower bound
          assert(aRootNeighborhood);
          const auto& neighborhood = aRootNeighborhood->getNeighborhood();
          for (auto& var : neighborhood)
          {
            if (var->isAssigned()) return SearchStatus::SEARCH_FAIL;
            auto val = var->domain()->lowerBound();
            var->domain()->shrink(val, val);
          }
          solutionManager()->notify();
          break;
        }
        case 2:
        {
          // Label all the variables in the neighborhood
          // with their lower bound and then call restoreNeighborhood()
          // to reset to their default values
          assert(aRootNeighborhood);
          const auto& neighborhood = aRootNeighborhood->getNeighborhood();
          for (auto& var : neighborhood)
          {
            if (var->isAssigned()) return SearchStatus::SEARCH_FAIL;
            auto val = var->domain()->lowerBound();
            var->domain()->shrink(val, val);
            
            if (!var->isAssigned()) return SearchStatus::SEARCH_FAIL;
          }
          solutionManager()->notify();
          restoreNeighborhood();
          
          for (auto& var : neighborhood)
          {
            if (var->isAssigned()) return SearchStatus::SEARCH_FAIL;
          }
          
          // Multiple calls of restoreNeighborhood shouldn't
          // change the current state
          restoreNeighborhood();
          restoreNeighborhood();
          
          break;
        }
        case 3:
        {
          // Interleave multiple labelings and restores
          assert(aRootNeighborhood);
          const auto& neighborhood = aRootNeighborhood->getNeighborhood();
          for (auto& var : neighborhood)
          {
            if(var->isAssigned()) return SearchStatus::SEARCH_FAIL;
            auto val = var->domain()->lowerBound();
            var->domain()->shrink(val, val);
            
            if (!var->isAssigned()) return SearchStatus::SEARCH_FAIL;
          }
          solutionManager()->notify();
          restoreNeighborhood();
          
          for (auto& var : neighborhood)
          {
            if (var->isAssigned()) return SearchStatus::SEARCH_FAIL;
          }
          
          for (auto& var : neighborhood)
          {
            if (var->isAssigned()) return SearchStatus::SEARCH_FAIL;
            auto val = var->domain()->lowerBound();
            var->domain()->shrink(val, val);
            
            if (!var->isAssigned()) return SearchStatus::SEARCH_FAIL;
          }
          solutionManager()->notify();
          restoreNeighborhood();
          
          for (auto& var : neighborhood)
          {
            if (var->isAssigned()) return SearchStatus::SEARCH_FAIL;
          }
          
          break;
        }
        default:
          return SearchStatus::SEARCH_SUCCESS;
      }
      return SearchStatus::SEARCH_SUCCESS;
    }
  
  private:
    /// Search option to be used
    int pSearchOption = 0;
  };
  
}//end namespace Search

namespace {
  
  using namespace Search;
  
  class LocalBaseSearchTest : public ::testing::Test {
  public:
    using IntPair = std::pair<INT_64, INT_64>;
    using IntPairList = std::vector<IntPair>;
    using ObjExtr = Core::VariableSemanticObjective::ObjectiveExtremum;
    using ObjExtrList = std::vector<ObjExtr>;
    
  public:
    LocalBaseSearchTest()  {};
    ~LocalBaseSearchTest() {};
    
    std::shared_ptr<LocalBaseSearchMock> getLocalBaseSearch(const IntPairList& aBounds,
                                                            std::size_t aSizeNeighborhoods,
                                                            std::size_t aNumNeighborhoods,
                                                            int aSearchOption=0)
    {
      using namespace Core;
      using namespace Search;
      using namespace Semantics;
      
      std::set<Core::VariableSPtr> brcVars;
      std::unique_ptr<VariableFactory> varFactory(new VariableFactory());
      
      pEnvironmentVars.clear();
      
      // Fill branching variable set
      std::string varName{"V"};
      for(std::size_t idx{0}; idx < aBounds.size(); ++idx)
      {
        std::string id = varName + std::to_string(idx);
        VariableSemanticUPtr varSem(new VariableSemanticDecision(id));
        varSem->setInputOrder(idx);
        varSem->setBranchingPolicy(true);
        
        // Create variable
        VariableSPtr var(varFactory->variableInteger(aBounds[idx].first, aBounds[idx].second, std::move(varSem)));
        
        pEnvironmentVars.push_back(var);
        brcVars.insert(var);
      }
      
      // Create the search environment
      std::vector<SearchEnvironment::VarSem> venv;
      for(auto& var : brcVars)
      {
        auto sem = VariableSemanticSPtr(var->variableSemantic()->clone());
        venv.push_back({var, sem});
      }
      
      // Create an environment with an random value heuristic
      auto lsEnvironment = new LocalSearchEnvironmentMock(venv,
                                                          VariableChoiceMetricType::VAR_CM_INPUT_ORDER,
                                                          ValueChoiceMetricType::VAL_METRIC_INDOMAIN_RANDOM,
                                                          aSizeNeighborhoods,
                                                          aNumNeighborhoods,
                                                          false,
                                                          SearchEngineStrategyType::SE_GENETIC);
      // Set random seed to 0 to reproduce behavior in testing
      lsEnvironment->setRandomSeed(0);
      
      // Set warm start always to true
      lsEnvironment->setWarmStart(true);
      
      std::vector<std::unique_ptr<SearchEnvironment>> environmentList(1);
      environmentList[0].reset(lsEnvironment);
      
      // Create ID var map
      auto idVarMap = std::make_shared<Model::ModelVarMap>();
      for(auto& var : pEnvironmentVars)
      {
        (*idVarMap)[var->getVariableNameId()] = var;
      }
      
      // Create the search semantic
      std::unique_ptr<SearchSemantics> semantics(new SearchSemantics());
      
      // Create new DFS
      pLocalBaseSearch = std::make_shared<LocalBaseSearchMock>(std::move(semantics),
                                                               environmentList,
                                                               SearchEngineStrategyType::SE_GENETIC,
                                                               aSearchOption);
      
      pSolutionManager = std::make_shared<Search::SolutionManager>(idVarMap);
      pLocalBaseSearch->setSolutionManager(pSolutionManager);
      
      return pLocalBaseSearch;
    }// getLocalBaseSearch
    
    inline const std::vector<Core::VariableSPtr>& getSearchSpace() const { return pEnvironmentVars; }
    
    inline std::shared_ptr<Search::SolutionManager> getSolutionManager() const
    {
      return pSolutionManager;
    }
    
    SolutionManager* getSolutionManager()
    {
      assert(pLocalBaseSearch);
      return pLocalBaseSearch->solutionManager();
    }
    
    CombinatorNode* getRootNode()
    {
      assert(pLocalBaseSearch);
      return pLocalBaseSearch->getRootNode(0);
    }
    
  private:
    std::shared_ptr<LocalBaseSearchMock> pLocalBaseSearch;
    
    std::shared_ptr<Search::SolutionManager> pSolutionManager;
    
    /// Vector of decision variables
    std::vector<Core::VariableSPtr> pEnvironmentVars;
    
    /// Vector of optimization variables
    std::vector<Core::VariableSPtr> pOptimizationVars;
  };
  
}// end namespace

OPTILAB_TEST_F(LocalBaseSearchTest, baseSearchScheme)
{
  // Test base search scheme methods
  
  using namespace Search;
  using namespace Core;
  
  auto lbs = getLocalBaseSearch({{1, 10}, {1, 10}, {1, 10}, {1, 10}}, 2, 1);
  
  // The size of the neighborhood is -1 default
  ASSERT_EQ(-1, lbs->getNeighborhoodSize());
  
  // Global status is UNDEF upon initialization
  ASSERT_EQ(SearchStatus::SEARCH_UNDEF, lbs->getGlobalSearchStatus());
  
  // Set new status
  lbs->setGlobalSearchStatus(Search::SearchStatus::SEARCH_SUCCESS);
  ASSERT_EQ(SearchStatus::SEARCH_SUCCESS, lbs->getGlobalSearchStatus());
  
  // Envirotnment index is the size of the environment list upon initialization
  ASSERT_EQ(1, lbs->getActiveEnvironmentIndex());
  ASSERT_EQ(1, lbs->getNumEnvironments());
  
  // The number of root nodes corresponds to the size of the environment list
  ASSERT_EQ(1, lbs->getNumRootNodes());
  
  // Get and invalid root node should return nullptr
  ASSERT_FALSE(lbs->getRootNode(10));
}//baseSearchScheme

OPTILAB_TEST_F(LocalBaseSearchTest, defaultValuesAndCost)
{
  // Test default values on initialization and methods for changing
  // the cost of a neighborhood
  
  using namespace Search;
  using namespace Core;
  
  auto lbs = getLocalBaseSearch({{1, 10}, {1, 10}, {1, 10}, {1, 10}}, 2, 1);
  
  // Set all solutions
  auto solutionManager = getSolutionManager();
  solutionManager->setSolutionLimit(SM_SOLUTION_LIMIT_NONE);
  
  // Best cost is max cost
  ASSERT_EQ(std::numeric_limits<LocalBaseSearch::NeighborhoodCost>::max(), LocalBaseSearch::getMaximumCostValue());
  ASSERT_EQ(LocalBaseSearch::getMaximumCostValue(), lbs->getBestNeighborhoodCost());
  ASSERT_FALSE(lbs->isSearchAtGlobalMinimum());
  
  // Neighborhood
  auto rootNode = lbs->getRootNode(0);
  auto neighborhood = rootNode->searchNeighborhood();
  
  // Update it with new cost
  lbs->updateBestNeighborhoodCost(10, neighborhood);
  ASSERT_EQ(10, lbs->getBestNeighborhoodCost());
  
  // New update with worsening cost shouldn't update the best cost
  lbs->updateBestNeighborhoodCost(20, neighborhood);
  ASSERT_EQ(10, lbs->getBestNeighborhoodCost());
  ASSERT_FALSE(lbs->isSearchAtGlobalMinimum());
  
  // Cost of zero represents the global minimum
  auto domainElem = Core::DomainElementManager::getInstance().createDomainElementInt64(1);
  for(auto& var : getSearchSpace()) var->domain()->shrink(domainElem, domainElem);
  
  lbs->updateBestNeighborhoodCost(LocalBaseSearch::getGlobalMinimumCostValue(), neighborhood);
  ASSERT_TRUE(lbs->isSearchAtGlobalMinimum());
  
  // Reset the cost
  lbs->resetBestNeighborhoodCost();
  ASSERT_EQ(std::numeric_limits<LocalBaseSearch::NeighborhoodCost>::max(), lbs->getBestNeighborhoodCost());
  ASSERT_FALSE(lbs->isSearchAtGlobalMinimum());
}//defaultValuesAndCost

OPTILAB_TEST_F(LocalBaseSearchTest, getRootNode)
{
  // Test get root neighborhood combinator node
  using namespace Search;
  using namespace Core;
  
  auto lbs = getLocalBaseSearch({{1, 10}, {1, 10}, {1, 10}, {1, 10}}, 2, 1);
  
  // There should be 1 root node
  ASSERT_EQ(1, lbs->getNumRootNodes());
  
  // Get the root node
  auto rootNode = lbs->getRootNode(0);
  ASSERT_TRUE(rootNode);
  ASSERT_TRUE(Neighborhood::isa(rootNode->searchNeighborhood()));
  ASSERT_EQ(SearchStatus::SEARCH_UNDEF, rootNode->getStatus());
}//getRootNode

OPTILAB_TEST_F(LocalBaseSearchTest, startSearchEmptyEnvironment)
{
  // Test base search scheme methods with an empty environment
  
  using namespace Search;
  using namespace Core;
  
  auto lbs = getLocalBaseSearch({}, 0, 1);
  
  // Get the root node
  auto rootNode = lbs->getRootNode(0);
  lbs->start(rootNode);
  
  // Neighborhood size should be 0
  ASSERT_EQ(0, lbs->getNeighborhoodSize());
  
  // The status should be success on empty environment
  ASSERT_EQ(SearchStatus::SEARCH_SUCCESS, lbs->getGlobalSearchStatus());
}//baseSearchEmptyEnvironment

OPTILAB_TEST_F(LocalBaseSearchTest, startSearch1LN)
{
  // Test start local base search on root node with one large neighborhoods
  using namespace Search;
  using namespace Core;
  
  auto lbs = getLocalBaseSearch({{1, 10}, {1, 10}, {1, 10}, {1, 10}}, 2, 1, 1);
  getSolutionManager()->setSolutionLimit(1);
  
  const auto& searchSpace = getSearchSpace();
  
  // Add constraints to the constraint store
  // V0 == v1
  Core::ConstraintEqTest eq1(searchSpace[0], searchSpace[1]);
  
  // V2 == V3
  Core::ConstraintEqTest eq2(searchSpace[2], searchSpace[3]);
  
  // Register constraints into the constraint store
  eq1.getConstraint()->subscribeToConstraintStore(lbs->searchSemantics()->constraintStore());
  eq2.getConstraint()->subscribeToConstraintStore(lbs->searchSemantics()->constraintStore());
  
  // Get the root node
  auto rootNode = lbs->getRootNode(0);
  lbs->start(rootNode);
  
  // Neighborhood size should be 2
  ASSERT_EQ(2, lbs->getNeighborhoodSize());
  
  ASSERT_EQ(SearchStatus::SEARCH_SUCCESS, lbs->getGlobalSearchStatus());
}//startSearch1LN

OPTILAB_TEST_F(LocalBaseSearchTest, startSearch2LN)
{
  // Test start local base search on root node with two large neighborhoods
  using namespace Search;
  using namespace Core;
  
  auto lbs = getLocalBaseSearch({{1, 10}, {1, 10}, {1, 10}, {1, 10}}, 2, 2, 1);
  getSolutionManager()->setSolutionLimit(1);
  
  const auto& searchSpace = getSearchSpace();
  
  // Add constraints to constraint store
  // V0 == v1
  Core::ConstraintEqTest eq1(searchSpace[0], searchSpace[1]);
  
  // V2 == V3
  Core::ConstraintEqTest eq2(searchSpace[2], searchSpace[3]);
  
  // Register constraints into the constraint store
  eq1.getConstraint()->subscribeToConstraintStore(lbs->searchSemantics()->constraintStore());
  eq2.getConstraint()->subscribeToConstraintStore(lbs->searchSemantics()->constraintStore());

  // Get the root node
  auto rootNode = lbs->getRootNode(0);
  lbs->start(rootNode);
  
  ASSERT_EQ(SearchStatus::SEARCH_SUCCESS, lbs->getGlobalSearchStatus());
}//startSearch2LN

OPTILAB_TEST_F(LocalBaseSearchTest, startSearchFullLN)
{
  // Test start local base search on root node with one large neighborhood
  // that covers the entire search space
  using namespace Search;
  using namespace Core;
  
  auto lbs = getLocalBaseSearch({{1, 10}, {1, 10}, {1, 10}, {1, 10}}, 4, 1, 1);
  getSolutionManager()->setSolutionLimit(1);
  
  const auto& searchSpace = getSearchSpace();
  
  // Add constraints to constraint store
  // V0 == v1
  Core::ConstraintEqTest eq1(searchSpace[0], searchSpace[1]);
  
  // V2 == V3
  Core::ConstraintEqTest eq2(searchSpace[2], searchSpace[3]);
  
  // Register constraints into the constraint store
  eq1.getConstraint()->subscribeToConstraintStore(lbs->searchSemantics()->constraintStore());
  eq2.getConstraint()->subscribeToConstraintStore(lbs->searchSemantics()->constraintStore());
  
  // Get the root node
  auto rootNode = lbs->getRootNode(0);
  lbs->start(rootNode);
  
  ASSERT_EQ(SearchStatus::SEARCH_SUCCESS, lbs->getGlobalSearchStatus());
  
  // Call exit method.
  // It should reset the internal search node and set the status from the search
  lbs->exit(rootNode);
  ASSERT_FALSE(rootNode->searchNeighborhood());
  ASSERT_EQ(SearchStatus::SEARCH_SUCCESS, rootNode->getStatus());
}//startSearchFullLN

OPTILAB_TEST_F(LocalBaseSearchTest, exitNoStartSearch)
{
  // Test exit combinator method without start
  using namespace Search;
  using namespace Core;
  
  auto lbs = getLocalBaseSearch({{1, 10}, {1, 10}, {1, 10}, {1, 10}}, 4, 1, 1);
  getSolutionManager()->setSolutionLimit(1);
  
  const auto& searchSpace = getSearchSpace();
  
  // Add constraints to constraint store
  // V0 == v1
  Core::ConstraintEqTest eq1(searchSpace[0], searchSpace[1]);
  
  // V2 == V3
  Core::ConstraintEqTest eq2(searchSpace[2], searchSpace[3]);
  
  // Register constraints into the constraint store
  eq1.getConstraint()->subscribeToConstraintStore(lbs->searchSemantics()->constraintStore());
  eq2.getConstraint()->subscribeToConstraintStore(lbs->searchSemantics()->constraintStore());
  
  // Get the root node
  auto rootNode = lbs->getRootNode(0);
  ASSERT_TRUE(rootNode->searchNeighborhood());
  
  // Call exit method.
  // It should reset the internal search node and set the status from the global status
  lbs->exit(rootNode);
  ASSERT_FALSE(rootNode->searchNeighborhood());
  ASSERT_EQ(SearchStatus::SEARCH_UNDEF, lbs->getGlobalSearchStatus());
}//startSearchFullLN

OPTILAB_TEST_F(LocalBaseSearchTest, startSearchFullLNMultipleRestorePoints)
{
  // Test start local base search on root node with one large neighborhood
  // that covers the entire search space and restoring the neighborhood multiple times
  using namespace Search;
  using namespace Core;
  
  auto lbs = getLocalBaseSearch({{1, 10}, {1, 10}, {1, 10}, {1, 10}}, 4, 1, 2);
  getSolutionManager()->setSolutionLimit(1);
  
  const auto& searchSpace = getSearchSpace();
  
  // Add constraints to constraint store
  // V0 == v1
  Core::ConstraintEqTest eq1(searchSpace[0], searchSpace[1]);
  
  // V2 == V3
  Core::ConstraintEqTest eq2(searchSpace[2], searchSpace[3]);
  
  // Register constraints into the constraint store
  eq1.getConstraint()->subscribeToConstraintStore(lbs->searchSemantics()->constraintStore());
  eq2.getConstraint()->subscribeToConstraintStore(lbs->searchSemantics()->constraintStore());
  
  // Get the root node
  auto rootNode = lbs->getRootNode(0);
  lbs->start(rootNode);
  
  ASSERT_EQ(SearchStatus::SEARCH_SUCCESS, lbs->getGlobalSearchStatus());
  
  // Call exit method.
  // It should reset the internal search node and set the status from the search
  lbs->exit(rootNode);
  ASSERT_FALSE(rootNode->searchNeighborhood());
  ASSERT_EQ(SearchStatus::SEARCH_SUCCESS, rootNode->getStatus());
}//startSearchFullLNMultipleRestorePoints

OPTILAB_TEST_F(LocalBaseSearchTest, startSearchFullLNMultipleLabelings)
{
  // Test start local base search on root node with one large neighborhood
  // that covers the entire search space and labeling and
  // restoring the neighborhood multiple times
  using namespace Search;
  using namespace Core;
  
  auto lbs = getLocalBaseSearch({{1, 10}, {1, 10}, {1, 10}, {1, 10}}, 4, 1, 3);
  getSolutionManager()->setSolutionLimit(2);
  
  const auto& searchSpace = getSearchSpace();
  
  // Add constraints to constraint store
  // V0 == v1
  Core::ConstraintEqTest eq1(searchSpace[0], searchSpace[1]);
  
  // V2 == V3
  Core::ConstraintEqTest eq2(searchSpace[2], searchSpace[3]);
  
  // Register constraints into the constraint store
  eq1.getConstraint()->subscribeToConstraintStore(lbs->searchSemantics()->constraintStore());
  eq2.getConstraint()->subscribeToConstraintStore(lbs->searchSemantics()->constraintStore());
  
  // Get the root node
  auto rootNode = lbs->getRootNode(0);
  lbs->start(rootNode);
  
  ASSERT_EQ(SearchStatus::SEARCH_SUCCESS, lbs->getGlobalSearchStatus());
  
  // Call exit method.
  // It should reset the internal search node and set the status from the search
  lbs->exit(rootNode);
  ASSERT_FALSE(rootNode->searchNeighborhood());
  ASSERT_EQ(SearchStatus::SEARCH_SUCCESS, rootNode->getStatus());
}//startSearchFullLNMultipleLabelings
