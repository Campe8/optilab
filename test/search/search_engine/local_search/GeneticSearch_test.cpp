
#include "utest_globals.hpp"

#include "SearchEngineTestInc.hpp"

#include "GeneticSearchEnvironment.hpp"

namespace Search {
  
  class GeneticSearchMock: public GeneticSearch {
  public:
    GeneticSearchMock(SemanticPtr aSemantics,
                      EnvironmentList& aEnvironmentList,
                      std::size_t aSizeNeighborhoods,
                      std::size_t aNumNeighborhoods,
                      std::size_t aPopulationSize,
                      std::size_t aNumGenerations,
                      std::size_t aMutationChance,
                      std::size_t aMutationSize,
                      bool aAggressiveSearch)
    : GeneticSearch(std::move(aSemantics),
                    aEnvironmentList,
                    aSizeNeighborhoods,
                    aNumNeighborhoods,
                    aPopulationSize,
                    aNumGenerations,
                    aMutationChance,
                    aMutationSize,
                    aAggressiveSearch)
    {
    }
    
    ~GeneticSearchMock() {}
    
    inline SearchSemantics* searchSemantics() const { return LocalBaseSearch::searchSemantics(); }
    
  };
  
}// end namespace Search

namespace {
  
  using namespace Search;
  
  class GeneticSearchTest : public ::testing::Test {
  public:
    using IntPair = std::pair<INT_64, INT_64>;
    using IntPairList = std::vector<IntPair>;
    using ObjExtr = Core::VariableSemanticObjective::ObjectiveExtremum;
    using ObjExtrList = std::vector<ObjExtr>;
    
  public:
    GeneticSearchTest()  {};
    ~GeneticSearchTest() {};
    
    /// Sets up and returns a DFS on the variables identified by given domain bounds
    std::shared_ptr<SearchEngine> getGeneticSearch(const IntPairList& aBounds,
                                                   int aSizeNeighborhoods,
                                                   int aNumNeighborhoods,
                                                   std::size_t aPopulationSize=10,
                                                   std::size_t aNumGenerations=10,
                                                   std::size_t aMutationChance=60,
                                                   std::size_t aMutationSize=1,
                                                   bool aAggressiveSearch=false)
    {
      using namespace Core;
      using namespace Search;
      using namespace Semantics;
      
      assert(aPopulationSize > 1);
      
      std::set<Core::VariableSPtr> brcVars;
      std::unique_ptr<VariableFactory> varFactory(new VariableFactory());
      
      pEnvironmentVars.clear();
      
      // Fill branching variable set
      std::string varName{"V"};
      for(std::size_t idx{0}; idx < aBounds.size(); ++idx)
      {
        std::string id = varName + std::to_string(idx);
        VariableSemanticUPtr varSem(new VariableSemanticDecision(id));
        varSem->setInputOrder(idx);
        varSem->setBranchingPolicy(true);
        
        // Create variable
        VariableSPtr var(varFactory->variableInteger(aBounds[idx].first, aBounds[idx].second, std::move(varSem)));
        
        pEnvironmentVars.push_back(var);
        brcVars.insert(var);
      }
      
      // Create the search environment
      std::vector<SearchEnvironment::VarSem> venv;
      for(auto& var : brcVars)
      {
        auto sem = VariableSemanticSPtr(var->variableSemantic()->clone());
        venv.push_back({var, sem});
      }
      
      // Create an environment with an random value heuristic
      // Create an environment with an random value heuristic
      auto lsEnvironment = new GeneticSearchEnvironment(venv, aSizeNeighborhoods, aNumNeighborhoods, false);
      lsEnvironment->setRandomSeed(0);
      lsEnvironment->setWarmStart(true);
      lsEnvironment->setMutationSize(aMutationSize);
      lsEnvironment->setMutationChance(aMutationChance);
      lsEnvironment->setNumGenerations(aNumGenerations);
      lsEnvironment->setPopulationSize(aPopulationSize);
      
      std::vector<std::unique_ptr<SearchEnvironment>> environmentList(1);
      environmentList[0].reset(lsEnvironment);
      
      // Create ID var map
      auto idVarMap = std::make_shared<Model::ModelVarMap>();
      for(auto& var : pEnvironmentVars)
      {
        (*idVarMap)[var->getVariableNameId()] = var;
      }
      
      // Create the search semantic
      std::unique_ptr<SearchSemantics> semantics(new SearchSemantics());
      
      // Create new DFS
      pGeneticSearch = std::make_shared<GeneticSearchMock>(std::move(semantics),
                                                           environmentList,
                                                           aSizeNeighborhoods,
                                                           aNumNeighborhoods,
                                                           aPopulationSize,
                                                           aNumGenerations,
                                                           aMutationChance,
                                                           aMutationSize,
                                                           aAggressiveSearch);
      
      auto solManager = std::make_shared<Search::SolutionManager>(idVarMap);
      pGeneticSearch->setSolutionManager(solManager);
      
      // Create search strategy wrapper around the base search
      auto ss = std::unique_ptr<Search::SearchStrategy>(new Search::SearchStrategy(pGeneticSearch));
      
      // Create search engine on the search strategy
      return std::unique_ptr<Search::SearchEngine>(new Search::SearchEngine(std::move(ss)));
    }//getGeneticSearch
    
    void addConstraints(bool aUseNQ=false)
    {
      auto numVars = pEnvironmentVars.size();
      for(std::size_t i{0}; i < numVars; ++i)
      {
        for(std::size_t j{i+1}; j < numVars; ++j)
        {
          if(aUseNQ)
          {
            Core::ConstraintNqTest con(getVar(i), getVar(j));
            pConstraints.push_back(con.getConstraint());
            pConstraints.back()->subscribeToConstraintStore(getSearchSemantics()->constraintStore());
          }
          else
          {
            Core::ConstraintEqTest con(getVar(i), getVar(j));
            pConstraints.push_back(con.getConstraint());
            pConstraints.back()->subscribeToConstraintStore(getSearchSemantics()->constraintStore());
          }
        }
      }
    }
    
    void addUnsatConstraints()
    {
      auto numVars = pEnvironmentVars.size();
      for(std::size_t i{0}; i < numVars - 1; ++i)
      {
        Core::ConstraintEqTest con(getVar(i), getVar(i+1));
        pConstraints.push_back(con.getConstraint());
        pConstraints.back()->subscribeToConstraintStore(getSearchSemantics()->constraintStore());
      }
      
      // Add an unsat constraint
      Core::ConstraintNqTest con(getVar(0), getVar(numVars - 1));
      pConstraints.push_back(con.getConstraint());
      pConstraints.back()->subscribeToConstraintStore(getSearchSemantics()->constraintStore());
    }
    
    Core::VariableSPtr getVar(std::size_t aVarIdx)
    {
      return aVarIdx < pEnvironmentVars.size() ? pEnvironmentVars[aVarIdx] : nullptr;
    }

    SolutionManager* getSolutionManager()
    {
      assert(pGeneticSearch);
      return pGeneticSearch->solutionManager();
    }
    
    SearchSemantics* getSearchSemantics()
    {
      assert(pGeneticSearch);
      return pGeneticSearch->searchSemantics();
    }
    
    CombinatorNode* getRootNode()
    {
      assert(pGeneticSearch);
      return pGeneticSearch->getRootNode(0);
    }
    
    std::shared_ptr<GeneticSearchMock> getGeneticSearch()
    {
      assert(pGeneticSearch);
      return pGeneticSearch;
    }
    
  private:
    /// Genetic search instance pointer
    std::shared_ptr<GeneticSearchMock> pGeneticSearch;
    
    /// List of constraints
    std::vector<ConstraintSPtr> pConstraints;
    
    /// Vector of decision variables
    std::vector<Core::VariableSPtr> pEnvironmentVars;
    
    /// Vector of optimization variables
    std::vector<Core::VariableSPtr> pOptimizationVars;
  };
  
}// end namespace

OPTILAB_TEST_F(GeneticSearchTest, Label0Var)
{
  // Labeling on empty environment: should return success
  using namespace Search;
  using namespace Core;
  
  auto gs = getGeneticSearch({}, 0, 1);
  
  // Set all solutions
  auto solutionManager = getSolutionManager();
  solutionManager->setSolutionLimit(SM_SOLUTION_LIMIT_NONE);
  
  // Run DFS should return SEARCH_SUCCESS since all solutions are found
  auto searchStatus = gs->label(getRootNode());
  ASSERT_EQ(searchStatus, SearchStatus::SEARCH_SUCCESS);
  
  // Solution manager should have recorded 0 solutions
  ASSERT_EQ(solutionManager->getNumberOfRecordedSolutions(), 0);
}//Label0Var

OPTILAB_TEST_F(GeneticSearchTest, LabelVarsFullLargeNeighborhoodTerminate)
{
  // Labeling variables using genetic algorithm,
  // a neighborhood covering the wholw set of varibles,
  // and one large neighborhood with terminate expected outcome
  using namespace Search;
  using namespace Core;

  auto gs = getGeneticSearch({{1, 10}, {1, 10}, {1, 10}, {1, 10}, {1, 10}}, 5, 1, 2, 1);

  // Add constraints to the constraint store
  // Equality constraints
  addConstraints();
  
  // Set all solutions
  auto solutionManager = getSolutionManager();
  solutionManager->setSolutionLimit(SM_SOLUTION_LIMIT_NONE);
  
  // Run genetic algorithm should return SEARCH_TERMINATE
  // assuming the correctness of the random algorithm for
  // creating the population.
  // The constraints are equality constraints and there is
  // a population of only one individual and one generation.
  // In other words, it doesn't perform evolution and
  // there should be at least one variable which has a variable
  // assigned different than some other variable.
  // It should not return success
  auto searchStatus = gs->label(getRootNode());
  ASSERT_EQ(SearchStatus::SEARCH_TERMINATE, searchStatus);
  
  // Solution manager should have recorded 1 solutions,
  // i.e., the best solution found so far
  ASSERT_EQ(1, solutionManager->getNumberOfRecordedSolutions());
}//LabelVarsFullLargeNeighborhoodTerminate

OPTILAB_TEST_F(GeneticSearchTest, LabelVarsFullLargeNeighborhoodSuccess)
{
  // Labeling variables using genetic algorithm,
  // a neighborhood covering the wholw set of varibles,
  // and one large neighborhood with success expected outcome
  using namespace Search;
  using namespace Core;
  
  auto gs = getGeneticSearch({{1, 1}, {1, 1}, {1, 1}, {1, 2}, {1, 1}}, 5, 1);
  
  // Add constraints to the constraint store
  // Equality constraints
  addConstraints();
  
  // Set one solution
  auto solutionManager = getSolutionManager();
  solutionManager->setSolutionLimit(1);
  
  // Run genetic algorithm should return SEARCH_SUCCESS
  // assuming the correctness of the random algorithm for
  // creating the population.
  // There is only one variable with a domain of size two
  // which is not equal to the other variables.
  // There are multiple individual in the population (> 2).
  // Assuming the correctness of the random algorithm, at least
  // one individual should assign a value to the only non labeled variable
  // which should lead to a solution
  auto searchStatus = gs->label(getRootNode());
  ASSERT_EQ(SearchStatus::SEARCH_SUCCESS, searchStatus);
  
  // Solution manager should have recorded 0 solutions
  ASSERT_EQ(1, solutionManager->getNumberOfRecordedSolutions());
}//LabelVarsFullLargeNeighborhoodSuccess

OPTILAB_TEST_F(GeneticSearchTest, LabelVarsImprovingCost)
{
  // Labeling variables using genetic algorithm,
  // a neighborhood covering the whole set of varibles,
  // and one large neighborhood.
  // This test ensures that the genetic search improves
  // the cost during it's search (or, at least, keeps it the same)
  using namespace Search;
  using namespace Core;
  
  // Using 1 large neighborhood of 5 variables,
  // 100 as population size, and 10 generations
  // Default values as follows:
  // MutationChance=60,
  // MutationSize=1
  auto gs = getGeneticSearch({{1, 10}, {1, 10}, {1, 10}, {1, 10}, {1, 10}}, 5, 1, 100, 10);
  
  // Add constraints to the constraint store
  // Equality constraints
  addConstraints();
  
  // Set to find at least one improving solution
  auto solutionManager = getSolutionManager();
  solutionManager->setSolutionLimit(1);
  
  // Get the start value for the cost
  auto cost = getGeneticSearch()->getBestNeighborhoodCost();
  
  // Run genetic algorithm should return either SEARCH_TERMINATE
  // or SEARCH_SUCCESS assuming the correctness of the random algorithm.
  auto searchStatus = gs->label(getRootNode());
  ASSERT_TRUE(searchStatus == SearchStatus::SEARCH_TERMINATE || searchStatus == SearchStatus::SEARCH_SUCCESS);
  
  // Get the cost after search
  auto improvedCost = getGeneticSearch()->getBestNeighborhoodCost();
  ASSERT_LE(improvedCost, cost);
  
  // If the search returned success, the cost should be the cost
  // of the global minimum.
  // @note this happened while testing but the unit test cannot rely
  // on the randomness of the algorithm
  if(searchStatus == SearchStatus::SEARCH_SUCCESS)
  {
    ASSERT_EQ(LocalBaseSearch::getGlobalMinimumCostValue(), improvedCost);
  }
  
  // Solution manager should have recorded 1 solutions,
  // i.e., the best solution found so far
  ASSERT_EQ(solutionManager->getNumberOfRecordedSolutions(), 1);
}//LabelVarsImprovingCost

OPTILAB_TEST_F(GeneticSearchTest, LabelVarsImprovingCostNoSolutions)
{
  // Labeling variables using genetic algorithm,
  // a neighborhood covering the whole set of varibles,
  // and one large neighborhood.
  // This test ensures that the genetic search improves
  // the cost during it's search (or, at least, keeps it the same).
  // The constraints posted are for an unsatisfiable model to ensure
  // that all paths are covered during unit testing, i.e., there is
  // no premature return with success status
  using namespace Search;
  using namespace Core;
  
  // Using 1 large neighborhood of 5 variables,
  // 50 as population size, and 10 generations
  // Default values as follows:
  // MutationChance=60,
  // MutationSize=1
  auto gs = getGeneticSearch({{1, 10}, {1, 10}, {1, 10}, {1, 10}, {1, 10}}, 5, 1, 50, 10);
  
  // Add constraints to the constraint store to generate an unsatisfiable model
  addUnsatConstraints();
  
  // Set to find at least one improving solution
  auto solutionManager = getSolutionManager();
  solutionManager->setSolutionLimit(1);
  
  // Get the start value for the cost
  auto cost = getGeneticSearch()->getBestNeighborhoodCost();
  
  // Run genetic algorithm should return either SEARCH_TERMINATE
  // Since the problem is not satisfiable
  auto searchStatus = gs->label(getRootNode());
  ASSERT_EQ(SearchStatus::SEARCH_TERMINATE, searchStatus);
  
  // Get the cost after search
  auto improvedCost = getGeneticSearch()->getBestNeighborhoodCost();
  ASSERT_LE(improvedCost, cost);
  
  // Solution manager should have recorded 1 solutions,
  // i.e., the best solution found so far
  ASSERT_EQ(solutionManager->getNumberOfRecordedSolutions(), 1);
}//LabelVarsImprovingCost

OPTILAB_TEST_F(GeneticSearchTest, LabelVarsStableCostNoSolutions)
{
  // Labeling variables using genetic algorithm,
  // a neighborhood covering the whole set of varibles,
  // and one large neighborhood.
  // This test ensures that the genetic search doesn't change
  // or modify the solution space of a problem.
  // The problem is unsatisfiable and with only one constraint.
  // It should lead to an assignment of cost 1
  using namespace Search;
  using namespace Core;
  
  auto gs = getGeneticSearch({{1, 10}, {11, 12}}, 2, 1);
  
  // Add (one) equality constraints between the two variables
  // This generates an unsatisfiable problem since the domains
  // of the variables are not consistent w.r.t. the constraint
  addConstraints();
  
  // Set to find at least one improving solution
  auto solutionManager = getSolutionManager();
  solutionManager->setSolutionLimit(1);
  
  // Run genetic algorithm should return either SEARCH_TERMINATE
  // Since the problem is not satisfiable
  auto searchStatus = gs->label(getRootNode());
  ASSERT_EQ(SearchStatus::SEARCH_TERMINATE, searchStatus);
  
  // Get the cost after search.
  // The cost should be 1 since the only constraint in the model
  // cannot be satisfied
  auto cost = getGeneticSearch()->getBestNeighborhoodCost();
  ASSERT_EQ(1 * getGeneticSearch()->getNeighborhoodCostWeight(), cost);
  
  // Solution manager should have recorded 1 solutions
  ASSERT_EQ(solutionManager->getNumberOfRecordedSolutions(), 1);
}//LabelVarsStableCostNoSolutions

OPTILAB_TEST_F(GeneticSearchTest, LabelVarsStableCostNoSolutionsAggressiveSearch)
{
  // Labeling variables using genetic algorithm,
  // a neighborhood covering the whole set of varibles,
  // and one large neighborhood.
  // This test ensures that the genetic search doesn't change
  // or modify the solution space of a problem.
  // The problem is unsatisfiable and with only one constraint.
  // Since the search is aggressive, there shouldn't be any
  // recorded solutions
  using namespace Search;
  using namespace Core;
  
  auto gs = getGeneticSearch({{1, 10}, {11, 12}}, 2, 1, 10, 10, 60, 1, true);
  
  // Add (one) equality constraints between the two variables
  // This generates an unsatisfiable problem since the domains
  // of the variables are not consistent w.r.t. the constraint
  addConstraints();
  
  // Set to find at least one improving solution
  auto solutionManager = getSolutionManager();
  solutionManager->setSolutionLimit(1);
  
  // Run genetic algorithm should return either SEARCH_TERMINATE
  // Since the problem is not satisfiable
  auto searchStatus = gs->label(getRootNode());
  ASSERT_EQ(SearchStatus::SEARCH_TERMINATE, searchStatus);
  
  // Get the cost after search.
  // The cost should be 1 since the only constraint in the model
  // cannot be satisfied
  auto cost = getGeneticSearch()->getBestNeighborhoodCost();
  ASSERT_EQ(1 * getGeneticSearch()->getNeighborhoodCostWeight(), cost);
  
  // Solution manager should have recorded 1 solutions
  ASSERT_EQ(solutionManager->getNumberOfRecordedSolutions(), 1);
}//LabelVarsStableCostNoSolutionsAggressiveSearch
