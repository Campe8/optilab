#pragma once

#include "SearchEngine.hpp"
#include "SearchStrategy.hpp"
#include "GeneticSearch.hpp"
#include "Neighborhood.hpp"
#include "ConstraintStoreTestInc.hpp"

#include "BranchingStrategyRegister.hpp"
#include "BranchingStrategyBinaryChoicePoint.hpp"

#include "LocalBaseSearch.hpp"
#include "SearchHeuristic.hpp"

#include "LocalSearchEnvironment.hpp"
#include "VariableFactory.hpp"
#include "VariableInteger.hpp"
#include "VariableSemanticDecision.hpp"
#include "VariableSemanticObjective.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementVoid.hpp"
#include "DomainElementManager.hpp"

#include "SearchMacro.hpp"
#include "SolutionManager.hpp"

namespace Search {
  
  class LocalSearchEnvironmentMock : public LocalSearchEnvironment {
  public:
    LocalSearchEnvironmentMock(const std::vector<VarSem>& aEnvironment,
                               VariableChoiceMetricType aVarChoice,
                               ValueChoiceMetricType aValChoice,
                               std::size_t aSizeNeighborhoods,
                               std::size_t aNumNeighborhoods,
                               bool aRunAggressiveSearch,
                               SearchEngineStrategyType aStrategyType)
    : LocalSearchEnvironment(aEnvironment, aVarChoice, aValChoice,
                             aSizeNeighborhoods, aNumNeighborhoods,
                             aRunAggressiveSearch, aStrategyType)
    {
    }
    
    };
  
}// end namespace search
