
#include "utest_globals.hpp"

#include "Node.hpp"

#include "BranchingStrategyRegister.hpp"
#include "BranchingStrategyBinaryChoicePoint.hpp"

#include "SearchHeuristic.hpp"

#include "VariableFactory.hpp"
#include "VariableInteger.hpp"
#include "VariableSemanticDecision.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementVoid.hpp"
#include "DomainElementManager.hpp"

namespace Search {
  
  class NodeUnitTest : public Node {
  public:
    NodeUnitTest(const SearchHeuristicSPtr& aSearchHeuristic,
                 const Semantics::ConstraintStoreSPtr& aConstraintStore)
    : Node(0)
    , pSearchHeuristic(aSearchHeuristic)
    , pConstraintStore(aConstraintStore)
    , pBranchingPath(nullptr)
    , pBranchingStrategyRegister(nullptr)
    {
    }
    
    ~NodeUnitTest()
    {
      pBranchingPath.reset(nullptr);
      pBranchingStrategyRegister.reset(nullptr);
      setBranchingPath(nullptr);
      setBranchingStrategyRegister(nullptr);
    }
    
    void init()
    {
      assert(pSearchHeuristic);
      assert(pConstraintStore);
      Core::VariableSPtr branchingVar = pSearchHeuristic->getChoiceVariable();
      
      setBranchingVariable(branchingVar);
      
      setSearchHeuristic(pSearchHeuristic.get());
      
      pBranchingStrategyRegister.reset(new BranchingStrategyRegister());
      setBranchingStrategyRegister(pBranchingStrategyRegister.get());
      
      setConstraintStore(pConstraintStore);
      
      pBranchingPath.reset(new BranchingPath());
      setBranchingPath(pBranchingPath.get());
    }
    
    void extendNode()
    {
      Node::extendNode();
    }
    
    inline BranchingSet& getBranchingSet()
    {
      return Node::getBranchingSet();
    }
    
    inline Core::VariableSPtr getNextBranchingVariable()
    {
      return Node::getNextBranchingVariable();
    }
    
  private:
    SearchHeuristicSPtr pSearchHeuristic;
    
    Semantics::ConstraintStoreSPtr pConstraintStore;
    
    std::unique_ptr<BranchingPath> pBranchingPath;
    
    std::unique_ptr<BranchingStrategyRegister> pBranchingStrategyRegister;
  };
  
}// end namespace Search


namespace {
  
  using namespace Search;
  
  class NodeTest : public ::testing::Test {
  public:
    NodeTest()
    {
    }
    
    ~NodeTest()
    {
    }
    
    SearchHeuristicSPtr searchHeuristic()
    {
      using namespace Core;
      using namespace Search;
      
      std::unique_ptr<VariableFactory> varFactory(new VariableFactory());
      
      // Fill branching variable set
      VariableSemanticUPtr varSem1(new VariableSemanticDecision("Var1"));
      VariableSemanticUPtr varSem2(new VariableSemanticDecision("Var2"));
      
      varSem1->setInputOrder(0);
      varSem2->setInputOrder(1);
      VariableSPtr var1(varFactory->variableInteger(1, 10, std::move(varSem1)));
      VariableSPtr var2(varFactory->variableInteger(1, 5, std::move(varSem2)));
      
      pBranchingVarVec.push_back(var1);
      pBranchingVarVec.push_back(var2);
      
      SearchHeuristicSPtr searchHeuristic =
      std::make_shared<SearchHeuristic>(pBranchingVarVec,
                                        VariableChoiceMetricType::VAR_CM_INPUT_ORDER,
                                        ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MIN);
      
      return searchHeuristic;
    }
    
    inline std::vector<Core::VariableSPtr> getBranchingVariables() const
    {
      return pBranchingVarVec;
    }
    
    Semantics::ConstraintStoreSPtr constraintStore()
    {
      Semantics::ConstraintStoreSPtr constraintStore = std::make_shared<Semantics::ConstraintStore>();
      return constraintStore;
    }
    
    NodeUnitTest* getNode(const SearchHeuristicSPtr& aSearchHeuristic,
                          const Semantics::ConstraintStoreSPtr& aConstraintStore)
    {
      auto node = new NodeUnitTest(aSearchHeuristic, aConstraintStore);
      assert(node);
      
      node->init();
      return node;
    }
    
  private:
    std::vector<Core::VariableSPtr> pBranchingVarVec;
  };
  
}// end namespace

OPTILAB_TEST_F(NodeTest, staticMethods)
{
  using namespace Semantics;
  using namespace Search;
  
  SearchHeuristicSPtr sh = searchHeuristic();
  ConstraintStoreSPtr cs = constraintStore();
  
  std::unique_ptr<NodeUnitTest> node(getNode(sh, cs));
  ASSERT_TRUE(node);
  
  // Node type
  ASSERT_EQ(SearchChoiceType::SEARCH_CHOICE_NODE, node->getSearchChoiceType());
  
  // Isa and cast
  ASSERT_TRUE(Node::isa(node.get()));
  ASSERT_EQ(node.get(), Node::cast(node.get()));
  
  SearchChoice* choice = static_cast<SearchChoice*>(node.get());
  ASSERT_TRUE(Node::isa(choice));
  ASSERT_EQ(node.get(), Node::cast(choice));
}//staticMethods

OPTILAB_TEST_F(NodeTest, BaseMethods)
{
  using namespace Semantics;
  
  SearchHeuristicSPtr sh = searchHeuristic();
  ConstraintStoreSPtr cs = constraintStore();
  
   std::unique_ptr<NodeUnitTest> node(getNode(sh, cs));
  ASSERT_TRUE(node);
  
  // Branching var is set by init on NodeUnitTest
  ASSERT_TRUE(node->hasSuccessor());
  
  // Should be rightmost node since degree id is not set
  ASSERT_TRUE(node->isRightmostNode());
  
  // Branching path should be empty
  ASSERT_TRUE(node->getBranchingPath()->empty());
  
  // Branching set should be empty, no extendNode call
  ASSERT_TRUE(node->getBranchingSet().empty());
  
  // Branching variable should be null, no extendNode call
  ASSERT_FALSE(node->getNextBranchingVariable());
}//BaseMethods

OPTILAB_TEST_F(NodeTest, ExtendNode)
{
  using namespace Semantics;
  
  SearchHeuristicSPtr sh = searchHeuristic();
  ConstraintStoreSPtr cs = constraintStore();
  
  std::unique_ptr<NodeUnitTest> node(getNode(sh, cs));
  node->extendNode();
  
  // Branching set on branching variable, default uses
  // BRANCHING_BINARY_CHOICE_POINT
  ASSERT_EQ(node->getBranchingSet().size(), 2);
  ASSERT_NE(node->getBranchingSet()[0].get(), node->getBranchingSet()[1].get());
  
  // Branching variable should be not null
  ASSERT_TRUE(node->getNextBranchingVariable());
  
  // Branching path should still be empty
  ASSERT_TRUE(node->getBranchingPath()->empty());
}//ExtendNode

OPTILAB_TEST_F(NodeTest, Next)
{
  using namespace Semantics;
  
  SearchHeuristicSPtr sh = searchHeuristic();
  ConstraintStoreSPtr cs = constraintStore();
  auto bVec = getBranchingVariables();
  
  std::unique_ptr<NodeUnitTest> node(getNode(sh, cs));
  
  // Branching var is set by init on NodeUnitTest
  ASSERT_TRUE(node->hasSuccessor());
  ASSERT_TRUE(node->getBranchingPath()->empty());
  
  // Get next: should be 1/2
  // since BRANCHING_BINARY_CHOICE_POINT
  std::unique_ptr<Node> nodeNext1(node->next());
  ASSERT_TRUE(nodeNext1);
  ASSERT_EQ(node->getNextBranchingVariable().get(), bVec[0].get());
  
  // Should be 1 branching constriant in the branching path
  ASSERT_EQ(node->getBranchingPath()->size(), 1);
  
  // Branching var is set by init on NodeUnitTest
  ASSERT_TRUE(node->hasSuccessor());
  
  // Shouldn't be rightmost
  ASSERT_FALSE(nodeNext1->isRightmostNode());
  
  // Should have successor
  ASSERT_TRUE(nodeNext1->hasSuccessor());
  
  // Only the branching constraint for nodeNext1
  // should be registered
  ASSERT_EQ(cs->getNumRegisteredConstraints(), 1);
  
  // Get next: should be 2/2
  // since BRANCHING_BINARY_CHOICE_POINT
  std::unique_ptr<Node> nodeNext2(node->next());
  ASSERT_TRUE(nodeNext2);
  
  // Should be 1 branching constriant in the branching path,
  // since there is always 1 branching constraint for the
  // (same) level of the root node where the branch happens
  ASSERT_EQ(node->getBranchingPath()->size(), 1);
  
  // Should have successor, no variable is assigned yet
  // so the heuristic returns always the same variable
  // as next branching variable
  ASSERT_TRUE(nodeNext2->hasSuccessor());
  
  // Should be rightmost node since nodeNext1 is
  // the leftmost node
  ASSERT_TRUE(nodeNext2->isRightmostNode());
  
  // Branching constraints for nodeNext1 and
  // nodeNext2 should be registered, memory optimization
  // should keep only last branching constraint registered
  ASSERT_LE(cs->getNumRegisteredConstraints(), 2);
  ASSERT_GE(cs->getNumRegisteredConstraints(), 1);
  
  nodeNext1.reset(nullptr);
  nodeNext2.reset(nullptr);
  node.reset(nullptr);
}//Next

OPTILAB_TEST_F(NodeTest, NextAndRunConsistency)
{
  using namespace Semantics;
  
  SearchHeuristicSPtr sh = searchHeuristic();
  ConstraintStoreSPtr cs = constraintStore();
  auto bVec = getBranchingVariables();
  
  std::unique_ptr<NodeUnitTest> rootNode(getNode(sh, cs));
  
  // No constraints registered
  ASSERT_EQ(cs->getNumRegisteredConstraints(), 0);
  
  // Branching var is set by init on NodeUnitTest
  ASSERT_TRUE(rootNode->hasSuccessor());
  
  // Get next: should be 1/2
  // since BRANCHING_BINARY_CHOICE_POINT
  std::unique_ptr<Node> nodeNext1(rootNode->next());
  ASSERT_TRUE(nodeNext1);
  ASSERT_EQ(rootNode->getNextBranchingVariable().get(), bVec[0].get());
  
  // Branching var is set by init on NodeUnitTest
  ASSERT_TRUE(rootNode->hasSuccessor());
  
  // Shouldn't be rightmost
  ASSERT_FALSE(nodeNext1->isRightmostNode());
  
  // Should have successor, i.e., at least var2
  ASSERT_TRUE(nodeNext1->hasSuccessor());
  
  // Only the branching constraint for nodeNext1
  // should be registered
  ASSERT_EQ(cs->getNumRegisteredConstraints(), 1);
  
  // Run consistency on constraint store
  StoreConsistencyStatus status = cs->runConsistency();
  // Status should be subsumed
  ASSERT_EQ(status, StoreConsistencyStatus::CONSISTENCY_SUBSUMED);
  
  // Get next: should be 2/2
  // since BRANCHING_BINARY_CHOICE_POINT
  std::unique_ptr<Node> nodeNext2(rootNode->next());
  ASSERT_TRUE(nodeNext2);
  
  // Should have successor, i.e, at least var2
  // since this is the next node of root
  ASSERT_TRUE(nodeNext2->hasSuccessor());
  
  // Should be rightmost node since nodeNext1 is
  // the leftmost node
  ASSERT_TRUE(nodeNext2->isRightmostNode());
  
  // Branching constraints for nodeNext1 and
  // nodeNext2 should be registered, memory optimization
  // should keep only last branching constraint registered
  ASSERT_LE(cs->getNumRegisteredConstraints(), 2);
  ASSERT_GE(cs->getNumRegisteredConstraints(), 1);
  
  // Run consistency on constraint store
  StoreConsistencyStatus status1 = cs->runConsistency();
  // Status should be failed since imposing 2 mutual
  // exlusive constraints on the same branching variable
  ASSERT_EQ(status1, StoreConsistencyStatus::CONSISTENCY_FAILED);
  
  // Asking for next node to root node should return nullptr
  ASSERT_FALSE(rootNode->next());
  
  nodeNext1.reset(nullptr);
  nodeNext2.reset(nullptr);
  rootNode.reset(nullptr);
}//NextAndRunConsistency

OPTILAB_TEST_F(NodeTest, ExploreTreeNoConsistency)
{
  using namespace Semantics;
  
  SearchHeuristicSPtr sh = searchHeuristic();
  ConstraintStoreSPtr cs = constraintStore();
  auto bVec = getBranchingVariables();
  
  // RootNode
  std::unique_ptr<NodeUnitTest> rootNode(getNode(sh, cs));
  ASSERT_TRUE(rootNode->hasSuccessor());
  
  //   RootNode
  //     /
  //    /
  // nodeNex1
  std::unique_ptr<Node> nodeNext1(rootNode->next());
  ASSERT_TRUE(nodeNext1);
  ASSERT_FALSE(nodeNext1->isRightmostNode());
  ASSERT_TRUE(nodeNext1->hasSuccessor());
  
  //     RootNode
  //       /
  //      /
  // nodeNext1(=d1)
  ASSERT_TRUE(nodeNext1->hasSuccessor());
  
  //       RootNode
  //          /
  //         /
  //    nodeNext1(=d1)
  //       /
  //      /
  // nodeNext2(=d2)
  std::unique_ptr<Node> nodeNext2(nodeNext1->next());
  ASSERT_TRUE(nodeNext2);
  
  // Should have successor since
  // this is the last node branching on in the search tree
  // BUT it has not being assigned yet which means that it is
  // still possible to assign the variable in "nodeNext2"
  ASSERT_TRUE(nodeNext2->hasSuccessor());
  
  // Should be leftmost node of nodeNext1
  ASSERT_FALSE(nodeNext2->isRightmostNode());
  
  // Branching constraints for nodeNext1 and
  // nodeNext2 should be registered, memory optimization
  // should keep only last branching constraint registered
  ASSERT_LE(cs->getNumRegisteredConstraints(), 2);
  ASSERT_GE(cs->getNumRegisteredConstraints(), 1);
  
  //          RootNode
  //          /      \
  //         /        \
  //  nodeNext1(=d1) nodeNext1Bis(!=d1)
  //       /
  //      /
  // nodeNext2(=d2)
  //
  // Asking for next node to root node should return nodeNext1Bis != d1
  // which is the righmost node of RootNode and there shouldn't be
  // more branches for RootNode and it was generated the first time
  // next was called on rootNode
  std::unique_ptr<Node> nodeNext1Bis(rootNode->next());
  ASSERT_FALSE(rootNode->next());
  ASSERT_TRUE(nodeNext1Bis);
  ASSERT_TRUE(nodeNext1Bis->isRightmostNode());
  ASSERT_TRUE(nodeNext1Bis->hasSuccessor());
  
  //              RootNode
  //              /      \
  //             /        \
  //      nodeNext1(=d1) nodeNext1Bis(!=d1)
  //        /       \
  //       /         \
  // nodeNext2(=d2) nodeNext2Bis(!=d2)
  //
  // Asking for next node to nodeNext1 should return nodeNext2Bis != d2
  // which is the righmost node of nodeNext1 and there shouldn't be
  // more branches for nodeNext1
  std::unique_ptr<Node> nodeNext2Bis(nodeNext1->next());
  ASSERT_FALSE(nodeNext1->next());
  ASSERT_TRUE(nodeNext2Bis);
  ASSERT_TRUE(nodeNext2Bis->isRightmostNode());
  
  //              RootNode
  //              /      \
  //             /        \
  //      nodeNext1(=d1) nodeNext1Bis(!=d1)
  //        /      \       /
  //       /        \     /
  //     =d2       !=d2 nodeNext2Right(=d2)
  //
  // Asking for next node to nodeNext1Bis should return nodeNext2Right = d2
  // which is the leftmost node of nodeNext1Bis
  std::unique_ptr<Node> nodeNext2Right(nodeNext1Bis->next());
  ASSERT_TRUE(nodeNext2Right);
  ASSERT_FALSE(nodeNext2Right->isRightmostNode());
  ASSERT_TRUE(nodeNext2Right->hasSuccessor());
  
  //              RootNode
  //              /      \
  //             /        \
  //      nodeNext1(=d1) nodeNext1Bis(!=d1)
  //        /      \       /      \
  //       /        \     /        \
  //     =d2       !=d2 =d2     nodeNext2RightBis(!=d2)
  //
  // Asking for next node to nodeNext1Bis should return nodeNext2RightBis != d2
  // which is the rightmost node of nodeNext1Bis
  std::unique_ptr<Node> nodeNext2RightBis(nodeNext1Bis->next());
  ASSERT_TRUE(nodeNext2RightBis);
  ASSERT_TRUE(nodeNext2RightBis->isRightmostNode());
  ASSERT_TRUE(nodeNext2RightBis->hasSuccessor());
  
  
  // Constraint store should have all the branching constraints
  // of the search tree. Memory optimization
  // should keep only last branching constraint registered
  ASSERT_LE(cs->getNumRegisteredConstraints(), 6);
  ASSERT_GE(cs->getNumRegisteredConstraints(), 1);
  
  nodeNext1.reset(nullptr);
  nodeNext2.reset(nullptr);
  nodeNext1Bis.reset(nullptr);
  nodeNext2Bis.reset(nullptr);
  nodeNext2Right.reset(nullptr);
  nodeNext2RightBis.reset(nullptr);
  rootNode.reset(nullptr);
}//ExploreTreeNoConsistency

OPTILAB_TEST_F(NodeTest, ExploreTreeConsistency)
{
  using namespace Semantics;
  
  SearchHeuristicSPtr sh = searchHeuristic();
  ConstraintStoreSPtr cs = constraintStore();
  auto bVec = getBranchingVariables();
  
  // RootNode
  std::unique_ptr<NodeUnitTest> rootNode(getNode(sh, cs));
  
  //   RootNode
  //     /
  //    /
  // nodeNex1
  std::unique_ptr<Node> nodeNext1(rootNode->next());
  
  //     RootNode
  //       /
  //      /
  // nodeNext1(=d1)
  StoreConsistencyStatus status = cs->runConsistency();
  ASSERT_EQ(status, StoreConsistencyStatus::CONSISTENCY_SUBSUMED);
  ASSERT_TRUE(nodeNext1->hasSuccessor());
  
  //       RootNode
  //          /
  //         /
  //    nodeNext1(=d1)
  //       /
  //      /
  // nodeNext2(=d2)
  std::unique_ptr<Node> nodeNext2(nodeNext1->next());
  
  // Both branching constraints for nodeNext1 and
  // nodeNext2 should be registered (less due to optimization)
  ASSERT_LE(cs->getNumRegisteredConstraints(), 2);
  ASSERT_GE(cs->getNumRegisteredConstraints(), 1);
  
  // Run consistency on constraint store
  StoreConsistencyStatus status1 = cs->runConsistency();
  // Status should be SUBSUMED since imposing 2 branching
  // constraints on 2 different branching variables
  ASSERT_EQ(status1, StoreConsistencyStatus::CONSISTENCY_SUBSUMED);
  
  // Everything is assigned, nodeNext2 shouldn't have a successor
  ASSERT_FALSE(nodeNext2->hasSuccessor());
  
  
  //          RootNode
  //          /      \
  //         /        \
  //  nodeNext1(=d1) nodeNext1Bis(!=d1)
  //       /
  //      /
  // nodeNext2(=d2)
  std::unique_ptr<Node> nodeNext1Bis(rootNode->next());
  StoreConsistencyStatus status2 = cs->runConsistency();
  
  // Branching should be registered, memory optimization
  // should keep only last branching constraint registered
  ASSERT_LE(cs->getNumRegisteredConstraints(), 3);
  ASSERT_GE(cs->getNumRegisteredConstraints(), 1);
  
  // Running consistency should fail since the registered branching constraints
  // should all be mutually exclusive
  ASSERT_EQ(status2, StoreConsistencyStatus::CONSISTENCY_FAILED);
  
  nodeNext1.reset(nullptr);
  nodeNext2.reset(nullptr);
  nodeNext1Bis.reset(nullptr);
  rootNode.reset(nullptr);
}//ExploreTreeConsistency

OPTILAB_TEST_F(NodeTest, DestructorAndBranchingPath)
{
  // This test verifies the cleanup of the branching path
  // that is done in the destructor of the node
  using namespace Semantics;
  
  auto sh = searchHeuristic();
  auto cs = constraintStore();
  auto bVec = getBranchingVariables();
  
  std::unique_ptr<NodeUnitTest> rootNode(getNode(sh, cs));
  auto bpath = rootNode->getBranchingPath();
  
  // Creating next node, the branching path should have size 1
  // since there is 1 decision points
  std::unique_ptr<Node> nodeNext1(rootNode->next());
  ASSERT_EQ(1, (*bpath).size());
  ASSERT_TRUE((*bpath)[0]);
  
  // Creating next node, the branching path should have size 2
  // since there are 2 decision points
  std::unique_ptr<Node> nodeNext2(nodeNext1->next());
  ASSERT_EQ(2, (*bpath).size());
  ASSERT_TRUE((*bpath)[1]);
  
  // Call the destructor of nodeNext2 should clean
  // the branching path from the its level (2) on.
  // Since the path has size 2, it should not clean anything
  nodeNext2.reset(nullptr);
  ASSERT_EQ(2, (*bpath).size());
  ASSERT_TRUE((*bpath)[1]);
  
  // Clean the destructor of nodeNext1 should clean
  // the branching path from the its level (1) on.
  nodeNext1.reset(nullptr);
  ASSERT_EQ(2, (*bpath).size());
  ASSERT_FALSE((*bpath)[1]);
  
  rootNode.reset(nullptr);
}//DestructorAndBranchingPath

