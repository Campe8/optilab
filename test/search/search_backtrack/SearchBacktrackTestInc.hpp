#pragma once

#include "BacktrackableObject.hpp"
#include "Memento.hpp"
#include "MementoState.hpp"
#include "BacktrackManager.hpp"

namespace Search {
  
  class MementoStateUnitTest : public MementoState {
  public:
    MementoStateUnitTest(int aValue);
    
    ~MementoStateUnitTest();
    
    static bool isa(const MementoState* aMementoState)
    {
      assert(aMementoState);
      return aMementoState->getMementoStateId() == MementoStateId::MS_UNDEF;
    }
    
    static MementoStateUnitTest* cast(MementoState* aMementoState)
    {
      if(!isa(aMementoState))
      {
        return nullptr;
      }
      return static_cast<MementoStateUnitTest*>(aMementoState);
    }
    
    static const MementoStateUnitTest* cast(const MementoState* aMementoState)
    {
      if(!isa(aMementoState))
      {
        return nullptr;
      }
      return static_cast<const MementoStateUnitTest*>(aMementoState);
    }
    
    inline void setValue(int aValue)
    {
      pValue = aValue;
    }
    
    inline int getValue() const
    {
      return pValue;
    }
    
  private:
    mutable int pValue;
  };
  
  class MementoUnitTest : public Memento {
  public:
    MementoUnitTest();
    
    virtual ~MementoUnitTest();
    
    void setState(std::unique_ptr<MementoState> aState)
    {
      Memento::setState(std::move(aState));
    }
    
    const MementoState* getState()
    {
      return Memento::getState();
    }
  };
  
  class BacktrackableObjectUnitTest : public BacktrackableObject {
  public:
    BacktrackableObjectUnitTest(int aStateValue);
    
    ~BacktrackableObjectUnitTest();
    
    inline void setStateValue(int aStateValue)
    {
      pStateValue = aStateValue;
      notifyBacktrackManager();
    }
    
    inline int getStateValue() const
    {
      return pStateValue;
    }
    
  protected:
    inline std::unique_ptr<MementoState> snapshot() override
    {
      return std::unique_ptr<MementoStateUnitTest>(new MementoStateUnitTest(pStateValue));
    }
    
    inline void reinstateSnapshot(const MementoState* aSnapshot) override
    {
      assert(aSnapshot);
      assert(MementoStateUnitTest::isa(aSnapshot));
      pStateValue = MementoStateUnitTest::cast(aSnapshot)->getValue();
    }
    
  private:
    int pStateValue;
  };
  
} // end namespace Search
