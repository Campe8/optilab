#include "SearchBacktrackTestInc.hpp"

namespace Search {
  
  MementoStateUnitTest::MementoStateUnitTest(int aValue)
  : MementoState(MementoStateId::MS_UNDEF)
  , pValue(aValue)
  {
  }
  
  MementoStateUnitTest::~MementoStateUnitTest()
  {
  }
  
  MementoUnitTest::MementoUnitTest()
  {
  }
  
  MementoUnitTest::~MementoUnitTest()
  {
  }
  
  BacktrackableObjectUnitTest::BacktrackableObjectUnitTest(int aStateValue)
  : pStateValue(aStateValue)
  {
  }
  
  BacktrackableObjectUnitTest::~BacktrackableObjectUnitTest()
  {
  }
  
} // end namespace Search
