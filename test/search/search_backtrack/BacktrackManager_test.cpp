
#include "utest_globals.hpp"

#include "SearchBacktrackTestInc.hpp"

OPTILAB_TEST(BacktrackManager, SaveRestoreStateTest)
{
  using namespace Search;
  
  std::shared_ptr<BacktrackableObjectUnitTest> bktObj1(new BacktrackableObjectUnitTest(10));
  std::shared_ptr<BacktrackableObjectUnitTest> bktObj2(new BacktrackableObjectUnitTest(100));
  
  std::unique_ptr<BacktrackManager> bktManager(new BacktrackManager());
  
  bktManager->attachBacktrackableObject(bktObj1);
  bktManager->attachBacktrackableObject(bktObj2);
  
  // Before any operation on the backtrack manager the level should be 0
  ASSERT_EQ(bktManager->getLevel(), 0);
  
  // Save state A
  bktManager->saveState();
  
  // Current top level of the stack should be 0
  // since state A has been saved at level 0
  ASSERT_EQ(bktManager->getLevel(), 0);
  
  ASSERT_EQ(bktObj1->getStateValue(), 10);
  ASSERT_EQ(bktObj2->getStateValue(), 100);
  
  bktObj1->setStateValue(1234);
  ASSERT_EQ(bktObj1->getStateValue(), 1234);
  
  // Save state B
  bktManager->saveState();
  // Current top level of the stack should be 0
  // since state A has been saved at level1
  ASSERT_EQ(bktManager->getLevel(), 1);
  
  bktObj2->setStateValue(8);
  ASSERT_EQ(bktObj2->getStateValue(), 8);
  
  // Restore state
  bktManager->restoreState();
  
  // After restore the top level of the stack should be 0
  ASSERT_EQ(bktManager->getLevel(), 0);
  
  // Should be state B
  ASSERT_EQ(bktObj1->getStateValue(), 1234);
  ASSERT_EQ(bktObj2->getStateValue(), 100);
}//SaveRestoreStateTest

OPTILAB_TEST(BacktrackManager, SaveRestoreStateUponLevelTest)
{
  /*
   * Save states, increase level and restore unti level 0
   * is reached, this should bring the states to their original values.
   */
  
  using namespace Search;
  
  std::shared_ptr<BacktrackableObjectUnitTest> bktObj1(new BacktrackableObjectUnitTest(10));
  std::shared_ptr<BacktrackableObjectUnitTest> bktObj2(new BacktrackableObjectUnitTest(100));
  
  std::unique_ptr<BacktrackManager> bktManager(new BacktrackManager());
  
  bktManager->attachBacktrackableObject(bktObj1);
  bktManager->attachBacktrackableObject(bktObj2);
  
  auto referenceLevel = bktManager->getLevel();
  
  // Save state A
  bktManager->saveState();
  
  // Change state of bktObj1
  bktObj1->setStateValue(1234);
  
  // Save state B
  bktManager->saveState();
  
  // Change state of bktObj2
  bktObj2->setStateValue(8);
  
  // Save state C
  bktManager->saveState();
  
  // Change state of bktObj1
  bktObj1->setStateValue(1010);
  
  // Save state D
  bktManager->saveState();
  
  // Change state of bktObj1 and bktObj2
  bktObj1->setStateValue(100000);
  bktObj2->setStateValue(100001);
  
  // Save state E
  bktManager->saveState();
  
  // Restore state E
  bktManager->restoreState();
  ASSERT_EQ(bktObj1->getStateValue(), 100000);
  ASSERT_EQ(bktObj2->getStateValue(), 100001);
  
  // Restore state D
  bktManager->restoreState();
  ASSERT_EQ(bktObj1->getStateValue(), 1010);
  ASSERT_EQ(bktObj2->getStateValue(), 8);
  
  // Restore state C
  bktManager->restoreState();
  ASSERT_EQ(bktObj1->getStateValue(), 1234);
  ASSERT_EQ(bktObj2->getStateValue(), 8);
  
  // Restore state B
  bktManager->restoreState();
  ASSERT_EQ(bktObj1->getStateValue(), 1234);
  ASSERT_EQ(bktObj2->getStateValue(), 100);
  
  // Restore state A
  bktManager->restoreState();
  ASSERT_EQ(bktObj1->getStateValue(), 10);
  ASSERT_EQ(bktObj2->getStateValue(), 100);
  
  ASSERT_EQ(bktManager->getLevel(), referenceLevel);
}//SaveRestoreStateUponLevelTest

OPTILAB_TEST(BacktrackManager, DoubleSaveStateNoForceSave)
{
  using namespace Search;
  
  std::shared_ptr<BacktrackableObjectUnitTest> bktObj(new BacktrackableObjectUnitTest(10));
  
  std::unique_ptr<BacktrackManager> bktManager(new BacktrackManager());
  
  bktManager->attachBacktrackableObject(bktObj);
  
  // Restore state without any saved state should return
  bktManager->restoreState();
  
  // State A
  ASSERT_EQ(bktObj->getStateValue(), 10);
  
  // State B
  bktObj->setStateValue(1234);
  ASSERT_EQ(bktObj->getStateValue(), 1234);
  
  // Restore state without any saved state should return
  bktManager->restoreState();
  ASSERT_EQ(bktObj->getStateValue(), 1234);
  
  // Save state twice -> nothing changed: should not save twice
  // since force save is false by default
  bktManager->saveState();
  bktManager->saveState();
  
  // State C
  bktObj->setStateValue(2345);
  
  // Should restore state B
  bktManager->restoreState();
  ASSERT_EQ(bktObj->getStateValue(), 1234);
  
  // State D
  bktObj->setStateValue(3456);
  
  // Should not restore state B since there was no
  // change into backtrackable object state between
  // the first and the second call to saveState
  bktManager->restoreState();
  ASSERT_EQ(bktObj->getStateValue(), 3456);
}//DoubleSaveStateNoForceSave

OPTILAB_TEST(BacktrackManager, DoubleSaveStateNoForceSaveChange)
{
  using namespace Search;
  
  std::shared_ptr<BacktrackableObjectUnitTest> bktObj(new BacktrackableObjectUnitTest(10));
  
  std::unique_ptr<BacktrackManager> bktManager(new BacktrackManager());
  
  bktManager->attachBacktrackableObject(bktObj);
  
  // Restore state without any saved state should return
  bktManager->restoreState();
  
  // State A
  ASSERT_EQ(bktObj->getStateValue(), 10);
  
  // State B
  bktObj->setStateValue(1234);
  ASSERT_EQ(bktObj->getStateValue(), 1234);
  
  // Restore state without any saved state should return
  bktManager->restoreState();
  ASSERT_EQ(bktObj->getStateValue(), 1234);
  
  // Save state twice -> nothing changed: should not save twice
  // since force save is false by default
  bktManager->saveState();
  bktObj->setStateValue(1010);
  bktManager->saveState();
  
  // State C
  bktObj->setStateValue(2345);
  
  // Should restore state B
  bktManager->restoreState();
  ASSERT_EQ(bktObj->getStateValue(), 1010);
  
  // State D
  bktObj->setStateValue(3456);
  
  // Should restore state B since there was a
  // change into backtrackable object state between
  // the first and the second call to saveState
  bktManager->restoreState();
  ASSERT_EQ(bktObj->getStateValue(), 1234);
}//DoubleSaveStateNoForceSaveChange

OPTILAB_TEST(BacktrackManager, DoubleSaveStateForceSave)
{
  using namespace Search;
  
  std::shared_ptr<BacktrackableObjectUnitTest> bktObj(new BacktrackableObjectUnitTest(10));
  
  std::unique_ptr<BacktrackManager> bktManager(new BacktrackManager());
  
  bktManager->attachBacktrackableObject(bktObj);
  
  // Restore state without any saved state should return
  bktManager->restoreState();
  
  // State A
  ASSERT_EQ(bktObj->getStateValue(), 10);
  
  // State B
  bktObj->setStateValue(1234);
  ASSERT_EQ(bktObj->getStateValue(), 1234);
  
  // Restore state without any saved state should return
  bktManager->restoreState();
  ASSERT_EQ(bktObj->getStateValue(), 1234);
  
  // Save state twice -> nothing changed: should not save twice
  // since force save is false by default
  bktManager->saveState();
  bktManager->saveState(true);
  
  // State C
  bktObj->setStateValue(2345);
  
  // Should restore state B
  bktManager->restoreState();
  ASSERT_EQ(bktObj->getStateValue(), 1234);
  
  // State D
  bktObj->setStateValue(3456);
  
  // Should restore state B since there was no
  // change into backtrackable object state between
  // the first and the second call to saveState but
  // the backtrack manager was forced to save the state
  // of all attached backtrackable objects
  bktManager->restoreState();
  ASSERT_EQ(bktObj->getStateValue(), 1234);
  
  // Calling restore twice shouldn't affect the current state
  // since the trailstack level is 0
  bktManager->restoreState();
  ASSERT_EQ(bktObj->getStateValue(), 1234);
}//DoubleSaveStateForceSave
