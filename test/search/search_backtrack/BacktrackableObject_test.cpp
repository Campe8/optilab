
#include "utest_globals.hpp"

#include "SearchBacktrackTestInc.hpp"
#include "BacktrackManager.hpp"

OPTILAB_TEST(BacktrackableObjectTest, ID)
{
  using namespace Search;
  
  std::unique_ptr<BacktrackableObjectUnitTest> bk1(new BacktrackableObjectUnitTest(10));
  std::unique_ptr<BacktrackableObjectUnitTest> bk2(new BacktrackableObjectUnitTest(10));
  ASSERT_FALSE(bk1->getUUID() == bk2->getUUID());
}//ID

OPTILAB_TEST(BacktrackableObjectTest, AttachDetachCaretaker)
{
  using namespace Search;
  
  std::shared_ptr<BacktrackableObjectUnitTest> bkObj(new BacktrackableObjectUnitTest(10));
  ASSERT_FALSE(bkObj->isAttachedToCaretaker());
  
  std::unique_ptr<BacktrackManager> bktManager(new BacktrackManager());
  bktManager->attachBacktrackableObject(bkObj);
  ASSERT_TRUE(bkObj->isAttachedToCaretaker());
  
  bktManager->detachBacktrackableObject(bkObj);
  ASSERT_FALSE(bkObj->isAttachedToCaretaker());
}//AttachDetachCaretaker

OPTILAB_TEST(BacktrackableObjectTest, GetSetMemento)
{
  using namespace Search;
  
  int bktObjState = 10;
  
  std::unique_ptr<BacktrackableObjectUnitTest> bk1(new BacktrackableObjectUnitTest(bktObjState));
  ASSERT_EQ(bk1->getStateValue(), bktObjState);
  
  std::unique_ptr<Memento> memento = bk1->getMemento();
  ASSERT_TRUE(memento->hasValidState());
  ASSERT_EQ(bk1->getStateValue(), bktObjState);
  
  // Delete memento shouldn't affect bk1
  memento.reset(nullptr);
  ASSERT_EQ(bk1->getStateValue(), bktObjState);
  
  memento = bk1->getMemento();
  ASSERT_TRUE(memento->hasValidState());
  
  // Change internal object state
  bk1->setStateValue(100);
  ASSERT_EQ(bk1->getStateValue(), 100);
  
  // Reset old memento
  bk1->setMemento(std::move(memento));
  // Memento is no longer valid
  ASSERT_FALSE(memento);
  // bk1 has its old state
  ASSERT_EQ(bk1->getStateValue(), bktObjState);
}//GetSetMemento

