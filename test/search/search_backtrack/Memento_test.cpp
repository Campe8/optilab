
#include "utest_globals.hpp"

#include "SearchBacktrackTestInc.hpp"

OPTILAB_TEST(MementoTest, MementoStateUnitTest)
{
  using namespace Search;
  
  std::unique_ptr<MementoStateUnitTest> state(new MementoStateUnitTest(10));
  ASSERT_EQ(state->getValue(), 10);
  
  state->setValue(100);
  ASSERT_EQ(state->getValue(), 100);
}//MementoStateUnitTest

OPTILAB_TEST(MementoTest, BaseMethods)
{
  using namespace Search;
  
  std::unique_ptr<MementoUnitTest> memento(new MementoUnitTest());
  ASSERT_FALSE(memento->hasValidState());
  
  std::unique_ptr<MementoUnitTest> memento2(new MementoUnitTest(*memento.get()));
  ASSERT_FALSE(memento2->hasValidState());
}//BaseMethods

OPTILAB_TEST(MementoTest, GetSetMethods)
{
  using namespace Search;
  
  std::unique_ptr<MementoState> state(new MementoStateUnitTest(10));
  std::unique_ptr<MementoUnitTest> memento(new MementoUnitTest());
  
  memento->setState(std::move(state));
  ASSERT_FALSE(state);
  ASSERT_TRUE(memento->hasValidState());
  
  ASSERT_TRUE(memento->getState());
  ASSERT_TRUE(memento->hasValidState());
  ASSERT_TRUE(MementoStateUnitTest::isa(memento->getState()));
  ASSERT_EQ(MementoStateUnitTest::cast(memento->getState())->getValue(), 10);
}//GetSetMethods
