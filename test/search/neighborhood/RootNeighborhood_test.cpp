
#include "utest_globals.hpp"

#include "RootNeighborhood.hpp"

#include "BranchingStrategyRegister.hpp"
#include "BranchingStrategyBinaryChoicePoint.hpp"
#include "BaseTools.hpp"

#include "SearchHeuristic.hpp"

#include "VariableFactory.hpp"
#include "VariableInteger.hpp"
#include "VariableSemanticDecision.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementVoid.hpp"
#include "DomainElementManager.hpp"

#include <algorithm>
#include <numeric>
#include <cassert>

namespace {
  
  using namespace Search;
  
  class RootNeighborhoodTest : public ::testing::Test {
  public:
    void SetUp() override
    {
      pSearchHeuristic = nullptr;
      pRandomGenerator = std::make_shared<Base::Tools::RandomGenerator>(0);
    }
    
    inline Base::Tools::RandomGeneratorSPtr getRandomGenerator() const { return pRandomGenerator; }
    
    SearchHeuristicSPtr searchSpace(const std::vector<std::pair<INT_64, INT_64>>& aDomains)
    {
      using namespace Core;
      using namespace Search;
      
      std::set<Core::VariableSPtr> BranchingVariableSet;
      std::unique_ptr<VariableFactory> varFactory(new VariableFactory());
      
      // Fill branching variable set
      std::string varID{"V"};
      for(std::size_t idx{0}; idx < aDomains.size(); ++idx)
      {
        // Semantic
        VariableSemanticUPtr varSem(new VariableSemanticDecision(varID + std::to_string(idx)));
        varSem->setInputOrder(idx);
        
        // Variable
        VariableSPtr var(varFactory->variableInteger(aDomains[idx].first, aDomains[idx].second, std::move(varSem)));
        
        // Add variable to the list of variables
        pVarList.push_back(var);
      }
      
      
      // Create the search heuristic
      pSearchHeuristic =
      std::make_shared<SearchHeuristic>(pVarList, VariableChoiceMetricType::VAR_CM_INPUT_ORDER,
                                        ValueChoiceMetricType::VAL_METRIC_INDOMAIN_RANDOM);
      
      return pSearchHeuristic;
    }
    
    std::vector<Core::VariableSPtr> getSearchSpace() const { return pVarList; }
    
    NeighborhoodSpec getNeighborhoodSpec(int aSeed, std::size_t aSize)
    {
      return Internal::randomNeighborhoodSelector(&pVarList, aSize, pRandomGenerator.get());
    }//getNeighborhoodSpec
    
  private:
    /// List of variables
    std::vector<Core::VariableSPtr> pVarList;
    
    /// Search heuristic
    SearchHeuristicSPtr pSearchHeuristic;
    
    /// Random number generator
    Base::Tools::RandomGeneratorSPtr pRandomGenerator;
  };
  
}// end namespace

OPTILAB_TEST_F(RootNeighborhoodTest, staticMethods)
{
  // Test static methods
  using namespace Semantics;
  using namespace Search;
  
  auto heuristic = searchSpace({{1, 1}, {2, 2}});
  std::unique_ptr<RootNeighborhood> root(new RootNeighborhood(heuristic, 1, 1,
                                                              getRandomGenerator()));
  ASSERT_TRUE(root);
  
  // Node type
  ASSERT_EQ(SearchChoiceType::SEARCH_CHOICE_NEIGHBORHOOD, root->getSearchChoiceType());
  
  // Isa and cast
  ASSERT_TRUE(Neighborhood::isa(root.get()));
  ASSERT_EQ(root.get(), Neighborhood::cast(root.get()));
  
  SearchChoice* choice = static_cast<SearchChoice*>(root.get());
  ASSERT_TRUE(Neighborhood::isa(choice));
  ASSERT_EQ(root.get(), Neighborhood::cast(choice));
}//staticMethods

OPTILAB_TEST_F(RootNeighborhoodTest, BaseMethods)
{
  // Test set/get neighborhood from root neighborhood
  using namespace Semantics;
  using namespace Search;
  
  auto heuristic = searchSpace({{1, 10}, {2, 10}, {3, 10}, {4, 10}});
  std::unique_ptr<RootNeighborhood> root(new RootNeighborhood(heuristic, 2, 2,
                                                              getRandomGenerator()));
  
  auto neighborhood1 = root->getNeighborhood();
  ASSERT_EQ(2, neighborhood1.size());
  ASSERT_TRUE(root->hasSuccessor());
  
  std::unique_ptr<Neighborhood> succ(root->next());
  auto neighborhood2 = succ->getNeighborhood();
  ASSERT_EQ(2, neighborhood2.size());
  ASSERT_FALSE(succ->hasSuccessor());
}//BaseMethods
