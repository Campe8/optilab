
#include "utest_globals.hpp"

#include "Neighborhood.hpp"

#include "BaseTools.hpp"

#include "BranchingStrategyRegister.hpp"
#include "BranchingStrategyBinaryChoicePoint.hpp"

#include "SearchHeuristic.hpp"

#include "VariableFactory.hpp"
#include "VariableInteger.hpp"
#include "VariableSemanticDecision.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementVoid.hpp"
#include "DomainElementManager.hpp"

#include <algorithm>
#include <memory>
#include <numeric>
#include <cassert>

namespace Search {
  
  class NeighborhoodMock : public Neighborhood {
  public:
    NeighborhoodMock(std::size_t aNeighborhoodId,
                     std::size_t aNumNeighborhoods,
                     NeighborhoodSelector aNeighborhoodSelector,
                     Base::Tools::RandomGenerator* aRandomGenerator)
    : Neighborhood(aNeighborhoodId, aNumNeighborhoods, aNeighborhoodSelector, aRandomGenerator)
    {
    }
    
    NeighborhoodMock(NeighborhoodSearchSpace* aSearchSpace,
                     SearchHeuristicPtr aSearchHeuristic,
                     const NeighborhoodSpec& aNeighborhoodSpec,
                     std::size_t aNeighborhoodId,
                     std::size_t aNumNeighborhoods,
                     NeighborhoodSelector aNeighborhoodSelector,
                     Base::Tools::RandomGenerator* aRandomGenerator)
    : Neighborhood(aSearchSpace, aSearchHeuristic, aNeighborhoodSpec,
                   aNeighborhoodId, aNumNeighborhoods, aNeighborhoodSelector, aRandomGenerator)
    {
    }
    
    inline void setNeighborhoodSpec(const NeighborhoodSpec& aNeighborhoodSpec)
    {
      Neighborhood::setNeighborhoodSpec(aNeighborhoodSpec);
    }
    
    const NeighborhoodSpec& getNeighborhoodSpec() const
    {
      return Neighborhood::getNeighborhoodSpec();
    }
    
    inline void setSearchHeuristic(SearchHeuristicPtr aSearchHeuristic)
    {
      Neighborhood::setSearchHeuristic(aSearchHeuristic);
    }
    
    void extendNeighborhood()
    {
      Neighborhood::extendNeighborhood();
    }//extendNeighborhood
  };
  
}// end namespace Search

namespace {
  
  using namespace Search;
  
  class NeighborhoodTest : public ::testing::Test {
  public:
    void SetUp() override {
      pRandomGenerator.reset(new Base::Tools::RandomGenerator(0));
    }
    
    inline Base::Tools::RandomGenerator* getRandomGenerator() const
    {
      return pRandomGenerator.get();
    }
    
    SearchHeuristicSPtr searchHeuristic(const std::vector<std::pair<INT_64, INT_64>>& aDomains)
    {
      using namespace Core;
      using namespace Search;
      
      std::set<Core::VariableSPtr> BranchingVariableSet;
      std::unique_ptr<VariableFactory> varFactory(new VariableFactory());
      
      // Fill branching variable set
      std::string varID{"V"};
      for(std::size_t idx{0}; idx < aDomains.size(); ++idx)
      {
        // Semantic
        VariableSemanticUPtr varSem(new VariableSemanticDecision(varID + std::to_string(idx)));
        varSem->setInputOrder(idx);
        
        // Variable
        VariableSPtr var(varFactory->variableInteger(aDomains[idx].first, aDomains[idx].second, std::move(varSem)));
        
        // Add variable to the list of variables
        pVarList.push_back(var);
        
        // Add variables to the set of branching variables for the heuristic
        BranchingVariableSet.insert(var);
      }
      
      // Create heuristic
      SearchHeuristicSPtr searchHeuristic =
      std::make_shared<SearchHeuristic>(pVarList,
                                        VariableChoiceMetricType::VAR_CM_INPUT_ORDER,
                                        ValueChoiceMetricType::VAL_METRIC_INDOMAIN_RANDOM);
      
      return searchHeuristic;
    }
    
    inline std::vector<Core::VariableSPtr> getVariableList() const { return pVarList; }
    
    NeighborhoodSpec getNeighborhoodSpec(int aSeed, std::size_t aSize)
    {
      return Internal::randomNeighborhoodSelector(&pVarList, aSize, getRandomGenerator());
    }//getNeighborhoodSpec
    
    NeighborhoodMock* getNeighborhood(std::size_t aNumNeighborhoods)
    {
      return new NeighborhoodMock(0, aNumNeighborhoods, Internal::randomNeighborhoodSelector,
                                  pRandomGenerator.get());
    }
    
    NeighborhoodMock* getNeighborhood(const SearchHeuristicSPtr& aSearchHeuristic,
                                      const NeighborhoodSpec& aNeighborhoodSpec,
                                      std::size_t aNumNeighborhoods)
    {
      return new NeighborhoodMock(&pVarList, aSearchHeuristic.get(),
                                  aNeighborhoodSpec, 0, aNumNeighborhoods,
                                  Internal::randomNeighborhoodSelector,
                                  pRandomGenerator.get());
    }
    
  private:
    /// List of variables
    std::vector<Core::VariableSPtr> pVarList;
    
    /// Random number generator
    std::unique_ptr<Base::Tools::RandomGenerator> pRandomGenerator;
  };
  
}// end namespace

OPTILAB_TEST_F(NeighborhoodTest, staticMethods)
{
  // Test static methods
  using namespace Semantics;
  using namespace Search;
  
  SearchHeuristicSPtr sh = searchHeuristic({{1, 1}, {2, 2}});
  
  std::unique_ptr<NeighborhoodMock> neighborhood(getNeighborhood(sh, getNeighborhoodSpec(0,1), 1));
  ASSERT_TRUE(neighborhood);
  
  // Node type
  ASSERT_EQ(SearchChoiceType::SEARCH_CHOICE_NEIGHBORHOOD, neighborhood->getSearchChoiceType());
  
  // Isa and cast
  ASSERT_TRUE(Neighborhood::isa(neighborhood.get()));
  ASSERT_EQ(neighborhood.get(), Neighborhood::cast(neighborhood.get()));
  
  SearchChoice* choice = static_cast<SearchChoice*>(neighborhood.get());
  ASSERT_TRUE(Neighborhood::isa(choice));
  ASSERT_EQ(neighborhood.get(), Neighborhood::cast(choice));
}//staticMethods

OPTILAB_TEST_F(NeighborhoodTest, BaseMethods)
{
  // Test set/get neighborhood specification
  using namespace Semantics;
  using namespace Search;
  
  SearchHeuristicSPtr sh = searchHeuristic({{1, 1}, {2, 2}, {3, 3}, {4, 4}, {5, 5}});
  
  auto spec = getNeighborhoodSpec(1, 3);
  std::unique_ptr<NeighborhoodMock> neighborhood(getNeighborhood(sh, spec, 2));
  
  ASSERT_EQ(sh.get(), neighborhood->getSearchHeuristic());
  ASSERT_TRUE(neighborhood->getSearchSpace());
  ASSERT_EQ(5, neighborhood->getSearchSpace()->size());
  
  // Check the current spec
  const auto& nspec = neighborhood->getNeighborhoodSpec();
  
  ASSERT_EQ(spec.size(), nspec.size());
  for(std::size_t idx{0}; idx < spec.size(); ++idx)
  {
    ASSERT_EQ(spec[idx], nspec[idx]);
  }
  
  // Set a new neighborhood specification
  NeighborhoodSpec newSpec;
  newSpec.push_back(0);
  newSpec.push_back(1);
  newSpec.push_back(2);
  newSpec.push_back(3);
  neighborhood->setNeighborhoodSpec(newSpec);
  
  auto newSpecSet = neighborhood->getNeighborhoodSpec();
  ASSERT_EQ(newSpec.size(), newSpecSet.size());
  for(std::size_t idx{0}; idx < spec.size(); ++idx)
  {
    ASSERT_EQ(newSpec[idx], newSpecSet[idx]);
  }
}//BaseMethods

OPTILAB_TEST_F(NeighborhoodTest, NextNeighborhood)
{
  // Test get next neighborhood
  using namespace Semantics;
  using namespace Search;
  
  SearchHeuristicSPtr sh = searchHeuristic({{1, 1}, {2, 2}, {3, 3}, {4, 4}, {5, 5}});
  
  auto spec = getNeighborhoodSpec(1, 3);
  ASSERT_EQ(3, spec.size());
  
  std::unique_ptr<NeighborhoodMock> neighborhood(getNeighborhood(sh, spec, 2));
  
  // This neighborhood should have one successor
  ASSERT_TRUE(neighborhood->hasSuccessor());
  
  // Next neighborhood should be the last one
  std::unique_ptr<Neighborhood> nextNeighborhood(neighborhood->next());
  ASSERT_FALSE(nextNeighborhood->hasSuccessor());
  ASSERT_FALSE(nextNeighborhood->next());
}//NextNeighborhood

OPTILAB_TEST_F(NeighborhoodTest, NextNeighborhoodFullSize)
{
  // Test get next neighborhood with neighborhood size equal
  // to the size of the search space
  using namespace Semantics;
  using namespace Search;
  
  SearchHeuristicSPtr sh = searchHeuristic({{1, 1}, {2, 2}, {3, 3}, {4, 4}, {5, 5}});
  
  auto spec = getNeighborhoodSpec(1, 5);
  ASSERT_EQ(5, spec.size());
  
  std::unique_ptr<NeighborhoodMock> neighborhood(getNeighborhood(sh, spec, 2));
  
  // This neighborhood should have one successor
  ASSERT_TRUE(neighborhood->hasSuccessor());
  
  // Next neighborhood should be the last one
  std::unique_ptr<Neighborhood> nextNeighborhood(neighborhood->next());
  ASSERT_FALSE(nextNeighborhood->hasSuccessor());
  ASSERT_FALSE(nextNeighborhood->next());
}//NextNeighborhoodFullSize

OPTILAB_TEST_F(NeighborhoodTest, getNeighborhood)
{
  // Test get neighborhood
  using namespace Semantics;
  using namespace Search;
  
  SearchHeuristicSPtr sh = searchHeuristic({{1, 1}, {2, 2}, {3, 3}, {4, 4}, {5, 5}});
  
  // Force the spec
  NeighborhoodSpec spec;
  spec.push_back(4);
  spec.push_back(3);
  spec.push_back(2);
  
  std::unique_ptr<NeighborhoodMock> neighborhood(getNeighborhood(sh, spec, 1));
  
  // Returned variables should be according to the spec
  const auto& nvars = neighborhood->getNeighborhood();
  ASSERT_EQ(spec.size(), nvars.size());
  ASSERT_EQ(getVariableList()[4].get(), nvars[0].get());
  ASSERT_EQ(getVariableList()[3].get(), nvars[1].get());
  ASSERT_EQ(getVariableList()[2].get(), nvars[2].get());
}//NextNeighborhoodFullSize
