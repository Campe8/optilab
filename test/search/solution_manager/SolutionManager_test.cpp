
#include "utest_globals.hpp"

#include "SolutionManager.hpp"
#include "VariableFactory.hpp"
#include "VariableInteger.hpp"
#include "SearchEnvironment.hpp"
#include "VariableSemanticDecision.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"

namespace Search {
  class SolutionManagerTest : public SolutionManager
  {
  public:
    SolutionManagerTest(const SolutionManager::VarMapSPtr& aVarMap)
    : SolutionManager(aVarMap)
    {
      std::vector<SearchEnvironment::VarSem> env;
      for(auto& it : *aVarMap)
      {
        env.push_back({it.second, Core::VariableSemanticSPtr(it.second->variableSemantic()->clone())});
      }
      pSearchEnv = std::make_shared<SearchEnvironment>(env, VariableChoiceMetricType::VAR_CM_INPUT_ORDER, ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MIN);
    
      setSearchEnvironment(pSearchEnv.get());
    }
    
    // Expose protected methods
    void resetRegister()
    {
      SolutionManager::resetRegister();
    }
    
  private:
    std::shared_ptr<SearchEnvironment> pSearchEnv;
  };
}

OPTILAB_TEST(SolutionManager, VerifySolutionStatusForNonGroundBranchingVariables)
{
  using namespace Core;
  using namespace Search;
  
  VariableFactoryUPtr varFactory(new VariableFactory());
  
  // Test base class methods
  VariableSemanticUPtr semanticDecision1(new VariableSemanticDecision("Var1"));
  VariableSemanticUPtr semanticDecision2(new VariableSemanticDecision("Var2"));
  
  // Set branching and input order semantics
  semanticDecision2->setInputOrder(0);
  semanticDecision1->setInputOrder(1);
  semanticDecision1->setBranchingPolicy(true);
  semanticDecision2->setBranchingPolicy(true);
  
  VariableSPtr varInt1(varFactory->variableInteger(10, 12, std::move(semanticDecision1)));
  VariableSPtr varInt2(varFactory->variableInteger(11, 15, std::move(semanticDecision2)));
  
  SolutionManager::VarMapSPtr vm = std::make_shared<Model::ModelVarMap>();
  (*vm)[varInt1->getVariableNameId()] = varInt1;
  (*vm)[varInt2->getVariableNameId()] = varInt2;
  SolutionManagerTest solManager(vm);
  
  // Default no recorded solutions
  ASSERT_EQ(solManager.getNumberOfRecordedSolutions(), 0);
  
  // Default solution limit is 1
  ASSERT_EQ(solManager.getSolutionLimit(), 1);
  
  // Non ground branching variables: non valid status
  auto solStatus = solManager.notify();
  ASSERT_EQ(solStatus, SolutionStatus::SOLUTION_NOT_VALID);
}//VerifySolutionStatusForNonGroundBranchingVariables

OPTILAB_TEST(SolutionManager, VerifySolutionStatusForNonGroundNonBranchingVariables)
{
  using namespace Core;
  using namespace Search;
  
  VariableFactoryUPtr varFactory(new VariableFactory());
  
  // Test base class methods
  VariableSemanticUPtr semanticDecision1(new VariableSemanticDecision("Var1"));
  VariableSemanticUPtr semanticDecision2(new VariableSemanticDecision("Var2"));
  
  // Set input order semantics (no branching)
  semanticDecision2->setInputOrder(0);
  semanticDecision1->setInputOrder(1);
  
  VariableSPtr varInt1(varFactory->variableInteger(10, 12, std::move(semanticDecision1)));
  VariableSPtr varInt2(varFactory->variableInteger(11, 15, std::move(semanticDecision2)));
  
  SolutionManager::VarMapSPtr vm = std::make_shared<Model::ModelVarMap>();
  (*vm)[varInt1->getVariableNameId()] = varInt1;
  (*vm)[varInt2->getVariableNameId()] = varInt2;
  SolutionManagerTest solManager(vm);
  
  // Default no recorded solutions
  ASSERT_EQ(solManager.getNumberOfRecordedSolutions(), 0);
  
  // Default solution limit is 1
  ASSERT_EQ(solManager.getSolutionLimit(), 1);
  
  // Non ground non branching variables: non valid status
  auto solStatus = solManager.notify();
  ASSERT_EQ(solStatus, SolutionStatus::SOLUTION_LAST);
}//VerifySolutionStatusForNonGroundNonBranchingVariables

OPTILAB_TEST(SolutionManager, RegisterSolutions)
{
  using namespace Core;
  using namespace Search;
  
  VariableFactoryUPtr varFactory(new VariableFactory());
  
  // Test base class methods
  VariableSemanticUPtr semanticDecision1(new VariableSemanticDecision("Var1"));
  VariableSemanticUPtr semanticDecision2(new VariableSemanticDecision("Var2"));
  semanticDecision1->setBranchingPolicy(true);
  semanticDecision2->setBranchingPolicy(true);
  
  // Set input order for search heuristic
  semanticDecision2->setInputOrder(0);
  semanticDecision1->setInputOrder(1);
  
  VariableSPtr varInt1(varFactory->variableInteger(10, 10, std::move(semanticDecision1)));
  VariableSPtr varInt2(varFactory->variableInteger(11, 11, std::move(semanticDecision2)));
  SolutionManager::VarMapSPtr vm = std::make_shared<Model::ModelVarMap>();
  (*vm)[varInt1->getVariableNameId()] = varInt1;
  (*vm)[varInt2->getVariableNameId()] = varInt2;
  SolutionManagerTest solManager(vm);

  solManager.setSolutionLimit(2);
  
  auto solStatus = solManager.notify();
  ASSERT_EQ(solStatus, SolutionStatus::SOLUTION_NEED_MORE);
  
  solStatus = solManager.notify();
  ASSERT_EQ(solStatus, SolutionStatus::SOLUTION_LAST);
  ASSERT_EQ(solManager.getNumberOfRecordedSolutions(), 2);

  // Reset register
  solManager.resetRegister();
  
  ASSERT_EQ(solManager.getNumberOfRecordedSolutions(), 0);
  
  solStatus = solManager.notify();
  ASSERT_EQ(solStatus, SolutionStatus::SOLUTION_NEED_MORE);
  
  solStatus = solManager.notify();
  ASSERT_EQ(solStatus, SolutionStatus::SOLUTION_LAST);
  ASSERT_EQ(solManager.getNumberOfRecordedSolutions(), 2);
  
  auto solution = solManager.getSolution(1);
  ASSERT_EQ(solution.size(), 2);
  ASSERT_TRUE(solution.at(varInt1->getVariableNameId())[0].first->isEqual(DomainElementManager::getInstance().createDomainElementInt64(10)));
  ASSERT_TRUE(solution.at(varInt2->getVariableNameId())[0].first->isEqual(DomainElementManager::getInstance().createDomainElementInt64(11)));
  ASSERT_TRUE(solution.at(varInt1->getVariableNameId())[0].second->isEqual(DomainElementManager::getInstance().createDomainElementInt64(10)));
  ASSERT_TRUE(solution.at(varInt2->getVariableNameId())[0].second->isEqual(DomainElementManager::getInstance().createDomainElementInt64(11)));
  
  solution = solManager.getSolution(2);
  ASSERT_TRUE(solution.at(varInt1->getVariableNameId())[0].first->isEqual(DomainElementManager::getInstance().createDomainElementInt64(10)));
  ASSERT_TRUE(solution.at(varInt2->getVariableNameId())[0].first->isEqual(DomainElementManager::getInstance().createDomainElementInt64(11)));
  ASSERT_TRUE(solution.at(varInt1->getVariableNameId())[0].second->isEqual(DomainElementManager::getInstance().createDomainElementInt64(10)));
  ASSERT_TRUE(solution.at(varInt2->getVariableNameId())[0].second->isEqual(DomainElementManager::getInstance().createDomainElementInt64(11)));
}//RegisterSolutions

OPTILAB_TEST(SolutionManager, NoSolutionLimitToSolutionLimit)
{
  using namespace Core;
  using namespace Search;
  
  VariableFactoryUPtr varFactory(new VariableFactory());
  
  // Test base class methods
  VariableSemanticUPtr semanticDecision1(new VariableSemanticDecision("Var1"));
  VariableSemanticUPtr semanticDecision2(new VariableSemanticDecision("Var2"));
  semanticDecision1->setBranchingPolicy(true);
  semanticDecision2->setBranchingPolicy(true);
  
  // Set input order for search heuristic
  semanticDecision2->setInputOrder(0);
  semanticDecision1->setInputOrder(1);
  
  VariableSPtr varInt1(varFactory->variableInteger(10, 10, std::move(semanticDecision1)));
  VariableSPtr varInt2(varFactory->variableInteger(11, 11, std::move(semanticDecision2)));
  SolutionManager::VarMapSPtr vm = std::make_shared<Model::ModelVarMap>();
  (*vm)[varInt1->getVariableNameId()] = varInt1;
  (*vm)[varInt2->getVariableNameId()] = varInt2;
  SolutionManagerTest solManager(vm);
  
  solManager.setSolutionLimit(-1);
  
  auto solStatus = solManager.notify();
  ASSERT_EQ(solStatus, SolutionStatus::SOLUTION_NEED_MORE);
  
  solStatus = solManager.notify();
  ASSERT_EQ(solStatus, SolutionStatus::SOLUTION_NEED_MORE);
  ASSERT_EQ(solManager.getNumberOfRecordedSolutions(), 2);
  
  // Reset register
  solManager.resetRegister();
  
  // Set limit after calling reset should set the new limit
  solManager.setSolutionLimit(+2);
  ASSERT_EQ(solManager.getNumberOfRecordedSolutions(), 0);
  
  solStatus = solManager.notify();
  ASSERT_EQ(solStatus, SolutionStatus::SOLUTION_NEED_MORE);
  
  solStatus = solManager.notify();
  ASSERT_EQ(solStatus, SolutionStatus::SOLUTION_LAST);
  ASSERT_EQ(solManager.getNumberOfRecordedSolutions(), 2);
}//NoSolutionLimitToSolutionLimit
