
#include "utest_globals.hpp"

#include "BranchingStrategyRegister.hpp"

OPTILAB_TEST(BranchingStrategyRegister, BaseMethods)
{
  using namespace Search;
  std::unique_ptr<BranchingStrategyRegister> brcRegister(new BranchingStrategyRegister());
  
  BranchingStrategyType strType = BranchingStrategyType::BRANCHING_BINARY_CHOICE_POINT;
  BranchingStrategyPtr  strPtr  = brcRegister->branchingStrategy(strType);
  ASSERT_TRUE(strPtr);
  ASSERT_EQ(strPtr->getBranchingStrategyType(), strType);
  
  strType = BranchingStrategyType::BRANCHING_DOMAIN_SPLITTING;
  strPtr  = brcRegister->branchingStrategy(strType);
  ASSERT_TRUE(strPtr);
  ASSERT_EQ(strPtr->getBranchingStrategyType(), strType);
}//BaseMethods
