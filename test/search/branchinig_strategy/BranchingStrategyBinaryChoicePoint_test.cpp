
#include "utest_globals.hpp"

#include <vector>

#include "BranchingStrategyRegister.hpp"
#include "BranchingStrategyBinaryChoicePoint.hpp"

#include "SearchHeuristic.hpp"

#include "VariableFactory.hpp"
#include "VariableInteger.hpp"
#include "VariableSemanticDecision.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementVoid.hpp"
#include "DomainElementManager.hpp"

OPTILAB_TEST(BranchingStrategyBinaryChoicePoint, BaseMethods)
{
  using namespace Search;
  using namespace Core;
  
  VariableFactoryUPtr varFactory(new VariableFactory());
  
  // Test base class methods
  VariableSemanticUPtr semanticDecision1(new VariableSemanticDecision());
  VariableSemanticUPtr semanticDecision2(new VariableSemanticDecision());
  
  // Set input order for search heuristic
  semanticDecision2->setInputOrder(0);
  semanticDecision1->setInputOrder(1);
  
  VariableSPtr varInt1(varFactory->variableInteger(10, 12, std::move(semanticDecision1)));
  VariableSPtr varInt2(varFactory->variableInteger(11, 15, std::move(semanticDecision2)));
  std::vector<Core::VariableSPtr> branchingList{varInt1, varInt2};
  
  auto varChoice = VariableChoiceMetricType::VAR_CM_INPUT_ORDER;
  auto valChoice = ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MIN;
  SearchHeuristicSPtr searchHeuristic = std::make_shared<SearchHeuristic>(branchingList, varChoice,
                                                                          valChoice);
  
  std::unique_ptr<BranchingStrategyRegister> brcRegister(new BranchingStrategyRegister());
  
  BranchingStrategyType strType = BranchingStrategyType::BRANCHING_BINARY_CHOICE_POINT;
  BranchingStrategyPtr  strPtr  = brcRegister->branchingStrategy(strType);
  ASSERT_EQ(strPtr->getBranchingStrategyType(), strType);
  
  BranchingChoiceSet brcSet = strPtr->getBranchingSet(searchHeuristic.get());
  
  // Using input order: branching variable should be varInt2
  ASSERT_EQ(brcSet.first.get(), varInt2.get());
  
  // There should be 2 branching constraints
  ASSERT_EQ(brcSet.second.size(), 2);
  
  // Check branching constraint id
  ASSERT_EQ(brcSet.second[0]->getBranchingConstraintId(), BranchingConstraintId::BRC_CON_ID_BINARY_CHOICE_EQ);
  ASSERT_EQ(brcSet.second[1]->getBranchingConstraintId(), BranchingConstraintId::BRC_CON_ID_BINARY_CHOICE_NQ);
  
  // Check actual (base) constraint id
  ASSERT_EQ(brcSet.second[0]->getConstraintId(), ConstraintId::CON_ID_BRANCHING);
  ASSERT_EQ(brcSet.second[1]->getConstraintId(), ConstraintId::CON_ID_BRANCHING);
}//BaseMethods
