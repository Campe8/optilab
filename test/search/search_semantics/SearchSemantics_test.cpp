
#include "utest_globals.hpp"

#include "SearchSemantics.hpp"

#include "VariableSemanticObjective.hpp"
#include "VariableFactory.hpp"
#include "DomainElementInt64.hpp"

OPTILAB_TEST(SearchSemantics, initMethods)
{
  using namespace Search;
  
  SearchSemantics ss;
  
  // Default constraint store instantiated in search semantics
  auto cStore = ss.constraintStore();
  ASSERT_TRUE(cStore);

  // Default should not have an objective variable
  ASSERT_FALSE(ss.hasVariableObjective());
  ASSERT_FALSE(ss.variableObjective());
  
  // Default there is no objective value
  ASSERT_TRUE(ss.getObjectiveValue().empty());
  
  // Default no objective has been set
  ASSERT_FALSE(ss.isObjectiveValueSet());
}//initMethods

OPTILAB_TEST(SearchSemantics, setObjectiveVariable)
{
  // Test set/get objective variable and value
  using namespace Core;
  using namespace Search;
  
  // Get the constraint store and check that, by default, there are
  // no registered constraints
  SearchSemantics ss;
  auto cStore = ss.constraintStore();
  ASSERT_EQ(cStore->getNumRegisteredConstraints(), 0);
  
  // Create objective variable
  std::string varName{"test"};
  VariableFactoryUPtr varFactory(new VariableFactory());
  
  auto objExt = VariableSemanticObjective::ObjectiveExtremum::OBJ_EXT_MINIMIZE;
  auto semantic = new VariableSemanticObjective(objExt, varName);
  VariableSemanticUPtr sobj(semantic);
  VariableSPtr varObj(varFactory->variableInteger(10, std::move(sobj)));
  
  // Optimizing without registering any objective variable shouldn't do anything
  ss.optimizeObjective();
  ASSERT_EQ(cStore->getNumRegisteredConstraints(), 0);
  
  // Set objective variable
  ss.setVariableObjective(varObj);
  ASSERT_TRUE(ss.hasVariableObjective());
  
  // Setting the variable objective should not affect the constraint store
  ASSERT_EQ(cStore->getNumRegisteredConstraints(), 0);
  
  // Create an objective value (i.e., a solution) to optimize on
  SearchSemantics::ObjectiveValue objVal;
  objVal[varName] = { { varObj->domain()->lowerBound(), varObj->domain()->upperBound() } };
  ss.setObjectiveValue(objVal);
  
  // Objective value has been set
  ASSERT_TRUE(ss.isObjectiveValueSet());
  
  // Check the objective value
  ASSERT_FALSE(ss.getObjectiveValue().empty());
  ASSERT_TRUE(ss.getObjectiveValue().find(varName) != ss.getObjectiveValue().end());
  
  // Optimize on objective
  ss.optimizeObjective();
  ASSERT_EQ(cStore->getNumRegisteredConstraints(), 1);
  
  // Optimizing twice won't introduce new constraints
  ss.optimizeObjective();
  ASSERT_EQ(cStore->getNumRegisteredConstraints(), 1);
  
  ss.cleanupOptimizationObjective();
  ASSERT_EQ(cStore->getNumRegisteredConstraints(), 0);
}//setObjectiveVariable

OPTILAB_TEST(SearchSemantics, optimizeObjectiveMinimize)
{
  // Test minimization on objective variable
  
  using namespace Core;
  using namespace Search;
  
  SearchSemantics ss;
  
  std::string varName{"test"};
  VariableFactoryUPtr varFactory(new VariableFactory());
  
  auto objExt = VariableSemanticObjective::ObjectiveExtremum::OBJ_EXT_MINIMIZE;
  auto semantic = new VariableSemanticObjective(objExt, varName);
  VariableSemanticUPtr sobj(semantic);
  VariableSPtr varObj(varFactory->variableInteger(10, 20, std::move(sobj)));
  ss.setVariableObjective(varObj);
  
  SearchSemantics::ObjectiveValue objVal;
  auto domElement = DomainElementManager::getInstance().createDomainElementInt64(15);
  objVal[varName] = { { domElement, domElement } };
  ss.setObjectiveValue(objVal);
  
  // Optimize on objective 15 for the minimize semantic of the objective variable
  ss.optimizeObjective();
  ss.constraintStore()->runConsistency();
  
  // Check the bounds of the variable
  auto newUB = DomainElementManager::getInstance().createDomainElementInt64(14);
  auto newLB = DomainElementManager::getInstance().createDomainElementInt64(10);
  ASSERT_TRUE(varObj->domain()->lowerBound()->isEqual(newLB));
  ASSERT_TRUE(varObj->domain()->upperBound()->isEqual(newUB));
}//optimizeObjectiveMinimize

OPTILAB_TEST(SearchSemantics, optimizeObjectiveMaximize)
{
  // Test maximixzation on objective variable
  
  using namespace Core;
  using namespace Search;
  
  SearchSemantics ss;
  
  std::string varName{"test"};
  VariableFactoryUPtr varFactory(new VariableFactory());
  
  auto objExt = VariableSemanticObjective::ObjectiveExtremum::OBJ_EXT_MAXIMIZE;
  auto semantic = new VariableSemanticObjective(objExt, varName);
  VariableSemanticUPtr sobj(semantic);
  VariableSPtr varObj(varFactory->variableInteger(10, 20, std::move(sobj)));
  ss.setVariableObjective(varObj);
  
  SearchSemantics::ObjectiveValue objVal;
  auto domElement = DomainElementManager::getInstance().createDomainElementInt64(15);
  objVal[varName] = { { domElement, domElement } };
  ss.setObjectiveValue(objVal);
  
  // Optimize on objective 15 for the maximize semantic of the objective variable
  ss.optimizeObjective();
  ss.constraintStore()->runConsistency();
  
  // Check the bounds of the variable
  auto newUB = DomainElementManager::getInstance().createDomainElementInt64(20);
  auto newLB = DomainElementManager::getInstance().createDomainElementInt64(16);
  ASSERT_TRUE(varObj->domain()->lowerBound()->isEqual(newLB));
  ASSERT_TRUE(varObj->domain()->upperBound()->isEqual(newUB));
}//optimizeObjectiveMaximize
