
#include "utest_globals.hpp"
#include "SearchHeuristic.hpp"

#include <vector>

#include "VariableFactory.hpp"
#include "VariableInteger.hpp"
#include "VariableSemanticDecision.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementVoid.hpp"
#include "DomainElementManager.hpp"

OPTILAB_TEST(SearchHeuristic, BaseMethods)
{
  using namespace Core;
  using namespace Search;
  
  VariableFactoryUPtr varFactory(new VariableFactory());
  
  // Test base class methods
  VariableSemanticUPtr semanticDecision1(new VariableSemanticDecision("Var1"));
  VariableSemanticUPtr semanticDecision2(new VariableSemanticDecision("Var2"));
  
  // Set input order for search heuristic
  semanticDecision2->setInputOrder(0);
  semanticDecision1->setInputOrder(1);
  
  VariableSPtr varInt1(varFactory->variableInteger(10, 12, std::move(semanticDecision1)));
  VariableSPtr varInt2(varFactory->variableInteger(11, 15, std::move(semanticDecision2)));
  std::vector<Core::VariableSPtr> branchingList{varInt1, varInt2};
  
  auto varChoice = VariableChoiceMetricType::VAR_CM_INPUT_ORDER;
  auto valChoice = ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MIN;
  SearchHeuristicSPtr searchHeuristic = std::make_shared<SearchHeuristic>(branchingList, varChoice,
                                                                          valChoice);
  
  const auto& varList = searchHeuristic->getBranchingVarList();
  ASSERT_EQ(2, varList.size());
  ASSERT_TRUE(varList[0].get() == varInt1.get() || varList[1].get() == varInt1.get());
  ASSERT_TRUE(varList[0].get() == varInt2.get() || varList[1].get() == varInt2.get());
  
  // No modifications on domains, should return the same variable.
  // @note varInt2 should be returned instead or varInt1 since varInt2 has a lower
  // input order value than varInt1
  ASSERT_EQ(searchHeuristic->getChoiceVariable().get(), varInt2.get());
  ASSERT_EQ(searchHeuristic->getChoiceVariable().get(), varInt2.get());
  
  // Should return the min element of varInt2
  auto domainElement = searchHeuristic->getChoiceValue();
  ASSERT_TRUE(domainElement);
  ASSERT_TRUE(DomainElementInt64::isa(domainElement));
  ASSERT_TRUE(DomainElementInt64::cast(domainElement)->
              isEqual(DomainElementManager::getInstance().createDomainElementInt64(11)));
  
  // No modifications on domains, should return the same value
  domainElement = searchHeuristic->getChoiceValue();
  ASSERT_TRUE(domainElement);
  ASSERT_TRUE(DomainElementInt64::isa(domainElement));
  ASSERT_TRUE(DomainElementInt64::cast(domainElement)->
              isEqual(DomainElementManager::getInstance().createDomainElementInt64(11)));
}//BaseMethods

OPTILAB_TEST(SearchHeuristic, SingletonVars1)
{
  using namespace Core;
  using namespace Search;
  
  VariableFactoryUPtr varFactory(new VariableFactory());
  
  // Test base class methods
  VariableSemanticUPtr semanticDecision1(new VariableSemanticDecision());
  VariableSemanticUPtr semanticDecision2(new VariableSemanticDecision());
  
  // Set input order for search heuristic
  semanticDecision2->setInputOrder(0);
  semanticDecision1->setInputOrder(1);
  
  VariableSPtr varInt1(varFactory->variableInteger(10, 12, std::move(semanticDecision1)));
  VariableSPtr varInt2(varFactory->variableInteger(11, 11, std::move(semanticDecision2)));
  std::vector<Core::VariableSPtr> branchingList{varInt1, varInt2};
  
  auto varChoice = VariableChoiceMetricType::VAR_CM_INPUT_ORDER;
  auto valChoice = ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MIN;
  SearchHeuristicSPtr searchHeuristic = std::make_shared<SearchHeuristic>(branchingList,
                                                                          varChoice, valChoice);
  
  // Should return varInt1 despite the fact that varInt2 has a lower
  // input order than varInt1, since varInt2 is assigned
  ASSERT_EQ(searchHeuristic->getChoiceVariable().get(), varInt1.get());
  
  auto domainElement = searchHeuristic->getChoiceValue();
  ASSERT_TRUE(domainElement);
  ASSERT_TRUE(DomainElementInt64::isa(domainElement));
  ASSERT_TRUE(DomainElementInt64::cast(domainElement)->
              isEqual(DomainElementManager::getInstance().createDomainElementInt64(10)));
}//SingletonVars1

OPTILAB_TEST(SearchHeuristic, SingletonVars2)
{
  using namespace Core;
  using namespace Search;
  
  VariableFactoryUPtr varFactory(new VariableFactory());
  
  // Test base class methods
  VariableSemanticUPtr semanticDecision1(new VariableSemanticDecision());
  VariableSemanticUPtr semanticDecision2(new VariableSemanticDecision());
  
  // Set input order for search heuristic
  semanticDecision2->setInputOrder(0);
  semanticDecision1->setInputOrder(1);
  
  VariableSPtr varInt1(varFactory->variableInteger(10, 10, std::move(semanticDecision1)));
  VariableSPtr varInt2(varFactory->variableInteger(11, 11, std::move(semanticDecision2)));
  std::vector<Core::VariableSPtr> branchingList{varInt1, varInt2};
  
  auto varChoice = VariableChoiceMetricType::VAR_CM_INPUT_ORDER;
  auto valChoice = ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MIN;
  SearchHeuristicSPtr searchHeuristic = std::make_shared<SearchHeuristic>(branchingList, varChoice,
                                                                          valChoice);
  
  // Should return nullptr since all branching variables are assigned
  ASSERT_FALSE(searchHeuristic->getChoiceVariable());
  
  // Should return void element when there is no choice variable
  ASSERT_TRUE(DomainElementVoid::isa(searchHeuristic->getChoiceValue(nullptr)));
  ASSERT_TRUE(DomainElementVoid::isa(searchHeuristic->getChoiceValue()));
}//SingletonVars2

OPTILAB_TEST(SearchHeuristic, ChoiceVariable)
{
  using namespace Core;
  using namespace Search;
  
  VariableFactoryUPtr varFactory(new VariableFactory());
  
  // Test base class methods
  VariableSemanticUPtr semanticDecision1(new VariableSemanticDecision());
  VariableSemanticUPtr semanticDecision2(new VariableSemanticDecision());
  
  // Set input order for search heuristic
  semanticDecision1->setInputOrder(0);
  semanticDecision2->setInputOrder(1);
  
  VariableSPtr varInt1(varFactory->variableInteger(1, 10, std::move(semanticDecision1)));
  VariableSPtr varInt2(varFactory->variableInteger(2, 10, std::move(semanticDecision2)));
  std::vector<Core::VariableSPtr> branchingList{varInt1, varInt2};
  
  auto varChoice = VariableChoiceMetricType::VAR_CM_INPUT_ORDER;
  auto valChoice = ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MIN;
  SearchHeuristicSPtr searchHeuristic = std::make_shared<SearchHeuristic>(branchingList,
                                                                          varChoice, valChoice);
  
  // It should return the domain element "1" for min of the first variable
  auto domElem1 = searchHeuristic->getChoiceValue(varInt1.get());
  
  // It should return the domain element "2" for the min of varInt2
  auto domElem2 = searchHeuristic->getChoiceValue(varInt2.get());
  
  ASSERT_TRUE(DomainElementInt64::cast(domElem1)->
              isEqual(DomainElementManager::getInstance().createDomainElementInt64(1)));
  ASSERT_TRUE(DomainElementInt64::cast(domElem2)->
              isEqual(DomainElementManager::getInstance().createDomainElementInt64(2)));
}//ChoiceVariable
