
#include "utest_globals.hpp"

#include "VariableSemanticSupport.hpp"

#include <memory>

namespace Core {
  
  // Test class for protected members
  class VariableSemanticSupportTest : public VariableSemanticSupport {
  public:
    VariableSemanticSupportTest()
    : VariableSemanticSupport("")
    {
    }
    
    bool getBranchingPolicyTest(bool aBranchingSemantic)
    {
      return VariableSemanticSupport::getBranchingPolicy(aBranchingSemantic);
    }
  };
  
}// end namespace Core


OPTILAB_TEST(VariableSemanticSupport, Basics)
{
  using namespace Core;
  
  std::unique_ptr<VariableSemantic> varSupport(new VariableSemanticSupport());
  ASSERT_TRUE(VariableSemanticSupport::isa(varSupport.get()));
  
  VariableSemanticSupport *varSupportFromCast = VariableSemanticSupport::cast(varSupport.get());
  ASSERT_TRUE(VariableSemanticSupport::isa(varSupportFromCast));
}//Basics

OPTILAB_TEST(VariableSemanticSupport, Identifier)
{
  using namespace Core;
  
  VariableSemanticSupport varSupport;
  ASSERT_FALSE(varSupport.getVariableNameIdentifier().empty());
  ASSERT_EQ(varSupport.getVariableNameIdentifier().operator[](0), '_');
  
  VariableSemanticSupport varSupportWithName("testVar");
  ASSERT_FALSE(varSupportWithName.getVariableNameIdentifier().empty());
  ASSERT_EQ(varSupportWithName.getVariableNameIdentifier(), "_testVar");
  
  VariableSemanticSupport varSupportWithNameAndPrefix("_testVar");
  ASSERT_FALSE(varSupportWithNameAndPrefix.getVariableNameIdentifier().empty());
  ASSERT_EQ(varSupportWithNameAndPrefix.getVariableNameIdentifier(), "_testVar");
}//Identifier

OPTILAB_TEST(VariableSemanticSupport, OutputAndBranching)
{
  using namespace Core;
  
  VariableSemanticSupport varSupport;
  ASSERT_FALSE(varSupport.isSolutionElement());
  
  VariableSemanticSupportTest varSupportTest;
  ASSERT_FALSE(varSupportTest.getBranchingPolicyTest(true));
  ASSERT_FALSE(varSupportTest.getBranchingPolicyTest(false));
}//OutputAndBranching

