
#include "utest_globals.hpp"

#include "VariableArray.hpp"

// Domain element
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"
#include "DomainInteger.hpp"

// Semantics
#include "VariableSemanticSupport.hpp"
#include "VariableSemanticDecision.hpp"
#include "VariableSemanticObjective.hpp"

// Primitive type variables
#include "VariableBoolean.hpp"
#include "VariableInteger.hpp"
#include "VariableFactory.hpp"

#include <memory>

OPTILAB_TEST(VariableArray, Basics)
{
  using namespace Core;
  
  std::string varName{"VarTest"};
  VariableSemanticUPtr semanticDecision(new VariableSemanticDecision(varName));
  
  // Create a array of size 3
  std::size_t numCells{3};
  VariableFactoryUPtr varFactory(new VariableFactory());
  VariableSPtr varArray(varFactory->variableArray(numCells, std::move(semanticDecision)));
  
  Variable *varPtr = varArray.get();
  ASSERT_TRUE(VariableArray::isa(varPtr));
  
  ASSERT_EQ(VariableArray::cast(varPtr)->numberOfDimensions(), 1);
  ASSERT_EQ(VariableArray::cast(varPtr)->size(), numCells);
  ASSERT_FALSE(VariableArray::cast(varPtr)->isAssigned());
  
  for(std::size_t cellIdx = 0; cellIdx < numCells; ++cellIdx)
  {
    ASSERT_FALSE(VariableArray::cast(varPtr)->at(cellIdx));
  }
  
  for(std::size_t cellIdx = 0; cellIdx < numCells; ++cellIdx)
  {
    ASSERT_FALSE(VariableArray::cast(varPtr)->operator[](cellIdx));
  }
}//Identifier

OPTILAB_TEST(VariableArray, InsertAndReadElements)
{
  using namespace Core;
  
  std::string varName{"VarTest"};
  VariableSemanticUPtr semanticDecision(new VariableSemanticDecision(varName));
  
  // Create a array of size 3
  std::size_t numCells{3};

  VariableFactoryUPtr varFactory(new VariableFactory());
  VariableSPtr varArray(varFactory->variableArray(numCells, std::move(semanticDecision)));
  
  VariableSemanticUPtr semanticDecisionBool(new VariableSemanticDecision(varName));
  VariableSPtr varBool(varFactory->variableBoolean(std::move(semanticDecisionBool)));
  
  VariableSemanticUPtr semanticDecisionInt(new VariableSemanticDecision(varName));
  VariableSPtr varInt(varFactory->variableInteger(-10, +10, std::move(semanticDecisionInt)));
  
  VariableArray* varArrayCast = VariableArray::cast(varArray.get());
  varArrayCast->assignVariableToCell(0, varBool);
  varArrayCast->assignVariableToCell(1, varInt);

  
  ASSERT_TRUE(VariableBoolean::isa(varArrayCast->operator[](0).get()));
  ASSERT_TRUE(VariableInteger::isa(varArrayCast->operator[](1).get()));
  
  ASSERT_TRUE(varArrayCast->operator[](0).get() == varBool.get());
  ASSERT_TRUE(varArrayCast->operator[](1).get() == varInt.get());
  
  varArrayCast->assignVariableToCell(0, nullptr);
  ASSERT_FALSE(varArrayCast->operator[](0));
}//InsertAndReadElements
