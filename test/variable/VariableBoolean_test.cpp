
#include "utest_globals.hpp"

#include "VariableBoolean.hpp"

// Domain element
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"
#include "DomainBoolean.hpp"

// Semantics
#include "VariableSemanticSupport.hpp"
#include "VariableSemanticDecision.hpp"
#include "VariableSemanticObjective.hpp"

#include <memory>

namespace Core {
  class VariableBooleanTest : public VariableBoolean {
  public:
    VariableBooleanTest(VariableSemanticUPtr aVariableSemantic)
    : VariableBoolean(std::move(aVariableSemantic))
    {
      setType(new VariableType(VariableTypeClass::VAR_TYPE_BOOLEAN));
    }
    
    inline DomainBoolean *getDomainForTest()
    {
      return DomainBoolean::cast(domain().get());
    }
  };
}// end namespace Core

OPTILAB_TEST(VariableBoolean, Basics)
{
  using namespace Core;
  
  // Test base class methods
  std::string varName{"VarTest"};
  VariableSemanticUPtr semanticDecision(new VariableSemanticDecision(varName));

  VariableBooleanTest varBoolDecision(std::move(semanticDecision));
  
  Variable *varPtr = &varBoolDecision;
  ASSERT_TRUE(VariableBoolean::isa(varPtr));
  
  ASSERT_TRUE(varBoolDecision.hasPrimitiveType());
  ASSERT_EQ(varBoolDecision.getVariableNameId(), varName);
  ASSERT_TRUE(varBoolDecision.variableSemantic()->isSolutionElement());
  
  // Default is not branching (must be part of a branch statement to be branching)
  ASSERT_FALSE(varBoolDecision.variableSemantic()->isBranching());
  ASSERT_FALSE(varBoolDecision.isAssigned());
  
  VariableBooleanTest varBoolDecisionCopy = varBoolDecision;
  ASSERT_TRUE(varBoolDecisionCopy.hasPrimitiveType());
  ASSERT_EQ(varBoolDecisionCopy.getVariableNameId(), varName);
  ASSERT_TRUE(varBoolDecisionCopy.variableSemantic()->isSolutionElement());
  
  // Default is not branching (must be part of a branch statement to be branching)
  ASSERT_FALSE(varBoolDecisionCopy.variableSemantic()->isBranching());
  ASSERT_FALSE(varBoolDecisionCopy.isAssigned());
}//Basics

OPTILAB_TEST(VariableBoolean, Domain)
{
  using namespace Core;
  
  // Test base class methods
  std::string varName{"VarTest"};
  VariableSemanticUPtr semanticDecision(new VariableSemanticDecision(varName));
  
  VariableBooleanTest varBoolDecision(std::move(semanticDecision));
  
  DomainBoolean *varDomain = varBoolDecision.getDomainForTest();
  ASSERT_TRUE(varDomain);
  ASSERT_TRUE(DomainElement::isEqual(varDomain->lowerBound(), DomainElementManager::getInstance().createDomainElementInt64(0)));
  ASSERT_TRUE(DomainElement::isEqual(varDomain->upperBound(), DomainElementManager::getInstance().createDomainElementInt64(1)));
  ASSERT_EQ(varDomain->getSize(), 2);
}//Domain
