
#include "utest_globals.hpp"

#include "VariableMatrix2D.hpp"

// Domain element
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"
#include "DomainInteger.hpp"

// Semantics
#include "VariableSemanticSupport.hpp"
#include "VariableSemanticDecision.hpp"
#include "VariableSemanticObjective.hpp"

// Primitive type variables
#include "VariableBoolean.hpp"
#include "VariableInteger.hpp"
#include "VariableFactory.hpp"

#include <memory>

OPTILAB_TEST(VariableMatrix2D, Basics)
{
  using namespace Core;
  
  std::string varName{"VarTest"};
  VariableSemanticUPtr semanticDecision(new VariableSemanticDecision(varName));
  
  // Create a 2 x 3 matrix
  std::size_t numRows{2};
  std::size_t numCols{3};
  VariableFactoryUPtr varFactory(new VariableFactory());
  VariableSPtr varMatrix(varFactory->variableMatrix2D(numRows, numCols, std::move(semanticDecision)));
  
  
  Variable *varPtr = varMatrix.get();
  ASSERT_TRUE(VariableMatrix2D::isa(varPtr));
  
  ASSERT_EQ(VariableMatrix2D::cast(varPtr)->numberOfDimensions(), 2);
  ASSERT_EQ(VariableMatrix2D::cast(varPtr)->numRows(), numRows);
  ASSERT_EQ(VariableMatrix2D::cast(varPtr)->numCols(), numCols);
  ASSERT_EQ(VariableMatrix2D::cast(varPtr)->size(), numRows * numCols);
  ASSERT_FALSE(VariableMatrix2D::cast(varPtr)->isAssigned());
  
  for(std::size_t rowIdx = 0; rowIdx < numRows; ++rowIdx)
  {
    for (std::size_t colIdx = 0; colIdx < numCols; ++colIdx)
    {
      ASSERT_FALSE(VariableMatrix2D::cast(varPtr)->at(rowIdx, colIdx));
    }
  }
  
  VariableMatrix2D varCopy(*VariableMatrix2D::cast(varPtr));
  for(std::size_t rowIdx = 0; rowIdx < numRows; ++rowIdx)
  {
    for (std::size_t colIdx = 0; colIdx < numCols; ++colIdx)
    {
      ASSERT_FALSE(varCopy[rowIdx][colIdx]);
    }
  }
}//Identifier

OPTILAB_TEST(VariableMatrix2D, InsertAndReadElements)
{
  using namespace Core;
  
  std::string varName{"VarTest"};
  VariableSemanticUPtr semanticDecision(new VariableSemanticDecision(varName));
  
  // Create a 2 x 3 matrix
  std::size_t numRows{2};
  std::size_t numCols{3};
  VariableFactoryUPtr varFactory(new VariableFactory());
  VariableSPtr varMatrix(varFactory->variableMatrix2D(numRows, numCols, std::move(semanticDecision)));
  
  VariableSemanticUPtr semanticDecisionBool(new VariableSemanticDecision(varName));
  VariableSPtr varBool(varFactory->variableBoolean(std::move(semanticDecisionBool)));
  
  VariableSemanticUPtr semanticDecisionInt(new VariableSemanticDecision(varName));
  VariableSPtr varInt(varFactory->variableInteger(-10, +10, std::move(semanticDecisionInt)));
  
  VariableMatrix2D* varMatrix2DCast = VariableMatrix2D::cast(varMatrix.get());
  varMatrix2DCast->assignVariableToCell(0, 0, varBool);
  varMatrix2DCast->assignVariableToCell(0, 1, varInt);

  VariableMatrix2D varCopy(*varMatrix2DCast);
  
  ASSERT_TRUE(VariableBoolean::isa(varCopy[0][0].get()));
  ASSERT_TRUE(VariableInteger::isa(varCopy[0][1].get()));
  
  ASSERT_TRUE(varCopy[0][0].get() == varBool.get());
  ASSERT_TRUE(varCopy[0][1].get() == varInt.get());
  
  varCopy.assignVariableToCell(0, 0, nullptr);
  ASSERT_FALSE(varCopy[0][0]);
}//InsertAndReadElements
