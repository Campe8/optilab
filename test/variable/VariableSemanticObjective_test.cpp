
#include "utest_globals.hpp"

#include "VariableSemanticObjective.hpp"

#include <memory>

namespace Core {
  
  // Test class for protected members
  class VariableSemanticObjectiveTest : public VariableSemanticObjective {
  public:
    VariableSemanticObjectiveTest(VariableSemanticObjective::ObjectiveExtremum aObjExtremum)
    : VariableSemanticObjective(aObjExtremum)
    {
    }
    
    bool getBranchingPolicyTest(bool aBranchingSemantic)
    {
      return VariableSemanticObjective::getBranchingPolicy(aBranchingSemantic);
    }
  };
  
}// end namespace Core


OPTILAB_TEST(VariableSemanticObjective, Basics)
{
  using namespace Core;
  
  std::unique_ptr<VariableSemanticObjective> varObjective(new VariableSemanticObjective(VariableSemanticObjective::ObjectiveExtremum::OBJ_EXT_MAXIMIZE));
  ASSERT_TRUE(VariableSemanticObjective::isa(varObjective.get()));
  
  VariableSemanticObjective *varObjectiveFromCast = VariableSemanticObjective::cast(varObjective.get());
  ASSERT_TRUE(VariableSemanticObjective::isa(varObjectiveFromCast));
  ASSERT_EQ(varObjectiveFromCast->getObjectiveExtremum(), VariableSemanticObjective::ObjectiveExtremum::OBJ_EXT_MAXIMIZE);
}//Basics

OPTILAB_TEST(VariableSemanticObjective, Identifier)
{
  using namespace Core;
  
  VariableSemanticObjective varObjective(VariableSemanticObjective::ObjectiveExtremum::OBJ_EXT_MINIMIZE);
  ASSERT_FALSE(varObjective.getVariableNameIdentifier().empty());
  ASSERT_EQ(varObjective.getVariableNameIdentifier().operator[](0), 'O');
  
  VariableSemanticObjective varObjectiveWithName(VariableSemanticObjective::ObjectiveExtremum::OBJ_EXT_MINIMIZE, "testVar");
  ASSERT_FALSE(varObjectiveWithName.getVariableNameIdentifier().empty());
  ASSERT_EQ(varObjectiveWithName.getVariableNameIdentifier(), "testVar");
  ASSERT_EQ(varObjectiveWithName.getObjectiveExtremum(), VariableSemanticObjective::ObjectiveExtremum::OBJ_EXT_MINIMIZE);
}//Identifier

OPTILAB_TEST(VariableSemanticObjective, OutputAndBranching)
{
  using namespace Core;
  
  // Default variable objective has output semantic
  VariableSemanticObjective varObjective(VariableSemanticObjective::ObjectiveExtremum::OBJ_EXT_MAXIMIZE);
  ASSERT_TRUE(varObjective.isSolutionElement());
  
  // Default semantic is using a non branching policy
  VariableSemanticObjectiveTest varObjetiveTest(VariableSemanticObjective::ObjectiveExtremum::OBJ_EXT_MAXIMIZE);
  ASSERT_FALSE(varObjetiveTest.isBranching());
  ASSERT_TRUE(varObjetiveTest.getBranchingPolicyTest(true));
  ASSERT_FALSE(varObjetiveTest.getBranchingPolicyTest(false));
}//OutputAndBranching

