
#include "utest_globals.hpp"

#include "VariableInteger.hpp"

// Domain element
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"
#include "DomainInteger.hpp"

// Semantics
#include "VariableSemanticSupport.hpp"
#include "VariableSemanticDecision.hpp"
#include "VariableSemanticObjective.hpp"

#include <memory>

namespace Core {
  class VariableIntegerTest : public VariableInteger {
  public:
    VariableIntegerTest(DomainElementInt64 *aLowerBound,
                        DomainElementInt64 *aUpperBound,
                        VariableSemanticUPtr aVariableSemantic)
    : VariableInteger(aLowerBound, aUpperBound, std::move(aVariableSemantic))
    {
      setType(new VariableType(VariableTypeClass::VAR_TYPE_INTEGER));
    }
    
    VariableIntegerTest(const std::unordered_set<DomainElementInt64 *>& aSetOfElements,
                        VariableSemanticUPtr aVariableSemantic)
    : VariableInteger(aSetOfElements, std::move(aVariableSemantic))
    {
    }
    
    inline DomainInteger *getDomainForTest()
    {
      return DomainInteger::cast(domainList()->at(0).get());
    }
  };
}// end namespace Core


OPTILAB_TEST(VariableInteger, Basics)
{
  using namespace Core;
  
  // Test base class methods
  std::string varName{"VarTest"};
  VariableSemanticUPtr semanticDecision(new VariableSemanticDecision(varName));
  
  auto lowerBound = DomainElementManager::getInstance().createDomainElementInt64(-10);
  auto upperBound = DomainElementManager::getInstance().createDomainElementInt64(+10);
  VariableIntegerTest varIntDecision(lowerBound,
                                     upperBound,
                                     std::move(semanticDecision));
  
  Variable *varPtr = &varIntDecision;
  ASSERT_TRUE(VariableInteger::isa(varPtr));
  
  ASSERT_TRUE(varIntDecision.hasPrimitiveType());
  ASSERT_EQ(varIntDecision.getVariableNameId(), varName);
  ASSERT_TRUE(varIntDecision.variableSemantic()->isSolutionElement());
  
  // Default is not branching (must be part of a branch statement to be branching)
  ASSERT_FALSE(varIntDecision.variableSemantic()->isBranching());
  ASSERT_FALSE(varIntDecision.isAssigned());
  
  VariableIntegerTest varIntDecisionCopy = varIntDecision;
  ASSERT_TRUE(varIntDecisionCopy.hasPrimitiveType());
  ASSERT_EQ(varIntDecisionCopy.getVariableNameId(), varName);
  ASSERT_TRUE(varIntDecisionCopy.variableSemantic()->isSolutionElement());
  ASSERT_FALSE(varIntDecisionCopy.variableSemantic()->isBranching());
  ASSERT_FALSE(varIntDecisionCopy.isAssigned());
}//Identifier

OPTILAB_TEST(VariableInteger, Domain)
{
  using namespace Core;
  
  // Test base class methods
  std::string varName{"VarTest"};
  VariableSemanticUPtr semanticDecision(new VariableSemanticDecision(varName));
  
  DomainElementInt64* lowerBound = DomainElementManager::getInstance().createDomainElementInt64(-10);
  DomainElementInt64* upperBound =DomainElementManager::getInstance().createDomainElementInt64(+10);
  VariableIntegerTest varIntLbUb(lowerBound,
                                 upperBound,
                                 std::move(semanticDecision));
  
  DomainInteger *varDomain = varIntLbUb.getDomainForTest();
  ASSERT_TRUE(varDomain);
  ASSERT_TRUE(DomainElement::isEqual(varDomain->lowerBound(), lowerBound));
  ASSERT_TRUE(DomainElement::isEqual(varDomain->upperBound(), upperBound));
  ASSERT_EQ(varDomain->getSize(), 21);
}//Domain
