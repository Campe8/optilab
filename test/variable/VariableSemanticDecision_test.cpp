
#include "utest_globals.hpp"

#include "VariableSemanticDecision.hpp"

#include <memory>

namespace Core {
  
  // Test class for protected members
  class VariableSemanticDecisionTest : public VariableSemanticDecision {
  public:
    VariableSemanticDecisionTest()
    : VariableSemanticDecision("")
    {
    }
    
    bool getBranchingPolicyTest(bool aBranchingSemantic)
    {
      return VariableSemanticDecision::getBranchingPolicy(aBranchingSemantic);
    }
  };
  
}// end namespace Core


OPTILAB_TEST(VariableSemanticDecision, Basics)
{
  using namespace Core;
  
  std::unique_ptr<VariableSemanticDecision> varDecision(new VariableSemanticDecision());
  ASSERT_TRUE(VariableSemanticDecision::isa(varDecision.get()));
  
  VariableSemanticDecision *varDecisionFromCast = VariableSemanticDecision::cast(varDecision.get());
  ASSERT_TRUE(VariableSemanticDecision::isa(varDecisionFromCast));
}//Basics

OPTILAB_TEST(VariableSemanticDecision, Identifier)
{
  using namespace Core;
  
  VariableSemanticDecision varDecision;
  ASSERT_FALSE(varDecision.getVariableNameIdentifier().empty());
  ASSERT_EQ(varDecision.getVariableNameIdentifier().operator[](0), 'D');
  
  VariableSemanticDecision varDecisionWithName("testVar");
  ASSERT_FALSE(varDecisionWithName.getVariableNameIdentifier().empty());
  ASSERT_EQ(varDecisionWithName.getVariableNameIdentifier(), "testVar");
}//Identifier

OPTILAB_TEST(VariableSemanticDecision, OutputAndBranching)
{
  using namespace Core;
  
  // Default decision variable is an output variable
  VariableSemanticDecision varDecision;
  ASSERT_TRUE(varDecision.isSolutionElement());
  
  // Default semantic is using a branching policy
  VariableSemanticDecisionTest varDecisionTest;
  ASSERT_FALSE(varDecisionTest.isBranching());
  ASSERT_TRUE(varDecisionTest.getBranchingPolicyTest(true));
  ASSERT_FALSE(varDecisionTest.getBranchingPolicyTest(false));
}//OutputAndBranching

