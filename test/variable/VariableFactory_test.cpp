
#include "utest_globals.hpp"

#include "VariableFactory.hpp"

#include "VariableInteger.hpp"
#include "VariableBoolean.hpp"
#include "VariableArray.hpp"
#include "VariableMatrix2D.hpp"
#include "VariableSemanticDecision.hpp"

#include "DomainElementManager.hpp"

OPTILAB_TEST(VariableFactory, VariableInteger)
{
  using namespace Core;
  
  VariableFactoryUPtr varFactory(new VariableFactory());
  
  // Test base class methods
  std::string varName{"VarTest"};
  VariableSemanticUPtr semanticDecision(new VariableSemanticDecision(varName));


  VariableSPtr varInt(varFactory->variableInteger(10, std::move(semanticDecision)));
  ASSERT_TRUE(VariableInteger::isa(varInt.get()));
  
  // Is assigned should return true since varInt is singleton
  ASSERT_TRUE(VariableInteger::cast(varInt.get())->isAssigned());
  ASSERT_TRUE(VariableInteger::cast(varInt.get())->hasPrimitiveType());
  
  VariableSemanticUPtr semanticDecision1(new VariableSemanticDecision(varName));
  VariableSPtr varInt1(varFactory->variableInteger(10, 10, std::move(semanticDecision1)));
  ASSERT_TRUE(VariableInteger::cast(varInt1.get())->isAssigned());
  
  VariableSemanticUPtr semanticDecision2(new VariableSemanticDecision(varName));
  VariableSPtr varInt2(varFactory->variableInteger(10, 20, std::move(semanticDecision2)));
  ASSERT_FALSE(VariableInteger::cast(varInt2.get())->isAssigned());
  
  VariableSemanticUPtr semanticDecision3(new VariableSemanticDecision(varName));
  VariableSPtr varInt3(varFactory->variableInteger(DomainElementManager::getInstance().createDomainElementInt64(10),
                                                   DomainElementManager::getInstance().createDomainElementInt64(20),
                                                   std::move(semanticDecision3)));
  ASSERT_FALSE(VariableInteger::cast(varInt3.get())->isAssigned());
}//VariableInteger

OPTILAB_TEST(VariableFactory, VariableBool)
{
  using namespace Core;
  
  VariableFactoryUPtr varFactory(new VariableFactory());
  
  // Test base class methods
  std::string varName{"VarTest"};
  VariableSemanticUPtr semanticDecision(new VariableSemanticDecision(varName));
  
  
  VariableSPtr varBool(varFactory->variableBoolean(std::move(semanticDecision)));
  ASSERT_TRUE(VariableBoolean::isa(varBool.get()));
  
  // Is assigned should return true since varInt is singleton
  ASSERT_FALSE(VariableBoolean::cast(varBool.get())->isAssigned());
  ASSERT_TRUE(VariableBoolean::cast(varBool.get())->hasPrimitiveType());
  
  VariableSemanticUPtr semanticDecision1(new VariableSemanticDecision(varName));
  VariableSPtr varBool1(varFactory->variableBoolean(true, std::move(semanticDecision1)));
  ASSERT_TRUE(VariableBoolean::cast(varBool1.get())->isAssigned());
  
  VariableSemanticUPtr semanticDecision2(new VariableSemanticDecision(varName));
  VariableSPtr varBool2(varFactory->variableBoolean(false, std::move(semanticDecision2)));
  ASSERT_TRUE(VariableBoolean::cast(varBool2.get())->isAssigned());
}//VariableBool

OPTILAB_TEST(VariableFactory, VariableArray)
{
  using namespace Core;
  
  VariableFactoryUPtr varFactory(new VariableFactory());
  
  // Test base class methods
  VariableSemanticUPtr semanticDecision1(new VariableSemanticDecision());
  VariableSemanticUPtr semanticDecision2(new VariableSemanticDecision());
  VariableSemanticUPtr semanticDecision3(new VariableSemanticDecision());
  VariableSemanticUPtr semanticDecision4(new VariableSemanticDecision());
  VariableSPtr varBool(varFactory->variableBoolean(std::move(semanticDecision1)));
  VariableSPtr varInt(varFactory->variableInteger(10, std::move(semanticDecision2)));
  
  VariableSPtr varArray(varFactory->variableArray({varBool, varInt}, std::move(semanticDecision3)));
  ASSERT_TRUE(VariableArray::isa(varArray.get()));
  
  ASSERT_FALSE(VariableArray::cast(varArray.get())->isAssigned());
  ASSERT_EQ(VariableArray::cast(varArray.get())->numberOfDimensions(), 1);
  ASSERT_EQ(VariableArray::cast(varArray.get())->size(), 2);
  ASSERT_EQ(VariableArray::cast(varArray.get())->at(0).get(), varBool.get());
  ASSERT_EQ(VariableArray::cast(varArray.get())->at(1).get(), varInt.get());
  
  VariableSPtr varArrayRec(varFactory->variableArray(3, std::move(semanticDecision4)));
  ASSERT_TRUE(VariableArray::isa(varArrayRec.get()));
  
  VariableArray::cast(varArrayRec.get())->assignVariableToCell(0, varBool);
  VariableArray::cast(varArrayRec.get())->assignVariableToCell(1, varInt);
  VariableArray::cast(varArrayRec.get())->assignVariableToCell(2, varArray);
  ASSERT_FALSE(VariableArray::cast(varArrayRec.get())->isAssigned());
  ASSERT_EQ(VariableArray::cast(varArrayRec.get())->numberOfDimensions(), 1);
  ASSERT_EQ(VariableArray::cast(varArrayRec.get())->size(), 3);
  ASSERT_EQ(VariableArray::cast(varArrayRec.get())->at(0).get(), varBool.get());
  ASSERT_EQ(VariableArray::cast(varArrayRec.get())->at(1).get(), varInt.get());
  ASSERT_EQ(VariableArray::cast(varArrayRec.get())->at(2).get(), varArray.get());
  
  // Domain list should contain the domains for the exploded variable
  DomainListPtr domainList1 = VariableArray::cast(varArray.get())->domainList();
  DomainListPtr domainList2 = VariableArray::cast(varArrayRec.get())->domainList();
  ASSERT_TRUE(domainList1);
  ASSERT_TRUE(domainList2);
  ASSERT_EQ(domainList2->size(), 2 + domainList1->size());
}//VariableArray

OPTILAB_TEST(VariableFactory, VariableMatrix2D)
{
  using namespace Core;
  
  VariableFactoryUPtr varFactory(new VariableFactory());
  
  // Test base class methods
  VariableSemanticUPtr semanticDecision1(new VariableSemanticDecision());
  VariableSemanticUPtr semanticDecision2(new VariableSemanticDecision());
  VariableSemanticUPtr semanticDecision3(new VariableSemanticDecision());
  VariableSemanticUPtr semanticDecision4(new VariableSemanticDecision());
  VariableSemanticUPtr semanticDecision5(new VariableSemanticDecision());
  VariableSPtr varBool(varFactory->variableBoolean(std::move(semanticDecision1)));
  VariableSPtr varInt(varFactory->variableInteger(10, std::move(semanticDecision2)));
  VariableSPtr varArray(varFactory->variableArray({varBool, varInt}, std::move(semanticDecision3)));
  
  // Defining a 3 x 2 variable matrix
  VariableSPtr varMatrix(varFactory->variableMatrix2D(
  {
    {varBool,  varInt},
    {varArray, varInt},
    {varBool,  varInt}
  },
  std::move(semanticDecision4)));
  
  ASSERT_TRUE(VariableMatrix2D::isa(varMatrix.get()));
  ASSERT_EQ(VariableMatrix2D::cast(varMatrix.get())->numberOfDimensions(), 2);
  ASSERT_EQ(VariableMatrix2D::cast(varMatrix.get())->numRows(), 3);
  ASSERT_EQ(VariableMatrix2D::cast(varMatrix.get())->numCols(), 2);
  ASSERT_EQ(VariableMatrix2D::cast(varMatrix.get())->size(), 6);
  ASSERT_EQ(VariableMatrix2D::cast(varMatrix.get())->at(2, 0).get(), varBool.get());
  ASSERT_EQ(VariableMatrix2D::cast(varMatrix.get())->at(2, 1).get(), varInt.get());
  
  DomainListPtr domainList1 = VariableMatrix2D::cast(varMatrix.get())->domainList();
  ASSERT_TRUE(domainList1);
  ASSERT_EQ(domainList1->size(), 7);
  
  // Defining a 2 x 2 variable matrix
  VariableSPtr varMatrix2(varFactory->variableMatrix2D(2, 2, std::move(semanticDecision5)));
  ASSERT_TRUE(VariableMatrix2D::isa(varMatrix2.get()));
  ASSERT_EQ(VariableMatrix2D::cast(varMatrix2.get())->numberOfDimensions(), 2);
  ASSERT_EQ(VariableMatrix2D::cast(varMatrix2.get())->numRows(), 2);
  ASSERT_EQ(VariableMatrix2D::cast(varMatrix2.get())->numCols(), 2);
  
  // Matrix of primitive types, arrays and matrices
  VariableMatrix2D::cast(varMatrix2.get())->assignVariableToCell(0, 0, varInt);
  VariableMatrix2D::cast(varMatrix2.get())->assignVariableToCell(0, 1, varBool);
  VariableMatrix2D::cast(varMatrix2.get())->assignVariableToCell(1, 0, varArray);
  VariableMatrix2D::cast(varMatrix2.get())->assignVariableToCell(1, 1, varMatrix);
  DomainListPtr domainList2 = VariableMatrix2D::cast(varMatrix2.get())->domainList();
  ASSERT_TRUE(domainList2);
  ASSERT_EQ(domainList2->size(), 4 + domainList1->size());
  ASSERT_EQ(VariableMatrix2D::cast(varMatrix2.get())->at(0, 0).get(), varInt.get());
  ASSERT_EQ(VariableMatrix2D::cast(varMatrix2.get())->at(0, 1).get(), varBool.get());
  ASSERT_EQ(VariableMatrix2D::cast(varMatrix2.get())->at(1, 0).get(), varArray.get());
  ASSERT_EQ(VariableMatrix2D::cast(varMatrix2.get())->at(1, 1).get(), varMatrix.get());
}//VariableMatrix2D
