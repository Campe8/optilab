
#include "utest_globals.hpp"

#include "ParserTestInc.hpp"

#ifdef RUN_TEST_OLD

OPTILAB_TEST(ParseStringFcnCall, FcnCallNoArgs)
{
  using namespace CLI;
  
  auto out = parsePP("fcn();");
  std::string eout{"|1=fcn_call|10=fcn|11=0"};
  ASSERT_EQ(eout, out);
}//FcnCallNoArgs

OPTILAB_TEST(ParseStringFcnCall, FcnCallArgs)
{
  using namespace CLI;
  
  auto out = parsePP("fcn(x);");
  std::string eout{"|1=fcn_call|10=fcn|11=1|12=x"};
  ASSERT_EQ(eout, out);
  
  out = parsePP("fcn(x, y);");
  eout = "|1=fcn_call|10=fcn|11=2|12=x,y";
  ASSERT_EQ(eout, out);
  
  out = parsePP("fcn(x, t[y], q[1]);");
  eout = "|1=fcn_call|10=fcn|11=3|12=x,t[y],q[1]";
  ASSERT_EQ(eout, out);
  
  out = parsePP("fcn(\"string\");");
  eout = "|1=fcn_call|10=fcn|11=1|12=\"string\"";
  ASSERT_EQ(eout, out);
}//FcnCallArgs

#endif
