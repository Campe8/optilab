
#include "utest_globals.hpp"

#include "ParserTestInc.hpp"

#ifdef RUN_TEST_OLD

OPTILAB_TEST(ParseStringConstraint, stdConstraintExprDecl)
{
  using namespace CLI;
  
  auto o1 = parsePP("3;");
  std::string s1{"|1=constraint|10=expr|11=c|12=3"};
  ASSERT_EQ(o1, s1);
  
  auto o2 = parsePP("a;");
  std::string s2{"|1=constraint|10=expr|11=i|12=a"};
  ASSERT_EQ(o2, s2);
}//stdConstraintExprDecl

#endif
