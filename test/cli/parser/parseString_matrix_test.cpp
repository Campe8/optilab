
#include "utest_globals.hpp"

#include "ParserTestInc.hpp"

#ifdef RUN_TEST_OLD

OPTILAB_TEST(ParseStringMatrix, arrayDecl)
{
  // Parse array declaration
  using namespace CLI;
  
  // Empty array
  auto o1 = parsePP("x:[];");
  std::string s1{"|1=matrix|10=x|11=integer|12=0|13=0"};
  ASSERT_EQ(o1, s1);
  
  // Array with 1 element
  o1 = parsePP("x:[1];");
  s1 = "|1=matrix|10=x|11=integer|12=1|13=1|14=1";
  ASSERT_EQ(o1, s1);
}//arrayDecl

OPTILAB_TEST(ParseStringMatrix, matrixDecl)
{
  // Parse matrix declaration
  using namespace CLI;
  
  // Empty matrix
  auto o1 = parsePP("x:[[]];");
  std::string s1{"|1=matrix|10=x|11=integer|12=0|13=0"};
  ASSERT_EQ(o1, s1);
  
  // Matrix with 1 row and 2 columns
  o1 = parsePP("x:[[1, 2]];");
  s1 = "|1=matrix|10=x|11=integer|12=1|13=2|14=1, 2";
  ASSERT_EQ(o1, s1);
  
  // Matrix with 2 rows and 2 columns
  o1 = parsePP("x:[[1, 2], [3, 4]];");
  s1 = "|1=matrix|10=x|11=integer|12=2|13=2|14=1, 2#3, 4";
  ASSERT_EQ(o1, s1);
}//matrixDecl

OPTILAB_TEST(ParseStringMatrix, matrixBoolDecl)
{
  // Parse matrix declaration with Boolean values
  using namespace CLI;
  
  // Matrix with 2 rows and 2 columns
  std::string o1 = parsePP("x:[[1, true], [3, 4]];");
  std::string s1 = "|1=matrix|10=x|11=boolean|12=2|13=2|14=1, 1#3, 4";
  ASSERT_EQ(o1, s1);
}//matrixBoolDecl

OPTILAB_TEST(ParseStringMatrix, matrixSaturationDecl)
{
  // Parse matrix declaration with overflow
  using namespace CLI;
  
  // Matrix with 2 rows and 2 columns
  std::string o1 = parsePP("x:[[1, 1], [9999999999999999999, true]];");
  std::string s1 = "|1=matrix|3=saturation|10=x|11=boolean|12=2|13=2|14=1, 1#9223372036854775807, 1";
  ASSERT_EQ(o1, s1);
}//matrixSaturationDecl

#endif
