
#include "utest_globals.hpp"

#include "ParserTestInc.hpp"

#ifdef RUN_TEST_OLD

OPTILAB_TEST(ParseStringVariable, stdSingletonDecl)
{
  using namespace CLI;
  
  auto o1 = parsePP("x:10;");
  std::string s1{"|1=variable|10=x|11=integer|14=expr|15=10|19=0|20=0"};
  ASSERT_EQ(o1, s1);
  
  auto o2 = parsePP("x:+0;");
  std::string s2{"|1=variable|10=x|11=integer|14=expr|15=+0|19=0|20=0"};
  ASSERT_EQ(o2, s2);
  
  auto o3 = parsePP("x:-0;");
  std::string s3{"|1=variable|10=x|11=integer|14=expr|15=-0|19=0|20=0"};
  ASSERT_EQ(o3, s3);
  
  auto o4 = parsePP("x:-5;");
  std::string s4{"|1=variable|10=x|11=integer|14=expr|15=-5|19=0|20=0"};
  ASSERT_EQ(o4, s4);
  
  auto o5 = parsePP("x:INF;");
  std::string s5{"|1=variable|10=x|11=integer|14=expr|15="};
  s5 += infToString();
  s5 += "|19=0|20=0";
  ASSERT_EQ(o5, s5);
  
  auto o6 = parsePP("x:+INF;");
  std::string s6{"|1=variable|10=x|11=integer|14=expr|15="};
  s6 += "+";
  s6 += infToString();
  s6 += "|19=0|20=0";
  ASSERT_EQ(o6, s6);
  
  auto o7 = parsePP("x:-INF;");
  std::string s7{"|1=variable|10=x|11=integer|14=expr|15="};
  s7 += "-";
  s7 += infToString();
  s7 += "|19=0|20=0";
  ASSERT_EQ(o7, s7);
}//stdSingletonDecl

OPTILAB_TEST(ParseStringVariable, annotationList)
{
  using namespace CLI;
  
  auto o1 = parsePP("x:10, test: string;");
  std::string s1{"|1=variable|8=test string|10=x|11=integer|14=expr|15=10|19=0|20=0"};
  ASSERT_EQ(o1, s1);
  
  auto o2 = parsePP("my_var:10, a: b, c: d;");
  std::string s2{"|1=variable|8=a b c d|10=my_var|11=integer|14=expr|15=10|19=0|20=0"};
  ASSERT_EQ(o2, s2);
  
  auto o3 = parsePP("my_var:10..11, a: b, c: d;");
  std::string s3{"|1=variable|8=a b c d|10=my_var|11=integer|14=range|15=10 11|19=0|20=0"};
  ASSERT_EQ(o3, s3);
  
  auto o4 = parsePP("my_var:{-10, 11, 12}, a: b, c: d;");
  std::string s4{"|1=variable|8=a b c d|10=my_var|11=integer|14=list|15=-10 11 12|19=0|20=0"};
  ASSERT_EQ(o4, s4);
}//annotationList

OPTILAB_TEST(ParseStringVariable, stdRangeDecl)
{
  using namespace CLI;
  
  auto prs = parsePP("x:10..11;");
  std::string trg{"|1=variable|10=x|11=integer|14=range|15=10 11|19=0|20=0"};
  ASSERT_EQ(prs, trg);
  
  prs = parsePP("x:10..011;");
  ASSERT_EQ(prs, trg);
  
  prs = parsePP("x:-10..-5;");
  trg = "|1=variable|10=x|11=integer|14=range|15=-10 -5|19=0|20=0";
  ASSERT_EQ(prs, trg);
  
  prs = parsePP("x:-10..5;");
  trg = "|1=variable|10=x|11=integer|14=range|15=-10 5|19=0|20=0";
  ASSERT_EQ(prs, trg);
  
  prs = parsePP("x:-10..+5;");
  trg = "|1=variable|10=x|11=integer|14=range|15=-10 +5|19=0|20=0";
  ASSERT_EQ(prs, trg);
  
  prs = parsePP("x:-INF..+INF;");
  trg = "|1=variable|10=x|11=integer|14=range|15=-";
  trg += infToString();
  trg += " +";
  trg += infToString();
  trg += "|19=0|20=0";
  ASSERT_EQ(prs, trg);
}//stdRangeDecl

OPTILAB_TEST(ParseStringVariable, stdSetDecl)
{
  using namespace CLI;
  
  auto o1 = parsePP("x:{10,11, 12};");
  std::string s1{"|1=variable|10=x|11=integer|14=list|15=10 11 12|19=0|20=0"};
  ASSERT_EQ(o1, s1);
  
  auto o2 = parsePP("x:{true, false, INF};");
  std::string s2{"|1=variable|10=x|11=boolean|14=list|15=1 0 "};
  s2 += infToString();
  s2 += "|19=0|20=0";
  ASSERT_EQ(o2, s2);
  
  auto o3 = parsePP("x:{10};");
  std::string s3{"|1=variable|10=x|11=integer|14=list|15=10|19=0|20=0"};
  ASSERT_EQ(o3, s3);
  
  auto o4 = parsePP("x:{-10, +10, 10};");
  std::string s4{"|1=variable|10=x|11=integer|14=list|15=-10 +10 10|19=0|20=0"};
  ASSERT_EQ(o4, s4);
}//stdSetDecl

OPTILAB_TEST(ParseStringVariable, infDecl)
{
  using namespace CLI;
  
  auto o1 = parsePP("x:INF;");
  std::string s1{"|1=variable|10=x|11=integer|14=expr|15="};
  s1 += infToString();
  s1 += "|19=0|20=0";
  ASSERT_EQ(o1, s1);
  
  auto o2 = parsePP("x:+INF;");
  std::string s2{"|1=variable|10=x|11=integer|14=expr|15=+"};
  s2 += infToString();
  s2 += "|19=0|20=0";
  ASSERT_EQ(o2, s2);
  
  auto o3 = parsePP("x:-INF;");
  std::string s3{"|1=variable|10=x|11=integer|14=expr|15=-"};
  s3 += infToString();
  s3 += "|19=0|20=0";
  ASSERT_EQ(o3, s3);
}//infDecl

OPTILAB_TEST(ParseStringVariable, trueFalseDecl)
{
  using namespace CLI;
  
  auto o1 = parsePP("x:true;");
  std::string s1{"|1=variable|10=x|11=boolean|14=expr|15=1|19=0|20=0"};
  ASSERT_EQ(o1, s1);
  
  auto o2 = parsePP("x:false;");
  std::string s2{"|1=variable|10=x|11=boolean|14=expr|15=0|19=0|20=0"};
  ASSERT_EQ(o2, s2);
  
  auto o3 = parsePP("x:+true;");
  std::string s3{"|1=variable|10=x|11=boolean|14=expr|15=+1|19=0|20=0"};
  ASSERT_EQ(o3, s3);
  
  auto o4 = parsePP("x:-true;");
  std::string s4{"|1=variable|10=x|11=boolean|14=expr|15=-1|19=0|20=0"};
  ASSERT_EQ(o4, s4);
  
  auto o5 = parsePP("x:+false;");
  std::string s5{"|1=variable|10=x|11=boolean|14=expr|15=+0|19=0|20=0"};
  ASSERT_EQ(o5, s5);
  
  auto o6 = parsePP("x:-false;");
  std::string s6{"|1=variable|10=x|11=boolean|14=expr|15=-0|19=0|20=0"};
  ASSERT_EQ(o6, s6);
}//trueFalseDecl

OPTILAB_TEST(ParseStringVariable, outputField)
{
  using namespace CLI;
  
  auto o = parsePP("x:3, output:true;");
  std::string s{"|1=variable|8=output 1|10=x|11=integer|13=1|14=expr|15=3|19=0|20=0"};
  ASSERT_EQ(o, s);
  
  o = parsePP("x:3, output:false;");
  s = "|1=variable|8=output 0|10=x|11=integer|13=0|14=expr|15=3|19=0|20=0";
  ASSERT_EQ(o, s);
}//outputField

OPTILAB_TEST(ParseStringVariable, semanticObjectiveField)
{
  using namespace CLI;
  
  auto o = parsePP("x:3, semantic:objective, extremum:minimize;");
  std::string s{""};
  s = "|1=variable|8=semantic objective extremum minimize|10=x|11=integer|12=objective|14=expr|15=3|17=minimize|19=0|20=0";
  ASSERT_EQ(o, s);
  
  o = parsePP("x:3, semantic:objective, extremum:maximize;");
  s = "|1=variable|8=semantic objective extremum maximize|10=x|11=integer|12=objective|14=expr|15=3|17=maximize|19=0|20=0";
  ASSERT_EQ(o, s);
  
  o = parsePP("x:3, extremum:maximize;");
  s = "|1=variable|8=extremum maximize|10=x|11=integer|14=expr|15=3|17=maximize|19=0|20=0";
  ASSERT_EQ(o, s);
  
  o = parsePP("x:3, extremum:minimize;");
  s = "|1=variable|8=extremum minimize|10=x|11=integer|14=expr|15=3|17=minimize|19=0|20=0";
  ASSERT_EQ(o, s);
  
  o = parsePP("x:3, semantic:objective;");
  s = "|1=variable|8=semantic objective|10=x|11=integer|12=objective|14=expr|15=3|19=0|20=0";
  ASSERT_EQ(o, s);
}//outputField

OPTILAB_TEST(ParseStringVariable, semanticDecisionField)
{
  using namespace CLI;
  
  auto o = parsePP("x:3, semantic:decision;");
  std::string s{"|1=variable|8=semantic decision|10=x|11=integer|12=decision|14=expr|15=3|19=0|20=0"};
  ASSERT_EQ(o, s);
}//semanticDecisionField

OPTILAB_TEST(ParseStringVariable, branchingField)
{
  using namespace CLI;
  
  auto o = parsePP("x:3, semantic:decision, branching:true;");
  std::string s{"|1=variable|8=semantic decision branching 1|10=x|11=integer|12=decision|14=expr|15=3|16=1|19=0|20=0"};
  ASSERT_EQ(o, s);
  
  o = parsePP("x:3, semantic:decision, branching:false;");
  s = "|1=variable|8=semantic decision branching 0|10=x|11=integer|12=decision|14=expr|15=3|16=0|19=0|20=0";
  ASSERT_EQ(o, s);
}//outputField

OPTILAB_TEST(ParseStringVariable, domainExpr)
{
  using namespace CLI;
  
  auto o = parsePP("x:(3);");
  std::string s{"|1=variable|10=x|11=integer|14=expr|15=3|19=0|20=0"};
  ASSERT_EQ(o, s);
  
  o = parsePP("x:(3+5);");
  s = "|1=variable|10=x|11=integer|14=expr|15=8|19=0|20=0";
  ASSERT_EQ(o, s);
}//domainExpr

OPTILAB_TEST(ParseStringVariable, matrixDeclaration)
{
  using namespace CLI;
  
  auto o = parsePP("x:1..2:[2];");
  std::string s{"|1=variable|10=x|11=integer|14=range|15=1 2|19=1|20=2"};
  ASSERT_EQ(o, s);
  
  o = parsePP("x:1..2:[2, 3];");
  s = "|1=variable|10=x|11=integer|14=range|15=1 2|19=2|20=3";
  ASSERT_EQ(o, s);
  
  // Matrices with dimensions greater than 2 are not parsed
  o = parsePP("x:1..2:[2, 3, 4];");
  s = "|1=variable|10=x|11=integer|14=range|15=1 2|19=2|20=3";
  ASSERT_EQ(o, s);
}//matrixDeclaration

OPTILAB_TEST(ParseStringVariable, declarationWithExpr)
{
  using namespace CLI;
  
  auto o = parsePP("x:1..m[2];");
  std::string s{"|1=variable|10=x|11=integer|14=range|15=1 m[2]|19=0|20=0"};
  ASSERT_EQ(o, s);
  
  o = parsePP("x:1..2:[2, x[3]];");
  s = "|1=variable|10=x|11=integer|14=range|15=1 2|19=2|20=x[3]";
  ASSERT_EQ(o, s);
}//matrixDeclaration

#endif

