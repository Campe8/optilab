
#include "utest_globals.hpp"

#include "ParserTestInc.hpp"

namespace {
  CLI::CLIStmt getCLIFromPrettyPrint(const std::string& aStr)
  {
    return CLI::Utils::getCLIStmtFromString(CLI::Utils::cliStmtStringFromPrettyPrint(aStr));
  }
}// end unamed namespace

OPTILAB_TEST(CLIUtils, getMsgType)
{
  // Test getMsgType
  using namespace CLI;
  using namespace Utils;
  
  auto varCLI = getCLIFromPrettyPrint("|1=variable|10=x|11=integer|14=expr|15=10");
  ASSERT_EQ(CLIMsgType::CLI_MSG_VAR_DECL, getMsgType(varCLI));
  
  auto conCLI = getCLIFromPrettyPrint("|1=constraint|10=expr|11=c|12=3");
  ASSERT_EQ(CLIMsgType::CLI_MSG_CON_DECL, getMsgType(conCLI));
  
  auto matCLI = getCLIFromPrettyPrint("|1=matrix|10=x|11=integer|12=0|13=0");
  ASSERT_EQ(CLIMsgType::CLI_MSG_MATRIX_DECL, getMsgType(matCLI));
  
  auto fcnCLI = getCLIFromPrettyPrint("|1=fcn_call|10=fcn|11=2|12=x y");
  ASSERT_EQ(CLIMsgType::CLI_MSG_FCN_CALL, getMsgType(fcnCLI));
  
  auto srcCLI = getCLIFromPrettyPrint("|1=search|10=dfs|11=input_order|12=indomain_min");
  ASSERT_EQ(CLIMsgType::CLI_MSG_SRC_DECL, getMsgType(srcCLI));
  
  auto errCLI = getCLIFromPrettyPrint("|1=error|10=error");
  ASSERT_EQ(CLIMsgType::CLI_MSG_ERR, getMsgType(errCLI));
  
  auto udfCLI = getCLIFromPrettyPrint("|1=undef");
  ASSERT_EQ(CLIMsgType::CLI_MSG_UNDEF, getMsgType(udfCLI));
}//getMsgType
