#include "ParserTestInc.hpp"

#include <sstream>

std::string parsePP(const std::string& aInString)
{
  using namespace CLI;
  
  std::vector<char> strVec(aInString.length() + 1);
  std::strcpy(&strVec[0], aInString.c_str());
  
  //char* strVecCharPtr = &strVec[0];
  char outCharPtr[128]{0};
  //parseString(strVecCharPtr, outCharPtr);
  
  std::string outStr(outCharPtr);
  outStr = Utils::prettyPrintStringFromCLIStmt(outStr);
  
  return outStr;
}//parsePP

CLI::CLIStmt parseStmt(const std::string& aInString)
{
  using namespace CLI;
  
  std::vector<char> strVec(aInString.length() + 1);
  std::strcpy(&strVec[0], aInString.c_str());
  
  //char* strVecCharPtr = &strVec[0];
  char outCharPtr[128]{0};
  //parseString(strVecCharPtr, outCharPtr);
  
  std::string outStr(outCharPtr);
  return Utils::getCLIStmtFromString(outStr);
}//aInString

std::string infToString()
{
  auto inf = CLI_INF;
  std::stringstream ss;
  ss << inf;
  return ss.str();
}//infToString

