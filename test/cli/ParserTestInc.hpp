#pragma once

// Macros
#include "CLIMacro.hpp"

// Parser
//#include "parser_ext.h"

// Utilities
#include "CLIUtils.hpp"

#include <cstring>
#include <cassert>

/// Parses "inString" and returns the pretty print string
std::string parsePP(const std::string& aInString);

/// Parses "inString" and returns the correspondent CLI stmt
CLI::CLIStmt parseStmt(const std::string& aInString);

/// Returns the string representing "INF"
std::string infToString();
