
#include "utest_globals.hpp"

#include "MetricsRegister.hpp"

OPTILAB_TEST(MetricsRegister, registerMetric)
{
  // Test metric registration
  using namespace Base::Tools;
  
  MetricsRegister mr;
  MetricsRegister::MetricId id{"metric"};
  
  ASSERT_FALSE(mr.isMetricRegistered(id));
  
  // Register metric
  mr.addMetric(id);
  ASSERT_TRUE(mr.isMetricRegistered(id));
}//registerAndUpdateMetric

OPTILAB_TEST(MetricsRegister, updateMetric)
{
  // Test metric value update
  using namespace Base::Tools;
  
  MetricsRegister mr;
  MetricsRegister::MetricId id{"metric"};
  MetricsRegister::MetricValue val{1000};

  // Register metric
  mr.addMetric(id);
  EXPECT_EQ(0, mr.getMetric(id));
  
  // Update the metric
  mr.updateMetric(id, val);
  EXPECT_EQ(val, mr.getMetric(id));
}//updateMetric

OPTILAB_TEST(MetricsRegister, throwErrors)
{
  // Test metric invalid operations
  using namespace Base::Tools;
  
  MetricsRegister mr;
  MetricsRegister::MetricId id{"metric"};
  
  // Update a metric that is not registered should throw
  EXPECT_THROW(mr.updateMetric(id, 1000), std::invalid_argument);
  
  // Get a metric that is not registered should throw
  EXPECT_THROW(mr.getMetric(id), std::invalid_argument);
  
  // Register metric twice, the second time should throw
  mr.addMetric(id);
  EXPECT_THROW(mr.addMetric(id), std::invalid_argument);
}//throwErrors
