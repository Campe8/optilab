
#include "utest_globals.hpp"

#include "BaseTools.hpp"

OPTILAB_TEST(BaseTools, RandomGenerator)
{
  // Test random generator class
  using namespace Base::Tools;
  
  // Create a random generator given a fixed seed 0
  RandomGenerator rg1(0);
  ASSERT_EQ(0, rg1.getSeed());
  
  std::vector<int> rndIntVec;
  for (int idx{0}; idx < 10; ++idx) rndIntVec.push_back(rg1.randInt(0, 100));
  
  std::vector<double> rndDoubleVec;
  for (int idx{0}; idx < 10; ++idx) rndDoubleVec.push_back(rg1.randReal(0, 100.0));
  
  // Create another random generator with same seed and verify that the random
  // generated numbers are the same
  RandomGenerator rg2(0);
  for (int idx{0}; idx < 10; ++idx)
  {
    EXPECT_EQ(rndIntVec[idx], rg2.randInt(0, 100));
  }
  
  for (int idx{0}; idx < 10; ++idx)
  {
    EXPECT_EQ(rndDoubleVec[idx], rg2.randReal(0, 100.0));
  }
  
  // Check that another random generator with different seed returns a different
  // sequence of numbers.
  // Verify for the first 5 numbers
  RandomGenerator rg3(42);
  for (int idx{0}; idx < 5; ++idx)
  {
    EXPECT_NE(rndIntVec[idx], rg3.randInt(0, 100));
  }
}//RandomGenerator
