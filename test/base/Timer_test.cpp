
#include "utest_globals.hpp"

#include "Timer.hpp"

#include <thread>  // for std::this_thread::sleep_fo
#include <chrono>  // for std::chrono::seconds

OPTILAB_TEST(Timer, isRunnning)
{
  // Test Timer class constructor, check (non) running timer
  using namespace Base::Tools;
  
  // Create a timer and test that is running or not depending on the constructor flag
  Timer t1;
  Timer t2(false);
  
  EXPECT_TRUE(t1.isRunning());
  EXPECT_FALSE(t2.isRunning());
}//isRunnning

OPTILAB_TEST(Timer, wallClockTime)
{
  // Test wallclock time
  using namespace Base::Tools;
  
  // Create timer (this starts the timer)
  Timer t1;
  
  // Put this thread into sleep for 50 msec
  std::this_thread::sleep_for (std::chrono::milliseconds(50));
  
  // Get the wallclock time and check that is at in [50, 60) time span
  auto msec = t1.getWallClockTime();
  EXPECT_LE(50, msec);
  EXPECT_GT(60, msec);
  
  // Reset the timer and stops it
  t1.reset();
  EXPECT_EQ(0, t1.getWallClockTime());
}//wallClockTime
