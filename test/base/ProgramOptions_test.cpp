
#include "utest_globals.hpp"

#include "ProgramOptions.hpp"

#include <stdexcept>  // for std::logic_error

OPTILAB_TEST(ProgramOptions, registerInvalidOptions)
{
  // Test invalid option name registration
  Base::ProgramOptions popt;
  
  char sopt = 'o';
  std::string opt{"option"};
  popt.registerOption("", sopt, false);
  popt.registerOption(opt, '*', false);
  
  EXPECT_FALSE(popt.isOptionRegistered(opt));
  EXPECT_FALSE(popt.isOptionRegistered(sopt));
  EXPECT_FALSE(popt.isOptionSelected(opt));
  EXPECT_FALSE(popt.isOptionSelected(sopt));
}//registerInvalidOptions

OPTILAB_TEST(ProgramOptions, parseInvalidInput)
{
  // Test that parsing invalid input throws a logic error
  Base::ProgramOptions popt;
  
  std::string pname{"program"};
  char* argv[] = {const_cast<char*>(pname.c_str())};
  EXPECT_THROW(popt.parseOptions(0, argv), std::logic_error);
  EXPECT_THROW(popt.parseOptions(1, nullptr), std::logic_error);
}//parseInvalidInput

OPTILAB_TEST(ProgramOptions, parsingUnregisteredOptions)
{
  // Test that parsing unregistered options does not throw nor record values
  Base::ProgramOptions popt;
  
  std::string pname{"program"};
  std::string opt{"opt"};
  std::string val{"val"};
  std::string optVal = "--" + opt + "=" + val;
  int argc = 2;
  char* argv[] = { const_cast<char*>(pname.c_str()), const_cast<char*>(optVal.c_str())};
  
  ASSERT_NO_THROW(popt.parseOptions(argc, argv));
  EXPECT_FALSE(popt.isOptionRegistered(opt));
  EXPECT_FALSE(popt.isOptionSelected(opt));
  EXPECT_EQ(0, popt.getOptionValues(opt).size());
  EXPECT_TRUE(popt.getOptionValue(opt).empty());
}//parsingUnregisteredOptions

OPTILAB_TEST(ProgramOptions, parsingRegisteredOptions)
{
  // Test that parsing registered options does not throw and record option values
  Base::ProgramOptions popt;
  
  std::string pname{"program"};
  std::string opt1{"opt1"};
  std::string val{"val"};
  std::string optVal1 = "--" + opt1 + "=" + val;
  
  std::string opt2{"opt2"};
  std::string optVal2 = "--" + opt2;
  
  std::string opt3{"opt3"};
  std::string optVal3 = "--" + opt3;
  
  popt.registerOption(opt1, 'a', true);
  popt.registerOption(opt2, 'b', false);
  
  int argc = 4;
  char* argv[] =
  { const_cast<char*>(pname.c_str()),
    const_cast<char*>(optVal1.c_str()),
    const_cast<char*>(optVal2.c_str()),
    const_cast<char*>(optVal3.c_str())
  };
  
  ASSERT_NO_THROW(popt.parseOptions(argc, argv));
  
  EXPECT_TRUE(popt.isOptionRegistered(opt1));
  EXPECT_TRUE(popt.isOptionRegistered('a'));
  EXPECT_TRUE(popt.isArgumentRequired(opt1));
  EXPECT_TRUE(popt.isArgumentRequired('a'));
  EXPECT_TRUE(popt.isOptionSelected(opt1));
  EXPECT_TRUE(popt.isOptionSelected('a'));
  EXPECT_EQ(1, popt.getOptionValues(opt1).size());
  EXPECT_TRUE(popt.getOptionValue(opt1) == val);
  
  EXPECT_TRUE(popt.isOptionRegistered(opt2));
  EXPECT_TRUE(popt.isOptionRegistered('b'));
  EXPECT_FALSE(popt.isArgumentRequired(opt2));
  EXPECT_FALSE(popt.isArgumentRequired('b'));
  EXPECT_TRUE(popt.isOptionSelected(opt2));
  EXPECT_TRUE(popt.isOptionSelected('b'));
  EXPECT_EQ(0, popt.getOptionValues(opt2).size());
  EXPECT_TRUE(popt.getOptionValue(opt2).empty());
  
  EXPECT_FALSE(popt.isOptionRegistered(opt3));
  EXPECT_FALSE(popt.isArgumentRequired(opt3));
  EXPECT_FALSE(popt.isOptionSelected(opt3));
  EXPECT_EQ(0, popt.getOptionValues(opt3).size());
  EXPECT_TRUE(popt.getOptionValue(opt3).empty());
}//parsingRegisteredOptions

OPTILAB_TEST(ProgramOptions, toStream)
{
  // Test toStream method
  Base::ProgramOptions popt;
  
  std::string pname{"program"};
  std::string opt1{"opt1"};
  std::string opt2{"opt2"};
  
  popt.registerOption(opt1, 'a', true, "first option");
  popt.registerOption(opt2, 'b', false, "second option");
  
  std::stringstream ss;
  popt.toStream(ss);
  std::string streamStr = ss.str();
  EXPECT_NE(streamStr.find("OptiLab options:"), std::string::npos);
  EXPECT_NE(streamStr.find("-a [ --opt1 ] arg     first option"), std::string::npos);
  EXPECT_NE(streamStr.find("-b [ --opt2 ]         second option"), std::string::npos);
}//toStream
