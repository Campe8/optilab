
#include "utest_globals.hpp"

#include "AES.hpp"

OPTILAB_TEST(AES, encryptDecrypt)
{
  // Test AES encrypt/decrypt algorithm
  using namespace Base::Tools;
  
  AES aes("abcdefghilmnopqr");
  std::string msg = "this is a test 1";
  
  auto encMsg = aes.encrypt(msg);
  ASSERT_NE(msg, encMsg);
  ASSERT_EQ(msg.size(), encMsg.size());
  
  encMsg = aes.decrypt(encMsg);
  ASSERT_EQ(msg, encMsg);
  ASSERT_EQ(msg.size(), encMsg.size());
}//encryptDecrypt

OPTILAB_TEST(AES, encryptDecryptMsg)
{
  // Test AES encrypt/decrypt algorithm on short/long text
  using namespace Base::Tools;
  
  // Test encryption/decription of short messages
  AES aes("abcdefghilmnopqr");
  std::string msg = "test";
  
  std::string encMsg;
  ASSERT_NO_THROW(encMsg = aes.encryptMsg(msg));
  ASSERT_NE(msg, encMsg);
  ASSERT_EQ(16, encMsg.size());
  
  encMsg = aes.decryptMsg(encMsg);
  ASSERT_EQ(msg, encMsg);
  
  // Test encryption/decription of long messages
  msg = "this is a very long long message to test the AES algorithm\n";
  ASSERT_NO_THROW(encMsg = aes.encryptMsg(msg));
  EXPECT_NE(msg, encMsg);
  
  ASSERT_NO_THROW(encMsg = aes.decryptMsg(encMsg));
  EXPECT_EQ(msg, encMsg);
}//encryptDecryptMsg
