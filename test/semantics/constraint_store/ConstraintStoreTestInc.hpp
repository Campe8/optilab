#pragma once

#include "ConstraintStore.hpp"

#include "ConstraintTestInc.hpp"
#include "DomainTestInc.hpp"
#include "ConstraintEq.hpp"
#include "ConstraintNq.hpp"
#include "ConstraintFactory.hpp"
#include "ConstraintParameterFactory.hpp"

#include "VariableInteger.hpp"
#include "VariableSemanticDecision.hpp"
#include "VariableFactory.hpp"
#include "PropagatorTest.hpp"

namespace Core {
  typedef std::shared_ptr<ConstraintNq> ConstraintNqSPtr;
  typedef std::shared_ptr<ConstraintEq> ConstraintEqSPtr;
  
  class ConstraintNqTest {
  public:
    ConstraintNqTest(const VariableSPtr& aV1, const VariableSPtr& aV2)
    {
      // Create new constraint parameter with variable
      ConstraintParameterFactory cpf;
      ConstraintFactory cf;
      auto cnq = cf.constraint(Core::ConstraintId::CON_ID_NQ,
                               {cpf.constraintParameterVariable(aV1), cpf.constraintParameterVariable(aV2)},
                               PROPAGATOR_STRATEGY_TYPE_BOUND);
      pConstraintNq = std::shared_ptr<ConstraintNq>(static_cast<ConstraintNq*>(cnq));
    }
    
    inline ConstraintNqSPtr getConstraint() { return pConstraintNq; }
    
  private:
    ConstraintNqSPtr pConstraintNq;
  };
  
  class ConstraintEqTest {
  public:
    ConstraintEqTest(const VariableSPtr& aV1, const VariableSPtr& aV2)
    {
      // Create new constraint parameter with variable
      ConstraintParameterFactory cpf;
      ConstraintFactory cf;
      auto ceq = cf.constraint(Core::ConstraintId::CON_ID_EQ,
                               {cpf.constraintParameterVariable(aV1), cpf.constraintParameterVariable(aV2)},
                               PROPAGATOR_STRATEGY_TYPE_BOUND);
      pConstraintEq = std::shared_ptr<ConstraintEq>(static_cast<ConstraintEq*>(ceq));
    }
    
    ConstraintEqTest()
    {
      std::string varName1{"VarTest1"};
      std::string varName2{"VarTest2"};
      VariableSemanticUPtr semanticDecision1(new VariableSemanticDecision(varName1));
      VariableSemanticUPtr semanticDecision2(new VariableSemanticDecision(varName2));
      
      VariableFactoryUPtr varFactory(new VariableFactory());
      VariableSPtr varInt1(varFactory->variableInteger(-10, +5, std::move(semanticDecision1)));
      VariableSPtr varInt2(varFactory->variableInteger(-10, +10, std::move(semanticDecision2)));
      
      // Create new constraint parameter with variable
      ConstraintParameterFactory cpf;
      ConstraintFactory cf;
      auto ceq = cf.constraint(Core::ConstraintId::CON_ID_EQ,
                               {cpf.constraintParameterVariable(varInt1), cpf.constraintParameterVariable(varInt2)},
                               PROPAGATOR_STRATEGY_TYPE_BOUND);
      pConstraintEq = std::shared_ptr<ConstraintEq>(static_cast<ConstraintEq*>(ceq));
      
      auto ceq2 = cf.constraint(Core::ConstraintId::CON_ID_EQ,
                                {cpf.constraintParameterVariable(varInt1), cpf.constraintParameterVariable(varInt2)},
                                PROPAGATOR_STRATEGY_TYPE_DOMAIN);
      pConstraintEqHC = std::shared_ptr<ConstraintEq>(static_cast<ConstraintEq*>(ceq2));
    }
    
    inline ConstraintEqSPtr getConstraint()
    {
      return pConstraintEq;
    }
    
    inline ConstraintEqSPtr getConstraintWithHigherCost()
    {
      return pConstraintEqHC;
    }
    
  private:
    ConstraintEqSPtr pConstraintEq;
    ConstraintEqSPtr pConstraintEqHC;
  };
  
  class ConstraintUnsatTest {
  public:
    ConstraintUnsatTest()
    {
      std::string varName1{"V1"};
      std::string varName2{"V2"};
      VariableSemanticUPtr semanticDecision1(new VariableSemanticDecision(varName1));
      VariableSemanticUPtr semanticDecision2(new VariableSemanticDecision(varName2));
      
      VariableFactoryUPtr varFactory(new VariableFactory());
      VariableSPtr varInt1(varFactory->variableInteger(+5, +5, std::move(semanticDecision1)));
      VariableSPtr varInt2(varFactory->variableInteger(+6, +6, std::move(semanticDecision2)));
      
      // Create new constraint parameter with variable
      ConstraintParameterFactory cpf;
      ConstraintFactory cf;
      auto ceq = cf.constraint(Core::ConstraintId::CON_ID_EQ,
                               {cpf.constraintParameterVariable(varInt1), cpf.constraintParameterVariable(varInt2)},
                               PROPAGATOR_STRATEGY_TYPE_BOUND);
      pConstraintEq = std::shared_ptr<ConstraintEq>(static_cast<ConstraintEq*>(ceq));
    }
  
    inline ConstraintEqSPtr getConstraint()
    {
      return pConstraintEq;
    }
  
  private:
    ConstraintEqSPtr pConstraintEq;
  };
  
  class ConstraintSatTest {
  public:
    ConstraintSatTest()
    {
      std::string varName1{"V1"};
      std::string varName2{"V2"};
      VariableSemanticUPtr semanticDecision1(new VariableSemanticDecision(varName1));
      VariableSemanticUPtr semanticDecision2(new VariableSemanticDecision(varName2));
      
      VariableFactoryUPtr varFactory(new VariableFactory());
      VariableSPtr varInt1(varFactory->variableInteger(+6, +6, std::move(semanticDecision1)));
      VariableSPtr varInt2(varFactory->variableInteger(+6, +6, std::move(semanticDecision2)));
      
      // Create new constraint parameter with variable
      ConstraintParameterFactory cpf;
      ConstraintFactory cf;
      auto ceq = cf.constraint(Core::ConstraintId::CON_ID_EQ,
                               {cpf.constraintParameterVariable(varInt1), cpf.constraintParameterVariable(varInt2)},
                               PROPAGATOR_STRATEGY_TYPE_BOUND);
      pConstraintEq = std::shared_ptr<ConstraintEq>(static_cast<ConstraintEq*>(ceq));
    }
    
    inline ConstraintEqSPtr getConstraint()
    {
      return pConstraintEq;
    }
    
  private:
    ConstraintEqSPtr pConstraintEq;
  };
  
}// end namespace Core

namespace Semantics {
  
  class ConstraintStoreTest : public ConstraintStore
  {
  public:
    ConstraintStoreTest()
    : ConstraintStore()
    {
    }
    
    ~ConstraintStoreTest()
    {
    }
  
    void registerConstraint(Core::Constraint* aConstraint)
    {
      ConstraintStore::registerConstraint(aConstraint);
    }
    
    void deregisterConstraint(Core::Constraint* aConstraint)
    {
      ConstraintStore::deregisterConstraint(aConstraint);
    }
    
    void reevaluateConstraint(Core::Constraint* aConstraint)
    {
      ConstraintStore::reevaluateConstraint(aConstraint);
    }
    
    Core::Constraint* getNextConstraintToReevaluate()
    {
      return ConstraintStore::getNextConstraintToReevaluate();
    }
  };
  
}// end namespace Semantics
