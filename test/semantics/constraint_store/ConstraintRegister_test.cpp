
#include "utest_globals.hpp"

#include "ConstraintStoreTestInc.hpp"
#include "ConstraintRegister.hpp"

OPTILAB_TEST(ConstraintRegisterTest, InterfaceRegisterRemoveConstraintTest)
{
  // Test interface for registering and removing constraints
  using namespace Core;
  using namespace Semantics;
  
  ConstraintRegister reg;
  ASSERT_EQ(0, reg.getNumRegisteredConstraints());
  
  ConstraintEqTest constraintEqTest;
  ConstraintEqSPtr constraint = constraintEqTest.getConstraint();
  reg.registerConstraint(constraint.get());
  ASSERT_EQ(1, reg.getNumRegisteredConstraints());
  
  // Remove the registered constraint.
  // Size of the register should be zero
  reg.removeConstraint(constraint->getUUID());
  ASSERT_EQ(0, reg.getNumRegisteredConstraints());
}//InterfaceRegisterRemoveConstraintTest

OPTILAB_TEST(ConstraintRegisterTest, InterfaceRegisterStoreConstraintTest)
{
  // Test interface for registering and storing constraints
  using namespace Core;
  using namespace Semantics;
  
  ConstraintRegister reg;
  
  ConstraintEqTest constraintEqTest1;
  ConstraintEqTest constraintEqTest2;
  ConstraintEqSPtr con1 = constraintEqTest1.getConstraint();
  ConstraintEqSPtr con2 = constraintEqTest2.getConstraint();
  reg.registerConstraint(con1.get());
  reg.storeConstraint(con2.get());
  ASSERT_EQ(2, reg.getNumRegisteredConstraints());
  
  // Remove the stored constraint: it shouldn't remove it
  reg.removeConstraint(con2->getUUID());
  ASSERT_EQ(2, reg.getNumRegisteredConstraints());
  
  // Both keys should be registered
  ASSERT_TRUE(reg.isKeyRegistered(con1->getUUID()));
  ASSERT_TRUE(reg.isKeyRegistered(con2->getUUID()));
  
  // Clearing the register should clear only the registered constraint
  reg.clearRegister();
  ASSERT_EQ(1, reg.getNumRegisteredConstraints());
  
  // Remove stored constraint
  reg.removeStoredConstraint(con2->getUUID());
  ASSERT_EQ(0, reg.getNumRegisteredConstraints());
  
  // Register and store again the constraints
  reg.registerConstraint(con1.get());
  reg.storeConstraint(con2.get());
  
  // Deep clean should clear all constraints
  reg.deepClearRegister();
  ASSERT_EQ(0, reg.getNumRegisteredConstraints());
  ASSERT_FALSE(reg.isKeyRegistered(con1->getUUID()));
  ASSERT_FALSE(reg.isKeyRegistered(con2->getUUID()));
}//InterfaceRegisterStoreConstraintTest

OPTILAB_TEST(ConstraintRegisterTest, InterfaceGetConstraintTest)
{
  // Test interface for retrieving the constraints
  using namespace Core;
  using namespace Semantics;
  
  ConstraintRegister reg;
  
  ConstraintEqTest constraintEqTest1;
  ConstraintEqTest constraintEqTest2;
  ConstraintEqSPtr con1 = constraintEqTest1.getConstraint();
  ConstraintEqSPtr con2 = constraintEqTest2.getConstraint();
  reg.registerConstraint(con1.get());
  reg.storeConstraint(con2.get());
  
  ASSERT_EQ(con1.get(), reg.getConstraint(con1->getUUID()));
  ASSERT_EQ(con2.get(), reg.getConstraint(con2->getUUID()));
}//InterfaceGetConstraintTest

