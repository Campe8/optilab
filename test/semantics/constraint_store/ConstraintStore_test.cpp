
#include "utest_globals.hpp"

#include "ConstraintStoreTestInc.hpp"

OPTILAB_TEST(ConstraintStoreTest, ConstraintStoreDefault)
{
  using namespace Core;
  using namespace Semantics;
  
  ConstraintStoreTest constraintStore;
  ASSERT_EQ(constraintStore.getNumRegisteredConstraints(), 0);
  ASSERT_EQ(constraintStore.runConsistency(), StoreConsistencyStatus::CONSISTENCY_SUBSUMED);
  ASSERT_EQ(constraintStore.getNextConstraintToReevaluate(), nullptr);
  ASSERT_EQ(0, constraintStore.getStdSemanticFitness());
  ASSERT_EQ(0, constraintStore.getNogoodSemanticFitness());
}//ConstraintStoreDefault

OPTILAB_TEST(ConstraintStoreTest, ConstraintStoreDeRegisterConstraint)
{
  using namespace Core;
  using namespace Semantics;
  
  ConstraintEqTest constraintEqTest;
  ConstraintEqSPtr constraint = constraintEqTest.getConstraint();
  
  std::shared_ptr<ConstraintStore> constraintStorePtr = std::make_shared<ConstraintStoreTest>();
  ConstraintStoreTest * cStorePtr = static_cast<ConstraintStoreTest*>(constraintStorePtr.get());
  constraint->subscribeToConstraintStore(constraintStorePtr);
  
  ASSERT_EQ(constraintStorePtr->getNumRegisteredConstraints(), 1);
  
  constraint->unsubscribeFromConstraintStore();
  ASSERT_EQ(cStorePtr->getNumRegisteredConstraints(), 0);
  
  cStorePtr->registerConstraint(constraint.get());
  ASSERT_EQ(cStorePtr->getNumRegisteredConstraints(), 1);
  
  cStorePtr->deregisterConstraint(constraint.get());
  ASSERT_EQ(cStorePtr->getNumRegisteredConstraints(), 0);
}//ConstraintStoreDeRegisterConstraint

OPTILAB_TEST(ConstraintStoreTest, NextConstraintToReevaluate)
{
  using namespace Core;
  using namespace Semantics;
  
  ConstraintEqTest constraintEqTest;
  ConstraintEqSPtr constraint = constraintEqTest.getConstraint();
  ConstraintEqSPtr constraintHigherCost = constraintEqTest.getConstraintWithHigherCost();
  
  std::shared_ptr<ConstraintStore> constraintStorePtr = std::make_shared<ConstraintStoreTest>();
  ConstraintStoreTest * cStorePtr = static_cast<ConstraintStoreTest*>(constraintStorePtr.get());
  ASSERT_EQ(cStorePtr->getNextConstraintToReevaluate(), nullptr);
  
  // getNextConstraintToReevaluate does not depend on order of subscription
  constraintHigherCost->subscribeToConstraintStore(constraintStorePtr);
  constraint->subscribeToConstraintStore(constraintStorePtr);
  // First low cost contraint
  ASSERT_EQ(cStorePtr->getNextConstraintToReevaluate(), constraint.get());
  // Then higher cost constraint
  ASSERT_EQ(cStorePtr->getNextConstraintToReevaluate(), constraintHigherCost.get());
  // No more constraints
  ASSERT_EQ(cStorePtr->getNextConstraintToReevaluate(), nullptr);
}//NextConstraintToReevaluate

OPTILAB_TEST(ConstraintStoreTest, NextConstraintToReevaluateSingleConstraint)
{
  using namespace Core;
  using namespace Semantics;
  
  ConstraintEqTest constraintEqTest;
  ConstraintEqSPtr constraint = constraintEqTest.getConstraint();
  
  std::shared_ptr<ConstraintStore> constraintStorePtr = std::make_shared<ConstraintStoreTest>();
  ConstraintStoreTest * cStorePtr = static_cast<ConstraintStoreTest*>(constraintStorePtr.get());
  ASSERT_EQ(cStorePtr->getNextConstraintToReevaluate(), nullptr);

  constraint->subscribeToConstraintStore(constraintStorePtr);
  // First low cost contraint
  ASSERT_EQ(cStorePtr->getNextConstraintToReevaluate(), constraint.get());
  // No more constraints
  ASSERT_EQ(cStorePtr->getNextConstraintToReevaluate(), nullptr);
}//NextConstraintToReevaluateSingleConstraint

OPTILAB_TEST(ConstraintStoreTest, RunConsistency)
{
  using namespace Core;
  using namespace Semantics;
  
  ConstraintEqTest constraintEqTest;
  ConstraintEqSPtr constraint = constraintEqTest.getConstraint();
  ConstraintEqSPtr constraintHigherCost = constraintEqTest.getConstraintWithHigherCost();
  
  std::shared_ptr<ConstraintStore> constraintStorePtr = std::make_shared<ConstraintStoreTest>();
  constraintHigherCost->subscribeToConstraintStore(constraintStorePtr);
  constraint->subscribeToConstraintStore(constraintStorePtr);
  
  StoreConsistencyStatus event = constraintStorePtr->runConsistency();
  ASSERT_EQ(event, StoreConsistencyStatus::CONSISTENCY_FIXPOINT);
}//RunConsistency

OPTILAB_TEST(ConstraintStoreTest, RunAggressiveConsistencyOnUnsatConstraints)
{
  // Test aggressive consistency on unsat constraints
  using namespace Core;
  using namespace Semantics;
  
  ConstraintUnsatTest ucon1;
  ConstraintUnsatTest ucon2;
  ConstraintEqSPtr con1 = ucon1.getConstraint();
  ConstraintEqSPtr con2 = ucon2.getConstraint();
  
  std::shared_ptr<ConstraintStore> constraintStorePtr = std::make_shared<ConstraintStoreTest>();
  con1->subscribeToConstraintStore(constraintStorePtr);
  con2->subscribeToConstraintStore(constraintStorePtr);
  
  StoreConsistencyStatus event = constraintStorePtr->runAggressiveConsistency();
  ASSERT_EQ(StoreConsistencyStatus::CONSISTENCY_FAILED, event);
  ASSERT_EQ(2, constraintStorePtr->getNumUnsatConstraint());
}//RunAggressiveConsistencyOnUnsatConstraints

OPTILAB_TEST(ConstraintStoreTest, calculateSemanticFitnessUnsatUnsat)
{
  // Test that the semantic fitness is calculated correctly on a set of unsatisfied constraints
  using namespace Core;
  using namespace Semantics;
  
  // Register two unsatisfied equality constraints (5 == 6)
  ConstraintUnsatTest ucon1;
  ConstraintUnsatTest ucon2;
  ConstraintEqSPtr con1 = ucon1.getConstraint();
  ConstraintEqSPtr con2 = ucon2.getConstraint();
  
  std::shared_ptr<ConstraintStore> constraintStorePtr = std::make_shared<ConstraintStoreTest>();
  con1->subscribeToConstraintStore(constraintStorePtr);
  con2->subscribeToConstraintStore(constraintStorePtr);
  
  // Fitness should be greater than 0
  auto fitness = constraintStorePtr->calculateSemanticFitness();
  ASSERT_LT(0, fitness);
  
  // Fitness should be 2 (1 fitness score for each constraint)
  EXPECT_EQ(2, fitness);
  
  // Re-run the fitness function, it should still have the same values among multiple runs
  fitness = constraintStorePtr->calculateSemanticFitness();
  ASSERT_LT(0, fitness);
  EXPECT_EQ(2, fitness);
  
  // Check that the fitness value returned by the calculateSemanticFitness is the sum of
  // the two fitness (standard constraints and nogood constraints)
  EXPECT_EQ(fitness, constraintStorePtr->getNogoodSemanticFitness() +
            constraintStorePtr->getStdSemanticFitness());
  
  // There should be 0 nogood constraints
  EXPECT_EQ(0, constraintStorePtr->getNogoodSemanticFitness());
}//calculateSemanticFitnessUnsatUnsat

OPTILAB_TEST(ConstraintStoreTest, calculateSemanticFitnessSatSat)
{
  // Test that the semantic fitness is calculated correctly on a set of satisfied constraints
  using namespace Core;
  using namespace Semantics;
  
  // Register two satisfied equality constraints (6 == 6)
  ConstraintSatTest ucon1;
  ConstraintSatTest ucon2;
  ConstraintEqSPtr con1 = ucon1.getConstraint();
  ConstraintEqSPtr con2 = ucon2.getConstraint();
  
  std::shared_ptr<ConstraintStore> constraintStorePtr = std::make_shared<ConstraintStoreTest>();
  con1->subscribeToConstraintStore(constraintStorePtr);
  con2->subscribeToConstraintStore(constraintStorePtr);
  
  // Fitness should be equal to 0
  auto fitness = constraintStorePtr->calculateSemanticFitness();
  ASSERT_EQ(0, fitness);
  
  // Check that the fitness value returned by the calculateSemanticFitness is the sum of
  // the two fitness (standard constraints and nogood constraints)
  EXPECT_EQ(fitness, constraintStorePtr->getNogoodSemanticFitness() +
            constraintStorePtr->getStdSemanticFitness());
}//calculateSemanticFitnessSatSat

OPTILAB_TEST(ConstraintStoreTest, calculateSemanticFitnessSatUnsat)
{
  // Test that the semantic fitness is calculated correctly on a set of (un)satisfied constraints
  using namespace Core;
  using namespace Semantics;
  
  // Register two satisfied equality constraints (6 == 6)
  ConstraintSatTest ucon1;
  ConstraintUnsatTest ucon2;
  ConstraintEqSPtr con1 = ucon1.getConstraint();
  ConstraintEqSPtr con2 = ucon2.getConstraint();
  
  std::shared_ptr<ConstraintStore> constraintStorePtr = std::make_shared<ConstraintStoreTest>();
  con1->subscribeToConstraintStore(constraintStorePtr);
  con2->subscribeToConstraintStore(constraintStorePtr);
  
  // Fitness should be greater than 0
  auto fitness = constraintStorePtr->calculateSemanticFitness();
  ASSERT_LT(0, fitness);
  
  // Fitness should be 1 (1 fitness score for the unsatisfied constraint)
  EXPECT_EQ(1, fitness);
  
  // Check that the fitness value returned by the calculateSemanticFitness is the sum of
  // the two fitness (standard constraints and nogood constraints)
  EXPECT_EQ(fitness, constraintStorePtr->getNogoodSemanticFitness() +
            constraintStorePtr->getStdSemanticFitness());
}//calculateSemanticFitnessSatUnsat
