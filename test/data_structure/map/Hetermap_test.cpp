
#include "utest_globals.hpp"

#include "Hetermap.hpp"

OPTILAB_TEST(Hetermap, defaultInterface)
{
  // Test interface on an empty map
  using namespace DataStructure::Map;
  
  Hetermap<std::string> hm;
  ASSERT_EQ(0, hm.size());
  ASSERT_FALSE(hm.isRegistered("test"));
  ASSERT_TRUE(hm.find("test") == hm.end());
}//defaultInterface

OPTILAB_TEST(Hetermap, lookupOnMap)
{
  // Test interface on the map with values
  using namespace DataStructure::Map;
  
  Hetermap<std::string> hm;
  
  // Insert three heterogenous values
  std::string key1{"key1"};
  std::string key2{"key2"};
  std::string key3{"key3"};
  std::string key4{"key4"};
  hm.insert(key1, 10);
  hm.insert(key2, true);
  hm.insert(key3, "test");
  hm.insert(key4, std::string("testString"));
  
  // There should be four elements in the map
  ASSERT_EQ(4, hm.size());
  ASSERT_TRUE(hm.isRegistered(key1));
  ASSERT_TRUE(hm.isRegistered(key2));
  ASSERT_TRUE(hm.isRegistered(key3));
  ASSERT_TRUE(hm.isRegistered(key4));
  ASSERT_FALSE(hm.isRegistered("test"));
  
  // Find should return iterators different from end
  ASSERT_TRUE(hm.find(key1) != hm.end());
  ASSERT_TRUE(hm.find(key2) != hm.end());
  ASSERT_TRUE(hm.find(key3) != hm.end());
  ASSERT_TRUE(hm.find(key4) != hm.end());
  
  // Type of values
  ASSERT_TRUE(hm.isType<int>(key1));
  ASSERT_TRUE(hm.isType<bool>(key2));
  ASSERT_TRUE(hm.isType<const char*>(key3));
  ASSERT_TRUE(hm.isType<std::string>(key4));
  
  // Negative test
  ASSERT_FALSE(hm.isType<bool>(key1));
  
  // Lookup values
  ASSERT_EQ(10, hm.lookupValue<int>(key1));
  ASSERT_EQ(true, hm.lookupValue<bool>(key2));
  ASSERT_EQ(std::string("test"), std::string(hm.lookupValue<const char*>(key3)));
  ASSERT_EQ(std::string("testString"), hm.lookupValue<std::string>(key4));
  
  // Erase values
  hm.erase(key3);
  ASSERT_EQ(3, hm.size());
  ASSERT_FALSE(hm.isRegistered(key3));
  
  // Clear map
  hm.clear();
  ASSERT_EQ(0, hm.size());
}//lookupOnMap
