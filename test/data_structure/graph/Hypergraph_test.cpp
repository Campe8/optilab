
#include "utest_globals.hpp"

#include "Hypergraph.hpp"

#include <string>

namespace DataStructure { namespace Graph {
  
  class NodeCoreTest : public NodeCore {
  public:
    NodeCoreTest(const std::string& id)
    : pId(id)
    {
      pUUID = generateUUIDTag();
    }
    
    inline std::string getNodeCoreTestId() const
    {
      return pId;
    }
    
    inline NodeCoreId getId() const override
    {
      return pUUID;
    }
    
  private:
    std::string pId;
    
    NodeCoreId pUUID;
  };
  
  typedef std::shared_ptr<NodeCoreTest> NodeCoreTestSPtr;
}}// end namespace DataStructure/Graph

OPTILAB_TEST(Hypergraph, nodeCore)
{
  using namespace DataStructure::Graph;
  
  Hypergraph hg;
  
  NodeCoreTestSPtr nodeA = std::make_shared<NodeCoreTest>("A");
  
  // Add an edge {A, A}
  auto idEdge = hg.addEdge({nodeA, nodeA});
  
  // Test node core
  auto nodeCore = hg.getNodeCore((idEdge.second)[nodeA->getId()]);
  ASSERT_EQ(nodeCore->getId(), nodeA->getId());
}//nodeCore

OPTILAB_TEST(Hypergraph, addSimpleNaiveEdge)
{
  using namespace DataStructure::Graph;
  
  Hypergraph hg;
  
  NodeCoreTestSPtr nodeA = std::make_shared<NodeCoreTest>("A");
  
  // Add an edge {A, A}
  auto idEdge = hg.addEdge({nodeA, nodeA});
  
  // NodeA must be connected to itself
  ASSERT_TRUE(hg.connectedNodes(nodeA, nodeA));
  ASSERT_TRUE(hg.connectedNodes((idEdge.second)[nodeA->getId()], (idEdge.second)[nodeA->getId()]));
  
  // Test node core
  auto nodeCore = hg.getNodeCore((idEdge.second)[nodeA->getId()]);
  ASSERT_EQ(nodeCore->getId(), nodeA->getId());
}//addSimpleEdge

OPTILAB_TEST(Hypergraph, addSimpleEdge)
{
  using namespace DataStructure::Graph;
  
  Hypergraph hg;
  
  NodeCoreTestSPtr nodeA = std::make_shared<NodeCoreTest>("A");
  NodeCoreTestSPtr nodeB = std::make_shared<NodeCoreTest>("B");
  
  // Add an edge {A, B}
  auto idEdge = hg.addEdge({nodeA, nodeB});

  // NodeA must be connected to itself
  ASSERT_TRUE(hg.connectedNodes(nodeA, nodeA));
  ASSERT_TRUE(hg.connectedNodes((idEdge.second)[nodeA->getId()], (idEdge.second)[nodeA->getId()]));
  
  // NodeB must be connected to itself
  ASSERT_TRUE(hg.connectedNodes(nodeB, nodeB));
  ASSERT_TRUE(hg.connectedNodes((idEdge.second)[nodeB->getId()], (idEdge.second)[nodeB->getId()]));
  
  // NodeA must be connected to NodeB
  ASSERT_TRUE(hg.connectedNodes(nodeA, nodeB));
  ASSERT_TRUE(hg.connectedNodes((idEdge.second)[nodeA->getId()], (idEdge.second)[nodeB->getId()]));
  
  // NodeB must be connected to NodeA
  ASSERT_TRUE(hg.connectedNodes(nodeB, nodeA));
  ASSERT_TRUE(hg.connectedNodes((idEdge.second)[nodeB->getId()], (idEdge.second)[nodeA->getId()]));
}//addSimpleEdge

OPTILAB_TEST(Hypergraph, addConnectingEdges)
{
  using namespace DataStructure::Graph;
  
  Hypergraph hg;
  
  NodeCoreTestSPtr nodeA = std::make_shared<NodeCoreTest>("A");
  NodeCoreTestSPtr nodeB = std::make_shared<NodeCoreTest>("B");
  NodeCoreTestSPtr nodeC = std::make_shared<NodeCoreTest>("C");
  NodeCoreTestSPtr nodeD = std::make_shared<NodeCoreTest>("D");
  NodeCoreTestSPtr nodeE = std::make_shared<NodeCoreTest>("E");
  
  // Add an edge {A, B}
  auto idEdge1 = hg.addEdge({nodeA, nodeB});
  
  // Add an edge {B, C}
  auto idEdge2 = hg.addEdge({nodeB, nodeC});
  
  // Add an edge {C, D}
  auto idEdge3 = hg.addEdge({nodeC, nodeD});
  
  // NodeA must be connected to nodeD
  ASSERT_TRUE(hg.connectedNodes(nodeA, nodeD));
  ASSERT_TRUE(hg.connectedNodes((idEdge1.second)[nodeA->getId()], (idEdge3.second)[nodeD->getId()]));
  
  // Add an edge {D, A} -> creates a loop
  auto idEdge4 = hg.addEdge({nodeD, nodeA});
  
  // NodeA must be connected to nodeD
  ASSERT_TRUE(hg.connectedNodes(nodeA, nodeD));
  ASSERT_TRUE(hg.connectedNodes((idEdge1.second)[nodeA->getId()], (idEdge3.second)[nodeD->getId()]));
}//addConnectingEdges
