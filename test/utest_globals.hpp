#pragma once

#include <boost/assert.hpp>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "utest_inc.hpp"

#define OPTILAB_TEST TEST
#define OPTILAB_TEST_F TEST_F
