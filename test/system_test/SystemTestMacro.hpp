#pragma once

#include "utest_globals.hpp"

// Assert that the output from a run contains the assignment "y: z"
#define ASSERT_VAR_ASSIGN(x, y) ASSERT_THAT(getOutputRun(), HasSubstr(std::string(x) + ":\t" + y))

// Assert that the output from a run has the solution "x"
#define ASSERT_SOLUTION(x) ASSERT_THAT(getOutputRun(), HasSubstr(x))

// Assert that the output from a run has no solutions
#define ASSERT_NO_SOLUTIONS() ASSERT_THAT(getOutputRun(), HasSubstr("No solutions"))

// Assert that the output from a run contains the given string
#define ASSERT_OUTPUT_CONTAINS(x) ASSERT_THAT(getOutputRun(), HasSubstr(x))

// Assert that the output from a run does not contain the given string
#define ASSERT_OUTPUT_NOT_CONTAINS(x) ASSERT_THAT(getOutputRun(), Not(HasSubstr(x)))

using ::testing::HasSubstr;
