#pragma once

#include "SystemTestMacro.hpp"

#include <string>

class SystemTest : public ::testing::Test {
public:
  SystemTest() = default;
  ~SystemTest() = default;
  
  /// Runs OptiLab on the given input file model.
  /// Returns the output stream as a standard string
  /// @note the path to the file must be relative to the test folder
  /// "test/system_test"
  /// In other words, if both the test model
  ///   "model.opt"
  /// and the test file are located at
  ///   "test/system_test/constraint/global/alldiff/"
  /// the input file name should be
  ///   "constraint/global/alldiff/model.opt"
  void runSystem(const std::string& aInFile);

  inline const std::string& getOutputRun() const { return pOutputRun; }
  
protected:
  void SetUp() override {
    pOutputRun.clear();
  }
  
private:
  /// String containing the output from a system run
  std::string pOutputRun;
};//SystemTest
