
#include "SystemTestInc.hpp"

OPTILAB_TEST_F(SystemTest, geneticOptimizationMin)
{
  // Tests genetic search min optimization problems
  runSystem("/search/incomplete_search/genetic/genetic_optimization_min_mdl.opt");
  
  // Assert assignment of values
  // @note the random seed is 0, therefore the assignment should be always the same
  ASSERT_VAR_ASSIGN("fruit", "1");
  ASSERT_VAR_ASSIGN("fries", "3");
  ASSERT_VAR_ASSIGN("salad", "3");
  ASSERT_VAR_ASSIGN("wings", "1");
  ASSERT_VAR_ASSIGN("sticks", "0");
  ASSERT_VAR_ASSIGN("sampler", "2");
  ASSERT_VAR_ASSIGN("obj", "10");
}//geneticOptimizationMin
