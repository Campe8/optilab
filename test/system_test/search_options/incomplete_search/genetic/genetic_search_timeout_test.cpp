
#include "SystemTestInc.hpp"

#include "Timer.hpp"

OPTILAB_TEST_F(SystemTest, geneticOptimizationTimeoutZeroSec)
{
  // Tests genetic search timeout of 0 seconds on CBLS
  Base::Tools::Timer timer;
  runSystem("/search_options/incomplete_search/genetic/genetic_timeout_0_sec_mdl.opt");
  auto time = timer.getWallClockTimeSec();
  
  EXPECT_LT(time, 2);
  
  // There should be a solution even with 0 seconds (initial assignment)
  ASSERT_OUTPUT_CONTAINS("Solution: 1");
  
  // @note the following may be too strict and system dependent
  ASSERT_VAR_ASSIGN("obj", "262562");
}//geneticOptimizationTimeoutZeroSec

OPTILAB_TEST_F(SystemTest, geneticOptimizationTimeoutOneSec)
{
  // Tests genetic search timeout of 1 second on CBLS
  Base::Tools::Timer timer;
  runSystem("/search_options/incomplete_search/genetic/genetic_timeout_1_sec_mdl.opt");
  auto time = timer.getWallClockTimeSec();
  
  EXPECT_LT(time, 3);
  
  // There should be a solution even with 1 seconds (initial assignment).
  // @note the solution should be bettern than the one found with zero seconds timeout
  ASSERT_OUTPUT_CONTAINS("Solution: 1");
  
  // @note the following may be too strict and system dependent
  //ASSERT_VAR_ASSIGN("obj", "12");
  //ASSERT_VAR_ASSIGN("obj", "9");
}//geneticOptimizationTimeoutOneSec

OPTILAB_TEST_F(SystemTest, geneticOptimizationTimeoutTwoSec)
{
  // Tests genetic search timeout of 2 seconds on CBLS
  Base::Tools::Timer timer;
  runSystem("/search_options/incomplete_search/genetic/genetic_timeout_2_sec_mdl.opt");
  auto time = timer.getWallClockTimeSec();
  
  EXPECT_LT(time, 4);
  
  // There should be a solution even with 2 seconds (initial assignment).
  // @note the solution should be bettern than the one found with one second timeout
  ASSERT_OUTPUT_CONTAINS("Solution: 1");
  
  // @note the following may be too strict and system dependent
  ASSERT_VAR_ASSIGN("obj", "10");
}//geneticOptimizationTimeoutTwoSec
