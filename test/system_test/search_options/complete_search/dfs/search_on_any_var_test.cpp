
#include "SystemTestInc.hpp"

OPTILAB_TEST_F(SystemTest, searchOnAnyVar)
{
  // Tests search options on "any" variable with syntax [_]
  runSystem("/search_options/complete_search/dfs/dfs_on_any_var_mdl.opt");
  ASSERT_OUTPUT_CONTAINS("Solution: 1");
  ASSERT_VAR_ASSIGN("y", "2");
  ASSERT_VAR_ASSIGN("x", "1");
}//searchOnAnyVar
