
#include "SystemTestInc.hpp"

OPTILAB_TEST_F(SystemTest, dfsDefaultOptions)
{
  // Tests default dfs options
  runSystem("/search_options/complete_search/dfs/dfs_default_mdl.opt");
  ASSERT_OUTPUT_CONTAINS("scope: [x, y, w, z]");
  ASSERT_OUTPUT_CONTAINS("searchOptions:");
  ASSERT_OUTPUT_CONTAINS("strategy: dfs");
  ASSERT_OUTPUT_CONTAINS("valueSelection: indomain_min");
  ASSERT_OUTPUT_CONTAINS("variableSelection: input_order");
  
  ASSERT_OUTPUT_NOT_CONTAINS("Metrics");
  
  ASSERT_VAR_ASSIGN("x", "1");
  ASSERT_VAR_ASSIGN("y", "2");
  ASSERT_VAR_ASSIGN("w", "3");
  ASSERT_VAR_ASSIGN("z", "4");
}//dfsDefaultOptions

OPTILAB_TEST_F(SystemTest, dfsModifiedSearchOptions)
{
  // Tests default dfs options
  runSystem("/search_options/complete_search/dfs/dfs_value_selection_mdl.opt");
  
  ASSERT_OUTPUT_CONTAINS("sopt:");
  ASSERT_OUTPUT_CONTAINS("strategy: dfs");
  ASSERT_OUTPUT_CONTAINS("valueSelection: indomain_min");
  ASSERT_OUTPUT_CONTAINS("valueSelection: indomain_max");
  ASSERT_OUTPUT_CONTAINS("variableSelection: input_order");
  
  ASSERT_OUTPUT_NOT_CONTAINS("Metrics");
  
  ASSERT_VAR_ASSIGN("x", "4");
  ASSERT_VAR_ASSIGN("y", "3");
  ASSERT_VAR_ASSIGN("w", "2");
  ASSERT_VAR_ASSIGN("z", "1");
}//dfsModifiedSearchOptions
