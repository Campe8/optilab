
#include "SystemTestInc.hpp"

#include "Timer.hpp"

OPTILAB_TEST_F(SystemTest, dfsTimeoutZeroSec)
{
  // Tests timeout of zero seconds on dfs search
  Base::Tools::Timer timer;
  runSystem("/search_options/complete_search/dfs/dfs_timeout_0_sec_mdl.opt");
  auto time = timer.getWallClockTimeSec();
  
  EXPECT_LT(time, 2);
  ASSERT_OUTPUT_CONTAINS("No solutions");
}//dfsTimeoutZeroSec

OPTILAB_TEST_F(SystemTest, dfsTimeoutOneSec)
{
  // Tests timeout of one second on dfs search
  Base::Tools::Timer timer;
  runSystem("/search_options/complete_search/dfs/dfs_timeout_1_sec_mdl.opt");
  auto time = timer.getWallClockTimeSec();

  EXPECT_LT(time, 3);
  ASSERT_OUTPUT_CONTAINS("No solutions");
}//dfsTimeoutOneSec
