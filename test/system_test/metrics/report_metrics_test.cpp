
#include "SystemTestInc.hpp"

OPTILAB_TEST_F(SystemTest, defaultMetrics)
{
  // Tests default metrics
  runSystem("/metrics/metrics_mdl.opt");
  ASSERT_OUTPUT_CONTAINS("Metrics");
  ASSERT_OUTPUT_CONTAINS("Time: \n");
  ASSERT_OUTPUT_CONTAINS("  searchTime:");
  ASSERT_OUTPUT_CONTAINS("  wallclockTime:");
  ASSERT_OUTPUT_CONTAINS("  compilerTime:");
}//defaultMetrics

OPTILAB_TEST_F(SystemTest, dfsMetrics)
{
  // Tests dfs metrics
  runSystem("/metrics/dfs_metrics_mdl.opt");
  
  ASSERT_OUTPUT_CONTAINS("Metrics");
  ASSERT_OUTPUT_CONTAINS("Time: \n");
  ASSERT_OUTPUT_CONTAINS("  searchTime:");
  ASSERT_OUTPUT_CONTAINS("  wallclockTime:");
  ASSERT_OUTPUT_CONTAINS("  compilerTime:");
  
  ASSERT_OUTPUT_CONTAINS("b: \n");
  ASSERT_OUTPUT_CONTAINS("  numNodes: 3");
  ASSERT_OUTPUT_CONTAINS("  maxDepth: 1");
  ASSERT_OUTPUT_CONTAINS("  numFailures: 0");
}//dfsMetrics

OPTILAB_TEST_F(SystemTest, geneticMetrics)
{
  // Tests dfs metrics
  runSystem("/metrics/genetic_metrics_mdl.opt");
  ASSERT_OUTPUT_CONTAINS("Metrics");
  ASSERT_OUTPUT_CONTAINS("Time: \n");
  ASSERT_OUTPUT_CONTAINS("  searchTime:");
  ASSERT_OUTPUT_CONTAINS("  wallclockTime:");
  ASSERT_OUTPUT_CONTAINS("  compilerTime:");
  
  ASSERT_OUTPUT_CONTAINS("b: \n");
  ASSERT_OUTPUT_CONTAINS("  numGenerations: 0");
  ASSERT_OUTPUT_CONTAINS("  numBestUpdates: 0");
}//geneticMetrics
