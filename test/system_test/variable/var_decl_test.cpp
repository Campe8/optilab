
#include "SystemTestInc.hpp"

OPTILAB_TEST_F(SystemTest, varDeclScalarVariables)
{
  // Tests declaration of scalar finite domain variables
  runSystem("/variable/var_scalar_decl_mdl.opt");
  ASSERT_OUTPUT_CONTAINS("a: 1\n");
  
  ASSERT_OUTPUT_CONTAINS("b:");
  ASSERT_OUTPUT_CONTAINS("domain: [1]");
  
  ASSERT_OUTPUT_CONTAINS("c:");
  ASSERT_OUTPUT_CONTAINS("domain: [1, 2]");
  
  ASSERT_OUTPUT_CONTAINS("d:");
  ASSERT_OUTPUT_CONTAINS("domain: {1, 5, 7}");
}//varDeclScalarVariables

OPTILAB_TEST_F(SystemTest, varDeclMatrixVariables)
{
  // Tests declaration of matrix finite domain variables
  runSystem("/variable/var_matrix_decl_mdl.opt");
  
  ASSERT_OUTPUT_CONTAINS("x:");
  ASSERT_OUTPUT_CONTAINS("domain: [[1, 2], [1, 2], [1, 2]]");
  ASSERT_OUTPUT_CONTAINS("domainDimensions: [3]");
  
  ASSERT_OUTPUT_CONTAINS("y:");
  ASSERT_OUTPUT_CONTAINS("domain:");
  ASSERT_OUTPUT_CONTAINS("[");
  ASSERT_OUTPUT_CONTAINS("[[1, 2], [1, 2], [1, 2], [1, 2]]");
  ASSERT_OUTPUT_CONTAINS("]");
  ASSERT_OUTPUT_CONTAINS("domainDimensions: [3, 4]");
}//varDeclMatrixVariables
