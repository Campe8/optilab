
#include "SystemTestInc.hpp"

OPTILAB_TEST_F(SystemTest, alldiffSolution)
{
  // Tests propagation of alldiff constraint
  runSystem("/constraint/global/alldiff/alldiff_mdl.opt");
  ASSERT_VAR_ASSIGN("v1", "1");
  ASSERT_VAR_ASSIGN("v2", "2");
  ASSERT_VAR_ASSIGN("v3", "3");
  ASSERT_VAR_ASSIGN("v4", "4");
  ASSERT_VAR_ASSIGN("v5", "5");
}//alldiffSolution

OPTILAB_TEST_F(SystemTest, alldiffNoSolution)
{
  // Tests propagation of alldiff constraint
  // on a model without solutions
  runSystem("/constraint/global/alldiff/alldiff_neg_mdl.opt");
  ASSERT_NO_SOLUTIONS();
}//alldiffNoSolution

OPTILAB_TEST_F(SystemTest, alldiffStandardNQueens)
{
  // Tests propagation of alldiff constraint
  // on a model with other constraints.
  // The model is the 4-queens problem
  runSystem("/constraint/global/alldiff/4queens_mdl.opt");
  ASSERT_SOLUTION("[2, 4, 1, 3]");
}//alldiffStandardNQueens

OPTILAB_TEST_F(SystemTest, alldiffQueensFailing)
{
  // Tests propagation of alldiff constraint
  // on a model with other constraints and no solutions.
  // This triggers a problem fixed with commit a569690
  // and domains with holes
  runSystem("/constraint/global/alldiff/18queens_failing_mdl.opt");
  ASSERT_NO_SOLUTIONS();
}//alldiffQueensFailing
