
#include "SystemTestInc.hpp"

OPTILAB_TEST_F(SystemTest, ForStatement)
{
  // Test for-loop declarations w.r.t. the iterator range operator or list
  runSystem("/interpreter/for_statement/for_declaration_mdl.opt");
  ASSERT_OUTPUT_CONTAINS("_ctx: 1");
  ASSERT_OUTPUT_CONTAINS("_ctx: 2");
  ASSERT_OUTPUT_CONTAINS("_ctx: 3");
  ASSERT_OUTPUT_CONTAINS("_ctx: 4");
  ASSERT_OUTPUT_CONTAINS("_ctx: 5");
  ASSERT_OUTPUT_CONTAINS("_ctx: 6");
}//ForStatement
