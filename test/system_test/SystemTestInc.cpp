#include "SystemTestInc.hpp"

#include "MVCModel.hpp"
#include "MVCViewCLI.hpp"
#include "MVCController.hpp"

#include <boost/filesystem.hpp>

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>

// Returns global path to OptiLab executable folder
static boost::filesystem::path getOptiLabExePath()
{
  return boost::filesystem::current_path();
}//getOptiLabRootPath

// Returns global path to OptiLab folder
static boost::filesystem::path getOptiLabRootPath()
{
  using Path = boost::filesystem::path;
  Path currDirr(getOptiLabExePath());
  
  auto pathStr = currDirr.generic_string();
  auto itp = pathStr.find("OptiLab");
  
  if(itp==-1){
    itp = pathStr.find("optilab");
  }

  return Path(pathStr.substr(0, itp+8));
}//getOptiLabRootPath

/// Returns the relative path of the file's location
static std::vector<std::string> getFileRelativePath()
{
  static std::vector<std::string> rpath{"test", "system_test"};
  return rpath;
}//getFileRelativePath

/// Returns the global path to the input file for OptiLab
static std::string getInputFilePath(const std::string& aInFile)
{
  auto rootPath = getOptiLabRootPath();
  auto relPath  = getFileRelativePath();
  
  auto inFile = aInFile;
#ifdef BOOST_POSIX_API   // workaround for user-input files
  std::replace(inFile.begin(), inFile.end(), '\\', '/');
#endif
  
  rootPath = rootPath / relPath[0] / relPath[1] / inFile;
  return std::string(rootPath.string());
}//getInputFilePath

/// Returns the global path to a random output file
static std::string getOutputFilePath()
{
  auto rootPath = testing::internal::FilePath(getOptiLabExePath().generic_string());
  auto testFileName = testing::internal::FilePath("outTestFile");
  std::string extension{"txt"};
  
  auto uniqueFileName = testing::internal::FilePath::GenerateUniqueFileName(rootPath, testFileName, extension.c_str());
  return uniqueFileName.c_str();
}//getOutputFilePath

/// Returns the string containing the command to run
static std::string getCommandString(const std::string& aInFile, const std::string& aOutFile)
{
  std::string cmd{"./OptiLab -f "};

  #ifdef WIN32
    cmd = ".\\OptiLab.exe -f ";
  #endif

  cmd += aInFile;
  cmd += " > ";
  cmd += aOutFile;
  return cmd;
}//getCommandString

static std::string parseOutputFile(const std::string& aOutFile)
{
  std::string output;
  
  auto outStream = std::fstream(aOutFile);
  assert(outStream.is_open());
  
  std::string line;
  while (std::getline(outStream, line))
  {
    output += line + '\n';
  }
  // Remove out file here
  outStream.close();
  
  return output;
}//parseOutputFile

static void deleteOutputFile(const std::string& aOutFile)
{
  std::remove(aOutFile.c_str());
}//deleteOutputFile

void SystemTest::runSystem(const std::string& aInFile)
{
  // Get input and output file paths
  auto modelFile = getInputFilePath(aInFile);
  auto outputFile = getOutputFilePath();
  
  // Get string command to to run
  auto cmdString = getCommandString(modelFile, outputFile);
  
  // std::cout << cmdString << std::endl;

  // Run the executable
  std::system(cmdString.c_str());
  
  // Read the output file and return its content as string
  pOutputRun = parseOutputFile(outputFile);
  
  // Remove the output file
  deleteOutputFile(outputFile);
}//runSystem
