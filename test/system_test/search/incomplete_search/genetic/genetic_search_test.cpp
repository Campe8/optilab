
#include "SystemTestInc.hpp"

OPTILAB_TEST_F(SystemTest, geneticSearchSimple)
{
  // Tests genetic search options construction and run.
  runSystem("/search/incomplete_search/genetic/genetic_search_simple_mdl.opt");
  
  ASSERT_OUTPUT_CONTAINS("sopt:");
  ASSERT_OUTPUT_CONTAINS("strategy: genetic");
  ASSERT_OUTPUT_CONTAINS("seed: 0");
  ASSERT_OUTPUT_CONTAINS("neighborhoodSize: -1");
  ASSERT_OUTPUT_CONTAINS("numNeighborhoods: 1");
  ASSERT_OUTPUT_CONTAINS("warmStart: true");
  ASSERT_OUTPUT_CONTAINS("numGenerations: 500");
  ASSERT_OUTPUT_CONTAINS("mutationChance: 70");
  ASSERT_OUTPUT_CONTAINS("mutationSize: 1");
  ASSERT_OUTPUT_CONTAINS("populationSize: 100");
  
  ASSERT_OUTPUT_CONTAINS("scope: [x, y, w, z]");
  ASSERT_OUTPUT_CONTAINS("searchOptions:");
  ASSERT_OUTPUT_CONTAINS("sopt");
  
  ASSERT_OUTPUT_NOT_CONTAINS("Metrics");
  
  // Assert assignment.
  // @note the random seed is 0, therefore the assignment should be always the same
  ASSERT_VAR_ASSIGN("x", "2");
  ASSERT_VAR_ASSIGN("y", "4");
  ASSERT_VAR_ASSIGN("w", "3");
  ASSERT_VAR_ASSIGN("z", "1");
}//geneticSearchSimple

OPTILAB_TEST_F(SystemTest, geneticSearchSetNSize)
{
  // Tests genetic search options construction and run.
  // Set the neighborhood size to the full neighborhood size
  runSystem("/search_options/incomplete_search/genetic/genetic_search_set_nsize_mdl.opt");
  
  ASSERT_OUTPUT_CONTAINS("sopt:");
  ASSERT_OUTPUT_CONTAINS("strategy: genetic");
  ASSERT_OUTPUT_CONTAINS("seed: 0");
  ASSERT_OUTPUT_CONTAINS("neighborhoodSize: 4");
  ASSERT_OUTPUT_CONTAINS("numNeighborhoods: 1");
  ASSERT_OUTPUT_CONTAINS("warmStart: true");
  ASSERT_OUTPUT_CONTAINS("numGenerations: 500");
  ASSERT_OUTPUT_CONTAINS("mutationChance: 70");
  ASSERT_OUTPUT_CONTAINS("mutationSize: 1");
  ASSERT_OUTPUT_CONTAINS("populationSize: 100");

  ASSERT_OUTPUT_CONTAINS("scope: [x, y, w, z]");
  ASSERT_OUTPUT_CONTAINS("searchOptions:");
  ASSERT_OUTPUT_CONTAINS("sopt");
  
  ASSERT_OUTPUT_NOT_CONTAINS("Metrics");
  
  // Assert assignment.
  // @note the random seed is 0, therefore the assignment should be always the same
  ASSERT_VAR_ASSIGN("x", "2");
  ASSERT_VAR_ASSIGN("y", "4");
  ASSERT_VAR_ASSIGN("w", "3");
  ASSERT_VAR_ASSIGN("z", "1");
}//geneticSearchSetNSize

OPTILAB_TEST_F(SystemTest, geneticSearchMultipleSolutions)
{
  // Tests genetic search options construction and run with multiple solutions
  runSystem("/search_options/incomplete_search/genetic/genetic_search_multiple_solutions_mdl.opt");
  ASSERT_OUTPUT_CONTAINS("scope: [x, y, w, z]");
  
  // Assert assignment.
  // @note the random seed is 0, therefore the assignment should be always the same
  ASSERT_VAR_ASSIGN("x", "1");
  ASSERT_VAR_ASSIGN("y", "3");
  ASSERT_VAR_ASSIGN("w", "4");
  ASSERT_VAR_ASSIGN("z", "2");
  
  ASSERT_VAR_ASSIGN("x", "4");
  ASSERT_VAR_ASSIGN("y", "2");
  ASSERT_VAR_ASSIGN("w", "3");
  ASSERT_VAR_ASSIGN("z", "1");
}//geneticSearchMultipleSolutions

OPTILAB_TEST_F(SystemTest, geneticSearchMultipleSolutions2)
{
  // Tests genetic search options construction and run with multiple solutions.
  // @note this model uses "getSearchOptions("genetic")" instead of "getCLBSOptions("genetic")"
  runSystem("/search_options/incomplete_search/genetic/genetic_search_multiple_solutions_2_mdl.opt");
  ASSERT_OUTPUT_CONTAINS("scope: [x, y, w, z]");
  
  // Assert assignment.
  // @note the random seed is 0, therefore the assignment should be always the same
  ASSERT_VAR_ASSIGN("x", "1");
  ASSERT_VAR_ASSIGN("y", "3");
  ASSERT_VAR_ASSIGN("w", "4");
  ASSERT_VAR_ASSIGN("z", "2");
  
  ASSERT_VAR_ASSIGN("x", "4");
  ASSERT_VAR_ASSIGN("y", "2");
  ASSERT_VAR_ASSIGN("w", "3");
  ASSERT_VAR_ASSIGN("z", "1");
}//geneticSearchMultipleSolutions2
