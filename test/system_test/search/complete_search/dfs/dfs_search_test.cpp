
#include "SystemTestInc.hpp"

OPTILAB_TEST_F(SystemTest, dfsOptimizationMin)
{
  // Tests min optimization problem with dfs strategy
  runSystem("/search/complete_search/dfs/dfs_optimization_min_mdl.opt");
  ASSERT_OUTPUT_CONTAINS("Solution: 1");
  ASSERT_VAR_ASSIGN("fruit", "1");
  ASSERT_VAR_ASSIGN("fries", "0");
  ASSERT_VAR_ASSIGN("salad", "0");
  ASSERT_VAR_ASSIGN("wings", "2");
  ASSERT_VAR_ASSIGN("sticks", "0");
  ASSERT_VAR_ASSIGN("sampler", "1");
  ASSERT_VAR_ASSIGN("obj", "4");
}//dfsOptimizationMin
