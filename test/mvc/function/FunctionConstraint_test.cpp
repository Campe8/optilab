
#include "MVCTestInc.hpp"

#include "FunctionConstraint.hpp"
#include "ObjectHelper.hpp"
#include "ObjectTools.hpp"
#include "OPCode.hpp"

namespace otools = Interpreter::ObjectTools;

OPTILAB_TEST(FunctionConstraint, nameAndOutput)
{
  // Test name and output of FunctionVariable
  using namespace MVC;
  
  FunctionConstraint fcnCon;
  EXPECT_EQ(std::string("Constraint"), fcnCon.name());
  EXPECT_TRUE(fcnCon.hasOutput());
}//nameAndOutput

OPTILAB_TEST(FunctionConstraint, callback)
{
  // Test the callback function
  using namespace MVC;
  
  FunctionConstraint fcnCon;
  Interpreter::DataObject cname("nq");
  Interpreter::DataObject carg1(0);
  Interpreter::DataObject carg2(1);

  Interpreter::DataObject con;
  ASSERT_NO_THROW(con = fcnCon.callback({cname, carg1, carg2}));
}  // FunctionConstraint
