
#include "MVCTestInc.hpp"

#include <string>

#include "DataObject.hpp"
#include "ObjectHelper.hpp"
#include "FunctionObject.hpp"

namespace otools = Interpreter::ObjectTools;

OPTILAB_TEST(FunctionObject, nameAndOutput)
{
  // Test name and output of getUnusedCtxVarName function
  using namespace MVC;
  
  FunctionObject objFcn;
  EXPECT_EQ(std::string("Object"), objFcn.name());
  EXPECT_TRUE(objFcn.hasOutput());
}//nameAndOutput

OPTILAB_TEST(FunctionObject, callbackFunction)
{
  // Test the Branch callback function
  using namespace MVC;
  
  FunctionObject objFcn;
  
  // Create an object from a primitive type
  const int val = 10;
  Interpreter::DataObject intObj(val);
  Interpreter::DataObject obj;
  ASSERT_NO_THROW(obj = objFcn.callback({intObj}));
  EXPECT_TRUE(Interpreter::isClassObjectType(obj));
  EXPECT_TRUE(otools::isScalarObject(obj.getClassObject().get()));
  
  auto value = otools::getScalarValue(obj.getClassObject().get());
  EXPECT_EQ(Interpreter::DataObject::DataObjectType::DOT_INT, value.dataObjectType());
  EXPECT_EQ(val, value.getDataValue<int>());
  
  // Create an empty object
  ASSERT_NO_THROW(obj = objFcn.callback({}));
  EXPECT_TRUE(Interpreter::isClassObjectType(obj));
}// callbackFunction
