
#include "MVCTestInc.hpp"

#include "FunctionSearch.hpp"
#include "DFSInformation.hpp"
#include "SearchOptionsTools.hpp"

namespace sotools = Interpreter::SearchOptionsTools;

OPTILAB_TEST(FunctionSearch, nameAndOutput)
{
  // Test name and output of branch function
  using namespace MVC;
  
  FunctionSearch fs;
  EXPECT_EQ(std::string("branch"), fs.name());
  EXPECT_TRUE(fs.hasOutput());
  EXPECT_TRUE(fs.requiresFcnObjectAsArgument());
}//nameAndOutput
