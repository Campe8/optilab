
#include "MVCTestInc.hpp"

#include "FunctionBranch.hpp"
#include "ObjectHelper.hpp"
#include "ObjectTools.hpp"
#include "OPCode.hpp"

namespace otools = Interpreter::ObjectTools;

OPTILAB_TEST(FunctionBranch, nameAndOutput)
{
  // Test name and output of Branch function
  using namespace MVC;
  
  FunctionBranch fb;
  EXPECT_EQ(std::string("Branch"), fb.name());
  EXPECT_TRUE(fb.hasOutput());
}//nameAndOutput

OPTILAB_TEST(FunctionBranch, callbackFunction)
{
  // Test the Branch callback function
  using namespace MVC;
  
  // Create a list with "any" object
  auto obj =
  otools::createListObject({Interpreter::DataObject(Interpreter::getAnyObjectData().c_str())});
  std::shared_ptr<Interpreter::BaseObject> listPtr(obj);
  Interpreter::DataObject listObj;
  listObj.setClassObject(listPtr);
  
  FunctionBranch fb;
  Interpreter::DataObject branchObject;
  ASSERT_NO_THROW(branchObject = fb.callback({listObj}));
  
  EXPECT_TRUE(branchObject.isClassObject());
  EXPECT_TRUE(otools::isSearchObject(branchObject.getClassObject().get()));
}// callbackFunction
