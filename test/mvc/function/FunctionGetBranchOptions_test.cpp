
#include "MVCTestInc.hpp"

#include "FunctionGetBranchOptions.hpp"
#include "DFSInformation.hpp"
#include "SearchOptionsTools.hpp"

namespace sotools = Interpreter::SearchOptionsTools;

OPTILAB_TEST(FunctionGetBranchOptions, nameAndOutput)
{
  // Test name and output of FunctionGetBranchOptions
  using namespace MVC;
  
  FunctionGetBranchOptions fbrc;
  ASSERT_EQ(std::string("getSearchOptions"), fbrc.name());
  ASSERT_TRUE(fbrc.hasOutput());
}//defaultObject

OPTILAB_TEST(FunctionGetBranchOptions, callback)
{
  // Test callback method
  using namespace MVC;
  
  FunctionGetBranchOptions fbrc;
  auto dobj = fbrc.callback({Model::DFSInformation::StrategyName.c_str()});
  
  ASSERT_TRUE(dobj.isClassObject());
  
  auto obj = dobj.getClassObject().get();
  ASSERT_TRUE(sotools::isSearchOptionsObject(obj));
  
  auto strName =
  Interpreter::ObjectTools::getObjectPropertyValue<std::string>(obj, sotools::getStrategyPropName());
  ASSERT_EQ(strName, Model::DFSInformation::StrategyName);
}//getSetModel
