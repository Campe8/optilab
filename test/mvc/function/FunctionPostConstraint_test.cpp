
#include "MVCTestInc.hpp"

#include "FunctionPostConstraint.hpp"
#include "DFSInformation.hpp"
#include "SearchOptionsTools.hpp"

namespace sotools = Interpreter::SearchOptionsTools;

OPTILAB_TEST(FunctionPostConstraint, nameAndOutput)
{
  // Test name and output of FunctionGetBranchOptions
  using namespace MVC;
  
  FunctionPostConstraint fpc;
  EXPECT_EQ(std::string("postConstraint"), fpc.name());
  EXPECT_TRUE(fpc.hasOutput());
  EXPECT_TRUE(fpc.requiresFcnObjectAsArgument());
}//nameAndOutput
