
#include "MVCTestInc.hpp"

#include "FunctionVariable.hpp"
#include "ObjectHelper.hpp"
#include "ObjectTools.hpp"
#include "OPCode.hpp"

namespace otools = Interpreter::ObjectTools;

OPTILAB_TEST(FunctionVariable, nameAndOutput)
{
  // Test name and output of FunctionVariable
  using namespace MVC;
  
  FunctionVariable fcnVar;
  EXPECT_EQ(std::string("Variable"), fcnVar.name());
  EXPECT_TRUE(fcnVar.hasOutput());
  EXPECT_FALSE(fcnVar.requiresFcnObjectAsArgument());
}//nameAndOutput

OPTILAB_TEST(FunctionVariable, callbackOnlyDomain)
{
  // Test the callback function with only the domain as input argument
  using namespace MVC;
  
  FunctionVariable fcnVar;
  auto obj = otools::createListObject({Interpreter::DataObject(0), Interpreter::DataObject(1)});
  std::shared_ptr<Interpreter::BaseObject> domainPtr(obj);
  Interpreter::DataObject listObj;
  listObj.setClassObject(domainPtr);
  
  Interpreter::DataObject var;
  ASSERT_NO_THROW(var = fcnVar.callback({listObj}));
  ASSERT_TRUE(Interpreter::isClassObjectType(var));
  
  auto varObj = var.getClassObject().get();
  ASSERT_TRUE(otools::isVariableObject(varObj));
  
  auto domain = otools::getVarDomain(varObj);
  ASSERT_TRUE(otools::isListObject(domain.getClassObject().get()));
  EXPECT_EQ(2, otools::getListSize(domain.getClassObject().get()));
}  // callbackOnlyDomain

OPTILAB_TEST(FunctionVariable, callbackDomainAndMinOptimization)
{
  // Test the callback function with the domain and min optimization as input arguments
  using namespace MVC;
  
  FunctionVariable fcnVar;
  auto obj = otools::createListObject({Interpreter::DataObject(0), Interpreter::DataObject(1)});
  std::shared_ptr<Interpreter::BaseObject> domainPtr(obj);
  Interpreter::DataObject listObj;
  listObj.setClassObject(domainPtr);
  
  // Optimization argument
  Interpreter::DataObject optVal("minimize");
  
  Interpreter::DataObject var;
  ASSERT_NO_THROW(var = fcnVar.callback({listObj, optVal}));
  ASSERT_TRUE(Interpreter::isClassObjectType(var));
  
  auto varObj = var.getClassObject().get();
  ASSERT_TRUE(otools::isVariableObject(varObj));
  
  auto domain = otools::getVarDomain(varObj);
  ASSERT_TRUE(otools::isListObject(domain.getClassObject().get()));
  EXPECT_EQ(2, otools::getListSize(domain.getClassObject().get()));
  EXPECT_EQ(VAR_SPEC_OPT_MIN, otools::getVarSemantic(varObj));
}  // callbackDomainAndMinOptimization

OPTILAB_TEST(FunctionVariable, callbackDomainAndMaxOptimization)
{
  // Test the callback function with the domain and max optimization as input arguments
  using namespace MVC;
  
  FunctionVariable fcnVar;
  auto obj = otools::createListObject({Interpreter::DataObject(0), Interpreter::DataObject(1)});
  std::shared_ptr<Interpreter::BaseObject> domainPtr(obj);
  Interpreter::DataObject listObj;
  listObj.setClassObject(domainPtr);
  
  // Optimization argument
  Interpreter::DataObject optVal("maximize");
  
  Interpreter::DataObject var;
  ASSERT_NO_THROW(var = fcnVar.callback({listObj, optVal}));
  ASSERT_TRUE(Interpreter::isClassObjectType(var));
  
  auto varObj = var.getClassObject().get();
  ASSERT_TRUE(otools::isVariableObject(varObj));
  
  auto domain = otools::getVarDomain(varObj);
  ASSERT_TRUE(otools::isListObject(domain.getClassObject().get()));
  EXPECT_EQ(2, otools::getListSize(domain.getClassObject().get()));
  EXPECT_EQ(VAR_SPEC_OPT_MAX, otools::getVarSemantic(varObj));
}  // callbackDomainAndMaxOptimization

OPTILAB_TEST(FunctionVariable, callbackDomainAndDimensions)
{
  // Test the callback function with the domain and dimensions as input arguments
  using namespace MVC;
  
  FunctionVariable fcnVar;
  auto obj = otools::createListObject({Interpreter::DataObject(0), Interpreter::DataObject(1)});
  std::shared_ptr<Interpreter::BaseObject> domainPtr(obj);
  Interpreter::DataObject listDomainObj;
  listDomainObj.setClassObject(domainPtr);
  
  auto objDim = otools::createListObject({Interpreter::DataObject(1), Interpreter::DataObject(2)});
  std::shared_ptr<Interpreter::BaseObject> dimPtr(objDim);
  Interpreter::DataObject listDimObj;
  listDimObj.setClassObject(dimPtr);
  
  Interpreter::DataObject var;
  ASSERT_NO_THROW(var = fcnVar.callback({listDomainObj, listDimObj}));
  ASSERT_TRUE(Interpreter::isClassObjectType(var));
  
  auto varObj = var.getClassObject().get();
  ASSERT_TRUE(otools::isVariableObject(varObj));
  
  auto domain = otools::getVarDomain(varObj);
  ASSERT_TRUE(otools::isListObject(domain.getClassObject().get()));
  EXPECT_EQ(2, otools::getListSize(domain.getClassObject().get()));
  EXPECT_EQ(2, otools::getVarDomainNumDimensions(varObj));
  
  auto listDims = otools::getVarDomainDimensionsList(varObj);
  ASSERT_EQ(2, otools::getListSize(listDims));
  
  auto dim1 = otools::getListElement(listDims, Interpreter::DataObject(0));
  auto dim2 = otools::getListElement(listDims, Interpreter::DataObject(1));
  EXPECT_EQ(Interpreter::DataObject::DataObjectType::DOT_INT, dim1.dataObjectType());
  EXPECT_EQ(Interpreter::DataObject::DataObjectType::DOT_INT, dim2.dataObjectType());
  EXPECT_EQ(1, dim1.getDataValue<int>());
  EXPECT_EQ(2, dim2.getDataValue<int>());
}  // callbackDomainAndDimensions

OPTILAB_TEST(FunctionVariable, callbackDomainAndDimensionsAndOptimization)
{
  // Test the callback function with the domain, dimensions, and optimization value
  // as input arguments
  using namespace MVC;
  
  FunctionVariable fcnVar;
  auto obj = otools::createListObject({Interpreter::DataObject(0), Interpreter::DataObject(1)});
  std::shared_ptr<Interpreter::BaseObject> domainPtr(obj);
  Interpreter::DataObject listDomainObj;
  listDomainObj.setClassObject(domainPtr);
  
  auto objDim = otools::createListObject({Interpreter::DataObject(1), Interpreter::DataObject(2)});
  std::shared_ptr<Interpreter::BaseObject> dimPtr(objDim);
  Interpreter::DataObject listDimObj;
  listDimObj.setClassObject(dimPtr);
  
  // Optimization argument
  Interpreter::DataObject optVal("minimize");
  
  Interpreter::DataObject var;
  ASSERT_NO_THROW(var = fcnVar.callback({listDomainObj, listDimObj, optVal}));
  ASSERT_TRUE(Interpreter::isClassObjectType(var));
  
  auto varObj = var.getClassObject().get();
  ASSERT_TRUE(otools::isVariableObject(varObj));
  
  auto domain = otools::getVarDomain(varObj);
  ASSERT_TRUE(otools::isListObject(domain.getClassObject().get()));
  EXPECT_EQ(2, otools::getListSize(domain.getClassObject().get()));
  EXPECT_EQ(2, otools::getVarDomainNumDimensions(varObj));
  
  auto listDims = otools::getVarDomainDimensionsList(varObj);
  ASSERT_EQ(2, otools::getListSize(listDims));
  
  auto dim1 = otools::getListElement(listDims, Interpreter::DataObject(0));
  auto dim2 = otools::getListElement(listDims, Interpreter::DataObject(1));
  EXPECT_EQ(Interpreter::DataObject::DataObjectType::DOT_INT, dim1.dataObjectType());
  EXPECT_EQ(Interpreter::DataObject::DataObjectType::DOT_INT, dim2.dataObjectType());
  EXPECT_EQ(1, dim1.getDataValue<int>());
  EXPECT_EQ(2, dim2.getDataValue<int>());
  
  EXPECT_EQ(VAR_SPEC_OPT_MIN, otools::getVarSemantic(varObj));
}  // callbackDomainAndDimensionsAndOptimization
