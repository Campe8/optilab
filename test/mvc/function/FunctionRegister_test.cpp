
#include "MVCTestInc.hpp"

#include "MVCViewCLI.hpp"
#include "FunctionRegister.hpp"

namespace MVC {
  
  class FunctionRegisterTest : public FunctionRegister {
  public:
    void registerFunction(const std::shared_ptr<Function>& aFcn)
    {
      FunctionRegister::registerFunction(aFcn->name(), aFcn->getFcnId(), aFcn);
    }
    
    std::shared_ptr<MVCModel> getModel() const
    {
      return FunctionRegister::getModel();
    }
  };
  
  class FcnPosterMapRegisterTest : public FcnPosterMapRegister {
  public:
    inline FcnPosterMap& getPosterMap()
    {
      return FcnPosterMapRegister::getPosterMap();
    }
  };
  
}// end namespace MVC


OPTILAB_TEST(FunctionRegister, defaultObject)
{
  // Test methods on a new instance of a class
  using namespace MVC;
  
  FunctionRegisterTest fReg;
  ASSERT_FALSE(fReg.getModel());
  
  // By default it registers all the post constraint functions
  ASSERT_GT(fReg.getRegisterSize(), 0);
  ASSERT_FALSE(fReg.getRegisteredFcnNameByIndex(0).empty());
  ASSERT_FALSE(fReg.getFunction("abc"));
  ASSERT_FALSE(fReg.getCallbackFunction("abc"));
}//defaultObject

OPTILAB_TEST(FunctionRegister, getSetModel)
{
  // Test set/get model
  using namespace MVC;
  
  FunctionRegisterTest fReg;
  std::shared_ptr<MVCModel> m = std::make_shared<MVCModel>();
  fReg.setModel(m);

  ASSERT_TRUE(fReg.getModel() == m);
}//getSetModel

OPTILAB_TEST(FunctionRegister, registerFunction)
{
  // Test registration of a function
  using namespace MVC;
  
  FunctionRegisterTest fReg;
  std::shared_ptr<FunctionTest> f = std::make_shared<FunctionTest>();
  auto regSize = fReg.getRegisterSize();
  fReg.registerFunction(f);
  
  // Now the callback should be registered
  std::string fcnName{"test"};
  ASSERT_EQ(fReg.getRegisterSize(), regSize + 1);
  ASSERT_TRUE(fReg.getCallbackFunction(fcnName));
  
  // Check model set by function register
  ASSERT_FALSE(f->getModel());
  
  std::shared_ptr<MVCModel> mvcModel = std::make_shared<MVCModel>();
  std::shared_ptr<MVCViewCLI> mvcView = std::make_shared<MVCViewCLI>();
  std::shared_ptr<MVCController> mvcController = std::make_shared<MVCController>(mvcView, mvcModel);
  
  fReg.setView(mvcView);
  fReg.setModel(mvcModel);
  fReg.setController(mvcController.get());
  auto f2 = fReg.getFunction("test");
  ASSERT_EQ(f.get(), f2.get());
  
  // Check that model, view, and controller are set
  ASSERT_TRUE(f->getModel() == mvcModel);
  ASSERT_TRUE(f->getView() == mvcView);
  ASSERT_TRUE(f->getController() == mvcController.get());
}//registerFunction

OPTILAB_TEST(FunctionRegister, callbackRegistration)
{
  using namespace MVC;
  
  FunctionRegisterTest fReg;
  
  std::shared_ptr<FunctionTest> f = std::make_shared<FunctionTest>();
  fReg.registerFunction(f);
  
  // Before invoking the callback, the default value is 0
  ASSERT_EQ(f->getValue(), 0);
  
  // Invoking the callback should change the internal value of f
  auto callback = fReg.getCallbackFunction("test");
  (*callback)({""});
  ASSERT_EQ(f->getValue(), 100);
}//registerFunction

OPTILAB_TEST(FunctionRegister, defaultFunctionRegistration)
{
  // Test that all defined functions are actually registered
  // by default when creating a new function register
  using namespace MVC;
  FunctionRegisterTest fcnReg;
  
  for(std::size_t idx{0}; idx < fcnReg.getRegisterSize(); ++idx)
  {
    auto fcnName = fcnReg.getRegisteredFcnNameByIndex(idx);
    ASSERT_TRUE(fcnReg.getCallbackFunction(fcnName));
  }
}//defaultFunctionRegistration
