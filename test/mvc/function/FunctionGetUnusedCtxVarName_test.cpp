
#include "MVCTestInc.hpp"

#include <string>

#include "DataObject.hpp"
#include "FunctionGetUnusedCtxVarName.hpp"
#include "MVCModelContext.hpp"

OPTILAB_TEST(FunctionGetUnusedCtxVarName, nameAndOutput)
{
  // Test name and output of getUnusedCtxVarName function
  using namespace MVC;
  
  FunctionGetUnusedCtxVarName ctxVarNameFcn;
  EXPECT_EQ(std::string("getUnusedCtxVarName"), ctxVarNameFcn.name());
  EXPECT_TRUE(ctxVarNameFcn.hasOutput());
}//nameAndOutput

OPTILAB_TEST(FunctionGetUnusedCtxVarName, DISABLED_callbackFunction)
{
  // Test the Branch callback function
  using namespace MVC;
  
  FunctionGetUnusedCtxVarName ctxVarNameFcn;
  Interpreter::DataObject nameObj;
  
  ASSERT_NO_THROW(nameObj = ctxVarNameFcn.callback({}));
  
  EXPECT_TRUE(Interpreter::isPrimitiveType(nameObj));
  EXPECT_EQ(Interpreter::DataObject::DataObjectType::DOT_STRING, nameObj.dataObjectType());
  
  auto name = nameObj.getDataValue<std::string>();
  std::string refName = MVCModelContext::MVCModelContextBaseObjectValue + std::to_string(1);
  EXPECT_EQ(refName, name);
}// callbackFunction
