
#include "MVCTestInc.hpp"

#include "FunctionGetCBLSOptions.hpp"
#include "GeneticInformation.hpp"
#include "SearchOptionsTools.hpp"

namespace sotools = Interpreter::SearchOptionsTools;

OPTILAB_TEST(FunctionGetCBLSOptions, nameAndOutput)
{
  // Test name and output of FunctionGetBranchOptions
  using namespace MVC;
  
  FunctionGetCBLSOptions fcbls;
  ASSERT_EQ(std::string("getCBLSOptions"), fcbls.name());
  ASSERT_TRUE(fcbls.hasOutput());
}//defaultObject

OPTILAB_TEST(FunctionGetCBLSOptions, callback)
{
  // Test callback method
  using namespace MVC;
  
  FunctionGetCBLSOptions fcbls;
  auto dobj = fcbls.callback({Model::GeneticInformation::StrategyName.c_str()});
  
  ASSERT_TRUE(dobj.isClassObject());
  
  auto obj = dobj.getClassObject().get();
  ASSERT_TRUE(sotools::isSearchOptionsObject(obj));
  
  auto strName =
  Interpreter::ObjectTools::getObjectPropertyValue<std::string>(obj, sotools::getStrategyPropName());
  ASSERT_EQ(strName, Model::GeneticInformation::StrategyName);
}//getSetModel
