#pragma once

#include "utest_globals.hpp"

#include "MVCView.hpp"
#include "MVCModel.hpp"
#include "MVCController.hpp"

#include "Function.hpp"

namespace MVC {
  
  class MVCModelTest : public MVCModel {
  public:
  };
  
  class MVCViewTest : public MVCView {
  public:
    MVCViewTest();
    
    // Expose protected methods for unit test
    void sendStmtToController(const std::string& aString, const std::string& aSrcStmt);
    
    void exe() override;
    
    inline const std::vector<std::string>& getOutBuffer() const
    {
      return MVCView::getOutBuffer();
    }
  };
  
  class MVCControllerTest : public MVCController {
  public:
    MVCControllerTest(const std::shared_ptr<MVCView>& aView, const std::shared_ptr<MVCModel>& aModel);
    
    // Expose protected methods for unit test
    void update(const std::shared_ptr<MVCEvent>& aEvent) override;
    
    inline std::shared_ptr<MVCEvent> getEvent() const
    {
      return pEvent;
    }
    
  private:
    std::shared_ptr<MVCEvent> pEvent;
  };
  
  class FunctionTest : public Function {
  public:
    FunctionTest();
    
    Interpreter::DataObject callback(const std::vector<Interpreter::DataObject>& aArgs) override;
    
    std::string name() const override;
    
    inline int getValue() const { return pValue; }
    
    inline std::shared_ptr<MVCModel> getModel() const { return Function::getModel(); }
    inline std::shared_ptr<MVCView> getView() const { return Function::getView(); }
    inline MVCController* getController() const { return Function::getController(); }
    
  private:
    int pValue;
    
    /// Actual solve function: runs the solver on the current context
    void test();
  };
  
}// end namespace MVC
