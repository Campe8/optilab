#include "MVCTestInc.hpp"
#include "CtxObjectCollector.hpp"

#include <sstream>

OPTILAB_TEST(CtxObjectCollector, getUnusedKeyDefault)
{
  using namespace MVC;
  CtxObjectCollector oc;
  
  // Unused key with empty collector should be 1
  ASSERT_EQ(oc.size(), 0);
  
  auto key = oc.getUnusedKey();
  ASSERT_EQ(1, key);
  
  // Calling twice to unused key without mappings should return 1
  ASSERT_EQ(key, oc.getUnusedKey());
  
  // Clearing the collector shouldn't change the default key
  oc.clear();
  ASSERT_EQ(key, oc.getUnusedKey());
  
  // Lookup should be false
  ASSERT_FALSE(oc.lookup(key));
}//getUnusedKeyDefault

OPTILAB_TEST(CtxObjectCollector, mapCtxKey)
{
  using namespace MVC;
  CtxObjectCollector oc;
  auto key = oc.getUnusedKey();
  oc.mapCtxKey(key, 1, "Test");
  
  ASSERT_TRUE(oc.lookup(key));
  ASSERT_EQ(1, oc.lookupTypeByKey(key));
  ASSERT_EQ("Test", oc.lookupIDByKey(key));
  ASSERT_EQ(key, oc.lookupKeyByID("Test"));
  
  // Lookup for a non valid ID should return inf
  ASSERT_EQ(CtxObjectCollector::contextKeyInf, oc.lookupKeyByID("Test1"));
}//mapCtxKey

OPTILAB_TEST(CtxObjectCollector, unmapCtxKey)
{
  using namespace MVC;
  CtxObjectCollector oc;
  auto key0 = oc.getUnusedKey();
  oc.mapCtxKey(key0, 0, "Test0");
  
  auto key1 = oc.getUnusedKey();
  ASSERT_EQ(2, key1);
  oc.mapCtxKey(key1, 1, "Test1");
  
  auto key2 = oc.getUnusedKey();
  ASSERT_EQ(3, key2);
  oc.mapCtxKey(key2, 2, "Test2");
  
  // Unmap key1
  oc.unmapCtxKey(key1);
  
  // Now unused key should be key1
  key1 = oc.getUnusedKey();
  ASSERT_EQ(2, key1);
  ASSERT_FALSE(oc.lookup(key1));
  ASSERT_EQ(CtxObjectCollector::contextKeyInf, oc.lookupKeyByID("Test1"));
  
  // Re-inserting a new object with unused key should map unused key
  oc.mapCtxKey(key1, 3, "Test3");
  ASSERT_TRUE(oc.lookup(key1));
  ASSERT_EQ(3, oc.lookupTypeByKey(key1));
  ASSERT_EQ("Test3", oc.lookupIDByKey(key1));
}//unmapCtxKey

OPTILAB_TEST(CtxObjectCollector, clearCtxObjKeyMapping)
{
  using namespace MVC;
  CtxObjectCollector oc;
  auto key0 = oc.getUnusedKey();
  oc.mapCtxKey(key0, 0, "Test0");
  
  auto key1 = oc.getUnusedKey();
  oc.mapCtxKey(key1, 1, "Test1");
  
  oc.clear();
  ASSERT_EQ(0, oc.size());
  ASSERT_FALSE(oc.lookup(key0));
  ASSERT_FALSE(oc.lookup(key1));
  ASSERT_EQ(CtxObjectCollector::contextKeyInf, oc.lookupKeyByID("Test0"));
  ASSERT_EQ(CtxObjectCollector::contextKeyInf, oc.lookupKeyByID("Test1"));
}//clearCtxObjKeyMapping

OPTILAB_TEST(CtxObjectCollector, getSetOfMappedKeys)
{
  using namespace MVC;
  CtxObjectCollector oc;
  auto key0 = oc.getUnusedKey();
  oc.mapCtxKey(key0, 0, "Test0");
  
  auto key1 = oc.getUnusedKey();
  oc.mapCtxKey(key1, 1, "Test1");
  
  auto key2 = oc.getUnusedKey();
  oc.mapCtxKey(key2, 2, "Test2");
  
  // Remove one key
  oc.unmapCtxKey(key1);
  
  auto usedKeys = oc.getSetOfMappedKeys();
  ASSERT_EQ(2, oc.size());
  ASSERT_EQ(key0, usedKeys[0]);
  ASSERT_EQ(key2, usedKeys[1]);
}//getSetOfMappedKeys
