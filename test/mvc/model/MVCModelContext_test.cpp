#include "MVCTestInc.hpp"

#include <string>

#include "MVCModelContext.hpp"
#include "CtxObjectCollector.hpp"

OPTILAB_TEST(MVCModelContext, valueContextKeyInf)
{
  // Test that the Inf value of the context key is equal to the value of the context key inf
  // from the object collector
  using namespace MVC;
  EXPECT_EQ(MVCModelContext::contextKeyInf, CtxObjectCollector::contextKeyInf);
  
  // Getting a context key for an object that is not in the context should return inf value
  // for the key
  MVCModelContext mctx;
  EXPECT_EQ(MVCModelContext::contextKeyInf, mctx.getContextKeyFromID("abc"));
}//valueContextKeyInf

OPTILAB_TEST(MVCModelContext, fullyInterpretedObjects)
{
  // Test the fully interpreted objects interface
  using namespace MVC;
  
  MVCModelContext mctx;
  
  // By default the model should be fully interpreted since there are no objects
  EXPECT_TRUE(mctx.isModelContextFullyInterpreted());
  
  // The list of not fully interpreted objects should be empty
  EXPECT_TRUE(mctx.getListOfNotFullyInterpretedContextObjects().empty());
}//fullyInterpretedObjects

OPTILAB_TEST(MVCModelContext, unusedCtxName)
{
  // Test the "getUnusedNameForContextObject(...)" method
  using namespace MVC;
  
  MVCModelContext mctx;
  auto name = mctx.getUnusedNameForContextObject();
  std::string refName = MVCModelContext::MVCModelContextBaseObjectValue + std::to_string(1);
  EXPECT_EQ(refName, name);
}//unusedCtxName

OPTILAB_TEST(MVCModelContext, prettyPrint)
{
  // Test the pretty print - related methods
  using namespace MVC;
  
  MVCModelContext mctx;
  
  // Defalut pretty print level should be none
  EXPECT_EQ(MVCModelContext::PrettyPrintLevel::PP_NONE, mctx.getPrettyPrintLevel());
  
  // Set another pretty print level
  auto oldLevel = mctx.setPrettyPrintLevel(MVCModelContext::PrettyPrintLevel::PP_LOW);
  EXPECT_EQ(MVCModelContext::PrettyPrintLevel::PP_NONE, oldLevel);
  EXPECT_EQ(MVCModelContext::PrettyPrintLevel::PP_LOW, mctx.getPrettyPrintLevel());
  
  // Reset the level and check that is correctly set
  oldLevel = mctx.setPrettyPrintLevel(oldLevel);
  EXPECT_EQ(MVCModelContext::PrettyPrintLevel::PP_LOW, oldLevel);
  EXPECT_EQ(MVCModelContext::PrettyPrintLevel::PP_NONE, mctx.getPrettyPrintLevel());
}//prettyPrint

#ifdef OLD_TEST

OPTILAB_TEST(MVCModelContext, addDeleteStmt)
{
  using namespace MVC;
  MVCModelContext ctx;
  
  // Before adding the stmt, the variable should not be present
  ASSERT_EQ(ctx.getContextKeyFromID("x"), MVCModelContext::contextKeyInf);
  
  // Add a stmt to the context
  auto s = parseStmt("x:1..2;");
  ASSERT_TRUE(ctx.isValidStmt(s));
  
  auto k = ctx.addStmtToContext(s);
  auto k1 = ctx.getContextKeyFromID("x");
  auto kInf = ctx.getContextKeyFromID("y");
  ASSERT_NE(k, MVCModelContext::contextKeyInf);
  ASSERT_EQ(kInf, MVCModelContext::contextKeyInf);
  ASSERT_EQ(k, k1);
  
  // Overriding the same object gives a new key
  auto k2 = ctx.addStmtToContext(s);
  ASSERT_NE(k2, MVCModelContext::contextKeyInf);
  ASSERT_NE(k, k2);
  
  // Delete the statement
  ctx.deleteContextObject(k1);
  ctx.deleteContextObject(k2);
  ASSERT_EQ(ctx.getContextKeyFromID("x"), MVCModelContext::contextKeyInf);
}//addDeleteStmt

OPTILAB_TEST(MVCModelContext, clearContext)
{
  using namespace MVC;
  MVCModelContext ctx;
  
  auto s = parseStmt("x:1..2;");
  ctx.addStmtToContext(s);
  
  // Clear context
  ctx.clearContext();
  ASSERT_EQ(ctx.getContextKeyFromID("x"), MVCModelContext::contextKeyInf);
}//clearContext

OPTILAB_TEST(MVCModelContext, prettyPrint)
{
  using namespace MVC;
  MVCModelContext ctx;
  
  std::stringstream ss;
  ctx.prettyPrint(ss);
  ASSERT_TRUE(ss.str().empty());
  
  auto s = parseStmt("x:1..2;");
  auto k = ctx.addStmtToContext(s);
  
  // Default pretty print level is "low"
  std::stringstream ss1;
  ctx.prettyPrint(k, ss1);
  auto str = ss1.str();
  auto cmp = "Variable:\n"
  "  id: x\n"
  "  domain: [1, 2]";
  ASSERT_EQ_STR(str, cmp);
  
  // Level 1 is default level
  ctx.setPrettyPrintLevel(MVCModelContext::PrettyPrintLevel::PP_LOW);
  std::stringstream ss2;
  ctx.prettyPrint(k, ss2);
  str = ss2.str();
  ASSERT_EQ_STR(str, cmp);
  
  // Level 2 is "high"
  ctx.setPrettyPrintLevel(MVCModelContext::PrettyPrintLevel::PP_HIGH);
  std::stringstream ss3;
  ctx.prettyPrint(k, ss3);
  str = ss3.str();
  auto cmp2 = "Variable:\n"
  "id: x\n"
  "type: integer\n"
  "semantic:\n"
  "  type: decision\n"
  "input_order: 0\n"
  "branching: false\n"
  "output: false\n"
  "domain: [1, 2]";
  ASSERT_EQ_STR(str, cmp2);
  
  // Level 0 is disable pretty print
  ctx.setPrettyPrintLevel(MVCModelContext::PrettyPrintLevel::PP_NONE);
  std::stringstream ss5;
  ctx.prettyPrint(k, ss5);
  str = ss5.str();
  ASSERT_TRUE(str.empty());
}//prettyPrint
#endif

