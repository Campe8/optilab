
#include "MVCTestInc.hpp"

#include "MVCEventFactory.hpp"

OPTILAB_TEST(MVCView, basics)
{
  using namespace MVC;
  
  MVCViewTest view;
  auto subject = MVCView::getSubject(&view);
  ASSERT_TRUE(subject);
  
  ASSERT_EQ(view.getOutBuffer().size(), 0);
}//basics

OPTILAB_TEST(MVCView, updateEvent)
{
  using namespace MVC;
  
  std::shared_ptr<MVCViewTest> cliView = std::make_shared<MVCViewTest>();
  MVCEventFactory ef;
  
  std::string sol1{"text1"};
  std::string sol2{"text2"};
  auto event = ef.mvcEventWriteOutput({sol1, sol2}, MVCEventFactory::EventSrcDst::EVT_C2V);
  
  cliView->update(std::shared_ptr<MVCEvent>(event));
  auto outBuffer = cliView->getOutBuffer();
  ASSERT_EQ(outBuffer.size(), 2);
  ASSERT_EQ(outBuffer[0], sol1);
  ASSERT_EQ(outBuffer[1], sol2);
}//updateEvent

OPTILAB_TEST(MVCView, sendToController)
{
  using namespace MVC;
  
  std::shared_ptr<MVCModel> cliModel = std::make_shared<MVCModel>();
  std::shared_ptr<MVCViewTest> cliView = std::make_shared<MVCViewTest>();
  std::shared_ptr<MVCControllerTest> cliController = std::make_shared<MVCControllerTest>(cliView, cliModel);
  
  // View observes the controller
  cliView->observe(MVC::MVCController::getSubject(cliController.get()));
  
  std::string stmtStr{"stmt"};
  std::string stmtOrigStr{"stmtOrig"};
  cliView->sendStmtToController(stmtStr, stmtOrigStr);
  
  auto controllerEvent = cliController->getEvent();
  ASSERT_TRUE(MVCEventEvalStmt::isa(controllerEvent.get()));
  
  // Received string should be the one sent by the view
  auto evalEvent = MVCEventEvalStmt::cast(controllerEvent.get());
  
  auto stmtEvl = evalEvent->getEvalStmt();
  auto stmtRef = CLI::Utils::getCLIStmtFromString(stmtStr);
  ASSERT_EQ(stmtEvl.size(), stmtRef.size());
  
  for(auto it = stmtEvl.begin(); it != stmtEvl.end(); ++it)
  {
    ASSERT_TRUE(stmtRef.find(it->first) != stmtRef.end());
    ASSERT_EQ(stmtRef[it->first], it->second);
  }
  
  ASSERT_EQ(stmtOrigStr, evalEvent->getSourceStmt());
}//sendToController
