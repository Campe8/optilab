#include "MVCTestInc.hpp"
#include "MVCViewCLI.hpp"

#include "MVCConstants.hpp"
#include "MVCEventFactory.hpp"

#include <sstream>

namespace {
  
  std::string getWelcomeMessage()
  {
    std::string msg;
    msg += "\t\t";
    msg += MVC::Constants::View::WELCOME_OPTILAB;
    msg += "\n\t";
    msg += MVC::Constants::View::WELCOME_COPYRIGHTS;
    msg += "\n\t";
    msg += MVC::Constants::View::WELCOME_RIGHTS;
    return msg;
  }// getWelcomeMessage
  
}  // namespace

OPTILAB_TEST(MVCViewCLI, basics)
{
  using namespace MVC;
  
  MVCViewCLI view;
  auto subject = MVCView::getSubject(&view);
  ASSERT_TRUE(subject);
}//basics

OPTILAB_TEST(MVCViewCLI, exe)
{
  using namespace MVC;
  
  MVCViewCLI view;
  
  std::stringstream inStream;
  std::stringstream outStream;
  
  inStream << "exit";
  view.setInStream(&inStream);
  view.setOutStream(&outStream);
  view.exe();
  auto outString = outStream.str();
  
  // Check welcome message
  auto welcomeMsg = outString.find(getWelcomeMessage());
  ASSERT_TRUE(welcomeMsg != std::string::npos);
}//exe
