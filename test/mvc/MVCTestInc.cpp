#include "MVCTestInc.hpp"

namespace MVC {

  MVCViewTest::MVCViewTest()
  : MVC::MVCView(&std::cin, &std::cout)
  {
  }
  
  void MVCViewTest::sendStmtToController(const std::string& aString, const std::string& aSrcStmt)
  {
    MVCView::sendStmtToController(aString, aSrcStmt);
  }//sendStmtToController
  
  MVCControllerTest::MVCControllerTest(const std::shared_ptr<MVCView>& aView, const std::shared_ptr<MVCModel>& aModel)
  : MVC::MVCController(aView, aModel)
  , pEvent(nullptr)
  {
  }
  
  void MVCViewTest::exe()
  {
  }
  
  void MVCControllerTest::update(const std::shared_ptr<MVCEvent>& aEvent)
  {
    pEvent = aEvent;
  }//update
  
  FunctionTest::FunctionTest()
  : Function(FunctionId::FCN_TEST)
  , pValue(0)
  {
  }
  
  Interpreter::DataObject FunctionTest::callback(const std::vector<Interpreter::DataObject>& aArgs)
  {
    (void)aArgs;
    test();
    
    return Interpreter::DataObject();
  }//callback
  
  void FunctionTest::test()
  {
    pValue = 100;
  }//test
  
  std::string FunctionTest::name() const
  {
    return "test";
  }//name
  
}// end namespace MVC
