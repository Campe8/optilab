
#include "MVCTestInc.hpp"

#include "MVCUtils.hpp"
#include "ObjectTools.hpp"

OPTILAB_TEST(MVCUtils, getIDFromDataObject)
{
  // Test MVC utilities getIDFromDataObject function
  using namespace MVC;
  
  // Void data object should have no identifier
  Interpreter::DataObject voidObj;
  ASSERT_THAT("", ::testing::StrEq(Utils::getIDFromDataObject(voidObj)));
  
  // String primitive data objects should return the string they wrap
  Interpreter::DataObject stringObj("test");
  ASSERT_THAT("test", ::testing::StrEq(Utils::getIDFromDataObject(stringObj)));
  
  // Any non-string primitive data object should not return an identifier
  Interpreter::DataObject intObj(10);
  ASSERT_THAT("", ::testing::StrEq(Utils::getIDFromDataObject(intObj)));
  
  // Class object types should return an identifier only in two cases:
  // 1) there is no property "MVCModelContext::MVCModelContextBaseObjectKey"
  auto objPtr =
  std::shared_ptr<Interpreter::BaseObject>(Interpreter::ObjectTools::createObject("obj"));
  objPtr->addProperty(Interpreter::ObjectTools::getObjectIDPropName(), "test");
  Interpreter::DataObject baseObj;
  baseObj.setClassObject(objPtr);
  ASSERT_THAT("", ::testing::StrEq(Utils::getIDFromDataObject(baseObj)));
  
  // 2) there is a property "MVCModelContext::MVCModelContextBaseObjectKey"
  auto objPtr2 =
  std::shared_ptr<Interpreter::BaseObject>(Interpreter::ObjectTools::createObject("obj"));
  objPtr2->addProperty(MVCModelContext::MVCModelContextBaseObjectKey, "test");
  Interpreter::DataObject baseObj2;
  baseObj2.setClassObject(objPtr2);
  ASSERT_THAT("test", ::testing::StrEq(Utils::getIDFromDataObject(baseObj2)));
  
  // Any other object should return an empty string
  auto objPtr3 =
  std::shared_ptr<Interpreter::BaseObject>(Interpreter::ObjectTools::createObject("obj"));
  objPtr3->addProperty("randomProperty", "test");
  Interpreter::DataObject baseObj3;
  baseObj3.setClassObject(objPtr3);
  ASSERT_THAT("", ::testing::StrEq(Utils::getIDFromDataObject(baseObj3)));
  
  // Any other data object, for example, a composite object, should return an empty string
  Interpreter::DataObject composeObj;
  composeObj.compose({voidObj, stringObj});
  ASSERT_THAT("", ::testing::StrEq(Utils::getIDFromDataObject(composeObj)));
}//getIDFromDataObject
