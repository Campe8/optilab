#include "utest_globals.hpp"

#include <cstddef>    // fot std::size_t
#include <functional>
#include <vector>

#include "GhostConstraint.hpp"
#include "GhostVariable.hpp"

namespace {
  
  class MyConstraint : public ghost::GhostConstraint
  {
    double required_cost() const override
    {
      return 0.;
    }
    
  public:
    MyConstraint() = default;
    
    MyConstraint( const std::vector< std::reference_wrapper<ghost::GhostVariable> >& variables )
    : GhostConstraint( variables ) {}
    
    ghost::GhostVariable& get_var( int index ) const { return variables[index]; }
    std::size_t number_variables() const { return variables.size(); }
    
  };
  
}// namespace

class GhostConstraintTest : public ::testing::Test
{
public:
  ghost::GhostVariable var1;
  ghost::GhostVariable var2;
  ghost::GhostVariable var3;

  std::vector< std::reference_wrapper<ghost::GhostVariable> > vec1;
  std::vector< std::reference_wrapper<ghost::GhostVariable> > vec2;
  
  MyConstraint* ctr1;
  MyConstraint* ctr2;

  GhostConstraintTest()
    : var1 { "v1", "v1", {1,3,5,7,9} },
      var2 { "v2", "v2", {2,4,6,8} },
      var3 { "v3", "v3", {1,2,3,4,5,6,7,8,9} }
  {
    vec1 = { var1, var2 };
    vec2 = { var1, var3 };
    ctr1 = new MyConstraint( vec1 );
    ctr2 = new MyConstraint( vec2 );
  }

  ~GhostConstraintTest()
  {
    delete ctr1;
    delete ctr2;
  }
};

OPTILAB_TEST_F(GhostConstraintTest, IDs)
{
  EXPECT_EQ( ctr1->get_id(), 0 );
  EXPECT_EQ( ctr2->get_id(), 1 );
}// IDs

OPTILAB_TEST_F(GhostConstraintTest, Copy)
{
  MyConstraint ctr_copy1( *ctr1 );
  
  EXPECT_EQ( ctr1->get_id(), 2 );
  EXPECT_EQ( ctr_copy1.get_id(), 2 );

  EXPECT_TRUE( ctr_copy1.has_variable( var1 ) );
  EXPECT_TRUE( ctr_copy1.has_variable( var2 ) );
  EXPECT_FALSE( ctr_copy1.has_variable( var3 ) );

  EXPECT_THAT( ctr1->get_var(0).possible_values(), ::testing::ElementsAre( 1,3,5,7,9 ) );
  EXPECT_THAT( ctr1->get_var(1).possible_values(), ::testing::ElementsAre( 2,4,6,8 ) );
  EXPECT_THAT( ctr2->get_var(1).possible_values(), ::testing::ElementsAre( 1,2,3,4,5,6,7,8,9 ) );

  EXPECT_EQ( ctr1->get_var(0).get_id(), ctr_copy1.get_var(0).get_id() );
  EXPECT_EQ( ctr1->get_var(0).get_value(), ctr_copy1.get_var(0).get_value() );

  ctr1->get_var(0).set_value( 5 );
  ctr_copy1.get_var(0).set_value( 3 );
  EXPECT_EQ( ctr1->get_var(0).get_value(), 3 );
  EXPECT_EQ( ctr_copy1.get_var(0).get_value(), 3 );
}// Copy

OPTILAB_TEST_F(GhostConstraintTest, has_variable)
{
  EXPECT_TRUE( ctr1->has_variable( var1 ) );
  EXPECT_TRUE( ctr1->has_variable( var2 ) );
  EXPECT_FALSE( ctr1->has_variable( var3 ) );

  EXPECT_TRUE( ctr2->has_variable( var1 ) );
  EXPECT_TRUE( ctr2->has_variable( var3 ) );
  EXPECT_FALSE( ctr2->has_variable( var2 ) );
}// has_variable
