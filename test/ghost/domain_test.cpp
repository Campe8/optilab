#include "utest_globals.hpp"

#include <cstddef>    // fot std::size_t
#include <functional>
#include <vector>

#include "DomainTypeDefs.hpp"
#include "GhostDomain.hpp"

class GhostDomainTest : public ::testing::Test
{
public:
  ghost::GhostDomain *domain1;
  ghost::GhostDomain *domain2;
  ghost::GhostDomain *domain3;
  std::vector< INT_64 > v {1,3,-5,7,9};

  GhostDomainTest()
  {
    domain1 = new ghost::GhostDomain( v );
    domain2 = new ghost::GhostDomain( v );
    domain3 = new ghost::GhostDomain( 1, 3 );
  }

  ~GhostDomainTest()
  {
    delete domain1;
    delete domain2;
    delete domain3;     
  }

  ::testing::AssertionResult CanFind( INT_64 n )
  {
    if( std::find( v.begin(), v.end(), n ) != v.end() )
      return ::testing::AssertionSuccess();
    else
      return ::testing::AssertionFailure() << n << " is NOT in the domain";
  }
};

OPTILAB_TEST_F(GhostDomainTest, ThrowException)
{
  EXPECT_ANY_THROW( domain1->get_value( -1 ) );
  EXPECT_ANY_THROW( domain2->get_value( 5 ) );
  EXPECT_ANY_THROW( domain3->get_value( 3 ) );

  EXPECT_ANY_THROW( domain1->index_of( -1 ) );
  EXPECT_ANY_THROW( domain1->index_of( 0 ) );
  EXPECT_ANY_THROW( domain2->index_of( 2 ) );
  EXPECT_ANY_THROW( domain2->index_of( 4 ) );
  EXPECT_ANY_THROW( domain3->index_of( 0 ) );
  EXPECT_ANY_THROW( domain3->index_of( 4 ) );
}// ThrowException

OPTILAB_TEST_F(GhostDomainTest, getSize)
{
  EXPECT_EQ( domain1->get_size(), 5 );
  EXPECT_EQ( domain2->get_size(), 5 );
  EXPECT_EQ( domain3->get_size(), 3 );
}// getSize

OPTILAB_TEST_F(GhostDomainTest, getValue)
{
  EXPECT_EQ( domain1->get_value( 0 ), 1 );
  EXPECT_EQ( domain1->get_value( 1 ), 3 );
  EXPECT_EQ( domain1->get_value( 2 ), -5 );
  EXPECT_EQ( domain1->get_value( 3 ), 7 );
  EXPECT_EQ( domain1->get_value( 4 ), 9 );

  EXPECT_EQ( domain2->get_value( 0 ), 1 );
  EXPECT_EQ( domain2->get_value( 1 ), 3 );
  EXPECT_EQ( domain2->get_value( 2 ), -5 );
  EXPECT_EQ( domain2->get_value( 3 ), 7 );
  EXPECT_EQ( domain2->get_value( 4 ), 9 );

  EXPECT_EQ( domain3->get_value( 0 ), 1 );
  EXPECT_EQ( domain3->get_value( 1 ), 2 );
  EXPECT_EQ( domain3->get_value( 2 ), 3 );
}// getValue

OPTILAB_TEST_F(GhostDomainTest, indexOf)
{
  EXPECT_EQ( domain1->index_of( 1 ), 0 );
  EXPECT_EQ( domain1->index_of( 3 ), 1 );
  EXPECT_EQ( domain1->index_of( -5 ), 2 );
  EXPECT_EQ( domain1->index_of( 7 ), 3 );
  EXPECT_EQ( domain1->index_of( 9 ), 4 );

  EXPECT_EQ( domain2->index_of( 1 ), 0 );
  EXPECT_EQ( domain2->index_of( 3 ), 1 );
  EXPECT_EQ( domain2->index_of( -5 ), 2 );
  EXPECT_EQ( domain2->index_of( 7 ), 3 );
  EXPECT_EQ( domain2->index_of( 9 ), 4 );

  EXPECT_EQ( domain3->index_of( 1 ), 0 );
  EXPECT_EQ( domain3->index_of( 2 ), 1 );
  EXPECT_EQ( domain3->index_of( 3 ), 2 );
}// indexOf

OPTILAB_TEST_F(GhostDomainTest, randomValue)
{
  EXPECT_TRUE( CanFind( domain1->random_value() ) );
  EXPECT_TRUE( CanFind( domain1->random_value() ) );
  EXPECT_TRUE( CanFind( domain1->random_value() ) );
  EXPECT_TRUE( CanFind( domain1->random_value() ) );
  EXPECT_TRUE( CanFind( domain1->random_value() ) );

  EXPECT_TRUE( CanFind( domain2->random_value() ) );
  EXPECT_TRUE( CanFind( domain2->random_value() ) );
  EXPECT_TRUE( CanFind( domain2->random_value() ) );
  EXPECT_TRUE( CanFind( domain2->random_value() ) );
  EXPECT_TRUE( CanFind( domain2->random_value() ) );

  /*
  std::vector<int> count(5);
  for( int i = 0 ; i < 10000 ; ++i )
    ++count[ domain2->index_of( domain2->random_value() ) ];
  std::cout << (double)count[0] / 100 << "% "
	    << (double)count[1] / 100 << "% "
	    << (double)count[2] / 100 << "% "
	    << (double)count[3] / 100 << "% "
	    << (double)count[4] / 100 << "%\n";*/
}// randomValue
