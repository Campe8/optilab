#pragma once

// boost assert
#include <boost/assert.hpp>

// Google unit test
#include "utest_globals.hpp"

// C++ std libs
#include <stdio.h>
#include <iostream>
#include <exception>
#include <stdexcept>
#include <limits>
#include <unordered_map>
#include <cassert>

int runGoogleUnitTest(int *argc, char **argv);
