
#include "utest_globals.hpp"

#include "ModelTestInc.hpp"
#include "SolverTestInc.hpp"

#include "ModelGenerator.hpp"

#include "VariableSemanticDecision.hpp"

#ifdef RUNTEST

namespace Model {
  
  class ModelGeneratorTest : public ModelGenerator {
  public:
    /// Expose contructor for unit test purposes
    ModelGeneratorTest(){}
  };
  
}// end namespace Model

OPTILAB_TEST(ModelGenerator, model)
{
  using namespace Model;
  
  ModelGeneratorTest modGen;
  auto metaInfo = std::unique_ptr<ModelMetaInfo>(getModelMetaInfo("mdl", 1));
  auto model = modGen.model(std::move(metaInfo));
  
  ASSERT_TRUE(model);
  ASSERT_EQ(model->getMetaInfo()->modelName(), "mdl");
  ASSERT_EQ(model->getMetaInfo()->solutionLimit(), 1);
}//model

OPTILAB_TEST(ModelGenerator, modelObject)
{
  using namespace Core;
  using namespace Model;
  
  ModelGeneratorTest modGen;
  auto metaInfo = std::unique_ptr<ModelMetaInfo>(getModelMetaInfo("mdl", 1));
  auto model = modGen.model(std::move(metaInfo));
  
  std::unique_ptr<ModelObjectDescriptorVariableDomainInteger> domDes(new ModelObjectDescriptorVariableDomainInteger(1));
  std::unique_ptr<VariableSemanticDecision> varSem(new VariableSemanticDecision("testVar"));
  
  std::shared_ptr<ModelObjectDescriptorPrimitiveVariable> primVar(new ModelObjectDescriptorPrimitiveVariable(std::move(domDes), std::move(varSem)));
  
  auto mdlObj = modGen.modelObject(primVar, model);
  ASSERT_TRUE(mdlObj);
  ASSERT_TRUE(ModelObjectVariable::isa(mdlObj.get()));
  
  auto mdlObjCast = ModelObjectVariable::cast(mdlObj.get());
  auto var = mdlObjCast->variable();
  ASSERT_TRUE(var);
  
  // Check semantic of the variable
  ASSERT_TRUE(var->variableSemantic()->getVariableNameIdentifier() == "testVar");
  
  // Check variable domain
  ASSERT_EQ(var->domainListSize(), 1);
}//modelObject

OPTILAB_TEST(ModelGenerator, constraintParameter)
{
  using namespace Core;
  using namespace Model;
  
  ModelGeneratorTest modGen;
  auto metaInfo = std::unique_ptr<ModelMetaInfo>(getModelMetaInfo("mdl", 1));
  auto model = modGen.model(std::move(metaInfo));

  auto conParam = std::unique_ptr<LeafConstraintParameterPrimitiveInt64>(new LeafConstraintParameterPrimitiveInt64(1));
  auto param = modGen.constraintParameter(std::move(conParam), model);
  
  ASSERT_TRUE(param);
  ASSERT_EQ(param->getParameterClass(), ConstraintParameter::ConstraintParameterClass::CON_PARAM_CLASS_DOMAIN);
}//constraintParameter

#endif
