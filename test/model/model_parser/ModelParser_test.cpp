
#include "utest_globals.hpp"

#include "SolverTestInc.hpp"

#include "ModelParser.hpp"
#include "ModelDefs.hpp"

#ifdef RUNTEST

OPTILAB_TEST(ModelParser, parse)
{
  using namespace Model;
  
  std::stringstream ss;
  ss << getSimpleJSONModel2();
  
  // Parse stream
  ModelParser mp;
  auto tree = mp.parse(ss);
  
  // Check AST
  ASSERT_TRUE(tree);
  
  auto treeRoot = tree->getRoot();
  ASSERT_TRUE(treeRoot);
  ASSERT_TRUE(treeRoot->modelName() == "Test");
  ASSERT_EQ(treeRoot->solutionLimit(), 1);
  ASSERT_EQ(treeRoot->modelFrameworkType(), ModelAbstractSyntaxTreeNodeRoot::ModelFrameworkType::MFT_CSP);
  
  ASSERT_EQ(tree->numLeaves(Model::ModelObjectDescriptorClass::MODEL_OBJ_DESCRIPTOR_VARIABLE), 3);
  ASSERT_EQ(tree->numLeaves(Model::ModelObjectDescriptorClass::MODEL_OBJ_DESCRIPTOR_CONSTRAINT), 0);
  ASSERT_EQ(tree->numLeaves(Model::ModelObjectDescriptorClass::MODEL_OBJ_DESCRIPTOR_SEARCH_SEMANTIC), 1);
  
  auto subtreeVar = tree->getSubtree(Model::ModelObjectDescriptorClass::MODEL_OBJ_DESCRIPTOR_VARIABLE);
  ASSERT_TRUE(subtreeVar);
  ASSERT_FALSE(subtreeVar->isLeaf());
  ASSERT_EQ(subtreeVar->numChildren(), 3);
  
  auto subtreeCon = tree->getSubtree(Model::ModelObjectDescriptorClass::MODEL_OBJ_DESCRIPTOR_CONSTRAINT);
  ASSERT_TRUE(subtreeCon);
  ASSERT_TRUE(subtreeCon->isLeaf());
  ASSERT_EQ(subtreeCon->numChildren(), 0);
  
  auto subtreeSrc = tree->getSubtree(Model::ModelObjectDescriptorClass::MODEL_OBJ_DESCRIPTOR_SEARCH_SEMANTIC);
  ASSERT_TRUE(subtreeSrc);
  ASSERT_FALSE(subtreeSrc->isLeaf());
  ASSERT_EQ(subtreeSrc->numChildren(), 1);
}//parse

#endif
