
#include "utest_globals.hpp"

#include "SolverTestInc.hpp"

#include "LeafConstraintParameter.hpp"
#include "ModelDefs.hpp"

#include <string>

namespace Model {
  class LeafConstraintParameterPrimitiveTest : public LeafConstraintParameterPrimitive {
  public:
    
    LeafConstraintParameterPrimitiveTest(LeafConstraintParameter::LeafConstraintParameterType aLeafConstraintParameterType)
    : LeafConstraintParameterPrimitive(aLeafConstraintParameterType)
    {
    }
    
  };
  
}// end namespace Model

OPTILAB_TEST(LeafConstraintParameter, LeafConstraintParameterPrimitive)
{
  using namespace Model;
  
  LeafConstraintParameterPrimitiveTest lcpt(LeafConstraintParameter::LeafConstraintParameterType::LC_PARAM_INT64);
  
  ASSERT_EQ(static_cast<int>(lcpt.leafConstraintParameterType()),
            static_cast<int>(LeafConstraintParameter::LeafConstraintParameterType::LC_PARAM_INT64));
  
  ASSERT_FALSE(lcpt.isComposite());
}//LeafConstraintParameterPrimitive

OPTILAB_TEST(LeafConstraintParameter, LeafConstraintParameterPrimitiveInt64)
{
  using namespace Model;
  
  INT_64 paramVal{100};
  LeafConstraintParameterPrimitiveInt64 lcpInt64(paramVal);
  
  ASSERT_EQ(static_cast<int>(lcpInt64.leafConstraintParameterType()),
            static_cast<int>(LeafConstraintParameter::LeafConstraintParameterType::LC_PARAM_INT64));
  ASSERT_FALSE(lcpInt64.isComposite());
  
  
  ASSERT_EQ(lcpInt64.getValue(), paramVal);
  
  // Override with neg inf
  lcpInt64.overrideValueWithNegInf();
  ASSERT_EQ(lcpInt64.getValue(), Core::Limits::int64Min());
  
  // Override with pos inf
  lcpInt64.overrideValueWithPosInf();
  ASSERT_EQ(lcpInt64.getValue(), Core::Limits::int64Max());
}//LeafConstraintParameterPrimitiveInt64

OPTILAB_TEST(LeafConstraintParameter, LeafConstraintParameterPrimitiveBool)
{
  using namespace Model;
  
  LeafConstraintParameterPrimitiveBool lcpBool;
  
  ASSERT_EQ(static_cast<int>(lcpBool.leafConstraintParameterType()),
            static_cast<int>(LeafConstraintParameter::LeafConstraintParameterType::LC_PARAM_BOOL));
  ASSERT_FALSE(lcpBool.isComposite());
  
  
  ASSERT_TRUE(lcpBool.isIndeterminate());
  
  // Override with true
  lcpBool.overrideValue(true);
  ASSERT_FALSE(lcpBool.isIndeterminate());
  ASSERT_TRUE(lcpBool.getValue());
  
  // Override with false
  lcpBool.overrideValue(false);
  ASSERT_FALSE(lcpBool.isIndeterminate());
  ASSERT_FALSE(lcpBool.getValue());
}//LeafConstraintParameterPrimitiveBool

OPTILAB_TEST(LeafConstraintParameter, LeafConstraintParameterPrimitiveVarId)
{
  using namespace Model;
  
  std::string testStringVarId{"test"};
  LeafConstraintParameterPrimitiveVarId lcpVarId(testStringVarId);
  
  ASSERT_EQ(static_cast<int>(lcpVarId.leafConstraintParameterType()),
            static_cast<int>(LeafConstraintParameter::LeafConstraintParameterType::LC_PARAM_VAR_ID));
  ASSERT_FALSE(lcpVarId.isComposite());
  
  
  ASSERT_TRUE(lcpVarId.getValue() == testStringVarId);
}//LeafConstraintParameterPrimitiveVarId

OPTILAB_TEST(LeafConstraintParameter, LeafConstraintParameterComposite)
{
  using namespace Model;
  
  LeafConstraintParameterComposite lcpc;
  std::shared_ptr<LeafConstraintParameterComposite> lcpcAux = std::make_shared<LeafConstraintParameterComposite>();
  
  std::shared_ptr<LeafConstraintParameterPrimitiveVarId> lcpVarId = std::make_shared<LeafConstraintParameterPrimitiveVarId>("test");
  std::shared_ptr<LeafConstraintParameterPrimitiveBool> lcpBool = std::make_shared<LeafConstraintParameterPrimitiveBool>();
  std::shared_ptr<LeafConstraintParameterPrimitiveInt64> lcpInt64 = std::make_shared<LeafConstraintParameterPrimitiveInt64>(100);
  std::shared_ptr<LeafConstraintParameterPrimitiveInt64> lcpInt64Aux = std::make_shared<LeafConstraintParameterPrimitiveInt64>(1000);
  
  // Add parameters to composite parameter
  lcpc.addLeafConstraintParameter(lcpVarId);
  lcpc.addLeafConstraintParameter(lcpBool);
  lcpc.addLeafConstraintParameter(lcpInt64);
  
  lcpcAux->addLeafConstraintParameter(lcpcAux);
  lcpc.addLeafConstraintParameter(lcpcAux);

  ASSERT_TRUE(lcpc.isComposite());
  ASSERT_EQ(lcpc.numLeafConstraintParameters(), 4);
  
  ASSERT_EQ(lcpc.getLeafConstraintParameters(0).get(), lcpVarId.get());
  ASSERT_EQ(lcpc.getLeafConstraintParameters(1).get(), lcpBool.get());
  ASSERT_EQ(lcpc.getLeafConstraintParameters(2).get(), lcpInt64.get());
  ASSERT_EQ(lcpc.getLeafConstraintParameters(3).get(), lcpcAux.get());
}//LeafConstraintParameterComposite
