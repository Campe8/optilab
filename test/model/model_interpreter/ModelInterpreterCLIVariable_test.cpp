
#include "utest_globals.hpp"

#include "SolverTestInc.hpp"

#include "ModelTestInc.hpp"

#include <string>
#include <sstream>

#ifdef RUNTEST
namespace Model {
  
  class StmtInterpreterVariableTest : public StmtInterpreterVariable  {
  public:
    StmtInterpreterVariableTest()
    {
      ModelInterpreterEngineTest mdlInterp;
      pMdlCtx = mdlInterp.createBaseModelContext("ctxTest");
    }

    ModelContext getModelContext()
    {
      return pMdlCtx;
    }
    
    ModelContext interpretStmt(CLI::CLIStmt& aCLIStmt)
    {
      return StmtInterpreterVariable::interpretStmt(pMdlCtx, aCLIStmt);
    }
    
    /// Utility function: returns the structured message from pretty print string
    CLI::CLIStmt getCLIStmt(const std::string& aPrettyPrintMsg)
    {
      auto msg = CLI::Utils::cliStmtStringFromPrettyPrint(aPrettyPrintMsg);
      return CLI::Utils::getCLIStmtFromString(msg);
    }
    
  private:
    ModelContext pMdlCtx;
  };
  
}// end namespace Model

OPTILAB_TEST(ModelInterpreterCLIVariable, emptyStmt)
{
  using namespace Model;
  
  StmtInterpreterVariableTest ivar;
  auto stmt = ivar.getCLIStmt("");
  auto ctx = ivar.interpretStmt(stmt);
  
  // Empty stmt should be interpreted as an empty context
  ASSERT_TRUE(ctx.isEmpty());
}//emptyStmt

OPTILAB_TEST(ModelInterpreterCLIVariable, interpretStmt)
{
  using namespace Model;
  
  StmtInterpreterVariableTest ivar;
  auto stmt = ivar.getCLIStmt("|1=variable|10=x|11=integer|14=expr|15=10");
  auto ctx = ivar.interpretStmt(stmt);
  
  // Model context should be the following:
  // {
  //   "id": "x",
  //   "type": "integer",
  //   "semantic": {
  //     "type": "decision"
  //   },
  //  "domain": {
  //    "type": "bounds",
  //    "values": [
  //      "10",
  //      "10"
  //    ]
  //   }
  // }
  ASSERT_EQ(ctx.context().getValue<std::string>({MDL_AST_LEAF_VAR_ID}), "x");
  ASSERT_EQ(ctx.context().getValue<std::string>({MDL_AST_LEAF_VAR_TYPE}), MDL_AST_LEAF_VAR_TYPE_INT);
  
  auto sem = ctx.context().getSubContext({MDL_AST_LEAF_VAR_SEMANTIC});
  ASSERT_EQ(sem.getValue<std::string>({MDL_AST_LEAF_VAR_SEMANTIC_TYPE}), CLI_MSG_VAL_VAR_SEMANTIC_TYPE_DECISION);
  
  auto dom = ctx.context().getSubContext({MDL_AST_LEAF_VAR_DOMAIN});
  ASSERT_EQ(dom.getValue<std::string>({MDL_AST_LEAF_VAR_DOM_TYPE}), MDL_AST_LEAF_VAR_DOM_TYPE_BOUNDS);
  
  auto br = ctx.context().getValue<std::string>({MDL_AST_LEAF_VAR_BRANCHING});
  ASSERT_EQ(br, MDL_AST_FALSE);
  
  auto out = ctx.context().getValue<std::string>({MDL_AST_LEAF_VAR_OUTPUT});
  ASSERT_EQ(out, MDL_AST_FALSE);
  
  auto vals = dom.getValueList<INT_64>({MDL_AST_LEAF_VAR_DOM_VALS});
  ASSERT_EQ(vals.size(), 2);
  ASSERT_EQ(vals[0], 10);
  ASSERT_EQ(vals[1], 10);
  
  // Another way to get values is using model context as sub-context of values
  auto vals2 = dom.getSubContextList({MDL_AST_LEAF_VAR_DOM_VALS});
  ASSERT_EQ(vals2.size(), 2);
  ASSERT_EQ(vals2[0].getValue<INT_64>({""}), 10);
  ASSERT_EQ(vals2[1].getValue<INT_64>({""}), 10);
}//interpretStmt

OPTILAB_TEST(ModelInterpreterCLIVariable, interpretStmtBranching)
{
  using namespace Model;
  
  StmtInterpreterVariableTest ivar;
  auto stmt = ivar.getCLIStmt("|1=variable|10=x|11=integer|14=expr|15=10|16=1");
  auto ctx = ivar.interpretStmt(stmt);
  auto br = ctx.context().getValue<std::string>({MDL_AST_LEAF_VAR_BRANCHING});
  ASSERT_EQ(br, "true");
  
  auto stmt2 = ivar.getCLIStmt("|1=variable|10=x|11=integer|14=expr|15=10|16=0");
  auto ctx2 = ivar.interpretStmt(stmt2);
  
  auto br2 = ctx2.context().getValue<std::string>({MDL_AST_LEAF_VAR_BRANCHING});
  ASSERT_EQ(br2, "false");
}//interpretStmtBranching

OPTILAB_TEST(ModelInterpreterCLIVariable, interpretStmtVarType)
{
  using namespace Model;
  
  StmtInterpreterVariableTest ivar;
  auto stmt = ivar.getCLIStmt("|1=variable|10=x|11=boolean|14=range|15=0 1|16=1");
  auto ctx = ivar.interpretStmt(stmt);
  auto tp = ctx.context().getValue<std::string>({MDL_AST_LEAF_VAR_TYPE});
  ASSERT_EQ(tp, MDL_AST_LEAF_VAR_TYPE_BOOL);
  
  auto stmt2 = ivar.getCLIStmt("|1=variable|10=x|11=integer|14=expr|15=10|16=1");
  auto ctx2 = ivar.interpretStmt(stmt2);
  auto tp2 = ctx2.context().getValue<std::string>({MDL_AST_LEAF_VAR_TYPE});
  ASSERT_EQ(tp2, MDL_AST_LEAF_VAR_TYPE_INT);
}//interpretStmtVarType

OPTILAB_TEST(ModelInterpreterCLIVariable, interpretStmtVarOutput)
{
  using namespace Model;
  
  StmtInterpreterVariableTest ivar;
  auto stmt = ivar.getCLIStmt("|1=variable|10=x|11=boolean|13=1|14=range|15=0 1|16=1");
  auto ctx = ivar.interpretStmt(stmt);
  auto out = ctx.context().getValue<std::string>({MDL_AST_LEAF_VAR_OUTPUT});
  ASSERT_EQ(out, "true");
  
  auto stmt2 = ivar.getCLIStmt("|1=variable|10=x|11=integer|13=0|14=expr|15=10|16=1");
  auto ctx2 = ivar.interpretStmt(stmt2);
  auto out2 = ctx2.context().getValue<std::string>({MDL_AST_LEAF_VAR_OUTPUT});
  ASSERT_EQ(out2, "false");
}//interpretStmtVarOutput

OPTILAB_TEST(ModelInterpreterCLIVariable, interpretStmtVarSemantic)
{
  using namespace Model;
  
  StmtInterpreterVariableTest ivar;
  auto stmt = ivar.getCLIStmt("|1=variable|10=x|11=boolean|12=support|13=1|14=range|15=0 1|16=1");
  auto ctx = ivar.interpretStmt(stmt);
  auto sm = ctx.context().getSubContext({"semantic"});
  auto tp = sm.getValue<std::string>({MDL_AST_LEAF_VAR_SEMANTIC_TYPE});
  ASSERT_EQ(tp, MDL_AST_LEAF_VAR_SEMANTIC_TYPE_SUPPORT);
  
  auto stmt2 = ivar.getCLIStmt("|1=variable|10=x|11=boolean|12=decision|13=1|14=range|15=0 1|16=1");
  auto ctx2 = ivar.interpretStmt(stmt2);
  auto sm2 = ctx2.context().getSubContext({"semantic"});
  auto tp2 = sm2.getValue<std::string>({MDL_AST_LEAF_VAR_SEMANTIC_TYPE});
  ASSERT_EQ(tp2, MDL_AST_LEAF_VAR_SEMANTIC_TYPE_DECISION);
  
  auto stmt3 = ivar.getCLIStmt("|1=variable|10=x|11=boolean|12=objective|13=1|14=range|15=0 1|16=1");
  auto ctx3 = ivar.interpretStmt(stmt3);
  auto sm3 = ctx3.context().getSubContext({"semantic"});
  auto tp3 = sm3.getValue<std::string>({MDL_AST_LEAF_VAR_SEMANTIC_TYPE});
  auto ext3 = sm3.getValue<std::string>({MDL_AST_LEAF_VAR_SEMANTIC_OPT_EXTREMUM});
  ASSERT_EQ(tp3, MDL_AST_LEAF_VAR_SEMANTIC_TYPE_OBJECTIVE);
  ASSERT_EQ(ext3, MDL_AST_LEAF_VAR_SEMANTIC_OPT_EXTREMUM_MIN);
  
  auto stmt4 = ivar.getCLIStmt("|1=variable|10=x|11=boolean|12=objective|13=1|14=range|15=0 1|16=1|17=minimize");
  auto ctx4 = ivar.interpretStmt(stmt4);
  auto sm4 = ctx4.context().getSubContext({"semantic"});
  auto tp4 = sm4.getValue<std::string>({MDL_AST_LEAF_VAR_SEMANTIC_TYPE});
  auto ext4 = sm4.getValue<std::string>({MDL_AST_LEAF_VAR_SEMANTIC_OPT_EXTREMUM});
  ASSERT_EQ(tp4, MDL_AST_LEAF_VAR_SEMANTIC_TYPE_OBJECTIVE);
  ASSERT_EQ(ext4, MDL_AST_LEAF_VAR_SEMANTIC_OPT_EXTREMUM_MIN);
  
  auto stmt5 = ivar.getCLIStmt("|1=variable|10=x|11=boolean|12=objective|13=1|14=range|15=0 1|16=1|17=maximize");
  auto ctx5 = ivar.interpretStmt(stmt5);
  auto sm5 = ctx5.context().getSubContext({"semantic"});
  auto tp5 = sm5.getValue<std::string>({MDL_AST_LEAF_VAR_SEMANTIC_TYPE});
  auto ext5 = sm5.getValue<std::string>({MDL_AST_LEAF_VAR_SEMANTIC_OPT_EXTREMUM});
  ASSERT_EQ(tp5, MDL_AST_LEAF_VAR_SEMANTIC_TYPE_OBJECTIVE);
  ASSERT_EQ(ext5, MDL_AST_LEAF_VAR_SEMANTIC_OPT_EXTREMUM_MAX);
}//interpretStmtVarSemantic

OPTILAB_TEST(ModelInterpreterCLIVariable, interpretStmtVarDomain)
{
  using namespace Model;
  
  StmtInterpreterVariableTest ivar;
  
  // Range
  auto stmt = ivar.getCLIStmt("|1=variable|10=x|11=boolean|13=1|14=range|15=0 1|16=1");
  auto ctx = ivar.interpretStmt(stmt);
  auto dm = ctx.context().getSubContext({MDL_AST_LEAF_VAR_DOMAIN});
  ASSERT_EQ(dm.getValue<std::string>({MDL_AST_LEAF_VAR_DOM_TYPE}), MDL_AST_LEAF_VAR_DOM_TYPE_BOUNDS);
  
  auto vals = dm.getValueList<INT_64>({MDL_AST_LEAF_VAR_DOM_VALS});
  ASSERT_EQ(vals.size(), 2);
  ASSERT_EQ(vals[0], 0);
  ASSERT_EQ(vals[1], 1);
  
  // Expr
  auto stmt2 = ivar.getCLIStmt("|1=variable|10=x|11=integer|13=0|14=expr|15=10|16=1");
  auto ctx2 = ivar.interpretStmt(stmt2);
  auto dm2 = ctx2.context().getSubContext({MDL_AST_LEAF_VAR_DOMAIN});
  ASSERT_EQ(dm2.getValue<std::string>({MDL_AST_LEAF_VAR_DOM_TYPE}), MDL_AST_LEAF_VAR_DOM_TYPE_BOUNDS);
  
  auto vals2 = dm2.getValueList<INT_64>({MDL_AST_LEAF_VAR_DOM_VALS});
  ASSERT_EQ(vals2.size(), 2);
  ASSERT_EQ(vals2[0], 10);
  ASSERT_EQ(vals2[1], 10);
  
  // List
  auto stmt3 = ivar.getCLIStmt("|1=variable|10=x|11=integer|13=0|14=list|15=10 11 12|16=1");
  auto ctx3 = ivar.interpretStmt(stmt3);
  auto dm3 = ctx3.context().getSubContext({MDL_AST_LEAF_VAR_DOMAIN});
  ASSERT_EQ(dm3.getValue<std::string>({MDL_AST_LEAF_VAR_DOM_TYPE}), MDL_AST_LEAF_VAR_DOM_TYPE_EXTENSIONAL);
  
  auto vals3 = dm3.getValueList<INT_64>({MDL_AST_LEAF_VAR_DOM_VALS});
  ASSERT_EQ(vals3.size(), 3);
  ASSERT_EQ(vals3[0], 10);
  ASSERT_EQ(vals3[1], 11);
  ASSERT_EQ(vals3[2], 12);
}//interpretStmtVarDomain

OPTILAB_TEST(ModelInterpreterCLIVariable, prettyPrintFull)
{
  // Test pretty print with full verbose
  using namespace Model;
  
  StmtInterpreterVariableTest ivar;
  auto eStmt = ivar.getCLIStmt("");
  auto eCtx = ivar.interpretStmt(eStmt);
  std::stringstream ess;
  ivar.prettyPrint(eCtx, ess, 2);
  auto estr = ess.str();
  ASSERT_TRUE(estr.empty());
  
  auto stmt = ivar.getCLIStmt("|1=variable|10=x|11=boolean|12=objective|13=1|14=range|15=0 1|16=1|17=minimize");
  auto ctx = ivar.interpretStmt(stmt);

  std::stringstream ss;
  
  // Pretty print context variable
  ivar.prettyPrint(ctx, ss, 1);
  auto str = ss.str();
  std::string cmpStr = ""
  " id: x\n"
  " type: boolean\n"
  " semantic:\n"
  "   type: objective\n"
  "   extremum: minimize\n"
  " input_order: 0\n"
  " branching: true\n"
  " output: true\n"
  " domain: [0, 1]";
  
  ASSERT_EQ_STR(str, cmpStr);
}//prettyPrintFull

OPTILAB_TEST(ModelInterpreterCLIVariable, prettyPrintNoVerbose)
{
  // Test pretty print with no verbose
  using namespace Model;
  
  StmtInterpreterVariableTest ivar;
  auto eStmt = ivar.getCLIStmt("");
  auto eCtx = ivar.interpretStmt(eStmt);
  std::stringstream ess;
  ivar.prettyPrint(eCtx, ess, 2);
  auto estr = ess.str();
  ASSERT_TRUE(estr.empty());
  
  auto stmt = ivar.getCLIStmt("|1=variable|10=x|11=boolean|12=objective|13=1|14=range|15=0 1|16=1|17=minimize");
  auto ctx = ivar.interpretStmt(stmt);
  
  std::stringstream ss;

  ivar.prettyPrint(ctx, ss, 0);
  auto str = ss.str();
  std::string cmpStr = ""
  " id: x\n"
  " domain: [0, 1]";
  
  ASSERT_EQ_STR(str, cmpStr);
}//prettyPrintNoVerbose
#endif
