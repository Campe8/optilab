
#include "utest_globals.hpp"

#include "SolverTestInc.hpp"
#include "ModelTestInc.hpp"

#ifdef OLD_TEST

namespace {
  CLI::CLIStmt getCLIFromPrettyPrint(const std::string& aStr)
  {
    return CLI::Utils::getCLIStmtFromString(CLI::Utils::cliStmtStringFromPrettyPrint(aStr));
  }
}//end unamed namespace

OPTILAB_TEST(ModelInterpreterEngine, getSymbolType)
{
  // Test getSymbolType method
  using namespace CLI::Utils;
  using namespace Model;
  
  ModelInterpreterEngine mif;
  
  auto varCLI = getCLIFromPrettyPrint("|1=variable|10=x|11=integer|14=expr|15=10");
  ASSERT_EQ(ModelInterpreter::SymbolType::ST_VARIABLE_DECL, mif.getSymbolType(varCLI));
  
  auto conCLI = getCLIFromPrettyPrint("|1=constraint|10=expr|11=c|12=3");
  ASSERT_EQ(ModelInterpreter::SymbolType::ST_CONSTRAINT_DECL, mif.getSymbolType(conCLI));
  
  auto matCLI = getCLIFromPrettyPrint("|1=matrix|10=x|11=integer|12=0|13=0");
  ASSERT_EQ(ModelInterpreter::SymbolType::ST_MATRIX_DECL, mif.getSymbolType(matCLI));
  
  auto fcnCLI = getCLIFromPrettyPrint("|1=fcn_call|10=fcn|11=2|12=x y");
  ASSERT_EQ(ModelInterpreter::SymbolType::ST_UNDEF, mif.getSymbolType(fcnCLI));
  
  auto srcCLI = getCLIFromPrettyPrint("|1=search|10=dfs|11=input_order|12=indomain_min");
  ASSERT_EQ(ModelInterpreter::SymbolType::ST_SEARCH_DECL, mif.getSymbolType(srcCLI));
  
  auto errCLI = getCLIFromPrettyPrint("|1=error|10=error");
  ASSERT_EQ(ModelInterpreter::SymbolType::ST_ERROR, mif.getSymbolType(errCLI));
  
  auto udfCLI = getCLIFromPrettyPrint("|1=undef");
  ASSERT_EQ(ModelInterpreter::SymbolType::ST_UNDEF, mif.getSymbolType(udfCLI));
}//getSymbolType

OPTILAB_TEST(ModelInterpreterEngine, getStmtIntrepreterInstance)
{
  // Test getIntrepreterInstance method
  using namespace Model;
  ModelInterpreterEngineTest mif;
  
  auto mivar = mif.getStmtIntrepreterInstance(ModelInterpreter::SymbolType::ST_VARIABLE_DECL);
  ASSERT_TRUE(mivar);
  ASSERT_EQ(ModelInterpreter::SymbolType::ST_VARIABLE_DECL, mivar->getInterpretedSymbolType());
  
  auto micon = mif.getStmtIntrepreterInstance(ModelInterpreter::SymbolType::ST_CONSTRAINT_DECL);
  ASSERT_TRUE(micon);
  ASSERT_EQ(ModelInterpreter::SymbolType::ST_CONSTRAINT_DECL, micon->getInterpretedSymbolType());
  
  auto mimat = mif.getStmtIntrepreterInstance(ModelInterpreter::SymbolType::ST_MATRIX_DECL);
  ASSERT_TRUE(mimat);
  ASSERT_EQ(ModelInterpreter::SymbolType::ST_MATRIX_DECL, mimat->getInterpretedSymbolType());
  
  auto misrc = mif.getStmtIntrepreterInstance(ModelInterpreter::SymbolType::ST_SEARCH_DECL);
  ASSERT_TRUE(misrc);
  ASSERT_EQ(ModelInterpreter::SymbolType::ST_SEARCH_DECL, misrc->getInterpretedSymbolType());
  
  auto mierr = mif.getStmtIntrepreterInstance(ModelInterpreter::SymbolType::ST_ERROR);
  ASSERT_FALSE(mierr);
  
  auto miudf = mif.getStmtIntrepreterInstance(ModelInterpreter::SymbolType::ST_UNDEF);
  ASSERT_FALSE(miudf);
}//getStmtIntrepreterInstance

OPTILAB_TEST(ModelInterpreterEngine, getCtxIntrepreterInstance)
{
  // Test getIntrepreterInstance method
  using namespace Model;
  ModelInterpreterEngineTest mif;
  
  auto mivar = mif.getCtxIntrepreterInstance(ModelInterpreter::SymbolType::ST_VARIABLE_DECL);
  ASSERT_TRUE(mivar);
  ASSERT_EQ(ModelInterpreter::SymbolType::ST_VARIABLE_DECL, mivar->getInterpretedSymbolType());
  
  auto micon = mif.getCtxIntrepreterInstance(ModelInterpreter::SymbolType::ST_CONSTRAINT_DECL);
  ASSERT_TRUE(micon);
  ASSERT_EQ(ModelInterpreter::SymbolType::ST_CONSTRAINT_DECL, micon->getInterpretedSymbolType());
  
  auto mimat = mif.getCtxIntrepreterInstance(ModelInterpreter::SymbolType::ST_MATRIX_DECL);
  ASSERT_TRUE(mimat);
  ASSERT_EQ(ModelInterpreter::SymbolType::ST_MATRIX_DECL, mimat->getInterpretedSymbolType());
  
  auto misrc = mif.getCtxIntrepreterInstance(ModelInterpreter::SymbolType::ST_SEARCH_DECL);
  ASSERT_TRUE(misrc);
  //ASSERT_EQ(ModelInterpreter::SymbolType::ST_SEARCH_DECL, misrc->getInterpretedSymbolType());
  
  auto mierr = mif.getCtxIntrepreterInstance(ModelInterpreter::SymbolType::ST_ERROR);
  //ASSERT_FALSE(mierr);
  
  auto miudf = mif.getCtxIntrepreterInstance(ModelInterpreter::SymbolType::ST_UNDEF);
  //ASSERT_FALSE(miudf);
}//getCtxIntrepreterInstance

OPTILAB_TEST(ModelInterpreterEngine, createBaseModelContext)
{
  // Test createBaseModelContext method
  using namespace Model;
  ModelInterpreterEngineTest mie;
  
  auto mctx = mie.createBaseModelContext("MCtx");
  ASSERT_FALSE(mctx.isEmpty());
  ASSERT_EQ(mctx.getView().get(), &(mctx.view()));
  ASSERT_EQ(mctx.getContext().get(), &(mctx.context()));
  
  // View should be empty
  ASSERT_TRUE(mctx.view().getSerializedView().empty());
  
  // Context should have the same name as the model context
  ASSERT_EQ(mctx.context().getContextId(), MDL_AST_INSTANCE);
  
  // Context shouldn't be empty
  ASSERT_FALSE(mctx.context().isEmpty());
  
  /*
   Base context should be the following:
   {
   "instance": {
   "variables": {
   "var": ""
   },
   "constraints": {
   "con": ""
   },
   "parameters": {
   "mat": ""
   },
   "search": "",
   "name": "ModelCtx",
   "type": "CSP",
   "solution": "1"
   }
   }
   */
  std::stringstream ss;
  mctx.context().serializeContext(ss);
  auto strCtx = ss.str();
  ASSERT_NE(strCtx.find(R"("name": "MCtx")"), std::string::npos);
  ASSERT_NE(strCtx.find(R"("search":)"), std::string::npos);
  ASSERT_NE(strCtx.find(R"("type": "CSP")"), std::string::npos);
  ASSERT_NE(strCtx.find(R"("solution": "1")"), std::string::npos);
  ASSERT_NE(strCtx.find(R"("variables":)"), std::string::npos);
  ASSERT_NE(strCtx.find(R"("constraints":)"), std::string::npos);
  ASSERT_NE(strCtx.find(R"("parameters":)"), std::string::npos);
  ASSERT_NE(strCtx.find(R"("var": "")"), std::string::npos);
  ASSERT_NE(strCtx.find(R"("con": "")"), std::string::npos);
  ASSERT_NE(strCtx.find(R"("mat": "")"), std::string::npos);
  ASSERT_NE(strCtx.find(R"("brc": "")"), std::string::npos);
}//createBaseModelContext

OPTILAB_TEST(ModelInterpreterEngine, expandModelContext)
{
  // Test expandModelContext method
  using namespace Model;
  ModelInterpreterEngineTest mie;
  
  auto mctx = mie.createBaseModelContext("MCtx");
  auto varCLI = getCLIFromPrettyPrint("|1=variable|10=x|11=integer|14=expr|15=10");
  auto sictx = mie.interpretStmt(mctx, varCLI);
  mie.expandModelContext(mctx, sictx.second, sictx.first);
  
  /*
   mctx should contain the following
   variable declaration:
   "variables": {
   "var": [
   {
   "id": "x",
   "branching": "false",
   "type": "integer",
   "output": "false",
   "input_order": "0",
   "semantic": {
   "type": "decision"
   },
   "domain": {
   "type": "bounds",
   "values": [
   "10"
   ]
   },
   "_objtype": "var",
   "_finterp": "1"
   }
   ]
   },
  */
  std::stringstream ss;
  mctx.context().serializeContext(ss);
  auto strCtx = ss.str();
  
  // Variable is in the context
  ASSERT_NE(strCtx.find(R"("var": [)"), std::string::npos);
  
  // Check variable context
  ASSERT_NE(strCtx.find(R"("id": "x")"), std::string::npos);
  ASSERT_NE(strCtx.find(R"("branching": "false")"), std::string::npos);
  ASSERT_NE(strCtx.find(R"("output": "false")"), std::string::npos);
  ASSERT_NE(strCtx.find(R"("input_order": "0")"), std::string::npos);
  ASSERT_NE(strCtx.find(R"("type": "decision")"), std::string::npos);
  ASSERT_NE(strCtx.find(R"("type": "bounds")"), std::string::npos);
  ASSERT_NE(strCtx.find(R"("_objtype": "var")"), std::string::npos);
  ASSERT_NE(strCtx.find(R"("_finterp": "1")"), std::string::npos);
  
  // Empty context do not affect the model in expansion
  mie.expandModelContext(mctx, Context(), ModelInterpreter::SymbolType::ST_CONSTRAINT_DECL);
  
  // Constraint list is empty
  ASSERT_NE(strCtx.find(R"("con": "")"), std::string::npos);
}//expandModelContext

#endif

