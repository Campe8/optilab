
#include "utest_globals.hpp"

#include "MCPPConstraint.hpp"
#include "ConstraintContextObject.hpp"

#include "ObjectTools.hpp"
#include "ObjectHelper.hpp"

#include <sstream>

namespace otools = Interpreter::ObjectTools;

#define BOSPtr(x) std::shared_ptr<Interpreter::BaseObject>(x)

OPTILAB_TEST(MCPPConstraint, printConstraintObjects)
{
  // Test pretty print of constraint objects
  using namespace Model;
  MCPPConstraint pp;
  
  // Create a constraint context object
  Interpreter::DataObject constraint;
  
  Interpreter::ObjectFcn* doFcn = new Interpreter::ObjectFcn("nq");
  Interpreter::DataObject dobj("nq");
  dobj.setFcnObject(doFcn);
  dobj.compose({Interpreter::DataObject(0), Interpreter::DataObject(1)});
  
  Interpreter::BaseObjectSPtr conObj(otools::createObjectConstraint(dobj));
  constraint.setClassObject(conObj);
  
  std::stringstream ss1;
  auto constraintCtx1 = std::make_shared<ConstraintContextObject>("c1", constraint);
  constraintCtx1->setPropagationType(Model::ConstraintContextObject::PropagationType::PT_BOUNDS);
  pp.prettyPrint(constraintCtx1, ss1, false);
  std::string constraintPP = ss1.str();
  
  // Pretty print (not full) on constraint objects:
  // <id>:
  //  name: <constraint_name>
  //  scope: [<arg_1>, <arg_n>]
  EXPECT_THAT(constraintPP, testing::HasSubstr("c1: \n"));
  EXPECT_THAT(constraintPP, testing::HasSubstr("name: nq"));
  EXPECT_THAT(constraintPP, testing::HasSubstr("scope: [0, 1]"));
  
  std::stringstream ss2;
  auto constraintCtx2 = std::make_shared<ConstraintContextObject>("c2", constraint);
  constraintCtx2->setPropagationType(Model::ConstraintContextObject::PropagationType::PT_BOUNDS);
  pp.prettyPrint(constraintCtx2, ss2, true);
  std::string fConstraintPP = ss2.str();
  
  // Pretty print (full) on constraint objects:
  // <id>:
  //  type: Constraint
  //  scope: [<arg_1>, <arg_n>]
  //  propagation: [bounds, domain]
  //  name: <constraint_name>
  EXPECT_THAT(fConstraintPP, testing::HasSubstr("c2: \n"));
  EXPECT_THAT(fConstraintPP, testing::HasSubstr("name: nq"));
  EXPECT_THAT(fConstraintPP, testing::HasSubstr("type: Constraint"));
  EXPECT_THAT(fConstraintPP, testing::HasSubstr("scope: [0, 1]"));
  EXPECT_THAT(fConstraintPP, testing::HasSubstr("propagation: bounds"));
}//printScalarObjects
