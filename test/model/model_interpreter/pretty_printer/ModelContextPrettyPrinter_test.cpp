
#include "utest_globals.hpp"

#include "BaseObject.hpp"
#include "ObjectTools.hpp"
#include "ModelContextPrettyPrinter.hpp"
#include "PrettyPrinterTools.hpp"

#include <sstream>

namespace Model {
  
  class MCPPTest : public ModelContextPrettyPrinter {
  public:
    void prettyPrint(const std::shared_ptr<ContextObject>&, std::ostream&, bool) override {}

    void outTag(const std::string& aTag, std::ostream& aOut, int aLevel = 0)
    {
      ModelContextPrettyPrinter::outTag(aTag, aOut, aLevel);
    }
    
    void outTagLn(const std::string& aTag, std::ostream& aOut, int aLevel = 0)
    {
      ModelContextPrettyPrinter::outTagLn(aTag, aOut, aLevel);
    }
  
    void prettyPrintFullyInterpreted(Interpreter::BaseObject* aObj, std::ostream& aOut)
    {
      ModelContextPrettyPrinter::prettyPrintFullyInterpreted(aObj, aOut);
    }
    
    void prettyPrintFullyInterpreted(bool aFullyInterpreted, std::ostream& aOut)
    {
      ModelContextPrettyPrinter::prettyPrintFullyInterpreted(aFullyInterpreted, aOut);
    }
    
    std::string objToString(const Interpreter::DataObject& aObj)
    {
      std::stringstream ss;
      PrettyPrinter::Tools::toStream(aObj, ss);
      return ss.str();
    }
  };
  
}// enend namespace Modee

OPTILAB_TEST(ModelContextPrettyPrinter, printPrimitiveDataObject)
{
  // Test pretty print of primitive objects
  using namespace Model;
  using namespace Interpreter;
  
  MCPPTest mcpp;
  
  // int
  ASSERT_TRUE(mcpp.objToString(1) =="1");
  
  // bool
  ASSERT_TRUE(mcpp.objToString(true) == "true");
  ASSERT_TRUE(mcpp.objToString(false) == "false");
  
  // double
  ASSERT_TRUE(mcpp.objToString(1.5) == "1.5");
  
  // size_t
  ASSERT_TRUE(mcpp.objToString(static_cast<std::size_t>(1000)) == "1000");
  
  // string
  ASSERT_TRUE(mcpp.objToString("abc") == "abc");
  
  // void
  ASSERT_TRUE(mcpp.objToString(DataObject()).empty());
}//printPrimitiveDataObject

OPTILAB_TEST(ModelContextPrettyPrinter, printCompositeDataObject)
{
  // Test pretty print of composite objects
  using namespace Model;
  using namespace Interpreter;
  
  MCPPTest mcpp;
  
  std::vector<DataObject> comp = { DataObject(1), DataObject(true), DataObject(5.0) };
  DataObject docomp;
  docomp.compose(comp);
  
  auto pp = mcpp.objToString(docomp);
  ASSERT_TRUE(pp == "[1, true, 5]");
}//printCompositeDataObject

OPTILAB_TEST(ModelContextPrettyPrinter, printList)
{
  // Test pretty print of list objects
  using namespace Model;
  using namespace Interpreter;
  
  MCPPTest mcpp;
  
  Interpreter::BaseObjectSPtr bobj(ObjectTools::createListObject({
    DataObject(1), DataObject(2), DataObject(3) }));

  DataObject doclass;
  doclass.setClassObject(bobj);
  
  auto pp = mcpp.objToString(doclass);
  ASSERT_TRUE(pp == "[1, 2, 3]");
}//printList
