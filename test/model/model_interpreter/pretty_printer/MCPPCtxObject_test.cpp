
#include "utest_globals.hpp"

#include "MCPPCtxObject.hpp"

#include "ObjectTools.hpp"
#include "ObjectHelper.hpp"

#include <sstream>

namespace otools = Interpreter::ObjectTools;

#define BOSPtr(x) std::shared_ptr<Interpreter::BaseObject>(x)

OPTILAB_TEST(MCPPCtxObject, printScalarObjects)
{
  // Test pretty print of scalar objects
  using namespace Model;
  MCPPCtxObject pp;
  
  // Create a standard context object on a scalar base object
  Interpreter::DataObject scalar;
  scalar.setClassObject(BOSPtr(otools::createScalarObject(Interpreter::DataObject(1))));
  
  std::stringstream ss1;
  pp.prettyPrint(std::make_shared<ContextObject>("Id1", scalar), ss1, false);
  std::string scalarPP = ss1.str();
  
  // Pretty print (not full) on scalar objects:
  // <id>: <value>
  EXPECT_THAT(scalarPP, testing::HasSubstr("Id1: 1"));
  
  std::stringstream ss2;
  pp.prettyPrint(std::make_shared<ContextObject>("Id2", scalar), ss2, true);
  std::string scalarFullPP = ss2.str();
  
  // Pretty print (full) on scalar objects:
  // <id>: <value>
  // type: <type>
  EXPECT_THAT(scalarFullPP, testing::HasSubstr("Id2: 1"));
  EXPECT_THAT(scalarFullPP, testing::HasSubstr("type: int"));
}//printScalarObjects

OPTILAB_TEST(MCPPCtxObject, printEmptyListObjects)
{
  // Test pretty print of an empty list objects
  using namespace Model;
  MCPPCtxObject pp;
  
  Interpreter::DataObject list;
  list.setClassObject(BOSPtr(otools::createListObject({})));
  
  std::stringstream ss1;
  pp.prettyPrint(std::make_shared<ContextObject>("Id1", list), ss1, false);
  std::string listPP = ss1.str();
  
  // Pretty print (not full) on list objects:
  // <id>: [<value_1>, ..., <value_n>]
  EXPECT_THAT(listPP, testing::HasSubstr("Id1: []"));
}//printEmptyListObjects

OPTILAB_TEST(MCPPCtxObject, printListObjects)
{
  // Test pretty print of list objects
  using namespace Model;
  MCPPCtxObject pp;
  
  // Create a standard context object on a list base object
  Interpreter::DataObject list;
  list.setClassObject(BOSPtr(otools::createListObject({Interpreter::DataObject(1),
    Interpreter::DataObject(2)})));
  
  std::stringstream ss1;
  pp.prettyPrint(std::make_shared<ContextObject>("Id1", list), ss1, false);
  std::string listPP = ss1.str();
  
  // Pretty print (not full) on list objects:
  // <id>: [<value_1>, ..., <value_n>]
  EXPECT_THAT(listPP, testing::HasSubstr("Id1: [1, 2]"));
  
  std::stringstream ss2;
  pp.prettyPrint(std::make_shared<ContextObject>("Id2", list), ss2, true);
  std::string listFullPP = ss2.str();
  
  // Pretty print (full) on list objects:
  // <id>: [<value_1>, ..., <value_n>]
  // type: List
  EXPECT_THAT(listFullPP, testing::HasSubstr("Id2: [1, 2]"));
  EXPECT_THAT(listFullPP, testing::HasSubstr("type: List"));
  
  // Pretty print on long list objects
  std::vector<Interpreter::DataObject> listVals;
  for (int valIdx{0}; valIdx < 20; ++valIdx)
  {
    listVals.push_back(Interpreter::DataObject(valIdx));
  }
  Interpreter::DataObject longList;
  longList.setClassObject(BOSPtr(otools::createListObject(listVals)));
  
  std::stringstream ss3;
  pp.prettyPrint(std::make_shared<ContextObject>("Id3", longList), ss3, false);
  std::string longListPP = ss3.str();
  
  // Pretty print (not full) on long list objects:
  // <id>: [<value_1>, ..., <value_n>]
  EXPECT_THAT(longListPP, testing::HasSubstr("Id3: [0, 1, 2, ..., 17, 18, 19]"));
}//printListObjects

OPTILAB_TEST(MCPPCtxObject, printListOfListsObjects)
{
  // Test pretty print of list of lists objects
  using namespace Model;
  MCPPCtxObject pp;
  
  // Create a standard context object on a list base object
  Interpreter::DataObject subList1;
  subList1.setClassObject(BOSPtr(otools::createListObject(
  {Interpreter::DataObject(1),Interpreter::DataObject(2)})));
  
  Interpreter::DataObject subList2;
  subList2.setClassObject(BOSPtr(otools::createListObject(
  {Interpreter::DataObject(3), Interpreter::DataObject(4)})));
  
  Interpreter::DataObject list;
  list.setClassObject(BOSPtr(otools::createListObject({subList1, subList2})));
  
  std::stringstream ss1;
  pp.prettyPrint(std::make_shared<ContextObject>("Id1", list), ss1, false);
  std::string listPP = ss1.str();
  EXPECT_THAT(listPP, testing::HasSubstr("Id1: [[1, 2], [3, 4]]"));
  
  std::vector<Interpreter::DataObject> longList;
  for (int idx{0}; idx < 20; ++idx)
  {
    longList.push_back(subList1);
  }
  Interpreter::DataObject llist;
  llist.setClassObject(BOSPtr(otools::createListObject(longList)));
  
  std::stringstream ss2;
  pp.prettyPrint(std::make_shared<ContextObject>("Id2", llist), ss2, false);
  std::string llistPP = ss2.str();
  EXPECT_THAT(llistPP, testing::HasSubstr("Id2: \n"));
  EXPECT_THAT(llistPP, testing::HasSubstr("[[1, 2], [1, 2], [1, 2], "));
  EXPECT_THAT(llistPP, testing::HasSubstr("\n  ...,\n"));
  EXPECT_THAT(llistPP, testing::HasSubstr("[1, 2], [1, 2], [1, 2]]\n"));
  
  longList.clear();
  for (int idx{0}; idx < 20; ++idx)
  {
    longList.push_back(Interpreter::DataObject(idx));
  }
  Interpreter::DataObject longSubList;
  longSubList.setClassObject(BOSPtr(otools::createListObject(longList)));
  
  std::vector<Interpreter::DataObject> llongList;
  for (int idx{0}; idx < 20; ++idx)
  {
    llongList.push_back(longSubList);
  }
  Interpreter::DataObject lllist;
  lllist.setClassObject(BOSPtr(otools::createListObject(llongList)));
  
  std::stringstream ss3;
  pp.prettyPrint(std::make_shared<ContextObject>("Id3", lllist), ss3, false);
  std::string lllistPP = ss3.str();
  EXPECT_THAT(lllistPP, testing::HasSubstr("Id3: \n"));
  EXPECT_THAT(lllistPP, testing::HasSubstr("[[0, 1, 2, ..., 17, 18, 19],"));
  EXPECT_THAT(lllistPP, testing::HasSubstr("[0, 1, 2, ..., 17, 18, 19],"));
  EXPECT_THAT(lllistPP, testing::HasSubstr("\n  ...,\n"));
  EXPECT_THAT(lllistPP, testing::HasSubstr("[0, 1, 2, ..., 17, 18, 19]]"));
}//printListOfListsObjects

OPTILAB_TEST(MCPPCtxObject, printMatrixObjects)
{
  // Test pretty print of matrix objects
  using namespace Model;
  MCPPCtxObject pp;
  
  // Create a standard context object on a matrix base object.
  // @note vector of composite data objects: each composite data object is a row of the matrix
  Interpreter::DataObject matrix;
  Interpreter::DataObject row1;
  Interpreter::DataObject row2;
  row1.compose({Interpreter::DataObject(1), Interpreter::DataObject(2)});
  row2.compose({Interpreter::DataObject(3), Interpreter::DataObject(4)});
  matrix.setClassObject(BOSPtr(otools::createMatrixObject({row1, row2})));

  std::stringstream ss1;
  pp.prettyPrint(std::make_shared<ContextObject>("Id1", matrix), ss1, false);
  std::string matrixPP = ss1.str();
  EXPECT_THAT(matrixPP, testing::HasSubstr("Id1: \n"));
  EXPECT_THAT(matrixPP, testing::HasSubstr("[[1, 2],\n"));
  EXPECT_THAT(matrixPP, testing::HasSubstr("[3, 4]]\n"));
  
  // Create a 20 x 20 matrix
  Interpreter::DataObject lmatrix;
  Interpreter::DataObject lrow;
  
  std::vector<Interpreter::DataObject> llist;
  for (int idx{0}; idx < 20; ++idx)
  {
    llist.push_back(Interpreter::DataObject(idx));
  }
  lrow.compose(llist);
  
  std::vector<Interpreter::DataObject> rows;
  for (int idx{0}; idx < 20; ++idx)
  {
    rows.push_back(lrow);
  }
  lmatrix.setClassObject(BOSPtr(otools::createMatrixObject(rows)));
  
  std::stringstream ss2;
  pp.prettyPrint(std::make_shared<ContextObject>("Id2", lmatrix), ss2, false);
  std::string lmatrixPP = ss2.str();
  EXPECT_THAT(lmatrixPP, testing::HasSubstr("Id2: \n"));
  EXPECT_THAT(lmatrixPP, testing::HasSubstr("[[0, 1, 2, ..., 17, 18, 19],\n"));
  EXPECT_THAT(lmatrixPP, testing::HasSubstr("[0, 1, 2, ..., 17, 18, 19],\n"));
  EXPECT_THAT(lmatrixPP, testing::HasSubstr("\n  ...,\n"));
  EXPECT_THAT(lmatrixPP, testing::HasSubstr("[0, 1, 2, ..., 17, 18, 19]]\n"));
  
  // Test full pretty print on matrix
  std::stringstream ss3;
  pp.prettyPrint(std::make_shared<ContextObject>("Id3", lmatrix), ss3, true);
  std::string lFullmatrixPP = ss3.str();
  EXPECT_THAT(lFullmatrixPP, testing::HasSubstr("type: Matrix"));
  EXPECT_THAT(lFullmatrixPP, testing::HasSubstr("dims: [20, 20]"));
}//printMatrixObjects
