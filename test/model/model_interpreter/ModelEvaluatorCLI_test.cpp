
#include "utest_globals.hpp"

#include "SolverTestInc.hpp"

#include "ModelTestInc.hpp"
#include "CLIUtils.hpp"

#include <string>
#include <sstream>

#ifdef RUNTEST

OPTILAB_TEST(ModelEvaluatorCLI, createModelContext)
{
  using namespace Model;
  
  ModelEvaluatorCLITest mdlEval;
  auto ctx = mdlEval.createModelContext();
  
  // Model ctx shoudl contain default sub-trees
  auto ctxStr = modelContextToString(ctx);
  auto baseCtx = getBaseModelContext();
  
  ASSERT_EQ_STR(ctxStr, baseCtx);
}//createModelContext

OPTILAB_TEST(ModelEvaluatorCLI, createDefaultModelContext)
{
  using namespace Model;
  
  ModelEvaluatorCLITest mdlEval;
  auto ctx = mdlEval.createDefaultModelContext("ctxTest");
  
  // Model ctx shoudl contain default sub-trees and ctx name
  auto ctxStr = modelContextToString(ctx);
  auto baseCtx = getDefaultModelContext("ctxTest");

  ASSERT_EQ_STR(ctxStr, baseCtx);
}//createDefaultModelContext

OPTILAB_TEST(ModelEvaluatorCLI, getSymbolType)
{
  using namespace Model;
  
  ModelEvaluatorCLITest mdlEval;
  
  // Empty string should have undef type
  //auto undefSymb = mdlEval.getSymbolType("");
  //ASSERT_EQ(undefSymb, ModelEvaluator::SymbolType::ST_UNDEF);
  
  // Variable type declaration
  auto varMsg = CLI::Utils::cliStmtStringFromPrettyPrint("|1=variable|10=x|11=integer|14=expr|15=10");
  auto varSymb = mdlEval.getSymbolType(varMsg);
  ASSERT_EQ(varSymb, ModelEvaluator::SymbolType::ST_VARIABLE_DECL);
  
  // Constraint type declaration
  auto conMsg = CLI::Utils::cliStmtStringFromPrettyPrint("|1=constraint|10=expr|11=c|12=3");
  auto conSymb = mdlEval.getSymbolType(conMsg);
  ASSERT_EQ(conSymb, ModelEvaluator::SymbolType::ST_CONSTRAINT_DECL);
}//getSymbolType

OPTILAB_TEST(ModelEvaluatorCLI, getIntrepreterInstance)
{
  using namespace Model;
  
  ModelEvaluatorCLITest mdlEval;
  
  // CLI Interpreter for variables
  auto varInterp = mdlEval.getIntrepreterInstance(ModelEvaluator::SymbolType::ST_VARIABLE_DECL);
  ASSERT_EQ(varInterp->getInterpretedSymbolType(), ModelEvaluator::SymbolType::ST_VARIABLE_DECL);
  
  // CLI Interpreter for constraints
  auto conInterp = mdlEval.getIntrepreterInstance(ModelEvaluator::SymbolType::ST_CONSTRAINT_DECL);
  ASSERT_EQ(conInterp->getInterpretedSymbolType(), ModelEvaluator::SymbolType::ST_CONSTRAINT_DECL);
}//getIntrepreterInstance

OPTILAB_TEST(ModelEvaluatorCLI, setDefaultSearchContext)
{
  using namespace Model;
  
  ModelEvaluatorCLITest mdlEval;
  auto ctx = mdlEval.createModelContext();
  
  // Set default search context
  mdlEval.setDefaultSearchContext(ctx);
  auto ctxStr = modelContextToString(ctx);
  ASSERT_TRUE(ctxStr.find(R"("type": "dfs")") != std::string::npos);
  ASSERT_TRUE(ctxStr.find(R"("semantic")") != std::string::npos);
  ASSERT_TRUE(ctxStr.find(R"("variable_choice": "input_order")") != std::string::npos);
  ASSERT_TRUE(ctxStr.find(R"("value_choice": "indomain_min")") != std::string::npos);
}//setDefaultSearchContext

#endif
