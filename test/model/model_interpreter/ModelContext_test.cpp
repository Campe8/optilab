
#include "utest_globals.hpp"

#include "Context.hpp"

#include <sstream>

OPTILAB_TEST(Context, emptyContext)
{
  using namespace Model;
  
  Context ctx;
  ASSERT_TRUE(ctx.isEmpty());
  
  Context ctx2("ctx");
  ASSERT_TRUE(ctx2.isEmpty());
  
  Context::Path p = {"a"};
  ctx.addValue<std::string>(p, "val");
  ASSERT_FALSE(ctx.isEmpty());
}//emptyContext

OPTILAB_TEST(Context, contextId)
{
  using namespace Model;
  
  Context ctx;
  ASSERT_TRUE(ctx.getContextId().empty());
  
  Context ctx2("ctx");
  ASSERT_EQ(ctx2.getContextId(), "ctx");
}//contextId

OPTILAB_TEST(Context, addGetValue)
{
  using namespace Model;
  
  Context ctx("ctx");
  
  Context::Path p = {"a"};
  ctx.addValue<std::string>(p, "val");
  auto a_elem = ctx.getValue<std::string>(p);
  ASSERT_EQ(a_elem, "val");
  
  Context::Path p2 = {"b"};
  ctx.addValue<int>(p2, 1);
  auto b_elem = ctx.getValue<int>(p2);
  ASSERT_EQ(b_elem, 1);
  
  // Nested path
  Context::Path p3 = {"c", "d"};
  ctx.addValue<int>(p3, 10);
  auto c_elem = ctx.getValue<int>(p3);
  ASSERT_EQ(c_elem, 10);
  
  ctx.addValue<double>(p, 1.2);
  auto a2_elem = ctx.getValue<double>(p);
  ASSERT_EQ(a2_elem, 1.2);
  
  // Same path as context name
  Context ctx2("a");
  Context::Path p4 = {"a"};
  ctx2.addValue<std::string>(p4, "val");
  ASSERT_EQ(ctx2.getValue<std::string>(p4), "val");
}//addGetValue

OPTILAB_TEST(Context, addGetSubContext)
{
  using namespace Model;
  
  Context ctx("ctx");
  Context ctx1("ctx1");
  
  // Context ctx1:
  //  {
  //    "ctx1": {
  //      "b": "1"
  //    }
  //  }
  Context::Path pb = {"b"};
  ctx1.addValue<int>(pb, 1);
  
  // Add sub-context sCtx under path "a":
  //  {
  //    "ctx": {
  //      "a": {
  //        "ctx1": {
  //          "b": "1"
  //        }
  //      }
  //    }
  //  }
  Context::Path p = {"a"};
  ctx.addSubContext(p, ctx1);
  
  // At address "p" should get ctx1:
  auto c = ctx.getSubContext(p);
  
  // ctx1 should be contex "a":
  // {
  //   "a": {
  //     "ctx1": {
  //       "b": "1"
  //     }
  //   }
  // }
  ASSERT_FALSE(c.isEmpty());
  ASSERT_EQ(c.getValue<int>({"ctx1", "b"}), 1);
}//addSubContext

OPTILAB_TEST(Context, addGetSubContext2)
{
  using namespace Model;
  Context ctx("ctx");
  
  // Add sub-context: ctx2
  Context ctx2("ctx2");
  ctx.addSubContext({""}, ctx2);
  
  // ctx should be
  // {
  //   "ctx": {
  //     "ctx2": ""
  //   }
  // }
  auto c = ctx.getSubContext({"ctx2"});
  ASSERT_TRUE(c.isEmpty());
  
  // Add sub-context: ctx3
  Context ctx3("ctx3");
  ctx.addSubContext({"ctx2"}, ctx3);
  
  // ctx should be
  // {
  //   "ctx": {
  //     "ctx2": {
  //        "ctx3": ""
  //      }
  //   }
  // }
  c = ctx.getSubContext({"ctx2"});
  ASSERT_FALSE(c.isEmpty());
  
  // c should be
  // {
  //   "ctx2": {
  //     "ctx3": ""
  //  }
  // }
  // and sub-context list for "ctx2"
  // should contain
  // {
  //   "ctx3": ""
  // }
  auto v = c.getSubContextList({"ctx2"});
  ASSERT_EQ(v.size(), 1);
  ASSERT_TRUE(v[0].isEmpty());
  
  auto v2 = ctx.getSubContextList({"ctx"});
  ASSERT_EQ(v2.size(), 1);
  ASSERT_FALSE(v2[0].isEmpty());
}//addGetSubContext2

OPTILAB_TEST(Context, expandSubContext)
{
  using namespace Model;
  
  Context ctx("ctx");
  
  Context sctx1;
  sctx1.addValue<std::string>({ "c1" }, "v1");
  
  Context sctx2;
  sctx2.addValue<std::string>({ "c2" }, "v2");
  
  // Add sub-sub-context to ctx at path "subCtx"
  Context ssctx("subsubCtx");
  ctx.addSubContext({ "subCtx" }, ssctx);
  
  // Expand subsubCtx, the path is subCtx (where subsubCtx is) and then
  // the name of the context which is "subsubCtx"
  ctx.expandSubContext({ "subCtx", "subsubCtx" }, sctx1);
  ctx.expandSubContext({ "subCtx", "subsubCtx" }, sctx2);
  
  // Get the sub-context:
  // {
  //  "subsubCtx": [
  //                {
  //                "c1": "v1"
  //                },
  //                {
  //                "c2": "v2"
  //                }
  //                ]
  // }
  auto c = ctx.getSubContext({"subCtx", "subsubCtx"});
  ASSERT_FALSE(c.isEmpty());
  
  auto v = c.getSubContextList({"subsubCtx"});
  ASSERT_EQ(v.size(), 2);
  ASSERT_EQ(v[0].getValue<std::string>({"c1"}), "v1");
  ASSERT_EQ(v[1].getValue<std::string>({"c2"}), "v2");
}//expandSubContext

OPTILAB_TEST(Context, addValueList)
{
  using namespace Model;
  
  Context ctx;
  ctx.addValueList<int>({ "values" }, 1);
  ctx.addValueList<int>({ "values" }, 2);
  
  auto valsEmpty = ctx.getValueList<int>({"non-valid-path"});
  ASSERT_TRUE(valsEmpty.empty());
  
  auto vals = ctx.getValueList<int>({"values"});
  ASSERT_EQ(vals.size(), 2);
  ASSERT_EQ(vals[0], 1);
  ASSERT_EQ(vals[1], 2);
}//addValueList

OPTILAB_TEST(Context, serializeContext)
{
  using namespace Model;
  
  Context ctx("ctx");
  Context::Path p = {"a"};
  ctx.addValue<std::string>(p, "val");
  
  std::stringstream ss;
  ctx.serializeContext(ss);
  
  auto str = ss.str();
  std::string strCmp = "{\n    \"ctx\": {\n        \"a\": \"val\"\n    }\n}\n";

  ASSERT_EQ_STR(str, strCmp);
}//serializeContext

OPTILAB_TEST(Context, complicatedCtx)
{
  using namespace Model;
  
  // Create main context
  Context ctx("ctx");
  ctx.addValue<std::string>({"name"}, "main");
  
  // Create unnamed context and add a single value
  Context ssctx;
  ssctx.addValue<std::string>({ "id" }, "ssctx");
  
  // Add sequence values to unnamed context
  // @note first create a context to contain them, i.e., vals
  Context vals;
  vals.addValueList<int>({ "values" }, 1);
  vals.addValueList<int>({ "values" }, 2);
  ssctx.addSubContext({ "vals" }, vals);
  
  // Create another unnamed context and add a value
  Context ssctx2;
  ssctx2.addValue<std::string>({ "id" }, "ssctx2");
  
  // Create a named context sctx and add it to main context
  Context sctx("sctx");
  ctx.addSubContext({ "subctx" }, sctx);
  
  // Expand main context with a sequence of 2 unnamed context
  // inside subctx
  ctx.expandSubContext({ "subctx", "sctx" }, ssctx);
  ctx.expandSubContext({ "subctx", "sctx" }, ssctx2);
  
  // From ctx retrieve sctx which should contain ssctx and ssctx2
  auto sCtx = ctx.getSubContext({"subctx", "sctx"});
  
  // Get sub-context
  auto v = sCtx.getSubContextList({"sctx"});
  
  // The vector should contain the two unnamed context
  ASSERT_EQ(v.size(), 2);
  
  auto& c1 = v[0];
  auto& c2 = v[1];
  ASSERT_EQ(c1.getValue<std::string>({"id"}), "ssctx");
  ASSERT_EQ(c2.getValue<std::string>({"id"}), "ssctx2");
  
  auto intVals = c1.getValueList<int>({"vals", "values"});
  ASSERT_EQ(intVals.size(), 2);
  ASSERT_EQ(intVals[0], 1);
  ASSERT_EQ(intVals[1], 2);
}//complicatedCtx

OPTILAB_TEST(Context, deleteSubContext)
{
  using namespace Model;
  Context ctx("ctx");
  
  // Add sub-context: ctx2
  Context ctx2("ctx2");
  ctx.addSubContext({""}, ctx2);
  
  // Add sub-context: ctx3
  Context ctx3("ctx3");
  ctx.addSubContext({"ctx2"}, ctx3);
  
  auto v = ctx.getSubContextList({"ctx"});
  // ctx should be
  // {
  //   "ctx": {
  //     "ctx2": {
  //        "ctx3": ""
  //      }
  //   }
  // }
  
  ctx.deleteSubContext({"ctx2"});
  ASSERT_TRUE(ctx.isEmpty());
}//deleteSubContext

OPTILAB_TEST(Context, deleteSubContext2)
{
  using namespace Model;
  
  Context ctx("ctx");
  
  Context sctx1;
  sctx1.addValue<std::string>({ "c1" }, "v1");
  sctx1.addValue<std::string>({ "c12" }, "v12");
  
  Context sctx2;
  sctx2.addValue<std::string>({ "c1" }, "v2");
  
  Context ssctx("subsubCtx");
  ctx.addSubContext({ "subCtx" }, ssctx);
  ctx.expandSubContext({ "subCtx", "subsubCtx" }, sctx1);
  ctx.expandSubContext({ "subCtx", "subsubCtx" }, sctx2);
  
  // The sub-context is now as follows:
  // {
  //  "subsubCtx": [
  //                {
  //                "c1": "v1"
  //                "c12": "v12"
  //                },
  //                {
  //                "c2": "v2"
  //                }
  //                ]
  // }
  // at path "subCtx"
  ctx.deleteSubContext<std::string>({"subCtx", "subsubCtx"}, "c1", "v2");
  auto v = ctx.getSubContextList({"subCtx", "subsubCtx"});
  ASSERT_EQ(v.size(), 1);
  ASSERT_EQ(v[0].getValue<std::string>({"c1"}), "v1");
}//deleteSubContext2

OPTILAB_TEST(Context, getValue2)
{
  using namespace Model;
  
  Context ctx("ctx");
  
  Context sctx1;
  sctx1.addValue<std::string>({ "c1" }, "v1");
  sctx1.addValue<std::string>({ "c12" }, "v12");
  
  Context sctx2;
  sctx2.addValue<std::string>({ "c1" }, "v2");
  
  Context ssctx("subsubCtx");
  ctx.addSubContext({ "subCtx" }, ssctx);
  ctx.expandSubContext({ "subCtx", "subsubCtx" }, sctx1);
  ctx.expandSubContext({ "subCtx", "subsubCtx" }, sctx2);
  
  // The sub-context is now as follows:
  // {
  //  "subsubCtx": [
  //                {
  //                "c1": "v1"
  //                "c12": "v12"
  //                },
  //                {
  //                "c2": "v2"
  //                }
  //                ]
  // }
  // at path "subCtx"
  auto c = ctx.getValue<std::string>({"subCtx", "subsubCtx"}, "c1", "v1");
  ASSERT_EQ(c.getValue<std::string>({"c1"}), "v1");
  ASSERT_EQ(c.getValue<std::string>({"c12"}), "v12");
}//getValue2

OPTILAB_TEST(Context, copyConstructors)
{
  using namespace Model;
  
  Context ctx("ctx");
  ctx.addValue<std::string>({"val"}, "string");
  
  Context ctx1("sc");
  ctx1.addValue<int>({"val"}, 1);
  ctx.addSubContext({"b"}, ctx1);
  
  // Copy constructor
  Context ctxcpy1(ctx);
  ASSERT_EQ(ctxcpy1.getContextId(), "ctx");
  ASSERT_EQ(ctxcpy1.getValue<std::string>({"val"}), "string");
  
  auto c = ctxcpy1.getSubContext({"b"});
  ASSERT_FALSE(c.isEmpty());
  ASSERT_EQ(c.getValue<int>({"sc", "val"}), 1);
  
  // Assignment operator
  Context ctxcpy2;
  ctxcpy2 = ctx;
  
  ASSERT_EQ(ctxcpy2.getContextId(), "ctx");
  ASSERT_EQ(ctxcpy2.getValue<std::string>({"val"}), "string");
  
  auto c2 = ctxcpy2.getSubContext({"b"});
  ASSERT_FALSE(c2.isEmpty());
  ASSERT_EQ(c2.getValue<int>({"sc", "val"}), 1);
}//copyConstructors
