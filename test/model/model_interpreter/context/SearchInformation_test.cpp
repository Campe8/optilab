
#include "utest_globals.hpp"

#include "SearchInformation.hpp"

#include <sstream>
#include <stdexcept>  // for std::runtime_error

namespace Model {
  class SearchInformationTest : public SearchInformation
  {
  public:
    SearchInformationTest(SearchInformationType aType)
    : SearchInformation(aType)
    {
    }
    
    void readSearchOptionsObject(Interpreter::BaseObject*){}
  };
}// end namespace Model

OPTILAB_TEST(SearchInformation, interface)
{
  // Test SearchInformation interface
  using namespace Model;
  SearchInformationTest sit(SearchInformation::SearchInformationType::SI_DFS);
  
  ASSERT_TRUE(sit.getMap());
  
  // Default the map has the timeput entry
  EXPECT_EQ(1, (sit.getMap())->size());
  
  // Search information type
  ASSERT_EQ(SearchInformation::SearchInformationType::SI_DFS, sit.getSearchInfoType());
  
  // Add member and get map
  sit.addMember("test", 10);
  ASSERT_EQ(2, (sit.getMap())->size());
  EXPECT_EQ(10, (sit.getMap())->lookupValue<int>("test"));
}//interface

OPTILAB_TEST(SearchInformation, addAndUpdateMembers)
{
  // Test adding vs updating members in the internal map
  using namespace Model;
  SearchInformationTest sit(SearchInformation::SearchInformationType::SI_DFS);
  
  // Adding twice the same key should throw an error
  sit.addMember("test", 10);
  EXPECT_THROW(sit.addMember("test", 10), std::runtime_error);
  
  // Updating should not throw
  EXPECT_NO_THROW(sit.updateMember("test", 50));
  EXPECT_EQ(50, (sit.getMap())->lookupValue<int>("test"));
}//addAndUpdateMembers
