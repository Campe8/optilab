
#include "utest_globals.hpp"

#include "SearchContextObject.hpp"
#include "SearchOptionsTools.hpp"
#include "ObjectHelper.hpp"

#include "DFSInformation.hpp"
#include "GeneticInformation.hpp"

#include <sstream>

OPTILAB_TEST(SearchContextObject, defaultSearchInformation)
{
  // Test that a default search options object is created if not provided in input
  using namespace Model;
  using namespace Interpreter;
  using namespace Interpreter::ObjectTools;
  
  // Static function
  ASSERT_TRUE(SearchContextObject::getSearchContextObjectIdPrefix() == std::string("_s"));
  
  // Create variable that represents the scope of the search
  std::shared_ptr<BaseObject> var(createObjectVariable("var", DOM_SINGLETON, { DataObject(5) }));
  Interpreter::DataObject dobjVar("var");
  
  // Create search scope
  Interpreter::DataObject srcScope;
  srcScope.setClassObject(createListObjectSPtr({dobjVar}));
  
  // Create search object on given scope
  auto srcObject  = std::shared_ptr<BaseObject>(createObjectSearch(srcScope));
  
  Interpreter::DataObject dobj;
  dobj.setClassObject(srcObject);
  SearchContextObject sco("id", dobj);
  
  // Get the search information
  auto srcInfo = sco.getSearchStrategyInformation();
  ASSERT_TRUE(srcInfo);
  
  // Check default search information values
  ASSERT_EQ(SearchInformation::SearchInformationType::SI_DFS, srcInfo->getSearchInfoType());
  
  auto map = srcInfo->getMap();
  ASSERT_TRUE(map);
  ASSERT_EQ(DFSInformation::getDefaultVariableChoice(),
            map->lookupValue<std::string>(DFSInformation::VariableSelectionKey));
  ASSERT_EQ(DFSInformation::getDefaultValueChoice(),
            map->lookupValue<std::string>(DFSInformation::ValueSelectionKey));
}//setDefaultSearchInformation

OPTILAB_TEST(SearchContextObject, searchInformationDFS)
{
  // Test search information DFS object set from the data object
  using namespace Model;
  using namespace Interpreter;
  using namespace Interpreter::ObjectTools;
  using namespace Interpreter::SearchOptionsTools;
  
  // Create variable that represents the scope of the search
  std::shared_ptr<BaseObject> var(createObjectVariable("var", DOM_SINGLETON, { DataObject(5) }));
  Interpreter::DataObject dobjVar("var");

  // Create search scope
  Interpreter::DataObject srcScope;
  srcScope.setClassObject(createListObjectSPtr({dobjVar}));
  
  // Create search options
  Interpreter::DataObject srcOptions;
  srcOptions.setClassObject(std::shared_ptr<BaseObject>(getDFSSearchOptionsObject()));
  
  // Create search object on given scope and options
  auto srcObject  = std::shared_ptr<BaseObject>(createObjectSearch(srcScope, srcOptions));
  
  Interpreter::DataObject dobj;
  dobj.setClassObject(srcObject);
  SearchContextObject sco("id", dobj);
  
  // Get the search information
  auto srcInfo = sco.getSearchStrategyInformation();
  ASSERT_TRUE(srcInfo);
  
  ASSERT_EQ(SearchInformation::SearchInformationType::SI_DFS, srcInfo->getSearchInfoType());
  
  auto map = srcInfo->getMap();
  ASSERT_TRUE(map);
  ASSERT_EQ(Model::DFSInformation::getDefaultVariableChoice(),
            map->lookupValue<std::string>(DFSInformation::VariableSelectionKey));
  ASSERT_EQ(Model::DFSInformation::getDefaultValueChoice(),
            map->lookupValue<std::string>(DFSInformation::ValueSelectionKey));
}//searchInformationDFS

OPTILAB_TEST(SearchContextObject, searchInformationGenetic)
{
  // Test search information DFS object set from the data object
  using namespace Model;
  using namespace Interpreter;
  using namespace Interpreter::ObjectTools;
  using namespace Interpreter::SearchOptionsTools;
  
  // Create variable that represents the scope of the search
  std::shared_ptr<BaseObject> var(createObjectVariable("var", DOM_SINGLETON, { DataObject(5) }));
  Interpreter::DataObject dobjVar("var");
  
  // Create search scope
  Interpreter::DataObject srcScope;
  srcScope.setClassObject(createListObjectSPtr({dobjVar}));
  
  // Create search options
  Interpreter::DataObject srcOptions;
  srcOptions.setClassObject(std::shared_ptr<BaseObject>(getGeneticSearchOptionsObject()));
  
  // Create search object on given scope and options
  auto srcObject  = std::shared_ptr<BaseObject>(createObjectSearch(srcScope, srcOptions));
  
  Interpreter::DataObject dobj;
  dobj.setClassObject(srcObject);
  SearchContextObject sco("id", dobj);
  
  // Get the search information
  auto srcInfo = sco.getSearchStrategyInformation();
  ASSERT_TRUE(srcInfo);
  
  ASSERT_EQ(SearchInformation::SearchInformationType::SI_GENETIC, srcInfo->getSearchInfoType());
  
  auto map = srcInfo->getMap();
  ASSERT_TRUE(map);
  EXPECT_EQ(-1, map->lookupValue<int>(SearchInformation::TimeoutKey));
  EXPECT_EQ(0, map->lookupValue<int>(CBLSInformation::RandomSeedKey));
  EXPECT_TRUE(map->lookupValue<bool>(CBLSInformation::WarmStartKey));
  EXPECT_EQ(-1, map->lookupValue<int>(CBLSInformation::NeighborhoodSizeKey));
  EXPECT_EQ(1, map->lookupValue<int>(CBLSInformation::NumNeighborhoodsKey));
  EXPECT_FALSE(map->lookupValue<bool>(CBLSInformation::AggressiveCBLSKey));
  EXPECT_EQ(100, map->lookupValue<std::size_t>(GeneticInformation::PopulationSizeKey));
  EXPECT_EQ(500, map->lookupValue<std::size_t>(GeneticInformation::NumGenerationsKey));
  EXPECT_EQ(70, map->lookupValue<std::size_t>(GeneticInformation::MutationChanceKey));
  EXPECT_EQ(1, map->lookupValue<std::size_t>(GeneticInformation::MutationSizeKey));
}//searchInformationGenetic
