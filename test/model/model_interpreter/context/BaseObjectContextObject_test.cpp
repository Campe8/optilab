
#include "utest_globals.hpp"

#include "BaseObjectContextObject.hpp"
#include "SearchOptionsTools.hpp"

#include <sstream>

OPTILAB_TEST(BaseObjectContextObject, constructor)
{
  // Test constructor for BaseObjectContextObject
  using namespace Model;
  using namespace Interpreter::SearchOptionsTools;
  
  // Create an object
  auto obj = std::shared_ptr<Interpreter::BaseObject>(getDFSSearchOptionsObject());
  
  Interpreter::DataObject dobj;
  dobj.setClassObject(obj);
  
  BaseObjectContextObject boco("id", dobj);
  ASSERT_EQ(ContextObject::Type::CTX_OBJ_DBO, boco.getType());
  ASSERT_EQ(std::string("id"), boco.getID());
  ASSERT_TRUE(boco.payload().isClassObject());
}//constructor
