
#include "utest_globals.hpp"

#include "ContextObject.hpp"

#include "ObjectTools.hpp"
#include "ObjectHelper.hpp"

namespace otools = Interpreter::ObjectTools;

#define BOSPtr(x) std::shared_ptr<Interpreter::BaseObject>(x)

OPTILAB_TEST(ContextObject, interface)
{
  // Test ContextObject interface
  using namespace Model;
  
  // Create a standard context object
  ContextObject ctxObj("ID", Interpreter::DataObject(1));
  EXPECT_EQ(ContextObject::Type::CTX_OBJ_STD, ctxObj.getType());
  EXPECT_EQ("ID", ctxObj.getID());
  EXPECT_TRUE(Interpreter::isPrimitiveType(ctxObj.payload()));
  EXPECT_EQ(1, ctxObj.payload().getDataValue<int>());
  
  // Check reference to payload
  ctxObj.payload() = Interpreter::DataObject(100);
  EXPECT_EQ(100, ctxObj.payload().getDataValue<int>());
}//interface

OPTILAB_TEST(ContextObject, contextObjectStdClass)
{
  // Test ContextObject on standard class objects
  using namespace Model;
  
  // Create a standard context object
  Interpreter::DataObject scalar;
  scalar.setClassObject(BOSPtr(otools::createScalarObject(Interpreter::DataObject(1))));
  
  Interpreter::DataObject list;
  list.setClassObject(BOSPtr(otools::createListObject({Interpreter::DataObject(1)})));
  
  Interpreter::DataObject matrix;
  matrix.setClassObject(BOSPtr(otools::createMatrixObject({Interpreter::DataObject(1)})));
  
  ContextObject ctxObjScalar("ID", scalar);
  ContextObject ctxObjList("ID", list);
  ContextObject ctxObjMatrix("ID", matrix);
  EXPECT_EQ(ContextObject::Type::CTX_OBJ_STD, ctxObjScalar.getType());
  EXPECT_EQ(ContextObject::Type::CTX_OBJ_STD, ctxObjList.getType());
  EXPECT_EQ(ContextObject::Type::CTX_OBJ_STD, ctxObjMatrix.getType());
}//contextObjectStdClass

OPTILAB_TEST(ContextObject, contextObjectBaseObject)
{
  // Test ContextObject on base objects
  using namespace Model;
  
  // Create a standard context object
  Interpreter::DataObject baseObject;
  Interpreter::DataObject list;
  list.setClassObject(BOSPtr(otools::createListObject({Interpreter::DataObject(1)})));
  baseObject.setClassObject(BOSPtr(otools::createObjectSearch(list)));
  ContextObject ctxObj("ID", baseObject);
  EXPECT_EQ(ContextObject::Type::CTX_OBJ_DBO, ctxObj.getType());
}//contextObjectBaseObject

