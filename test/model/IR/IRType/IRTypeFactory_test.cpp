#include "utest_globals.hpp"

#include "IRTypeFactory.hpp"

OPTILAB_TEST(IRTypeFactoryTest, typeVoid)
{
  using namespace IR;
  
  IRTypeFactory factory;
  
  IRTypeVoidSPtr typeVoid1 = factory.typeVoid();
  IRTypeVoidSPtr typeVoid2 = factory.typeVoid();
  
  ASSERT_TRUE(IRTypeVoid::isa(typeVoid1.get()));
  
  ASSERT_EQ(typeVoid1, typeVoid2);
}// typeVoid

OPTILAB_TEST(IRTypeFactoryTest, typeBoolean)
{
  using namespace IR;
  
  IRTypeFactory factory;
  
  IRTypeBooleanSPtr typeBool1 = factory.typeBoolean();
  IRTypeBooleanSPtr typeBool2 = factory.typeBoolean();
  
  ASSERT_TRUE(IRTypeBoolean::isa(typeBool1.get()));
  
  ASSERT_EQ(typeBool1, typeBool2);
}// typeBoolean

OPTILAB_TEST(IRTypeFactoryTest, typeInteger64)
{
  using namespace IR;
  
  IRTypeFactory factory;
  
  IRTypeIntegerSPtr typeInt1 = factory.typeInteger64();
  IRTypeIntegerSPtr typeInt2 = factory.typeInteger64();
  
  ASSERT_TRUE(IRTypeInteger::isa(typeInt1.get()));
  
  ASSERT_EQ(typeInt1, typeInt2);
}// typeInteger64

OPTILAB_TEST(IRTypeFactoryTest, typeDouble)
{
  using namespace IR;
  
  IRTypeFactory factory;
  
  IRTypeDoubleSPtr typeDouble1 = factory.typeDouble();
  IRTypeDoubleSPtr typeDouble2 = factory.typeDouble();
  
  ASSERT_TRUE(IRTypeDouble::isa(typeDouble1.get()));
  
  ASSERT_EQ(typeDouble1, typeDouble2);
}// typeDouble

OPTILAB_TEST(IRTypeFactoryTest, typeFunction)
{
  using namespace IR;
  
  IRTypeFactory factory;
  
  IRTypeBooleanSPtr typeBool = factory.typeBoolean();
  IRTypeDoubleSPtr typeDouble = factory.typeDouble();
  std::vector<IRTypeSPtr> vector { typeDouble };
  
  // bool(double)
  IRTypeFunctionSPtr typeFunction1 = factory.typeFunction(typeBool, vector);
  IRTypeFunctionSPtr typeFunction2 = factory.typeFunction(typeBool, vector);
  
  ASSERT_TRUE(IRTypeFunction::isa(typeFunction1.get()));
  ASSERT_TRUE(typeFunction1->isEqual(typeFunction2.get()));
  
  // redundant check
  std::stringstream ss;
  typeFunction1->prettyPrint(ss);
  ASSERT_EQ(ss.str(), "bool(double)");
}// typeFunction

OPTILAB_TEST(IRTypeFactoryTest, typeFunctionConstraint)
{
  using namespace IR;
  
  IRTypeFactory factory;
  
  IRTypeBooleanSPtr typeBool = factory.typeBoolean();
  IRTypeDoubleSPtr typeDouble = factory.typeDouble();
  std::vector<IRTypeSPtr> vecEmpty { };
  std::vector<IRTypeSPtr> vec1 { typeBool, typeDouble };
  std::vector<IRTypeSPtr> vec2 { typeDouble, typeDouble };
  
  
  // void() void()
  IRTypeFunctionSPtr typeFunctionCon1 = factory.typeFunctionConstraint(vecEmpty);
  IRTypeFunctionSPtr typeFunctionCon2 = factory.typeFunctionConstraint(vecEmpty);
  
  ASSERT_TRUE(IRTypeFunction::isa(typeFunctionCon1.get()));
  ASSERT_TRUE(typeFunctionCon1->isEqual(typeFunctionCon2.get()));
  ASSERT_EQ(typeFunctionCon1, typeFunctionCon2);
  
  
  // void(boolean,double) void(boolean,double)
  typeFunctionCon1 = factory.typeFunctionConstraint(vec1);
  typeFunctionCon2 = factory.typeFunctionConstraint(vec1);
  
  ASSERT_TRUE(IRTypeFunction::isa(typeFunctionCon1.get()));
  ASSERT_TRUE(typeFunctionCon1->isEqual(typeFunctionCon2.get()));
  ASSERT_EQ(typeFunctionCon1, typeFunctionCon2);
  
  // redundant check
  std::stringstream ss;
  typeFunctionCon1->prettyPrint(ss);
  ASSERT_EQ(ss.str(), "void(bool, double)");
  
  
  // void(boolean,double) void(double,double)
  typeFunctionCon1 = factory.typeFunctionConstraint(vec1);
  typeFunctionCon2 = factory.typeFunctionConstraint(vec2);
  
  ASSERT_TRUE(IRTypeFunction::isa(typeFunctionCon1.get()));
  ASSERT_FALSE(typeFunctionCon1->isEqual(typeFunctionCon2.get()));
  ASSERT_NE(typeFunctionCon1, typeFunctionCon2);
}// typeFunctionConstraint

OPTILAB_TEST(IRTypeFactoryTest, typeStruct)
{
  using namespace IR;
  
  IRTypeFactory factory;
  
  IRTypeBooleanSPtr typeBool   = factory.typeBoolean();
  IRTypeDoubleSPtr  typeDouble = factory.typeDouble();
  std::vector<IRTypeStruct::IRTypeSPtr> types { typeBool, typeDouble };
  
  std::string typeName{"Struct"};
  auto typeStruct = factory.typeStruct(typeName, types);
  ASSERT_TRUE(IRTypeStruct::isa(typeStruct.get()));
  ASSERT_EQ(typeName, typeStruct->getName());
  ASSERT_EQ(2, typeStruct->getNumElements());
  
  // Registered types
  ASSERT_TRUE(typeBool->isEqual(typeStruct->getElement(0).get()));
  ASSERT_TRUE(typeDouble->isEqual(typeStruct->getElement(1).get()));
}// typeStruct
