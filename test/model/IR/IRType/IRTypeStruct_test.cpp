#include "utest_globals.hpp"

#include "IRTypeStruct.hpp"
#include "IRTypeFactory.hpp"

namespace IR
{
  class IRTypeStructTest : public IRTypeStruct {
  public:
    IRTypeStructTest(std::string aName)
    : IRTypeStruct(aName)
    {
    }
  };
}// end namespace IR

OPTILAB_TEST(IRTypeStruct, getTypeID)
{
  // Test that the ID of the type is correct
  using namespace IR;
  
  IRTypeStructTest ts("test");
  ASSERT_EQ(ts.getTypeID(), IR_TYPE_STRUCT);
}//getTypeID

OPTILAB_TEST(IRTypeStruct, staticMethods)
{
  // Test isa and cast methods
  using namespace IR;
  
  IRTypeStructTest ts("test");
  ASSERT_TRUE(IRTypeStruct::isa(&ts));
  
  IRType* tts = &ts;
  ASSERT_TRUE(IRTypeStruct::cast(tts));
  
  const IRType* ctts = &ts;
  ASSERT_TRUE(IRTypeStruct::cast(ctts));
  
  IRTypeFactory factory;
  auto dt = factory.typeDouble();
  ASSERT_FALSE(IRTypeStruct::isa(dt.get()));
  
  auto pdt = dt.get();
  ASSERT_FALSE(IRTypeStruct::cast(pdt));
  
  const IRType* cpdt= dt.get();
  ASSERT_FALSE(IRTypeStruct::cast(cpdt));
}//getTypeID

OPTILAB_TEST(IRTypeStruct, getName)
{
  // Test name of struct
  using namespace IR;
  std::string name{"test"};
  IRTypeStructTest ts(name);
  
  ASSERT_EQ(name, ts.getName());
}//getName

OPTILAB_TEST(IRTypeStruct, isEqual)
{
  // Test isEqual method
  using namespace IR;
  
  IRTypeFactory factory;
  auto tDbl = factory.typeDouble();
  auto tInt = factory.typeInteger64();
  std::vector<IRTypeSPtr> types { tDbl, tInt };
  
  std::string name{"test"};
  IRTypeStructTest ts(name);
  
  // Type different than struct should not be equal
  ASSERT_FALSE(ts.isEqual(tDbl.get()));
  
  // Type struct with different name should not be equal
  IRTypeStructTest ts2("test2");
  ASSERT_FALSE(ts.isEqual(&ts2));
  
  // Type struct with different number of elements
  // and same name should not be equal
  ts.setBody(types);
  IRTypeStructTest ts3(name);
  ASSERT_FALSE(ts.isEqual(&ts3));
  
  // Type struct with same name, same number of elements
  // but different element types should not be equal
  IRTypeStructTest ts4(name);
  std::vector<IRTypeSPtr> types4 { tDbl, tDbl };
  ASSERT_FALSE(ts.isEqual(&ts4));
  
  // Pointer comparison
  ASSERT_TRUE(ts.isEqual(&ts));
  
  // Type struct with same name, same elements
  // should be equal
  IRTypeStructTest ts5(name);
  ts5.setBody(types);
  ASSERT_TRUE(ts.isEqual(&ts5));
}//isEqual

OPTILAB_TEST(IRTypeStruct, getElements)
{
  // Test getNumElements and getElement methods
  using namespace IR;
  
  IRTypeFactory factory;
  auto tDbl = factory.typeDouble();
  auto tInt = factory.typeInteger64();
  std::vector<IRTypeSPtr> types { tDbl, tInt };
  
  IRTypeStructTest ts("test");
  ASSERT_EQ(0, ts.getNumElements());
  
  ts.setBody(types);
  ASSERT_EQ(2, ts.getNumElements());
  
  ASSERT_TRUE(tDbl->isEqual(ts.getElement(0).get()));
  ASSERT_TRUE(tInt->isEqual(ts.getElement(1).get()));
}//getElements

OPTILAB_TEST(IRTypeStruct, prettyPrint)
{
  // Test pretty print function
  using namespace IR;
  
  IRTypeFactory factory;
  auto tDbl = factory.typeDouble();
  auto tInt = factory.typeInteger64();
  std::vector<IRTypeSPtr> types { tDbl, tInt };
  
  IRTypeStructTest ts("test");
  ts.setBody(types);
  
  std::stringstream ss;
  ts.prettyPrint(ss);
  auto ppString = ss.str();
  auto ppDesired{"test{double;\nint64\n}"};
  ASSERT_EQ(ppDesired, ppString);
}//prettyPrint
