#include "utest_globals.hpp"

#include "IRTypeBoolean.hpp"

namespace IR
{
  class IRTypeBooleanTest : public IRTypeBoolean {
  public:
    IRTypeBooleanTest()
    : IRTypeBoolean()
    {
    }
  };
}

OPTILAB_TEST(IRTypeBoolean, getTypeID)
{
  using namespace IR;
  
  IRTypeBooleanTest typeBoolean;
  ASSERT_EQ(typeBoolean.getTypeID(), IR_TYPE_BOOLEAN);
}//getTypeID

OPTILAB_TEST(IRTypeBoolean, numContainedTypes)
{
  using namespace IR;
  
  IRTypeBooleanTest typeBoolean;
  ASSERT_EQ(typeBoolean.numContainedTypes(), 0);
}//numContainedTypes

OPTILAB_TEST(IRTypeBoolean, isEqual)
{
  using namespace IR;
  
  IRTypeBooleanTest typeBoolean1;
  IRTypeBooleanTest typeBoolean2;
  ASSERT_TRUE(typeBoolean1.isEqual(&typeBoolean2));
  ASSERT_TRUE(typeBoolean2.isEqual(&typeBoolean1));
}//isEqual

OPTILAB_TEST(IRTypeBoolean, isa)
{
  using namespace IR;
  
  IRTypeBooleanTest typeBoolean;
  ASSERT_TRUE(IRTypeBoolean::isa(&typeBoolean));
}//isa

OPTILAB_TEST(IRTypeBoolean, cast)
{
  using namespace IR;
  
  std::unique_ptr<IRType> pIRTypePointer1(new IRTypeBooleanTest);
  ASSERT_TRUE(IRTypeBoolean::cast(pIRTypePointer1.get()));
  
  std::unique_ptr<const IRType> pIRTypePointer2(new IRTypeBooleanTest);
  ASSERT_TRUE(IRTypeBoolean::cast(pIRTypePointer2.get()));
}//cast

OPTILAB_TEST(IRTypeBoolean, prettyPrint)
{
  using namespace IR;
  
  IRTypeBooleanTest typeBoolean;
  std::stringstream ss;
  typeBoolean.prettyPrint(ss);
  
  ASSERT_EQ(ss.str(), "bool");
}//prettyPrint
