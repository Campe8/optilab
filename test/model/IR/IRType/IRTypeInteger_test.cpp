#include "utest_globals.hpp"

#include "IRTypeInteger.hpp"

namespace IR
{
  class IRTypeIntegerTest : public IRTypeInteger {
  public:
    IRTypeIntegerTest(std::size_t aNumBits)
    : IRTypeInteger(aNumBits)
    {
    }
  };
}

OPTILAB_TEST(IRTypeInteger, getTypeID)
{
  using namespace IR;
  
  IRTypeIntegerTest typeInteger(0);
  ASSERT_EQ(typeInteger.getTypeID(), IR_TYPE_INTEGER);
}//getTypeID

OPTILAB_TEST(IRTypeInteger, numContainedTypes)
{
  using namespace IR;
  
  IRTypeIntegerTest typeInteger(0);
  ASSERT_EQ(typeInteger.numContainedTypes(), 0);
}//numContainedTypes

OPTILAB_TEST(IRTypeInteger, isEqual)
{
  using namespace IR;
  
  IRTypeIntegerTest typeInteger1(0);
  IRTypeIntegerTest typeInteger2(0);
  ASSERT_TRUE(typeInteger1.isEqual(&typeInteger2));
  ASSERT_TRUE(typeInteger2.isEqual(&typeInteger1));
  
  typeInteger2 = 1;
  ASSERT_FALSE(typeInteger1.isEqual(&typeInteger2));
  ASSERT_FALSE(typeInteger2.isEqual(&typeInteger1));
}//isEqual

OPTILAB_TEST(IRTypeInteger, isa)
{
  using namespace IR;
  
  IRTypeIntegerTest typeInteger(0);
  ASSERT_TRUE(IRTypeInteger::isa(&typeInteger));
}//isa

OPTILAB_TEST(IRTypeInteger, getNumBits)
{
  using namespace IR;
  
  IRTypeIntegerTest typeInteger1(0);
  ASSERT_TRUE(typeInteger1.getNumBits() == 0);
  
  IRTypeIntegerTest typeInteger2(SIZE_MAX);
  ASSERT_EQ(typeInteger2.getNumBits(), SIZE_MAX);
}//getNumBits

OPTILAB_TEST(IRTypeInteger, cast)
{
  using namespace IR;
  
  std::unique_ptr<IRType> pIRTypePointer1(new IRTypeIntegerTest(0));
  ASSERT_TRUE(IRTypeInteger::cast(pIRTypePointer1.get()));
  
  std::unique_ptr<const IRType> pIRTypePointer2(new IRTypeIntegerTest(0));
  ASSERT_TRUE(IRTypeInteger::cast(pIRTypePointer2.get()));
}//cast

OPTILAB_TEST(IRTypeInteger, prettyPrint)
{
  using namespace IR;
  
  IRTypeIntegerTest typeInteger(32);
  std::stringstream ss;
  typeInteger.prettyPrint(ss);
  
  ASSERT_EQ(ss.str(), "int32");
}//prettyPrint

