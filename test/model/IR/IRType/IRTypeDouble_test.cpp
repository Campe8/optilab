#include "utest_globals.hpp"

#include "IRTypeDouble.hpp"

namespace IR
{
  class IRTypeDoubleTest : public IRTypeDouble {
  public:
    IRTypeDoubleTest()
    : IRTypeDouble()
    {
    }
  };
}

OPTILAB_TEST(IRTypeDouble, getTypeID)
{
  using namespace IR;
  
  IRTypeDoubleTest typeDouble;
  ASSERT_EQ(typeDouble.getTypeID(), IR_TYPE_DOUBLE);
}//getTypeID

OPTILAB_TEST(IRTypeDouble, numContainedTypes)
{
  using namespace IR;
  
  IRTypeDoubleTest typeDouble;
  ASSERT_EQ(typeDouble.numContainedTypes(), 0);
}//numContainedTypes

OPTILAB_TEST(IRTypeDouble, isEqual)
{
  using namespace IR;
  
  IRTypeDoubleTest typeDouble1;
  IRTypeDoubleTest typeDouble2;
  ASSERT_TRUE(typeDouble1.isEqual(&typeDouble2));
  ASSERT_TRUE(typeDouble2.isEqual(&typeDouble1));
}//isEqual

OPTILAB_TEST(IRTypeDouble, isa)
{
  using namespace IR;
  
  IRTypeDoubleTest typeDouble;
  ASSERT_TRUE(IRTypeDouble::isa(&typeDouble));
}//isa

OPTILAB_TEST(IRTypeDouble, cast)
{
  using namespace IR;
  
  std::unique_ptr<IRType> pIRTypePointer1(new IRTypeDoubleTest);
  ASSERT_TRUE(IRTypeDouble::cast(pIRTypePointer1.get()));
  
  std::unique_ptr<const IRType> pIRTypePointer2(new IRTypeDoubleTest);
  ASSERT_TRUE(IRTypeDouble::cast(pIRTypePointer2.get()));
}//cast

OPTILAB_TEST(IRTypeDouble, prettyPrint)
{
  using namespace IR;
  
  IRTypeDoubleTest typeDouble;
  std::stringstream ss;
  typeDouble.prettyPrint(ss);
  
  ASSERT_EQ(ss.str(), "double");
}//prettyPrint

