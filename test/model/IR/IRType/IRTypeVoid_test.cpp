#include "utest_globals.hpp"

#include "IRTypeVoid.hpp"

namespace IR
{
  class IRTypeVoidTest : public IRTypeVoid {
  public:
    IRTypeVoidTest()
    : IRTypeVoid()
    {
    }
  };
}

OPTILAB_TEST(IRTypeVoid, getTypeID)
{
  using namespace IR;
  
  IRTypeVoidTest typeVoid;
  ASSERT_EQ(typeVoid.getTypeID(), IR_TYPE_VOID);
}//getTypeID

OPTILAB_TEST(IRTypeVoid, numContainedTypes)
{
  using namespace IR;
  
  IRTypeVoidTest typeVoid;
  ASSERT_EQ(typeVoid.numContainedTypes(), 0);
}//numContainedTypes

OPTILAB_TEST(IRTypeVoid, isEqual)
{
  using namespace IR;
  
  IRTypeVoidTest typeVoid1;
  IRTypeVoidTest typeVoid2;
  ASSERT_TRUE(typeVoid1.isEqual(&typeVoid2));
  ASSERT_TRUE(typeVoid2.isEqual(&typeVoid1));
}//isEqual

OPTILAB_TEST(IRTypeVoid, isa)
{
  using namespace IR;
  
  IRTypeVoidTest typeVoid;
  ASSERT_TRUE(IRTypeVoid::isa(&typeVoid));
}//isa

OPTILAB_TEST(IRTypeVoid, cast)
{
  using namespace IR;
  
  std::unique_ptr<IRType> pIRTypePointer1(new IRTypeVoidTest);
  ASSERT_TRUE(IRTypeVoid::cast(pIRTypePointer1.get()));
  
  std::unique_ptr<const IRType> pIRTypePointer2(new IRTypeVoidTest);
  ASSERT_TRUE(IRTypeVoid::cast(pIRTypePointer2.get()));
}//cast

OPTILAB_TEST(IRTypeVoid, prettyPrint)
{
  using namespace IR;
  
  IRTypeVoidTest typeVoid;
  std::stringstream ss;
  typeVoid.prettyPrint(ss);
  
  ASSERT_EQ(ss.str(), "void");
}//prettyPrint
