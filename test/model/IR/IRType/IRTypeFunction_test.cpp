#include "utest_globals.hpp"

#include "IRTypeFunction.hpp"
#include "IRTypeFactory.hpp"

namespace IR
{
  class IRTypeFunctionTest : public IRTypeFunction {
  public:
    IRTypeFunctionTest(const IRTypeSPtr& aResultType, const std::vector<IRTypeSPtr>& aParamType)
    : IRTypeFunction(aResultType, aParamType)
    {
    }
  };
}// end namespace IR

OPTILAB_TEST(IRTypeFunction, getTypeID)
{
  using namespace IR;
  
  IRTypeFactory factory;
  IRTypeSPtr pIRTypePointer(factory.typeVoid());
  std::vector<IRTypeSPtr> vector {};
  
  IRTypeFunctionTest typeFunction(pIRTypePointer, vector);
  ASSERT_EQ(typeFunction.getTypeID(), IR_TYPE_FUNCTION);
}//getTypeID

OPTILAB_TEST(IRTypeFunction, isEqual)
{
  using namespace IR;
  
  IRTypeFactory factory;
  IRTypeSPtr pIRTypePointerV(factory.typeVoid());
  IRTypeSPtr pIRTypePointerD(factory.typeDouble());
  IRTypeSPtr pIRTypePointerI(factory.typeInteger64());
  std::vector<IRTypeSPtr> vector1 {};
  std::vector<IRTypeSPtr> vector2 { pIRTypePointerD, pIRTypePointerI };
  
  // void() == void()
  IRTypeFunctionTest typeFunction1(pIRTypePointerV, vector1);
  IRTypeFunctionTest typeFunction2(pIRTypePointerV, vector1);
  ASSERT_TRUE(typeFunction1.isEqual(&typeFunction2));
  
  //integer64() != void()
  typeFunction1 = IRTypeFunctionTest(pIRTypePointerI, vector1);
  ASSERT_FALSE(typeFunction1.isEqual(&typeFunction2));
  
  // void(double,integer64) != void()
  typeFunction1 = IRTypeFunctionTest(pIRTypePointerV, vector2);
  ASSERT_FALSE(typeFunction1.isEqual(&typeFunction2));
  
  // void(double,integer64) != double(double,integer64)
  typeFunction2 = IRTypeFunctionTest(pIRTypePointerD, vector2);
  ASSERT_FALSE(typeFunction1.isEqual(&typeFunction2));
  
  // double(double,integer64) == double(double,integer64)
  typeFunction1 = IRTypeFunctionTest(pIRTypePointerD, vector2);
  ASSERT_TRUE(typeFunction1.isEqual(&typeFunction2));
}//isEqual

OPTILAB_TEST(IRTypeFunction, isa)
{
  using namespace IR;
  
  IRTypeFactory factory;
  IRTypeSPtr pIRTypePointer(factory.typeVoid());
  std::vector<IRTypeSPtr> vector {};
  
  IRTypeFunctionTest typeFunction(pIRTypePointer, vector);
  ASSERT_TRUE(IRTypeFunction::isa(&typeFunction));
  
  IRTypeSPtr pIRTypePointerV(factory.typeVoid());
  ASSERT_FALSE(IRTypeFunction::isa(pIRTypePointerV.get()));
}//isa

OPTILAB_TEST(IRTypeFunction, getReturnType)
{
  using namespace IR;
  
  IRTypeFactory factory;
  IRTypeSPtr pIRTypePointerV(factory.typeVoid());
  IRTypeSPtr pIRTypePointerD(factory.typeDouble());
  IRTypeSPtr pIRTypePointerI(factory.typeInteger64());
  std::vector<IRTypeSPtr> vector1 {};
  std::vector<IRTypeSPtr> vector2 { pIRTypePointerD, pIRTypePointerI };
  
  // void()
  IRTypeFunctionTest typeFunction(pIRTypePointerV, vector1);
  ASSERT_EQ(typeFunction.getReturnType(), pIRTypePointerV);
  
  // void(double,integer64)
  typeFunction = IRTypeFunctionTest(pIRTypePointerV, vector2);
  ASSERT_EQ(typeFunction.getReturnType(), pIRTypePointerV);
  
  // double()
  typeFunction = IRTypeFunctionTest(pIRTypePointerD, vector1);
  ASSERT_EQ(typeFunction.getReturnType(), pIRTypePointerD);
  
  // double(double,integer64)
  typeFunction = IRTypeFunctionTest(pIRTypePointerD, vector2);
  ASSERT_EQ(typeFunction.getReturnType(), pIRTypePointerD);
}//getReturnType

OPTILAB_TEST(IRTypeFunction, getParamType)
{
  using namespace IR;
  
  IRTypeFactory factory;
  IRTypeSPtr pIRTypePointerV(factory.typeVoid());
  IRTypeSPtr pIRTypePointerD(factory.typeDouble());
  IRTypeSPtr pIRTypePointerI(factory.typeInteger64());
  std::vector<IRTypeSPtr> vector1 {};
  std::vector<IRTypeSPtr> vector2 { pIRTypePointerD, pIRTypePointerI };
  
  // void() return
  IRTypeFunctionTest typeFunction(pIRTypePointerV, vector1);
  ASSERT_EQ(typeFunction.getReturnType(), pIRTypePointerV);
  
  // void(double,integer64) 1
  typeFunction = IRTypeFunctionTest(pIRTypePointerV, vector2);
  ASSERT_EQ(typeFunction.getParamType(1), pIRTypePointerI);
  
  // double() return
  typeFunction = IRTypeFunctionTest(pIRTypePointerD, vector1);
  ASSERT_EQ(typeFunction.getReturnType(), pIRTypePointerD);
  
  // double(double,integer64) 0
  typeFunction = IRTypeFunctionTest(pIRTypePointerD, vector2);
  ASSERT_EQ(typeFunction.getParamType(0), pIRTypePointerD);
}//getParamType

OPTILAB_TEST(IRTypeFunction, getNumParams)
{
  using namespace IR;
  
  IRTypeFactory factory;
  IRTypeSPtr pIRTypePointerV(factory.typeVoid());
  IRTypeSPtr pIRTypePointerD(factory.typeDouble());
  IRTypeSPtr pIRTypePointerI(factory.typeInteger64());
  std::vector<IRTypeSPtr> vector1 {};
  std::vector<IRTypeSPtr> vector2 { pIRTypePointerD, pIRTypePointerI };
  
  // void()
  IRTypeFunctionTest typeFunction(pIRTypePointerV, vector1);
  ASSERT_EQ(typeFunction.getNumParams(), 0);
  
  // void(double,integer64)
  typeFunction = IRTypeFunctionTest(pIRTypePointerV, vector2);
  ASSERT_EQ(typeFunction.getNumParams(), 2);
  
  // double()
  typeFunction = IRTypeFunctionTest(pIRTypePointerD, vector1);
  ASSERT_EQ(typeFunction.getNumParams(), 0);
  
  // double(double,integer64)
  typeFunction = IRTypeFunctionTest(pIRTypePointerD, vector2);
  ASSERT_EQ(typeFunction.getNumParams(), 2);
}//getNumParams

OPTILAB_TEST(IRTypeFunction, cast)
{
  using namespace IR;
  
  IRTypeFactory factory;
  IRTypeSPtr pIRTypePointerV(factory.typeVoid());
  std::vector<IRTypeSPtr> vector {};
  
  std::unique_ptr<IRType> typeFunction1(new IRTypeFunctionTest(pIRTypePointerV, vector));
  ASSERT_TRUE(IRTypeFunction::cast(typeFunction1.get()));
  
  std::unique_ptr<const IRType> typeFunction2(new IRTypeFunctionTest(pIRTypePointerV, vector));
  ASSERT_TRUE(IRTypeFunction::cast(typeFunction2.get()));
}//cast

OPTILAB_TEST(IRTypeFunction, prettyPrint)
{
  using namespace IR;
  
  IRTypeFactory factory;
  IRTypeSPtr pIRTypePointerV(factory.typeVoid());
  IRTypeSPtr pIRTypePointerD(factory.typeDouble());
  IRTypeSPtr pIRTypePointerI(factory.typeInteger64());
  std::vector<IRTypeSPtr> vector1 {};
  std::vector<IRTypeSPtr> vector2 { pIRTypePointerD, pIRTypePointerI };
  
  std::stringstream ss;
  
  // void()
  IRTypeFunctionTest typeFunction(pIRTypePointerV, vector1);
  typeFunction.prettyPrint(ss);
  ASSERT_EQ(ss.str(), "void()");
  
  // void(double,integer64)
  typeFunction = IRTypeFunctionTest(pIRTypePointerV, vector2);
  ss.str("");
  ss.clear();
  typeFunction.prettyPrint(ss);
  ASSERT_EQ(ss.str(), "void(double, int64)");
  
  // double()
  typeFunction = IRTypeFunctionTest(pIRTypePointerD, vector1);
  ss.str("");
  ss.clear();
  typeFunction.prettyPrint(ss);
  ASSERT_EQ(ss.str(), "double()");
  
  // double(double,integer64)
  typeFunction = IRTypeFunctionTest(pIRTypePointerD, vector2);
  ss.str("");
  ss.clear();
  typeFunction.prettyPrint(ss);
  ASSERT_EQ(ss.str(), "double(double, int64)");
}//prettyPrint
