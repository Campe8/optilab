#include "utest_globals.hpp"

#include "IRContext.hpp"

OPTILAB_TEST(IRContext, getModuleName)
{
  // Test module naming
  using namespace IR;
  auto ctx = createIRContext();
  ASSERT_TRUE(ctx);
  
  ASSERT_EQ("model_module", ctx->getModuleName(IRContext::ContextModule::CM_GLOBAL));
  ASSERT_EQ("variable_module", ctx->getModuleName(IRContext::ContextModule::CM_VARIABLE));
  ASSERT_EQ("constraint_module", ctx->getModuleName(IRContext::ContextModule::CM_CONSTRAINT));
  ASSERT_EQ("parameter_module", ctx->getModuleName(IRContext::ContextModule::CM_PARAMETER));
  ASSERT_EQ("search_module", ctx->getModuleName(IRContext::ContextModule::CM_SEARCH));
}//getTypeID

OPTILAB_TEST(IRContext, globalModule)
{
  // Test global module creation
  using namespace IR;
  auto ctx = createIRContext();
  ASSERT_TRUE(ctx);
  
  auto glb = ctx->globalModule();
  ASSERT_TRUE(glb);
  
  // Should be global module
  ASSERT_EQ("model_module", glb->getName());
  ASSERT_TRUE(glb->isGlobalModule());
  
  // Should have 4 children: var, con, par, src
  ASSERT_EQ(4, glb->numChildren());
  
}//getTypeID
