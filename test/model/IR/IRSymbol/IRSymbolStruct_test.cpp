#include "utest_globals.hpp"

#include "IRContext.hpp"
#include "IRSymbolFactory.hpp"
#include "IRSymbolStruct.hpp"
#include "IRTypeFactory.hpp"

namespace IR
{
  class IRSymbolStructTest : public IRSymbolStruct {
  public:
    IRSymbolStructTest(const std::shared_ptr<IRModule>& aModule,
                       const std::shared_ptr<IRTypeStruct>& aType)
    : IRSymbolStruct(aModule, aType)
    {
    }
  };
}// end namespace IR

namespace {
  
  using namespace IR;
  
  class SymbolStructMock : public ::testing::Test {
  public:
    SymbolStructMock()
    {
      pContext = createIRContext();
    }
    
    ~SymbolStructMock() {}
    
    std::shared_ptr<IRModule> getModule()
    {
      return pContext->globalModule();
    }
    
    IRTypeStructSPtr getStructType(const std::string& aTypeName="")
    {
      std::string name{"StructType"};
      if(!aTypeName.empty()) name = aTypeName;
      
      IRTypeFactory factory;
      auto tDbl = factory.typeDouble();
      auto tInt = factory.typeInteger64();
      std::vector<IRTypeSPtr> types { tDbl, tInt };
      
      return factory.typeStruct(name, types);
    }
    
    IRTypeStructSPtr getStructType2(const std::string& aTypeName="")
    {
      std::string name{"StructType"};
      if(!aTypeName.empty()) name = aTypeName;
      
      IRTypeFactory factory;
      auto tDbl = factory.typeDouble();
      std::vector<IRTypeSPtr> types { tDbl, tDbl };
      
      return factory.typeStruct(name, types);
    }
    
    std::shared_ptr<IRSymbolStructTest> getSymbolStruct()
    {
      return std::make_shared<IRSymbolStructTest>(getModule(), getStructType());
    }//getSymbolStruct
    
    std::shared_ptr<IRSymbolStructTest> getSymbolStruct2()
    {
      return std::make_shared<IRSymbolStructTest>(getModule(), getStructType2());
    }//getSymbolStruct
    
  private:
    IRContextSPtr pContext;
    
  };
  
}  // end unnamed namespace

OPTILAB_TEST_F(SymbolStructMock, staticMethods)
{
  // Test SymbolStruct static methods
  using namespace IR;
  
  auto sst = getSymbolStruct();
  ASSERT_TRUE(IRSymbolStruct::isa(sst.get()));
  
  IRSymbolFactory sf;
  sf.setModule(getModule());
  auto sd = sf.symbolConstDouble(1.0);
  ASSERT_FALSE(IRSymbolStruct::isa(sd.get()));
  ASSERT_FALSE(IRSymbolStruct::cast(sd.get()));
  
  IRSymbol* sstRaw = sst.get();
  ASSERT_EQ(sst.get(), IRSymbolStruct::cast(sstRaw));
  
  const IRSymbol* sstRawConst = sst.get();
  ASSERT_EQ(sst.get(), IRSymbolStruct::cast(sstRawConst));
}//staticMethods

OPTILAB_TEST_F(SymbolStructMock, memberMethods)
{
  // Test SymbolStruct isMemebr, getMember, addMember
  using namespace IR;
  
  auto sst = getSymbolStruct();
  
  IRSymbolFactory sf;
  sf.setModule(getModule());
  auto sd = sf.symbolConstDouble(1.0);
  auto si = sf.symbolConstInteger(1);
  
  std::string mdbl{"mdbl"};
  std::string mint{"mint"};
  sst->addMember(mdbl, sd, 0);
  sst->addMember(mint, si, 1);
  
  ASSERT_TRUE(sst->isMember(mdbl));
  ASSERT_TRUE(sst->isMember(mint));
  ASSERT_FALSE(sst->isMember("other"));
  
  auto dblMember = sst->getMember(mdbl);
  auto intMember = sst->getMember(mint);
  ASSERT_EQ(sd.get(), dblMember.get());
  ASSERT_EQ(si.get(), intMember.get());
  ASSERT_FALSE(sst->getMember("other"));
}//memberMethods

OPTILAB_TEST_F(SymbolStructMock, isEqual)
{
  // Test isEqual method
  using namespace IR;
  
  auto sst1 = getSymbolStruct();
  auto sst2 = getSymbolStruct();
  ASSERT_TRUE(sst1->isEqual(sst2.get()));
  
  IRSymbolFactory sf;
  sf.setModule(getModule());
  auto sd = sf.symbolConstDouble(1.0);
  auto si = sf.symbolConstInteger(1);
  std::string mdbl{"mdbl"};
  std::string mint{"mint"};
  sst1->addMember(mdbl, sd, 0);
  sst1->addMember(mint, si, 1);
  sst2->addMember(mdbl, sd, 0);
  sst2->addMember(mint, si, 1);
  ASSERT_TRUE(sst1->isEqual(sst2.get()));

  // Struct - not struct
  ASSERT_FALSE(sst1->isEqual(sd.get()));
  
  // Struct with different types
  auto sst3 = getSymbolStruct2();
  sst3->addMember(mdbl, sd, 0);
  sst3->addMember(mdbl, sd, 1);
  ASSERT_FALSE(sst1->isEqual(sst3.get()));
  
  // Struct same types but different names
  auto sst4 = getSymbolStruct();
  sst4->addMember(mdbl, sd, 0);
  sst4->addMember("other", si, 1);
  ASSERT_FALSE(sst1->isEqual(sst4.get()));
}//isEqual
