#include "utest_globals.hpp"

#include "ModelTestInc.hpp"
#include "SolverTestInc.hpp"

#ifdef RUNTEST

OPTILAB_TEST(ModelObjectDescriptorSearchSemantic, basics)
{
  using namespace Model;
  
  ModelObjectDescriptorSearchSemantic mdlDesSearch(Search::SearchEngineStrategyType::SE_DEPTH_FIRST_SEARCH,
                                                   Search::VariableChoiceMetricType::VAR_CM_FIRST_FAIL,
                                                   Search::ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MIN);
  
  ASSERT_TRUE(mdlDesSearch.searchStrategyType() == Search::SearchEngineStrategyType::SE_DEPTH_FIRST_SEARCH);
  ASSERT_TRUE(mdlDesSearch.variableChoiceMetricType() == Search::VariableChoiceMetricType::VAR_CM_FIRST_FAIL);
  ASSERT_TRUE(mdlDesSearch.valueChoiceMetricType() == Search::ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MIN);
}//basics

#endif
