

#include "utest_globals.hpp"

#include "ModelTestInc.hpp"
#include "SolverTestInc.hpp"

#include "VariableSemanticDecision.hpp"

#ifdef RUNTEST

OPTILAB_TEST(ModelObjectDescriptorVariable, primitiveVariable)
{
  using namespace Core;
  using namespace Model;
  
  std::unique_ptr<ModelObjectDescriptorVariableDomainInteger> domDes(new ModelObjectDescriptorVariableDomainInteger(1));
  std::unique_ptr<VariableSemanticDecision> varSem(new VariableSemanticDecision("testVar"));
  
  ModelObjectDescriptorPrimitiveVariable primVar(std::move(domDes), std::move(varSem));
  ASSERT_FALSE(primVar.isComposite());
  ASSERT_TRUE(primVar.domainDescriptor());
  ASSERT_TRUE(primVar.domainDescriptor()->isSingleton());
  
  auto semantic = primVar.releaseSemantic();
  ASSERT_TRUE(semantic);
  ASSERT_EQ(semantic->getVariableNameIdentifier(), "testVar");
}//primitiveVariable

OPTILAB_TEST(ModelObjectDescriptorVariable, compositeVariable)
{
  using namespace Core;
  using namespace Model;
  
  std::unique_ptr<VariableSemanticDecision> varSem(new VariableSemanticDecision("testVar"));
  
  std::unique_ptr<ModelObjectDescriptorVariableDomainInteger> domDes1(new ModelObjectDescriptorVariableDomainInteger(1));
  std::unique_ptr<VariableSemanticDecision> varSem1(new VariableSemanticDecision("testVarP1"));
  std::unique_ptr<ModelObjectDescriptorPrimitiveVariable> primVar1(new ModelObjectDescriptorPrimitiveVariable(std::move(domDes1), std::move(varSem1)));
  
  std::unique_ptr<ModelObjectDescriptorVariableDomainInteger> domDes2(new ModelObjectDescriptorVariableDomainInteger(1));
  std::unique_ptr<VariableSemanticDecision> varSem2(new VariableSemanticDecision("testVarP2"));
  std::unique_ptr<ModelObjectDescriptorPrimitiveVariable> primVar2(new ModelObjectDescriptorPrimitiveVariable(std::move(domDes2), std::move(varSem2)));
  
  ModelObjectDescriptorCompositeVariable compVar(2, 1, std::move(varSem));
  ASSERT_TRUE(compVar.isComposite());
  
  compVar.addDescriptorVariable(std::move(primVar1), 0, 0);
  compVar.addDescriptorVariable(std::move(primVar2), 1, 0);
  
  auto compDesc = compVar.getCompositeDescriptor();
  ASSERT_EQ(compDesc->numRows(), 2);
  ASSERT_EQ(compDesc->numCols(), 1);
  ASSERT_EQ(compDesc->size(), 2);
  
  auto semC = compVar.releaseSemantic();
  auto sem1 = (*compDesc)[0][0]->releaseSemantic();
  auto sem2 = (*compDesc)[1][0]->releaseSemantic();
  ASSERT_TRUE(semC);
  ASSERT_TRUE(sem1);
  ASSERT_TRUE(sem2);
  ASSERT_EQ(semC->getVariableNameIdentifier(), "testVar");
  ASSERT_EQ(sem1->getVariableNameIdentifier(), "testVarP1");
  ASSERT_EQ(sem2->getVariableNameIdentifier(), "testVarP2");
}//compositeVariable

#endif
