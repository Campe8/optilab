
#include "utest_globals.hpp"

#include "ModelTestInc.hpp"
#include "SolverTestInc.hpp"

#include "DomainElementManager.hpp"

#ifdef RUNTEST

OPTILAB_TEST(ModelObjectDescriptorVariableDomainBoolean, staticMethods)
{
  using namespace Model;
  
  std::shared_ptr<ModelObjectDescriptorVariableDomain> mdlObjDesc(new ModelObjectDescriptorVariableDomainBoolean());
  ASSERT_TRUE(ModelObjectDescriptorVariableDomainBoolean::isa(mdlObjDesc.get()));
  
  ModelObjectDescriptorVariableDomainBoolean* mdlObjDescBool = ModelObjectDescriptorVariableDomainBoolean::cast(mdlObjDesc.get());
  ASSERT_TRUE(mdlObjDescBool);
}//staticMethods

OPTILAB_TEST(ModelObjectDescriptorVariableDomainBoolean, Bool)
{
  using namespace Model;
  ModelObjectDescriptorVariableDomainBoolean mdlObjDes;
  ASSERT_FALSE(mdlObjDes.isSingleton());
  ASSERT_FALSE(mdlObjDes.getBooleanOnSingleton());
  
  auto varConf = mdlObjDes.descriptorVariableDomainConfig();
  ASSERT_TRUE(varConf == ModelObjectDescriptorVariableDomain::DescriptorVariableDomainConfig::DOM_CONFIG_BOUNDS);
  
  auto varType = mdlObjDes.type();
  ASSERT_TRUE(varType);
  ASSERT_FALSE(varType->isComposite());
  ASSERT_TRUE(varType->variableTypeClass() == Core::VariableTypeClass::VAR_TYPE_BOOLEAN);
}//Bool

OPTILAB_TEST(ModelObjectDescriptorVariableDomainBoolean, SingletonTrue)
{
  using namespace Model;
  ModelObjectDescriptorVariableDomainBoolean mdlObjDes(true);
  ASSERT_TRUE(mdlObjDes.isSingleton());
  ASSERT_TRUE(mdlObjDes.getBooleanOnSingleton());
  
  auto varConf = mdlObjDes.descriptorVariableDomainConfig();
  ASSERT_TRUE(varConf == ModelObjectDescriptorVariableDomain::DescriptorVariableDomainConfig::DOM_CONFIG_SINGLETON);
  
  auto varType = mdlObjDes.type();
  ASSERT_TRUE(varType);
  ASSERT_FALSE(varType->isComposite());
  ASSERT_TRUE(varType->variableTypeClass() == Core::VariableTypeClass::VAR_TYPE_BOOLEAN);
}//SingletonTrue

OPTILAB_TEST(ModelObjectDescriptorVariableDomainBoolean, SingletonFalse)
{
  using namespace Model;
  ModelObjectDescriptorVariableDomainBoolean mdlObjDes(false);
  ASSERT_TRUE(mdlObjDes.isSingleton());
  ASSERT_FALSE(mdlObjDes.getBooleanOnSingleton());
  
  auto varConf = mdlObjDes.descriptorVariableDomainConfig();
  ASSERT_TRUE(varConf == ModelObjectDescriptorVariableDomain::DescriptorVariableDomainConfig::DOM_CONFIG_SINGLETON);
  
  auto varType = mdlObjDes.type();
  ASSERT_TRUE(varType);
  ASSERT_FALSE(varType->isComposite());
  ASSERT_TRUE(varType->variableTypeClass() == Core::VariableTypeClass::VAR_TYPE_BOOLEAN);
}//SingletonFalse

#endif
