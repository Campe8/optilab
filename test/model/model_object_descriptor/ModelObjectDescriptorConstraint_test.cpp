
#include "utest_globals.hpp"

#include "ModelTestInc.hpp"
#include "SolverTestInc.hpp"

#include "ConstraintParameterFactory.hpp"
#include "DomainElementManager.hpp"

#ifdef RUNTEST

OPTILAB_TEST(ModelObjectDescriptorConstraint, basics)
{
  using namespace Core;
  using namespace Model;
  
  ModelObjectDescriptorConstraint conDes(Core::ConstraintId::CON_ID_ELEMENT, Core::PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND);
  
  ASSERT_EQ(conDes.constraintId(), Core::ConstraintId::CON_ID_ELEMENT);
  ASSERT_EQ(conDes.strategyType(), Core::PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND);
  ASSERT_EQ(conDes.constraintParameters().size(), 0);
  
  ConstraintParameterFactory cpf;
  std::vector<DomainElement*> domElem;
  domElem.push_back(DomainElementManager::getInstance().createDomainElementInt64(1));
  auto conParam = cpf.constraintParameterDomain(domElem, DomainClass::DOM_INT64);
  
  conDes.addConstraintParameter(std::move(conParam));
  ASSERT_EQ(conDes.constraintParameters().size(), 1);
}//basics

#endif
