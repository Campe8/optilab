
#include "utest_globals.hpp"

#include "ModelTestInc.hpp"
#include "SolverTestInc.hpp"

#include "DomainElementManager.hpp"

#ifdef RUNTEST

OPTILAB_TEST(ModelObjectDescriptorVariableDomainInteger, staticMethods)
{
  using namespace Model;
  
  std::shared_ptr<ModelObjectDescriptorVariableDomain> mdlObjDesc(new ModelObjectDescriptorVariableDomainInteger(1));
  ASSERT_TRUE(ModelObjectDescriptorVariableDomainInteger::isa(mdlObjDesc.get()));
  
  ModelObjectDescriptorVariableDomainInteger* mdlObjDescInt = ModelObjectDescriptorVariableDomainInteger::cast(mdlObjDesc.get());
  ASSERT_TRUE(mdlObjDescInt);
}//staticMethods

OPTILAB_TEST(ModelObjectDescriptorVariableDomainInteger, singleton)
{
  using namespace Model;
  ModelObjectDescriptorVariableDomainInteger mdlObjDes(1);
  auto domConf = mdlObjDes.domainConfiguration();
  
  ASSERT_TRUE(mdlObjDes.isSingleton());
  ASSERT_EQ(domConf.size(), 1);
  auto domElem = domConf[0];
  
  ASSERT_TRUE(domElem);
  ASSERT_TRUE(domElem->isEqual(Core::DomainElementManager::getInstance().createDomainElementInt64(1)));
  
  auto varConf = mdlObjDes.descriptorVariableDomainConfig();
  ASSERT_TRUE(varConf == ModelObjectDescriptorVariableDomain::DescriptorVariableDomainConfig::DOM_CONFIG_SINGLETON);
  
  auto varType = mdlObjDes.type();
  ASSERT_TRUE(varType);
  ASSERT_FALSE(varType->isComposite());
  ASSERT_TRUE(varType->variableTypeClass() == Core::VariableTypeClass::VAR_TYPE_INTEGER);
}//singleton

OPTILAB_TEST(ModelObjectDescriptorVariableDomainInteger, bounds)
{
  using namespace Model;
  ModelObjectDescriptorVariableDomainInteger mdlObjDes(1, 10);
  auto domConf = mdlObjDes.domainConfiguration();
  
  ASSERT_FALSE(mdlObjDes.isSingleton());
  ASSERT_EQ(domConf.size(), 2);
  auto domElemLB = domConf[0];
  auto domElemUB = domConf[1];
  
  ASSERT_TRUE(domElemLB);
  ASSERT_TRUE(domElemUB);
  ASSERT_TRUE(domElemLB->isEqual(Core::DomainElementManager::getInstance().createDomainElementInt64(1)));
  ASSERT_TRUE(domElemUB->isEqual(Core::DomainElementManager::getInstance().createDomainElementInt64(10)));
  
  auto varConf = mdlObjDes.descriptorVariableDomainConfig();
  ASSERT_TRUE(varConf == ModelObjectDescriptorVariableDomain::DescriptorVariableDomainConfig::DOM_CONFIG_BOUNDS);
  
  auto varType = mdlObjDes.type();
  ASSERT_TRUE(varType);
  ASSERT_FALSE(varType->isComposite());
  ASSERT_TRUE(varType->variableTypeClass() == Core::VariableTypeClass::VAR_TYPE_INTEGER);
  
  // Check order of bounds
  ModelObjectDescriptorVariableDomainInteger mdlObjDes1(10, 1);
  auto domConf1 = mdlObjDes.domainConfiguration();
  domElemLB = domConf[0];
  domElemUB = domConf[1];
  
  ASSERT_TRUE(domElemLB->isEqual(Core::DomainElementManager::getInstance().createDomainElementInt64(1)));
  ASSERT_TRUE(domElemUB->isEqual(Core::DomainElementManager::getInstance().createDomainElementInt64(10)));
}//bounds

OPTILAB_TEST(ModelObjectDescriptorVariableDomainInteger, boundsToSingleton)
{
  using namespace Model;
  ModelObjectDescriptorVariableDomainInteger mdlObjDes(10, 10);
  auto domConf = mdlObjDes.domainConfiguration();
  
  ASSERT_TRUE(mdlObjDes.isSingleton());
  ASSERT_EQ(domConf.size(), 1);
  auto domElem = domConf[0];
  
  ASSERT_TRUE(domElem);
  ASSERT_TRUE(domElem->isEqual(Core::DomainElementManager::getInstance().createDomainElementInt64(10)));
  
  auto varConf = mdlObjDes.descriptorVariableDomainConfig();
  ASSERT_TRUE(varConf == ModelObjectDescriptorVariableDomain::DescriptorVariableDomainConfig::DOM_CONFIG_SINGLETON);
}//boundsToSingleton

OPTILAB_TEST(ModelObjectDescriptorVariableDomainInteger, set)
{
  using namespace Model;
  std::unordered_set<INT_64> setElem{1,2,3};
  ModelObjectDescriptorVariableDomainInteger mdlObjDes(setElem);
  auto domConf = mdlObjDes.domainConfiguration();
  
  ASSERT_FALSE(mdlObjDes.isSingleton());
  ASSERT_EQ(domConf.size(), 3);
  auto domElem1 = domConf[0];
  auto domElem2 = domConf[1];
  auto domElem3 = domConf[2];
  
  ASSERT_TRUE(domElem1);
  ASSERT_TRUE(domElem2);
  ASSERT_TRUE(domElem3);
  
  auto dom1 = Core::DomainElementManager::getInstance().createDomainElementInt64(1);
  auto dom2 = Core::DomainElementManager::getInstance().createDomainElementInt64(2);
  auto dom3 = Core::DomainElementManager::getInstance().createDomainElementInt64(3);
  ASSERT_TRUE(domElem1->isEqual(dom1) || domElem1->isEqual(dom2) || domElem1->isEqual(dom3));
  ASSERT_TRUE(domElem2->isEqual(dom1) || domElem2->isEqual(dom2) || domElem2->isEqual(dom3));
  ASSERT_TRUE(domElem3->isEqual(dom1) || domElem3->isEqual(dom2) || domElem3->isEqual(dom3));
  
  auto varConf = mdlObjDes.descriptorVariableDomainConfig();
  ASSERT_TRUE(varConf == ModelObjectDescriptorVariableDomain::DescriptorVariableDomainConfig::DOM_CONFIG_SET);
  
  auto varType = mdlObjDes.type();
  ASSERT_TRUE(varType);
  ASSERT_FALSE(varType->isComposite());
  ASSERT_TRUE(varType->variableTypeClass() == Core::VariableTypeClass::VAR_TYPE_INTEGER);
}//bounds

OPTILAB_TEST(ModelObjectDescriptorVariableDomainInteger, setToSingleton)
{
  using namespace Model;
  std::unordered_set<INT_64> setElem{10, 10, 10};
  ModelObjectDescriptorVariableDomainInteger mdlObjDes(setElem);
  auto domConf = mdlObjDes.domainConfiguration();
  
  ASSERT_TRUE(mdlObjDes.isSingleton());
  ASSERT_EQ(domConf.size(), 1);
  auto domElem = domConf[0];
  
  ASSERT_TRUE(domElem);
  ASSERT_TRUE(domElem->isEqual(Core::DomainElementManager::getInstance().createDomainElementInt64(10)));
  
  auto varConf = mdlObjDes.descriptorVariableDomainConfig();
  ASSERT_TRUE(varConf == ModelObjectDescriptorVariableDomain::DescriptorVariableDomainConfig::DOM_CONFIG_SINGLETON);
}//setToSingleton

OPTILAB_TEST(ModelObjectDescriptorVariableDomainInteger, setToBounds)
{
  using namespace Model;
  std::unordered_set<INT_64> setElem{10, 10, 100, 100, 100};
  ModelObjectDescriptorVariableDomainInteger mdlObjDes(setElem);
  auto domConf = mdlObjDes.domainConfiguration();
  
  ASSERT_FALSE(mdlObjDes.isSingleton());
  ASSERT_EQ(domConf.size(), 2);
  auto domElem1 = domConf[0];
  auto domElem2 = domConf[1];
  
  ASSERT_TRUE(domElem1);
  ASSERT_TRUE(domElem2);
  ASSERT_TRUE(domElem1->isEqual(Core::DomainElementManager::getInstance().createDomainElementInt64(10)));
  ASSERT_TRUE(domElem2->isEqual(Core::DomainElementManager::getInstance().createDomainElementInt64(100)));
  
  auto varConf = mdlObjDes.descriptorVariableDomainConfig();
  ASSERT_TRUE(varConf == ModelObjectDescriptorVariableDomain::DescriptorVariableDomainConfig::DOM_CONFIG_BOUNDS);
}//setToBounds

#endif

