
#include "utest_globals.hpp"
#include "InterpreterUtils.hpp"
#include "ObjectTools.hpp"
#include "ParserUtils.hpp"

OPTILAB_TEST(ParserUtils, getExprType)
{
  using namespace Model::Utils::Interpreter;
  
  ExprType t = ExprType::AT_UNDEF;
  
  // Parse integers
  t = getExprType("-10");
  ASSERT_EQ(ExprType::AT_INT, t);
  
  t = getExprType("+10");
  ASSERT_EQ(ExprType::AT_INT, t);
  
  t = getExprType("10");
  ASSERT_EQ(ExprType::AT_INT, t);
  
  // Parse IDs
  t = getExprType("abc_def");
  ASSERT_EQ(ExprType::AT_ID, t);
  
  // Parse Arrays
  t = getExprType("[]");
  ASSERT_EQ(ExprType::AT_ARRAY, t);
  
  t = getExprType("[1, 2]");
  ASSERT_EQ(ExprType::AT_ARRAY, t);
  
  t = getExprType("[1, 2, x[34], []]");
  ASSERT_EQ(ExprType::AT_ARRAY, t);
  
  // Parse Subscription
  t = getExprType("x[]");
  ASSERT_EQ(ExprType::AT_SUBSCRIPT, t);
  
  t = getExprType("x[1, 2]");
  ASSERT_EQ(ExprType::AT_SUBSCRIPT, t);

  t = getExprType("_x[1, 2]");
  ASSERT_EQ(ExprType::AT_SUBSCRIPT, t);
  
  // Undefined
  t = getExprType("+x[1, 2]");
  ASSERT_EQ(ExprType::AT_UNDEF, t);
}//getExprType

OPTILAB_TEST(ParserUtils, getDataObjectFromPrimitiveOrScalarType)
{
  // Test getDataObjectFromPrimitiveOrScalarType function
  Interpreter::DataObject primitive(1);
  const auto& prim = Model::Utils::Parser::getDataObjectFromPrimitiveOrScalarType(primitive);
  ASSERT_TRUE(Interpreter::isPrimitiveType(prim));
  EXPECT_EQ(1, prim.getDataValue<int>());
  
  Interpreter::DataObject scalar;
  auto scalarPtr =
  std::shared_ptr<Interpreter::BaseObject>(Interpreter::ObjectTools::createScalarObject(primitive));
  scalar.setClassObject(scalarPtr);
  const auto& scal = Model::Utils::Parser::getDataObjectFromPrimitiveOrScalarType(scalar);
  ASSERT_TRUE(Interpreter::isPrimitiveType(scal));
  EXPECT_EQ(1, scal.getDataValue<int>());
}//getDataObjectFromPrimitiveOrScalarType
