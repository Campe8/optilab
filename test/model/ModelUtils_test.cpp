
#include "utest_globals.hpp"
#include "ModelUtils.hpp"

OPTILAB_TEST(ModelUtils, ConstraintHelper)
{
  using namespace Model::Utils;
  const auto& conHelper = Model::Utils::ConstraintHelper::getInstance();
  ASSERT_FALSE(conHelper.isConstraintRegistered("__RandomNameConstraint__"));
  
  std::string cname = "nq";
  ASSERT_TRUE(conHelper.isConstraintRegistered(cname));
  ASSERT_FALSE(conHelper.isConstraintReified(cname));
  ASSERT_FALSE(conHelper.isConstraintGlobal(cname));
  
  // Constraint arguments
  ASSERT_EQ(2, conHelper.numConstraintArguments(cname));
  ASSERT_FALSE(conHelper.isConstraintArgumentArrayAllowed(cname, 0));
  ASSERT_FALSE(conHelper.isConstraintArgumentArrayAllowed(cname, 1));
  
  auto arrTypesArg0    = conHelper.getConstraintArgumentArrayAllowedTypes(cname, 0);
  auto nonArrTypesArg0 = conHelper.getConstraintArgumentNonArrayAllowedTypes(cname, 0);
  ASSERT_TRUE(arrTypesArg0.empty());
  ASSERT_EQ(3, nonArrTypesArg0.size());
  
  auto arrTypesArg1    = conHelper.getConstraintArgumentArrayAllowedTypes(cname, 1);
  auto nonArrTypesArg1 = conHelper.getConstraintArgumentNonArrayAllowedTypes(cname, 1);
  ASSERT_TRUE(arrTypesArg1.empty());
  ASSERT_EQ(3, nonArrTypesArg1.size());
}//ConstraintHelper
