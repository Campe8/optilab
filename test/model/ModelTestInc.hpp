#pragma once

#include "Model.hpp"
#include "ModelContext.hpp"
#include "CLIUtils.hpp"

#include <string>
#include <vector>
#include <sstream>

/// Returns a model meta information instance with given name and solution limit
Model::ModelMetaInfo* getModelMetaInfo(const std::string& aModelName, int aSolutionLimit);

/// Converts model context to string
std::string modelContextToString(const Model::ModelContext& aCtx);

/// Returns base empty model context
std::string getBaseModelContext();

/// Returns default model context
std::string getDefaultModelContext(const std::string& aCtxName);

/// Returns variable model context
std::string getVariableModelContext(const std::string& aVarId,
                                    const std::string& aVarType,
                                    const std::string& aDomType,
                                    const std::vector<std::string>& aDomValues,
                                    const std::string& aSemanticType,
                                    bool aBranching=false);
