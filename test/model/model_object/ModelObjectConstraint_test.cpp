
#include "utest_globals.hpp"

#include "ModelTestInc.hpp"
#include "SolverTestInc.hpp"

#include "ModelConstraint.hpp"
#include "ConstraintFactory.hpp"
#include "VariableFactory.hpp"

#ifdef RUNTEST

namespace Model {
  
  class ModelObjectConstraintTest : public ModelObjectConstraint {
  public:
    ModelObjectConstraintTest(const ModelObjectDescriptorConstraintSPtr& aDescriptorConstraint,
                                    Core::ConstraintFactoryPtr aConstraintFactory)
    : ModelObjectConstraint(aDescriptorConstraint, aConstraintFactory)
    {
    }
    
  };
  
}// end namespace Model

OPTILAB_TEST(ModelObjectConstraint, basics)
{
  using namespace Core;
  using namespace Model;
  
  std::unique_ptr<ConstraintFactory> conFac(new ConstraintFactory());
  
  VariableFactory vf;
  ConstraintParameterFactory cpf;
  
  VariableSemanticUPtr varSem1 = std::unique_ptr<VariableSemanticDecision>(new VariableSemanticDecision("var1"));
  VariableSemanticUPtr varSem2 = std::unique_ptr<VariableSemanticDecision>(new VariableSemanticDecision("var2"));
  VariableSPtr var1(vf.variableInteger(10, std::move(varSem1)));
  VariableSPtr var2(vf.variableInteger(11, std::move(varSem2)));
  auto conParam1 = cpf.constraintParameterVariable(var1);
  auto conParam2 = cpf.constraintParameterVariable(var2);
  
  auto mdlDesCon = std::make_shared<ModelObjectDescriptorConstraint>(Core::ConstraintId::CON_ID_NQ,
                                                                     Core::PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND);
  mdlDesCon->addConstraintParameter(std::move(conParam1));
  mdlDesCon->addConstraintParameter(std::move(conParam2));
  
  std::unique_ptr<ModelObject> mdlObj(new ModelObjectConstraintTest(mdlDesCon, conFac.get()));
  ASSERT_TRUE(ModelObjectConstraint::isa(mdlObj.get()));
  
  auto mdlObjCast = ModelObjectConstraint::cast(mdlObj.get());
  ASSERT_TRUE(mdlObjCast);
}//basics

OPTILAB_TEST(ModelObjectConstraint, conDesc)
{
  using namespace Core;
  using namespace Model;
  
  std::unique_ptr<ConstraintFactory> conFac(new ConstraintFactory());
  
  VariableFactory vf;
  ConstraintParameterFactory cpf;
  
  VariableSemanticUPtr varSem1 = std::unique_ptr<VariableSemanticDecision>(new VariableSemanticDecision("var1"));
  VariableSemanticUPtr varSem2 = std::unique_ptr<VariableSemanticDecision>(new VariableSemanticDecision("var2"));
  VariableSPtr var1(vf.variableInteger(10, std::move(varSem1)));
  VariableSPtr var2(vf.variableInteger(11, std::move(varSem2)));
  auto conParam1 = cpf.constraintParameterVariable(var1);
  auto conParam2 = cpf.constraintParameterVariable(var2);
  
  
  auto mdlDesCon = std::make_shared<ModelObjectDescriptorConstraint>(Core::ConstraintId::CON_ID_NQ,
                                                                     Core::PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND);
  mdlDesCon->addConstraintParameter(std::move(conParam1));
  mdlDesCon->addConstraintParameter(std::move(conParam2));
  
  std::unique_ptr<ModelObject> mdlObj(new ModelObjectConstraintTest(mdlDesCon, conFac.get()));
  auto mdlObjCast = ModelObjectConstraint::cast(mdlObj.get());
  
  auto con = mdlObjCast->constraint();
  ASSERT_EQ(con->getConstraintId(), Core::ConstraintId::CON_ID_NQ);
  
  // Nq constraint supports only domain propagation strategy
  ASSERT_EQ(con->getStrategyType(), Core::PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN);
  
  // Verify variables in the constraint's scope
  ASSERT_EQ(con->scopeSize(), 2);
  ASSERT_EQ(con->getScopeVariableId(0), var1->getUUID());
  ASSERT_EQ(con->getScopeVariableId(1), var2->getUUID());
}//conDesc

#endif
