
#include "utest_globals.hpp"

#include "ModelTestInc.hpp"
#include "SolverTestInc.hpp"

#ifdef RUNTEST

namespace Model {
  
  class ModelObjectSearchSemanticTest : public ModelObjectSearchSemantic {
  public:
    ModelObjectSearchSemanticTest(const ModelObjectDescriptorSearchSemanticSPtr& aDescriptorSearchSemantic)
    : ModelObjectSearchSemantic(aDescriptorSearchSemantic)
    {
    }
    
  };
  
}// end namespace Model

OPTILAB_TEST(ModelObjectSearchSemantic, basics)
{
  using namespace Core;
  using namespace Model;
  
  auto mdlDesSearch = std::make_shared<ModelObjectDescriptorSearchSemantic>(Search::SearchEngineStrategyType::SE_DEPTH_FIRST_SEARCH, Search::VariableChoiceMetricType::VAR_CM_FIRST_FAIL, Search::ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MIN);
  
  std::unique_ptr<ModelObject> mdlObj(new ModelObjectSearchSemanticTest(mdlDesSearch));
  ASSERT_TRUE(ModelObjectSearchSemantic::isa(mdlObj.get()));
  
  auto mdlObjCast = ModelObjectSearchSemantic::cast(mdlObj.get());
  ASSERT_TRUE(mdlObjCast);
}//basics

OPTILAB_TEST(ModelObjectSearchSemantic, searchDesc)
{
  using namespace Core;
  using namespace Model;
  
  auto mdlDesSearch = std::make_shared<ModelObjectDescriptorSearchSemantic>(Search::SearchEngineStrategyType::SE_DEPTH_FIRST_SEARCH, Search::VariableChoiceMetricType::VAR_CM_FIRST_FAIL, Search::ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MIN);
  
  std::unique_ptr<ModelObject> mdlObj(new ModelObjectSearchSemanticTest(mdlDesSearch));
  auto mdlObjCast = ModelObjectSearchSemantic::cast(mdlObj.get());
  ASSERT_EQ(mdlObjCast->searchEngineStrategyType(), Search::SearchEngineStrategyType::SE_DEPTH_FIRST_SEARCH);

  auto hChoice = mdlObjCast->heuristicChoice();
  ASSERT_EQ(hChoice.first, Search::VariableChoiceMetricType::VAR_CM_FIRST_FAIL);
  ASSERT_EQ(hChoice.second, Search::ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MIN);
}//searchDesc

#endif
