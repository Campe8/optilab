
#include "utest_globals.hpp"

#include "ModelTestInc.hpp"
#include "SolverTestInc.hpp"

#include "ModelVariable.hpp"
#include "VariableSemanticDecision.hpp"

#ifdef RUNTEST

namespace Model {
  
  class ModelVariableTest : public ModelVariable {
  public:
    ModelVariableTest(const ModelObjectDescriptorVariableSPtr& aDescriptorVariable,
                            Core::VariableFactoryPtr aVariableFactory)
    : ModelVariable(aDescriptorVariable, aVariableFactory)
    {
    }
    
  };
  
}// end namespace Model

OPTILAB_TEST(ModelObjectVariable, basics)
{
  using namespace Core;
  using namespace Model;
  
  std::unique_ptr<ModelObjectDescriptorVariableDomainInteger> domDes(new ModelObjectDescriptorVariableDomainInteger(1));
  std::unique_ptr<VariableSemanticDecision> varSem(new VariableSemanticDecision("testVar"));
  
  auto primVar = std::make_shared<ModelObjectDescriptorPrimitiveVariable>(std::move(domDes), std::move(varSem));
  std::unique_ptr<VariableFactory> varFac(new VariableFactory());
  
  std::unique_ptr<ModelObject> mdlObjVar(new ModelObjectVariableTest(primVar, varFac.get()));
  ASSERT_TRUE(ModelObjectVariable::isa(mdlObjVar.get()));
  
  auto mdlObjVarCast = ModelObjectVariable::cast(mdlObjVar.get());
  ASSERT_TRUE(mdlObjVarCast);
}//basics

OPTILAB_TEST(ModelObjectVariable, variable)
{
  using namespace Core;
  using namespace Model;
  
  std::unique_ptr<ModelObjectDescriptorVariableDomainInteger> domDes(new ModelObjectDescriptorVariableDomainInteger(1));
  std::unique_ptr<VariableSemanticDecision> varSem(new VariableSemanticDecision("testVar"));
  
  std::shared_ptr<ModelObjectDescriptorPrimitiveVariable> primVar = std::make_shared<ModelObjectDescriptorPrimitiveVariable>(std::move(domDes), std::move(varSem));
  std::unique_ptr<VariableFactory> varFac(new VariableFactory());
  
  std::unique_ptr<ModelObject> mdlObjVar(new ModelObjectVariableTest(primVar, varFac.get()));
  auto mdlObjVarCast = ModelObjectVariable::cast(mdlObjVar.get());
  
  auto var = mdlObjVarCast->variable();
  ASSERT_TRUE(var);
  
  // Check semantic of the variable
  ASSERT_TRUE(var->variableSemantic()->getVariableNameIdentifier() == "testVar");
  
  // Check variable domain
  ASSERT_EQ(var->domainListSize(), 1);
  
  auto domain = var->domain();
  ASSERT_EQ(domain->getSize(), 1);
  ASSERT_TRUE(domain->lowerBound()->isEqual(DomainElementManager::getInstance().createDomainElementInt64(1)));
}//variable

#endif

