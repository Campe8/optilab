
#include "utest_globals.hpp"

#include "ModelTestInc.hpp"
#include "SolverTestInc.hpp"

#include "GeneticDescriptor.hpp"
#include "GeneticInformation.hpp"

namespace Model {
  
  class GeneticDescriptorTest : public GeneticDescriptor {
  public:
    void initCBLSSearchDescriptor(const SearchInformation* const aSrcInfo)
    {
      GeneticDescriptor::initCBLSSearchDescriptor(aSrcInfo);
    }
    
  };
  
}// end namespace Model

OPTILAB_TEST(GeneticSearchDescriptor, defaultValues)
{
  // Test default values
  using namespace Model;
  GeneticDescriptor gd;
  ASSERT_EQ(Search::SearchEngineStrategyType::SE_GENETIC, gd.getSearchStrategy());
  
  // Heuristic should be input order and random value for domains
  ASSERT_EQ(Search::VariableChoiceMetricType::VAR_CM_INPUT_ORDER, gd.getVariableChoice());
  ASSERT_EQ(Search::ValueChoiceMetricType::VAL_METRIC_INDOMAIN_RANDOM, gd.getValueChoice());
  
  // Default values from SearchDescriptor (not set)
  ASSERT_TRUE(gd.getSemanticList().empty());
  ASSERT_EQ(-1, gd.getNeighborhoodSize());
  ASSERT_EQ(1, gd.getNumNeighborhoods());
  ASSERT_TRUE(gd.isAggressiveCBLS());
}// defaultValues

OPTILAB_TEST(GeneticSearchDescriptor, initDescriptor)
{
  // Test value initialization using search information
  using namespace Model;
  GeneticDescriptorTest gdt;
  
  auto ginfo = std::make_shared<GeneticInformation>();
  ginfo->addMember<std::size_t>(GeneticInformation::PopulationSizeKey, 10);
  ginfo->addMember<std::size_t>(GeneticInformation::NumGenerationsKey, 20);
  ginfo->addMember<std::size_t>(GeneticInformation::MutationChanceKey, 30);
  ginfo->addMember<std::size_t>(GeneticInformation::MutationSizeKey, 40);
  
  gdt.initCBLSSearchDescriptor(ginfo.get());

  // GeneticSearchDescriptor values
  ASSERT_EQ(10, gdt.getPopulationSize());
  ASSERT_EQ(20, gdt.getNumGenerations());
  ASSERT_EQ(30, gdt.getMutationChance());
  ASSERT_EQ(40, gdt.getMutationSize());
}// initDescriptor
