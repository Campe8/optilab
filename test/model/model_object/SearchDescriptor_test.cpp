
#include "utest_globals.hpp"

#include "ModelTestInc.hpp"
#include "SolverTestInc.hpp"

#include "SearchDescriptor.hpp"
#include "DFSInformation.hpp"

namespace Model {
  
  class SearchDescriptorTest : public SearchDescriptor {
  public:
    SearchDescriptorTest(Search::SearchEngineStrategyType aStrategyType)
    : SearchDescriptor(aStrategyType)
    {
    }
    
    void setVarValChoiceHeuristic(Search::VariableChoiceMetricType aVarChoice,
                                  Search::ValueChoiceMetricType aValChoice)
    {
      SearchDescriptor::setVarValChoiceHeuristic(aVarChoice, aValChoice);
    }
    
    void initInnerSearchDescriptor(const SearchInformation* const) override
    {
      pPrivate = 10;
    }
    
    inline int getPrivate() const { return pPrivate; }
    
  private:
    int pPrivate = 0;
  };
  
}// end namespace Model

OPTILAB_TEST(SearchDescriptor, basics)
{
  // Test interface for search descriptor
  using namespace Model;
  SearchDescriptorTest sdt(Search::SearchEngineStrategyType::SE_DEPTH_FIRST_SEARCH);
  ASSERT_EQ(Search::SearchEngineStrategyType::SE_DEPTH_FIRST_SEARCH, sdt.getSearchStrategy());
  
  // Default values for variable and value choice
  ASSERT_EQ(Search::VariableChoiceMetricType::VAR_CM_UNDEF, sdt.getVariableChoice());
  ASSERT_EQ(Search::ValueChoiceMetricType::VAL_METRIC_UNDEF, sdt.getValueChoice());
}//basics

OPTILAB_TEST(SearchDescriptor, setGetMethods)
{
  // Test interface for search descriptor get/set methods
  using namespace Model;
  SearchDescriptorTest sdt(Search::SearchEngineStrategyType::SE_DEPTH_FIRST_SEARCH);
  
  // Heuristic
  sdt.setVarValChoiceHeuristic(Search::VariableChoiceMetricType::VAR_CM_LARGEST,
                               Search::ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MAX);
  ASSERT_EQ(Search::VariableChoiceMetricType::VAR_CM_LARGEST, sdt.getVariableChoice());
  ASSERT_EQ(Search::ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MAX, sdt.getValueChoice());
  
  // Test protected method for setting the inner search descriptor
  ASSERT_EQ(0, sdt.getPrivate());
  
  // Initializing the descriptor should trigger inner initialization
  auto info = std::make_shared<DFSInformation>();
  sdt.initSearchDescriptor(info.get());
  ASSERT_EQ(10, sdt.getPrivate());
  
  // Semantic list
  SearchDescriptor::SemanticList slist;
  slist.push_back({"nullVar", nullptr});
  sdt.setSemanticList(slist);
  ASSERT_EQ(1, sdt.getSemanticList().size());
}//setGetMethods
