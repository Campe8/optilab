#include "ModelTestInc.hpp"

std::string modelContextToString(const Model::ModelContext& aCtx)
{
  assert(false);
  return "";
  //std::stringstream ss;
  //aCtx.context().serializeContext(ss);
  //return ss.str();
}//modelContextToString

Model::ModelMetaInfo* getModelMetaInfo(const std::string& aModelName, int aSolutionLimit)
{
  return new Model::ModelMetaInfo(aModelName, aSolutionLimit);
}//getModelMetaInfo

std::string getBaseModelContext()
{
  static std::string defaultModelContext =
  R"({
    "instance": {
      "variables": {
        "var": ""
      },
      "constraints": {
        "con": ""
      },
      "search": ""
    }
  }
)";
  
  return defaultModelContext;
}//getDefaulModelContext

std::string getDefaultModelContext(const std::string& aCtxName)
{
  static std::string defaultModelContext =
  R"({
  "instance": {
    "variables": {
      "var": ""
    },
    "constraints": {
      "con": ""
    }, 
    "search": {
      "type": "dfs",
      "semantic": {
        "variable_choice": "input_order",
        "value_choice": "indomain_min"
      }
    },
)";

// Add name to context
auto ctxWithName = defaultModelContext + R"("name": )" + "\"" + aCtxName + "\",";
    
// Add type and solution to context
ctxWithName += R"(
    "type": "CSP",
    "solution": "1"
  }
}
)";

return ctxWithName;
}//getDefaulModelContext

std::string getVariableModelContext(const std::string& aVarId,
                                    const std::string& aVarType,
                                    const std::string& aDomType,
                                    const std::vector<std::string>& aDomValues,
                                    const std::string& aSemanticType,
                                    bool aBranching)
{
  // Set context
  std::string variableModelContext =
  "{\n"
  "\"semantic\": {\n"
  "\"type\": \"" + aSemanticType + "\"\n"
  "},\n"
  "\"domain\": {\n"
  "\"type\": \"" + aDomType + "\",\n"
  "\"values\": [\n";
  for(auto& val: aDomValues)
  {
    variableModelContext += "\"";
    variableModelContext += val;
    variableModelContext += "\",\n";
  }
  variableModelContext.pop_back();
  variableModelContext.pop_back();
  variableModelContext += "\n]\n},\n";
  
  variableModelContext +=
  "\"id\": \"" + aVarId + "\",\n"
  "\"type\": \"" + aVarType + "\"";
  
  if(aBranching)
  {
    variableModelContext += ",\n\"branching\": \"true\"\n}\n";
  }
  else
  {
    variableModelContext += "\n}\n";
  }
  
  return variableModelContext;
}//getDefaulModelContext

