
#include "utest_globals.hpp"

#include "DomainIterator.hpp"
#include "DomainElementManager.hpp"
#include "DomainElementInt64Container.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementVoid.hpp"

#include <unordered_set>

OPTILAB_TEST(DomainIterator, Basics)
{
  using namespace Core;
  
  // Create a Int64 container for [-10..10] to iterate over
  DomainElementInt64 lowerBound(-10);
  DomainElementInt64 upperBound( 10);
  std::unique_ptr<DomainElementContainer> container(new DomainElementInt64Container(&lowerBound, &upperBound));
  
  DomainIterator it(container.get());
  DomainIterator it2(it);
  ASSERT_TRUE(it == it2);
  
  // Iterators over different containers should be different
  std::unique_ptr<DomainElementContainer> container2(new DomainElementInt64Container(&lowerBound, &upperBound));
  DomainIterator itAux(container2.get());
  ASSERT_FALSE(it == itAux);
  ASSERT_TRUE(it != itAux);
  
  // Iterators over the same container but reversed should be different
  // Using it2 since rbegin changes the internal iterator pointer of the caller
  DomainIterator itRBegin(it2.rbegin());
  ASSERT_FALSE(it == itRBegin);
  
  // Test assignment operator
  itRBegin = it;
  ASSERT_TRUE(it == itRBegin);
}//Basics

OPTILAB_TEST(DomainIterator, Begin)
{
  using namespace Core;
  
  DomainElementInt64 lowerBound(-10);
  DomainElementInt64 upperBound( 10);
  std::unique_ptr<DomainElementContainer> container(new DomainElementInt64Container(&lowerBound, &upperBound));
  DomainIterator it(container.get());
  
  auto itBegin = it.begin();
  
  // Check pointed element
  ASSERT_TRUE(DomainElement::isEqual(&lowerBound, &(*itBegin)));
}//Begin

OPTILAB_TEST(DomainIterator, rBegin)
{
  using namespace Core;
  
  DomainElementInt64 lowerBound(-10);
  DomainElementInt64 upperBound( 10);
  std::unique_ptr<DomainElementContainer> container(new DomainElementInt64Container(&lowerBound, &upperBound));
  DomainIterator it(container.get());
  
  auto itrBegin = it.rbegin();
  
  // Check pointed element
  ASSERT_TRUE(DomainElement::isEqual(&upperBound, &(*itrBegin)));
}//rBegin

OPTILAB_TEST(DomainIterator, End)
{
  using namespace Core;
  
  DomainElementInt64 lowerBound(-10);
  DomainElementInt64 upperBound( 10);
  std::unique_ptr<DomainElementContainer> container(new DomainElementInt64Container(&lowerBound, &upperBound));
  DomainIterator it(container.get());
  
  auto itEnd = it.end();
  
  // Check pointed element
  ASSERT_TRUE(DomainElementVoid::isa(&(*itEnd)));
}//End

OPTILAB_TEST(DomainIterator, Increment)
{
  using namespace Core;
  
  DomainElementInt64 lowerBound(-10);
  DomainElementInt64 upperBound( 10);
  std::unique_ptr<DomainElementContainer> container(new DomainElementInt64Container(&lowerBound, &upperBound));
  DomainIterator it(container.get());
  
  INT_64 domVal = -10;
  auto iter = it.begin();
  for(; iter != it.end(); ++iter)
  {
    DomainElementInt64 domainElement(domVal++);
    ASSERT_EQ(domainElement.getValue(), DomainElementInt64::cast(&*iter)->getValue());
  }
}//Increment

OPTILAB_TEST(DomainIterator, ReverseIterator)
{
  using namespace Core;
  
  DomainElementInt64 lowerBound(-10);
  DomainElementInt64 upperBound( 10);
  std::unique_ptr<DomainElementContainer> container(new DomainElementInt64Container(&lowerBound, &upperBound));
  DomainIterator it(container.get());
  
  INT_64 domVal = 10;
  auto iter = it.rbegin();
  for(; iter != it.end(); ++iter)
  {
    DomainElementInt64 domainElement(domVal--);
    ASSERT_EQ(domainElement.getValue(), DomainElementInt64::cast(&*iter)->getValue());
  }
}//ReverseIterator

OPTILAB_TEST(DomainIterator, RandomIterator)
{
  using namespace Core;
  
  // Check single element
  DomainElementInt64 elem1(-20);
  DomainElementInt64 elem2(-2);
  DomainElementInt64 elem3( 0);
  DomainElementInt64 elem4(30);
  DomainElementInt64 elem5(50);
  
  std::unordered_set<DomainElementInt64*> set;
  set.insert(&elem1);
  set.insert(&elem2);
  set.insert(&elem3);
  set.insert(&elem4);
  set.insert(&elem5);
  
  std::unique_ptr<DomainElementContainer> container(new DomainElementInt64Container(set));
  DomainIterator it(container.get());
  
  std::size_t ctr{0};
  auto iter = it.randBegin();
  while(iter != it.end())
  {
    container->subtract(&*iter);
    ctr++;
    
    iter = iter.randBegin();
  }
  ASSERT_EQ(ctr, set.size());
}//RandomIterator
