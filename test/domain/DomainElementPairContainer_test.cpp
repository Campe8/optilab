
#include "utest_globals.hpp"

#include "DomainElementPairContainer.hpp"
#include "DomainElementInt64.hpp"

#include <memory>
#include <unordered_set>

namespace Core
{
  class DomainElementPairContainerTest : public DomainElementPairContainer {
  public:
    DomainElementPairContainerTest(DomainElementInt64 *aLowerBound, DomainElementInt64 *aUpperBound)
    : DomainElementPairContainer(aLowerBound, aUpperBound)
    {
    }
    
    inline std::size_t getDomainSize() const
    {
      return DomainElementPairContainer::getDomainSize();
    }
    
    inline DomainElement *lowerBound()
    {
      return DomainElementPairContainer::getLowerBound();
    }
    
    inline DomainElement *upperBound()
    {
      return DomainElementPairContainer::getUpperBound();
    }
  };
}// end namespace Core

namespace {
  typedef std::shared_ptr<Core::DomainElementPairContainerTest> ContainerPtr;
  
  ContainerPtr getDomain(INT_64 aLower, INT_64 aUpper)
  {
    return std::make_shared<Core::DomainElementPairContainerTest>(
    Core::DomainElementManager::getInstance().createDomainElementInt64(aLower),
    Core::DomainElementManager::getInstance().createDomainElementInt64(aUpper));
  }//getDomain
}// end namespace

OPTILAB_TEST(DomainElementPairContainer, Basics)
{
  using namespace Core;
  
  // Create a Int64 container for <-10, 10>
  ContainerPtr container1 = getDomain(-10, +10);

  for (INT_64 idx = -15; idx <= 15; ++idx)
  {
    DomainElementInt64 domainElement(idx);
    if(idx == -10 || idx == 10)
    {
      ASSERT_TRUE(container1->contains(&domainElement));
    }
    else
    {
      ASSERT_FALSE(container1->contains(&domainElement));
    }
  }
  
  // Copy constructor
  DomainElementPairContainer containerCopy(*container1.get());
  
  for (INT_64 idx = -15; idx <= 15; ++idx)
  {
    DomainElementInt64 domainElement(idx);
    if(idx == -10 || idx == 10)
    {
      ASSERT_TRUE(containerCopy.contains(&domainElement));
    }
    else
    {
      ASSERT_FALSE(containerCopy.contains(&domainElement));
    }
  }
}//Basics

OPTILAB_TEST(DomainElementPairContainer, Shrink)
{
  using namespace Core;
  
  // Create a Int64 container for <-10, 10>
  DomainElementInt64 lowerBound1(-10);
  DomainElementInt64 upperBound1( 10);
  std::unique_ptr<DomainElementPairContainerTest> container(new DomainElementPairContainerTest(&lowerBound1, &upperBound1));
  
  container->shrink(&lowerBound1, &upperBound1);
  ASSERT_TRUE(container->contains(&lowerBound1));
  ASSERT_TRUE(container->contains(&upperBound1));
  
  container->shrink(DomainElementManager::getInstance().createDomainElementInt64(-1),
                    DomainElementManager::getInstance().createDomainElementInt64(+1));
  ASSERT_EQ(container->getDomainSize(), 0);
  
  std::unique_ptr<DomainElementPairContainerTest> container2(new DomainElementPairContainerTest(&lowerBound1, &upperBound1));
  
  container2->shrink(DomainElementManager::getInstance().createDomainElementInt64(-10),
                     DomainElementManager::getInstance().createDomainElementInt64(-10));
  ASSERT_TRUE(container2->contains(&lowerBound1));
  ASSERT_FALSE(container2->contains(&upperBound1));
  ASSERT_EQ(container2->getDomainSize(), 1);
  
  std::unique_ptr<DomainElementPairContainerTest> container3(new DomainElementPairContainerTest(&lowerBound1, &upperBound1));
  
  container3->shrink(DomainElementManager::getInstance().createDomainElementInt64(10),
                     DomainElementManager::getInstance().createDomainElementInt64(10));
  ASSERT_FALSE(container3->contains(&lowerBound1));
  ASSERT_TRUE(container3->contains(&upperBound1));
  ASSERT_EQ(container3->getDomainSize(), 1);
  
  std::unique_ptr<DomainElementPairContainerTest> container4(new DomainElementPairContainerTest(&lowerBound1, &upperBound1));
  
  container4->shrink(DomainElementManager::getInstance().createDomainElementInt64(-9),
                     DomainElementManager::getInstance().createDomainElementInt64(+9));
  ASSERT_EQ(container4->getDomainSize(), 0);
  
  std::unique_ptr<DomainElementPairContainerTest> container5(new DomainElementPairContainerTest(&lowerBound1, &upperBound1));
  
  container5->shrink(DomainElementManager::getInstance().createDomainElementInt64(+11),
                     DomainElementManager::getInstance().createDomainElementInt64(+12));
  ASSERT_EQ(container5->getDomainSize(), 0);
  
  std::unique_ptr<DomainElementPairContainerTest> container6(new DomainElementPairContainerTest(&lowerBound1, &upperBound1));
  
  container6->shrink(DomainElementManager::getInstance().createDomainElementInt64(-12),
                     DomainElementManager::getInstance().createDomainElementInt64(-11));
  ASSERT_EQ(container6->getDomainSize(), 0);
  
  std::unique_ptr<DomainElementPairContainerTest> container7(new DomainElementPairContainerTest(&lowerBound1, &upperBound1));
  
  container7->shrink(DomainElementManager::getInstance().createDomainElementInt64(0),
                     DomainElementManager::getInstance().createDomainElementInt64(11));
  ASSERT_TRUE(container7->contains(&upperBound1));
  ASSERT_EQ(container7->getDomainSize(), 1);
  
  std::unique_ptr<DomainElementPairContainerTest> container8(new DomainElementPairContainerTest(&lowerBound1, &upperBound1));
  
  container8->shrink(DomainElementManager::getInstance().createDomainElementInt64(-13),
                     DomainElementManager::getInstance().createDomainElementInt64(0));
  ASSERT_TRUE(container8->contains(&lowerBound1));
  ASSERT_EQ(container8->getDomainSize(), 1);
}//Shrink

#ifdef TESTRUN
OPTILAB_TEST(DomainElementPairContainer, Subtract)
{
  using namespace Core;
  
  // Create a Int64 container for [-10..10]
  DomainElementInt64 lowerBound(-10);
  DomainElementInt64 upperBound( 10);
  DomainElementInt64 elementZero(0);
  std::unique_ptr<DomainElementContainer> container(new DomainElementPairContainer(&lowerBound, &upperBound));
  
  container->subtract(&lowerBound);
  ASSERT_FALSE(container->contains(&lowerBound));
  
  container->subtract(&upperBound);
  ASSERT_FALSE(container->contains(&upperBound));
  
  ASSERT_TRUE(container->contains(&elementZero));
  container->subtract(&elementZero);
  ASSERT_FALSE(container->contains(&elementZero));
  
  // For bigger containers represented as list, subtraction might not affect elements
  DomainElementInt64 lowerBound2(-10000);
  DomainElementInt64 upperBound2(+10000);
  std::unique_ptr<DomainElementContainer> container2(new DomainElementPairContainer(&lowerBound2, &upperBound2));
  
  ASSERT_TRUE(container2->contains(&lowerBound2));
  ASSERT_TRUE(container2->contains(&upperBound2));
  ASSERT_TRUE(container2->contains(&elementZero));
  
  // Removing bounds should reduce the size of the container
  container2->subtract(&lowerBound2);
  container2->subtract(&upperBound2);
  ASSERT_FALSE(container2->contains(&lowerBound2));
  ASSERT_FALSE(container2->contains(&upperBound2));
  
  // Removing internal elements should not be possible
  container2->subtract(&elementZero);
  ASSERT_TRUE(container2->contains(&elementZero));
}//Subtract

OPTILAB_TEST(DomainElementPairContainer, SubtractContainer)
{
  using namespace Core;
  
  // Create a Int64 container for [-10..10]
  ContainerPtr container1 = getDomain(-10, +10);
  
  // Subtract out of bound containers should not change current container
  ContainerPtr container1OutMin = getDomain(-12, -11);
  ContainerPtr container1OutMax = getDomain(+11, +12);
  
  container1->subtract(container1OutMin.get());
  ASSERT_TRUE(DomainElement::isEqual(container1->lowerBound(), DomainElementManager::getInstance().createDomainElementInt64(-10)));
  ASSERT_TRUE(DomainElement::isEqual(container1->upperBound(), DomainElementManager::getInstance().createDomainElementInt64(+10)));
  
  container1->subtract(container1OutMax.get());
  ASSERT_TRUE(DomainElement::isEqual(container1->lowerBound(), DomainElementManager::getInstance().createDomainElementInt64(-10)));
  ASSERT_TRUE(DomainElement::isEqual(container1->upperBound(), DomainElementManager::getInstance().createDomainElementInt64(+10)));
  
  // Subtract boundaries containers should change lower and upper bounds
  ContainerPtr container1OutMinBound = getDomain(-12, -10);
  ContainerPtr container1OutMaxBound = getDomain(+10, +12);
  
  container1->subtract(container1OutMinBound.get());
  ASSERT_TRUE(DomainElement::isEqual(container1->lowerBound(), DomainElementManager::getInstance().createDomainElementInt64(-9)));
  ASSERT_TRUE(DomainElement::isEqual(container1->upperBound(), DomainElementManager::getInstance().createDomainElementInt64(+10)));
  
  container1->subtract(container1OutMaxBound.get());
  ASSERT_TRUE(DomainElement::isEqual(container1->lowerBound(), DomainElementManager::getInstance().createDomainElementInt64(-9)));
  ASSERT_TRUE(DomainElement::isEqual(container1->upperBound(), DomainElementManager::getInstance().createDomainElementInt64(+9)));
  
  // Subtract set of elements
  ContainerPtr container1Range = getDomain(5, +9);
  container1->subtract(container1Range.get());
  ASSERT_TRUE(DomainElement::isEqual(container1->lowerBound(), DomainElementManager::getInstance().createDomainElementInt64(-9)));
  ASSERT_TRUE(DomainElement::isEqual(container1->upperBound(), DomainElementManager::getInstance().createDomainElementInt64(+4)));
  // Size : 9 + 1 + 4
  ASSERT_EQ(container1->size(), 14);
  
  // Subtract internal set of elements
  ContainerPtr container1RangeInternal = getDomain(0, 3);
  container1->subtract(container1RangeInternal.get());
  // Bitset: remains the same but with less elements
  ASSERT_TRUE(DomainElement::isEqual(container1->lowerBound(), DomainElementManager::getInstance().createDomainElementInt64(-9)));
  ASSERT_TRUE(DomainElement::isEqual(container1->upperBound(), DomainElementManager::getInstance().createDomainElementInt64(+4)));
  // Size : (9 + 1 + 4) - (1 + 3) = 10
  ASSERT_EQ(container1->size(), 10);
  
  // Create a Int64 container for [-10000..10000]
  ContainerPtr container2 = getDomain(-10000, +10000);
}//SubtractContainer

OPTILAB_TEST(DomainElementInt64Container, Contains)
{
  using namespace Core;
  
  // Create a Int64 container for [-10..10]
  DomainElementInt64 lowerBound(-10);
  DomainElementInt64 upperBound( 10);
  std::unique_ptr<DomainElementContainer> container(new DomainElementInt64Container(&lowerBound, &upperBound));
  
  for(INT_64 elem = -15; elem <= 15; ++elem)
  {
    DomainElementInt64 domainElement(elem);
    if(elem < -10 || elem > 10)
    {
      ASSERT_FALSE(container->contains(&domainElement));
    }
    else
    {
      ASSERT_TRUE(container->contains(&domainElement));
    }
  }
}//Contains

OPTILAB_TEST(DomainElementInt64Container, InMin)
{
  using namespace Core;
  
  // Create a Int64 container for [-10..10]
  DomainElementInt64 lowerBound(-10);
  DomainElementInt64 upperBound( 10);
  DomainElementInt64 elementZero(0);
  std::unique_ptr<DomainElementContainer> container(new DomainElementInt64Container(&lowerBound, &upperBound));
  
  // Should not affect the container
  DomainElementInt64 lowerBound2(-20);
  container->inMin(&lowerBound2);
  ASSERT_TRUE(container->contains(&lowerBound));
  
  // Should change lower bound
  DomainElementInt64 lowerBound3(8);
  container->inMin(&lowerBound3);
  ASSERT_FALSE(container->contains(&lowerBound));
  ASSERT_TRUE(container->contains(&lowerBound3));
  
  container->inMin(&upperBound);
  ASSERT_FALSE(container->contains(&lowerBound3));
  ASSERT_TRUE(container->contains(&upperBound));
  
  DomainElementInt64 lowerBoundBig(-10000);
  DomainElementInt64 upperBoundBig(+10000);
  std::unique_ptr<DomainElementContainer> container2(new DomainElementInt64Container(&lowerBoundBig, &upperBoundBig));
  
  DomainElementInt64 lowerBoundBig2(9999);
  container2->inMin(&lowerBoundBig2);
  
  DomainElementInt64 randElem(500);
  DomainElementInt64 randElem2(9998);
  ASSERT_FALSE(container2->contains(&randElem));
  ASSERT_FALSE(container2->contains(&randElem2));
  ASSERT_TRUE(container2->contains(&lowerBoundBig2));
}//InMin

OPTILAB_TEST(DomainElementInt64Container, InMax)
{
  using namespace Core;
  
  // Create a Int64 container for [-10..10]
  DomainElementInt64 lowerBound(-10);
  DomainElementInt64 upperBound( 10);
  DomainElementInt64 elementZero(0);
  std::unique_ptr<DomainElementContainer> container(new DomainElementInt64Container(&lowerBound, &upperBound));
  
  // Should not affect the container
  DomainElementInt64 upperBound2(20);
  container->inMax(&upperBound2);
  ASSERT_TRUE(container->contains(&upperBound));
  
  // Should change upper bound
  DomainElementInt64 upperBound3(-2);
  container->inMax(&upperBound3);
  ASSERT_FALSE(container->contains(&upperBound));
  ASSERT_TRUE(container->contains(&upperBound3));
  
  container->inMax(&lowerBound);
  ASSERT_FALSE(container->contains(&upperBound3));
  ASSERT_TRUE(container->contains(&lowerBound));
  
  DomainElementInt64 lowerBoundBig(-10000);
  DomainElementInt64 upperBoundBig(+10000);
  std::unique_ptr<DomainElementContainer> container2(new DomainElementInt64Container(&lowerBoundBig, &upperBoundBig));
  
  DomainElementInt64 upperBoundBig2(-9999);
  container2->inMax(&upperBoundBig2);
  
  DomainElementInt64 randElem(-500);
  DomainElementInt64 randElem2(-9998);
  ASSERT_FALSE(container2->contains(&randElem));
  ASSERT_FALSE(container2->contains(&randElem2));
  ASSERT_TRUE(container2->contains(&upperBoundBig2));
}//InMax

OPTILAB_TEST(DomainElementInt64Container, IntersectContainer)
{
  using namespace Core;
  
  // Empty container -> intersection should be empty
  ContainerPtr container0 = getDomain(-10, +10);
  ContainerPtr emptyContainer = getDomain(-10, +10);
  emptyContainer->inMin(getElem(20).get());
  container0->intersect(emptyContainer.get());
  ASSERT_EQ(container0->size(), 0);
  
  // Create a Int64 container for [-10..10]
  ContainerPtr container1 = getDomain(-10, +10);
  ContainerPtr container2 = getDomain(-10, +10);
  
  // Intersect out of bound containers should empty domain
  ContainerPtr container1OutMin = getDomain(-12, -11);
  ContainerPtr container1OutMax = getDomain(+11, +12);
  
  container1->intersect(container1OutMin.get());
  container2->intersect(container1OutMax.get());
  ASSERT_EQ(container1->size(), 0);
  ASSERT_EQ(container2->size(), 0);
  
  // Intersect boundaries containers should let a singleton
  ContainerPtr container1OutMinBound = getDomain(-12, -10);
  ContainerPtr container1OutMaxBound = getDomain(+10, +12);
  
  ContainerPtr container3 = getDomain(-10, +10);
  ContainerPtr container4 = getDomain(-10, +10);
  
  container3->intersect(container1OutMinBound.get());
  ASSERT_EQ(container3->size(), 1);
  ASSERT_TRUE(DomainElement::isEqual(container3->lowerBound(), getElem(-10).get()));
  ASSERT_TRUE(DomainElement::isEqual(container3->upperBound(), getElem(-10).get()));
  
  container4->intersect(container1OutMaxBound.get());
  ASSERT_EQ(container4->size(), 1);
  ASSERT_TRUE(DomainElement::isEqual(container4->lowerBound(), getElem(+10).get()));
  ASSERT_TRUE(DomainElement::isEqual(container4->upperBound(), getElem(+10).get()));
  
  // Intersect set of elements
  ContainerPtr container5 = getDomain(-10, +10);
  std::vector<INT_64> set;
  set.push_back(-20);
  set.push_back(-15);
  set.push_back(-8);
  set.push_back(-5);
  set.push_back(0);
  set.push_back(+5);
  set.push_back(+10);
  set.push_back(+15);
  set.push_back(+20);
  
  ContainerPtr container5Aux = getDomain(set);
  container5->intersect(container5Aux.get());
  ASSERT_EQ(container5->size(), 5);
  ASSERT_TRUE(DomainElement::isEqual(container5->lowerBound(), getElem(-8).get()));
  ASSERT_TRUE(DomainElement::isEqual(container5->upperBound(), getElem(+10).get()));

  
  // Create a Int64 container for [-10000..10000]
  ContainerPtr container6 = getDomain(-10000, +10000);
  ContainerPtr container7 = getDomain(-10000, +10000);
  
  // Intersect out of bound containers should empty domain
  ContainerPtr container6OutMin = getDomain(-20500, -20000);
  ContainerPtr container7OutMax = getDomain(+20000, +20500);
  
  container6->intersect(container6OutMin.get());
  container7->intersect(container7OutMax.get());
  ASSERT_EQ(container6->size(), 0);
  ASSERT_EQ(container7->size(), 0);
  
  // Intersect boundaries containers should let a singleton
  ContainerPtr container8 = getDomain(-10000, +10000);
  ContainerPtr container9 = getDomain(-10000, +10000);
  ContainerPtr container8OutMinBound = getDomain(-10100, -10000);
  ContainerPtr container9OutMaxBound = getDomain(+10000, +10002);
  
  container8->intersect(container8OutMinBound.get());
  ASSERT_EQ(container8->size(), 1);
  ASSERT_TRUE(DomainElement::isEqual(container8->lowerBound(), getElem(-10000).get()));
  ASSERT_TRUE(DomainElement::isEqual(container8->upperBound(), getElem(-10000).get()));
  
  container9->intersect(container9OutMaxBound.get());
  ASSERT_EQ(container9->size(), 1);
  ASSERT_TRUE(DomainElement::isEqual(container9->lowerBound(), getElem(+10000).get()));
  ASSERT_TRUE(DomainElement::isEqual(container9->upperBound(), getElem(+10000).get()));

  ContainerPtr container10 = getDomain(-10000, +10000);
  ContainerPtr container11 = getDomain(+9000, +10000);
  container10->intersect(container11.get());
  ASSERT_TRUE(DomainElement::isEqual(container10->lowerBound(), getElem(+9000).get()));
  ASSERT_TRUE(DomainElement::isEqual(container10->upperBound(), getElem(+10000).get()));
  
  ContainerPtr container12 = getDomain(-10000, +10000);
  ContainerPtr container13 = getDomain(-10000, -9000);
  container12->intersect(container13.get());
  ASSERT_TRUE(DomainElement::isEqual(container12->lowerBound(), getElem(-10000).get()));
  ASSERT_TRUE(DomainElement::isEqual(container12->upperBound(), getElem(-9000).get()));
  
  ContainerPtr container14 = getDomain(-10000, +10000);
  ContainerPtr container15 = getDomain(-10, +10);
  container14->intersect(container15.get());
  ASSERT_TRUE(DomainElement::isEqual(container14->lowerBound(), getElem(-10).get()));
  ASSERT_TRUE(DomainElement::isEqual(container14->upperBound(), getElem(+10).get()));
  
  std::vector<INT_64> setDom;
  for(INT_64 elem = -1000; elem <= 1000; elem += 2)
  {
    setDom.push_back(elem);
  }
  ContainerPtr container16 = getDomain(setDom);
  ContainerPtr container17 = getDomain(setDom);
  
  // Intersect out of bound containers should empty domain
  ContainerPtr container16Aux = getDomain(-20500, -1001);
  ContainerPtr container17Aux = getDomain(+1001, +20500);
  
  container16->intersect(container16Aux.get());
  container17->intersect(container17Aux.get());
  ASSERT_EQ(container16->size(), 0);
  ASSERT_EQ(container17->size(), 0);
  
  // Intersect boundaries containers should let a singleton
  ContainerPtr container18 = getDomain(setDom);
  ContainerPtr container19 = getDomain(setDom);
  ContainerPtr container18OutMinBound = getDomain(-20500, -1000);
  ContainerPtr container19OutMaxBound = getDomain(+1000, +20000);
  
  container18->intersect(container18OutMinBound.get());
  ASSERT_EQ(container18->size(), 1);
  ASSERT_TRUE(DomainElement::isEqual(container18->lowerBound(), getElem(-1000).get()));
  ASSERT_TRUE(DomainElement::isEqual(container18->upperBound(), getElem(-1000).get()));
  
  container19->intersect(container19OutMaxBound.get());
  ASSERT_EQ(container19->size(), 1);
  ASSERT_TRUE(DomainElement::isEqual(container19->lowerBound(), getElem(+1000).get()));
  ASSERT_TRUE(DomainElement::isEqual(container19->upperBound(), getElem(+1000).get()));
  
  // Subtract set of elements
  ContainerPtr container20 = getDomain(setDom);
  ContainerPtr container20Range = getDomain(+900, +998);
  container20->intersect(container20Range.get());
  ASSERT_TRUE(DomainElement::isEqual(container20->lowerBound(), getElem(+900).get()));
  ASSERT_TRUE(DomainElement::isEqual(container20->upperBound(), getElem(+998).get()));
}//IntersectContainer

OPTILAB_TEST(DomainElementInt64Container, IntersectArray)
{
  using namespace Core;
  
  // Empty array -> intersection should be empty
  ContainerPtr container0 = getDomain(-10, +10);
  ArrayPtr emptyArray = getArray(0, -1);
  container0->intersect(emptyArray.get());
  ASSERT_EQ(container0->size(), 0);
  
  // Create a Int64 container for [-10..10]
  ContainerPtr container1 = getDomain(-10, +10);
  ContainerPtr container2 = getDomain(-10, +10);
  
  // Intersect out of bound containers should empty domain
  ArrayPtr container1OutMin = getArray(-12, -11);
  ArrayPtr container1OutMax = getArray(+11, +12);
  
  container1->intersect(container1OutMin.get());
  container2->intersect(container1OutMax.get());
  ASSERT_EQ(container1->size(), 0);
  ASSERT_EQ(container2->size(), 0);
  
  // Intersect boundaries containers should let a singleton
  ArrayPtr container1OutMinBound = getArray(-12, -10);
  ArrayPtr container1OutMaxBound = getArray(+10, +12);
  
  ContainerPtr container3 = getDomain(-10, +10);
  ContainerPtr container4 = getDomain(-10, +10);
  
  container3->intersect(container1OutMinBound.get());
  ASSERT_EQ(container3->size(), 1);
  ASSERT_TRUE(DomainElement::isEqual(container3->lowerBound(), getElem(-10).get()));
  ASSERT_TRUE(DomainElement::isEqual(container3->upperBound(), getElem(-10).get()));
  
  container4->intersect(container1OutMaxBound.get());
  ASSERT_EQ(container4->size(), 1);
  ASSERT_TRUE(DomainElement::isEqual(container4->lowerBound(), getElem(+10).get()));
  ASSERT_TRUE(DomainElement::isEqual(container4->upperBound(), getElem(+10).get()));
  
  // Intersect set of elements
  ContainerPtr container5 = getDomain(-10, +10);
  std::vector<INT_64> set;
  set.push_back(-20);
  set.push_back(-15);
  set.push_back(-8);
  set.push_back(-5);
  set.push_back(0);
  set.push_back(+5);
  set.push_back(+10);
  set.push_back(+15);
  set.push_back(+20);
  
  ArrayPtr container5Aux = getArray(set);
  container5->intersect(container5Aux.get());
  ASSERT_EQ(container5->size(), 5);
  ASSERT_TRUE(DomainElement::isEqual(container5->lowerBound(), getElem(-8).get()));
  ASSERT_TRUE(DomainElement::isEqual(container5->upperBound(), getElem(+10).get()));
  
  
  // Create a Int64 container for [-10000..10000]
  ContainerPtr container6 = getDomain(-10000, +10000);
  ContainerPtr container7 = getDomain(-10000, +10000);
  
  // Intersect out of bound containers should empty domain
  ArrayPtr container6OutMin = getArray(-20500, -20000);
  ArrayPtr container7OutMax = getArray(+20000, +20500);
  
  container6->intersect(container6OutMin.get());
  container7->intersect(container7OutMax.get());
  ASSERT_EQ(container6->size(), 0);
  ASSERT_EQ(container7->size(), 0);
  
  // Intersect boundaries containers should let a singleton
  ContainerPtr container8 = getDomain(-10000, +10000);
  ContainerPtr container9 = getDomain(-10000, +10000);
  ArrayPtr container8OutMinBound = getArray(-10100, -10000);
  ArrayPtr container9OutMaxBound = getArray(+10000, +10002);
  
  container8->intersect(container8OutMinBound.get());
  ASSERT_EQ(container8->size(), 1);
  ASSERT_TRUE(DomainElement::isEqual(container8->lowerBound(), getElem(-10000).get()));
  ASSERT_TRUE(DomainElement::isEqual(container8->upperBound(), getElem(-10000).get()));
  
  container9->intersect(container9OutMaxBound.get());
  ASSERT_EQ(container9->size(), 1);
  ASSERT_TRUE(DomainElement::isEqual(container9->lowerBound(), getElem(+10000).get()));
  ASSERT_TRUE(DomainElement::isEqual(container9->upperBound(), getElem(+10000).get()));
  
  ContainerPtr container10 = getDomain(-10000, +10000);
  ArrayPtr container11 = getArray(+9000, +10000);
  container10->intersect(container11.get());
  ASSERT_TRUE(DomainElement::isEqual(container10->lowerBound(), getElem(+9000).get()));
  ASSERT_TRUE(DomainElement::isEqual(container10->upperBound(), getElem(+10000).get()));
  
  ContainerPtr container12 = getDomain(-10000, +10000);
  ArrayPtr container13 = getArray(-10000, -9000);
  container12->intersect(container13.get());
  ASSERT_TRUE(DomainElement::isEqual(container12->lowerBound(), getElem(-10000).get()));
  ASSERT_TRUE(DomainElement::isEqual(container12->upperBound(), getElem(-9000).get()));
  
  ContainerPtr container14 = getDomain(-10000, +10000);
  ArrayPtr container15 = getArray(-10, +10);
  container14->intersect(container15.get());
  ASSERT_TRUE(DomainElement::isEqual(container14->lowerBound(), getElem(-10).get()));
  ASSERT_TRUE(DomainElement::isEqual(container14->upperBound(), getElem(+10).get()));
  
  std::vector<INT_64> setDom;
  for(INT_64 elem = -1000; elem <= 1000; elem += 2)
  {
    setDom.push_back(elem);
  }
  ContainerPtr container16 = getDomain(setDom);
  ContainerPtr container17 = getDomain(setDom);
  
  // Intersect out of bound containers should empty domain
  ArrayPtr container16Aux = getArray(-20500, -1001);
  ArrayPtr container17Aux = getArray(+1001, +20500);
  
  container16->intersect(container16Aux.get());
  container17->intersect(container17Aux.get());
  ASSERT_EQ(container16->size(), 0);
  ASSERT_EQ(container17->size(), 0);
  
  // Intersect boundaries containers should let a singleton
  ContainerPtr container18 = getDomain(setDom);
  ContainerPtr container19 = getDomain(setDom);
  ArrayPtr container18OutMinBound = getArray(-20500, -1000);
  ArrayPtr container19OutMaxBound = getArray(+1000, +20000);
  
  container18->intersect(container18OutMinBound.get());
  ASSERT_EQ(container18->size(), 1);
  ASSERT_TRUE(DomainElement::isEqual(container18->lowerBound(), getElem(-1000).get()));
  ASSERT_TRUE(DomainElement::isEqual(container18->upperBound(), getElem(-1000).get()));
  
  container19->intersect(container19OutMaxBound.get());
  ASSERT_EQ(container19->size(), 1);
  ASSERT_TRUE(DomainElement::isEqual(container19->lowerBound(), getElem(+1000).get()));
  ASSERT_TRUE(DomainElement::isEqual(container19->upperBound(), getElem(+1000).get()));
  
  // Subtract set of elements
  ContainerPtr container20 = getDomain(setDom);
  ArrayPtr container20Range = getArray(+900, +998);
  container20->intersect(container20Range.get());
  ASSERT_TRUE(DomainElement::isEqual(container20->lowerBound(), getElem(+900).get()));
  ASSERT_TRUE(DomainElement::isEqual(container20->upperBound(), getElem(+998).get()));
}//IntersectArray
#endif
