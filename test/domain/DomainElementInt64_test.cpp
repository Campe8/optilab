
#include "utest_globals.hpp"

#include "DomainElementKey.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementVoid.hpp"

#include <unordered_map>

OPTILAB_TEST(DomainElementInt64, Constructor)
{
  using namespace Core;
  
  // Test isa and cast
  DomainElement *domElemInt64_1000 = new DomainElementInt64(1000);
  DomainElementInt64 *domElemInt64Cast = DomainElementInt64::cast(domElemInt64_1000);
  DomainElementInt64 domElemZero(0);
  
  ASSERT_TRUE(DomainElementInt64::isa(&domElemZero));
  ASSERT_TRUE(DomainElementInt64::isa(domElemInt64_1000));
  ASSERT_NE(domElemInt64Cast, nullptr);
  ASSERT_TRUE(DomainElementInt64::isa(domElemInt64Cast));
  
  delete domElemInt64_1000;
}

OPTILAB_TEST(DomainElementInt64, ZeroELement)
{
  using namespace Core;
  
  // Test isa and cast
  DomainElement *domElemInt64_1000 = new DomainElementInt64(1000);
  DomainElement *domElemInt64_0 = domElemInt64_1000->zero();

  
  ASSERT_TRUE(DomainElementInt64::isa(domElemInt64_0));
  ASSERT_EQ(DomainElementInt64::cast(domElemInt64_0)->getValue(), 0);
  
  delete domElemInt64_1000;
}//ZeroELement

OPTILAB_TEST(DomainElementInt64, GetValue)
{
  using namespace Core;
  
  DomainElement *domElemInt64_1000 = new DomainElementInt64(1000);
  DomainElementInt64 *domElemInt64Cast = DomainElementInt64::cast(domElemInt64_1000);
  DomainElementInt64 domElemZero(0);
  
  // Test get value
  ASSERT_EQ(domElemInt64Cast->getValue(), 1000);
  ASSERT_EQ(domElemZero.getValue(), 0);
  
  // Test getDomainKey: different domain elements should have different keys
  ASSERT_FALSE((*(domElemInt64Cast->getDomainKey()) == *(domElemZero.getDomainKey())));
  
  // Test copy constructor and assignment operator
  DomainElementInt64 domElemCopy(domElemZero);
  ASSERT_EQ(domElemCopy.getValue(), 0);
  
  DomainElementInt64 domElemAssigned(1234);
  ASSERT_EQ(domElemAssigned.getValue(), 1234);
  
  domElemAssigned = domElemZero;
  ASSERT_EQ(domElemAssigned.getValue(), 0);
  
  ASSERT_TRUE(*(domElemCopy.getDomainKey()) == *(domElemCopy.getDomainKey()));
  ASSERT_TRUE(*(domElemAssigned.getDomainKey()) == *(domElemZero.getDomainKey()));
  
  delete domElemInt64_1000;
}

OPTILAB_TEST(DomainElementInt64, BoolMethods)
{
  using namespace Core;
  
  DomainElementInt64 domElemZero(0);
  DomainElementInt64 domElemCopy(domElemZero);
  DomainElement *domElemInt64_1000 = new DomainElementInt64(1000);
  DomainElement *domElemInt64ZeroCopy = new DomainElementInt64(domElemZero);
  
  ASSERT_TRUE(domElemCopy.isEqual(domElemInt64ZeroCopy));
  ASSERT_TRUE(domElemCopy.isNotEqual(domElemInt64_1000));
  ASSERT_TRUE(domElemCopy.isLessThan(domElemInt64_1000));
  ASSERT_TRUE(domElemCopy.isLessThanOrEqual(domElemInt64_1000));
  ASSERT_TRUE(domElemCopy.isLessThanOrEqual(domElemInt64ZeroCopy));
  
  delete domElemInt64_1000;
  delete domElemInt64ZeroCopy;
  
  DomainElementVoid voidElem;
  ASSERT_FALSE(domElemCopy.isEqual(&voidElem));
  ASSERT_TRUE(domElemCopy.isNotEqual(&voidElem));
}

OPTILAB_TEST(DomainElementInt64, NonBoolMethods)
{
  using namespace Core;
  
  DomainElementInt64 domElemZero(0);
  DomainElement *domElemInt64_1000 = new DomainElementInt64(1000);
  DomainElement *opDomElem = domElemZero.min(domElemInt64_1000);
  
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(domElemZero.getDomainKey()));
  
  opDomElem = domElemZero.max(domElemInt64_1000);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(domElemInt64_1000->getDomainKey()));
  
  opDomElem = domElemZero.mirror();
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(domElemZero.getDomainKey()));
  
  opDomElem = domElemZero.abs();
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(domElemZero.getDomainKey()));
  
  // Div should return void since no DomainElementCategoryMap has been set to check elements even if result is DomainElementInt64(0)
  opDomElem = domElemZero.div(domElemInt64_1000);
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(domElemZero.getDomainKey()));
  
  opDomElem = domElemZero.plus(domElemInt64_1000);
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(domElemInt64_1000->getDomainKey()));
  
  opDomElem = domElemZero.times(domElemInt64_1000);
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(domElemZero.getDomainKey()));
  
  delete domElemInt64_1000;
}

OPTILAB_TEST(DomainElementInt64, DomainElementCategoryMapMin)
{
  using namespace Core;
  
  DomainElement *domElemInt64_3 = new DomainElementInt64(3);
  DomainElement *domElemInt64_Neg3 = new DomainElementInt64(-3);
  DomainElement *domElemInt64_PosInf = new DomainElementInt64(std::numeric_limits<INT_64>::max());
  DomainElement *domElemInt64_NegInf = new DomainElementInt64(std::numeric_limits<INT_64>::lowest() + 1);
  
  DomainElement *opDomElem = nullptr;
  
  // Test min
  opDomElem = DomainElementInt64::cast(domElemInt64_Neg3)->min(domElemInt64_3);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_Neg3)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_3)->min(domElemInt64_3);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_3)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_3)->min(domElemInt64_Neg3);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_Neg3)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_3)->min(domElemInt64_PosInf);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_3)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_3)->min(domElemInt64_NegInf);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_NegInf)->getDomainKey()));
  
  delete domElemInt64_3;
  delete domElemInt64_Neg3;
  delete domElemInt64_PosInf;
  delete domElemInt64_NegInf;
}

OPTILAB_TEST(DomainElementInt64, DomainElementMax)
{
  using namespace Core;

  DomainElement *domElemInt64_3 = new DomainElementInt64(3);
  DomainElement *domElemInt64_Neg3 = new DomainElementInt64(-3);
  DomainElement *domElemInt64_PosInf = new DomainElementInt64(std::numeric_limits<INT_64>::max());
  DomainElement *domElemInt64_NegInf = new DomainElementInt64(std::numeric_limits<INT_64>::lowest() + 1);
  
  DomainElement *opDomElem = nullptr;
  
  opDomElem = DomainElementInt64::cast(domElemInt64_Neg3)->max(domElemInt64_3);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_3)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_3)->max(domElemInt64_3);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_3)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_3)->max(domElemInt64_Neg3);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_3)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_3)->max(domElemInt64_PosInf);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_PosInf)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_3)->max(domElemInt64_NegInf);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_3)->getDomainKey()));
  
  delete domElemInt64_3;
  delete domElemInt64_Neg3;
  delete domElemInt64_PosInf;
  delete domElemInt64_NegInf;
}

OPTILAB_TEST(DomainElementInt64, DomainElementAbs)
{
  using namespace Core;
  
  DomainElement *domElemInt64_3 = new DomainElementInt64(3);
  DomainElement *domElemInt64_Neg3 = new DomainElementInt64(-3);
  DomainElement *domElemInt64_PosInf = new DomainElementInt64(std::numeric_limits<INT_64>::max());
  DomainElement *domElemInt64_NegInf = new DomainElementInt64(std::numeric_limits<INT_64>::lowest() + 1);
  
  DomainElement *opDomElem = nullptr;
  
  opDomElem = DomainElementInt64::cast(domElemInt64_Neg3)->abs();
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_3)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_3)->abs();
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_3)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_NegInf)->abs();
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_PosInf)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_PosInf)->abs();
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_PosInf)->getDomainKey()));
  
  delete domElemInt64_3;
  delete domElemInt64_Neg3;
  delete domElemInt64_PosInf;
  delete domElemInt64_NegInf;
}//DomainElementAbs

OPTILAB_TEST(DomainElementInt64, DomainElementMirror)
{
  using namespace Core;
  
  DomainElement *domElemInt64_3 = new DomainElementInt64(3);
  DomainElement *domElemInt64_Neg3 = new DomainElementInt64(-3);
  DomainElement *domElemInt64_PosInf = new DomainElementInt64(std::numeric_limits<INT_64>::max());
  DomainElement *domElemInt64_NegInf = new DomainElementInt64(std::numeric_limits<INT_64>::lowest() + 1);
  
  DomainElement *opDomElem = nullptr;
  
  opDomElem = DomainElementInt64::cast(domElemInt64_Neg3)->mirror();
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) ==
              *(DomainElementInt64::cast(domElemInt64_3)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_3)->mirror();
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) ==
              *(DomainElementInt64::cast(domElemInt64_Neg3)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_NegInf)->mirror();
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) ==
              *(DomainElementInt64::cast(domElemInt64_PosInf)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_PosInf)->mirror();
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) ==
              *(DomainElementInt64::cast(domElemInt64_NegInf)->getDomainKey()));
  
  delete domElemInt64_3;
  delete domElemInt64_Neg3;
  delete domElemInt64_PosInf;
  delete domElemInt64_NegInf;
}//DomainElementMirror

OPTILAB_TEST(DomainElementInt64, DomainElementDiv)
{
  using namespace Core;
  
  DomainElement *domElemInt64_0 = new DomainElementInt64(0);
  DomainElement *domElemInt64_3 = new DomainElementInt64(3);
  DomainElement *domElemInt64_4 = new DomainElementInt64(4);
  DomainElement *domElemInt64_12 = new DomainElementInt64(12);
  DomainElement *domElemInt64_Neg3 = new DomainElementInt64(-3);
  DomainElement *domElemInt64_Neg4 = new DomainElementInt64(-4);
  DomainElement *domElemInt64_PosInf = new DomainElementInt64(std::numeric_limits<INT_64>::max());
  DomainElement *domElemInt64_NegInf = new DomainElementInt64(std::numeric_limits<INT_64>::lowest() + 1);
  
  DomainElement *opDomElem = nullptr;
  
  opDomElem = DomainElementInt64::cast(domElemInt64_12)->div(domElemInt64_4);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_3)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_12)->div(domElemInt64_Neg4);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_Neg3)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_PosInf)->div(domElemInt64_NegInf);
  
  DomainElementInt64 elementInt64_neg1(-1);
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(&elementInt64_neg1)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_NegInf)->div(domElemInt64_PosInf);
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(&elementInt64_neg1)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_12)->div(domElemInt64_0);
  ASSERT_TRUE(DomainElementVoid::isa(opDomElem));
  
  delete domElemInt64_0;
  delete domElemInt64_3;
  delete domElemInt64_4;
  delete domElemInt64_12;
  delete domElemInt64_Neg3;
  delete domElemInt64_Neg4;
  delete domElemInt64_PosInf;
  delete domElemInt64_NegInf;
}

OPTILAB_TEST(DomainElementInt64, DomainElementPlus)
{
  using namespace Core;
  
  DomainElement *domElemInt64_0 = new DomainElementInt64(0);
  DomainElement *domElemInt64_3 = new DomainElementInt64(3);
  DomainElement *domElemInt64_4 = new DomainElementInt64(4);
  DomainElement *domElemInt64_7 = new DomainElementInt64(7);
  DomainElement *domElemInt64_12 = new DomainElementInt64(12);
  DomainElement *domElemInt64_Neg3 = new DomainElementInt64(-3);
  DomainElement *domElemInt64_Neg4 = new DomainElementInt64(-4);
  DomainElement *domElemInt64_Neg7 = new DomainElementInt64(-7);
  DomainElement *domElemInt64_Neg12 = new DomainElementInt64(-12);
  DomainElement *opDomElem = nullptr;
  
  opDomElem = DomainElementInt64::cast(domElemInt64_12)->plus(domElemInt64_0);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_12)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_Neg12)->plus(domElemInt64_0);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_Neg12)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_3)->plus(domElemInt64_4);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_7)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_7)->plus(domElemInt64_Neg4);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_3)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_Neg7)->plus(domElemInt64_4);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_Neg3)->getDomainKey()));
  
  delete domElemInt64_0;
  delete domElemInt64_3;
  delete domElemInt64_4;
  delete domElemInt64_7;
  delete domElemInt64_12;
  delete domElemInt64_Neg3;
  delete domElemInt64_Neg4;
  delete domElemInt64_Neg7;
  delete domElemInt64_Neg12;
}

OPTILAB_TEST(DomainElementInt64, DomainElementTimes)
{
  using namespace Core;
  
  DomainElement *domElemInt64_0 = new DomainElementInt64(0);
  DomainElement *domElemInt64_3 = new DomainElementInt64(3);
  DomainElement *domElemInt64_4 = new DomainElementInt64(4);
  DomainElement *domElemInt64_12 = new DomainElementInt64(12);
  DomainElement *domElemInt64_Neg3 = new DomainElementInt64(-3);
  DomainElement *domElemInt64_Neg4 = new DomainElementInt64(-4);
  DomainElement *domElemInt64_Neg12 = new DomainElementInt64(-12);
  DomainElement *opDomElem = nullptr;
  
  opDomElem = DomainElementInt64::cast(domElemInt64_12)->times(domElemInt64_0);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_0)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_Neg12)->times(domElemInt64_0);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_0)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_3)->times(domElemInt64_4);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_12)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_3)->times(domElemInt64_Neg4);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_Neg12)->getDomainKey()));
  
  opDomElem = DomainElementInt64::cast(domElemInt64_Neg3)->times(domElemInt64_Neg4);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_12)->getDomainKey()));
  
  delete domElemInt64_0;
  delete domElemInt64_3;
  delete domElemInt64_4;
  delete domElemInt64_12;
  delete domElemInt64_Neg3;
  delete domElemInt64_Neg4;
  delete domElemInt64_Neg12;
}

OPTILAB_TEST(DomainElementInt64, DomainElementSuccessor)
{
  using namespace Core;
  
  DomainElementInt64 domElemInt64_Neg2(-2);
  DomainElementInt64 domElemInt64_Pos2(+2);
  DomainElementInt64 domElemInt64_Neg1(-1);
  DomainElementInt64 domElemInt64_Pos3(+3);
  DomainElementInt64 domElemInt64_PosInf(std::numeric_limits<INT_64>::max());
  
  ASSERT_TRUE(*(domElemInt64_Neg2.successor()->getDomainKey()) ==
              *(domElemInt64_Neg1.getDomainKey()));
  
  ASSERT_TRUE(*(domElemInt64_Pos2.successor()->getDomainKey()) ==
              *(domElemInt64_Pos3.getDomainKey()));
  
  ASSERT_TRUE(*(domElemInt64_PosInf.successor()->getDomainKey()) ==
              *(domElemInt64_PosInf.getDomainKey()));
}//DomainElementSuccessor

OPTILAB_TEST(DomainElementInt64, DomainElementPredecessor)
{
  using namespace Core;
  
  DomainElementInt64 domElemInt64_Neg2(-2);
  DomainElementInt64 domElemInt64_Pos2(+2);
  DomainElementInt64 domElemInt64_Neg1(-1);
  DomainElementInt64 domElemInt64_Pos3(+3);
  DomainElementInt64 domElemInt64_NegInf(std::numeric_limits<INT_64>::lowest() + 1);
  
  ASSERT_TRUE(*(domElemInt64_Neg1.predecessor()->getDomainKey()) ==
              *(domElemInt64_Neg2.getDomainKey()));
  
  ASSERT_TRUE(*(domElemInt64_Pos3.predecessor()->getDomainKey()) ==
              *(domElemInt64_Pos2.getDomainKey()));
  
  ASSERT_TRUE(*(domElemInt64_NegInf.predecessor()->getDomainKey()) ==
              *(domElemInt64_NegInf.getDomainKey()));
}//DomainElementPredecessor

OPTILAB_TEST(DomainElementInt64, ToString)
{
  using namespace Core;
  
  DomainElementInt64 domElemInt64_Pos(+10);
  DomainElementInt64 domElemInt64_Neg(-10);
  
  ASSERT_EQ(domElemInt64_Pos.toString(), "10");
  ASSERT_EQ(domElemInt64_Neg.toString(), "-10");
}//ToString

OPTILAB_TEST(DomainElementInt64, sizeSpan)
{
  using namespace Core;
  
  DomainElementInt64 domElemInt64_Neg4(-4);
  DomainElementInt64 domElemInt64_Neg2(-2);
  ASSERT_EQ(domElemInt64_Neg4.sizeSpan(&domElemInt64_Neg2), 3);
  
  
  DomainElementInt64 domElemInt64_Neg3(-3);
  DomainElementInt64 domElemInt64_Pos3(+3);
  ASSERT_EQ(domElemInt64_Neg3.sizeSpan(&domElemInt64_Pos3), 7);
  
  DomainElementInt64 domElemInt64_Pos10(+10);
  DomainElementInt64 domElemInt64_Pos20(+20);
  ASSERT_EQ(domElemInt64_Pos10.sizeSpan(&domElemInt64_Pos20), 11);
}//sizeSpan
