
#include "utest_globals.hpp"

#include "DomainElementInt64.hpp"
#include "DomainElementVoid.hpp"
#include "DomainElementKey.hpp"

#include <memory>
#include <unordered_map>

OPTILAB_TEST(DomainElementVoid, Constructor)
{
  using namespace Core;
  
  DomainElementVoid domElemVoid;
  std::unique_ptr<DomainElement> domElemVoidCast(new DomainElementVoid());
  ASSERT_TRUE(DomainElementVoid::isa(&domElemVoid));
  ASSERT_TRUE(DomainElementVoid::isa(domElemVoidCast.get()));
}

OPTILAB_TEST(DomainElementVoid, Methods)
{
  using namespace Core;
  
  DomainElementVoid domElemVoid;
  
  std::unique_ptr<DomainElement> domElemVoidCast(new DomainElementVoid());
  std::unique_ptr<DomainElement> domElemInt64_3(new DomainElementInt64(3));
  
  ASSERT_TRUE(domElemVoid.isEqual(domElemVoidCast.get()));
  ASSERT_FALSE(domElemVoid.isEqual(domElemInt64_3.get()));
  
  ASSERT_FALSE(domElemVoid.isNotEqual(domElemVoidCast.get()));
  ASSERT_TRUE(domElemVoid.isNotEqual(domElemInt64_3.get()));
  
  ASSERT_FALSE(domElemVoid.isLessThan(domElemVoidCast.get()));
  ASSERT_FALSE(domElemVoid.isLessThan(domElemInt64_3.get()));
  
  ASSERT_FALSE(domElemVoid.isLessThanOrEqual(domElemVoidCast.get()));
  ASSERT_FALSE(domElemVoid.isLessThanOrEqual(domElemInt64_3.get()));
  
  ASSERT_TRUE(DomainElementVoid::isa(domElemVoid.min(domElemVoidCast.get())));
  ASSERT_TRUE(DomainElementVoid::isa(domElemVoid.min(domElemInt64_3.get())));
  
  ASSERT_TRUE(DomainElementVoid::isa(domElemVoid.max(domElemVoidCast.get())));
  ASSERT_TRUE(DomainElementVoid::isa(domElemVoid.max(domElemInt64_3.get())));
  
  ASSERT_TRUE(DomainElementVoid::isa(domElemVoid.zero()));
  
  ASSERT_TRUE(DomainElementVoid::isa(domElemVoid.mirror()));
  
  ASSERT_TRUE(DomainElementVoid::isa(domElemVoid.abs()));
  
  ASSERT_TRUE(DomainElementVoid::isa(domElemVoid.div(domElemVoidCast.get())));
  ASSERT_TRUE(DomainElementVoid::isa(domElemVoid.div(domElemInt64_3.get())));
  
  ASSERT_TRUE(DomainElementVoid::isa(domElemVoid.plus(domElemVoidCast.get())));
  ASSERT_TRUE(DomainElementVoid::isa(domElemVoid.plus(domElemInt64_3.get())));
  
  ASSERT_TRUE(DomainElementVoid::isa(domElemVoid.times(domElemVoidCast.get())));
  ASSERT_TRUE(DomainElementVoid::isa(domElemVoid.times(domElemInt64_3.get())));

  ASSERT_TRUE(DomainElementVoid::isa(domElemVoid.predecessor()));
  ASSERT_TRUE(DomainElementVoid::isa(domElemVoid.successor()));
}

OPTILAB_TEST(DomainElementVoid, ToString)
{
  using namespace Core;
  
  DomainElementVoid domElemVoid;
  ASSERT_EQ(domElemVoid.toString(), "");
}//ToString

OPTILAB_TEST(DomainElementVoid, sizeSpan)
{
  using namespace Core;
  
  DomainElementVoid domElemVoid;
  ASSERT_EQ(domElemVoid.sizeSpan(&domElemVoid), 0);
}//sizeSpan

