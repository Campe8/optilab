
#include "utest_globals.hpp"

#include "DomainEventBound.hpp"
#include "DomainEventFail.hpp"
#include "DomainEventVoid.hpp"
#include "DomainEventChange.hpp"
#include "DomainEventLwb.hpp"
#include "DomainEventUpb.hpp"
#include "DomainEventSingleton.hpp"
#include "DomainEventUndef.hpp"

OPTILAB_TEST(DomainEventLwb, Basics)
{
  using namespace Core;
  
  // Check type
  std::unique_ptr<DomainEvent> event(new DomainEventLwb());
  ASSERT_TRUE(event->getEventType() == DomainEventType::LWB_EVENT);
  
  // Check isa and cast
  DomainEventLwb *castEvent = DomainEventLwb::cast(event.get());
  ASSERT_TRUE(DomainEventLwb::isa(event.get()));
  ASSERT_TRUE(DomainEventLwb::isa(castEvent));
}// Basics

OPTILAB_TEST(DomainEventLwb, overlap)
{
  using namespace Core;
  
  std::unique_ptr<DomainEvent> eventUndef(new DomainEventUndef());
  std::unique_ptr<DomainEvent> eventBound(new DomainEventBound());
  std::unique_ptr<DomainEvent> eventFail(new DomainEventFail());
  std::unique_ptr<DomainEvent> eventVoid(new DomainEventVoid());
  std::unique_ptr<DomainEvent> eventChange(new DomainEventBound());
  std::unique_ptr<DomainEvent> eventLwb(new DomainEventLwb());
  std::unique_ptr<DomainEvent> eventUbp(new DomainEventUpb());
  std::unique_ptr<DomainEvent> eventSingleton(new DomainEventSingleton());
  
  ASSERT_TRUE(eventLwb->overlaps(eventChange.get()));
  ASSERT_TRUE(eventLwb->overlaps(eventLwb.get()));
  ASSERT_FALSE(eventLwb->overlaps(eventUbp.get()));
  ASSERT_TRUE(eventLwb->overlaps(eventBound.get()));
  ASSERT_FALSE(eventLwb->overlaps(eventFail.get()));
  ASSERT_FALSE(eventLwb->overlaps(eventVoid.get()));
  ASSERT_FALSE(eventLwb->overlaps(eventSingleton.get()));
  ASSERT_FALSE(eventLwb->overlaps(eventUndef.get()));
}// overlap
