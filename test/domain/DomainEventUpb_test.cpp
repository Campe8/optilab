
#include "utest_globals.hpp"

#include "DomainEventBound.hpp"
#include "DomainEventFail.hpp"
#include "DomainEventVoid.hpp"
#include "DomainEventChange.hpp"
#include "DomainEventLwb.hpp"
#include "DomainEventUpb.hpp"
#include "DomainEventSingleton.hpp"
#include "DomainEventUndef.hpp"

OPTILAB_TEST(DomainEventUpb, Basics)
{
  using namespace Core;
  
  // Check type
  DomainEvent *event = new DomainEventUpb();
  ASSERT_TRUE(event->getEventType() == DomainEventType::UPB_EVENT);
  
  // Check isa and cast
  DomainEventUpb *castEvent = DomainEventUpb::cast(event);
  ASSERT_TRUE(DomainEventUpb::isa(event));
  ASSERT_TRUE(DomainEventUpb::isa(castEvent));
  
  delete event;
}// Basics

OPTILAB_TEST(DomainEventUpb, overlap)
{
  using namespace Core;
  
  std::unique_ptr<DomainEvent> eventUndef(new DomainEventUndef());
  std::unique_ptr<DomainEvent> eventBound(new DomainEventBound());
  std::unique_ptr<DomainEvent> eventFail(new DomainEventFail());
  std::unique_ptr<DomainEvent> eventVoid(new DomainEventVoid());
  std::unique_ptr<DomainEvent> eventChange(new DomainEventBound());
  std::unique_ptr<DomainEvent> eventLwb(new DomainEventLwb());
  std::unique_ptr<DomainEvent> eventUbp(new DomainEventUpb());
  std::unique_ptr<DomainEvent> eventSingleton(new DomainEventSingleton());
  
  ASSERT_TRUE(eventUbp->overlaps(eventChange.get()));
  ASSERT_FALSE(eventUbp->overlaps(eventLwb.get()));
  ASSERT_TRUE(eventUbp->overlaps(eventUbp.get()));
  ASSERT_TRUE(eventUbp->overlaps(eventBound.get()));
  ASSERT_FALSE(eventUbp->overlaps(eventFail.get()));
  ASSERT_FALSE(eventUbp->overlaps(eventVoid.get()));
  ASSERT_FALSE(eventUbp->overlaps(eventSingleton.get()));
  ASSERT_FALSE(eventUbp->overlaps(eventUndef.get()));
}// overlap
