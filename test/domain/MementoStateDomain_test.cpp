
#include "utest_globals.hpp"

#include "DomainConcreteStorageBounds.hpp"
#include "DomainConcreteStorageBitmap.hpp"
#include "DomainConcreteStorageSet.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"
#include "DomainStateInt64Container.hpp"
#include "MementoStateDomain.hpp"

OPTILAB_TEST(MementoStateDomain, Basics)
{
  using namespace Core;
  using namespace Search;
  
  std::unique_ptr<DomainStateInt64Container> dc(new DomainStateInt64Container());
  
  MementoState* mementoState = new MementoStateDomain(std::move(dc));
  
  ASSERT_TRUE(MementoStateDomain::isa(mementoState));
  MementoStateDomain* mementoStateDomain = MementoStateDomain::cast(mementoState);
  ASSERT_TRUE(mementoStateDomain);
  ASSERT_TRUE(mementoStateDomain->getMementoStateId() == MementoStateId::MS_DOMAIN);
  
  
  delete mementoStateDomain;
}//Basics

OPTILAB_TEST(MementoStateDomain, GetState)
{
  using namespace Core;
  using namespace Search;
  
  std::unique_ptr<DomainStateInt64Container> dc(new DomainStateInt64Container());
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  DomainElementInt64 lowerBound(0);
  DomainElementInt64 upperBound(10);
  DomainConcreteStorageBounds storage(&lowerBound, &upperBound, &elementManager);
  dc->setInternalState(&storage);
  
  std::unique_ptr<MementoStateDomain> mementoState(new MementoStateDomain(std::move(dc)));
  
  auto state = mementoState->getDomainState();
  ASSERT_TRUE(state);
  ASSERT_TRUE(state->getDomainStateType() == DomainStateType::DOM_STATE_INT64_CONT);
  
  DomainStateInt64Container* dcInt64 = DomainStateInt64Container::cast(state.get());
  ASSERT_TRUE(dcInt64->validState());
  
  std::unique_ptr<DomainConcreteStorage> storage2 = dcInt64->getInternalStateCopy();
  ASSERT_TRUE(storage2);
  
  // Storage fro container should be a copy of the orginal storage
  ASSERT_EQ(storage2->getSize(), 11);
  ASSERT_TRUE(storage2->getLowerBound()->isEqual(&lowerBound));
  ASSERT_TRUE(storage2->getUpperBound()->isEqual(&upperBound));
}//GetState

