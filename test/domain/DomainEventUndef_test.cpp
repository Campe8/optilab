
#include "utest_globals.hpp"

#include "DomainEventUndef.hpp"
#include "DomainEventBound.hpp"
#include "DomainEventFail.hpp"
#include "DomainEventVoid.hpp"
#include "DomainEventChange.hpp"
#include "DomainEventSingleton.hpp"
#include "DomainEventLwb.hpp"
#include "DomainEventUpb.hpp"

OPTILAB_TEST(DomainEventUndef, Basics)
{
  using namespace Core;
  
  // Check type
  std::unique_ptr<DomainEvent> event(new DomainEventUndef());
  ASSERT_TRUE(event->getEventType() == DomainEventType::UNDEF_EVENT);
  
  // Check isa and cast
  DomainEventUndef *castEvent = DomainEventUndef::cast(event.get());
  ASSERT_TRUE(DomainEventUndef::isa(event.get()));
  ASSERT_TRUE(DomainEventUndef::isa(castEvent));
}// Basics

OPTILAB_TEST(DomainEventUndef, overlap)
{
  using namespace Core;
  
  std::unique_ptr<DomainEvent> eventUndef(new DomainEventUndef());
  std::unique_ptr<DomainEvent> eventBound(new DomainEventBound());
  std::unique_ptr<DomainEvent> eventFail(new DomainEventFail());
  std::unique_ptr<DomainEvent> eventVoid(new DomainEventVoid());
  std::unique_ptr<DomainEvent> eventChange(new DomainEventBound());
  std::unique_ptr<DomainEvent> eventLwb(new DomainEventLwb());
  std::unique_ptr<DomainEvent> eventUbp(new DomainEventUpb());
  std::unique_ptr<DomainEvent> eventSingleton(new DomainEventSingleton());
  
  ASSERT_FALSE(eventUndef->overlaps(eventChange.get()));
  ASSERT_FALSE(eventUndef->overlaps(eventLwb.get()));
  ASSERT_FALSE(eventUndef->overlaps(eventUbp.get()));
  ASSERT_FALSE(eventUndef->overlaps(eventBound.get()));
  ASSERT_FALSE(eventUndef->overlaps(eventFail.get()));
  ASSERT_FALSE(eventUndef->overlaps(eventVoid.get()));
  ASSERT_FALSE(eventUndef->overlaps(eventSingleton.get()));
  ASSERT_TRUE(eventUndef->overlaps(eventUndef.get()));
}// overlap

