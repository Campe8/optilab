
#include "utest_globals.hpp"

#include "DArrayMatrix.hpp"
#include "DomainElementManager.hpp"
#include "DomainElementInt64.hpp"

OPTILAB_TEST(DomainElementMatrix2D, Basics)
{
  using namespace Core;
  
  // Create a DomainElementMatrix2D of size 2 x 3
  DomainElementMatrix2D domMatrix(2, 3);
  
  ASSERT_EQ(domMatrix.numRows(), 2);
  ASSERT_EQ(domMatrix.numCols(), 3);
  ASSERT_EQ(domMatrix.size(), 6);
  
  for(std::size_t rowIdx = 0; rowIdx < domMatrix.numRows(); ++rowIdx)
  {
    for(std::size_t colIdx = 0; colIdx < domMatrix.numCols(); ++colIdx)
    {
      ASSERT_FALSE(domMatrix[rowIdx][colIdx]);
    }
  }
}//Basics

OPTILAB_TEST(DomainElementMatrix2D, DomainElementAssignment)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  
  // Create a DomainElementArray of size 3
  DomainElementMatrix2D domMatrix(2, 3);
  
  for(std::size_t rowIdx = 0; rowIdx < domMatrix.numRows(); ++rowIdx)
  {
    for(std::size_t colIdx = 0; colIdx < domMatrix.numCols(); ++colIdx)
    {
      domMatrix.assignElementToCell(rowIdx, colIdx, elementManager.createDomainElementInt64(rowIdx * domMatrix.numRows() + colIdx));
    }
  }
  ASSERT_EQ(domMatrix.size(), 6);
  
  for(std::size_t rowIdx = 0; rowIdx < domMatrix.numRows(); ++rowIdx)
  {
    for(std::size_t colIdx = 0; colIdx < domMatrix.numCols(); ++colIdx)
    {
      ASSERT_TRUE(DomainElement::isEqual(elementManager.createDomainElementInt64(rowIdx * domMatrix.numRows() + colIdx),
                                         domMatrix[rowIdx][colIdx]));
    }
  }
  ASSERT_FALSE(domMatrix.empty());
  
  domMatrix.clear();
  for(std::size_t rowIdx = 0; rowIdx < domMatrix.numRows(); ++rowIdx)
  {
    for(std::size_t colIdx = 0; colIdx < domMatrix.numCols(); ++colIdx)
    {
      ASSERT_TRUE(domMatrix[rowIdx][colIdx] == nullptr);
    }
  }
  ASSERT_TRUE(domMatrix.empty());
}//DomainElementAssignment

OPTILAB_TEST(DomainElementMatrix2D, DomainElementFind)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  
  // Create a DomainElementArray of size 3
  DomainElementMatrix2D domMatrix(2, 3);
  
  INT_64 elemToStore{0};
  for(std::size_t rowIdx = 0; rowIdx < domMatrix.numRows(); ++rowIdx)
  {
    for(std::size_t colIdx = 0; colIdx < domMatrix.numCols(); ++colIdx)
    {
      domMatrix.assignElementToCell(rowIdx, colIdx, elementManager.createDomainElementInt64(elemToStore++));
    }
  }
  
  elemToStore = 0;
  for(std::size_t rowIdx = 0; rowIdx < domMatrix.numRows(); ++rowIdx)
  {
    for(std::size_t colIdx = 0; colIdx < domMatrix.numCols(); ++colIdx)
    {
      auto findIdx = domMatrix.find(elementManager.createDomainElementInt64(elemToStore++));
      ASSERT_EQ(findIdx.first, rowIdx);
      ASSERT_EQ(findIdx.second, colIdx);
    }
  }
  
  auto findIdx = domMatrix.find(elementManager.createDomainElementInt64(-1));
  ASSERT_EQ(findIdx.first, domMatrix.numRows());
  ASSERT_EQ(findIdx.second, domMatrix.numCols());
  
  findIdx = domMatrix.find(elementManager.createDomainElementInt64(+9));
  ASSERT_EQ(findIdx.first, domMatrix.numRows());
  ASSERT_EQ(findIdx.second, domMatrix.numCols());
}//DomainElementFind

OPTILAB_TEST(DomainElementMatrix2D, CopyConstructorAssignmentOperator)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  
  // Create a DomainElementArray of size 3
  DomainElementMatrix2D domMatrix(2, 3);
  
  for(std::size_t rowIdx = 0; rowIdx < domMatrix.numRows(); ++rowIdx)
  {
    for(std::size_t colIdx = 0; colIdx < domMatrix.numCols(); ++colIdx)
    {
      domMatrix.assignElementToCell(rowIdx, colIdx, elementManager.createDomainElementInt64(rowIdx * domMatrix.numRows() + colIdx));
    }
  }
  
  DomainElementMatrix2D domMatrixCopy = domMatrix;
  ASSERT_EQ(domMatrixCopy.size(), 6);
  
  for(std::size_t rowIdx = 0; rowIdx < domMatrix.numRows(); ++rowIdx)
  {
    for(std::size_t colIdx = 0; colIdx < domMatrix.numCols(); ++colIdx)
    {
      ASSERT_TRUE(DomainElement::isEqual(elementManager.createDomainElementInt64(rowIdx * domMatrix.numRows() + colIdx),
                                         domMatrixCopy[rowIdx][colIdx]));
    }
  }
  
  DomainElementMatrix2D domMatrixAssignment(10, 20);
  domMatrixAssignment = domMatrix;
  ASSERT_EQ(domMatrixAssignment.size(), 6);
  
  for(std::size_t rowIdx = 0; rowIdx < domMatrix.numRows(); ++rowIdx)
  {
    for(std::size_t colIdx = 0; colIdx < domMatrix.numCols(); ++colIdx)
    {
      ASSERT_TRUE(DomainElement::isEqual(elementManager.createDomainElementInt64(rowIdx * domMatrix.numRows() + colIdx),
                                         domMatrixAssignment[rowIdx][colIdx]));
    }
  }
}//CopyConstructorAssignmentOperator
