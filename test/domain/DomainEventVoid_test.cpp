
#include "utest_globals.hpp"

#include "DomainEventBound.hpp"
#include "DomainEventFail.hpp"
#include "DomainEventVoid.hpp"
#include "DomainEventChange.hpp"
#include "DomainEventLwb.hpp"
#include "DomainEventUpb.hpp"
#include "DomainEventSingleton.hpp"
#include "DomainEventUndef.hpp"

OPTILAB_TEST(DomainEventVoid, Basics)
{
  using namespace Core;
  
  // Check type
  DomainEvent *event = new DomainEventVoid();
  ASSERT_TRUE(event->getEventType() == DomainEventType::VOID_EVENT);
  
  // Check isa and cast
  DomainEventVoid *castEvent = DomainEventVoid::cast(event);
  ASSERT_TRUE(DomainEventVoid::isa(event));
  ASSERT_TRUE(DomainEventVoid::isa(castEvent));
  
  delete event;
}// Basics

OPTILAB_TEST(DomainEventVoid, overlap)
{
  using namespace Core;
  
  std::unique_ptr<DomainEvent> eventUndef(new DomainEventUndef());
  std::unique_ptr<DomainEvent> eventBound(new DomainEventBound());
  std::unique_ptr<DomainEvent> eventFail(new DomainEventFail());
  std::unique_ptr<DomainEvent> eventVoid(new DomainEventVoid());
  std::unique_ptr<DomainEvent> eventChange(new DomainEventBound());
  std::unique_ptr<DomainEvent> eventLwb(new DomainEventLwb());
  std::unique_ptr<DomainEvent> eventUbp(new DomainEventUpb());
  std::unique_ptr<DomainEvent> eventSingleton(new DomainEventSingleton());
  
  ASSERT_FALSE(eventVoid->overlaps(eventChange.get()));
  ASSERT_FALSE(eventVoid->overlaps(eventLwb.get()));
  ASSERT_FALSE(eventVoid->overlaps(eventUbp.get()));
  ASSERT_FALSE(eventVoid->overlaps(eventBound.get()));
  ASSERT_FALSE(eventVoid->overlaps(eventFail.get()));
  ASSERT_TRUE(eventVoid->overlaps(eventVoid.get()));
  ASSERT_FALSE(eventVoid->overlaps(eventSingleton.get()));
  ASSERT_FALSE(eventVoid->overlaps(eventUndef.get()));
}// overlap
