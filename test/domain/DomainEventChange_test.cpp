
#include "utest_globals.hpp"

#include "DomainEventBound.hpp"
#include "DomainEventFail.hpp"
#include "DomainEventVoid.hpp"
#include "DomainEventChange.hpp"
#include "DomainEventLwb.hpp"
#include "DomainEventUpb.hpp"
#include "DomainEventSingleton.hpp"
#include "DomainEventUndef.hpp"

OPTILAB_TEST(DomainEventChange, Basics)
{
  using namespace Core;
  
  // Check type
  std::unique_ptr<DomainEvent> event(new DomainEventChange());
  ASSERT_TRUE(event->getEventType() == DomainEventType::CHANGE_EVENT);
  
  // Check isa and cast
  DomainEventChange *castEvent = DomainEventChange::cast(event.get());
  ASSERT_TRUE(DomainEventChange::isa(event.get()));
  ASSERT_TRUE(DomainEventChange::isa(castEvent));
}// Basics

OPTILAB_TEST(DomainEventChange, overlap)
{
  using namespace Core;
  
  std::unique_ptr<DomainEvent> eventUndef(new DomainEventUndef());
  std::unique_ptr<DomainEvent> eventBound(new DomainEventBound());
  std::unique_ptr<DomainEvent> eventFail(new DomainEventFail());
  std::unique_ptr<DomainEvent> eventVoid(new DomainEventVoid());
  std::unique_ptr<DomainEvent> eventChange(new DomainEventChange());
  std::unique_ptr<DomainEvent> eventLwb(new DomainEventLwb());
  std::unique_ptr<DomainEvent> eventUbp(new DomainEventUpb());
  std::unique_ptr<DomainEvent> eventSingleton(new DomainEventSingleton());
  
  ASSERT_TRUE(eventChange->overlaps(eventChange.get()));
  ASSERT_FALSE(eventChange->overlaps(eventLwb.get()));
  ASSERT_FALSE(eventChange->overlaps(eventUbp.get()));
  ASSERT_FALSE(eventChange->overlaps(eventBound.get()));
  ASSERT_FALSE(eventChange->overlaps(eventFail.get()));
  ASSERT_FALSE(eventChange->overlaps(eventVoid.get()));
  ASSERT_FALSE(eventChange->overlaps(eventSingleton.get()));
  ASSERT_FALSE(eventChange->overlaps(eventUndef.get()));
}// overlap
