
#include "utest_globals.hpp"

#include "DomainEventBound.hpp"
#include "DomainEventFail.hpp"
#include "DomainEventVoid.hpp"
#include "DomainEventChange.hpp"
#include "DomainEventSingleton.hpp"
#include "DomainEventLwb.hpp"
#include "DomainEventUpb.hpp"
#include "DomainEventUndef.hpp"

OPTILAB_TEST(DomainEventSingleton, Basics)
{
  using namespace Core;
  
  // Check type
  std::unique_ptr<DomainEvent> event(new DomainEventSingleton());
  ASSERT_TRUE(event->getEventType() == DomainEventType::SINGLETON_EVENT);
  
  // Check isa and cast
  DomainEventSingleton *castEvent = DomainEventSingleton::cast(event.get());
  ASSERT_TRUE(DomainEventSingleton::isa(event.get()));
  ASSERT_TRUE(DomainEventSingleton::isa(castEvent));
}// Basics

OPTILAB_TEST(DomainEventSingleton, overlap)
{
  using namespace Core;
  
  std::unique_ptr<DomainEvent> eventUndef(new DomainEventUndef());
  std::unique_ptr<DomainEvent> eventBound(new DomainEventBound());
  std::unique_ptr<DomainEvent> eventFail(new DomainEventFail());
  std::unique_ptr<DomainEvent> eventVoid(new DomainEventVoid());
  std::unique_ptr<DomainEvent> eventChange(new DomainEventBound());
  std::unique_ptr<DomainEvent> eventLwb(new DomainEventLwb());
  std::unique_ptr<DomainEvent> eventUbp(new DomainEventUpb());
  std::unique_ptr<DomainEvent> eventSingleton(new DomainEventSingleton());
  
  ASSERT_TRUE(eventSingleton->overlaps(eventChange.get()));
  ASSERT_FALSE(eventSingleton->overlaps(eventLwb.get()));
  ASSERT_FALSE(eventSingleton->overlaps(eventUbp.get()));
  ASSERT_TRUE(eventSingleton->overlaps(eventBound.get()));
  ASSERT_FALSE(eventSingleton->overlaps(eventFail.get()));
  ASSERT_FALSE(eventSingleton->overlaps(eventVoid.get()));
  ASSERT_TRUE(eventSingleton->overlaps(eventSingleton.get()));
  ASSERT_FALSE(eventSingleton->overlaps(eventUndef.get()));
}// overlap
