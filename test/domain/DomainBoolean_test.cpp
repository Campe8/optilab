
#include "utest_globals.hpp"

#include "DomainBoolean.hpp"
#include "DomainInteger.hpp"
#include "DomainElementKey.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementVoid.hpp"
#include "DomainElementManager.hpp"
#include "DomainEventInc.hpp"

OPTILAB_TEST(DomainBoolean, Basics)
{
  using namespace Core;
  
  DomainElement *lower = DomainElementManager::getInstance().createDomainElementInt64(0);
  DomainElement *upper = DomainElementManager::getInstance().createDomainElementInt64(1);
  
  DomainBoolean domBool;
  DomainInteger domInteger(DomainElementInt64::cast(lower), DomainElementInt64::cast(upper));
  ASSERT_TRUE(DomainBoolean::isa(&domBool));
  ASSERT_FALSE(DomainBoolean::isa(&domInteger));
  ASSERT_TRUE(DomainBoolean::cast(&domInteger) == nullptr);
  ASSERT_TRUE(DomainBoolean::cast(&domBool) == &domBool);
  
  ASSERT_EQ(domBool.getSize(), 2);
  ASSERT_TRUE(DomainElement::isEqual(domBool.lowerBound(), lower));
  ASSERT_TRUE(DomainElement::isEqual(domBool.upperBound(), upper));
  
  // Copy constructors
  DomainBoolean domBoolCopy(domBool);
  ASSERT_EQ(domBoolCopy.getSize(), 2);
  ASSERT_TRUE(DomainElement::isEqual(domBoolCopy.lowerBound(), lower));
  ASSERT_TRUE(DomainElement::isEqual(domBoolCopy.upperBound(), upper));
  
  // Assignment operator
  DomainBoolean domBoolAssignment;
  domBoolAssignment.subtract(DomainElementManager::getInstance().createDomainElementInt64(1));
  ASSERT_EQ(domBoolAssignment.getSize(), 1);
  
  domBoolAssignment = domBool;
  ASSERT_EQ(domBoolAssignment.getSize(), 2);
  ASSERT_TRUE(DomainElement::isEqual(domBoolAssignment.lowerBound(), lower));
  ASSERT_TRUE(DomainElement::isEqual(domBoolAssignment.upperBound(), upper));
  
  // Default event is void event
  ASSERT_TRUE(DomainEventVoid::isa(domBool.getEvent()));
  
  DomainBoolean domBool1;
  domBool1.setTrue();
  ASSERT_EQ(domBool1.getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domBool1.lowerBound(), upper));
  ASSERT_TRUE(DomainElement::isEqual(domBool1.upperBound(), upper));
  
  DomainBoolean domBool2;
  domBool2.setFalse();
  ASSERT_EQ(domBool2.getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domBool2.lowerBound(), lower));
  ASSERT_TRUE(DomainElement::isEqual(domBool2.upperBound(), lower));
}//Basics

OPTILAB_TEST(DomainBoolean, SetTrueFalse)
{
  using namespace Core;
  
  DomainBoolean domBoolT;
  ASSERT_FALSE(domBoolT.isTrue());
  ASSERT_FALSE(domBoolT.isFalse());
  
  domBoolT.setTrue();
  ASSERT_TRUE(domBoolT.isTrue());
  ASSERT_FALSE(domBoolT.isFalse());
  
  DomainBoolean domBoolF;
  domBoolF.setFalse();
  ASSERT_FALSE(domBoolF.isTrue());
  ASSERT_TRUE(domBoolF.isFalse());
}// SetTrueFalse

OPTILAB_TEST(DomainBoolean, Contains)
{
  using namespace Core;
  
  DomainElement *lower = DomainElementManager::getInstance().createDomainElementInt64(0);
  DomainElement *upper = DomainElementManager::getInstance().createDomainElementInt64(1);
  DomainBoolean domBool;
  
  ASSERT_TRUE(domBool.contains(lower));
  ASSERT_TRUE(domBool.contains(upper));
  ASSERT_FALSE(domBool.contains(DomainElementManager::getInstance().getVoidElement()));
  ASSERT_FALSE(domBool.contains(DomainElementManager::getInstance().createDomainElementInt64(-1)));
  ASSERT_FALSE(domBool.contains(DomainElementManager::getInstance().createDomainElementInt64(+2)));
}//Contains

OPTILAB_TEST(DomainBoolean, Shrink)
{
  using namespace Core;
  
  DomainElement *lower = DomainElementManager::getInstance().createDomainElementInt64(0);
  DomainElement *upper = DomainElementManager::getInstance().createDomainElementInt64(1);
  DomainBoolean domBool;
  
  // Shrink on lower/upper should not change the domain
  domBool.shrink(lower, upper);
  ASSERT_EQ(domBool.getSize(), 2);
  ASSERT_TRUE(DomainElement::isEqual(domBool.lowerBound(), lower));
  ASSERT_TRUE(DomainElement::isEqual(domBool.upperBound(), upper));
  
  // Shrink outside upper should empty domain
  domBool.shrink(DomainElementManager::getInstance().createDomainElementInt64(2),
                 DomainElementManager::getInstance().createDomainElementInt64(3));
  ASSERT_EQ(domBool.getSize(), 0);
  ASSERT_TRUE(DomainEventFail::isa(domBool.getEvent()));
  
  // Shrink outside lower should empty domain
  DomainBoolean domBool2;
  domBool2.shrink(DomainElementManager::getInstance().createDomainElementInt64(-2),
                 DomainElementManager::getInstance().createDomainElementInt64(-1));
  ASSERT_EQ(domBool2.getSize(), 0);
  ASSERT_TRUE(DomainEventFail::isa(domBool2.getEvent()));
  
  // Shrink on upper should reduce domain to singleton
  DomainBoolean domBool3;
  domBool3.shrink(DomainElementManager::getInstance().createDomainElementInt64(1),
                  DomainElementManager::getInstance().createDomainElementInt64(2));
  ASSERT_EQ(domBool3.getSize(), 1);
  ASSERT_TRUE(DomainEventSingleton::isa(domBool3.getEvent()));
  
  // Shrink on lower should reduce domain to singleton
  DomainBoolean domBool4;
  domBool4.shrink(DomainElementManager::getInstance().createDomainElementInt64(-1),
                  DomainElementManager::getInstance().createDomainElementInt64(0));
  ASSERT_EQ(domBool4.getSize(), 1);
  ASSERT_TRUE(DomainEventSingleton::isa(domBool4.getEvent()));
  
  // Shrink on voids should throw
  /*
  DomainBoolean domBool5;
  domBool5.shrink(DomainElementManager::getInstance().getVoidElement(),
                  DomainElementManager::getInstance().createDomainElementInt64(0));
  ASSERT_EQ(domBool5.getSize(), 2);
  
  DomainBoolean domBool6;
  domBool6.shrink(DomainElementManager::getInstance().createDomainElementInt64(0),
                  DomainElementManager::getInstance().getVoidElement());
  ASSERT_EQ(domBool6.getSize(), 2);
  
  DomainBoolean domBool7;
  domBool7.shrink(DomainElementManager::getInstance().getVoidElement(),
                  DomainElementManager::getInstance().getVoidElement());
  ASSERT_EQ(domBool7.getSize(), 2);
   */
}//Shrink

OPTILAB_TEST(DomainBoolean, Subtract)
{
  using namespace Core;
  
  DomainElement *lower = DomainElementManager::getInstance().createDomainElementInt64(0);
  DomainElement *upper = DomainElementManager::getInstance().createDomainElementInt64(1);
  DomainBoolean domBool;
  
  // Subtract lower should leave with singleton upper
  domBool.subtract(lower);
  ASSERT_EQ(domBool.getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domBool.lowerBound(), upper));
  ASSERT_TRUE(DomainElement::isEqual(domBool.upperBound(), upper));
  
  // Subtract singleton should empty domain
  domBool.subtract(upper);
  ASSERT_EQ(domBool.getSize(), 0);
  
  DomainBoolean domBool1;
  
  // Subtract upper should leave with singleton lower
  domBool1.subtract(upper);
  ASSERT_EQ(domBool1.getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domBool1.lowerBound(), lower));
  ASSERT_TRUE(DomainElement::isEqual(domBool1.upperBound(), lower));
  
  // Subtract singleton should empty domain
  domBool1.subtract(lower);
  ASSERT_EQ(domBool1.getSize(), 0);
  
  DomainBoolean domBool2;
  
  // Subtract elements outside [0, 1] should not change domain
  domBool2.subtract(DomainElementManager::getInstance().createDomainElementInt64(-1));
  ASSERT_EQ(domBool2.getSize(), 2);
  ASSERT_TRUE(DomainElement::isEqual(domBool2.lowerBound(), lower));
  ASSERT_TRUE(DomainElement::isEqual(domBool2.upperBound(), upper));
  
  domBool2.subtract(DomainElementManager::getInstance().createDomainElementInt64(2));
  ASSERT_EQ(domBool2.getSize(), 2);
  ASSERT_TRUE(DomainElement::isEqual(domBool2.lowerBound(), lower));
  ASSERT_TRUE(DomainElement::isEqual(domBool2.upperBound(), upper));
  
  // Subtract void should throw
  /*
  domBool2.subtract(DomainElementManager::getInstance().getVoidElement());
  ASSERT_EQ(domBool2.getSize(), 2);
  ASSERT_TRUE(DomainElement::isEqual(domBool2.lowerBound(), lower));
  ASSERT_TRUE(DomainElement::isEqual(domBool2.upperBound(), upper));
   */
}//Subtract

OPTILAB_TEST(DomainBoolean, ShrinkOnLowerBound)
{
  using namespace Core;
  
  DomainElement *lower = DomainElementManager::getInstance().createDomainElementInt64(0);
  DomainElement *upper = DomainElementManager::getInstance().createDomainElementInt64(1);
  DomainBoolean domBool;
  
  /*
  domBool.shrinkOnLowerBound(DomainElementManager::getInstance().getVoidElement());
  ASSERT_EQ(domBool.getSize(), 2);
  ASSERT_TRUE(DomainElement::isEqual(domBool.lowerBound(), lower));
  ASSERT_TRUE(DomainElement::isEqual(domBool.upperBound(), upper));
  */
  
  domBool.shrinkOnLowerBound(DomainElementManager::getInstance().createDomainElementInt64(-1));
  ASSERT_EQ(domBool.getSize(), 2);
  ASSERT_TRUE(DomainElement::isEqual(domBool.lowerBound(), lower));
  ASSERT_TRUE(DomainElement::isEqual(domBool.upperBound(), upper));
  
  domBool.shrinkOnLowerBound(lower);
  ASSERT_EQ(domBool.getSize(), 2);
  ASSERT_TRUE(DomainElement::isEqual(domBool.lowerBound(), lower));
  ASSERT_TRUE(DomainElement::isEqual(domBool.upperBound(), upper));
  
  domBool.shrinkOnLowerBound(DomainElementManager::getInstance().createDomainElementInt64(2));
  ASSERT_EQ(domBool.getSize(), 0);
  
  DomainBoolean domBool1;
  domBool1.shrinkOnLowerBound(upper);
  ASSERT_EQ(domBool1.getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domBool1.lowerBound(), upper));
  ASSERT_TRUE(DomainElement::isEqual(domBool1.upperBound(), upper));
}//ShrinkOnLowerBound

OPTILAB_TEST(DomainBoolean, ShrinkOnUpperBound)
{
  using namespace Core;
  
  DomainElement *lower = DomainElementManager::getInstance().createDomainElementInt64(0);
  DomainElement *upper = DomainElementManager::getInstance().createDomainElementInt64(1);
  DomainBoolean domBool;
  
  /*
  domBool.shrinkOnUpperBound(DomainElementManager::getInstance().getVoidElement());
  ASSERT_EQ(domBool.getSize(), 2);
  ASSERT_TRUE(DomainElement::isEqual(domBool.lowerBound(), lower));
  ASSERT_TRUE(DomainElement::isEqual(domBool.upperBound(), upper));
  */
  
  domBool.shrinkOnUpperBound(DomainElementManager::getInstance().createDomainElementInt64(2));
  ASSERT_EQ(domBool.getSize(), 2);
  ASSERT_TRUE(DomainElement::isEqual(domBool.lowerBound(), lower));
  ASSERT_TRUE(DomainElement::isEqual(domBool.upperBound(), upper));
  
  domBool.shrinkOnUpperBound(upper);
  ASSERT_EQ(domBool.getSize(), 2);
  ASSERT_TRUE(DomainElement::isEqual(domBool.lowerBound(), lower));
  ASSERT_TRUE(DomainElement::isEqual(domBool.upperBound(), upper));
  
  domBool.shrinkOnUpperBound(DomainElementManager::getInstance().createDomainElementInt64(-1));
  ASSERT_EQ(domBool.getSize(), 0);
  
  DomainBoolean domBool1;
  domBool1.shrinkOnUpperBound(lower);
  ASSERT_EQ(domBool1.getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domBool1.lowerBound(), lower));
  ASSERT_TRUE(DomainElement::isEqual(domBool1.upperBound(), lower));
}//ShrinkOnLowerBound

OPTILAB_TEST(DomainBoolean, IntersectDomain)
{
  using namespace Core;
  
  DomainElement *lower = DomainElementManager::getInstance().createDomainElementInt64(0);
  DomainElement *upper = DomainElementManager::getInstance().createDomainElementInt64(1);
  DomainBoolean domBool;
  DomainBoolean domBool1;
  
  domBool.intersect(&domBool1);
  ASSERT_EQ(domBool.getSize(), 2);
  ASSERT_TRUE(DomainElement::isEqual(domBool.lowerBound(), lower));
  ASSERT_TRUE(DomainElement::isEqual(domBool.upperBound(), upper));
  
  domBool1.subtract(lower);
  domBool.intersect(&domBool1);
  ASSERT_EQ(domBool.getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domBool.lowerBound(), upper));
  ASSERT_TRUE(DomainElement::isEqual(domBool.upperBound(), upper));
  
  domBool1.subtract(upper);
  domBool.intersect(&domBool1);
  ASSERT_EQ(domBool.getSize(), 0);
}//IntersectDomain

OPTILAB_TEST(DomainBoolean, IntersectArray)
{
  using namespace Core;
  
  DomainElement *lower = DomainElementManager::getInstance().createDomainElementInt64(0);
  DomainElement *upper = DomainElementManager::getInstance().createDomainElementInt64(1);
  DomainBoolean domBool;
  DomainElementArray array(2);
  array.assignElementToCell(0, lower);
  array.assignElementToCell(1, upper);
  
  domBool.intersect(&array);
  ASSERT_EQ(domBool.getSize(), 2);
  ASSERT_TRUE(DomainElement::isEqual(domBool.lowerBound(), lower));
  ASSERT_TRUE(DomainElement::isEqual(domBool.upperBound(), upper));
  
  array.clear();
  array.assignElementToCell(0, upper);
  domBool.intersect(&array);
  ASSERT_EQ(domBool.getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domBool.lowerBound(), upper));
  ASSERT_TRUE(DomainElement::isEqual(domBool.upperBound(), upper));
  
  array.clear();
  domBool.intersect(&array);
  ASSERT_EQ(domBool.getSize(), 0);
}//IntersectDomain

OPTILAB_TEST(DomainBoolean, SubtractDomain)
{
  using namespace Core;
  
  DomainElement *lower = DomainElementManager::getInstance().createDomainElementInt64(0);
  DomainElement *upper = DomainElementManager::getInstance().createDomainElementInt64(1);
  DomainBoolean domBool;
  DomainBoolean domBool1;
  
  domBool1.subtract(lower);
  domBool.subtract(&domBool1);
  ASSERT_EQ(domBool.getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domBool.lowerBound(), lower));
  ASSERT_TRUE(DomainElement::isEqual(domBool.upperBound(), lower));
  
  // Subtract empty domain should not affect the current domain
  domBool1.subtract(upper);
  domBool.subtract(&domBool1);
  ASSERT_EQ(domBool.getSize(), 1);
}//SubtractDomain

OPTILAB_TEST(DomainBoolean, SubtractArray)
{
  using namespace Core;
  
  DomainElement *lower = DomainElementManager::getInstance().createDomainElementInt64(0);
  DomainElement *upper = DomainElementManager::getInstance().createDomainElementInt64(1);
  DomainBoolean domBool;
  DomainBoolean domBool1;
  DomainElementArray array(2);
  array.assignElementToCell(0, lower);
  
  domBool.subtract(&array);
  ASSERT_EQ(domBool.getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domBool.lowerBound(), upper));
  ASSERT_TRUE(DomainElement::isEqual(domBool.upperBound(), upper));
  
  // Subtract empty array should not affect the current domain
  array.clear();
  domBool.subtract(&array);
  ASSERT_EQ(domBool.getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(domBool.lowerBound(), upper));
  ASSERT_TRUE(DomainElement::isEqual(domBool.upperBound(), upper));
}//SubtractArray
