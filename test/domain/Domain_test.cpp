
#include "utest_globals.hpp"

#include "DomainTestInc.hpp"
#include "ConstraintTestInc.hpp"

#include "DomainElementManager.hpp"

//=================== TEST ===================

OPTILAB_TEST(Domain, SubjectObserver)
{
  using namespace Core;
  
  DomainTest domain;
  ConstraintTest constraint;
  
  std::unique_ptr<Core::ConstraintObserverEvent> constraintObserverEvent(new Core::ConstraintObserverEvent(&constraint));
  
  ASSERT_EQ(constraint.getValueTest(), 0);
  
  constraintObserverEvent->setConstraintPropagationStatus(Core::ConstraintObserverEvent::ConstraintObserverPropagationStatus::CON_EVT_PROPAGATION_INIT);
  constraintObserverEvent->setConstraintPropagationEvent(Core::PropagationEvent::PROP_EVENT_NO_FIXPOINT);
  
  domain.updateOnConstraintEvent(constraintObserverEvent.get());
  ASSERT_EQ(constraint.getValueTest(), 0);
  
  constraintObserverEvent->setConstraintPropagationStatus(Core::ConstraintObserverEvent::ConstraintObserverPropagationStatus::CON_EVT_PROPAGATION_TERM);
  ASSERT_EQ(constraint.getValueTest(), 0);
  
  domain.attachConstraintObserver(&constraint);
  constraintObserverEvent->setConstraintPropagationStatus(Core::ConstraintObserverEvent::ConstraintObserverPropagationStatus::CON_EVT_PROPAGATION_INIT);
  domain.updateOnConstraintEvent(constraintObserverEvent.get());
  
  ASSERT_EQ(constraint.getValueTest(), 0);
  
  constraintObserverEvent->setConstraintPropagationStatus(Core::ConstraintObserverEvent::ConstraintObserverPropagationStatus::CON_EVT_PROPAGATION_TERM);
  domain.updateOnConstraintEvent(constraintObserverEvent.get());
  ASSERT_EQ(constraint.getValueTest(), 0);
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  constraintObserverEvent->setConstraintPropagationStatus(Core::ConstraintObserverEvent::ConstraintObserverPropagationStatus::CON_EVT_PROPAGATION_INIT);
  domain.updateOnConstraintEvent(constraintObserverEvent.get());
  // Change bound -> creates new event
  domain.shrinkOnLowerBound(elementManager.createDomainElementInt64(0));
  // New event should notify the constraint which is an observer
  constraintObserverEvent->setConstraintPropagationStatus(Core::ConstraintObserverEvent::ConstraintObserverPropagationStatus::CON_EVT_PROPAGATION_TERM);
  domain.updateOnConstraintEvent(constraintObserverEvent.get());
  ASSERT_EQ(constraint.getValueTest(), 1000);
  
  constraint.resetInternalValue(10);
  ASSERT_EQ(constraint.getValueTest(), 10);
  // Detach constraint
  domain.detachConstraintObserver(&constraint);
  // Should not notify the constraint which is now detached
  domain.shrinkOnUpperBound(elementManager.createDomainElementInt64(5));
  constraintObserverEvent->setConstraintPropagationStatus(Core::ConstraintObserverEvent::ConstraintObserverPropagationStatus::CON_EVT_PROPAGATION_TERM);
  domain.updateOnConstraintEvent(constraintObserverEvent.get());
  ASSERT_EQ(constraint.getValueTest(), 10);
}//SubjectObserver

OPTILAB_TEST(Domain, SubjectDoubleObserver)
{
  using namespace Core;
  
  DomainTest domain;
  ConstraintTest constraint1;
  ConstraintTest constraint2;
  
  domain.attachConstraintObserver(&constraint1);
  domain.attachConstraintObserver(&constraint2);
  
  std::unique_ptr<Core::ConstraintObserverEvent> constraintObserverEvent(new Core::ConstraintObserverEvent(&constraint1));
  constraintObserverEvent->setConstraintPropagationEvent(Core::PropagationEvent::PROP_EVENT_NO_FIXPOINT);
  
  constraintObserverEvent->setConstraintPropagationStatus(Core::ConstraintObserverEvent::ConstraintObserverPropagationStatus::CON_EVT_PROPAGATION_INIT);
  domain.updateOnConstraintEvent(constraintObserverEvent.get());
  
  domain.shrinkOnLowerBound(DomainElementManager::getInstance().createDomainElementInt64(0));
  
  constraintObserverEvent->setConstraintPropagationStatus(Core::ConstraintObserverEvent::ConstraintObserverPropagationStatus::CON_EVT_PROPAGATION_TERM);
  domain.updateOnConstraintEvent(constraintObserverEvent.get());
  ASSERT_EQ(constraint1.getValueTest(), 1000);
  ASSERT_EQ(constraint2.getValueTest(), 1000);
}//SubjectDoubleObserver

OPTILAB_TEST(Domain, BacktrackableObject)
{
  using namespace Core;
  
  DomainTest domain;
  ASSERT_EQ(domain.getSize(), 21);
  ASSERT_TRUE(domain.lowerBound()->isEqual(DomainElementManager::getInstance().createDomainElementInt64(-10)));
  ASSERT_TRUE(domain.upperBound()->isEqual(DomainElementManager::getInstance().createDomainElementInt64(+10)));
  ASSERT_TRUE(domain.contains(DomainElementManager::getInstance().createDomainElementInt64(+5)));
  
  auto memento = domain.getMemento();
  ASSERT_TRUE(memento->hasValidState());
  
  // Modify original domain
  domain.shrink(DomainElementManager::getInstance().createDomainElementInt64(-1),
                DomainElementManager::getInstance().createDomainElementInt64(+1));
  ASSERT_EQ(domain.getSize(), 3);
  ASSERT_TRUE(domain.lowerBound()->isEqual(DomainElementManager::getInstance().createDomainElementInt64(-1)));
  ASSERT_TRUE(domain.upperBound()->isEqual(DomainElementManager::getInstance().createDomainElementInt64(+1)));
  ASSERT_FALSE(domain.contains(DomainElementManager::getInstance().createDomainElementInt64(+5)));
  
  // Reset mememnto, it should be the original domain
  domain.setMemento(std::move(memento));
  ASSERT_EQ(domain.getSize(), 21);
  ASSERT_TRUE(domain.lowerBound()->isEqual(DomainElementManager::getInstance().createDomainElementInt64(-10)));
  ASSERT_TRUE(domain.upperBound()->isEqual(DomainElementManager::getInstance().createDomainElementInt64(+10)));
  ASSERT_TRUE(domain.contains(DomainElementManager::getInstance().createDomainElementInt64(+5)));
  ASSERT_EQ(domain.getEvent()->getEventType(), DomainEventType::CHANGE_EVENT);
}//BacktrackableObject

OPTILAB_TEST(Domain, BacktrackableObjectOnEmptyDomain)
{
  using namespace Core;
  
  DomainTest domain;
  ASSERT_EQ(domain.getSize(), 21);
  ASSERT_TRUE(domain.lowerBound()->isEqual(DomainElementManager::getInstance().createDomainElementInt64(-10)));
  ASSERT_TRUE(domain.upperBound()->isEqual(DomainElementManager::getInstance().createDomainElementInt64(+10)));
  ASSERT_TRUE(domain.contains(DomainElementManager::getInstance().createDomainElementInt64(+5)));
  
  auto memento = domain.getMemento();
  ASSERT_TRUE(memento->hasValidState());
  
  // Modify original domain
  domain.shrink(DomainElementManager::getInstance().createDomainElementInt64(+10),
                DomainElementManager::getInstance().createDomainElementInt64(-10));
  ASSERT_EQ(domain.getSize(), 0);
  ASSERT_EQ(domain.getEvent()->getEventType(), DomainEventType::FAIL_EVENT);
  
  // Reset mememnto, it should be the original domain
  domain.setMemento(std::move(memento));
  ASSERT_EQ(domain.getSize(), 21);
  ASSERT_TRUE(domain.lowerBound()->isEqual(DomainElementManager::getInstance().createDomainElementInt64(-10)));
  ASSERT_TRUE(domain.upperBound()->isEqual(DomainElementManager::getInstance().createDomainElementInt64(+10)));
  ASSERT_TRUE(domain.contains(DomainElementManager::getInstance().createDomainElementInt64(+5)));
  ASSERT_EQ(domain.getEvent()->getEventType(), DomainEventType::CHANGE_EVENT);
}//BacktrackableObjectOnEmptyDomain
