
#include "utest_globals.hpp"

#include "DomainInteger.hpp"
#include "DomainBoolean.hpp"
#include "DomainElementKey.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementVoid.hpp"
#include "DomainElementManager.hpp"

OPTILAB_TEST(DomainInteger, Basics)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  DomainElementInt64 *lowerBound = elementManager.createDomainElementInt64(-10);
  DomainElementInt64 *upperBound = elementManager.createDomainElementInt64(+10);
  
  DomainInteger domInt(lowerBound, upperBound);
  
  DomainBoolean domBool;
  ASSERT_TRUE(DomainInteger::isa(&domInt));
  ASSERT_FALSE(DomainInteger::isa(&domBool));
  ASSERT_TRUE(DomainInteger::cast(&domBool) == nullptr);
  ASSERT_TRUE(DomainInteger::cast(&domInt) == &domInt);
  
  // Copy constructors
  
  
}//Basics

