
#include "utest_globals.hpp"

#include "DomainDecorator.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementInt64Container.hpp"

OPTILAB_TEST(DomainDecorator, Basics)
{
  using namespace Core;
  
  // Create container for the decorator
  DomainElementInt64 lowerBound(-10);
  DomainElementInt64 upperBound( 10);
  DomainElementInt64Container container(&lowerBound, &upperBound);
  
  // Create decorator
  DomainDecorator decorator(&container);
  
  // Default values: void event
  ASSERT_TRUE(DomainEventVoid::isa(decorator.getEvent()));
  
  // Default values: lower and upper bounds as for registration
  ASSERT_TRUE(DomainElement::isEqual(&lowerBound, decorator.getLowerBound()));
  ASSERT_TRUE(DomainElement::isEqual(&upperBound, decorator.getUpperBound()));
  
  // Default size: domain's size
  ASSERT_EQ(decorator.getDomainSize(), lowerBound.sizeSpan(&upperBound));
}//Basics

OPTILAB_TEST(DomainDecorator, DomainModificationsOnSmallDomain)
{
  using namespace Core;
  
  // Create container for the decorator
  DomainElementInt64 lowerBound(-10);
  DomainElementInt64 upperBound(+10);
  DomainElementInt64Container container(&lowerBound, &upperBound);
  
  // Create decorator
  DomainDecorator decorator(&container);
  
  DomainElementInt64 lowerBound2(-5);
  DomainElementInt64 upperBound2(+5);
  
  // Lower bound event
  container.inMin(&lowerBound2);
  ASSERT_TRUE(DomainElement::isEqual(&lowerBound2, decorator.getLowerBound()));
  ASSERT_TRUE(DomainEventLwb::isa(decorator.getEvent()));
  ASSERT_EQ(decorator.getDomainSize(), lowerBound2.sizeSpan(&upperBound));
  
  // Upper bound event
  container.inMax(&upperBound2);
  ASSERT_TRUE(DomainElement::isEqual(&upperBound2, decorator.getUpperBound()));
  ASSERT_TRUE(DomainEventUpb::isa(decorator.getEvent()));
  ASSERT_EQ(decorator.getDomainSize(), lowerBound2.sizeSpan(&upperBound2));
  
  // Singleton event
  container.inMin(&upperBound2);
  ASSERT_TRUE(DomainElement::isEqual(&upperBound2, decorator.getLowerBound()));
  ASSERT_TRUE(DomainElement::isEqual(&upperBound2, decorator.getUpperBound()));
  ASSERT_TRUE(DomainEventSingleton::isa(decorator.getEvent()));
  ASSERT_EQ(decorator.getDomainSize(), 1);
  
  // Fail event
  container.inMax(&lowerBound2);
  ASSERT_TRUE(DomainElement::isEqual(DomainElementManager::getInstance().getVoidElement(), decorator.getLowerBound()));
  ASSERT_TRUE(DomainElement::isEqual(DomainElementManager::getInstance().getVoidElement(), decorator.getUpperBound()));
  ASSERT_TRUE(DomainEventFail::isa(decorator.getEvent()));
  ASSERT_EQ(decorator.getDomainSize(), 0);
}//DomainModificationsOnSmallDomain

OPTILAB_TEST(DomainDecorator, DomainModificationsOnLargeDomain)
{
  using namespace Core;
  
  // Create container for the decorator
  DomainElementInt64 lowerBound(-1000);
  DomainElementInt64 upperBound(+1000);
  DomainElementInt64Container container(&lowerBound, &upperBound);
  
  // Create decorator
  DomainDecorator decorator(&container);
  
  DomainElementInt64 lowerBound2(-900);
  DomainElementInt64 upperBound2(+900);
  
  // Lower bound event
  container.inMin(&lowerBound2);
  ASSERT_TRUE(DomainElement::isEqual(&lowerBound2, decorator.getLowerBound()));
  ASSERT_TRUE(DomainEventLwb::isa(decorator.getEvent()));
  ASSERT_EQ(decorator.getDomainSize(), lowerBound2.sizeSpan(&upperBound));
  
  // Upper bound event
  container.inMax(&upperBound2);
  ASSERT_TRUE(DomainElement::isEqual(&upperBound2, decorator.getUpperBound()));
  ASSERT_TRUE(DomainEventUpb::isa(decorator.getEvent()));
  ASSERT_EQ(decorator.getDomainSize(), lowerBound2.sizeSpan(&upperBound2));
  
  DomainElementInt64 lowerBound3(-5);
  DomainElementInt64 upperBound3(+5);
  
  // Lower bound event
  container.inMin(&lowerBound3);
  ASSERT_TRUE(DomainElement::isEqual(&lowerBound3, decorator.getLowerBound()));
  ASSERT_TRUE(DomainEventLwb::isa(decorator.getEvent()));
  ASSERT_EQ(decorator.getDomainSize(), lowerBound3.sizeSpan(&upperBound2));
  
  // Upper bound event
  container.inMax(&upperBound3);
  ASSERT_TRUE(DomainElement::isEqual(&upperBound3, decorator.getUpperBound()));
  ASSERT_TRUE(DomainEventUpb::isa(decorator.getEvent()));
  ASSERT_EQ(decorator.getDomainSize(), lowerBound3.sizeSpan(&upperBound3));
  
  // Singleton event
  container.inMin(&upperBound3);
  ASSERT_TRUE(DomainElement::isEqual(&upperBound3, decorator.getLowerBound()));
  ASSERT_TRUE(DomainElement::isEqual(&upperBound3, decorator.getUpperBound()));
  ASSERT_TRUE(DomainEventSingleton::isa(decorator.getEvent()));
  ASSERT_EQ(decorator.getDomainSize(), 1);
  
  // Fail event
  container.inMax(&lowerBound3);
  ASSERT_TRUE(DomainElement::isEqual(DomainElementManager::getInstance().getVoidElement(), decorator.getLowerBound()));
  ASSERT_TRUE(DomainElement::isEqual(DomainElementManager::getInstance().getVoidElement(), decorator.getUpperBound()));
  ASSERT_TRUE(DomainEventFail::isa(decorator.getEvent()));
  ASSERT_EQ(decorator.getDomainSize(), 0);
}//DomainModificationsOnLargeDomain
