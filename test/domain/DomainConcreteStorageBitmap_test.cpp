
#include "utest_globals.hpp"

#include "DomainConcreteStorageBitmap.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"

OPTILAB_TEST(DomainConcreteStorageBitmap, Constructor)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  // Check single element
  DomainElementInt64 lowerBound1(0);
  DomainElementInt64 upperBound1(0);
  DomainConcreteStorageBitmap storage1(&lowerBound1, &upperBound1, &elementManager);
  ASSERT_EQ(storage1.getSize(), 1);
  
  // Check empty domain
  DomainElementInt64 lowerBound2(10);
  DomainElementInt64 upperBound2(0);
  DomainConcreteStorageBitmap storage2(&lowerBound2, &upperBound2, &elementManager);
  ASSERT_EQ(storage2.getSize(), 0);
  
  // Check standard domain
  DomainElementInt64 lowerBound3(0);
  DomainElementInt64 upperBound3(10);
  DomainConcreteStorageBitmap storage3(&lowerBound3, &upperBound3, &elementManager);
  ASSERT_EQ(storage3.getSize(), 11);
  
  // Check standard domain
  DomainElementInt64 lowerBound4(-10);
  DomainElementInt64 upperBound4(-1);
  DomainConcreteStorageBitmap storage4(&lowerBound4, &upperBound4, &elementManager);
  ASSERT_EQ(storage4.getSize(), 10);
  
  // Check standard domain
  DomainElementInt64 lowerBound5(-10);
  DomainElementInt64 upperBound5(10);
  DomainConcreteStorageBitmap storage5(&lowerBound5, &upperBound5, &elementManager);
  ASSERT_EQ(storage5.getSize(), 21);
}// Constructor

OPTILAB_TEST(DomainConcreteStorageBitmap, Bounds)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  // Check single element
  DomainElementInt64 lowerBound1(0);
  DomainElementInt64 upperBound1(0);
  DomainConcreteStorageBitmap storage1(&lowerBound1, &upperBound1, &elementManager);
  
  DomainElement *lb1 = storage1.getLowerBound();
  DomainElement *ub1 = storage1.getUpperBound();
  
  ASSERT_TRUE(DomainElement::isEqual(lb1, &lowerBound1));
  ASSERT_TRUE(DomainElement::isEqual(lb1, ub1));
  
  DomainElementInt64 lowerBound2(-10);
  DomainElementInt64 upperBound2(10);
  DomainConcreteStorageBitmap storage2(&lowerBound2, &upperBound2, &elementManager);
  
  DomainElement *lb2 = storage2.getLowerBound();
  DomainElement *ub2 = storage2.getUpperBound();
  
  ASSERT_TRUE(DomainElement::isEqual(lb2, &lowerBound2));
  ASSERT_TRUE(DomainElement::isEqual(ub2, &upperBound2));
  
  DomainElementInt64 lowerBound3(-10);
  DomainElementInt64 upperBound3(-1);
  DomainConcreteStorageBitmap storage3(&lowerBound3, &upperBound3, &elementManager);
  
  DomainElement *lb3 = storage3.getLowerBound();
  DomainElement *ub3 = storage3.getUpperBound();
  
  ASSERT_TRUE(DomainElement::isEqual(lb3, &lowerBound3));
  ASSERT_TRUE(DomainElement::isEqual(ub3, &upperBound3));
}// Bounds

OPTILAB_TEST(DomainConcreteStorageBitmap, Shrink)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  // Shrink to current domain should not affect the DomainConcreteStorageBitmap
  DomainElementInt64 lowerBound1(0);
  DomainElementInt64 upperBound1(0);
  DomainConcreteStorageBitmap storage1(&lowerBound1, &upperBound1, &elementManager);
  
  storage1.shrink(&lowerBound1, &upperBound1);
  ASSERT_EQ(storage1.getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(storage1.getLowerBound(), &lowerBound1));
  ASSERT_TRUE(DomainElement::isEqual(storage1.getLowerBound(), storage1.getUpperBound()));
  
  // Shrink to bigger domain should not affect the DomainConcreteStorageBitmap
  DomainElementInt64 lowerBound2(-1);
  DomainElementInt64 upperBound2(1);
  storage1.shrink(&lowerBound2, &upperBound2);
  ASSERT_EQ(storage1.getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(storage1.getLowerBound(), &lowerBound1));
  ASSERT_TRUE(DomainElement::isEqual(storage1.getLowerBound(), storage1.getUpperBound()));
  
  storage1.shrink(&lowerBound1, &upperBound2);
  ASSERT_EQ(storage1.getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(storage1.getLowerBound(), &lowerBound1));
  ASSERT_TRUE(DomainElement::isEqual(storage1.getLowerBound(), storage1.getUpperBound()));
  
  storage1.shrink(&lowerBound2, &upperBound1);
  ASSERT_EQ(storage1.getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(storage1.getLowerBound(), &lowerBound1));
  ASSERT_TRUE(DomainElement::isEqual(storage1.getLowerBound(), storage1.getUpperBound()));
  
  DomainElementInt64 lowerBound3(-10);
  DomainElementInt64 upperBound3(10);
  DomainConcreteStorageBitmap storage2(&lowerBound3, &upperBound3, &elementManager);
  
  DomainElementInt64 upperBound4(9);
  storage2.shrink(&lowerBound3, &upperBound4);
  ASSERT_EQ(storage2.getSize(), 20);
  ASSERT_TRUE(DomainElement::isEqual(storage2.getLowerBound(), &lowerBound3));
  ASSERT_TRUE(DomainElement::isEqual(storage2.getUpperBound(), &upperBound4));
  
  DomainElementInt64 lowerBound4(-9);
  storage2.shrink(&lowerBound4, &upperBound4);
  ASSERT_EQ(storage2.getSize(), 19);
  ASSERT_TRUE(DomainElement::isEqual(storage2.getLowerBound(), &lowerBound4));
  ASSERT_TRUE(DomainElement::isEqual(storage2.getUpperBound(), &upperBound4));

  DomainElementInt64 lowerBound5(9);
  DomainElementInt64 upperBound5(7);
  storage2.shrink(&lowerBound5, &upperBound5);
  ASSERT_EQ(storage2.getSize(), 0);
}// Shrink

OPTILAB_TEST(DomainConcreteStorageBitmap, Subtract)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  DomainElementInt64 lowerBound1(0);
  DomainElementInt64 upperBound1(0);
  DomainConcreteStorageBitmap storage1(&lowerBound1, &upperBound1, &elementManager);
  
  DomainElementInt64 domainElement(1);
  storage1.subtract(&domainElement);
  
  ASSERT_EQ(storage1.getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(storage1.getLowerBound(), &lowerBound1));
  ASSERT_TRUE(DomainElement::isEqual(storage1.getLowerBound(), storage1.getUpperBound()));
  
  // Subtract the only element: size should be 0
  storage1.subtract(&lowerBound1);
  ASSERT_EQ(storage1.getSize(), 0);
  
  // For loop subtract
  DomainElementInt64 lowerBound2(-10);
  DomainElementInt64 upperBound2( 10);
  DomainConcreteStorageBitmap storage2(&lowerBound2, &upperBound2, &elementManager);
  
  std::size_t domainSize = storage2.getSize();
  for (INT_64 idx = -10; idx <= 10; ++idx)
  {
    DomainElementInt64 domainElementIdx(idx);
    storage2.subtract(&domainElementIdx);
    
    domainSize--;
    ASSERT_EQ(storage2.getSize(), domainSize);
    if(domainSize)
    {
      DomainElementInt64 domainElementIdxComp(idx+1);
      ASSERT_TRUE(DomainElement::isEqual(storage2.getLowerBound(), &domainElementIdxComp));
    }
  }
  
  DomainConcreteStorageBitmap storage3(&lowerBound2, &upperBound2, &elementManager);
  std::size_t domainSizeAux = storage3.getSize();
  for (INT_64 idx = 10; idx >= -10; --idx)
  {
    DomainElementInt64 domainElementIdx(idx);
    storage3.subtract(&domainElementIdx);
    
    domainSizeAux--;
    ASSERT_EQ(storage3.getSize(), domainSizeAux);
    if(domainSizeAux)
    {
      DomainElementInt64 domainElementIdxComp(idx-1);
      ASSERT_TRUE(DomainElement::isEqual(storage3.getUpperBound(), &domainElementIdxComp));
    }
  }
}// Subtract

OPTILAB_TEST(DomainConcreteStorageBitmap, InMin)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  DomainElementInt64 lowerBound(-10);
  DomainElementInt64 upperBound( 10);
  DomainConcreteStorageBitmap storage(&lowerBound, &upperBound, &elementManager);
  
  DomainElementInt64 dElement(0);
  storage.inMin(&dElement);
  ASSERT_EQ(storage.getSize(), 11);
  ASSERT_TRUE(DomainElement::isEqual(storage.getLowerBound(), &dElement));
  ASSERT_TRUE(DomainElement::isEqual(storage.getUpperBound(), &upperBound));
  
  DomainElementInt64 dElement2(10);
  storage.inMin(&dElement2);
  ASSERT_EQ(storage.getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(storage.getLowerBound(), &dElement2));
  ASSERT_TRUE(DomainElement::isEqual(storage.getUpperBound(), &dElement2));
  
  DomainElementInt64 dElement3(11);
  storage.inMin(&dElement3);
  ASSERT_EQ(storage.getSize(), 0);
}//InMin

OPTILAB_TEST(DomainConcreteStorageBitmap, InMax)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  DomainElementInt64 lowerBound(-10);
  DomainElementInt64 upperBound( 10);
  DomainConcreteStorageBitmap storage(&lowerBound, &upperBound, &elementManager);
  
  DomainElementInt64 dElement(0);
  storage.inMax(&dElement);
  ASSERT_EQ(storage.getSize(), 11);
  ASSERT_TRUE(DomainElement::isEqual(storage.getLowerBound(), &lowerBound));
  ASSERT_TRUE(DomainElement::isEqual(storage.getUpperBound(), &dElement));
  
  DomainElementInt64 dElement2(-10);
  storage.inMax(&dElement2);
  ASSERT_EQ(storage.getSize(), 1);
  ASSERT_TRUE(DomainElement::isEqual(storage.getLowerBound(), &dElement2));
  ASSERT_TRUE(DomainElement::isEqual(storage.getUpperBound(), &dElement2));
  
  DomainElementInt64 dElement3(-11);
  storage.inMax(&dElement3);
  ASSERT_EQ(storage.getSize(), 0);
}//InMax

OPTILAB_TEST(DomainConcreteStorageBitmap, Contains)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  DomainElementInt64 lowerBound(-10);
  DomainElementInt64 upperBound( 10);
  DomainConcreteStorageBitmap storage(&lowerBound, &upperBound, &elementManager);
  
  for (INT_64 idx = -20; idx < 20; ++idx)
  {
    DomainElementInt64 domainElementIdx(idx);
    
    if(idx >= -10 && idx <= 10)
    {
      ASSERT_TRUE(storage.contains(&domainElementIdx));
    }
    else
    {
      ASSERT_FALSE(storage.contains(&domainElementIdx));
    }
  }
}//Contains

OPTILAB_TEST(DomainConcreteStorageBitmap, Mix)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  DomainElementInt64 lowerBound(-10);
  DomainElementInt64 upperBound( 10);
  DomainConcreteStorageBitmap storage(&lowerBound, &upperBound, &elementManager);
  
  DomainElementInt64 dElem(-8);
  storage.subtract(&dElem);
  ASSERT_EQ(storage.getSize(), 20);
  ASSERT_TRUE(DomainElement::isEqual(storage.getLowerBound(), &lowerBound));
  ASSERT_FALSE(storage.contains(&dElem));
  
  DomainElementInt64 dElem2(-7);
  storage.subtract(&dElem);
  storage.subtract(&dElem2);
  ASSERT_EQ(storage.getSize(), 19);
  ASSERT_TRUE(DomainElement::isEqual(storage.getLowerBound(), &lowerBound));
  ASSERT_FALSE(storage.contains(&dElem2));
  
  storage.inMin(&dElem);
  ASSERT_EQ(storage.getSize(), 17);
  DomainElementInt64 dElem6Neg(-6);
  ASSERT_TRUE(DomainElement::isEqual(storage.getLowerBound(), &dElem6Neg));
  ASSERT_TRUE(DomainElement::isEqual(storage.getUpperBound(), &upperBound));
  
  DomainElementInt64 dElem3(6);
  DomainElementInt64 dElem4(5);
  storage.subtract(&dElem3);
  storage.subtract(&dElem4);
  storage.inMax(&dElem3);
  
  DomainElementInt64 dElem4Pos(4);
  ASSERT_TRUE(DomainElement::isEqual(storage.getLowerBound(), &dElem6Neg));
  ASSERT_TRUE(DomainElement::isEqual(storage.getUpperBound(), &dElem4Pos));
  
  DomainElementInt64 dElem5(-1);
  DomainElementInt64 dElem6(0);
  storage.subtract(&dElem5);
  storage.subtract(&dElem6);
  storage.shrink(&dElem6, &dElem4);
  DomainElementInt64 dElem1Pos(1);
  ASSERT_TRUE(DomainElement::isEqual(storage.getLowerBound(), &dElem1Pos));
  ASSERT_TRUE(DomainElement::isEqual(storage.getUpperBound(), &dElem4Pos));

  ASSERT_EQ(storage.getSize(), 4);
}//Mix

OPTILAB_TEST(DomainConcreteStorageBitmap, NextElement)
{
  // Given lowerBound <= "aDomainElement" <= upperBound, return successor of "aDomainElement".
  // If "aDomainElement" == upperBound, return upperBound.
  
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  DomainElementInt64 lowerBound(-10);
  DomainElementInt64 upperBound( 10);
  DomainConcreteStorageBitmap storage(&lowerBound, &upperBound, &elementManager);
  
  ASSERT_TRUE(DomainElement::isEqual(storage.getNextElement(&upperBound), &upperBound));
  
  for (INT_64 idx = -10; idx < 10; ++idx)
  {
    DomainElementInt64 domainElementIdx(idx);
    DomainElementInt64 domainElementIdxNext(idx+1);
    ASSERT_TRUE(DomainElement::isEqual(storage.getNextElement(&domainElementIdx), &domainElementIdxNext));
  }
  
  DomainElementInt64 elementToDeletePre(-1);
  DomainElementInt64 elementToDeletePost(1);
  DomainElementInt64 elementToDelete(0);
  storage.subtract(&elementToDelete);
  
  ASSERT_TRUE(DomainElement::isEqual(storage.getNextElement(&elementToDelete), &elementToDeletePost));
  ASSERT_TRUE(DomainElement::isEqual(storage.getNextElement(&elementToDeletePre), &elementToDeletePost));
}//NextElement

OPTILAB_TEST(DomainConcreteStorageBitmap, PrevElement)
{
  // Given lowerBound <= "aDomainElement" <= upperBound, return predecessor of "aDomainElement".
  // If "aDomainElement" == lowerBound, return lowerBound.
  
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  DomainElementInt64 lowerBound(-10);
  DomainElementInt64 upperBound( 10);
  DomainConcreteStorageBitmap storage(&lowerBound, &upperBound, &elementManager);
  
  ASSERT_TRUE(DomainElement::isEqual(storage.getPrevElement(&lowerBound), &lowerBound));
  
  for (INT_64 idx = 10; idx > -10; --idx)
  {
    DomainElementInt64 domainElementIdx(idx);
    DomainElementInt64 domainElementIdxPrev(idx-1);
    ASSERT_TRUE(DomainElement::isEqual(storage.getPrevElement(&domainElementIdx), &domainElementIdxPrev));
  }
  
  DomainElementInt64 elementToDeletePre(-1);
  DomainElementInt64 elementToDeletePost(1);
  DomainElementInt64 elementToDelete(0);
  storage.subtract(&elementToDelete);
  
  ASSERT_TRUE(DomainElement::isEqual(storage.getPrevElement(&elementToDelete), &elementToDeletePre));
  ASSERT_TRUE(DomainElement::isEqual(storage.getPrevElement(&elementToDeletePost), &elementToDeletePre));
}//PrevElement

OPTILAB_TEST(DomainConcreteStorageBitmap, ElementByIndex)
{
  // Returns the "aElementIndex" - 1 ^th DomainElement, where
  // 0 <= "aElementIndex" < domain size.
  
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  DomainElementInt64 lowerBound(-10);
  DomainElementInt64 upperBound( 10);
  DomainConcreteStorageBitmap storage(&lowerBound, &upperBound, &elementManager);
  
  std::size_t thIdx{0};
  for (INT_64 idx = -10; idx < 10; ++idx)
  {
    DomainElementInt64 domainElementIdx(idx);
    ASSERT_TRUE(DomainElement::isEqual(storage.getElementByIndex(thIdx++), &domainElementIdx));
  }
}//ElementByIndex
