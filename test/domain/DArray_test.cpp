
#include "utest_globals.hpp"

#include "DArrayMatrix.hpp"
#include "DomainElementManager.hpp"
#include "DomainElementInt64.hpp"

OPTILAB_TEST(DomainElementArray, Basics)
{
  using namespace Core;
  
  // Create a DomainElementArray of size 3
  DomainElementArray domArray(3);
  
  ASSERT_EQ(domArray.size(), 3);
  for(std::size_t idx = 0; idx < domArray.size(); ++idx)
  {
    ASSERT_FALSE(domArray[idx]);
  }
}//Basics

OPTILAB_TEST(DomainElementArray, DomainElementAssignment)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  
  // Create a DomainElementArray of size 3
  DomainElementArray domArray(3);
  
  for(std::size_t idx = 0; idx < domArray.size(); ++idx)
  {
    domArray.assignElementToCell(idx, elementManager.createDomainElementInt64(idx));
  }
  ASSERT_EQ(domArray.size(), 3);
  
  // Check assigned elements
  for(std::size_t idx = 0; idx < domArray.size(); ++idx)
  {
    ASSERT_TRUE(DomainElement::isEqual(elementManager.createDomainElementInt64(idx), domArray[idx]));
  }
  
  ASSERT_FALSE(domArray.empty());
  
  domArray.clear();
  for(std::size_t idx = 0; idx < domArray.size(); ++idx)
  {
    ASSERT_TRUE(domArray[idx] == nullptr);
  }
  ASSERT_TRUE(domArray.empty());
}//DomainElementAssignment

OPTILAB_TEST(DomainElementArray, DomainElementFind)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  
  // Create a DomainElementArray of size 3
  DomainElementArray domArray(3);
  
  for(std::size_t idx = 0; idx < domArray.size(); ++idx)
  {
    domArray.assignElementToCell(idx, elementManager.createDomainElementInt64(idx));
  }
  
  // Check assigned elements
  for(std::size_t idx = 0; idx < domArray.size(); ++idx)
  {
    ASSERT_EQ(idx, domArray.find(elementManager.createDomainElementInt64(idx)));
  }
  ASSERT_EQ(domArray.size(), domArray.find(elementManager.createDomainElementInt64(-1)));
  ASSERT_EQ(domArray.size(), domArray.find(elementManager.createDomainElementInt64(+3)));
}//DomainElementFind

OPTILAB_TEST(DomainElementArray, CopyConstructorAssignmentOperator)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  
  // Create a DomainElementArray of size 3
  DomainElementArray domArray(3);
  
  for(std::size_t idx = 0; idx < domArray.size(); ++idx)
  {
    domArray.assignElementToCell(idx, elementManager.createDomainElementInt64(idx));
  }
  
  DomainElementArray domArrayCopy = domArray;
  ASSERT_EQ(domArrayCopy.size(), 3);
  
  // Check assigned elements
  for(std::size_t idx = 0; idx < domArray.size(); ++idx)
  {
    ASSERT_TRUE(DomainElement::isEqual(elementManager.createDomainElementInt64(idx), domArrayCopy[idx]));
  }
  
  DomainElementArray domArrayAssignment(5);
  domArrayAssignment = domArray;
  ASSERT_EQ(domArrayAssignment.size(), 3);
  
  // Check assigned elements
  for(std::size_t idx = 0; idx < domArray.size(); ++idx)
  {
    ASSERT_TRUE(DomainElement::isEqual(elementManager.createDomainElementInt64(idx), domArrayAssignment[idx]));
  }
}//CopyConstructorAssignmentOperator
