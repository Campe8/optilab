
#include "utest_globals.hpp"

#include "DomainElementKey.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementVoid.hpp"

#include <memory>
#include <unordered_map>

namespace Core {
  class DomainElementTest : public DomainElementInt64 {
  public:
    DomainElementTest(INT_64 aVal)
    : DomainElementInt64(aVal)
    {
    }
    
    ~DomainElementTest()
    {
    }
    
    inline RoundingMode getDomainElementRoundingMode() const
    {
      return getRoundingMode();
    }
  };
}

OPTILAB_TEST(DomainElement, RoundingMode)
{
  using namespace Core;
  std::unique_ptr<DomainElementTest> dElement(new DomainElementTest(0));
  
  // Default rounding mode is UNSPEC
  ASSERT_EQ(dElement->getDomainElementRoundingMode(), DomainElement::RoundingMode::DE_ROUNDING_UNSPEC);
  
  dElement->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_CEILING);
  ASSERT_EQ(dElement->getDomainElementRoundingMode(), DomainElement::RoundingMode::DE_ROUNDING_CEILING);
  
  dElement->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_FLOOR);
  ASSERT_EQ(dElement->getDomainElementRoundingMode(), DomainElement::RoundingMode::DE_ROUNDING_FLOOR);
}//StaticZero

OPTILAB_TEST(DomainElement, StaticZero)
{
  using namespace Core;
  DomainElement *domElemInt64 = new DomainElementInt64(3);
  DomainElement *domElemInt64Zero = new DomainElementInt64(0);
  DomainElement *domElemZero = DomainElement::zero(domElemInt64);
  
  ASSERT_TRUE(DomainElement::isEqual(domElemInt64Zero, domElemZero));
  
  delete domElemInt64;
  delete domElemInt64Zero;
}//StaticZero

OPTILAB_TEST(DomainElement, StaticMin)
{
  using namespace Core;
  
  DomainElement *domElemInt64_3 = new DomainElementInt64(3);
  DomainElement *domElemInt64_Neg3 = new DomainElementInt64(-3);
  DomainElement *domElemInt64_PosInf = new DomainElementInt64(std::numeric_limits<INT_64>::max());
  DomainElement *domElemInt64_NegInf = new DomainElementInt64(std::numeric_limits<INT_64>::lowest() + 1);
  
  DomainElement *opDomElem = nullptr;
  
  // Test min
  opDomElem = DomainElement::min(domElemInt64_Neg3, domElemInt64_3);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_Neg3)->getDomainKey()));
  
  opDomElem = DomainElement::min(domElemInt64_3, domElemInt64_3);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_3)->getDomainKey()));
  
  opDomElem = DomainElement::min(domElemInt64_3, domElemInt64_Neg3);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_Neg3)->getDomainKey()));
  
  opDomElem = DomainElement::min(domElemInt64_3, domElemInt64_PosInf);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_3)->getDomainKey()));
  
  opDomElem = DomainElement::min(domElemInt64_3, domElemInt64_NegInf);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_NegInf)->getDomainKey()));
  
  delete domElemInt64_3;
  delete domElemInt64_Neg3;
  delete domElemInt64_PosInf;
  delete domElemInt64_NegInf;
}

OPTILAB_TEST(DomainElement, StaticMax)
{
  using namespace Core;
  
  DomainElement *domElemInt64_3 = new DomainElementInt64(3);
  DomainElement *domElemInt64_Neg3 = new DomainElementInt64(-3);
  DomainElement *domElemInt64_PosInf = new DomainElementInt64(std::numeric_limits<INT_64>::max());
  DomainElement *domElemInt64_NegInf = new DomainElementInt64(std::numeric_limits<INT_64>::lowest() + 1);
  
  DomainElement *opDomElem = nullptr;
  
  opDomElem = DomainElement::max(domElemInt64_Neg3, domElemInt64_3);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_3)->getDomainKey()));
  
  opDomElem = DomainElement::max(domElemInt64_3, domElemInt64_3);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_3)->getDomainKey()));
  
  opDomElem = DomainElement::max(domElemInt64_3, domElemInt64_Neg3);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_3)->getDomainKey()));
  
  opDomElem = DomainElement::max(domElemInt64_3, domElemInt64_PosInf);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_PosInf)->getDomainKey()));
  
  opDomElem = DomainElement::max(domElemInt64_3, domElemInt64_NegInf);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_3)->getDomainKey()));
  
  delete domElemInt64_3;
  delete domElemInt64_Neg3;
  delete domElemInt64_PosInf;
  delete domElemInt64_NegInf;
}

OPTILAB_TEST(DomainElement, StaticAbs)
{
  using namespace Core;
  
  DomainElementCategoryMap categoryMap;
  
  DomainElement *domElemInt64_3 = new DomainElementInt64(3);
  DomainElement *domElemInt64_Neg3 = new DomainElementInt64(-3);
  DomainElement *domElemInt64_PosInf = new DomainElementInt64(std::numeric_limits<INT_64>::max());
  DomainElement *domElemInt64_NegInf = new DomainElementInt64(std::numeric_limits<INT_64>::lowest() + 1);
  
  DomainElement *opDomElem = nullptr;
  
  opDomElem = DomainElement::abs(domElemInt64_Neg3);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_3)->getDomainKey()));
  
  opDomElem = DomainElement::abs(domElemInt64_3);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_3)->getDomainKey()));
  
  opDomElem = DomainElement::abs(domElemInt64_NegInf);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_PosInf)->getDomainKey()));
  
  opDomElem = DomainElement::abs(domElemInt64_PosInf);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_PosInf)->getDomainKey()));
  
  delete domElemInt64_3;
  delete domElemInt64_Neg3;
  delete domElemInt64_PosInf;
  delete domElemInt64_NegInf;
}//StaticAbs

OPTILAB_TEST(DomainElement, StaticMirror)
{
  using namespace Core;
  
  DomainElementCategoryMap categoryMap;
  
  DomainElement *domElemInt64_3 = new DomainElementInt64(3);
  DomainElement *domElemInt64_Neg3 = new DomainElementInt64(-3);
  DomainElement *domElemInt64_PosInf = new DomainElementInt64(std::numeric_limits<INT_64>::max());
  DomainElement *domElemInt64_NegInf = new DomainElementInt64(std::numeric_limits<INT_64>::lowest() + 1);
  
  DomainElement *opDomElem = nullptr;
  
  opDomElem = DomainElement::mirror(domElemInt64_Neg3);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) ==
              *(DomainElementInt64::cast(domElemInt64_3)->getDomainKey()));
  
  opDomElem = DomainElement::mirror(domElemInt64_3);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) ==
              *(DomainElementInt64::cast(domElemInt64_Neg3)->getDomainKey()));
  
  opDomElem = DomainElement::mirror(domElemInt64_NegInf);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) ==
              *(DomainElementInt64::cast(domElemInt64_PosInf)->getDomainKey()));
  
  opDomElem = DomainElement::mirror(domElemInt64_PosInf);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) ==
              *(DomainElementInt64::cast(domElemInt64_NegInf)->getDomainKey()));
  
  delete domElemInt64_3;
  delete domElemInt64_Neg3;
  delete domElemInt64_PosInf;
  delete domElemInt64_NegInf;
}//StaticMirror

OPTILAB_TEST(DomainElement, StaticDiv)
{
  using namespace Core;
  
  DomainElement *domElemInt64_0 = new DomainElementInt64(0);
  DomainElement *domElemInt64_3 = new DomainElementInt64(3);
  DomainElement *domElemInt64_4 = new DomainElementInt64(4);
  DomainElement *domElemInt64_12 = new DomainElementInt64(12);
  DomainElement *domElemInt64_Neg3 = new DomainElementInt64(-3);
  DomainElement *domElemInt64_Neg4 = new DomainElementInt64(-4);
  DomainElement *domElemInt64_PosInf = new DomainElementInt64(std::numeric_limits<INT_64>::max());
  DomainElement *domElemInt64_NegInf = new DomainElementInt64(std::numeric_limits<INT_64>::lowest() + 1);
  
  DomainElement *opDomElem = nullptr;
  
  opDomElem = DomainElement::div(domElemInt64_12, domElemInt64_4);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_3)->getDomainKey()));
  
  opDomElem = DomainElement::div(domElemInt64_12, domElemInt64_Neg4);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_Neg3)->getDomainKey()));
  
  opDomElem = DomainElement::div(domElemInt64_PosInf, domElemInt64_NegInf);
  
  DomainElementInt64 elementInt64_neg1(-1);
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(&elementInt64_neg1)->getDomainKey()));
  
  opDomElem = DomainElement::div(domElemInt64_NegInf, domElemInt64_PosInf);
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(&elementInt64_neg1)->getDomainKey()));
  
  opDomElem = DomainElement::div(domElemInt64_12, domElemInt64_0);
  ASSERT_TRUE(DomainElementVoid::isa(opDomElem));
  
  delete domElemInt64_0;
  delete domElemInt64_3;
  delete domElemInt64_4;
  delete domElemInt64_12;
  delete domElemInt64_Neg3;
  delete domElemInt64_Neg4;
  delete domElemInt64_PosInf;
  delete domElemInt64_NegInf;
}

OPTILAB_TEST(DomainElement, StaticPlus)
{
  using namespace Core;
  
  DomainElement *domElemInt64_0 = new DomainElementInt64(0);
  DomainElement *domElemInt64_3 = new DomainElementInt64(3);
  DomainElement *domElemInt64_4 = new DomainElementInt64(4);
  DomainElement *domElemInt64_7 = new DomainElementInt64(7);
  DomainElement *domElemInt64_12 = new DomainElementInt64(12);
  DomainElement *domElemInt64_Neg3 = new DomainElementInt64(-3);
  DomainElement *domElemInt64_Neg4 = new DomainElementInt64(-4);
  DomainElement *domElemInt64_Neg7 = new DomainElementInt64(-7);
  DomainElement *domElemInt64_Neg12 = new DomainElementInt64(-12);
  DomainElement *opDomElem = nullptr;
  
  opDomElem = DomainElement::plus(domElemInt64_12, domElemInt64_0);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_12)->getDomainKey()));
  
  opDomElem = DomainElement::plus(domElemInt64_Neg12, domElemInt64_0);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_Neg12)->getDomainKey()));
  
  opDomElem = DomainElement::plus(domElemInt64_3, domElemInt64_4);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_7)->getDomainKey()));
  
  opDomElem = DomainElement::plus(domElemInt64_7, domElemInt64_Neg4);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_3)->getDomainKey()));
  
  opDomElem = DomainElement::plus(domElemInt64_Neg7, domElemInt64_4);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_Neg3)->getDomainKey()));
  
  delete domElemInt64_0;
  delete domElemInt64_3;
  delete domElemInt64_4;
  delete domElemInt64_7;
  delete domElemInt64_12;
  delete domElemInt64_Neg3;
  delete domElemInt64_Neg4;
  delete domElemInt64_Neg7;
  delete domElemInt64_Neg12;
}

OPTILAB_TEST(DomainElement, StaticTimes)
{
  using namespace Core;
  
  DomainElement *domElemInt64_0 = new DomainElementInt64(0);
  DomainElement *domElemInt64_3 = new DomainElementInt64(3);
  DomainElement *domElemInt64_4 = new DomainElementInt64(4);
  DomainElement *domElemInt64_12 = new DomainElementInt64(12);
  DomainElement *domElemInt64_Neg3 = new DomainElementInt64(-3);
  DomainElement *domElemInt64_Neg4 = new DomainElementInt64(-4);
  DomainElement *domElemInt64_Neg12 = new DomainElementInt64(-12);
  DomainElement *opDomElem = nullptr;
  
  opDomElem = DomainElement::times(domElemInt64_12, domElemInt64_0);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_0)->getDomainKey()));
  
  opDomElem = DomainElement::times(domElemInt64_Neg12, domElemInt64_0);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_0)->getDomainKey()));
  
  opDomElem = DomainElement::times(domElemInt64_3, domElemInt64_4);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_12)->getDomainKey()));
  
  opDomElem = DomainElement::times(domElemInt64_3, domElemInt64_Neg4);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_Neg12)->getDomainKey()));
  
  opDomElem = DomainElement::times(domElemInt64_Neg3, domElemInt64_Neg4);
  ASSERT_TRUE(DomainElementInt64::isa(opDomElem));
  ASSERT_TRUE(*(DomainElementInt64::cast(opDomElem)->getDomainKey()) == *(DomainElementInt64::cast(domElemInt64_12)->getDomainKey()));
  
  delete domElemInt64_0;
  delete domElemInt64_3;
  delete domElemInt64_4;
  delete domElemInt64_12;
  delete domElemInt64_Neg3;
  delete domElemInt64_Neg4;
  delete domElemInt64_Neg12;
}

OPTILAB_TEST(DomainElement, sizeSpan)
{
  using namespace Core;
  
  std::unique_ptr<DomainElement>domElemInt64_10(new DomainElementInt64(10));
  std::unique_ptr<DomainElement>domElemInt64_20(new DomainElementInt64(20));
  
  ASSERT_EQ(DomainElement::sizeSpan(domElemInt64_10.get(), domElemInt64_20.get()), 11);
}//sizeSpan

OPTILAB_TEST(DomainElement, StaticSuccessor)
{
  using namespace Core;
  
  std::unique_ptr<DomainElement>domElemInt64_0(new DomainElementInt64(0));
  std::unique_ptr<DomainElement>domElemInt64_1(new DomainElementInt64(1));
  
  DomainElement *opDomElem = DomainElement::successor(domElemInt64_0.get());
  ASSERT_TRUE(*(DomainElementInt64::cast(domElemInt64_1.get())->getDomainKey()) ==
              *(DomainElementInt64::cast(opDomElem)->getDomainKey()));
}//StaticSuccessor

OPTILAB_TEST(DomainElement, StaticPredecessor)
{
  using namespace Core;
  
  std::unique_ptr<DomainElement>domElemInt64_0(new DomainElementInt64(0));
  std::unique_ptr<DomainElement>domElemInt64_1(new DomainElementInt64(1));
  
  DomainElement *opDomElem = DomainElement::predecessor(domElemInt64_1.get());
  ASSERT_TRUE(*(DomainElementInt64::cast(domElemInt64_0.get())->getDomainKey()) ==
              *(DomainElementInt64::cast(opDomElem)->getDomainKey()));
}//StaticPredecessor
