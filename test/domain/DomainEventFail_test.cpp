
#include "utest_globals.hpp"

#include "DomainEventBound.hpp"
#include "DomainEventFail.hpp"
#include "DomainEventVoid.hpp"
#include "DomainEventChange.hpp"
#include "DomainEventLwb.hpp"
#include "DomainEventUpb.hpp"
#include "DomainEventSingleton.hpp"
#include "DomainEventUndef.hpp"

OPTILAB_TEST(DomainEventFail, Basics)
{
  using namespace Core;
  
  // Check type
  std::unique_ptr<DomainEvent> event(new DomainEventFail());
  ASSERT_TRUE(event->getEventType() == DomainEventType::FAIL_EVENT);
  
  // Check isa and cast
  DomainEventFail *castEvent = DomainEventFail::cast(event.get());
  ASSERT_TRUE(DomainEventFail::isa(event.get()));
  ASSERT_TRUE(DomainEventFail::isa(castEvent));
}// Basics

OPTILAB_TEST(DomainEventFail, overlap)
{
  using namespace Core;
  
  std::unique_ptr<DomainEvent> eventUndef(new DomainEventUndef());
  std::unique_ptr<DomainEvent> eventBound(new DomainEventBound());
  std::unique_ptr<DomainEvent> eventFail(new DomainEventFail());
  std::unique_ptr<DomainEvent> eventVoid(new DomainEventVoid());
  std::unique_ptr<DomainEvent> eventChange(new DomainEventBound());
  std::unique_ptr<DomainEvent> eventLwb(new DomainEventLwb());
  std::unique_ptr<DomainEvent> eventUbp(new DomainEventUpb());
  std::unique_ptr<DomainEvent> eventSingleton(new DomainEventSingleton());
  
  ASSERT_FALSE(eventFail->overlaps(eventChange.get()));
  ASSERT_FALSE(eventFail->overlaps(eventLwb.get()));
  ASSERT_FALSE(eventFail->overlaps(eventUbp.get()));
  ASSERT_FALSE(eventFail->overlaps(eventBound.get()));
  ASSERT_TRUE(eventFail->overlaps(eventFail.get()));
  ASSERT_FALSE(eventFail->overlaps(eventVoid.get()));
  ASSERT_FALSE(eventFail->overlaps(eventSingleton.get()));
  ASSERT_FALSE(eventFail->overlaps(eventUndef.get()));
}// overlap
