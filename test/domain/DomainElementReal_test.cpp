
#include "utest_globals.hpp"

#include "DomainElementKey.hpp"
#include "DomainElementReal.hpp"
#include "DomainElementVoid.hpp"

#include <memory>
#include <limits>
#include <unordered_map>

OPTILAB_TEST(DomainElementReal, Constructor)
{
  using namespace Core;
  
  // Test isa and cast
  std::unique_ptr<DomainElement> domElementReal_1p234(new DomainElementReal(1.234));
  DomainElementReal *domElementReal_1p234Cast = DomainElementReal::cast(domElementReal_1p234.get());
  
  ASSERT_TRUE(DomainElementReal::isa(domElementReal_1p234.get()));
  ASSERT_NE(domElementReal_1p234Cast, nullptr);
  ASSERT_TRUE(DomainElementReal::isa(domElementReal_1p234Cast));
  
  // Test copy constructor and assignment operator
  DomainElementReal realBase(1.234);
  DomainElementReal realBaseAux(5.678);
  ASSERT_FALSE(*realBase.getDomainKey() == *realBaseAux.getDomainKey());
  
  realBaseAux = realBase;
  ASSERT_TRUE(*realBase.getDomainKey() == *realBaseAux.getDomainKey());
  
  DomainElementReal realBaseMove = DomainElementReal(2.345);
  ASSERT_FALSE(*realBaseMove.getDomainKey() == *realBaseAux.getDomainKey());
  
  realBaseMove = realBaseAux;
  ASSERT_TRUE(*realBaseMove.getDomainKey() == *realBaseAux.getDomainKey());
}//Constructor

OPTILAB_TEST(DomainElementReal, GetSetRealStep)
{
  using namespace Core;
  
  DomainElementReal realDomainElement(1.234);
  
  // Default step
  ASSERT_EQ(realDomainElement.getRealStep(), REAL_DFT_STEP);
  
  REAL step{0.0000001};
  realDomainElement.setRealStep(step);
  ASSERT_EQ(realDomainElement.getRealStep(), step);
}//GetSetRealStep

OPTILAB_TEST(DomainElementReal, IsEqualNotEqual)
{
  using namespace Core;
  
  // isEqual compares using best precision
  DomainElementReal real1p234(1.234);
  DomainElementReal real1p234567(1.234567);
  DomainElementReal real1p2(1.2);
  
  ASSERT_FALSE(real1p234.isEqual(&real1p234567));
  ASSERT_TRUE(real1p234.isEqual(&real1p234));
  ASSERT_FALSE(real1p234.isEqual(&real1p2));
  
  // Coverage with not equal
  ASSERT_TRUE(real1p234.isNotEqual(&real1p234567));
  ASSERT_TRUE(real1p234.isNotEqual(&real1p2));
  ASSERT_FALSE(real1p234.isNotEqual(&real1p234));
}//IsEqual

OPTILAB_TEST(DomainElementReal, IsLessThanOrEqual)
{
  using namespace Core;
  
  // isLessThan and isLessThanOrEqual compares using best precision
  DomainElementReal real1p23(1.23);
  DomainElementReal real1p234(1.234);
  DomainElementReal real1p234567(1.234567);
  
  DomainElementReal realNeg1p23(-1.23);
  DomainElementReal realNeg1p234(-1.234);
  DomainElementReal realNeg1p234567(-1.234567);
  
  // Less than or equal
  ASSERT_TRUE(real1p23.isLessThanOrEqual(&real1p23));
  ASSERT_TRUE(real1p234.isLessThanOrEqual(&real1p234));
  ASSERT_TRUE(real1p234567.isLessThanOrEqual(&real1p234567));
  ASSERT_TRUE(realNeg1p23.isLessThanOrEqual(&realNeg1p23));
  ASSERT_TRUE(realNeg1p234.isLessThanOrEqual(&realNeg1p234));
  ASSERT_TRUE(realNeg1p234567.isLessThanOrEqual(&realNeg1p234567));
  
  ASSERT_TRUE(real1p23.isLessThan(&real1p234));
  ASSERT_TRUE(real1p234.isLessThan(&real1p234567));
  
  ASSERT_FALSE(real1p234567.isLessThanOrEqual(&real1p23));
  ASSERT_FALSE(real1p234567.isLessThanOrEqual(&real1p234));
  ASSERT_FALSE(real1p234567.isLessThan(&real1p23));
  ASSERT_FALSE(real1p234567.isLessThan(&real1p234));
  
  ASSERT_TRUE(realNeg1p23.isLessThanOrEqual(&real1p23));
  ASSERT_TRUE(realNeg1p23.isLessThanOrEqual(&real1p234));
  ASSERT_TRUE(realNeg1p23.isLessThanOrEqual(&real1p234567));
  
  ASSERT_TRUE(realNeg1p234567.isLessThanOrEqual(&realNeg1p234));
  ASSERT_TRUE(realNeg1p234.isLessThanOrEqual(&realNeg1p23));
}//IsLessThanOrEqual

OPTILAB_TEST(DomainElementReal, MinMax)
{
  using namespace Core;
  
  // isLessThan and isLessThanOrEqual compares using best precision
  DomainElementReal real1p234(1.234);
  DomainElementReal real1p234567(1.234567);
  
  ASSERT_TRUE(real1p234.min(&real1p234)->isEqual(&real1p234));
  ASSERT_TRUE(real1p234.min(&real1p234567)->isEqual(&real1p234));
  
  ASSERT_TRUE(real1p234.max(&real1p234)->isEqual(&real1p234));
  ASSERT_TRUE(real1p234.max(&real1p234567)->isEqual(&real1p234567));
  
  DomainElementReal realNegInf(std::numeric_limits<REAL>::lowest());
  DomainElementReal realPosInf(std::numeric_limits<REAL>::max());
  ASSERT_TRUE(realNegInf.min(&realPosInf)->isEqual(&realNegInf));
}//MinMax

OPTILAB_TEST(DomainElementReal, Abs)
{
  using namespace Core;
  
  DomainElementReal real1p234(1.234);
  DomainElementReal realNeg1p234(-1.234);
  
  ASSERT_TRUE(real1p234.abs()->isEqual(&real1p234));
  ASSERT_TRUE(realNeg1p234.abs()->isEqual(&real1p234));
}//abs

OPTILAB_TEST(DomainElementReal, Zero)
{
  using namespace Core;
  
  DomainElementReal real1p234(1.234);
  DomainElementReal realZero = *DomainElementReal::cast(real1p234.zero());
  DomainElementReal realZeroAux(0.0);
  
  ASSERT_TRUE(realZeroAux.isEqual(&realZero));
}//Zero

OPTILAB_TEST(DomainElementReal, Mirror)
{
  using namespace Core;
  
  DomainElementReal real1p234(1.234);
  DomainElementReal realNeg1p234(-1.234);
  DomainElementReal real0(0);
  
  ASSERT_TRUE(real1p234.mirror()->isEqual(&realNeg1p234));
  ASSERT_TRUE(realNeg1p234.mirror()->isEqual(&real1p234));
  ASSERT_TRUE(real0.mirror()->isEqual(&real0));
}//Mirror

OPTILAB_TEST(DomainElementReal, SuccessorPredecessor)
{
  using namespace Core;
  
  DomainElementReal real1p234(1.234);
  DomainElementReal realNeg1p234(-1.234);
  
  DomainElementReal real1p234Succ(1.234 + REAL_DFT_STEP);
  DomainElementReal real1p234Pred(1.234 - REAL_DFT_STEP);
  
  DomainElementReal realNeg1p234Succ(-1.234 + REAL_DFT_STEP);
  DomainElementReal realNeg1p234Pred(-1.234 - REAL_DFT_STEP);
  
  ASSERT_TRUE(real1p234.successor()->isEqual(&real1p234Succ));
  ASSERT_TRUE(real1p234.predecessor()->isEqual(&real1p234Pred));
  
  ASSERT_TRUE(realNeg1p234.successor()->isEqual(&realNeg1p234Succ));
  ASSERT_TRUE(realNeg1p234.predecessor()->isEqual(&realNeg1p234Pred));
  
  REAL newPrec = 1.0;
  real1p234.setRealStep(newPrec);
  DomainElementReal real1p234SuccLowPrec(1.234 + newPrec);
  DomainElementReal real1p234PredLowPrec(1.234 - newPrec);
  
  ASSERT_TRUE(real1p234.successor()->isEqual(&real1p234SuccLowPrec));
  ASSERT_TRUE(real1p234.predecessor()->isEqual(&real1p234PredLowPrec));
}//SuccessorPredecessor

OPTILAB_TEST(DomainElementReal, Div)
{
  using namespace Core;
  
  DomainElementReal real1p234(1.234);
  DomainElementReal real2p468(2.468);
  DomainElementReal real1p0(1.0);
  DomainElementReal real0p1(0.1);
  DomainElementReal real0p5(0.5);
  DomainElementReal real12p34(12.34);
  DomainElementReal real0(0);
  
  ASSERT_TRUE(real1p234.div(&real0)->isEqual(DomainElementManager::getInstance().getVoidElement()));
  ASSERT_TRUE(real1p234.div(&real1p234)->isEqual(&real1p0));
  ASSERT_TRUE(real1p234.div(&real0p1)->isEqual(&real12p34));
  ASSERT_TRUE(real1p234.div(&real2p468)->isEqual(&real0p5));
}//Div

OPTILAB_TEST(DomainElementReal, Times)
{
  using namespace Core;
  
  DomainElementReal real1p234(1.234);
  DomainElementReal realNeg2p468(1.234 * 2.0 * -1.0);
  DomainElementReal real1p0(1.0);
  DomainElementReal realNeg2p0(-2.0);
  DomainElementReal real0(0);
  DomainElementReal real0p1(0.1);
  DomainElementReal real0p1234(1.234 * 0.1);
  DomainElementReal real1000p0(1000.0);
  DomainElementReal real1234p0(1.234 * 1000.0);
  
  ASSERT_TRUE(real1p234.times(&real0)->isEqual(&real0));
  ASSERT_TRUE(real1p234.times(&realNeg2p0)->isEqual(&realNeg2p468));
  ASSERT_TRUE(real1p234.times(&real1p0)->isEqual(&real1p234));
  ASSERT_TRUE(real1p234.times(&real0p1)->isEqual(&real0p1234));
  ASSERT_TRUE(real1p234.times(&real1000p0)->isEqual(&real1234p0));
}//Times

OPTILAB_TEST(DomainElementReal, Plus)
{
  using namespace Core;
  
  DomainElementReal real1p234(1.234);
  DomainElementReal realNeg1p234(-1.234);
  DomainElementReal real2p468(1.234 * 2.0);
  DomainElementReal real0(0);
  DomainElementReal real10p234(10.234);
  DomainElementReal real11p468(10.234 + 1.234);
  
  ASSERT_TRUE(real1p234.plus(&real0)->isEqual(&real1p234));
  ASSERT_TRUE(real1p234.plus(&real1p234)->isEqual(&real2p468));
  ASSERT_TRUE(real1p234.plus(&realNeg1p234)->isEqual(&real0));
  ASSERT_TRUE(real1p234.plus(&real10p234)->isEqual(&real11p468));
}//Plus

OPTILAB_TEST(DomainElementReal, SizeSpan)
{
  using namespace Core;
  
  DomainElementReal real1p234(1.234);
  DomainElementReal realNeg1p234(-1.234);
  DomainElementReal real2p468(1.234 * 2.0);
  DomainElementReal real0(0);
  DomainElementReal real10p234(10.234);
  DomainElementReal real11p468(10.234 + 1.234);
  
  ASSERT_EQ(real1p234.sizeSpan(&real1p234), 0);
  ASSERT_TRUE(real1p234.sizeSpan(&real0) > 0);
}//SizeSpan
