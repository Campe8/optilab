#pragma once

#include "Domain.hpp"
#include "DomainElementKey.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementVoid.hpp"
#include "DomainElementManager.hpp"
#include "DomainElementInt64Container.hpp"

#include "DomainInteger.hpp"
#include "DomainBoolean.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"

namespace Core {
  
  class DomainTest : public Domain
  {
  public:
    DomainTest()
    : Domain(DomainClass::DOM_UNDEF)
    {
      DomainElementManager& elementManager = DomainElementManager::getInstance();
      
      // Instantiate new container
      pElementContainer.reset(new DomainElementInt64Container(elementManager.createDomainElementInt64(-10),
                                                              elementManager.createDomainElementInt64(+10)));
      
      // Instantiate new decorator
      pDecorator.reset(new DomainDecorator(pElementContainer.get()));
    }
    
    ~DomainTest()
    {
    }
    
  protected:
    DomainElementContainer* getDomainContainer() const override
    {
      return pElementContainer.get();
    }
    
    DomainDecorator* getDomainDecorator() const override
    {
      return pDecorator.get();
    }
    
  private:
    /// Domain element container
    std::unique_ptr<DomainElementContainer> pElementContainer;
    
    /// Domain decorator
    std::unique_ptr<DomainDecorator> pDecorator;
  };
  
}// end namespace Core
