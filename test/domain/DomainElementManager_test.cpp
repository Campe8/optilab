
#include "utest_globals.hpp"

#include "DomainElementManager.hpp"
#include "DomainElementVoid.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementReal.hpp"

#include <unordered_map>

OPTILAB_TEST(DomainElementManager, Basics)
{
  using namespace Core;
  
  DomainElementManager& manager = DomainElementManager::getInstance();
  manager.clear();
  ASSERT_EQ(manager.size(), 0);
  
  DomainElementInt64 localINT64 = manager.createLocalDomainElementInt64(100);
  ASSERT_EQ(manager.size(), 0);
  
  DomainElementReal localREAL = manager.createLocalDomainElementReal(100.123);
  ASSERT_EQ(manager.size(), 0);
  
  DomainElementKey key(static_cast<INT_64>(0), DomainElementType::DET_INT_64);
  ASSERT_TRUE(manager.find(&key) == nullptr);
  ASSERT_EQ(manager.size(), 0);
  
  // Create a new element using the DomainElementManager
  DomainElement *element = manager.createDomainElementInt64(1000);
  ASSERT_FALSE(element == nullptr);
  ASSERT_TRUE(DomainElementInt64::isa(element));
  
  auto keyIn = element->getDomainKey();
  ASSERT_EQ(manager.size(), 1);
  
  // Check that the element is in the manager
  auto elementIn = manager.find(keyIn);
  ASSERT_FALSE(elementIn == nullptr);
  ASSERT_TRUE(*(elementIn->getDomainKey()) == *keyIn);
  
  elementIn = manager.find(keyIn);
  ASSERT_FALSE(elementIn == nullptr);
  ASSERT_TRUE(*(elementIn->getDomainKey()) == *keyIn);
  
  // Delete elements and check size
  manager.clear();
  ASSERT_EQ(manager.size(), 0);
}// Basics

OPTILAB_TEST(DomainElementManager, findAndCreateDomainElementInt64)
{
  using namespace Core;
  
  DomainElementManager& manager = DomainElementManager::getInstance();
  manager.clear();
  
  DomainElementInt64 int64Element(1000);
  ASSERT_TRUE(manager.findDomainElementInt64(int64Element.getValue()) == nullptr);
  
  DomainElementInt64 *createdElement = manager.createDomainElementInt64(2000);
  ASSERT_FALSE(manager.findDomainElementInt64(createdElement->getValue()) == nullptr);
}//findAndCreateDomainElementInt64

OPTILAB_TEST(DomainElementManager, findAndCreateDomainElementReal)
{
  using namespace Core;
  
  DomainElementManager& manager = DomainElementManager::getInstance();
  manager.clear();
  
  DomainElementReal realElement(1.234);
  ASSERT_TRUE(manager.findDomainElementReal(realElement.getValue()) == nullptr);
  
  DomainElementReal *createdElement = manager.createDomainElementReal(2000.123);
  ASSERT_FALSE(manager.findDomainElementReal(createdElement->getValue()) == nullptr);
}//findAndCreateDomainElementReal

OPTILAB_TEST(DomainElementManager, Clone)
{
  using namespace Core;
  
  DomainElementManager& manager = DomainElementManager::getInstance();
  manager.clear();
  
  DomainElementVoid  voidElement;
  DomainElementInt64 int64Element(1000);
  DomainElementInt64 *createdElement = manager.createDomainElementInt64(2000);
  
  // int64Element should not be present before cloning it
  ASSERT_TRUE(manager.findDomainElementInt64(int64Element.getValue()) == nullptr);
  
  ASSERT_TRUE(DomainElementVoid::isa(manager.clone(&voidElement)));
  ASSERT_TRUE(DomainElement::isEqual(&int64Element, manager.clone(&int64Element)));
  ASSERT_TRUE(DomainElement::isEqual(createdElement, manager.clone(createdElement)));
  
  // int64Element should be present after cloning it
  ASSERT_FALSE(manager.findDomainElementInt64(int64Element.getValue()) == nullptr);
  
  DomainElementReal realElement(1000.123);
  DomainElementReal *createdRealElement = manager.createDomainElementReal(2000.234);
  
  // realElement should not be present before cloning it
  ASSERT_TRUE(manager.find(realElement.getDomainKey()) == nullptr);
  ASSERT_TRUE(DomainElement::isEqual(&realElement, manager.clone(&realElement)));
  ASSERT_TRUE(DomainElement::isEqual(createdRealElement, manager.clone(createdRealElement)));
}// Clone

OPTILAB_TEST(DomainElementManager, VoidElement)
{
  using namespace Core;
  
  DomainElementManager& manager = DomainElementManager::getInstance();
  manager.clear();
  
  DomainElement * voidElement = manager.getVoidElement();
  ASSERT_FALSE(voidElement == nullptr);
  ASSERT_TRUE(DomainElementVoid::isa(voidElement));
}// VoidElement

