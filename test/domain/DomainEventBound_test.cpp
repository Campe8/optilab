
#include "utest_globals.hpp"

#include "DomainEventBound.hpp"
#include "DomainEventFail.hpp"
#include "DomainEventVoid.hpp"
#include "DomainEventChange.hpp"
#include "DomainEventLwb.hpp"
#include "DomainEventUpb.hpp"
#include "DomainEventSingleton.hpp"
#include "DomainEventUndef.hpp"

OPTILAB_TEST(DomainEventBound, Basics)
{
  using namespace Core;
  
  // Check type
  std::unique_ptr<DomainEvent> event(new DomainEventBound());
  ASSERT_TRUE(event->getEventType() == DomainEventType::BOUND_EVENT);
  
  // Check isa and cast
  DomainEventBound *castEvent = DomainEventBound::cast(event.get());
  ASSERT_TRUE(DomainEventBound::isa(event.get()));
  ASSERT_TRUE(DomainEventBound::isa(castEvent));
}// Basics

OPTILAB_TEST(DomainEventBound, overlap)
{
  using namespace Core;
  std::unique_ptr<DomainEvent> eventUndef(new DomainEventUndef());
  std::unique_ptr<DomainEvent> eventBound(new DomainEventBound());
  std::unique_ptr<DomainEvent> eventFail(new DomainEventFail());
  std::unique_ptr<DomainEvent> eventVoid(new DomainEventVoid());
  std::unique_ptr<DomainEvent> eventChange(new DomainEventBound());
  std::unique_ptr<DomainEvent> eventLwb(new DomainEventLwb());
  std::unique_ptr<DomainEvent> eventUbp(new DomainEventUpb());
  std::unique_ptr<DomainEvent> eventSingleton(new DomainEventSingleton());
  
  ASSERT_TRUE(eventBound->overlaps(eventChange.get()));
  ASSERT_FALSE(eventBound->overlaps(eventLwb.get()));
  ASSERT_FALSE(eventBound->overlaps(eventUbp.get()));
  ASSERT_TRUE(eventBound->overlaps(eventBound.get()));
  ASSERT_FALSE(eventBound->overlaps(eventFail.get()));
  ASSERT_FALSE(eventBound->overlaps(eventVoid.get()));
  ASSERT_FALSE(eventBound->overlaps(eventSingleton.get()));
  ASSERT_FALSE(eventBound->overlaps(eventUndef.get()));
}// overlap
