
#include "utest_globals.hpp"

#include "DomainConcreteStorageSet.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"

OPTILAB_TEST(DomainConcreteStorageSet, Constructor)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  // Check single element
  DomainElementInt64 elem1(-20);
  DomainElementInt64 elem2(0);
  DomainElementInt64 elem3(30);
  DomainElementInt64 elem4(-2);
  DomainElementInt64 elem5(50);
  
  std::unordered_set<DomainElement*> set;
  set.insert(&elem1);
  set.insert(&elem2);
  set.insert(&elem3);
  set.insert(&elem4);
  set.insert(&elem5);
  
  DomainElementInt64 upperBound1(0);
  DomainConcreteStorageSet storage(set, &elementManager);
  ASSERT_EQ(storage.getSize(), 5);
}// Constructor

OPTILAB_TEST(DomainConcreteStorageSet, Bounds)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  // Check single element
  DomainElementInt64 elem1(-20);
  DomainElementInt64 elem2(0);
  DomainElementInt64 elem3(30);
  DomainElementInt64 elem4(-2);
  DomainElementInt64 elem5(50);
  
  std::unordered_set<DomainElement*> set;
  set.insert(&elem1);
  set.insert(&elem2);
  set.insert(&elem3);
  set.insert(&elem4);
  set.insert(&elem5);
  
  DomainElementInt64 upperBound1(0);
  DomainConcreteStorageSet storage(set, &elementManager);
  
  ASSERT_TRUE(storage.getLowerBound()->isEqual(&elem1));
  ASSERT_TRUE(storage.getUpperBound()->isEqual(&elem5));
}// Bounds

OPTILAB_TEST(DomainConcreteStorageSet, Shrink)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  // Check single element
  DomainElementInt64 elem1(-20);
  DomainElementInt64 elem2(0);
  DomainElementInt64 elem3(30);
  DomainElementInt64 elem4(-2);
  DomainElementInt64 elem5(50);
  
  std::unordered_set<DomainElement*> set;
  set.insert(&elem1);
  set.insert(&elem2);
  set.insert(&elem3);
  set.insert(&elem4);
  set.insert(&elem5);
  
  DomainConcreteStorageSet storage(set, &elementManager);
  
  DomainElementInt64 lb1(-30);
  DomainElementInt64 ub1( 40);
  storage.shrink(&lb1, &ub1);
  
  // Should delete only element 50
  ASSERT_EQ(storage.getSize(), 4);
  ASSERT_TRUE(storage.getLowerBound()->isEqual(&elem1));
  ASSERT_TRUE(storage.getUpperBound()->isEqual(&elem3));
  
  storage.shrink(&elem4, &elem3);
  ASSERT_EQ(storage.getSize(), 3);
  ASSERT_TRUE(storage.getLowerBound()->isEqual(&elem4));
  ASSERT_TRUE(storage.getUpperBound()->isEqual(&elem3));
  
  storage.shrink(&ub1, &elem5);
  ASSERT_EQ(storage.getSize(), 0);
}// Shrink

OPTILAB_TEST(DomainConcreteStorageSet, Subtract)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  // Check single element
  DomainElementInt64 elem1(-20);
  DomainElementInt64 elem2(0);
  DomainElementInt64 elem3(30);
  DomainElementInt64 elem4(-2);
  DomainElementInt64 elem5(50);
  
  std::unordered_set<DomainElement*> set;
  set.insert(&elem1);
  set.insert(&elem2);
  set.insert(&elem3);
  set.insert(&elem4);
  set.insert(&elem5);
  
  DomainElementInt64 upperBound1(0);
  DomainConcreteStorageSet storage(set, &elementManager);

  
  DomainElementInt64 domainElement(1);
  storage.subtract(&domainElement);
  
  ASSERT_EQ(storage.getSize(), 5);
  ASSERT_TRUE(storage.getLowerBound()->isEqual(&elem1));
  ASSERT_TRUE(storage.getUpperBound()->isEqual(&elem5));
  
  storage.subtract(&elem4);
  ASSERT_EQ(storage.getSize(), 4);
  ASSERT_TRUE(storage.getLowerBound()->isEqual(&elem1));
  ASSERT_TRUE(storage.getUpperBound()->isEqual(&elem5));
  
  storage.subtract(&elem1);
  ASSERT_EQ(storage.getSize(), 3);
  ASSERT_TRUE(storage.getLowerBound()->isEqual(&elem2));
  ASSERT_TRUE(storage.getUpperBound()->isEqual(&elem5));
  
  storage.subtract(&elem5);
  ASSERT_EQ(storage.getSize(), 2);
  ASSERT_TRUE(storage.getLowerBound()->isEqual(&elem2));
  ASSERT_TRUE(storage.getUpperBound()->isEqual(&elem3));
  
  storage.subtract(&elem2);
  ASSERT_EQ(storage.getSize(), 1);
  ASSERT_TRUE(storage.getLowerBound()->isEqual(&elem3));
  ASSERT_TRUE(storage.getUpperBound()->isEqual(&elem3));
  
  storage.subtract(&elem2);
  ASSERT_EQ(storage.getSize(), 1);
  ASSERT_TRUE(storage.getLowerBound()->isEqual(&elem3));
  ASSERT_TRUE(storage.getUpperBound()->isEqual(&elem3));
  
  storage.subtract(&elem3);
  storage.subtract(&elem2);
  ASSERT_EQ(storage.getSize(), 0);
}// Subtract

OPTILAB_TEST(DomainConcreteStorageSet, InMin)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  // Check single element
  DomainElementInt64 elem1(-20);
  DomainElementInt64 elem2(0);
  DomainElementInt64 elem3(30);
  DomainElementInt64 elem4(-2);
  DomainElementInt64 elem5(50);
  
  std::unordered_set<DomainElement*> set;
  set.insert(&elem1);
  set.insert(&elem2);
  set.insert(&elem3);
  set.insert(&elem4);
  set.insert(&elem5);
  
  DomainConcreteStorageSet storage(set, &elementManager);
  
  DomainElementInt64 domainElement(0);
  storage.inMin(&domainElement);
  ASSERT_EQ(storage.getSize(), 3);
  ASSERT_TRUE(storage.getLowerBound()->isEqual(&elem2));
  ASSERT_TRUE(storage.getUpperBound()->isEqual(&elem5));
  
  DomainElementInt64 domainElement1(1);
  storage.inMin(&domainElement1);
  ASSERT_EQ(storage.getSize(), 2);
  ASSERT_TRUE(storage.getLowerBound()->isEqual(&elem3));
  ASSERT_TRUE(storage.getUpperBound()->isEqual(&elem5));
  
  DomainElementInt64 domainElement2(60);
  storage.inMin(&domainElement2);
  ASSERT_EQ(storage.getSize(), 0);
}//InMin

OPTILAB_TEST(DomainConcreteStorageSet, InMax)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  // Check single element
  DomainElementInt64 elem1(-20);
  DomainElementInt64 elem2(0);
  DomainElementInt64 elem3(30);
  DomainElementInt64 elem4(-2);
  DomainElementInt64 elem5(50);
  
  std::unordered_set<DomainElement*> set;
  set.insert(&elem1);
  set.insert(&elem2);
  set.insert(&elem3);
  set.insert(&elem4);
  set.insert(&elem5);
  
  DomainConcreteStorageSet storage(set, &elementManager);
  
  DomainElementInt64 domainElement(30);
  storage.inMax(&domainElement);
  ASSERT_EQ(storage.getSize(), 4);
  ASSERT_TRUE(storage.getLowerBound()->isEqual(&elem1));
  ASSERT_TRUE(storage.getUpperBound()->isEqual(&elem3));

  DomainElementInt64 domainElement1(-1);
  storage.inMax(&domainElement1);
  ASSERT_EQ(storage.getSize(), 2);
  ASSERT_TRUE(storage.getLowerBound()->isEqual(&elem1));
  ASSERT_TRUE(storage.getUpperBound()->isEqual(&elem4));
  
  DomainElementInt64 domainElement2(-60);
  storage.inMax(&domainElement2);
  ASSERT_EQ(storage.getSize(), 0);

}//InMax

OPTILAB_TEST(DomainConcreteStorageSet, Contains)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  // Check single element
  DomainElementInt64 elem1(-20);
  DomainElementInt64 elem2(0);
  DomainElementInt64 elem3(30);
  DomainElementInt64 elem4(-2);
  DomainElementInt64 elem5(50);
  
  std::unordered_set<DomainElement*> set;
  set.insert(&elem1);
  set.insert(&elem2);
  set.insert(&elem3);
  set.insert(&elem4);
  set.insert(&elem5);
  
  DomainConcreteStorageSet storage(set, &elementManager);
  
  DomainElementInt64 domainElement(15);
  ASSERT_FALSE(storage.contains(&domainElement));
  
  for(auto element : set)
  {
    ASSERT_TRUE(storage.contains(element));
  }
}//Contains

OPTILAB_TEST(DomainConcreteStorageSet, Mix)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  // Check single element
  DomainElementInt64 elem1(-20);
  DomainElementInt64 elem2(0);
  DomainElementInt64 elem3(30);
  DomainElementInt64 elem4(-2);
  DomainElementInt64 elem5(50);
  
  std::unordered_set<DomainElement*> set;
  set.insert(&elem1);
  set.insert(&elem2);
  set.insert(&elem3);
  set.insert(&elem4);
  set.insert(&elem5);
  
  DomainConcreteStorageSet storage(set, &elementManager);
  
  storage.subtract(&elem2);
  ASSERT_EQ(storage.getSize(), 4);
  
  DomainElementInt64 elem6(60);
  storage.shrink(&elem2, &elem6);
  ASSERT_EQ(storage.getSize(), 2);
  ASSERT_TRUE(storage.getLowerBound()->isEqual(&elem3));
  ASSERT_TRUE(storage.getUpperBound()->isEqual(&elem5));
}//Mix

OPTILAB_TEST(DomainConcreteStorageSet, NextElement)
{
  // Given lowerBound <= "aDomainElement" <= upperBound, return successor of "aDomainElement".
  // If "aDomainElement" == upperBound, return upperBound.
  
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  // Check single element
  DomainElementInt64 elem1(-20);
  DomainElementInt64 elem2(-2);
  DomainElementInt64 elem3( 0);
  DomainElementInt64 elem4(30);
  DomainElementInt64 elem5(50);
  
  std::unordered_set<DomainElement*> set;
  set.insert(&elem1);
  set.insert(&elem2);
  set.insert(&elem3);
  set.insert(&elem4);
  set.insert(&elem5);
  
  DomainConcreteStorageSet storage(set, &elementManager);
  
  ASSERT_TRUE(DomainElement::isEqual(storage.getNextElement(&elem5), &elem5));
  ASSERT_TRUE(DomainElement::isEqual(storage.getNextElement(&elem1), &elem2));
  ASSERT_TRUE(DomainElement::isEqual(storage.getNextElement(&elem2), &elem3));
  ASSERT_TRUE(DomainElement::isEqual(storage.getNextElement(&elem3), &elem4));
  ASSERT_TRUE(DomainElement::isEqual(storage.getNextElement(&elem4), &elem5));
  
  DomainElementInt64 elem6(-1);
  ASSERT_TRUE(DomainElement::isEqual(storage.getNextElement(&elem6), &elem3));
  
  DomainElementInt64 elem7(45);
  ASSERT_TRUE(DomainElement::isEqual(storage.getNextElement(&elem7), &elem5));
}//NextElement

OPTILAB_TEST(DomainConcreteStorageSet, PrevElement)
{
  // Given lowerBound <= "aDomainElement" <= upperBound, return predecessor of "aDomainElement".
  // If "aDomainElement" == upperBound, return upperBound.
  
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  // Check single element
  DomainElementInt64 elem1(-20);
  DomainElementInt64 elem2(-2);
  DomainElementInt64 elem3( 0);
  DomainElementInt64 elem4(30);
  DomainElementInt64 elem5(50);
  
  std::unordered_set<DomainElement*> set;
  set.insert(&elem1);
  set.insert(&elem2);
  set.insert(&elem3);
  set.insert(&elem4);
  set.insert(&elem5);
  
  DomainConcreteStorageSet storage(set, &elementManager);
  
  ASSERT_TRUE(DomainElement::isEqual(storage.getPrevElement(&elem1), &elem1));
  ASSERT_TRUE(DomainElement::isEqual(storage.getPrevElement(&elem5), &elem4));
  ASSERT_TRUE(DomainElement::isEqual(storage.getPrevElement(&elem4), &elem3));
  ASSERT_TRUE(DomainElement::isEqual(storage.getPrevElement(&elem3), &elem2));
  ASSERT_TRUE(DomainElement::isEqual(storage.getPrevElement(&elem2), &elem1));
  
  DomainElementInt64 elem6(-1);
  ASSERT_TRUE(DomainElement::isEqual(storage.getPrevElement(&elem6), &elem2));
  
  DomainElementInt64 elem7(45);
  ASSERT_TRUE(DomainElement::isEqual(storage.getPrevElement(&elem7), &elem4));
}//PrevElement

OPTILAB_TEST(DomainConcreteStorageSet, ElementByIndex)
{
  // Returns the "aElementIndex" - 1 ^th DomainElement, where
  // 0 <= "aElementIndex" < domain size.
  
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  // Check single element
  DomainElementInt64 elem1( 0);
  DomainElementInt64 elem2(30);
  DomainElementInt64 elem3(50);
  
  std::unordered_set<DomainElement*> set;
  set.insert(&elem1);
  set.insert(&elem2);
  set.insert(&elem3);
  
  DomainConcreteStorageSet storage(set, &elementManager);
  
  ASSERT_TRUE(DomainElement::isEqual(storage.getElementByIndex(0), &elem1) ||
              DomainElement::isEqual(storage.getElementByIndex(0), &elem2) ||
              DomainElement::isEqual(storage.getElementByIndex(0), &elem3));
  
  ASSERT_TRUE(DomainElement::isEqual(storage.getElementByIndex(1), &elem1) ||
              DomainElement::isEqual(storage.getElementByIndex(1), &elem2) ||
              DomainElement::isEqual(storage.getElementByIndex(1), &elem3));
  
  ASSERT_TRUE(DomainElement::isEqual(storage.getElementByIndex(2), &elem1) ||
              DomainElement::isEqual(storage.getElementByIndex(2), &elem2) ||
              DomainElement::isEqual(storage.getElementByIndex(2), &elem3));
}//ElementByIndex
