
#include "utest_globals.hpp"

#include "DomainConcreteStorageBounds.hpp"
#include "DomainConcreteStorageBitmap.hpp"
#include "DomainConcreteStorageSet.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"
#include "DomainStatePairContainer.hpp"

OPTILAB_TEST(DomainStatePairContainer, Basics)
{
  using namespace Core;
  
  DomainStatePairContainer dc;
  DomainState* ds = &dc;
  ASSERT_TRUE(DomainStatePairContainer::isa(ds));
  DomainStatePairContainer* dsp = DomainStatePairContainer::cast(ds);
  ASSERT_TRUE(dsp);
  
  ASSERT_FALSE(dc.validState());
  ASSERT_FALSE(dc.releaseState().first);
  ASSERT_FALSE(dc.releaseState().second);
}//Basics

OPTILAB_TEST(DomainStatePairContainer, setInternalStateBounds)
{
  using namespace Core;
  
  DomainStatePairContainer dc;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();

  // Check standard domain
  DomainElementInt64 lowerBound(0);
  DomainElementInt64 upperBound(10);
  auto pair = std::make_pair(&lowerBound, &upperBound);
  
  dc.setInternalState(pair);
  ASSERT_TRUE(dc.validState());
  
  // Modify original storage
  pair.first = nullptr;
  pair.second = nullptr;
  
  auto pair2 = dc.getInternalStateCopy();
  ASSERT_TRUE(dc.validState());
  
  ASSERT_TRUE(pair2.first);
  ASSERT_TRUE(pair2.second);
  
  ASSERT_TRUE(pair2.first->isEqual(&lowerBound));
  ASSERT_TRUE(pair2.second->isEqual(&upperBound));
  
  auto pair3 = dc.releaseState();
  ASSERT_TRUE(pair3.first);
  ASSERT_TRUE(pair3.second);
  
  ASSERT_TRUE(pair2.first->isEqual(pair3.first));
  ASSERT_TRUE(pair2.second->isEqual(pair3.second));
  ASSERT_FALSE(dc.validState());
}//setInternalStateBounds

OPTILAB_TEST(DomainStatePairContainer, setInternalStateBoundsDeleteOldDomainElements)
{
  using namespace Core;
  
  DomainStatePairContainer dc;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  // Check standard domain
  DomainElementInt64* lowerBound = new DomainElementInt64(0);
  DomainElementInt64* upperBound = new DomainElementInt64(10);
  auto pair = std::make_pair(lowerBound, upperBound);
  
  dc.setInternalState(pair);
  ASSERT_TRUE(dc.validState());
  
  delete lowerBound;
  delete upperBound;
  
  lowerBound = nullptr;
  upperBound = nullptr;
  
  auto pair2 = dc.getInternalStateCopy();
  ASSERT_TRUE(dc.validState());
  
  ASSERT_TRUE(pair2.first);
  ASSERT_TRUE(pair2.second);
  
  ASSERT_TRUE(pair2.first->isEqual(elementManager.createDomainElementInt64(0)));
  ASSERT_TRUE(pair2.second->isEqual(elementManager.createDomainElementInt64(10)));
}//setInternalStateBoundsDeleteOldDomainElements
