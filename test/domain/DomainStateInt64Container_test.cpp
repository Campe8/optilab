
#include "utest_globals.hpp"

#include "DomainConcreteStorageBounds.hpp"
#include "DomainConcreteStorageBitmap.hpp"
#include "DomainConcreteStorageSet.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"
#include "DomainStateInt64Container.hpp"

OPTILAB_TEST(DomainStateInt64Container, Basics)
{
  using namespace Core;
  
  DomainStateInt64Container dc;
  ASSERT_TRUE(DomainStateInt64Container::isa(&dc));
  
  DomainState* ds = &dc;
  ASSERT_TRUE(DomainStateInt64Container::isa(ds));
  DomainStateInt64Container* dsp = DomainStateInt64Container::cast(ds);
  ASSERT_TRUE(dsp);
  
  ASSERT_FALSE(dc.validState());
  ASSERT_FALSE(dc.releaseState());
}//Basics

OPTILAB_TEST(DomainStateInt64Container, setInternalStateBounds)
{
  using namespace Core;
  
  DomainStateInt64Container dc;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();

  // Check standard domain
  DomainElementInt64 lowerBound(0);
  DomainElementInt64 upperBound(10);
  DomainConcreteStorageBounds storage(&lowerBound, &upperBound, &elementManager);
  ASSERT_EQ(storage.getSize(), 11);
  
  dc.setInternalState(&storage);
  ASSERT_TRUE(dc.validState());
  
  // Modify original storage
  storage.inMax(elementManager.createDomainElementInt64(5));
  ASSERT_EQ(storage.getSize(), 6);
  
  std::unique_ptr<DomainConcreteStorage> storage2 = dc.getInternalStateCopy();
  ASSERT_TRUE(storage2);
  ASSERT_TRUE(dc.validState());
  
  std::unique_ptr<DomainConcreteStorage> storage3(dc.releaseState());
  ASSERT_FALSE(dc.validState());
  
  // Storage fro container should be a copy of the orginal storage
  ASSERT_EQ(storage2->getSize(), 11);
  ASSERT_TRUE(storage2->getLowerBound()->isEqual(&lowerBound));
  ASSERT_TRUE(storage2->getUpperBound()->isEqual(&upperBound));
  
  storage2->subtract(elementManager.createDomainElementInt64(0));
  ASSERT_FALSE(storage2->contains(elementManager.createDomainElementInt64(0)));
  ASSERT_TRUE(storage.contains(elementManager.createDomainElementInt64(0)));
}//setInternalStateBounds

OPTILAB_TEST(DomainStateInt64Container, setInternalStateBitmap)
{
  using namespace Core;
  
  DomainStateInt64Container dc;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  // Check standard domain
  DomainElementInt64 lowerBound(0);
  DomainElementInt64 upperBound(10);
  DomainConcreteStorageBitmap storage(&lowerBound, &upperBound, &elementManager);
  ASSERT_EQ(storage.getSize(), 11);
  
  dc.setInternalState(&storage);
  ASSERT_TRUE(dc.validState());
  
  // Modify original storage
  storage.inMax(elementManager.createDomainElementInt64(5));
  storage.subtract(elementManager.createDomainElementInt64(3));
  ASSERT_FALSE(storage.contains(elementManager.createDomainElementInt64(3)));
  ASSERT_EQ(storage.getSize(), 5);
  
  std::unique_ptr<DomainConcreteStorage> storage2 = dc.getInternalStateCopy();
  ASSERT_TRUE(storage2);
  ASSERT_TRUE(dc.validState());
  
  // Storage fro container should be a copy of the orginal storage
  ASSERT_EQ(storage2->getSize(), 11);
  ASSERT_TRUE(storage2->getLowerBound()->isEqual(&lowerBound));
  ASSERT_TRUE(storage2->getUpperBound()->isEqual(&upperBound));
  ASSERT_TRUE(storage2->contains(elementManager.createDomainElementInt64(3)));
  
  storage2->subtract(elementManager.createDomainElementInt64(0));
  ASSERT_FALSE(storage2->contains(elementManager.createDomainElementInt64(0)));
  ASSERT_TRUE(storage.contains(elementManager.createDomainElementInt64(0)));
}//setInternalStateBitmap

OPTILAB_TEST(DomainStateInt64Container, setInternalStateSet)
{
  using namespace Core;
  
  DomainStateInt64Container dc;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  elementManager.clear();
  
  // Check standard domain
  // Check single element
  DomainElementInt64 elem1(-20);
  DomainElementInt64 elem2(-2);
  DomainElementInt64 elem3( 0);
  DomainElementInt64 elem4(30);
  DomainElementInt64 elem5(50);
  
  std::unordered_set<DomainElement*> set;
  set.insert(&elem1);
  set.insert(&elem2);
  set.insert(&elem3);
  set.insert(&elem4);
  set.insert(&elem5);
  
  DomainConcreteStorageSet storage(set, &elementManager);
  ASSERT_EQ(storage.getSize(), 5);
  
  dc.setInternalState(&storage);
  ASSERT_TRUE(dc.validState());
  
  // Modify original storage
  storage.subtract(elementManager.createDomainElementInt64(0));
  ASSERT_FALSE(storage.contains(elementManager.createDomainElementInt64(0)));
  ASSERT_EQ(storage.getSize(), 4);
  
  std::unique_ptr<DomainConcreteStorage> storage2 = dc.getInternalStateCopy();
  ASSERT_TRUE(storage2);
  ASSERT_TRUE(dc.validState());
  
  // Storage fro container should be a copy of the orginal storage
  ASSERT_EQ(storage2->getSize(), 5);
  ASSERT_TRUE(storage2->getLowerBound()->isEqual(&elem1));
  ASSERT_TRUE(storage2->getUpperBound()->isEqual(&elem5));
  ASSERT_TRUE(storage2->contains(elementManager.createDomainElementInt64(0)));
  
  storage2->subtract(elementManager.createDomainElementInt64(-20));
  ASSERT_FALSE(storage2->contains(elementManager.createDomainElementInt64(-20)));
  ASSERT_TRUE(storage.contains(elementManager.createDomainElementInt64(-20)));
}//setInternalStateSet
