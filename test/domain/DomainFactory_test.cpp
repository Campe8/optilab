#include "utest_globals.hpp"

#include "DomainFactory.hpp"

#include "DomainElementInt64.hpp"

OPTILAB_TEST(DomainFactory, GetSharedFromRawDomainPointer)
{
  using namespace Core;
  
  DomainFactoryUPtr domFactory(new DomainFactory());
  
  DomainPtr domBool(domFactory->domainBoolean());
  DomainSPtr ptr(domFactory->getSharedFromRawDomainPointer(domBool));
  ASSERT_EQ(ptr.get(), domBool);
}

OPTILAB_TEST(DomainFactory, Domain)
{
  using namespace Core;
  
  DomainFactoryUPtr domFactory(new DomainFactory());
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  
  DomainSPtr dom(domFactory->domain(elementManager.createDomainElementInt64(12), DomainClass::DOM_INT64));
  ASSERT_TRUE(DomainInteger::isa(dom.get()));
  ASSERT_TRUE(DomainInteger::cast(dom.get())->lowerBound() == elementManager.createDomainElementInt64(12));
  ASSERT_TRUE(DomainInteger::cast(dom.get())->upperBound() == elementManager.createDomainElementInt64(12));
  
  DomainSPtr dom1(domFactory->domain(elementManager.createDomainElementInt64(-12), elementManager.createDomainElementInt64(3), DomainClass::DOM_INT64));
  ASSERT_TRUE(DomainInteger::isa(dom1.get()));
  ASSERT_TRUE(DomainInteger::cast(dom1.get())->lowerBound() == elementManager.createDomainElementInt64(-12));
  ASSERT_TRUE(DomainInteger::cast(dom1.get())->upperBound() == elementManager.createDomainElementInt64(3));
}

OPTILAB_TEST(DomainFactory, DomainInteger)
{
  using namespace Core;
  
  DomainFactoryUPtr domFactory(new DomainFactory());
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  
  DomainSPtr domInt(domFactory->domainInteger(10));
  ASSERT_TRUE(DomainInteger::isa(domInt.get()));
  ASSERT_TRUE(DomainInteger::cast(domInt.get())->lowerBound() == elementManager.createDomainElementInt64(10));
  ASSERT_TRUE(DomainInteger::cast(domInt.get())->upperBound() == elementManager.createDomainElementInt64(10));
  
  DomainSPtr domInt1(domFactory->domainInteger(10, 20));
  ASSERT_TRUE(DomainInteger::isa(domInt1.get()));
  ASSERT_TRUE(DomainInteger::cast(domInt1.get())->lowerBound() == elementManager.createDomainElementInt64(10));
  ASSERT_TRUE(DomainInteger::cast(domInt1.get())->upperBound() == elementManager.createDomainElementInt64(20));
  
  std::unordered_set<INT_64> elements = {-2,0,1,2,10};
  DomainSPtr domInt2(domFactory->domainInteger(elements));
  ASSERT_TRUE(DomainInteger::isa(domInt2.get()));
  ASSERT_TRUE(DomainInteger::cast(domInt2.get())->lowerBound() == elementManager.createDomainElementInt64(-2));
  ASSERT_TRUE(DomainInteger::cast(domInt2.get())->upperBound() == elementManager.createDomainElementInt64(10));
  for (const auto& elem: elements)
  {
    ASSERT_TRUE(DomainInteger::cast(domInt2.get())->contains(elementManager.createDomainElementInt64(elem)));
  }
  
  DomainSPtr domInt3(domFactory->domainInteger(elementManager.createDomainElementInt64(-12), elementManager.createDomainElementInt64(3)));
  ASSERT_TRUE(DomainInteger::isa(domInt3.get()));
  ASSERT_TRUE(DomainInteger::cast(domInt3.get())->lowerBound() == elementManager.createDomainElementInt64(-12));
  ASSERT_TRUE(DomainInteger::cast(domInt3.get())->upperBound() == elementManager.createDomainElementInt64(3));
  
  std::unordered_set<DomainElementInt64 *> elements2;
  elements2.insert(elementManager.createDomainElementInt64(10));
  elements2.insert(elementManager.createDomainElementInt64(20));
  elements2.insert(elementManager.createDomainElementInt64(25));
  DomainSPtr domInt4(domFactory->domainInteger(elements2));
  ASSERT_TRUE(DomainInteger::isa(domInt4.get()));
  ASSERT_TRUE(DomainInteger::cast(domInt4.get())->lowerBound() == elementManager.createDomainElementInt64(10));
  ASSERT_TRUE(DomainInteger::cast(domInt4.get())->upperBound() == elementManager.createDomainElementInt64(25));
  for (const auto& elem: elements2)
  {
    ASSERT_TRUE(DomainInteger::cast(domInt4.get())->contains(elem));
  }
  
}

OPTILAB_TEST(DomainFactory, DomainBool)
{
  using namespace Core;
  
  DomainElementManager& elementManager = DomainElementManager::getInstance();
  
  DomainFactoryUPtr domFactory(new DomainFactory());
  
  DomainSPtr domBool(domFactory->domainBoolean());
  ASSERT_TRUE(DomainBoolean::isa(domBool.get()));
  
  DomainSPtr domBool1(domFactory->domainBoolean(true));
  ASSERT_TRUE(DomainBoolean::isa(domBool1.get()));
  ASSERT_TRUE(DomainBoolean::cast(domBool1.get())->contains(elementManager.createDomainElementInt64(1)));
  ASSERT_FALSE(DomainBoolean::cast(domBool1.get())->contains(elementManager.createDomainElementInt64(0)));
}
