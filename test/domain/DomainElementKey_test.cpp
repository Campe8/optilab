
#include "utest_globals.hpp"

#include "DomainElementKey.hpp"

#include <unordered_map>


OPTILAB_TEST(DomainElementKey, Constructor)
{
  using namespace Core;
  
  DomainElementKey key(static_cast<INT_64>(0), DomainElementType::DET_INT_64);
  
  // Copy constructor
  DomainElementKey keyCopy(key);
  ASSERT_TRUE(key == keyCopy);
  
  // Assignment operator
  DomainElementKey keyAssigned(static_cast<INT_64>(100), DomainElementType::DET_INT_64);
  
  keyAssigned = keyCopy;
  ASSERT_TRUE(key == keyAssigned);
  
  DomainElementKey keyReal(1.234, DomainElementType::DET_REAL);
  DomainElementKey keyRealCopy(keyReal);
  ASSERT_TRUE(keyReal == keyRealCopy);
  
  DomainElementKey keyRealAux(10.245, DomainElementType::DET_REAL);
  
  keyRealCopy = keyRealAux;
  ASSERT_TRUE(keyRealAux == keyRealCopy);
}//Constructor

OPTILAB_TEST(DomainElementKey, DomainElementKeyHasher)
{
  using namespace Core;
  
  DomainElementKey key(static_cast<INT_64>(0), DomainElementType::DET_INT_64);
  DomainElementKey keyAssigned(static_cast<INT_64>(0), DomainElementType::DET_INT_64);
  DomainElementKeyHasher keyHash;
  std::size_t val1 = keyHash(key);
  
  DomainElementKey pInfKey(std::numeric_limits<INT_64>::max(), DomainElementType::DET_INT_64);
  DomainElementKey nInfKey(std::numeric_limits<INT_64>::lowest(), DomainElementType::DET_INT_64);
  
  std::size_t valP = keyHash(pInfKey);
  std::size_t valN = keyHash(nInfKey);
  
  ASSERT_TRUE(valP != valN);
  ASSERT_TRUE(valP != val1);
  ASSERT_TRUE(valN != val1);
  
  DomainElementKey key2(static_cast<INT_64>(3), DomainElementType::DET_INT_64);
  DomainElementKey key3(static_cast<INT_64>(3), DomainElementType::DET_INT_64);
  
  std::unordered_map<DomainElementKey, std::size_t, DomainElementKeyHasher> myMap;
  
  ASSERT_TRUE(myMap.find(key) == myMap.end());
  myMap[key] = 100;
  
  ASSERT_TRUE(myMap.find(key2) == myMap.end());
  myMap[key2] = 100;
  
  ASSERT_TRUE(myMap.find(key) != myMap.end());
  ASSERT_TRUE(myMap.find(key3) != myMap.end());
}//DomainElementKeyHasher

