# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)
# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)
# Find the QtWidgets library
find_package(Qt5Widgets REQUIRED)

#generating correct tree of cpp files
set(gui_blacklist "")
set(gui_blackmasks "")
set(gui_src "")
set(gui_headers "")
set(gui_ui "")

set(gui_moc_sources "")
set(gui_ui_sources "")

set(CMAKE_AUTOUIC_SEARCH_PATHS ${CMAKE_AUTOUIC_SEARCH_PATHS} "${CMAKE_CURRENT_SOURCE_DIR}/gui/ui" )

do_file_list(gui_src "${CMAKE_CURRENT_SOURCE_DIR}/gui/src" ".cpp" "${domain_blackmasks}" "${domain_blacklist}")
#message( "${gui_src}" )

do_file_list(gui_headers "${CMAKE_CURRENT_SOURCE_DIR}/gui/include" ".h" "${domain_blackmasks}" "${domain_blacklist}")

do_file_list(gui_ui "${CMAKE_CURRENT_SOURCE_DIR}/gui/ui" ".ui" "${domain_blackmasks}" "${domain_blacklist}")

qt5_wrap_cpp(gui_moc_sources ${gui_headers})
qt5_wrap_ui(gui_ui_sources ${gui_ui})

# Tell CMake to create the optilab_gui executable
#add_executable(optilab_gui WIN32 main.cpp)
add_executable(optilab_gui ${CMAKE_CURRENT_SOURCE_DIR}/gui/main.cpp ${gui_src} ${gui_moc_sources} ${gui_ui_sources} )
qt5_use_modules(optilab_gui Widgets)
target_compile_options(optilab_gui PRIVATE -fpermissive)

target_include_directories(optilab_gui PUBLIC
    "${CMAKE_CURRENT_SOURCE_DIR}/gui/include"
)

# Use the Widgets module from Qt 5.
target_link_libraries(optilab_gui Qt5::Widgets
                      domain
                      opt_parser
                       )
