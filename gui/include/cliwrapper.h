#ifndef CLIWRAPPER_H
#define CLIWRAPPER_H

#include <iostream>
#include <sstream>

#include <QObject>
#include <QString>

#include "MVCModel.hpp"
#include "MVCViewCLI.hpp"
#include "MVCControllerIO.hpp"

class CliWrapper : public QObject
{
    Q_OBJECT
  public:
    explicit CliWrapper(QObject *parent = nullptr);

    /**
     * Loads optilab engine. Actually instantiates optilab MVC
     * objects as class parameters
     */
    void loadEngine();

  private:
    // input / output streams
    std::ostringstream pOut;
    std::istringstream pIn;

    // optilab mvc internals
    std::shared_ptr<MVC::MVCViewCLI> pView;
    std::shared_ptr<MVC::MVCModel> pModel;
    std::shared_ptr<MVC::MVCControllerIO> pController;

  signals:
    /**
     * Signal is emitted when output is ready to be processed by
     * gui widget
     */ 
    void outputReady(const QString &str);

  public slots:
    /**
     * Set input to be read by optilab engine
     */
    void setInput(QString input);

    /**
     * Optilab engine reads data from pIn stream,
     * evaluate it and writes output into pOut stream
     */
    void evaluateInput();

    /**
     * Handles output produces by engine, sending it 
     * to gui
     */
    void writeOutput();
};

#endif // CLIWRAPPER_H
