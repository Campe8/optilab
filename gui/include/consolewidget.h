#ifndef CONSOLEWIDGET_H
#define CONSOLEWIDGET_H

#include <QMainWindow>
#include <QPlainTextEdit>

#include "cliwrapper.h"

namespace Ui {
class ConsoleWidget;
}

class ConsoleWidget : public QPlainTextEdit
{
  Q_OBJECT

  public:
    explicit ConsoleWidget(QWidget *parent = 0);
    ConsoleWidget& operator<< (std::string const &str);
    ~ConsoleWidget();

  private:
    CliWrapper * wrapper;
  
  public slots:
    void appendOutput(QString str);
};

#endif // CONSOLEWIDGET_H
