#include "cliwrapper.h"

#include "MVCModel.hpp"
#include "MVCViewCLI.hpp"
#include "MVCControllerIO.hpp"

CliWrapper::CliWrapper(QObject *parent) : QObject(parent)
{
  loadEngine();
}

void CliWrapper::loadEngine()
{
  // Create a view for a sigle input file
  // Setting custom streams to mvcview
  pView = std::make_shared<MVC::MVCViewCLI>(false);

  pView->setInStream(&pIn);
  pView->setOutStream(&pOut);
  // Generate the Model
  pModel = std::make_shared<MVC::MVCModel>();
  // Generate the Controller
  pController = std::make_shared<MVC::MVCControllerIO>(pView, pModel);
  
  // View observes the controller
  pView->observe(MVC::MVCController::getSubject(pController.get()));
  
  // View observes the model
  pView->observe(MVC::MVCModel::getSubject(pModel.get()));

  pView->initializeView();

  //prints optilab header
  //writeOutput();
}

void CliWrapper::setInput(QString input)
{
  if(input.isEmpty())
  {
    pIn.str(input.toStdString());
  }
}

void CliWrapper::evaluateInput()
{
  pController->step();
}

void CliWrapper::writeOutput()
{
  QString output = QString::fromStdString(pOut.str());
  emit outputReady(output);
}