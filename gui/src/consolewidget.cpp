#include "consolewidget.h"

#include <QObject>
#include <QScrollBar>
#include <QDataStream>
#include <QTextCursor>

#include <QtCore/QDebug>

ConsoleWidget::ConsoleWidget(QWidget *parent)
  : QPlainTextEdit(parent)
{
  document()->setMaximumBlockCount(100);
  QPalette p = palette();
  p.setColor(QPalette::Base, Qt::black);
  p.setColor(QPalette::Text, Qt::green);
  setPalette(p);

  //initializing cli wrapper
  auto wrapper = std::make_shared<CliWrapper>();

  //connecting wrapper to widget
  connect(
    wrapper.get(), &CliWrapper::outputReady,
    this, &ConsoleWidget::appendOutput
  );

  wrapper->writeOutput();
}

ConsoleWidget& operator<< (ConsoleWidget * w, std::string const &str) {
  w->appendPlainText(QString::fromStdString(str));
}

void ConsoleWidget::appendOutput(QString str)
{
  appendPlainText(str);
}

ConsoleWidget::~ConsoleWidget()
{
    
}
