// Self first
#include "FunctionGetUnusedCtxVarName.hpp"

#include "FunctionMacro.hpp"
#include "MVCUtils.hpp"
#include "DataObject.hpp"

namespace MVC {
  
  FunctionGetUnusedCtxVarName::FunctionGetUnusedCtxVarName()
  : Function(FunctionId::FCN_GET_UNUSED_CTX_VAR_NAME)
  {
  }
  
  Interpreter::DataObject
  FunctionGetUnusedCtxVarName::callback(const std::vector<Interpreter::DataObject>& aArgs)
  {
    assertAndNotify(aArgs.empty(), ERR_MSG_NUM_ARGS);
    
    // Invoke the context function
    return getName();
  }//callback
  
  Interpreter::DataObject FunctionGetUnusedCtxVarName::getName()
  {
    auto name = (getModel()->getModelContext()).getUnusedNameForContextObject();
    Interpreter::DataObject objName(name.c_str());
    
    return objName;
  }//getName
  
  bool FunctionGetUnusedCtxVarName::hasOutput() const
  {
    return true;
  }//hasOutput
  
  std::string FunctionGetUnusedCtxVarName::name() const
  {
    return "getUnusedCtxVarName";
  }//name
  
}//end namespace MVC
