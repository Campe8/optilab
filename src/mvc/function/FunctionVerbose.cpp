// Base class
#include "FunctionVerbose.hpp"
#include "FunctionMacro.hpp"

#include "CLIUtils.hpp"

#include <algorithm>

namespace MVC {
  
  FunctionVerbose::FunctionVerbose()
  : Function(FunctionId::FCN_VERBOSE)
  {
  }
  
  Interpreter::DataObject FunctionVerbose::callback(const std::vector<Interpreter::DataObject>& aArgs)
  {
    assertAndNotify(aArgs.size() == 1, ERR_MSG_NUM_ARGS);
    assertAndNotify(aArgs[0].dataObjectType() == Interpreter::DataObject::DataObjectType::DOT_INT, ERR_MSG_INPUT_ARG_TYPE);
    
    auto verbLevel = aArgs[0].getDataValue<int>();
    verbLevel = std::max<int>(0, verbLevel);
    verbLevel = std::min<int>(2, verbLevel);
    
    verbose(static_cast<int>(verbLevel));
    
    return Interpreter::DataObject();
  }//callback
  
  void FunctionVerbose::verbose(int aVerbLevel)
  {
    assert(getModel());
    
    // Set verbose information
    MVCModelContext::PrettyPrintLevel ppLevel = MVCModelContext::PrettyPrintLevel::PP_LOW;
    if (aVerbLevel <= static_cast<int>(MVCModelContext::PrettyPrintLevel::PP_NONE))
    {
      ppLevel = MVCModelContext::PrettyPrintLevel::PP_NONE;
    }
    else if (aVerbLevel >= static_cast<int>(MVCModelContext::PrettyPrintLevel::PP_HIGH))
    {
      ppLevel = MVCModelContext::PrettyPrintLevel::PP_HIGH;
    }
    
    // Set model context's pretty print level
    (getModel()->getModelContext()).setPrettyPrintLevel(ppLevel);
  }//verbose
  
  std::string FunctionVerbose::name() const
  {
    return "verbose";
  }//name
  
}//end namespace MVC
