// Self first
#include "FunctionSolve.hpp"

#include "FunctionMacro.hpp"
#include "MVCMacro.hpp"
#include "ModelMacro.hpp"
#include "CLIUtils.hpp"
#include "CLIMacroSearch.hpp"
#include "DataObject.hpp"
#include "BaseObject.hpp"
#include "ObjectToolsAux.hpp"
#include "MetricsManager.hpp"

#include <cassert>

namespace MVC {
  
  static void setMetricsOptions(Model::ModelMetaInfo* aInfo, Interpreter::BaseObject* aMetricsOpts)
  {
    assert(aInfo);
    if (!aMetricsOpts) return;
    
    // Enable metrics reporting
    aInfo->enableMetrics();
    
    if (aMetricsOpts->hasProperty(Interpreter::ObjectTools::getMetricsTimeOptionsName()))
    {
      const auto& prop =
      aMetricsOpts->getProperty(Interpreter::ObjectTools::getMetricsTimeOptionsName());
      if ((prop.dataObjectType() == Interpreter::DataObject::DataObjectType::DOT_BOOL) &&
          prop.getDataValue<bool>())
      {
        aInfo->addMetricsCategory(Base::Tools::MetricsManager::MetricsCategory::TIME_METRICS);
      }
    }
    
    if (aMetricsOpts->hasProperty(Interpreter::ObjectTools::getMetricsSearchOptionsName()))
    {
      const auto& prop =
      aMetricsOpts->getProperty(Interpreter::ObjectTools::getMetricsSearchOptionsName());
      if ((prop.dataObjectType() == Interpreter::DataObject::DataObjectType::DOT_BOOL) &&
          prop.getDataValue<bool>())
      {
        aInfo->addMetricsCategory(Base::Tools::MetricsManager::MetricsCategory::SEARCH_METRICS);
      }
    }
  }//setMetricsOptions
  
  FunctionSolve::FunctionSolve()
  : Function(FunctionId::FCN_SOLVE)
  {
  }
  
  Interpreter::DataObject FunctionSolve::callback(const std::vector<Interpreter::DataObject>& aArgs)
  {
    // There must be [0, 2] arguments
    assertAndNotify(aArgs.size() < 3, ERR_MSG_NUM_ARGS);
    if (aArgs.size() == 0)
    {
      // With no arguments run the engine to find 1 solutions
      solve(1, nullptr);
      return Interpreter::DataObject();
    }
    
    // The first argument must be the number of solutions
    assertAndNotify(aArgs[0].dataObjectType() ==
                    Interpreter::DataObject::DataObjectType::DOT_INT, ERR_MSG_INPUT_ARG_TYPE);
    
    // If there are other arguments, those must be class types
    for (std::size_t idx{1}; idx < aArgs.size(); ++idx)
    {
      assertAndNotify(Interpreter::isClassObjectType(aArgs[idx]),
                      ERR_MSG_INPUT_ARG_TYPE);
    }
    
    // Only class object supported is metric options
    Interpreter::BaseObject* metricsOpts{nullptr};
    if (aArgs.size() > 1)
    {
      metricsOpts = aArgs[1].getClassObject().get();
    }
    solve(static_cast<long long>(aArgs[0].getDataValue<int>()), metricsOpts);
    
    return Interpreter::DataObject();
  }//callback
  
  void FunctionSolve::solve(long long aNumSol, Interpreter::BaseObject* aMetricsOpts)
  {
    auto mvcModel = getModel();
    assert(mvcModel);
    
    // Generate the model to be solved
    Model::ModelSPtr model{nullptr};
    try
    {
      model = mvcModel->generateModel();
    }
    catch(Model::ModelException& e)
    {
      notifyController(e.what(), Function::NotificationLevel::LEVEL_ERROR);
      return;
    }
    catch (MVC::MVCException& e)
    {
      notifyController(e.what(), Function::NotificationLevel::LEVEL_ERROR);
      return;
    }
    catch(std::logic_error& e)
    {
      std::string err(e.what());
      notifyController(err, Function::NotificationLevel::LEVEL_ERROR);
      return;
    }
    catch(...)
    {
      notifyController("unable to generate the model", Function::NotificationLevel::LEVEL_ERROR);
      return;
    }
    
    // Update meta information
    model->getMetaInfo()->setSolutionLimit(aNumSol);
    
    // Get information about metrics
    setMetricsOptions(model->getMetaInfo().get(), aMetricsOpts);
    
    // Solve the model by calling the back engine on it
    try
    {
      mvcModel->solveModel(model);
    }
    catch (MVC::MVCException& e)
    {
      notifyController(e.what(), Function::NotificationLevel::LEVEL_ERROR);
    }
    catch(...)
    {
      notifyController("unable to run the model", Function::NotificationLevel::LEVEL_ERROR);
    }
  }//solve
  
  std::string FunctionSolve::name() const
  {
    return "solve";
  }//name
  
}//end namespace MVC
