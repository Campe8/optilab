// Self first
#include "FunctionObject.hpp"

#include "ObjectTools.hpp"
#include "FunctionMacro.hpp"
#include "MVCUtils.hpp"
#include "DataObject.hpp"

namespace otools = Interpreter::ObjectTools;

namespace MVC {
  
  FunctionObject::FunctionObject()
  : Function(FunctionId::FCN_OBJECT)
  {
  }
  
  Interpreter::DataObject FunctionObject::callback(const std::vector<Interpreter::DataObject>& aArgs)
  {
    assertAndNotify(aArgs.size() <= 1, ERR_MSG_NUM_ARGS);
    if (aArgs.size() == 1)
    {
      assertAndNotify(Interpreter::isPrimitiveType(aArgs[0]), ERR_MSG_INPUT_ARG_TYPE);
      
      Interpreter::DataObject scalarObj = aArgs[0];
      scalarObj.setClassObject(Interpreter::BaseObjectSPtr(otools::createScalarObject(aArgs[0])));
      return scalarObj;
    }
    
    Interpreter::DataObject obj;
    obj.setClassObject(Interpreter::BaseObjectSPtr(otools::createObject()));
    
    // Invoke the context function
    return obj;
  }//callback
  
  bool FunctionObject::hasOutput() const
  {
    return true;
  }//hasOutput
  
  std::string FunctionObject::name() const
  {
    return "Object";
  }//name
  
}//end namespace MVC
