// Self first
#include "FunctionBranch.hpp"
#include "FunctionMacro.hpp"
#include "MVCUtils.hpp"
#include "MVCMacro.hpp"
#include "ObjectHelper.hpp"
#include "SearchOptionsTools.hpp"
#include "CLIMacroSearch.hpp"

namespace otools = Interpreter::ObjectTools;
namespace sotools = Interpreter::SearchOptionsTools;

namespace {
  
  /// Returns true if the given DataObject represents an array of identifiers.
  /// Returns false otherwise
  bool isArrayOfIdentifiers(const Interpreter::DataObject& aDO)
  {
    if (Interpreter::isClassObjectType(aDO))
    {
      auto obj = aDO.getClassObject().get();
      if (otools::isVariableObject(obj)) return true;
      else if (otools::isListObject(obj))
      {
        for (const auto& listObj : otools::getListValues(obj))
        {
          if (!isArrayOfIdentifiers(listObj)) return false;
        }
        return true;
      }
    }
    else if (Interpreter::isAnyObjectType(aDO))
    {
      return true;
    }
    
    return false;
  }//isArrayOfIdentifiers
  
}  // namespace

namespace MVC {
  
  FunctionBranch::FunctionBranch()
  : Function(FunctionId::FCN_BRANCH)
  {
  }
  
  void FunctionBranch::checkFunctionArguments(const Interpreter::DataObject& aDO)
  {
    // Branch function should have the following arguments:
    // aArgs[0][0]: list of variables to search on
    // aArgs[0][1]: BranchOptions object
    // @note aArgs[0][1] is an optional argument
    
    // Check first input to be a list of identifiers for variables to branch on
    assertAndNotify(aDO.isComposite() && aDO.composeSize() < 3, ERR_MSG_NUM_ARGS);
    assertAndNotify(isArrayOfIdentifiers(aDO[0]), ERR_MSG_INPUT_ARG);
    
    // Returns if there are no arguments besides the list of identifiers
    if (aDO.composeSize() == 1) return;

    if (isClassObjectType(aDO[1]))
    {
      // If it is a class object, check that it is a SearchOptions object
      assertAndNotify(sotools::isSearchOptionsObject(aDO[1].getClassObject().get()),
                      ERR_MSG_INPUT_ARG_TYPE);
    }
  }//checkFunctionArguments
  
  Interpreter::DataObject FunctionBranch::callback(const std::vector<Interpreter::DataObject>& aArgs)
  {
    // The arguments should contain only the function object
    assertAndNotify(aArgs.size() == 1 || aArgs.size() == 2, ERR_MSG_NUM_ARGS);
    
    // Create a function object Constraint
    const std::string branchFcnName = "Branch";
    Interpreter::DataObject bobj(branchFcnName.c_str());
    
    Interpreter::ObjectFcn* doFcn = new Interpreter::ObjectFcn(branchFcnName);
    bobj.setFcnObject(doFcn);
    bobj.compose(aArgs);
    
    try
    {
      return Branch(bobj);
    }
    catch(MVCException& mvce)
    {
      assertAndNotify(false, mvce.what());
    }
    catch (...)
    {
      assertAndNotify(false, "Branch object creation failed");
    }
    
    return Interpreter::DataObject(static_cast<int>(EXIT_FAILURE));
  }//callback
  
  Interpreter::DataObject FunctionBranch::Branch(const Interpreter::DataObject& aDO)
  {
    assertAndNotify(aDO.isFcnObject() && aDO.isComposite(), ERR_MSG_NUM_ARGS);
    
    // Check the arguments only if the object doesn't need any further interpretation
    checkFunctionArguments(aDO);
    
    // Wrap the post function around a constraint object
    Interpreter::BaseObject* branchObj{nullptr};
    
    // Prepare the scope object
    const auto& scope = aDO[0];
    if (!Interpreter::isClassObjectType(scope) ||
        (!otools::isListObject(scope.getClassObject().get())))
    {
      Interpreter::DataObject scopeList;
      
      auto list = otools::createListObject({scope});
      scopeList.setClassObject(std::shared_ptr<Interpreter::BaseObject>(list));
      branchObj = (aDO.composeSize() == 1) ?
      otools::createObjectSearch(scopeList) :
      otools::createObjectSearch(scopeList, aDO[1]);
    }
    else
    {
      assert(otools::isListObject(scope.getClassObject().get()));
      branchObj = (aDO.composeSize() == 1) ?
      otools::createObjectSearch(scope) :
      otools::createObjectSearch(scope, aDO[1]);
    }
    assert(branchObj);
    
    // Add unnamed object to context, this will give the object a default name in case
    // the (post) constraint is not assigned to any variable/id.
    // If it is assigned to an id, the id will be overwritten during the execution of
    // the STORE_NAME instruction
    Interpreter::DataObject dobjSearch;
    dobjSearch.setClassObject(std::shared_ptr<Interpreter::BaseObject>(branchObj));
    
    return dobjSearch;
  }// Branch
  
  bool FunctionBranch::hasOutput() const
  {
    return true;
  }// hasOutput
  
  std::string FunctionBranch::name() const
  {
    return "Branch";
  }//name
  
  
}//end namespace MVC
