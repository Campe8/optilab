// Base class
#include "FunctionVariable.hpp"
#include "FunctionMacro.hpp"

#include "OPCode.hpp"
#include "ObjectHelper.hpp"
#include "ObjectTools.hpp"

#include <algorithm>

namespace otools = Interpreter::ObjectTools;

namespace MVC {
  
  FunctionVariable::FunctionVariable()
  : Function(FunctionId::FCN_VARIABLE)
  {
  }
  
  Interpreter::DataObject FunctionVariable::callback(const std::vector<Interpreter::DataObject>& aArgs)
  {
    std::string optVal;
    std::vector<Interpreter::DataObject> domain;
    std::vector<Interpreter::DataObject> dimensions;
    if (aArgs.size() == 2 || aArgs.size() == 3)
    {
      // 2 arguments must be list, one must be a string
      for (auto& arg : aArgs)
      {
        if (arg.dataObjectType() == Interpreter::DataObject::DataObjectType::DOT_STRING)
        {
          if (optVal.empty())
          {
            optVal = arg.getDataValue<std::string>();
            continue;
          }
          else
          {
            assertAndNotify(false, ERR_MSG_INPUT_ARG_TYPE);
          }
        }
        
        // Here all other arguments must be lists
        assertAndNotify(Interpreter::isClassObjectType(arg), ERR_MSG_INPUT_ARG_TYPE);
        assertAndNotify(otools::isListObject(arg.getClassObject().get()), ERR_MSG_INPUT_ARG_TYPE);
        auto domainElements = otools::getListValues(arg.getClassObject().get());
        if (domain.empty())
        {
          domain = domainElements.getCompose();
        }
        else
        {
          dimensions = domainElements.getCompose();
        }
      }// for
      
    }
    else if (aArgs.size() == 1)
    {
      // 1 argument must be a list
      assertAndNotify(Interpreter::isClassObjectType(aArgs[0]), ERR_MSG_INPUT_ARG_TYPE);
      assertAndNotify(otools::isListObject(aArgs[0].getClassObject().get()), ERR_MSG_INPUT_ARG_TYPE);
      auto domainElements = otools::getListValues(aArgs[0].getClassObject().get());
      domain = domainElements.getCompose();
    }
    else
    {
      assertAndNotify(false, ERR_MSG_NUM_ARGS);
    }
    assertAndNotify(!domain.empty(), ERR_MSG_INPUT_ARG_TYPE);
    
    return Variable(domain, dimensions, optVal);
  }//callback
  
  Interpreter::DataObject FunctionVariable::Variable(const std::vector<Interpreter::DataObject>& domain,
                                                     const std::vector<Interpreter::DataObject>& dimensions,
                                                     const std::string& optimization)
  {
    int varSemantic = VAR_SPEC_DECISION;
    if (optimization == "minimize")
    {
      varSemantic = VAR_SPEC_OPT_MIN;
    }
    else if (optimization == "maximize")
    {
      varSemantic = VAR_SPEC_OPT_MAX;
    }
    else if (!optimization.empty())
    {
      assertAndNotify(false, ERR_MSG_INPUT_ARG);
    }
    
    Interpreter::BaseObject* variable;
    if (!dimensions.empty())
    {
      // Trick: let domains be BOUNDS, the object constructor will make it as a list
      variable = otools::createObjectMatrixVariable("", DOM_BOUNDS, domain,
                                                    dimensions, {}, varSemantic);
    }
    else
    {
      // Trick: let domains be BOUNDS, the object constructor will make it as a list
      variable = otools::createObjectVariable("", DOM_BOUNDS, domain, varSemantic);
    }
    
    // Set the object instance
    std::shared_ptr<Interpreter::BaseObject> variablePtr(variable);
    
    // Set the Variable object in the data object
    Interpreter::DataObject varObj;
    varObj.setClassObject(variablePtr);
    
    // Return the data object
    return varObj;
  }  // Variable
  
  bool FunctionVariable::hasOutput() const
  {
    return true;
  }  // hasOutput
  
  std::string FunctionVariable::name() const
  {
    return "Variable";
  }//name
  
}//end namespace MVC
