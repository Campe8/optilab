// Base class
#include "FunctionRegister.hpp"

#include "Function.hpp"
#include "FunctionInc.hpp"
#include "ModelUtils.hpp"

namespace MVC {
  
  FunctionRegister::FunctionRegister()
  : pController(nullptr)
  , pView(nullptr)
  , pModel(nullptr)
  {
    populateRegister();
  }
  
  void FunctionRegister::populateRegister()
  {
    FunctionIdType id{0};
    
    FcnPosterMapRegister reg;
    for(; id < static_cast<FunctionIdType>(FunctionId::FCN_UNDEF); ++id)
    {
      std::shared_ptr<Function> fcn;
      fcn = std::shared_ptr<Function>(reg.getPosterMap().at(id)());
      
      // Special handling for post-function:
      // call "post" on any constraint name
      if(static_cast<FunctionIdType>(id) == FunctionId::FCN_POST_CONSTRAINT)
      {
        registerPostConstraint(fcn, static_cast<FunctionId>(id));
      }
      else
      {
        // Register function
        registerFunction(fcn->name(), static_cast<FunctionId>(id), fcn);
      }
    }// for
  }//populateRegister
  
  void FunctionRegister::registerFunction(const std::string& aFcnName, FunctionId aFcnID, const std::shared_ptr<Function>& aFcn)
  {
    assert(aFcn);
    aFcn->setCallback(this);
    
    // Map ID to function
    pFcnRegMap.push_back({aFcnName, aFcnID});
    
    // Don't map twice function to same ID, this allows the register to use
    // the same function invoked with different names
    if(pFcnReg.find(static_cast<FunctionIdType>(aFcnID)) == pFcnReg.end())
    {
      pFcnReg[static_cast<FunctionIdType>(aFcnID)] = aFcn;
    }
  }//registerFunction
  
  void FunctionRegister::registerPostConstraint(const std::shared_ptr<Function>& aFcn, FunctionId aFcnID)
  {
    assert(aFcn);
    aFcn->setCallback(this);
    
    const auto& conHelper = Model::Utils::ConstraintHelper::getInstance();
    for(const auto& name : conHelper.getAllRegisteredConstraintNames())
    {
      // For all constraint names map them to their function ID
      pFcnRegMap.push_back({name, aFcnID});
    }
    
    if(pFcnReg.find(static_cast<FunctionIdType>(aFcnID)) == pFcnReg.end())
    {
      pFcnReg[static_cast<FunctionIdType>(aFcnID)] = aFcn;
    }
  }//registerPostConstraint
  
  void FunctionRegister::registerCallbackHandler(CallbackFcn aCallback, FunctionId aFcnId)
  {
    auto fId = static_cast<FunctionIdType>(aFcnId);
    pHandlerReg[fId] = aCallback;
  }//registerCallbackHandler
  
  std::string FunctionRegister::getRegisteredFcnNameByIndex(std::size_t aIdx) const
  {
    assert(aIdx < getRegisterSize());
    return pFcnRegMap.at(aIdx).first;
  }//getRegisteredFcnNameByIndex
  
  FunctionRegister::FcnSPtr FunctionRegister::getFunction(const std::string& aFcn) const
  {
    for(const auto& fcn : pFcnRegMap)
    {
      if(fcn.first == aFcn)
      {
        assert(pFcnReg.find(static_cast<FunctionIdType>(fcn.second)) != pFcnReg.end());
        auto fcnPtr = pFcnReg.at(static_cast<FunctionIdType>(fcn.second));
        fcnPtr->setModel(pModel);
        fcnPtr->setView(pView);
        fcnPtr->setController(pController);
        
        return fcnPtr;
      }
    }
    return nullptr;
  }//getFunction
  
  boost::optional<CallbackFcn> FunctionRegister::getCallbackFunction(const std::string& aFcn) const
  {
    for(const auto& fcn : pFcnRegMap)
    {
      if(fcn.first == aFcn)
      {
        assert(pHandlerReg.find(static_cast<FunctionIdType>(fcn.second)) != pHandlerReg.end());
        assert(pFcnReg.find(static_cast<FunctionIdType>(fcn.second)) != pFcnReg.end());
        
        // Set model and view for function callback
        auto fncPtr = pFcnReg.at(static_cast<FunctionIdType>(fcn.second));
        fncPtr->setModel(pModel);
        fncPtr->setView(pView);
        fncPtr->setController(pController);
        
        return boost::optional<CallbackFcn>(pHandlerReg.at(static_cast<FunctionIdType>(fcn.second)));
      }
    }
    return boost::optional<CallbackFcn>();
  }//getCallbackFunction
    
}//end namespace MVC
