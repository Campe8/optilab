// Base class
#include "Function.hpp"

#include "FunctionMacro.hpp"
#include "MVCController.hpp"

#include <iostream>
#include <cassert>

namespace MVC {
  
  Function::Function(FunctionId aFcnId)
  : pFcnId(aFcnId)
  , pEventFactory(new MVCEventFactory())
  , pController(nullptr)
  , pView(nullptr)
  , pModel(nullptr)
  {
  }
  
  void Function::setCallback(FunctionRegister* aReg)
  {
    assert(aReg);
    aReg->registerCallbackHandler(std::bind(&Function::callback, this, std::placeholders::_1), getFcnId());
  }//setCallback
  
  void Function::notifyController(const std::string& aMsg, Function::NotificationLevel aLevel)
  {
    // Notify the owner of this function about a message or an error
    assert(getController());
    
    switch (aLevel) {
      case Function::NotificationLevel::LEVEL_NONE:
        getController()->notifyOnMessage(aMsg);
        break;
      case Function::NotificationLevel::LEVEL_WARNING:
        getController()->notifyOnWarning(aMsg, MVCEvent::MVCEventActor::MVC_ACTOR_MODEL);
        break;
      default:
        assert(aLevel == Function::NotificationLevel::LEVEL_ERROR);
        getController()->notifyOnError(aMsg, MVCEvent::MVCEventActor::MVC_ACTOR_MODEL);
        break;
    }
  }//notifyController
  
  void Function::assertAndNotify(bool aTest, const std::string& aMsg)
  {
    if(aTest) { return; }
    
    // Prepare message:
    std::string errMsg = name();
    errMsg += "\n\t" + aMsg;
    
    // Assert on "aTest"
    if(!aTest) notifyController(errMsg, Function::NotificationLevel::LEVEL_ERROR);
    FCN_ASSERT_MSG(aTest, errMsg.c_str());
  }//assertAndNotify
  
}//end namespace MVC
