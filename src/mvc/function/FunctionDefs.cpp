// Self first
#include "FunctionDefs.hpp"
#include "FunctionInc.hpp"

#include <exception>

namespace MVC {
  
  //=== Poster functions ===//
  
  Function* pbranch()
  {
    return new FunctionBranch();
  }//pbranch
  
  Function* pconstraint()
  {
    return new FunctionConstraint();
  }//pconstraint
  
  Function* pctx()
  {
    return new FunctionCtx();
  }//pctx
  
  Function* pdelete()
  {
    return new FunctionDelete();
  }//pdelete
  
  Function* pget_branch_options()
  {
    return new FunctionGetBranchOptions();
  }//pget_branch_options
  
  Function* pget_cbls_options()
  {
    return new FunctionGetCBLSOptions();
  }//pget_cbls_options
  
  Function* pget_metrics_options()
  {
    return new FunctionGetMetricsOptions();
  }//pget_metrics_options
  
  Function* pget_unused_ctx_var_name()
  {
    return new FunctionGetUnusedCtxVarName();
  }//pget_unused_ctx_var_name
  
  Function* plog()
  {
    return new FunctionLog();
  }//plog
  
  Function* pobject()
  {
    return new FunctionObject();
  }//pobject
  
  Function* poutput()
  {
    return new FunctionOutput();
  }//poutput
  
  Function* prun()
  {
    return new FunctionRun();
  }//psolve
  
  Function* psearch()
  {
    return new FunctionSearch();
  }//psearch
  
  Function* psolve()
  {
    return new FunctionSolve();
  }//psolve
  
  Function* pvariable()
  {
    return new FunctionVariable();
  }//pvariable
  
  Function* pverbose()
  {
    return new FunctionVerbose();
  }//pverbose
  
  Function* ppost_constraint()
  {
    return new FunctionPostConstraint();
  }//ppost_constraint
  
  /*
   * Add a new poster function with the
   * following signature and definition here:
   *
   * Function* p<function_name>()
   * {
   *		return new <function_name_constructor>();
   * }
   *
   * where <function_name> is the name of the function and
   * <function_name_constructor> its class constructor.
   */
  
  //=== Map of poster functions ===//
  
  FcnPosterMap FcnPosterMapRegister::pFcnPosterMap =
  {
      { static_cast<FunctionIdType>(FCN_BRANCH), pbranch     }
    , { static_cast<FunctionIdType>(FCN_CONSTRAINT), pconstraint }
    , { static_cast<FunctionIdType>(FCN_CTX),      pctx      }
    , { static_cast<FunctionIdType>(FCN_DELETE),   pdelete   }
    , { static_cast<FunctionIdType>(FCN_GET_BRANCH_OPTIONS),  pget_branch_options  }
    , { static_cast<FunctionIdType>(FCN_GET_CBLS_OPTIONS),    pget_cbls_options    }
    , { static_cast<FunctionIdType>(FCN_GET_METRICS_OPTIONS), pget_metrics_options }
    , { static_cast<FunctionIdType>(FCN_GET_UNUSED_CTX_VAR_NAME), pget_unused_ctx_var_name }
    , { static_cast<FunctionIdType>(FCN_LOG),      plog      }
    , { static_cast<FunctionIdType>(FCN_OBJECT),   pobject   }
    , { static_cast<FunctionIdType>(FCN_OUTPUT),   poutput   }
    , { static_cast<FunctionIdType>(FCN_RUN),      prun      }
    , { static_cast<FunctionIdType>(FCN_SEARCH),   psearch   }
    , { static_cast<FunctionIdType>(FCN_SOLVE),    psolve    }
    , { static_cast<FunctionIdType>(FCN_VARIABLE), pvariable }
    , { static_cast<FunctionIdType>(FCN_VERBOSE),  pverbose  }
    , { static_cast<FunctionIdType>(FCN_POST_CONSTRAINT), ppost_constraint }
    
    /*
     * add new entry here:
     * , { static_cast<FunctionIdType>(<function_enum>), p<function_name> }
     */
    
  };//createPosterMap
  
  MVC_EXPORT_FCN void throwWithMessage(const char* aMsg)
  {
    throw std::runtime_error(aMsg);
  }//throwWithMessage
} // end namespace MVC
