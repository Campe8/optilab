// Base class
#include "FunctionLog.hpp"
#include "FunctionMacro.hpp"

#include "CLIUtils.hpp"

#include <boost/date_time/posix_time/posix_time.hpp>

#include <sstream>

namespace MVC {
  
  static std::string getDefaultLogName()
  {
    // Setup file name
    std::string logFileName = "optlog_";
    logFileName += CLI::Utils::getCurrentDate();
    logFileName += ".txt";
    
    return logFileName;
  }//getDefaultLogName
  
  FunctionLog::FunctionLog()
  : Function(FunctionId::FCN_LOG)
  , pLogOn(false)
  , pLogOut()
  , pOutStream(nullptr)
  {
    pLogOut = getDefaultLogName();
  }
  
  Interpreter::DataObject FunctionLog::callback(const std::vector<Interpreter::DataObject>& aArgs)
  {
    assertAndNotify(aArgs.size() == 1, ERR_MSG_NUM_ARGS);
    if(aArgs[0].dataObjectType() == Interpreter::DataObject::DataObjectType::DOT_STRING)
    {
      // If the input is a string it represents the file to log into
      auto filePath = aArgs[0].getDataValue<std::string>();
      assertAndNotify(!filePath.empty(), ERR_MSG_INPUT_ARG);
      if(CLI::Utils::isQuotedString(filePath))
      {
        // Set output file and return
        setOutFile(CLI::Utils::removeQuotesFromQuotedString(filePath));
        return Interpreter::DataObject();
      }
    }
    
    // The only other input argument type is an integer and it represents
    // the flag for turning on/off the logging function
    assertAndNotify(aArgs[0].dataObjectType() == Interpreter::DataObject::DataObjectType::DOT_INT, ERR_MSG_INPUT_ARG_TYPE);

    // Call to the log function
    auto logOn = aArgs[0].getDataValue<int>() == 0 ? false : true;
    log(logOn);
    
    return Interpreter::DataObject();
  }//callback
  
  void FunctionLog::openOutStream()
  {
    if(pOutStream)
    {
      closeOutStream();
    }
    assert(!pOutStream);
    assert(!pLogOut.empty());
    
    pOutStream.reset(new std::fstream(pLogOut, std::fstream::out | std::fstream::app));
    assertAndNotify(pOutStream->is_open(), "cannot open log file");
  }//openOutStream
  
  void FunctionLog::closeOutStream()
  {
    if(!pOutStream)
    {
      return;
    }
    
    pOutStream->close();
    pOutStream.reset(nullptr);
  }//closeOutStream
  
  void FunctionLog::log(bool aLogOn)
  {
    assert(getView());
    
    if(aLogOn)
    {
      openOutStream();
      
      // Send change out-stream event to view
      assert(pOutStream);
      assert(pOutStream->is_open());
      getView()->setOutStream(pOutStream.get());
    }
    else
    {
      // Reset output stream on view
      getView()->resetOutStream(pOutStream.get());
      
      // Close output stream
      closeOutStream();
    }
  }//log
  
  std::string FunctionLog::name() const
  {
    return "log";
  }//name
  
}//end namespace MVC
