// Self first
#include "FunctionConstraint.hpp"

#include "CLIUtils.hpp"
#include "FunctionMacro.hpp"
#include "ModelUtils.hpp"
#include "MVCUtils.hpp"
#include "ObjectTools.hpp"
#include "ObjectHelper.hpp"

#include <boost/optional.hpp>
#include <algorithm>

namespace otools = Interpreter::ObjectTools;

namespace {
  
  boost::optional<std::string> checkConstraintNameValidity(const std::string& aName)
  {
    const auto& conHelper = Model::Utils::ConstraintHelper::getInstance();
    if (!conHelper.isConstraintRegistered(aName))
    {
      // Constraint not found
      std::string undefFcn = ERR_MSG_UNDEF_FUNCTION;
      undefFcn += ": " + aName;
      return undefFcn;
    }
    
    return boost::none;
  }  // checkConstraintNameValidity
  
  bool isTypeContained(Model::Utils::ConstraintHelper::ArgType::Type aType,
                       const std::vector<Model::Utils::ConstraintHelper::ArgType::Type>& aTypes)
  {
    return std::find(aTypes.begin(), aTypes.end(), aType) != aTypes.end();
  }//isTypeContained
  
  /// Returns true if the element "aDO" has valid type w.r.t. the given array of types
  bool checkArgument(const Interpreter::DataObject& aDO,
                     const std::vector<Model::Utils::ConstraintHelper::ArgType::Type>& aArgTypes)
  {
    using namespace Model::Utils;
    if(Interpreter::isPrimitiveType(aDO) || (Interpreter::isClassObjectType(aDO) &&
                                             otools::isScalarObject(aDO.getClassObject().get())))
    {
      auto doType = Interpreter::isClassObjectType(aDO) ?
      otools::getScalarValue(aDO.getClassObject().get()).dataObjectType() :
      aDO.dataObjectType();
      switch (doType)
      {
        case Interpreter::DataObject::DataObjectType::DOT_INT:
          return isTypeContained(ConstraintHelper::ArgType::Type::T_INT, aArgTypes);
        case Interpreter::DataObject::DataObjectType::DOT_BOOL:
          return isTypeContained(ConstraintHelper::ArgType::Type::T_BOOL, aArgTypes);
        default:
          return false;
      }
    }
    else
    {
      assert(Interpreter::isClassObjectType(aDO));
      auto obj = aDO.getClassObject().get();
      if (otools::isVariableObject(obj) || otools::isVarSubscript(obj))
      {
        return isTypeContained(ConstraintHelper::ArgType::Type::T_VAR, aArgTypes);
      }
      
      // @todo handle matrix and list cases
      assert(false);
    }
    return false;
  }//checkArgument
  
  /// Returns true if the elements in the array of "aDO" have valid type
  /// w.r.t. the given array of types
  bool checkArrayArgument(const Interpreter::DataObject& aDO,
                          const std::vector<Model::Utils::ConstraintHelper::ArgType::Type>& aArgTypes)
  {
    for (const auto& elem : aDO)
    {
      if (!checkArgument(elem, aArgTypes)) return false;
    }
    return true;
  }//checkArrayArgument
  
  int getArgumentSize(const Interpreter::DataObject& aArg)
  {
    if(Interpreter::isClassObjectType(aArg))
    {
      auto obj = aArg.getClassObject().get();
      if (otools::isVariableObject(obj) || otools::isVarSubscript(obj))
      {
        return 1;
      }
      else
      {
        // Parameter can be a scalar, list, or matrix
        assert(otools::isParameterObject(obj));
        
        auto dims = otools::getParameterDimensions(obj);
        assert(!dims.empty());
        
        auto size = dims[0];
        for(std::size_t idx{1}; idx < dims.size(); ++idx) size *= dims[idx];
        return size;
      }
    }
    else
    {
      assert(Interpreter::isPrimitiveType(aArg));
      return 1;
    }
  }//getArgumentSize
  
  // Returns true if "aDO" has the arguments specified in "aArgs" that have same size.
  // Returns false otherwise
  bool hasSameSizeOnArgs(const std::vector<Interpreter::DataObject>& aConstraintArgs,
                         const std::vector<int>& aSameArgs)
  {
    int sizeArg{-1};
    for (auto idx : aSameArgs)
    {
      // Return false if there is no such argument in the constraint argument list
      if (idx >= aConstraintArgs.size()) return false;
      
      // Set the size to compare to
      if(sizeArg < 0)
      {
        sizeArg = getArgumentSize(aConstraintArgs[idx]);
        continue;
      }
      
      // Return false if an argument has size different from the base comparison size
      if(sizeArg != getArgumentSize(aConstraintArgs[idx])) return false;
    }
    
    return true;
  }//hasSameSizeOnArgs
  
  // Checks the validity of the arguments of the constraint encoded by the given data object.
  // Returns an error message if the arguments are not valid
  boost::optional<std::string> checkConstraintArgumentsValidity(const std::string& aName,
                                                                const std::vector<Interpreter::DataObject>& aConstraintArgs)
  {
    // For each constraint argument check its type w.r.t. the description contained in the helper
    const auto& conHelper = Model::Utils::ConstraintHelper::getInstance();
    
    if (aConstraintArgs.empty())
    {
      return std::string(ERR_MSG_INPUT_ARG_TYPE);
    }
    
    // Iterate over all constraint arguments
    std::size_t idx{0};
    for (const auto& arg : aConstraintArgs)
    {
      if (arg.isClassObject() &&
          (otools::isMatrixParameterObject(arg.getClassObject().get()) ||
           otools::isListObject(arg.getClassObject().get())))
      {
        // Check if the constraint doesn't allow matrices for this argument
        if (!conHelper.isConstraintArgumentArrayAllowed(aName, idx) ||
            !checkArrayArgument(arg, conHelper.getConstraintArgumentArrayAllowedTypes(aName, idx)))
        {
          return std::string(ERR_MSG_INPUT_ARG_TYPE);
        }
      }
      else
      {
        // The argument is not a matrix
        if (!checkArgument(arg, conHelper.getConstraintArgumentNonArrayAllowedTypes(aName, idx)))
        {
          return std::string(ERR_MSG_INPUT_ARG_TYPE);
        }
      }
      idx++;
    }//for
    
    // Check same size constraints
    if (conHelper.hasSameSizeArgumentConstraint(aName))
    {
      const auto& sameSizeArgs = conHelper.getSameSizeArgumentConstraint(aName);
      for (const auto& argList : sameSizeArgs)
      {
        if (!hasSameSizeOnArgs(aConstraintArgs, argList))
        {
          return std::string(ERR_MSG_INPUT_ARG_SIZE);
        }
      }//for
    }//if
    
    return boost::none;
  }  // checkConstraintArgumentsValidity
  
}  // namespace


namespace MVC {
  
  FunctionConstraint::FunctionConstraint()
  : Function(FunctionId::FCN_CONSTRAINT)
  {
  }
  
  Interpreter::DataObject FunctionConstraint::callback(const std::vector<Interpreter::DataObject>& aArgs)
  {
    if (aArgs.empty())
    {
      assertAndNotify(false, ERR_MSG_INPUT_ARG);
    }
    
    for (auto& arg : aArgs)
    {
      if (!arg.isFullyInterpreted())
      {
        assertAndNotify(false, ERR_MSG_INPUT_ARG);
      }
    }
    
    // Prepare the constraint data object
    if (aArgs[0].dataObjectType() != Interpreter::DataObject::DataObjectType::DOT_STRING)
    {
      assertAndNotify(false, ERR_MSG_INPUT_ARG);
    }
    
    // Set constraint name
    std::string cname =
    CLI::Utils::removeQuotesFromQuotedString(aArgs[0].getDataValue<std::string>());
    Interpreter::DataObject cobj(cname.c_str());
    
    Interpreter::ObjectFcn* doFcn = new Interpreter::ObjectFcn(cname);
    cobj.setFcnObject(doFcn);
    
    // Set constraint arguments
    auto iter = aArgs.begin();
    iter++;
    std::vector<Interpreter::DataObject> cargs(iter, aArgs.end());
    cobj.compose(cargs);
    
    // Create constraint and return the object
    return Constraint(cobj);
  }//callback
  
  Interpreter::DataObject FunctionConstraint::Constraint(const Interpreter::DataObject& aConDesc)
  {
    if (aConDesc.dataObjectType() != Interpreter::DataObject::DataObjectType::DOT_STRING)
    {
      assertAndNotify(false, ERR_MSG_INPUT_ARG);
    }
    std::string constraintName = aConDesc.getDataValue<std::string>();
    
    // Check constraint name
    assertAndNotify(!constraintName.empty(), ERR_MSG_INPUT_ARG);
    if (auto invalidArgs = checkConstraintNameValidity(constraintName))
    {
      // Throws if the arguments are not valid
      std::string errMsg = "- Constraint: " + *invalidArgs;
      assertAndNotify(false, errMsg);
    }
    
    // Check constraint arguments
    if (auto invalidArgs = checkConstraintArgumentsValidity(constraintName, aConDesc.getCompose()))
    {
      // Throws if the arguments are not valid
      std::string errMsg = "- Constraint: " + *invalidArgs;
      assertAndNotify(false, errMsg);
    }
    
    // Wrap the post function around a constraint object
    auto constraintBaseObj = otools::createObjectConstraint(aConDesc);
    auto constraintObj = std::shared_ptr<Interpreter::BaseObject>(constraintBaseObj);
    
    // Add unnamed object to context, this will give the object a default name in case
    // the (post) constraint is not assigned to any variable/id.
    // If it is assigned to an id, the id will be overwritten during the execution of
    // the STORE_NAME instruction
    Interpreter::DataObject dobjConstraint;
    dobjConstraint.setClassObject(constraintObj);
    
    // Set fully interpretation flag
    dobjConstraint.setFullyInterpretedTag(true);
    
    // Return the object
    return dobjConstraint;
  }  // Constraint
  
  bool FunctionConstraint::hasOutput() const
  {
    return true;
  }//hasOutput
  
  std::string FunctionConstraint::name() const
  {
    return "Constraint";
  }//name
  
}//end namespace MVC
