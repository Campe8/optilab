// Self first
#include "FunctionPostConstraint.hpp"

#include "CLIUtils.hpp"
#include "FunctionMacro.hpp"
#include "ModelUtils.hpp"
#include "MVCUtils.hpp"
#include "ObjectTools.hpp"
#include "ObjectHelper.hpp"

#include <boost/optional.hpp>
#include <algorithm>

namespace {
  /// Returns true if the constraint if fully interpreted, false otherwise
  bool isConstrainFullyInterpreted(const Interpreter::DataObject& aDO)
  {
    for (const auto& arg : aDO)
    {
      if (!arg.isFullyInterpreted()) return false;
    }
    return true;
  }//isConstrainFullyInterpreted
}  // namespace

namespace MVC {
  
  FunctionPostConstraint::FunctionPostConstraint()
  {
    resetFunctionId(FunctionId::FCN_POST_CONSTRAINT);
  }
  
  bool FunctionPostConstraint::requiresFcnObjectAsArgument() const
  {
    // Post constraint function requires the function data object
    // itself as an input parameter and as the object that
    // will be stored in the model and, in case, re-interpreted
    return true;
  }//requiresFcnObjectAsArgument
  
  Interpreter::DataObject FunctionPostConstraint::callback(const std::vector<Interpreter::DataObject>& aArgs)
  {
    // The arguments should contain only the function object
    assert(aArgs.size() == 1);
    
    // Check constraint arguments validity if the constraint is fully interpreted
    if (!isConstrainFullyInterpreted(aArgs[0]))
    {
      assertAndNotify(false, ERR_MSG_INPUT_ARG);
    }
    
    try
    {
      auto fcnObj = Constraint(aArgs[0]);
      
      // Add unnamed object to context, this will give the object a default name in case
      // the (post) constraint is not assigned to any variable/id.
      // If it is assigned to an id, the id will be overwritten during the execution of
      // the STORE_NAME instruction
      getModel()->addObjectToContext(fcnObj);
      
      // Return the object constraint to the caller
      return fcnObj;
    }
    catch(MVCException& mvce)
    {
      assertAndNotify(false, mvce.what());
    }
    catch (...)
    {
      assertAndNotify(false, "Post constraint failed");
    }
    
    return Interpreter::DataObject(static_cast<int>(EXIT_FAILURE));
  }//callback
  
  bool FunctionPostConstraint::hasOutput() const
  {
    return true;
  }//hasOutput
  
  std::string FunctionPostConstraint::name() const
  {
    // This name won't be used.
    // It will be replaced by the actual names
    // of the constraints to post
    return "postConstraint";
  }//name
  
}//end namespace MVC
