// Self first
#include "FunctionSearch.hpp"
#include "FunctionMacro.hpp"
#include "MVCUtils.hpp"
#include "MVCMacro.hpp"
#include "ObjectHelper.hpp"
#include "SearchOptionsTools.hpp"
#include "CLIMacroSearch.hpp"

namespace MVC {
  
  FunctionSearch::FunctionSearch()
  : FunctionBranch()
  {
    resetFunctionId(FunctionId::FCN_SEARCH);
  }
  
  bool FunctionSearch::requiresFcnObjectAsArgument() const
  {
    // Search function requires the function data object
    // itself as an input parameter and as the object that
    // will be stored in the model and, in case, re-interpreted
    return true;
  }//requiresFcnObjectAsArgument

  Interpreter::DataObject FunctionSearch::callback(const std::vector<Interpreter::DataObject>& aArgs)
  {
    // The arguments should contain only the function object
    assertAndNotify(aArgs.size() == 1 && aArgs[0].isFcnObject(), ERR_MSG_INPUT_ARG);
    
    // Check arguments consistency
    auto fullyInterp = aArgs[0].isFullyInterpreted();
    if (!fullyInterp)
    {
      assertAndNotify(false, ERR_MSG_INVALID_INTERP);
    }
    
    try
    {

      // Create a new Branch object
      auto brcObject = Branch(aArgs[0]);
      
      // Add unnamed object to context, this will give the object a default name in case
      // the (post) constraint is not assigned to any variable/id.
      // If it is assigned to an id, the id will be overwritten during the execution of
      // the STORE_NAME instruction
      getModel()->addObjectToContext(brcObject);
      
      return brcObject;
    }
    catch(MVCException& mvce)
    {
      assertAndNotify(false, mvce.what());
    }
    catch (...)
    {
      assertAndNotify(false, "Branch object creation failed");
    }
    
    return Interpreter::DataObject(static_cast<int>(EXIT_FAILURE));
  }//callback
  
  bool FunctionSearch::hasOutput() const
  {
    return true;
  }//hasOutput
  
  std::string FunctionSearch::name() const
  {
    return "branch";
  }//name
  
  
}//end namespace MVC
