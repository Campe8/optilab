// Base class
#include "FunctionCtx.hpp"

#include "FunctionMacro.hpp"
#include "MVCUtils.hpp"
#include "ObjectTools.hpp"

namespace MVC {
  
  FunctionCtx::FunctionCtx()
  : Function(FunctionId::FCN_CTX)
  {
  }
  
  MVCModelContext::PrettyPrintLevel FunctionCtx::setPrettyPrintLevel()
  {
    auto ppLevel = (getModel()->getModelContext()).getPrettyPrintLevel();
    if (ppLevel == MVCModelContext::PrettyPrintLevel::PP_NONE)
    {
      (getModel()->getModelContext()).setPrettyPrintLevel(MVCModelContext::PrettyPrintLevel::PP_LOW);
    }
    return ppLevel;
  }// setPrettyPrintLevel
  
  Interpreter::DataObject FunctionCtx::callback(const std::vector<Interpreter::DataObject>& aArgs)
  {
    // Set the pretty print level to pretty print the object
    auto originPPLevel = setPrettyPrintLevel();

    if (aArgs.empty())
    {
      // With no arguments the "ctx" function prints the entire context.
      // Invoke the actual context function
      ctx();
      
      // Reset the pretty print level to its original level
      (getModel()->getModelContext()).setPrettyPrintLevel(originPPLevel);
      
      return Interpreter::DataObject();
    }
    assertAndNotify(aArgs.size() == 1, "invalid number of arguments");
    
    // Invoke the context function
    ctx(aArgs[0]);
    
    // Reset the pretty print level to its original level
    (getModel()->getModelContext()).setPrettyPrintLevel(originPPLevel);
    
    return Interpreter::DataObject();
  }//callback
  
  void FunctionCtx::ctx()
  {
    assert(getModel());
    
    // Check if the model context needs to be re-interpreted before printing
    const auto& mdlCtx = getModel()->getModelContext();
    if(!mdlCtx.isModelContextFullyInterpreted())
    {
      getModel()->interpretModelContext();
    }
    
    // Set pretty print level to LOW
    auto ppLevel = (getModel()->getModelContext()).setPrettyPrintLevel(MVCModelContext::PrettyPrintLevel::PP_LOW);
    
    // Print the context
    std::stringstream ss;
    mdlCtx.prettyPrint(ss);
    
    // Reset pretty print level to original value
    mdlCtx.setPrettyPrintLevel(ppLevel);
    
    notifyController(ss.str());
  }//ctx
  
  void FunctionCtx::ctx(const Interpreter::DataObject& aDO)
  {
    assert(getModel());
    
    // Get key for var object from context
    auto objID = Utils::getIDFromDataObject(aDO);
    assertAndNotify(!objID.empty(), ERR_MSG_INPUT_ARG_TYPE);
    
    auto key = (getModel()->getModelContext()).getContextKeyFromID(objID);
    if(key == MVCModelContext::contextKeyInf)
    {
      std::string notFound{ ERR_MSG_OBJ_NOT_FOUND };
      notFound += ": " + objID;
      notifyController(notFound);
      
      return;
    }

    // Check if the object if fully interpreted
    const auto& mdlCtx = getModel()->getModelContext();
    if(!mdlCtx.isContextObjectFullyInterpreted(key))
    {
      // If not re-interpret it and return.
      // @note re-interpreting an object will automatically
      // pretty print it
      getModel()->interpretContextObject(key);
      return;
    }
    
    // Print the context object
    std::stringstream ss;
    (getModel()->getModelContext()).prettyPrint(key, ss);
    
    notifyController(ss.str());
  }//ctx
  
  std::string FunctionCtx::name() const
  {
    return "ctx";
  }//name
  
}//end namespace MVC
