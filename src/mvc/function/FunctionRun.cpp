// Self first
#include "FunctionRun.hpp"

#include "FunctionMacro.hpp"
#include "MVCMacro.hpp"

#include "MVCController.hpp"

#include <algorithm>
#include <iterator>
#include <fstream>
#include <sstream>

namespace MVC {
  
  static bool exists(const std::string& aPath)
  {
    std::ifstream f(aPath.c_str());
    return f.good();
  }//exists
  
  static bool validFile(const std::string& aPath)
  {
    if(aPath.size() < 5)
    {
      return false;
    }
    auto ext = aPath.substr(aPath.size() - 4);
    return ext == MVC_FILE_MODEL_EXT;
  }//exists
  
  FunctionRun::FunctionRun()
  : Function(FunctionId::FCN_RUN)
  , pFileReaderStream(nullptr)
  {
  }
  
  Interpreter::DataObject FunctionRun::callback(const std::vector<Interpreter::DataObject>& aArgs)
  {
    assertAndNotify(aArgs.size() == 1, ERR_MSG_NUM_ARGS);
    assertAndNotify(aArgs[0].dataObjectType() == Interpreter::DataObject::DataObjectType::DOT_STRING, ERR_MSG_INPUT_ARG_TYPE);
    
    auto filePath = aArgs[0].getDataValue<std::string>();
    assertAndNotify(!filePath.empty(), ERR_MSG_INPUT_ARG);
    
    // Run the back-end on the input file
    run(CLI::Utils::removeQuotesFromQuotedString(filePath));
    
    return Interpreter::DataObject();
  }//callback
  
  void FunctionRun::switchOnFileReader(const std::string& aPath)
  {
    pFileReaderStream.reset(new std::fstream(aPath));
    
    if (!pFileReaderStream->is_open())
    {
      switchOffFileReader();

      notifyController(ERR_MSG_FILE_INVALID, Function::NotificationLevel::LEVEL_ERROR);
      return;
    }
  }//switchOnFileReader
  
  void FunctionRun::switchOffFileReader()
  {
    if (!pFileReaderStream)
    {
      return;
    }
    
    // Reset the input stream to the previous one
    getView()->resetInStream(pFileReaderStream.get());
    
    // Close the stream
    pFileReaderStream.reset(nullptr);
  }//switchOffFileReader
  
  void FunctionRun::run(const std::string& aPath)
  {
    assert(getView());
    
    if(!exists(aPath))
    {
      notifyController(ERR_MSG_FILE_NOT_FOUND, Function::NotificationLevel::LEVEL_ERROR);
      return;
    }
    
    if(!validFile(aPath))
    {
      notifyController(ERR_MSG_FILE_INVALID, Function::NotificationLevel::LEVEL_ERROR);
      return;
    }
    
    /*
     * Following is the old code for run function
     * changing the input stream on the view.
     *
     * // Open file reader
     * switchOnFileReader(aPath);
     *
     * // Set new input stream
     * getView()->setInStream(pFileReaderStream.get());
     * return;
     */
    
    // Open file reader
    std::ifstream fileReader(aPath);
    
    // If not open, notify the view and return
    if (!fileReader.is_open())
    {
      notifyController(ERR_MSG_FILE_INVALID, Function::NotificationLevel::LEVEL_ERROR);
      return;
    }
    
    // Copy the file buffer onto the string stream
    std::stringstream ssFile;
    ssFile << fileReader.rdbuf();
    
    // Close the file right away
    fileReader.close();

    // Add new line at the end of the stream (required by the parser infrastructure)
    ssFile << "\n";

    // Get the controller and run the interpreter on the input stream
    bool processOK{false};
    try
    {
      processOK = getController()->runInterpreter(ssFile, true);
    }
    catch(...)
    {
      // Notify the view on error and return
      notifyController(ERR_MSG_ERR_MODEL, Function::NotificationLevel::LEVEL_ERROR);
      return;
    }
    
    // Close the stream
    if(!processOK)
    {
      // Notify the view on error and return
      notifyController(ERR_MSG_ERR_MODEL, Function::NotificationLevel::LEVEL_ERROR);
    }
  }//run
  
  std::string FunctionRun::name() const
  {
    return "run";
  }//name
  
}//end namespace MVC
