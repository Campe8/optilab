// Self first
#include "FunctionGetBranchOptions.hpp"

#include "FunctionMacro.hpp"
#include "MVCUtils.hpp"
#include "SearchOptionsTools.hpp"

#include "DataObject.hpp"
#include "DFSInformation.hpp"
#include "GeneticInformation.hpp"
#include "CLIUtils.hpp"
#include <memory>

namespace sotools = Interpreter::SearchOptionsTools;

namespace MVC {
  
  FunctionGetBranchOptions::FunctionGetBranchOptions()
  : Function(FunctionId::FCN_GET_BRANCH_OPTIONS)
  {
  }
  
  Interpreter::DataObject FunctionGetBranchOptions::callback(const std::vector<Interpreter::DataObject>& aArgs)
  {
    assertAndNotify(aArgs.size() == 1, "invalid number of arguments");
    
    // Invoke the context function
    return getBranchOptions(aArgs[0]);
  }//callback
  
  Interpreter::DataObject FunctionGetBranchOptions::getBranchOptions(const Interpreter::DataObject& aDO)
  {
    assertAndNotify(aDO.dataObjectType() ==
                    Interpreter::DataObject::DataObjectType::DOT_STRING, ERR_MSG_INPUT_ARG_TYPE);
    
    // Get the type of search strategy to instantiate the search option object
    auto searchStrategy = aDO.getDataValue<std::string>();
    if (CLI::Utils::isQuotedString(searchStrategy))
    {
      searchStrategy = CLI::Utils::removeQuotesFromQuotedString(searchStrategy);
    }
    
    Interpreter::BaseObject* strategy{nullptr};
    if (searchStrategy == Model::DFSInformation::StrategyName)
    {
      strategy = Interpreter::SearchOptionsTools::getDFSSearchOptionsObject();
      
      
      auto dfsObject = Interpreter::SearchOptionsTools::getDFSSearchOptionsObject();
      auto dfsObj = std::shared_ptr<Interpreter::BaseObject>(dfsObject);
      
      Interpreter::DataObject dobjSrcOpt;
      dobjSrcOpt.setClassObject(dfsObj);
      
      return dobjSrcOpt;
    }
    else
    {
      // Only available CBLS strategy is genetic
      assertAndNotify(searchStrategy == Model::GeneticInformation::StrategyName,
                      ERR_MSG_INPUT_ARG_TYPE);
      strategy = Interpreter::SearchOptionsTools::getGeneticSearchOptionsObject();
    }
    assert(strategy);
    
    auto searchObj = std::shared_ptr<Interpreter::BaseObject>(strategy);
    
    Interpreter::DataObject dobjSrcOpt;
    dobjSrcOpt.setClassObject(searchObj);
    
    return dobjSrcOpt;
  }//getBranchOptions
  
  bool FunctionGetBranchOptions::hasOutput() const
  {
    return true;
  }//hasOutput
  
  std::string FunctionGetBranchOptions::name() const
  {
    return "getSearchOptions";
  }//name
  
}//end namespace MVC
