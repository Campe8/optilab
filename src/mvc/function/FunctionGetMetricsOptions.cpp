// Self first
#include "FunctionGetMetricsOptions.hpp"

#include "FunctionMacro.hpp"
#include "MVCUtils.hpp"
#include "SearchOptionsTools.hpp"

#include "DataObject.hpp"
#include "ObjectToolsAux.hpp"

#include <memory>

namespace otools = Interpreter::ObjectTools;

namespace MVC {
  
  FunctionGetMetricsOptions::FunctionGetMetricsOptions()
  : Function(FunctionId::FCN_GET_METRICS_OPTIONS)
  {
  }
  
  Interpreter::DataObject FunctionGetMetricsOptions::callback(
                                                  const std::vector<Interpreter::DataObject>& aArgs)
  {
    assertAndNotify(aArgs.size() == 0, "invalid number of arguments");
    
    // Invoke the function
    return getMetricsOptions();
  }//callback
  
  Interpreter::DataObject FunctionGetMetricsOptions::getMetricsOptions()
  {
    auto metricsObject = otools::getMetricsOptionsObject();
    auto metsObj = std::shared_ptr<Interpreter::BaseObject>(metricsObject);
    
    Interpreter::DataObject dobjMetsOpt;
    dobjMetsOpt.setClassObject(metsObj);
    
    return dobjMetsOpt;
  }//getCBLSOptions
  
  bool FunctionGetMetricsOptions::hasOutput() const
  {
    return true;
  }//hasOutput
  
  std::string FunctionGetMetricsOptions::name() const
  {
    return "getMetricsOptions";
  }//name
  
}//end namespace MVC
