// Self first
#include "FunctionGetCBLSOptions.hpp"

#include "FunctionMacro.hpp"
#include "MVCUtils.hpp"
#include "SearchOptionsTools.hpp"

#include "DataObject.hpp"
#include "GeneticInformation.hpp"
#include "CLIUtils.hpp"

#include <memory>

namespace sotools = Interpreter::SearchOptionsTools;

namespace MVC {
  
  FunctionGetCBLSOptions::FunctionGetCBLSOptions()
  : Function(FunctionId::FCN_GET_CBLS_OPTIONS)
  {
  }
  
  Interpreter::DataObject FunctionGetCBLSOptions::callback(const std::vector<Interpreter::DataObject>& aArgs)
  {
    assertAndNotify(aArgs.size() == 1, "invalid number of arguments");
    
    // Invoke the context function
    return getCBLSOptions(aArgs[0]);
  }//callback
  
  Interpreter::DataObject FunctionGetCBLSOptions::getCBLSOptions(const Interpreter::DataObject& aDO)
  {
    assertAndNotify(aDO.dataObjectType() ==
                    Interpreter::DataObject::DataObjectType::DOT_STRING, ERR_MSG_INPUT_ARG_TYPE);
    
    // Get the type of cbls strategy to instantiate the search option object.
    // @note only available cbls strategy is genetic
    auto cblsSearch = aDO.getDataValue<std::string>();
    if (CLI::Utils::isQuotedString(cblsSearch))
    {
      cblsSearch = CLI::Utils::removeQuotesFromQuotedString(cblsSearch);
    }
    assertAndNotify(cblsSearch == Model::GeneticInformation::StrategyName, ERR_MSG_INPUT_ARG_TYPE);
      
    auto geneticObject = Interpreter::SearchOptionsTools::getGeneticSearchOptionsObject();
    auto cblsObj = std::shared_ptr<Interpreter::BaseObject>(geneticObject);
    
    Interpreter::DataObject dobjSrcOpt;
    dobjSrcOpt.setClassObject(cblsObj);
    
    return dobjSrcOpt;
  }//getCBLSOptions
  
  bool FunctionGetCBLSOptions::hasOutput() const
  {
    return true;
  }//hasOutput
  
  std::string FunctionGetCBLSOptions::name() const
  {
    return "getCBLSOptions";
  }//name
  
}//end namespace MVC
