// Self first
#include "FunctionOutput.hpp"

#include "FunctionMacro.hpp"
#include "VariableContextObject.hpp"
#include "ObjectHelper.hpp"
#include "MVCUtils.hpp"

#include <algorithm>

namespace otools = Interpreter::ObjectTools;

namespace MVC {
  
  static bool isArrayOfIdentifiers(const Interpreter::DataObject& aDO)
  {
    if(Interpreter::isClassObjectType(aDO))
    {
      auto obj = aDO.getClassObject().get();
      if(otools::isVariableObject(obj)) return true;
      else if(otools::isListObject(obj))
      {
        for(const auto& listObj : otools::getListValues(obj))
        {
          if(!isArrayOfIdentifiers(listObj)) return false;
        }
        return true;
      }
    }
    return false;
  }//isArrayOfIdentifiers
  
  static std::vector<std::string> collectVarIDs(const Interpreter::DataObject& aDO)
  {
    std::vector<std::string> IDs;
    for(const auto& var : aDO)
    {
      if(!(Interpreter::isClassObjectType(aDO) && otools::isVariableObject(aDO.getClassObject().get()))) continue;
      IDs.push_back(Utils::getLinkedVariableIDFromDomainDataObject(var));
    }
    
    return IDs;
  }//collectVarIDs
  
  FunctionOutput::FunctionOutput()
  : Function(FunctionId::FCN_OUTPUT)
  {
  }
  
  Interpreter::DataObject FunctionOutput::callback(const std::vector<Interpreter::DataObject>& aArgs)
  {
    if (aArgs.empty())
    {
      // With no arguments the function clear the output semantic of
      // all variables in the context
      clearOutputSemantic();
      return Interpreter::DataObject();
    }
    
    // Invoke the context function
    output(aArgs[0]);
    
    return Interpreter::DataObject();
  }//callback
  
  void FunctionOutput::clearOutputSemantic()
  {
    assert(getModel());
    
    const auto& mdlCtx = getModel()->getModelContext();
    for(const auto& it : mdlCtx)
    {
      if(it.second->getType() != Model::ContextObject::Type::CTX_OBJ_VAR) continue;
      
      // For the context object variables, clear their output semantic
      // Continue for non fully interpreted objects
      if(!it.second->payload().isFullyInterpreted()) continue;
      static_cast<Model::VariableContextObject*>(it.second.get())->suppressOutput();
    }
  }//clearOutputSemantic
  
  void FunctionOutput::output(const Interpreter::DataObject& aDO)
  {
    assert(getModel());

    bool checkVarSet = isArrayOfIdentifiers(aDO);
    assertAndNotify(checkVarSet, ERR_MSG_INPUT_ARG);
    
    // For each variable in the context, if it is fully interpreted
    // and it is in the output variable IDs vector, then force its output.
    // All the other variables must have their output switched off
    auto varIDs = collectVarIDs(aDO);
    const auto& mdlCtx = getModel()->getModelContext();
    for(const auto& it : mdlCtx)
    {
      if(it.second->getType() != Model::ContextObject::Type::CTX_OBJ_VAR) continue;
      
      // Continue for non fully interpreted objects
      if(!it.second->payload().isFullyInterpreted()) continue;
      auto varCtx = static_cast<Model::VariableContextObject*>(it.second.get());
      if(std::find(varIDs.begin(), varIDs.end(), varCtx->getID()) == varIDs.end())
      {
        // The variable is not in the list of output variable, suppress its output
        varCtx->suppressOutput();
      }
      else
      {
        // The variable is in the list of output variable, force its output
        varCtx->forceOutput();
      }
    }
  }//output
  
  std::string FunctionOutput::name() const
  {
    return "output";
  }//name
  
}//end namespace MVC
