// Base class
#include "FunctionDelete.hpp"

#include "FunctionMacro.hpp"
#include "MVCController.hpp"
#include "ObjectHelper.hpp"
#include "MVCUtils.hpp"
#include "CLIUtils.hpp"

namespace MVC {

	FunctionDelete::FunctionDelete()
  : Function(FunctionId::FCN_DELETE)
	{
	}

	Interpreter::DataObject FunctionDelete::callback(const std::vector<Interpreter::DataObject>& aArgs)
	{
		if (aArgs.empty())
		{
			deleteCtx();
			return Interpreter::DataObject();
		}
		assertAndNotify(aArgs.size() == 1, ERR_MSG_NUM_ARGS);

		// Invoke the actual ctx function
		deleteCtx(aArgs[0]);
    
    return Interpreter::DataObject();
	}//callback

	void FunctionDelete::deleteCtx()
	{
		assert(getModel());

		// Delete the context
    getModel()->clearModelContext();
    
    // Clear the state of the interpreter
    getController()->resetInterpreterState();
	}//deleteCtx

	void FunctionDelete::deleteCtx(const Interpreter::DataObject& aDO)
	{
		assert(getModel());
    auto doType  = aDO.dataObjectType();
    bool isValidType = (doType == Interpreter::DataObject::DataObjectType::DOT_STRING) ||
                       (aDO.isClassObject() &&
                        (Interpreter::ObjectTools::isVariableObject(aDO.getClassObject().get())));
    assertAndNotify(isValidType, ERR_MSG_INPUT_ARG_TYPE);
    
    std::string ID;
    if(doType == Interpreter::DataObject::DataObjectType::DOT_STRING)
    {
      ID = aDO.getDataValue<std::string>();
    }
    else
    {
      ID = Utils::getLinkedVariableIDFromDomainDataObject(aDO);
    }
    
    if(CLI::Utils::isQuotedString(ID))
    {
      ID = CLI::Utils::removeQuotesFromQuotedString(ID);
    }
    
		// Get key for the object with given ID from context
		auto key = (getModel()->getModelContext()).getContextKeyFromID(ID);
		if (key == MVCModelContext::contextKeyInf)
		{
			std::string notFound{ ERR_MSG_OBJ_NOT_FOUND };
      notFound += " " + ID;
			notifyController(notFound);

			return;
		}

		// Delete the object
    getModel()->eraseContextObject(key);
	}//deleteCtx
  
	std::string FunctionDelete::name() const
	{
		return "del";
	}//name

}//end namespace MVC
