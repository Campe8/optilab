// Self first
#include "MVCController.hpp"

#include "Function.hpp"
#include "MVCDefs.hpp"
#include "GlobalsSysLib.hpp"
#include "MVCMacroController.hpp"
#include "CLIUtils.hpp"
#include "MVCMacro.hpp"
#include "MVCUtils.hpp"
#include "ObjectTools.hpp"
#include "ObjectHelper.hpp"

#include <stdlib.h>

#include <exception>

// #define MVC_DEBUG
// #define MVC_DEBUG_PARSER

namespace otools = Interpreter::ObjectTools;

namespace MVC {
  
  // Merges "aSrcMap" into "aDstMap"
  static void mergeMap(const Interpreter::Frame::NameMapSPtr& aSrcMap,
                       const Interpreter::Frame::NameMapSPtr& aDstMap)
  {
    if (aSrcMap)
    {
      for (const auto& it : *aSrcMap)
      {
        aDstMap->insert({it.first, it.second});
      }
    }
  }//mergeMap
  
  // Merges "aSrcEnv" into "aDestEnv"
  static void mergeEnvironment(const Interpreter::VirtualMachine::VMEnv& aSrcEnv,
                               Interpreter::VirtualMachine::VMEnv& aDstEnv)
  {
    if (!aDstEnv.first)
    {
      aDstEnv.first  = aSrcEnv.first;
    }
    else
    {
      mergeMap(aSrcEnv.first, aDstEnv.first);
    }
    
    if (!aDstEnv.second)
    {
      aDstEnv.second  = aSrcEnv.second;
    }
    else
    {
      mergeMap(aSrcEnv.second, aDstEnv.second);
    }
  }//mergeEnvironment
  
  static Interpreter::CodeObject getCodeObjectFromByteCode(const CLI::CLIDriver::ByteCode& aByteCode)
  {
    Interpreter::CodeObject codObj;
    
    // Add constants
    for (const auto& cArray : aByteCode.const_array)
    {
      assert(!cArray.empty());
      if (cArray.size() == 1)
      {
        if(cArray[0].getType() == 0)
        {
          codObj.COConstants.push_back( Interpreter::DataObject(static_cast<int>(cArray[0].getInt())) );
        }
        else
        {
          assert(cArray[0].getType() == 1);
          codObj.COConstants.push_back( Interpreter::DataObject(cArray[0].getString().c_str()) );
        }
      }
      else
      {
        codObj.COConstants.push_back(Interpreter::DataObject());
        std::vector<Interpreter::DataObject> compConst;
        compConst.reserve(cArray.size());
        for (const auto& cVal : cArray)
        {
          if(cVal.getType() == 0)
          {
            compConst.push_back( Interpreter::DataObject(static_cast<int>(cVal.getInt())) );
          }
          else
          {
            assert(cVal.getType() == 1);
            compConst.push_back( Interpreter::DataObject(cVal.getString().c_str()) );
          }
        }
        codObj.COConstants.back().compose(compConst);
      }
    }//for
    
    // Add global and local names
    for (const auto& n : aByteCode.global_array) codObj.COGlbNames.push_back(Interpreter::DataObject(n.c_str()));
    for (const auto& n : aByteCode.local_array)  codObj.COLocNames.push_back(Interpreter::DataObject(n.c_str()));
    
    // Add the byte code
    codObj.COCode = aByteCode.byte_code;
    
    return codObj;
  }//getCodeObjectFromByteCode
  
  // Registers the function callbacks in "aFcnReg" into the virtual machine "aVM"
  static void registerInlineFunctionsInVM(Interpreter::VirtualMachine* aVM, FunctionRegister* aFcnReg)
  {
    assert(aVM && aFcnReg);
    for(std::size_t idx{0}; idx < aFcnReg->getRegisterSize(); ++idx)
    {
      auto fcnName = aFcnReg->getRegisteredFcnNameByIndex(idx);
      if(auto callback = aFcnReg->getCallbackFunction(fcnName))
      {
        assert(aFcnReg->getFunction(fcnName));
        aVM->registerInlineCallbackFcn(fcnName,   // function name
                                       *callback, // callback function
                                       aFcnReg->getFunction(fcnName)->requiresFcnObjectAsArgument(), // requires the object fcn
                                       aFcnReg->getFunction(fcnName)->hasOutput());                  // has output values
      }
    }
  }//registerInlineFunctionsInVM
  
  // Returns true if the given object has been mapped into the MVC model context.
  // Returns false otherwise
  static bool isObjectMappedIntoContext(const Interpreter::DataObject& aDO)
  {
    return (!Utils::getIDFromDataObject(aDO).empty());
  }//getObjectFromContext
  
  MVCController::MVCController(const std::shared_ptr<MVCView>& aView,
                               const std::shared_ptr<MVCModel>& aModel)
  : MVC::MVCObserver(MVCEvent::MVCEventActor::MVC_ACTOR_CONTROLLER)
  , pVMRun(0)
  , pErrorNotification(false)
  , pDriver(nullptr)
  , pVirtualMachine(nullptr)
  , pFcnRegister(nullptr)
  , pControllerImpl(nullptr)
  , pView(aView)
  , pModel(aModel)
  {
    // Set observed subjects
    assert(aView && aModel);
    observe(MVCView::getSubject(aView.get()));
    observe(MVCModel::getSubject(aModel.get()));
    
    // Instantiate the driver for parsing input statements
    pDriver.reset(new CLI::CLIDriver());
    
    // Register all the callback (inline) functions
    pFcnRegister.reset(new FunctionRegister());
    
    // Set MVC components into the function register which,
    // in turn, will set them into the invoked functions
    pFcnRegister->setView(getView());
    pFcnRegister->setModel(getModel());
    pFcnRegister->setController(this);
    
    // Instantiate the virtual machine for interpreting bytecode generated by the driver
    pVirtualMachine.reset(new Interpreter::VirtualMachine());
    
    // Setup the virtual machine, for example, register all the inline functions
    registerInlineFunctionsInVM(pVirtualMachine.get(), pFcnRegister.get());
    
    // Register model-specific callbacks into the virtual machine
    pVirtualMachine->
    registerCallbackForModelCtxDeclarations(std::bind(&MVCController::addNamedObjectToContext, this,
                                                      std::placeholders::_1, std::placeholders::_2));
    
    pVirtualMachine->
    registerCallbackForObjectReporting(std::bind(&MVCController::getOrCreateObjectFromContext, this,
                                                 std::placeholders::_1, std::placeholders::_2));
    
    // Reset environment and interpreter
    resetInterpreterState();
    
    // Create concrete implementation for this subject
    pControllerImpl =
    std::shared_ptr<_private::MVCControllerImpl>(new _private::MVCControllerImpl(this));
    
    // Small optimization, initializes static instances in the object helper
    otools::initStaticObjectInstances();
  }
  
  void MVCController::resetInterpreterState()
  {
    // Empty global names environment on initialization
    pVMEnvironment.first = nullptr;
    
    // Empty local names environment on initialization
    pVMEnvironment.second = nullptr;
    
    // Add global environment variables
    prepareVMEnvironment();
    
    // Clear VM state
    pVirtualMachine->clearState();
  }//resetInterpreterState
  
  void MVCController::prepareVMEnvironment()
  {
    pVMEnvironment.first = std::make_shared<Interpreter::Frame::NameMap>();
    pVMEnvironment.second = std::make_shared<Interpreter::Frame::NameMap>();
    
    const auto& anyObject = Interpreter::getAnyObject();
    const auto anyName = anyObject.getDataValue<std::string>();
    pVMEnvironment.first->insert({anyName, anyObject});
    pVMEnvironment.second->insert({anyName, anyObject});
  }//prepareVMEnvironment
  
  void MVCController::registerEventHandlers(MVCEventDispatcher* aDispatcher)
  {
    assert(aDispatcher);
    
    // Register handle for evaluate code object event
    aDispatcher->registerEventHandler(std::bind(&MVCController::handleEvalCodeObjectEvent, this, std::placeholders::_1),
                                      MVCEventType::MVC_EVT_EVAL_CODE_OBJ);
  }//registerEventHandlers
  
  void MVCController::handleEvalCodeObjectEvent(MVCEvent* aEvent)
  {
    // Get the event and the code object image from it
    // @todo assert that the code object points to an object code object image
    auto evalCode = MVCEventEvaluateCodeObject::cast(aEvent);
    auto codeImg = static_cast<const Interpreter::CodeObjectImage*>(evalCode->getCodeObject());
    interpretByteCodeImage(*codeImg);
  }//handleEvalCodeObjectEvent
  
  void MVCController::addNamedObjectToContext(const std::string& aName, const Interpreter::DataObject& aDO)
  {
    try
    {
      getModel()->addObjectToContext(aName, aDO);
    }
    catch (MVCException& mvce)
    {
      std::string msg = "Cannot add " + aName + " to the context\n" + std::string(mvce.what());
      notifyOnError(msg, MVCEvent::MVCEventActor::MVC_ACTOR_CONTROLLER);
      throw;
    }
    catch (...)
    {
      notifyOnError("Cannot add " + aName + " to the context", MVCEvent::MVCEventActor::MVC_ACTOR_CONTROLLER);
      throw;
    }
  }//addNamedObjectToContext
  
  void MVCController::processStmt(const std::string& aStmt)
  {
    
#ifdef MVC_DEBUG
    std::cout << "== Input Statement ==\n" << aStmt << "\n================\n";
#endif
    
    // Nothing to do on empty statements, return
    if(aStmt.empty()) return;
    
    // Create an input stream wrapping the input statement
    // and process it
    bool processOK{false};
    std::stringstream ss(aStmt);
    try
    {
      processOK = runInterpreter(ss);
    }
    catch (...)
    {
#ifdef MVC_DEBUG
      // Notify the view on error and return
      notifyOnError("Invalid input:\n\t" + aStmt, MVCEvent::MVCEventActor::MVC_ACTOR_CONTROLLER);
#endif
      return;
    }
    
#ifdef MVC_DEBUG
    if(!processOK)
    {
      // Notify the view on error and return
      notifyOnError("Invalid input:\n\t" + aStmt, MVCEvent::MVCEventActor::MVC_ACTOR_CONTROLLER);
    }
#endif
  }//processStmt
  
  bool MVCController::runInterpreter(std::istream& aInStream, bool aFromFile)
  {
    // Set file stream flag on driver
    pDriver->setInputStreamLine(!aFromFile);
    
    // Invoke the driver of the parser on the input stream
    // @note the driver can throw exceptions
    pDriver->parse(aInStream);
    
#ifdef MVC_DEBUG_PARSER
    std::cout << "======== Parser ========\n";
    pDriver->printStack();
    exit(2);
#endif

    if(pDriver->hasError())
    {
      notifyOnError(pDriver->getErrorMsg(), MVCEvent::MVCEventActor::MVC_ACTOR_CONTROLLER);
      return false;
    }
    
    // Generate the bytecode from the parsed input
    auto cobj = pDriver->byteCode();
    
#ifdef MVC_DEBUG
    printByteCode(cobj);
#endif
    
    // Prepare the code object to be run on the virtual machine
    auto codeObject = getCodeObjectFromByteCode(cobj);

    // Run the virtual machine on the byte code
    return interpretByteCode(codeObject, pVMEnvironment);
  }//runInterpreter
  
  bool MVCController::interpretByteCode(const Interpreter::CodeObject& aCodeObject,
                                        Interpreter::VirtualMachine::VMEnv& aGlbEnv,
                                        bool aReinterpretedCode)
  {
    // Run the virtual machine on the code object with the current environment
    try
    {
      // Set running virtual machine
      setRunningVM(true);
      
      auto vmEnv = pVirtualMachine->runCode(aCodeObject, aGlbEnv, aReinterpretedCode);
      
      // Update the environment with the interpreted one,
      // i.e., merge the interpreted environment into the global environment
      mergeEnvironment(vmEnv, aGlbEnv);
    }
    catch (...)
    {
      if(!pVirtualMachine->hasThrownException())
      {
        // Virtual machine stopped running
        setRunningVM(false);
        
        // Catch exceptions that virtual machine wasn't able to catch
        notifyOnError("Unable to run the statement", MVCEvent::MVCEventActor::MVC_ACTOR_CONTROLLER);
        return false;
      }
    }
    
    // Handle virtual machine excpetions if any
    auto vmException = pVirtualMachine->hasThrownException();
    if (vmException)
    {
      handleVMException(pVirtualMachine->getExceptionType(), pVirtualMachine->getExceptionValue());
    }
    
    // Virtual machine stopped running
    setRunningVM(false);
    
    // Returns true if VM didn't throw any exception
    return !vmException;
  }//interpretByteCode
  
  void MVCController::interpretByteCodeImage(const Interpreter::CodeObjectImage& aCodeImg)
  {
    // Create a copy of the global environment (first: global, second:local) for the VM
    // that will run on the bytecode image.
    // @note check for the validity of the the VMEnvironment: the caller can be a recursive
    // run of the virtual machine, e.g., executing a command "run" which needs to
    // re-interpret an object. At this point in time, the VMEnvironment is not set yet
    // (i.e., the recursive call to the virtual machine is not done yet).
    Interpreter::VirtualMachine::VMEnv tempGlbEnv = {nullptr, nullptr};
    
    // Add to the global and local names of the environment the global and local
    // names of the code image which represent the active environment at the
    // moment the code image was generated
    tempGlbEnv.first = pVMEnvironment.first ?
    std::make_shared<Interpreter::Frame::NameMap>(*(pVMEnvironment.first.get())) :
    std::make_shared<Interpreter::Frame::NameMap>();
    
    tempGlbEnv.second = pVMEnvironment.second ?
    std::make_shared<Interpreter::Frame::NameMap>(*(pVMEnvironment.second.get())) :
    std::make_shared<Interpreter::Frame::NameMap>();
    
    // Update global names
    for(const auto& it : aCodeImg.GlobalNames)
    {
      (*tempGlbEnv.first)[it.first] = it.second;
    }
    
    // Update local names
    for(const auto& it : aCodeImg.LocalNames)
    {
      (*tempGlbEnv.second)[it.first] = it.second;
    }

    // Run the virtual machine on the byte code image
    interpretByteCode(aCodeImg, tempGlbEnv, true);
    
    // Update global environment with the interpreted one.
    // Update if:
    // 1) a name from the temporary global environmant is not contained in
    //    the current global environment;
    // 2) a name from the temporary global environmant is contained in
    //    the current global environment but the global environment has
    //    the correspondent object that is not fully interpreted.
    if(!pVMEnvironment.first)
    {
      pVMEnvironment.first = tempGlbEnv.first;
    }
    else
    {
      for(const auto& it : *(tempGlbEnv.first.get()))
      {
        if( pVMEnvironment.first->find(it.first) == pVMEnvironment.first->end() ||
           (pVMEnvironment.first->find(it.first) != pVMEnvironment.first->end() &&
            !(pVMEnvironment.first->at(it.first)).isFullyInterpreted()))
        {
          pVMEnvironment.first->at(it.first) = it.second;
        }
      }
    }
    
    if(!pVMEnvironment.second)
    {
      pVMEnvironment.second = tempGlbEnv.second;
    }
    else
    {
      for(const auto& it : *(tempGlbEnv.second.get()))
      {
        if( pVMEnvironment.second->find(it.first) == pVMEnvironment.second->end() ||
           (pVMEnvironment.second->find(it.first) != pVMEnvironment.second->end() &&
            !(pVMEnvironment.second->at(it.first)).isFullyInterpreted()))
        {
          pVMEnvironment.second->at(it.first) = it.second;
        }
      }
    }
  }//interpretByteCodeImage
  
  void MVCController::updateContextVariable(const Interpreter::DataObject& aDO)
  {
    // Update the context variable according to the type of data object:
    // 1) Primitive type objects: scalar parameter
    // 2) Class objects:
    //      a) matrix: parameter
    //      b) variable: variable
    //      c) parameter: parameter
    //      d) base objectL base object
    if (Interpreter::isPrimitiveType(aDO))
    {
      Interpreter::DataObject updVar;
      
      // This previously was a singleton variable:
      // otools::createObjectVariable(pModel->getContextVarName(), DOM_SINGLETON, {aDO});
      auto objVar = otools::createScalarObject(aDO);
      updVar.setClassObject(Interpreter::BaseObjectSPtr(objVar));
      getModel()->updateCtxVariable(updVar);
    }
    else if (Interpreter::isVoidObjectType(aDO))
    {      
      Interpreter::DataObject updVar;
      updVar.setClassObject(Interpreter::BaseObjectSPtr(otools::createObject()));
      getModel()->updateCtxVariable(updVar);
    }
    else
    {
      // It must be a class object
      assert(Interpreter::isClassObjectType(aDO));
      getModel()->updateCtxVariable(aDO);
    }
  }//updateContextVariable
  
  void MVCController::getOrCreateObjectFromContext(const Interpreter::DataObject& aDO,
                                                   bool aCallBackFcn)
  {
    // This function is called as a callback function in the virtual machine upon object reporting.
    // For example, when the virtual machine is executing a "PRINT_TOP" instruction.
    // The argument "aDO" is the DataObject passed as argument to the callback function and
    // it can be an object that has been previously mapped in the model context or a
    // non-mapped data object. In the second case, assigned it to the context variable
    if (isObjectMappedIntoContext(aDO))
    {
      // If level is NONE and the given object is the result of a callback function,
      // do not print the object
      const auto ppLevel = (pModel->getModelContext()).getPrettyPrintLevel();
      if (ppLevel == MVCModelContext::PrettyPrintLevel::PP_NONE && aCallBackFcn)
      {
        return;
      }
      
      
      // Use the "ctx" function callback to print the object with the ID hold
      // by the given data object
      assert(pVirtualMachine->isInlineCallbackRegistered("ctx"));
      pVirtualMachine->getInlineCallbackFcn("ctx")({aDO});
    }
    else
    {
      // If the data object is not a get request, its content must be
      // declared into the context in the context variable.
      // I.e., the context variable must be updated with the content of the given data object
      updateContextVariable(aDO);
      
      // Always print the context variable
      if ((pModel->getModelContext()).getPrettyPrintLevel() ==
          MVCModelContext::PrettyPrintLevel::PP_NONE)
      {
        assert(pVirtualMachine->isInlineCallbackRegistered("ctx"));
        Interpreter::DataObject doCtx((pModel->getContextVarName().c_str()));
        pVirtualMachine->getInlineCallbackFcn("ctx")({ doCtx });
      }
    }
  }//getOrCreateObjectFromContext
  
  void MVCController::handleVMException(Interpreter::InterpreterException::ExceptionType aEType,
                                        const std::string& aEValue)
  {
    if(!filterErrors())
    {
      pErrorNotification = true;
      pControllerImpl->handleVMException(aEType, aEValue);
    }
  }//handleVMException
  
  void MVCController::update(const std::shared_ptr<MVCEvent>& aEvent)
  {
    pControllerImpl->update(aEvent);
  }//update
  
  void MVCController::setRunningVM(bool aIsRunning)
  {
    if(aIsRunning)
    {
      pVMRun++;
    }
    else
    {
      assert(pVMRun > 0);
      pVMRun--;
      
      if(pVMRun == 0)
      {
        // No instance of VM is currently running,
        // reset the error notification flag
        pErrorNotification = false;
      }
    }
  }//setRunningVM
  
  bool MVCController::filterErrors()
  {
    // On high verbose level print all errors
    /*
    if(pModel->getModelContext().getPrettyPrintLevel() == MVCModelContext::PrettyPrintLevel::PP_HIGH)
    {
      return false;
    }
    */
    return pVMRun > 0 && pErrorNotification;
  }//filterErrors
  
  void MVCController::notifyOnMessage(const std::string& aMsg)
  {
    pControllerImpl->sendMsgEventToView(aMsg);
  }//notifyOnMessage
  
  void MVCController::notifyOnError(const std::string& aMsg, MVCEvent::MVCEventActor aSrc)
  {
    // Source is not used in this context.
    // It is still available for future uses,
    // for example, for logging events
    (void) aSrc;
    
    // Check if this error message needs to be filtered out,
    // if not, send it to the view
    if(!filterErrors())
    {
      pErrorNotification = true;
      pControllerImpl->sendErrorEventToView(aMsg, MVCEventError::ErrorLevel::LEVEL_ERROR);
    }
  }//notifyOnError
  
  void MVCController::notifyOnWarning(const std::string& aMsg, MVCEvent::MVCEventActor aSrc)
  {
    // Source is not used in this context.
    // It is still available for future uses,
    // for example, for logging events
    (void) aSrc;
    pControllerImpl->sendErrorEventToView(aMsg, MVCEventError::ErrorLevel::LEVEL_WARNING);
  }//notifyOnWarning
  
}//end namespace MVC
