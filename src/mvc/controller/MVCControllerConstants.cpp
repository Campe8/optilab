// Self first
#include "MVCControllerConstants.hpp"

namespace MVC {
  
  namespace ModelConst {
    
    const char* COMMAND_ERR    = "Command not recognized:";
    const char* SATURATION_ERR = "Saturation occurred";
    
  }// end namespace ControllerConst
  
}// end namespace MVC
