// Self - first
#include "MVCControllerImpl.hpp"

#include "MVCController.hpp"
#include "MVCModelConstants.hpp"
#include "OutputConverterSolverSolution.hpp"
#include "PrettyPrinterTools.hpp"

#include <cassert>
#include <sstream>

namespace MVC {
  
  _private::MVCControllerImpl::MVCControllerImpl(MVCController* aController)
  : pController(aController)
  , pEventDispatcher(new MVCEventDispatcher())
  , pEventFactory(new MVCEventFactory())
  {
    assert(pController);
    
    // Register event handlers
    registerEventHandlers();
    
    // Register event handlers owned by the controller
    pController->registerEventHandlers(pEventDispatcher.get());
  }
  
  void _private::MVCControllerImpl::registerEventHandlers()
  {
    // Register handle for error event
    pEventDispatcher->registerEventHandler(std::bind(&MVCControllerImpl::handleErrorEvent, this,
                                                     std::placeholders::_1),
                                           MVCEventType::MVC_EVT_ERROR);
    
    // Register handle for solution event
    pEventDispatcher->registerEventHandler(std::bind(&MVCControllerImpl::handleSolutionEvent, this,
                                                     std::placeholders::_1),
                                           MVCEventType::MVC_EVT_SOLUTION);
    
    // Register handle for metrics event
    pEventDispatcher->registerEventHandler(std::bind(&MVCControllerImpl::handleMetricsEvent, this,
                                                     std::placeholders::_1),
                                           MVCEventType::MVC_EVT_METRICS);
  }//registerEventHandlers
  
  void _private::MVCControllerImpl::update(const std::shared_ptr<MVCEvent>& aEvent)
  {
    assert(aEvent);
    assert(aEvent->getEventDestination() == MVCEvent::MVCEventActor::MVC_ACTOR_CONTROLLER);
    
    // Dispatch event
    pEventDispatcher->dispatch(aEvent);
  }//update
  
  void _private::MVCControllerImpl::sendMsgEventToView(const std::string& aMsg)
  {
    auto event = pEventFactory->mvcEventWriteOutput({aMsg}, MVCEventFactory::EventSrcDst::EVT_C2V);
    pController->notify(std::shared_ptr<MVCEvent>(event));
  }//sendMsgEventToView
  
  void _private::MVCControllerImpl::sendErrorEventToView(const std::string& aErr,
                                                         MVCEventError::ErrorLevel aErrLevel)
  {
    switch (aErrLevel)
    {
      case MVCEventError::ErrorLevel::LEVEL_ERROR:
      {
        auto event = pEventFactory->mvcEventError(aErr, MVCEventFactory::EventSrcDst::EVT_C2V);
        pController->notify(std::shared_ptr<MVCEvent>(event));
        break;
      }
      default:
      {
        assert(aErrLevel == MVCEventError::ErrorLevel::LEVEL_WARNING);
        auto event = pEventFactory->mvcEventWarning(aErr, MVCEventFactory::EventSrcDst::EVT_C2V);
        pController->notify(std::shared_ptr<MVCEvent>(event));
        break;
      }
    }
  }//sendErrorEventToView
  
  void _private::MVCControllerImpl::handleMetricsEvent(MVCEvent* aEvent)
  {
    assert(aEvent && MVCEventMetrics::isa(aEvent));
    
    MVCEventMetrics* event = MVCEventMetrics::cast(aEvent);
    
    // Get the pointer to the metrics object and send it to the string stream
    std::stringstream ss;
    PrettyPrinter::Tools::toStream((event->getMetrics()).get(), ss);
    
    auto metricsString = ss.str();
    if (metricsString.empty()) return;
    
    // Send the string to the view
    sendMsgEventToView(std::string("\n") + ModelMetrics::METRICS + "\n" + metricsString);
  }//handleMetricsEvent
  
  void _private::MVCControllerImpl::handleErrorEvent(MVCEvent* aEvent)
  {
    assert(aEvent && MVCEventError::isa(aEvent) &&
           aEvent->getEventDestination() == MVCEvent::MVCEventActor::MVC_ACTOR_CONTROLLER);
    
    MVCEventError* event = MVCEventError::cast(aEvent);
    
    // Forward event to view
    sendErrorEventToView(event->what(), MVCEventError::ErrorLevel::LEVEL_ERROR);
  }//handleErrorEvent
  
  void _private::MVCControllerImpl::handleSolutionEvent(MVCEvent* aEvent)
  {
    auto solEvent = MVCEventSolution::cast(aEvent);
    assert(solEvent);
    
    /// Get the list of solutons
    const auto& solReg = solEvent->getSolutionList();
    if (solReg.empty())
    {
      assert(solEvent->getNumSolutions() > 0);
      
      // Send notification on the number of solutions if the solution list is empty.
      // For example, the user may have disabled the output
      // MVC_C_NUM_SOL_STR(std::to_string(solEvent->getNumSolutions()))
      auto event = pEventFactory->mvcEventWriteOutput({ ModelConst::MODEL_SATISFIED },
                                                      MVCEventFactory::EventSrcDst::EVT_C2V);
      pController->notify(std::shared_ptr<MVCEvent>(event));
      return;
    }
    
    auto event = pEventFactory->mvcEventWriteOutput(solReg, MVCEventFactory::EventSrcDst::EVT_C2V);
    pController->notify(std::shared_ptr<MVCEvent>(event));
  }//evaluateSolutionEvent
  
  void _private::MVCControllerImpl::handleVMException(Interpreter::InterpreterException::ExceptionType aEType,
                                                      const std::string& aEValue)
  {
    std::string errMsg;
    switch (aEType)
    {
      case Interpreter::InterpreterException::ExceptionType::ET_FCN_UNDEF:
        errMsg = "Undefined function " + aEValue;
        break;
      case Interpreter::InterpreterException::ExceptionType::ET_FCN_ERR:
        errMsg = "Unable to run " + aEValue;
        break;
      case Interpreter::InterpreterException::ExceptionType::ET_NON_INTERP:
        errMsg = "Invalid argument " + aEValue;
        break;
      case Interpreter::InterpreterException::ExceptionType::ET_OUT_OF_BOUND:
        errMsg = "Out of bound indexing " + aEValue;
        break;
      case Interpreter::InterpreterException::ExceptionType::ET_BAD_SUBSCR:
        errMsg = "Object does not have a subscript operator " + aEValue;
        break;
      case Interpreter::InterpreterException::ExceptionType::ET_WRONG_TYPE:
        errMsg = "Non supported types " + aEValue;
        break;
      case Interpreter::InterpreterException::ExceptionType::ET_WRONG_TYPE_OBJ:
        errMsg = "Not an object";
        break;
      case Interpreter::InterpreterException::ExceptionType::ET_PROP_NOT_FOUND:
        errMsg = "Property " + aEValue + " not available";
        break;
      case Interpreter::InterpreterException::ExceptionType::ET_PROP_INVALID_KEY:
        errMsg = "Invalid property \"" + aEValue + "\"";
        break;
      case Interpreter::InterpreterException::ExceptionType::ET_PROP_CONST:
        errMsg = "Property \"" + aEValue + "\" cannot be modified";
        break;
      case Interpreter::InterpreterException::ExceptionType::ET_FEATURE_OFF:
        errMsg = "Unavailable feature";
        break;
      case Interpreter::InterpreterException::ExceptionType::ET_INVALID_INTERP:
        errMsg = "Object\n\t" + aEValue + "\ncannot be interpreted";
        break;
      case Interpreter::InterpreterException::ExceptionType::ET_LOGIC_ERROR:
        errMsg = aEValue;
        break;
      case Interpreter::InterpreterException::ExceptionType::ET_BAD_ALLOC:
        errMsg = "== Bad Alloc ==";
        break;
      default:
        assert(aEType == Interpreter::InterpreterException::ExceptionType::ET_UNSPEC);
        errMsg = "Unable to run the statement " + aEValue;
        break;
    }
    
    // Send error message to the view
    sendErrorEventToView(errMsg, MVCEventError::ErrorLevel::LEVEL_ERROR);
  }//handleVMException
  
}//end namespace MVC
