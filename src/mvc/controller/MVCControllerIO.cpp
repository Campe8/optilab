// Base class
#include "MVCControllerIO.hpp"

#include "MVCConstants.hpp"
#include "MVCDefs.hpp"
#include "CLIUtils.hpp"

#include <algorithm>
#include <cassert>

namespace MVC {
  
  static bool isSkipLine(const std::string& aString)
  {
    if (aString.empty())     return true;
    if (aString.size() == 1) return false;
    
    // Skip single line comments
    if (aString.substr(0, 2) == std::string(MVC::Constants::View::COMMENT_SYMB))
    {
      return true;
    }
    
    return false;
  }//isSkipLine
  
  static bool isExitCmd(const std::string& aString)
  {
    static std::string exitCmd(MVC::Constants::View::EXIT_COMMAND);
    return aString.substr(0, exitCmd.size()) == exitCmd;
  }//isExitCmd
  
  // Returns true if the given statement is the beginning of a multi-line statement,
  // i.e., a statement that is declared on multiple lines.
  // For example, the following begin a multi-line statatement:
  // - for i in 1..2:
  // - while(i < 5):
  // - x:[1, 2, 3, ...
  // - fcn(a, b, ...
  static bool beginOfMultiLineStmt(const std::string& aStmt)
  {
    // Look for begging of block statements,
    // e.g., for, while, etc.
    for(const auto& blk : BlockOpeningStmtSet)
    {
      // Return true if the string contains an opening block statement
      if(aStmt.find(blk) != std::string::npos) return true;
    }
    
    // Look for open square bracket or open parenthesis,
    // e.g., x:[1, 2, ...
    for(const auto& par : MultiLineOpeningStmtSet)
    {
      // Return true if the string contains an opening block statement
      if(aStmt.find(par) != std::string::npos) return true;
    }
    
    return false;
  }//beginOfMultiLineStmt
  
  // Returns true if the given statement represents the end of a multi-line statement.
  // For example, the following terminate a multi-line statatement:
  // - end
  // - ..., 4, 5]
  // - ..., c, d)
  static bool endOfMultiLineStmt(const std::string& aStmt)
  {
    // Look for end of block statements
    for(const auto& blk : BlockClosingStmtSet)
    {
      // Return true if the string contains an opening block statement
      if(aStmt.find(blk) != std::string::npos) return true;
    }
    
    // Look for close square bracket or close parenthesis
    for(const auto& par : MultiLineClosingStmtSet)
    {
      // Return true if the string contains an opening block statement
      if(aStmt.find(par) != std::string::npos) return true;
    }
    
    return false;
  }//endOfMultiLineStmt
  
  static std::size_t numBlockOpeningStmts(const std::string& aStmt)
  {
    std::size_t ctr{0};
    for(const auto& blk : BlockOpeningStmtSet)
    {
      ctr += CLI::Utils::numSubStrInString(aStmt, blk);
    }
    return ctr;
  }//numBlockOpeningStmts
  
  static std::size_t numMultiLineOpeningStmts(const std::string& aStmt)
  {
    std::size_t ctr{0};
    for(const auto& par : MultiLineOpeningStmtSet)
    {
      ctr += CLI::Utils::numSubStrInString(aStmt, par);
    }
    return ctr;
  }//numMultiLineOpeningStmts
  
  static std::size_t numBlockClosingStmts(const std::string& aStmt)
  {
    std::size_t ctr{0};
    for(const auto& blk : BlockClosingStmtSet)
    {
      ctr += CLI::Utils::numSubStrInString(aStmt, blk);
    }
    return ctr;
  }//numBlockClosingStmts
  
  static std::size_t numMultiLineClosingStmts(const std::string& aStmt)
  {
    std::size_t ctr{0};
    for(const auto& par : MultiLineClosingStmtSet)
    {
      ctr += CLI::Utils::numSubStrInString(aStmt, par);
    }
    return ctr;
  }//numMultiLineClosingStmts
  
  MVCControllerIO::MVCControllerIO(const std::shared_ptr<MVCView>& aView, const std::shared_ptr<MVCModel>& aModel)
  : MVCController(aView, aModel)
  , pPromptSymb(MVC::Constants::View::PROMPT_SYMB)
  , pPromptMultilineSymb(MVC::Constants::View::PROMPT_MULTILINE_SYMB)
  , pTerminate(false)
  {
  }
  
  bool MVCControllerIO::exit() const
  {
    return pTerminate;
  }//exit

  void MVCControllerIO::step()
  {
    // Main loop:
    //  read input 
    //      |      
    // process input
    //      |       
    // write output

    // Ask view to read next input
    readInput();
    
    // Process input
    processInput();
    
    // Ask view to dump the output
    writeOutput();
  }

  void MVCControllerIO::exe()
  {
    // Initialize the view
    getView()->initializeView();
    
    while (!exit())
    {
      step();
    }
  }//exe
  
  void MVCControllerIO::clearInBuffer()
  {
    pInBuffer.clear();
  }//clearLine
  
  void MVCControllerIO::readInput()
  {
    // Get next input line from the view
    const auto& inLine = getView()->getNextInputLine(pPromptSymb);
    if (beginOfMultiLineStmt(inLine))
    {
      // Collect the set of lines composing the multi-line statement:
      // keep a counter of opening and closing statements and when
      // when the counter reaches zero, the multi-line statement is complete.
      // While the counter is greater than zero, any new line is part of
      // the multi-line statement.
      // Initialize the multi-line statement with the first line
      std::string multiLine = inLine;
      if(multiLine.back() == ';') multiLine.pop_back();
      
      auto openings = numBlockOpeningStmts(multiLine) + numMultiLineOpeningStmts(multiLine);
      auto closing  = numBlockClosingStmts(multiLine) + numMultiLineClosingStmts(multiLine);
      if (closing > openings)
      {
        notifyOnError("Invalid syntax", MVCEvent::MVCEventActor::MVC_ACTOR_CONTROLLER);
        return;
      }
      auto multiLineCtr = openings - closing;
      
      // Loop and get new input lines until the counter reaches zero
      std::string promptPrefix(pPromptSymb.size(), ' ');
      while (multiLineCtr)
      {
        // Get next input line.
        std::string promptMultiLine = promptPrefix;
        for (int rep = 0; rep < multiLineCtr; ++rep)
        {
          promptMultiLine += pPromptMultilineSymb;
        }
        auto nextLine = getView()->getNextInputLine(promptMultiLine);
        
        // Check again if next input line is a nestested multi-line statement
        if(beginOfMultiLineStmt(nextLine))
        {
          if(nextLine.back() == ';') nextLine.pop_back();
          
          // Increase the counter of opened multi-line statements
          multiLineCtr += numBlockOpeningStmts(nextLine) + numMultiLineOpeningStmts(nextLine);
        }
        
        // Check is the same line is also a closing multi-line statement
        if(endOfMultiLineStmt(nextLine))
        {
          // Open multi-line statement counter must be greater than zero
          assert(multiLineCtr > 0);
          if(nextLine.back() == ';') nextLine.pop_back();
          if(nextLine.back() != '\n') nextLine += '\n';
          
          // Decrease the counter of opened multi-line statements
          //closing  = numBlockClosingStmts(multiLine) + numMultiLineClosingStmts(multiLine);
          closing  = numBlockClosingStmts(nextLine) + numMultiLineClosingStmts(nextLine);
          if(closing > multiLineCtr)
          {
            notifyOnError("Invalid syntax", MVCEvent::MVCEventActor::MVC_ACTOR_CONTROLLER);
            return;
          }
          multiLineCtr -= closing;
        }
        
        // Add next line to the multi-line statement
        if(multiLine.back() != '\n') multiLine += '\n';
        multiLine += nextLine;
      }//while
      
      // Store the multi-line statement into the input buffer
      if(multiLine.back() != '\n') multiLine += '\n';
      pInBuffer.push_back(multiLine);
    }
    else
    {
      // Not a multi-line statement:
      // store it into the input buffer
      pInBuffer = CLI::Utils::tokenizeStringOnSymbol(inLine, ';');
      for(auto& stmt : pInBuffer)
      {
        if(stmt.back() != ';') stmt.push_back(';');
        if(stmt.back() != '\n') stmt.push_back('\n');
      }//for
    }
  }//readInput
  
  void MVCControllerIO::processInput()
  {
    //Execute each statement of the input buffer
    for(const auto& inStmt : pInBuffer)
    {
      if (isExitCmd(inStmt))
      {
        clearInBuffer();
        pTerminate = true;
        return;
      }
      
      // Check whether to skip line or not
      if (inStmt.empty() || isSkipLine(inStmt)) continue;
      
      processStmt(inStmt);
    }
    
    clearInBuffer();
  }//processInput
  
  void MVCControllerIO::writeOutput()
  {
    getView()->writeOutStream();
  }//writeOutput
  
}//end namespace MVC
