// Base class
#include "MVCModel.hpp"

#include "OPCode.hpp"

#include "Timer.hpp"
#include "MetricsManager.hpp"
#include "MVCUtils.hpp"
#include "ObjectHelper.hpp"
#include "IRException.hpp"
#include "ModelMacro.hpp"
#include "MVCMacro.hpp"
#include "MVCUtils.hpp"
#include "OutputConverterSolverSolution.hpp"
#include "CLIUtils.hpp"
#include "MVCModelConstants.hpp"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <vector>
#include <utility>
#include <algorithm>
#include <cassert>
#include <stdexcept>  // for std::runtime_error

#define ASSERT_API_NO_ERROR(err)            \
do                                          \
{                                           \
if (err != Solver::SolverApi::SolverError::kNoError)      \
throw std::runtime_error("runtime error on engine API");  \
} while (0)

//#define IR_DEBUG

namespace MVC {
  
  static std::string getNonInterpretedDataObject(const Interpreter::DataObject& aDO)
  {
    if(aDO.isComposite())
    {
      std::string arrayID;
      for(const auto& arg : aDO)
      {
        if(arg.isFullyInterpreted()) continue;
        auto ID = getNonInterpretedDataObject(arg);
        if(ID.empty()) continue;
        arrayID += ID + ", ";
      }
      if(!arrayID.empty()) arrayID.resize(arrayID.size() - 2);
      return arrayID;
    }
    else if (!aDO.isFullyInterpreted())
    {
      return Utils::getIDFromDataObject(aDO);
    }
    
    return "";
  }//getNonInterpretedDataObject
  
  static std::string getNonInterpretedObjectID(const MVCModelContext::CtxObjSPtr& aCtxObj)
  {
    const auto& payload = aCtxObj->payload();
    if(payload.isClassObject() &&
       Interpreter::ObjectTools::isVariableObject(payload.getClassObject().get()))
    {
      return aCtxObj->getID();
    }
    
    // All other cases, return the non interpreted data object
    return getNonInterpretedDataObject(payload);
  }//getNonInterpretedObjectID
  
  MVCModel::MVCModel()
  : pCtxVarName(ModelConst::CTX_VARIABLE_NAME)
  , pCtxVarKey(MVCModelContext::contextKeyInf)
  , pCompileTimeMsec(0)
  , pSearchTimeMsec(0)
  , pTransformEngine(new IR::Transforms::TransformEngine)
  , pModelParser(new Model::ModelParser())
  , pModelGenerator(new Model::ModelGenerator())
  , pSolverApi(new Solver::SolverApi())
  , pCallbackHandler(new Solver::ModelCallbackHandler())
  , pEventFactory(new MVCEventFactory())
  {
    pSolverApi->setEngineHandler(pCallbackHandler);
  }
  
  void MVCModel::collectMetrics(const Model::ModelSPtr& aModel)
  {
    using namespace Base::Tools;
    
    // Return if metrics for this model are not enabled
    if (!aModel->getMetaInfo()->isMetricsEnabled()) return;
    
    auto metricsRegister = std::make_shared<MetricsRegister>();
    
    // Add the metrics to the register
    metricsRegister->addMetric(ModelMetrics::COMPILER_TIME, pCompileTimeMsec);
    metricsRegister->addMetric(ModelMetrics::SEARCH_TIME, pSearchTimeMsec);
    metricsRegister->addMetric(ModelMetrics::WALLCLOCK_TIME, pCompileTimeMsec + pSearchTimeMsec);
    
    // Add the metrics to the manager
    
    // Get the manager and cleanup existing metrics
    auto& metManager = MetricsManager::getInstance();
    metManager.clearMetrics();
    
    // Time metrics
    if (aModel->getMetaInfo()->
        isMetricCategoryEnabled(MetricsManager::MetricsCategory::TIME_METRICS))
    {
      if (!metManager.isCategoryEnabled(MetricsManager::MetricsCategory::TIME_METRICS))
      {
        // Enable time metrics if not already enabled
        metManager.enableMetricCategory(MetricsManager::MetricsCategory::TIME_METRICS, true);
      }
      metManager.addMetricsOnCategory(MetricsManager::MetricsCategory::TIME_METRICS,
                                      ModelMetrics::TIME_METRICS, metricsRegister);
    }
    
    // Search metrics
    if (aModel->getMetaInfo()->
        isMetricCategoryEnabled(MetricsManager::MetricsCategory::SEARCH_METRICS))
    {
      // Register search specific metrics
      if (!metManager.isCategoryEnabled(MetricsManager::MetricsCategory::SEARCH_METRICS))
      {
        // Enable time metrics if not already enabled
        metManager.enableMetricCategory(MetricsManager::MetricsCategory::SEARCH_METRICS, true);
      }
      
      for (const auto& mets : pCallbackHandler->getMetrics())
      {
        metManager.addMetricsOnCategory(MetricsManager::MetricsCategory::SEARCH_METRICS,
                                        mets.first, mets.second);
      }
    }
  }//collectMetrics
  
  void MVCModel::updateCtxVariable(const Interpreter::DataObject& aDO)
  {
    assert(Interpreter::isClassObjectType(aDO));
    addObjectToContext(pCtxVarName, aDO);
    
    if(pCtxVarKey == MVCModelContext::contextKeyInf)
    {
      // If the context variable wasn't already present in the context, store its key
      pCtxVarKey = pModelContext.getContextKeyFromID(pCtxVarName);
    }
  }//updateCtxVariable
  
  void MVCModel::clearModelContext()
  {
    pCtxVarKey = MVCModelContext::contextKeyInf;
    pModelContext.clear(PassKey<MVCModel>());
  }//clearModelContext
  
  void MVCModel::eraseContextObject(MVCModelContext::MdlCtxObjKey aKey)
  {
    if(aKey == pCtxVarKey)
    {
      pCtxVarKey = MVCModelContext::contextKeyInf;
    }
    pModelContext.erase(PassKey<MVCModel>(), aKey);
  }//eraseContextObject
  
  void MVCModel::addObjectToContext(const std::string& aName, const Interpreter::DataObject& aDO)
  {
    // Add a named object to the context
    auto keyType = pModelContext.addNamedObjectToContext(PassKey<MVCModel>(), aName, aDO);
    
    // Send a string representation of the object just added to the view
    printContextObject(keyType.first);
    
    if(keyType.second == Model::ContextObject::CTX_OBJ_SRC)
    {
      pSrcCtxKeyList.push_back(keyType.first);
    }
  }//addObjectToContext
  
  void MVCModel::addObjectToContext(const Interpreter::DataObject& aDO)
  {
    // Add a named object to the context
    auto keyType = pModelContext.addUnnamedObjectToContext(PassKey<MVCModel>(), aDO);
    
    // Send a string representation of the object just added to the view
    //printContextObject(keyType.first);
    
    if(keyType.second == Model::ContextObject::CTX_OBJ_SRC)
    {
      pSrcCtxKeyList.push_back(keyType.first);
    }
  }//addObjectToContext
  
  void MVCModel::printContextObject(MVCModelContext::MdlCtxObjKey aKey)
  {
    std::stringstream ss;
    pModelContext.prettyPrint(aKey, ss);
    
    // Notify view about new context object
    auto event = pEventFactory->mvcEventWriteOutput({ ss.str() }, MVCEventFactory::EventSrcDst::EVT_M2V);
    notify(std::shared_ptr<MVCEvent>(event));
  }//printContextObject
  
  void MVCModel::interpretModelContext()
  {
    // No need to re-interpret the context if it is already
    // fully interpreted
    if (pModelContext.isModelContextFullyInterpreted()) return;
    
    // Set off verbose
    auto oldPP = pModelContext.setPrettyPrintLevel(MVCModelContext::PrettyPrintLevel::PP_NONE);
    
    // Interpret all the object that are not fully interpreted
    auto notInterpList = pModelContext.getListOfNotFullyInterpretedContextObjects();
    for(auto& key : notInterpList)
    {
      assert(key != MVCModelContext::contextKeyInf);
      interpretContextObject(key);
    }
    
    // Reset verbose as it was before
    pModelContext.setPrettyPrintLevel(oldPP);
  }//interpretModelContext
  
  void MVCModel::interpretContextObject(MVCModelContext::MdlCtxObjKey aKey)
  {
    // Return if the object doesn't need interpretation
    if (pModelContext.isContextObjectFullyInterpreted(aKey)) return;
    
    // Return if there is no object mapped to the given key
    auto ctxObj = pModelContext.lookup(PassKey<MVCModel>(), aKey);
    if (!ctxObj) return;
    
    std::string str("Not fully Interpreted");
    
    // Delete context object since it will be re-interpreted and re-inserted by
    // the callbacks again.
    pModelContext.erase(PassKey<MVCModel>(), aKey);
    
    // Send a notification to the controller to re-interpret the given DataObject
    auto event = pEventFactory->mvcEventEvalCodeObject(ctxObj->payload(),
                                                       MVCEventFactory::EventSrcDst::EVT_M2C);
    notify(std::shared_ptr<MVCEvent>(event));
  }//interpretContextObject
  
  Model::ModelContext MVCModel::getModelCtx()
  {
    Model::ModelContext mctx;
    
    // Add all the context objects into the model context
    for(const auto& it : pModelContext)
    {
      mctx.addContextObject(it.second);
    }
    
    return mctx;
  }//getModelCtx
  
  Model::ModelSPtr MVCModel::generateModel()
  {
    // For the current version (12/3/18) re-interpretation is disabled
    assert(pModelContext.isModelContextFullyInterpreted());
    
    if (!pModelContext.isModelContextFullyInterpreted())
    {
      interpretModelContext();
    }
    
    // Throw error message if after re-interpretation, before generating the model,
    // the context is still not fully interpreted
    if (!pModelContext.isModelContextFullyInterpreted())
    {
      auto notInterpList = pModelContext.getListOfNotFullyInterpretedContextObjects();
      
      std::string errMsg{ "Model context not fully interpreted on\n" };
      for(auto& key : notInterpList)
      {
        assert(key != MVCModelContext::contextKeyInf);
        auto ctxObj = pModelContext.lookup(PassKey<MVCModel>(), key);
        errMsg += "- " + getNonInterpretedObjectID(ctxObj) + "\n";
      }
      errMsg.pop_back();
      
      // Send informative error message
      mvc_assert_msg(false, errMsg);
    }
    
    // Start the timer keeping track of the compiler time
    Base::Tools::Timer timer;
    
    // Call the parser to parse the context and generate IR
    IR::IRContextSPtr IRCtx{nullptr};
    try
    {
      IRCtx = pModelParser->parse(getModelCtx());
    }
    catch(Model::ModelException& me)
    {
      throw;
    }
    catch(...)
    {
      throw std::logic_error("Error in compiling the model");
    }
    assert(IRCtx);
    
#ifdef IR_DEBUG
    pTransformEngine->setDebug(true);
#endif
     
    // Run the transform chain on the model
    pTransformEngine->setContext(IRCtx);
    
    try
    {
      pTransformEngine->runTransforms();
    }
    catch(ir_exception& e)
    {
      auto info = ir_get_exception_info(e);
      throw std::logic_error(info.c_str());
    }
    
    // Generate model from IR
    Model::ModelSPtr model{nullptr};
    try
    {
      model = pModelGenerator->generateModel(IRCtx);
    }
    catch (MVC::MVCException& e)
    {
      throw;
    }
    catch(std::logic_error& e)
    {
      mvc_assert_msg(false, e.what());
    }
    catch(std::out_of_range& e)
    {
      mvc_assert_msg(false, e.what());
    }
    catch(...)
    {
      throw std::logic_error("Unable to build the model");
    }
    assert(model);
    
    // Get the timer value
    pCompileTimeMsec = timer.getWallClockTime();
    
    return model;
  }//generateModel
  
  std::vector<MVCModelContext::CtxObjSPtr>
  MVCModel::getContextObjectListFromKeysList(const std::vector<MVCModelContext::MdlCtxObjKey>& aKyes)
  {
    std::vector<MVCModelContext::CtxObjSPtr> objList;
    
    auto passKey = PassKey<MVCModel>();
    for (auto& key : aKyes)
    {
      objList.push_back(pModelContext.lookup(passKey, key));
    }
    return objList;
  }//getContextObjectListFromKeysList
  
  MVCModelContext::CtxObjSPtr
  MVCModel::getContextObjectListFromKey(const MVCModelContext::MdlCtxObjKey& aKey)
  {
    auto passKey = PassKey<MVCModel>();
    return pModelContext.lookup(passKey, aKey);
  }// getContextObjectListFromKey
  
  void MVCModel::updateModelContext(const Model::ModelSPtr& aModel,
                                    const Solver::ModelCallbackHandler::Solution& aSol)
  {
    // Step 1: replace all domains of the variable and collect the list of singleton variables
    std::vector<std::string> singletonVarIDs;
    for (const auto& varDomPair : aSol)
    {
      // Get the context object "variable" corresponding to the variable id in the
      // solution element pair
      const auto key = pModelContext.getContextKeyFromID(varDomPair.first);
      const auto& varObj = getContextObjectListFromKey(key);
      
      // Replace the domain of the variable with the domain specified in the solution
      if (Utils::replaceDomainInVarCtxObj(varObj, varDomPair.second))
      {
        singletonVarIDs.push_back(varDomPair.first);
      }
    }
    
    // Step 2: remove all branching declarations from the context
    removeBranchDeclsFromContext();
    
    
    // Step 3: collect all the constraints which scope is ground.
    // This set of constraints can be removed from the context
    std::vector<MVCModelContext::MdlCtxObjKey> keyList;
    for(auto& it : *(aModel->getIDConMap()))
    {
      std::size_t groundVars{0};
      auto scopeSize = it.second->scopeSize();
      for(std::size_t idx{0}; idx < it.second->scopeSize(); ++idx)
      {
        auto varID = (it.second)->getScopeVariableNameId(idx);
        if (std::find(singletonVarIDs.begin(), singletonVarIDs.end(), varID) ==
            singletonVarIDs.end()) continue;
        groundVars++;
      }
      
      // Check if all variables in the scope are ground variables,
      // if so set this constraint as a candidate for removal from the context
      if(groundVars == scopeSize)
      {
        auto key = pModelContext.getContextKeyFromID(it.first);
        assert(key != CtxObjectCollector::contextKeyInf);
        keyList.push_back(key);
      }
    }
    
    // Step 4: delete from the context all the constraints with ground scope
    auto passKey = PassKey<MVCModel>();
    for(auto& key : keyList)
    {
      pModelContext.erase(passKey, key);
    }
  }// updateModelContext
  
  void MVCModel::postSolvingEvaluationNoSolutions(const Model::ModelSPtr& aModel)
  {
    // There can be different reasons why the number of solutions is 0:
    // 1) unsatisfiable model (including no variables but unsat constraints);
    // 2) no variables;
    // 3) etc.
    // In any case, clear the branch declarations from the context to prepare it for next run
    removeBranchDeclsFromContext();
    
    if (aModel->numModelObjects(Model::ModelObjectClass::MODEL_OBJ_CONSTRAINT) == 0 ||
                                pCallbackHandler->satisfied())
    {
      // No constraint or constraints but satisfied model.
      // For example 2 == 2 is always satisfied
      auto event = pEventFactory->mvcEventWriteOutput({ ModelConst::MODEL_SATISFIED },
                                                      MVCEventFactory::EventSrcDst::EVT_M2V);
      notify(std::shared_ptr<MVCEvent>(event));
    }
    else
    {
      // Constraints > 0 but not satisfied.
      // For example, 2 !== 2 is never satisfied
      auto event = pEventFactory->mvcEventNoSolution(MVCEventFactory::EventSrcDst::EVT_M2V);
      notify(std::shared_ptr<MVCEvent>(event));
    }
  }//postSolvingEvaluationNoSolutions
  
  void MVCModel::postSolvingEvaluationSolutions(const Model::ModelSPtr& aModel)
  {
    const auto numSol = pCallbackHandler->numSolutions();
    assert(numSol > 0);
    
    // Solution register and converter
    std::list<std::string> solutionRegister;
    Solver::OutputConverterSolverSolution ocs;
    
    // Iterate over all solutions and notify the controller on each solution
    std::size_t solutionCtr{1};
    for (std::size_t sol{0}; sol < numSol; ++sol)
    {
      const auto& solution = pCallbackHandler->getSolution(sol);
      
      // Update the context with the first found solution
      if (sol == 0)
      {
        // Update model context on first solution found
        updateModelContext(aModel, solution);
      }
      
      // Continue if current solution is not an output solution
      if (!ocs.isOutputSolution(aModel, solution)) continue;
      
      // Convert solution into a string stream
      std::stringstream ss;
      ocs.ostreamSolution(ss, aModel, solution);
      
      // Register the solution
      auto currIdxStr = CLI::Utils::toString<std::size_t>(solutionCtr++);
      solutionRegister.push_back(MVC_C_CUR_SOL_STR(currIdxStr));
      solutionRegister.push_back(ss.str());
    }
    
    // Create an event solution with the register of solutions to send to the controller
    auto event = pEventFactory->mvcEventSolution(numSol, solutionRegister,
                                                 MVCEventFactory::EventSrcDst::EVT_M2C);
    notify(std::shared_ptr<MVCEvent>(event));
  }//postSolvingEvaluationSolutions
  
  void MVCModel::postSolvingEvaluation(const Model::ModelSPtr& aModel)
  {
    // Evaluate solutions
    if (pCallbackHandler->numSolutions() == 0)
    {
      postSolvingEvaluationNoSolutions(aModel);
    }
    else
    {
      postSolvingEvaluationSolutions(aModel);
    }
    
    // Collect metrics
    collectMetrics(aModel);
    
    // Send metrics to view
    notify(std::shared_ptr<MVCEvent>(pEventFactory->mvcEventMetrics()));
  }//postSolvingEvaluation
  
  void MVCModel::removeBranchDeclsFromContext()
  {
    auto passKey = PassKey<MVCModel>();
    for(auto& key : pSrcCtxKeyList)
    {
      pModelContext.erase(passKey, key);
    }
    pSrcCtxKeyList.clear();
  }//removeBranchDeclsFromContext
  
  void MVCModel::solveModel(const Model::ModelSPtr& aModel)
  {
    // Register the model into the solver
    assert(aModel);
  
    // Create a new engine
    const auto engineId = aModel->getMetaInfo()->modelName();
    
    auto err = Solver::SolverApi::SolverError::kNoError;
    err = pSolverApi->createEngine(engineId);
    ASSERT_API_NO_ERROR(err);
    
    // Load the model into the engine
    err = pSolverApi->loadModel(engineId, aModel);
    ASSERT_API_NO_ERROR(err);
    
    err = pSolverApi->engineWait(engineId, -1);
    ASSERT_API_NO_ERROR(err);

    // Start the timer keeping track of the search time
    Base::Tools::Timer timer;
    
    // Run the loaded model
    err = pSolverApi->runModel(engineId);
    ASSERT_API_NO_ERROR(err);
    
    err = pSolverApi->engineWait(engineId, -1);
    ASSERT_API_NO_ERROR(err);
    
    // Get the time spent during search
    pSearchTimeMsec = timer.getWallClockTime();
    
    // Collect all solutions
    err = pSolverApi->collectSolutions(engineId, aModel->getMetaInfo()->solutionLimit());
    ASSERT_API_NO_ERROR(err);
    
    // Run post solving evaluation
    postSolvingEvaluation(aModel);
    
    
    /*
     StdOutCallbackHandler::SPtr handler = std::make_shared<StdOutCallbackHandler>();
     pSolverApi->setEngineHandler(handler);
     
     const std::string engineId = "EngineId";
     pSolverApi->createEngine(engineId);
     pSolverApi->loadModel(engineId, pModel);
     pSolverApi->engineWait(engineId, -1);
     pSolverApi->runModel(engineId);
     pSolverApi->engineWait(engineId, -1);
     pSolverApi->collectSolutions(engineId, 1);
     */
  }//solveModel
  
}//end namespace MVC
