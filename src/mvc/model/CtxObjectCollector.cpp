// Self first
#include "CtxObjectCollector.hpp"

#include <algorithm>
#include <cassert>
#include <stdexcept> 

namespace MVC {
  
	CtxObjectCollector::MdlCtxObjKey CtxObjectCollector::contextKeyInf = 0;

	CtxObjectCollector::CtxObjectCollector()
	{
		// Reserve memory for at least 20 elements
		// and set a dummy element as first element
		pRegister.reserve(20);
		pRegister.push_back({ 0, "" });
	}

	void CtxObjectCollector::clear()
	{
		pRegister.erase(pRegister.begin() + 1, pRegister.end());
    pReverseRegister.clear();
    pFreeKeySet.clear();
	}//clear

	CtxObjectCollector::MdlCtxObjKey CtxObjectCollector::getUnusedKey() const
	{
		// Check if the free set contains an unused key
		// if so return it
		if (!pFreeKeySet.empty())
		{
			return pFreeKeySet.back();
		}

		// Returns a key that has not been used yet
		return pRegister.size();
	}//getUnusedKey

	CtxObjectCollector::MdlCtxType& CtxObjectCollector::lookupType(MdlCtxObjKey aKey)
	{
		assert(aKey < pRegister.size());
		return pRegister[aKey].first;
	}//lookupType

	CtxObjectCollector::MdlCtxID& CtxObjectCollector::lookupID(MdlCtxObjKey aKey)
	{
		assert(aKey < pRegister.size());
		return pRegister[aKey].second;
	}//lookupID

  std::vector<CtxObjectCollector::MdlCtxObjKey> CtxObjectCollector::getSetOfMappedKeys() const
  {
    std::vector<MdlCtxObjKey> usedKeys;
    for(MdlCtxObjKey key{1}; key < pRegister.size(); ++key)
    {
      if(!lookup(key)) continue;
      usedKeys.push_back(key);
    }
    return usedKeys;
  }//getSetOfMappedKeys
  
	bool CtxObjectCollector::lookup(MdlCtxObjKey aKey) const
	{
		if (aKey >= pRegister.size() || std::find(pFreeKeySet.begin(), pFreeKeySet.end(), aKey) != pFreeKeySet.end())
		{
			return false;
		}
		return true;
	}//lookup

	void CtxObjectCollector::mapCtxKey(MdlCtxObjKey aKey, MdlCtxType aType, const std::string& aID)
	{
		// Map type and ID
		assert(aKey > 0);
		assert(aKey <= pRegister.size());
		if (aKey == pRegister.size())
		{
			pRegister.push_back({ aType, aID });
		}
		else
		{
			lookupType(aKey) = aType;
			lookupID(aKey) = aID;
		}

		// Map ID on reverse lookup map
		pReverseRegister[aID] = aKey;

		// If the key was in the set of free keys,
		// remove it since it has just been used
		auto it = std::find(pFreeKeySet.begin(), pFreeKeySet.end(), aKey);
		if (it != pFreeKeySet.end())
		{
			pFreeKeySet.erase(it);
		}
	}//mapCtxKey

	void CtxObjectCollector::unmapCtxKey(MdlCtxObjKey aKey)
	{
		// If the key is not registered, return
		if (!lookup(aKey)) return;

    // Remove ID from reverse map
    assert(pReverseRegister.find(lookupID(aKey)) != pReverseRegister.end());
    pReverseRegister.erase(lookupID(aKey));
    
		// Set the key as unused
		auto it = std::find(pFreeKeySet.begin(), pFreeKeySet.end(), aKey);
		if (it == pFreeKeySet.end())
		{
			pFreeKeySet.push_back(aKey);
		}
	}//unmapCtxKey

	CtxObjectCollector::MdlCtxType CtxObjectCollector::lookupTypeByKey(MdlCtxObjKey aKey) const
	{
		if (!lookup(aKey))
		{
			throw std::out_of_range("CtxKey not found");
		}
		return pRegister.at(aKey).first;
	}//lookupTypeByKey

	CtxObjectCollector::MdlCtxID CtxObjectCollector::lookupIDByKey(MdlCtxObjKey aKey) const
	{
		if (!lookup(aKey))
		{
			throw std::out_of_range("CtxKey not found");
		}
		return pRegister.at(aKey).second;
	}//lookupIDByKey

	CtxObjectCollector::MdlCtxObjKey CtxObjectCollector::lookupKeyByID(const MdlCtxID& aID) const
	{
		auto it = pReverseRegister.find(aID);
		if(it == pReverseRegister.end() || !lookup(it->second)) return CtxObjectCollector::contextKeyInf;
		return it->second;
	}//lookupKeyByID
  
}// end namespace MVC
