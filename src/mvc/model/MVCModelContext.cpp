// Base class
#include "MVCModelContext.hpp"

#include "MVCUtils.hpp"
#include "ModelUtils.hpp"
#include "InterpreterUtils.hpp"
#include "ObjectHelper.hpp"
#include "ObjectTools.hpp"
#include "CLIUtils.hpp"

// Context objects
#include "BaseObjectContextObject.hpp"
#include "ConstraintContextObject.hpp"
#include "SearchContextObject.hpp"
#include "VariableContextObject.hpp"

// Pretty printers
#include "MCPPBaseObject.hpp"
#include "MCPPCtxObject.hpp"
#include "MCPPConstraint.hpp"
#include "MCPPVariable.hpp"

#include "MVCMacro.hpp"
#include "MVCModelConstants.hpp"
#include "ModelMacro.hpp"

#include <sstream>
#include <algorithm>
#include <cassert>

namespace cutils = CLI::Utils;
namespace otools = Interpreter::ObjectTools;

namespace MVC {
  
  MVCModelContext::MdlCtxObjKey MVCModelContext::contextKeyInf = CtxObjectCollector::contextKeyInf;
  Interpreter::BaseObject::PropertyKey MVCModelContext::MVCModelContextBaseObjectKey =
  "__MVC_model_context_base_object_key__";
  std::string MVCModelContext::MVCModelContextBaseObjectValue =
  std::string(ModelConst::CTX_VARIABLE_NAME) + "_";
  
  static bool needsLaterInterpretation(const MVCModelContext::MdlObj& aMdlObj)
  {
    return !aMdlObj.isFullyInterpreted();
  }//needsLaterInterpretation
  
  // Returns true if the given MdlObj has a class object type and the wrapped base object
  // has been already registered into the context.
  // Rreturns false otherwise
  static bool isBaseObjectAlreadyRegistered(const MVCModelContext::MdlObj& aMdlObj)
  {
    return Interpreter::isClassObjectType(aMdlObj) &&
           aMdlObj.getClassObject()->hasProperty(MVCModelContext::MVCModelContextBaseObjectKey);
  }//isBaseObjectAlreadyRegistered
  
  MVCModelContext::MVCModelContext()
  : pPrettyPrintLevel(PrettyPrintLevel::PP_NONE)
  , pVariableInputOrderCtr(0)
  , pModelContext(nullptr)
  {
    clear();
    
    // Register pretty printers
    registerPrettyPrinters();
  }
  
  std::string MVCModelContext::getUnusedNameForContextObject() const
  {
    std::stringstream ss;
    ss << MVCModelContext::MVCModelContextBaseObjectValue << getUnusedObjKey();
    return ss.str();
  }// getUnusedNameForContextObject
  
  void MVCModelContext::mapModelContextKeyIntoBaseObject(const MdlObj& aDO,
                                                         const std::string& aName)
  {
    if(!Interpreter::isClassObjectType(aDO)) return;
    
    auto obj = aDO.getClassObject().get();
    if(obj->hasProperty(MVCModelContext::MVCModelContextBaseObjectKey))
    {
      // Unmap old name
      auto oldName = otools::getObjectPropertyValue<std::string>(obj,
                                                     MVCModelContext::MVCModelContextBaseObjectKey);
      auto oldKey = pCtxObjCollector.lookupKeyByID(oldName);
      if(oldKey != CtxObjectCollector::contextKeyInf) { erase(oldKey); }
      
      // Update the value of the property to the new name
      obj->setProperty(MVCModelContext::MVCModelContextBaseObjectKey, aName.c_str());
    }
    else
    {
      // Add the property and set its value to the given name
      obj->addProperty(MVCModelContext::MVCModelContextBaseObjectKey, aName.c_str());
      obj->setPropertyVisibility(MVCModelContext::MVCModelContextBaseObjectKey, false);
    }
  }//mapModelContextKeyIntoBaseObject
  
  void MVCModelContext::resetMappings()
  {
    pReqLaterInterpList.clear();
    pCtxObjCollector.clear();
  }//resetMappings
  
  void MVCModelContext::clear()
  {
    pCtxObjCollector.clear();
    
    // Reset input order variable declaration
    pVariableInputOrderCtr = 0;

    // Generate default model context and reset the interpreter
    pModelContext.reset(new CtxObjMap());
    
    // Clear maps
    resetMappings();
    
    // Reset undef variable IDs
    Model::Utils::getUndefVarName(true);
  }//clearContext
  
  void MVCModelContext::addKeyForLaterInterpretation(MdlCtxObjKey aKey)
  {
    for(auto& key : pReqLaterInterpList)
    {
      if(key == contextKeyInf)
      {
        key = aKey;
        return;
      }
    }
    pReqLaterInterpList.push_back(aKey);
  }//addKeyForLaterInterpretation
  
  void MVCModelContext::removeKeyForLaterInterpretation(MVCModelContext::MdlCtxObjKey aKey)
  {
    for(auto& key : pReqLaterInterpList)
    {
      if(key == aKey)
      {
        key = contextKeyInf;
        return;
      }
    }
  }//removeKeyForLaterInterpretation
  
  bool MVCModelContext::isModelContextFullyInterpreted() const
  {
    for(const auto& key : pReqLaterInterpList)
    {
      if(key != MVCModelContext::contextKeyInf)
      {
        return false;
      }
    }
    return true;
  }//isModelContextFullyInterpreted
  
  std::vector<MVCModelContext::MdlCtxObjKey>
  MVCModelContext::getListOfNotFullyInterpretedContextObjects() const
  {
    std::vector<MdlCtxObjKey> listNotFullyInterp;
    for(const auto& key : pReqLaterInterpList)
    {
      if(key != MVCModelContext::contextKeyInf)
      {
        listNotFullyInterp.push_back(key);
      }
    }
    
    return listNotFullyInterp;
  }//getListOfNotFullyInterpretedContextObjects
  
  bool MVCModelContext::isContextObjectFullyInterpreted(MdlCtxObjKey aKey) const
  {
    return std::find(pReqLaterInterpList.begin(), pReqLaterInterpList.end(), aKey) ==
                     pReqLaterInterpList.end();
  }//isContextObjectFullyInterpreted
  
  std::pair<MVCModelContext::MdlCtxObjKey, Model::ContextObject::Type>
  MVCModelContext::addNamedObjectToContext(const std::string& aName, const MdlObj& aDO)
  {
    // Check if there is another object in the context with the same name.
    // If so, delete the object from since it will be overwritten by "aDO"
    CtxObjSPtr oldObj{nullptr};
    auto key = getContextKeyFromID(aName);
    if(key != contextKeyInf)
    {
      // Delete the object already registered
      oldObj = lookup(key);
      erase(key);
    }
    else
    {
      // The key is not in the context: get a new key
      key = getUnusedObjKey();
    }
    assert(key != contextKeyInf);
    
    // @note the given MdlObj may be a class object type,
    // i.e., a BaseObject mapped into the context with another name.
    // For example, because an unnamed object was given a default name, e.g., _1
    // and now that object is stored to a user-given name.
    // Delete the previous named object already registered
    if (isBaseObjectAlreadyRegistered(aDO))
    {
      auto prevName = otools::getObjectPropertyValue<std::string>(aDO.getClassObject().get(),
                                                     MVCModelContext::MVCModelContextBaseObjectKey);
      auto prevKey = getContextKeyFromID(prevName);
      if(prevKey != contextKeyInf) { erase(prevKey); }
    }
    
    // Add the object to the context and
    // map it to the internal data structures
    Model::ContextObject::Type ctxObjType = Model::ContextObject::Type::CTX_OBJ_TYPE_LAST;
    try
    {
      ctxObjType = addObjectToContext(aName, aDO, key);
    }
    catch (...)
    {
      // Re-insert the object after a failure
      // and rethrow whatever exception was caught
      if(oldObj)
      {
        addObjectToContext(aName, oldObj->payload(), key);
      }
      
      throw;
    }
    assert(ctxObjType != Model::ContextObject::Type::CTX_OBJ_TYPE_LAST);
    
    return {key, ctxObjType};
  }//addNamedObjectToContext
  
  Model::ContextObject::Type MVCModelContext::addObjectToContext(const std::string& aName,
                                                                 const MdlObj& aDO,
                                                                 MdlCtxObjKey aKey)
  {
    // 1 - get the object context wrapping the given model object
    auto ctxObject = createContextObject(aName, aDO);
    assert(ctxObject);
    
    // 2 - check if another object with the same ID
    //     is already declared in the context. If so, delete it
    auto oldKey = pCtxObjCollector.lookupKeyByID(aName);
    if(oldKey != CtxObjectCollector::contextKeyInf)
    {
      pCtxObjCollector.unmapCtxKey(oldKey);
    }
    
    // 3 - base objects and context objects don't have a 1-to-1 mapping.
    //     In particular, an instance "x" of a base object doesn't have any
    //     property that indicates to which name-id it is linked to (this is correct
    //     since the id of the variable should not be realted to the rhs value of an assignment).
    //     The problem arises when pushing "x" on the top stack of the virtual machine and
    //     printing-retrieving it from the model context.
    //     Since the model context assumes that every object is identified by an id,
    //     and "x" doesn't have one, the object is re-assigned to the _ctx variable.
    //     To avoid the above problem, the name is added as an hidden property to any
    //     base object instance and the retrieval of a base object is done using
    //     the same name.
    // @update 10/29/2018 use this key for every base object
    // @note this removes any previous mapping of a BaseObject with a name different from "aName"
    mapModelContextKeyIntoBaseObject(aDO, aName);
    
    // 4 - map name to context key
    pCtxObjCollector.mapCtxKey(aKey, ctxObjectTypeToCollectorType(ctxObject->getType()), aName);
    
    // 5 - check if the object is fully evaluated. If not, add it
    //     to the set of objects that require later interpretation
    if (needsLaterInterpretation(aDO))
    {
      addKeyForLaterInterpretation(aKey);
    }
    
    // 6 - add the object to the context
    modelContext()[aKey] = ctxObject;
    
    return ctxObject->getType();
  }//addObjectToContext
  
  MVCModelContext::CtxObjSPtr MVCModelContext::createContextObject(const std::string& aName,
                                                                   const MdlObj& aDO)
  {
    // Create ad-hoc context objects for:
    // - variables
    // - constraints
    // - search
    // Create general context objects for any other type of MdlObj
    assert(aDO.isClassObject());
    auto obj = aDO.getClassObject().get();
    try {
      if(otools::isVariableObject(obj))
      {
        return createVariableContextObject(aName, aDO);
      }
      else if(otools::isConstraintObject(obj))
      {
        return createConstraintContextObject(aName, aDO);
      }
      else if(otools::isSearchObject(obj))
      {
        return std::make_shared<Model::SearchContextObject>(aName, aDO);
      }
      else
      {
        // All other objects are standard context objects
        return std::make_shared<Model::ContextObject>(aName, aDO);
      }
    }
    catch (Model::ModelException& me)
    {
      mvc_assert_msg(false, me.what());
    }
    catch (...)
    {
      mvc_assert_msg(false, "Cannot generate object");
    }
    
    return nullptr;
  }//createContextObject
  
  MVCModelContext::CtxObjSPtr MVCModelContext::createVariableContextObject(const std::string& aID,
                                                                           const MdlObj& aDO)
  {
    // Set input order on the variable
    assert(aDO.isClassObject());
    otools::setVarInputOrder(aDO.getClassObject().get(), pVariableInputOrderCtr++);

    // Set input order
    return std::make_shared<Model::VariableContextObject>(aID, aDO);
  }//createVariableContextObject
  
  MVCModelContext::CtxObjSPtr MVCModelContext::createConstraintContextObject(const std::string& aID,
                                                                             const MdlObj& aDO)
  {
    // Set propagation type.
    // @note propagation type is hard-coded to bounds propagation type
    auto conObj = std::make_shared<Model::ConstraintContextObject>(aID, aDO);
    conObj->setPropagationType(Model::ConstraintContextObject::PropagationType::PT_BOUNDS);

    return conObj;
  }//createConstraintContextObject
  
  MVCModelContext::CtxObjSPtr MVCModelContext::lookup(MdlCtxObjKey aKey) const
  {
    // Check if object is mapped, if not return empty context
    if(!pCtxObjCollector.lookup(aKey)) return nullptr;
    assert(modelContext().find(aKey) != modelContext().end());
    
    return modelContext().at(aKey);
  }//getContext
  
  void MVCModelContext::erase(MdlCtxObjKey aKey)
  {
    // Check if object is mapped, if not return
    if(!pCtxObjCollector.lookup(aKey)) return;
    
    // Remove mapping from key-ID map
    pCtxObjCollector.unmapCtxKey(aKey);
    
    // Remove key from set of objects that require later interpretation
    removeKeyForLaterInterpretation(aKey);
    
    // Delete object from context
    modelContext().erase(aKey);
  }//erase
  
  MVCModelContext::PrettyPrintLevel MVCModelContext::setPrettyPrintLevel(PrettyPrintLevel aPPL) const
  {
    PrettyPrintLevel currPPLevel = pPrettyPrintLevel;
    pPrettyPrintLevel = aPPL;
    
    return currPPLevel;
  }//setPrettyPrintLevel
  
  void MVCModelContext::prettyPrint(std::ostream& aOut) const
  {
    // Return if pretty print is disabled
    if (pPrettyPrintLevel <= static_cast<int>(PrettyPrintLevel::PP_NONE))
    {
      return;
    }
    
    // Ordered pretty print:
    // 1 - variables
    // 2 - constraints
    // 3 - search
    // 4 - everything else
    std::stringstream ssVar;
    std::stringstream ssCon;
    std::stringstream ssSrc;
    std::stringstream ssObj;
    
    // Pretty printing object can trigger interpretation
    // and hence invalidate pCtxObjMap.
    // For this reason, create a copy of the map and use it
    // to print the context content
    auto mappedKeys = pCtxObjCollector.getSetOfMappedKeys();
    
    // The following is just to give an order
    for (auto& key : mappedKeys)
    {
      auto type = collectorTypeToCtxObjectType(pCtxObjCollector.lookupTypeByKey(key));
      switch(type)
      {
        case Model::ContextObject::Type::CTX_OBJ_VAR:
        {
          prettyPrint(key, ssVar);
          ssVar << '\n';
        }
          break;
        case Model::ContextObject::Type::CTX_OBJ_CON:
        {
          prettyPrint(key, ssCon);
          ssCon << '\n';
        }
          break;
        case Model::ContextObject::Type::CTX_OBJ_SRC:
        {
          prettyPrint(key, ssSrc);
          ssSrc << '\n';
        }
          break;
        default:
          assert(type == Model::ContextObject::Type::CTX_OBJ_STD ||
                 type == Model::ContextObject::Type::CTX_OBJ_DBO);
          prettyPrint(key, ssObj);
          ssObj << '\n';
          break;
      }
    }
    
    // Sort output
    aOut << ssVar.str() << ssCon.str() << ssSrc.str() << ssObj.str();
  }//prettyPrint
  
  void MVCModelContext::prettyPrint(MdlCtxObjKey aKey, std::ostream& aOut) const
  {
    // Return if pretty print is disabled
    if (pPrettyPrintLevel <= static_cast<int>(PrettyPrintLevel::PP_NONE))
    {
      return;
    }
    
    if(!pCtxObjCollector.lookup(aKey)) return;
    
    auto ctype = pCtxObjCollector.lookupTypeByKey(aKey);
    bool fullPP = static_cast<int>(pPrettyPrintLevel) >=
                  static_cast<int>(PrettyPrintLevel::PP_HIGH) ? true : false;
    auto ctxObj = lookup(aKey);
    assert(ctxObj);
    
    prettyPrint(collectorTypeToCtxObjectType(ctype), ctxObj, aOut, fullPP);
  }//prettyPrint
  
  void MVCModelContext::registerPrettyPrinters()
  {
    int key{ 0 };
    key = static_cast<int>(Model::ContextObject::Type::CTX_OBJ_VAR);
    pMCPPMap[key] = std::make_shared<Model::MCPPVariable>();
    
    key = static_cast<int>(Model::ContextObject::Type::CTX_OBJ_CON);
    pMCPPMap[key] = std::make_shared<Model::MCPPConstraint>();
    
    // Context object search uses the same pretty printer of a base object
    key = static_cast<int>(Model::ContextObject::Type::CTX_OBJ_SRC);
    pMCPPMap[key] = std::make_shared<Model::MCPPBaseObject>();
    
    key = static_cast<int>(Model::ContextObject::Type::CTX_OBJ_STD);
    pMCPPMap[key] = std::make_shared<Model::MCPPCtxObject>();
    
    key = static_cast<int>(Model::ContextObject::Type::CTX_OBJ_DBO);
    pMCPPMap[key] = std::make_shared<Model::MCPPBaseObject>();
  }//registerPrettyPrinters
  
  Model::ModelContextPrettyPrinter*
  MVCModelContext::getMCPP(const Model::ContextObject::Type aType) const
  {
    auto key = static_cast<int>(aType);
    if (pMCPPMap.find(key) == pMCPPMap.end())
    {
      return nullptr;
    }
    
    return pMCPPMap.at(key).get();
  }//getMCPP
  
  CtxObjectCollector::MdlCtxType ctxObjectTypeToCollectorType(Model::ContextObject::Type aType)
  {
    return static_cast<CtxObjectCollector::MdlCtxType>(aType);
  }//ctxObjectTypeToCollectorType
  
  Model::ContextObject::Type collectorTypeToCtxObjectType(CtxObjectCollector::MdlCtxType aType)
  {
    return static_cast<Model::ContextObject::Type>(aType);
  }//collectorTypeToCtxObjectType
  
}//end namespace MVC
