// Self first
#include "MVCModelConstants.hpp"

namespace MVC {
  namespace ModelMetrics {
    
    const char* COMPILER_TIME  = "compilerTime";
    const char* METRICS        = "Metrics";
    const char* SEARCH_TIME    = "searchTime";
    const char* TIME_METRICS   = "Time";
    const char* WALLCLOCK_TIME = "wallclockTime";
    
  }// end namespace GeneticMetrics
  
  namespace ModelConst {
    
    const char* CTX_VARIABLE_NAME = "_ctx";
    const char* MODEL_SATISFIED   = "Satisfied";
    
  }// end namespace ModelConst
  
}// end namespace MVC
