// Base class
#include "MVCViewFile.hpp"

#include "MVCConstants.hpp"

// Utilities
#include "CLIUtils.hpp"

#include "GlobalsSysLib.hpp"
#include "GlobalsSysMacro.hpp"

#include <cassert>

namespace MVC {
  
  static bool exists(const std::string& aPath)
  {
    std::ifstream f(aPath.c_str());
    return f.good();
  }//exists
  
  static bool validFile(const std::string& aPath)
  {
    if(aPath.size() < 5)
    {
      return false;
    }
    auto ext = aPath.substr(aPath.size() - 4);
    return ext == MVC_FILE_MODEL_EXT;
  }//exists
  
  static bool isASCII(const char& aChar)
  {
    auto cint = static_cast<int>(aChar);
    return cint >= 0 && cint < 128;
  }//isASCII
  
  static bool isSkipLine(const std::string& aString)
  {
    if (aString.size() < 2)
    {
      return false;
    }
    
    // Skip single line comments
    if (aString.substr(0, 2) == std::string(MVC::Constants::View::COMMENT_SYMB))
    {
      return true;
    }
    return false;
  }//isSkipLine
  
  static std::string polishLine(const std::string &aString)
  {
    std::string pline;
    if(aString.empty())
    {
      return pline;
    }
    
    for (auto& c : aString)
    {
      if (isASCII(c))
      {
        pline += c;
      }
    }
    
    CLI::Utils::trim(pline);
    
    // Return empty strings
    if (pline.empty())
    {
      return pline;
    }
    
    // Add ';' if not present
    if (pline.back() != ';')
    {
      pline.push_back(';');
    }
    
    return pline;
  }//polishLine
  
  MVCViewFile::MVCViewFile(const std::string& aFilePath)
  : MVCView(&std::cin, &std::cout)
  , pFilePath(aFilePath)
  , pInputPC(0)
  , pExitCommand(false)
  {
  }
  
  void MVCViewFile::initializeView()
  {
    if(!exists(pFilePath) || !validFile(pFilePath))
    {
      setExitCommand();
      return;
    }
    
    // Open file reader
    std::ifstream fileReader(pFilePath);
    
    // If not open, set the exit command and return
    if (!fileReader.is_open())
    {
      setExitCommand();
      return;
    }
    
    // Copy the file buffer into the input string stream
    std::stringstream inputStream;
    inputStream << fileReader.rdbuf();
    
    // Close the file right away
    fileReader.close();
    
    // Add new line at the end of the stream (required by the parser infrastructure)
    inputStream << "\n";

    // Set the exit command
    inputStream << MVC::Constants::View::EXIT_COMMAND;
    
    // Fill the input stream
    fillInputStream(inputStream);
  }//initializeView
  
  std::string MVCViewFile::getNextInputLine(const std::string&)
  {
    // Return exit command if set
    if(isExitCommandSet()) return MVC::Constants::View::EXIT_COMMAND;
    
    if(inputStreamHasNextLine())
    {
      return getNextLineFromInputStream();
    }
    
    return MVC::Constants::View::EXIT_COMMAND;
  }//getNextInputLine
  
  void MVCViewFile::writeOutStream()
  {
    if (getOutBuffer().empty())
    {
      // Output buffer is empty, nothing to send
      // to the output stream
      return;
    }
    
    if(isOutBufferEmpty())
    {
      clearOutBuffer();
      return;
    }
    
    if(!isDefaultInStreamActive())
    {
      // If the default stream is not active,
      // print new line.
      // @note on the default stream, i.e., std::cin,
      // new line is given by the user
      getOutStream() << '\n';
    }
    
    for (const auto& str : getOutBuffer())
    {
      if (str.empty())
      {
        continue;
      }
      
      getOutStream() << str;
      if (str.back() != '\n')
      {
        getOutStream() << '\n';
      }
    }//for
    getOutStream() << '\n';
    
    clearOutBuffer();
  }//writeOutStream
  
  bool MVCViewFile::isOutBufferEmpty()
  {
    for (const auto& str : getOutBuffer())
    {
      if (!str.empty()) return false;
    }
    return true;
  }//isOutBufferEmpty
  
  void MVCViewFile::fillInputStream(std::stringstream& aStringStream)
  {
    std::string line;
    while( std::getline( aStringStream, line ) )
    {
      if(isSkipLine(line)) continue;
      
      line = polishLine(line);
      if(line.empty()) continue;
      
      pInputStream.push_back(line);
    }
  }//fillInputStream
  
}//end namespace MVC
