// Base class
#include "MVCView.hpp"

#include "MVCConstants.hpp"
#include "CLIUtils.hpp"

#include "GlobalsSysLib.hpp"

#include <fstream>
#include <cassert>

namespace MVC {
  
  MVCView::MVCView(std::istream* aDefaultInStream, std::ostream* aDefaultOutStream)
  : MVC::MVCObserver(MVCEvent::MVCEventActor::MVC_ACTOR_VIEW)
  , pEventFactory(new MVCEventFactory())
  , pEventDispatcher(new MVCEventDispatcher())
  {
    assert(aDefaultInStream);
    assert(aDefaultOutStream);
    
    // Set default I/O streams
    setInStream(aDefaultInStream);
    setOutStream(aDefaultOutStream);
    
    // Register handlers for events
    registerEventHandlers();
  }
  
  MVCSubject* MVCView::getSubject(MVCView* aView)
  {
    return static_cast<MVCSubject*>(aView);
  }//getSubject
  
  void MVCView::registerEventHandlers()
  {
    // Register handle for error event
    pEventDispatcher->registerEventHandler(std::bind(&MVCView::handleErrorEvent, this,
                                                     std::placeholders::_1),
                                           MVCEventType::MVC_EVT_ERROR);
    
    // Register handle for change out stream
    pEventDispatcher->registerEventHandler(std::bind(&MVCView::handleChangeOutStreamEvent, this,
                                                     std::placeholders::_1),
                                           MVCEventType::MVC_EVT_CHANGE_OUTSTREAM);
    
    // Register handle for output event
    pEventDispatcher->registerEventHandler(std::bind(&MVCView::handleWriteOutputEvent, this,
                                                     std::placeholders::_1),
                                           MVCEventType::MVC_EVT_WRITE_OUTPUT);
    
    // Register handle for output event
    pEventDispatcher->registerEventHandler(std::bind(&MVCView::handleNoSolutionEvent, this,
                                                     std::placeholders::_1),
                                           MVCEventType::MVC_EVT_NO_SOLUTION);
  }//registerEventHandlers
  
  void MVCView::sendStmtToController(const std::string& aString, const std::string& aSrcString)
  {
    auto event = pEventFactory->mvcEventEvalStmt(aString, aSrcString,
                                                 MVCEventFactory::EventSrcDst::EVT_V2C);
    notify(std::shared_ptr<MVCEvent>(event));
  }//sendInStringToController
  
  void MVCView::update(const std::shared_ptr<MVCEvent>& aEvent)
  {
    assert(aEvent);
    assert(aEvent->getEventDestination() == MVCEvent::MVCEventActor::MVC_ACTOR_VIEW);
    
    // Dispatch event
    pEventDispatcher->dispatch(aEvent);
  }//update
  
  boost::optional<std::string> MVCView::readInStream()
  {
    // Reset bit status before new read
    getInStream().clear();

    std::string line;
    if (!std::getline(getInStream(), line))
    {
      return boost::none;
    }
    return line;
  }//readInStream
  
  void MVCView::setInStream(std::istream* aInStream)
  {
    assert(aInStream);
    pInStream.push_back(aInStream);
  }//setInStream
  
  void MVCView::setOutStream(std::ostream* aOutStream)
  {
    assert(aOutStream);
    pOutStream.push_back(aOutStream);
  }//setOutStream
  
  void MVCView::resetInStream(std::istream* aInStream)
  {
    if(aInStream == pInStream.back() && pInStream.size() > 1)
    {
      pInStream.pop_back();
    }
    getInStream().clear();
  }//resetInStream
  
  void MVCView::resetOutStream(std::ostream* aOutStream)
  {
    if (aOutStream == pOutStream.back() && pOutStream.size() > 1)
    {
      pOutStream.pop_back();
    }
  }//resetOutStream
  
  void MVCView::handleNoSolutionEvent(MVCEvent* aEvent)
  {
    (void)aEvent;
    pOutBuffer.push_back(MVC::Constants::View::UNSAT_MODEL_MESSAGE);
  }//handleNoSolutionEvent
  
  void MVCView::handleChangeOutStreamEvent(MVCEvent* aEvent)
  {
    assert(MVCEventChangeOutStream::isa(aEvent));
    if(MVCEventChangeOutStream::cast(aEvent)->resetOutStream())
    {
      resetOutStream(pOutStream.back());
      return;
    }
    auto outStream = MVCEventChangeOutStream::cast(aEvent)->getOutStream();
    assert(outStream);
    
    setOutStream(outStream);
  }//handleChangeOutStreamEvent
  
  void MVCView::handleErrorEvent(MVCEvent* aEvent)
  {
    assert(aEvent);
    assert(aEvent->getEventDestination() == MVCEvent::MVCEventActor::MVC_ACTOR_VIEW);
    
    auto e = MVCEventError::cast(aEvent);
    assert(e);
    
    std::string errorMsg{""};
    auto errorLevel = e->getErrorLevel();
    switch (errorLevel) {
      case MVCEventError::ErrorLevel::LEVEL_WARNING:
        errorMsg = std::string(MVC::Constants::View::WARNING_MESSAGE);
        break;
      case MVCEventError::ErrorLevel::LEVEL_ERROR:
        errorMsg = std::string(MVC::Constants::View::ERROR_MESSAGE);
        break;
      default:
        assert(errorLevel == MVCEventError::ErrorLevel::LEVEL_NONE);
        break;
    }
    errorMsg += e->what();
    
    if(errorLevel == MVCEventError::ErrorLevel::LEVEL_ERROR)
    {
      // On error event:
      // 1) close the current output stream and restore it
      //    to the previous one;
      // 2) flush all the events in the queue of the dispatcher
      //    since everything else is took over by this error
      
      // (1) reset output stream
      resetOutStream(pOutStream.back());
      
      // (2) flush event queue
      pEventDispatcher->clearEventQueue();
    }
    
    pOutBuffer.push_back(errorMsg);
  }//evaluateErrorEvent
  
  void MVCView::handleWriteOutputEvent(MVCEvent* aEvent)
  {
    assert(aEvent);
    
    MVCEventWriteOutput* e = MVCEventWriteOutput::cast(aEvent);
    assert(e);
    
    for(std::size_t idx = 0; idx < e->size(); ++idx)
    {
      pOutBuffer.push_back(e->getString(idx));
    }
  }//writeOutputOnBuffer
  
}//end namespace MVC
