// Base class
#include "MVCViewCLI.hpp"

// Parser
//#include "parser_ext.h"

// Utilitie
#include "CLIUtils.hpp"

#include "GlobalsSysLib.hpp"
#include "GlobalsSysMacro.hpp"
#include "MVCConstants.hpp"
#include "Readline.hpp"

#include <cassert>

#define PARSE_STRING_LENGTH 128

namespace {
  
  std::string getWelcomeMessage()
  {
    std::string msg;
    msg += "\t\t";
    msg += MVC::Constants::View::WELCOME_OPTILAB;
    msg += "\n\t";
    msg += MVC::Constants::View::WELCOME_COPYRIGHTS;
    msg += "\n\t";
    msg += MVC::Constants::View::WELCOME_RIGHTS;
    return msg;
  }// getWelcomeMessage
  
}  // namespace

namespace MVC {
  
  MVCViewCLI::MVCViewCLI(bool aUseSmartCLI)
  : MVCView(&std::cin, &std::cout)
  , pUseSmartCLI(aUseSmartCLI)
  , pRunning(true)
  , pInLine("")
  , pOutBufferUsed(false)
  {
    if(isReadlineEnabled()) {
      Readline::initialize("optilab_console", true);
    }
  }
  
  static bool isASCII(const char& aChar)
  {
    auto cint = static_cast<int>(aChar);
    return cint >= 0 && cint < 128;
  }//isASCII
  
  static std::string parse(const std::string& aInString)
  {
    using namespace CLI;
    char* strVecCharPtr;
    CLI_STR_CPY(strVecCharPtr, aInString.c_str());
    
    // Call external parser
    char outCharPtr[PARSE_STRING_LENGTH]{ 0 };
    //parseString(strVecCharPtr, outCharPtr);
    
    std::string outStr(outCharPtr);
    
    // Free memory and return
    delete[] strVecCharPtr;
    return outStr;
  }//parsePP
  
  static bool isExitCmd(const std::string& aString)
  {
    static std::string exitCmd(MVC::Constants::View::EXIT_COMMAND);
    return aString.substr(0, exitCmd.size()) == exitCmd;
  }//isExitCmd
  
  static std::string polishLine(const std::string &aString)
  {
    std::string pline;
    if(aString.empty())
    {
      return pline;
    }
    
    for (auto& c : aString)
    {
      if (isASCII(c))
      {
        pline += c;
      }
    }
    
    CLI::Utils::trim(pline);
    
    // Return empty strings
    if (pline.empty())
    {
      return pline;
    }
    
    // Add ';' if not present
    if (pline.back() != ';')
    {
      pline.push_back(';');
    }
    
    return pline;
  }//polishLine
  
  static bool isSkipLine(const std::string& aString)
  {
    if (aString.size() < 2)
    {
      return false;
    }
    
    // Skip single line comments
    if (aString.substr(0, 2) == std::string(MVC::Constants::View::COMMENT_SYMB))
    {
      return true;
    }
    return false;
  }//isSkipLine
  
  bool MVCViewCLI::printNewLine()
  {
    return pOutBufferUsed;
  }//printNewLine
  
  void MVCViewCLI::printWelcomeScreen(std::ostream &aOut)
  {
    aOut << getWelcomeMessage() << "\n\n";
  }//printIntroScreen
  
  void MVCViewCLI::initializeView()
  {
    // Initialize the output stream
    printWelcomeScreen(getOutStream());
  }//initializeView
  
  void MVCViewCLI::readLine(const std::string& aPrompt)
  {
    // Print prompt symbol
    if (!isReadlineEnabled() && isDefaultInStreamActive() && ISSATTY(STDIN_FILENO))
    {
      // If the default input stream is active, prints the prompt symbol
      // on the default output stream.
      // This prints the prompt symbol on the default output stream
      // even if the default output stream is not active
      getDefaultOutStream() << aPrompt;
    }

    boost::optional<std::string> line;
    
    // Read next line from input stream
    if (isReadlineEnabled())
    {
      line = Readline::getLine(aPrompt.c_str());
    }
    else
    {
      line = readInStream();
    }
    
    while(!line)
    {
      // If there was an error in reading the input line
      // and the default stream, i.e., std::cin is the active stream,
      // try to re-read user input
      if(isDefaultInStreamActive())
      {
        getDefaultOutStream() << aPrompt;
        line = readInStream();
        
        // Continue checking if line is a valid string
        continue;
      }
      
      // Reset input stream to the last input stream used
      // before the current one since the current one had caused
      // an error in reading the current line
      resetInStream(&getInStream());
      if(isDefaultInStreamActive())
      {
        // The current input stream, after re-setting,
        // it's the default input, output the prompt
        // to wait for user input
        getDefaultOutStream() << aPrompt;
      }
      
      line = readInStream();
    }// while
    assert(line);
    pInLine = *line;
    
    // Print input line into the current output stream
    // if the current output stream is not the default one
    // @note !isDefaultInStreamActive() && isDefaultOutStreamActive()
    // will print all the input read from the script
    if(!isDefaultOutStreamActive())
    {
      getOutStream() << aPrompt << pInLine;
      if(pInLine.back() != '\n')
      {
        getOutStream() << '\n';
      }
    }
  }//readLine
  
  void MVCViewCLI::cleanInLine()
  {
    pInLine.clear();
  }//cleanInLine
  
  void MVCViewCLI::handleLine()
  {
    auto pline = polishLine(pInLine);
    
    // Exit on command
    if (isExitCmd(pline))
    {
      cleanInLine();
      pRunning = false;
      return;
    }
    
    // Check whether to skip line or not
    if (isSkipLine(pline))
    {
      cleanInLine();
      return;
    }
    
    // Parse string using external parser
    auto parsedString = parse(pline);
    if (parsedString.empty())
    {
      cleanInLine();
      return;
    }
    
    // Send parsed string as event to controller
    sendStmtToController(parsedString, pInLine);
  }//handleLine
  
  bool MVCViewCLI::isReadlineEnabled()
  {
    return pUseSmartCLI;
  }//isReadlineEnabled

  bool MVCViewCLI::isOutBufferEmpty()
  {
    for (const auto& str : getOutBuffer())
    {
      if (!str.empty()) return false;
    }
    return true;
  }//isOutBufferEmpty
  
  void MVCViewCLI::writeOutStream()
  {
    if (getOutBuffer().empty())
    {
      // Output buffer is empty, nothing to send
      // to the output stream
      return;
    }
    
    if(isOutBufferEmpty())
    {
      clearOutBuffer();
      return;
    }
    
    if(!isDefaultInStreamActive())
    {
      // If the default stream is not active,
      // print new line.
      // @note on the default stream, i.e., std::cin,
      // new line is given by the user
      getOutStream() << '\n';
    }
    
    for (const auto& str : getOutBuffer())
    {
      if (str.empty())
      {
        continue;
      }
      
      getOutStream() << str;
      if (str.back() != '\n')
      {
        getOutStream() << '\n';
      }
    }//for
    getOutStream() << '\n';
    
    clearOutBuffer();
  }//writeOutStream
  
  std::string MVCViewCLI::getNextInputLine(const std::string& aPrompt)
  {
    // Reads line from current input stream
    readLine(aPrompt);
    
    // Add input line to history without polishing
    Readline::addHistory(pInLine);

    // Cleans input line
    auto iline = polishLine(pInLine);
    pInLine.clear();
    
    return iline;
  }//getNextInputLine
  
  void MVCViewCLI::exe()
  {
    // Initialize view, i.e., prepare output stream
    // to receive output
    initializeView();
    
    // Main loop, waiting for user's input
    while (pRunning)
    {
      // Read next line from the current input stream
      readLine(MVC::Constants::View::PROMPT_SYMB);
      
      // Parse the input line
      handleLine();
      
      // Dump the output buffer into the current output stream
      // @note the output buffer can contain data coming from
      // model and/or controller
      writeOutStream();
    }
  }//exe
  
}//end namespace MVC
