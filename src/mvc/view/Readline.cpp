//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Claudio Desideri on 09/02/2018
//

#include "Readline.hpp"

#include "MVCExportDefs.hpp"

#include <memory>
#include <iostream>
#include <list>
#include <vector>

//function list
#include "FunctionRegister.hpp"

//readline
#include <readline/readline.h>

#ifdef __APPLE__
#include <readline/history.h>
#endif

//Managed via cmake
#ifdef IS_LINUX
#include <readline/history.h>
#endif

namespace Readline {

  //initialize autocompletion function list
  std::vector<std::string> Readline::Data::pFunctions = {};

  //Readline's constructor
  MVC_EXPORT_FCN void initialize(std::string aName, bool aHistory)
  {
    Readline::loadFunctions();

    //naming virtual console
    rl_readline_name = aName.c_str();

    //enabling history
    if (aHistory) using_history();

    //enabling autocompletion
    rl_attempted_completion_function = Readline::completer;
  }

  MVC_EXPORT_FCN void loadFunctions()
  {
    // empty function name list
    Data::pFunctions = {};

    MVC::FunctionRegister functionRegister;
    std::size_t registerSize = functionRegister.getRegisterSize();

    // iterate over all the defined functions
    for( std::size_t i=0; i<registerSize; i++ ){
      std::string functionName = functionRegister.getRegisteredFcnNameByIndex(i);
      Data::pFunctions.push_back(functionName);
    }
  }

  char * completionGenerator(const char* aText, int aState) {
    // This function is called with state=0 the first time; subsequent calls are
    // with a nonzero state. state=0 can be used to perform one-time
    // initialization for this completion session.
    static std::vector<std::string> matches;
    static std::size_t matchIndex{0};

    if (aState == 0) {
      // During initialization, compute the actual matches for 'text' and keep
      // them in a static vector.
      matches.clear();
      matchIndex = 0;

      // Collect a vector of matches: vocabulary words that begin with text.
      std::string textStr = std::string(aText);
      for (const auto& word : Data::pFunctions) {
        if (word.size() >= textStr.size() &&
            word.compare(0, textStr.size(), textStr) == 0) {
          matches.push_back(word);
        }
      }
    }

    if (matchIndex >= matches.size()) {
      // We return nullptr to notify the caller no more matches are available.
      return nullptr;
    } else {
      // Return a malloc'd char* for the match. The caller frees it.
      return strdup(matches[matchIndex++].c_str());
    }
  }

  char** completer(const char* aText, int aStart, int aEnd) {
    // Don't do filename completion even if our generator finds no matches.
    rl_attempted_completion_over = 1;

    // Note: returning nullptr here will make readline use the default filename
    // completer.
    return rl_completion_matches(aText, Readline::completionGenerator);
  }

  MVC_EXPORT_FCN boost::optional<std::string> getLine(std::string aPrompt)
  {   
    //asks for user input (a single line)
    //can be blocking
    char * buf;
    std::string line(buf = readline(aPrompt.c_str()));
    free(buf);

    return line;
  }

  MVC_EXPORT_FCN void addHistory(std::string aLine)
  {
    char * entry = new char[aLine.length() + 1];
    strcpy(entry, aLine.c_str());
    add_history(entry);
  }

}
