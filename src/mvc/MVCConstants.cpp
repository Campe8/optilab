// Self first
#include "MVCConstants.hpp"

namespace MVC {
namespace Constants {
namespace View {

  const char* COMMENT_SYMB = "//";
  const char* ERROR_MESSAGE = "Error: ";
  const char* EXIT_COMMAND = "exit";
  const char* PROMPT_MULTILINE_SYMB = "  ";
  const char* PROMPT_SYMB = ">> ";
  const char* UNSAT_MODEL_MESSAGE = "No solutions";
  const char* WARNING_MESSAGE = "Warning: ";
  const char* WELCOME_COPYRIGHTS = "Copyright (c) 2017-2019 OptiLab team.";
  const char* WELCOME_OPTILAB = "==== OptiLab ====";
  const char* WELCOME_RIGHTS = "All rights reserved.";
  
}  // namespace View
}  // namespace Constants
}  // namespace MVC
