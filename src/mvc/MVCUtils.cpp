// Self first
#include "MVCUtils.hpp"

#include "ModelUtils.hpp"
#include "InterpreterUtils.hpp"
#include "CLIUtils.hpp"
#include "OPCode.hpp"
#include "ObjectHelper.hpp"
#include "ObjectTools.hpp"
#include "SearchOptionsTools.hpp"

#include "ModelMacro.hpp"
#include "VirtualMachine.hpp"

#include <algorithm>
#include <cassert>
#include <sstream>
#include <string>
#include <utility>  // for std::pair

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace iutils = Model::Utils::Interpreter;
namespace cutils = CLI::Utils;
namespace otools = Interpreter::ObjectTools;
namespace sotools = Interpreter::SearchOptionsTools;

namespace {
  
  // Helper function to convert a string array to an int array
  std::vector<INT_64_T>
  boundsToIntArray(const std::vector<std::pair<std::string, std::string>>& pairList)
  {
    std::vector<INT_64_T> intArray;
    intArray.reserve(pairList.size());
    for (const auto& pair : pairList)
    {
      if (pair.first == pair.second)
      {
        intArray.push_back(std::stoi(pair.first));
      }
      else
      {
        intArray.push_back(std::stoi(pair.first));
        intArray.push_back(std::stoi(pair.second));
      }
    }

    return intArray;
  }// boundsToIntArray
  
  std::vector<Interpreter::DataObject>
  toDataObjectArray(const std::vector<INT_64_T>& aBaseArray,
                    Interpreter::DataObject::DataObjectType aType)
  {
    std::vector<Interpreter::DataObject> dataObjectArray;
    dataObjectArray.reserve(aBaseArray.size());
    
    for(auto elem : aBaseArray)
    {
      if(aType == Interpreter::DataObject::DataObjectType::DOT_BOOL)
      {
        dataObjectArray.push_back(Interpreter::DataObject(static_cast<bool>(elem)));
      }
      else
      {
        assert(aType == Interpreter::DataObject::DataObjectType::DOT_INT);
        dataObjectArray.push_back(Interpreter::DataObject(static_cast<int>(elem)));
      }
    }
    
    return dataObjectArray;
  }//toDataObjectArray
  
  bool replaceDomain(Interpreter::BaseObject* varObj, std::vector<std::vector<INT_64_T>>& domain)
  {
    // Create a new variable base object with updated domains
    if (domain.size() == 1)
    {
      // Replace the domain of the variable
      assert(otools::getListSize(otools::getVarDomainDimensionsList(varObj)) == 0);
      auto& varDomain = otools::getListValues(otools::getVarDomainList(varObj));
      
      // Replace old domain with new domain
      assert(varDomain.composeSize() > 0);
      varDomain.getCompose() = toDataObjectArray(domain[0], varDomain[0].dataObjectType());
      
      // If the single domain has one element, it means that the domain is a singleton domain
      return domain[0].size() == 1;
    }
    
    // Matrix of domains
    bool isMatrixSingleton = true;
    
    const auto size = otools::getVarDomainDimensionsSize(varObj);
    assert(domain.size() == static_cast<std::size_t>(size));
    
    // Prepare the domain array
    std::vector<Interpreter::DataObject> domainArray;
    domainArray.reserve(domain.size());
    
    const auto& varDomain = otools::getListValues(otools::getVarDomainList(varObj));
    assert(varDomain.isComposite());
    
    auto objType = varDomain[0].dataObjectType();
    for(std::size_t idx{0}; idx < domain.size(); ++idx)
    {
      // Check if the size of the sub-domain.
      // If the size is greater than one, the matrix is not singleton
      if (domain[idx].size() > 1) isMatrixSingleton = false;
      
      std::vector<Interpreter::DataObject> subDomainList;
      for(std::size_t elemIdx{0}; elemIdx < domain[idx].size(); ++elemIdx)
      {
        if(objType == Interpreter::DataObject::DataObjectType::DOT_BOOL)
        {
          subDomainList.push_back( Interpreter::DataObject(static_cast<bool>(domain[idx][elemIdx])) );
        }
        else
        {
          assert(objType == Interpreter::DataObject::DataObjectType::DOT_INT);
          subDomainList.push_back( Interpreter::DataObject(static_cast<int>(domain[idx][elemIdx])) );
        }
      }// for
      
      Interpreter::DataObject subDomain;
      subDomain.setClassObject(Interpreter::BaseObjectSPtr(otools::createListObject(subDomainList)));
      domainArray.push_back(subDomain);
    }//for
    
    Interpreter::DataObject updatedDomainArray;
    updatedDomainArray.setClassObject(Interpreter::BaseObjectSPtr(otools::createListObject(domainArray)));
    otools::getVarDomainArray(varObj) = updatedDomainArray;
    
    return isMatrixSingleton;
  }// replaceDomain
  
}// namespace

namespace MVC { namespace Utils {
  
  // Helper function to convert a string array to an int array
  static std::vector<INT_64_T> toIntArray(const std::vector<std::string>& aArray)
  {
    std::vector<INT_64_T> intArray(aArray.size());
    std::transform(aArray.begin(), aArray.end(), intArray.begin(), cutils::stoi);
    
    return intArray;
  }//toIntArray
  
  bool replaceDomainInVarCtxObj(const MVCModelContext::CtxObjSPtr& aCtxObj,
                                const Solver::ModelCallbackHandler::DomainList& aDom)
  {
    assert(aCtxObj && aCtxObj->getType() == Model::ContextObject::CTX_OBJ_VAR);
    auto var = static_cast<Model::VariableContextObject*>(aCtxObj.get());
    
    assert((var->payload()).isClassObject());
    auto varObj = (var->payload()).getClassObject().get();
    
    // Get the values of the domain to update
    std::vector<std::vector<INT_64_T>> domain;
    domain.reserve(aDom.size());
    for (const auto& pair : aDom)
    {
      if (pair.first == pair.second)
      {
        domain.push_back({std::stoi(pair.first)});
      }
      else
      {
        domain.push_back({std::stoi(pair.first), std::stoi(pair.second)});
      }
    }
    // std::transform(aDom.begin(), aDom.end(), domain.begin(), boundsToIntArray);
    
    return replaceDomain(varObj, domain);
  }// replaceDomainInVarCtxObj
  
  void replaceDomainInVarCtxObj(const MVCModelContext::CtxObjSPtr& aCtxObj,
                                const std::vector<std::vector<std::string>>& aDom)
  {
    assert(aCtxObj && aCtxObj->getType() == Model::ContextObject::CTX_OBJ_VAR);
    auto var = static_cast<Model::VariableContextObject*>(aCtxObj.get());
    
    assert((var->payload()).isClassObject());
    auto varObj = (var->payload()).getClassObject().get();
    
    // Get the values of the domain to update
    std::vector<std::vector<INT_64_T>> domain(aDom.size());
    std::transform(aDom.begin(), aDom.end(), domain.begin(), toIntArray);
    
    replaceDomain(varObj, domain);
  }//replaceDomainInVarCtx
  
  std::string getLinkedVariableIDFromDomainDataObject(const Interpreter::DataObject& aDO)
  {
    assert(aDO.isClassObject() && otools::isVariableObject(aDO.getClassObject().get()));
    return otools::getVarID(aDO.getClassObject().get());
  }//getLinkedVariableIDFromDomainDataObject
  
  bool isVarMatrixSubscr(const Interpreter::DataObject& aDO)
  {
    auto varObj = aDO.getClassObject().get();
    if(!aDO.isClassObject() || !otools::isVariableObject(varObj)) return false;
    return otools::isVarSubscript(varObj);
  }//isVarMatrixSubscr
  
  bool isVarOptimization(const Interpreter::DataObject& aDO)
  {
    auto varObj = aDO.getClassObject().get();
    if(!aDO.isClassObject() || !otools::isVariableObject(varObj)) return false;
    auto semantic = otools::getVarSemantic(varObj);
    return semantic == VAR_SPEC_OPT_MIN || semantic == VAR_SPEC_OPT_MAX;
  }//isVarOptimization
  
  std::string getIDFromDataObject(const Interpreter::DataObject& aDO)
  {
    if (Interpreter::isPrimitiveType(aDO))
    {
      if(aDO.dataObjectType() == Interpreter::DataObject::DataObjectType::DOT_STRING)
      {
        return aDO.getDataValue<std::string>();
      }
    }
    else if (Interpreter::isClassObjectType(aDO))
    {
      return getIDFromDataObject(aDO.getClassObject().get());
    }
    
    return "";
  }//getIDFromDataObject
  
  std::string getIDFromDataObject(const Interpreter::BaseObject* aBaseObject)
  {
    // The object may not have the property.
    // For example, an object printed from the top of the stack that will be paired
    // to the context variable
    if(!aBaseObject->hasProperty(MVCModelContext::MVCModelContextBaseObjectKey)) return "";
    return Interpreter::ObjectTools::getObjectPropertyValue<std::string>(
                    const_cast<Interpreter::BaseObject*>(aBaseObject),
                    MVCModelContext::MVCModelContextBaseObjectKey);
  }//getIDFromDataObject
  
}}//end namespace MVC/Utils
