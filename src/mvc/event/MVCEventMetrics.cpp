// Base class
#include "MVCEventMetrics.hpp"

#include "GlobalsSysLib.hpp"

#include "MetricsManager.hpp"
#include "BaseObject.hpp"


#include <cassert>
#include <sstream>

namespace MVC {
  
  MVCEventMetrics::MVCEventMetrics(MVCEvent::MVCEventActor aSrc, MVCEvent::MVCEventActor aDst)
  : MVCEvent(aSrc, aDst, MVCEventType::MVC_EVT_METRICS)
  , pMetrics(nullptr)
  {
  }
  
  bool MVCEventMetrics::isa(const MVCEvent* aEvent)
  {
    return aEvent &&
    aEvent->getEventType() == MVCEventType::MVC_EVT_METRICS;
  }
  
  MVCEventMetrics* MVCEventMetrics::cast(MVCEvent* aEvent)
  {
    if (!isa(aEvent)) return nullptr;
    return static_cast<MVCEventMetrics*>(aEvent);
  }
  
  std::shared_ptr<Interpreter::BaseObject> MVCEventMetrics::getMetrics()
  {
    if (pMetrics) return pMetrics;
    pMetrics =
    std::shared_ptr<Interpreter::BaseObject>(Base::Tools::MetricsManager::getInstance().toObject());
    
    return pMetrics;
  }//getMetrics
  
}//end namespace MVC
