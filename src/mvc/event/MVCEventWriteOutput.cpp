// Base class
#include "MVCEventWriteOutput.hpp"

#include "GlobalsSysLib.hpp"

#include <cassert>

namespace MVC {
  
  MVCEventWriteOutput::MVCEventWriteOutput(const std::list<std::string>& aOutStrings, MVCEvent::MVCEventActor aSrc, MVCEvent::MVCEventActor aDst)
  : MVCEvent(aSrc, aDst, MVCEventType::MVC_EVT_WRITE_OUTPUT)
  , pStringRegister(aOutStrings)
  {
  }
  
  bool MVCEventWriteOutput::isa(const MVCEvent* aEvent)
  {
    return aEvent &&
    aEvent->getEventType() == MVCEventType::MVC_EVT_WRITE_OUTPUT;
  }
  
  MVCEventWriteOutput* MVCEventWriteOutput::cast(MVCEvent* aEvent)
  {
    if(!isa(aEvent))
    {
      return nullptr;
    }
    return static_cast<MVCEventWriteOutput*>(aEvent);
  }
  
  std::string MVCEventWriteOutput::getString(std::size_t aIdx) const
  {
    auto it = pStringRegister.begin();
    std::advance(it, aIdx);
    return *it;
  }//getString
  
}//end namespace MVC
