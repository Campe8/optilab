// Base class
#include "MVCEventSolution.hpp"

#include "GlobalsSysLib.hpp"
#include <cassert>

namespace MVC {
  
  MVCEventSolution::MVCEventSolution(std::size_t aNumSolutions, const std::list<std::string>& aSolutionList,
                                     MVCEvent::MVCEventActor aSrc, MVCEvent::MVCEventActor aDst)
  : MVCEvent(aSrc, aDst, MVCEventType::MVC_EVT_SOLUTION)
  , pSolutions(aNumSolutions)
  , pSolutionList(aSolutionList)
  {
  }
  
  bool MVCEventSolution::isa(const MVCEvent* aEvent)
  {
    return aEvent &&
    aEvent->getEventType() == MVCEventType::MVC_EVT_SOLUTION;
  }
  
  MVCEventSolution* MVCEventSolution::cast(MVCEvent* aEvent)
  {
    if(!isa(aEvent))
    {
      return nullptr;
    }
    return static_cast<MVCEventSolution*>(aEvent);
  }
  
}//end namespace MVC
