// Base class
#include "MVCEventNoSolution.hpp"

#include "GlobalsSysLib.hpp"
#include <cassert>

namespace MVC {
  
	MVCEventNoSolution::MVCEventNoSolution(MVCEvent::MVCEventActor aSrc, MVCEvent::MVCEventActor aDst)
  : MVCEvent(aSrc, aDst, MVCEventType::MVC_EVT_NO_SOLUTION)
  {
  }
  
  bool MVCEventNoSolution::isa(const MVCEvent* aEvent)
  {
    return aEvent &&
    aEvent->getEventType() == MVCEventType::MVC_EVT_NO_SOLUTION;
  }
  
	MVCEventNoSolution* MVCEventNoSolution::cast(MVCEvent* aEvent)
  {
    if(!isa(aEvent))
    {
      return nullptr;
    }
    return static_cast<MVCEventNoSolution*>(aEvent);
  }
  
}//end namespace MVC
