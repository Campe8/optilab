// Base class
#include "MVCEventEvalStmt.hpp"

#include <cassert>

namespace MVC {
  
  MVCEventEvalStmt::MVCEventEvalStmt(const std::string& aStmt, MVCEvent::MVCEventActor aSrc, MVCEvent::MVCEventActor aDst)
  : MVCEvent(aSrc, aDst, MVCEventType::MVC_EVT_EVAL_STMT)
  , pSrcStmt("")
  {
    try {
      if(!aStmt.empty())
      {
        pStmt = CLI::Utils::getCLIStmtFromString(aStmt);
      }
    } catch (...)
    {
      pStmt = CLI::Utils::getCLIStmtFromString("");
    }
  }
  
  bool MVCEventEvalStmt::isa(const MVCEvent* aEvent)
  {
    return aEvent &&
    aEvent->getEventType() == MVCEventType::MVC_EVT_EVAL_STMT;
  }
  
  MVCEventEvalStmt* MVCEventEvalStmt::cast(MVCEvent* aEvent)
  {
    if(!isa(aEvent))
    {
      return nullptr;
    }
    return static_cast<MVCEventEvalStmt*>(aEvent);
  }
  
}//end namespace MVC
