// Self first
#include "MVCEventEvaluateCodeObject.hpp"

#include "GlobalsSysLib.hpp"

#include <cassert>

namespace MVC {
  
  MVCEventEvaluateCodeObject::MVCEventEvaluateCodeObject(const Interpreter::DataObject& aDataObject,
                                                         MVCEvent::MVCEventActor aSrcActor,
                                                         MVCEvent::MVCEventActor aDstActor)
  : MVCEvent(aSrcActor, aDstActor, MVCEventType::MVC_EVT_EVAL_CODE_OBJ)
  , pDataObject(aDataObject)
  {
    assert(aDataObject.getByteCode());
  }
  
  bool MVCEventEvaluateCodeObject::isa(const MVCEvent* aEvent)
  {
    return aEvent &&
    aEvent->getEventType() == MVCEventType::MVC_EVT_EVAL_CODE_OBJ;
  }//isa
  
  MVCEventEvaluateCodeObject* MVCEventEvaluateCodeObject::cast(MVCEvent* aEvent)
  {
    if(!isa(aEvent)) return nullptr;
    return static_cast<MVCEventEvaluateCodeObject*>(aEvent);
  }//cast
  
}//end namespace MVC
