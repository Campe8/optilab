// Base class
#include "MVCObserver.hpp"

namespace MVC {
  
  void MVCSubject::notify(const std::shared_ptr<MVCEvent>& aEvent) const
  {
    assert(aEvent);
    assert(pObserverRegister.find(static_cast<int>(aEvent->getEventDestination())) != pObserverRegister.end());
    
    for(auto obs : pObserverRegister.at(static_cast<int>(aEvent->getEventDestination())))
    {
      obs->update(aEvent);
    }
  }//notify
  
  
  MVCObserver::MVCObserver(MVCEvent::MVCEventActor aActor)
  : pEventActor(aActor)
  {
  }//MVCObserver
  
  void MVCObserver::observe(MVCSubject* aSubject)
  {
    assert(aSubject);
    aSubject->registerObserver(pEventActor, this);
  }//observe
  
}//end namespace MVC
