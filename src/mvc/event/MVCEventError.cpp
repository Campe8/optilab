// Base class
#include "MVCEventError.hpp"

#include "GlobalsSysLib.hpp"

#include <cassert>

namespace MVC {
  
  MVCEventError::MVCEventError(const std::string& aMsg,
                               ErrorLevel aLevel,
                               MVCEvent::MVCEventActor aSrcActor,
                               MVCEvent::MVCEventActor aDstActor)
  : MVCEvent(aSrcActor, aDstActor, MVCEventType::MVC_EVT_ERROR)
  , pErrorMsg(aMsg)
  , pLevel(aLevel)
  {
  }
  
  bool MVCEventError::isa(const MVCEvent* aEvent)
  {
    return aEvent &&
    aEvent->getEventType() == MVCEventType::MVC_EVT_ERROR;
  }
  
  MVCEventError* MVCEventError::cast(MVCEvent* aEvent)
  {
    if(!isa(aEvent))
    {
      return nullptr;
    }
    return static_cast<MVCEventError*>(aEvent);
  }
  
}//end namespace MVC
