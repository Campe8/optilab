// Base class
#include "MVCEventChangeOutStream.hpp"

#include "GlobalsSysLib.hpp"
#include <cassert>

namespace MVC {
  
	MVCEventChangeOutStream::MVCEventChangeOutStream(bool aReset, MVCEvent::MVCEventActor aSrc, MVCEvent::MVCEventActor aDst)
  : MVCEvent(aSrc, aDst, MVCEventType::MVC_EVT_CHANGE_OUTSTREAM)
  , pResetStream(aReset)
  , pOutStream(nullptr)
  {
  }
  
  bool MVCEventChangeOutStream::isa(const MVCEvent* aEvent)
  {
    return aEvent &&
    aEvent->getEventType() == MVCEventType::MVC_EVT_CHANGE_OUTSTREAM;
  }
  
	MVCEventChangeOutStream* MVCEventChangeOutStream::cast(MVCEvent* aEvent)
  {
    if(!isa(aEvent))
    {
      return nullptr;
    }
    return static_cast<MVCEventChangeOutStream*>(aEvent);
  }
  
  void MVCEventChangeOutStream::setOutStream(std::ostream* aOutStream)
  {
    assert(aOutStream);
    pOutStream = aOutStream;
  }//setOutStream
  
}//end namespace MVC
