// Base class
#include "MVCEventReadFile.hpp"

#include "GlobalsSysLib.hpp"
#include <cassert>

namespace MVC {
  
  MVCEventReadFile::MVCEventReadFile(const std::string& aPath, MVCEvent::MVCEventActor aSrc)
  : MVCEvent(aSrc, MVCEvent::MVCEventActor::MVC_ACTOR_VIEW, MVCEventType::MVC_EVT_READ_FILE)
  , pPath(aPath)
  {
  }
  
  bool MVCEventReadFile::isa(const MVCEvent* aEvent)
  {
    return aEvent &&
    aEvent->getEventType() == MVCEventType::MVC_EVT_READ_FILE;
  }
  
  MVCEventReadFile* MVCEventReadFile::cast(MVCEvent* aEvent)
  {
    if(!isa(aEvent))
    {
      return nullptr;
    }
    return static_cast<MVCEventReadFile*>(aEvent);
  }
  
}//end namespace MVC
