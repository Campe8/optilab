// Base class
#include "MVCEventFactory.hpp"

#include "GlobalsSysLib.hpp"
#include <cassert>

namespace MVC {
  
  // Returns the pair <source, destination> of actors based on given EventSrcDst
  static std::pair<MVCEvent::MVCEventActor, MVCEvent::MVCEventActor> getSrcDstActors(MVCEventFactory::EventSrcDst aSrcDst)
  {
    switch (aSrcDst) {
      case MVCEventFactory::EventSrcDst::EVT_V2C:
        return std::make_pair(MVCEvent::MVCEventActor::MVC_ACTOR_VIEW, MVCEvent::MVCEventActor::MVC_ACTOR_CONTROLLER);
      case MVCEventFactory::EventSrcDst::EVT_V2M:
        return std::make_pair(MVCEvent::MVCEventActor::MVC_ACTOR_VIEW, MVCEvent::MVCEventActor::MVC_ACTOR_MODEL);
      case MVCEventFactory::EventSrcDst::EVT_C2V:
        return std::make_pair(MVCEvent::MVCEventActor::MVC_ACTOR_CONTROLLER, MVCEvent::MVCEventActor::MVC_ACTOR_VIEW);
      case MVCEventFactory::EventSrcDst::EVT_C2M:
        return std::make_pair(MVCEvent::MVCEventActor::MVC_ACTOR_CONTROLLER, MVCEvent::MVCEventActor::MVC_ACTOR_MODEL);
      case MVCEventFactory::EventSrcDst::EVT_M2V :
        return std::make_pair(MVCEvent::MVCEventActor::MVC_ACTOR_MODEL, MVCEvent::MVCEventActor::MVC_ACTOR_VIEW);
      default:
        assert(aSrcDst == MVCEventFactory::EventSrcDst::EVT_M2C);
        return std::make_pair(MVCEvent::MVCEventActor::MVC_ACTOR_MODEL, MVCEvent::MVCEventActor::MVC_ACTOR_CONTROLLER);
    }
  }//getSrcDstActors
  
  MVCEventFactory::EventSrcDst MVCEventFactory::actorsToSrcDst(MVCEvent::MVCEventActor aAct1,
                                                               MVCEvent::MVCEventActor aAct2)
  {
    if(aAct1 == MVCEvent::MVCEventActor::MVC_ACTOR_VIEW)
    {
      if(aAct2 == MVCEvent::MVCEventActor::MVC_ACTOR_CONTROLLER)
      {
        return MVCEventFactory::EventSrcDst::EVT_V2C;
      }
      else
      {
        assert(aAct2 == MVCEvent::MVCEventActor::MVC_ACTOR_MODEL);
        return MVCEventFactory::EventSrcDst::EVT_V2M;
      }
    }
    else if(aAct1 == MVCEvent::MVCEventActor::MVC_ACTOR_CONTROLLER)
    {
      if(aAct2 == MVCEvent::MVCEventActor::MVC_ACTOR_VIEW)
      {
        return MVCEventFactory::EventSrcDst::EVT_C2V;
      }
      else
      {
        assert(aAct2 == MVCEvent::MVCEventActor::MVC_ACTOR_MODEL);
        return MVCEventFactory::EventSrcDst::EVT_C2M;
      }
    }
    else
    {
      assert(aAct2 == MVCEvent::MVCEventActor::MVC_ACTOR_MODEL);
      if(aAct2 == MVCEvent::MVCEventActor::MVC_ACTOR_VIEW)
      {
        return MVCEventFactory::EventSrcDst::EVT_M2V;
      }
      else
      {
        assert(aAct2 == MVCEvent::MVCEventActor::MVC_ACTOR_CONTROLLER);
        return MVCEventFactory::EventSrcDst::EVT_M2C;
      }
    }
  }//actorsToSrcDst
  
  MVCEventEvalStmt* MVCEventFactory::mvcEventEvalStmt(const CLI::CLIString& aStmt, EventSrcDst aSD)
  {
    auto sd = getSrcDstActors(aSD);
    return new MVCEventEvalStmt(aStmt, sd.first, sd.second);
  }//mvcEventEvalStmt
  
  MVCEventEvalStmt* MVCEventFactory::mvcEventEvalStmt(const CLI::CLIString& aStmt,
                                                      const CLI::CLIString& aSrcStmt, EventSrcDst aSD)
  {
    auto e = mvcEventEvalStmt(aStmt, aSD);
    e->setSourceStmt(aSrcStmt);
    
    return e;
  }//mvcEventEvalStmt
  
  MVCEventEvaluateCodeObject* MVCEventFactory::mvcEventEvalCodeObject(const Interpreter::DataObject& aDataObject,
                                                                      EventSrcDst aSD)
  {
    auto sd = getSrcDstActors(aSD);
    return new MVCEventEvaluateCodeObject(aDataObject, sd.first, sd.second);
  }//mvcEventEvalCodeObject
  
  MVCEventSolution* MVCEventFactory::mvcEventSolution(std::size_t aNum,
                                                      const std::list<std::string>& aList,
                                                      EventSrcDst aSD)
  {
    auto sd = getSrcDstActors(aSD);
    return new MVCEventSolution(aNum, aList, sd.first, sd.second);
  }//mvcEventSolution
  
  MVCEventNoSolution* MVCEventFactory::mvcEventNoSolution(EventSrcDst aSD)
  {
    auto sd = getSrcDstActors(aSD);
    return new MVCEventNoSolution(sd.first, sd.second);
  }//mvcEventNoSolution
  
  MVCEventWriteOutput* MVCEventFactory::mvcEventWriteOutput(const std::list<std::string>& aStringList,
                                                            EventSrcDst aSD)
  {
    auto sd = getSrcDstActors(aSD);
    return new MVCEventWriteOutput(aStringList, sd.first, sd.second);
  }//mvcEventWriteOutput
  
  MVCEventError* MVCEventFactory::mvcEventError(const std::string& aErrMsg, EventSrcDst aSD)
  {
    auto sd = getSrcDstActors(aSD);
    return new MVCEventError(aErrMsg, MVCEventError::ErrorLevel::LEVEL_ERROR, sd.first, sd.second);
  }//mvcEventErrorMdlToCtr
  
  MVCEventError* MVCEventFactory::mvcEventWarning(const std::string& aErrMsg, EventSrcDst aSD)
  {
    auto sd = getSrcDstActors(aSD);
    return new MVCEventError(aErrMsg, MVCEventError::ErrorLevel::LEVEL_WARNING, sd.first, sd.second);
  }//mvcEventErrorMdlToCtr
  
  MVCEventReadFile* MVCEventFactory::mvcEventReadFile(const std::string& aPath, EventSrcDst aSD)
  {
    auto sd = getSrcDstActors(aSD);
    return new MVCEventReadFile(aPath, sd.first);
  }//mvcEventErrorMdlToCtr
  
  MVCEventChangeOutStream* MVCEventFactory::mvcEventChangeOutStream(std::ostream* aOStream,
                                                                    EventSrcDst aSD)
  {
    auto sd = getSrcDstActors(aSD);
    auto event = new MVCEventChangeOutStream(false, sd.first, sd.second);
    event->setOutStream(aOStream);
    return event;
  }//mvcEventChangeOutStream
  
  MVCEventChangeOutStream* MVCEventFactory::mvcEventResetOutStream(EventSrcDst aSD)
  {
    auto sd = getSrcDstActors(aSD);
    auto event = new MVCEventChangeOutStream(true, sd.first, sd.second);
    return event;
  }//mvcEventResetOutStream
  
  MVCEventMetrics* MVCEventFactory::mvcEventMetrics()
  {
    return new MVCEventMetrics(MVCEvent::MVCEventActor::MVC_ACTOR_MODEL,
                               MVCEvent::MVCEventActor::MVC_ACTOR_CONTROLLER);
  }//mvcEventMetrics
  
}//end namespace MVC
