// Base class
#include "MVCEvent.hpp"

#include "GlobalsSysLib.hpp"
#include <cassert>

namespace MVC {
  
  MVCEvent::MVCEvent(MVCEventActor aSrcEvent, MVCEventActor aDstEvent, MVCEventType aEventType)
  : pEventType(aEventType)
  , pEventSrc(aSrcEvent)
  , pEventDst(aDstEvent)
  {
  }
  
}//end namespace MVC
