// Base class
#include "MVCEventDispatcher.hpp"

// Terminate
#include "BaseUtils.hpp"

#include <algorithm>
#include <cassert>

namespace MVC {
  
  MVCEventDispatcher::MVCEventDispatcher()
  : pEventDispatcherLock(false)
  {
  }
  
  void MVCEventDispatcher::registerEventHandler(std::function<void(MVCEvent*)> aFcn, MVCEventType aEventType)
  {
    pHandlerMap[static_cast<int>(aEventType)] = aFcn;
  }//registerEventHandler
  
  void MVCEventDispatcher::clearEventQueue()
  {
    std::queue<std::shared_ptr<MVCEvent>>().swap( pEventQueue );
    pEventDispatcherLock = false;
  }//clearEventQueue
  
  void MVCEventDispatcher::dispatch(const std::shared_ptr<MVCEvent>& aEvent)
  {
    assert(aEvent);
    
    // Push event into queue for dispatching
    pEventQueue.push(aEvent);
    
    // If the dispatcher is locked return.
    // @note the events has been already stored in the queue
    if (pEventDispatcherLock)
    {
      return;
    }
    
    // Lock dispatcher and start dispatching events to function handlers
    pEventDispatcherLock = true;
    
    while (!pEventQueue.empty())
    {
      // Get next event in queue
      auto qEvent = pEventQueue.front();
      pEventQueue.pop();
      assert(qEvent);
      
      auto eventType = qEvent->getEventType();
      switch (eventType)
      {
        case MVC::MVC_EVT_EVAL_CODE_OBJ:
        case MVC::MVC_EVT_EVAL_STMT:
        case MVC::MVC_EVT_SOLUTION:
        case MVC::MVC_EVT_METRICS:
        case MVC::MVC_EVT_WRITE_OUTPUT:
        case MVC::MVC_EVT_NO_SOLUTION:
        case MVC::MVC_EVT_READ_FILE:
        case MVC::MVC_EVT_CHANGE_OUTSTREAM:
        case MVC::MVC_EVT_ERROR:
          dipatchEventToFunctor(qEvent.get());
          break;
        default:
          ASSERT_ALWAYS("Unsupported event");
          break;
      }
    }//while
    
    // Unlock dispatcher
    pEventDispatcherLock = false;
  }//dispatch
  
  boost::optional<EventHandlerFcn> MVCEventDispatcher::findEventHandler(int aEventType) const
  {
    if (pHandlerMap.find(aEventType) == pHandlerMap.end())
    {
      return boost::optional<EventHandlerFcn>();
    }
    return boost::optional<EventHandlerFcn>(pHandlerMap.at(aEventType));
  }//findEventHandler
  
  void MVCEventDispatcher::dipatchEventToFunctor(MVCEvent* aEvent)
  {
    assert(aEvent);
    auto eventType = static_cast<int>(aEvent->getEventType());
    
    // If no event handler is assigned to eventType, return
    auto handler = findEventHandler(eventType);
    if (!handler)
    {
      return;
    }
    
    // Execute handler on event
    (*handler)(aEvent);
  }//dipatchEventToFunctor
  
}//end namespace MVC
