// Self first
#include "GhostConstraint.hpp"

#include <algorithm> // for std::find_if

int ghost::GhostConstraint::NBER_CTR = 0;

namespace ghost {
  
  GhostConstraint::GhostConstraint(const VariablesList& variables)
  : variables( variables )
  , id( NBER_CTR++ )
  {
  }
  
  bool GhostConstraint::has_variable(const GhostVariable& var) const
  {
    auto it = std::find_if(variables.cbegin(),
                           variables.cend(),
                           [&](const std::reference_wrapper<GhostVariable>& v) {
                             return v.get().get_id() == var.get_id();
                           });
    
    return it != variables.cend();
  }// has_variable

}// namespace ghost
