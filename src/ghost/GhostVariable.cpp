// Self first
#include "GhostVariable.hpp"

#include <algorithm>
#include <typeinfo>

int ghost::GhostVariable::NBER_VAR = 0;

namespace ghost {
  
  GhostVariable::GhostVariable()
  : _id( -1 )
  , _name("")
  , _shortName("")
  , _domain(GhostDomain(0, 0))
  , _index(-1)
  , _cache_value(-1)
  {
  }
  
  GhostVariable::GhostVariable(const std::string& name, const std::string& shortName,
                               const GhostDomain& domain, INT_64 index)
  : _id( NBER_VAR++ )
  , _name( name )
  , _shortName( shortName )
  , _domain( domain )
  , _index( index )
  , _cache_value( domain.get_value( index ) )
  {
  }

  GhostVariable::GhostVariable(const std::string& name, const std::string& shortName,
                               const std::vector<INT_64>& domain, INT_64 index)
  : GhostVariable(name, shortName, GhostDomain{domain}, index)
  {
  }
  
  GhostVariable::GhostVariable(const std::string& name, const std::string& shortName,
                               INT_64 startValue, std::size_t size, INT_64 index)
  : GhostVariable(name, shortName, GhostDomain{startValue, size}, index)
  {
  }
  
  GhostVariable::GhostVariable(const GhostVariable &other)
  : _id( other._id )
  , _name( other._name )
  , _shortName  ( other._shortName )
  , _domain  ( other._domain )
  , _index  ( other._index )
  , _cache_value( other._cache_value )
  {
  }
  
  GhostVariable& GhostVariable::operator=(GhostVariable other)
  {
    this->swap( other );
    return *this;
  }
  
  void GhostVariable::swap(GhostVariable& other)
  {
    std::swap(this->_id, other._id);
    std::swap(this->_name, other._name);
    std::swap(this->_shortName, other._shortName);
    std::swap(this->_domain, other._domain);
    std::swap(this->_index, other._index);
    std::swap(this->_cache_value, other._cache_value);
  }// swap
  
  void GhostVariable::do_random_initialization()
  {
    set_value(_domain.random_value());
  }// do_random_initialization
  
}// namespace ghost
