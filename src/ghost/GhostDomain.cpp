// Self first
#include "GhostDomain.hpp"

#include <typeinfo>
#include <numeric>

namespace ghost {
  
  GhostDomain::GhostDomain(const std::vector<INT_64>& domain)
  : _domain(domain)
  , _minValue(*std::min_element(_domain.begin(), _domain.end()))
  , _maxValue(*std::max_element(_domain.begin(), _domain.end()))
  , _size(domain.size())
  {
    _indexes = std::vector<INT_64>(_maxValue - _minValue + 1, -1);
    for (int i = 0 ; i < static_cast<int>(_size); ++i)
    {
      _indexes[ _domain[ i ] - _minValue ] = i;
    }
  }
  
  GhostDomain::GhostDomain(INT_64 startValue, std::size_t size)
  : _domain(std::vector<INT_64>(size))
  , _minValue(startValue)
  , _maxValue(startValue + static_cast<int>(size) - 1)
  , _size(size)
  {
    std::iota(std::begin(_domain), std::end(_domain), startValue);
    
    _indexes = std::vector<INT_64>(_size, -1);
    for (int i = 0 ; i < static_cast<int>(_size); ++i)
    {
      _indexes[ _domain[ i ] - _minValue ] = i;
    }
  }
  
  GhostDomain::GhostDomain(const GhostDomain& other)
  : _domain(other._domain)
  , _indexes(other._indexes)
  , _minValue(other._minValue)
  , _maxValue(other._maxValue)
  , _size(other._size)
  , _random(other._random)
  {
  }
  
  GhostDomain::GhostDomain(GhostDomain&& other)
  : _domain(other._domain)
  , _indexes(other._indexes)
  , _minValue(other._minValue)
  , _maxValue(other._maxValue)
  , _size(other._size)
  , _random(other._random)
  {
    other._domain.clear();
    other._indexes.clear();
    other._size = 0;
  }
  
  GhostDomain& GhostDomain::operator=(GhostDomain other)
  {
    this->swap(other);
    return *this;
  }
  
  void GhostDomain::swap(GhostDomain &other)
  {
    std::swap(this->_domain, other._domain);
    std::swap(this->_indexes, other._indexes);
    std::swap(this->_minValue, other._minValue);
    std::swap(this->_maxValue, other._maxValue);
    std::swap(this->_size, other._size);
    std::swap(this->_random, other._random);
  }// swap
  
  INT_64 GhostDomain::get_value(INT_64 index) const
  {
    if (index >=0 && index < static_cast<int>(_size))
    {
      return _domain[ index ];
    }
    else
    {
      throw indexException();
    }
  }// get_value

  INT_64 GhostDomain::index_of(INT_64 value) const
  {
    if (value < _minValue || value > _maxValue)
      throw valueException();
    
    const INT_64 index = _indexes[ value - _minValue ];
    if (index == -1)
    {
      throw valueException();
    }
    else
    {
      return index;
    }
  }// index_of
  
}// namespace ghost
