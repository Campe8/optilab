// Self first
#include "GhostObjective.hpp"

#include <limits>  // for std::numeric_limits

namespace ghost
{
  GhostObjective::GhostObjective( const std::string& name )
  : name(name)
  {
  }
  
  INT_64 GhostObjective::expert_heuristic_value( const std::vector<GhostVariable>& variables,
                                                 GhostVariable& var,
                                                 const std::vector<INT_64>& possible_values ) const
  {
    double minCost = std::numeric_limits<double>::max();
    double simulatedCost;
    
    const auto backup = var.get_value();
    std::vector<INT_64> bestValues;
    
    for ( auto& v : possible_values )
    {
      var.set_value( v );
      simulatedCost = cost( variables );
      
      if( minCost > simulatedCost )
      {
        minCost = simulatedCost;
        bestValues.clear();
        bestValues.push_back( v );
      }
      else
      {
        if ( minCost == simulatedCost )
        {
          bestValues.push_back( v );
        }
      }
    }
    
    var.set_value( backup );
    
    return bestValues[ random.getRandomNumber( static_cast<int>(bestValues.size()) ) ];
  }// expert_heuristic_value
  
  GhostVariable
  GhostObjective::expert_heuristic_value( const std::vector<GhostVariable>& bad_variables ) const
  {
    return bad_variables[ random.getRandomNumber( static_cast<int>(bad_variables.size()) ) ];
  }// expert_heuristic_value
  
  void GhostObjective::expert_postprocess_satisfaction( std::vector<GhostVariable>& variables,
                                                        double& bestCost,
                                                        std::vector<INT_64>& solution ) const
  {
  }// expert_postprocess_satisfaction
  
  void GhostObjective::expert_postprocess_optimization( std::vector<GhostVariable>& variables,
                                                        double& bestCost,
                                                        std::vector<INT_64>& solution ) const
  {
  }
  
}// namespace ghost
