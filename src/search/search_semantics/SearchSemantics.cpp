// Self first
#include "SearchSemantics.hpp"

#include "VariableSemanticObjective.hpp"
#include "VariableExplosionTools.hpp"
#include "DomainElementVoid.hpp"

#include "NogoodConstraint.hpp"

#include <memory>
#include <cassert>

namespace Search {
  
  SearchSemantics::SearchSemantics()
  : pGoal(nullptr)
  , pNogoodLeaning(false)
  , pBranchingConstraintFactory(new Core::BranchingConstraintFactory())
  {
    pGoalValue.clear();
    pConstraintStore = std::make_shared<Semantics::ConstraintStore>();
  }
  
  void SearchSemantics::setVariableObjective(const Core::VariableSPtr& aObjective)
  {
    assert(aObjective && Core::VariableSemanticObjective::isa(aObjective->variableSemantic()));
    pGoal = aObjective;
    pExplodedGoalList = Core::Tools::explodeVariable(aObjective);
  }//setVariableObjective
  
  void SearchSemantics::optimizeObjective()
  {
    // Remove old optimization constraints if any
    pOptimizationConstraintSet.clear();
    
    // Create the new optimization constraint set based on the current
    // values of the domains of the objective variables
    for (auto& objVar : pExplodedGoalList)
    {
      assert(objVar->hasPrimitiveType() && (pGoalValue.find(objVar->getVariableNameId()) != pGoalValue.end()));
      assert(Core::VariableSemanticObjective::isa(objVar->variableSemantic()));
      
      auto objExtremum = Core::VariableSemanticObjective::cast(objVar->variableSemantic())->getObjectiveExtremum();
      switch (objExtremum)
      {
        case Core::VariableSemanticObjective::ObjectiveExtremum::OBJ_EXT_MINIMIZE:
        {
          // Obj <= Obj.LB - 1
          auto branchingVal = (pGoalValue[objVar->getVariableNameId()][0].first)->predecessor();
          assert(branchingVal && !Core::DomainElementVoid::isa(branchingVal));
          
          pOptimizationConstraintSet.insert(pBranchingConstraintFactory->
                                            branchingConstraint(objVar, branchingVal,
                                                                Core::BranchingConstraintId::BRC_CON_ID_DOMAIN_SPLIT_LQ));
        }
          break;
        default:
        {
          // Obj > Obj.UB
          assert(objExtremum == Core::VariableSemanticObjective::ObjectiveExtremum::OBJ_EXT_MAXIMIZE);
          
          auto branchingVal = pGoalValue[objVar->getVariableNameId()][0].second;
          assert(branchingVal && !Core::DomainElementVoid::isa(branchingVal));
          pOptimizationConstraintSet.insert(pBranchingConstraintFactory->
                                            branchingConstraint(objVar, branchingVal,
                                                                Core::BranchingConstraintId::BRC_CON_ID_DOMAIN_SPLIT_GT));
        }
          break;
      }//switch
    }//for
    
    // Subscribe objective constraints to the constraint store
    for (auto& objConstraint : pOptimizationConstraintSet)
    {
      objConstraint->subscribeToConstraintStore(pConstraintStore);
    }
  }// optimizeObjective
  
  void SearchSemantics::storeNogood(const std::vector<Core::VariableSPtr>& aNogoodVars,
                                    const std::vector<Core::DomainSPtr>& aNogoodDoms)
  {
    auto nogood = std::make_shared<Core::NogoodConstraint>(aNogoodVars, aNogoodDoms);
    pNogoodStore.push_back(nogood);
    pNogoodStore.back()->subscribeToConstraintStore(pConstraintStore);
  }//storeNogood
  
  void SearchSemantics::forceObjectiveReevaluation()
  {
    for (auto& objConstraint : pOptimizationConstraintSet)
    {
      objConstraint->forceReevaluation();
    }
  }//forceNogoodsReevaluation
  
  void SearchSemantics::forceNogoodsReevaluation()
  {
    for (auto& nogood : pNogoodStore)
    {
      nogood->forceReevaluation();
    }
  }//forceNogoodsReevaluation
  
}// end namespace Search
