// Self first
#include "SolutionManager.hpp"

#include "VariableExplosionTools.hpp"
#include "SearchEnvironment.hpp"

// Domain
#include "Domain.hpp"
#include "DomainElement.hpp"

#include <boost/logic/tribool.hpp>
#include <iostream>
#include <sstream>
#include <cassert>

namespace Search {
  
  SolutionManager::SolutionManager(const VarMapSPtr& aVarMap)
  : pMetaSolutionLimit(SM_SOLUTION_LIMIT_SET)
  , pOutputVariables(false)
  , pSolutionLimit(1)
  , pSolutionsCounter(0)
  , pVarMap(aVarMap)
  {
    registerVariables();
  }
  
  /// Increments solution counter after checks,
  /// for example, after checking the output set size
  void SolutionManager::incrementSolutionCounter()
  {
    // Check if the set of branching variables is empty.
    // If not, increment the solution counter
    if(!pBrcVarList.empty() || pOutputVariables) pSolutionsCounter++;
  }//incrementSolutionCounter
  
  void SolutionManager::registerVariables()
  {
    assert(pVarMap);
    pSolVarList.reserve(varMap().size());
    for (auto& it : varMap())
    {
      auto var = it.second.get();
      if (var->variableBaseSemantic()->isSolutionElement())
      {
        pSolVarList.push_back(it.first);
        
        if(var->hasPrimitiveType())
        {
          pSolutionRegister[var->getVariableNameId()].resize(1);
        }
        else
        {
          assert(pExplodedVarSet.find(var->getVariableNameId()) == pExplodedVarSet.end());
          auto& varSetEntry = pExplodedVarSet[var->getVariableNameId()];
          varSetEntry = Core::Tools::explodeVariable(it.second);
          pSolutionRegister[var->getVariableNameId()].resize(varSetEntry.size());
        }
      }//if
      
      // Check output variables
      if(var->variableSemantic()->isOutput() ||
         boost::indeterminate(var->variableSemantic()->isOutput())) pOutputVariables = true;
      
    }
  }//registerVariables
  
  void SolutionManager::setSearchEnvironment(const SearchEnvironment* aSEnv)
  {
    assert(aSEnv);
    
    pBrcVarList.clear();
    for (auto& var : aSEnv->getBranchingVariables())
    {
      assert(var);
      
      auto vID = var->getVariableNameId();
      assert(varMap().find(vID) != varMap().end());
      
      pBrcVarList.push_back(vID);
    }
  }//setSearchEnvironment
  
  void SolutionManager::setSolutionLimit(SolutionCtr aSolutionLimit)
  {
    pMetaSolutionLimit = SM_SOLUTION_LIMIT_SET;
    if (aSolutionLimit < SM_SOLUTION_LIMIT_SET)
    {
      pMetaSolutionLimit = static_cast<SolutionCtr>(SM_SOLUTION_LIMIT_NONE);
      return;
    }
    pSolutionLimit = aSolutionLimit;
  }// setSolutionLimit
  
  Core::Variable* SolutionManager::getVariableFromMap(const std::string aID)
  {
    if (varMap().find(aID) == varMap().end()) return nullptr;
    return varMap().at(aID).get();
  }//getVariableFromMap
  
  void SolutionManager::resetRegister()
  {
    pSolutionsCounter = 0;
    
    for(auto& it : pSolutionRegister)
    {
      auto& solDesc = it.second;
      for(auto& domList : solDesc)
      {
        domList.clear();
      }
    }
  }//resetRegister
  
  SolutionStatus SolutionManager::notify(bool aForceDuplicateSolutionCheck)
  {
    // Register the current variable configuration/domains
    // into the solution set
    registerSolution(aForceDuplicateSolutionCheck);
    
    /*
     * No variables or no solutions to record,
     * return as last solution without incrementing
     * the internal solution counter.
     */
    if(pBrcVarList.empty() || pSolutionLimit == 0)
    {
      return SolutionStatus::SOLUTION_LAST;
    }
    
    // Check validity of notified solution
    if (!checkSolutionSoundness())
    {
      return SolutionStatus::SOLUTION_NOT_VALID;
    }
    
    if (needsMoreSolutions())
    {
      return SolutionStatus::SOLUTION_NEED_MORE;
    }
    
    return SolutionStatus::SOLUTION_LAST;
  }//notify
  
  bool SolutionManager::checkSolutionSoundness()
  {
    for (auto& ID : pBrcVarList)
    {
      auto var = getVariableFromMap(ID);
      assert(var);
      
      if (var->variableBaseSemantic()->isSolutionElement() && !var->isAssigned())
      {
        return false;
      }
    }
    return true;
  }// checkSolutionSoundness
  
  void SolutionManager::addBoundPairToSolutionDescriptor(Core::Variable* aVar, std::size_t aVarIdx,
                                                         SolutionDescriptor& aSolDesc)
  {
    auto domain = aVar->domain();
    assert(domain);
    
    // Add the solution
    if (!needsMoreSolutions())
    {
      // Replace old solutions in RR fashion the limit of solutions to
      // record has been already reached
      auto solutionIdx = static_cast<std::size_t>(pSolutionsCounter % pSolutionLimit);
      assert(solutionIdx < aSolDesc[aVarIdx].size());
      
      aSolDesc[aVarIdx][solutionIdx] = std::make_pair(domain->lowerBound(), domain->upperBound());
    }
    else
    {
      aSolDesc[aVarIdx].push_back(std::make_pair(domain->lowerBound(), domain->upperBound()));
    }
  }//addBoundPairToSolutionDescriptor
  
  void SolutionManager::registerSolution(bool aForceDuplicateSolutionCheck)
  {
    if (aForceDuplicateSolutionCheck && getSolutionLimit() > 1)
    {
      auto solutionHash = calculateSolutionHash();
      if (pSolutionHashSet.find(solutionHash) != pSolutionHashSet.end())
      {
        // A solution with same hash has been already calculated, return
        return;
      }
      
      // Insert the hash in the set of solution hashes
      pSolutionHashSet.insert(solutionHash);
    }
    
    for (auto& ID : pSolVarList)
    {
      assert(pSolutionRegister.find(ID) != pSolutionRegister.end());
      auto& solDesc = pSolutionRegister[ID];
      
      if (pExplodedVarSet.find(ID) != pExplodedVarSet.end())
      {
        auto& varSet = pExplodedVarSet[ID];
        assert(varSet.size() == solDesc.size());
        
        std::size_t idx{0};
        for(auto& varPtr : varSet)
        {
          auto var = varPtr.get();
          addBoundPairToSolutionDescriptor(var, idx++, solDesc);
        }
      }
      else
      {
        auto var = getVariableFromMap(ID);
        addBoundPairToSolutionDescriptor(var, 0, solDesc);
      }
    }//for
    
    incrementSolutionCounter();
  }// registerSolution
  
  std::string SolutionManager::calculateSolutionHash()
  {
    std::string solutionHash;
    for (const auto& ID : pSolVarList)
    {
      assert(pSolutionRegister.find(ID) != pSolutionRegister.end());
      const auto& solDesc = pSolutionRegister[ID];
      if (pExplodedVarSet.find(ID) != pExplodedVarSet.end())
      {
        const auto& varSet = pExplodedVarSet[ID];
        assert(varSet.size() == solDesc.size());
        
        for(auto& varPtr : varSet)
        {
          solutionHash += varPtr->getVariableNameId() +
          varPtr->domain()->lowerBound()->toString() + varPtr->domain()->upperBound()->toString();
        }
      }
      else
      {
        auto var = getVariableFromMap(ID);
        
        solutionHash += var->getVariableNameId() +
        var->domain()->lowerBound()->toString() + var->domain()->upperBound()->toString();
      }
    }
    
    return solutionHash;
  }//calculateSolutionHash
  
  SolutionManager::Solution SolutionManager::getSolution(std::size_t aIdx)
  {
    SolutionCtr solIdx = aIdx - 1;
    assert(solIdx < static_cast<SolutionCtr>(getNumberOfRecordedSolutions()));
    
    Solution sol;
    if (solIdx < 0) return sol;
    
    for (auto& ID : pSolVarList)
    {
      assert(pSolutionRegister.find(ID) != pSolutionRegister.end());
      auto& solDesc = pSolutionRegister[ID];
      
      if(pExplodedVarSet.find(ID) != pExplodedVarSet.end())
      {
        for(std::size_t idx{0}; idx < pExplodedVarSet[ID].size(); ++idx)
        {
          sol[ID].push_back({ solDesc[idx][aIdx - 1].first, solDesc[idx][aIdx - 1].second });
        }
      }
      else
      {
        assert(solIdx < solDesc[0].size());
        sol[ID] = { { solDesc[0][aIdx - 1].first, solDesc[0][aIdx - 1].second } };
      }
    }

    return sol;
  }//getSolution
  
}// end namespace Search
