// Base class
#include "BranchingStrategyDomainSplitting.hpp"

#include <cassert>

namespace Search {

  BranchingStrategyDomainSplitting::BranchingStrategyDomainSplitting(Core::BranchingConstraintFactoryPtr aBCFactory)
    : BranchingStrategy(BranchingStrategyType::BRANCHING_DOMAIN_SPLITTING, aBCFactory)
  {
  }

  BranchingStrategyDomainSplitting::~BranchingStrategyDomainSplitting()
  {
  }

  BranchingChoiceSet BranchingStrategyDomainSplitting::getBranchingSet(SearchHeuristicPtr aSearchHeuristic)const
  {
    assert(aSearchHeuristic);
    BranchingChoiceSet bcSet;
    
    // 2-step process:
    // 1) Set variable to branch on
    bcSet.first = aSearchHeuristic->getChoiceVariable();
    
    // 2) Get corresponding domain element
    Core::DomainElement *branchingElement = aSearchHeuristic->getChoiceValue();
    
    auto aBCFactory = getBranchingConstraintFactory();
    assert(aBCFactory);
    
    // Set constraint 1) x <= d
    bcSet.second.push_back(aBCFactory->branchingConstraint(bcSet.first, branchingElement,
                                                           Core::BranchingConstraintId::BRC_CON_ID_DOMAIN_SPLIT_LQ));
    // Set constraint 2) x != d
    bcSet.second.push_back(aBCFactory->branchingConstraint(bcSet.first, branchingElement,
                                                           Core::BranchingConstraintId::BRC_CON_ID_DOMAIN_SPLIT_GT));
    
    return bcSet;
  }//getBranchingSet

}// end namespace Search
