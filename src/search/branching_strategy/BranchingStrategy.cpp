// Base class
#include "BranchingStrategy.hpp"

#include <cassert>

namespace Search {
  
  BranchingStrategy::BranchingStrategy(BranchingStrategyType aBranchingStrategyType,
                                       Core::BranchingConstraintFactoryPtr aBCFactor)
  : pBranchingStrategyType(aBranchingStrategyType)
  , pBranchingConstraintFactory(aBCFactor)
  {
  }
  
  BranchingStrategy::~BranchingStrategy()
  {
  }
  
}// end namespace Search

