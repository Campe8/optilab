// Base class
#include "BranchingStrategyBinaryChoicePoint.hpp"

#include <cassert>

namespace Search {

  BranchingStrategyBinaryChoicePoint::BranchingStrategyBinaryChoicePoint(Core::BranchingConstraintFactoryPtr aBCFactory)
    : BranchingStrategy(BranchingStrategyType::BRANCHING_BINARY_CHOICE_POINT, aBCFactory)
  {
  }

  BranchingStrategyBinaryChoicePoint::~BranchingStrategyBinaryChoicePoint()
  {
  }

  BranchingChoiceSet BranchingStrategyBinaryChoicePoint::getBranchingSet(SearchHeuristicPtr aSearchHeuristic) const
  {
    assert(aSearchHeuristic);
    BranchingChoiceSet bcSet;
    
    // 2-step process:
    // 1) Set variable to branch on
    bcSet.first = aSearchHeuristic->getChoiceVariable();
    
    // 2) Get corresponding domain element
    Core::DomainElement *branchingElement = aSearchHeuristic->getChoiceValue();
    
    auto aBCFactory = getBranchingConstraintFactory();
    assert(aBCFactory);
    
    // Set constraint 1) x == d
    bcSet.second.push_back(aBCFactory->branchingConstraint(bcSet.first, branchingElement,
                                                           Core::BranchingConstraintId::BRC_CON_ID_BINARY_CHOICE_EQ));
    // Set constraint 2) x != d
    bcSet.second.push_back(aBCFactory->branchingConstraint(bcSet.first, branchingElement,
                                                           Core::BranchingConstraintId::BRC_CON_ID_BINARY_CHOICE_NQ));
    
    return bcSet;
  }//getBranchingSet

}// end namespace Search





