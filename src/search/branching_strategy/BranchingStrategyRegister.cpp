// Base class
#include "BranchingStrategyRegister.hpp"

// Branching strategies
#include "BranchingStrategyBinaryChoicePoint.hpp"
#include "BranchingStrategyDomainSplitting.hpp"

#include <cassert>

namespace Search {

  BranchingStrategyRegister::BranchingStrategyRegister()
  : pBranchingConstraintFactory(new Core::BranchingConstraintFactory())
  {
    // Init strategies
    registerStrategies();
  }

  BranchingStrategyRegister::~BranchingStrategyRegister()
  {
  }
  
  BranchingStrategyPtr BranchingStrategyRegister::branchingStrategy(BranchingStrategyType aBrcStrType)
  {
    int registerKey = static_cast<int>(aBrcStrType);
    assert(pBranchingStrategyRegister.find(registerKey) != pBranchingStrategyRegister.end());
    
    return pBranchingStrategyRegister[registerKey].get();
  }//branchingStrategy

  void BranchingStrategyRegister::registerStrategies()
  {
    pBranchingStrategyRegister[static_cast<int>(BranchingStrategyType::BRANCHING_BINARY_CHOICE_POINT)].reset(new BranchingStrategyBinaryChoicePoint(pBranchingConstraintFactory.get()));
    pBranchingStrategyRegister[static_cast<int>(BranchingStrategyType::BRANCHING_DOMAIN_SPLITTING)].reset(new BranchingStrategyDomainSplitting(pBranchingConstraintFactory.get()));
  }//registerStrategies

}// end namespace Search
