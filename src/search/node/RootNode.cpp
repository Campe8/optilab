// Base class
#include "RootNode.hpp"

#include <cassert>

namespace Search {

  RootNode::RootNode(const SearchHeuristicSPtr& aSearchHeuristic, const Semantics::ConstraintStoreSPtr& aConstraintStore)
    : Node(0)
    , pBranchingPathRoot(new BranchingPath())
    , pSearchHeuristic(aSearchHeuristic)
  {
    assert(aSearchHeuristic);
    
    // Set search heuristic
    setSearchHeuristic(pSearchHeuristic.get());
    
    // Get variable to branch on
    Core::VariableSPtr branchingVar = pSearchHeuristic->getChoiceVariable();
    
    // If no branching variable is available
    // return since the search tree as only the root node
    if (!branchingVar)
    {
      return;
    }
    setBranchingVariable(branchingVar);

    // Instantiate and set a branching strategy register
    pBranchingStrategyRegister = std::make_shared<BranchingStrategyRegister>();
    setBranchingStrategyRegister(pBranchingStrategyRegister.get());

    // Set new Branching path starting from the root node
    setBranchingPath(pBranchingPathRoot.get());

    // Set constraint store
    setConstraintStore(aConstraintStore);

    /*
     * Extend this node here,
     * i.e., this node is extended right after
     * creation without any constraint propagation.
     * This will create all the starting branches
     * extending the root of the search tree.
     */
    assert(hasSuccessor());
    extendNode();

    /*
     * Branching variable and next branching variable should be the
     * same since no propagation happened.
     */
    assert(getNextBranchingVariable().get() == branchingVar.get());
  }

  RootNode::~RootNode()
  {
    cleanupTree();
  }
  
  void RootNode::cleanupTree()
  {
    // Deregister any constraint from the constraint store
    // if any is still present in the branching path
    assert(pBranchingPathRoot);
    for(auto& choice : *pBranchingPathRoot)
    {
      if(choice.second)
      {
        choice.second->unsubscribeFromConstraintStore();
        (*pBranchingPathRoot)[choice.first] = nullptr;
      }
    }
    
    // Reset the branching path to nullptr
    // so the destructor of node won't use a bad pointer
    pBranchingPathRoot.reset(nullptr);
    setBranchingPath(nullptr);
  }//cleanupTree
  
}// end namespace Search





