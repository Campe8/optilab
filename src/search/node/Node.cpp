// Base class
#include "Node.hpp"

#include "DomainElementManager.hpp"
#include "DomainElementInt64.hpp"

#include <cassert>

// #define DEBUG_NODE

namespace Search {
  
  Node::Node(std::size_t aSearchTreeLevel)
  : SearchChoice(SearchChoiceType::SEARCH_CHOICE_NODE)
  , pSearchTreeLevel(aSearchTreeLevel)
  , pNextBranchId(0)
  , pBranchingPath(nullptr)
  , pBranchingVar(nullptr)
  , pSearchHeuristic(nullptr)
  , pBranchingStrategyRegister(nullptr)
  {
    pDegreeId = std::make_pair(0, 0);
  }
  
  Node::Node(BranchingPathPtr aBranchingPath,
             const Core::VariableSPtr& aBranchingVar,
             SearchHeuristicPtr aSearchHeuristic,
             BranchingStrategyRegisterPtr aBrcStrRegister,
             const Semantics::ConstraintStoreSPtr& aConstraintStore,
             NodeDegreeId aDegreeId,
             std::size_t aSearchTreeLevel)
  : SearchChoice(SearchChoiceType::SEARCH_CHOICE_NODE)
  , pSearchTreeLevel(aSearchTreeLevel)
  , pNextBranchId(0)
  , pBranchingPath(aBranchingPath)
  , pBranchingVar(aBranchingVar)
  , pSearchHeuristic(aSearchHeuristic)
  , pBranchingStrategyRegister(aBrcStrRegister)
  , pConstraintStore(aConstraintStore)
  , pDegreeId(aDegreeId)
  {
    assert(pBranchingPath);
    assert(aSearchHeuristic);
  }
  
  Node::~Node()
  {
    // Delete this node which is at level "pSearchTreeLevel"
    // of the search tree.
    // Since "pBranchingPath" is shared between ALL nodes in the tree,
    // past, current, and future nodes, this path may contain pointers
    // to branching constraints that are removed from the constraint store
    // (for example, used in previous levels) but not detached from
    // from their observers.
    // The subject can notify the observers, and this can cause an assertion
    // since, on update, the constraint doesn't result registered anymore.
    // Here it should be safe to delete and therefore detached from observers
    // all the constraints from this level on.
    // @note it is safe to delete the constraint from this level
    // even if this is the first choice of this level since this
    // (branching) constraint will be replaced with the new branching constraint
    // for this level
    if(pBranchingPath)
    {
      for(; pSearchTreeLevel < (*pBranchingPath).size(); ++pSearchTreeLevel)
      {
        assert(pBranchingPath->find(pSearchTreeLevel) != pBranchingPath->end());
        (*pBranchingPath)[pSearchTreeLevel] = nullptr;
      }
    }
  }
  
  bool Node::isa(const SearchChoice* aChoice)
  {
    return aChoice &&
    aChoice->getSearchChoiceType() == SearchChoiceType::SEARCH_CHOICE_NODE;
  }//isa
  
  Node* Node::cast(SearchChoice* aChoice)
  {
    if (!isa(aChoice)) return nullptr;
    return static_cast<Node*>(aChoice);
  }//cast
  
  const Node* Node::cast(const SearchChoice* aChoice)
  {
    if (!isa(aChoice)) return nullptr;
    return static_cast<const Node*>(aChoice);
  }//cast
  
  bool Node::hasSuccessor()
  {
    /*
     * There is a successor for this node if:
     * 1) this node has not been branched yet and
     *    this not is not assigned: 
     *    there is at least one node, i.e., this node;
     * 2) this node has been branched and 
     *    the choice set has at least 1 node:
     *    a) the successor of the branched node
     */
    assert(pSearchHeuristic);
    
    if(!isNodeExtended())
    {
      // Case (1)
      if(pBranchingVar && !pBranchingVar->isAssigned())
      {
        return true;
      }
    }
    // Case (2)
    return  !pSearchHeuristic->isChoiceSetEmpty();
  }// hasSuccessor
  
  Node* Node::next()
  {
    if (!isNodeExtended())
    {
      assert(getBranchingSet().empty());
      
      /*
       * Init branching set.
       * @note this must be done here
       * and not at init time in constructor
       * since it should be done after propagation
       * took place and domains are shrinked.
       */
      extendNode();
    }
    
    /*
     * Return nullptr if there are no next nodes.
     * @note pNextBranchId is 1-based.
     */
    if(pNextBranchId > getBranchingSet().size())
    {
      return nullptr;
    }
    
    assert(!getBranchingSet().empty());
    
    // Extend the current path with the next branching
    // constraint, this actually defines the next node
    extendBranchingPath(getNextBranchingConstraint());
    
    // Return the new node extending this node
    // and updates the index of the next branch id
    // as side effect
    return new Node(pBranchingPath, getNextBranchingVariable(),
                    pSearchHeuristic, pBranchingStrategyRegister,
                    pConstraintStore,
                    std::make_pair(pNextBranchId++, getBranchingSet().size()),
                    pSearchTreeLevel+1);
  }// next
  
  void Node::extendNode()
  {
    assert(pBranchingStrategyRegister);
    if (!hasSuccessor())
    {
      /*
       * Cannot extend if there is no
       * successor for this node,
       * e.g., there is no variable to branch on
       */
      return;
    }
    
    assert(pBranchingVar->variableSemantic());
    auto branchingStrategy = pBranchingStrategyRegister->
    branchingStrategy(pBranchingVar->variableSemantic()->getBranchingStrategyType());
    
    assert(branchingStrategy);
    pBranchingChoiceSet = branchingStrategy->getBranchingSet(pSearchHeuristic);
    
    ++pNextBranchId;
  }//extendNode
  
  void Node::extendBranchingPath(const Core::BranchingConstraintSPtr& aBranchingConstraint)
  {
    assert(aBranchingConstraint);
    assert(pConstraintStore);
    
    // Register constraint into constraint store
    aBranchingConstraint->subscribeToConstraintStore(pConstraintStore);
    
    // Deregister previous branching constraint from constraint store if any
    if (pBranchingPath->find(pSearchTreeLevel) != pBranchingPath->end())
    {
      // The following check is needed since the branching path at
      // level "pSearchTreeLevel" can be nullptr.
      // This is because the destructor removes all the branching constraints
      // from this level on.
      // @note the branching constraint is not nullptr,
      // for example, when "pSearchTreeLevel" is 0
      if(pBranchingPath->operator[](pSearchTreeLevel))
      {
        pBranchingPath->operator[](pSearchTreeLevel)->unsubscribeFromConstraintStore();
      }
      
      // Remove previous constraints -> optimizes memory
      if (pNextBranchId - 1 > 0)
      {
        assert(static_cast<int>(pNextBranchId)-2 >= 0);
        assert(pNextBranchId - 2 < getBranchingSet().size());
        assert(getBranchingSet().at(pNextBranchId - 2) == pBranchingPath->operator[](pSearchTreeLevel));
        
        getBranchingSet()[pNextBranchId - 2] = nullptr;
      }
    }
    
#ifdef DEBUG_NODE
    std::cout << "L: " << pSearchTreeLevel << " - " <<
    aBranchingConstraint->prettyPrintConstraint() << std::endl;
#endif
    
    // Add branching constraint to path.
    pBranchingPath->operator[](pSearchTreeLevel) = aBranchingConstraint;
    
#ifdef DEBUG_NODE
    std::cout << "\tBranching path:\n\t";
    for (auto& it : *pBranchingPath)
    {
      if (it.second)
      {
        std::cout << "[" << it.first << ", " << it.second->prettyPrintConstraint() << "], ";
      }
    }
    std::cout << std::endl;
#endif
  }//extendBranchingPath
  
}// end namespace Search
