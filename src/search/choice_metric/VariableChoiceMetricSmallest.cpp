// Base class
#include "VariableChoiceMetricSmallest.hpp"

#include "Domain.hpp"
#include "DomainElement.hpp"
#include "DomainElementVoid.hpp"

#include <cassert>

namespace Search {

  VariableChoiceMetricSmallest::VariableChoiceMetricSmallest()
    : VariableChoiceMetric(VariableChoiceMetricType::VAR_CM_SMALLEST)
  {
  }

  VariableChoiceMetricSmallest::~VariableChoiceMetricSmallest()
  {
  }

  VariableMetricEval VariableChoiceMetricSmallest::compare(Core::Variable* aVar1, Core::Variable* aVar2)
  {
    assert(aVar1);
    assert(aVar2);

    VariableMetricEval metricEval = VariableMetricEval::VAR_METRIC_EQ;

    std::size_t domVar1MetricCtr{ 0 };
    std::size_t domVar2MetricCtr{ 0 };

    auto domainsVar1 = aVar1->domainList();
    auto domainsVar2 = aVar2->domainList();
    for (std::size_t domIdx1 = 0; domIdx1 < domainsVar1->size(); ++domIdx1)
    {
      Core::DomainElement* domainElementVar1 = domainsVar1->at(domIdx1)->lowerBound();
      assert(domainElementVar1);
      assert(!Core::DomainElementVoid::isa(domainElementVar1));
      for (std::size_t domIdx2 = 0; domIdx2 < domainsVar2->size(); ++domIdx2)
      {
        Core::DomainElement* domainElementVar2 = domainsVar2->at(domIdx2)->lowerBound();
        assert(domainElementVar2);
        assert(!Core::DomainElementVoid::isa(domainElementVar2));

        // Cannot compare different types
        if (domainElementVar1->getType() != domainElementVar2->getType() ||
            domainElementVar1->isEqual(domainElementVar2))
        {
          continue;
        }
        // Compare domain elements and increase metric counter accordingly
        if (domainElementVar1->isLessThan(domainElementVar2))
        {
          domVar1MetricCtr++;
        }
        else
        {
          domVar2MetricCtr++;
        }
      }
    }

    if (domVar1MetricCtr < domVar2MetricCtr)
    {
      metricEval = VariableMetricEval::VAR_METRIC_LT;
    }
    else if (domVar1MetricCtr > domVar2MetricCtr)
    {
      metricEval = VariableMetricEval::VAR_METRIC_GT;
    }

    return metricEval;
  }//compare

  double VariableChoiceMetricSmallest::getMetricValue(Core::Variable* aVar)
  {
    return 0;
  }//getMetricValue

}// end namespace Search
