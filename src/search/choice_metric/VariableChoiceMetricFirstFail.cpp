// Base class
#include "VariableChoiceMetricFirstFail.hpp"

#include "Domain.hpp"

#include <cassert>

namespace Search {

  VariableChoiceMetricFirstFail::VariableChoiceMetricFirstFail()
    : VariableChoiceMetric(VariableChoiceMetricType::VAR_CM_FIRST_FAIL)
  {
  }

  VariableChoiceMetricFirstFail::~VariableChoiceMetricFirstFail()
  {
  }

  double VariableChoiceMetricFirstFail::getMetricValue(Core::Variable* aVar)
  {
    assert(aVar);
    assert(aVar->variableSemantic());
    
    /// Invert metric -> smaller domains better metric
    std::size_t domainTotalSize{ 0 };
    auto domains = aVar->domainList();
    for (std::size_t domIdx = 0; domIdx < domains->size(); ++domIdx)
    {
      domainTotalSize += domains->at(domIdx)->getSize();
    }
    return -1.0 * static_cast<double>(domainTotalSize);
  }//getMetricValue

}// end namespace Search


