// Base class
#include "ValueChoiceMetric.hpp"

#include <cassert>

namespace Search {

  ValueChoiceMetric::ValueChoiceMetric(ValueChoiceMetricType aValMetricType)
    : pValMetricType(aValMetricType)
  {
  }

  ValueChoiceMetric::~ValueChoiceMetric()
  {
  }

}// end namespace Search





