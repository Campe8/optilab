// Base class
#include "ValueChoiceMetricIndomainMin.hpp"

#include "DomainElement.hpp"

#include <cassert>

namespace Search {

  ValueChoiceMetricIndomainMin::ValueChoiceMetricIndomainMin()
    : ValueChoiceMetric(ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MIN)
  {
  }

  ValueChoiceMetricIndomainMin::~ValueChoiceMetricIndomainMin()
  {
  }

}// end namespace Search





