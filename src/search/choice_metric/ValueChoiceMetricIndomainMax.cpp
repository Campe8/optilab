// Base class
#include "ValueChoiceMetricIndomainMax.hpp"

#include "DomainElement.hpp"

#include <cassert>

namespace Search {

  ValueChoiceMetricIndomainMax::ValueChoiceMetricIndomainMax()
    : ValueChoiceMetric(ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MAX)
  {
  }

  ValueChoiceMetricIndomainMax::~ValueChoiceMetricIndomainMax()
  {
  }

}// end namespace Search





