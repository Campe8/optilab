// Base class
#include "ValueChoiceMetricIndomainRandom.hpp"

#include "DomainElement.hpp"

// Domain iterator
#include "DomainIterator.hpp"

#include <cassert>

namespace Search {

  ValueChoiceMetricIndomainRandom::ValueChoiceMetricIndomainRandom()
  : ValueChoiceMetric(ValueChoiceMetricType::VAL_METRIC_INDOMAIN_RANDOM)
  , pRandomGenerator(nullptr)
  {
  }

  Core::DomainElement* ValueChoiceMetricIndomainRandom::getValue(Core::Domain*  aDomain)
  {
    assert(aDomain);
    auto domIt = aDomain->getIterator();

    assert(domIt);
    return &*(domIt->randBegin(pRandomGenerator));
  }//getValue

}// end namespace Search
