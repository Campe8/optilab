// Base class
#include "VariableChoiceMetricAntiFirstFail.hpp"

#include "Domain.hpp"

#include <cassert>

namespace Search {

  VariableChoiceMetricAntiFirstFail::VariableChoiceMetricAntiFirstFail()
    : VariableChoiceMetric(VariableChoiceMetricType::VAR_CM_ANTI_FIRST_FAIL)
  {
  }

  VariableChoiceMetricAntiFirstFail::~VariableChoiceMetricAntiFirstFail()
  {
  }

  double VariableChoiceMetricAntiFirstFail::getMetricValue(Core::Variable* aVar)
  {
    assert(aVar);
    assert(aVar->variableSemantic());
    
    std::size_t domainTotalSize{ 0 };
    auto domains = aVar->domainList();
    for (std::size_t domIdx = 0; domIdx < domains->size(); ++domIdx)
    {
      domainTotalSize += domains->at(domIdx)->getSize();
    }
    return static_cast<double>(domainTotalSize);
  }//getMetricValue

}// end namespace Search


