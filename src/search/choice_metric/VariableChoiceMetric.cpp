// Base class
#include "VariableChoiceMetric.hpp"

#include <cassert>

namespace Search {

  VariableChoiceMetric::VariableChoiceMetric(VariableChoiceMetricType aVarMetricType)
    : pVarMetricType(aVarMetricType)
  {
  }

  VariableChoiceMetric::~VariableChoiceMetric()
  {
  }

  VariableMetricEval VariableChoiceMetric::compare(Core::Variable*  aVar1, Core::Variable* aVar2)
  {
    assert(aVar1);
    assert(aVar2);

    auto comparisonValue = compare(getMetricValue(aVar1), getMetricValue(aVar2));
    if (comparisonValue == 0)
    {
      return VariableMetricEval::VAR_METRIC_EQ;
    }
    if (comparisonValue < 0)
    {
      return VariableMetricEval::VAR_METRIC_LT;
    }
    return VariableMetricEval::VAR_METRIC_GT;
  }// compare

}// end namespace Search





