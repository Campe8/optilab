// Base class
#include "VariableChoiceMetricInputOrder.hpp"

#include <cassert>

namespace Search {

  VariableChoiceMetricInputOrder::VariableChoiceMetricInputOrder()
    : VariableChoiceMetric(VariableChoiceMetricType::VAR_CM_INPUT_ORDER)
  {
  }

  VariableChoiceMetricInputOrder::~VariableChoiceMetricInputOrder()
  {
  }

  double VariableChoiceMetricInputOrder::getMetricValue(Core::Variable* aVar)
  {
    assert(aVar);
    assert(aVar->variableSemantic());
    assert(aVar->variableSemantic()->hasInputOrder());
    
    /// Invert metric -> the lower the value the better the metric is
    return -1.0 * static_cast<double>(aVar->variableSemantic()->getInputOrder());
  }//getMetricValue

}// end namespace Search


