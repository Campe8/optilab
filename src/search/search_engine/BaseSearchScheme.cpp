// Self first
#include "BaseSearchScheme.hpp"

#include "CombinatorNode.hpp"
#include "SolutionManager.hpp"

// Semantic objective
#include "VariableSemanticDecision.hpp"
#include "VariableSemanticObjective.hpp"

// Variable explosion tool
#include "VariableExplosionTools.hpp"

// Variable factory
#include "VariableArray.hpp"
#include "VariableFactory.hpp"

#include <unordered_set>
#include <cassert>

namespace Search {

	BaseSearchScheme::BaseSearchScheme(SemanticPtr aSemantics,
                                     EnvironmentList& aEnvironmentList,
                                     SearchEngineStrategyType aSearchEngineType)
  : pEnvironmentIdx(aEnvironmentList.size())
  , pGlobalSearchStatus(SearchStatus::SEARCH_UNDEF)
  , pSolutionManager(nullptr)
  , pRootNodeList(aEnvironmentList.size(), nullptr)
  , pSearchStrategyType(aSearchEngineType)
  , pSemantics(std::move(aSemantics))
	{
		assert(pSemantics);

		// Set the list of environments
		//pEnvironmentList = std::move(aEnvironmentList);
		pEnvironmentList.resize(aEnvironmentList.size());
		for (std::size_t idx{ 0 }; idx < pEnvironmentList.size(); ++idx)
		{
			pEnvironmentList[idx].reset(aEnvironmentList[idx].release());
		}
		assert(!pEnvironmentList.empty());
	}

	void BaseSearchScheme::initializeSemanticAndEnvironment(std::size_t aIdx)
	{
    assert(pSolutionManager);
		assert(aIdx < getNumEnvironments());

		// Return if the environment to initialize is the one currently in use
		if (aIdx == pEnvironmentIdx) return;

		// Set the environment pointer to the current index
		pEnvironmentIdx = aIdx;

		// Set output variables as solution variable into the solution manager
    pSolutionManager->setSearchEnvironment(searchEnvironment());

		/*
     * Initialize objective variables if any:
		 * @note set objective variable if:
		 * 1 - is a branching variable;
		 * 2 - the variable is reachable.
		 */
		std::unordered_set<Core::VariableSPtr> objectiveVariableSet;
		for (auto& var : searchEnvironment()->getObjectiveVariables())
		{
			assert(var->variableSemantic());
			assert(Core::VariableSemanticObjective::isa(var->variableSemantic()));
			if (var->variableSemantic()->isBranching() ||
          Core::VariableSemanticObjective::cast(var->variableSemantic())->isReachable())
			{
				for (const auto& varPtr : Core::Tools::explodeVariable(var))
				{
					assert(varPtr && varPtr->hasPrimitiveType());
					objectiveVariableSet.insert(var);
				}
			}
		}

		// Return if there are no objective variables
		if (objectiveVariableSet.empty()) return;

		// One objective variable: set as only objective variable
		if (objectiveVariableSet.size() == 1)
		{
			pSemantics->setVariableObjective(*objectiveVariableSet.begin());
			return;
		}

		// More than one objective variables: create new array variable
		Core::VariableFactoryUPtr varFactory(new Core::VariableFactory());
		Core::VariableSemanticUPtr semanticDecisionObjVariable(new Core::VariableSemanticDecision());
		Core::VariableSPtr varArray(varFactory->variableArray(objectiveVariableSet.size(), std::move(semanticDecisionObjVariable)));

		std::size_t idx{ 0 };
		for (auto it = objectiveVariableSet.begin(); it != objectiveVariableSet.end(); ++it)
		{
			Core::VariableArray::cast(varArray.get())->assignVariableToCell(idx++, *it);
		}

		// Assign to the semantic the new objective (array) variable
		pSemantics->setVariableObjective(varArray);
	}//initializeSemanticAndEnvironment

  bool BaseSearchScheme::isValidRootNode(std::size_t aIdx)
  {
    return aIdx < getNumEnvironments()
    && pRootNodeList[aIdx]
    && pRootNodeList[aIdx]->isSearchChoiceSet();
  }//isValidRootNode
  
	CombinatorNode* BaseSearchScheme::getRootNode(std::size_t aIdx)
	{
    assert(pSolutionManager);
    
		if (aIdx >= getNumEnvironments()) return nullptr;
    assert(aIdx < pRootNodeList.size());
    
    // If the root node has been already created and
    // the query is for the environment currently in use,
    // return the root node
    if (isValidRootNode(aIdx) && aIdx == getActiveEnvironmentIndex()) return pRootNodeList[aIdx].get();
    
		if (aIdx != getActiveEnvironmentIndex() || !isValidRootNode(aIdx))
		{
			// If the root node is not instantiated yet or the
			// given index is different from the current active environment,
			// set the environment and create a new root node
			createRootNode(aIdx);
		}
    assert(isValidRootNode(aIdx) && aIdx == pEnvironmentIdx);
    
		return pRootNodeList[aIdx].get();
	}//getRootNode

	void BaseSearchScheme::createRootNode(std::size_t aEnvIdx)
	{
		// Initialize the environment and semantic w.r.t. aEnvIdx
		initializeSemanticAndEnvironment(aEnvIdx);

		// Create a new search root node
		auto rootNode = createRootCombinatorNode(searchEnvironment(), searchSemantics());
    assert(rootNode);
    
    pRootNodeList[aEnvIdx] = std::shared_ptr<CombinatorNode>(rootNode);
	}//createRootNode

}// end namespace Search
