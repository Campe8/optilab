// Self first
#include "LocalBaseSearch.hpp"

// Environment
#include "LocalSearchEnvironment.hpp"

#include "Neighborhood.hpp"
#include "RootNeighborhood.hpp"
#include "CombinatorNode.hpp"
#include "SearchHeuristic.hpp"
#include "VariableExplosionTools.hpp"

#include <algorithm>

//#define DEBUG_LOCAL_BASE_SEARCH
//#define DEBUG_LOCAL_BASE_SEARCH_ALL

namespace Search {

  static void generateBestObjectiveFromVar(const Core::Variable* aVar,
                                           SolutionManager::Solution& aBest,
                                           const SolutionManager::Solution& aSolution)
  {
    assert(aSolution.find(aVar->getVariableNameId()) != aSolution.end());
    
    auto ID = aVar->getVariableNameId();
    aBest[ID] = aSolution.at(ID);
  }//addDomainValuesToBestSolution
  
  static void assignValuesFromHeuristic(const std::vector<Core::VariableSPtr>& space,
                                        spp::sparse_hash_map<Core::Variable*, Core::DomainElement*>& assign,
                                        SearchHeuristicPtr heuristic)
  {
    
#ifdef DEBUG_LOCAL_BASE_SEARCH
    std::cout << "Initial assignement:\n";
#endif
    
    for (const auto& var : space)
    {
      Core::DomainElement* domVal{nullptr};
      if (!var->isAssigned())
      {
        domVal = heuristic->getChoiceValue(var.get());
        
        // Shrink the domain of the variable
        var->domain()->shrink(domVal, domVal);
      }
      else
      {
        domVal = var->domain()->lowerBound();
      }
      assert(var->isAssigned() && domVal);
      
#ifdef DEBUG_LOCAL_BASE_SEARCH
      std::cout << var->getVariableNameId() << ": " << domVal->toString() << std::endl;
#endif
      
      // Store the current assignment for the variable
      assert(assign.find(var.get()) == assign.end());
      assign[var.get()] = domVal;
    }
  }//assignValuesFromHeuristic
  
	LocalBaseSearch::LocalBaseSearch(SemanticPtr aSemantics,
                                   EnvironmentList& aEnvironmentList,
                                   SearchEngineStrategyType aSearchEngineType)
  : BaseSearchScheme(std::move(aSemantics), aEnvironmentList, aSearchEngineType)
  , pBacktrackManager(nullptr)
  , pNeighborhoodCostWeight(10)
  , pRandomSeed(0)
  , pNeighborhoodSize(-1)
  , pNumNeighborhoods(0)
  , pRunAggressiveSearch(true)
  , pWarmStart(true)
  , pTimeout(-1)
  , pRestorePoint(0)
  , pTimer(nullptr)
  , pActiveNeighborhood(nullptr)
  , pRandomGenerator(nullptr)
  , pImprovingSolutionInit(false)
	{
    resetBestNeighborhoodCost();
    
    pInitialAssignment.clear();
    pInitialAssignmentObj.clear();
    pBestAssignment.clear();
	}

  CombinatorNode* LocalBaseSearch::createRootCombinatorNode(SearchEnvironment* aEnv, SearchSemantics* aSem)
  {
    // Check that the environment is a LocalSearchEnvironment
    // and extract the information about the neighborhood search
    assert(aEnv && aSem && aEnv->isLocalSearchEnvironment());
    
    // Initialize search environment.
    // The search environment is used to create the root node.
    // For example, the (local) search environment contains information about the
    // size of the neighborhoods
    initBaseLocalSearchEnvironment(static_cast<LocalSearchEnvironment*>(aEnv));
    
    // If the size of neighborhoods is less than zero, set it to the number of variables
    // in the environment
    if (pNeighborhoodSize < 0)
    {
      pNeighborhoodSize = static_cast<int>((aEnv->searchHeuristic()->getBranchingVarList()).size());
    }
    
    auto rootNeighborhood = new RootNeighborhood(aEnv->searchHeuristic(), pNeighborhoodSize,
                                                 pNumNeighborhoods, pRandomGenerator);
    return new CombinatorNode(std::unique_ptr<Neighborhood>(rootNeighborhood));
  }//createRootCombinatorNode
  
  void LocalBaseSearch::resetBestNeighborhoodCost()
  {
    pBestNeighborhoodCost = getMaximumCostValue();
  }//resetBestNeighborhoodCost
  
  void LocalBaseSearch::updateBestNeighborhoodCost(NeighborhoodCost aCost, Neighborhood* aNeighborhood)
  {
    assert(aNeighborhood);
    
    // Return if there is no improvement in the solution's cost
    if (aCost >= pBestNeighborhoodCost) return;
    
    // Update best cost and best solution
    pBestNeighborhoodCost = aCost;
    
    if (!pImprovingSolutionInit)
    {
      pImprovingSolutionInit = true;
      return;
    }
    
    // Store best assignment only if the search is not aggressive
    if (pRunAggressiveSearch) return;
    
    // Reset previous best assignment
    pBestAssignment.clear();
    
    auto searchSpace = aNeighborhood->getSearchSpace();
    assert(searchSpace);
    
    for (auto& var : *searchSpace)
    {
      assert(var->isAssigned());
      pBestAssignment[var.get()] = var->domain()->lowerBound();
      
#ifdef DEBUG_LOCAL_BASE_SEARCH
      if (pBestNeighborhoodCost == 0)
      {
        std::cout << var->getVariableNameId() << ": ";
        std::cout << var->domain()->lowerBound()->toString() << std::endl;
      }
#endif
      
    }
    
    for (const auto& it : pInitialAssignmentObj)
    {
      assert((it.first)->isAssigned());
      pBestAssignment[it.first] = (it.first)->domain()->lowerBound();
    }
  }//updateBestNeighborhoodCost
  
  void LocalBaseSearch::initBaseLocalSearchEnvironment(LocalSearchEnvironment* aEnv)
  {
    assert(aEnv);
    pRandomSeed          = aEnv->getRandomSeed();
    pNeighborhoodSize    = aEnv->getNeighborhoodSize();
    pNumNeighborhoods    = aEnv->getNumNeighborhoods();
    pRunAggressiveSearch = aEnv->isAggressiveSearch();
    pWarmStart           = aEnv->isWarmStart();
    pTimeout             = aEnv->getTimeout();
    assert(pNumNeighborhoods > 0);
    
    // Initialize the random generator
    pRandomGenerator = std::make_shared<Base::Tools::RandomGenerator>(pRandomSeed);
    
    // Initialize the environment for the specific local search strategy
    initLocalSearchEnvironment(aEnv);
  }//initLocalSearchEnvironment
  
  void LocalBaseSearch::resetTimer()
  {
    if (pTimeout < 0) return;
    pTimer.reset(new Base::Tools::Timer());
  }//resetTimer
  
	void LocalBaseSearch::start(CombinatorNode* aRootNode)
	{
		assert(aRootNode && aRootNode->searchNeighborhood());
    
		// Make sure an allocation happened
		assert(getActiveEnvironmentIndex() < getNumEnvironments());
    
		auto status = exploreNeighborhood(aRootNode->searchNeighborhood());
		setGlobalSearchStatus(status);
	}//start

	void LocalBaseSearch::exit(CombinatorNode* aCurrNode)
	{
		// Make sure an allocation happened
    assert(getActiveEnvironmentIndex() < getNumEnvironments());

		aCurrNode->resetSearchChoice(nullptr);
		aCurrNode->setStatus(getGlobalSearchStatus());
	}//exit

  void LocalBaseSearch::initBacktrackSemantics()
  {
    assert(searchEnvironment());
    
    // Setup a new backtrack manager
    pBacktrackManager.reset(new BacktrackManager());
    
    // Register domain as backtrackable objects
    for (auto& var : searchEnvironment()->getEnvironmentVariables())
    {
      for (auto& domain : *(var->domainList()))
      {
        pBacktrackManager->attachBacktrackableObject(domain);
      }
    }
    
    // Register heuristic as backtrackable object
    pBacktrackManager->attachBacktrackableObject(searchEnvironment()->searchHeuristic());
    
    // Register objective variable if any
    if (searchSemantics()->hasVariableObjective())
    {
      assert(searchSemantics()->variableObjective());
      for (const auto& var : searchSemantics()->variableObjectiveList())
      {
        for (auto& domain : *(var->domainList()))
        {
          pBacktrackManager->attachBacktrackableObject(domain);
        }
      }
    }
    
    // Register constraint store as backtrackable object
    pBacktrackManager->attachBacktrackableObject(searchSemantics()->constraintStore());
  }//initBacktrackSemantics
  
  void LocalBaseSearch::initSearchSpace(Neighborhood* aNeighborhood)
  {
    assert(aNeighborhood);
    assert(pBacktrackManager->getLevel() == 0);
    
    // Rest the cost of the best solution found so far
    resetBestNeighborhoodCost();
    
    // Prepare the initial and the best assignment
    pInitialAssignment.clear();
    pInitialAssignmentObj.clear();
    pInitialAssignment.reserve((searchEnvironment()->getEnvironmentVariables()).size());
    
    // No need to reserve, the initial assignment will be copied over
    // onto the best assignment for the first improving solution
    pImprovingSolutionInit = false;
    pBestAssignment.clear();
    
    // Randomly assign values to the variables in the search space
    auto heuristic = aNeighborhood->getSearchHeuristic();
    auto searchSpace = aNeighborhood->getSearchSpace();
    assert(heuristic && searchSpace);
    
    // Set the random generator into the heuristic and initialize the search space
    heuristic->setRandomGenerator(pRandomGenerator);
    assignValuesFromHeuristic(*searchSpace, pInitialAssignment, heuristic);

    // Set values for the objective variables
    if (searchSemantics()->hasVariableObjective())
    {
      assignValuesFromHeuristic(searchSemantics()->variableObjectiveList(), pInitialAssignmentObj,
                                heuristic);
    }
    
    // Get the cose of the initial solution.
    // @note this checks if the initial random assignment corresponds to a global minimum
    auto ncost = getNeighborhoodCost(aNeighborhood);
    updateBestNeighborhoodCost(ncost, aNeighborhood);
    
#ifdef DEBUG_LOCAL_BASE_SEARCH_ALL
    std::cout << "Cost " << ncost << std::endl;
#endif
    
    // Restore global status of domains and save the state.
    // This should have all the variables in the search spaces
    // in the same state they were when entering this method
    pBacktrackManager->restoreState();
    
    assert(pBacktrackManager->getLevel() == 0);
  }//initSearchSpace
  
  void LocalBaseSearch::setBestAssignment()
  {
    // Label the variables in the search space with the values of the initial assignment and
    // the best assignment
    for (const auto& it : pInitialAssignment)
    {
      // If an improving solution different from the initial assignment
      // has been found, use it updating the variables in the initial assignment
      // with the values in the best assignment
      auto assign = it.second;
      if (pBestAssignment.find(it.first) != pBestAssignment.end()) assign = pBestAssignment[it.first];
      
      // Shrink the domain of the variable
      it.first->domain()->shrink(assign, assign);
    }
    
    for (const auto& it : pInitialAssignmentObj)
    {
      // If an improving solution different from the initial assignment
      // has been found, use it updating the variables in the initial assignment
      // with the values in the best assignment
      auto assign = it.second;
      if (pBestAssignment.find(it.first) != pBestAssignment.end()) assign = pBestAssignment[it.first];
      
      // Shrink the domain of the variable
      it.first->domain()->shrink(assign, assign);
    }

    // @note this solution has value pBestNeighborhoodCost
    // @note check for solution duplicates
    // @note remove the following assert since there this function call comes from
    // a search status that is not success but the solution manager may have recorded solutions.
    // This can happen, for example, when the local search triggers the timeout and not all
    // solutions to the model have been found (if the solution limit was "all solutions")
    //assert(solutionManager()->getNumberOfRecordedSolutions() == 0);
    solutionManager()->notify(true);
    
    // Reset the variables
    pBacktrackManager->restoreState();
    assert(pBacktrackManager->getLevel() == 0);
  }//setBestAssignment
  
  void LocalBaseSearch::saveCurrentMinima() const
  {
    if (!searchSemantics()->hasVariableObjective()) return;
    
    // Best solution found so far
    SolutionManager::Solution best;
    
    // Get variable objective
    auto varObj = (searchSemantics()->variableObjective());
    assert(varObj);
    
    // At least one solution must be present in the solution manager
    assert(solutionManager()->getNumberOfRecordedSolutions() > 0);
    
    // Check if the variable objective is an array of variables,
    // for example because of multiple variable objectives
    const auto& lastSolution = solutionManager()->getLastSolution();
    const auto& objList = Core::Tools::explodeVariable(varObj);
    for (const auto& obj : objList)
    {
      generateBestObjectiveFromVar(obj.get(), best, lastSolution);
    }
    
    // Set current best objective value in the search semantics
    searchSemantics()->setObjectiveValue(best);
  }//saveCurrentMinima
  
  void LocalBaseSearch::initNeighborhood(Neighborhood* aNeighborhood)
  {
    assert(aNeighborhood);
    assert(pBacktrackManager->getLevel() == 0);
    
    // Get the search space and the indexes on the
    // search space of the neighborhood variables
    spp::sparse_hash_set<Core::Variable*> neighborhoodVars;
    
    // Optimization: if the neighborhood size is equal to the size
    // of the search space, skip the following loops
    const auto& neighborhood = aNeighborhood->getNeighborhood();
    if (neighborhood.size() < pInitialAssignment.size())
    {
      for(const auto& var : neighborhood)
      {
        neighborhoodVars.insert(var.get());
      }
      
      for (auto& it : pInitialAssignment)
      {
        // Label only the variables that are not part of the
        // current neighborhood
        if(neighborhoodVars.find(it.first) == neighborhoodVars.end())
        {
          it.first->domain()->shrink(it.second, it.second);
        }
      }
    }
    
    // Save current state.
    // @note force saving the state to be consistent with
    // the levels and restore points.
    // For example, if the size of the neighborhood is equal to
    // the size of the search space, no update on the variables
    // will be performed, and therefore the state won't be saved
    // if not forced
    pBacktrackManager->saveState(true);
    assert(pBacktrackManager->getLevel() == 1);
    
    // Use the current backtrack level as the restore point point
    // for the neighborhood during neighborhood search
    pRestorePoint = pBacktrackManager->getLevel();
  }//initNeighborhood
  
  void LocalBaseSearch::restoreNeighborhood()
  {
    auto currLevel = pBacktrackManager->getLevel();
    assert(currLevel >= pRestorePoint);
    
    while(currLevel >= pRestorePoint)
    {
      // @note currLevel represents the level
      // the state "will go to" after the restoreState.
      // In particular, when currLevel is 0,
      // the next restoreState will bring the state
      // to level 0 (which means that the current state is 1).
      // Therefore currLevel should be set after the current
      // restoreState and break the loop as soon as it becomes 0
      pBacktrackManager->restoreState();
      currLevel = pBacktrackManager->getLevel();
    }
    
    // Here the level is < pRestorePoint.
    // Save the state to have restored the state to the
    // state it was at the restore point
    pBacktrackManager->saveState();
    assert(pRestorePoint == pBacktrackManager->getLevel());
  }//restoreNeighborhood
  
  void LocalBaseSearch::restoreNeighborhoodGlobalState()
  {
    auto currLevel = pBacktrackManager->getLevel();
    do {
      currLevel = pBacktrackManager->getLevel();
      pBacktrackManager->restoreState();
    } while(currLevel > 0);
  }//restoreNeighborhoodGlobalState
  
  LocalBaseSearch::NeighborhoodCost LocalBaseSearch::getNeighborhoodCost(Neighborhood* aNeighborhood)
  {
    // @note the following was previously returning the cost as the number of unsatisfied
    // constraints (as per CBLS) but that led to local minimas when non binary constraints
    // where highly unsatified (e.g., alldiff with all variable having the same value) but
    // the constraint was treated like a binary unsatisfied nq. In other words, it wasn't able
    // to distinguish between an assignment with few constraints but highly unsatisfied from
    // an assignment with many constraints but almost all satiafied:
    // auto cost = searchSemantics()->constraintStore()->getNumUnsatConstraint();
    // For this reason, the following has been switched to a fitness function that returns a value
    // of fitness accorting to how much a constraint is satiafied.
    // This fitness value is built from different components such as "standard" constraints and
    // no-good constraints and it must be normalized accordingly. For example, no-good constraints
    // for CBLS may represent the objective value for an assignment which is a solution.
    // This implies that first the CBLS must try to bring the fitness for standard constraints
    // to zero and only after it should try to minimize the fitness value for no-good constraints.
    auto cstore = (searchSemantics()->constraintStore()).get();
    
    // Force optimization re-evaluation
    searchSemantics()->forceObjectiveReevaluation();
    
    // Run consistency on optimization constraints to propagate on the objective variables (if any).
    // This should shrink the domain of the optimization variables to singletons.
    // If the propagation fails (e.g., the domain of the optimization variables are too tight),
    // the current assignment should not be considered a solution
    if (searchSemantics()->hasVariableObjective())
    {
      // Save the a snapshot of the current state of the constraint store.
      // The state will change upon running the consistency to calculate the values of the
      // objective variables
      cstore->saveSemanticState();
      
      const auto& optConsistency = cstore->runOptimizationConsistency();
      if (optConsistency.first == Semantics::StoreConsistencyStatus::CONSISTENCY_FAILED)
      {
        // Return fiteness of the constraint that failed the consistency of the
        // optimization constraint
        return static_cast<NeighborhoodCost>(optConsistency.second);
      }
      
      // Restore the state of the constraint store as it was before running consistency
      cstore->uploadSemanticState();
    }
    
    // Before calculating the semantic fitness, each nogood constraint (if any) must be
    // inserted (again) into the constraint queue of the constraint store of constraints
    // to reevaluate. This must be done since the semantics of the backtrack during CBLS remove
    // the nogood constraints from the constraints to reevaluate before the following call happens
    searchSemantics()->forceNogoodsReevaluation();
    
    // Calculate the semantic fitness
    cstore->calculateSemanticFitness();
    
    // Give more weight to the standard constraint fitness
    const auto nogoodFitness = cstore->getNogoodSemanticFitness();
    const auto stdSemFitness = cstore->getStdSemanticFitness();
    
    // Fitness should not be undef here
    assert(nogoodFitness >= 0);
    assert(stdSemFitness >= 0);
    
    auto cost = static_cast<NeighborhoodCost>(nogoodFitness) +
    pNeighborhoodCostWeight * static_cast<NeighborhoodCost>(stdSemFitness);
    
#ifdef DEBUG_LOCAL_BASE_SEARCH_ALL
    if (cost == 1)
    {
      auto searchSpace = aNeighborhood->getSearchSpace();
      assert(searchSpace);
      
      for(auto& var : *searchSpace)
      {
        
        std::cout << var->getVariableNameId() << " ";
        assert(var->isAssigned());
        std::cout << var->domain()->lowerBound()->toString() << std::endl;
      }
      std::cout << "=========================\n";
      getchar();
    }
#endif
    
    // Update the best cost if the cost is the global minimum cost
    checkAndRegisterGlobalMinimum(cost, aNeighborhood);
    
    return cost;
  }//getNeighborhoodCost
  
  void LocalBaseSearch::checkAndRegisterGlobalMinimum(NeighborhoodCost aCost, Neighborhood* aNeighborhood)
  {
    // Return if the cost is not the global minimum
    if (aCost != LocalBaseSearch::getGlobalMinimumCostValue()) return;
    
    // The neighborhood has reached its global minimum.
    // Notify the solution manager and update the best cost.
    // @note check for solution duplicates
    SolutionStatus solutionStatus = solutionManager()->notify(true);
    assert(solutionStatus != Search::SOLUTION_NOT_VALID);
    
    // Save the current objective value (if any) to be later on the base lane for the
    // objective constraint to post for optimization search
    saveCurrentMinima();
    
    // Here a solution has been found,
    // if there is there are more solutions to find, set the current one as a nogood solution
    // Add current solution as nogood constraint to avoid recording the same solution again
    if (solutionStatus == SolutionStatus::SOLUTION_NEED_MORE)
    {
      storeSolutionNogood(solutionManager()->getLastSolution());
    }
    
    updateBestNeighborhoodCost(aCost, aNeighborhood);
    
    if (searchSemantics()->hasVariableObjective())
    {
      postObjectiveConstraint();
    }
  }//checkAndRegisterMinimum
  
  void LocalBaseSearch::setupNeighborhoodSearch()
  {
    // Initialize the backtrack manager
    initBacktrackSemantics();
    
    // Save the current state of the variables
    pBacktrackManager->saveState();
    assert(pBacktrackManager->getLevel() == 0);
  }//setupNeighborhoodSearch
  
  void LocalBaseSearch::cleanupNeighborhoodSearch()
  {
    // Reset neighborhood assignment
    pInitialAssignment.clear();
    pInitialAssignmentObj.clear();
    
    // Reset the neighborhood restore point
    pRestorePoint = 0;
    
    // Restore global status of domains
    pBacktrackManager->restoreState();
    assert(pBacktrackManager->getLevel() == 0);
    
    // Cleanup all objective and nogood constraints
    if (searchSemantics()->hasVariableObjective())
    {
      searchSemantics()->cleanupOptimizationObjective();
      searchSemantics()->cleanupNogoods();
    }
  }//cleanupNeighborhoodSearch
  
  void LocalBaseSearch::setCurrentActiveNeighborhood(Neighborhood* aNeighborhood)
  {
    assert(aNeighborhood);
    
    pActiveNeighborhood = aNeighborhood;
    
    // For each neighborhood set the random generator used by the search heuristic
    // on that neighborhood
    pActiveNeighborhood->getSearchHeuristic()->setRandomGenerator(pRandomGenerator);
  }//setCurrentActiveNeighborhood
  
	SearchStatus LocalBaseSearch::exploreNeighborhood(Neighborhood* aRootNeighborhood)
	{
		assert(aRootNeighborhood);
    
    // If the search space is empty return asap
    if (aRootNeighborhood->getSearchSpace()->empty()) return SearchStatus::SEARCH_SUCCESS;
    
		// Make sure an allocation happened and the search space is not empty
    assert(!aRootNeighborhood->getSearchSpace()->empty());
		assert(getActiveEnvironmentIndex() < getNumEnvironments() && solutionManager());

    // Initialize the search space map
    for (const auto& var : *(aRootNeighborhood->getSearchSpace()))
    {
      pSearchSpaceMap[var->getVariableNameId()] = var;
    }
    
		// Reset solution manager since it could hold solutions
		// from previous runs
		solutionManager()->resetRegister();

		// Return if there is no need for more solutions
		if (!solutionManager()->needsMoreSolutions())
		{
			return SearchStatus::SEARCH_SUCCESS;
		}

		// Setup code before labeling
		setupNeighborhoodSearch();
    
    // Prepare the initial assignment to optimize using neighborhood search.
    // This is the "initialization function"
    initSearchSpace(aRootNeighborhood);
    
    // If the search is already at its global minimum after the initial assignment,
    // check whether to continue or not based on the number of solutions to find
    if (isSearchAtGlobalMinimum())
    {
      if (!solutionManager()->needsMoreSolutions())
      {
        return SearchStatus::SEARCH_SUCCESS;
      }
      
      // If there are more solutions to find,
      // reset the best solution cost find so far and continue
      resetBestNeighborhoodCost();
    }
    
    // Runs the large neighborhood search
    auto searchStatus = runLargeNeighborhoodSearch(aRootNeighborhood);
    
    // Cleanup code after labeling
    cleanupNeighborhoodSearch();
    
		return searchStatus;
	}//exploreNeighborhood
  
  SearchStatus LocalBaseSearch::runLargeNeighborhoodSearch(Neighborhood* aRootNeighborhood)
  {
    assert(aRootNeighborhood && !aRootNeighborhood->getSearchSpace()->empty());
    assert(!isSearchAtGlobalMinimum());
    
    resetTimer();
    
    // Return asap if the neighborhood size is zero
    if (getNeighborhoodSize() == 0)
    {
      return SearchStatus::SEARCH_TERMINATE;
    }
    
    // Initialize the status of the search as undefined
    SearchStatus searchStatus = SearchStatus::SEARCH_UNDEF;
    
    // Perform a neighborhood search for each large neighborhood
    Neighborhood* neighborhood{aRootNeighborhood};
    while (neighborhood)
    {
      // Break on search timeout
      if (searchTimeout())
      {
        if (searchStatus == SearchStatus::SEARCH_UNDEF)
        {
          searchStatus = SearchStatus::SEARCH_TERMINATE;
        }
        break;
      }
      
      // Set current active neighborhood
      setCurrentActiveNeighborhood(neighborhood);
      
      // Save the current state after initialization.
      // Each neighborhood starts from the level 0 where
      // all the variables of the neighborhood are not ground.
      // This invariant must be preserved between each loop iteration
      pBacktrackManager->saveState();
      assert(pBacktrackManager->getLevel() == 0);
      
      // Prepare the neighborhood
      initNeighborhood(neighborhood);
      
      // This is iteration over the neighborhoods that could be
      // potentially done in parallel
      searchStatus = neighborhoodSearch(neighborhood);
      
      // Get next neighborhood
      neighborhood = neighborhood->next();
      
      // Restore the neighborhood that was set before the search
      restoreNeighborhood();
      
      // Restore the state to preserve the invariant inside the loop.
      // This whould free the stack in the bracktrack manager and
      // reset the state as it was before entering the while loop
      restoreNeighborhoodGlobalState();
      assert(pBacktrackManager->getLevel() == 0);
      
      if (searchStatus == SearchStatus::SEARCH_SUCCESS)
      {
        // The search is at a local minima.
        // Break if there is no need to find more solutions and this is not an optimization problem
        if (!solutionManager()->needsMoreSolutions() &&
            !searchSemantics()->hasVariableObjective()) break;

        // If there are more solutions to find,
        // reset the best solution cost find so far and continue
        resetBestNeighborhoodCost();
        
        // Reset internal state of the neighborhood search strategy.
        // @note by default this is always a warm start
        resetNeighborhoodSearchState(pWarmStart);
        
        // If neighborhood is nullptr (no more neighborhoods to explore)
        // start again from the root neighborhood until all solutions are found or
        // a timeout has been triggered
        if (!neighborhood)
        {
          neighborhood = aRootNeighborhood;
        }
      }
    }//while
    
    // If search is not success, then update the solution manager with
    // the best solution found so far
    if (searchStatus != SearchStatus::SEARCH_SUCCESS)
    {
      assert(searchStatus == SearchStatus::SEARCH_TERMINATE);
      
      // Setup the best assignment found:
      // - use the initial assignment for the variables not in the best assignment
      // - use the best assignment on the remaining variable
      // @note assignment is set only if search is non-aggressive.
      // This hold even for satisfaction problems (see searchSemantics()->hasVariableObjective())
      // meaning that the last assignment may not be a solution but it will be part of
      // the solution set anyways if this is not an aggressive search
      if (!pRunAggressiveSearch) setBestAssignment();
    }

    return searchStatus;
  }//runLargeNeighborhoodSearch

  void LocalBaseSearch::storeSolutionNogood(const Search::SolutionManager::Solution& aSolution)
  {
    // Prepare the set of variables and domains representing the nogood
    std::vector<Core::VariableSPtr> nogoodVars;
    std::vector<Core::DomainSPtr>   nogoodDoms;
    
    for (const auto& sol : aSolution)
    {
      assert(pSearchSpaceMap.find(sol.first) != pSearchSpaceMap.end());
      nogoodVars.push_back(pSearchSpaceMap[sol.first]);
      
      assert((nogoodVars.back())->domain()->lowerBound()->
             isEqual((nogoodVars.back())->domain()->upperBound()));
      nogoodDoms.push_back(nogoodVars.back()->domain());
      
    }
    
    // Store the nogood constraint
    searchSemantics()->storeNogood(nogoodVars, nogoodDoms);
  }//storeSolutionNogood
  
}// end namespace Search
