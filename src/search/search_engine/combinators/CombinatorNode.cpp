// Self first
#include "CombinatorNode.hpp"

#include <cassert>

namespace Search {

	CombinatorNode::CombinatorNode(std::unique_ptr<SearchChoice> aChoice)
	: pSearchStatus(SearchStatus::SEARCH_UNDEF)
	, pSearchChoice(nullptr)
	{
		pSearchChoice = std::shared_ptr<SearchChoice>(aChoice.release());
	}

	void CombinatorNode::resetSearchChoice(std::unique_ptr<SearchChoice> aChoice)
	{
		pSearchChoice = std::shared_ptr<SearchChoice>(aChoice.release());
	}//resetSearchChoice

}// end namespace Search
