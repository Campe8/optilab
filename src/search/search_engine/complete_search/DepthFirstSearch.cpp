// Self first
#include "DepthFirstSearch.hpp"

#include <algorithm>
#include <cassert>

#include "BacktrackManager.hpp"
#include "BranchingConstraint.hpp"
#include "DepthFirstSearchConstants.hpp"
#include "Constraint.hpp"
#include "HybridBacktrackManager.hpp"
#include "MetricsRegister.hpp"
#include "Node.hpp"
#include "SearchMacro.hpp"
#include "SearchOutManager.hpp"
#include "SolutionManager.hpp"
#include "VariableArray.hpp"
#include "VariableExplosionTools.hpp"

// #define USE_HYBRID_TRAILSTACK

namespace Search {
  
  static void generateBestObjectiveFromVar(const Core::Variable* aVar,
                                           SolutionManager::Solution& aBest,
                                           const SolutionManager::Solution& aSolution)
  {
    assert(aSolution.find(aVar->getVariableNameId()) != aSolution.end());
    
    auto ID = aVar->getVariableNameId();
    aBest[ID] = aSolution.at(ID);
  }//addDomainValuesToBestSolution
  
  DepthFirstSearch::DepthFirstSearch(SemanticPtr aSemantics, EnvironmentList& aEnvironmentList)
  : BaseSearch(std::move(aSemantics), aEnvironmentList,
               SearchEngineStrategyType::SE_DEPTH_FIRST_SEARCH)
  , pNumNodes(0)
  , pNumFailures(0)
  , pMaxDepth(0)
  {    
  }
  
  std::shared_ptr<Base::Tools::MetricsRegister> DepthFirstSearch::getMetrics()
  {
    auto metrics = std::make_shared<Base::Tools::MetricsRegister>();
    metrics->addMetric(DFSMetrics::NUM_NODES, pNumNodes);
    metrics->addMetric(DFSMetrics::NUM_FAILURES, pNumFailures);
    metrics->addMetric(DFSMetrics::MAX_DEPTH, pMaxDepth);
    
    return metrics;
  }//getMetrics
  
  void DepthFirstSearch::registerConstraint(const std::shared_ptr<Core::Constraint>& aConstraint)
  {
    assert(aConstraint);
    aConstraint->subscribeToConstraintStore(searchSemantics()->constraintStore());
  }// registerConstraint
  
  void DepthFirstSearch::initBacktrackSemantics()
  {
    assert(searchEnvironment());
    
    // Setup a new backtrack manager
#ifdef USE_HYBRID_TRAILSTACK
    pBacktrackManager.reset(new HybridBacktrackManager(searchSemantics()->constraintStore()));
#else
    pBacktrackManager.reset(new BacktrackManager());
#endif
    
    // Register domain as backtrackable objects
    for(auto& var : searchEnvironment()->getEnvironmentVariables())
    {
      for(auto& domain : *(var->domainList()))
      {
        pBacktrackManager->attachBacktrackableObject(domain);
      }
    }
    
    // Register heuristic as backtrackable object
    pBacktrackManager->attachBacktrackableObject(searchEnvironment()->searchHeuristic());
    
    // Register objective if any
    if (searchSemantics()->hasVariableObjective())
    {
      for (const auto& var : searchSemantics()->variableObjectiveList())
      {
        for (auto& domain : *(var->domainList()))
        {
          pBacktrackManager->attachBacktrackableObject(domain);
        }
      }
    }
    
    // Register constraint store as backtrackable object
    pBacktrackManager->attachBacktrackableObject(searchSemantics()->constraintStore());
  }//initBacktrackSemantics
  
  void DepthFirstSearch::saveCurrentMinima() const
  {
    if (!searchSemantics()->hasVariableObjective()) return;

    // Best solution found so far
    SolutionManager::Solution best;
    
    // Get variable objective
    auto varObj = (searchSemantics()->variableObjective());
    assert(varObj);
    
    // At least one solution must be present in the solution manager
    assert(solutionManager()->getNumberOfRecordedSolutions() > 0);
    
    // Check if the variable objective is an array of variables,
    // for example because of multiple variable objectives
    const auto& lastSolution = solutionManager()->getLastSolution();
    const auto& objList = Core::Tools::explodeVariable(varObj);
    for (const auto& obj : objList)
    {
      generateBestObjectiveFromVar(obj.get(), best, lastSolution);
    }
    
    // Set current best objective value in the search semantics
    searchSemantics()->setObjectiveValue(best);
  }//saveCurrentMinima
  
  void DepthFirstSearch::storeNogoods(const Node* aNode)
  {
    assert(aNode);
    if (!searchSemantics()->useNogoodLearning()) return;
    
    // Get the current branching path and the branching constraints
    // up to the current tree level
    const auto& path = *(aNode->getBranchingPath());
    auto pathSize = aNode->getTreeLevel();
    
    // Return if the branching path contains one node
    // @note last decision (at position "pathSize") is a positive decision.
    // This is because this method is called not on force fail but on fail
    // from the DFS.
    // Next decision will be a negative decision and the path up to this
    // current decision can be recorded as a nogood path
    if (pathSize == 0) return;
    
    // Prepare the set of variables and domains representing the nogood
    std::vector<Core::VariableSPtr> nogoodVars;
    std::vector<Core::DomainSPtr>   nogoodDoms;
    for(std::size_t lvl{0}; lvl <= pathSize; ++lvl)
    {
      assert(path.find(lvl) != path.end());
      const auto& brcCon = path.at(lvl);
      
      // Assert that branching strategy is binary choice.
      // @note when other branching strategies are added, simply return
      auto brcConID = brcCon->getBranchingConstraintId();
      assert(brcConID != Core::BranchingConstraintId::BRC_CON_ID_BINARY_CHOICE_NQ ||
             brcConID != Core::BranchingConstraintId::BRC_CON_ID_BINARY_CHOICE_EQ);
      
      // Store only positive decisions as nogood constraints.
      // For more information, see
      // Nogood Recording from Restarts
      // Christophe Lecoutre et al., IJCAI-07
      if(brcConID == Core::BranchingConstraintId::BRC_CON_ID_BINARY_CHOICE_EQ)
      {
        nogoodVars.push_back(brcCon->getBranchingVariable());
        nogoodDoms.push_back(brcCon->getBranchingDomain());
      }
    }
    
    // Register the nogood constraint
    postNogoodConstraint(nogoodVars, nogoodDoms);
  }//storeNogoods
  
	void DepthFirstSearch::setupLabeling()
	{
		// Initialize backtrack semantics
		initBacktrackSemantics();
    
    // Initialize metric variables
    pNumNodes = 0;
    pNumFailures = 0;
    pMaxDepth = 0;
	}// setupLabeling

  void DepthFirstSearch::cleanupLabeling()
  {
    // Cleanup all objective and nogood constraints
    if (searchSemantics()->hasVariableObjective())
    {
      searchSemantics()->cleanupOptimizationObjective();
      searchSemantics()->cleanupNogoods();
    }
  }// cleanupLabeling
  
  SearchStatus DepthFirstSearch::labelTree(Node* aRootNode)
  {
    assert(aRootNode);
    
    // Set undefined global search status before dfs
    setGlobalSearchStatus(SearchStatus::SEARCH_UNDEF);
    
    // Save current initial state before any propagation
    pBacktrackManager->saveState();
    assert(pBacktrackManager->getLevel() == 0);

    auto searchStatus = dfs(aRootNode);
    
    // Reset global status of domains
    pBacktrackManager->restoreState();
    assert(pBacktrackManager->getLevel() == 0);
    
    // In case of objective variables that are set
    // and cannot be further optimized (search failed by propagation)
    // set the search status to terminate
    if(searchSemantics()->hasVariableObjective() &&
       searchSemantics()->isObjectiveValueSet()  &&
       searchStatus == SearchStatus::SEARCH_FAIL)
    {
      searchStatus = SearchStatus::SEARCH_TERMINATE;
    }
    
    RETURN_STATUS(searchStatus);
  }//label
  
  SearchStatus DepthFirstSearch::dfs(Node* aNode)
  {
    assert(aNode);
    
    // Increment the counter for the number of explored nodes
    pNumNodes++;
    pMaxDepth = std::max(static_cast<int64_t>(aNode->getTreeLevel()), pMaxDepth);
    
    // Check global status for early return
    if (getGlobalSearchStatus() == SearchStatus::SEARCH_TERMINATE)
    {
      return SearchStatus::SEARCH_TERMINATE;
    }
    
    // Run consistency on constraint store
    switch (runConsistency())
    {
      case Semantics::StoreConsistencyStatus::CONSISTENCY_FAILED:
      {
        // Increment the counter for the number of failures
        pNumFailures++;
        
        // Return fail status
        RETURN_SEARCH_STATUS(SearchStatus::SEARCH_FAIL);
      }
      case Semantics::StoreConsistencyStatus::CONSISTENCY_SUBSUMED:
      case Semantics::StoreConsistencyStatus::CONSISTENCY_FIXPOINT:
      {
        if (!aNode->hasSuccessor())
        {
          // No more successor to branch on,
          // i.e., aNode is a leaf of the search tree: solution found
          SolutionStatus solutionStatus = solutionManager()->notify();
          assert(solutionStatus != Search::SOLUTION_NOT_VALID);
          
          // If there is at least one objective variable,
          // force finding more solutions until failure or not labeling occurs
          if (searchSemantics()->hasVariableObjective())
          {
            solutionStatus = Search::SOLUTION_NEED_MORE;
            
            // Before restoring the state of the search, save the current
            // objective value (if any) to be later on the base line for the
            // objective constraint to post for optimization search
            saveCurrentMinima();
          }
          
          switch (solutionStatus)
          {
            case Search::SOLUTION_LAST:
              assert(!searchSemantics()->hasVariableObjective());
              RETURN_SEARCH_STATUS(SearchStatus::SEARCH_TERMINATE);
            default:
            {
              assert(solutionStatus == Search::SOLUTION_NEED_MORE);
              
              // Need more solutions, fail and continue with search
              RETURN_SEARCH_STATUS(SearchStatus::SEARCH_FORCED_FAIL);
            }
          }
        }
        
        // Not a solution or need more solutions,
        // save current consistent state and continue search
        pBacktrackManager->saveState();
        break;
      }
      default:
        assert(Semantics::StoreConsistencyStatus::CONSISTENCY_NO_FIXPOINT);
        pBacktrackManager->saveState();
        break;
    }// runConsistency
    
    // Return asap in case of timeout
    if (searchTimeout())
    {
      // If the search is not an optimization search and the computation got here,
      // it means that there is still some search tree to explore, i.e., the search failed
      // if there are no recorded solutions
      if (solutionManager()->getNumberOfRecordedSolutions() == 0)
      {
        RETURN_SEARCH_STATUS(SearchStatus::SEARCH_FAIL);
      }
      
      RETURN_SEARCH_STATUS(SearchStatus::SEARCH_TERMINATE);
    }
    
    do
    {
      // Branch the current search tree node
      // @node next node should be not null since
      // aNode has a successor, i.e, aNode is NOT a leaf node
      NodeUPtr nextNode(aNode->next());
      assert(nextNode);
      
      // Save the current node choice for recomputation
#ifdef USE_HYBRID_TRAILSTACK
      static_cast<HybridBacktrackManager*>(pBacktrackManager.get())->saveNodeChoice(nextNode.get());
#endif

      // Recursive dfs call
      SearchStatus recursiveSearchStatus = dfs(nextNode.get());
      
      // No matter the results of the recursive search,
      // restore previous state before continuing since it may have been
      // modified by the recursive dfs
      pBacktrackManager->restoreState();
      
      switch (recursiveSearchStatus)
      {
        case Search::SEARCH_SUCCESS:
        case Search::SEARCH_TERMINATE:
          return recursiveSearchStatus;
        default:
          assert(VERIFY_FAILURE(recursiveSearchStatus));
          
          // Return fail only if nextNode is the rightmost node
          // aNode branched on, i.e., this level cannot be further explored
          if (nextNode->isRightmostNode())
          {
						if (recursiveSearchStatus == Search::SEARCH_FORCED_FAIL)
						{
							RETURN_SEARCH_STATUS(SearchStatus::SEARCH_FORCED_FAIL);
						}
						RETURN_SEARCH_STATUS(SearchStatus::SEARCH_FAIL);
          }
          
          // nextNode is not the rightmost node which means that there are
          // other nodes to try: save current state that will be later restored
          // after the recursive DFS call
          pBacktrackManager->saveState();
          
          // Perform nogood learning
          if(recursiveSearchStatus == SearchStatus::SEARCH_FAIL)
          {
            storeNogoods(nextNode.get());
          }
          
          // If recursive search status was forced to fail to find more solutions
          // and the objective variable is defined:
          // post new objective constraint to find better solutions
          if(recursiveSearchStatus == SearchStatus::SEARCH_FORCED_FAIL &&
             searchSemantics()->hasVariableObjective())
          {
            postObjectiveConstraint();
          }
          
          break;
      }
    } while (getGlobalSearchStatus() != SearchStatus::SEARCH_TERMINATE);
    
    RETURN_SEARCH_STATUS(SearchStatus::SEARCH_TERMINATE);
  }// dfs
  
}// end namespace Search
