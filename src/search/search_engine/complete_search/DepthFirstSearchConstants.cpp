// Self first
#include "DepthFirstSearchConstants.hpp"

namespace Search {
  namespace DFSMetrics {
    
    const char* NUM_FAILURES = "numFailures";
    const char* NUM_NODES    = "numNodes";
    const char* MAX_DEPTH    = "maxDepth";
    
  }// end namespace DFSMetrics
}// end namespace Search
