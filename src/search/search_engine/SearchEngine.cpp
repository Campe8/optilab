// Self first
#include "SearchEngine.hpp"

#include "CombinatorNode.hpp"

#include <cassert>


namespace Search {
  
  SearchEngine::SearchEngine(std::unique_ptr<SearchStrategy> aSearchStrategy)
  {
    assert(aSearchStrategy);
    pSearchStrategy = std::shared_ptr<SearchStrategy>(aSearchStrategy.release());
  }
  
  SearchStatus SearchEngine::label(CombinatorNode* aRootNode)
  {
    assert(aRootNode);
    
    // Start the search from the search strategy combinator
    pSearchStrategy->start(aRootNode);
    
    // Exit the search and cleanup resources from the
    // search strategy combinator
    pSearchStrategy->exit(aRootNode);
    
    // Return the status of the root combinator node,
    // i.e., the status of the tree
    return aRootNode->getStatus();
  }//label
  
}// end namespace Search
