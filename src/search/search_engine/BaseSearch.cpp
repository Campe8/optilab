// Self first
#include "BaseSearch.hpp"

#include "Node.hpp"
#include "RootNode.hpp"
#include "CombinatorNode.hpp"
#include "SolutionManager.hpp"

// Semantic objective
#include "VariableSemanticDecision.hpp"
#include "VariableSemanticObjective.hpp"

// Variable explosion tool
#include "VariableExplosionTools.hpp"

// Variable factory
#include "VariableArray.hpp"
#include "VariableFactory.hpp"

#include <unordered_set>

namespace Search {

	BaseSearch::BaseSearch(SemanticPtr aSemantics,
		                     EnvironmentList& aEnvironmentList,
		                     SearchEngineStrategyType aSearchEngineType)
  : BaseSearchScheme(std::move(aSemantics), aEnvironmentList, aSearchEngineType)
  , pTimeout(-1)
  , pTimer(nullptr)
	{
	}

	CombinatorNode* BaseSearch::createRootCombinatorNode(SearchEnvironment* aEnv, SearchSemantics* aSem)
  {
    assert(aEnv);
    assert(aSem);
    
    pTimeout = aEnv->getTimeout();
    auto rootNode = new RootNode(aEnv->searchHeuristic(), aSem->constraintStore());
    
    return new CombinatorNode(std::unique_ptr<Node>(rootNode));
  }//createRootCombinatorNode
  
  void BaseSearch::resetTimer()
  {
    if (pTimeout < 0) return;
    pTimer.reset(new Base::Tools::Timer());
  }//resetTimer
  
	void BaseSearch::start(CombinatorNode* aRootNode)
	{
		assert(aRootNode && aRootNode->searchNode());

		// Make sure an allocation happened
		assert(getActiveEnvironmentIndex() < getNumEnvironments());

		auto status = label(aRootNode->searchNode());
		setGlobalSearchStatus(status);
	}//start

	void BaseSearch::exit(CombinatorNode* aCurrNode)
	{
		// Make sure an allocation happened
		assert(getActiveEnvironmentIndex() < getNumEnvironments());

		aCurrNode->resetSearchChoice(nullptr);
		aCurrNode->setStatus(getGlobalSearchStatus());
	}//exit

	SearchStatus BaseSearch::label(Node* aRootNode)
	{
		assert(aRootNode);

		// Make sure an allocation happened
		assert(getActiveEnvironmentIndex() < getNumEnvironments() && solutionManager());

		// Reset solution manager since it could hold solutions
		// from previous runs
		solutionManager()->resetRegister();

		// Return if there is no need for more solutions
		if (!solutionManager()->needsMoreSolutions())
		{
			return SearchStatus::SEARCH_SUCCESS;
		}

		// Setup code before labeling
		setupLabeling();

    // Reset the timer for the search
    resetTimer();
    
		// Actual labeling
		auto searchStatus = labelTree(aRootNode);

		// Cleanup code after labeling
		cleanupLabeling();

		return searchStatus;
	}//label

}// end namespace Search
