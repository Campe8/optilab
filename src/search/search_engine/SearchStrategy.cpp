// Self first
#include "SearchStrategy.hpp"

#include <cassert>

namespace Search {
  
	SearchStrategy::SearchStrategy(const CombinatorSPtr& aSearch)
	: pSearch(aSearch)
	{
		assert(pSearch);
	}

	void SearchStrategy::start(CombinatorNode* aRoot)
	{
		// Set both parent and root combinators as this combinator
		// since this search strategy initiates the search
		pSearch->setRootCombinator(this);
		pSearch->setParentCombinator(this);

		// Start the search on the root node
		pSearch->start(aRoot);
	}//start

	void SearchStrategy::init(CombinatorNode* aParent, CombinatorNode* aChild)
	{
		pSearch->init(aParent, aChild);
	}//init

	void SearchStrategy::exit(CombinatorNode* aNode)
	{
		pSearch->exit(aNode);
	}//exit
  
}// end namespace Search
