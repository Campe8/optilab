// Self first
#include "GeneticSearch.hpp"

#include "Neighborhood.hpp"
#include "SearchHeuristic.hpp"

#include "GeneticSearchEnvironment.hpp"

#include "MetricsRegister.hpp"
#include "GeneticConstants.hpp"

#include <algorithm>
#include <cassert>
#include <utility> // for std::move

namespace Search {
  
  bool fitnessFunction(const std::pair<LocalBaseSearch::NeighborhoodCost, std::size_t>& cost1,
                       const std::pair<LocalBaseSearch::NeighborhoodCost, std::size_t>& cost2)
  {
    return (cost1.first < cost2.first);
  }
  
  std::size_t GeneticSearch::getGeneticMinimumPopulationSize()
  {
    return GeneticLimits::MIN_POPULATION_SIZE;
  }
  
  std::size_t GeneticSearch::getGeneticMaximumPopulationSize()
  {
    return GeneticLimits::MAX_POPULATION_SIZE;
  }
  
  std::size_t GeneticSearch::getGeneticMaximumGenerations()
  {
    return GeneticLimits::MAX_GENERATIONS;
  }
  
  GeneticSearch::GeneticSearch(SemanticPtr aSemantics, EnvironmentList& aEnvironmentList)
  : LocalBaseSearch(std::move(aSemantics), aEnvironmentList,
                    SearchEngineStrategyType::SE_GENETIC)
  , pGenerationCount(0)
  , pImprovingPopulationsCount(0)
  , pActivePopulation(0)
  , pPopulationSize(0)
  , pNumGenerations(0)
  , pMutationChance(0)
  , pMutationSize(0)
  , pBestFitness(LocalBaseSearch::getMaximumCostValue())
  , pActiveNeighborhood(nullptr)
  {
  }
  
  std::shared_ptr<Base::Tools::MetricsRegister> GeneticSearch::getMetrics()
  {
    auto metrics = std::make_shared<Base::Tools::MetricsRegister>();
    metrics->addMetric(GeneticMetrics::NUM_GENERATIONS, pGenerationCount);
    metrics->addMetric(GeneticMetrics::NUM_BEST_UPDATES, pImprovingPopulationsCount);
    
    return metrics;
  }//getMetrics
  
  GeneticSearch::GeneticSearch(SemanticPtr aSemantics, EnvironmentList& aEnvironmentList,
                               std::size_t aSizeNeighborhoods, std::size_t aNumNeighborhoods,
                               std::size_t aPopulationSize, std::size_t aNumGenerations,
                               std::size_t aMutationChance, std::size_t aMutationSize,
                               bool aRunAggressiveSearch)
  : LocalBaseSearch(std::move(aSemantics), aEnvironmentList,
                    SearchEngineStrategyType::SE_GENETIC)
  , pWarmStart(false)
  , pGenerationCount(0)
  , pImprovingPopulationsCount(0)
  , pActivePopulation(0)
  , pPopulationSize(aPopulationSize)
  , pNumGenerations(aNumGenerations)
  , pMutationChance(aMutationChance)
  , pMutationSize(aMutationSize)
  , pBestFitness(LocalBaseSearch::getMaximumCostValue())
  , pActiveNeighborhood(nullptr)
  {
    // Initialize genetic search parameters
    initGeneticParameters();
  }
  
  void GeneticSearch::initGeneticParameters()
  {
    // Mutation chance must be between 0 and 100
    assert(pMutationChance <= 100);
    assert(getNeighborhoodSize() <= 0 ||
           (pMutationSize >= 1 && pMutationSize <= getNeighborhoodSize()));
    assert(pPopulationSize >= getGeneticMinimumPopulationSize());
    assert(pPopulationSize <= getGeneticMaximumPopulationSize());
    assert(pNumGenerations <= getGeneticMaximumGenerations());

    // Reset the internal state of the neighborhood search as if it wasn't a warm restart
    resetNeighborhoodSearchState(false);
  }//initGeneticParameters
  
  void GeneticSearch::resetNeighborhoodSearchState(bool aWarmStart)
  {
    // Keep track of what kind of start is this start
    pWarmStart = aWarmStart;
    
    // Do not reset anything for Genetic Search if it is a warm start
    if (aWarmStart)
    {
      assert(pPopulationList.size() == 2);
      assert(pPopulationCostList.size() == 2);
      for (const auto& population : pPopulationList)
      {
        assert(population.size() == pPopulationSize);
      }
      
      for (const auto& fitness : pPopulationCostList)
      {
        assert(fitness.size() == pPopulationSize);
      }
      
      return;
    }
    // For not warm start, reset the internal state
    
    // Reset the generation count
    pGenerationCount = 0;

    // Preallocate space for the population
    pPopulationList.clear();
    pPopulationList.resize(2);
    for (auto& population : pPopulationList)
    {
      population.resize(pPopulationSize);
    }
    
    pPopulationCostList.clear();
    pPopulationCostList.resize(2);
    for (auto& fitness : pPopulationCostList)
    {
      fitness.resize(pPopulationSize);
    }
  }//resetNeighborhoodSearchState
  
  void GeneticSearch::initLocalSearchEnvironment(LocalSearchEnvironment* aEnv)
  {
    // Set population size, number of generations, mutation change, and mutation size
    assert(aEnv && aEnv->getSearchEngineStrategyType() == SearchEngineStrategyType::SE_GENETIC);
    auto genEnv = static_cast<GeneticSearchEnvironment*>(aEnv);
    
    pPopulationSize = genEnv->getPopulationSize();
    pNumGenerations = genEnv->getNumGenerations();
    pMutationChance = genEnv->getMutationChance();
    pMutationSize   = genEnv->getMutationSize();
    
    // Initialize metric variables
    pGenerationCount = 0;
    pImprovingPopulationsCount = 0;
    
    // Initialize genetic search parameters
    initGeneticParameters();
  }//initLocalSearchEnvironment
  
  GeneticSearch::Population& GeneticSearch::getNextActivePopulation()
  {
    auto popIdx = pActivePopulation + 1;
    assert(!pPopulationList.empty());
    
    return pPopulationList[popIdx % pPopulationList.size()];
  }//getNextActivePopulation
  
  GeneticSearch::PopulationCost& GeneticSearch::getNextActivePopulationCost()
  {
    auto popIdx = pActivePopulation + 1;
    assert(!pPopulationCostList.empty());
    return pPopulationCostList[popIdx % pPopulationCostList.size()];
  }//getNextActivePopulationCost
  
  void GeneticSearch::changeActivePopulation()
  {
    auto popIdx = pActivePopulation + 1;
    pActivePopulation = popIdx % pPopulationList.size();
  }//changeActivePopulation
  
  void GeneticSearch::generatePopulation()
  {
    for(std::size_t indIdx{0}; indIdx < pPopulationSize; ++indIdx)
    {
      auto& individual     = getActivePopulation()[indIdx];
      auto& individualCost = getActivePopulationCost()[indIdx];
      
      auto cost = generateRandomIndividual(individual);
      
      // Set the cost and the index of the individual
      individualCost.first  = cost;
      individualCost.second = indIdx;
    }
  }//generatePopulation
  
  void GeneticSearch::sortPopulation()
  {
    std::sort(getActivePopulationCost().begin(), getActivePopulationCost().end(), fitnessFunction);
  }//sortPopulation
  
  GeneticSearch::NeighborhoodCost GeneticSearch::generateRandomIndividual(Individual& aIndividual)
  {
    assert(aIndividual.first.empty());
    assert(aIndividual.second.empty());
    
    // Assign values to the variables in the neighborhood according to the
    // heuristic set into the neighborhood
    auto neighborhood = getActiveNeighborhood();
    assert(neighborhood);
    
    // Get the heuristic to use to label the neighborhood
    auto heuristic = neighborhood->getSearchHeuristic();
    assert(heuristic);
    
    // Get the list of variables that are part of the neighborhood
    const auto& neighborhoodVars = neighborhood->getNeighborhood();
    assert(!neighborhoodVars.empty());
    
    auto& neighborhoodIndividual = aIndividual.first;
    neighborhoodIndividual.reserve(neighborhoodVars.size());
    for (auto& var : neighborhoodVars)
    {
      // Some of the variables in the neighborhood may be already assigned.
      // This can happen, for example, if some variables in the neighborhood
      // have been declaraed on a singleton domain
      Core::DomainElement* assign{nullptr};
      if (var->isAssigned())
      {
        assign = var->domain()->lowerBound();
      }
      else
      {
        assign = heuristic->getChoiceValue(var.get());
        var->domain()->shrink(assign, assign);
      }
      assert(assign);
      neighborhoodIndividual.push_back(assign);
    }
    
    // Get the cost of this neighborhood
    auto ncost = getNeighborhoodCost(neighborhood);
    
    // When calling the "getNeighborhoodCost()" method, the propagation process "labeled" the
    // objective variables (if any). Register here the values of the objetive variables
    // for this individual before restoring the neighborhoods
    if (searchSemantics()->hasVariableObjective())
    {
      const auto& objList = searchSemantics()->variableObjectiveList();
      assert(aIndividual.second.empty());
      assert(!objList.empty());
      
      int ctr{0};
      aIndividual.second.resize(objList.size());
      for (const auto& var : objList)
      {
        assert(var->isAssigned());
        aIndividual.second[ctr++] = var->domain()->lowerBound();
      }
    }
    
    // Reset the neighborhood status
    restoreNeighborhood();
    
    // Return the cost of the neighborhood
    return ncost;
  }//generateRandomIndividual
  
  std::vector<std::size_t> GeneticSearch::selectParents(std::size_t aNumParents)
  {
    // List of parents to use for reproduction.
    // Default is two parents per individual
    std::vector<std::size_t> parentIdxList;
    for(std::size_t parentIdx{0}; parentIdx < aNumParents; ++parentIdx)
    {
      auto rndChoice = getRandomGenerator().randReal();
      assert(rndChoice >= 0 && rndChoice < 1);
      
      // Get the parent with higher probability depending on its fitness
      auto rawIdx = static_cast<int>(pPopulationSize * pow(rndChoice, 5));
      parentIdxList.push_back(getActivePopulationCost()[rawIdx].second);
    }
    
    return parentIdxList;
  }//selectParents
  
  GeneticSearch::Population GeneticSearch::reproduce(const Individual& aParent1,
                                                     const Individual& aParent2,
                                                     int aCrossoverPoint)
  {
    Population children(2);

    auto& children0 = children[0].first;
    auto& children1 = children[1].first;
    children0.reserve(getNeighborhoodSize());
    children1.reserve(getNeighborhoodSize());
    for (std::size_t geneIdx{0}; geneIdx < getNeighborhoodSize(); ++geneIdx)
    {
      // Copy the genes of the parents into the children
      if (geneIdx <= aCrossoverPoint)
      {
        children0.push_back(aParent1.first[geneIdx]);
        children1.push_back(aParent2.first[geneIdx]);
      }
      else
      {
        children0.push_back(aParent2.first[geneIdx]);
        children1.push_back(aParent1.first[geneIdx]);
      }
    }
    
    return children;
  }//reproduce
  
  void GeneticSearch::mutateGenes(Individual& aIndividual)
  {
    auto rndMutation = getRandomGenerator().randInt(0, 100);
    if(rndMutation >= static_cast<int>(pMutationChance)) return;
    
    // Get the neighborhood as list of variables
    auto neighborhood = getActiveNeighborhood();
    assert(neighborhood);
    
    // Get the heuristic to use to label the neighborhood
    auto heuristic = neighborhood->getSearchHeuristic();
    assert(heuristic);
    
    // Get the list of variables that are part of the neighborhood
    const auto& neighborhoodVars = neighborhood->getNeighborhood();
    assert(!neighborhoodVars.empty());
    
    auto neighborhoodSize = getNeighborhoodSize();
    assert(neighborhoodSize == aIndividual.first.size() &&
           neighborhoodSize == neighborhoodVars.size());

    for(std::size_t geneIdx{0}; geneIdx < pMutationSize; ++geneIdx)
    {
      auto rndGeneIdx = getRandomGenerator().randInt(0, neighborhoodSize-1);
      assert(rndGeneIdx < neighborhoodVars.size() &&
             rndGeneIdx < aIndividual.first.size());
      
      auto& var = neighborhoodVars[rndGeneIdx];
      assert(!var->isAssigned());
      
      auto rndGene = heuristic->getChoiceValue(var.get());
      aIndividual.first[rndGeneIdx]  = rndGene;
    }
  }//mutateGenes
  
  void GeneticSearch::setIndividualOnNeighborhood(const Individual& aIndividual,
                                                  bool aSetObjIndividual)
  {
    auto neighborhood = getActiveNeighborhood();
    assert(neighborhood);
    
    // Get the list of variables that are part of the neighborhood
    const auto& neighborhoodVars = neighborhood->getNeighborhood();
    assert(!neighborhoodVars.empty());
    assert(getNeighborhoodSize() == aIndividual.first.size());
    
    std::size_t geneIdx{0};
    for (auto& var : neighborhoodVars)
    {
      // Some of the variables in the neighborhood may be already assigned.
      // This can happen, for example, if some variables in the neighborhood
      // have been declaraed on a singleton domain
      if (!var->isAssigned())
      {
        auto delement = aIndividual.first[geneIdx];
        var->domain()->shrink(delement, delement);
      }
      geneIdx++;
    }
    
    // Set the objective variables
    if (aSetObjIndividual && searchSemantics()->hasVariableObjective())
    {
      const auto& objList = searchSemantics()->variableObjectiveList();
      assert(aIndividual.second.size() == objList.size());

      int idx{0};
      for (const auto& var : objList)
      {
        // Assign the objective variable with the values of the individual that represent
        // the assignments to the optimized objective variables
        auto objElement = aIndividual.second[idx++];
        
        // Some of the variables in the neighborhood may be already assigned.
        // This can happen, for example, if some variables in the neighborhood
        // have been declaraed on a singleton domain
        if (!var->isAssigned())
        {
          var->domain()->shrink(objElement, objElement);
        }
      }
    }
  }//setIndividualOnNeighborhood
  
  GeneticSearch::NeighborhoodCost GeneticSearch::calculateIndividualFitness(Individual& aIndividual)
  {
    // @note Do not set the objetive values since they will be set by the propagation process
    // when calculating the cost of the current individual
    setIndividualOnNeighborhood(aIndividual, false);
    
    // Get the cose of this neighborhood
    auto fitness = getNeighborhoodCost(getActiveNeighborhood());
    
    // Before restoring the neighborhood, set the optimization value of the given individual
    if (searchSemantics()->hasVariableObjective())
    {
      const auto& objList = searchSemantics()->variableObjectiveList();
      if (aIndividual.second.size() != objList.size())
      {
        aIndividual.second.resize(objList.size());
      }
      
      int ctr{0};
      for (const auto& var : objList)
      {
        assert(var->isAssigned());
        aIndividual.second[ctr++] = var->domain()->lowerBound();
      }
    }

    // Reset the neighborhood status
    restoreNeighborhood();
    
    return fitness;
  }//calculateIndividualFitness
  
  void GeneticSearch::nextGeneration()
  {
    // Generated the new population selecting two individuals
    // according to their fitness and using them to produce two children
    auto& generation = getNextActivePopulation();
    auto& generationCost = getNextActivePopulationCost();
    
    // Initialize the size only once
    if (generation.size() != pPopulationSize)
    {
      assert(generation.size() == generationCost.size());
      
      // Reset previous possible states
      generation.clear();
      generationCost.clear();
      
      // Resize the generation
      generation.resize(pPopulationSize);
      generationCost.resize(pPopulationSize);
    }
    
    int populationIdx{0};
    int populationCostIdx{0};
    for (int childIdx{ 0 }; childIdx < static_cast<int>(pPopulationSize) / 2; ++childIdx)
    {
      // Choose parents to reproduce
      auto parents = selectParents();
      assert(parents.size() == 2);
      
      // Get the reference to the actual parent individuals
      assert(parents[0] < getActivePopulation().size());
      assert(parents[1] < getActivePopulation().size());
      const auto& parent1 = getActivePopulation()[parents[0]];
      const auto& parent2 = getActivePopulation()[parents[1]];
      
      // Choose a crossover point used to "merge" the genes of the two parents
      auto crossoverPoint =
      getRandomGenerator().randInt(0, getNeighborhoodSize() - 1);
      
      // Create two children given the parents and the crossover point
      auto children = reproduce(parent1, parent2, crossoverPoint);
      assert(children.size() == 2);
      
      // Perform a random mutation on both children with probability "pMutationChance"
      mutateGenes(children[0]);
      mutateGenes(children[1]);
      
      // Calculate the cost of the first child.
      // @note this action has side effects on the child in case of optimization problems
      IndividualCost childCost1;
      childCost1.first  = calculateIndividualFitness(children[0]);
      
      // Calculate the cost of the second child.
      // @note this action has side effects on the child in case of optimization problems
      IndividualCost childCost2;
      childCost2.first  = calculateIndividualFitness(children[1]);
      
      // Add the children to the new generation
      generation[populationIdx++] = std::move(children[0]);
      generation[populationIdx++] = std::move(children[1]);
      
      // Get the index of the newly added elements
      childCost1.second = populationIdx - 2;
      childCost2.second = populationIdx - 1;
      
      // Update the costs 
      generationCost[populationCostIdx++] = childCost1;
      generationCost[populationCostIdx++] = childCost2;
      
      // If the cost of the individual is already equal to the global minimum:
      // if the number of solutions to find is 1, there is no need to continue;
      if ((!searchSemantics()->hasVariableObjective()) &&
          (solutionManager()->getSolutionLimit() == 1) &&
          (childCost1.first == LocalBaseSearch::getGlobalMinimumCostValue() ||
           childCost2.first == LocalBaseSearch::getGlobalMinimumCostValue()))
      {
        updateBestNeighborhoodCost(LocalBaseSearch::getGlobalMinimumCostValue(), getActiveNeighborhood());
        
        // Change to next active population
        changeActivePopulation();
        
        // Return asap
        return;
      }
      
    }//for
    assert(generation.size() == pPopulationSize && generationCost.size() == pPopulationSize);
    
    // Switch to the new generation
    changeActivePopulation();
  }//nextGeneration
  
  SearchStatus GeneticSearch::neighborhoodSearch(Neighborhood* aNeighborhood)
  {
    assert(aNeighborhood);
    
    // Set current active neighborhood
    pActiveNeighborhood = aNeighborhood;
    
    // Return asap on timeout
    if (searchTimeout())
    {
      return SearchStatus::SEARCH_FORCED_FAIL;
    }
    
    // Generate a new population only when this is not a warm start,
    // i.e., a population has not been generated yet
    if (!pWarmStart)
    {
      // Generate the initial population
      generatePopulation();
      
      // Check if the population already contains the best individual and this is NOT
      // an optimization search.
      // @note if this is an optimization search, there is no point to check if the cost
      // is equal to the global minima since there is no global minima for optimization search.
      // @note the cost can be the same as the cost of a global minima but this is because
      // the cost was calculated before the optimization constraints were added to the model
      if(getBestNeighborhoodCost() == LocalBaseSearch::getGlobalMinimumCostValue())
      {
        // Return if there is no need to proceed with optimization
        if (!searchSemantics()->hasVariableObjective())
        {
          return SearchStatus::SEARCH_SUCCESS;
        }
        
        // Otherwise reset the best cost for optimizing it with following search
        resetBestNeighborhoodCost();
      }
    }
    
    // Iterate on all the generations:
    // - reset the best fitness found so far
    pBestFitness = LocalBaseSearch::getMaximumCostValue();
    
    bool firstRelativeGeneration{true};
    while ((pGenerationCount < pNumGenerations) && !searchTimeout())
    {
      if (firstRelativeGeneration && pWarmStart)
      {
        // If it is the first (relative) generation and this is a warm start,
        // the population is not changed from previous run meaning that the best individual
        // is still the same and the code below will always pick the same assignment.
        // To avoid this, before sorting the population we give to the first individual
        // the cost of the last individual + 1 (indeed, the already picked individual
        // should not be picked again)
        getActivePopulationCost()[0].first = getActivePopulationCost().back().first + 1;
        
        // Next generation won't be the first anymore
        firstRelativeGeneration = false;
      }
      
      // Sort population by cost
      sortPopulation();
      
      // Update the best fitness score found so far.
      // Return in case of global minima
      auto firstFitness = getActivePopulationCost()[0].first;
      if (firstFitness < pBestFitness)
      {
        pBestFitness = firstFitness;
        
        // Increase the number of improving populations
        pImprovingPopulationsCount++;
        
        // Update the neighborhood that gave the better solution in terms of cost.
        // @note here the variables of the neighborhood are not ground.
        // To update the best neighborhood, the variables must be labeled with the
        // values of the population with cost "firstFitness",
        // i.e., the population used to update the best fitness
        // The following is done as an optimization to avoid useless labeling
        bool setNeighborhood{false};
        if (pBestFitness < getBestNeighborhoodCost())
        {
          // This if check statement is performed inside the method updateBestNeighborhoodCost().
          // Doing it here avoids to label variables if they are not going to be used
          // to improve the current best neighborhood solution
          setNeighborhood = true;
          setIndividualOnNeighborhood(getActivePopulation()[0], true);
        }
        
        // Update the cost
        updateBestNeighborhoodCost(pBestFitness, getActiveNeighborhood());
        
        // Backtrack on neighborhood
        if (setNeighborhood)
        {
          restoreNeighborhood();
        }
        
        // Break the loop in case of global minima and when the current search is not an
        // optimization search (see notes above)
        if (pBestFitness == LocalBaseSearch::getGlobalMinimumCostValue())
        {
          // Increase counter to prepare it for (possible) next warm run
          pGenerationCount++;
          
          // Return if this is not an optimization search, otherwise reset the best
          // cost for next iteration
          if (!searchSemantics()->hasVariableObjective())
          {
            return SearchStatus::SEARCH_SUCCESS;
          }
          
          resetBestNeighborhoodCost();
        }
      }
      
      // Generated the new population selecting two individuals
      // according to their fitness
      nextGeneration();
      
      // Next generation
      pGenerationCount++;
    }//while
    
    assert(aNeighborhood);
    return SearchStatus::SEARCH_TERMINATE;
  }//neighborhoodSearch
  
}// end namespace Search
