// Self first
#include "GeneticConstants.hpp"

namespace Search {
  namespace GeneticMetrics {
    
    const char* NUM_BEST_UPDATES = "numBestUpdates";
    const char* NUM_GENERATIONS  = "numGenerations";
    
  }// end namespace GeneticMetrics
  
  namespace GeneticLimits {
    
    const std::size_t MAX_GENERATIONS = 50000;
    const std::size_t MAX_POPULATION_SIZE = 2048;
    const std::size_t MIN_POPULATION_SIZE = 2;
    
  }// end namespace GeneticLimits
  
}// end namespace Search
