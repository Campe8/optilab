// Base class
#include "HybridBacktrackManager.hpp"

#include <iostream>

#include "BranchingConstraint.hpp"
#include "Node.hpp"

namespace Search {
  
  HybridBacktrackManager::HybridBacktrackManager(const Semantics::ConstraintStoreSPtr& aConstraintStore)
  : pTrailStack(new HybridTrailStack(aConstraintStore))
  {
  }
  
  void HybridBacktrackManager::attachBacktrackableObject(const BacktrackableObjectSPtr& aBckObj)
  {
    assert(aBckObj);
    
    // Return asap if the given object is already registered
    if (pBacktrackableRegister.find(aBckObj->getUUID()) != pBacktrackableRegister.end())
    {
      return;
    }
    
    // Otherwise attach this hybrid backtrack manager to the given backtrackable object.
    // Each backtrackable object (e.g., a domain of a variable) will notify this backtrack
    // manager upon a change
    aBckObj->attachBacktrackManager(this);
   
    // Register the object
    pBacktrackableRegister[aBckObj->getUUID()] = aBckObj;
    
    // Notify an "initial" change to add this new object to the set of backtrackable objects
    // recently changes
    notifyChange(aBckObj.get());
  }//attachBacktrackableObject
  
  void HybridBacktrackManager::detachBacktrackableObject(const BacktrackableObjectSPtr& aBckObj)
  {
    assert(aBckObj);
    
    // Return asap if the given object was not registered within this backtrack manager
    if (pBacktrackableRegister.find(aBckObj->getUUID()) == pBacktrackableRegister.end())
    {
      return;
    }
    
    // Detach backtrack manager
    aBckObj->detachBacktrackManager();
    
    // Clear up the backtrackable object both from the recently changed objects and from
    // the backtrackable objects register
    pChangedBacktrackables.erase(aBckObj->getUUID());
    pBacktrackableRegister.erase(aBckObj->getUUID());
  }//detachBacktrackableObject
  
  void HybridBacktrackManager::saveNodeChoice(Search::Node* aNode)
  {
    assert(aNode);
    assert(!aNode->getBranchingPath()->empty());
    
    pTrailStack->pushNodeChoice(aNode->getBranchingPath()->at(aNode->getTreeLevel()));
  }
  
  void HybridBacktrackManager::saveState(bool aForceSave)
  {
    /*
     * If trail stack is empty and the current state
     * of the search has to be recorded, store every
     * backtrackable since they may have not changed
     * (e.g., a value has been subtracted from the domain of
     * a variable but the other variables do not have any change)
     * but they need to be stored for later propagation.
     */
    if (pTrailStack->empty() || aForceSave)
    {
      forceFullSnapshot();
    }
    
    // Push the current changed backtrackables into the stack
    pTrailStack->push(pChangedBacktrackables, &pBacktrackableRegister);
    
    // Clear changed backtrackable set after push
    pChangedBacktrackables.clear();
  }//saveState
  
  void HybridBacktrackManager::restoreState()
  {
    assert(pTrailStack);
    pTrailStack->pop(pChangedBacktrackables, &pBacktrackableRegister);
  }//restoreState
  
  void HybridBacktrackManager::notifyChange(BacktrackableObject* aBktObject)
  {
    assert(aBktObject);
    
    // Insert the given object in the set of recently changed backtrackable objects
    insertBacktrackableObjectInChangedSet(aBktObject->getUUID());
  }// notifyChange
  
  StackLevel HybridBacktrackManager::getLevel() const
  {
    return pTrailStack->getStackLevel();
  }// getLevel
  
}// end namespace Search
