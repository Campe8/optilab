// Base class
#include "BacktrackableObject.hpp"

#include "BacktrackManager.hpp"

namespace Search {
  
  BacktrackableObject::BacktrackableObject()
  : pBaseBacktrackManager(nullptr)
  {
    pUUIDTag = generateUUIDTag();
  }
  
  BacktrackableObject::~BacktrackableObject()
  {
  }
  
  std::unique_ptr<Memento> BacktrackableObject::getMemento()
  {
    Memento* memento = new Memento();
    assert(memento);
    
    memento->setState(snapshot());
    return std::unique_ptr<Memento>(memento);
  }
  
  void BacktrackableObject::setMemento(std::unique_ptr<Memento> aMemento)
  {
    assert(aMemento);
    reinstateSnapshot(aMemento->getState());
  }
  
  void BacktrackableObject::setMemento(Memento* aMemento)
  {
    assert(aMemento);
    assert(aMemento->hasValidState());
    reinstateSnapshot(aMemento->getState());
  }
  
  void BacktrackableObject::notifyBacktrackManager()
  {
    if(pBaseBacktrackManager)
    {
      pBaseBacktrackManager->notifyChange(this);
    }
  }//notifyBacktrackManager
  
}// end namespace Search
