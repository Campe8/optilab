// Base class
#include "MementoState.hpp"

namespace Search {
  
  MementoState::MementoState(MementoStateId aMementoStateId)
  : pMementoStateId(aMementoStateId)
  {
  }
  
  MementoState::MementoState(const MementoState& aOther)
  {
    pMementoStateId = aOther.pMementoStateId;
  }
  
  MementoState::MementoState(MementoState&& aOther)
  {
    pMementoStateId = aOther.pMementoStateId;
    aOther = MementoStateId::MS_UNDEF;
  }
  
  MementoState::~MementoState()
  {
  }
  
  MementoState& MementoState::operator= (const MementoState& aOther)
  {
    if(this != &aOther)
    {
      pMementoStateId = aOther.pMementoStateId;
    }
    return *this;
  }
  
  MementoState& MementoState::operator= (MementoState&& aOther)
  {
    if(this != &aOther)
    {
      pMementoStateId = aOther.pMementoStateId;
      aOther = MementoStateId::MS_UNDEF;
    }
    return *this;
  }
  
}// end namespace Search
