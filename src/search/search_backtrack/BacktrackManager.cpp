// Base class
#include "BacktrackManager.hpp"

namespace Search {
  
  BacktrackManager::BacktrackManager()
  : pTrailStack(new TrailStack())
  {
  }
  
  void BacktrackManager::attachBacktrackableObject(const BacktrackableObjectSPtr& aBckObj)
  {
    assert(aBckObj);
    if (pBacktrackableRegister.find(aBckObj->getUUID()) != pBacktrackableRegister.end())
    {
      return;
    }
    
    /*
     * Register the manager to the backtrackable object to
     * allow notifications for changes.
     */
    aBckObj->attachBacktrackManager(this);
   
    pBacktrackableRegister[aBckObj->getUUID()] = aBckObj;
    notifyChange(aBckObj.get());
  }//attachBacktrackableObject
  
  void BacktrackManager::detachBacktrackableObject(const BacktrackableObjectSPtr& aBckObj)
  {
    assert(aBckObj);
    if (pBacktrackableRegister.find(aBckObj->getUUID()) == pBacktrackableRegister.end())
    {
      return;
    }
    
    // Detach backtrack manager
    aBckObj->detachBacktrackManager();
    
    pChangedBacktrackables.erase(aBckObj->getUUID());
    pBacktrackableRegister.erase(aBckObj->getUUID());
  }//detachBacktrackableObject
  
  void BacktrackManager::saveState(bool aForceSave)
  {
    assert(pTrailStack);
    /*
     * If trail stack is empty and the current state
     * of the search has to be recorded, store every
     * backtrackable since they may have not changed
     * (e.g., a value has been subtracted from the domain of
     * a variable but the other variables do not have any change)
     * but they need to be stored for later propagation.
     */
    if (pTrailStack->empty() || aForceSave)
    {
      forceFullSnapshot();
    }
    
    // Push the current changed backtrackables into the stack
    pTrailStack->push(pChangedBacktrackables, &pBacktrackableRegister);
    
    // Clear changed backtrackable set after push
    pChangedBacktrackables.clear();
  }//saveState
  
  void BacktrackManager::restoreState()
  {
    assert(pTrailStack);
    pTrailStack->pop(pChangedBacktrackables, &pBacktrackableRegister);
  }//restoreState
  
  void BacktrackManager::notifyChange(BacktrackableObject* aBktObject)
  {
    assert(aBktObject);
    insertBacktrackableObjectInChangedSet(aBktObject->getUUID());
  }// notifyChange
  
  StackLevel BacktrackManager::getLevel() const
  {
    return pTrailStack->getStackLevel();
  }// getLevel
  
}// end namespace Search
