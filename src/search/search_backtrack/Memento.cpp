// Base class
#include "Memento.hpp"
#include "MementoState.hpp"

namespace Search {
  
  Memento::Memento()
  : pState(nullptr)
  {
  }
  
  Memento::Memento(const Memento& aOther)
  {
    if(!aOther.hasValidState())
    {
      pState = nullptr;
    }
    else
    {
      pState = std::unique_ptr<MementoState>(new MementoState(*aOther.pState.get()));
    }
  }
  
  Memento::Memento(Memento&& aOther)
  {
    pState = std::move(aOther.pState);
  }
  
  Memento::~Memento()
  {
  }
  
  Memento& Memento::operator= (const Memento& aOther)
  {
    if (this != &aOther)
    {
      if(!aOther.hasValidState())
      {
        pState = nullptr;
      }
      else
      {
        pState = std::unique_ptr<MementoState>(new MementoState(*aOther.pState.get()));
      }
    }
    return *this;
  }
  
  Memento& Memento::operator= (Memento&& aOther)
  {
    if (this != &aOther)
    {
      pState = std::move(aOther.pState);
    }
    return *this;
  }
  
}// end namespace Search
