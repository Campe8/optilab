// Base class
#include "TrailStack.hpp"

#include <cassert>

namespace Search {
  
  TrailStack::TrailStack()
  : pStackLevel(0)
  {
  }
  
  void TrailStack::pop(ChangedUUIDSet& aBacktrackableObjUUIDSet,
                       BacktrackableObjectRegister* aBkjRegister)
  {
    if (empty())
    {
      // If stack is empty, force level to 0 and return
      pStackLevel = 0;
      return;
    }
    assert(aBkjRegister);
    
    /*
     * Get the set of all backtrackable objects that are on or above the level
     * of the stack that is going to be restored.
     * @note take only the backtrackable objects that are on top of the stack
     */
    StackLevel levelToPopFromStackCtr{ 0 };
    auto stackNodeIt = pStack.begin();
    for (; stackNodeIt != pStack.end() && stackNodeIt->first >= getStackLevel(); ++stackNodeIt)
    {
      aBacktrackableObjUUIDSet.insert(stackNodeIt->second.begin(), stackNodeIt->second.end());
      levelToPopFromStackCtr++;
    }
    
    // Restore backtrackable objects collected from the internal stack
    for (auto& bktObjUUID : aBacktrackableObjUUIDSet)
    {
      restoreBacktrackable(bktObjUUID, aBkjRegister);
    }
 
    // Pop from the stack all levels higher than the level to restore
    for (std::size_t popCtr = 0; popCtr < levelToPopFromStackCtr; ++popCtr)
    {
      pStack.pop_front();
    }

    // Current top of stack is the new active level
    if(empty())
    {
      pStackLevel = 0;
    }
    else
    {
      // Stack level is the top of the stack PLUS 1 as for pushing
      // data into the stack
      pStackLevel = pStack.front().first + 1;
    }
  }//pop
  
  void TrailStack::restoreBacktrackable(const UUIDBacktrackableObj& bktObjUUID,
                                        BacktrackableObjectRegister* aBkjRegister)
  {
    /*
     * First removes the states associated to higher levels of the stack
     * w.r.t. the level that is going to be restored.
     * This will get the correct state to restore from the front of the list associated
     * to the given id.
     */
    assert(aBkjRegister);
    assert(pTrailStackInfo.find(bktObjUUID) != pTrailStackInfo.end());
    
    auto infoNodePairLevel = pTrailStackInfo[bktObjUUID].front().first;
    while (infoNodePairLevel > getStackLevel())
    {
      pTrailStackInfo[bktObjUUID].pop_front();
      infoNodePairLevel = pTrailStackInfo[bktObjUUID].front().first;
    }
    
    // Now the correct state to restore can be retrieved from
    // the front of the list associated to id
    assert(aBkjRegister->find(bktObjUUID) != aBkjRegister->end());
    assert(pTrailStackInfo[bktObjUUID].front().second->hasValidState());
    
    aBkjRegister->at(bktObjUUID)->setMemento(pTrailStackInfo[bktObjUUID].front().second.get());
    assert(pTrailStackInfo[bktObjUUID].front().second->hasValidState());
    
    /*
     * If the restored state is paired to a level that is lower
     * than the level to be restored, then there is no need to remove
     * that node from the map.
     * Otherwise, remove the node from the map.
     */
    if (pTrailStackInfo[bktObjUUID].front().first >= getStackLevel())
    {
      pTrailStackInfo[bktObjUUID].pop_front();
    }
  }//restoreBacktrackable
  
  void TrailStack::push(ChangedUUIDSet& aBacktrackableObjUUIDSet,
                        BacktrackableObjectRegister* aBkjRegister)
  {
    if (aBacktrackableObjUUIDSet.empty())
    {
      return;
    }
    
    for (auto bktObjUUID : aBacktrackableObjUUIDSet)
    {
      assert(aBkjRegister);
      assert(aBkjRegister->find(bktObjUUID) != aBkjRegister->end());

      // Push next level info
      pTrailStackInfo[bktObjUUID].push_front(
      {
        pStackLevel,
        aBkjRegister->at(bktObjUUID)->getMemento()
      });
      assert(pTrailStackInfo[bktObjUUID].front().second);
      assert(pTrailStackInfo[bktObjUUID].front().second->hasValidState());
    }
    
    // Push backtrackable objects on the stack
    pStack.push_front(std::make_pair(pStackLevel, aBacktrackableObjUUIDSet));
    
    // Increase the top level of the stack
    pStackLevel++;
  }//push
  
}// end namespace Search
