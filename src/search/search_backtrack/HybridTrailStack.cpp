// Base class
#include "HybridTrailStack.hpp"

#include <algorithm>  // for std::min
#include <cassert>
#include <stdexcept>  // for std::invalid_argument

namespace {
  constexpr std::size_t kChoiceStackMinSize = 2;
  constexpr std::size_t kChoiceStackMaxSize = 2;
}// namespace

namespace Search {
  
  HybridTrailStack::HybridTrailStack(const Semantics::ConstraintStoreSPtr& aConstraintStore)
  : pChoiceStackMaxSize(kChoiceStackMaxSize)
  , pRecomputationStore(aConstraintStore)
  {
    if (!pRecomputationStore)
    {
      throw std::invalid_argument("Empty constraint store");
    }
  }
  
  void HybridTrailStack::push(BacktrackableIdSet& aBacktrackableIds,
                              BacktrackableObjectRegister* aBacktrackableRegister)
  {
    // If the set of backtrackable ids is empty, there is nothing to do, return asap
    if (aBacktrackableIds.empty())
    {
      return;
    }

    // The current state should be saved ONLY IF it doesn't need to be recomputed.
    // Check the current size of the choice stack for the current level and, if the size
    // is smaller than the choice stack limit, record the changed objects for later
    // restore and return asap
    if (!pStack.empty() &&
        (pStack.top().recomputationStack.size() < pStack.top().recomputationStackMaxSize))
    {
      return;
    }
    
    // Otherwise save the current state
    assert(aBacktrackableRegister);
    
    // Push the backtrackable object ids on the stack
    pStack.push(StackNode(pStackLevel, kChoiceStackMaxSize));
    for (auto& bktPair : *aBacktrackableRegister)
    {
      pStack.top().state[bktPair.first] = bktPair.second->getMemento();
    }

    // Increase the top level of the stack
    pStackLevel++;
  }//push
  
  void HybridTrailStack::pushNodeChoice(const std::shared_ptr<Core::BranchingConstraint>& aChoice)
  {
    // The internal stack shouldn't be empty when pushing a branching choice but it should contain
    // at least the first state
    assert(!pStack.empty());
    pStack.top().recomputationStack.push_back(aChoice);
  }// pushNodeChoice
  
  void HybridTrailStack::pop(BacktrackableIdSet& aBacktrackableIds,
                             BacktrackableObjectRegister* aBkjRegister)
  {
    // If stack is empty, force level to 0 and return asap
    if (empty())
    {
      pStackLevel = 0;
      return;
    }
    assert(aBkjRegister);
    
    // Restore all the state as it was before any changes
    for (auto& objPair : pStack.top().state)
    {
      aBkjRegister->at(objPair.first)->setMemento(objPair.second.get());
      aBacktrackableIds.erase(objPair.first);
    }

    // Work on reconstruction.
    // @note that the reconstruction stack can be empty. This can happen, for example, on a give
    // unsatisfiable problem where there are not choices after the first and only propagation.
    // The first and only propagation fails, and the search restores the original space.
    // Restoring the original space implies calling this method which, in this case, has an
    // empty reconstruction stack
    auto& choiceStack = pStack.top().recomputationStack;
    if (!choiceStack.empty())
    {
      // De-register the choice that is at the top of the stack and detach it from notifiers
      (choiceStack.back())->unsubscribeFromConstraintStore();
      (choiceStack.back())->detachFromSubjects();
      
      // Clear all the changed set of ids since the corresponding objects have been restored
      aBacktrackableIds.clear();
      
      // Remove the constraint that is at the top of the stack
      choiceStack.pop_back();
      
      // Register again all the constraints in the stack.
      // @note register into the constraint store AFTER resetting the state so the constraint
      // store has the state before registration
      for (auto& choice : choiceStack)
      {
        choice->subscribeToConstraintStore(pRecomputationStore);
      }
      
      // Perform adaptive recomputation by reducing to half the maximum size of allowd
      // branching choices in the current recomputation stack
      pStack.top().recomputationStackMaxSize =
      std::max(kChoiceStackMinSize, pStack.top().recomputationStackMaxSize / 2);
    }
  
    // If the stack is empty, the current level should be removed from the stack
    if (choiceStack.empty())
    {
      pStack.pop();
    }

    // Current top of stack is the new active level
    if (empty())
    {
      pStackLevel = 0;
    }
    else
    {
      // Stack level is the top of the stack PLUS 1 as for pushing
      // data into the stack
      pStackLevel = pStack.top().level + 1;
    }
  }//pop
  
}// end namespace Search
