// Self first
#include "RootNeighborhood.hpp"

#include <cassert>

namespace Search {

  RootNeighborhood::RootNeighborhood(const SearchHeuristicSPtr& aSearchHeuristic,
                                     std::size_t aSizeNeighborhoods,
                                     std::size_t aNumNeighborhoods,
                                     const Base::Tools::RandomGeneratorSPtr& aRandomGenerator)
    : Neighborhood(0, aNumNeighborhoods, Internal::randomNeighborhoodSelector,
                   aRandomGenerator.get())
    , pRandomGenerator(aRandomGenerator)
  {
    assert(pRandomGenerator);
    assert(aSearchHeuristic);
    pSearchSpaceRoot = aSearchHeuristic->getBranchingVarList();
    
    // Set the search space.
    // @note for the root node, this is a pointer to itself
    setSearchSpace(&pSearchSpaceRoot);

    // Set the search heuristic
    pSearchHeuristicSPtr = aSearchHeuristic;
    setSearchHeuristic(pSearchHeuristicSPtr.get());
    
    // Generate and set the first neighborhood
    setNeighborhoodSpec(Internal::randomNeighborhoodSelector(&pSearchSpaceRoot, aSizeNeighborhoods,
                                                             pRandomGenerator.get()));
  }
  
}// end namespace Search
