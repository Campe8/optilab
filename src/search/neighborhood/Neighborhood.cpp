// Base class
#include "Neighborhood.hpp"

#include "DomainElementManager.hpp"
#include "DomainElementInt64.hpp"

#include "BaseTools.hpp"

#include <algorithm>
#include <numeric>
#include <cassert>

namespace Search {
  
  namespace Internal {
    
    NeighborhoodSpec randomNeighborhoodSelector(NeighborhoodSearchSpace* aSearchSpace,
                                                std::size_t aSize,
                                                Base::Tools::RandomGenerator* aRandomGenerator)
    {
      assert(aRandomGenerator);
      
      // Randomly select a large neighborhood from
      // the set of variables in the search space.
      // Use the Fisher–Yates shuffle algorithm for
      // determining the next neighborhood in O(n) time
      auto neighborhoodSize = static_cast<int>(aSize);
      auto searchSpaceSize  = static_cast<int>(aSearchSpace->size());
      assert(neighborhoodSize <= searchSpaceSize);
      
      // Initialize the distribution
      //boost::random::uniform_int_distribution<> dist(0, searchSpaceSize-1);
      
      std::vector<std::size_t> allIndeces(searchSpaceSize);
      std::iota(std::begin(allIndeces), std::end(allIndeces), 0);
      
      auto startingPoint = searchSpaceSize - neighborhoodSize;
      for(int idx{searchSpaceSize - 1}; idx >= startingPoint; --idx)
      {
        // Pick a random index from 0 to idx
        int rndIdx = aRandomGenerator->randInt(0, searchSpaceSize-1);
        
        // Swap allIndeces[i] with the element at random index
        std::swap(allIndeces[idx], allIndeces[rndIdx]);
      }
      
      NeighborhoodSpec neighborhoodSpec(neighborhoodSize);
      for(int idx{0}; idx < neighborhoodSize; ++idx)
      {
        neighborhoodSpec[idx] = allIndeces[startingPoint + idx];
      }
      
      return neighborhoodSpec;
    }//randomNeighborhoodSelector
    
  }//end namespace internal
  
  Neighborhood::Neighborhood(std::size_t aNeighborhoodId,
                             std::size_t aNumNeighborhoods,
                             NeighborhoodSelector aNeighborhoodSelector,
                             Base::Tools::RandomGenerator* aRandomGenerator)
  : SearchChoice(SearchChoiceType::SEARCH_CHOICE_NEIGHBORHOOD)
  , pNeighborhoodId(aNeighborhoodId)
  , pNeighborhoodSetSize(aNumNeighborhoods)
  , pNeighborhoodSelector(aNeighborhoodSelector)
  , pSearchSpace(nullptr)
  , pRandomGenerator(aRandomGenerator)
  {
    assert(pRandomGenerator);
  }
  
  Neighborhood::Neighborhood(NeighborhoodSearchSpace* aSearchSpace,
                             SearchHeuristicPtr aSearchHeuristic,
                             const NeighborhoodSpec& aNeighborhoodSpec,
                             std::size_t aNeighborhoodId,
                             std::size_t aNumNeighborhoods,
                             NeighborhoodSelector aNeighborhoodSelector,
                             Base::Tools::RandomGenerator* aRandomGenerator)
  : SearchChoice(SearchChoiceType::SEARCH_CHOICE_NEIGHBORHOOD)
  , pNeighborhoodId(aNeighborhoodId)
  , pNeighborhoodSetSize(aNumNeighborhoods)
  , pNeighborhoodSelector(aNeighborhoodSelector)
  , pNeighborhoodSpec(aNeighborhoodSpec)
  , pSearchSpace(aSearchSpace)
  , pSearchHeuristic(aSearchHeuristic)
  , pRandomGenerator(aRandomGenerator)
  {
    assert(aSearchSpace);
    assert(aSearchHeuristic);
    assert(!pNeighborhoodSpec.empty());
    assert(pRandomGenerator);
  }
  
  bool Neighborhood::isa(const SearchChoice* aChoice)
  {
    return aChoice &&
    aChoice->getSearchChoiceType() == SearchChoiceType::SEARCH_CHOICE_NEIGHBORHOOD;
  }//isa
  
  Neighborhood* Neighborhood::cast(SearchChoice* aChoice)
  {
    if (!isa(aChoice)) return nullptr;
    return static_cast<Neighborhood*>(aChoice);
  }//cast
  
  const Neighborhood* Neighborhood::cast(const SearchChoice* aChoice)
  {
    if (!isa(aChoice)) return nullptr;
    return static_cast<const Neighborhood*>(aChoice);
  }//cast
  
  bool Neighborhood::hasSuccessor()
  {
    // @note last neighborhood does not have a successor
    return pNeighborhoodId < (pNeighborhoodSetSize - 1);
  }//hasSuccessor
  
  NeighborhoodSearchSpace Neighborhood::getNeighborhood() const
  {
    if(pNeighborhoodVars.empty())
    {
      pNeighborhoodVars.reserve(pNeighborhoodSpec.size());
      for(auto idx : pNeighborhoodSpec)
      {
        pNeighborhoodVars.push_back(pSearchSpace->at(idx));
      }
    }
    return pNeighborhoodVars;
  }//getNeighborhood
  
  void Neighborhood::setNeighborhoodSpec(const NeighborhoodSpec& aNeighborhoodSpec)
  {
    // Reset current neighborhood vars if any
    pNeighborhoodVars.clear();
    
    // Set the new spec
    pNeighborhoodSpec = aNeighborhoodSpec;
  }//setNeighborhoodSpec
  
  Neighborhood* Neighborhood::next()
  {
    // If this was the last neighborhood, return nullptr
    if(pNeighborhoodId + 1 >= pNeighborhoodSetSize) return nullptr;
    
    if (!isNeighborhoodExtended())
    {
      assert(!pSearchSpace->empty());
      
      /*
       * Init next neighborhood specification.
       * @note this must be done here
       * and not at init time in constructor
       * since it should be done after propagation
       * took place and domains are shrinked in case of
       * the init function to be a solution to the model.
       */
      extendNeighborhood();
    }
    
    assert(isNeighborhoodExtended());
    
    // Return the new neighborhood extending this neighborhood
    // and updates the index of the next neighborhood id
    // as side effect
    return new Neighborhood(pSearchSpace,
                            pSearchHeuristic,
                            pNextNeighborhoodSpec,
                            pNeighborhoodId + 1,
                            pNeighborhoodSetSize,
                            pNeighborhoodSelector,
                            pRandomGenerator);
  }//next
  
  void Neighborhood::extendNeighborhood()
  {
    // Call the selector on the search space with the same size
    // of the current spec for the next neighborhood
    pNextNeighborhoodSpec = pNeighborhoodSelector(pSearchSpace, pNeighborhoodSpec.size(),
                                                  pRandomGenerator);
  }//extendNeighborhood
  
}// end namespace Search
