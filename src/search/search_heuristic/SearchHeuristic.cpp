// Base class
#include "SearchHeuristic.hpp"

// Metric factories
#include "ChoiceMetricFactory.hpp"

// Void element
#include "DomainElementManager.hpp"

// Variable explosion tool
#include "VariableExplosionTools.hpp"

// Memento semantics
#include "MementoStateSearchHeuristic.hpp"
#include "SearchHeuristicState.hpp"

#include "ValueChoiceMetricIndomainRandom.hpp"

#include "BaseTools.hpp"

#include <algorithm>
#include <numeric>
#include <cassert>

namespace Search {
  
  SearchHeuristic::SearchHeuristic(const std::vector<Core::VariableSPtr>& aBranchingVariableList,
                                   VariableChoiceMetricType aVarChoiceMetricType,
                                   ValueChoiceMetricType aValChoiceMetricType)
  : pChoiceVarIdx(-1)
  , pPartitionSetIdx(0)
  , pRandomGenerator(nullptr)
  {
    // Reserve at least |aBranchingVariableList| variables for the branching list
    pBranchingVarVector = Core::Tools::explodeVariables(aBranchingVariableList);
    
    // Instantiate metrics according to given types
    pValChoiceMetric = ValueChoiceMetricFactory::getValueChoiceMetric(aValChoiceMetricType);
    pVarChoiceMetric = VariableChoiceMetricFactory::getVariableChoiceMetric(aVarChoiceMetricType);
    
    // Fill the partition set
    pBranchingVarPartitionSet = std::vector<std::size_t>(pBranchingVarVector.size());
    std::iota(std::begin(pBranchingVarPartitionSet), std::end(pBranchingVarPartitionSet), 0);
    
    // Sort the partition set s.t. no variables are assigned if their index
    // in pBranchingVarVector is greater than pPartitionSetIdx
    sortPartitionSet();
  }
  
  void SearchHeuristic::setRandomGenerator(const Base::Tools::RandomGeneratorSPtr& aRandomGenerator)
  {
    // Set the pointer to the random generator
    pRandomGenerator = aRandomGenerator;
    
    // Set the random generator for all the metrics that use randomness:
    // - ValueChoiceMetricIndomainRandom
    if (pValChoiceMetric->getValueChoiceMetricType() ==
        ValueChoiceMetricType::VAL_METRIC_INDOMAIN_RANDOM)
    {
      static_cast<ValueChoiceMetricIndomainRandom*>(pValChoiceMetric.get())->
      setRandomGenerator(pRandomGenerator.get());
    }
  }//setRandomGenerator
  
  std::unique_ptr<Search::MementoState> SearchHeuristic::snapshot()
  {
    std::unique_ptr<SearchHeuristicState> shState(new SearchHeuristicState(pChoiceVarIdx, pPartitionSetIdx, pBranchingVarPartitionSet));
    return std::unique_ptr<MementoStateSearchHeuristic>(new MementoStateSearchHeuristic(std::move(shState)));
  }//snapshot
  
  void SearchHeuristic::reinstateSnapshot(const Search::MementoState* aSnapshot)
  {
    assert(aSnapshot);
    assert(MementoStateSearchHeuristic::isa(aSnapshot));
    
    const SearchHeuristicState* mementoState = MementoStateSearchHeuristic::cast(aSnapshot)->getSearchHeuristicState();
    assert(mementoState);
    
    pChoiceVarIdx = mementoState->choiceVarIdx();
    pPartitionSetIdx = mementoState->partitionSetIdx();
    pBranchingVarPartitionSet = mementoState->branchingVarPartitionSet();
  }//reinstateSnapshot
  
  void SearchHeuristic::sortPartitionSet()
  {
    assert(pBranchingVarVector.size() == pBranchingVarPartitionSet.size());
    for(std::size_t tempIdx = 0; tempIdx < pBranchingVarPartitionSet.size(); ++tempIdx)
    {
      if(pBranchingVarVector[pBranchingVarPartitionSet[tempIdx]]->isAssigned())
      {
        std::swap(pBranchingVarPartitionSet[pPartitionSetIdx], pBranchingVarPartitionSet[tempIdx]);
        pPartitionSetIdx++;
      }
    }
  }//sortPartitionSet
  
  std::size_t SearchHeuristic::choiceSetSize() const
  {
    std::size_t nonAssignedVars{0};
    std::size_t ctr = pPartitionSetIdx;
    
    if (!isAttachedToCaretaker())
    {
      // If this object doesn't have memento semantics,
      // it doesn't hold an internal consistent state
      ctr = 0;
    }
    for (; ctr < pBranchingVarPartitionSet.size(); ++ctr)
    {
      if (!pBranchingVarVector[pBranchingVarPartitionSet[ctr]]->isAssigned())
      {
        nonAssignedVars++;
      }
    }
    return nonAssignedVars;
  }// choiceSetSize
  
  bool SearchHeuristic::isChoiceSetEmpty() const
  {
    std::size_t ctr = pPartitionSetIdx;
    if (!isAttachedToCaretaker())
    {
      // If this object doesn't have memento semantics,
      // it doesn't hold an internal consistent state
      ctr = 0;
    }
    for (; ctr < pBranchingVarPartitionSet.size(); ++ctr)
    {
      if (!pBranchingVarVector[pBranchingVarPartitionSet[ctr]]->isAssigned())
      {
        // At least one unassigned variable, which means that this choice set
        // is not empty
        return false;
      }
    }
    return true;
  }// isChoiceSetEmpty
  
  Core::DomainElement* SearchHeuristic::getChoiceValue(Core::VariablePtr aVar)
  {
    Core::VariablePtr currentVar = aVar;
    if(!currentVar)
    {
      currentVar = getInternalChoiceVar();
    }
    if (!currentVar)
    {
      // Return void element if current variable is nullptr
      return Core::DomainElementManager::getInstance().getVoidElement();
    }
    
    assert(currentVar->hasPrimitiveType());
    return pValChoiceMetric->getValue(currentVar->domain().get());
  }//getChoiceValue
  
  Core::VariableSPtr SearchHeuristic::getChoiceVariable()
  {
    // If there are no variables return nullptr
    if (pBranchingVarVector.empty())
    {
      pChoiceVarIdx = -1;
      return nullptr;
    }
    
    if(!isAttachedToCaretaker())
    {
      // If this object doesn't have memento semantics,
      // it doesn't hold an internal consistent state
      pPartitionSetIdx = 0;
    }
    
    /*
     * The partition set and the pPartitionSetIdx cannot hold the invariant
     * here since before getChoiceVariable some variable could have been assigned,
     * for example, after propagation.
     * Find the next unassigned variable.
     */
    while(pPartitionSetIdx < pBranchingVarPartitionSet.size() &&
          pBranchingVarVector[pBranchingVarPartitionSet[pPartitionSetIdx]]->isAssigned())
    {
      pPartitionSetIdx++;
    }
    
    // If he size of the choice set is zero,
    // return nullptr
    if(choiceSetSize() == 0)
    {
      pChoiceVarIdx = -1;
      return nullptr;
    }
    
    // Find the first non singleton variable to branch on
    pChoiceVarIdx = pBranchingVarPartitionSet[pPartitionSetIdx];
    assert(!getInternalChoiceVar()->isAssigned());
    
    if(pPartitionSetIdx == pBranchingVarPartitionSet.size() - 1)
    {
      // Handle case where there is only one non assigned variable left.
      // Notify backtrack manager before returning
      notifyBacktrackManager();
      
      return pBranchingVarVector[pChoiceVarIdx];
    }
    
    // Get current variable as base comparison variable
    Core::Variable* branchingVar = getInternalChoiceVar();
    
    // Start evaluation and comparison from next index, greater than current var
    std::size_t trialVarIdx{ pPartitionSetIdx + 1 };
    for (; trialVarIdx < pBranchingVarPartitionSet.size(); ++trialVarIdx)
    {
      // Current variable is assigned -> swap it with pPartitionSetIdx and increase
      // pPartitionSetIdx to preserve the invariant
      if(pBranchingVarVector[pBranchingVarPartitionSet[trialVarIdx]]->isAssigned())
      {
        std::swap(pBranchingVarPartitionSet[pPartitionSetIdx], pBranchingVarPartitionSet[trialVarIdx]);
        pPartitionSetIdx++;
        
        // Current variable has been already evaluated, continue;
        continue;
      }
      assert(!pBranchingVarVector[pBranchingVarPartitionSet[trialVarIdx]]->isAssigned());
      
      // Check metric
      auto metricEval = pVarChoiceMetric->compare(pBranchingVarVector[pBranchingVarPartitionSet[trialVarIdx]].get(),
                                                  branchingVar);
      
      if (metricEval == VariableMetricEval::VAR_METRIC_GT)
      {
        // trialVarIdx has a better metric
        branchingVar = pBranchingVarVector[pBranchingVarPartitionSet[trialVarIdx]].get();
        pChoiceVarIdx = pBranchingVarPartitionSet[trialVarIdx];
      }
    }//trialVarIdx
    
    assert(pChoiceVarIdx >= 0 && pChoiceVarIdx < pBranchingVarVector.size());
    notifyBacktrackManager();
    
    return pBranchingVarVector[pChoiceVarIdx];
  }//getChoiceValue
  
}// end namespace Search





