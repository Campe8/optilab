// Base class
#include "MementoStateSearchHeuristic.hpp"

#include <cassert>

namespace Search {
  
  MementoStateSearchHeuristic::MementoStateSearchHeuristic(std::unique_ptr<SearchHeuristicState> aSearchHeuristicState)
  : MementoState(MementoStateId::MS_SEARCH_HEURISTIC)
  , pSearchHeuristicState(std::move(aSearchHeuristicState))
  {
  }
  
  MementoStateSearchHeuristic::~MementoStateSearchHeuristic()
  {
  }
  
}// end namespace Search
