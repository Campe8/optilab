// Base class
#include "SearchHeuristicState.hpp"

#include <cassert>

namespace Search {
  
  SearchHeuristicState::SearchHeuristicState(long int aChoiceVarIdx, std::size_t aPartitionSetIdx,
                                             const std::vector<std::size_t>& aBranchingVarPartitionSet)
  : pChoiceVarIdx(aChoiceVarIdx)
  , pPartitionSetIdx(aPartitionSetIdx)
  , pBranchingVarPartitionSet(aBranchingVarPartitionSet)
  {
  }
  
  SearchHeuristicState::~SearchHeuristicState()
  {
  }
  
}// end namespace Search

