// Self first
#include "LocalSearchEnvironment.hpp"

namespace Search {
  
  LocalSearchEnvironment::LocalSearchEnvironment(const std::vector<VarSem>& aEnvironment,
                                                 VariableChoiceMetricType aVarChoice,
                                                 ValueChoiceMetricType aValChoice,
                                                 int aSizeNeighborhoods,
                                                 int aNumNeighborhoods,
                                                 bool aRunAggressiveSearch,
                                                 SearchEngineStrategyType aStrategyType,
                                                 const std::string& aEnvId)
  : SearchEnvironment(aEnvironment, aVarChoice, aValChoice, aEnvId)
  , pNeighborhoodSize(aSizeNeighborhoods)
  , pNumNeighborhoods(aNumNeighborhoods)
  , pRunAggressiveSearch(aRunAggressiveSearch)
  , pWarmStart(true)
  , pRandomSeed(-1)
  , pStrategyType(aStrategyType)
  {
  }
  
  
  
}// end namespace Search
