// Base class
#include "SearchEnvironment.hpp"
#include "VariableSemanticObjective.hpp"

#include <cassert>

namespace Search {
  
  SearchEnvironment::SearchEnvironment(const std::vector<VarSem>& aEnvironment,
                                       VariableChoiceMetricType aVarChoice,
                                       ValueChoiceMetricType aValChoice,
                                       const std::string& aId)
  : pEnvironmentId(aId)
  , pTimeout(-1)
  {
    // Initialize the heuristic for this environment
    initializeEnvironment(aEnvironment, aVarChoice, aValChoice);
  }
  
  bool SearchEnvironment::isEnvironmentVariable(const Core::VariableSPtr& aVar)
  {
    assert(aVar);
    return isEnvironmentSemantic(aVar->variableSemantic());
  }//isEnvironmentVariable
  
  bool SearchEnvironment::isEnvironmentSemantic(const Core::VariableSemantic* aSem)
  {
    assert(aSem);
    return aSem->isBranching() || aSem->isSolutionElement();
  }//isEnvironmentSemantic
  
  std::vector<Core::VariableSPtr> SearchEnvironment::getBranchingVariables() const
  {
    std::vector<Core::VariableSPtr> brcVars;
    for(auto& idx : pBrcVars)
    {
      assert(idx < pEnvVars.size());
      assert(idx < pEnvSems.size());
      pEnvVars[idx]->uploadSemantic(pEnvSems[idx]);
      brcVars.push_back(pEnvVars[idx]);
    }
    return brcVars;
  }//getOutputVariables
  
  std::vector<Core::VariableSPtr> SearchEnvironment::getObjectiveVariables() const
  {
    std::vector<Core::VariableSPtr> objVars;
    for(auto& idx : pObjVars)
    {
      assert(idx < pEnvVars.size());
      assert(idx < pEnvSems.size());
      pEnvVars[idx]->uploadSemantic(pEnvSems[idx]);
      objVars.push_back(pEnvVars[idx]);
    }
    return objVars;
  }//getObjectiveVariables
  
  const std::vector<Core::VariableSPtr>& SearchEnvironment::getEnvironmentVariables() const
  {
    std::size_t idx{0};
    for(auto& var : pEnvVars)
    {
      var->uploadSemantic(pEnvSems[idx++]);
    }
    return pEnvVars;
  }//getObjectiveVariables
  
  void SearchEnvironment::initializeEnvironment(const std::vector<VarSem>& aEnvironment,
                                                VariableChoiceMetricType aVarChoice,
                                                ValueChoiceMetricType aValChoice)
  {
    // Prepare the set of branching variables for the heuristic
    std::vector<Core::VariableSPtr> branchingVars;
    
    branchingVars.reserve(aEnvironment.size());
    pEnvVars.reserve(aEnvironment.size());
    pEnvSems.reserve(aEnvironment.size());
    for(auto& vs : aEnvironment)
    {
      pEnvVars.push_back(vs.first);
      pEnvSems.push_back(vs.second);
      if (vs.second->isBranching())
      {
        // Check that all the branching variables are all different variables
        branchingVars.push_back(vs.first);
        pBrcVars.push_back(pEnvVars.size() - 1);
      }
      if (vs.second->getSemanticClassType() == Core::VariableSemanticClassType::VAR_SEMANTIC_CLASS_OBJECTIVE)
      {
        pObjVars.push_back(pEnvVars.size() - 1);
      }
    }
    
    pSearchHeuristic = std::make_shared<SearchHeuristic>(branchingVars, aVarChoice, aValChoice);
  }//initSearchHeuristic
  
}// end namespace Search
