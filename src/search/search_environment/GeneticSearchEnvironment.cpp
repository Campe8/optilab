// Self first
#include "GeneticSearchEnvironment.hpp"

#include "GeneticSearch.hpp"
#include "SearchHeuristicDefs.hpp"


#include <algorithm>

namespace Search {
  
  GeneticSearchEnvironment::GeneticSearchEnvironment(const std::vector<VarSem>& aEnvironment,
                                                     int aSizeNeighborhoods,
                                                     int aNumNeighborhoods,
                                                     bool aRunAggressiveSearch,
                                                     const std::string& aEnvId)
  : LocalSearchEnvironment(aEnvironment,
                           VariableChoiceMetricType::VAR_CM_INPUT_ORDER,      // Use input order for variable selection
                           ValueChoiceMetricType::VAL_METRIC_INDOMAIN_RANDOM, // Use random value assignments
                           aSizeNeighborhoods,
                           aNumNeighborhoods,
                           aRunAggressiveSearch,
                           SearchEngineStrategyType::SE_GENETIC,              // Environment used by genetic search
                           aEnvId)
  , pPopulationSize(0)
  , pNumGenerations(0)
  , pMutationChance(0)
  , pMutationSize(0)
  {
  }
  
  void GeneticSearchEnvironment::setPopulationSize(std::size_t aPopSize)
  {
    pPopulationSize = aPopSize;
    pPopulationSize = std::min( std::max(pPopulationSize, GeneticSearch::getGeneticMinimumPopulationSize()),
                                GeneticSearch::getGeneticMaximumPopulationSize() );
  }//setPopulationSize
  
  void GeneticSearchEnvironment::setNumGenerations(std::size_t aNumGen)
  {
    pNumGenerations = aNumGen;
    pNumGenerations = std::min( pNumGenerations, GeneticSearch::getGeneticMaximumGenerations() );
  }//setNumGenerations
  
  void GeneticSearchEnvironment::setMutationChance(std::size_t aMutChance)
  {
    pMutationChance = aMutChance;
    pMutationChance = std::min( pMutationChance, static_cast<std::size_t>(100) );
  }//setMutationChance
  
  void GeneticSearchEnvironment::setMutationSize(std::size_t aMutSize)
  {
    pMutationSize = aMutSize;
    
    // Set min-max values on mutation size
    pMutationSize = std::max(pMutationSize, static_cast<std::size_t>(1));
    pMutationSize = std::min(pMutationSize,
                             static_cast<std::size_t>(getNeighborhoodSize() > 0 ?
                                                      getNeighborhoodSize() : 0));
  }//setMutationSize
  
}// end namespace Search
