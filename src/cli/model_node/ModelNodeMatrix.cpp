// Base class
#include "ModelNodeMatrix.hpp"

#include "CLIMacro.hpp"
#include "CLIUtils.hpp"
#include <cassert>

namespace CLI {

  ModelNodeMatrix::ModelNodeMatrix(const std::string& aID, std::unique_ptr<MatrixDeclaration> aMatDecl)
  : ModelNode(CLIMsgType::CLI_MSG_MATRIX_DECL)
  , pMatrixID(aID)
  , pMatrix(aMatDecl->getMatrix())
  {
    pBoolean = aMatDecl->isBoolean();
    pOverflow = aMatDecl->hasOverflow();
  }

  bool ModelNodeMatrix::isa(const ModelNode* aModelNode)
  {
    return aModelNode &&
    aModelNode->nodeType() == CLIMsgType::CLI_MSG_MATRIX_DECL;
  }//isa

  const ModelNodeMatrix* ModelNodeMatrix::cast(const ModelNode* aModelNode)
  {
    if (!isa(aModelNode))
    {
      return nullptr;
    }
    return static_cast<const ModelNodeMatrix*>(aModelNode);
  }//cast

  ModelNodeMatrix* ModelNodeMatrix::cast(ModelNode* aModelNode)
  {
    if (!isa(aModelNode))
    {
      return nullptr;
    }
    return static_cast<ModelNodeMatrix*>(aModelNode);
  }//cast

  CLIStmt ModelNodeMatrix::toCLIStmt()
  {
    CLIStmt strMsg;
    
    // Set type of message
    strMsg[CLI_MSG_FIELD_DELIM] = CLI_MSG_TYPE_VALUE_MAT_DECL;
    
    // Set matrix id
    strMsg[CLI_MSG_TAG_MAT_ID] = pMatrixID;
    
    // Set matrix values type
    strMsg[CLI_MSG_TAG_MAT_TYPE] = pBoolean ? CLI_MSG_VAL_MAT_TYPE_BOOL : CLI_MSG_VAL_MAT_TYPE_INT;
    
    // Set number of rows
    strMsg[CLI_MSG_TAG_MAT_NUM_ROWS] = Utils::toString<std::size_t>(pMatrix.size());
    
    // Set number of columns
    strMsg[CLI_MSG_TAG_MAT_NUM_COLS] = pMatrix.empty() ?  Utils::toString<std::size_t>(0) : Utils::toString<std::size_t>(pMatrix[0].size());
    
    // Set matrix rows
    std::string matRows;
    for(const auto& r : pMatrix)
    {
      std::string row;
      for(const auto& e : r)
      {
        row += e;
        row += ", ";
      }
      if(!row.empty())
      {
        row.pop_back();
        row.pop_back();
      }
      matRows += row;
      matRows += CLI_MSG_TAG_MAT_ROWS_SEP;
    }
    if(!matRows.empty())
    {
      matRows.pop_back();
    }
    if(!matRows.empty())
    {
      strMsg[CLI_MSG_TAG_MAT_ROWS] = matRows;
    }

    // Set overflow if any
    if(pOverflow)
    {
      strMsg[CLI_MSG_TAG_EVENT] = CLI_MSG_VAL_EVENT_SATURATION;
    }
    
    return strMsg;
  }//toCLIStmt

}// end namespace CLI

