// Base class
#include "ModelNodeError.hpp"

#include "CLIMacro.hpp"

#include <cassert>

namespace CLI {

  ModelNodeError::ModelNodeError(std::string aErrorMsg)
    : ModelNode(CLIMsgType::CLI_MSG_ERR)
    , pErrorMsg(aErrorMsg)
  {
  }

  bool ModelNodeError::isa(const ModelNode* aModelNode)
  {
    assert(aModelNode);
    return aModelNode->nodeType() == CLIMsgType::CLI_MSG_ERR;
  }//isa

  /// Cast to ModelNodeError.
  /// Returns nullptr if "aModelNode" is not an instance of ModelNodeVariable
  const ModelNodeError* ModelNodeError::cast(const ModelNode* aModelNode)
  {
    if (!isa(aModelNode))
    {
      return nullptr;
    }
    return static_cast<const ModelNodeError*>(aModelNode);
  }//cast

  ModelNodeError* ModelNodeError::cast(ModelNode* aModelNode)
  {
    if (!isa(aModelNode))
    {
      return nullptr;
    }
    return static_cast<ModelNodeError*>(aModelNode);
  }//cast

  CLIStmt ModelNodeError::toCLIStmt()
  {
    CLIStmt strMsg;

    // Set type of message
    strMsg[CLI_MSG_FIELD_DELIM] = CLI_MSG_TYPE_VALUE_ERR_DECL;

    // Set error message
    strMsg[CLI_MSG_TAG_ERR_MSG] = pErrorMsg;

    return strMsg;
  }//toCLIStmt

}// end namespace CLI

