// Base class
#include "ModelNodeFunctionCall.hpp"

#include "FcnArg.hpp"

#include "CLIMacro.hpp"

#include "CLIUtils.hpp"

#include <iostream>
#include <sstream>
#include <cassert>

namespace CLI {

  ModelNodeFunctionCall::ModelNodeFunctionCall(const std::string& aFcn, const std::string& aArgs)
    : ModelNode(CLIMsgType::CLI_MSG_FCN_CALL)
    , pFcnName(aFcn)
  {
    setArgs(aArgs);
  }

  bool ModelNodeFunctionCall::isa(const ModelNode* aModelNode)
  {
    assert(aModelNode);
    return aModelNode->nodeType() == CLIMsgType::CLI_MSG_FCN_CALL;
  }//isa

  /// Cast to ModelNodeVariable.
  /// Returns nullptr if "aModelNode" is not an instance of ModelNodeVariable
  const ModelNodeFunctionCall* ModelNodeFunctionCall::cast(const ModelNode* aModelNode)
  {
    if (!isa(aModelNode))
    {
      return nullptr;
    }
    return static_cast<const ModelNodeFunctionCall*>(aModelNode);
  }//cast

  ModelNodeFunctionCall* ModelNodeFunctionCall::cast(ModelNode* aModelNode)
  {
    if (!isa(aModelNode))
    {
      return nullptr;
    }
    return static_cast<ModelNodeFunctionCall*>(aModelNode);
  }//cast

  CLIStmt ModelNodeFunctionCall::toCLIStmt()
  {
    
    CLIStmt strMsg;

    // Set type of message
    strMsg[CLI_MSG_FIELD_DELIM] = CLI_MSG_TYPE_VALUE_FCN_CALL_DECL;

    // Set variable id
    strMsg[CLI_MSG_TAG_FCN_CALL_ID] = pFcnName;

    // Set arguments
    std::stringstream ss;
    ss << pNumArgs;
    strMsg[CLI_MSG_TAG_FCN_CALL_NUM_ARGS] = ss.str();
    
    if(pNumArgs > 0)
    {
      strMsg[CLI_MSG_TAG_FCN_CALL_ARGS] = pArgs;
    }
    
    return strMsg;
  }//toCLIStmt
  
  void ModelNodeFunctionCall::setArgs(const std::string& aArgs)
  {
    auto args = CLI::Utils::splitArguments(aArgs);
    pNumArgs = args.size();
    pArgs = aArgs;
    return;
  }//setArgs
  
}// end namespace CLI

