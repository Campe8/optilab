// Base class
#include "ModelNodeVariable.hpp"

#include "CLIMacro.hpp"

#include <cassert>

namespace CLI {

  ModelNodeVariable::ModelNodeVariable(const std::string& aVarId,
                                       std::unique_ptr<DomainDeclaration> aDomainDeclaration)
  : ModelNode(CLIMsgType::CLI_MSG_VAR_DECL)
  , pVarId(aVarId)
  , pDomainDeclaration(std::move(aDomainDeclaration))
  , pMartrixDeclaration(nullptr)
  {
  }
  
  ModelNodeVariable::ModelNodeVariable(const std::string& aVarId,
                                       std::unique_ptr<DomainDeclaration> aDomainDeclaration,
                                       std::unique_ptr<MatrixRowDeclaration> aMatrixDeclaration)
  : ModelNode(CLIMsgType::CLI_MSG_VAR_DECL)
  , pVarId(aVarId)
  , pDomainDeclaration(std::move(aDomainDeclaration))
  , pMartrixDeclaration(std::move(aMatrixDeclaration))
  {
  }

  bool ModelNodeVariable::isa(const ModelNode* aModelNode)
  {
    assert(aModelNode);
    return aModelNode->nodeType() == CLIMsgType::CLI_MSG_VAR_DECL;
  }//isa

  /// Cast to ModelNodeVariable.
  /// Returns nullptr if "aModelNode" is not an instance of ModelNodeVariable
  const ModelNodeVariable* ModelNodeVariable::cast(const ModelNode* aModelNode)
  {
    if (!isa(aModelNode))
    {
      return nullptr;
    }
    return static_cast<const ModelNodeVariable*>(aModelNode);
  }//cast

  ModelNodeVariable* ModelNodeVariable::cast(ModelNode* aModelNode)
  {
    if (!isa(aModelNode))
    {
      return nullptr;
    }
    return static_cast<ModelNodeVariable*>(aModelNode);
  }//cast

  CLIStmt ModelNodeVariable::toCLIStmt()
  {
    assert(pDomainDeclaration);

    CLIStmt strMsg;

    // Set type of message
    strMsg[CLI_MSG_FIELD_DELIM] = CLI_MSG_TYPE_VALUE_VAR_DECL;

    // Set variable id
    strMsg[CLI_MSG_TAG_VAR_ID] = pVarId;

    // Get domain type which declares the type of the variable
    strMsg[CLI_MSG_TAG_VAR_TYPE] = pDomainDeclaration->domainType();

    // Set type of domain format
    auto domTypeDec = pDomainDeclaration->domainDeclarationType();
    switch (domTypeDec)
    {
    case DomainDeclaration::DomainDeclarationType::DOM_DEC_EXP:
      strMsg[CLI_MSG_TAG_VAR_DOM_DEC_FORMAT] = CLI_MSG_VAL_VAR_DOM_DEC_FORMAT_EXP;
      break;
    case DomainDeclaration::DomainDeclarationType::DOM_DEC_RNG:
      strMsg[CLI_MSG_TAG_VAR_DOM_DEC_FORMAT] = CLI_MSG_VAL_VAR_DOM_DEC_FORMAT_RNG;
      break;
    default:
      assert(domTypeDec == DomainDeclaration::DomainDeclarationType::DOM_DEC_SET);
      strMsg[CLI_MSG_TAG_VAR_DOM_DEC_FORMAT] = CLI_MSG_VAL_VAR_DOM_DEC_FORMAT_LST;
      break;
    }

    // Domain as plain text
    strMsg[CLI_MSG_TAG_VAR_DOM_DEC_TEXT] = pDomainDeclaration->domainDeclaration();

    // Optional fields

    // Output variable
    if (parameterMap().find(CLI_MSG_VAL_VAR_OUTPUT) != parameterMap().end())
    {
      strMsg[CLI_MSG_TAG_VAR_OUTPUT] = parameterMap()[CLI_MSG_VAL_VAR_OUTPUT];
    }
    
    // Semantic of the variable
    if (parameterMap().find(CLI_MSG_VAL_VAR_SEMANTIC_TYPE) != parameterMap().end())
    {
      strMsg[CLI_MSG_TAG_VAR_SEMANTIC_TYPE] = parameterMap()[CLI_MSG_VAL_VAR_SEMANTIC_TYPE];
    }
    
    // Branching of variable
    if (parameterMap().find(CLI_MSG_VAL_VAR_BRANCHING) != parameterMap().end())
    {
      strMsg[CLI_MSG_TAG_VAR_BRANCHING] = parameterMap()[CLI_MSG_VAL_VAR_BRANCHING];
    }
    
    // Extremum for objective variables
    if (parameterMap().find(CLI_MSG_VAL_VAR_OBJ_EXTREMUM) != parameterMap().end())
    {
      strMsg[CLI_MSG_TAG_VAR_OBJ_EXTREMUM] = parameterMap()[CLI_MSG_VAL_VAR_OBJ_EXTREMUM];
    }
    
    // Set matrix
    if(isMatrixDeclaration())
    {
      const auto& row = pMartrixDeclaration->getRow();
      assert(!row.empty());
      
      std::string numRows;
      std::string numCols;
      if(row.size() == 1)
      {
        numRows = "1";
        numCols = row[0];
      }
      else
      {
        numRows = row[0];
        numCols = row[1];
      }
      
      strMsg[CLI_MSG_TAG_VAR_NUM_ROWS] = numRows;
      strMsg[CLI_MSG_TAG_VAR_NUM_COLS] = numCols;
    }
    else
    {
      strMsg[CLI_MSG_TAG_VAR_NUM_ROWS] = "0";
      strMsg[CLI_MSG_TAG_VAR_NUM_COLS] = "0";
    }
    
    // Set overflow if any
    if(pDomainDeclaration->hasOverflow())
    {
      strMsg[CLI_MSG_TAG_EVENT] = CLI_MSG_VAL_EVENT_SATURATION;
    }
    
    return strMsg;
  }//toCLIStmt

}// end namespace CLI

