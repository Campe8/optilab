// Base class
#include "DomainDeclaration.hpp"

#include <cassert>

namespace CLI {

  DomainDeclaration::DomainDeclaration(DomainDeclaration::DomainDeclarationType aDecType, std::string aDomDec)
  : pDomainType(aDecType)
  , pDomain(aDomDec)
  , pOverflow(false)
  , pBoolean(false)
  {
  }

  std::string DomainDeclaration::domainType()
  {
    if(isBoolean())
    {
      return CLI_MSG_VAL_VAR_TYPE_BOOL;
    }
    return CLI_MSG_VAL_VAR_TYPE_INT;
  }//domainType
  
  //===================== DomainDeclaration EXPRESSION =====================//

  DomainDeclarationExpression::DomainDeclarationExpression(std::string aDomDec)
    : DomainDeclaration(DomainDeclaration::DomainDeclarationType::DOM_DEC_EXP, aDomDec)
  {
  }

  bool DomainDeclarationExpression::isa(const DomainDeclaration* aDomainDecl)
  {
    assert(aDomainDecl);
    return aDomainDecl->domainDeclarationType() == DomainDeclaration::DomainDeclarationType::DOM_DEC_EXP;
  }//isa

  DomainDeclarationExpression* DomainDeclarationExpression::cast(DomainDeclaration* aDomainDecl)
  {
    if (!isa(aDomainDecl))
    {
      return nullptr;
    }

    return static_cast<DomainDeclarationExpression*>(aDomainDecl);
  }//cast

  const DomainDeclarationExpression* DomainDeclarationExpression::cast(const DomainDeclaration* aDomainDecl)
  {
    if (!isa(aDomainDecl))
    {
      return nullptr;
    }

    return static_cast<const DomainDeclarationExpression*>(aDomainDecl);
  }//cast

   //===================== DomainDeclaration RANGE =====================//

  DomainDeclarationRange::DomainDeclarationRange(std::string aDomDec)
    : DomainDeclaration(DomainDeclaration::DomainDeclarationType::DOM_DEC_RNG, aDomDec)
  {
  }

  bool DomainDeclarationRange::isa(const DomainDeclaration* aDomainDecl)
  {
    assert(aDomainDecl);
    return aDomainDecl->domainDeclarationType() == DomainDeclaration::DomainDeclarationType::DOM_DEC_RNG;
  }//isa

  DomainDeclarationRange* DomainDeclarationRange::cast(DomainDeclaration* aDomainDecl)
  {
    if (!isa(aDomainDecl))
    {
      return nullptr;
    }

    return static_cast<DomainDeclarationRange*>(aDomainDecl);
  }//cast

  const DomainDeclarationRange* DomainDeclarationRange::cast(const DomainDeclaration* aDomainDecl)
  {
    if (!isa(aDomainDecl))
    {
      return nullptr;
    }

    return static_cast<const DomainDeclarationRange*>(aDomainDecl);
  }//cast

  //===================== DomainDeclaration SET =====================//

  DomainDeclarationSet::DomainDeclarationSet(std::string aDomDec)
    : DomainDeclaration(DomainDeclaration::DomainDeclarationType::DOM_DEC_SET, aDomDec)
  {
  }

  bool DomainDeclarationSet::isa(const DomainDeclaration* aDomainDecl)
  {
    assert(aDomainDecl);
    return aDomainDecl->domainDeclarationType() == DomainDeclaration::DomainDeclarationType::DOM_DEC_SET;
  }//isa

  DomainDeclarationSet* DomainDeclarationSet::cast(DomainDeclaration* aDomainDecl)
  {
    if (!isa(aDomainDecl))
    {
      return nullptr;
    }

    return static_cast<DomainDeclarationSet*>(aDomainDecl);
  }//cast

  const DomainDeclarationSet* DomainDeclarationSet::cast(const DomainDeclaration* aDomainDecl)
  {
    if (!isa(aDomainDecl))
    {
      return nullptr;
    }

    return static_cast<const DomainDeclarationSet*>(aDomainDecl);
  }//cast

}// end namespace CLI


