// Base class
#include "MatrixDeclaration.hpp"

#include <cassert>

namespace CLI {

  MatrixRowDeclaration::MatrixRowDeclaration()
  : pBoolean(false)
  , pOverflow(false)
  {
  }
  
  void MatrixRowDeclaration::addElement(ModelExpr* aExpr)
  {
    if(!aExpr) { return; }
    pRow.push_back(aExpr->expr());
    
    if(aExpr->isBoolean())
    {
      setBoolean();
    }
    
    if(aExpr->hasOverflow())
    {
      setOverflow();
    }
  }//addElement
  
  MatrixDeclaration::MatrixDeclaration()
  : pBoolean(false)
  , pOverflow(false)
  {
  }
  
  MatrixDeclaration::MatrixRow& MatrixDeclaration::getRow(std::size_t aRowIdx)
  {
    assert(aRowIdx < numRows());
    return pMatrix[aRowIdx];
  }//getRow
  
  void MatrixDeclaration::addRow(MatrixRowDeclaration* aRow)
  {
    if(!aRow) { return; }
    assert(!numRows() || (aRow->getRow()).size() == numCols());
    
    pMatrix.push_back(aRow->getRow());
    if(aRow->isBoolean())
    {
      setBoolean();
    }
    
    if(aRow->hasOverflow())
    {
      setOverflow();
    }
  }//addRow
  
}// end namespace CLI

