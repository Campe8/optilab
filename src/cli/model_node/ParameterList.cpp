// Base class
#include "ParameterList.hpp"

#include <iterator>
#include <cassert>

namespace CLI {

  ParameterList::Parameter ParameterList::getParameter(std::size_t aIndexParam)
  {
    assert(aIndexParam < numParameters());

    auto it = pParameters.begin();
    std::advance(it, aIndexParam);

    return std::make_pair(it->first, it->second);
  }//getParameter

}// end namespace CLI



