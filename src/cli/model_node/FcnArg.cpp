// Base class
#include "FcnArg.hpp"

#include <iterator>
#include <cassert>

namespace CLI {

  FunctionArg::FunctionArg(const std::string& aArg)
  : pArgument(aArg)
  , pBoolean(false)
  {
  }
  
  void FunctionArgList::addArgArray(const FunctionArg& aArg)
  {
    auto alist = "[" + aArg.getArgument() + "]";
    addArg(alist);
  }//addArgArray
  
}// end namespace CLI



