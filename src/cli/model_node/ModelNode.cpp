// Base class
#include "ModelNode.hpp"

#include <cassert>

namespace CLI {

  ModelNode::ModelNode(CLIMsgType aNodeMessageType)
    : pNodeMessageType(aNodeMessageType)
  {
  }

  std::string ModelNode::stringify()
  {
    auto strMsg = toCLIStmt();
    
    // Add parameter list
    if (!pParameterMap.empty())
    {
      std::string paramList{ "" };
      for (auto& pair : pParameterMap)
      {
        paramList += pair.first;
        paramList += CLI_MSG_TAG_INT_TO_CHAR(CLI_MSG_ASCII_SPACE);
        paramList += pair.second;
        paramList += CLI_MSG_TAG_INT_TO_CHAR(CLI_MSG_ASCII_STX);
      }
      paramList.pop_back();
      
      strMsg[CLI_MSG_TAG_PARAM_LIST] = paramList;
    }
    
    return Utils::getStringFromCLIStmt(strMsg);
  }//stringify

  void ModelNode::setAnnotationList(std::unique_ptr<ParameterList> aAnnotationList)
  {
    assert(aAnnotationList);

    for (std::size_t idx = 0; idx < aAnnotationList->numParameters(); ++idx)
    {
      auto param = aAnnotationList->getParameter(idx);
      pParameterMap[param.first] = param.second;
    }
  }//setAnnotationList


}// end namespace CLI

