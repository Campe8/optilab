// Base class
#include "ModelNodeConstraint.hpp"

#include "CLIMacro.hpp"

#include <cassert>

namespace CLI {

  ModelNodeConstraint::ModelNodeConstraint()
  : ModelNode(CLIMsgType::CLI_MSG_CON_DECL)
  {
  }
  
  bool ModelNodeConstraint::isa(const ModelNode* aModelNode)
  {
    assert(aModelNode);
    return aModelNode->nodeType() == CLIMsgType::CLI_MSG_CON_DECL;
  }//isa
  
  /// Cast to ModelNodeVariable.
  /// Returns nullptr if "aModelNode" is not an instance of ModelNodeVariable
  const ModelNodeConstraint* ModelNodeConstraint::cast(const ModelNode* aModelNode)
  {
    if (!isa(aModelNode))
    {
      return nullptr;
    }
    return static_cast<const ModelNodeConstraint*>(aModelNode);
  }//cast
  
  ModelNodeConstraint* ModelNodeConstraint::cast(ModelNode* aModelNode)
  {
    if (!isa(aModelNode))
    {
      return nullptr;
    }
    return static_cast<ModelNodeConstraint*>(aModelNode);
  }//cast
  
  ModelNodeConstraintExpr::ModelNodeConstraintExpr(std::unique_ptr<ModelExpr> aModelExpr)
  : pExpr(std::move(aModelExpr))
  {
  }//ModelNodeConstraintExpr
  
  CLIStmt ModelNodeConstraintExpr::toCLIStmt()
  {
    assert(pExpr);
    
    CLIStmt strMsg;
    
    // Set type of message
    strMsg[CLI_MSG_FIELD_DELIM] = CLI_MSG_TYPE_VALUE_CON_DECL;
    
    // Set constraint declaration type
    strMsg[CLI_MSG_TAG_CON_DECL_TYPE] = CLI_MSG_VAL_CON_DECL_TYPE_EXPR;
    
    // Set expression type
    if (pExpr->isScalar())
    {
      strMsg[CLI_MSG_TAG_CON_EXPR_TYPE] = CLI_MSG_VAL_CON_EXPR_TYPE_C;
    }
    else if (pExpr->isId())
    {
      strMsg[CLI_MSG_TAG_CON_EXPR_TYPE] = CLI_MSG_VAL_CON_EXPR_TYPE_I;
    }
    else
    {
      strMsg[CLI_MSG_TAG_CON_EXPR_TYPE] = CLI_MSG_VAL_CON_EXPR_TYPE_E;
    }
    
    // Set expression string
    strMsg[CLI_MSG_TAG_CON_EXPR_PREORDER] = pExpr->expr();
    
    return strMsg;
  }//toCLIStmt

}// end namespace CLI

