// Base class
#include "ModelExpr.hpp"

#include "CLIUtils.hpp"

#include <sstream>
#include <cassert>

namespace CLI {
 
  ModelExpr::ModelExpr(CLI_INT aScalar)
  : pOverflow(false)
  , pBoolean(false)
  {
    std::stringstream ss;
    ss << aScalar;
    pExpr = ss.str();
    
    pExprType = ExprType::ET_SCALAR_INT;
  }
  
  ModelExpr::ModelExpr(CLI_STR aId)
  : pOverflow(false)
  , pBoolean(false)
  {
    // Create new string
    CLI_STR idStr = new char[strlen(aId) + 1];
    CLI_STR_CPY(idStr, aId);
    
    // Convert to std string
    std::string idStrStd(idStr);
    
    pExpr = idStrStd;
    pExprType = ExprType::ET_ID;
    
    // Free memory
    delete [] idStr;
  }
  
  ModelExpr::ModelExpr(ModelExpr::ExprOp aExprOp, const std::shared_ptr<ModelExpr>& aExpr)
  : pBoolean(false)
  , pOverflow(false)
  {
    assert(aExpr);
    
    if (aExprOp == ExprOp::EOP_UNARY_PLUS)
    {
      pExpr = "+";
    }
    else
    {
      assert(aExprOp == ExprOp::EOP_UNARY_MINUS);
      pExpr = "-";
    }
    pExpr += aExpr->expr();
    
    // Set scalar type according to the type of "aExpr"
    if (aExpr->isScalar())
    {
      pExprType = ExprType::ET_SCALAR_INT;
    }
    else
    {
      pExprType = ExprType::ET_CEXPR;
    }
    
    if(aExpr->hasOverflow())
    {
      setOverflow();
    }
    
    if(aExpr->isBoolean())
    {
      setBoolean();
    }
  }//ModelExpr
  
  ModelExpr::ModelExpr(ModelExpr::ExprOp aExprOp, const std::shared_ptr<ModelExpr>& aExpr1, const std::shared_ptr<ModelExpr>& aExpr2)
  : pBoolean(false)
  , pOverflow(false)
  {
    assert(aExpr1);
    assert(aExpr2);
    pExprType = ExprType::ET_CEXPR;
    
    if (aExpr1->isScalar() && aExpr2->isScalar() && aExprOp != EOP_SUBSCRIPT)
    {
      // Evaluate
      pExpr = evaluateScalarExpr(aExprOp, aExpr1, aExpr2);
      pExprType = ExprType::ET_SCALAR_INT;
    }
    else
    {
      pExpr= "";
      
      if(aExprOp == EOP_SUBSCRIPT)
      {
        assert(aExpr1->pExprType == ExprType::ET_ID);
        pExpr = aExpr1->expr() + "[" + aExpr2->expr() + "]";
      }
      else
      {
        // First expression
        pExpr += aExpr1->isScalarOrId() ? "" : "(";
        pExpr += aExpr1->expr();
        pExpr += aExpr1->isScalarOrId() ? "" : ")";
        
        // Operator
        pExpr += getOperatorSymb(aExprOp);
        
        // Second expression
        pExpr += aExpr2->isScalarOrId() ? "" : "(";
        pExpr += aExpr2->expr();
        pExpr += aExpr2->isScalarOrId() ? "" : ")";
        
        pExprType = ExprType::ET_CEXPR;
      }
    }
    
    if(aExpr1->hasOverflow() || aExpr2->hasOverflow())
    {
      setOverflow();
    }
    
    if(aExpr1->isBoolean() || aExpr2->isBoolean())
    {
      setBoolean();
    }
  }
  
  std::string ModelExpr::getOperatorSymb(ModelExpr::ExprOp aOp)
  {
    switch (aOp)
    {
      case CLI::ModelExpr::EOP_EQUALS:
        return "=";
      case CLI::ModelExpr::EOP_PLUS:
      case CLI::ModelExpr::EOP_UNARY_PLUS:
        return "+";
      case CLI::ModelExpr::EOP_MINUS:
      case CLI::ModelExpr::EOP_UNARY_MINUS:
        return "-";
      case CLI::ModelExpr::EOP_MULT:
        return "*";
      case CLI::ModelExpr::EOP_DIV:
        return "/";
      case CLI::ModelExpr::EOP_LT:
        return "<";
      case CLI::ModelExpr::EOP_GT:
        return ">";
      case CLI::ModelExpr::EOP_GE:
        return ">=";
      case CLI::ModelExpr::EOP_LE:
        return "<=";
      case CLI::ModelExpr::EOP_NE:
        return "!=";
      case CLI::ModelExpr::EOP_EQ:
        return "==";
      case CLI::ModelExpr::EOP_AND:
        return "&&";
      case CLI::ModelExpr::EOP_OR:
        return "||";
      case CLI::ModelExpr::EOP_REIF:
        return "<->";
      case CLI::ModelExpr::EOP_HREIF_L:
        return "<-";
      default:
        assert(aOp == CLI::ModelExpr::EOP_HREIF_R);
        return "->";
    }
  }//getOperatorSymb
  
  std::string ModelExpr::evaluateScalarExpr(ModelExpr::ExprOp aExprOp, const std::shared_ptr<ModelExpr>& aExpr1, const std::shared_ptr<ModelExpr>& aExpr2)
  {
    assert(aExpr1);
    assert(aExpr2);
    assert(aExpr1->isScalar());
    assert(aExpr2->isScalar());
    assert(aExpr1->pExprType == ExprType::ET_SCALAR_INT);
    assert(aExpr2->pExprType == ExprType::ET_SCALAR_INT);
    
    auto valExpr1 = static_cast<double>(std::stol(aExpr1->expr()));
    auto valExpr2 = static_cast<double>(std::stol(aExpr2->expr()));
    
    long evalExpr{ 0 };
    switch (aExprOp)
    {
      case CLI::ModelExpr::EOP_PLUS:
        evalExpr = Utils::evalAndSaturate<CLI_INT>(valExpr1 + valExpr2);
        break;
      case CLI::ModelExpr::EOP_MINUS:
        evalExpr = Utils::evalAndSaturate<CLI_INT>(valExpr1 - valExpr2);
        break;
      case CLI::ModelExpr::EOP_MULT:
        evalExpr = Utils::evalAndSaturate<CLI_INT>(valExpr1 * valExpr2);
        break;
      case CLI::ModelExpr::EOP_DIV:
        evalExpr = Utils::evalAndSaturate<CLI_INT>(valExpr1 / valExpr2);
        break;
      case CLI::ModelExpr::EOP_EQUALS:
      case CLI::ModelExpr::EOP_EQ:
      {
        if (valExpr1 == valExpr2)
        {
          evalExpr = 1;
        }
        else
        {
          evalExpr = 0;
        }
      }
        break;
      case CLI::ModelExpr::EOP_LT:
      {
        if (valExpr1 < valExpr2)
        {
          evalExpr = 1;
        }
        else
        {
          evalExpr = 0;
        }
      }
        break;
      case CLI::ModelExpr::EOP_GT:
      {
        if (valExpr1 > valExpr2)
        {
          evalExpr = 1;
        }
        else
        {
          evalExpr = 0;
        }
      }
        break;
      case CLI::ModelExpr::EOP_GE:
      {
        if (valExpr1 >= valExpr2)
        {
          evalExpr = 1;
        }
        else
        {
          evalExpr = 0;
        }
      }
        break;
      case CLI::ModelExpr::EOP_LE:
      {
        if (valExpr1 <= valExpr2)
        {
          evalExpr = 1;
        }
        else
        {
          evalExpr = 0;
        }
      }
        break;
      case CLI::ModelExpr::EOP_NE:
      {
        if (valExpr1 != valExpr2)
        {
          evalExpr = 1;
        }
        else
        {
          evalExpr = 0;
        }
      }
        break;
      case CLI::ModelExpr::EOP_AND:
      {
        if (valExpr1 && valExpr2)
        {
          evalExpr = 1;
        }
        else
        {
          evalExpr = 0;
        }
      }
        break;
      case CLI::ModelExpr::EOP_OR:
      {
        if (valExpr1 || valExpr2)
        {
          evalExpr = 1;
        }
        else
        {
          evalExpr = 0;
        }
      }
        break;
      case CLI::ModelExpr::EOP_REIF:
      {
        if ((valExpr1 && valExpr2) || (!valExpr1 && !valExpr2))
        {
          evalExpr = 1;
        }
        else
        {
          evalExpr = 0;
        }
      }
        break;
      case CLI::ModelExpr::EOP_HREIF_L:
      {
        if (valExpr2 && !valExpr1)
        {
          evalExpr = 0;
        }
        else
        {
          evalExpr = 1;
        }
      }
        break;
      case CLI::ModelExpr::EOP_HREIF_R:
      {
        if (valExpr1 && !valExpr2)
        {
          evalExpr = 0;
        }
        else
        {
          evalExpr = 1;
        }
      }
        break;
      case CLI::ModelExpr::EOP_UNARY_PLUS:
      default:
        assert(aExprOp == CLI::ModelExpr::EOP_UNARY_MINUS);
        assert(false);
    }
    
    std::stringstream ss;
    ss << evalExpr;
    return ss.str();
  }//evaluateScalarExpr
  

}// end namespace CLI



