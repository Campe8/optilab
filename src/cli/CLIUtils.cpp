// Self first
#include "CLIUtils.hpp"

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/filesystem.hpp>

#include <iostream>
#include <sstream>
#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>
#include <cassert>
#include <limits>

namespace CLI {
  namespace Utils {
    
    void addCLIStmtVal(CLIStmt& aCLIStmt, const std::pair<int, std::string>& aTagVal)
    {
      aCLIStmt[aTagVal.first] = aTagVal.second;
    }//addCLIStmtVal
    
    boost::optional<std::string> getCLIStmtVal(const CLIStmt& aCLIStmt, int aTag)
    {
      if(aCLIStmt.find(aTag) == aCLIStmt.end())
      {
        return boost::optional<std::string>();
      }
      
      return boost::optional<std::string>(aCLIStmt.at(aTag));
      
    }//getCLIStmtVal
    
    CLIStmt getCLIStmtFromString(const std::string& aCLIMsg)
    {
      CLIStmt strMsg;
      std::stringstream ss(aCLIMsg);
      
      bool firstDelim{ true };
      std::string tagValuePair;
      while (std::getline(ss, tagValuePair, CLI_MSG_TAG_INT_TO_CHAR(CLI_MSG_FIELD_DELIM)))
      {
        std::size_t tagDelimIt = tagValuePair.find_first_of(CLI_MSG_TAG_INT_TO_CHAR(CLI_MSG_TAG_VAL_DELIM));
        if (tagDelimIt == std::string::npos && firstDelim)
        {
          firstDelim = false;
          continue;
        }
        assert(tagDelimIt != std::string::npos);
        
        // Add value pair to structured message
        // @note replace all param delim char with spaces
        auto valStr = tagValuePair.substr(tagDelimIt + 1);
        std::replace(valStr.begin(), valStr.end(),
                     CLI_MSG_TAG_INT_TO_CHAR(CLI_MSG_ASCII_STX),
                     CLI_MSG_TAG_INT_TO_CHAR(CLI_MSG_ASCII_SPACE));
        strMsg[std::stoi(tagValuePair.substr(0, tagDelimIt))] = valStr;
      }
      return strMsg;
    }//getCLIStmtFromString
    
    std::string getStringFromCLIStmt(const CLIStmt& aCLIStmt)
    {
      std::string msg{ "" };
      if (aCLIStmt.empty())
      {
        return msg;
      }
      
      // Transform structured message into correspondent string.
      // For example:
      // "|1=variable|10=d|11=0|12=x|14=r|20=0..10"
      msg += CLI_MSG_TAG_INT_TO_CHAR(CLI_MSG_FIELD_DELIM);
      for (auto& pair : aCLIStmt)
      {
        std::stringstream ss;
        ss << pair.first;
        msg += ss.str();
        msg += CLI_MSG_TAG_INT_TO_CHAR(CLI_MSG_TAG_VAL_DELIM);
        msg += pair.second;
        msg += CLI_MSG_TAG_INT_TO_CHAR(CLI_MSG_FIELD_DELIM);
      }
      msg.pop_back();
      
      return msg;
    }//getStringFromCLIStmt
    
    std::string cliStmtStringFromPrettyPrint(const std::string& aPrettyPrintString)
    {
      std::string strMsg{ "" };
      for (auto& c : aPrettyPrintString)
      {
        if (c == '|')
        {
          strMsg += CLI_MSG_TAG_INT_TO_CHAR(CLI_MSG_FIELD_DELIM);
        }
        else if (c == '=')
        {
          strMsg += CLI_MSG_TAG_INT_TO_CHAR(CLI_MSG_TAG_VAL_DELIM);
        }
        else if (c == ' ')
        {
          strMsg += CLI_MSG_TAG_INT_TO_CHAR(CLI_MSG_ASCII_STX);
        }
        else
        {
          strMsg += c;
        }
      }
      return strMsg;
    }//cliStmtStringFromPrettyPrint
    
    std::string prettyPrintStringFromCLIStmt(const std::string& aCLIStmtString)
    {
      std::string prettyPrint{ "" };
      for (auto& c : aCLIStmtString)
      {
        if (c == CLI_MSG_TAG_INT_TO_CHAR(CLI_MSG_FIELD_DELIM))
        {
          prettyPrint += '|';
        }
        else if (c == CLI_MSG_TAG_INT_TO_CHAR(CLI_MSG_TAG_VAL_DELIM))
        {
          prettyPrint += "=";
        }
        else if (c == CLI_MSG_TAG_INT_TO_CHAR(CLI_MSG_ASCII_STX))
        {
          prettyPrint += " ";
        }
        else
        {
          prettyPrint += c;
        }
      }
      return prettyPrint;
    }//prettyPrintStringFromStructuredMsg
    
    CLIMsgType getMsgType(const CLIStmt& aStructuredMsg)
    {
      assert(aStructuredMsg.find(CLI_MSG_TAG_TYPE) != aStructuredMsg.end());
      auto msgType = aStructuredMsg.at(CLI_MSG_TAG_TYPE);
      
      if (msgType == CLI_MSG_TYPE_VALUE_VAR_DECL)      { return CLIMsgType::CLI_MSG_VAR_DECL;    }
      if (msgType == CLI_MSG_TYPE_VALUE_CON_DECL)      { return CLIMsgType::CLI_MSG_CON_DECL;    }
      if (msgType == CLI_MSG_TYPE_VALUE_MAT_DECL)      { return CLIMsgType::CLI_MSG_MATRIX_DECL; }
      if (msgType == CLI_MSG_TYPE_VALUE_BRC_DECL)      { return CLIMsgType::CLI_MSG_BRC_DECL; }
      if (msgType == CLI_MSG_TYPE_VALUE_FCN_CALL_DECL) { return CLIMsgType::CLI_MSG_FCN_CALL;    }
      if (msgType == CLI_MSG_TYPE_VALUE_SRC_DECL)      { return CLIMsgType::CLI_MSG_SRC_DECL;    }
      if (msgType == CLI_MSG_TYPE_VALUE_ERR_DECL)      { return CLIMsgType::CLI_MSG_ERR;         }
      
      // if (msgType == CLI_MSG_TYPE_VALUE_OBJ_ASSIGN) { return CLIMsgType::CLI_MSG_OBJ_ASSIGN; }
      
      return CLIMsgType::CLI_MSG_UNDEF;;
    }//getMsgType
    
    CLIMsgEvent getMsgEvent(const CLIStmt& aStructuredMsg)
    {
      if (aStructuredMsg.find(CLI_MSG_TAG_EVENT) == aStructuredMsg.end())
      {
        return CLIMsgEvent::CLI_MSG_EVENT_NONE;
      }
      auto msgEvent = aStructuredMsg.at(CLI_MSG_TAG_EVENT);
      assert(msgEvent == CLI_MSG_VAL_EVENT_SATURATION);
      
      return CLIMsgEvent::CLI_MSG_EVENT_SATURATION;
    }//getMsgEvent
    
    void ltrim(std::string& aString)
    {
      aString.erase(aString.begin(),
                    std::find_if(aString.begin(), aString.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
    }//ltrim
    
    void rtrim(std::string& aString)
    {
      aString.erase(std::find_if(aString.rbegin(),
                                 aString.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), aString.end());
    }//rtrim
    
    void trim(std::string& aString)
    {
      ltrim(aString);
      rtrim(aString);
    }//trim
    
    std::string ltrimmed(std::string aString)
    {
      ltrim(aString);
      return aString;
    }//ltrimmed
    
    std::string rtrimmed(std::string aString)
    {
      rtrim(aString);
      return aString;
    }//rtrimmed
    
    std::string trimmed(std::string aString)
    {
      trim(aString);
      return aString;
    }//trimmed
    
    std::size_t numSubStrInString(const std::string& aStr, const std::string& aSubStr)
    {
      int occurrences = 0;
      std::string::size_type pos = 0;
      while ((pos = aStr.find( aSubStr, pos )) != std::string::npos) {
        ++occurrences;
        pos += aSubStr.length();
      }
      return static_cast<std::size_t>(occurrences);
    }//numSubStrInString
    
    bool isInteger(const std::string& aString)
    {
      auto trimStr = trimmed(aString);
      if (trimStr.empty() || ((!isdigit(trimStr[0])) && (trimStr[0] != '-') && (trimStr[0] != '+')))
      {
        return false;
      }
      
      auto limitIntMax = std::numeric_limits<long>::max();
      auto limitIntMin = std::numeric_limits<long>::lowest() + 1;
      
      char * p;
      strtol(trimStr.c_str(), &p, 10);
      if (*p == 0)
      {
        std::string::size_type sz;   // alias of size_t
        auto liDec = std::stod(trimStr, &sz);
        if (liDec < static_cast<double>(limitIntMin) || liDec > static_cast<double>(limitIntMax))
        {
          return false;
        }
      }
      
      return (*p == 0);
    }//isInteger
    
   bool isQuotedString(const std::string& aString)
   {
     if(aString.size() < 2 || aString[0] != '"' ||  aString[aString.size() - 1] != '"')
     {
       return false;
     }
     return true;
   }//isQuotedString
    
    std::string removeQuotesFromQuotedString(const std::string& aString)
    {
      if(!isQuotedString(aString))
      {
        return aString;
      }
      auto subStr = aString.substr(1);
      subStr.pop_back();
      return subStr;
    }//removeQuotesFromQuotedString
    
    INT_64_T stoi(const std::string& aString)
    {
      assert(isInteger(aString));
      return std::stol(aString);
    }//stoi
    
    std::vector<std::string> tokenizeStringOnSymbol(const std::string& aString, char aChar)
    {
      std::vector<std::string> tokens;
      if (!aString.empty())
      {
        std::istringstream ss(aString);
        std::string token;
        while (std::getline(ss, token, aChar))
        {
          tokens.push_back(token);
        }
      }
      return tokens;
    }//tokenizeStringOnSymbol
    
    static std::string cleanArgument(const std::string& aArg)
    {
      std::size_t startIdx{ 0 };
      for (auto& c : aArg)
      {
        if ((c == ',' || c == ' '))
        {
          startIdx++;
          continue;
        }
        break;
      }
      
      return CLI::Utils::trimmed(aArg.substr(startIdx));
    }//cleanArgument
    
    std::vector<std::string> splitArguments(const std::string& aArgs)
    {
      // Vector of fcn arguments to return
      std::vector<std::string> args;
      if (aArgs.empty()) { return args; }
      
      // Stack for counting parentheses
      int stack{ 0 };
      
      // Set 2 indexes for substr:
      // start and length of string to be copied
      std::size_t start{ 0 };
      std::size_t len{ 0 };
      for (auto& c : aArgs)
      {
        if (c == ',' && stack == 0)
        {
          auto cleanArg = cleanArgument(aArgs.substr(start, len));
          if (!cleanArg.empty())
          {
            args.push_back(cleanArg);
          }
          start += len;
          len = 0;
        }
        else if (c == '[')
        {
          stack++;
        }
        else if (c == ']')
        {
          stack--;
          if (stack < 0) { stack = 0; }
        }
        len++;
      }
      
      // Push last argument in the list
      auto cleanArg = cleanArgument(aArgs.substr(start, len));
      if (!cleanArg.empty())
      {
        args.push_back(cleanArg);
      }
      
      return args;
    }//splitArguments
    
    template<> std::string toString<BOOL_T>(const BOOL_T& aVal)
    {
      return aVal == 0 ? "0" : "1";
    }
    
    std::string getCurrentDate(char aSep)
    {
      namespace pt = boost::posix_time;
      
      // Output string stream
      std::stringstream ss;
      
      // Current date/time based on current system
      pt::ptime now = pt::second_clock::local_time();
      
      // Get current date
      ss << now.date().year() << aSep << static_cast<int>(now.date().month()) << aSep << now.date().day();
      
      return ss.str();
    }//getCurrentDate

    std::string getRootPath()
    {
      using Path = boost::filesystem::path;
      
      // Get global path
      Path glbPath(boost::filesystem::current_path());
      // Get path to root
      Path rootPath;
      for (auto& p : glbPath)
      {
        rootPath = rootPath / p;
        if (p == "optilab" || p == "OptiLab") break;
      }//for
      
      return rootPath.string();
    }//getRootPath
    
  }// end namespace Utils
}// end namespace Model
