%{
  
  /**
   * Context free grammar for OPTILAB Language.
   * Initial proposal 03/17/2015.
   * Created by Federico Campeotto.
   * Copyright 2015-2017 Federico Campeotto. All rights reserved.
   */
  
  #include <iostream>
  #include <string>
  #include <cstdlib>
  #include <memory>
  #include <sstream>
  #include <cassert>
  
  #define LOOKAHEAD	yychar
  
  #include "CLIMacro.hpp"
  #include "ModelNodeInc.hpp"
  #include "CLIUtils.hpp"
  
// Debug printing
// #define PARSER_DEBUG
  
  //-- Lexer prototype required by bison, aka getNextToken()
  extern void yyerror(const char *aErr);
  extern int yylex();
  
  extern "C" int yyparse(void);
  
  void collectInputLine(const std::string& str);
  
%}

//-- SYMBOL SEMANTIC VALUES -----------------------------
%start input

%token LINE
%token QUOTE
%token RANGE_DEC
%token LEXERROR

%union {
  CLI_INT				CLIIntVal;
  CLI_STR				CLIStringVal;
  
  CLI_MDL_EXPR            CLIModelExpr;
  CLI_MDL_FCN_ARG_LIST    CLIModelFcnArgList;
  CLI_MDL_PARAM_LIST      CLIModelParamList;
  CLI_MDL_DOM_DECL        CLIModelDomainDecl;
  CLI_MDL_DOM_DECL_SET	  CLIModelDomainDeclSet;
  CLI_MDL_MATRIX_DECL     CLIModelMatrixDecl;
  CLI_MDL_MATRIX_ROW_DECL CLIModelMatrixRowDecl;
  
  CLI_MDL_NODE            CLIModelNode;
  CLI_MDL_NODE_VAR        CLIModelNodeVar;
  CLI_MDL_NODE_CON        CLIModelNodeCon;
  CLI_MDL_NODE_FCN_CALL   CLIModelNodeFcnCall;
  CLI_MDL_NODE_MATRIX     CLIModelNodeMatrix;
  CLI_MDL_NODE_ERR        CLIModelNodeError;
};

%token <CLIIntVal>    INTEGER
%token <CLIIntVal>    INTEGER_OVF
%token <CLIIntVal>    INTEGER_BOOL
%token <CLIStringVal> SYMBOL_CHAR
%token <CLIStringVal> IDENTIFIER
%token <CLIStringVal> STRING
%token <CLIStringVal> COMMAND_ID_PAR
%token <CLIStringVal> OBJ_ACCESS

%type <CLIStringVal>          identifier assign_ls assign_rs
%type <CLIModelExpr>          expr
%type <CLIModelParamList>     assign_list assign
%type <CLIModelDomainDecl>    domain_decl
%type <CLIModelDomainDeclSet> domain_set expr_list
%type <CLIModelFcnArgList>    fcn_args

%type <CLIModelNode>        stmt_list stmt

%type <CLIModelNode>        var_decl
%type <CLIModelNodeVar>     v_base_decl

%type <CLIModelNode>        con_decl
%type <CLIModelNodeCon>     c_base_decl

%type <CLIModelNode>        fcn_call
%type <CLIModelNodeFcnCall> f_call_base_decl

%type <CLIModelNode>           matrix_decl
%type <CLIModelNodeMatrix>     m_base_decl
%type <CLIModelMatrixDecl>     m_rows
%type <CLIModelMatrixRowDecl>  m_row m_row_comma

%type <CLIModelNodeError>	parse_error

%left ',' ':'
%left AND OR
%left LT LE GT GE EQ NE REIF '>' '<' '='
%left '-' '+'
%left '*' '/'
%left '.'

%left RANGE_DEC
%nonassoc UNARYPLUS UNARYMINUS

//-- GRAMMAR RULES ---------------------------------------

%%

input: m_body                 {
                                // Input reads "m_body" rules and return
                                return 0;
                              }
                              ;

m_body: m_decl                {
                                // Model body: Empty rule
                              }
| parse_error                 {
                                // Model body: error coming from downstream rules
                                if($1)
                                {
                                  collectInputLine($1->stringify());
                                  
#ifdef PARSER_DEBUG
                                  std::cout << CLI::Utils::prettyPrintStringFromCLIStmt($1->stringify()) << std::endl;
#endif
                                  
                                  // Free memory
                                  delete $1;
                                }
                              }
                              ;

m_decl: stmt_list             {
                                if($1)
                                {
                                  collectInputLine($1->stringify());
                                
#ifdef PARSER_DEBUG
                                  std::cout << CLI::Utils::prettyPrintStringFromCLIStmt($1->stringify()) << std::endl;
#endif

                                  // Free memory
                                  delete $1;
                                }
                              }
                              ;

stmt_list: empty_lines        {
                                $$ = nullptr;
                              }
| empty_lines stmt ';'        {
                                $$ = $2;
                              }
| empty_lines stmt LINE ';'   {
                                $$ = $2;
                              }
                              ;

empty_lines :                 {
                                // Empty rule
                              }
| empty_lines empty_line      {
                                // Empty rule
                              }
                              ;

empty_line : ';'              {
                                // Empty rule
                              }
| LINE                        {
                                // Empty rule
                              }
                              ;

stmt: var_decl                {
                                // Variable declaration.
                                // For example:
                                // x: 10..100, Output: true
                                $$ = $1;
                              }
| con_decl                    {
                                // Constraint declaration.
                                // For example:
                                // x + 2 = z;
                                $$ = $1;
                              }
| fcn_call                    {
                                // Function call.
                                // For example:
                                // ctx("a");
                                $$ = $1;
                              }
| matrix_decl                 {
                                // Matrix declaration.
                                // For example:
                                // m: [1, 2, 3];
                                $$ = $1;
                              }
                              ;

var_decl: v_base_decl         {
                                // Declaration id: domain
                                if($1)
                                {
                                  $$ = $1;
                                }
                                else
                                {
                                  // Variable is nullptr due to an error
                                  std::string errMsg = "Error:Variable_Declaration";
                                  $$ = new CLI::ModelNodeError(errMsg);
                                }
                              
                              }
| v_base_decl ',' assign_list {
                                // Declaration id: domain + assignment list
                                if($1)
                                {
                                  // Set parameter list
                                  $1->setAnnotationList(std::unique_ptr<CLI::ParameterList>($3));
                                  $$ = $1;
                                }
                                else
                                {
                                  // Variable is nullptr due to an error
                                  std::string errMsg = "Error:Variable_Declaration";
                                  $$ = new CLI::ModelNodeError(errMsg);
                                }
                              }
                              ;

v_base_decl: identifier ':'		        {
                                        // Input string error
                                        $$ = nullptr;
                                      }
| identifier ':' domain_decl	        {
                                          if($3)
                                          {
                                            $$ = new CLI::ModelNodeVariable($1,
                                            std::unique_ptr<CLI::DomainDeclaration>($3));
                                          }
                                          else
                                          {
                                            // A nullptr for domain can be caused
                                            // by an error in the domain declaration
                                            $$ = nullptr;
                                          }
                                        }
| identifier ':' domain_decl ':' m_row {
                                          if($3 && $5)
                                          {
                                            $$ = new CLI::ModelNodeVariable($1,
                                            std::unique_ptr<CLI::DomainDeclaration>($3),
                                            std::unique_ptr<CLI::MatrixRowDeclaration>($5));
                                          }
                                          else
                                          {
                                            // A nullptr for domain can be caused
                                            // by an error in the domain declaration
                                            $$ = nullptr;
                                            
                                            // Free memory
                                            if($3) { delete $3; }
                                            if($5) { delete $5; }
                                          }
                                       }
                                       ;

assign_list: assign           {
                                $$ = $1;
                              }
|  assign_list ',' assign     {
                                assert($1);
                                assert($3);
                                assert($3->numParameters() == 1);
                                
                                CLI::ParameterList* paramList = new CLI::ParameterList();
                                for(std::size_t idx = 0; idx < $1->numParameters(); ++idx)
                                {
                                  paramList->addParameter($1->getParameter(idx));
                                }
                                paramList->addParameter($3->getParameter(0));
                                
                                $$ = paramList;
                                
                                // Free memory
                                delete $1;
                                delete $3;
                              }
                              ;

assign:	assign_ls ':' assign_rs	{
                                  assert($1);
                                  assert($3);
                                  
                                  // Create new string
                                  CLI_STR lsStr = new char[strlen($1) + 1];
                                  CLI_STR_CPY(lsStr, $1);
                                  
                                  CLI_STR rsStr = new char[strlen($3) + 1];
                                  CLI_STR_CPY(rsStr, $3);
                                  
                                  // Convert to std strings
                                  std::string lsStrStd(lsStr);
                                  std::string rsStrStd(rsStr);
                                  
                                  // Create new parameter
                                  CLI::ParameterList::Parameter param;
                                  
                                  param.first  = lsStrStd;
                                  param.second = rsStrStd;
                                  CLI::ParameterList* paramList = new CLI::ParameterList();
                                  paramList->addParameter(param);
                                  
                                  $$ = paramList;
                                  
                                  // Free memory
                                  delete[] lsStr;
                                  delete[] $1;
                                  delete[] $3;
                                }
                                ;

assign_ls: identifier         {
                                assert($1);
                                $$ = $1;
                              }
| STRING                      {
                                assert($1);
                                $$ = $1;
                              }
                              ;

assign_rs: expr               {
                                assert($1);
                                std::string exprStrStd = $1->expr();
                                
                                // Create new string
                                CLI_STR exprStr = new char[exprStrStd.size() + 1];
                                CLI_STR_CPY(exprStr, exprStrStd.c_str());
                                
                                $$ = exprStr;
                                
                                // Free memory
                                delete $1;
                              }
| STRING                      {
                                assert($1);
                                $$ = $1;
                              }
                              ;

domain_decl: expr             {
                                assert($1);
                                
                                CLI::DomainDeclarationExpression* dexpr =
                                new CLI::DomainDeclarationExpression($1->expr());
                                
                                if($1->hasOverflow())
                                {
                                  dexpr->setOverflow();
                                }
                                
                                if($1->isBoolean())
                                {
                                  dexpr->setBoolean();
                                }
                                
                                $$ = dexpr;
                                
                                // Free memory
                                delete $1;
                              }
| expr RANGE_DEC expr         {
                                assert($1);
                                assert($3);
                                
                                // Convert into c++ std strings
                                std::string lbBnd = $1->expr();
                                std::string upBnd = $3->expr();
                                
                                std::string domRange = lbBnd;
                                domRange += CLI_MSG_VAL_VAR_DOM_DEC_ELEM_SEP;
                                domRange += upBnd;
                                
                                CLI::DomainDeclarationRange* drange =
                                new CLI::DomainDeclarationRange(domRange);
                                
                                if($1->hasOverflow() || $3->hasOverflow())
                                {
                                  drange->setOverflow();
                                }
                                
                                if($1->isBoolean() || $3->isBoolean())
                                {
                                  drange->setBoolean();
                                }
                                
                                $$ = drange;
                                
                                // Free memory
                                delete $1;
                                delete $3;
                              }
| domain_set                  {
                                $$ = $1;
                              }
| domain_decl_err             {
                                $$ = nullptr;
                                
                                // Flush Lex buffer on yacc error
                                while(yylex());
                              }
                              ;

domain_decl_err: domain_set '}'
| domain_set identifier '}'
| domain_set INTEGER '}'
| domain_set INTEGER_OVF '}'
| domain_set SYMBOL_CHAR '}'
| domain_set SYMBOL_CHAR identifier '}'
| domain_set SYMBOL_CHAR INTEGER '}'
| domain_set SYMBOL_CHAR INTEGER_OVF '}'
;

domain_set: '{' expr '}'      {
                                assert($2);
                                
                                CLI::DomainDeclarationSet* dset =
                                new CLI::DomainDeclarationSet($2->expr());
                                
                                if($2->hasOverflow())
                                {
                                  dset->setOverflow();
                                }
                                
                                if($2->isBoolean())
                                {
                                  dset->setBoolean();
                                }
                                
                                $$ = dset;
                                delete $2;
                              }
| '{' expr_list ',' expr '}'  {
                                assert($2);
                                assert($4);
                              
                                std::string list1 = $2->domainDeclaration();
                                std::string list2 = $4->expr();
                              
                                std::string domSet = list1;
                                domSet += CLI_MSG_VAL_VAR_DOM_DEC_ELEM_SEP;
                                domSet += list2;
                              
                                CLI::DomainDeclarationSet* dset = new CLI::DomainDeclarationSet(domSet);
                                if($2->hasOverflow() || $4->hasOverflow())
                                {
                                  dset->setOverflow();
                                }
                              
                               if($2->isBoolean() || $4->isBoolean())
                               {
                                 dset->setBoolean();
                               }
                              
                                $$ = dset;
                              
                                // Free memory
                                delete $2;
                                delete $4;
                              }
                              ;

expr_list : expr              {
                                assert($1);
                                std::string domSet = $1->expr();
                                CLI::DomainDeclarationSet* dset = new CLI::DomainDeclarationSet(domSet);
                                
                                if($1->hasOverflow())
                                {
                                  dset->setOverflow();
                                }
                                
                                if($1->isBoolean())
                                {
                                  dset->setBoolean();
                                }
                                
                                $$ = dset;
                                
                                // Free memory
                                delete $1;
                              }
| expr_list ',' expr          {
                                assert($3);
                                std::string list1 = $1->domainDeclaration();
                                std::string list2 = $3->expr();
                                
                                std::string domSet = list1;
                                domSet += CLI_MSG_VAL_VAR_DOM_DEC_ELEM_SEP;
                                domSet += list2;
                                
                                CLI::DomainDeclarationSet* dset =
                                new CLI::DomainDeclarationSet(domSet);
                                
                                if($1->hasOverflow() || $3->hasOverflow())
                                {
                                  dset->setOverflow();
                                }
                                
                                if($1->isBoolean() || $3->isBoolean())
                                {
                                  dset->setBoolean();
                                }
                                
                                $$ = dset;
                                
                                // Free memory
                                delete $1;
                                delete $3;
                              }
                              ;

con_decl: c_base_decl         {
                                // Declaration expr
                                if($1)
                                {
                                  $$ = $1;
                                }
                                else
                                {
                                  // Constraint is nullptr due to an error
                                  std::string errMsg = "Error:Constraint_Declaration";
                                  $$ = new CLI::ModelNodeError(errMsg);
                                }
                              }
                              ;

c_base_decl: expr             {
                                assert($1);
                                $$ = new CLI::ModelNodeConstraintExpr(std::unique_ptr<CLI::ModelExpr>($1));
                              }
                              ;

expr: INTEGER                 {
                                $$ = new CLI::ModelExpr($1);
                              }
| INTEGER_OVF                 {
                                CLI::ModelExpr* expr = new CLI::ModelExpr($1);
                                expr->setOverflow();
                                $$ = expr;
                              }
| INTEGER_BOOL                {
                                CLI::ModelExpr* expr = new CLI::ModelExpr($1);
                                expr->setBoolean();
                                $$ = expr;
                              }
| identifier                  {
                                assert($1);
                                $$ = new CLI::ModelExpr($1);
                                
                                // Free memory
                                delete[] $1;
                              }
| '(' expr ')'                {
                                $$ = $2;
                              }
| identifier '[' expr ']'     {
                                $$ = new CLI::ModelExpr(CLI::ModelExpr::ExprOp::EOP_SUBSCRIPT,
                                std::shared_ptr<CLI::ModelExpr>(new CLI::ModelExpr($1)),
                                std::shared_ptr<CLI::ModelExpr>($3));
                              }
| '+' expr %prec UNARYPLUS		{
                                $$ = new CLI::ModelExpr(CLI::ModelExpr::ExprOp::EOP_UNARY_PLUS, std::shared_ptr<CLI::ModelExpr>($2));
                              }
| '-' expr %prec UNARYMINUS		{
                                $$ = new CLI::ModelExpr(CLI::ModelExpr::ExprOp::EOP_UNARY_MINUS, std::shared_ptr<CLI::ModelExpr>($2));
                              }
| expr '+' expr               {
                                assert($1);
                                assert($3);
                                $$ = new CLI::ModelExpr(CLI::ModelExpr::ExprOp::EOP_PLUS,
                                std::shared_ptr<CLI::ModelExpr>($1),
                                std::shared_ptr<CLI::ModelExpr>($3));
                              }
                              ;

fcn_call: f_call_base_decl    {
                                // Declaration of function call
                                if($1)
                                {
                                  $$ = $1;
                                }
                                else
                                {
                                  // Function call is nullptr due to an error
                                  std::string errMsg = "Error:Function_Call_Declaration";
                                  $$ = new CLI::ModelNodeError(errMsg);
                                }
                              }
                              ;

f_call_base_decl: COMMAND_ID_PAR ')'  {
                                        assert($1);
                                        $$ = new CLI::ModelNodeFunctionCall($1, "");
                                        
                                        delete $1;
                                      }
| COMMAND_ID_PAR fcn_args ')'         {
                                        assert($1);
                                        assert($2);

                                        std::string args = "";
                                        for(std::size_t idx = 0; idx < $2->numArgs(); ++idx)
                                        {
                                          args += ($2->getArg(idx)).getArgument();
                                          args += ",";
                                        }
                                        args.pop_back();
                                        
                                        $$ = new CLI::ModelNodeFunctionCall($1, args);

                                        // Free memory
                                        delete $1;
                                        delete $2;
                                      }
                                      ;

fcn_args: expr                    {
                                    assert($1);
                                    
                                    CLI::FunctionArg a = CLI::FunctionArg($1->expr());
                                    if($1->isBoolean())
                                    {
                                      a.setBoolean();
                                    }
                                    
                                    CLI::FunctionArgList* alist = new CLI::FunctionArgList();
                                    alist->addArg(a);
  
                                    $$ = alist;
  
                                    // Free memory
                                    delete $1;
                                  }
| STRING                          {
                                    assert($1);
                                    
                                    // Convert to std strings
                                    std::string exprStrStd( $1);
                                    
                                    CLI::FunctionArgList* alist = new CLI::FunctionArgList();
                                    alist->addArg(CLI::FunctionArg(exprStrStd));
                                    
                                    $$ = alist;

                                    // Free memory
                                    delete $1;
                                  }
| '['']'                          {
                                    CLI::FunctionArgList* alist = new CLI::FunctionArgList();
                                    $$ = alist;
                                  }
| '[' fcn_args ']'                {
                                    assert($2);
                                    
                                    std::string argl = "";
                                    for(std::size_t idx = 0; idx < $2->numArgs(); ++idx)
                                    {
                                      argl += ($2->getArg(idx)).getArgument();
                                      argl += ", ";
                                    }
                                    if($2->numArgs() > 0)
                                    {
                                      argl.pop_back();
                                      argl.pop_back();
                                    }
  
                                    CLI::FunctionArgList* alist = new CLI::FunctionArgList();
                                    alist->addArgArray(CLI::FunctionArg(argl));
  
                                    $$ = alist;
                                    
                                    // Free memory
                                    delete $2;
                                  }
| fcn_args ',' '[' expr_list ']'  {
                                    assert($1);
                                    assert($4);
                                    assert(CLI::DomainDeclarationSet::isa($4));
                                    
                                    CLI::DomainDeclarationSet* ddecl =
                                    CLI::DomainDeclarationSet::cast($4);
                                    
                                    std::string args = ddecl->domainDeclaration();
                                    
                                    std::string argl = "";
                                    for(auto& arg :CLI::Utils::tokenizeStringOnSymbol(args))
                                    {
                                      argl += arg;
                                      argl += ", ";
                                    }
                                    if(!argl.empty())
                                    {
                                      argl.pop_back();
                                      argl.pop_back();
                                    }
                                    
                                    $1->addArgArray(CLI::FunctionArg(argl));
                                    
                                    $$ = $1;
                                    
                                    // Free memory
                                    delete $4;
                                  }
| fcn_args ',' expr               {
                                    assert($1);
                                    assert($3);
                                    
                                    CLI::FunctionArg a = CLI::FunctionArg($3->expr());
                                    if($3->isBoolean())
                                    {
                                      a.setBoolean();
                                    }
                                    
                                    $1->addArg(a);
                                    
                                    $$ = $1;
                                    
                                    // Free memory
                                    delete $3;
                                  }
                                  ;

matrix_decl: m_base_decl      {
                                // Matrix declaration
                                if($1)
                                {
                                  $$ = $1;
                                }
                                else
                                {
                                  // Matrix declaration is nullptr due to an error
                                  std::string errMsg = "Error:Matrix_Declaration";
                                  $$ = new CLI::ModelNodeError(errMsg);
                                }
                              }
                              ;

m_base_decl: identifier ':' '[' m_rows ']' {
                                             if($1 && $4)
                                             {
                                               $$ = new CLI::ModelNodeMatrix($1, std::unique_ptr<CLI::MatrixDeclaration>($4));
                                             }
                                             else
                                             {
                                               $$ = nullptr;
                                               if($4)
                                               {
                                                 // Free memory
                                                 delete $4;
                                               }
                                             }
                                           }
| identifier ':' m_row                     {
                                             if($1 && $3)
                                             {
                                               CLI::MatrixDeclaration* mat = new CLI::MatrixDeclaration();
                                               mat->addRow($3);
                                               
                                               $$ = new CLI::ModelNodeMatrix($1, std::unique_ptr<CLI::MatrixDeclaration>(mat));

                                               // Free memory
                                               delete $3;
                                             }
                                             else
                                             {
                                               $$ = nullptr;
                                               if($3)
                                               {
                                                 // Free memory
                                                 delete $3;
                                               }
                                             }
                                          }
                                          ;

m_rows:            {
                     $$ = new CLI::MatrixDeclaration();
                   }
| '[' ']'          {
                     $$ = new CLI::MatrixDeclaration();
                   }
| m_row            {
                      if(!$1)
                      {
                        delete $1;
                        $$ = nullptr;
                      }
                      else
                      {
                        $$ = new CLI::MatrixDeclaration();
                        $$->addRow($1);
                        
                        // Free memory
                        delete $1;
                      }
                   }
| m_rows ',' m_row {
                     if(!$1 || !$3)
                     {
                       delete $1;
                       delete $3;
                       $$ = nullptr;
                     }
                     else
                     {
                       $$ = $1;
                       $$->addRow($3);
                       
                       
                       delete $3;
                     }
                   }
                   ;

m_row: '[' expr ']'        {
                              $$ = new CLI::MatrixRowDeclaration();
                              $$->addElement($2);
                              
                              // Free memory
                              delete $2;
                           }
| '[' m_row_comma expr ']' {
                              if(!$2 || !$3)
                              {
                                delete $2;
                                delete $3;
                                $$ = nullptr;
                              }
                              else
                              {
                                $2->addElement($3);
                                $$ = $2;
                                
                                // Free memory
                                delete $3;
                              }
                           }
                           ;

m_row_comma: expr ','      {
                              $$ = new CLI::MatrixRowDeclaration();
                              $$->addElement($1);
                              
                              // Free memory
                              delete $1;
                           }
| m_row_comma expr ','     {
                              if(!$1 || !$2)
                              {
                                delete $1;
                                delete $2;
                                $$ = nullptr;
                              }
                              else
                              {
                                $1->addElement($2);
                                $$ = $1;
                                
                                // Free memory
                                delete $2;
                              }
                           }
                           ;

identifier: IDENTIFIER        {
                                $$ = $1;
                              }
                              ;

parse_error: LEXERROR         {
                                std::string errMsg = "Error-Lexer";
                                $$ = new CLI::ModelNodeError(errMsg);
                              }
| error                       {
                                std::string errMsg = "Error-Parser";
                                $$ = new CLI::ModelNodeError(errMsg);
                                
                                // Flush Lex buffer on yacc error
                                while(yylex());
                              }
                              ;

%%

//-- FUNCTION DEFINITIONS ---------------------------------
