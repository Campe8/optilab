%{
  #include "CLIMacro.hpp"
  #include "ModelNodeInc.hpp"
  
  #include <iostream>
  #include <string>
  #include <memory>
  #include <sstream>
  #include <limits>
  #include <errno.h>
  
  // Must be last
  #include "y.tab.h"
  
  extern int fromStringInput(char *buf, int maxlen);
  
  //%option noyywrap
%}

HSPACE          [ \t]
HSPACES         {HSPACE}+
NEWLINE         \n|\r|\f
NEWLINES        {NEWLINE}+

CONT            \\
CONTINUATION    {CONT}[^\n\r\f]*{NEWLINE}?

IDENTIFIER      [_a-zA-Z][-_a-zA-Z0-9]*
CMD_ID_PAR      {IDENTIFIER}{HSPACES}*\(

OBJ_ACCESS      {IDENTIFIER}\.{IDENTIFIER}

DIGIT           [0-9]
INTEGER         {DIGIT}+
INTEGER_OVF     {DIGIT}+
INTEGER_BOOL    {DIGIT}+
NUMBER          {DIGIT}
NUMBER_ERR      {NUMBER}{HSPACES}*{IDENTIFIER}

SYMBOL          [&%$#@!_~<>=]+

%%

,               {
                  return ',';
                }

;               {
                  return ';';
                }

{INTEGER}       {
                  yylval.CLIIntVal = strtol(yytext, nullptr, 10);
                  if(errno == ERANGE)
                  {
                    // Return integer on overflow
                    return INTEGER_OVF;
                  }
                  return INTEGER;
                }

"INF"           {
                  yylval.CLIIntVal = CLI_INF;
                  return INTEGER;
                }

"true"          {
                  yylval.CLIIntVal = CLI_TRUE;
                  return INTEGER_BOOL;
                }

"false"         {
                  yylval.CLIIntVal = CLI_FALSE;
                  return INTEGER_BOOL;
                }

{IDENTIFIER}    {
                  // Create new string
                  std::size_t strLen = strlen(yytext);
                  CLI_STR strCpy = new char[strLen + 1];
                  
                  // Copy string
                  CLI_STR_CPY(strCpy, yytext);
                  yylval.CLIStringVal = strCpy;
                  
                  return IDENTIFIER;
                }

{NEWLINES}      {
                  return LINE;
                }

{CONTINUATION}	{
                  // Empty Lexer rule
                }

{HSPACES}       {
                  // Empty Lexer rule
                }

[/][/].*        {
                  // Empty Lexer rule.
                  // Skip comment on single line
                }

"\""            {
                  return QUOTE;
                }

\<              {
                  return LT;
                }

\<=             {
                  return LE;
                }

>               {
                  return GT;
                }

>=              {
                  return GE;
                }

"&&"            {
                  return AND;
                }

"||"            {
                  return OR;
                }

\|              {
                  return '|';
                }

\<->            {
                  return REIF;
                }

==              {
                  return EQ;
                }

=               {
                  return EQ;
                }

!=              {
                  return NE;
                }

\+              {
                  return '+';
                }

-               {
                  return '-';
                }

\*              {
                  return '*';
                }

\/              {
                  return '/';
                }

\.              {
                  return '.';
                }

\(              {
                  return '(';
                }

\)              {
                  return ')';
                }

\{              {
                  return '{';
                }

\}              {
                  return '}';
                }

:               {
                  return ':';
                }

".."            {
                  return RANGE_DEC;
                }

\[              {
                  return '[';
                }

\]              {
                  return ']';
                }

{OBJ_ACCESS}    {
                  // Create new string
                  std::size_t strLen = strlen(yytext);
                  CLI_STR strCpy = new char[strLen + 1];
                  
                  // Copy string
                  CLI_STR_CPY(strCpy, yytext);
                  yylval.CLIStringVal = strCpy;
                  
                  return OBJ_ACCESS;
                }

{CMD_ID_PAR}    {
                  // Create new string
                  std::size_t strLen = strlen(yytext);
                  CLI_STR strCpy = new char[strLen + 1];
                  
                  // Parse the string and read the command name
                  int i = 0;
                  while(i < strLen && yytext[i] != '\0')
                  {
                    if(yytext[i] == ' ' || yytext[i] == '(' || yytext[i] == '\0')
                    {
                      break;
                    }
                    strCpy[i] = yytext[i];
                    i++;
                  }
                  strCpy[i] = '\0';
                  
                  // Copy string
                  yylval.CLIStringVal = strCpy;
                  
                  return COMMAND_ID_PAR;
                }

\"[^'\n\r\f]*\" {
                  // Create new string
                  std::size_t strLen = strlen(yytext);
                  CLI_STR strCpy = new char[strLen + 1];

                  // Copy string
                  CLI_STR_CPY(strCpy, yytext);
                  yylval.CLIStringVal = strCpy;

                  return STRING;
                }

{SYMBOL}        {
                  return SYMBOL_CHAR;
                }

{NUMBER_ERR}    {
                  // Treat non syntactically correct
                  // numbers as an error
                  YY_FLUSH_BUFFER;
                  BEGIN(INITIAL);
                  return LEXERROR;
                }

.               {
                  // Treat everything else as an error
                  YY_FLUSH_BUFFER;
                  BEGIN(INITIAL);
                  return LEXERROR;
                }

%%

int yywrap(void)      /* called at end of input */
{
  return 1;           /* terminate now */
}//yywrap

void yyerror(const char *aErr)
{
  (void)aErr;
}//yyerror
