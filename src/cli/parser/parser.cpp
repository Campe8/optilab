//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 05/20/2017
//
// Parser function using Bison for input line strings.
// To enable compilation, define macro
// #define CLI_PARSER_BUILD
//

#ifdef CLI_PARSER_BUILD

#include "ModelNodeInc.hpp"
#include "CLIMacro.hpp"

#include "parser_ext.h"

#include "y.tab.h"

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>

extern int yy_scan_string(const char *);

// Output string containing the syntax tree
char *globalSyntaticTree = nullptr;

void collectInputLine(const std::string& str)
{
  if(globalSyntaticTree)
  {
    delete[] globalSyntaticTree;
    globalSyntaticTree = nullptr;
  }
  
  if(str.empty())
  {
    return;
  }
  
  // Copy input line into syntax tree
  globalSyntaticTree = new char[str.size() + 1];
  CLI_STR_CPY(globalSyntaticTree, str.c_str());
}//collectInputLine

int parseString(char *inStr, char *outStr)
{
  // Scan input string
  yy_scan_string(inStr);
  
  // Call parser on input string to generate
  // the syntax tree
  yyparse();
  
  if(!globalSyntaticTree)
  {
    delete [] globalSyntaticTree;
    globalSyntaticTree = NULL;
    
    outStr[0] = '\0';
    return 1;
  }
  
  int numBytesToRead = (int)strlen(globalSyntaticTree);
  int idxChar = 0;
  for (; idxChar < numBytesToRead; idxChar++)
  {
    outStr[idxChar] = globalSyntaticTree[idxChar];
  }
  outStr[numBytesToRead] = '\0';
  
  delete[] globalSyntaticTree;
  globalSyntaticTree = NULL;
  
  return numBytesToRead;
}//parseString

#endif
