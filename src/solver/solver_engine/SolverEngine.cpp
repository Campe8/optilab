// Base class
#include "SolverEngine.hpp"

// Search semantic
#include "ModelSearch.hpp"
#include "DFSDescriptor.hpp"
#include "GeneticDescriptor.hpp"

// Variables
#include "ModelVariable.hpp"

// Constraints
#include "ModelConstraint.hpp"
#include "Constraint.hpp"

#include "DepthFirstSearch.hpp"
#include "GeneticSearch.hpp"
#include "SearchStrategy.hpp"
#include "SearchSemantics.hpp"
#include "GeneticSearchEnvironment.hpp"
#include "SolutionManager.hpp"
#include "Variable.hpp"

// Macros for search
#include "SearchMacro.hpp"

#include <cassert>
#include <stdexcept>
#include <utility>
#include <vector>

namespace {
  
  struct EnvDescription {
    EnvDescription(const std::string& aId="", Model::SearchDescriptor* aSrcDesc=nullptr,
                   Search::SearchEnvironment* aSrcEnv=nullptr)
    : SrcId(aId)
    , SrcDescription(aSrcDesc)
    , SrcEnvironment(aSrcEnv)
    {
    }
    
    std::string SrcId;
    Model::SearchDescriptor* SrcDescription;
    Search::SearchEnvironment* SrcEnvironment;
  };
  
}//end unnamed namespace

namespace Solver {
  
  static void updateDomain(const Core::VariableSPtr& aVar, const std::vector<Search::SolutionManager::BoundsPair>& aDomains)
  {
    const auto& domList = aVar->domainList();
    assert(domList->size() == aDomains.size());
    
    // For each domain in the list, update it with the correspondent pairs
    std::size_t idx{0};
    for(auto& dom : *domList)
    {
      if(!dom->lowerBound()->isEqual(aDomains[idx].first) ||
         !dom->upperBound()->isEqual(aDomains[idx].second))
      {
        // Shrink the domain to the updated bounds.
        // @note this update does not consider internal domain holes
        dom->shrink(aDomains[idx].first, aDomains[idx].second);
      }
      idx++;
    }
  }//updateDomain
  
  // Updates the given search environment w.r.t. content of the given solution
  static void updateSearchEnvironment(Search::SearchEnvironment* aSEnv, const Search::SolutionManager::Solution& aSol)
  {
    assert(aSEnv);
    const auto& envVars = aSEnv->getEnvironmentVariables();
    
    // For each variable that is part of the current environment, check if it is
    // part of the given solution and, if so, change its domain w.r.t. to the
    // given solution
    for(auto& var : envVars)
    {
      assert(var && var->variableSemantic());
      auto vID = var->variableSemantic()->getVariableNameIdentifier();
      auto it = aSol.find(vID);
      if(it != aSol.end())
      {
        // The variable is part of the solution:
        // change its domain
        updateDomain(var, it->second);
      }
    }
  }//updateSearchEnvironment
  
  // Returns the environment variables as registered in "aModel" w.r.t. the variables in varMap
  static std::vector<Search::SearchEnvironment::VarSem> getEnvironmentSet(Model::Model* aModel, const Model::ModelSearch* aSrc)
  {
    assert(aSrc);
    
    // Check the semantic list and if a semantic is an environment semantic,
    // se the variable in the set
    std::vector<Search::SearchEnvironment::VarSem> varSemList;
    const auto& semList = aSrc->getSearchDescriptor()->getSemanticList();
    
    varSemList.reserve(semList.size());
    for(auto& spair : semList)
    {
      if(Search::SearchEnvironment::isEnvironmentSemantic(spair.second.get()))
      {
        auto obj = Model::getModelObjectBySID(aModel, spair.first);
        assert(Model::ModelVariable::isa(obj));
        
        varSemList.push_back({ Model::ModelVariable::cast(obj)->variable(), spair.second });
      }
    }//for
    
    return varSemList;
  }//getEnvironmentSet
  
  // Returns an instance of a search semantic based on the objects (e.g., constraints)
  // of the given input model
  static std::unique_ptr<Search::SearchSemantics> generateSearchSemantics(Model::Model* aModel)
  {
    // Create a new instance of SearchSemantics
    auto ss = new Search::SearchSemantics();
    
    // Register the constraints in the model on the constraint store
    // of the search semantics
    auto cstore = ss->constraintStore();
    
    // Subscribe each constraint in the model to the constraint store
    auto objType = Model::ModelObjectClass::MODEL_OBJ_CONSTRAINT;
    for (std::size_t cidx{0}; cidx < aModel->numModelObjects(objType); ++cidx)
    {
      auto cobj = Model::ModelConstraint::cast(aModel->getModelObject(objType, cidx));
      assert(cobj && cobj->constraint());
      
      // Subscribe constraint to the constraint store of the search semantic
      cobj->constraint()->subscribeToConstraintStore(cstore);
    }
    
    // Returns the search semantic
    return std::unique_ptr<Search::SearchSemantics>(ss);
  }//generateSearchSemantics
  
  /// Returns the search environment generated according to the given search descriptor
  static Search::SearchEnvironment* getSearchEnvironment(Model::SearchDescriptor* aSrcDescriptor,
                                                         Model::Model* aModel,
                                                         const Model::ModelSearch* aSrcObject,
                                                         const std::string& aEnvId)
  {
    assert(aSrcDescriptor && aModel && aSrcObject);
    
    // Get the environment set
    auto envSet = getEnvironmentSet(aModel, aSrcObject);
    
    // Return the environment according to the type of strategy
    Search::SearchEnvironment* srcEnv{nullptr};
    if(aSrcDescriptor->getSearchStrategy() == Search::SearchEngineStrategyType::SE_DEPTH_FIRST_SEARCH)
    {
      auto dfsDesc = static_cast<Model::DFSDescriptor*>(aSrcDescriptor);
      srcEnv =  new Search::SearchEnvironment(envSet, dfsDesc->getVariableChoice(),
                                              dfsDesc->getValueChoice(), aEnvId);
      
      
    }
    else
    {
      assert(aSrcDescriptor->getSearchStrategy() == Search::SearchEngineStrategyType::SE_GENETIC);
      
      auto genDesc = static_cast<Model::GeneticDescriptor*>(aSrcDescriptor);
      auto genEnv = new Search::GeneticSearchEnvironment(envSet,
                                                         genDesc->getNeighborhoodSize(),
                                                         genDesc->getNumNeighborhoods(),
                                                         genDesc->isAggressiveCBLS(),
                                                         aEnvId);
      
      genEnv->setRandomSeed(genDesc->getRandomSeed());
      genEnv->setWarmStart(genDesc->getWarmStart());
      genEnv->setPopulationSize(genDesc->getPopulationSize());
      genEnv->setNumGenerations(genDesc->getNumGenerations());
      genEnv->setMutationChance(genDesc->getMutationChance());
      genEnv->setMutationSize(genDesc->getMutationSize());
      srcEnv = genEnv;
    }
    assert(srcEnv);
    
    // Set common information for all search strategies
    srcEnv->setTimeout(aSrcDescriptor->getTimeout());
    
    return srcEnv;
  }//getSearchEnvironment
  
  // Returns the list of environment descriptors containing all the search statements
  // that generated search objects in the model.
  // Ideally, there is one pair for each "branch"/"cbls" construct
  static std::vector<EnvDescription> generateEnvironmentList(Model::Model* aModel)
  {
    assert(aModel);
    
    // Generate the list of search environments according to each search object
    std::vector<EnvDescription> envList;
    
    // Iterate on the search semantics
    auto srcClass = Model::ModelObjectClass::MODEL_OBJ_SEARCH_SEMANTIC;
    auto numObjs = aModel->numModelObjects(srcClass);
    envList.resize(numObjs);
    
    for(std::size_t srcIdx{0}; srcIdx < numObjs; ++srcIdx)
    {
      // Get the model search at given index
      auto src = Model::ModelSearch::cast(aModel->getModelObject(srcClass, srcIdx));
      
      envList[srcIdx].SrcId = src->getID();
      envList[srcIdx].SrcDescription = (src->getSearchDescriptor()).get();
      envList[srcIdx].SrcEnvironment = getSearchEnvironment(envList[srcIdx].SrcDescription, aModel,
                                                            src, envList[srcIdx].SrcId);
    }
    
    return envList;
  }//generateEnvironmentList
  
  // Factory method generating search strategies
  static Search::BaseSearchScheme* getSearchStrategy(Search::SearchEngineStrategyType aStrategyType,
                                                     Model::Model* aModel,
                                                     Search::BaseSearchScheme::EnvironmentList& aEnvList)
  {
    if(aStrategyType == Search::SearchEngineStrategyType::SE_DEPTH_FIRST_SEARCH)
    {
      return new Search::DepthFirstSearch(generateSearchSemantics(aModel), aEnvList);
    }
    
    assert(aStrategyType == Search::SearchEngineStrategyType::SE_GENETIC);
    return new Search::GeneticSearch(generateSearchSemantics(aModel), aEnvList);
  }//getSearchStrategy
  
  // Generates the list of search schemes.
  // There is a search scheme for each search strategy that will be used to explore the search space.
  // Search schemes are indexed by the correspondent Search::SearchEngineStrategyType enum value.
  // If the model doesn't have an object for a given strategy, the scheme is nullptr
  static std::vector<std::shared_ptr<Search::BaseSearchScheme>>
  generateSearchSchemeList(Model::Model* aModel)
  {
    // List of search schemes in the model
    std::vector<std::shared_ptr<Search::BaseSearchScheme>> srcSchemeList;
    
    // List of environments for each scheme
    std::vector<std::vector<std::pair<std::string, Search::SearchEnvironment*>>> envSchemeList;
    
    // Resize the list w.r.t. the number of different search strategies
    auto numStrategies = static_cast<int>(Search::SearchEngineStrategyType::SE_UNDEF);
    srcSchemeList.resize(numStrategies, nullptr);
    envSchemeList.resize(numStrategies);
    
    // Generate the list of all the search environments and initialize the schemes
    for (const auto& envDesc : generateEnvironmentList(aModel))
    {
      assert(envDesc.SrcDescription);
      auto strategyIdx = static_cast<int>(envDesc.SrcDescription->getSearchStrategy());
      envSchemeList[strategyIdx].push_back({envDesc.SrcId, envDesc.SrcEnvironment});
    }
    
    // For each strategy generate an EnvironmentList
    for (int strategyIdx{0}; strategyIdx < numStrategies; ++strategyIdx)
    {
      // Continue if there is no environment for the current scheme
      const auto& envList = envSchemeList[strategyIdx];
      if(envList.empty()) continue;
      
      // Create an EnvironmentList
      Search::BaseSearchScheme::EnvironmentList envPtrList(envList.size());
      for (std::size_t idx{0}; idx < envList.size(); ++idx)
      {
        envPtrList[idx].reset(envList[idx].second);
      }
      
      auto strategy = static_cast<Search::SearchEngineStrategyType>(strategyIdx);
      auto scheme = getSearchStrategy(strategy, aModel, envPtrList);
      assert(scheme);
      
      srcSchemeList[strategyIdx] = std::shared_ptr<Search::BaseSearchScheme>(scheme);
    }//for
    
    return srcSchemeList;
  }//generateSearchSchemeList
  
  /// Uploads meta information from "aModel" to the base search "aBaseSearch"
  static void uploadMetaInfo(Model::Model* aModel, Search::BaseSearchScheme* aBaseSearch)
  {
    assert(aModel);
    assert(aBaseSearch);
    
    // Solution limit
    auto slimit = aModel->getMetaInfo()->solutionLimit();
    if(slimit <= 0) slimit = SM_SOLUTION_LIMIT_NONE;
    
    aBaseSearch->solutionManager()->setSolutionLimit(slimit);
  }//uploadMetaInfo
  
  static std::unique_ptr<Search::SearchEngine> generateEngine(Model::Model* aModel,
                                                              const std::shared_ptr<Search::BaseSearchScheme>& aBaseSearch)
  {
    // Create search strategy wrapper around the base search
    auto ss = std::unique_ptr<Search::SearchStrategy>(new Search::SearchStrategy(aBaseSearch));
    
    // Create search engine on the search strategy
    auto searchEngine = std::unique_ptr<Search::SearchEngine>(new Search::SearchEngine(std::move(ss)));
    
    // Upload meta information on the search engine
    uploadMetaInfo(aModel, aBaseSearch.get());
    
    return searchEngine;
  }//generateEngine
  
  SolverEngine::SolverEngine()
  : pModelKeySeed(0)
  {
  }
  
  std::shared_ptr<SolverSolution> SolverEngine::getSolution(ModelKey aModelKey) const
  {
    assert(pModelRegister.find(aModelKey) != pModelRegister.end());
    assert(pSolutionRegister.find(aModelKey) != pSolutionRegister.end());
    
    return pSolutionRegister.at(aModelKey);
  }//getSolution
  
  SolverEngine::ModelKey SolverEngine::registerModel(Model::ModelSPtr aModel)
  {
    assert(aModel);
    
    // Register the model a nullptr solution
    pModelRegister[pModelKeySeed] = aModel;
    pSolutionRegister[pModelKeySeed] = nullptr;
    
    return pModelKeySeed++;
  }//registerModel
  
  /// Creates an engine on the strategy in "aListScheme" at position "aSchemeIdx"
  SolverEngine::EngineResult SolverEngine::runEngine(const SearchSchemeList& aListScheme,
                                                     int aSchemeIdx, Model::Model* aModel,
                                                     std::size_t aModelKey)
  {
    assert(aSchemeIdx < aListScheme.size());
    
    // Run the engine on all the root nodes and collect results
    Search::SearchStatus searchStatus = Search::SearchStatus::SEARCH_UNDEF;
    
    auto src = aListScheme[aSchemeIdx];
    if (!src) return std::make_pair(nullptr, searchStatus);
    
    // The given index is on an instantiated strategy
    
    // Create a SolutionManager
    auto idVarMap = aModel->getIDVarMap();
    auto solutionManager = std::make_shared<Search::SolutionManager>(idVarMap);
    
    // Set the solution manager into the search scheme
    src->setSolutionManager(solutionManager);
    
    // Create an engine to run the given strategy
    std::unique_ptr<Search::SearchEngine> engine = generateEngine(aModel, src);
    
    auto numRootNodes = src->getNumRootNodes();
    auto solutionLimit = solutionManager->getSolutionLimit();
    for (std::size_t rootIdx{0}; rootIdx < numRootNodes; ++rootIdx)
    {
      // If there is more than one search tree to explore,
      // all search trees except the last one set the solution limit to 1
      // since all solutions are equivalent
      if (numRootNodes > 1)
      {
        if (rootIdx < numRootNodes - 1)
        {
          solutionManager->setSolutionLimit(1);
        }
        else
        {
          // Last search tree sets the number of solution to
          // the limit given by the client
          solutionManager->setSolutionLimit(solutionLimit);
        }
      }
      
      searchStatus = engine->label(src->getRootNode(rootIdx));
      
      // Break if the search status if FAIL since there is no point
      // in continuing with the search on another root node
      if(searchStatus == Search::SearchStatus::SEARCH_FAIL)
      {
        break;
      }
      
      // Get metrics for the current search environment
      auto baseSearch = static_cast<Search::BaseSearch*>(src.get());
      
      pMetricsRegister[aModelKey].
      push_back({baseSearch->getSearchEnvironment()->getEnvironmentId(), src->getMetrics()});
      
      // The labeling step gave a solution for the current root node.
      // If there is more than one root node to explore, the current solution should set the
      // domains of the variables that will be part of the next search tree
      if(numRootNodes > 1)
      {
        assert(solutionManager->getNumberOfRecordedSolutions() == 1);
        updateSearchEnvironment(baseSearch->getSearchEnvironment(), solutionManager->getSolution(1));
      }
    }//for
    
    return std::make_pair(solutionManager, searchStatus);
  }//runEngine
  
  void SolverEngine::runModel(ModelKey aKey)
  {
    // Given model key must be present in the register
    assert(pModelRegister.find(aKey) != pModelRegister.end());
    
    Model::Model* model = pModelRegister[aKey].get();
    assert(model);
    
    // Generate a search scheme for each search strategy in the model
    auto srcSchemeList  = generateSearchSchemeList(model);
    auto totStrategies  = static_cast<int>(Search::SearchEngineStrategyType::SE_UNDEF);
    auto completeSrcIdx = static_cast<int>(Search::SearchEngineStrategyType::SE_CBLS_STRATEGIES);
    
    // Generate a solution "collector" for all the solutions
    // found by the different search strategies/schemes
    auto solverSoluton = new SolverSolution(model->getIDVarMap());
    
    // First run the engines on complete search strategies
    for (int schemeIdx{0}; schemeIdx < completeSrcIdx; ++schemeIdx)
    {
      auto solutionPair = runEngine(srcSchemeList, schemeIdx, model, aKey);
      if(solutionPair.second == Search::SearchStatus::SEARCH_UNDEF) continue;
      
      // Read the solution from the solution manager and set it
      // into the solver solution
      solverSoluton->readSolution(solutionPair.second, solutionPair.first);
    }
    
    // Then run the engines on cbls search strategies
    for (int schemeIdx{completeSrcIdx+1}; schemeIdx < totStrategies; ++schemeIdx)
    {
      auto solutionPair = runEngine(srcSchemeList, schemeIdx, model, aKey);
      if(solutionPair.second == Search::SearchStatus::SEARCH_UNDEF) continue;
      
      // Read the solution from the solution manager and set it
      // into the solver solution
      solverSoluton->readSolution(solutionPair.second, solutionPair.first);
    }
    
    // Set solution for model identified by
    pSolutionRegister[aKey].reset(solverSoluton);
  }//runModel
  
  std::set<Core::VariableSPtr> SolverEngine::getSolutionSet(Model::Model* aModel)
  {
    assert(aModel);
    std::set<Core::VariableSPtr> solutionSet;
    
    for (std::size_t varIdx = 0;
         varIdx < aModel->numModelObjects(Model::ModelObjectClass::MODEL_OBJ_VARIABLE); ++varIdx)
    {
      const Model::ModelObject* mdlObj =
      aModel->getModelObject(Model::ModelObjectClass::MODEL_OBJ_VARIABLE, varIdx);
      assert(mdlObj);
      
      const Model::ModelVariable* mdlObjVar = Model::ModelVariable::cast(mdlObj);
      assert(mdlObjVar);
      assert(mdlObjVar->variable());
      assert(mdlObjVar->variable()->variableSemantic());
      
      // Insert variable in the set if it is a variable to branch on
      if (mdlObjVar->variable()->variableSemantic()->getSemanticClassType() !=
          Core::VariableSemanticClassType::VAR_SEMANTIC_CLASS_SUPPORT)
      {
        solutionSet.insert(mdlObjVar->variable());
      }
    }
    
    return solutionSet;
  }//getSolutionSet
  
  const SolverEngine::EnvMetrics& SolverEngine::getMetrics(ModelKey aModelKey,
                                                           std::size_t aMetricIdx) const
  {
    if (aMetricIdx >= getNumMetricsForModel(aModelKey))
    {
      throw std::out_of_range("metrics not found");
    }
    return pMetricsRegister.at(aModelKey).at(aMetricIdx);
  }//getMetrics
  
}// end namespace Solver
