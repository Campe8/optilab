// Base class
#include "AsyncEngineProcessor.hpp"

#include <functional>  // std::bind
#include <stdexcept>   // for std::runtime_error

namespace {
  /// Waiting value for an empty task queue
constexpr int kWaitOnEmptyTaskQueue{-1};
}// namespace

namespace Solver {
  
  AsyncEngineProcessor::AsyncEngineProcessor(int aThreadPoolSize)
  : pThreadPool(new DataStructure::ctpl::thread_pool(aThreadPoolSize))
  {
    setupModelFcn = std::bind(&AsyncEngineProcessor::setupModel, this, std::placeholders::_1);
    runEngineFcn = std::bind(&AsyncEngineProcessor::runEngine, this);
    
    pMetricsList = std::make_shared<SearchMetricsList>();
  }
  
  AsyncEngineProcessor::~AsyncEngineProcessor()
  {
    // Finish up the current tasks and close the thread pool
    pThreadPool->stop();
  }
  
  void AsyncEngineProcessor::processorWait(int aTimeoutSec)
  {
    if (aTimeoutSec == kWaitOnEmptyTaskQueue)
    {
      pThreadPool->wait(kWaitOnEmptyTaskQueue);
    }
    else
    {
      pThreadPool->wait(aTimeoutSec * 1000);
    }
  }// pipelineWait
  
  void AsyncEngineProcessor::registerModel(const Model::ModelSPtr& aModel)
  {
    if (!aModel)
    {
      throw std::runtime_error("Registering an empty model");
    }
    
    // Create the solution "collector" for all the solutions
    // found by the different search strategies/schemes
    pSolverSolutionCollector = std::make_shared<SolverSolutionCollector>(aModel->getIDVarMap());
    
    // Bind the functor to the input argument and add it to the queue of task
    auto handleRegistration = std::bind(AsyncEngineProcessor::setupModelFcn, aModel);
    pThreadPool->push(handleRegistration);
  }// registerModel
  
  void AsyncEngineProcessor::runModel()
  {
    // Bind the functor and add it to the queue of tasks
    auto handleRegistration = std::bind(AsyncEngineProcessor::runEngineFcn);
    pThreadPool->push(handleRegistration);
  }// runModel
  
}// end namespace Solver
