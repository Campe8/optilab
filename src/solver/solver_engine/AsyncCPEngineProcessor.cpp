// Base class
#include "AsyncCPEngineProcessor.hpp"

#include <cassert>
#include <stdexcept>   // for std::runtime_error
#include <stack>
#include <thread>

#include "DepthFirstSearch.hpp"
#include "DFSDescriptor.hpp"
#include "GeneticDescriptor.hpp"
#include "GeneticSearch.hpp"
#include "GeneticSearchEnvironment.hpp"
#include "ModelSearch.hpp"
#include "SearchDescriptor.hpp"
#include "SearchEngine.hpp"
#include "SearchEnvironment.hpp"
#include "SearchStrategy.hpp"
#include "SolutionManager.hpp"

namespace {
  
  /// Returns the list of pairs <variable, semantic> retrieved from the given search object
  std::vector<Search::SearchEnvironment::VarSem> getEnvironmentList(Model::Model* aModel,
                                                                    const Model::ModelSearch* aSrc)
  {
    // Get the list of pairs:
    // <variable_id, variable_semantic>
    const auto& idVarSemanticList = aSrc->getSearchDescriptor()->getSemanticList();
    
    // Reserve a list of pairs:
    // <variable, variable_semantic>
    // of the same size
    std::vector<Search::SearchEnvironment::VarSem> varSemanticPairList;
    varSemanticPairList.reserve(idVarSemanticList.size());
    for (auto& idVarSemanticPair : idVarSemanticList)
    {
      // For each pair, check if the semantic is an "environment" semantic (e.g., it is a
      // branching semantic) and, if so, get the correspondent variable from the model
      // and add it to the list
      if (Search::SearchEnvironment::isEnvironmentSemantic(idVarSemanticPair.second.get()))
      {
        auto obj = Model::getModelObjectBySID(aModel, idVarSemanticPair.first);
        assert(Model::ModelVariable::isa(obj));
        
        varSemanticPairList.push_back({ Model::ModelVariable::cast(obj)->variable(),
          idVarSemanticPair.second });
      }
    }//for
    
    return varSemanticPairList;
  }//getEnvironmentSet
  
  /// Returns the search environment generated according to the given search descriptor
  Search::SearchEnvironment* getSearchEnvironment(Model::SearchDescriptor* aSrcDescriptor,
                                                  Model::Model* aModel,
                                                  const Model::ModelSearch* aSearchObject,
                                                  const std::string& aEnvironmentId)
  {
    // Get the list of pairs:
    // <variable, variable_semantic>
    auto varSemanticPairList = getEnvironmentList(aModel, aSearchObject);
    
    // Create a search environment w.r.t. the type of search strategy hold by the search descriptor
    Search::SearchEnvironment* srcEnv{nullptr};
    if(aSrcDescriptor->getSearchStrategy() ==
       Search::SearchEngineStrategyType::SE_DEPTH_FIRST_SEARCH)
    {
      auto dfs = static_cast<Model::DFSDescriptor*>(aSrcDescriptor);
      srcEnv = new Search::SearchEnvironment(varSemanticPairList, dfs->getVariableChoice(),
                                             dfs->getValueChoice(), aEnvironmentId);
    }
    else
    {
      assert(aSrcDescriptor->getSearchStrategy() == Search::SearchEngineStrategyType::SE_GENETIC);
      
      auto genetic = static_cast<Model::GeneticDescriptor*>(aSrcDescriptor);
      auto genEnv = new Search::GeneticSearchEnvironment(varSemanticPairList,
                                                         genetic->getNeighborhoodSize(),
                                                         genetic->getNumNeighborhoods(),
                                                         genetic->isAggressiveCBLS(),
                                                         aEnvironmentId);
      
      // Setup the parameters of the environment according to the descriptor
      genEnv->setRandomSeed(genetic->getRandomSeed());
      genEnv->setWarmStart(genetic->getWarmStart());
      genEnv->setPopulationSize(genetic->getPopulationSize());
      genEnv->setNumGenerations(genetic->getNumGenerations());
      genEnv->setMutationChance(genetic->getMutationChance());
      genEnv->setMutationSize(genetic->getMutationSize());
      srcEnv = genEnv;
    }
    assert(srcEnv);
    
    // Set common information for all search strategies
    srcEnv->setTimeout(aSrcDescriptor->getTimeout());
    
    // Return the environment
    return srcEnv;
  }//getSearchEnvironment
  
  /// Creates and returns an instance of a search semantic based on the objects (e.g., constraints)
  /// of the given input model
  std::unique_ptr<Search::SearchSemantics> createSearchSemantics(Model::Model* aModel)
  {
    // Create a new instance of SearchSemantics.
    // @note this creates a new constraint store
    auto ss = new Search::SearchSemantics();
    
    // Register all the constraint into the constraint store
    auto cstore = ss->constraintStore();
    
    // Subscribe each constraint in the model to the constraint store
    auto objType = Model::ModelObjectClass::MODEL_OBJ_CONSTRAINT;
    for (std::size_t cidx{0}; cidx < aModel->numModelObjects(objType); ++cidx)
    {
      auto cobj = Model::ModelConstraint::cast(aModel->getModelObject(objType, cidx));
      assert(cobj && cobj->constraint());
      
      // Subscribe constraint to the constraint store of the search semantic
      cobj->constraint()->subscribeToConstraintStore(cstore);
    }
    
    // Returns the search semantic
    return std::unique_ptr<Search::SearchSemantics>(ss);
  }//generateSearchSemantics
  
  /// Creates and returns a BaseSearchScheme instance,
  /// i.e., the algorithm that explores the search tree.
  /// The returned search scheme is created w.r.t. the given search strategy type and the
  /// list of environments for that search strategy
  Search::BaseSearchScheme* createSearchAlgorithm(Search::SearchEngineStrategyType aStrategyType,
                                                  Model::Model* aModel,
                                                  Search::BaseSearchScheme::EnvironmentList& aEnvList)
  {
    // Create a new search semantics instance
    auto searchSemantics = createSearchSemantics(aModel);
    
    // Create and return the right algorithm (search scheme) w.r.t. the strategy type
    if (aStrategyType == Search::SearchEngineStrategyType::SE_DEPTH_FIRST_SEARCH)
    {
      return new Search::DepthFirstSearch(std::move(searchSemantics), aEnvList);
    }
    
    // Only other option (as of 04/28/2019) is genetic search
    assert(aStrategyType == Search::SearchEngineStrategyType::SE_GENETIC);
    return new Search::GeneticSearch(std::move(searchSemantics), aEnvList);
  }//getSearchStrategy
  
  /// Uploads meta information from "aModel" to the base search algorithm//scheme "aBaseSearch"
  void uploadMetaInfoIntoAlgorithm(Model::Model* aModel, Search::BaseSearchScheme* aBaseSearch)
  {
    assert(aModel);
    assert(aBaseSearch);
    
    // Solution limit
    auto slimit = aModel->getMetaInfo()->solutionLimit();
    if(slimit <= 0) slimit = SM_SOLUTION_LIMIT_NONE;
    
    aBaseSearch->solutionManager()->setSolutionLimit(slimit);
  }//uploadMetaInfoIntoAlgorithm
  
}// namespace

namespace Solver {
  
  AsyncCPEngineProcessor::AsyncCPEngineProcessor(int aThreadPoolSize)
  : AsyncEngineProcessor(aThreadPoolSize)
  {
  }

  void AsyncCPEngineProcessor::setupModel(const Model::ModelSPtr& aModel)
  {
    assert(aModel);
    pModel = aModel;
    
    // Initialize the map of search environments
    initSearchEnvironmentMap();
  }// setupModel
  
  void AsyncCPEngineProcessor::initSearchEnvironmentMap()
  {
    assert(pModel);
    
    // Generate the list of all the search environments and initialize the schemes
    for (const auto& srcInfo : getEnvironmentDescriptorList())
    {
      pSearchEnvironmentMap[srcInfo.searchStrategyId].push_back(
      {srcInfo.searchInfoId, srcInfo.searchEnvironment});
    }
  }// initSearchEnvironmentMap
  
  AsyncCPEngineProcessor::SrcInfoList AsyncCPEngineProcessor::getEnvironmentDescriptorList()
  {
    assert(pModel);
    SrcInfoList searchInfoList;
    
    // Generate the list of search environments according to each search object
    const auto searchModelObjectClass = Model::ModelObjectClass::MODEL_OBJ_SEARCH_SEMANTIC;
    const auto numSearchObjects = pModel->numModelObjects(searchModelObjectClass);
    
    // Resize the vector for adding the search information objects in parallel
    searchInfoList.resize(numSearchObjects);
    
    // Initialize the search information list in parallel
    std::vector<std::thread> threadList;
    for (std::size_t srcIdx{0}; srcIdx < numSearchObjects; ++srcIdx)
    {
      // Get the model search object at given index
      auto srcObject =
      Model::ModelSearch::cast(pModel->getModelObject(searchModelObjectClass, srcIdx));
      
      threadList.push_back(std::thread(&AsyncCPEngineProcessor::addSearchInfoToList, this,
                                       srcObject, &searchInfoList[srcIdx]));
    }
    
    // Join the threads with the main thread
    for (auto &thr : threadList)
    {
      thr.join();
    }
    
    // Return the initialized list
    return searchInfoList;
  }// getEnvironmentDescriptorList
  
  void AsyncCPEngineProcessor::addSearchInfoToList(const Model::ModelSearch* aSrcObj,
                                                   SearchInfo* aSrcInfo)
  {
    aSrcInfo->searchInfoId = aSrcObj->getID();
    
    auto srcDescriptor = (aSrcObj->getSearchDescriptor()).get();
    aSrcInfo->searchStrategyId = static_cast<int>(srcDescriptor->getSearchStrategy());
    
    aSrcInfo->searchEnvironment = getSearchEnvironment(srcDescriptor, pModel.get(), aSrcObj,
                                                       aSrcInfo->searchInfoId);
  }// addSearchInfoToList
  
  Search::SearchEngine*
  AsyncCPEngineProcessor::createSearchEngine(const Search::BaseSearchScheme::SPtr& aAlgorithm)
  {
    // Create search strategy wrapper around the base search scheme, i.e., the algorithm
    auto ss = std::unique_ptr<Search::SearchStrategy>(new Search::SearchStrategy(aAlgorithm));
    
    // Create a search engine on the search strategy
    auto searchEngine = new Search::SearchEngine(std::move(ss));
    
    // Create a solution manager to coordinate solutions and add it as part of the algorithm
    auto idVarMap = pModel->getIDVarMap();
    aAlgorithm->setSolutionManager(std::make_shared<Search::SolutionManager>(idVarMap));
    
    // Upload meta information on the search engine
    uploadMetaInfoIntoAlgorithm(pModel.get(), aAlgorithm.get());
    
    return searchEngine;
  }// createSearchEngine
  
  void AsyncCPEngineProcessor::runEngine()
  {
    // Each search environment should trigger a new search.
    // Search environments are run as found in the map, i.e., there is no ordering
    for (auto& srcEnvironmentTypeListPair : pSearchEnvironmentMap)
    {
      // Get the list of search environments all related to a specific search strategy.
      // Each environment will be the root node of the search tree which will be explored
      // by the same search engine
      const auto environmentList = srcEnvironmentTypeListPair.second;
      assert(!environmentList.empty());
      
      // Create the list of unique pointers used by the BaseSearchScheme
      Search::BaseSearchScheme::EnvironmentList envPtrList(environmentList.size());
      for (std::size_t idx{0}; idx < environmentList.size(); ++idx)
      {
        envPtrList[idx].reset(environmentList[idx].second);
      }
      
      // Get the strategy type
      auto strategyType =
      static_cast<Search::SearchEngineStrategyType>(srcEnvironmentTypeListPair.first);
      
      // Create the search algorithm according to the given strategy.
      // The search algorithm is encoded as a "SearchScheme" which contains all the information
      // required by the search engine to run the algorithm.
      // @note the search scheme contains, among other things, the search semantics which,
      // in turn, contains the constraint store.
      // Every time a new search semantics is created, all the constraints in the model will be
      // deregistered from previous constraint stores and registered to the new constraint store
      // within the new search semantics
      Search::BaseSearchScheme::SPtr searchAlgorithm(createSearchAlgorithm(strategyType,
                                                                           pModel.get(),
                                                                           envPtrList));
      
      // Create a search engine to run the search algorithm
      Search::SearchEngine::SPtr searchEngine(createSearchEngine(searchAlgorithm));
      
      // Run the search engine to explore the search space according to the given algorithm
      auto searchStatus = performSearch(searchEngine.get(), searchAlgorithm.get());
      
      // Undefined answer is not recorded, continue with next search environment
      if (searchStatus == Search::SearchStatus::SEARCH_UNDEF) continue;
      
      // Read the solution and set it into the solver solution
      getModelSolutionCollector()->readSolution(searchStatus, searchAlgorithm->getSolutionManager());
    }
  }// runEngine
  
  Search::SearchStatus AsyncCPEngineProcessor::performSearch(Search::SearchEngine* aEngine,
                                                             Search::BaseSearchScheme* aAlgorithm)
  {
    // Get the number of root nodes of trees to explore and the upper bound on number
    // of solutions to find
    const auto numRootNodes = aAlgorithm->getNumRootNodes();
    if (numRootNodes > 1)
    {
      throw std::runtime_error("More than one root node in the search tree forest");
    }
    
    // Start a search on each root node.
    // The search must be over the forest of search trees and it explores the forst using
    // a DFS algorithm to backtrack in case of failures.
    // Start by exploring the first search tree in the forest. This will be the root of
    // the forest.
    // Note that only if the number of root nodes in the forest is greater than one it is required
    // to perform a DFS search on the forest of search trees
    auto searchStatus = aEngine->label(aAlgorithm->getRootNode(0));
    
    // Save the metrics for the constructed search tree
    getMetricsList()->push_back(
    {
      aAlgorithm->getSearchEnvironment()->getEnvironmentId(),
      aAlgorithm->getMetrics()
    });
    
    // return the status of the search
    return searchStatus;
  }// performSearch
  
  
}// end namespace Solver
