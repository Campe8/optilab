// Base class
#include "CPEngine.hpp"

#include <memory>     // for std::make_shared
#include <sstream>
#include <stdexcept>  // for std::runtime_error
#include <vector>

#include "AsyncCPEngineProcessor.hpp"
#include "JSON.hpp"
#include "SolverUtilities.hpp"

namespace {
  /// Waiting value for an idle processor
  constexpr int kWaitOnIdleProcessor{-1};
}// namespace

namespace Solver {
  
  CPEngine::CPEngine(const std::string& aEngineId, const EngineCallbackHandler::SPtr& aHandler)
  : pEngineId(aEngineId)
  , pEngineProcessor(std::make_shared<AsyncCPEngineProcessor>())
  , pCallbackHandler(aHandler)
  {
    if (pEngineId.empty()) throw std::invalid_argument("Empty engine identifier");
    if (!pCallbackHandler) throw std::invalid_argument("Empty engine callback handler");
  }
  
  void CPEngine::registerModel(const Model::ModelSPtr& aModel)
  {
    if (!aModel)
    {
      std::ostringstream ss;
      ss << "Registering an empty model for engine " << pEngineId;
      throw std::runtime_error(ss.str());
    }
    
    if (pRegisteredModel)
    {
      std::ostringstream ss;
      ss << "Already registered a model for engine " << pEngineId;
      throw std::runtime_error(ss.str());
    }
    
    // Register the model into the engine processor
    engineWait(kWaitOnIdleProcessor);
    pEngineProcessor->registerModel(aModel);
    
    // Set flag registered model flag
    pRegisteredModel = true;
  }// registerModel
  
  void CPEngine::engineWait(int aTimeoutSec)
  {
    pEngineProcessor->processorWait(aTimeoutSec);
  }
  
  void CPEngine::notifyEngine(const EngineEvent& aEvent)
  {
    if (!pRegisteredModel)
    {
      std::ostringstream ss;
      ss << "No registered model for engine " << pEngineId;
      throw std::runtime_error(ss.str());
    }
    
    switch(aEvent.getType())
    {
      case EngineEvent::EventType::kRunModel:
      {
        processRunModelEvent();
        break;
      }
      case EngineEvent::EventType::kCollectSolutions:
      {
        processCollectSolutionsEvent(aEvent.getNumSolutions());
        break;
      }
      default:
      {
        assert(aEvent.getType() == EngineEvent::EventType::kKill);
        break;
      }
    }
  }// notifyEngine
  
  void CPEngine::processRunModelEvent()
  {
    // Return if the engine is already active
    if (pActiveEngine)
    {
      return;
    }
    
    // Wait on previous tasks to complete.
    // For example, it waits on model registration
    engineWait(kWaitOnIdleProcessor);
    
    // Trigger the engine to run and return
    pEngineProcessor->runModel();
  }  // processRunModelEvent
  
  void CPEngine::processCollectSolutionsEvent(std::size_t aNumSolutions)
  {
    const auto numSol = pEngineProcessor->getModelSolutionCollector()->numSolutions();
    if (aNumSolutions > numSol || aNumSolutions <= 0)
    {
      aNumSolutions = numSol;
    }
    
    const auto& solutionCollector = pEngineProcessor->getModelSolutionCollector();
    std::vector<DataStructure::JSON::SPtr> solutionJSONCollection;
    for (int idx = 0; idx < aNumSolutions; ++idx)
    {
      auto jsonSol =
      Utilities::convertSolutionToJson(solutionCollector->getSolution(static_cast<std::size_t>(idx)));
      solutionJSONCollection.push_back(jsonSol);
    }
    
    // Create the JSON representing the collection of solutions
    auto searchStatus = solutionCollector->searchStatus();
    auto JSONSolutionCollection = Utilities::createJSONSolutionCollection(solutionJSONCollection,
                                                                          searchStatus);
    
    // Callback method to send back results to the caller
    pCallbackHandler->solutionCallback(pEngineId, JSONSolutionCollection->toString());
    
    // Send back metrics
    auto metrics = pEngineProcessor->getMetricsList();
    auto JSONMetricsCollection = Utilities::createJSONMetricsCollection(*metrics);
    pCallbackHandler->metricsCallback(pEngineId, JSONMetricsCollection->toString());
  }// processRunModelEvent
  
}// end namespace Solver
