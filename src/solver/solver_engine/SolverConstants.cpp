#include "SolverConstants.hpp"

namespace Solver { namespace Constants {
  
  namespace Solution {
    const char* SOLUTION = "solution";
    const char* SOLUTION_COLLECTION = "solutionCollection";
    const char* SOLUTION_STATUS = "solutionStatus";
    const char* VAR_DOM = "varDom";
    const char* VAR_ID = "varId";
  }// end namespace Solution
  
  namespace Metrics {
    const char* METRICS_COLLECTION = "metricsCollection";
    const char* SEARCH_ENVIRONMENT = "searchEnvironment";
  }// end namespace Metrics
  
}}// end namespace Solver
