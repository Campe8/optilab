// Base class
#include "ModelCallbackHandler.hpp"

#include <stdexcept> // fot std::out_of_range

#include "JSON.hpp"
#include "SolverConstants.hpp"

namespace Solver {
 
  void ModelCallbackHandler::solutionCallback(const std::string& engineId, const std::string& json)
  {
    DataStructure::JSON solutionCollection(json);
    assert(solutionCollection.hasObject(Constants::Solution::SOLUTION_COLLECTION));
    assert(solutionCollection.hasObject(Constants::Solution::SOLUTION_STATUS));
    
    auto sstatus = solutionCollection.getObject(Constants::Solution::SOLUTION_STATUS).GetInt();
    pSearchStatus = static_cast<Search::SearchStatus>(sstatus);
    
    auto& scollection = solutionCollection.getObject(Constants::Solution::SOLUTION_COLLECTION);
    assert(scollection.IsArray());
    
    for (rapidjson::SizeType idx = 0; idx < scollection.Size(); ++idx)
    {
      auto& jsonSolution = scollection[idx];
      assert(jsonSolution.IsArray());
      
      // @note jsonSolution is something like:
      // [{"varId":"y","varDom":[["2"]]},{"varId":"x","varDom":[["1"]]}]
      Solution solution;
      solution.reserve(jsonSolution.Size());
      for (rapidjson::SizeType solObjIdx = 0; solObjIdx < jsonSolution.Size(); ++solObjIdx)
      {
        // @note solObj is something like:
        // {"varId":"y","varDom":[["2"]]}
        auto& solObj = jsonSolution[solObjIdx];
        assert(solObj.HasMember(Constants::Solution::VAR_ID));
        
        // Store the variable identifier
        std::string varId(solObj[Constants::Solution::VAR_ID].GetString());
        
        // Store the domain of the variable
        DomainList domainList;
        assert(solObj.HasMember(Constants::Solution::VAR_DOM));
        assert(solObj[Constants::Solution::VAR_DOM].IsArray());
        
        // @note domain is something like:
        // [["2"], ["1", "4"]]
        auto& domain = solObj[Constants::Solution::VAR_DOM];
        for (rapidjson::SizeType boundsIdx = 0; boundsIdx < domain.Size(); ++boundsIdx)
        {
          DomainBounds domBounds;
          auto& bounds = domain[boundsIdx];
          assert(bounds.IsArray());
          
          if (bounds.Size() == 1)
          {
            domBounds = std::make_pair<std::string>(bounds[0].GetString(), bounds[0].GetString());
          }
          else if (bounds.Size() == 2)
          {
            domBounds = std::make_pair<std::string>(bounds[0].GetString(), bounds[1].GetString());
          }
          else
          {
            throw std::runtime_error("Domain bounds do not contains 1 or 2 elements but " +
                                     std::to_string(bounds.Size()) + " elements");
          }
          domainList.push_back(domBounds);
        }
        
        // Add variable id and domain list as a solution element
        solution.push_back({varId, domainList});
      }
      
      // Add the solution to the solution list
      pSolutionList.push_back(solution);
    }
  }// solutionCallback
  
  void ModelCallbackHandler::metricsCallback(const std::string& engineId, const std::string& json)
  {
    DataStructure::JSON metricsJson(json);
    if (metricsJson.hasObject(Constants::Metrics::METRICS_COLLECTION))
    {
      auto& metrics = metricsJson.getObject(Constants::Metrics::METRICS_COLLECTION);
      assert(metrics.IsArray());
      
      const std::string srcEnvStr(Constants::Metrics::SEARCH_ENVIRONMENT);
      for (rapidjson::SizeType idx = 0; idx < metrics.Size(); idx++)
      {
        Base::Tools::MetricsRegister::SPtr metricsRegister =
        std::make_shared<Base::Tools::MetricsRegister>();
        
        auto& val = metrics[idx];
        assert(val.HasMember(Constants::Metrics::SEARCH_ENVIRONMENT));
        
        for (auto it = val.MemberBegin(); it != val.MemberEnd(); ++it)
        {
          // Continue on the name of the current search environment, which will be used
          // as key in the metrics map
          const std::string objName(it->name.GetString());
          if (srcEnvStr == objName) continue;
          
          metricsRegister->addMetric(objName, it->value.GetInt64());
        }
        
        // Store the register in the map
        pMetricsMap[val[Constants::Metrics::SEARCH_ENVIRONMENT].GetString()] = metricsRegister;
      }
    }
  }// metricsCallback
  
  bool ModelCallbackHandler::satisfied() const
  {
    if (pSolutionList.empty()) return false;
    
    switch (pSearchStatus)
    {
      case Search::SearchStatus::SEARCH_SUCCESS:
        return true;
      case Search::SearchStatus::SEARCH_TERMINATE:
        return false;
      default:
        assert(pSearchStatus == Search::SearchStatus::SEARCH_FAIL);
        return false;
    }
  }// satisfied
  
  const ModelCallbackHandler::Solution& ModelCallbackHandler::getSolution(std::size_t solIdx) const
  {
    if (solIdx >= numSolutions())
    {
      throw std::out_of_range("Invalid solution index " + std::to_string(solIdx) +
                              " greater than the number of solutions " +
                              std::to_string(numSolutions()));
    }
    
    return pSolutionList.at(solIdx);
  }// getSolution
  
}// end namespace Solver
