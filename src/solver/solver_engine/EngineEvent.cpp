#include "EngineEvent.hpp"

namespace Solver {
  
  EngineEvent::EngineEvent(EventType aType, std::size_t aNumSolutions)
  : pEventType(aType)
  , pNumSolutions(aNumSolutions)
  {
  }
  
}// end namespace Solver
