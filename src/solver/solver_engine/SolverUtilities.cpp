#include "SolverUtilities.hpp"

#include <memory>  // for std::make_shared
#include <stdexcept>  // for std::runtime_error

#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

#include "SolverConstants.hpp"

namespace Solver { namespace Utilities {
  
  DataStructure::JSON::SPtr
  convertSolutionToJson(const SolverSolutionCollector::Solution& solution)
  {
    DataStructure::JSON::SPtr json = std::make_shared<DataStructure::JSON>();
    
    auto jsonSolution = json->createArray();
    for (auto& pairVarDom : solution)
    {
      // Create the object [id, domain]
      auto obj = json->createObject();
      
      // Create the id object
      auto varId = json->createObject(pairVarDom.first);
      json->addObjToObject(Constants::Solution::VAR_ID, obj, varId);
      
      // Create the domain object
      auto vadDom = json->createArray();
      for (const auto& domPair: pairVarDom.second)
      {
        // Create the bounds domain
        auto bounds = json->createArray();
        if (domPair.first->isEqual(domPair.second))
        {
          auto domVal = json->createObject(domPair.first->toString());
          json->addObjToArray(bounds, domVal);
        }
        else
        {
          auto domVal1 = json->createObject(domPair.first->toString());
          auto domVal2 = json->createObject(domPair.second->toString());
          json->addObjToArray(bounds, domVal1);
          json->addObjToArray(bounds, domVal2);
        }
        json->addObjToArray(vadDom, bounds);
      }
      json->addObjToObject(Constants::Solution::VAR_DOM, obj, vadDom);
      
      // Add the pair [id, domain] to the solution
      json->addObjToArray(jsonSolution, obj);
    }
    
    json->addValue(Constants::Solution::SOLUTION, jsonSolution);
    
    return json;
  }// convertSolutionToJson
  
  DataStructure::JSON::SPtr
  createJSONSolutionCollection(const std::vector<DataStructure::JSON::SPtr>& jsonSolutionCollection,
                               Search::SearchStatus searchStatus)
  {
    DataStructure::JSON::SPtr json = std::make_shared<DataStructure::JSON>();
    
    auto solutionCollection = json->createArray();
    for (const auto& sol : jsonSolutionCollection)
    {
      if (!sol->hasObject(Constants::Solution::SOLUTION))
      {
        throw std::runtime_error("No \"" + std::string(Constants::Solution::SOLUTION) +
                                 "\" object in JSON solution collection");
      }
      json->addObjToArray(solutionCollection, sol->getObject("solution"));
    }
    json->addValue(Constants::Solution::SOLUTION_COLLECTION, solutionCollection);
    
    auto sstatusObj = json->createObject<int>(static_cast<int>(searchStatus));
    json->addValue(Constants::Solution::SOLUTION_STATUS, sstatusObj);
    
    return json;
  }// createJSONSolutionCollection

  DataStructure::JSON::SPtr
  createJSONMetricsCollection(const AsyncEngineProcessor::SearchMetricsList& metrics)
  {
    DataStructure::JSON::SPtr json = std::make_shared<DataStructure::JSON>();
    
    using MetricValueType = Base::Tools::MetricsRegister::MetricValue;
    
    auto metricsCollection = json->createArray();
    for (const auto& met : metrics)
    {
      auto obj = json->createObject();
      auto environment = json->createObject(met.first);
      json->addObjToObject(Constants::Metrics::SEARCH_ENVIRONMENT, obj, environment);
      
      // Iterate over the metrics and create the correspondent objects
      for (const auto& m : *(met.second))
      {
        auto metVal = json->createObject<MetricValueType>(m.second);
        json->addObjToObject(m.first, obj, metVal);
      }
      
      json->addObjToArray(metricsCollection, obj);
    }
    
    // Add the collection of metrics to the JSON object
    json->addValue(Constants::Metrics::METRICS_COLLECTION, metricsCollection);
    
    return json;
  }// createJSONMetricsCollection
  
}// end namespace Utilities
}// end namespace Solver
