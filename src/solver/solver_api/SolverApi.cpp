// Base class
#include "SolverApi.hpp"

#include "CPEngine.hpp"
#include "EngineEvent.hpp"

#define ASSERT_AND_RETURN_ERROR(expr)         \
  do                                          \
  {                                           \
    if (!(expr)) return SolverError::kError;  \
  } while (0)

namespace Solver {
  
  SolverApi::SolverError SolverApi::createEngine(const std::string& aEngineId) noexcept
  {
    ASSERT_AND_RETURN_ERROR(!aEngineId.empty());
    {
      // Lock mutex on critical section
      LockGuard lock(pMutex);
      if (pEngineMap.find(aEngineId) != pEngineMap.end())
      {
        // Registering twice the same engine is not allowed
        return SolverError::kError;
      }
      
      Engine::SPtr engine;
      try
      {
        engine = std::make_shared<CPEngine>(aEngineId, pEngineHandler);
      }
      catch (...)
      {
        return SolverError::kError;
      }
      
      pEngineMap[aEngineId] = engine;
    }
    
    return SolverError::kNoError;
  }// createEngine
  
  SolverApi::SolverError SolverApi::deleteEngine(const std::string& aEngineId) noexcept
  {
    ASSERT_AND_RETURN_ERROR(!aEngineId.empty());
    {
      // Lock mutex on critical section
      LockGuard lock(pMutex);
      
      // Lookup the instance on the map
      if (pEngineMap.find(aEngineId) != pEngineMap.end())
      {
        pEngineMap.erase(aEngineId);
        return SolverError::kNoError;
      }
    }
    
    // If the flow reached this point, return an error
    return SolverError::kError;
  }// deleteEngine
  
  SolverApi::SolverError SolverApi::loadModel(const std::string& aEngineId,
                                              const Model::ModelSPtr& aModel) noexcept
  {
    ASSERT_AND_RETURN_ERROR(!aEngineId.empty());
    ASSERT_AND_RETURN_ERROR(aModel);
    
    auto engine = getEngine(aEngineId);
    ASSERT_AND_RETURN_ERROR(engine);
    
    try
    {
      engine->registerModel(aModel);
    }
    catch (...)
    {
      return SolverError::kError;
    }
    
    return SolverError::kNoError;
  }// loadModel
  
  SolverApi::SolverError SolverApi::runModel(const std::string& aEngineId) noexcept
  {
    ASSERT_AND_RETURN_ERROR(!aEngineId.empty());
    
    auto engine = getEngine(aEngineId);
    ASSERT_AND_RETURN_ERROR(engine);
    
    try
    {
      EngineEvent event(EngineEvent::EventType::kRunModel);
      engine->notifyEngine(event);
    }
    catch (...)
    {
      return SolverError::kError;
    }
    
    return SolverError::kNoError;
  }// notifyEvent
  
  SolverApi::SolverError SolverApi::collectSolutions(const std::string& aEngineId,
                                                     std::size_t aNumSol) noexcept
  {
    ASSERT_AND_RETURN_ERROR(!aEngineId.empty());
    
    auto engine = getEngine(aEngineId);
    ASSERT_AND_RETURN_ERROR(engine);
    
    try
    {
      EngineEvent event(EngineEvent::EventType::kCollectSolutions, aNumSol);
      engine->notifyEngine(event);
    }
    catch (...)
    {
      return SolverError::kError;
    }
    
    return SolverError::kNoError;
  }// collectSolutions
  
  SolverApi::SolverError SolverApi::engineWait(const std::string& aEngineId,
                                               int aTimeoutSec) noexcept
  {
    ASSERT_AND_RETURN_ERROR(!aEngineId.empty());
    
    auto engine = getEngine(aEngineId);
    ASSERT_AND_RETURN_ERROR(engine);
    try
    {
      static_cast<CPEngine*>(engine.get())->engineWait(aTimeoutSec);
    }
    catch (...)
    {
      return SolverError::kError;
    }
    
    return SolverError::kNoError;
  }// engineWait
  
  SolverApi::SolverError
  SolverApi::setEngineHandler(const EngineCallbackHandler::SPtr& aHandler) noexcept
  {
    ASSERT_AND_RETURN_ERROR(aHandler);
    pEngineHandler = aHandler;
    
    return SolverError::kNoError;
  }// setEngineHandler
  
  Engine::SPtr SolverApi::getEngine(const std::string& aEngineId)
  {
    {
      // Lock mutex on critical section
      LockGuard lock(pMutex);
      
      // Lookup the instance on the map
      if (pEngineMap.find(aEngineId) != pEngineMap.end())
      {
        return pEngineMap[aEngineId];
      }
    }
    
    Engine::SPtr emptyPtr;
    return emptyPtr;
  }// getEngine
  
}// end namespace Solver
