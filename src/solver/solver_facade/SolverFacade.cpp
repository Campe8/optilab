// Base class
#include "SolverFacade.hpp"

#include "StdOutCallbackHandler.hpp"

#include <unordered_map>
#include <string>
#include <cassert>

namespace Solver {
  
  SolverFacade::SolverFacade()
  : pSolverEngine(new SolverEngine())
  {
  }
  
  std::size_t SolverFacade::numSolutions(SolverEngine::ModelKey aModelKey) const
  {
    // Return no solutions if model is not registered or the model has no solutions
    if ((pModelRegister.find(aModelKey) == pModelRegister.end()) ||
        (pSolverEngine->getSolution(aModelKey) == nullptr))
    {
      return 0;
    }
    
    return pSolverEngine->getSolution(aModelKey)->numSolutions();
  }//numSolutions
  
  bool SolverFacade::satisfied(SolverEngine::ModelKey aModelKey) const
  {
    // Return no solutions if model is not registered or the model has no solutions
    if ((pModelRegister.find(aModelKey) == pModelRegister.end()) ||
        (pSolverEngine->getSolution(aModelKey) == nullptr))
    {
      return false;
    }
    
    auto sol = pSolverEngine->getSolution(aModelKey);
    switch (sol->searchStatus())
    {
      case Search::SearchStatus::SEARCH_SUCCESS:
        return true;
      case Search::SearchStatus::SEARCH_TERMINATE:
        return false;
      default:
        assert(sol->searchStatus() == Search::SearchStatus::SEARCH_FAIL);
        return false;
    }
  }//satisfied
  
  SolverSolution::Solution SolverFacade::getSolution(SolverEngine::ModelKey aModelKey,
                                                     std::size_t aIdx)
  {
    assert(aIdx < numSolutions(aModelKey));
    return pSolverEngine->getSolution(aModelKey)->getSolution(aIdx);
  }//getSolution
  
  SolverEngine::ModelKey SolverFacade::registerModel(const Model::ModelSPtr& aModel)
  {
    assert(aModel);
    
    // Register model to the engine
    auto modelKey = pSolverEngine->registerModel(aModel);
    assert(pModelRegister.find(modelKey) == pModelRegister.end());
    
    pModelRegister.insert(modelKey);
    return modelKey;
  }//registerModel
  
  std::unordered_map<std::string, Base::Tools::MetricsRegister::SPtr>
  SolverFacade::getMetrics(SolverEngine::ModelKey aModelKey) const
  {
    assert(pModelRegister.find(aModelKey) != pModelRegister.end());
    std::unordered_map<std::string, Base::Tools::MetricsRegister::SPtr> mreg;
    
    for (std::size_t idx{0}; idx < pSolverEngine->getNumMetricsForModel(aModelKey); ++idx)
    {
      const auto& mets = pSolverEngine->getMetrics(aModelKey, idx);
      mreg[mets.first] = mets.second;
    }
    return mreg;
  }//getMetrics
  
  void SolverFacade::runModel(SolverEngine::ModelKey aModelKey)
  {
    assert(pModelRegister.find(aModelKey) != pModelRegister.end());
    pSolverEngine->runModel(aModelKey);
    
    /*
    StdOutCallbackHandler::SPtr handler = std::make_shared<StdOutCallbackHandler>();
    pSolverApi->setEngineHandler(handler);
    
    const std::string engineId = "EngineId";
    pSolverApi->createEngine(engineId);
    pSolverApi->loadModel(engineId, pModel);
    pSolverApi->engineWait(engineId, -1);
    pSolverApi->runModel(engineId);
    pSolverApi->engineWait(engineId, -1);
    pSolverApi->collectSolutions(engineId, 1);
     */
  }//runModel
  
}// end namespace Solver
