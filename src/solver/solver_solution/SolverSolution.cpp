// Self class
#include "SolverSolution.hpp"

#include "VariableExplosionTools.hpp"
#include "VariableArray.hpp"
#include "DomainElement.hpp"

#include <sstream>
#include <cassert>

namespace Solver {
  
  SolverSolution::SolverSolution(const Model::ModelVarMapSPtr& aVarMap)
  : pSearchStatus(Search::SearchStatus::SEARCH_UNDEF)
  , pIDVarMap(aVarMap)
  {
  }
  
  void SolverSolution::readSolution(Search::SearchStatus aSearchStatus,
                                    const std::shared_ptr<Search::SolutionManager>& aSManager)
  {
    assert(pIDVarMap && aSManager);
    
    pSearchStatus = aSearchStatus;
    for (std::size_t solIdx{1}; solIdx <= aSManager->getNumberOfRecordedSolutions(); ++solIdx)
    {
      const auto& solution = aSManager->getSolution(solIdx);
      
      Solution ssol;
      ssol.reserve(solution.size());
      pSolutionRegister.push_back(ssol);
      for(auto& it : solution)
      {
        assert(modelVarMap().find(it.first) != modelVarMap().end());
        pSolutionRegister.back().push_back({modelVarMap()[it.first], it.second});
      }
    }
  }// readSolution
  
  std::string SolverSolution::getVariableID(const SolverSolution::SolutionElement& aSElem)
  {
    assert(aSElem.first);
    return aSElem.first->getVariableNameId();
  }//getVariableID
  
  std::vector<std::vector<std::string>> SolverSolution::getVariableDomain(const SolutionElement& aSElem)
  {
    std::vector<std::vector<std::string>> ppSol;
    for (auto& s : aSElem.second)
    {
      assert(s.first && s.second);
      
      // Update bounds
      std::stringstream ssd1;
      std::stringstream ssd2;
      ssd1 << *(s.first);
      ssd2 << *(s.second);
      
      // Push the string representing the bounds on the solution
      ppSol.push_back({ ssd1.str(), ssd2.str() });
      if(s.first->isEqual(s.second))
      {
        // Update singleton (remove one of the bounds)
        ppSol.back().pop_back();
      }
    }
    
    return ppSol;
  }//getVariableDomain
  
  bool SolverSolution::isVariableDomainSingleton(const std::vector<std::vector<std::string>>& aDomain)
  {
    assert(aDomain.size() > 0);
    for(const auto& dom : aDomain)
    {
      assert(!dom.empty());
      if(dom.size() != 1) return false;
    }
    return true;
  }//isVariableDomainSingleton
  
}// end namespace Solver
