// Base class
#include "OutputConverterSolverSolution.hpp"

#include "DomainElement.hpp"

#include "Variable.hpp"
#include "VariableArray.hpp"
#include "VariableBoolean.hpp"

#include "CLIUtils.hpp"

#include <algorithm>
#include <cassert>
#include <sstream>
#include <string>

namespace {
  
  constexpr std::size_t kMaxLineLength = 10;
  
}// namespace

namespace Solver {
  
  static std::string printBoolValue(bool aVal)
  {
    return aVal ? CLI::Utils::toString<BOOL_T>(1) : CLI::Utils::toString<BOOL_T>(0);
  }//printBoolValue
  
  static std::string printBoolDomain(bool aLB, bool aUB)
  {
    if(aLB == aUB)
    {
      return aLB ? CLI::Utils::toString<BOOL_T>(1) : CLI::Utils::toString<BOOL_T>(0);
    }
    
    std::stringstream ss;
    ss << "[";
    ss << printBoolValue(aLB);
    ss << ", ";
    ss << printBoolValue(aUB);
    ss << "]";
    
    return ss.str();
  }//printBoolDomain
  
  // Returns a string representing the domain as ["aLowerBound", "aUpperBound"]
  static std::string domainToString(const std::string& aLB, const std::string& aUB, bool isBool)
  {
    if(isBool)
    {
      auto isLBZero = std::stoi(aLB) == 0;
      auto isUBZero = std::stoi(aUB) == 0;
      return printBoolDomain(isLBZero, isUBZero);
    }
    
    std::stringstream ss;
    if (aLB == aUB)
    {
      ss << aLB;
    }
    else
    {
      ss << "[";
      ss << aLB;
      ss << ", ";
      ss << aUB;
      ss << "]";
    }
    
    return ss.str();
  }//domainToString
  
  static std::string solutionToString(const Model::ModelSPtr& model,
                                      const ModelCallbackHandler::Solution& solution)
  {
    // Get the map of variables
    const auto& varMap = model->getIDVarMap();
    
    // Print the solution as a string
    std::stringstream ss;
    for (const auto& sol : solution)
    {
      auto varIter = varMap->find(sol.first);
      if (varIter == varMap->end() || !varIter->second->variableSemantic()->isOutput()) continue;
      
      // Boolean variables require a special representation
      auto isBool = Core::VariableBoolean::isa((varIter->second).get());
      
      ss << sol.first;
      ss << ":\t";
      if (Core::VariableArray::isa((varIter->second).get()))
      {
        ss << "[";
        assert (sol.second.size() == Core::VariableArray::cast((varIter->second).get())->size());
        for (std::size_t idx{0}; idx < sol.second.size() - 1; ++idx)
        {
          ss << domainToString(sol.second[idx].first, sol.second[idx].second, isBool);
          ss << ", ";
          if (idx > 0 && (idx % kMaxLineLength) == 0)
          {
            ss << '\n';
          }
        }
        ss << domainToString(sol.second[sol.second.size() - 1].first,
                             sol.second[sol.second.size() - 1].second, isBool);
        ss << "]";
      }
      else
      {
        assert(sol.second.size() == 1);
        ss << domainToString(sol.second[0].first, sol.second[0].second, isBool);
      }
      
      ss << '\n';
    }
    
    auto solStr = ss.str();
    solStr.pop_back();
    return solStr;
  }//solutionToString
  
  std::ostream&
  OutputConverterSolverSolution::ostreamSolution(std::ostream& OStream,
                                                 const Model::ModelSPtr& model,
                                                 const ModelCallbackHandler::Solution& solution)
  {
    OStream << solutionToString(model, solution);
    return OStream;
  }//ostreamSolution
  
  bool
  OutputConverterSolverSolution::isOutputSolution(const Model::ModelSPtr& model,
                                                  const ModelCallbackHandler::Solution& solution) const
  {
    assert(model);
    
    const auto& varMap = model->getIDVarMap();
    for (const auto& sol : solution)
    {
      auto varIter = varMap->find(sol.first);
      if (varIter == varMap->end()) continue;
      if (varIter->second->variableSemantic()->isOutput()) return true;
    }
    
    return false;
  }//isOutputSolution
  
}// end namespace Solver
