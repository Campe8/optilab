// Self class
#include "SolverSolutionCollector.hpp"

#include <stdexcept>  // for std::runtime_error

namespace Solver {
  
  SolverSolutionCollector::SolverSolutionCollector(const Model::ModelVarMapSPtr& aVarMap)
  : pSearchStatus(Search::SearchStatus::SEARCH_UNDEF)
  , pIDVarMap(aVarMap)
  {
    if (!aVarMap) throw std::invalid_argument("Invalid empty pointer to a ModelVarMap instance");
  }
  
  const SolverSolutionCollector::Solution&
  SolverSolutionCollector::getSolution(std::size_t idx) const
  {
    if (idx >= numSolutions())
    {
      throw std::out_of_range("Invalid solution index");
    }
    
    {
      // Lock mutex on critical section
      LockGuard lock(pMutex);
      return pSolutionCollection.at(idx);
    }
  }// getSolution
  
  const SolverSolutionCollector::SolutionCollection&
  SolverSolutionCollector::getSolutionCollection() const
  {
    {
      // Lock mutex on critical section
      LockGuard lock(pMutex);
      return pSolutionCollection;
    }
  }// getSolutionCollection
  
  std::size_t SolverSolutionCollector::numSolutions() const
  {
    {
      // Lock mutex on critical section
      LockGuard lock(pMutex);
      return pSolutionCollection.size();
    }
  }// numSolutions
  
  void SolverSolutionCollector::readSolution(Search::SearchStatus searchStatus,
                                             const Search::SolutionManager::SPtr& solutionManager)
  {
    if (!solutionManager)
    {
      throw std::invalid_argument("Empty solution manager");
    }
    
    {
      // Lock mutex on critical section
      LockGuard lock(pMutex);
      
      // Set the status of the search
      pSearchStatus = searchStatus;
      
      for (int idx{1}; idx <= static_cast<int>(solutionManager->getNumberOfRecordedSolutions()); ++idx)
      {
        const auto& solution = solutionManager->getSolution(idx);
        
        Solution ssol;
        ssol.reserve(solution.size());
        pSolutionCollection.push_back(ssol);
        for (auto& it : solution)
        {
          assert(modelVarMap().find(it.first) != modelVarMap().end());
          pSolutionCollection.back().push_back(
          {
            modelVarMap()[it.first]->getVariableNameId(),
            it.second
          });
        }
      }// for
    }
  }// readSolution
  
  std::pair<bool, std::vector<std::vector<std::string>>>
  SolverSolutionCollector::getVariableDomain(const DomainList& domainList)
  {
    std::vector<std::vector<std::string>> ppSol;
    
    bool isSingleton = true;
    for (auto& pairDomElements : domainList)
    {
      // Update bounds
      std::stringstream ssd1;
      std::stringstream ssd2;
      ssd1 << *(pairDomElements.first);
      ssd2 << *(pairDomElements.second);
      
      // Push the string representing the bounds on the solution
      ppSol.push_back({ ssd1.str(), ssd2.str() });
      if (pairDomElements.first->isEqual(pairDomElements.second))
      {
        // Update singleton (remove one of the bounds)
        ppSol.back().pop_back();
      }
      else
      {
        isSingleton = false;
      }
    }
    
    return {isSingleton, ppSol};
  }//getVariableDomain
  
}// end namespace Solver
