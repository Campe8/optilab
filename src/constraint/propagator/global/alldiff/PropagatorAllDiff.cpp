// Self first
#include "PropagatorAllDiff.hpp"

// Utility
#include "PropagatorUtils.hpp"

// Specular domain
#include "DomainSpecular.hpp"

#include <memory>
#include <unordered_set>
#include <cassert>

// Macro for less accurate but faster fitness function
//#define FAST_FITNESS

namespace Core {
  
  // Returns true if "elem" is a unique singleton element, i.e., never seen before.
  // Returns false otherwise
  static bool isUniqueSingleton(DomainElement* elem, std::vector<DomainElement*>& elemList)
  {
    for(auto e : elemList)
    {
      if(elem->isEqual(e)) return false;
    }
    elemList.push_back(elem);
    return true;
  }//isUniqueSingleton
  
  PropagatorAllDiff::PropagatorAllDiff(DomainArray& aDomArray)
  : PropagatorArray(PropagatorSemantics(aDomArray.size(),
                                        PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
                                        PropagationPriority::PROP_PRIORITY_LINEAR,
                                        PropagatorSemanticType::PROP_SEMANTIC_TYPE_ALLDIFF,
                                        PropagatorSemanticClass::PROP_SEMANTIC_CLASS_GLOBAL), aDomArray)
  {
    Domain* domain;
    minSortedDomains.reserve(arraySizeINT64()); 
    maxSortedDomains.reserve(arraySizeINT64()); 
    for(std::size_t idx = 0; idx < arraySizeINT64(); ++idx)
    {
      domain = domainArrayAtINT64(idx);
      addContextDomain(idx, domain);
      minSortedDomains.push_back(std::make_pair(domain, idx));
      maxSortedDomains.push_back(std::make_pair(domain, idx));
    }
    
    // REGISTER FILTERS
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
  }
  
  double PropagatorAllDiff::getFitness()
  {
    // Fitness for alldifferent propagator
    // return Card(V_i == V_j, i < j)
    
    std::size_t equalCtr{0};
    
#ifdef FAST_FITNESS
    
    // This method is O(nlogn)
    // 1 - Sort the vector of domains
    std::sort(minSortedDomains.begin(), minSortedDomains.end(), compareLowerBounds);
    
    // 2 - Use two pointers scanning the list from left to right
    std::size_t idx1{0};
    std::size_t idx2{1};
    
    // Check if the first domain is empty or not singleton, if so return
    if (PropagatorUtils::isDomainEmpty(minSortedDomains[idx1].first) ||
        !PropagatorUtils::isDomainSingleton(minSortedDomains[idx1].first))
    {
      return Propagator::getUnspecFitness();
    }
    
    // Iterate over the sorted list of domains and count the equal elements
    for (; idx2 < minSortedDomains.size(); ++idx2)
    {
      auto dom1 = minSortedDomains[idx1].first;
      auto dom2 = minSortedDomains[idx2].first;
      
      // Return if a domain is empty or not singleton
      if (PropagatorUtils::isDomainEmpty(dom2) || !PropagatorUtils::isDomainSingleton(dom2))
      {
        return Propagator::getUnspecFitness();
      }
      
      // If dom2 is equal to dom1, increase the counter
      if (dom2->lowerBound()->isEqual(dom1->lowerBound()))
      {
        equalCtr++;
      }
      else
      {
        // Otherwise update the first pointer
        idx1 = idx2;
      }
    }
    
#else
    
    // This method is O(n^2) since it is iterating over all domain pairs (i < j)
    auto numDoms = minSortedDomains.size();
    for (std::size_t idx1{0}; idx1 < numDoms - 1; ++idx1)
    {
      auto dom1 = minSortedDomains[idx1].first;
      if (PropagatorUtils::isDomainEmpty(dom1) || !PropagatorUtils::isDomainSingleton(dom1))
      {
        return Propagator::getUnspecFitness();
      }
      for (std::size_t idx2{idx1+1}; idx2 < numDoms; ++idx2)
      {
        auto dom2 = minSortedDomains[idx2].first;
        if (PropagatorUtils::isDomainEmpty(dom2) || !PropagatorUtils::isDomainSingleton(dom2))
        {
          return Propagator::getUnspecFitness();
        }
        
        // If dom2 is equal to dom1, increase the counter
        if (dom2->lowerBound()->isEqual(dom1->lowerBound()))
        {
          equalCtr++;
        }
      }
    }
    
#endif
    
    // Return the number of equal domains as fitness value
    return static_cast<double>(equalCtr);
  }//getFitness
  
  PropagationEvent PropagatorAllDiff::post()
  {
    return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
  }//post
  
  PropagationEvent PropagatorAllDiff::runPropagation()
  {
    std::sort(minSortedDomains.begin(), minSortedDomains.end(), compareLowerBounds);
    std::sort(maxSortedDomains.begin(), maxSortedDomains.end(), compareUpperBounds);
    
    const std::size_t n = arraySizeINT64();
    std::vector<Hall> hall(n*2+2);
    std::vector<Rank> rank(n);
    
    int nb = 0;
    {
      DomainElement* min = minSortedDomains[0].first->lowerBound();
      DomainElement* max = DomainElement::successor(maxSortedDomains[0].first->upperBound());
      DomainElement* last = DomainElement::predecessor(DomainElement::predecessor(min));

      hall[0].bounds = last;

      int i = 0;
      int j = 0;
      while (true)
      {
        if ((i < n) && DomainElement::lessThan(min, max))
        {
          if (DomainElement::isNotEqual(min, last))
            hall[++nb].bounds = last = min;
          rank[minSortedDomains[i].second].min = nb;
          if (++i < n)
            min = minSortedDomains[i].first->lowerBound();
        }
        else
        {
          if (DomainElement::isNotEqual(max, last))
            hall[++nb].bounds = last = max;
          rank[maxSortedDomains[j].second].max = nb;
          if (++j == n)
            break;
          max = DomainElement::successor(maxSortedDomains[j].first->upperBound());
        }
      }
      hall[nb+1].bounds = DomainElement::successor(DomainElement::successor(hall[nb].bounds));
    }
    
    // Record the singletons that are caused by running this propagator
    std::vector<DomainElement*> singletonList;
    
    // Propagate lower bounds
    for (int i=nb+2; --i;)
    {
      hall[i].crit = hall[i].index = i-1;
      hall[i].diff = DomainElement::plus(hall[i].bounds, DomainElement::mirror(hall[i-1].bounds));
    }
    for (int i=0; i<n; i++) // visit intervals in increasing max order
    {
      int x = rank[maxSortedDomains[i].second].min;
      int z = pathmax_crit(hall, x+1);
      int j = hall[z].crit;
      {
        hall[z].diff = DomainElement::predecessor(hall[z].diff);
        if(DomainElement::isEqual(hall[z].diff, DomainElement::zero(hall[z].diff)))
          hall[z = pathmax_crit(hall, hall[z].crit=z+1)].crit = j;
      }
      pathset_crit(hall, x+1, z, z); // path compression
      int y = rank[maxSortedDomains[i].second].max;
      if (DomainElement::lessThan(hall[z].diff, DomainElement::plus(hall[z].bounds, DomainElement::mirror(hall[y].bounds))))
        return PropagationEvent::PROP_EVENT_FAIL;
      if (hall[x].index > x)
      {
        int w = pathmax_index(hall, hall[x].index);
        DomainElement* m = hall[w].bounds;
        
        setFilterOutputPtr(maxSortedDomains[i].first);
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
        setFilterInput(0, m);

        Domain *outDomain{ nullptr };
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        // If the domain has been reduced to singleton
        // and the singleton is not unique, return failure
        if(outDomain->getSize() == 1 &&
           !isUniqueSingleton(outDomain->lowerBound(), singletonList)) return PropagationEvent::PROP_EVENT_FAIL;
        
        pathset_index(hall, x, w, w); // path compression
      }
      if (DomainElement::isEqual(hall[z].diff, DomainElement::plus(hall[z].bounds, DomainElement::mirror(hall[y].bounds))))
      {
        pathset_index(hall, hall[y].index, j-1, y); // mark hall interval
        hall[y].index = j-1;
      }
    }
    
    // Propagate upper bounds
    for(int i=nb+1; i--;)
    {
      hall[i].crit = hall[i].index = i+1;
      hall[i].diff = DomainElement::plus(hall[i+1].bounds, DomainElement::mirror(hall[i].bounds));
    }
    for(int i=static_cast<int>(n); --i>=0; ) // visit intervals in decreasing min order
    {
      int x = rank[minSortedDomains[i].second].max;
      int z = pathmin_crit(hall, x-1);
      int j = hall[z].crit;
      {
        hall[z].diff = DomainElement::predecessor(hall[z].diff);
        if(DomainElement::isEqual(hall[z].diff, DomainElement::zero(hall[z].diff)))
          hall[z = pathmin_crit(hall, hall[z].crit=z-1)].crit = j;
      }
      pathset_crit(hall, x-1, z, z); // path compression
      int y = rank[minSortedDomains[i].second].min;

      // This check is only useful if some domain doesn't contain all the values
      // between its upper and lower bound.
      // In this case, propagation on lower bounds might shrink one of these domains
      // and this check becomes meaningful.
      if (DomainElement::lessThan(hall[z].diff, DomainElement::plus(hall[y].bounds, DomainElement::mirror(hall[z].bounds))))
        return PropagationEvent::PROP_EVENT_FAIL;
      if(hall[x].index < x)
      {
        int w = pathmin_index(hall, hall[x].index);
        DomainElement* m = DomainElement::predecessor(hall[w].bounds);
        
        setFilterOutputPtr(minSortedDomains[i].first);
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
        setFilterInput(0, m);
        
        Domain *outDomain{ nullptr };
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        // If the domain has been reduced to singleton
        // and the singleton is not unique, return failure
        if(outDomain->getSize() == 1 &&
           !isUniqueSingleton(outDomain->lowerBound(), singletonList)) return PropagationEvent::PROP_EVENT_FAIL;

        pathset_index(hall, x, w, w); // path compression
      }
      if(DomainElement::isEqual(hall[z].diff, DomainElement::plus(hall[y].bounds, DomainElement::mirror(hall[z].bounds))))
      {
        pathset_index(hall, hall[y].index, j+1, y); // mark hall interval
        hall[y].index = j+1;
      }
    }
    
    // All domains are singleton -> the constraint is subsumed
    if(isArraySingleton()) return PropagationEvent::PROP_EVENT_SUBSUMED;
    
    return PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagation
  
}// end namespace Core
