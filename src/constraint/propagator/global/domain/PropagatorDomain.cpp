// Base class
#include "PropagatorDomain.hpp"
#include "PropagatorArray.hpp"

// Utility
#include "PropagatorUtils.hpp"

#include "Domain.hpp"
#include "DomainElement.hpp"

#include <memory>
#include <cassert>

namespace Core {
  
  PropagatorDomain::PropagatorDomain(DomainArray& aDomArray,
                                     DomainElement* aLowerBound,
                                     DomainElement* aUpperBound)
  : PropagatorArray(PropagatorSemantics(2, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN,
                                        PropagationPriority::PROP_PRIORITY_LINEAR,
                                        PropagatorSemanticType::PROP_SEMANTIC_TYPE_DOMAIN,
                                        PropagatorSemanticClass::PROP_SEMANTIC_CLASS_GLOBAL), aDomArray)
  {
    for (std::size_t idx = 0; idx < aDomArray.size(); ++idx)
    {
      assert(!PropagatorUtils::isDomainEmpty(aDomArray[idx]));
      addContextDomain(idx, aDomArray[idx]);
    }
    
    pLB = aLowerBound;
    pUB = aUpperBound;
  }
  
  double PropagatorDomain::getFitness()
  {
    DomainElement* lowerBound = getLB();
    DomainElement* upperBound = getUB();
    
    Domain* dom{nullptr};
    
    auto fitnessVal = getMinFitness();
    for (size_t idx = 0; idx < arraySizeINT64(); ++idx)
    {
      dom = domainArrayAtINT64(idx);
      if (!PropagatorUtils::isDomainSingleton(dom))
      {
        return getUnspecFitness();
      }
      
      auto sing = PropagatorUtils::getSingleton(dom);
      if (sing->isLessThan(lowerBound))
      {
        fitnessVal += PropagatorUtils::toFitness(lowerBound->plus(sing->mirror()));
      }
      else if (upperBound->isLessThan(sing))
      {
        fitnessVal += PropagatorUtils::toFitness(sing->plus(upperBound->mirror()));
      }
    }
    assert(fitnessVal >= getMinFitness());
    
    return fitnessVal;
  }//getFitness
  
  PropagationEvent PropagatorDomain::post()
  {
    DomainElement* lowerBound = getLB();
    DomainElement* upperBound = getUB();
    
    Domain* dom{nullptr};
    for (size_t idx = 0; idx < arraySizeINT64(); ++idx)
    {
      dom = domainArrayAtINT64(idx);
      if (dom->upperBound()->isLessThan(lowerBound) ||
          upperBound->isLessThan(dom->lowerBound()))
      {
        return PropagationEvent::PROP_EVENT_FAIL;
      }
    }
    
    return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
  }// post
  
  PropagationEvent PropagatorDomain::runPropagation()
  {
    DomainElement* lowerBound = getLB();
    DomainElement* upperBound = getUB();
    
    Domain* dom{nullptr};
    for (size_t idx = 0; idx < arraySizeINT64(); ++idx)
    {
      dom = domainArrayAtINT64(idx);
      dom->shrink(lowerBound, upperBound);
      DOMAIN_FAILURE_CHECK(dom);
    }
    
    return PropagationEvent::PROP_EVENT_SUBSUMED;
  }// runPropagation
  
}// end namespace Core
