// Base class
#include "PropagatorElement.hpp"

// Domain iterator
#include "DomainIterator.hpp"

// Domain element
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"

// Domain event
#include "DomainEventSingleton.hpp"

// Filtering algorithm
#include "FilteringAlgorithmSUBTRACT.hpp"

// Utility
#include "PropagatorUtils.hpp"

#include <memory>
#include <cassert>

namespace Core {
  
  PropagatorElement::PropagatorElement(Domain *aDomIdx, DomainArray& aDomArray, Domain *aDom)
  : PropagatorArray(PropagatorSemantics(2, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN,
                                        PropagationPriority::PROP_PRIORITY_LINEAR,
                                        PropagatorSemanticType::PROP_SEMANTIC_TYPE_ELEMENT,
                                        PropagatorSemanticClass::PROP_SEMANTIC_CLASS_GLOBAL),
                    aDomArray)
  {
    assert(aDomIdx);
    assert(aDom);
    
    // Add context domains
    addContextDomain(D0, aDomIdx);
    addContextDomain(D1, aDom);
    
    assert(getIndexingDomain()->getDomainType() == DomainClass::DOM_INT64);
    
    // Register filters
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_SUBTRACT);
  }
  
  PropagatorElement::PropagatorElement(Domain *aDomIdx, DomainElementArray& aDomElemArray, Domain *aDom)
  : PropagatorArray(PropagatorSemantics(2,
                                        PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN,
                                        PropagationPriority::PROP_PRIORITY_LINEAR,
                                        PropagatorSemanticType::PROP_SEMANTIC_TYPE_ELEMENT,
                                        PropagatorSemanticClass::PROP_SEMANTIC_CLASS_GLOBAL),
                    aDomElemArray)
  {
    assert(aDomIdx);
    assert(aDom);
    
    // Add context domains
    addContextDomain(D0, aDomIdx);
    addContextDomain(D1, aDom);
    
    assert(getIndexingDomain()->getDomainType() == DomainClass::DOM_INT64);
    
    // Register filters
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_EQ);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_SUBTRACT);
  }
  
  double PropagatorElement::getFitness()
  {
    // check if all domains are singleton non-empty
    if (PropagatorUtils::isDomainEmpty(getIndexingDomain()) ||
        PropagatorUtils::isDomainEmpty(getAssignmentDomain()))
    {
      return getInvalidFitness();
    }
    
    if(!PropagatorUtils::isDomainSingleton(getIndexingDomain()) ||
       !PropagatorUtils::isDomainSingleton(getAssignmentDomain()))
    {
      return getUnspecFitness();
    }

    if (hasDomainArray())
    {
      for (INT_64 idx = 0; idx < arraySizeINT64(); ++idx)
      {
        Domain* dom = domainArrayAtINT64(idx);
        if (!PropagatorUtils::isDomainSingleton(dom))
        {
          return getUnspecFitness();
        }
        else if (PropagatorUtils::isDomainEmpty(dom))
        {
          return getInvalidFitness();
        }
      }
    }

    // element(I,[x1,x2,x3,...,xN],y)
    // if I>=1 /\ I<=N then return |xI - y|
    if(getIndexingDomain()->lowerBound()->zero()->successor()->isLessThanOrEqual(getIndexingDomain()->lowerBound()) &&
       getIndexingDomain()->lowerBound()->isLessThanOrEqual(arraySize()))
    {
      if(hasDomainArray())
      {
        DomainElement* xI = domainArrayAt(getIndexingDomain()->lowerBound()->predecessor())->lowerBound();
        DomainElement* y = getAssignmentDomain()->lowerBound();
        return PropagatorUtils::toFitness((xI->plus(y->mirror()))->abs());
      }
      else
      {
        assert(hasDomainElementArray());
        DomainElement* xI = domainElementArrayAt(getIndexingDomain()->lowerBound()->predecessor());
        DomainElement* y = getAssignmentDomain()->lowerBound();
        return PropagatorUtils::toFitness((xI->plus(y->mirror()))->abs());
      }
    }
    // element(I,[x1,x2,x3,...,xN],y)
    // if I<1 \/ I>N then return |I-j|+|xj-y| where j is s.t. the fitness value is minimum
    else
    {
      if(hasDomainArray())
      {
        DomainElement* j = getIndexingDomain()->lowerBound()->zero()->successor();
        DomainElement* xj = domainArrayAt(j->predecessor())->lowerBound();
        DomainElement* y = getAssignmentDomain()->lowerBound();
        DomainElement* firstSub = (getIndexingDomain()->lowerBound()->plus(j->mirror()))->abs();
        DomainElement* secondSub = (xj->plus(y->mirror()))->abs();
        DomainElement* min = firstSub->plus(secondSub);

        for (INT_64 idx = 1; idx < arraySizeINT64(); ++idx)
        {
          j = j->successor();
          xj = domainArrayAt(j->predecessor())->lowerBound();
          firstSub = (getIndexingDomain()->lowerBound()->plus(j->mirror()))->abs();
          secondSub = (xj->plus(y->mirror()))->abs();
          DomainElement* newVal = firstSub->plus(secondSub);
          if(newVal->isLessThan(min))
          {
            min = newVal;
          }
        }

        return PropagatorUtils::toFitness(min);
      }
      else
      {
        assert(hasDomainElementArray());

        DomainElement* j = getIndexingDomain()->lowerBound()->zero()->successor();
        DomainElement* xj = domainElementArrayAt(j->predecessor());
        DomainElement* y = getAssignmentDomain()->lowerBound();
        DomainElement* firstSub = (getIndexingDomain()->lowerBound()->plus(j->mirror()))->abs();
        DomainElement* secondSub = (xj->plus(y->mirror()))->abs();
        DomainElement* min = firstSub->plus(secondSub);

        for (INT_64 idx = 1; idx < arraySizeINT64(); ++idx)
        {
          j = j->successor();
          xj = domainElementArrayAt(j->predecessor());
          firstSub = (getIndexingDomain()->lowerBound()->plus(j->mirror()))->abs();
          secondSub = (xj->plus(y->mirror()))->abs();
          DomainElement* newVal = firstSub->plus(secondSub);
          if(newVal->isLessThan(min))
          {
            min = newVal;
          }
        }

        return PropagatorUtils::toFitness(min);
      }
      
    }
  }//getFitness
  
  PropagationEvent PropagatorElement::post()
  {
    // initial propagation on the bounds of the indexing domain
    getIndexingDomain()->shrink(DomainElementManager::getInstance().createDomainElementInt64(1), arraySize());
    DOMAIN_FAILURE_CHECK(getIndexingDomain());
    
    auto idxDom = getIndexingDomain();
    if (PropagatorUtils::isDomainSingleton(idxDom))
    {
      // Indexing domain "d0" is singleton,
      // propagate on assignment domain:
      // d1 = array[d0-1]
      setFilterOutputPtr(getAssignmentDomain());
      
      // Get the singleton domain element used to filter the output domain
      auto singleton = PropagatorUtils::getSingleton(idxDom);
      if (hasDomainArray())
      {
        DomainElement* elem = domainArrayAt(singleton->predecessor())->lowerBound();
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
        
        setFilterInput(0, elem);
        Domain *outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        elem = domainArrayAt(singleton->predecessor())->upperBound();
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
        
        setFilterInput(0, elem);
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
      }
      else
      {
        assert(hasDomainElementArray());
        DomainElement* elem = domainElementArrayAt(singleton->predecessor());
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_EQ);
        
        setFilterInput(0, elem);
        Domain *outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
      }
    }
    else if (PropagatorUtils::isDomainSingleton(getAssignmentDomain()))
    {
      // Propagate on indexing domain
      auto propagationEvent = filterIndexingDomain();
      if (propagationEvent == PropagationEvent::PROP_EVENT_FAIL)
      {
        return propagationEvent;
      }
    }
    
    return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
  }// post
  
  PropagationEvent PropagatorElement::runPropagation()
  {
    if (PropagatorUtils::isDomainSingleton(getIndexingDomain()))
    {
      if (PropagatorUtils::isDomainSingleton(getAssignmentDomain()))
      {
        // d0 and d1 are both singletons: check for consistency
        if (hasDomainArray())
        {
          auto propagationEvent = filterTableDomains();
          if (propagationEvent == PropagationEvent::PROP_EVENT_FAIL)
          {
            return propagationEvent;
          }
          
          if(PropagatorUtils::getSingleton(getAssignmentDomain())->isEqual(domainArrayAt((PropagatorUtils::getSingleton(getIndexingDomain()))->predecessor())->lowerBound()))
          {
            return PropagationEvent::PROP_EVENT_SUBSUMED;
          }
        }
        if (hasDomainElementArray() &&
            PropagatorUtils::getSingleton(getAssignmentDomain())->isEqual(domainElementArrayAt((PropagatorUtils::getSingleton(getIndexingDomain()))->predecessor())))
        {
          return PropagationEvent::PROP_EVENT_SUBSUMED;
        }
        return PropagationEvent::PROP_EVENT_FAIL;
      }
      
      // Propagate on assignment domain:
      // d1 = array[d0-1]
      if (hasDomainArray())
      {
        setFilterInput(0, domainArrayAt((PropagatorUtils::getSingleton(getIndexingDomain()))->predecessor())->lowerBound());
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
        setFilterOutputPtr(getAssignmentDomain());
        
        Domain *outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        setFilterInput(0, domainArrayAt((PropagatorUtils::getSingleton(getIndexingDomain()))->predecessor())->upperBound());
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
        setFilterOutputPtr(getAssignmentDomain());
        
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        if(PropagatorUtils::isDomainSingleton(getAssignmentDomain()))
          return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
      else
      {
        assert(hasDomainElementArray());
        setFilterInput(0, domainElementArrayAt((PropagatorUtils::getSingleton(getIndexingDomain()))->predecessor()));
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_EQ);
        setFilterOutputPtr(getAssignmentDomain());
        
        Domain *outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
      
      // Propagate on table domains
      if(hasDomainArray())
      {
        auto propagationEvent = filterTableDomains();
        if (propagationEvent == PropagationEvent::PROP_EVENT_FAIL)
        {
          return propagationEvent;
        }
      }
      return PropagationEvent::PROP_EVENT_FIXPOINT;
    }
    
    if (PropagatorUtils::isDomainSingleton(getAssignmentDomain()))
    {
      auto propagationEvent = filterIndexingDomain();
      if (propagationEvent == PropagationEvent::PROP_EVENT_FAIL)
      {
        return propagationEvent;
      }
      
      if (hasDomainArray())
      {
        propagationEvent = filterTableDomains();
        if (propagationEvent == PropagationEvent::PROP_EVENT_FAIL)
        {
          return propagationEvent;
        }
      }
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    
    /*
     * Neither the indexing domain nor the assignment domain
     * are singletons:
     * - indexing domain can be shrink to the indexes of the domains
     *   that are super-set of the assignment domain;
     * - assignment domain can be shrink to min of all lower bounds
     *   in the array and max of all upper bound in the array.
     */
    
    auto propagationEvent = filterIndexingDomain();
    if (propagationEvent == PropagationEvent::PROP_EVENT_FAIL)
    {
      return propagationEvent;
    }
    
    propagationEvent = filterAssignmentDomain();
    if (propagationEvent == PropagationEvent::PROP_EVENT_FAIL)
    {
      return propagationEvent;
    }
    
    if (hasDomainArray())
    {
      propagationEvent = filterTableDomains();
      if (propagationEvent == PropagationEvent::PROP_EVENT_FAIL)
      {
        return propagationEvent;
      }
    }
    
    if (PropagatorUtils::isDomainSingleton(getIndexingDomain()) &&
        PropagatorUtils::isDomainSingleton(getAssignmentDomain()))
    {
      if (hasDomainArray())
      {
        if (PropagatorUtils::isDomainSingleton(domainArrayAt((PropagatorUtils::getSingleton(getIndexingDomain()))->predecessor())))
        {
          return PropagationEvent::PROP_EVENT_SUBSUMED;
        }
      }
      else
      {
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
    }
    
    return PropagationEvent::PROP_EVENT_FIXPOINT;
  }// runPropagation
  
  PropagationEvent PropagatorElement::filterIndexingDomain()
  {
    /*
     * Propagate on indexing domain:
     * iterate through all the elements of the assignment domain,
     * i.e., all indexes, and save in another array B all the indexes i s.t.,
     * B[i] != d1.
     * The subtract B from array:
     * array = array - B.
     * so array will contain only the indexes corresponding to elements in array
     * that are equal to d1.
     */
    
    // Array of elements to subtract from indexing domain
    std::vector<DomainElement*> subDomainElementIdxArray;
    
    bool isAssignmentDomainSingleton = PropagatorUtils::isDomainSingleton(getAssignmentDomain());
    DomainElement* domainElementAssignedLb = getAssignmentDomain()->lowerBound();
    DomainElement* domainElementAssignedUb = getAssignmentDomain()->upperBound();
    assert(!isAssignmentDomainSingleton || (domainElementAssignedLb->isEqual(domainElementAssignedUb)));
    
    for (auto it = getIndexingDomain()->getIterator()->begin(); it != getIndexingDomain()->getIterator()->end(); ++it)
    {
      /*
       * Keep track of:
       * - Domain: singleton and not equal to assignment domain OR
       *           not singleton but not containing assignment domain;
       * - DomainElement: not equal to assignment domain.
       */
      
      bool idxToRemove = false;
      if (hasDomainArray())
      {
        Domain* domain = domainArrayAt((&*it)->predecessor());
        assert(domain);
        
        if (PropagatorUtils::isDomainSingleton(domain))
        {
          // Singleton, check if it is different from assignment domain or not contained in it
          idxToRemove =
          (isAssignmentDomainSingleton && PropagatorUtils::getSingleton(domain)->isNotEqual(domainElementAssignedLb)) ||
          (!isAssignmentDomainSingleton && !(getAssignmentDomain()->contains(PropagatorUtils::getSingleton(domain))));
        }
        else
        {
          // Not singleton, check if it cannot be shrink to the assignment domain
          idxToRemove =
          (isAssignmentDomainSingleton && !(domain->contains(domainElementAssignedLb))) ||
          (!isAssignmentDomainSingleton && (domain->upperBound()->isLessThan(domainElementAssignedLb) || domainElementAssignedUb->isLessThan(domain->lowerBound())));
        }
      }
      else
      {
        assert(hasDomainElementArray());
        DomainElement* domainElement = domainElementArrayAt((&*it)->predecessor());
        assert(domainElement);
        
        idxToRemove =
        (isAssignmentDomainSingleton && domainElement->isNotEqual(domainElementAssignedLb)) ||
        (!isAssignmentDomainSingleton && !(getAssignmentDomain()->contains(domainElement)));
      }
      
      if (idxToRemove)
      {
        subDomainElementIdxArray.push_back(&*it);
      }
    }
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_SUBTRACT);
    assert(FilteringAlgorithmSUBTRACT::isa(getCurrentFilterStrategy().get()));
    
    FilteringAlgorithmSUBTRACT::cast(getCurrentFilterStrategy().get())->setSubtractSetType(FilteringAlgorithmSUBTRACT::SubtractSetType::SS_DOMAIN_ELEMENT_ARRAY);
    DomainElementArraySPtr arrayNotSupported = std::make_shared<DomainElementArray>(subDomainElementIdxArray.size());
    for (std::size_t idx = 0; idx < subDomainElementIdxArray.size(); ++idx)
    {
      arrayNotSupported->assignElementToCell(idx, subDomainElementIdxArray[idx]);
    }
    if(arrayNotSupported->size() > 0)
    {
      setFilterInput(arrayNotSupported);
      setFilterOutputPtr(getIndexingDomain());
      applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(getIndexingDomain());
    }
    
    return PropagationEvent::PROP_EVENT_FIXPOINT;
  }// filterIndexingDomain
  
  PropagationEvent PropagatorElement::filterAssignmentDomain()
  {
    DomainElement* minAmongAll{ nullptr };
    DomainElement* maxAmongAll{ nullptr };
    for (auto it = getIndexingDomain()->getIterator()->begin(); it != getIndexingDomain()->getIterator()->end(); ++it)
    {
      DomainElement* currentLowerBound{ nullptr };
      DomainElement* currentUpperBound{ nullptr };
      if (hasDomainArray())
      {
        Domain* domain = domainArrayAt((&*it)->predecessor());
        assert(domain);
        currentLowerBound = domain->lowerBound();
        currentUpperBound = domain->upperBound();
      }
      else
      {
        assert(hasDomainElementArray());
        DomainElement* domainElement = domainElementArrayAt((&*it)->predecessor());
        assert(domainElement);
        currentLowerBound = domainElement;
        currentUpperBound = domainElement;
      }
      
      // Update min/max among all
      if (!minAmongAll || (minAmongAll && currentLowerBound->isLessThan(minAmongAll)))
      {
        minAmongAll = currentLowerBound;
      }
      
      if (!maxAmongAll || (maxAmongAll && maxAmongAll->isLessThan(currentUpperBound)))
      {
        maxAmongAll = currentUpperBound;
      }
    }
    assert(minAmongAll);
    assert(maxAmongAll);
    
    // Shrink assignment domain
    getAssignmentDomain()->shrink(minAmongAll, maxAmongAll);
    DOMAIN_FAILURE_CHECK(getAssignmentDomain());
    
    return PropagationEvent::PROP_EVENT_FIXPOINT;
  }//filterAssignmentDomain
  
  PropagationEvent  PropagatorElement::filterTableDomains()
  {
    DomainElement* assignmentLowerBound = getAssignmentDomain()->lowerBound();
    DomainElement* assignmentUpperBound = getAssignmentDomain()->upperBound();
    
    if(hasDomainArray())
    {
      for (auto it = getIndexingDomain()->getIterator()->begin(); it != getIndexingDomain()->getIterator()->end(); ++it)
      {
        domainArrayAt((&*it)->predecessor())->shrink(assignmentLowerBound, assignmentUpperBound);
        DOMAIN_FAILURE_CHECK(domainArrayAt((&*it)->predecessor()));
      }
    }
    else
    {
      for (auto it = getIndexingDomain()->getIterator()->begin(); it != getIndexingDomain()->getIterator()->end(); ++it)
      {
        if(DomainElement::lessThan(domainElementArrayAt((&*it)->predecessor()), assignmentLowerBound) ||
           DomainElement::lessThan(assignmentUpperBound, domainElementArrayAt((&*it)->predecessor())))
          return PropagationEvent::PROP_EVENT_FAIL;
      }
    }
    
    return PropagationEvent::PROP_EVENT_FIXPOINT;
  }//filterTableDomains
  
}// end namespace Core
