// Base class
#include "PropagatorBinary.hpp"

#include "PropagatorUtils.hpp"

#include <cassert>

namespace Core {
  
  PropagatorBinary::PropagatorBinary(Domain *aDom0, Domain *aDom1,
                                     PropagatorStrategyType aStrategyType,
                                     PropagatorSemanticType aSemanticType,
                                     PropagatorSemanticClass aSemanticClass,
                                     PropagationPriority aPriority)
  : Propagator(PropagatorSemantics(2, aStrategyType, aPriority, aSemanticType, aSemanticClass))
  {
    addContextDomain(D0, aDom0);
    addContextDomain(D1, aDom1);
  }
  
  PropagatorBinary::~PropagatorBinary()
  {
  }
  
  PropagatorBinary::BinaryDomainSignConfig PropagatorBinary::getBinaryDomainSignConfig() const
  {
    if (PropagatorUtils::isDomainNegative(getContextDomain(D0)))
    {
      if (PropagatorUtils::isDomainNegative(getContextDomain(D1)))
      {
        return PropagatorBinary::BinaryDomainSignConfig::BDSC_NN;
      }
      else if (PropagatorUtils::isDomainPositive(getContextDomain(D1)))
      {
        return PropagatorBinary::BinaryDomainSignConfig::BDSC_NP;
      }
      else
      {
        assert(PropagatorUtils::isDomainNegativePositive(getContextDomain(D1)));
        return PropagatorBinary::BinaryDomainSignConfig::BDSC_NX;
      }
    }
    else if (PropagatorUtils::isDomainPositive(getContextDomain(D0)))
    {
      if (PropagatorUtils::isDomainNegative(getContextDomain(D1)))
      {
        return PropagatorBinary::BinaryDomainSignConfig::BDSC_PN;
      }
      else if (PropagatorUtils::isDomainPositive(getContextDomain(D1)))
      {
        return PropagatorBinary::BinaryDomainSignConfig::BDSC_PP;
      }
      else
      {
        assert(PropagatorUtils::isDomainNegativePositive(getContextDomain(D1)));
        return PropagatorBinary::BinaryDomainSignConfig::BDSC_PX;
      }
    }
    else
    {
      assert(PropagatorUtils::isDomainNegativePositive(getContextDomain(D0)));
      if (PropagatorUtils::isDomainNegative(getContextDomain(D1)))
      {
        return PropagatorBinary::BinaryDomainSignConfig::BDSC_XN;
      }
      else if (PropagatorUtils::isDomainPositive(getContextDomain(D1)))
      {
        return PropagatorBinary::BinaryDomainSignConfig::BDSC_XP;
      }
      else
      {
        assert(PropagatorUtils::isDomainNegativePositive(getContextDomain(D1)));
        return PropagatorBinary::BinaryDomainSignConfig::BDSC_XX;
      }
    }
  }//getBinaryDomainSignConfig
  
}// end namespace Core
