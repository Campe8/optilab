#include "PropagatorUtils.hpp"

#include "Domain.hpp"
#include "DomainElement.hpp"
#include "DomainElementManager.hpp"
#include "DomainIterator.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementReal.hpp"

#include <unordered_set>
#include <cassert>

namespace Core { namespace PropagatorUtils {
  
  bool isDomainEmpty(const Domain *aDomain)
  {
    assert(aDomain);
    return aDomain->getSize() == 0;
  }//isDomainEmpty
  
  bool isDomainSingleton(const Domain *aDomain)
  {
    assert(aDomain);
    return aDomain->getSize() == 1;
  }//isDomainSingleton
  
  bool isDomainPositive(const Domain *aDomain)
  {
    assert(aDomain);
    if(isDomainEmpty(aDomain))
    {
      return false;
    }
    
    DomainElement *lowerBound = const_cast<Domain*>(aDomain)->lowerBound();
    return DomainElement::lessThan(lowerBound->zero(), lowerBound);
  }//isDomainPositive
  
  bool isDomainNegative(const Domain *aDomain)
  {
    assert(aDomain);
    if(isDomainEmpty(aDomain))
    {
      return false;
    }
    
    DomainElement *upperBound = const_cast<Domain*>(aDomain)->upperBound();
    return DomainElement::lessThan(upperBound, upperBound->zero());
  }//isDomainNegative
  
  bool isDomainNegativePositive(const Domain *aDomain)
  {
    assert(aDomain);
    if(isDomainEmpty(aDomain))
    {
      return false;
    }
    
    DomainElement *lowerBound = const_cast<Domain*>(aDomain)->lowerBound();
    DomainElement *upperBound = const_cast<Domain*>(aDomain)->upperBound();
    
    return
    DomainElement::lessThanOrEqual(lowerBound, lowerBound->zero()) &&
    DomainElement::lessThanOrEqual(lowerBound->zero(), upperBound);
  }//isDomainNegativePositive
  
  DomainElement* getSingleton(const Domain *aDomain)
  {
    assert(aDomain);
    if(isDomainEmpty(aDomain) || !isDomainSingleton(aDomain))
    {
      return DomainElementManager::getInstance().getVoidElement();
    }
    return const_cast<Domain*>(aDomain)->lowerBound();
  }//getSingleton
  
  DomainElementArraySPtr createDomainElementArraySPtr(std::vector<DomainElement*>&& aDomainElementArray)
  {
    DomainElementArraySPtr domElemArray = std::make_shared<DomainElementArray>(aDomainElementArray.size());
    
    std::size_t idx{ 0 };
    for (auto domElem : aDomainElementArray)
    {
      domElemArray->assignElementToCell(idx++, domElem);
    }
    
    return domElemArray;
  }//createDomainElementArraySPtr
  
  bool sameBounds(const Domain *aDomain1, const Domain* aDomain2)
  {
    assert(aDomain1);
    assert(aDomain2);
    return (const_cast<Domain*>(aDomain1)->lowerBound()->isEqual(const_cast<Domain*>(aDomain2)->lowerBound())) &&
    (const_cast<Domain*>(aDomain1)->upperBound()->isEqual(const_cast<Domain*>(aDomain2)->upperBound()));
  }//sameBounds
  
  bool sameDomains(const Domain *aDomain1, const Domain* aDomain2)
  {
    assert(aDomain1);
    assert(aDomain2);
    
    if (!sameBounds(aDomain1, aDomain2))
    {
      return false;
    }
    
    if (aDomain1->getSize() != aDomain2->getSize())
    {
      return false;
    }
    
    std::vector<DomainElement*> vectorOfDomainElements;
    DomainIterator *d1Iter = const_cast<Domain*>(aDomain1)->getIterator();
    for (auto d1It = d1Iter->begin(); d1It != d1Iter->end(); ++d1It)
    {
      if (!const_cast<Domain*>(aDomain2)->contains(&(*d1It)))
      {
        return false;
      }
    }
    
    return true;
  }//sameDomains
  
  DomainElementArraySPtr getDomainUnionArraySPtr(const Domain* aDomain1, const Domain* aDomain2)
  {
    assert(aDomain1);
    assert(aDomain2);
    
    std::unordered_set<DomainElementKey, DomainElementKeyHasher> tempSet;
    
    std::vector<DomainElement*> vectorOfDomainElements;
    DomainIterator *d1Iter = const_cast<Domain*>(aDomain1)->getIterator();
    for (auto d1It = d1Iter->begin(); d1It != d1Iter->end(); ++d1It)
    {
      vectorOfDomainElements.push_back(&(*d1It));
      tempSet.insert(*(*d1It).getDomainKey());
    }
    
    DomainIterator *d2Iter = const_cast<Domain*>(aDomain2)->getIterator();
    for (auto d2It = d2Iter->begin(); d2It != d2Iter->end(); ++d2It)
    {
      if(tempSet.find(*(*d2It).getDomainKey()) != tempSet.end())
      {
        continue;
      }
      vectorOfDomainElements.push_back(&(*d2It));
    }
    
    std::size_t idx{ 0 };
    DomainElementArraySPtr domElemArray = std::make_shared<DomainElementArray>(vectorOfDomainElements.size());
    for (auto domElemPtr : vectorOfDomainElements)
    {
      domElemArray->assignElementToCell(idx++, domElemPtr);
    }
    
    return domElemArray;
  }//getDomainUnion
  
  double toFitness(DomainElement* aElement)
  {
    assert(aElement);
    auto type = aElement->getType();
    switch (type)
    {
      case DomainElementType::DET_INT_64:
        return static_cast<double>(static_cast<DomainElementInt64*>(aElement)->getValue());
      default:
        assert(type == DomainElementType::DET_REAL);
        return static_cast<double>(static_cast<DomainElementReal*>(aElement)->getValue());
    }
  }//toFitness
  
}}// end namespace Core::PropagatorUtils
