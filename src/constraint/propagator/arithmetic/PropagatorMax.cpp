// Base class
#include "PropagatorMax.hpp"

// Utility
#include "PropagatorUtils.hpp"

// Specular domain
#include "DomainSpecular.hpp"
// Domain iterator
#include "DomainIterator.hpp"

// Internal propagators
#include "PropagatorEq.hpp"
#include "PropagatorLq.hpp"

// Intersect filtering algorithm
#include "FilteringAlgorithmINTERSECT.hpp"

#include <memory>
#include <cassert>
#include <algorithm>
#include <vector>

namespace Core {
  
  PropagatorMax::PropagatorMax(Domain *aDom0, Domain *aDom1, Domain *aDom2, PropagatorStrategyType aStrategyType, PropagationPriority aPropagatorPriority)
    : PropagatorTernary(aDom0, aDom1, aDom2, aStrategyType,
    PropagatorSemanticType::PROP_SEMANTIC_TYPE_MAX,
    PropagatorSemanticClass::PROP_SEMANTIC_CLASS_ARITHMETIC,
    aPropagatorPriority)
    , pUseInternalPropagator(false)
    , pInternalPropagatorType(PropagatorSemanticType::PROP_SEMANTIC_TYPE_UNDEF)
  {
    assert(aDom0);
    assert(aDom1);
    assert(aDom2);
    
    // Register filters
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
  }
  
  PropagatorMax::~PropagatorMax()
  {
  }
  
  double PropagatorMax::getFitness()
  {
    Domain* dom0 = getContextDomain(D0);
    Domain* dom1 = getContextDomain(D1);
    Domain* dom2 = getContextDomain(D2);
    
    // Check if the domains are empty or not singleton, if so return
    if (PropagatorUtils::isDomainEmpty(dom0) || PropagatorUtils::isDomainEmpty(dom1) ||
        PropagatorUtils::isDomainEmpty(dom2))
    {
      return getInvalidFitness();
    }
    
    if (!PropagatorUtils::isDomainSingleton(dom0) || !PropagatorUtils::isDomainSingleton(dom1) ||
        !PropagatorUtils::isDomainSingleton(dom2))
    {
      return getUnspecFitness();
    }

    // return |max(D0,D1)-D2|
    DomainElement* max = dom0->lowerBound()->max(dom1->lowerBound());
    return PropagatorUtils::toFitness((max->plus(dom2->lowerBound()->mirror()))->abs());
  }//getFitness
  
  PropagationEvent PropagatorMax::post()
  {
    Domain *outDomain{ nullptr };

    // D2 >= max(D0.LB, D1.LB)
    setFilterOutput(D2);
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
    setFilterInput(0, getContextDomain(D0)->lowerBound()->max(getContextDomain(D1)->lowerBound()));
    outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);

    // D2 <= max(D0.UB, D1.UB)
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
    setFilterInput(0, getContextDomain(D0)->upperBound()->max(getContextDomain(D1)->upperBound()));
    outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);

    PropagatorSPtr internalPropagator{ nullptr };
    if (PropagatorUtils::sameBounds(getContextDomain(D0), getContextDomain(D1)))
    {
      // D0 == D1 => D0 == D2
      pUseInternalPropagator = true;
      internalPropagator = std::make_shared<PropagatorEqBound>(getContextDomain(D0), getContextDomain(D2));
      pInternalPropagatorType = internalPropagator->getPropagatorTypeId();
      registerPropagator(internalPropagator);
      return postInternalPropagator(pInternalPropagatorType);
    }
    if (PropagatorUtils::sameBounds(getContextDomain(D0), getContextDomain(D2)))
    {
      // D0 == D2 => D1 <= D2
      pUseInternalPropagator = true;
      internalPropagator = std::make_shared<PropagatorLq>(getContextDomain(D1), getContextDomain(D2));
      pInternalPropagatorType = internalPropagator->getPropagatorTypeId();
      registerPropagator(internalPropagator);
      return postInternalPropagator(pInternalPropagatorType);
    }
    if (PropagatorUtils::sameBounds(getContextDomain(D1), getContextDomain(D2)))
    {
      // D1 == D2 => D0 <= D2
      pUseInternalPropagator = true;
      internalPropagator = std::make_shared<PropagatorLq>(getContextDomain(D0), getContextDomain(D2));
      pInternalPropagatorType = internalPropagator->getPropagatorTypeId();
      registerPropagator(internalPropagator);
      return postInternalPropagator(pInternalPropagatorType);
    }
    return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
  }//post
  
  PropagatorMaxBound::PropagatorMaxBound(Domain *aDom0, Domain *aDom1, Domain *aDom2)
    : PropagatorMax(aDom0, aDom1, aDom2, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, PropagationPriority::PROP_PRIORITY_TERNARY)
  {
  }

  PropagatorMaxBound::~PropagatorMaxBound()
  {
  }

  PropagationEvent PropagatorMaxBound::runPropagation()
  {
    if (useInternalPropagator())
    {
      return runInternalPropagator(getInternalPropagatorType());
    }

    Domain *outDomain{ nullptr };
    Domain* domD0 = getContextDomain(D0);
    Domain* domD1 = getContextDomain(D1);
    Domain* domD2 = getContextDomain(D2);

    // Iterate until fix point is reached
    bool eventChanged{ false };
    do
    {
      eventChanged = false;
      {
        // Propagate on D2
        setFilterOutputPtr(domD2);

        DomainEvent* preFilterEvent = domD2->getEvent();

        // D2 <= max(D0.UB, D1.UB)
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
        setFilterInput(0, domD0->upperBound()->max(domD1->upperBound()));
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);

        // Keep iterating until reaching the fix point
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));

        // D2 >= max(D0.LB, D1.LB)
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
        setFilterInput(0, domD0->lowerBound()->max(domD1->lowerBound()));
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);

        // Keep iterating until reaching the fix point
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
      }
      {
        // Propagate on D0
        setFilterOutputPtr(domD0);

        DomainEvent* preFilterEvent = domD0->getEvent();

        // D0 <= D2.UB
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
        setFilterInput(0, domD2->upperBound());
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);

        // Keep iterating until reaching the fix point
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
      }
      {
        // Propagate on D1
        setFilterOutputPtr(domD1);

        DomainEvent* preFilterEvent = domD1->getEvent();

        // D1 <= D2.UB
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
        setFilterInput(0, domD2->upperBound());
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);

        // Keep iterating until reaching the fix point
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
      }
    } while (eventChanged);

    auto numSingleton = numSingletonContextDomains();
    return (numSingleton == 3) ?
      PropagationEvent::PROP_EVENT_SUBSUMED : PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagation
  
  PropagatorMaxDomain::PropagatorMaxDomain(Domain *aDom0, Domain *aDom1, Domain *aDom2)
    : PropagatorMax(aDom0, aDom1, aDom2, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN, PropagationPriority::PROP_PRIORITY_LINEAR)
  {
    // Register filters
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_INTERSECT);
  }

  PropagatorMaxDomain::~PropagatorMaxDomain()
  {
  }

  PropagationEvent PropagatorMaxDomain::runPropagation()
  {
    // Domain propagation has the following steps
    // 1) D2 = intersect(union(D0, D1), D2)
    // 2) if D0 != D2 => post(D0 <= D2) /\ post(D1 <= D2)
    // 3) if D1 != D2 => post(D1 <= D2) /\ post(D0 <= D2)
    // 4) return FIXPOINT

    Domain* outDomain{ nullptr };
    Domain* domD0 = getContextDomain(D0);
    Domain* domD1 = getContextDomain(D1);
    Domain* domD2 = getContextDomain(D2);

    // 1) D2 = intersect(union(D0, D1), D2)
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_INTERSECT);
    assert(FilteringAlgorithmINTERSECT::isa(getCurrentFilterStrategy().get()));
    FilteringAlgorithmINTERSECT::cast(getCurrentFilterStrategy().get())->setIntersectionSetType(FilteringAlgorithmINTERSECT::IntersectionSetType::IS_DOMAIN_ELEMENT_ARRAY);

    setFilterOutputPtr(domD2);
    setFilterInput(PropagatorUtils::getDomainUnionArraySPtr(domD0, domD1));
    outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);

    PropagatorSPtr internalPropagator{ nullptr };

    // 2) if D0 != D2 => post(D0 <= D2) /\ propagate(D1 == D2)
    if (!PropagatorUtils::sameDomains(domD0, domD2))
    {
      internalPropagator = std::make_shared<PropagatorLq>(domD0, domD2);
      if (internalPropagator->post() == PropagationEvent::PROP_EVENT_FAIL)
      {
        return PropagationEvent::PROP_EVENT_FAIL;
      }

      internalPropagator = std::make_shared<PropagatorLq>(domD1, domD2);
      if (internalPropagator->post() == PropagationEvent::PROP_EVENT_FAIL)
      {
        return PropagationEvent::PROP_EVENT_FAIL;
      }
      return PropagationEvent::PROP_EVENT_FIXPOINT;
    }

    // 3) if D1 != D2 => post(D1 <= D2) /\ propagate(D0 == D2)
    if (!PropagatorUtils::sameDomains(domD1, domD2))
    {
      internalPropagator = std::make_shared<PropagatorLq>(domD1, domD2);
      if (internalPropagator->post() == PropagationEvent::PROP_EVENT_FAIL)
      {
        return PropagationEvent::PROP_EVENT_FAIL;
      }

      internalPropagator = std::make_shared<PropagatorLq>(domD0, domD2);
      if (internalPropagator->post() == PropagationEvent::PROP_EVENT_FAIL)
      {
        return PropagationEvent::PROP_EVENT_FAIL;
      }
      return PropagationEvent::PROP_EVENT_FIXPOINT;
    }

    return PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagation

}// end namespace Core
