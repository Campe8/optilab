// Base class
#include "PropagatorDiv.hpp"

// Utility
#include "PropagatorUtils.hpp"

// Specular domain
#include "DomainSpecular.hpp"
// Domain iterator
#include "DomainIterator.hpp"

#include "FilteringAlgorithmDIVIDE.hpp"
#include "FilteringAlgorithmSUBTRACT.hpp"

#include <memory>
#include <cassert>
#include <algorithm>
#include <vector>

namespace Core {
  
  PropagatorDiv::PropagatorDiv(Domain *aDom0, Domain *aDom1, Domain *aDom2)
  : PropagatorTernary(aDom0, aDom1, aDom2, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
                      PropagatorSemanticType::PROP_SEMANTIC_TYPE_DIV,
                      PropagatorSemanticClass::PROP_SEMANTIC_CLASS_ARITHMETIC,
                      PropagationPriority::PROP_PRIORITY_TERNARY)
  {
    assert(aDom0);
    assert(aDom1);
    assert(aDom2);
    
    // Register filters
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_NQ);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_DIVIDE);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GT);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_TIMES);
    
    
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_SUBTRACT);
  }
  
  PropagatorDiv::~PropagatorDiv()
  {
  }
  
  double PropagatorDiv::getFitness()
  {
    Domain* dom0 = getContextDomain(D0);
    Domain* dom1 = getContextDomain(D1);
    Domain* dom2 = getContextDomain(D2);
    
    // Check if the domains are empty or not singleton, if so return
    if (PropagatorUtils::isDomainEmpty(dom0) || PropagatorUtils::isDomainEmpty(dom1) ||
        PropagatorUtils::isDomainEmpty(dom2))
    {
      return getInvalidFitness();
    }
    
    if (!PropagatorUtils::isDomainSingleton(dom0) || !PropagatorUtils::isDomainSingleton(dom1) ||
        !PropagatorUtils::isDomainSingleton(dom2))
    {
      return getUnspecFitness();
    }

    // return |(D0/D1)-D2|
    DomainElement* div = dom0->lowerBound()->div(dom1->lowerBound());
    return PropagatorUtils::toFitness((div->plus(dom2->lowerBound()->mirror()))->abs());
  }//getFitness
  
  PropagationEvent PropagatorDiv::post()
  {
    // D1 must be != 0
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_NQ);
    setFilterOutput(D1);
    setFilterInput(0, getContextDomain(D1)->lowerBound()->zero());
    
    Domain *outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);
    
    // Run checks on singleton domains
    std::size_t singletonDomains = numSingletonContextDomains();
    if (singletonDomains > 1)
    {
      // Run easy propagation when there are 2 or more
      // domains in the current context which are singletons
      return runEasyPropagation(singletonDomains);
    }
    
    // Run bound propagation right after posting this propagator.
    // Consider check depending on domains' sign, see "runPropagation"
    TernaryDomainSignConfig contextSignConfig = getTernaryDomainSignConfig();
    if (contextSignConfig == TernaryDomainSignConfig::TDSC_PPN ||
        contextSignConfig == TernaryDomainSignConfig::TDSC_PNP ||
        contextSignConfig == TernaryDomainSignConfig::TDSC_NNN ||
        contextSignConfig == TernaryDomainSignConfig::TDSC_NPP)
    {
      return PropagationEvent::PROP_EVENT_FAIL;
    }
    else if (contextSignConfig == TernaryDomainSignConfig::TDSC_PPP ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_XPP ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_PPX ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_PXP)
    {
      return postOnPositiveDomains(getContextDomain(D0), getContextDomain(D1), getContextDomain(D2));
    }
    else if (contextSignConfig == TernaryDomainSignConfig::TDSC_PNN ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_XNN ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_PNX ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_PXN)
    {
      std::unique_ptr<DomainSpecular> d1SPec(new DomainSpecular(getContextDomain(D1)));
      std::unique_ptr<DomainSpecular> d2SPec(new DomainSpecular(getContextDomain(D2)));
      return postOnPositiveDomains(getContextDomain(D0), d1SPec.get(), d2SPec.get());
    }
    else if (contextSignConfig == TernaryDomainSignConfig::TDSC_NNP ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_XNP ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_NNX ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_NXP)
    {
      std::unique_ptr<DomainSpecular> d0SPec(new DomainSpecular(getContextDomain(D0)));
      std::unique_ptr<DomainSpecular> d1SPec(new DomainSpecular(getContextDomain(D1)));
      return postOnPositiveDomains(d0SPec.get(), d1SPec.get(), getContextDomain(D2));
    }
    else if (contextSignConfig == TernaryDomainSignConfig::TDSC_NPN ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_XPN ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_NPX ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_NXN)
    {
      std::unique_ptr<DomainSpecular> d0SPec(new DomainSpecular(getContextDomain(D0)));
      std::unique_ptr<DomainSpecular> d2SPec(new DomainSpecular(getContextDomain(D2)));
      return postOnPositiveDomains(d0SPec.get(), getContextDomain(D1), d2SPec.get());
    }
    return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
  }//post
  
  PropagationEvent PropagatorDiv::postOnPositiveDomains(Domain* aDom0, Domain* aDom1, Domain* aDom2)
  {
    assert(aDom0);
    assert(aDom1);
    assert(aDom2);
    
    // D0.LB > 0
    setFilterOutputPtr(aDom0);
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GT);
    setFilterInput(0, aDom0->lowerBound()->zero());
    Domain *outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);
    
    // D1.LB > 0
    setFilterOutputPtr(aDom1);
    outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);
    
    // D2.LB >= floor(D0.LB/D1.UB)
    setFilterOutputPtr(aDom2);
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
    aDom0->lowerBound()->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_FLOOR);
    setFilterInput(0, aDom0->lowerBound()->div(aDom1->upperBound()));
    outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);
    
    return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
  }//postOnPositiveDomains
  
  PropagationEvent PropagatorDiv::runPropagation()
  {
    // There are several cases to consider, based on
    // the configuration of the sign of the domains
    // of D0, D1, and D2 respectively
    TernaryDomainSignConfig contextSignConfig = getTernaryDomainSignConfig();
    
    if (contextSignConfig == TernaryDomainSignConfig::TDSC_PPN ||
        contextSignConfig == TernaryDomainSignConfig::TDSC_PNP ||
        contextSignConfig == TernaryDomainSignConfig::TDSC_NNN ||
        contextSignConfig == TernaryDomainSignConfig::TDSC_NPP)
    {
      // Non matching domain signs
      //              0    LB     UB
      // -------------|----|------|---- D0
      //              0    LB     UB
      // -------------|----|------|---- D1
      //   LB    UB   0
      //---|-----|----|---------------- D2
      // FAIL
      return PropagationEvent::PROP_EVENT_FAIL;
    }
    else if (contextSignConfig == TernaryDomainSignConfig::TDSC_PPP ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_XPP ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_PPX ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_PXP)
    {
      //              0    LB     UB
      // -------------|----|------|---- D0
      //              0    LB     UB
      // -------------|----|------|---- D1
      //              0    LB     UB
      // -------------|----|------|---- D2
      // or
      //   LB         0       UB
      //---|----------|-------|-------- D0
      //              0    LB     UB
      // -------------|----|------|---- D1
      //              0    LB     UB
      // -------------|----|------|---- D2
      // Propagate
      return runPropagatorDivOnPositiveDomains(getContextDomain(D0), getContextDomain(D1), getContextDomain(D2));
    }
    else if (contextSignConfig == TernaryDomainSignConfig::TDSC_PNN ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_XNN ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_PNX ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_PXN)
    {
      //              0    LB     UB
      // -------------|----|------|---- D0
      //   LB    UB   0
      //---|-----|----|---------------- D1
      //   LB    UB   0
      //---|-----|----|---------------- D2
      // or
      //   LB         0       UB
      //---|----------|-------|-------- D0
      //   LB    UB   0
      //---|-----|----|---------------- D1
      //   LB    UB   0
      //---|-----|----|---------------- D2
      // Propagate
      // Create specular domain for D1 and D2 and propagate
      // using propagator times
      std::unique_ptr<DomainSpecular> d1SPec(new DomainSpecular(getContextDomain(D1)));
      std::unique_ptr<DomainSpecular> d2SPec(new DomainSpecular(getContextDomain(D2)));
      return runPropagatorDivOnPositiveDomains(getContextDomain(D0), d1SPec.get(), d2SPec.get());
    }
    else if (contextSignConfig == TernaryDomainSignConfig::TDSC_NNP ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_XNP ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_NNX ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_NXP)
    {
      //   LB    UB   0
      //---|-----|----|---------------- D0
      //   LB    UB   0
      //---|-----|----|---------------- D1
      //              0    LB     UB
      // -------------|----|------|---- D2
      // or
      //   LB         0       UB
      //---|----------|-------|-------- D0
      //   LB    UB   0
      //---|-----|----|---------------- D1
      //              0    LB     UB
      // -------------|----|------|---- D2
      // Propagate
      // Create specular domain for D0 and D1 and propagate
      // using propagator times
      std::unique_ptr<DomainSpecular> d0SPec(new DomainSpecular(getContextDomain(D0)));
      std::unique_ptr<DomainSpecular> d1SPec(new DomainSpecular(getContextDomain(D1)));
      return runPropagatorDivOnPositiveDomains(d0SPec.get(), d1SPec.get(), getContextDomain(D2));
    }
    else if (contextSignConfig == TernaryDomainSignConfig::TDSC_NPN ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_XPN ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_NPX ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_NXN)
    {
      //   LB    UB   0
      //---|-----|----|---------------- D0
      //              0    LB     UB
      // -------------|----|------|---- D1
      //   LB    UB   0
      //---|-----|----|---------------- D2
      // or
      //   LB         0       UB
      //---|----------|-------|-------- D0
      //              0    LB     UB
      // -------------|----|------|---- D1
      //   LB    UB   0
      //---|-----|----|---------------- D2
      // Propagate
      // Create specular domain for D0 and D2 and propagate
      // using propagator times
      // @note that UB of D0 is greater than 0, this case handle in particular the
      // case when UB of D0 is exactly 0 (when greater than zero propagates less)
      std::unique_ptr<DomainSpecular> d0SPec(new DomainSpecular(getContextDomain(D0)));
      std::unique_ptr<DomainSpecular> d2SPec(new DomainSpecular(getContextDomain(D2)));
      return runPropagatorDivOnPositiveDomains(d0SPec.get(), getContextDomain(D1), d2SPec.get());
    }
    else if (contextSignConfig == TernaryDomainSignConfig::TDSC_PXX ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_NXX ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_XXX)
    {
      assert(PropagatorUtils::isDomainNegativePositive(getContextDomain(D1)));
      assert(PropagatorUtils::isDomainNegativePositive(getContextDomain(D2)));
      
      Domain* domD1 = getContextDomain(D1);
      Domain* domD2 = getContextDomain(D2);
      
      setFilterOutput(D0);
      
      // D0.UB <= max( ((D1.UB * (D2.UB + 1)) - 1), ((D1.LB * (D2.LB - 1)) - 1) )
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
      
      // (D1.UB * (D2.UB + 1)) - 1
      DomainElement *D1UBD2UB = (domD1->upperBound()->times(domD2->upperBound()->successor()))->predecessor();
      // (D1.LB * (D2.LB - 1)) - 1
      DomainElement *D1LBD2LB = (domD1->lowerBound()->times(domD2->lowerBound()->predecessor()))->predecessor();
      setFilterInput(0, D1UBD2UB->max(D1LBD2LB));
      
      Domain *outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      // D0.LB >= min( (D1.LB * (D2.UB + 1)), (D1.UB * (D2.LB - 1)) )
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
      
      // D1.LB * (D2.UB + 1)
      DomainElement *D1LBD2UB = domD1->lowerBound()->times(domD2->upperBound()->successor());
      // D1.UB * (D2.LB - 1)
      DomainElement *D1UBD2LB = domD1->upperBound()->times(domD2->lowerBound()->predecessor());
      setFilterInput(0, D1LBD2UB->min(D1UBD2LB));
      
      outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      return PropagationEvent::PROP_EVENT_NO_FIXPOINT;
    }
    else if (contextSignConfig == TernaryDomainSignConfig::TDSC_XPX)
    {
      //   LB         0       UB
      //---|----------|-------|-------- D0
      //              0    LB     UB
      // -------------|----|------|---- D1
      //   LB         0       UB
      //---|----------|-------|-------- D2
      
      Domain* outDomain{ nullptr };
      DomainElement *bound{ nullptr };
      Domain* domD0 = getContextDomain(D0);
      Domain* domD1 = getContextDomain(D1);
      Domain* domD2 = getContextDomain(D2);
      
      // D0.UB <= D1.UB * (D2.UB + 1)
      setFilterOutput(D0);
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
      bound = domD1->upperBound()->times(domD2->upperBound()->successor());
      setFilterInput(0, bound);
      outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      // D0.LB >= D1.UB * (D2.UB - 1)
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
      bound = domD1->upperBound()->times(domD2->lowerBound()->predecessor());
      setFilterInput(0, bound);
      outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      if (PropagatorUtils::isDomainPositive(outDomain))
      {
        return runPropagatorDivOnPositiveDomains(domD0, domD1, domD2);
      }
      if (PropagatorUtils::isDomainNegative(outDomain))
      {
        std::unique_ptr<DomainSpecular> d0SPec(new DomainSpecular(domD0));
        std::unique_ptr<DomainSpecular> d2SPec(new DomainSpecular(domD2));
        return runPropagatorDivOnPositiveDomains(d0SPec.get(), domD1, d2SPec.get());
      }
      
      setFilterOutput(D2);
      
      // D2 <= floor(D0.UB / D1.LB)
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
      domD0->upperBound()->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_FLOOR);
      setFilterInput(0, domD0->upperBound()->div(domD1->lowerBound()));
      outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      // D2 >= floor(D0.LB, D1.LB)
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
      domD0->lowerBound()->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_FLOOR);
      setFilterInput(0, domD0->lowerBound()->div(domD1->lowerBound()));
      outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      if (PropagatorUtils::isDomainSingleton(domD0) && PropagatorUtils::isDomainSingleton(domD1))
      {
        return runEasyPropagation(2);
      }
      
      return PropagationEvent::PROP_EVENT_NO_FIXPOINT;
    }
    else if (contextSignConfig == TernaryDomainSignConfig::TDSC_XNX)
    {
      //   LB         0       UB
      //---|----------|-------|-------- D0
      //   LB    UB   0
      //---|-----|----|---------------- D1
      //   LB         0       UB
      //---|----------|-------|-------- D2
      
      Domain* outDomain{ nullptr };
      DomainElement *bound{ nullptr };
      Domain* domD0 = getContextDomain(D0);
      Domain* domD1 = getContextDomain(D1);
      Domain* domD2 = getContextDomain(D2);
      
      // D0 <= D1.LB * (D2.LB - 1)
      setFilterOutput(D0);
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
      bound = domD1->lowerBound()->times(domD2->lowerBound()->predecessor());
      setFilterInput(0, bound);
      outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      // D0 >= D1.LB * (D2.UB + 1)
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
      bound = domD1->lowerBound()->times(domD2->upperBound()->successor());
      setFilterInput(0, bound);
      outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      if (PropagatorUtils::isDomainPositive(outDomain))
      {
        // PNN case
        std::unique_ptr<DomainSpecular> d1SPec(new DomainSpecular(domD1));
        std::unique_ptr<DomainSpecular> d2SPec(new DomainSpecular(domD2));
        return runPropagatorDivOnPositiveDomains(domD0, d1SPec.get(), d2SPec.get());
      }
      if (PropagatorUtils::isDomainNegative(outDomain))
      {
        // NNP case
        std::unique_ptr<DomainSpecular> d0SPec(new DomainSpecular(domD0));
        std::unique_ptr<DomainSpecular> d1SPec(new DomainSpecular(domD1));
        return runPropagatorDivOnPositiveDomains(d0SPec.get(), d1SPec.get(), domD2);
      }
      
      setFilterOutput(D2);
      
      // D2 <= floor(D0.LB / D1.UB)
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
      domD0->lowerBound()->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_FLOOR);
      setFilterInput(0, domD0->lowerBound()->div(domD1->upperBound()));
      outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      // D2 >= floor(D0.UB, D1.UB)
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
      domD0->upperBound()->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_FLOOR);
      setFilterInput(0, domD0->upperBound()->div(domD1->upperBound()));
      outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      if (PropagatorUtils::isDomainSingleton(domD0) && PropagatorUtils::isDomainSingleton(domD1))
      {
        return runEasyPropagation(2);
      }
      
      return PropagationEvent::PROP_EVENT_NO_FIXPOINT;
    }
    else if (contextSignConfig == TernaryDomainSignConfig::TDSC_XXP)
    {
      //   LB         0       UB
      //---|----------|-------|-------- D0
      //   LB         0       UB
      //---|----------|-------|-------- D0
      //   LB         0       UB
      // -------------|----|------|---- D2
      
      Domain* outDomain{ nullptr };
      DomainElement *bound{ nullptr };
      Domain* domD0 = getContextDomain(D0);
      Domain* domD1 = getContextDomain(D1);
      Domain* domD2 = getContextDomain(D2);
      
      // D0 <= D1.UB * (D2.UB + 1)
      setFilterOutput(D0);
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
      bound = domD1->upperBound()->times(domD2->upperBound()->successor());
      setFilterInput(0, bound);
      outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      // D0 >= D1.LB * (D2.UB + 1)
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
      bound = domD1->lowerBound()->times(domD2->upperBound()->successor());
      setFilterInput(0, bound);
      outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      if (PropagatorUtils::isDomainPositive(outDomain))
      {
        // PPP case
        return runPropagatorDivOnPositiveDomains(domD0, domD1, domD2);
      }
      if (PropagatorUtils::isDomainNegative(outDomain))
      {
        // NNP case
        std::unique_ptr<DomainSpecular> d0SPec(new DomainSpecular(domD0));
        std::unique_ptr<DomainSpecular> d1SPec(new DomainSpecular(domD1));
        return runPropagatorDivOnPositiveDomains(d0SPec.get(), d1SPec.get(), domD2);
      }
      
      setFilterOutput(D1);
      
      // D1 <= floor(D0.UB / D2.LB)
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
      domD0->upperBound()->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_FLOOR);
      setFilterInput(0, domD0->upperBound()->div(domD2->lowerBound()));
      outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      // D1 >= ceil(D0.LB, D1.LB)
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
      domD0->lowerBound()->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_CEILING);
      setFilterInput(0, domD0->lowerBound()->div(domD2->lowerBound()));
      outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      if (PropagatorUtils::isDomainSingleton(domD0) && PropagatorUtils::isDomainSingleton(domD1))
      {
        return runEasyPropagation(2);
      }
      
      return PropagationEvent::PROP_EVENT_NO_FIXPOINT;
    }
    else
    {
      //   LB         0       UB
      //---|----------|-------|-------- D0
      //   LB         0       UB
      //---|----------|-------|-------- D0
      //   LB         0       UB
      //---|----------|-------|-------- D2
      
      assert(contextSignConfig == TernaryDomainSignConfig::TDSC_XXN);
      
      Domain* outDomain{ nullptr };
      DomainElement *bound{ nullptr };
      Domain* domD0 = getContextDomain(D0);
      Domain* domD1 = getContextDomain(D1);
      Domain* domD2 = getContextDomain(D2);
      
      // D0 <= D1.LB * (D2.LB - 1)
      setFilterOutput(D0);
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
      bound = domD1->lowerBound()->times(domD2->lowerBound()->predecessor());
      setFilterInput(0, bound);
      outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      // D0 >= D1.UB * (D2.LB - 1)
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
      bound = domD1->upperBound()->times(domD2->lowerBound()->predecessor());
      setFilterInput(0, bound);
      outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      if (PropagatorUtils::isDomainPositive(outDomain))
      {
        // PNN case
        std::unique_ptr<DomainSpecular> d1SPec(new DomainSpecular(domD1));
        std::unique_ptr<DomainSpecular> d2SPec(new DomainSpecular(domD2));
        return runPropagatorDivOnPositiveDomains(domD0, d1SPec.get(), d2SPec.get());
      }
      if (PropagatorUtils::isDomainNegative(outDomain))
      {
        // NPN case
        std::unique_ptr<DomainSpecular> d0SPec(new DomainSpecular(domD0));
        std::unique_ptr<DomainSpecular> d2SPec(new DomainSpecular(domD2));
        return runPropagatorDivOnPositiveDomains(d0SPec.get(), domD1, d2SPec.get());
      }
      
      // if (D2.UB + 1) != 0 => D1 <= ceil(D0.LB / (D2.UB + 1))
      setFilterOutput(D1);
      if (domD2->upperBound()->successor()->isNotEqual(domD2->upperBound()->zero()))
      {
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
        domD0->lowerBound()->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_CEILING);
        setFilterInput(0, domD0->lowerBound()->div(domD2->upperBound()->successor()));
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
      }
      
      
      // if (D2.UB + 1) != 0 => D1 >= ceil(D0.UB / (D2.UB + 1))
      if (domD2->upperBound()->successor()->isNotEqual(domD2->upperBound()->zero()))
      {
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
        domD0->upperBound()->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_CEILING);
        setFilterInput(0, domD0->upperBound()->div(domD2->upperBound()->successor()));
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
      }
      
      if (PropagatorUtils::isDomainSingleton(domD0) && PropagatorUtils::isDomainSingleton(domD1))
      {
        return runEasyPropagation(2);
      }
      
      return PropagationEvent::PROP_EVENT_NO_FIXPOINT;
    }
  }//runPropagation
  
  PropagationEvent PropagatorDiv::runEasyPropagation(std::size_t aNumSingletons)
  {
    assert(aNumSingletons >= 2 && aNumSingletons <= 3);
    Domain* filteredDomain{ nullptr };
    
    /*
     * Two or three domains are singleton, they can propagate
     * on the third non singleton domain for
     * D0 / D1 = D2
     */
    if ((aNumSingletons == 2) && !PropagatorUtils::isDomainSingleton(getContextDomain(D0)))
    {
      // D0 = D2 * D1
      filteredDomain = filterTIMESFromSingleton(D0, D1, D2);
    }
    else if ((aNumSingletons == 2) && !PropagatorUtils::isDomainSingleton(getContextDomain(D1)))
    {
      // D1 = D0 / D2
      filteredDomain = filterDIVIDEFromSingleton(D1, D0, D2);
    }
    else
    {
      // D2 = D0 / D1
      filteredDomain = filterDIVIDEFromSingleton(D2, D0, D1);
    }
    
    // If filteredDomain is empty after filtering, propagation failed,
    // otherwise this propagator is subsumed
    assert(filteredDomain);
    DOMAIN_FAILURE_CHECK(filteredDomain);
    
    return PropagationEvent::PROP_EVENT_SUBSUMED;
  }//runEasyPropagation
  
  Domain* PropagatorDiv::filterTIMESFromSingleton(std::size_t aD2, std::size_t aD0, std::size_t aD1)
  {
    // Set aD2 as output domain, aD0 and aD1 singleton
    // domain elements for filter TIMES
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_TIMES);
    setFilterOutput(aD2);
    setFilterInput(0, PropagatorUtils::getSingleton(getContextDomain(aD0)));
    setFilterInput(1, PropagatorUtils::getSingleton(getContextDomain(aD1)));
    
    // Apply filtering algorithm on D2
    return  applyFilteringStrategy();
  }//filterTIMESFromSingleton
  
  Domain* PropagatorDiv::filterDIVIDEFromSingleton(std::size_t aD2, std::size_t aD0, std::size_t aD1)
  {
    // Set aD2 as output domain, aD0 and aD1 singleton
    // domain elements for filter DIVIDE
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_DIVIDE);
    auto filterDivide = getCurrentFilterStrategy();
    assert(FilteringAlgorithmDIVIDE::isa(filterDivide.get()));
    
    auto castFilterDiv = FilteringAlgorithmDIVIDE::cast(filterDivide.get());
    auto oldRoundingMode = castFilterDiv->getDivisionRoundingMode();
    castFilterDiv->setDivisionRoundingMode(FilteringAlgorithmDIVIDE::DivisionRoundingMode::DIV_ROUNDING_FLOOR);
    
    setFilterOutput(aD2);
    setFilterInput(0, PropagatorUtils::getSingleton(getContextDomain(aD0)));
    setFilterInput(1, PropagatorUtils::getSingleton(getContextDomain(aD1)));
    
    // Apply filtering algorithm on D2
    auto filteredDomain = applyFilteringStrategy();
    
    // Reset rounding mode
    castFilterDiv->setDivisionRoundingMode(oldRoundingMode);
    
    return filteredDomain;
  }//filterDIVIDEFromSingleton
  
  PropagationEvent PropagatorDiv::runPropagatorDivOnPositiveDomains(Domain* aDom0, Domain* aDom1, Domain* aDom2)
  {
    assert(aDom0);
    assert(aDom1);
    assert(aDom2);
    
    if(!PropagatorUtils::isDomainPositive(aDom0) ||
       !PropagatorUtils::isDomainPositive(aDom1) ||
       PropagatorUtils::isDomainNegative(aDom2))
    {
      postOnPositiveDomains(aDom0, aDom1, aDom2);
      assert(PropagatorUtils::isDomainPositive(aDom0)  &&
             PropagatorUtils::isDomainPositive(aDom1)  &&
             !PropagatorUtils::isDomainNegative(aDom2));
    }
    
    // Iterate until fix point is reached
    bool eventChanged;
    do
    {
      eventChanged = false;
      {
        // Propagate on D2
        setFilterOutputPtr(aDom2);
        
        DomainEvent* preFilterEvent = aDom2->getEvent();
        
        // D2 <= floor(D0.UB / D1.LB)
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
        aDom0->upperBound()->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_FLOOR);
        setFilterInput(0, aDom0->upperBound()->div(aDom1->lowerBound()));
        Domain *outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        // Keep iterating until reaching the fix point
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
        
        // D2 >= floor(D0.LB / D1.UB)
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
        aDom0->lowerBound()->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_FLOOR);
        setFilterInput(0, aDom0->lowerBound()->div(aDom1->upperBound()));
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        // Keep iterating until reaching the fix point
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
      }
      {
        // Propagate on D0
        setFilterOutputPtr(aDom0);
        
        DomainEvent* preFilterEvent = aDom0->getEvent();
        
        // D0 <= D1.UB * (D2.UB + 1)
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
        setFilterInput(0, aDom1->upperBound()->times(aDom2->upperBound()->successor()));
        Domain *outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        // Keep iterating until reaching the fix point
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
        
        // D0 >= D1.LB * D2.LB
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
        setFilterInput(0, aDom1->lowerBound()->times(aDom2->lowerBound()));
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        // Keep iterating until reaching the fix point
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
      }
      {
        // Propagate on D1
        setFilterOutputPtr(aDom1);
        
        DomainEvent* preFilterEvent = aDom1->getEvent();
        
        if (aDom2->lowerBound()->zero()->isLessThan(aDom2->lowerBound()))
        {
          // D1 <= floor(D0.UB / D2.LB)
          setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
          aDom0->upperBound()->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_FLOOR);
          setFilterInput(0, aDom0->upperBound()->div(aDom2->lowerBound()));
          Domain *outDomain = applyFilteringStrategy();
          DOMAIN_FAILURE_CHECK(outDomain);
          
          // Keep iterating until reaching the fix point
          eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
        }
        
        // D1 >=  ceil(D0.LB / (D2.UB + 1))
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
        aDom0->upperBound()->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_CEILING);
        setFilterInput(0, aDom0->lowerBound()->div(aDom2->upperBound()->successor()));
        Domain *outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        // Keep iterating until reaching the fix point
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
      }
    } while (eventChanged);
    
    return PropagatorUtils::isDomainSingleton(aDom0) && PropagatorUtils::isDomainSingleton(aDom1) ?
    PropagationEvent::PROP_EVENT_SUBSUMED : PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagatorDivOnPositiveDomains
  
}// end namespace Core
