// Base class
#include "PropagatorPlus.hpp"

// Utility
#include "PropagatorUtils.hpp"

// Domain iterator
#include "DomainIterator.hpp"

#include "FilteringAlgorithmSUBTRACT.hpp"

#include <memory>
#include <cassert>
#include <algorithm>
#include <vector>

namespace Core {
  
  PropagatorPlus::PropagatorPlus(Domain *aDom0, Domain *aDom1, Domain *aDom2,
                                 PropagatorStrategyType aStrategyType,
                                 PropagationPriority aPriority)
  : PropagatorTernary(aDom0, aDom1, aDom2, aStrategyType,
                      PropagatorSemanticType::PROP_SEMANTIC_TYPE_PLUS,
                      PropagatorSemanticClass::PROP_SEMANTIC_CLASS_ARITHMETIC,
                      aPriority)
  {
    assert(aDom0);
    assert(aDom1);
    assert(aDom2);
    
    // Register filters
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_EQ);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
  }
  
  double PropagatorPlus::getFitness()
  {
    Domain* dom0 = getContextDomain(D0);
    Domain* dom1 = getContextDomain(D1);
    Domain* dom2 = getContextDomain(D2);
    
    // Check if the domains are empty or not singleton, if so return
    if (PropagatorUtils::isDomainEmpty(dom0) || PropagatorUtils::isDomainEmpty(dom1) ||
        PropagatorUtils::isDomainEmpty(dom2))
    {
      return getInvalidFitness();
    }
    
    if (!PropagatorUtils::isDomainSingleton(dom0) || !PropagatorUtils::isDomainSingleton(dom1) ||
        !PropagatorUtils::isDomainSingleton(dom2))
    {
      return getUnspecFitness();
    }

    // return |(D0+D1)-D2|
    DomainElement* sum = dom0->lowerBound()->plus(dom1->lowerBound());
    return PropagatorUtils::toFitness((sum->plus(dom2->lowerBound()->mirror()))->abs());
  }//getFitness
  
  PropagationEvent PropagatorPlus::post()
  {
    // Run checks on singleton domains
    std::size_t singletonDomains = numSingletonContextDomains();
    if (singletonDomains > 1)
    {
      // Run easy propagation when there are 2 or more
      // domains in the current context which are singletons
      return runEasyPropagation(singletonDomains);
    }
    
    // Run bound propagation right after posting this propagator.
    // Consider check depending on domains' sign, see "runPropagation"
    TernaryDomainSignConfig contextSignConfig = getTernaryDomainSignConfig();
    if (contextSignConfig == TernaryDomainSignConfig::TDSC_PPN ||
        contextSignConfig == TernaryDomainSignConfig::TDSC_NNP)
    {
      return PropagationEvent::PROP_EVENT_FAIL;
    }

    // D2 <= max(max(D0.LB * D1.LB, D0.LB * D1.UB), max(D0.UB * D1.LB, D0.UB * D1.UB)))
    Domain *outDomain{ nullptr };
    Domain *domD0 = getContextDomain(D0);
    Domain *domD1 = getContextDomain(D1);

    // Propagate on D2: D0.LB + D1.LB <= D2 <= D0.UB + D1.UB 
    setFilterOutput(D2);
   
    // D0.LB + D1.LB <= D2
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
    setFilterInput(0, domD0->lowerBound()->plus(domD1->lowerBound()));
    outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);

    // D2 <= D0.UB + D1.UB
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
    setFilterInput(0, domD0->upperBound()->plus(domD1->upperBound()));
    outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);

    return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
  }//post

  PropagationEvent PropagatorPlus::runPropagation()
  {
    // Run basic propagation on singleton domains
    std::size_t singletonDomains = numSingletonContextDomains();
    if (singletonDomains > 1)
    {
      // Run easy propagation when there are 2 or more
      // domains in the current context which are singletons
      return runEasyPropagation(singletonDomains);
    }
    else
    {
      // More than 1 domain is not singletons
      assert(singletonDomains <= 1);

      // There are several cases to consider, based on
      // the configuration of the sign of the domains
      // of D0, D1, and D2 respectively
      TernaryDomainSignConfig contextSignConfig = getTernaryDomainSignConfig();

      if (contextSignConfig == TernaryDomainSignConfig::TDSC_PPN ||
        contextSignConfig == TernaryDomainSignConfig::TDSC_NNP)
      {
        return PropagationEvent::PROP_EVENT_FAIL;
      }
    }

    return runPropagatorPlus(getContextDomain(D0), getContextDomain(D1), getContextDomain(D2));
  }//runPropagation
  
  PropagationEvent PropagatorPlus::runEasyPropagation(std::size_t aNumSingletons)
  {
    assert(aNumSingletons >= 2 && aNumSingletons <= 3);
    Domain* filteredDomain{ nullptr };
    
    /*
     * Two or three domains are singleton, they can propagate
     * on the third non singleton domain.
     */
    if ((aNumSingletons == 2) && !PropagatorUtils::isDomainSingleton(getContextDomain(D0)))
    {
      // D0 = D2 - D1
      setFilterOutput(D0);
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_EQ);
      setFilterInput(0, PropagatorUtils::getSingleton(getContextDomain(D2))->plus(PropagatorUtils::getSingleton(getContextDomain(D1))->mirror()));
      filteredDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(filteredDomain);
    }
    else if ((aNumSingletons == 2) && !PropagatorUtils::isDomainSingleton(getContextDomain(D1)))
    {
      // D1 = D2 - D0
      setFilterOutput(D1);
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_EQ);
      setFilterInput(0, PropagatorUtils::getSingleton(getContextDomain(D2))->plus(PropagatorUtils::getSingleton(getContextDomain(D0))->mirror()));
      filteredDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(filteredDomain);
    }
    else
    {
      // D2 = D0 + D1
      setFilterOutput(D2);
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_EQ);
      setFilterInput(0, PropagatorUtils::getSingleton(getContextDomain(D0))->plus(PropagatorUtils::getSingleton(getContextDomain(D1))));
      filteredDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(filteredDomain);
    }
    
    return PropagationEvent::PROP_EVENT_SUBSUMED;
  }//runEasyPropagation
  
  PropagatorPlusBound::PropagatorPlusBound(Domain *aDom0, Domain *aDom1, Domain *aDom2)
  : PropagatorPlus(aDom0, aDom1, aDom2, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND)
  {
  }
  
  PropagatorPlusBound::~PropagatorPlusBound()
  {
  }

  PropagationEvent PropagatorPlusBound::runPropagatorPlus(Domain* aDom0, Domain* aDom1, Domain* aDom2)
  {
    assert(aDom0);
    assert(aDom1);
    assert(aDom2);

    // Iterate until fix point is reached
    bool eventChanged;
    do
    {
      eventChanged = false;
      {
        // Propagate on D2
        setFilterOutputPtr(aDom2);

        DomainEvent* preFilterEvent = aDom2->getEvent();

        // D2 <= D0.UB + D1.UB
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
        setFilterInput(0, aDom0->upperBound()->plus(aDom1->upperBound()));

        Domain *outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);

        // Keep iterating until reaching the fix point
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));

        // D0.LB + D1.LB <= D2
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
        setFilterInput(0, aDom0->lowerBound()->plus(aDom1->lowerBound()));

        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);

        // Keep iterating until reaching the fix point
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
      }
      {
        // Propagate on D0
        setFilterOutputPtr(aDom0);

        DomainEvent* preFilterEvent = aDom0->getEvent();

        // D0 <= D2.UB - D1.LB
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
        setFilterInput(0, aDom2->upperBound()->plus(aDom1->lowerBound()->mirror()));

        Domain *outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);

        // Keep iterating until reaching the fix point
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));

        // D2.LB - D1.UB <= D0
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
        setFilterInput(0, aDom2->lowerBound()->plus(aDom1->upperBound()->mirror()));

        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);

        // Keep iterating until reaching the fix point
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
      }
      {
        // Propagate on D1
        setFilterOutputPtr(aDom1);

        DomainEvent* preFilterEvent = aDom1->getEvent();

        // D1 <= D2.UB - D0.LB
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
        setFilterInput(0, aDom2->upperBound()->plus(aDom0->lowerBound()->mirror()));

        Domain *outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);

        // Keep iterating until reaching the fix point
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));

        // D2.LB - D0.UB <= D1
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
        setFilterInput(0, aDom2->lowerBound()->plus(aDom0->upperBound()->mirror()));

        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);

        // Keep iterating until reaching the fix point
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
      }
    } while (eventChanged);

    return PropagatorUtils::isDomainSingleton(aDom0) && PropagatorUtils::isDomainSingleton(aDom1) ?
      PropagationEvent::PROP_EVENT_SUBSUMED : PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagatorPlus

  PropagatorPlusDomain::PropagatorPlusDomain(Domain *aDom0, Domain *aDom1, Domain *aDom2)
  : PropagatorPlus(aDom0, aDom1, aDom2, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN, PropagationPriority::PROP_PRIORITY_VERY_SLOW)
  {
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_SUBTRACT);
  }
  
  PropagatorPlusDomain::~PropagatorPlusDomain()
  {
  }
  
  PropagationEvent PropagatorPlusDomain::runPropagatorPlus(Domain* aDom0, Domain* aDom1, Domain* aDom2)
  {
    assert(aDom0);
    assert(aDom1);
    assert(aDom2);
    
    // Find if D0 + D1 has support in D2
    std::size_t idxD0{0};
    std::size_t idxD1{0};
    std::size_t idxD2{0};
    std::vector<bool> supportD0(aDom0->getSize(), false);
    std::vector<bool> supportD1(aDom1->getSize(), false);
    std::vector<bool> supportD2(aDom2->getSize(), false);
    
    DomainIterator* d0Iter = aDom0->getIterator();
    DomainIterator* d1Iter = aDom1->getIterator();
    DomainIterator* d2Iter = aDom2->getIterator();
    for(auto d0It = d0Iter->begin(); d0It != d0Iter->end(); ++d0It)
    {
      idxD1 = 0;
      for(auto d1It = d1Iter->begin(); d1It != d1Iter->end(); ++d1It)
      {
        // sum = D0[idxD0] + D1[idxD1]
        DomainElement *sum = d0It->plus(&(*d1It));
        
        idxD2 = 0;
        for(auto d2It = d2Iter->begin(); d2It != d2Iter->end(); ++d2It)
        {
          // Check if prod = D2[idxD2]
          if (d2It->isEqual(sum))
          {
            // If sum == D2[idxD2] there is support
            supportD0[idxD0] = true;
            supportD1[idxD1] = true;
            supportD2[idxD2] = true;
            break;
          }
          idxD2++;
        }
        idxD1++;
      }
      idxD0++;
    }
    
    std::size_t numFalseD0 = count_if(supportD0.begin(), supportD0.end(), PropagatorUtils::isFalse);
    std::size_t numFalseD1 = count_if(supportD1.begin(), supportD1.end(), PropagatorUtils::isFalse);
    std::size_t numFalseD2 = count_if(supportD2.begin(), supportD2.end(), PropagatorUtils::isFalse);
    
    // Remove elements that don't have a support
    DomainElementArraySPtr arrayNotSupportedD0 = std::make_shared<DomainElementArray>(numFalseD0);
    DomainElementArraySPtr arrayNotSupportedD1 = std::make_shared<DomainElementArray>(numFalseD1);
    DomainElementArraySPtr arrayNotSupportedD2 = std::make_shared<DomainElementArray>(numFalseD2);
    
    std::size_t notSupportIdx{0};
    if(numFalseD0 > 0)
    {
      idxD0 = 0;
      for(auto d0It = d0Iter->begin(); d0It != d0Iter->end(); ++d0It)
      {
        if(!supportD0[idxD0++])
        {
          arrayNotSupportedD0->assignElementToCell(notSupportIdx++, &(*d0It));
        }
      }
    }
    
    if(numFalseD1 > 0)
    {
      idxD1 = 0;
      notSupportIdx = 0;
      for(auto d1It = d1Iter->begin(); d1It != d1Iter->end(); ++d1It)
      {
        if(!supportD1[idxD1++])
        {
          arrayNotSupportedD1->assignElementToCell(notSupportIdx++, &(*d1It));
        }
      }
    }
    
    if(numFalseD2 > 0)
    {
      idxD2 = 0;
      notSupportIdx = 0;
      for(auto d2It = d2Iter->begin(); d2It != d2Iter->end(); ++d2It)
      {
        if(!supportD2[idxD2++])
        {
          arrayNotSupportedD2->assignElementToCell(notSupportIdx++, &(*d2It));
        }
      }
    }
    
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_SUBTRACT);
    assert(FilteringAlgorithmSUBTRACT::isa(getCurrentFilterStrategy().get()));
    FilteringAlgorithmSUBTRACT::cast(getCurrentFilterStrategy().get())->setSubtractSetType(FilteringAlgorithmSUBTRACT::SubtractSetType::SS_DOMAIN_ELEMENT_ARRAY);
    
    // Filter D0
    if(numFalseD0 > 0)
    {
      setFilterOutputPtr(aDom0);
      setFilterInput(arrayNotSupportedD0);
      applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(aDom0);
    }
    
    // Filter D1
    if(numFalseD1 > 0)
    {
      setFilterOutputPtr(aDom1);
      setFilterInput(arrayNotSupportedD1);
      applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(aDom1);
    }
    
    // Filter D2
    if(numFalseD2 > 0)
    {
      setFilterOutputPtr(aDom2);
      setFilterInput(arrayNotSupportedD2);
      applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(aDom2);
    }
    
    return PropagatorUtils::isDomainSingleton(aDom0) && PropagatorUtils::isDomainSingleton(aDom1) ?
    PropagationEvent::PROP_EVENT_SUBSUMED : PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagatorTimesOnPositiveDomains
    
}// end namespace Core
