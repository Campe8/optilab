// Base class
#include "PropagatorTimes.hpp"

// Utility
#include "PropagatorUtils.hpp"

// Specular domain
#include "DomainSpecular.hpp"
// Domain iterator
#include "DomainIterator.hpp"

#include "FilteringAlgorithmSUBTRACT.hpp"

#include <memory>
#include <cassert>
#include <algorithm>
#include <vector>

namespace Core {
  
  PropagatorTimes::PropagatorTimes(Domain *aDom0, Domain *aDom1, Domain *aDom2,
                                   PropagatorStrategyType aStrategyType,
                                   PropagationPriority aPriority)
  : PropagatorTernary(aDom0, aDom1, aDom2, aStrategyType,
                      PropagatorSemanticType::PROP_SEMANTIC_TYPE_TIMES,
                      PropagatorSemanticClass::PROP_SEMANTIC_CLASS_ARITHMETIC,
                      aPriority)
  {
    assert(aDom0);
    assert(aDom1);
    assert(aDom2);
    
    // Register filters
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_SUBTRACT);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_TIMES);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_DIVIDE);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GT);
  }
  
  PropagatorTimes::~PropagatorTimes()
  {
  }
  
  double PropagatorTimes::getFitness()
  {
    Domain* dom0 = getContextDomain(D0);
    Domain* dom1 = getContextDomain(D1);
    Domain* dom2 = getContextDomain(D2);
    
    // Check if the domains are empty or not singleton, if so return
    if (PropagatorUtils::isDomainEmpty(dom0) || PropagatorUtils::isDomainEmpty(dom1) ||
        PropagatorUtils::isDomainEmpty(dom2))
    {
      return getInvalidFitness();
    }
    
    if (!PropagatorUtils::isDomainSingleton(dom0) || !PropagatorUtils::isDomainSingleton(dom1) ||
        !PropagatorUtils::isDomainSingleton(dom2))
    {
      return getUnspecFitness();
    }

    // return |(D0*D1)-D2|
    DomainElement* product = dom0->lowerBound()->times(dom1->lowerBound());
    return PropagatorUtils::toFitness((product->plus(dom2->lowerBound()->mirror()))->abs());
  }//getFitness
  
  PropagationEvent PropagatorTimes::post()
  {
    // Run checks on singleton domains
    std::size_t singletonDomains = numSingletonContextDomains();
    if (singletonDomains > 1)
    {
      // Run easy propagation when there are 2 or more
      // domains in the current context which are singletons
      return runEasyPropagation(singletonDomains);
    }
    
    // Run bound propagation right after posting this propagator.
    // Consider check depending on domains' sign, see "runPropagation"
    TernaryDomainSignConfig contextSignConfig = getTernaryDomainSignConfig();
    if (contextSignConfig == TernaryDomainSignConfig::TDSC_PPN ||
        contextSignConfig == TernaryDomainSignConfig::TDSC_PNP ||
        contextSignConfig == TernaryDomainSignConfig::TDSC_NNN ||
        contextSignConfig == TernaryDomainSignConfig::TDSC_NPP)
    {
      return PropagationEvent::PROP_EVENT_FAIL;
    }
    else if (contextSignConfig == TernaryDomainSignConfig::TDSC_PPP ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_XPP ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_PPX ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_PXP)
    {
      return postOnPositiveDomains(getContextDomain(D0), getContextDomain(D1), getContextDomain(D2));
    }
    else if (contextSignConfig == TernaryDomainSignConfig::TDSC_PNN ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_XNN ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_PNX ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_PXN)
    {
      std::unique_ptr<DomainSpecular> d1SPec(new DomainSpecular(getContextDomain(D1)));
      std::unique_ptr<DomainSpecular> d2SPec(new DomainSpecular(getContextDomain(D2)));
      return postOnPositiveDomains(getContextDomain(D0), d1SPec.get(), d2SPec.get());
    }
    else if (contextSignConfig == TernaryDomainSignConfig::TDSC_NNP ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_XNP ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_NNX ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_NXP)
    {
      std::unique_ptr<DomainSpecular> d0SPec(new DomainSpecular(getContextDomain(D0)));
      std::unique_ptr<DomainSpecular> d1SPec(new DomainSpecular(getContextDomain(D1)));
      return postOnPositiveDomains(d0SPec.get(), d1SPec.get(), getContextDomain(D2));
    }
    else if (contextSignConfig == TernaryDomainSignConfig::TDSC_NPN ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_XPN ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_NPX ||
             contextSignConfig == TernaryDomainSignConfig::TDSC_NXN)
    {
      std::unique_ptr<DomainSpecular> d0SPec(new DomainSpecular(getContextDomain(D0)));
      std::unique_ptr<DomainSpecular> d2SPec(new DomainSpecular(getContextDomain(D2)));
      return postOnPositiveDomains(d0SPec.get(), getContextDomain(D1), d2SPec.get());
    }
    else
    {
      // min(min(D0.LB * D1.LB, D0.LB * D1.UB), min(D0.UB * D1.LB, D0.UB * D1.UB))) <= D2
      // D2<= max(max(D0.LB * D1.LB, D0.LB * D1.UB), max(D0.UB * D1.LB, D0.UB * D1.UB)))
      Domain *dom0 = getContextDomain(D0);
      Domain *dom1 = getContextDomain(D1);
      
      DomainElement *timesLBLB = dom0->lowerBound()->times(dom1->lowerBound());
      DomainElement *timesLBUB = dom0->lowerBound()->times(dom1->upperBound());
      DomainElement *timesUBLB = dom0->upperBound()->times(dom1->lowerBound());
      DomainElement *timesUBUB = dom0->upperBound()->times(dom1->upperBound());
      
      DomainElement *minLBLBLBUB = timesLBLB->min(timesLBUB);
      DomainElement *minUBLBUBUB = timesUBLB->min(timesUBUB);
      
      // Propagate on D2
      setFilterOutput(D2);
      
      // D0.LB * D1.UB <= D2
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
      setFilterInput(0, minLBLBLBUB->min(minUBLBUBUB));
      
      Domain *outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      DomainElement *maxLBLBLBUB = timesLBLB->max(timesLBUB);
      DomainElement *maxUBLBUBUB = timesUBLB->max(timesUBUB);
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
      setFilterInput(0, maxLBLBLBUB->max(maxUBLBUBUB));
      
      outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    }
  }//post
  
  PropagationEvent PropagatorTimes::postOnPositiveDomains(Domain* aDom0, Domain* aDom1, Domain* aDom2)
  {
    assert(aDom0);
    assert(aDom1);
    assert(aDom2);
    
    setFilterOutputPtr(aDom0);
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GT);
    setFilterInput(0, aDom0->lowerBound()->zero());
    Domain *outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);
    
    setFilterOutputPtr(aDom1);
    outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);
    
    // (aDom0.LB * aDom1.LB) <= aDom2
    setFilterOutputPtr(aDom2);
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
    setFilterInput(0, aDom0->lowerBound()->times(aDom1->lowerBound()));
    outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);
    
    // aDom2 <= (aDom0.UB * aDom1.UB)
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
    setFilterInput(0, aDom0->upperBound()->times(aDom1->upperBound()));
    outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);
    
    return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
  }//postOnPositiveDomains
  
  PropagationEvent PropagatorTimes::runPropagation()
  {
    // Run basic propagation on singleton domains
    std::size_t singletonDomains = numSingletonContextDomains();
    if (singletonDomains > 1)
    {
      // Run easy propagation when there are 2 or more
      // domains in the current context which are singletons
      return runEasyPropagation(singletonDomains);
    }
    else
    {
      // More than 1 domain is not singletons
      assert(singletonDomains <= 1);
      
      // There are several cases to consider, based on
      // the configuration of the sign of the domains
      // of D0, D1, and D2 respectively
      TernaryDomainSignConfig contextSignConfig = getTernaryDomainSignConfig();
      
      if (contextSignConfig == TernaryDomainSignConfig::TDSC_PPN ||
          contextSignConfig == TernaryDomainSignConfig::TDSC_PNP ||
          contextSignConfig == TernaryDomainSignConfig::TDSC_NNN ||
          contextSignConfig == TernaryDomainSignConfig::TDSC_NPP)
      {
        // Non matching domain signs
        //              0    LB     UB
        // -------------|----|------|---- D0
        //              0    LB     UB
        // -------------|----|------|---- D1
        //   LB    UB   0
        //---|-----|----|---------------- D2
        // FAIL
        return PropagationEvent::PROP_EVENT_FAIL;
      }
      else if (contextSignConfig == TernaryDomainSignConfig::TDSC_PPP ||
               contextSignConfig == TernaryDomainSignConfig::TDSC_XPP ||
               contextSignConfig == TernaryDomainSignConfig::TDSC_PPX ||
               contextSignConfig == TernaryDomainSignConfig::TDSC_PXP)
      {
        //              0    LB     UB
        // -------------|----|------|---- D0
        //              0    LB     UB
        // -------------|----|------|---- D1
        //              0    LB     UB
        // -------------|----|------|---- D2
        // or
        //   LB         0       UB
        //---|----------|-------|-------- D0
        //              0    LB     UB
        // -------------|----|------|---- D1
        //              0    LB     UB
        // -------------|----|------|---- D2
        // Propagate
        return runPropagatorTimesOnPositiveDomains(getContextDomain(D0), getContextDomain(D1), getContextDomain(D2));
      }
      else if (contextSignConfig == TernaryDomainSignConfig::TDSC_PNN ||
               contextSignConfig == TernaryDomainSignConfig::TDSC_XNN ||
               contextSignConfig == TernaryDomainSignConfig::TDSC_PNX ||
               contextSignConfig == TernaryDomainSignConfig::TDSC_PXN)
      {
        //              0    LB     UB
        // -------------|----|------|---- D0
        //   LB    UB   0
        //---|-----|----|---------------- D1
        //   LB    UB   0
        //---|-----|----|---------------- D2
        // or
        //   LB         0       UB
        //---|----------|-------|-------- D0
        //   LB    UB   0
        //---|-----|----|---------------- D1
        //   LB    UB   0
        //---|-----|----|---------------- D2
        // Propagate
        // Create specular domain for D1 and D2 and propagate
        // using propagator times
        std::unique_ptr<DomainSpecular> d1SPec(new DomainSpecular(getContextDomain(D1)));
        std::unique_ptr<DomainSpecular> d2SPec(new DomainSpecular(getContextDomain(D2)));
        return runPropagatorTimesOnPositiveDomains(getContextDomain(D0), d1SPec.get(), d2SPec.get());
      }
      else if (contextSignConfig == TernaryDomainSignConfig::TDSC_NNP ||
               contextSignConfig == TernaryDomainSignConfig::TDSC_XNP ||
               contextSignConfig == TernaryDomainSignConfig::TDSC_NNX ||
               contextSignConfig == TernaryDomainSignConfig::TDSC_NXP)
      {
        //   LB    UB   0
        //---|-----|----|---------------- D0
        //   LB    UB   0
        //---|-----|----|---------------- D1
        //              0    LB     UB
        // -------------|----|------|---- D2
        // or
        //   LB         0       UB
        //---|----------|-------|-------- D0
        //   LB    UB   0
        //---|-----|----|---------------- D1
        //              0    LB     UB
        // -------------|----|------|---- D2
        // Propagate
        // Create specular domain for D0 and D1 and propagate
        // using propagator times
        std::unique_ptr<DomainSpecular> d0SPec(new DomainSpecular(getContextDomain(D0)));
        std::unique_ptr<DomainSpecular> d1SPec(new DomainSpecular(getContextDomain(D1)));
        return runPropagatorTimesOnPositiveDomains(d0SPec.get(), d1SPec.get(), getContextDomain(D2));
      }
      else if (contextSignConfig == TernaryDomainSignConfig::TDSC_NPN ||
               contextSignConfig == TernaryDomainSignConfig::TDSC_XPN ||
               contextSignConfig == TernaryDomainSignConfig::TDSC_NPX ||
               contextSignConfig == TernaryDomainSignConfig::TDSC_NXN)
      {
        //   LB    UB   0
        //---|-----|----|---------------- D0
        //              0    LB     UB
        // -------------|----|------|---- D1
        //   LB    UB   0
        //---|-----|----|---------------- D2
        // or
        //   LB         0       UB
        //---|----------|-------|-------- D0
        //              0    LB     UB
        // -------------|----|------|---- D1
        //   LB    UB   0
        //---|-----|----|---------------- D2
        // Propagate
        // Create specular domain for D0 and D2 and propagate
        // using propagator times
        // @note that UB of D0 is greater than 0, this case handle in particular the
        // case when UB of D0 is exactly 0 (when greater than zero propagates less)
        std::unique_ptr<DomainSpecular> d0SPec(new DomainSpecular(getContextDomain(D0)));
        std::unique_ptr<DomainSpecular> d2SPec(new DomainSpecular(getContextDomain(D2)));
        return runPropagatorTimesOnPositiveDomains(d0SPec.get(), getContextDomain(D1), d2SPec.get());
      }
      else if (contextSignConfig == TernaryDomainSignConfig::TDSC_PXX ||
               contextSignConfig == TernaryDomainSignConfig::TDSC_XPX)
      {
        //              0    LB     UB
        // -------------|----|------|---- D0
        //   LB         0       UB
        //---|----------|-------|-------- D1
        //   LB         0       UB
        //---|----------|-------|-------- D2
        // D0.UB * D1.LB <= D2 <= D0.UB * D1.UB
        // or
        //   LB         0       UB
        //---|----------|-------|-------- D0
        //              0    LB     UB
        // -------------|----|------|---- D1
        //   LB         0       UB
        //---|----------|-------|-------- D2
        // Swap D0 and D1
        // D0.UB * D1.LB <= D2 <= D0.UB * D1.UB
        
        // To avoid duplicate code, handle here both PXX and XPX cases
        std::size_t D0mod = D0;
        std::size_t D1mod = D1;
        if (contextSignConfig == TernaryDomainSignConfig::TDSC_XPX)
        {
          // swap D0 and D1
          D0mod = D1;
          D1mod = D0;
        }
        
        // Propagate on D2
        setFilterOutput(D2);
        Domain *contextDomainD2 = getContextDomain(D2);
        
        // D0.UB * D1.LB <= D2
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
        setFilterInput(0, getContextDomain(D0mod)->upperBound()->times(getContextDomain(D1mod)->lowerBound()));
        
        Domain *outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        // D2 <= D0.UB * D1.UB
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
        setFilterInput(0, getContextDomain(D0mod)->upperBound()->times(getContextDomain(D1mod)->upperBound()));
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        // In order to reach the fix point, keep propagating
        if (PropagatorUtils::isDomainPositive(contextDomainD2))
        {
          // Case: TDSC_PXP == TDSC_PPP
          return runPropagatorTimesOnPositiveDomains(getContextDomain(D0mod), getContextDomain(D1mod), contextDomainD2);
        }
        if (PropagatorUtils::isDomainNegative(contextDomainD2))
        {
          // Case: TDSC_PXN == TDSC_PNN
          std::unique_ptr<DomainSpecular> d1SPec(new DomainSpecular(getContextDomain(D1mod)));
          std::unique_ptr<DomainSpecular> d2SPec(new DomainSpecular(contextDomainD2));
          return runPropagatorTimesOnPositiveDomains(getContextDomain(D0mod), d1SPec.get(), d2SPec.get());
        }
        
        // D2 neither positive nor negative: propagate on D1
        // ceil(D2.LB / D0.LB) <= D1 <= floor(D2.UB / D0.LB)
        setFilterOutput(D1mod);
        
        DomainElement::RoundingMode oldRM = contextDomainD2->lowerBound()->getRoundingMode();
        contextDomainD2->lowerBound()->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_CEILING);
        DomainElement *D2LBD0LB = contextDomainD2->lowerBound()->div(getContextDomain(D0mod)->lowerBound());
        contextDomainD2->lowerBound()->setRoundingMode(oldRM);
        
        oldRM = contextDomainD2->upperBound()->getRoundingMode();
        contextDomainD2->upperBound()->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_FLOOR);
        DomainElement *D2UBD0LB = contextDomainD2->upperBound()->div(getContextDomain(D0mod)->lowerBound());
        contextDomainD2->upperBound()->setRoundingMode(oldRM);
        
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
        setFilterInput(0, D2LBD0LB);
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
        setFilterInput(0, D2UBD0LB);
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        // After propagating on D1, D1 can be singleton, run easy propagation
        if (PropagatorUtils::isDomainSingleton(getContextDomain(D0mod)) &&
            PropagatorUtils::isDomainSingleton(getContextDomain(D1mod)))
        {
          return runEasyPropagation(numSingletonContextDomains());
        }
        
        // Propagation has run but not fixed point has been reached yet
        return PropagationEvent::PROP_EVENT_NO_FIXPOINT;
      }
      else if (contextSignConfig == TernaryDomainSignConfig::TDSC_NXX ||
               contextSignConfig == TernaryDomainSignConfig::TDSC_XNX)
      {
        //   LB    UB   0
        //---|-----|----|---------------- D0
        //   LB         0       UB
        //---|----------|-------|-------- D1
        //   LB         0       UB
        //---|----------|-------|-------- D2
        // D0.LB * D1.UB <= D2 <= D0.LB * D1.LB
        // or
        //   LB         0       UB
        //---|----------|-------|-------- D0
        //   LB    UB   0
        //---|-----|----|---------------- D1
        //   LB         0       UB
        //---|----------|-------|-------- D2
        // Swap D0 and D1
        // D0.LB * D1.UB <= D2 <= D0.LB * D1.LB
        
        // To avoid duplicate code, handle here both PXX and XPX cases
        std::size_t D0mod = D0;
        std::size_t D1mod = D1;
        if (contextSignConfig == TernaryDomainSignConfig::TDSC_XNX)
        {
          // swap D0 and D1
          D0mod = D1;
          D1mod = D0;
        }
        
        // Propagate on D2
        setFilterOutput(D2);
        Domain *contextDomainD2 = getContextDomain(D2);
        
        // D0.LB * D1.UB <= D2
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
        setFilterInput(0, getContextDomain(D0mod)->lowerBound()->times(getContextDomain(D1mod)->upperBound()));
        
        Domain *outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        // D2 <= D0.LB * D1.LB
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
        setFilterInput(0, getContextDomain(D0mod)->lowerBound()->times(getContextDomain(D1mod)->lowerBound()));
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        // In order to reach the fix point, keep propagating
        if (PropagatorUtils::isDomainPositive(getContextDomain(D2)))
        {
          // Case: TDSC_NXP == TDSC_NNP
          std::unique_ptr<DomainSpecular> d0SPec(new DomainSpecular(getContextDomain(D0mod)));
          std::unique_ptr<DomainSpecular> d1SPec(new DomainSpecular(getContextDomain(D1mod)));
          return runPropagatorTimesOnPositiveDomains(d0SPec.get(), d0SPec.get(), getContextDomain(D2));
        }
        if (PropagatorUtils::isDomainNegative(getContextDomain(D2)))
        {
          // Case: TDSC_NXN == TDSC_NPN
          std::unique_ptr<DomainSpecular> d0SPec(new DomainSpecular(getContextDomain(D0mod)));
          std::unique_ptr<DomainSpecular> d2SPec(new DomainSpecular(getContextDomain(D2)));
          return runPropagatorTimesOnPositiveDomains(d0SPec.get(), getContextDomain(D1mod), d2SPec.get());
        }
        
        // D2 neither positive nor negative: propagate on D1
        // ceil(D2.UB / D0.UB)->predecessor <= D1 <= floor(D2.LB / D0.UB)
        // (D2.UB / D0.UB)->predecessor <= D1 <= (D2.LB / D0.UB)
        setFilterOutput(D1mod);
        
        DomainElement::RoundingMode oldRM = contextDomainD2->upperBound()->getRoundingMode();
        contextDomainD2->upperBound()->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_CEILING);
        DomainElement *D2UBD0UB = contextDomainD2->upperBound()->div(getContextDomain(D0mod)->upperBound());
        contextDomainD2->upperBound()->setRoundingMode(oldRM);
        
        oldRM = contextDomainD2->lowerBound()->getRoundingMode();
        contextDomainD2->lowerBound()->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_FLOOR);
        DomainElement *D2LBD0UB = contextDomainD2->lowerBound()->div(getContextDomain(D0mod)->upperBound());
        contextDomainD2->lowerBound()->setRoundingMode(oldRM);
        
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
        setFilterInput(0, D2UBD0UB);
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
        setFilterInput(0, D2LBD0UB);
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        // After propagating on D1, D1 can be singleton, run easy propagation
        if (PropagatorUtils::isDomainSingleton(getContextDomain(D0mod)) &&
            PropagatorUtils::isDomainSingleton(getContextDomain(D1mod)))
        {
          return runEasyPropagation(numSingletonContextDomains());
        }
        
        // Propagation has run but not fixed point has been reached yet
        return PropagationEvent::PROP_EVENT_NO_FIXPOINT;
      }
      else
      {
        assert(contextSignConfig == TernaryDomainSignConfig::TDSC_XXP ||
               contextSignConfig == TernaryDomainSignConfig::TDSC_XXN ||
               contextSignConfig == TernaryDomainSignConfig::TDSC_XXX);
        
        //   LB         0       UB
        //---|----------|-------|-------- D0
        //   LB         0       UB
        //---|----------|-------|-------- D0
        //   LB         0       UB
        //---|----------|-------|-------- D2
        // min(D0.LB * D1.UB, D0.UB * D1.LB) <= D2 <= max(D0.LB * D1.LB, D0.UB * D1.UB)
        
        // Propagate on D2
        setFilterOutput(D2);
        
        Domain *dom0 = getContextDomain(D0);
        Domain *dom1 = getContextDomain(D1);
        
        // D2 <= max(D0.LB * D1.LB, D0.UB * D1.UB)
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
        
        DomainElement *timesLBLB = dom0->lowerBound()->times(dom1->lowerBound());
        DomainElement *timesUBUB = dom0->upperBound()->times(dom1->upperBound());
        
        setFilterInput(0, timesLBLB->max(timesUBUB));
        Domain *outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        //min(D0.LB * D1.UB, D0.UB * D1.LB) <= D2
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
        DomainElement *timesLBUB = dom0->lowerBound()->times(dom1->upperBound());
        DomainElement *timesUBLB = dom0->upperBound()->times(dom1->lowerBound());
        
        setFilterInput(0, timesLBUB->max(timesUBLB));
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        // if D0 is singleton it must be 0 and D2 must be 0
        if (PropagatorUtils::isDomainSingleton(dom0))
        {
          assert(PropagatorUtils::isDomainSingleton(outDomain));
          assert(PropagatorUtils::getSingleton(dom0)->isEqual(PropagatorUtils::getSingleton(outDomain)));
          assert(PropagatorUtils::getSingleton(dom0)->isEqual(PropagatorUtils::getSingleton(outDomain)->zero()));
          return PropagationEvent::PROP_EVENT_SUBSUMED;
        }
        
        // if D1 is singleton it must be 0 and D2 must be 0
        if (PropagatorUtils::isDomainSingleton(dom1))
        {
          assert(PropagatorUtils::isDomainSingleton(outDomain));
          assert(PropagatorUtils::getSingleton(dom1)->isEqual(PropagatorUtils::getSingleton(outDomain)));
          assert(PropagatorUtils::getSingleton(dom1)->isEqual(PropagatorUtils::getSingleton(outDomain)->zero()));
          return PropagationEvent::PROP_EVENT_SUBSUMED;
        }
        
        // Here no fix point has been reached yet
        return PropagationEvent::PROP_EVENT_NO_FIXPOINT;
      }
    }
  }//runPropagation
  
  PropagationEvent PropagatorTimes::runEasyPropagation(std::size_t aNumSingletons)
  {
    assert(aNumSingletons >= 2 && aNumSingletons <= 3);
    Domain* filteredDomain{ nullptr };
    
    /*
     * Two or three domains are singleton, they can propagate
     * on the third non singleton domain.
     */
    if ((aNumSingletons == 2) && !PropagatorUtils::isDomainSingleton(getContextDomain(D0)))
    {
      // D0 = D2 / D1
      filteredDomain = filterDIVIDEFromSingleton(D0, D2, D1);
    }
    else if ((aNumSingletons == 2) && !PropagatorUtils::isDomainSingleton(getContextDomain(D1)))
    {
      // D1 = D2 / D0
      filteredDomain = filterDIVIDEFromSingleton(D1, D2, D0);
    }
    else
    {
      // D2 = D0 * D1
      filteredDomain = filterTIMESFromSingleton(D2, D0, D1);
    }
    
    // If filteredDomain is empty after filtering, propagation failed,
    // otherwise this propagator is subsumed
    assert(filteredDomain);
    DOMAIN_FAILURE_CHECK(filteredDomain);
    
    return PropagationEvent::PROP_EVENT_SUBSUMED;
  }//runEasyPropagation
  
  Domain* PropagatorTimes::filterTIMESFromSingleton(std::size_t aD2, std::size_t aD0, std::size_t aD1)
  {
    // Set aD2 as output domain, aD0 and aD1 singleton
    // domain elements for filter TIMES
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_TIMES);
    setFilterOutput(aD2);
    setFilterInput(0, PropagatorUtils::getSingleton(getContextDomain(aD0)));
    setFilterInput(1, PropagatorUtils::getSingleton(getContextDomain(aD1)));
    
    // Apply filtering algorithm on D2
    return  applyFilteringStrategy();
  }//filterTIMESFromSingleton
  
  Domain* PropagatorTimes::filterDIVIDEFromSingleton(std::size_t aD2, std::size_t aD0, std::size_t aD1)
  {
    // Set aD2 as output domain, aD0 and aD1 singleton
    // domain elements for filter TIMES
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_DIVIDE);
    setFilterOutput(aD2);
    setFilterInput(0, PropagatorUtils::getSingleton(getContextDomain(aD0)));
    setFilterInput(1, PropagatorUtils::getSingleton(getContextDomain(aD1)));
    
    // Apply filtering algorithm on D2
    return  applyFilteringStrategy();
  }//filterDIVIDEFromSingleton
  
  PropagatorTimesBound::PropagatorTimesBound(Domain *aDom0, Domain *aDom1, Domain *aDom2)
  : PropagatorTimes(aDom0, aDom1, aDom2, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND)
  {
  }
  
  PropagatorTimesBound::~PropagatorTimesBound()
  {
  }
  
  PropagationEvent PropagatorTimesBound::runPropagatorTimesOnPositiveDomains(Domain* aDom0, Domain* aDom1, Domain* aDom2)
  {
    assert(aDom0);
    assert(aDom1);
    assert(aDom2);
    
    setFilterOutputPtr(aDom0);
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GT);
    setFilterInput(0, aDom0->lowerBound()->zero());
    Domain *outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);
    
    setFilterOutputPtr(aDom1);
    outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);

    setFilterOutputPtr(aDom2);
    outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);
    
    assert(PropagatorUtils::isDomainPositive(aDom0));
    assert(PropagatorUtils::isDomainPositive(aDom1));
    assert(PropagatorUtils::isDomainPositive(aDom2));
    
    // Iterate until fix point is reached
    bool eventChanged;
    do
    {
      eventChanged = false;
      {
        // Propagate on D2
        setFilterOutputPtr(aDom2);
        
        DomainEvent* preFilterEvent = aDom2->getEvent();
        
        // D2 <= D0.UB * D1.UB
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
        setFilterInput(0, aDom0->upperBound()->times(aDom1->upperBound()));
        
        Domain *outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        // Keep iterating until reaching the fix point
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));

        // D0.LB * D1.LB <= D2
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
        setFilterInput(0, aDom0->lowerBound()->times(aDom1->lowerBound()));
        
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        // Keep iterating until reaching the fix point
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
      }
      {
        // Propagate on D0
        setFilterOutputPtr(aDom0);
        
        DomainEvent* preFilterEvent = aDom0->getEvent();
        
        // D0 <= floor(D2.UB / D1.LB)
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
        DomainElement::RoundingMode oldRM = aDom2->upperBound()->getRoundingMode();
        
        aDom2->upperBound()->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_FLOOR);
        setFilterInput(0, aDom2->upperBound()->div(aDom1->lowerBound()));
        
        Domain *outDomain = applyFilteringStrategy();
        
        // Reset rounding mode
        aDom2->upperBound()->setRoundingMode(oldRM);
        DOMAIN_FAILURE_CHECK(outDomain);
        
        // Keep iterating until reaching the fix point
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
        
        // ceil(D2.LB / D1.UB) <= D0
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
        oldRM = aDom2->lowerBound()->getRoundingMode();
        
        aDom2->lowerBound()->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_CEILING);
        setFilterInput(0, aDom2->lowerBound()->div(aDom1->upperBound()));

        outDomain = applyFilteringStrategy();
        
        // Reset rounding mode
        aDom2->lowerBound()->setRoundingMode(oldRM);
        DOMAIN_FAILURE_CHECK(outDomain);
        
        // Keep iterating until reaching the fix point
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
      }
      {
        // Propagate on D1
        setFilterOutputPtr(aDom1);
        
        DomainEvent* preFilterEvent = aDom1->getEvent();
        
        // D1 <= floor(D2.UB / D0.LB)
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
        DomainElement::RoundingMode oldRM = aDom2->upperBound()->getRoundingMode();
        
        aDom2->upperBound()->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_FLOOR);
        setFilterInput(0, aDom2->upperBound()->div(aDom0->lowerBound()));
        
        Domain *outDomain = applyFilteringStrategy();
        
        // Reset rounding mode
        aDom2->upperBound()->setRoundingMode(oldRM);
        DOMAIN_FAILURE_CHECK(outDomain);
        
        // Keep iterating until reaching the fix point
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
        
        // ceil(D2.LB / D0.UB) <= D1
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
        oldRM = aDom2->lowerBound()->getRoundingMode();
        
        aDom2->lowerBound()->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_CEILING);
        setFilterInput(0, aDom2->lowerBound()->div(aDom0->upperBound()));
        
        outDomain = applyFilteringStrategy();
        
        // Reset rounding mode
        aDom2->lowerBound()->setRoundingMode(oldRM);
        DOMAIN_FAILURE_CHECK(outDomain);
        
        // Keep iterating until reaching the fix point
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
      }
    } while (eventChanged);
    
    return PropagatorUtils::isDomainSingleton(aDom0) && PropagatorUtils::isDomainSingleton(aDom1) ?
    PropagationEvent::PROP_EVENT_SUBSUMED : PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagatorTimesOnPositiveDomains
  
  PropagatorTimesDomain::PropagatorTimesDomain(Domain *aDom0, Domain *aDom1, Domain *aDom2)
  : PropagatorTimes(aDom0, aDom1, aDom2, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN, PropagationPriority::PROP_PRIORITY_VERY_SLOW)
  {
  }
  
  PropagatorTimesDomain::~PropagatorTimesDomain()
  {
  }
  
  PropagationEvent PropagatorTimesDomain::runPropagatorTimesOnPositiveDomains(Domain* aDom0, Domain* aDom1, Domain* aDom2)
  {
    assert(aDom0);
    assert(aDom1);
    assert(aDom2);

    setFilterOutputPtr(aDom0);
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GT);
    setFilterInput(0, aDom0->lowerBound()->zero());
    Domain *outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);
    
    setFilterOutputPtr(aDom1);
    outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);
    
    setFilterOutputPtr(aDom2);
    outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);
    
    assert(PropagatorUtils::isDomainPositive(aDom0));
    assert(PropagatorUtils::isDomainPositive(aDom1));
    assert(PropagatorUtils::isDomainPositive(aDom2));
    
    // Find if D0 * D1 has support in D2
    std::size_t idxD0{0};
    std::size_t idxD1{0};
    std::size_t idxD2{0};
    std::vector<bool> supportD0(aDom0->getSize(), false);
    std::vector<bool> supportD1(aDom1->getSize(), false);
    std::vector<bool> supportD2(aDom2->getSize(), false);
    
    DomainIterator* d0Iter = aDom0->getIterator();
    DomainIterator* d1Iter = aDom1->getIterator();
    DomainIterator* d2Iter = aDom2->getIterator();
    for(auto d0It = d0Iter->begin(); d0It != d0Iter->end(); ++d0It)
    {
      idxD1 = 0;
      for(auto d1It = d1Iter->begin(); d1It != d1Iter->end(); ++d1It)
      {
        // prod = D0[idxD0] * D1[idxD1]
        DomainElement *prod = d0It->times(&(*d1It));
        
        idxD2 = 0;
        for(auto d2It = d2Iter->begin(); d2It != d2Iter->end(); ++d2It)
        {
          // Check if prod = D2[idxD2]
          if(d2It->isEqual(prod))
          {
            // If prod == D2[idxD2] there is support
            supportD0[idxD0] = true;
            supportD1[idxD1] = true;
            supportD2[idxD2] = true;
            break;
          }
          idxD2++;
        }
        idxD1++;
      }
      idxD0++;
    }
    
    std::size_t numFalseD0 = count_if(supportD0.begin(), supportD0.end(), PropagatorUtils::isFalse);
    std::size_t numFalseD1 = count_if(supportD1.begin(), supportD1.end(), PropagatorUtils::isFalse);
    std::size_t numFalseD2 = count_if(supportD2.begin(), supportD2.end(), PropagatorUtils::isFalse);
    
    // Remove elements that don't have a support
    DomainElementArraySPtr arrayNotSupportedD0 = std::make_shared<DomainElementArray>(numFalseD0);
    DomainElementArraySPtr arrayNotSupportedD1 = std::make_shared<DomainElementArray>(numFalseD1);
    DomainElementArraySPtr arrayNotSupportedD2 = std::make_shared<DomainElementArray>(numFalseD2);
    
    std::size_t notSupportIdx{0};
    if(numFalseD0 > 0)
    {
      idxD0 = 0;
      for(auto d0It = d0Iter->begin(); d0It != d0Iter->end(); ++d0It)
      {
        if(!supportD0[idxD0++])
        {
          arrayNotSupportedD0->assignElementToCell(notSupportIdx++, &(*d0It));
        }
      }
    }
    
    if(numFalseD1 > 0)
    {
      idxD1 = 0;
      notSupportIdx = 0;
      for(auto d1It = d1Iter->begin(); d1It != d1Iter->end(); ++d1It)
      {
        if(!supportD1[idxD1++])
        {
          arrayNotSupportedD1->assignElementToCell(notSupportIdx++, &(*d1It));
        }
      }
    }
    
    if(numFalseD2 > 0)
    {
      idxD2 = 0;
      notSupportIdx = 0;
      for(auto d2It = d2Iter->begin(); d2It != d2Iter->end(); ++d2It)
      {
        if(!supportD2[idxD2++])
        {
          arrayNotSupportedD2->assignElementToCell(notSupportIdx++, &(*d2It));
        }
      }
    }
    
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_SUBTRACT);
    assert(FilteringAlgorithmSUBTRACT::isa(getCurrentFilterStrategy().get()));
    FilteringAlgorithmSUBTRACT::cast(getCurrentFilterStrategy().get())->setSubtractSetType(FilteringAlgorithmSUBTRACT::SubtractSetType::SS_DOMAIN_ELEMENT_ARRAY);
    
    // Filter D0
    if(numFalseD0 > 0)
    {
      setFilterOutputPtr(aDom0);
      setFilterInput(arrayNotSupportedD0);
      applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(aDom0);
    }
    
    // Filter D1
    if(numFalseD1 > 0)
    {
      setFilterOutputPtr(aDom1);
      setFilterInput(arrayNotSupportedD1);
      applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(aDom1);
    }
    
    // Filter D2
    if(numFalseD2 > 0)
    {
      setFilterOutputPtr(aDom2);
      setFilterInput(arrayNotSupportedD2);
      applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(aDom2);
    }
    
    return PropagatorUtils::isDomainSingleton(aDom0) && PropagatorUtils::isDomainSingleton(aDom1) ?
    PropagationEvent::PROP_EVENT_SUBSUMED : PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagatorTimesOnPositiveDomains
    
}// end namespace Core
