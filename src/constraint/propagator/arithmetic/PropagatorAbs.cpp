// Base class
#include "PropagatorAbs.hpp"
#include "PropagatorEq.hpp"

// Utility
#include "PropagatorUtils.hpp"

// Specular domain
#include "DomainSpecular.hpp"

// Intersect filter
#include "FilteringAlgorithmINTERSECT.hpp"

#include <memory>
#include <cassert>

namespace Core {
  
  PropagatorAbs::PropagatorAbs(Domain *aDom0, Domain *aDom1,
                               PropagatorStrategyType aStrategyType,
                               PropagationPriority aPriority)
  : PropagatorBinary(aDom0, aDom1, aStrategyType,
                     PropagatorSemanticType::PROP_SEMANTIC_TYPE_ABS,
                     PropagatorSemanticClass::PROP_SEMANTIC_CLASS_ARITHMETIC,
                     aPriority)
  , pUsePositivePropagation(false)
  , pInternalPropagatorType(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_EQ)
  , pPropagatorStrategy(aStrategyType)
  {
    assert(aDom0);
    assert(aDom1);
    
    // Register filters
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_EQ);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_INTERSECT);
  }
  
  PropagatorAbsDomain::PropagatorAbsDomain(Domain *aDom0, Domain *aDom1)
  : PropagatorAbs(aDom0, aDom1, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN)
  {
  }
  
  PropagatorAbsDomain::~PropagatorAbsDomain()
  {
  }
  
  PropagatorAbsBound::PropagatorAbsBound(Domain *aDom0, Domain *aDom1)
  : PropagatorAbs(aDom0, aDom1, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND)
  {
  }
  
  PropagatorAbsBound::~PropagatorAbsBound()
  {
  }
  
  PropagatorAbs::~PropagatorAbs()
  {
  }
  
  double PropagatorAbs::getFitness()
  {
    Domain* dom0 = getContextDomain(D0);
    Domain* dom1 = getContextDomain(D1);
    
    // Check if the domains are empty or not singleton, if so return
    if (PropagatorUtils::isDomainEmpty(dom0) || PropagatorUtils::isDomainEmpty(dom1))
    {
      return getInvalidFitness();
    }
    
    if (!PropagatorUtils::isDomainSingleton(dom0) || !PropagatorUtils::isDomainSingleton(dom1))
    {
      return getUnspecFitness();
    }

    // return ||D0|-D1|
    return PropagatorUtils::toFitness((dom0->lowerBound()->abs()->plus(dom1->lowerBound()->mirror()))->abs());
  }//getFitness
  
  PropagationEvent PropagatorAbs::post()
  {
    Domain *domD0{ getContextDomain(D0) };
    Domain *domD1{ getContextDomain(D1) };
    
    DomainElement *dom0LB = domD0->lowerBound();
    DomainElement *dom0UB = domD0->upperBound();
    
    assert(dom0LB);
    assert(dom0UB);
    if ((dom0LB->zero()->isLessThanOrEqual(dom0LB)) || (dom0UB->isLessThanOrEqual(dom0UB->zero())))
    {
      pUsePositivePropagation = true;
      
      PropagatorSPtr propagatorEq{ nullptr };
      if ((dom0UB->isLessThan(dom0UB->zero())))
      {
        // |D0| = D1
        // where
        // if
        // D0 >= 0
        // =>
        // D0 == D1
        // else
        // D0 < 0
        // =>
        // -D0 == D1
        pDomainSpecular.reset(new DomainSpecular(domD0));
        domD0 = pDomainSpecular.get();
      }
      if (pPropagatorStrategy == PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND)
      {
        propagatorEq = std::make_shared<PropagatorEqBound>(domD0, domD1);
      }
      else
      {
        assert(pPropagatorStrategy == PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN);
        propagatorEq = std::make_shared<PropagatorEqDomain>(domD0, domD1);
      }
      // Register internal propagator
      registerPropagator(propagatorEq);
      return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ);
    }
    else
    {
      assert(!PropagatorUtils::isDomainSingleton(domD0));
      
      // Propagate on D1
      setFilterOutputPtr(domD1);
      
      // D1.LB >= 0
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
      setFilterInput(0, dom0LB->zero());
      
      Domain *outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      if (PropagatorUtils::isDomainSingleton(domD1))
      {
        // If D1 is singleton -> D0 can be either D1.LB or -D1.LB
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_INTERSECT);
        setFilterOutput(D0);
        DomainElementArraySPtr domElemArray = PropagatorUtils::createDomainElementArraySPtr({ PropagatorUtils::getSingleton(domD1)->mirror(), PropagatorUtils::getSingleton(domD1) });
        
        
        FilteringAlgorithmSPtr currentFilter = getCurrentFilterStrategy();
        assert(FilteringAlgorithmINTERSECT::isa(currentFilter.get()));
        
        // Get current intersection type and set it as for domain element array
        auto oldIntersectionType = FilteringAlgorithmINTERSECT::cast(currentFilter.get())->getIntersectionSetType();
        FilteringAlgorithmINTERSECT::cast(currentFilter.get())->setIntersectionSetType(FilteringAlgorithmINTERSECT::IntersectionSetType::IS_DOMAIN_ELEMENT_ARRAY);
        
        // Apply filter
        setFilterInput(domElemArray);
        outDomain = applyFilteringStrategy();
        
        // Reset intersection type as it was before checks and possible return
        FilteringAlgorithmINTERSECT::cast(currentFilter.get())->setIntersectionSetType(oldIntersectionType);
        
        DOMAIN_FAILURE_CHECK(outDomain);
      }
      else
      {
        // D1 is not singleton => D1.UB <= max(-D0.LB, D0.UB)
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
        setFilterOutput(D1);
        
        DomainElement *newUpperBound = dom0UB->max(dom0LB->mirror());
        setFilterInput(0, newUpperBound);
        
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
      }
    }
    
    return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
  }//post
  
  PropagationEvent PropagatorAbs::runPropagation()
  {
    if (pUsePositivePropagation)
    {
      return runInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ);
    }
    
    Domain* domD0 = getContextDomain(D0);
    Domain* domD1 = getContextDomain(D1);
    
    DomainElement *dom0LB = domD0->lowerBound();
    DomainElement *dom0UB = domD0->upperBound();
    
    Domain* outDomain{ nullptr };
    
    bool eventChanged;
    do
    {
      // D0 singleton => propagate on D1
      if (PropagatorUtils::isDomainSingleton(domD0))
      {
        // D1 is not singleton => D1.UB <= max(-D0.LB, D0.UB)
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_EQ);
        setFilterOutput(D1);
        
        DomainElement *shrinkElem{ PropagatorUtils::getSingleton(domD0) };
        if (shrinkElem->isLessThan(shrinkElem->zero()))
        {
          shrinkElem = shrinkElem->mirror();
        }
        setFilterInput(0, shrinkElem);
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
      
      // D1 singleton => propagate on D0
      if (PropagatorUtils::isDomainSingleton(domD1))
      {
        if (domD0->lowerBound()->zero()->isLessThanOrEqual(domD0->lowerBound()))
        {
          // D0.LB >= 0
          setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_EQ);
          setFilterOutput(D0);
          setFilterInput(0, PropagatorUtils::getSingleton(domD1));
          outDomain = applyFilteringStrategy();
          DOMAIN_FAILURE_CHECK(outDomain);
          return PropagationEvent::PROP_EVENT_SUBSUMED;
        }
        else if (domD0->upperBound()->isLessThanOrEqual(domD0->upperBound()->zero()))
        {
          // D0.UB <= 0
          setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_EQ);
          setFilterOutput(D0);
          setFilterInput(0, PropagatorUtils::getSingleton(domD1)->mirror());
          outDomain = applyFilteringStrategy();
          DOMAIN_FAILURE_CHECK(outDomain);
          return PropagationEvent::PROP_EVENT_SUBSUMED;
        }
        else if (PropagatorUtils::getSingleton(domD1)->isEqual(domD1->lowerBound()->zero()))
        {
          // D1.LB == D1.UB == 0
          setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_EQ);
          setFilterOutput(D0);
          setFilterInput(0, domD1->lowerBound()->zero());
          outDomain = applyFilteringStrategy();
          DOMAIN_FAILURE_CHECK(outDomain);
          return PropagationEvent::PROP_EVENT_SUBSUMED;
        }
        else
        {
          // If D1 is singleton -> D0 can be either D1.LB or -D1.LB
          setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_INTERSECT);
          setFilterOutput(D0);
          DomainElementArraySPtr domElemArray = PropagatorUtils::createDomainElementArraySPtr({ PropagatorUtils::getSingleton(domD1)->mirror(), PropagatorUtils::getSingleton(domD1) });
          
          
          FilteringAlgorithmSPtr currentFilter = getCurrentFilterStrategy();
          assert(FilteringAlgorithmINTERSECT::isa(currentFilter.get()));
          
          // Get current intersection type and set it as for domain element array
          auto oldIntersectionType = FilteringAlgorithmINTERSECT::cast(currentFilter.get())->getIntersectionSetType();
          FilteringAlgorithmINTERSECT::cast(currentFilter.get())->setIntersectionSetType(FilteringAlgorithmINTERSECT::IntersectionSetType::IS_DOMAIN_ELEMENT_ARRAY);
          
          // Apply filter
          setFilterInput(domElemArray);
          outDomain = applyFilteringStrategy();
          
          // Reset intersection type as it was before checks and possible return
          FilteringAlgorithmINTERSECT::cast(currentFilter.get())->setIntersectionSetType(oldIntersectionType);
          
          DOMAIN_FAILURE_CHECK(outDomain);
          return PropagationEvent::PROP_EVENT_SUBSUMED;
        }
      }
      
      eventChanged = false;
      {
        // Propagate on D1
        setFilterOutput(D1);
        
        DomainEvent* preFilterEvent = domD1->getEvent();
        
        // D1 is not singleton => D1.UB <= max(-D0.LB, D0.UB)
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
        
        
        DomainElement *newUpperBound = dom0UB->max(dom0LB->mirror());
        setFilterInput(0, newUpperBound);
        
        
        
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
      }
      {
        // Propagate on D0
        setFilterOutput(D0);
        
        DomainEvent* preFilterEvent = domD0->getEvent();
        
        // D0 <= D1.UB
        setFilterInput(0, domD1->upperBound());
        
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
        
        // D0 >= -D1.UB
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
        setFilterInput(0, domD1->upperBound()->mirror());
        
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
      }
    } while (eventChanged);
    
    return PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagation
  
}// end namespace Core


