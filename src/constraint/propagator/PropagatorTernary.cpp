// Base class
#include "PropagatorTernary.hpp"

#include "PropagatorUtils.hpp"

#include <cassert>

namespace Core {
  
  PropagatorTernary::PropagatorTernary(Domain *aDom0, Domain *aDom1, Domain *aDom2,
                                       PropagatorStrategyType aStrategyType,
                                       PropagatorSemanticType aSemanticType,
                                       PropagatorSemanticClass aSemanticClass,
                                       PropagationPriority aPriority)
  : Propagator(PropagatorSemantics(3, aStrategyType, aPriority, aSemanticType, aSemanticClass))
  {
    addContextDomain(D0, aDom0);
    addContextDomain(D1, aDom1);
    addContextDomain(D2, aDom2);
  }
  
  PropagatorTernary::~PropagatorTernary()
  {
  }
  
  PropagatorTernary::TernaryDomainSignConfig PropagatorTernary::getTernaryDomainSignConfig() const
  {
    if (PropagatorUtils::isDomainNegative(getContextDomain(D0)))
    {
      if (PropagatorUtils::isDomainNegative(getContextDomain(D1)))
      {
        if (PropagatorUtils::isDomainNegative(getContextDomain(D2)))
        {
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_NNN;
        }
        else if (PropagatorUtils::isDomainPositive(getContextDomain(D2)))
        {
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_NNP;
        }
        else
        {
          assert(PropagatorUtils::isDomainNegativePositive(getContextDomain(D2)));
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_NNX;
        }
      }
      else if (PropagatorUtils::isDomainPositive(getContextDomain(D1)))
      {
        if (PropagatorUtils::isDomainNegative(getContextDomain(D2)))
        {
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_NPN;
        }
        else if (PropagatorUtils::isDomainPositive(getContextDomain(D2)))
        {
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_NPP;
        }
        else
        {
          assert(PropagatorUtils::isDomainNegativePositive(getContextDomain(D2)));
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_NPX;
        }
      }
      else
      {
        assert(PropagatorUtils::isDomainNegativePositive(getContextDomain(D1)));
        if (PropagatorUtils::isDomainNegative(getContextDomain(D2)))
        {
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_NXN;
        }
        else if (PropagatorUtils::isDomainPositive(getContextDomain(D2)))
        {
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_NXP;
        }
        else
        {
          assert(PropagatorUtils::isDomainNegativePositive(getContextDomain(D2)));
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_NXX;
        }
      }
    }
    else if (PropagatorUtils::isDomainPositive(getContextDomain(D0)))
    {
      if (PropagatorUtils::isDomainNegative(getContextDomain(D1)))
      {
        if (PropagatorUtils::isDomainNegative(getContextDomain(D2)))
        {
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_PNN;
        }
        else if (PropagatorUtils::isDomainPositive(getContextDomain(D2)))
        {
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_PNP;
        }
        else
        {
          assert(PropagatorUtils::isDomainNegativePositive(getContextDomain(D2)));
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_PNX;
        }
      }
      else if (PropagatorUtils::isDomainPositive(getContextDomain(D1)))
      {
        if (PropagatorUtils::isDomainNegative(getContextDomain(D2)))
        {
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_PPN;
        }
        else if (PropagatorUtils::isDomainPositive(getContextDomain(D2)))
        {
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_PPP;
        }
        else
        {
          assert(PropagatorUtils::isDomainNegativePositive(getContextDomain(D2)));
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_PPX;
        }
      }
      else
      {
        assert(PropagatorUtils::isDomainNegativePositive(getContextDomain(D1)));
        if (PropagatorUtils::isDomainNegative(getContextDomain(D2)))
        {
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_PXN;
        }
        else if (PropagatorUtils::isDomainPositive(getContextDomain(D2)))
        {
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_PXP;
        }
        else
        {
          assert(PropagatorUtils::isDomainNegativePositive(getContextDomain(D2)));
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_PXX;
        }
      }
    }
    else
    {
      assert(PropagatorUtils::isDomainNegativePositive(getContextDomain(D0)));
      if (PropagatorUtils::isDomainNegative(getContextDomain(D1)))
      {
        if (PropagatorUtils::isDomainNegative(getContextDomain(D2)))
        {
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_XNN;
        }
        else if (PropagatorUtils::isDomainPositive(getContextDomain(D2)))
        {
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_XNP;
        }
        else
        {
          assert(PropagatorUtils::isDomainNegativePositive(getContextDomain(D2)));
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_XNX;
        }
      }
      else if (PropagatorUtils::isDomainPositive(getContextDomain(D1)))
      {
        if (PropagatorUtils::isDomainNegative(getContextDomain(D2)))
        {
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_XPN;
        }
        else if (PropagatorUtils::isDomainPositive(getContextDomain(D2)))
        {
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_XPP;
        }
        else
        {
          assert(PropagatorUtils::isDomainNegativePositive(getContextDomain(D2)));
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_XPX;
        }
      }
      else
      {
        assert(PropagatorUtils::isDomainNegativePositive(getContextDomain(D1)));
        if (PropagatorUtils::isDomainNegative(getContextDomain(D2)))
        {
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_XXN;
        }
        else if (PropagatorUtils::isDomainPositive(getContextDomain(D2)))
        {
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_XXP;
        }
        else
        {
          assert(PropagatorUtils::isDomainNegativePositive(getContextDomain(D2)));
          return PropagatorTernary::TernaryDomainSignConfig::TDSC_XXX;
        }
      }
    }
  }//getTernaryDomainSignConfig
  
}// end namespace Core
