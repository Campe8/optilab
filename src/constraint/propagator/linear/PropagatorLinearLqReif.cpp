// Base class
#include "PropagatorLinearLqReif.hpp"

#include "PropagatorUtils.hpp"

// Domain
#include "DArrayMatrix.hpp"
#include "Domain.hpp"
#include "DomainElement.hpp"

// Internal propagators
#include "PropagatorLinearLq.hpp"

#include <cassert>

namespace Core {
  
  PropagatorLinearLqReif::PropagatorLinearLqReif(DomainElementArray& aCoeffArray,
                                                 DomainArray&        aDomainArray,
                                                 DomainElement*      aConstant,
                                                 DomainBoolean*      aDomBool,
                                                 PropagatorSemanticType aReifType,
                                                 bool aUseGCDOnNormalization)
    : Core::PropagatorLinearReif(aCoeffArray, aDomainArray, aConstant, aDomBool,
    PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
    aReifType,
    PropagatorSemanticClass::PROP_SEMANTIC_CLASS_LINEAR,
    PropagationPriority::PROP_PRIORITY_LINEAR,
    aUseGCDOnNormalization)
    , pUseGCDOnNormalization(aUseGCDOnNormalization)
  {
    assert(aConstant);
    assert(aDomBool);

    // Register propagators: lin Lq
    PropagatorSPtr propagatorLinLq = std::make_shared<PropagatorLinearLq>(aCoeffArray, aDomainArray,
                                                                          aConstant,
                                                                          aUseGCDOnNormalization);
    registerPropagator(propagatorLinLq);
  }

  double PropagatorLinearLqReif::getFitness()
  {
    if (!isContextReifSingleton())
    {
      return getUnspecFitness();
    }
    
    if (isContextReifTrue())
    {
      return getRegisteredPropagator(PROP_SEMANTIC_TYPE_LIN_LQ)->getFitness();
    }
    
    if (areAllDomainsSingleton())
    {
      //  k < leftSide => returns max(0, 1 + k - leftSide)
      auto leftSide = getLeftSideExpressionValue();
      auto k = getLinearConstant();
      auto sum = k->successor()->plus(leftSide->mirror());
      
      return PropagatorUtils::toFitness(sum->max(sum->zero()));
    }
    
    return getUnspecFitness();
  }//getFitness
  
  PropagationEvent PropagatorLinearLqReif::post()
  {
    if (isContextReifSingleton() && isContextReifTrue())
    {
      // b -> Lin Lq
      return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_LQ);
    }

    PropagationEvent eventUponReification = PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    if (areAllDomainsSingleton())
    {
      if (getLeftSideExpressionValue()->isLessThanOrEqual(getLinearConstant()))
      {
        eventUponReification = setContextReifTrue();
      }
      else
      {
        eventUponReification = setContextReifFalse();
      }
    }
    
    return eventUponReification;
  }//post
  
  PropagationEvent PropagatorLinearLqReif::runInverseLqPropagation()
  {
    // k < ax - by       =>
    // 0 < ax - by - k   =>
    // -ax + by + k < 0  =>
    // -ax + by < -k     =>
    // -ax + by <= -k - 1 

    // Coefficients
    DomainElementArraySPtr posCoeff = getPosCoefficients();
    DomainElementArraySPtr negCoeff = getNegCoefficients();
    DomainElementArray coeffArray(posCoeff->size() + negCoeff->size());
    for (std::size_t idx = 0; idx < posCoeff->size(); ++idx)
    {
      coeffArray.assignElementToCell(idx, posCoeff->operator[](idx)->mirror());
    }
    for (std::size_t idx = 0; idx < negCoeff->size(); ++idx)
    {
      coeffArray.assignElementToCell(idx, negCoeff->operator[](idx));
    }

    // Domains
    DomainArraySPtr posDomains = getPosDomains();
    DomainArraySPtr negDomains = getNegDomains();
    DomainArray domainArray(posDomains->size() + negDomains->size());
    for (std::size_t idx = 0; idx < posDomains->size(); ++idx)
    {
      domainArray.assignElementToCell(idx, posDomains->operator[](idx));
    }
    for (std::size_t idx = 0; idx < negDomains->size(); ++idx)
    {
      domainArray.assignElementToCell(idx, negDomains->operator[](idx));
    }

    // k => -k - 1
    DomainElement* linearConstant = getLinearConstant();
    linearConstant = linearConstant->mirror()->predecessor();

    PropagatorSPtr propagatorLinLq = std::make_shared<PropagatorLinearLq>(coeffArray, domainArray, linearConstant, pUseGCDOnNormalization);
    auto eventLinLq = propagatorLinLq->post();
    if (eventLinLq == PropagationEvent::PROP_EVENT_FAIL)
    {
      return eventLinLq;
    }
    return propagatorLinLq->propagate();
  }//runInverseLqPropagation

  PropagationEvent PropagatorLinearLqReif::runPropagation()
  {
    if (isContextReifSingleton())
    {
      if (isContextReifTrue())
      {
        // b -> Lin Lq
        auto eventLinLq = postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_LQ);
        if (eventLinLq == PropagationEvent::PROP_EVENT_FAIL)
        {
          return eventLinLq;
        }
        return runInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_LQ);
      }
      else
      {
        // !b -> k < ax - by       =>
        //       0 < ax - by - k   =>
        //       -ax + by + k < 0  =>
        //       -ax + by < -k     =>
        //       -ax + by <= -k - 1 
        return runInverseLqPropagation();
      }
    }

    if (areAllDomainsSingleton())
    {
      if (getLeftSideExpressionValue()->isLessThanOrEqual(getLinearConstant()))
      {
        // Lin Lq -> b
        return setContextReifTrue();
      }
      else
      {
        // Lin Gt -> !b
        return setContextReifFalse();
      }
    }

    return PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagation
  
  PropagatorHLinLqReif::PropagatorHLinLqReif(DomainElementArray& aCoeffArray,
                                             DomainArray&        aDomainArray,
                                             DomainElement*      aConstant,
                                             DomainBoolean*      aDomBool,
                                             PropagatorSemanticType aReifType,
                                             bool aUseGCDOnNormalization)
    : PropagatorLinearLqReif(aCoeffArray, aDomainArray, aConstant, aDomBool, aReifType, aUseGCDOnNormalization)
    , pReifType(aReifType)
  {
    assert(pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_LQ_HREIF_L ||
           pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_LQ_HREIF_R);
  }

  PropagationEvent PropagatorHLinLqReif::post()
  {
    if (isContextReifSingleton())
    {
      if (isHalfReifLeft() && isContextReifTrue())
      {
        // b -> Lin Lq
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_LQ);
      }
    }

    PropagationEvent eventUponReification = PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    if (areAllDomainsSingleton())
    {
      bool domainLessThanOrEqual = getLeftSideExpressionValue()->isLessThanOrEqual(getLinearConstant());
      if (!domainLessThanOrEqual && isHalfReifLeft())
      {
        eventUponReification = setContextReifFalse();
      }
      else if (domainLessThanOrEqual && isHalfReifRight())
      {
        eventUponReification = setContextReifTrue();
      }
      if (eventUponReification == PropagationEvent::PROP_EVENT_FAIL)
      {
        return PropagationEvent::PROP_EVENT_FAIL;
      }
    }
    return eventUponReification;
  }//post

  PropagationEvent PropagatorHLinLqReif::runPropagation()
  {
    if (isContextReifSingleton())
    {
      if (isHalfReifLeft() && isContextReifTrue())
      {
        // b -> Lin Eq
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_LQ);
      }
      else if (isHalfReifRight() && isContextReifFalse())
      {
        // !b -> k < ax - by       =>
        //       0 < ax - by - k   =>
        //       -ax + by + k < 0  =>
        //       -ax + by < -k     =>
        //       -ax + by <= -k - 1 
        return runInverseLqPropagation();
      }
    }

    if (areAllDomainsSingleton())
    {
      bool domainEquality = getLeftSideExpressionValue()->isLessThanOrEqual(getLinearConstant());
      if (domainEquality && isHalfReifRight())
      {
        // Lin Eq -> b
        return setContextReifTrue();
      }
      else if (!domainEquality && isHalfReifLeft())
      {
        // Lin Gt -> !b
        return setContextReifFalse();
      }
    }

    return PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagation

  PropagatorHLinLqReifL::PropagatorHLinLqReifL(DomainElementArray& aCoeffArray,
    DomainArray&        aDomainArray,
    DomainElement*      aConstant,
    DomainBoolean*      aDomBool,
    bool aUseGCDOnNormalization) :
    PropagatorHLinLqReif(aCoeffArray, aDomainArray, aConstant, aDomBool,
                         PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_LQ_HREIF_L,
                         aUseGCDOnNormalization)
  {
  }

  double PropagatorHLinLqReifL::getFitness()
  {
    if (!isContextReifSingleton())
    {
      return getUnspecFitness();
    }
    
    // Lin_lq <- F => 0
    if (isContextReifFalse())
    {
      return getMinFitness();
    }
    
    // // Lin_lq <- T => Lin_eq.getFitness()
    return getRegisteredPropagator(PROP_SEMANTIC_TYPE_LIN_LQ)->getFitness();
  }//getFitness
  
  PropagatorHLinLqReifR::PropagatorHLinLqReifR(DomainElementArray& aCoeffArray,
    DomainArray&        aDomainArray,
    DomainElement*      aConstant,
    DomainBoolean*      aDomBool,
    bool aUseGCDOnNormalization) :
    PropagatorHLinLqReif(aCoeffArray, aDomainArray, aConstant, aDomBool,
                         PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_LQ_HREIF_R,
                         aUseGCDOnNormalization)
  {
  }
  
  double PropagatorHLinLqReifR::getFitness()
  {
    if (!isContextReifSingleton())
    {
      return getUnspecFitness();
    }
    
    auto leftSideFitness = getRegisteredPropagator(PROP_SEMANTIC_TYPE_LIN_LQ)->getFitness();
    
    // T -> F => 1
    if (leftSideFitness == getMinFitness() && isContextReifFalse())
    {
      return 1.0;
    }
    
    if (leftSideFitness == getUnspecFitness())
    {
      return getUnspecFitness();
    }
    
    // T -> T      => 0
    // F -> [F, T] => 0
    return getMinFitness();
  }//getFitness

}// end namespace Core
