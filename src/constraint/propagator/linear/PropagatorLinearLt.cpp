// Base class
#include "PropagatorLinearLt.hpp"
#include "PropagatorLinearLq.hpp"

#include "PropagatorUtils.hpp"

#include "DArrayMatrix.hpp"

#include "DomainElement.hpp"

#include <cassert>

namespace Core {
  
  PropagatorLinearLt::PropagatorLinearLt(DomainElementArray& aCoeffArray,
                                         DomainArray&        aDomainArray,
                                         DomainElement*      aConstant,
                                         bool aUseGCDOnNormalization)
  : Core::PropagatorLinear(aCoeffArray, aDomainArray, aConstant,
                           PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
                           PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_LT,
                           PropagatorSemanticClass::PROP_SEMANTIC_CLASS_LINEAR,
                           PropagationPriority::PROP_PRIORITY_LINEAR,
                           false),
  pInternalPropagatorType(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_LQ)
  {
    // Register internal propagator: x < y <=> x <= y - 1
    PropagatorSPtr propagatorLq = std::make_shared<PropagatorLinearLq>(aCoeffArray,
                                                                       aDomainArray,
                                                                       aConstant->predecessor(),
                                                                       aUseGCDOnNormalization);
    registerPropagator(propagatorLq);
  }
  
  double PropagatorLinearLt::getFitness()
  {
    // Given
    // c_1 * x_1 + c_2 * x_2 + ... + k_n * x_n < k
    // the fitness function should implement the following function
    // f(C, X, K) = max(0, 1 + (c_1 * x_1 + c_2 * x_2 + ... + k_n * x_n) - k)
    
    // Coefficients
    auto posCoeff = getPosCoefficients().get();
    auto negCoeff = getNegCoefficients().get();
    
    // Domains
    auto posDom = getPosDomains().get();
    auto negDom = getNegDomains().get();
    
    // Constant
    DomainElement* linConst = getLinearConstant();
    
    assert(posCoeff->size() == posDom->size());
    assert(negCoeff->size() == negDom->size());
    assert(linConst);
    
    // No coefficients, i.e., the linear lt constraint is something like
    // 10 < 9 => 1 < 0
    // 8 < 9 => -1 < 0
    if(posCoeff->size() + negCoeff->size() == 0)
    {
      if (linConst->zero()->isLessThan(linConst))
      {
        return getMinFitness();
      }
      
      auto diff = linConst->mirror();
      diff = diff->plus(diff->zero())->successor();
      PropagatorUtils::toFitness(diff);
    }
    
    // Calculate the sum:
    // Sum(posCoeff * posDom) - Sum(negCoeff * negDom)
    DomainElement* leftHandSideEquation = linConst->zero();
    for (std::size_t idx{0}; idx < negCoeff->size(); ++idx)
    {
      // Get current domain
      Domain* domain = negDom->operator[](idx);
      
      // Return if the domain is empty or not singleton
      if (PropagatorUtils::isDomainEmpty(domain))
      {
        return getInvalidFitness();
      }
      else if (!PropagatorUtils::isDomainSingleton(domain))
      {
        return getUnspecFitness();
      }

      // Get current coefficient and calculate
      // Sum(negCoeff * negDom)
      DomainElement* coeff = negCoeff->operator[](idx);
      leftHandSideEquation = leftHandSideEquation->plus(coeff->times(domain->upperBound()));
    }
    
    // Calculate -Sum(negCoeff * negDom)
    leftHandSideEquation = leftHandSideEquation->mirror();
    
    for (std::size_t idx{0}; idx < posCoeff->size(); ++idx)
    {
      // Get current domain
      Domain* domain = posDom->operator[](idx);
      
      // Return if the domain is empty not singleton
      if (PropagatorUtils::isDomainEmpty(domain))
      {
        return getInvalidFitness();
      }
      else if (!PropagatorUtils::isDomainSingleton(domain))
      {
        return getUnspecFitness();
      }
      
      // Get current coefficient and calculate
      // Sum(posCoeff * negDom)
      DomainElement* coeff = posCoeff->operator[](idx);
      leftHandSideEquation = leftHandSideEquation->plus(coeff->times(domain->lowerBound()));
    }
    
    // If left < k -> constraint unsatisfied, return 0
    if(leftHandSideEquation->isLessThan(linConst))
    {
      return getMinFitness();
    }
    
    // Calculate (1 + leftHandSideEquation - k)
    auto sum = leftHandSideEquation->plus(linConst->mirror());
    sum = sum->plus(sum->zero()->successor());
    
    // if ((1 + leftHandSideEquation - k) < 0)
    // {
    //    return getMinFitness();
    // }
    // else
    // {
    //    return (1+ leftHandSideEquation - k);
    // }
    return sum->isLessThan(sum->zero()) ? getMinFitness() : PropagatorUtils::toFitness(sum);
  }//getFitness
  
  PropagationEvent PropagatorLinearLt::post()
  {
    return postInternalPropagator(pInternalPropagatorType);
  }//post
  
  PropagationEvent PropagatorLinearLt::runPropagation()
  {
    // Propagation of
    // Sum(posCoeff * posDom) - Sum(negCoeff * negDom) < linConst
    return runInternalPropagator(pInternalPropagatorType);
  }//runPropagation
  
}// end namespace Core
