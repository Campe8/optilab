// Base class
#include "PropagatorLinearEq.hpp"

#include "PropagatorUtils.hpp"

#include "DArrayMatrix.hpp"

#include "Domain.hpp"
#include "DomainElement.hpp"

#include <cassert>

namespace Core {
  
  PropagatorLinearEq::PropagatorLinearEq(DomainElementArray& aCoeffArray,
                                         DomainArray&        aDomainArray,
                                         DomainElement*      aConstant,
                                         bool aUseGCDOnNormalization)
  : Core::PropagatorLinear(aCoeffArray, aDomainArray, aConstant,
                           PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
                           PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_EQ,
                           PropagatorSemanticClass::PROP_SEMANTIC_CLASS_LINEAR,
                           PropagationPriority::PROP_PRIORITY_LINEAR,
                           aUseGCDOnNormalization)
  {
    // Register filters
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
  }
  
  double PropagatorLinearEq::getFitness()
  {
    // Given
    // c_1 * x_1 + c_2 * x_2 + ... + k_n * x_n = k
    // the fitness function should implement the following function
    // f(C, X, K) = abs(k - (c_1 * x_1 + c_2 * x_2 + ... + k_n * x_n))
    
    // Coefficients
    auto posCoeff = getPosCoefficients().get();
    auto negCoeff = getNegCoefficients().get();
    
    // Domains
    auto posDom = getPosDomains().get();
    auto negDom = getNegDomains().get();
    
    // Constant
    DomainElement* linConst = getLinearConstant();
    
    assert(posCoeff->size() == posDom->size());
    assert(negCoeff->size() == negDom->size());
    assert(linConst);
    
    // No coefficients, i.e., the linear nq constraint is something like
    // 10 == 9 => 1 == 0
    if(posCoeff->size() + negCoeff->size() == 0)
    {
      return (linConst->zero()->isEqual(linConst)) ?
      getMinFitness() : PropagatorUtils::toFitness(linConst->abs());
    }
    
    // Calculate the sum:
    // Sum(posCoeff * posDom) - Sum(negCoeff * negDom)
    DomainElement* leftHandSideEquation = linConst->zero();
    for (std::size_t idx{0}; idx < negCoeff->size(); ++idx)
    {
      // Get current domain
      Domain* domain = negDom->operator[](idx);
      
      // Return if the domain is empty or not singleton
      if (PropagatorUtils::isDomainEmpty(domain))
      {
        return getInvalidFitness();
      }
      else if (!PropagatorUtils::isDomainSingleton(domain))
      {
        return getUnspecFitness();
      }
      
      // Get current coefficient and calculate
      // Sum(negCoeff * negDom)
      DomainElement* coeff = negCoeff->operator[](idx);
      leftHandSideEquation = leftHandSideEquation->plus(coeff->times(domain->upperBound()));
    }
    
    // Calculate -Sum(negCoeff * negDom)
    leftHandSideEquation = leftHandSideEquation->mirror();
    
    for (std::size_t idx{0}; idx < posCoeff->size(); ++idx)
    {
      // Get current domain
      Domain* domain = posDom->operator[](idx);
      
      // Return if the domain is empty not singleton
      if (PropagatorUtils::isDomainEmpty(domain))
      {
        return getInvalidFitness();
      }
      else if (!PropagatorUtils::isDomainSingleton(domain))
      {
        return getUnspecFitness();
      }
      
      // Get current coefficient and calculate
      // Sum(posCoeff * negDom)
      DomainElement* coeff = posCoeff->operator[](idx);
      leftHandSideEquation = leftHandSideEquation->plus(coeff->times(domain->lowerBound()));
    }
    
    // If left == k -> constraint satisfied, return 0
    if(leftHandSideEquation->isEqual(linConst))
    {
      return getMinFitness();
    }
    
    // Calculate abs(k - leftHandSideEquation) as
    // lb0 = abs(k);
    // lb1 = abs(leftHandSideEquation);
    // absDiff = lb1 > lb0 ? (lb1 - lb0) : (lb0 - lb1);
    DomainElement* lb0 = linConst->abs();
    DomainElement* lb1 = leftHandSideEquation->abs();
    
    DomainElement* absDiff{nullptr};
    if (lb0->isLessThan(lb1))
    {
      absDiff = lb1->plus(lb0->mirror());
    }
    else
    {
      absDiff = lb0->plus(lb1->mirror());
    }
    assert(absDiff);
    
    // Constraint satisfied, return 0
    return PropagatorUtils::toFitness(absDiff);
  }//getFitness
  
  PropagationEvent PropagatorLinearEq::post()
  {
    // Post linear equation
    // Sum(posCoeff * posDom) - Sum(negCoeff * negDom) = linConst
    
    // Coefficients
    DomainElementArraySPtr posCoeff = getPosCoefficients();
    DomainElementArraySPtr negCoeff = getNegCoefficients();
    
    // Domains
    DomainArraySPtr posDom = getPosDomains();
    DomainArraySPtr negDom = getNegDomains();
    
    // Constant
    DomainElement* linConst = getLinearConstant();
    
    assert(posCoeff->size() == posDom->size());
    assert(negCoeff->size() == negDom->size());
    assert(linConst);
    
    if(posCoeff->size() + negCoeff->size() == 0)
    {
      if(linConst->isEqual(linConst->zero()))
      {
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
      return PropagationEvent::PROP_EVENT_FAIL;
    }
    return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
  }//post
  
  PropagationEvent PropagatorLinearEq::runPropagation()
  {
    // Propagation of
    // Sum(posCoeff * posDom) - Sum(negCoeff * negDom) = linConst
    
    // Coefficients
    DomainElementArraySPtr posCoeff = getPosCoefficients();
    DomainElementArraySPtr negCoeff = getNegCoefficients();
    
    // Domains
    DomainArraySPtr posDom = getPosDomains();
    DomainArraySPtr negDom = getNegDomains();
    
    // Constant
    DomainElement* linConst = getLinearConstant();
    
    assert(posCoeff->size() == posDom->size());
    assert(negCoeff->size() == negDom->size());
    assert(linConst);
    
    if(posCoeff->size() + negCoeff->size() == 0)
    {
      if(linConst->isEqual(linConst->zero()))
      {
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
      return PropagationEvent::PROP_EVENT_FAIL;
    }
    
    assert(posCoeff->size() + negCoeff->size() != 0);
    
    // Calculate starting bounds
    auto posBounds = calculatePositiveBounds();
    auto negBounds = calculateNegativeBounds();
    
    // Propagate on bounds ->
    // iterate until fix point is reached
    bool eventChanged;
    do
    {
      eventChanged = false;
      {
        // Upper bound on positive domains
        // UB_j = min(UB_j, alpha)
        // where
        // j \in POS
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
        for(std::size_t idx = 0; idx < posCoeff->size(); ++idx)
        {
          Domain *outDomain = posDom->operator[](idx);
          
          // Skip singletons
          if (PropagatorUtils::isDomainSingleton(outDomain))
          {
            continue;
          }
          
          DomainEvent* preFilterEvent = outDomain->getEvent();
          
          setFilterOutputPtr(outDomain);
          
          DomainElement *alpha = getAlpha(posCoeff->operator[](idx),
                                          posDom->operator[](idx),
                                          posBounds.first, negBounds.second);
          setFilterInput(0, outDomain->upperBound()->min(alpha));
          
          outDomain = applyFilteringStrategy();
          DOMAIN_FAILURE_CHECK(outDomain);
          
          // Keep iterating until reaching the fix point
          eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
        }
        // Update bounds
        if (eventChanged)
        {
          posBounds = calculatePositiveBounds();
          negBounds = calculateNegativeBounds();
        }
      }
      
      {
        // Lower bound on negative domains
        // LB_j = max(LB_j, beta)
        // where
        // j \in NEG
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
        for(std::size_t idx = 0; idx < negCoeff->size(); ++idx)
        {
          Domain *outDomain = negDom->operator[](idx);
          
          // Skip singletons
          if (PropagatorUtils::isDomainSingleton(outDomain))
          {
            continue;
          }
          
          DomainEvent* preFilterEvent = outDomain->getEvent();
          
          setFilterOutputPtr(outDomain);
          
          DomainElement *beta = getBeta(negCoeff->operator[](idx),
                                        negDom->operator[](idx),
                                        posBounds.first, negBounds.second);
          setFilterInput(0, outDomain->lowerBound()->max(beta));
          
          outDomain = applyFilteringStrategy();
          DOMAIN_FAILURE_CHECK(outDomain);
          
          // Keep iterating until reaching the fix point
          eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
        }
        // Update bounds
        if (eventChanged)
        {
          posBounds = calculatePositiveBounds();
          negBounds = calculateNegativeBounds();
        }
      }
      
      {
        // Lower bound on positive domains
        // LB_j = max(LB_j, gamma)
        // where
        // j \in POS
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
        for(std::size_t idx = 0; idx < posCoeff->size(); ++idx)
        {
          Domain *outDomain = posDom->operator[](idx);
          
          // Skip singletons
          if (PropagatorUtils::isDomainSingleton(outDomain))
          {
            continue;
          }
          
          DomainEvent* preFilterEvent = outDomain->getEvent();
          
          setFilterOutputPtr(outDomain);
          
          DomainElement *gamma = getGamma(posCoeff->operator[](idx),
                                          posDom->operator[](idx),
                                          posBounds.second, negBounds.first);
          setFilterInput(0, outDomain->lowerBound()->max(gamma));
          
          outDomain = applyFilteringStrategy();
          DOMAIN_FAILURE_CHECK(outDomain);
          
          // Keep iterating until reaching the fix point
          eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
        }
        // Update bounds
        if (eventChanged)
        {
          posBounds = calculatePositiveBounds();
          negBounds = calculateNegativeBounds();
        }
      }
      
      {
        // Upper bound on negative domains
        // UB_j = min(UB_j, delta)
        // where
        // j \in NEG
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
        for(std::size_t idx = 0; idx < negCoeff->size(); ++idx)
        {
          Domain *outDomain = negDom->operator[](idx);
          
          // Skip singletons
          if (PropagatorUtils::isDomainSingleton(outDomain))
          {
            continue;
          }
          
          DomainEvent* preFilterEvent = outDomain->getEvent();
          
          setFilterOutputPtr(outDomain);
          
          DomainElement *delta = getDelta(negCoeff->operator[](idx),
                                          negDom->operator[](idx),
                                          posBounds.second, negBounds.first);
          setFilterInput(0, outDomain->upperBound()->min(delta));
          
          outDomain = applyFilteringStrategy();
          DOMAIN_FAILURE_CHECK(outDomain);
          
          // Keep iterating until reaching the fix point
          eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
        }
        // Update bounds
        if (eventChanged)
        {
          posBounds = calculatePositiveBounds();
          negBounds = calculateNegativeBounds();
        }
      }
    } while (eventChanged);
    
    DomainElement *leftSideLinearEquation = linConst->zero();
    for(std::size_t idx = 0; idx < negCoeff->size(); ++idx)
    {
      Domain *domain = negDom->operator[](idx);
      if(!PropagatorUtils::isDomainSingleton(domain))
      {
        return PropagationEvent::PROP_EVENT_FIXPOINT;
      }
      leftSideLinearEquation = leftSideLinearEquation->plus(negCoeff->operator[](idx)->times(PropagatorUtils::getSingleton(domain)));
    }
    
    leftSideLinearEquation = leftSideLinearEquation->mirror();
    for(std::size_t idx = 0; idx < posCoeff->size(); ++idx)
    {
      Domain *domain = posDom->operator[](idx);
      if(!PropagatorUtils::isDomainSingleton(domain))
      {
        return PropagationEvent::PROP_EVENT_FIXPOINT;
      }
      leftSideLinearEquation = leftSideLinearEquation->plus(posCoeff->operator[](idx)->times(PropagatorUtils::getSingleton(domain)));
    }
    
    if(leftSideLinearEquation->isEqual(linConst))
    {
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    return PropagationEvent::PROP_EVENT_FAIL;
  }//runPropagation
  
  DomainElement* PropagatorLinearEq::getAlpha(DomainElement* aCoeff, Domain* aDomain,
                                              DomainElement* aPosLowerBound, DomainElement* aNegUpperBound)
  {
    assert(aCoeff);
    assert(aDomain);
    
    DomainElement* coeffLower = aCoeff->times(aDomain->lowerBound());
    DomainElement* sumPosNormalized = aPosLowerBound ? ((aPosLowerBound->plus(coeffLower->mirror()))->mirror()) : aCoeff->zero();
    if (!aNegUpperBound)
    {
      aNegUpperBound = aCoeff->zero();
    }
    
    DomainElement* dividend = (getLinearConstant()->plus(sumPosNormalized))->plus(aNegUpperBound);
    
    auto oldRoundingMode = dividend->getRoundingMode();
    dividend->setRoundingMode(Core::DomainElement::RoundingMode::DE_ROUNDING_FLOOR);
    DomainElement* divToReturn = dividend->div(aCoeff);
    dividend->setRoundingMode(oldRoundingMode);
    return divToReturn;
  }//getAlpha
  
  DomainElement* PropagatorLinearEq::getBeta(DomainElement* aCoeff, Domain* aDomain,
                                             DomainElement* aPosLowerBound, DomainElement* aNegUpperBound)
  {
    assert(aCoeff);
    assert(aDomain);
    
    DomainElement* coeffUpper= aCoeff->times(aDomain->upperBound());
    DomainElement* sumNegNormalized = aNegUpperBound ? ((aNegUpperBound->plus(coeffUpper->mirror()))->mirror()) : aCoeff->zero();
    if (!aPosLowerBound)
    {
      aPosLowerBound = aCoeff->zero();
    }
    
    DomainElement* dividend = ((getLinearConstant()->mirror())->plus(aPosLowerBound))->plus(sumNegNormalized);
    
    auto oldRoundingMode = dividend->getRoundingMode();
    dividend->setRoundingMode(Core::DomainElement::RoundingMode::DE_ROUNDING_CEILING);
    DomainElement* divToReturn = dividend->div(aCoeff);
    dividend->setRoundingMode(oldRoundingMode);
    return divToReturn;
  }//getBeta
  
  DomainElement* PropagatorLinearEq::getGamma(DomainElement* aCoeff, Domain* aDomain,
                                              DomainElement* aPosUpperBound, DomainElement* aNegLowerBound)
  {
    assert(aCoeff);
    assert(aDomain);
    
    DomainElement* coeffUpper = aCoeff->times(aDomain->upperBound());
    DomainElement* sumPosNormalized = aPosUpperBound ? ((aPosUpperBound->plus(coeffUpper->mirror()))->mirror()) : aCoeff->zero();
    if (!aNegLowerBound)
    {
      aNegLowerBound = aCoeff->zero();
    }
    
    DomainElement* dividend = (getLinearConstant()->plus(sumPosNormalized))->plus(aNegLowerBound);
    
    auto oldRoundingMode = dividend->getRoundingMode();
    dividend->setRoundingMode(Core::DomainElement::RoundingMode::DE_ROUNDING_CEILING);
    DomainElement* divToReturn = dividend->div(aCoeff);
    dividend->setRoundingMode(oldRoundingMode);
    return divToReturn;
  }//getGamma
  
  DomainElement* PropagatorLinearEq::getDelta(DomainElement* aCoeff, Domain* aDomain,
                                              DomainElement* aPosUpperBound, DomainElement* aNegLowerBound)
  {
    assert(aCoeff);
    assert(aDomain);
    
    DomainElement* coeffLower = aCoeff->times(aDomain->lowerBound());
    DomainElement* sumNegNormalized = aNegLowerBound ? ((aNegLowerBound->plus(coeffLower->mirror()))->mirror()) : aCoeff->zero();
    if (!aPosUpperBound)
    {
      aPosUpperBound = aCoeff->zero();
    }
    
    DomainElement* dividend = ((getLinearConstant()->mirror())->plus(aPosUpperBound))->plus(sumNegNormalized);
    
    auto oldRoundingMode = dividend->getRoundingMode();
    dividend->setRoundingMode(Core::DomainElement::RoundingMode::DE_ROUNDING_FLOOR);
    DomainElement* divToReturn = dividend->div(aCoeff);
    dividend->setRoundingMode(oldRoundingMode);
    return divToReturn;
  }//getGamma
  
}// end namespace Core
