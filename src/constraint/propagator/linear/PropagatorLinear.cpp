// Base class
#include "PropagatorLinear.hpp"

#include "PropagatorUtils.hpp"

#include "DArrayMatrix.hpp"

#include "Domain.hpp"
#include "DomainElement.hpp"

#include <algorithm>
#include <utility>
#include <cassert>

namespace Core {
  
  static std::vector<std::size_t> getSingletonDomainIndices(DomainArray& aDomainArray)
  {
    std::size_t idx{0};
    std::vector<std::size_t> singletonDomainIdx;
    for(const auto domain : aDomainArray)
    {
      if(PropagatorUtils::isDomainSingleton(domain))
      {
        singletonDomainIdx.push_back(idx);
      }
      idx++;
    }
    
    return singletonDomainIdx;
  }//getSingletonDomainIndices
  
  // Returns the vectors of indices of positive and negative coefficients.
  // @note last element of the given array of coefficients is assumed to be the constant of the linear propagator
  std::pair<std::vector<std::size_t>, std::vector<std::size_t>> getPosNegCoefficientIndices(DomainElementArray& aCoeff)
  {
    std::vector<std::size_t> positive;
    std::vector<std::size_t> negative;
    if(aCoeff.size() < 2) return {positive, negative};
    
    // Get the zero element
    auto constIdx = aCoeff.size()-1;
    DomainElement *zeroElement = aCoeff[constIdx]->zero();
    
    // Iterate over the vector and split positive coefficients from negative coefficients
    for(std::size_t idx{0}; idx < constIdx; ++idx)
    {
      DomainElement* elem = aCoeff[idx];
      
      // Zero elements do no make any contribution, skip them
      if(elem->isEqual(zeroElement))
      {
        continue;
      }
      
      if(elem->isLessThan(zeroElement))
      {
        // Negative values
        negative.push_back(idx);
      }
      else
      {
        // Positive elements
        positive.push_back(idx);
      }
    }
    
    return {positive, negative};
  }//getPosNegCoefficientIndices
  
  PropagatorLinear::PropagatorLinear(DomainElementArray& aCoeffArray,
                                     DomainArray&        aDomainArray,
                                     DomainElement*      aConstant,
                                     PropagatorStrategyType aStrategyType,
                                     PropagatorSemanticType aSemanticType,
                                     PropagatorSemanticClass aSemanticClass,
                                     PropagationPriority aPriority,
                                     bool aUseGCDOnNormalization)
  : Propagator(PropagatorSemantics(aDomainArray.size(), aStrategyType, aPriority, aSemanticType, aSemanticClass))
  {
    assert(aConstant);
    
    // At least one element
    assert(aCoeffArray.size());
    
    // Same size for coefficients and Domains
    assert(aCoeffArray.size() == aDomainArray.size());
    
    // Same type all coefficients
    auto coeffType = aCoeffArray[0]->getType();
    for(std::size_t idx = 1; idx < aCoeffArray.size(); ++idx)
    {
      assert(coeffType == aCoeffArray[idx]->getType());
    }
    assert(coeffType == aConstant->getType());
    
    // Same type all domains
    // Add context domains
    for(std::size_t idx = 0; idx < aDomainArray.size(); ++idx)
    {
      assert(!PropagatorUtils::isDomainEmpty(aDomainArray[idx]));
      assert(coeffType == aDomainArray[idx]->lowerBound()->getType());
      addContextDomain(idx, aDomainArray[idx]);
    }
    
    // Normalize linear expression
    // This fills the array of domains and coefficients
    normalizeLinearExpression(aCoeffArray, aDomainArray, aConstant, aUseGCDOnNormalization);
  }
  
  PropagatorLinear::~PropagatorLinear()
  {
  }
  
  void PropagatorLinear::divideArrayByDomainElement(DomainElementArray& aDomainElementArray, DomainElement* aDomainElement)
  {
    for(std::size_t idx = 0; idx < aDomainElementArray.size(); ++idx)
    {
      aDomainElementArray.assignElementToCell(idx, aDomainElementArray[idx]->div(aDomainElement));
    }
  }//divideArrayByDomainElement
  
  DomainElement *PropagatorLinear::findGCDOnArray(DomainElementArray& aCoeffArray)
  {
    DomainElement* gcd;
    if(aCoeffArray.size() == 1)
    {
      gcd = aCoeffArray[0];
    }
    else
    {
      gcd = aCoeffArray[0];
      for(std::size_t idx = 1; idx < aCoeffArray.size(); ++idx)
      {
        gcd = findGCD(gcd, aCoeffArray[idx]);
      }
    }
    
    assert(gcd->zero()->isLessThan(gcd));
    return gcd;
  }//findGCDOnArray
  
  DomainElement* PropagatorLinear::findGCD(DomainElement* aElem1, DomainElement* aElem2)
  {
    assert(aElem1);
    assert(aElem2);
    DomainElement *r;
    
    DomainElement *zeroElem = aElem2->zero();
    while (aElem2->isNotEqual(zeroElem))
    {
      r = mod(aElem1, aElem2);
      aElem1 = aElem2;
      aElem2 = r;
    }
    
    if(aElem1->isLessThan(zeroElem))
    {
      return aElem1->mirror();
    }
    
    return aElem1;
  }//findGCD
  
  DomainElement* PropagatorLinear::mod(DomainElement* aElem1, DomainElement* aElem2)
  {
    assert(aElem1);
    assert(aElem2);
    
    DomainElement* prodElem = aElem2->times(aElem1->div(aElem2));
    return aElem1->plus(prodElem->mirror());
  }//mod
  
  void PropagatorLinear::normalizeLinearExpression(DomainElementArray& aCoeffArray,
                                                   DomainArray& aDomainArray,
                                                   DomainElement* aConstant,
                                                   bool aUseGCDOnNormalization)
  {
    // Normalize coefficients for non singleton domain.
    // 1) Retrieve the indices of all singleton domains
    auto singletonDomainIdx = getSingletonDomainIndices(aDomainArray);
    
    // 2) Add (mirror of) singleton domains to constant.
    //    For example:
    //    lin_eq([1, -1], [x, y], 1)
    //    with x = [10, 10] and y = [2, 3]
    //    x - y = 1
    //    is
    //    10 - y = 1
    //    and becomes
    //    -y = -9
    //    where -10 is added to the constant 1
    DomainElement* costantScaleFactor{nullptr};
    for(auto idx : singletonDomainIdx)
    {
      if(!costantScaleFactor)
      {
        // Initialize the constant scale factor
        costantScaleFactor = aCoeffArray[idx]->times(PropagatorUtils::getSingleton(aDomainArray[idx]));
      }
      else
      {
        // Add the other singleton domains to the constant scale factor
        costantScaleFactor = costantScaleFactor->plus(aCoeffArray[idx]->times(PropagatorUtils::getSingleton(aDomainArray[idx])));
      }
    }
    
    // Add the mirror of the singleton domains multiplied by their coefficients to the constant
    if(costantScaleFactor)
    {
      aConstant = aConstant->plus(costantScaleFactor->mirror());
    }
    
    // 3) Prepare the array of coefficients.
    // @note the coefficients used to normalize the constant must be now removed from the
    // set of coefficients of the linear constraint.
    // In the example above, the array of coefficients is
    // [-1] + [1] (the constant)
    // [-1, 1]
    // @note the +1 in the formula below is needed for the constant which will be part
    // of the array of coefficients
    DomainElementArray coeffArray(aCoeffArray.size() + 1 - singletonDomainIdx.size());
    assert(coeffArray.size() > 0);
    
    // Set in the array of coefficients all the coefficients
    // corresponding to the domains that are not singleton, plus the constant.
    // @note the index of the coefficients is mapped into a vector since coeffArray
    // holds all the coefficients for the non singleton domains but their index is coming
    // from the full array of coefficients
    // @note domainMapping maps directly on the full array of domains
    std::size_t indexForAssignment{0};
    std::vector<std::size_t> coeffMapping;
    std::vector<std::size_t> domainMapping;
    for (std::size_t idx{0}; idx < aCoeffArray.size(); ++idx)
    {
      // Exclude singleton domains
      if(std::find(singletonDomainIdx.begin(), singletonDomainIdx.end(), idx) != singletonDomainIdx.end()) continue;
      coeffArray.assignElementToCell(indexForAssignment, aCoeffArray[idx]);
      coeffMapping.push_back(indexForAssignment++);
      domainMapping.push_back(idx);
    }
    
    // Assign the constant to the array of coefficients
    coeffArray.assignElementToCell(coeffArray.size() - 1, aConstant);
    
    // If the only coefficient is the constant then set the constant and retun
    if (coeffArray.size() == 1)
    {
      // Coefficients
      pDomElemNeg = std::make_shared<DomainElementArray>(0);
      pDomElemPos = std::make_shared<DomainElementArray>(0);
      
      // Domains
      pDomNeg = std::make_shared<DomainArray>(0);
      pDomPos = std::make_shared<DomainArray>(0);
      
      // Constant
      pConstant = coeffArray[coeffArray.size()-1];
      
      return;
    }
    
    if (aUseGCDOnNormalization)
    {
      // Calculate GCD
      DomainElement *gcdElement = findGCDOnArray(coeffArray);
      
      // Divide by GCD
      divideArrayByDomainElement(coeffArray, gcdElement);
    }
    
    // 4) Set the constant
    pConstant = coeffArray[coeffArray.size()-1];
    
    // 5) Split positive and negative elements based on their coefficients
    auto posNegIdx = getPosNegCoefficientIndices(coeffArray);
    
    // 6) Prepare the array of positive/negative coefficients and domains
    // Coefficients
    pDomElemPos = std::make_shared<DomainElementArray>(posNegIdx.first.size());
    pDomElemNeg = std::make_shared<DomainElementArray>(posNegIdx.second.size());
    
    // Domains
    pDomPos = std::make_shared<DomainArray>(posNegIdx.first.size());
    pDomNeg = std::make_shared<DomainArray>(posNegIdx.second.size());
    
    std::size_t ctr{0};
    for(auto& idx : posNegIdx.first)
    {
      assert(idx < coeffMapping.size());
      pDomElemPos->assignElementToCell(ctr, coeffArray[coeffMapping[idx]]);
      pDomPos->assignElementToCell(ctr, aDomainArray[domainMapping[idx]]);
      ctr++;
    }
    
    ctr = 0;
    for(auto& idx : posNegIdx.second)
    {
      assert(idx < coeffMapping.size());
      pDomElemNeg->assignElementToCell(ctr, coeffArray[coeffMapping[idx]]->mirror());
      pDomNeg->assignElementToCell(ctr, aDomainArray[domainMapping[idx]]);
      ctr++;
    }
  }//normalizeLinearExpression
  
  std::pair<DomainElement*, DomainElement*> PropagatorLinear::calculatePositiveBounds()
  {
    assert(pDomPos->size() == pDomElemPos->size());
    if(pDomPos->empty())
    {
      return std::make_pair(nullptr, nullptr);
    }
    
    DomainElement* lowerBound = ((pDomPos->operator[](0))->lowerBound())->times(pDomElemPos->operator[](0));
    DomainElement* upperBound = ((pDomPos->operator[](0))->upperBound())->times(pDomElemPos->operator[](0));
    
    for(std::size_t idx = 1; idx < pDomPos->size(); ++idx)
    {
      lowerBound = lowerBound->plus((pDomPos->operator[](idx))->lowerBound()->times(pDomElemPos->operator[](idx)));
      upperBound = upperBound->plus((pDomPos->operator[](idx))->upperBound()->times(pDomElemPos->operator[](idx)));
    }
    
    return std::make_pair(lowerBound, upperBound);
  }//calculatePositiveBounds
  
  std::pair<DomainElement*, DomainElement*> PropagatorLinear::calculateNegativeBounds()
  {
    assert(pDomNeg->size() == pDomElemNeg->size());
    if(pDomNeg->empty())
    {
      return std::make_pair(nullptr, nullptr);
    }
    
    DomainElement* lowerBound = ((pDomNeg->operator[](0))->lowerBound())->times(pDomElemNeg->operator[](0));
    DomainElement* upperBound = ((pDomNeg->operator[](0))->upperBound())->times(pDomElemNeg->operator[](0));
    
    for(std::size_t idx = 1; idx < pDomNeg->size(); ++idx)
    {
      lowerBound = lowerBound->plus((pDomNeg->operator[](idx)->lowerBound())->times(pDomElemNeg->operator[](idx)));
      upperBound = upperBound->plus((pDomNeg->operator[](idx)->upperBound())->times(pDomElemNeg->operator[](idx)));
    }
    
    return std::make_pair(lowerBound, upperBound);
  }//calculateNegativeBounds
  
}// end namespace Core
