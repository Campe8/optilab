// Base class
#include "PropagatorLinearEqReif.hpp"

#include "PropagatorUtils.hpp"

// Domain
#include "DArrayMatrix.hpp"
#include "Domain.hpp"
#include "DomainElement.hpp"

// Internal propagators
#include "PropagatorLinearEq.hpp"
#include "PropagatorLinearNq.hpp"

#include <cassert>

namespace Core {
  
    PropagatorLinearEqReif::PropagatorLinearEqReif(DomainElementArray& aCoeffArray,
                                                   DomainArray&        aDomainArray,
                                                   DomainElement*      aConstant,
                                                   DomainBoolean*      aDomBool,
                                                   PropagatorSemanticType aReifType,
                                                   bool aUseGCDOnNormalization)
    : Core::PropagatorLinearReif(aCoeffArray, aDomainArray, aConstant, aDomBool,
    PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
    aReifType,
    PropagatorSemanticClass::PROP_SEMANTIC_CLASS_LINEAR,
    PropagationPriority::PROP_PRIORITY_LINEAR,
    aUseGCDOnNormalization)
  {
    assert(aConstant);
    assert(aDomBool);

    // Register propagators: lin Eq
    PropagatorSPtr propagatorLinEq = std::make_shared<PropagatorLinearEq>(aCoeffArray, aDomainArray,
                                                                          aConstant,
                                                                          aUseGCDOnNormalization);
    
    // Register propagators: lin Nq
    PropagatorSPtr propagatorLinNq = std::make_shared<PropagatorLinearNq>(aCoeffArray, aDomainArray,
                                                                          aConstant,
                                                                          aUseGCDOnNormalization);

    // Register propagators
    registerPropagator(propagatorLinEq);
    registerPropagator(propagatorLinNq);
  }
  
  double PropagatorLinearEqReif::getFitness()
  {
    if (!isContextReifSingleton())
    {
      return getUnspecFitness();
    }
    
    if (isContextReifTrue())
    {
      // Return fitness of propagator linear eq
      return getRegisteredPropagator(PROP_SEMANTIC_TYPE_LIN_EQ)->getFitness();
    }
    
    // Otherwise return the fitness of linear nq
    return getRegisteredPropagator(PROP_SEMANTIC_TYPE_LIN_NQ)->getFitness();
  }//getFitness
  
  PropagationEvent PropagatorLinearEqReif::post()
  {
    if (isContextReifSingleton())
    {
      if (isContextReifTrue())
      {
        // b -> Lin Eq
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_EQ);
      }
      else
      {
        // !b -> Lin Nq
        assert(isContextReifFalse());
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_NQ);
      }
    }

    PropagationEvent eventUponReification = PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    if (areAllDomainsSingleton())
    {
      if (getLeftSideExpressionValue()->isEqual(getLinearConstant()))
      {
        // Lin Eq -> b
        eventUponReification = setContextReifTrue();
      }
      else
      {
        // Lin Nq -> !b
        eventUponReification = setContextReifFalse();
      }
    }
    return eventUponReification;
  }//post
  
  PropagationEvent PropagatorLinearEqReif::runPropagation()
  {
    if (isContextReifSingleton())
    {
      if (isContextReifTrue())
      {
        // b -> Lin Eq
        return runInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_EQ);
      }
      else
      {
        // !b -> Lin Nq
        assert(isContextReifFalse());
        return runInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_NQ);
      }
    }

    if (areAllDomainsSingleton())
    {
      if (getLeftSideExpressionValue()->isEqual(getLinearConstant()))
      {
        // Lin Eq -> b
        return setContextReifTrue();
      }
      else
      {
        // Lin Nq -> !b
        return setContextReifFalse();
      }
    }

    return PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagation
  
  PropagatorHLinEqReif::PropagatorHLinEqReif(DomainElementArray& aCoeffArray,
    DomainArray&        aDomainArray,
    DomainElement*      aConstant,
    DomainBoolean*      aDomBool,
    PropagatorSemanticType aReifType,
    bool aUseGCDOnNormalization)
    : PropagatorLinearEqReif(aCoeffArray, aDomainArray, aConstant, aDomBool, aReifType,
                             aUseGCDOnNormalization)
    , pReifType(aReifType)
  {
    assert(pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_EQ_HREIF_L ||
           pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_EQ_HREIF_R);
  }

  PropagationEvent PropagatorHLinEqReif::post()
  {
    if (isContextReifSingleton())
    {
      if (isHalfReifLeft() && isContextReifTrue())
      {
        // b -> Lin Eq
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_EQ);
      }
      else if (isHalfReifRight() && isContextReifFalse())
      {
        // !b -> Lin Nq
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_NQ);
      }
    }

    PropagationEvent eventUponReification = PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    if (areAllDomainsSingleton())
    {
      bool domainEquality = getLeftSideExpressionValue()->isEqual(getLinearConstant());
      if (!domainEquality && isHalfReifLeft())
      {
        // Lin Nq -> b = false
        eventUponReification = setContextReifFalse();
      }
      else if (domainEquality && isHalfReifRight())
      {
        // Lin Eq -> b = true
        eventUponReification = setContextReifTrue();
      }
      if (eventUponReification == PropagationEvent::PROP_EVENT_FAIL)
      {
        return PropagationEvent::PROP_EVENT_FAIL;
      }
    }
    return eventUponReification;
  }//post

  PropagationEvent PropagatorHLinEqReif::runPropagation()
  {
    if (isContextReifSingleton())
    {
      if (isHalfReifLeft() && isContextReifTrue())
      {
        // b -> Lin Eq
        return runInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_EQ);
      }
      else if (isHalfReifRight() && isContextReifFalse())
      {
        // !b -> Lin Nq
        return runInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_NQ);
      }
    }

    if (areAllDomainsSingleton())
    {
      bool domainEquality = getLeftSideExpressionValue()->isEqual(getLinearConstant());
      if (domainEquality && isHalfReifRight())
      {
        //  Lin Nq -> b = false
        return setContextReifTrue();
      }
      else if (!domainEquality && isHalfReifLeft())
      {
        // Lin Eq -> b = false
        return setContextReifFalse();
      }
    }

    return PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagation

  PropagatorHLinEqReifL::PropagatorHLinEqReifL(DomainElementArray& aCoeffArray,
    DomainArray&        aDomainArray,
    DomainElement*      aConstant,
    DomainBoolean*      aDomBool,
    bool aUseGCDOnNormalization) :
    PropagatorHLinEqReif(aCoeffArray, aDomainArray, aConstant, aDomBool,
                         PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_EQ_HREIF_L,
                         aUseGCDOnNormalization)
  {
  }

  double PropagatorHLinEqReifL::getFitness()
  {
    if (!isContextReifSingleton())
    {
      return getUnspecFitness();
    }
    
    // Lin_eq <- F => 0
    if (isContextReifFalse())
    {
      return getMinFitness();
    }
    
    // // Lin_eq <- T => Lin_eq.getFitness()
    return getRegisteredPropagator(PROP_SEMANTIC_TYPE_LIN_EQ)->getFitness();
  }//getFitness

  PropagatorHLinEqReifR::PropagatorHLinEqReifR(DomainElementArray& aCoeffArray,
    DomainArray&        aDomainArray,
    DomainElement*      aConstant,
    DomainBoolean*      aDomBool,
    bool aUseGCDOnNormalization) :
    PropagatorHLinEqReif(aCoeffArray, aDomainArray, aConstant, aDomBool,
                         PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_EQ_HREIF_R,
                         aUseGCDOnNormalization)
  {
  }

  double PropagatorHLinEqReifR::getFitness()
  {
    if (!isContextReifSingleton())
    {
      return getUnspecFitness();
    }

    auto leftSideFitness = getRegisteredPropagator(PROP_SEMANTIC_TYPE_LIN_EQ)->getFitness();
    
    // T -> F => 1
    if (leftSideFitness == getMinFitness() && isContextReifFalse())
    {
      return 1.0;
    }
    
    if (leftSideFitness == getUnspecFitness())
    {
      return getUnspecFitness();
    }
    
    // T -> T      => 0
    // F -> [F, T] => 0
    return getMinFitness();
  }//getFitness
  
}// end namespace Core
