  // Base class
  #include "PropagatorLinearNqReif.hpp"

  #include "PropagatorUtils.hpp"

  // Domain
  #include "DArrayMatrix.hpp"
  #include "Domain.hpp"
  #include "DomainElement.hpp"

  // Internal propagators
  #include "PropagatorLinearEq.hpp"
  #include "PropagatorLinearNq.hpp"

  #include <cassert>

  namespace Core {
    
      PropagatorLinearNqReif::PropagatorLinearNqReif(DomainElementArray& aCoeffArray,
                                                     DomainArray&        aDomainArray,
                                                     DomainElement*      aConstant,
                                                     DomainBoolean*      aDomBool,
                                                     PropagatorSemanticType aReifType,
                                                     bool aUseGCDOnNormalization)
      : Core::PropagatorLinearReif(aCoeffArray, aDomainArray, aConstant, aDomBool,
      PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
      aReifType,
      PropagatorSemanticClass::PROP_SEMANTIC_CLASS_LINEAR,
      PropagationPriority::PROP_PRIORITY_LINEAR,
      aUseGCDOnNormalization)
    {
      assert(aConstant);
      assert(aDomBool);

      // Register propagators: lin Eq
      PropagatorSPtr propagatorLinEq = std::make_shared<PropagatorLinearEq>(aCoeffArray,
                                                                            aDomainArray, aConstant,
                                                                            aUseGCDOnNormalization);
      // Register propagators: lin Nq
      PropagatorSPtr propagatorLinNq = std::make_shared<PropagatorLinearNq>(aCoeffArray,
                                                                            aDomainArray, aConstant,
                                                                            aUseGCDOnNormalization);

      // Register propagators
      registerPropagator(propagatorLinEq);
      registerPropagator(propagatorLinNq);
    }
    
    double PropagatorLinearNqReif::getFitness()
    {
      if (!isContextReifSingleton())
      {
        return getUnspecFitness();
      }
      
      if (isContextReifTrue())
      {
        // Return fitness of propagator linear nq
        return getRegisteredPropagator(PROP_SEMANTIC_TYPE_LIN_NQ)->getFitness();
      }
      
      // Otherwise return the fitness of linear eq
      return getRegisteredPropagator(PROP_SEMANTIC_TYPE_LIN_EQ)->getFitness();
    }//getFitness
    
    PropagationEvent PropagatorLinearNqReif::post()
    {
      if (isContextReifSingleton())
      {
        if (isContextReifTrue())
        {
          // b -> Lin Nq
          return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_NQ);
        }
        else
        {
          // !b -> Lin Eq
          assert(isContextReifFalse());
          return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_EQ);
        }
      }

      PropagationEvent eventUponReification = PropagationEvent::PROP_EVENT_RUN_UNSPEC;
      if (areAllDomainsSingleton())
      {
        if (getLeftSideExpressionValue()->isNotEqual(getLinearConstant()))
        {
          // Lin Nq -> b
          eventUponReification = setContextReifTrue();
        }
        else
        {
          // Lin Eq -> !b
          eventUponReification = setContextReifFalse();
        }
      }
      return eventUponReification;
    }//post
    
    PropagationEvent PropagatorLinearNqReif::runPropagation()
    {
      if (isContextReifSingleton())
      {
        if (isContextReifTrue())
        {
          // b -> Lin Nq
          return runInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_NQ);
        }
        else
        {
          // !b -> Lin Eq
          assert(isContextReifFalse());
          return runInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_EQ);
        }
      }

      if (areAllDomainsSingleton())
      {
        if (getLeftSideExpressionValue()->isNotEqual(getLinearConstant()))
        {
          // Lin Nq -> b
          return setContextReifTrue();
        }
        else
        {
          // Lin Eq -> !b
          return setContextReifFalse();
        }
      }

      return PropagationEvent::PROP_EVENT_FIXPOINT;
    }//runPropagation
    
    PropagatorHLinNqReif::PropagatorHLinNqReif(DomainElementArray& aCoeffArray,
      DomainArray&        aDomainArray,
      DomainElement*      aConstant,
      DomainBoolean*      aDomBool,
      PropagatorSemanticType aReifType,
      bool aUseGCDOnNormalization)
      : PropagatorLinearNqReif(aCoeffArray, aDomainArray, aConstant, aDomBool, aReifType,
                               aUseGCDOnNormalization)
      , pReifType(aReifType)
    {
      assert(pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_NQ_HREIF_L ||
             pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_NQ_HREIF_R);
    }

    PropagationEvent PropagatorHLinNqReif::post()
    {
      if (isContextReifSingleton())
      {
        if (isHalfReifLeft() && isContextReifTrue())
        {
          // b -> Lin Nq
          return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_NQ);
        }
        else if (isHalfReifRight() && isContextReifFalse())
        {
          // !b -> Lin Eq
          return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_EQ);
        }
      }

      PropagationEvent eventUponReification = PropagationEvent::PROP_EVENT_RUN_UNSPEC;
      if (areAllDomainsSingleton())
      {
        bool domainDisEquality = getLeftSideExpressionValue()->isNotEqual(getLinearConstant());
        if (!domainDisEquality && isHalfReifRight())
        {
          // Lin Eq -> b = false
          eventUponReification = setContextReifFalse();
        }
        else if (domainDisEquality && isHalfReifLeft())
        {
          // Lin Nq -> b = false
          eventUponReification = setContextReifTrue();
        }
        if (eventUponReification == PropagationEvent::PROP_EVENT_FAIL)
        {
          return PropagationEvent::PROP_EVENT_FAIL;
        }
      }
      return eventUponReification;
    }//post

    PropagationEvent PropagatorHLinNqReif::runPropagation()
    {
      if (isContextReifSingleton())
      {
        if (isHalfReifLeft() && isContextReifTrue())
        {
          // b -> Lin Nq
          return runInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_NQ);
        }
        else if (isHalfReifRight() && isContextReifFalse())
        {
          // !b -> Lin Eq
          return runInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_EQ);
        }
      }

      if (areAllDomainsSingleton())
      {
        bool domainDisequality = getLeftSideExpressionValue()->isNotEqual(getLinearConstant());
        if (domainDisequality && isHalfReifRight())
        {
          // Lin Eq -> b = true
          return setContextReifTrue();
        }
        else if (!domainDisequality && isHalfReifLeft())
        {
          // Lin Nq -> b = false
          return setContextReifFalse();
        }
      }

      return PropagationEvent::PROP_EVENT_FIXPOINT;
    }//runPropagation

    PropagatorHLinNqReifL::PropagatorHLinNqReifL(DomainElementArray& aCoeffArray,
      DomainArray&        aDomainArray,
      DomainElement*      aConstant,
      DomainBoolean*      aDomBool,
      bool aUseGCDOnNormalization) :
      PropagatorHLinNqReif(aCoeffArray, aDomainArray, aConstant, aDomBool,
                           PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_NQ_HREIF_L,
                           aUseGCDOnNormalization)
    {
    }

    double PropagatorHLinNqReifL::getFitness()
    {
      if (!isContextReifSingleton())
      {
        return getUnspecFitness();
      }
      
      // Lin_nq <- F => 0
      if (isContextReifFalse())
      {
        return getMinFitness();
      }
      
      // // Lin_eq <- T => Lin_eq.getFitness()
      return getRegisteredPropagator(PROP_SEMANTIC_TYPE_LIN_NQ)->getFitness();
    }//getFitness


    PropagatorHLinNqReifR::PropagatorHLinNqReifR(DomainElementArray& aCoeffArray,
      DomainArray&        aDomainArray,
      DomainElement*      aConstant,
      DomainBoolean*      aDomBool,
      bool aUseGCDOnNormalization) :
      PropagatorHLinNqReif(aCoeffArray, aDomainArray, aConstant, aDomBool,
                           PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_NQ_HREIF_R,
                           aUseGCDOnNormalization)
    {
    }

    double PropagatorHLinNqReifR::getFitness()
    {
      if (!isContextReifSingleton())
      {
        return getUnspecFitness();
      }
      
      auto leftSideFitness = getRegisteredPropagator(PROP_SEMANTIC_TYPE_LIN_NQ)->getFitness();
      
      // T -> F => 1
      if (leftSideFitness == getMinFitness() && isContextReifFalse())
      {
        return 1.0;
      }
      
      if (leftSideFitness == getUnspecFitness())
      {
        return getUnspecFitness();
      }
      
      // T -> T      => 0
      // F -> [F, T] => 0
      return getMinFitness();
    }//getFitness

  }// end namespace Core
