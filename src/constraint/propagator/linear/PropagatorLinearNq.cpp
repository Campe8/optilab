// Base class
#include "PropagatorLinearNq.hpp"

#include "PropagatorUtils.hpp"

#include "DArrayMatrix.hpp"

#include "Domain.hpp"
#include "DomainElement.hpp"

#include <cassert>

namespace Core {
  
  PropagatorLinearNq::PropagatorLinearNq(DomainElementArray& aCoeffArray,
                                         DomainArray&        aDomainArray,
                                         DomainElement*      aConstant,
                                         bool aUseGCDOnNormalization)
  : Core::PropagatorLinear(aCoeffArray, aDomainArray, aConstant,
                           PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN,
                           PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_NQ,
                           PropagatorSemanticClass::PROP_SEMANTIC_CLASS_LINEAR,
                           PropagationPriority::PROP_PRIORITY_LINEAR,
                           aUseGCDOnNormalization)
  {
  }

  double PropagatorLinearNq::getFitness()
  {
    // Fitness for linear equation
    // return (Sum(posCoeff * posDom) - Sum(negCoeff * negDom) != linConst) ? 1 : 0
    // Note that the fitness is either 0 or 1 for a not equal relation
    
    // Coefficients
    auto posCoeff = getPosCoefficients().get();
    auto negCoeff = getNegCoefficients().get();
    
    // Domains
    auto posDom = getPosDomains().get();
    auto negDom = getNegDomains().get();
    
    // Constant
    DomainElement* linConst = getLinearConstant();
    
    assert(posCoeff->size() == posDom->size());
    assert(negCoeff->size() == negDom->size());
    assert(linConst);
    
    // No coefficients, i.e., the linear nq constraint is something like
    // 10 != 9 => 1 != 0
    if(posCoeff->size() + negCoeff->size() == 0)
    {
      return (linConst->zero()->isEqual(linConst)) ? 1 : getMinFitness();
    }
    
    // Calculate the sum:
    // Sum(posCoeff * posDom) - Sum(negCoeff * negDom)
    DomainElement* leftHandSideEquation = linConst->zero();
    for (std::size_t idx{0}; idx < negCoeff->size(); ++idx)
    {
      // Get current domain
      Domain* domain = negDom->operator[](idx);
      
      // Return if the domain is empty or not singleton
      if (PropagatorUtils::isDomainEmpty(domain))
      {
        return getInvalidFitness();
      }
      else if (!PropagatorUtils::isDomainSingleton(domain))
      {
        return getUnspecFitness();
      }
      
      // Get current coefficient and calculate
      // Sum(negCoeff * negDom)
      DomainElement* coeff = negCoeff->operator[](idx);
      leftHandSideEquation = leftHandSideEquation->plus(coeff->times(domain->upperBound()));
    }
    
    // Calculate -Sum(negCoeff * negDom)
    leftHandSideEquation = leftHandSideEquation->mirror();
    
    for (std::size_t idx{0}; idx < posCoeff->size(); ++idx)
    {
      // Get current domain
      Domain* domain = posDom->operator[](idx);
      
      // Return if the domain is empty not singleton
      if (PropagatorUtils::isDomainEmpty(domain))
      {
        return getInvalidFitness();
      }
      else if (!PropagatorUtils::isDomainSingleton(domain))
      {
        return getUnspecFitness();
      }
      
      // Get current coefficient and calculate
      // Sum(posCoeff * negDom)
      DomainElement* coeff = posCoeff->operator[](idx);
      leftHandSideEquation = leftHandSideEquation->plus(coeff->times(domain->lowerBound()));
    }
    
    // If left = k -> constraint unsatisfied, return 1
    if(leftHandSideEquation->isEqual(linConst))
    {
      return 1.0;
    }
    
    // Constraint satisfied, return 0
    return getMinFitness();
  }//getFitness
  
  PropagationEvent PropagatorLinearNq::post()
  {
    // Post linear equation
    // Sum(posCoeff * posDom) - Sum(negCoeff * negDom) != linConst
    
    // Coefficients
    auto posCoeff = getPosCoefficients().get();
    auto negCoeff = getNegCoefficients().get();
    
    // Domains
    auto posDom = getPosDomains().get();
    auto negDom = getNegDomains().get();
    
    // Constant
    DomainElement* linConst = getLinearConstant();
    
    assert(posCoeff->size() == posDom->size());
    assert(negCoeff->size() == negDom->size());
    assert(linConst);
    
    if(posCoeff->size() + negCoeff->size() == 0)
    {
      if (linConst->zero()->isEqual(linConst))
      {
        return PropagationEvent::PROP_EVENT_FAIL;
      }
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
  }//post
  
  PropagationEvent PropagatorLinearNq::runPropagation()
  {
    // Propagation of
    // Sum(posCoeff * posDom) - Sum(negCoeff * negDom) != linConst
    
    // Coefficients
    auto posCoeff = getPosCoefficients().get();
    auto negCoeff = getNegCoefficients().get();
    
    // Domains
    auto posDom = getPosDomains().get();
    auto negDom = getNegDomains().get();

    // Constant
    DomainElement* linConst = getLinearConstant();
    
    assert(posCoeff->size() == posDom->size());
    assert(negCoeff->size() == negDom->size());
    assert(linConst);
    
    // No variables: return immediately
    if (posCoeff->size() + negCoeff->size() == 0)
    {
      if (linConst->zero()->isEqual(linConst))
      {
        return PropagationEvent::PROP_EVENT_FAIL;
      }
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    assert(posCoeff->size() + negCoeff->size() != 0);
    
    DomainElement* minLeftSide = linConst->zero();
    DomainElement* maxLeftSide = linConst->zero();
    
    // The "nonSingleton" variables are needed when we propagate on a domain
    std::size_t nonSingleton{0};
    bool posNonSingleton{false};
    Domain* nonSingletonDomain{nullptr};
    DomainElement* nonSingletonElement{nullptr};
    for (std::size_t idx{0}; idx < negCoeff->size(); ++idx)
    {
      Domain* domain = negDom->operator[](idx);
      
      // @note using the value 2 here since it is possible to propagate or take
      // actions only when the number of non singleton domains is 0 or 1
      if (nonSingleton < 2 && !PropagatorUtils::isDomainSingleton(domain))
      {
        ++nonSingleton;
        nonSingletonDomain = domain;
        nonSingletonElement = negCoeff->operator[](idx)->mirror();
      }
      minLeftSide = minLeftSide->plus(negCoeff->operator[](idx)->times(domain->upperBound()));
      maxLeftSide = maxLeftSide->plus(negCoeff->operator[](idx)->times(domain->lowerBound()));
    }
    minLeftSide = minLeftSide->mirror();
    maxLeftSide = maxLeftSide->mirror();
    
    for (std::size_t idx{0}; idx < posCoeff->size(); ++idx)
    {
      Domain* domain = posDom->operator[](idx);
      
      // @note using the value 2 here since it is possible to propagate or take
      // actions only when the number of non singleton domains is 0 or 1
      if (nonSingleton < 2 && !PropagatorUtils::isDomainSingleton(domain))
      {
        ++nonSingleton;
        nonSingletonDomain = domain;
        nonSingletonElement = posCoeff->operator[](idx);
        posNonSingleton = true;
      }
      minLeftSide = minLeftSide->plus(posCoeff->operator[](idx)->times(domain->lowerBound()));
      maxLeftSide = maxLeftSide->plus(posCoeff->operator[](idx)->times(domain->upperBound()));
    }
    
    // If linConst isn't in the [leftSide, maxLeftSide] range:
    // constraint is satisfied regardless of the number of ground variables
    if (DomainElement::lessThan(linConst, minLeftSide) ||
        DomainElement::lessThan(maxLeftSide, linConst))
    {
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }

    // 2 or more variables aren't ground: can't propagate
    if (nonSingleton > 1)
    {
      return PropagationEvent::PROP_EVENT_FIXPOINT;
    }
    
    // exactly one non-ground variable:
    // delete one value from its domain, then the constraint becomes satisfied
    if(nonSingleton == 1)
    {
      assert(nonSingletonDomain);
      assert(nonSingletonElement);
      
      if(posNonSingleton)
      {
        // We use 2 domain elements, one is rounded up and the other is rounded down.
        // we subtract the value from the domain only if the two elements have the same value,
        // otherwise we can subsume without changing any domain
        DomainElement* elemToSubtractCeiling = DomainElement::plus(
            linConst,
            DomainElement::mirror(DomainElement::plus(
                    minLeftSide,
                    DomainElement::mirror(
                      (nonSingletonDomain->lowerBound()->times(nonSingletonElement))))));
        
        DomainElement* elemToSubtractFloor = DomainElementManager::getInstance().clone(elemToSubtractCeiling);
        
        elemToSubtractCeiling->setRoundingMode(DomainElement::DE_ROUNDING_CEILING);
        elemToSubtractCeiling = elemToSubtractCeiling->div(nonSingletonElement);
        
        elemToSubtractFloor->setRoundingMode(DomainElement::DE_ROUNDING_FLOOR);
        elemToSubtractFloor = elemToSubtractFloor->div(nonSingletonElement);
        
        if(elemToSubtractCeiling->isEqual(elemToSubtractFloor))
          nonSingletonDomain->subtract(elemToSubtractCeiling);
      }
      else
      {
        DomainElement* elemToSubtractCeiling = DomainElement::plus(
            linConst,
            DomainElement::mirror(DomainElement::plus(
                    minLeftSide,
                    DomainElement::mirror(
                      (nonSingletonDomain->upperBound()->times(nonSingletonElement))))));
                      
        DomainElement* elemToSubtractFloor = DomainElementManager::getInstance().clone(elemToSubtractCeiling);
        
        elemToSubtractCeiling->setRoundingMode(DomainElement::DE_ROUNDING_CEILING);
        elemToSubtractCeiling = elemToSubtractCeiling->div(nonSingletonElement);
        
        elemToSubtractFloor->setRoundingMode(DomainElement::DE_ROUNDING_FLOOR);
        elemToSubtractFloor = elemToSubtractFloor->div(nonSingletonElement);
        
        if(elemToSubtractCeiling->isEqual(elemToSubtractFloor))
          nonSingletonDomain->subtract(elemToSubtractCeiling);
      }
      
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    
    // All variables ground
    if(minLeftSide->isEqual(linConst))
    {
      return PropagationEvent::PROP_EVENT_FAIL;
    }
    return PropagationEvent::PROP_EVENT_SUBSUMED;
  }//runPropagation

}// end namespace Core
