// Base class
#include "PropagatorLinearLq.hpp"

#include "PropagatorUtils.hpp"

#include "DArrayMatrix.hpp"

#include "Domain.hpp"
#include "DomainElement.hpp"

#include <cassert>

namespace Core {
  
  PropagatorLinearLq::PropagatorLinearLq(DomainElementArray& aCoeffArray,
                                         DomainArray&        aDomainArray,
                                         DomainElement*      aConstant,
                                         bool aUseGCDOnNormalization)
  : Core::PropagatorLinear(aCoeffArray, aDomainArray, aConstant,
                           PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
                           PropagatorSemanticType::PROP_SEMANTIC_TYPE_LIN_LQ,
                           PropagatorSemanticClass::PROP_SEMANTIC_CLASS_LINEAR,
                           PropagationPriority::PROP_PRIORITY_LINEAR,
                           aUseGCDOnNormalization)
  {
    // Register filters
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
  }
  
  double PropagatorLinearLq::getFitness()
  {
    // Given
    // c_1 * x_1 + c_2 * x_2 + ... + k_n * x_n <= k
    // the fitness function should implement the following function
    // f(C, X, K) = max(0, (c_1 * x_1 + c_2 * x_2 + ... + k_n * x_n) - k)
    
    // Coefficients
    auto posCoeff = getPosCoefficients().get();
    auto negCoeff = getNegCoefficients().get();
    
    // Domains
    auto posDom = getPosDomains().get();
    auto negDom = getNegDomains().get();
    
    // Constant
    DomainElement* linConst = getLinearConstant();
    
    assert(posCoeff->size() == posDom->size());
    assert(negCoeff->size() == negDom->size());
    assert(linConst);
    
    // No coefficients, i.e., the linear lq constraint is something like
    // 10 <= 9 => 1 <= 0
    // 8 <= 9 => -1 <= 0
    if(posCoeff->size() + negCoeff->size() == 0)
    {
      return linConst->zero()->isLessThanOrEqual(linConst) ? getMinFitness() :
      PropagatorUtils::toFitness(linConst->mirror());
    }
    
    // Calculate the sum:
    // Sum(posCoeff * posDom) - Sum(negCoeff * negDom)
    DomainElement* leftHandSideEquation = linConst->zero();
    for (std::size_t idx{0}; idx < negCoeff->size(); ++idx)
    {
      // Get current domain
      Domain* domain = negDom->operator[](idx);
      
      // Return if the domain is empty or not singleton
      if (PropagatorUtils::isDomainEmpty(domain))
      {
        return getInvalidFitness();
      }
      else if (!PropagatorUtils::isDomainSingleton(domain))
      {
        return getUnspecFitness();
      }
      
      // Get current coefficient and calculate
      // Sum(negCoeff * negDom)
      DomainElement* coeff = negCoeff->operator[](idx);
      leftHandSideEquation = leftHandSideEquation->plus(coeff->times(domain->upperBound()));
    }
    
    // Calculate -Sum(negCoeff * negDom)
    leftHandSideEquation = leftHandSideEquation->mirror();
    
    for (std::size_t idx{0}; idx < posCoeff->size(); ++idx)
    {
      // Get current domain
      Domain* domain = posDom->operator[](idx);
      
      // Return if the domain is empty not singleton
      if (PropagatorUtils::isDomainEmpty(domain))
      {
        return getInvalidFitness();
      }
      else if (!PropagatorUtils::isDomainSingleton(domain))
      {
        return getUnspecFitness();
      }
      
      // Get current coefficient and calculate
      // Sum(posCoeff * negDom)
      DomainElement* coeff = posCoeff->operator[](idx);
      leftHandSideEquation = leftHandSideEquation->plus(coeff->times(domain->lowerBound()));
    }
    
    // If left <= k -> constraint unsatisfied, return 0
    if(leftHandSideEquation->isLessThanOrEqual(linConst))
    {
      return getMinFitness();
    }
    
    // Calculate (leftHandSideEquation - k)
    auto diff = leftHandSideEquation->plus(linConst->mirror());
    
    // if ((leftHandSideEquation - k) < 0)
    // {
    //    return getMinFitness();
    // }
    // else
    // {
    //    return (leftHandSideEquation - k);
    // }
    return diff->isLessThan(diff->zero()) ? getMinFitness() : PropagatorUtils::toFitness(diff);
  }//getFitness
  
  PropagationEvent PropagatorLinearLq::post()
  {
    // Post linear equation
    // Sum(posCoeff * posDom) - Sum(negCoeff * negDom) <= linConst
    
    // Coefficients
    DomainElementArraySPtr posCoeff = getPosCoefficients();
    DomainElementArraySPtr negCoeff = getNegCoefficients();
    
    // Domains
    DomainArraySPtr posDom = getPosDomains();
    DomainArraySPtr negDom = getNegDomains();
    
    // Constant
    DomainElement* linConst = getLinearConstant();
    
    assert(posCoeff->size() == posDom->size());
    assert(negCoeff->size() == negDom->size());
    assert(linConst);
    
    if(posCoeff->size() + negCoeff->size() == 0)
    {
      if (linConst->zero()->isLessThanOrEqual(linConst))
      {
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
      return PropagationEvent::PROP_EVENT_FAIL;
    }
    return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
  }//post
  
  PropagationEvent PropagatorLinearLq::runPropagation()
  {
    // Propagation of
    // Sum(posCoeff * posDom) - Sum(negCoeff * negDom) <= linConst
    
    // Coefficients
    DomainElementArraySPtr posCoeff = getPosCoefficients();
    DomainElementArraySPtr negCoeff = getNegCoefficients();
    
    // Domains
    DomainArraySPtr posDom = getPosDomains();
    DomainArraySPtr negDom = getNegDomains();
    
    // Constant
    DomainElement* linConst = getLinearConstant();
    
    assert(posCoeff->size() == posDom->size());
    assert(negCoeff->size() == negDom->size());
    assert(linConst);
    
    if (posCoeff->size() + negCoeff->size() == 0)
    {
      if (linConst->zero()->isLessThanOrEqual(linConst))
      {
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
      return PropagationEvent::PROP_EVENT_FAIL;
    }
    
    assert(posCoeff->size() + negCoeff->size() != 0);
    
    // Calculate starting bounds
    auto posBounds = calculatePositiveBounds();
    auto negBounds = calculateNegativeBounds();

    // Propagate on bounds ->
    // iterate until fix point is reached
    bool eventChanged;
    do
    {
      eventChanged = false;
      {
        // Upper bound on positive domains
        // UB_j = min(UB_j, alpha)
        // where 
        // j \in POS
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
        for(std::size_t idx = 0; idx < posCoeff->size(); ++idx)
        {
          Domain *outDomain = posDom->operator[](idx);

          // Skip singletons
          if (PropagatorUtils::isDomainSingleton(outDomain))
          {
            continue;
          }

          DomainEvent* preFilterEvent = outDomain->getEvent();
          
          setFilterOutputPtr(outDomain);
          
          DomainElement *alpha = getAlpha(posCoeff->operator[](idx),
                                          posDom->operator[](idx),
                                          posBounds.first, negBounds.second);
          setFilterInput(0, outDomain->upperBound()->min(alpha));
          
          outDomain = applyFilteringStrategy();
          DOMAIN_FAILURE_CHECK(outDomain);
          
          // Keep iterating until reaching the fix point
          eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
        }
        // Update bounds
        if (eventChanged)
        {
          posBounds = calculatePositiveBounds();
          negBounds = calculateNegativeBounds();
        }
      }

      {
        // Lower bound on negative domains
        // LB_j = max(LB_j, beta)
        // where 
        // j \in NEG
        setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
        for(std::size_t idx = 0; idx < negCoeff->size(); ++idx)
        {
          Domain *outDomain = negDom->operator[](idx);

          // Skip singletons
          if (PropagatorUtils::isDomainSingleton(outDomain))
          {
            continue;
          }

          DomainEvent* preFilterEvent = outDomain->getEvent();
          
          setFilterOutputPtr(outDomain);
          
          DomainElement *beta = getBeta(negCoeff->operator[](idx),
                                        negDom->operator[](idx),
                                        posBounds.first, negBounds.second);
          setFilterInput(0, outDomain->lowerBound()->max(beta));
          
          outDomain = applyFilteringStrategy();
          DOMAIN_FAILURE_CHECK(outDomain);
          
          // Keep iterating until reaching the fix point
          eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
        }
        // Update bounds
        if (eventChanged)
        {
          posBounds = calculatePositiveBounds();
          negBounds = calculateNegativeBounds();
        }
      }
    } while (eventChanged);
    
    DomainElement *leftSideLinearEquation = linConst->zero();
    for(std::size_t idx = 0; idx < negCoeff->size(); ++idx)
    {
      Domain *domain = negDom->operator[](idx);
      if(!PropagatorUtils::isDomainSingleton(domain))
      {
        return PropagationEvent::PROP_EVENT_FIXPOINT;
      }
      leftSideLinearEquation = leftSideLinearEquation->plus(negCoeff->operator[](idx)->times(PropagatorUtils::getSingleton(domain)));
    }
    
    leftSideLinearEquation = leftSideLinearEquation->mirror();
    for(std::size_t idx = 0; idx < posCoeff->size(); ++idx)
    {
      Domain *domain = posDom->operator[](idx);
      if(!PropagatorUtils::isDomainSingleton(domain))
      {
        return PropagationEvent::PROP_EVENT_FIXPOINT;
      }
      leftSideLinearEquation = leftSideLinearEquation->plus(posCoeff->operator[](idx)->times(PropagatorUtils::getSingleton(domain)));
    }
    
    if(leftSideLinearEquation->isLessThanOrEqual(linConst))
    {
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    return PropagationEvent::PROP_EVENT_FAIL;
  }//runPropagation
  
  DomainElement* PropagatorLinearLq::getAlpha(DomainElement* aCoeff, Domain* aDomain,
                                              DomainElement* aPosLowerBound, DomainElement* aNegUpperBound)
  {
    assert(aCoeff);
    assert(aDomain);
    
    DomainElement* coeffLower = aCoeff->times(aDomain->lowerBound());
    DomainElement* sumPosNormalized = aPosLowerBound ? ((aPosLowerBound->plus(coeffLower->mirror()))->mirror()) : aCoeff->zero();
    if (!aNegUpperBound)
    {
      aNegUpperBound = aCoeff->zero();
    }
    
    DomainElement* dividend = (getLinearConstant()->plus(sumPosNormalized))->plus(aNegUpperBound);
    
    auto oldRoundingMode = dividend->getRoundingMode();
    dividend->setRoundingMode(Core::DomainElement::RoundingMode::DE_ROUNDING_FLOOR);
    DomainElement* divToReturn = dividend->div(aCoeff);
    dividend->setRoundingMode(oldRoundingMode);
    return divToReturn;
  }//getAlpha
  
  DomainElement* PropagatorLinearLq::getBeta(DomainElement* aCoeff, Domain* aDomain,
                                             DomainElement* aPosLowerBound, DomainElement* aNegUpperBound)
  {
    assert(aCoeff);
    assert(aDomain);
    
    DomainElement* coeffUpper= aCoeff->times(aDomain->upperBound());
    DomainElement* sumNegNormalized = aNegUpperBound ? ((aNegUpperBound->plus(coeffUpper->mirror()))->mirror()) : aCoeff->zero();
    if (!aPosLowerBound)
    {
      aPosLowerBound = aCoeff->zero();
    }
    
    DomainElement* dividend = ((getLinearConstant()->mirror())->plus(aPosLowerBound))->plus(sumNegNormalized);
    
    auto oldRoundingMode = dividend->getRoundingMode();
    dividend->setRoundingMode(Core::DomainElement::RoundingMode::DE_ROUNDING_CEILING);
    DomainElement* divToReturn = dividend->div(aCoeff);
    dividend->setRoundingMode(oldRoundingMode);
    return divToReturn;
  }//getBeta

}// end namespace Core
