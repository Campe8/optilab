// Base class
#include "PropagationCost.hpp"

#include <cassert>

namespace Core {
  
  
  bool PropagationCostSort(const PropagationCost &lhs, const PropagationCost &rhs) 
  {
    return (static_cast<int>(lhs.getCost()) < static_cast<int>(rhs.getCost()));
  }
  
  PropagationCost::PropagationCost(PropagationPriority aPriority)
  : pPriority(aPriority)
  {
  }
  
  PropagationCost::PropagationCost(const PropagationCost& aOther)
  {
    pPriority = aOther.pPriority;
  }
  
  PropagationCost::PropagationCost(PropagationCost&& aOther)
  {
    pPriority = aOther.pPriority;
  }
  
  PropagationCost::~PropagationCost()
  {
  }
  
  PropagationCost& PropagationCost::operator=(const PropagationCost& aOther)
  {
    if (&aOther == this)
    {
      return *this;
    }
    
    pPriority = aOther.pPriority;
    return *this;
  }
  
  PropagationCost& PropagationCost::operator=(PropagationCost&& aOther)
  {
    if (&aOther == this)
    {
      return *this;
    }
    
    pPriority = aOther.pPriority;
    return *this;
  }
  
}// end namespace Core
