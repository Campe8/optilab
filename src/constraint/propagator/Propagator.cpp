// Base class
#include "Propagator.hpp"

// Filters
#include "FilteringAlgorithmFactory.hpp"

// Propagator utilities
#include "PropagatorUtils.hpp"

#include <memory>

namespace Core {
  
  Propagator::Propagator(const PropagatorSemantics& aPropagatorSemantics)
  : pPropagatorEnabled(true)
  , pPropagatorSemantics(aPropagatorSemantics)
  , pPropagatorCost(aPropagatorSemantics.PropPriority)
  , pFilterToUse(nullptr)
  , pContext(aPropagatorSemantics.ContextSize, nullptr)
  {
    pPropagatorSemantics = aPropagatorSemantics;
    
    // Set filter parameters according to the given propagator semantics
    pFilterParams.setInputDomainArray(std::make_shared<DomainArray>(pPropagatorSemantics.ContextSize));
    pFilterParams.setDomainElementArray(std::make_shared<DomainElementArray>(pPropagatorSemantics.ContextSize));
  }
  
  double Propagator::getMinFitness()
  {
    static double fitness = 0;
    return fitness;
  }//getMinFitness
  
  double Propagator::getUnspecFitness()
  {
    return getMinFitness();
  }//getUnspecFitness
  
  double Propagator::getInvalidFitness()
  {
    static double fitness = -1;
    return fitness;
  }//getMinFitness
  
  PropagationEvent Propagator::propagate()
  {
    // Run propagation only if enabled
    if (pPropagatorEnabled && isContextRegistered())
    {
      // If there is already an empty domain
      // in the context return failure event
      if (isAnyContextDomainEmpty())
      {
        return PropagationEvent::PROP_EVENT_FAIL;
      }
      
      // Run actual propagation
      return runPropagation();
    }
    
    return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
  }//propagate
  
  void Propagator::registerFilteringStrategy(FilteringAlgorithmSemanticType aType)
  {
    assert(aType != FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_UNDEF);
    if (!isFilteringAlgorithmRegistered(aType))
    {
      pFilterAlgorithmRegister[static_cast<int>(aType)] = FilteringAlgorithmFactory::getFilteringAlgorithmInstance(aType);
    }
  }//registerFilteringAlgorithm
  
  void Propagator::registerPropagator(PropagatorSPtr aPropagator)
  {
    assert(aPropagator);
    auto typeId = aPropagator->getPropagatorTypeId();
    assert(typeId != PropagatorSemanticType::PROP_SEMANTIC_TYPE_UNDEF);
    
    pPropagatorRegister[static_cast<int>(typeId)] = aPropagator;
  }//registerPropagator
  
  PropagatorSPtr Propagator::getRegisteredPropagator(PropagatorSemanticType aPropagatorSemanticType)
  {
    int typeId = static_cast<int>(aPropagatorSemanticType);
    if(pPropagatorRegister.find(typeId) == pPropagatorRegister.end())
    {
      return nullptr;
    }
    return pPropagatorRegister.at(typeId);
  }//getRegisteredPropagator
  
  std::size_t Propagator::numSingletonContextDomains() const
  {
    std::size_t numSingletons{ 0 };
    for (std::size_t dIdx = 0; dIdx < getContextSize(); ++dIdx)
    {
      if (PropagatorUtils::isDomainSingleton(getContextDomain(dIdx)))
      {
        numSingletons++;
      }
    }
    
    return numSingletons;
  }//numSingletonContextDomains
  
  bool Propagator::isContextRegistered() const
  {
    for (std::size_t dIdx = 0; dIdx < getContextSize(); ++dIdx)
    {
      if(getContextDomain(dIdx))
      {
        return true;
      }
    }
    return false;
  }//isContextRegistered
  
  bool Propagator::isAnyContextDomainEmpty() const
  {
    for (std::size_t dIdx = 0; dIdx < getContextSize(); ++dIdx)
    {
      Domain *domain = getContextDomain(dIdx);
      assert(domain);
      
      if (PropagatorUtils::isDomainEmpty(domain))
      {
        return true;
      }
    }
    return false;
  }//isAnyContextDomainEmpty
  
}// end namespace Core
