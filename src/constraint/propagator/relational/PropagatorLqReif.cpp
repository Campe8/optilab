// Base class
#include "PropagatorLqReif.hpp"

// Propagators
#include "PropagatorLq.hpp"
#include "PropagatorLt.hpp"

// Utility
#include "PropagatorUtils.hpp"

#include <memory>
#include <cassert>

namespace Core {
  
  PropagatorLqReif::PropagatorLqReif(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif, PropagatorSemanticType aSemanticType)
  : PropagatorBinaryReif(aDom0, aDom1, aDomReif,
    PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
    aSemanticType,
    PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL)
  {
    assert(aDom0);
    assert(aDom1);
    assert(aDomReif);

    // Propagator Lq
    PropagatorSPtr propagatorLq = std::make_shared<PropagatorLq>(getContextDomain(D0), getContextDomain(D1));
    // Propagator Lt
    PropagatorSPtr propagatorLt = std::make_shared<PropagatorLt>(getContextDomain(D1), getContextDomain(D0));

    // Register propagators
    registerPropagator(propagatorLq);
    registerPropagator(propagatorLt);
  }
  
  PropagatorLqReif::~PropagatorLqReif()
  {
  }
  
  double PropagatorLqReif::getFitness()
  {
    if(isContextReifTrue())
    {
      return getRegisteredPropagator(PROP_SEMANTIC_TYPE_LQ)->getFitness();
    }
    else if(isContextReifFalse())
    {
      return getRegisteredPropagator(PROP_SEMANTIC_TYPE_LT)->getFitness();
    }
    else
    {
      return getUnspecFitness();
    }
  }//getFitness
  
  PropagationEvent PropagatorLqReif::post()
  {
    if (isContextReifSingleton())
    {
      if (isContextReifTrue())
      {
        // b -> D0 <= D1
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ);
      }
      else
      {
        // !b -> D1 < D0
        assert(isContextReifFalse());
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LT);
      }
    }

    PropagationEvent eventUponReification = PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)) && PropagatorUtils::isDomainSingleton(getContextDomain(D1)))
    {
      
      if (PropagatorUtils::getSingleton(getContextDomain(D0))->isLessThanOrEqual(PropagatorUtils::getSingleton(getContextDomain(D1))))
      {
        // D0 <= D1 -> b
        eventUponReification = setContextReifTrue();
      }
      else
      {
        // D1 < D0 -> !b
        eventUponReification = setContextReifFalse();
      }
    }
    
    return eventUponReification;
  }//post
  
  PropagationEvent PropagatorLqReif::runPropagation()
  {
    if (isContextReifSingleton())
    {
      if (isContextReifTrue())
      {
        // b -> D0 <= D1
        return runInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ);
      }
      else
      {
        // !b -> D1 < D0
        assert(isContextReifFalse());
        return runInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LT);
      }
    }

    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)) && PropagatorUtils::isDomainSingleton(getContextDomain(D1)))
    {
      if (PropagatorUtils::getSingleton(getContextDomain(D0))->isLessThanOrEqual(PropagatorUtils::getSingleton(getContextDomain(D1))))
      {
        // D0 <= D1 -> b
        return setContextReifTrue();
      }
      else
      {
        // D1 < D0 -> !b
        return setContextReifFalse();
      }
    }

    return PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagation

  PropagatorHLqReif::PropagatorHLqReif(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif, PropagatorSemanticType aReifType)
    : PropagatorLqReif(aDom0, aDom1, aDomReif, aReifType)
    , pReifType(aReifType)
  {
    assert(pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ_HREIF_L ||
      pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ_HREIF_R);
  }

  PropagatorHLqReif::~PropagatorHLqReif()
  {
  }

  double PropagatorHLqReif::getFitness()
  {
    Domain* dom0 = getContextDomain(D0);
    Domain* dom1 = getContextDomain(D1);
    Domain* dom2 = GetContextReifDomain();

    if (PropagatorUtils::isDomainEmpty(dom0) || PropagatorUtils::isDomainEmpty(dom1) ||
        PropagatorUtils::isDomainEmpty(dom2))
    {
      return getInvalidFitness();
    }
    
    if (!PropagatorUtils::isDomainSingleton(dom0) || !PropagatorUtils::isDomainSingleton(dom1) ||
        !PropagatorUtils::isDomainSingleton(dom2))
    {
      return getUnspecFitness();
    }

    if(isHalfReifLeft())
    {
      if(isContextReifTrue())
      {
        return getRegisteredPropagator(PROP_SEMANTIC_TYPE_LQ)->getFitness();
      }
      else if(isContextReifFalse())
      {
        return getMinFitness();
      }
    }
    else if(isHalfReifRight())
    {
      if(dom0->lowerBound()->isLessThanOrEqual(dom1->lowerBound()))
      {
        if(isContextReifTrue())
        {
          return getMinFitness();
        }
        else
        {
          return 1.0;
        }
      }
      else
      {
        return getMinFitness();
      }
    }
    
    return getUnspecFitness();
  }//getFitness
  
  PropagationEvent PropagatorHLqReif::post()
  {
    if (isContextReifSingleton())
    {
      if (isHalfReifLeft() && isContextReifTrue())
      {
        // b -> D0 <= D1
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ);
      }
      else if (isHalfReifRight() && isContextReifFalse())
      {
        // !b -> D1 < D0
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LT);
      }
    }

    PropagationEvent eventUponReification = PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)) && PropagatorUtils::isDomainSingleton(getContextDomain(D1)))
    {
      bool domainEquality = PropagatorUtils::getSingleton(getContextDomain(D0))->isLessThanOrEqual(PropagatorUtils::getSingleton(getContextDomain(D1)));
      if (domainEquality && isHalfReifRight())
      {
        // D0 <= D1 -> b
        eventUponReification = setContextReifTrue();
      }
      else if (!domainEquality && isHalfReifLeft())
      {
        // D1 < D0 -> !b
        eventUponReification = setContextReifFalse();
      }
      if (eventUponReification == PropagationEvent::PROP_EVENT_FAIL)
      {
        return PropagationEvent::PROP_EVENT_FAIL;
      }
    }

    return eventUponReification;
  }//post

  PropagationEvent PropagatorHLqReif::runPropagation()
  {
    if (isContextReifSingleton())
    {
      if (isHalfReifLeft() && isContextReifTrue())
      {
        // b -> D0 <= D1
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ);
      }
      else if (isHalfReifRight() && isContextReifFalse())
      {
        // !b -> D1 < D0
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LT);
      }
    }

    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)) && PropagatorUtils::isDomainSingleton(getContextDomain(D1)))
    {
      bool domainEquality = PropagatorUtils::getSingleton(getContextDomain(D0))->isLessThanOrEqual(PropagatorUtils::getSingleton(getContextDomain(D1)));
      if (domainEquality && isHalfReifRight())
      {
        // D0 <= D1 -> b
        return setContextReifTrue();
      }
      else if (!domainEquality && isHalfReifLeft())
      {
        // D1 < D0 -> !b
        return setContextReifFalse();
      }
    }

    return PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagation

  PropagatorLqHReifL::PropagatorLqHReifL(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif)
    : PropagatorHLqReif(aDom0, aDom1, aDomReif, PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ_HREIF_L)
  {
  }

  PropagatorLqHReifL::~PropagatorLqHReifL()
  {
  }

  PropagatorLqHReifR::PropagatorLqHReifR(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif)
    : PropagatorHLqReif(aDom0, aDom1, aDomReif, PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ_HREIF_R)
  {
  }

  PropagatorLqHReifR::~PropagatorLqHReifR()
  {
  }

}// end namespace Core


