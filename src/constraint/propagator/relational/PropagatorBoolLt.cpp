// Base class
#include "PropagatorBoolLt.hpp"

// Utility
#include "PropagatorUtils.hpp"

#include <memory>
#include <cassert>

namespace Core {
  
  PropagatorBoolLt::PropagatorBoolLt(DomainBoolean *aDom0, DomainBoolean *aDom1)
  : PropagatorBinary(aDom0, aDom1, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
                     PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_LT,
                     PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL)
  {
    assert(aDom0);
    assert(aDom1);
  }

  double PropagatorBoolLt::getFitness()
  {
    // Given lt(X, Y), implements the function:
    // (X == 0  &&  Y == 1) ? 0 : 1 ;
    if (numSingletonContextDomains() !=  2)
    {
      return getUnspecFitness();
    }
    
    if (DomainBoolean::cast(getContextDomain(D0))->isFalse() &&
        DomainBoolean::cast(getContextDomain(D1))->isTrue())
    {
      return getMinFitness();
    }
    
    return 1.0;
  }//getFitness
  
  PropagationEvent PropagatorBoolLt::post()
  {
    /*
     * Post on !a /\ b:
     * a is False AND b is True -> subsumed.
     */
    DomainBoolean::cast(getContextDomain(D0))->setFalse();
    DomainBoolean::cast(getContextDomain(D1))->setTrue();
    if (PropagatorUtils::isDomainEmpty(getContextDomain(D0)) || PropagatorUtils::isDomainEmpty(getContextDomain(D1)))
    {
      return PropagationEvent::PROP_EVENT_FAIL;
    }

    return PropagationEvent::PROP_EVENT_SUBSUMED;
  }//post
  
  PropagationEvent PropagatorBoolLt::runPropagation()
  {
    std::size_t singletonDomains = numSingletonContextDomains();
    assert(singletonDomains <= 2);

    if (singletonDomains == 2)
    {
      if (DomainBoolean::cast(getContextDomain(D0))->isFalse() && DomainBoolean::cast(getContextDomain(D1))->isTrue())
      {
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }

      // All other cases are failure cases
      return PropagationEvent::PROP_EVENT_FAIL;
    }

    DomainBoolean::cast(getContextDomain(D0))->setFalse();
    DomainBoolean::cast(getContextDomain(D1))->setTrue();
    if (PropagatorUtils::isDomainEmpty(getContextDomain(D0)) || PropagatorUtils::isDomainEmpty(getContextDomain(D1)))
    {
      return PropagationEvent::PROP_EVENT_FAIL;
    }

    return PropagationEvent::PROP_EVENT_SUBSUMED;
  }//runPropagation
  
}// end namespace Core

