// Base class
#include "PropagatorLq.hpp"

// Utility
#include "PropagatorUtils.hpp"

#include <memory>
#include <cassert>

namespace Core {
  
  PropagatorLq::PropagatorLq(Domain *aDom0, Domain *aDom1)
  : PropagatorBinary(aDom0, aDom1, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
                     PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ,
                     PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL)
  {
    assert(aDom0);
    assert(aDom1);
    
    // Register filters
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
  }

  double PropagatorLq::getFitness()
  {
    // Fitness function, given X <= Y returns max(0, X - Y)
    Domain* dom0 = getContextDomain(D0);
    Domain* dom1 = getContextDomain(D1);
    
    // Check if the domains are empty or not singleton, if so return
    if (PropagatorUtils::isDomainEmpty(dom0) || PropagatorUtils::isDomainEmpty(dom1))
    {
      return getInvalidFitness();
    }
    
    if (!PropagatorUtils::isDomainSingleton(dom0) || !PropagatorUtils::isDomainSingleton(dom1))
    {
      return getUnspecFitness();
    }
    
    auto sum = dom0->lowerBound()->plus(dom1->lowerBound()->mirror());
    
    // Return ((X - Y) < 0) ? 0 : (X - Y);
    if (sum->isLessThan(sum->zero())) return getMinFitness();
    
    return PropagatorUtils::toFitness(sum);
  }//getFitness
  
  PropagationEvent PropagatorLq::post()
  {
    std::size_t singletonDomains = numSingletonContextDomains();
    if (singletonDomains == 2)
    {
      if (!PropagatorUtils::getSingleton(getContextDomain(D0))->isLessThanOrEqual(PropagatorUtils::getSingleton(getContextDomain(D1))))
      {
        return PropagationEvent::PROP_EVENT_FAIL;
      }
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    
    Domain *outDomain{ nullptr };

    // D0 <= D1.UB
    setFilterOutput(D0);
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
    setFilterInput(0, getContextDomain(D1)->upperBound());
    outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);
    
    // D1 >= D0.LB
    setFilterOutput(D1);
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
    setFilterInput(0, getContextDomain(D0)->lowerBound());
    outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);

    return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
  }//post
  
  PropagationEvent PropagatorLq::runPropagation()
  {
    Domain* domD0 = getContextDomain(D0);
    Domain* domD1 = getContextDomain(D1);
    Domain *outDomain{ nullptr };

    // D0 <= D1.UB
    setFilterOutput(D0);
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
    setFilterInput(0, domD1->upperBound());
    outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);

    // D1 >= D0.LB
    setFilterOutput(D1);
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
    setFilterInput(0, domD0->lowerBound());
    outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);
    
    if (domD0->upperBound()->isLessThanOrEqual(domD1->lowerBound()))
    {
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    
    return PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagation
  
}// end namespace Core

