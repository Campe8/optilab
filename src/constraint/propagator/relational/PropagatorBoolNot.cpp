// Base class
#include "PropagatorBoolNot.hpp"

// Utility
#include "PropagatorUtils.hpp"

#include <memory>
#include <cassert>

namespace Core {
  
  PropagatorBoolNot::PropagatorBoolNot(DomainBoolean *aDom0, DomainBoolean *aDom1)
  : PropagatorBinary(aDom0, aDom1, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
                     PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_NOT,
                     PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL)
  {
    assert(aDom0);
    assert(aDom1);
  }
  
  double PropagatorBoolNot::getFitness()
  {
    // Given not(X, Y), implements the function:
    // (X == 1 && Y == 0) || (X == 0 && Y == 1) ? 0 : 1
    if (numSingletonContextDomains() !=  2)
    {
      return getUnspecFitness();
    }
    
    if ((DomainBoolean::cast(getContextDomain(D0))->isTrue() &&
        DomainBoolean::cast(getContextDomain(D1))->isFalse()) ||
        (DomainBoolean::cast(getContextDomain(D0))->isFalse() &&
         DomainBoolean::cast(getContextDomain(D1))->isTrue()))
    {
      return getMinFitness();
    }
    
    return 1.0;
  }//getFitness
  
  PropagationEvent PropagatorBoolNot::post()
  {
    // Post on !a == b
    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)))
    {
      if (DomainBoolean::cast(getContextDomain(D0))->isTrue())
      {
        DomainBoolean::cast(getContextDomain(D1))->setFalse();
      }
      else
      {
        assert(DomainBoolean::cast(getContextDomain(D0))->isFalse());
        DomainBoolean::cast(getContextDomain(D1))->setTrue();
      }
      
      DOMAIN_FAILURE_CHECK(getContextDomain(D1));
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    else if (PropagatorUtils::isDomainSingleton(getContextDomain(D1)))
    {
      if (DomainBoolean::cast(getContextDomain(D1))->isTrue())
      {
        DomainBoolean::cast(getContextDomain(D0))->setFalse();
      }
      else
      {
        assert(DomainBoolean::cast(getContextDomain(D1))->isFalse());
        DomainBoolean::cast(getContextDomain(D0))->setTrue();
      }
      
      DOMAIN_FAILURE_CHECK(getContextDomain(D0));
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    
    return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
  }//post
  
  PropagationEvent PropagatorBoolNot::runPropagation()
  {
    auto event = post();
    if (event == PropagationEvent::PROP_EVENT_RUN_UNSPEC)
    {
      return PropagationEvent::PROP_EVENT_FIXPOINT;
    }
    return event;
  }//runPropagation
  
}// end namespace Core

