// Base class
#include "PropagatorBoolXor.hpp"

#include "PropagatorEq.hpp"
#include "PropagatorNq.hpp"

// Utility
#include "PropagatorUtils.hpp"

#include <memory>
#include <cassert>

namespace Core {
  
  PropagatorBoolXorBin::PropagatorBoolXorBin(DomainBoolean *aDom0, DomainBoolean *aDom1)
  : PropagatorBinary(aDom0, aDom1, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
    PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_XOR_BIN,
    PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL)
  {
    assert(aDom0);
    assert(aDom1);

    PropagatorSPtr propagatorEq = std::make_shared<PropagatorEqBound>(aDom0, aDom1);
    PropagatorSPtr propagatorNq = std::make_shared<PropagatorNq>(aDom0, aDom1);

    // Register propagators
    registerPropagator(propagatorEq);
    registerPropagator(propagatorNq);
  }
  
  double PropagatorBoolXorBin::getFitness()
  {
    // Given xor(X, Y), implements the function:
    // X != Y ? 0 : 1 ;
    if (numSingletonContextDomains() !=  2)
    {
      return getUnspecFitness();
    }
    
    if ((DomainBoolean::cast(getContextDomain(D0))->isTrue() &&
         DomainBoolean::cast(getContextDomain(D1))->isFalse()) ||
        (DomainBoolean::cast(getContextDomain(D0))->isFalse() &&
         DomainBoolean::cast(getContextDomain(D1))->isTrue()))
    {
      return getMinFitness();
    }
    
    return 1.0;
  }//getFitness
  
  PropagationEvent PropagatorBoolXorBin::post()
  {
    /*
     * Post on a != b:
     * a is True AND b is False -> subsumed
     * a is False AND b is True -> subsumed
     * a is False -> b must be True
     * a is True  -> b must be False
     * b is False -> a must be True
     * b is True  -> a must be False
     */
    std::size_t singletonDomains = numSingletonContextDomains();
    assert(singletonDomains <= 2);

    if (singletonDomains == 0)
    {
      return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    }

    // At least one domain is singleton
    if (singletonDomains == 2)
    {
      if ((DomainBoolean::cast(getContextDomain(D0))->isTrue() &&
           DomainBoolean::cast(getContextDomain(D1))->isFalse()) ||
         (DomainBoolean::cast(getContextDomain(D0))->isFalse() &&
          DomainBoolean::cast(getContextDomain(D1))->isTrue()))
      {
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
      
      return PropagationEvent::PROP_EVENT_FAIL;
    }

    // Only one domain is singleton
    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)))
    {
      if (DomainBoolean::cast(getContextDomain(D0))->isTrue())
      {
        DomainBoolean::cast(getContextDomain(D1))->setFalse();
      }
      else
      {
        assert(DomainBoolean::cast(getContextDomain(D0))->isFalse());
        DomainBoolean::cast(getContextDomain(D1))->setTrue();
      }
      
      if (PropagatorUtils::isDomainEmpty(getContextDomain(D1)))
      {
        return PropagationEvent::PROP_EVENT_FAIL;
      }
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }

    assert(PropagatorUtils::isDomainSingleton(getContextDomain(D1)));
    if (DomainBoolean::cast(getContextDomain(D1))->isTrue())
    {
      DomainBoolean::cast(getContextDomain(D0))->setFalse();
    }
    else
    {
      assert(DomainBoolean::cast(getContextDomain(D1))->isFalse());
      DomainBoolean::cast(getContextDomain(D0))->setTrue();
    }
    
    if (PropagatorUtils::isDomainEmpty(getContextDomain(D0)))
    {
      return PropagationEvent::PROP_EVENT_FAIL;
    }
    
    return PropagationEvent::PROP_EVENT_SUBSUMED;
  }//post
  
  PropagationEvent PropagatorBoolXorBin::runPropagation()
  {
    auto event = post();
    if (event == PropagationEvent::PROP_EVENT_RUN_UNSPEC)
    {
      return PropagationEvent::PROP_EVENT_FIXPOINT;
    }

    return event;
  }//runPropagation
  
  PropagatorBoolXorTer::PropagatorBoolXorTer(DomainBoolean *aDom0, DomainBoolean *aDom1, DomainBoolean *aDom2)
    : PropagatorBinaryReif(aDom0, aDom1, aDom2, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
      PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_XOR_TER,
      PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL)
  {
    assert(aDom0);
    assert(aDom1);
    assert(aDom2);

    // Register propagators: Bool Or Bin
    PropagatorSPtr propagatorBoolOrBin = std::make_shared<PropagatorBoolXorBin>(aDom0, aDom1);

    // Register propagator
    registerPropagator(propagatorBoolOrBin);
  }
  
  double PropagatorBoolXorTer::getFitness()
  {
    auto domA = DomainBoolean::cast(getContextDomain(D0));
    auto domB = DomainBoolean::cast(getContextDomain(D1));
    assert(domA);
    assert(domB);
    
    if (domA->getSize() != 1 || domB->getSize() != 1)
    {
      return getUnspecFitness();
    }
    
    if(isContextReifTrue())
    {
      if ((domA->isTrue() && domB->isFalse()) || (domA->isFalse() && domB->isTrue()))
      {
        return getMinFitness();
      }
      
      return 1.0;
    }
    else if(isContextReifFalse())
    {
      if ((domA->isFalse() && domB->isFalse()) || (domA->isTrue() && domB->isTrue()))
      {
        return getMinFitness();
      }
      
      return 1.0;
    }
    else
    {
      return getUnspecFitness();
    }
  }//getFitness
  
  PropagationEvent PropagatorBoolXorTer::post()
  {
    /*
     * Post
     * (a != b) <-> r
     * if r is True  -> run a != b;
     * if r is False -> run a == b;
     * if a != b -> r must be True;
     * if a == b -> r must be False.
     */
    if (isContextReifSingleton())
    {
      PropagatorSPtr internalPropagator { nullptr };
      if (isContextReifTrue())
      {
        internalPropagator = std::make_shared<PropagatorNq>(getContextDomain(D0), getContextDomain(D1));
        registerPropagator(internalPropagator);
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_NQ);
      }
      
      assert(isContextReifFalse());
      internalPropagator = std::make_shared<PropagatorEqBound>(getContextDomain(D0), getContextDomain(D1));
      registerPropagator(internalPropagator);  
      return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ);
    }

    std::size_t singletonDomains = numSingletonContextDomains();
    assert(singletonDomains <= 2);
    if (singletonDomains == 2)
    {
      // If a AND b are singletons, evaluate a != b and set r
      bool isTrueD0 = DomainBoolean::cast(getContextDomain(D0))->isTrue();
      bool isTrueD1 = DomainBoolean::cast(getContextDomain(D1))->isTrue();
      if (isTrueD0 != isTrueD1)
      {
        setContextReifTrue();
      }
      else
      {
        setContextReifFalse();
      }
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }

    // No actions possible in other cases, for example, no action possible
    // if a is True, b is unset and r is unset, return PROP_EVENT_RUN_UNSPEC
    return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
  }// post

  PropagationEvent PropagatorBoolXorTer::runPropagation()
  {
    auto event = post();
    
    std::size_t singletonDomains = numSingletonContextDomains();
    if (singletonDomains == 3)
    {
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    
    if (event == PropagationEvent::PROP_EVENT_RUN_UNSPEC)
    {
      return PropagationEvent::PROP_EVENT_FIXPOINT;
    }

    return event;
  }//runPropagation

}// end namespace Core

