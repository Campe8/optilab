// Base class
#include "PropagatorBoolOr.hpp"

// Utility
#include "PropagatorUtils.hpp"

#include <memory>
#include <cassert>

namespace Core {
  
  PropagatorBoolOrBin::PropagatorBoolOrBin(DomainBoolean *aDom0, DomainBoolean *aDom1)
  : PropagatorBinary(aDom0, aDom1, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
                     PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_OR_BIN,
                     PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL)
  {
    assert(aDom0);
    assert(aDom1);
  }
  
  double PropagatorBoolOrBin::getFitness()
  {
    // Given or(X, Y), implements the function:
    // (X == 1 ||  Y == 1) ? 0 : 1 ;
    if (numSingletonContextDomains() !=  2)
    {
      return getUnspecFitness();
    }
    
    if (DomainBoolean::cast(getContextDomain(D0))->isTrue() ||
        DomainBoolean::cast(getContextDomain(D1))->isTrue())
    {
      return getMinFitness();
    }
    
    return 1.0;
  }//getFitness
  
  PropagationEvent PropagatorBoolOrBin::post()
  {
    /*
     * Post on a \/ b:
     * a is True OR b is True -> subsumed
     * a is False -> b must be True
     * b is False -> a must be True
     */
    std::size_t singletonDomains = numSingletonContextDomains();
    assert(singletonDomains <= 2);

    if (singletonDomains == 0)
    {
      return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    }

    // At least one domain is singleton
    if (singletonDomains == 2)
    {
      if (DomainBoolean::cast(getContextDomain(D0))->isTrue() ||
          DomainBoolean::cast(getContextDomain(D1))->isTrue())
      {
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
      return PropagationEvent::PROP_EVENT_FAIL;
    }

    // Only one domain is singleton
    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)))
    {
      if (DomainBoolean::cast(getContextDomain(D0))->isTrue())
      {
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
      
      DomainBoolean::cast(getContextDomain(D1))->setTrue();
      if (PropagatorUtils::isDomainEmpty(getContextDomain(D1)))
      {
        return PropagationEvent::PROP_EVENT_FAIL;
      }
      
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }

    assert(PropagatorUtils::isDomainSingleton(getContextDomain(D1)));
    if (DomainBoolean::cast(getContextDomain(D1))->isTrue())
    {
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    
    DomainBoolean::cast(getContextDomain(D0))->setTrue();
    if (PropagatorUtils::isDomainEmpty(getContextDomain(D0)))
    {
      return PropagationEvent::PROP_EVENT_FAIL;
    }
    
    return PropagationEvent::PROP_EVENT_SUBSUMED;
  }//post
  
  PropagationEvent PropagatorBoolOrBin::runPropagation()
  {
    auto event = post();
    if (event == PropagationEvent::PROP_EVENT_RUN_UNSPEC)
    {
      return PropagationEvent::PROP_EVENT_FIXPOINT;
    }

    return event;
  }//runPropagation
  
  PropagatorBoolOrTer::PropagatorBoolOrTer(DomainBoolean *aDom0, DomainBoolean *aDom1, DomainBoolean *aDom2)
    : PropagatorBinaryReif(aDom0, aDom1, aDom2, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
      PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_OR_TER,
      PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL)
  {
    assert(aDom0);
    assert(aDom1);
    assert(aDom2);
  }

  double PropagatorBoolOrTer::getFitness()
  {
    auto domA = DomainBoolean::cast(getContextDomain(D0));
    auto domB = DomainBoolean::cast(getContextDomain(D1));
    assert(domA);
    assert(domB);
    
    if (domA->getSize() != 1 || domB->getSize() != 1)
    {
      return getUnspecFitness();
    }
    
    if(isContextReifTrue())
    {
      // a \/ b must be true
      if (domA->isTrue() || domB->isTrue())
      {
        return getMinFitness();
      }
      
      return 1.0;
    }
    else if(isContextReifFalse())
    {
      // a \/ b must be false
      if (domA->isFalse() && domB->isFalse())
      {
        return getMinFitness();
      }
      
      return 1.0;
    }
    else
    {
      return getUnspecFitness();
    }
  }//getFitness
  
  PropagationEvent PropagatorBoolOrTer::post()
  {
    /*
     * Post
     * (a \/ b) <-> r
     * 1 - r is False -> a AND b must be False
     * 2 - r is True  -> (a \/ b) must be true, check singletons
     * 3 - (a \/ b) is True -> r must be True
     * 4 - (a \/ b) is False -> r must be False
     */
    std::size_t singletonDomains = numSingletonContextDomains();
    assert(singletonDomains <= 2);

    if (isContextReifSingleton())
    {
      if (isContextReifFalse())
      {
        DomainBoolean::cast(getContextDomain(D0))->setFalse();
        DomainBoolean::cast(getContextDomain(D1))->setFalse();
        DOMAIN_FAILURE_CHECK(getContextDomain(D0));
        DOMAIN_FAILURE_CHECK(getContextDomain(D1));
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
      else
      {
        assert(isContextReifTrue());
        if (singletonDomains == 0)
        {
          // Neither a nor b is singleton, no action can be taken: return PROP_EVENT_RUN_UNSPEC
          return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
        }
        if ((PropagatorUtils::isDomainSingleton(getContextDomain(D0)) && DomainBoolean::cast(getContextDomain(D0))->isTrue()) ||
            (PropagatorUtils::isDomainSingleton(getContextDomain(D1)) && DomainBoolean::cast(getContextDomain(D1))->isTrue()))
        {
          // a or b are singleton and True -> propagator subsumed
          return PropagationEvent::PROP_EVENT_SUBSUMED;
        }
        else if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)) && DomainBoolean::cast(getContextDomain(D0))->isFalse())
        {
          // a is singleton and False -> b must be True since r is True
          DomainBoolean::cast(getContextDomain(D1))->setTrue();
          DOMAIN_FAILURE_CHECK(getContextDomain(D1));
          return PropagationEvent::PROP_EVENT_SUBSUMED;
        }
        else
        {
          assert(PropagatorUtils::isDomainSingleton(getContextDomain(D1)) && DomainBoolean::cast(getContextDomain(D1))->isFalse());
          // b is singleton and False -> a must be True since r is True
          DomainBoolean::cast(getContextDomain(D0))->setTrue();
          DOMAIN_FAILURE_CHECK(getContextDomain(D0));
          return PropagationEvent::PROP_EVENT_SUBSUMED;
        }
      }
    }// isContextReifSingleton

    // Propagate on r if possible
    if (singletonDomains >= 1)
    {
      if ((PropagatorUtils::isDomainSingleton(getContextDomain(D0)) && DomainBoolean::cast(getContextDomain(D0))->isTrue()) ||
          (PropagatorUtils::isDomainSingleton(getContextDomain(D1)) && DomainBoolean::cast(getContextDomain(D1))->isTrue()))
      {
        // a or b is True -> r must be True
        setContextReifTrue();
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
      if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)) && DomainBoolean::cast(getContextDomain(D0))->isFalse() &&
          PropagatorUtils::isDomainSingleton(getContextDomain(D1)) && DomainBoolean::cast(getContextDomain(D1))->isFalse())
      {
        // Both a and b are False -> r must be False
        setContextReifFalse();
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
    }

    // No actions are possible on other cases: return PROP_EVENT_RUN_UNSPEC
    return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
  }// post

  PropagationEvent PropagatorBoolOrTer::runPropagation()
  {
    auto event = post();
    if (event == PropagationEvent::PROP_EVENT_RUN_UNSPEC)
    {
      return PropagationEvent::PROP_EVENT_FIXPOINT;
    }

    return event;
  }//runPropagation

}// end namespace Core

