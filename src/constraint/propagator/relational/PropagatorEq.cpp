// Base class
#include "PropagatorEq.hpp"

#include "DomainElementInt64.hpp"
#include "DomainElementReal.hpp"

// Utility
#include "PropagatorUtils.hpp"

#include <memory>
#include <cassert>

namespace Core {
  
  PropagatorEq::PropagatorEq(Domain *aDom0, Domain *aDom1,
                             PropagatorStrategyType aStrategyType,
                             PropagationPriority aPriority)
  : PropagatorBinary(aDom0, aDom1, aStrategyType,
                     PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ,
                     PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL,
                     aPriority)
  {
    assert(aDom0);
    assert(aDom1);
    
    // Register filters
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_EQ);
  }
  
  double PropagatorEq::getFitness()
  {
    // Fitness for equality propagator
    // Implements the function f(X, Y) = abs(X - Y)
    
    Domain* dom0 = getContextDomain(D0);
    Domain* dom1 = getContextDomain(D1);
    
    // Check if the domains are empty or not singleton, if so return
    if (PropagatorUtils::isDomainEmpty(dom0) || PropagatorUtils::isDomainEmpty(dom1))
    {
      return getInvalidFitness();
    }
    
    if (!PropagatorUtils::isDomainSingleton(dom0) || !PropagatorUtils::isDomainSingleton(dom1))
    {
      return getUnspecFitness();
    }
    
    // Calculate abs(lb1 - lb0) as
    // lb0 = abs(lb0);
    // lb1 = abs(lb1);
    // absDiff = lb1 > lb0 ? (lb1 - lb0) : (lb0 - lb1);
    DomainElement* lb0 = dom0->lowerBound()->abs();
    DomainElement* lb1 = dom1->lowerBound()->abs();
    
    DomainElement* absDiff{nullptr};
    if (lb0->isLessThan(lb1))
    {
      absDiff = lb1->plus(lb0->mirror());
    }
    else
    {
      absDiff = lb0->plus(lb1->mirror());
    }
    assert(absDiff);
    
    // Get the type of domain element and return the cast to double of the value
    // represented by absDiff
    return PropagatorUtils::toFitness(absDiff);
  }//getFitness
  
  PropagationEvent PropagatorEq::post()
  {
    std::size_t singletonDomains = numSingletonContextDomains();
    if (singletonDomains == 2)
    {
      if (PropagatorUtils::getSingleton(getContextDomain(D0))->isNotEqual(PropagatorUtils::getSingleton(getContextDomain(D1))))
      {
        return PropagationEvent::PROP_EVENT_FAIL;
      }
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    
    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)))
    {
      // Propagate on D1
      setFilterOutput(D1);
      
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_EQ);
      setFilterInput(0, PropagatorUtils::getSingleton(getContextDomain(D0)));
      
      Domain *outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
    }
    else if (PropagatorUtils::isDomainSingleton(getContextDomain(D1)))
    {
      // Propagate on D0
      setFilterOutput(D0);
      
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_EQ);
      setFilterInput(0, PropagatorUtils::getSingleton(getContextDomain(D1)));
      
      Domain *outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
    }
    else
    {
      // Filter D0 <= D1.UB /\ D1 <= D0.UB
      Domain* dom0 = getContextDomain(D0);
      Domain* dom1 = getContextDomain(D1);
      Domain *outDomain{ nullptr };
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
      
      setFilterOutput(D0);
      setFilterInput(0, dom1->upperBound());
      outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      setFilterOutput(D1);
      setFilterInput(0, dom0->upperBound());
      outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      // Filter D0 >= D1.LB /\ D1 >= D0.LB
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
      
      setFilterOutput(D0);
      setFilterInput(0, dom1->lowerBound());
      outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      setFilterOutput(D1);
      setFilterInput(0, dom0->lowerBound());
      outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
    }
    
    if (numSingletonContextDomains() == 2)
    {
      assert(PropagatorUtils::getSingleton(getContextDomain(D0))->isEqual(PropagatorUtils::getSingleton(getContextDomain(D1))));
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    
    return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
  }//post
  
  PropagatorEqBound::PropagatorEqBound(Domain *aDom0, Domain *aDom1)
  : PropagatorEq(aDom0, aDom1, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND)
  {
  }
  
  PropagationEvent PropagatorEqBound::runPropagation()
  {
    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)))
    {
      // Propagate on D1
      setFilterOutput(D1);
      
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_EQ);
      setFilterInput(0, PropagatorUtils::getSingleton(getContextDomain(D0)));
      
      Domain *outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    else if (PropagatorUtils::isDomainSingleton(getContextDomain(D1)))
    {
      // Propagate on D0
      setFilterOutput(D0);
      
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_EQ);
      setFilterInput(0, PropagatorUtils::getSingleton(getContextDomain(D1)));
      
      Domain *outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    else
    {
      // No singletons, shrink on bounds
      Domain *outDomain{ nullptr };
      Domain *dom0 = getContextDomain(D0);
      Domain *dom1 = getContextDomain(D1);
      
      // Shrink on lower bound D0.LB == D1.LB
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQ);
      do
      {
        setFilterOutput(D0);
        setFilterInput(0, dom1->lowerBound());
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        setFilterOutput(D1);
        setFilterInput(0, dom0->lowerBound());
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
      } while (dom0->lowerBound()->isNotEqual(dom1->lowerBound()));
      
      // Shrink on upper bound D0.UB == D1.UB
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LQ);
      do
      {
        setFilterOutput(D0);
        setFilterInput(0, dom1->upperBound());
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
        
        setFilterOutput(D1);
        setFilterInput(0, dom0->upperBound());
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
      } while (dom0->upperBound()->isNotEqual(dom1->upperBound()));
      
      if (PropagatorUtils::isDomainSingleton(dom0))
      {
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
    }
    
    return PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagation
  
  PropagatorEqDomain::PropagatorEqDomain(Domain *aDom0, Domain *aDom1)
  : PropagatorEq(aDom0, aDom1,
                 PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN,
                 PropagationPriority::PROP_PRIORITY_LINEAR)
  {
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_INTERSECT);
  }
  
  PropagationEvent PropagatorEqDomain::runPropagation()
  {
    // Intersect D0 with D1 and vice-versa to make domains equal
    // iterate until failure or fix-point
    bool eventChanged;
    Domain *dom0 = getContextDomain(D0);
    Domain *dom1 = getContextDomain(D1);
    Domain *outDomain{ nullptr };
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_INTERSECT);
    do
    {
      eventChanged = false;
      {
        // Propagate on D0
        setFilterOutput(D0);
        
        // Get event pre-filtering
        DomainEvent* preFilterEvent = dom0->getEvent();
    
        setFilterInput(0, dom1);
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
      
        // Check if something changed
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
      }
      {
        // Propagate on D1
        setFilterOutput(D1);
        
        // Get event pre-filtering
        DomainEvent* preFilterEvent = dom1->getEvent();
        
        setFilterInput(0, dom0);
        outDomain = applyFilteringStrategy();
        DOMAIN_FAILURE_CHECK(outDomain);
      
        // Check if something changed
        eventChanged |= (*preFilterEvent != *(outDomain->getEvent()));
      }
    } while (eventChanged);
    
    if (PropagatorUtils::isDomainSingleton(dom0))
    {
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    return PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagation
  
}// end namespace Core


