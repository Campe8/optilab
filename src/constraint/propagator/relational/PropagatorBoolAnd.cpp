// Base class
#include "PropagatorBoolAnd.hpp"

// Utility
#include "PropagatorUtils.hpp"

#include <memory>
#include <cassert>

namespace Core {
  
  PropagatorBoolAndBin::PropagatorBoolAndBin(DomainBoolean *aDom0, DomainBoolean *aDom1)
  : PropagatorBinary(aDom0, aDom1, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
                     PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_AND_BIN,
                     PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL)
  {
    assert(aDom0);
    assert(aDom1);
  }
  
  double PropagatorBoolAndBin::getFitness()
  {
    // Given and(X, Y), implements the function:
    // (X == 1 && Y == 1) ? 0 : 1 ;
    if (numSingletonContextDomains() !=  2)
    {
      return getUnspecFitness();
    }
    
    if (DomainBoolean::cast(getContextDomain(D0))->isTrue() &&
        DomainBoolean::cast(getContextDomain(D1))->isTrue())
    {
      return getMinFitness();
    }
    
    return 1.0;
  }//getFitness
  
  PropagationEvent PropagatorBoolAndBin::post()
  {
    /*
     * Post on a /\ b:
     * a AND b must be both True.
     */
    DomainBoolean::cast(getContextDomain(D0))->setTrue();
    DomainBoolean::cast(getContextDomain(D1))->setTrue();
    if (PropagatorUtils::isDomainEmpty(getContextDomain(D0)) ||
        PropagatorUtils::isDomainEmpty(getContextDomain(D1)))
    {
      return PropagationEvent::PROP_EVENT_FAIL;
    }

    return PropagationEvent::PROP_EVENT_SUBSUMED;
  }//post
  
  PropagationEvent PropagatorBoolAndBin::runPropagation()
  {
    return post();
  }//runPropagation
  
  PropagatorBoolAndTer::PropagatorBoolAndTer(DomainBoolean *aDom0, DomainBoolean *aDom1, DomainBoolean *aDom2)
    : PropagatorBinaryReif(aDom0, aDom1, aDom2, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
      PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_AND_TER,
      PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL)
  {
    assert(aDom0);
    assert(aDom1);
    assert(aDom2);
  }
  
  double PropagatorBoolAndTer::getFitness()
  {
    auto domA = DomainBoolean::cast(getContextDomain(D0));
    auto domB = DomainBoolean::cast(getContextDomain(D1));
    assert(domA);
    assert(domB);
    
    if (domA->getSize() != 1 || domB->getSize() != 1)
    {
      return getUnspecFitness();
    }
    
    if(isContextReifTrue())
    {
      // a /\ b must be true
      if (domA->isTrue() && domB->isTrue())
      {
        return getMinFitness();
      }
      
      return 1.0;
    }
    else if(isContextReifFalse())
    {
      // a /\ b must be false
      if ((domA->isTrue() && domB->isFalse()) || (domA->isFalse() && domB->isTrue()))
      {
        return getMinFitness();
      }
      
      return 1.0;
    }
    else
    {
      return getUnspecFitness();
    }
  }//getFitness

  PropagationEvent PropagatorBoolAndTer::post()
  {
    /*
     * Post
     * (a /\ b) <-> r
     * 1 - r is False -> a OR b must be False, check singletons
     * 2 - r is True  -> (a /\ b) must be true
     * 3 - (a /\ b) is True -> r must be True
     * 4 - (a /\ b) is False -> r must be False
     */
    std::size_t singletonDomains = numSingletonContextDomains();
    assert(singletonDomains <= 2);

    if (isContextReifSingleton())
    {
      if (isContextReifFalse())
      {
        if (singletonDomains == 0)
        {
          // Neither a nor b is singleton, no action can be taken: return UNDEF
          return PropagationEvent::PROP_EVENT_UNDEF;
        }
        if ((PropagatorUtils::isDomainSingleton(getContextDomain(D0)) && DomainBoolean::cast(getContextDomain(D0))->isFalse()) ||
            (PropagatorUtils::isDomainSingleton(getContextDomain(D1)) && DomainBoolean::cast(getContextDomain(D1))->isFalse()))
        {
          // a or b are singleton and False -> propagator subsumed
          return PropagationEvent::PROP_EVENT_SUBSUMED;
        }
        else if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)) && DomainBoolean::cast(getContextDomain(D0))->isTrue())
        {
          // a is singleton and True -> b must be False since r is False
          DomainBoolean::cast(getContextDomain(D1))->setFalse();
          DOMAIN_FAILURE_CHECK(getContextDomain(D1));
          return PropagationEvent::PROP_EVENT_SUBSUMED;
        }
        else
        {
          assert(PropagatorUtils::isDomainSingleton(getContextDomain(D1)) && DomainBoolean::cast(getContextDomain(D1))->isTrue());
          // b is singleton and True -> a must be True since r is False
          DomainBoolean::cast(getContextDomain(D0))->setFalse();
          DOMAIN_FAILURE_CHECK(getContextDomain(D0));
          return PropagationEvent::PROP_EVENT_SUBSUMED;
        }
      }
      else
      {
        assert(isContextReifTrue());
        DomainBoolean::cast(getContextDomain(D0))->setTrue();
        DomainBoolean::cast(getContextDomain(D1))->setTrue();
        DOMAIN_FAILURE_CHECK(getContextDomain(D0));
        DOMAIN_FAILURE_CHECK(getContextDomain(D1));
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
    }// isContextReifSingleton

    // Propagate on r if possible
    if (singletonDomains >= 1)
    {
      if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)) && DomainBoolean::cast(getContextDomain(D0))->isTrue() &&
          PropagatorUtils::isDomainSingleton(getContextDomain(D1)) && DomainBoolean::cast(getContextDomain(D1))->isTrue())
      {
        // a and b are True -> r must be True
        setContextReifTrue();
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
      if ((PropagatorUtils::isDomainSingleton(getContextDomain(D0)) && DomainBoolean::cast(getContextDomain(D0))->isFalse()) ||
          (PropagatorUtils::isDomainSingleton(getContextDomain(D1)) && DomainBoolean::cast(getContextDomain(D1))->isFalse()))
      {
        // a or b is False -> r must be False
        setContextReifFalse();
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
    }

    // No actions are possible on other cases: return undef
    return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
  }// post

  PropagationEvent PropagatorBoolAndTer::runPropagation()
  {
    auto event = post();
    if (event == PropagationEvent::PROP_EVENT_RUN_UNSPEC)
    {
      return PropagationEvent::PROP_EVENT_FIXPOINT;
    }

    return event;
  }//runPropagation

}// end namespace Core

