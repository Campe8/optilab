// Base class
#include "PropagatorLt.hpp"

// Utility
#include "PropagatorUtils.hpp"

#include <memory>
#include <cassert>

namespace Core {
  
  PropagatorLt::PropagatorLt(Domain *aDom0, Domain *aDom1)
  : PropagatorBinary(aDom0, aDom1, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
                     PropagatorSemanticType::PROP_SEMANTIC_TYPE_LT,
                     PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL)
  {
    assert(aDom0);
    assert(aDom1);
    
    // Register filters
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LT);
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GT);
  }
  
  double PropagatorLt::getFitness()
  {
    // Fitness function, given X < Y returns max(0, 1 + X - Y)
    Domain* dom0 = getContextDomain(D0);
    Domain* dom1 = getContextDomain(D1);
    
    // Check if the domains are empty or not singleton, if so return
    if (PropagatorUtils::isDomainEmpty(dom0) || PropagatorUtils::isDomainEmpty(dom1))
    {
      return getInvalidFitness();
    }
    
    if (!PropagatorUtils::isDomainSingleton(dom0) || !PropagatorUtils::isDomainSingleton(dom1))
    {
      return getUnspecFitness();
    }
    
    auto sum = dom0->lowerBound()->plus(dom1->lowerBound()->mirror());
    sum = sum->plus(sum->zero()->successor());
    
    // Return ((1 + X - Y) < 0) ? 0 : (1 + X - Y);
    if (sum->isLessThan(sum->zero())) return getMinFitness();
    
    return PropagatorUtils::toFitness(sum);
  }//getFitness
  
  PropagationEvent PropagatorLt::post()
  {
    std::size_t singletonDomains = numSingletonContextDomains();
    if (singletonDomains == 2)
    {
      if (!PropagatorUtils::getSingleton(getContextDomain(D0))->isLessThan(PropagatorUtils::getSingleton(getContextDomain(D1))))
      {
        return PropagationEvent::PROP_EVENT_FAIL;
      }
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    
    Domain *outDomain{ nullptr };

    // D0 < D1.UB
    setFilterOutput(D0);
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LT);
    setFilterInput(0, getContextDomain(D1)->upperBound());
    outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);

    // D1 > D0.LB
    setFilterOutput(D1);
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GT);
    setFilterInput(0, getContextDomain(D0)->lowerBound());
    outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);

    return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
  }//post
  
  PropagationEvent PropagatorLt::runPropagation()
  {
    Domain* domD0 = getContextDomain(D0);
    Domain* domD1 = getContextDomain(D1);
    Domain *outDomain{ nullptr };

    // D0 < D1.UB
    setFilterOutput(D0);
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LT);
    setFilterInput(0, domD1->upperBound());
    outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);

    // D1 > D0.LB
    setFilterOutput(D1);
    setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GT);
    setFilterInput(0, domD0->lowerBound());
    outDomain = applyFilteringStrategy();
    DOMAIN_FAILURE_CHECK(outDomain);
    
    if (domD0->upperBound()->isLessThanOrEqual(domD1->lowerBound()))
    {
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }

    return PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagation
  
}// end namespace Core

