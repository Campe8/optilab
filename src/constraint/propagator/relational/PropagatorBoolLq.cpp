// Base class
#include "PropagatorBoolLq.hpp"

// Utility
#include "PropagatorUtils.hpp"

#include <memory>
#include <cassert>

namespace Core {
  
    PropagatorBoolLq::PropagatorBoolLq(DomainBoolean *aDom0, DomainBoolean *aDom1)
  : PropagatorBinary(aDom0, aDom1, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
                     PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_LQ,
                     PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL)
  {
    assert(aDom0);
    assert(aDom1);
  }
  
  double PropagatorBoolLq::getFitness()
  {
    // Given lq(X, Y), implements the function:
    // ((X == 0 && Y == 0) || (Y == 1)) ? 0 : 1 ;
    if (numSingletonContextDomains() !=  2)
    {
      return getUnspecFitness();
    }
    
    if (DomainBoolean::cast(getContextDomain(D1))->isTrue() ||
        (DomainBoolean::cast(getContextDomain(D0))->isFalse() &&
         DomainBoolean::cast(getContextDomain(D1))->isFalse()))
    {
      return getMinFitness();
    }
    
    return 1.0;
  }//getFitness
  
  PropagationEvent PropagatorBoolLq::post()
  {
    /*
     * Post on !a \/ b:
     * 1 - a is False -> subsumed;
     * 2 - b is True  -> subsumed;
     * 3 - a is True  -> b  == True ? subsumed : failed;
     * 4 - b is False -> a  == False ? subsumed : failed;
     * else
     * post Lq.
     */
    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)) &&
        DomainBoolean::cast(getContextDomain(D0))->isFalse())
    {
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    
    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)) &&
        DomainBoolean::cast(getContextDomain(D0))->isTrue())
    {
      DomainBoolean::cast(getContextDomain(D1))->setTrue();
      DOMAIN_FAILURE_CHECK(getContextDomain(D1));
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    
    if (PropagatorUtils::isDomainSingleton(getContextDomain(D1)) &&
        DomainBoolean::cast(getContextDomain(D1))->isTrue())
    {
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    
    if (PropagatorUtils::isDomainSingleton(getContextDomain(D1)) &&
        DomainBoolean::cast(getContextDomain(D1))->isFalse())
    {
      DomainBoolean::cast(getContextDomain(D0))->setFalse();
      DOMAIN_FAILURE_CHECK(getContextDomain(D0));
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    
    // Cases (3) and (4) hold when both domains are singletons
    std::size_t singletonDomains = numSingletonContextDomains();
    if (singletonDomains == 2)
    {
      if (DomainBoolean::cast(getContextDomain(D0))->isTrue())
      {
        return DomainBoolean::cast(getContextDomain(D1))->isTrue() ?
        PropagationEvent::PROP_EVENT_SUBSUMED : 
        PropagationEvent::PROP_EVENT_FAIL;
      }
      else if (DomainBoolean::cast(getContextDomain(D1))->isFalse())
      {
        return DomainBoolean::cast(getContextDomain(D0))->isFalse() ?
          PropagationEvent::PROP_EVENT_SUBSUMED :
          PropagationEvent::PROP_EVENT_FAIL;
      }
    }

    return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
  }//post
  
  PropagationEvent PropagatorBoolLq::runPropagation()
  {
    auto propagationEvent = post();
    
    if(propagationEvent == PropagationEvent::PROP_EVENT_RUN_UNSPEC)
    {
      return PropagationEvent::PROP_EVENT_FIXPOINT;
    }
    
    return propagationEvent;
  }//runPropagation
  
}// end namespace Core

