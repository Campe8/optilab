// Base class
#include "PropagatorBoolLqReif.hpp"

#include "PropagatorLq.hpp"
#include "PropagatorLqReif.hpp"

// Utility
#include "PropagatorUtils.hpp"

#include <memory>
#include <cassert>

namespace Core {
  
  PropagatorBoolLqReif::PropagatorBoolLqReif(DomainBoolean *aDom0, DomainBoolean *aDom1, DomainBoolean *aReif)
    : PropagatorBinaryReif(aDom0, aDom1, aReif, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
                     PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_LQ_REIF,
                     PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL)
  {
    assert(aDom0);
    assert(aDom1);

    // Register propagator Lq
    PropagatorSPtr propagatorLq = std::make_shared<PropagatorLq>(aDom0, aDom1);
    registerPropagator(propagatorLq);
  }
  
  double PropagatorBoolLqReif::getFitness()
  {
    if ((numSingletonContextDomains() + (isContextReifSingleton() ? 1 : 0)) != 3)
    {
      return getUnspecFitness();
    }
    
    if (isContextReifFalse())
    {
      // r false -> b < a
      if (DomainBoolean::cast(getContextDomain(D0))->isTrue() &&
          DomainBoolean::cast(getContextDomain(D1))->isFalse())
      {
        return getMinFitness();
      }
      
      return 1.0;
    }
    else
    {
      // r true -> a <= b
      if (DomainBoolean::cast(getContextDomain(D1))->isTrue() ||
          (DomainBoolean::cast(getContextDomain(D0))->isFalse() &&
           DomainBoolean::cast(getContextDomain(D1))->isFalse()))
      {
        return getMinFitness();
      }
      
      return 1.0;
    }
  }//PropagatorBoolLqReif
  
  PropagationEvent PropagatorBoolLqReif::post()
  {
    // Post on (!a \/ b) <-> r
    std::size_t singletonDomains = numSingletonContextDomains();
    assert(singletonDomains <= 2);

    if (isContextReifSingleton())
    {
      if (isContextReifFalse())
      {
        // r is false -> b < a
        DomainBoolean::cast(getContextDomain(D0))->setTrue();
        DomainBoolean::cast(getContextDomain(D1))->setFalse();

        DOMAIN_FAILURE_CHECK(getContextDomain(D0));
        DOMAIN_FAILURE_CHECK(getContextDomain(D1));
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
      // r is true -> a <= b
      assert(isContextReifTrue());
      return postInternalPropagator(PROP_SEMANTIC_TYPE_LQ);
    }

    // r is not singleton
    if (singletonDomains == 2)
    {
      assert(PropagatorUtils::isDomainSingleton(getContextDomain(D0)));
      assert(PropagatorUtils::isDomainSingleton(getContextDomain(D1)));

      if (DomainBoolean::cast(getContextDomain(D0))->isTrue() && DomainBoolean::cast(getContextDomain(D1))->isFalse())
      {
        // a is True and b is False -> r must be false
        setContextReifFalse();
      }
      else
      {
        // All other cases are valid cases
        setContextReifTrue();
      }
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }

    if (singletonDomains == 0)
    {
      return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    }
    
    // One domain is singleton
    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)))
    {
      if (DomainBoolean::cast(getContextDomain(D0))->isFalse())
      {
        // a is False -> b can be True or False, r must be True
        setContextReifTrue();
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
      return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    }

    assert(PropagatorUtils::isDomainSingleton(getContextDomain(D1)));
    if (DomainBoolean::cast(getContextDomain(D1))->isTrue())
    {
      // b is True -> a can be True or False, r must be True
      setContextReifTrue();
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
  }//post
  
  PropagationEvent PropagatorBoolLqReif::runPropagation()
  {
    auto event = post();
    if (event == PropagationEvent::PROP_EVENT_RUN_UNSPEC)
    {
      return PropagationEvent::PROP_EVENT_FIXPOINT;
    }
    return event;
  }//runPropagation
  
  PropagatorBoolLqHReifL::PropagatorBoolLqHReifL(DomainBoolean *aDom0, DomainBoolean *aDom1, DomainBoolean *aReif)
    : PropagatorBinary(aDom0, aDom1, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
    PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_LQ_HREIF_L,
    PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL)
  {
    assert(aDom0);
    assert(aDom1);
    PropagatorSPtr propagatorLqHReifL = std::make_shared<PropagatorLqHReifL>(aDom0, aDom1, aReif);
    registerPropagator(propagatorLqHReifL);
  }

  double PropagatorBoolLqHReifL::getFitness()
  {
    auto fitnessValue = getRegisteredPropagator(PROP_SEMANTIC_TYPE_LQ_HREIF_L)->getFitness();
    if (fitnessValue == getUnspecFitness() || fitnessValue == getMinFitness())
    {
      return fitnessValue;
    }
    
    return 1.0;
  }//getFitness

  PropagatorBoolLqHReifR::PropagatorBoolLqHReifR(DomainBoolean *aDom0, DomainBoolean *aDom1, DomainBoolean *aReif)
    : PropagatorBinary(aDom0, aDom1, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
    PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_LQ_HREIF_R,
    PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL)
  {
    assert(aDom0);
    assert(aDom1);
    PropagatorSPtr propagatorLqHReifR = std::make_shared<PropagatorLqHReifR>(aDom0, aDom1, aReif);
    registerPropagator(propagatorLqHReifR);
  }

  double PropagatorBoolLqHReifR::getFitness()
  {
    auto fitnessValue = getRegisteredPropagator(PROP_SEMANTIC_TYPE_LQ_HREIF_R)->getFitness();
    if (fitnessValue == getUnspecFitness() || fitnessValue == getMinFitness())
    {
      return fitnessValue;
    }
    
    return 1.0;
  }//getFitness

}// end namespace Core

