// Base class
#include "PropagatorBoolLtReif.hpp"

#include "PropagatorLt.hpp"
#include "PropagatorLtReif.hpp"

// Utility
#include "PropagatorUtils.hpp"

#include <memory>
#include <cassert>

namespace Core {
  
  PropagatorBoolLtReif::PropagatorBoolLtReif(DomainBoolean *aDom0, DomainBoolean *aDom1, DomainBoolean *aReif)
    : PropagatorBinaryReif(aDom0, aDom1, aReif, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
                     PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_LT_REIF,
                     PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL)
  {
    assert(aDom0);
    assert(aDom1);
  }
  
  double PropagatorBoolLtReif::getFitness()
  {
    if ((numSingletonContextDomains() + (isContextReifSingleton() ? 1 : 0)) != 3)
    {
      return getUnspecFitness();
    }
    
    if (isContextReifFalse())
    {
      // r false -> b < a
      if (DomainBoolean::cast(getContextDomain(D0))->isTrue() &&
          DomainBoolean::cast(getContextDomain(D1))->isFalse())
      {
        return getMinFitness();
      }
      
      return 1.0;
    }
    else
    {
      // r true -> a < b
      if (DomainBoolean::cast(getContextDomain(D0))->isFalse() &&
          DomainBoolean::cast(getContextDomain(D1))->isTrue())
      {
        return getMinFitness();
      }
      
      return 1.0;
    }
  }//getFitness
  
  PropagationEvent PropagatorBoolLtReif::post()
  {
    // Post on (!a /\ b) <-> r
    std::size_t singletonDomains = numSingletonContextDomains();
    assert(singletonDomains <= 2);

    if (isContextReifSingleton())
    {
      if (isContextReifFalse())
      {
        // r false -> b < a
        DomainBoolean::cast(getContextDomain(D0))->setTrue();
        DomainBoolean::cast(getContextDomain(D1))->setFalse();

        DOMAIN_FAILURE_CHECK(getContextDomain(D0));
        DOMAIN_FAILURE_CHECK(getContextDomain(D1));
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
      // r true -> a < b 
      assert(isContextReifTrue());
      DomainBoolean::cast(getContextDomain(D1))->setTrue();
      DomainBoolean::cast(getContextDomain(D0))->setFalse();

      DOMAIN_FAILURE_CHECK(getContextDomain(D0));
      DOMAIN_FAILURE_CHECK(getContextDomain(D1));
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }

    // r is not singleton
    if (singletonDomains == 2)
    {
      assert(PropagatorUtils::isDomainSingleton(getContextDomain(D0)));
      assert(PropagatorUtils::isDomainSingleton(getContextDomain(D1)));

      if (DomainBoolean::cast(getContextDomain(D0))->isTrue() || DomainBoolean::cast(getContextDomain(D1))->isFalse())
      {
        // a is True or b is False -> r must be false
        setContextReifFalse();
      }
      else
      {
        // All other cases are valid cases
        setContextReifTrue();
      }
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }

    if (singletonDomains == 0)
    {
      return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    }
    
    // One domain is singleton
    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)))
    {
      if (DomainBoolean::cast(getContextDomain(D0))->isTrue())
      {
        // a is True -> r must be True
        setContextReifFalse();
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
      return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    }

    assert(PropagatorUtils::isDomainSingleton(getContextDomain(D1)));
    if (DomainBoolean::cast(getContextDomain(D1))->isFalse())
    {
      // b is False -> r must be True
      setContextReifTrue();
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
  }//post
  
  PropagationEvent PropagatorBoolLtReif::runPropagation()
  {
    auto event = post();
    if (event == PropagationEvent::PROP_EVENT_RUN_UNSPEC)
    {
      return PropagationEvent::PROP_EVENT_FIXPOINT;
    }
    return event;
  }//runPropagation
  
  PropagatorBoolLtHReifL::PropagatorBoolLtHReifL(DomainBoolean *aDom0, DomainBoolean *aDom1, DomainBoolean *aReif)
    : PropagatorBinary(aDom0, aDom1, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
    PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_LT_HREIF_L,
    PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL)
  {
    assert(aDom0);
    assert(aDom1);
    PropagatorSPtr propagatorLtHReifL = std::make_shared<PropagatorLtHReifL>(aDom0, aDom1, aReif);
    registerPropagator(propagatorLtHReifL);
  }

  double PropagatorBoolLtHReifL::getFitness()
  {
    auto fitnessValue = getRegisteredPropagator(PROP_SEMANTIC_TYPE_EQ_HREIF_L)->getFitness();
    if (fitnessValue == getUnspecFitness() || fitnessValue == getMinFitness())
    {
      return fitnessValue;
    }
    
    return 1.0;
  }//getFitness

  PropagatorBoolLtHReifR::PropagatorBoolLtHReifR(DomainBoolean *aDom0, DomainBoolean *aDom1, DomainBoolean *aReif)
    : PropagatorBinary(aDom0, aDom1, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
    PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_LT_HREIF_R,
    PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL)
  {
    assert(aDom0);
    assert(aDom1);
    PropagatorSPtr propagatorLtHReifR = std::make_shared<PropagatorLtHReifR>(aDom0, aDom1, aReif);
    registerPropagator(propagatorLtHReifR);
  }

  double PropagatorBoolLtHReifR::getFitness()
  {
    auto fitnessValue = getRegisteredPropagator(PROP_SEMANTIC_TYPE_EQ_HREIF_R)->getFitness();
    if (fitnessValue == getUnspecFitness() || fitnessValue == getMinFitness())
    {
      return fitnessValue;
    }
    
    return 1.0;
  }//getFitness

}// end namespace Core

