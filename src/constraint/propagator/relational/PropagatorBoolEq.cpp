// Base class
#include "PropagatorBoolEq.hpp"
#include "PropagatorEq.hpp"
#include "PropagatorEqReif.hpp"

// Utility
#include "PropagatorUtils.hpp"

#include <memory>
#include <cassert>

namespace Core {
  
  PropagatorBoolEq::PropagatorBoolEq(DomainBoolean *aDom0, DomainBoolean *aDom1)
  : PropagatorBinary(aDom0, aDom1, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
                     PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_EQ,
                     PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL)
  {
    assert(aDom0);
    assert(aDom1);
    PropagatorSPtr propagatorEq = std::make_shared<PropagatorEqBound>(aDom0, aDom1);
    registerPropagator(propagatorEq);
  }
  
  double PropagatorBoolEq::getFitness()
  {
    // Given eq(X, Y), implements the function:
    // (X == Y)? 0 : 1
    if (numSingletonContextDomains() !=  2)
    {
      return getUnspecFitness();
    }
    
    if ((DomainBoolean::cast(getContextDomain(D0))->isTrue() &&
         DomainBoolean::cast(getContextDomain(D1))->isTrue()) ||
        (DomainBoolean::cast(getContextDomain(D0))->isFalse() &&
         DomainBoolean::cast(getContextDomain(D1))->isFalse()))
    {
      return getMinFitness();
    }
    
    return 1.0;
  }//getFitness
  
  PropagatorBoolEqReif::PropagatorBoolEqReif(DomainBoolean *aDom0, DomainBoolean *aDom1, DomainBoolean *aReif)
    : PropagatorBinary(aDom0, aDom1, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
    PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_EQ_REIF,
    PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL)
  {
    assert(aDom0);
    assert(aDom1);
    PropagatorSPtr propagatorEqReif = std::make_shared<PropagatorEqReifBound>(aDom0, aDom1, aReif);
    registerPropagator(propagatorEqReif);
  }

  double PropagatorBoolEqReif::getFitness()
  {
    auto fitnessValue = getRegisteredPropagator(PROP_SEMANTIC_TYPE_EQ_REIF)->getFitness();
    if (fitnessValue == getUnspecFitness() || fitnessValue == getMinFitness())
    {
      return fitnessValue;
    }
    
    return 1.0;
  }//getFitness
  
  PropagatorBoolEqHReifL::PropagatorBoolEqHReifL(DomainBoolean *aDom0, DomainBoolean *aDom1, DomainBoolean *aReif)
    : PropagatorBinary(aDom0, aDom1, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
    PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_EQ_HREIF_L,
    PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL)
  {
    assert(aDom0);
    assert(aDom1);
    PropagatorSPtr propagatorEqHReifL = std::make_shared<PropagatorEqHReifLBound>(aDom0, aDom1, aReif);
    registerPropagator(propagatorEqHReifL);
  }

  double PropagatorBoolEqHReifL::getFitness()
  {
    auto fitnessValue = getRegisteredPropagator(PROP_SEMANTIC_TYPE_EQ_HREIF_L)->getFitness();
    if (fitnessValue == getUnspecFitness() || fitnessValue == getMinFitness())
    {
      return fitnessValue;
    }
    
    return 1.0;
  }//getFitness
  
  PropagatorBoolEqHReifR::PropagatorBoolEqHReifR(DomainBoolean *aDom0, DomainBoolean *aDom1, DomainBoolean *aReif)
    : PropagatorBinary(aDom0, aDom1, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
    PropagatorSemanticType::PROP_SEMANTIC_TYPE_BOOL_EQ_HREIF_R,
    PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL)
  {
    assert(aDom0);
    assert(aDom1);
    PropagatorSPtr propagatorEqHReifL = std::make_shared<PropagatorEqHReifLBound>(aDom0, aDom1, aReif);
    registerPropagator(propagatorEqHReifL);
  }
  
  double PropagatorBoolEqHReifR::getFitness()
  {
    auto fitnessValue = getRegisteredPropagator(PROP_SEMANTIC_TYPE_EQ_HREIF_R)->getFitness();
    if (fitnessValue == getUnspecFitness() || fitnessValue == getMinFitness())
    {
      return fitnessValue;
    }
    
    return 1.0;
  }//getFitness
  
}// end namespace Core

