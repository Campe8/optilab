// Base class
#include "PropagatorNq.hpp"

// Utility
#include "PropagatorUtils.hpp"

// Specular domain
#include "DomainSpecular.hpp"

#include <memory>
#include <cassert>

namespace Core {
  
  PropagatorNq::PropagatorNq(Domain *aDom0, Domain *aDom1)
  : PropagatorBinary(aDom0, aDom1, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN,
                     PropagatorSemanticType::PROP_SEMANTIC_TYPE_NQ,
                     PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL)
  {
    assert(aDom0);
    assert(aDom1);
    
    // Register filters
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_NQ);
  }
  
  double PropagatorNq::getFitness()
  {
    // Fitness function, given X != Y returns X == Y ? 1 : 0;
    Domain* dom0 = getContextDomain(D0);
    Domain* dom1 = getContextDomain(D1);
    
    // Check if the domains are empty or not singleton, if so return
    if (PropagatorUtils::isDomainEmpty(dom0) || PropagatorUtils::isDomainEmpty(dom1))
    {
      return getInvalidFitness();
    }
    
    if (!PropagatorUtils::isDomainSingleton(dom0) || !PropagatorUtils::isDomainSingleton(dom1))
    {
      return getUnspecFitness();
    }

    return dom0->lowerBound()->isEqual(dom1->lowerBound()) ? 1 : getMinFitness();
  }//getFitness
  
  PropagationEvent PropagatorNq::post()
  {
    std::size_t singletonDomains = numSingletonContextDomains();
    if (singletonDomains == 2)
    {
      if (PropagatorUtils::getSingleton(getContextDomain(D0))->isEqual(PropagatorUtils::getSingleton(getContextDomain(D1))))
      {
        return PropagationEvent::PROP_EVENT_FAIL;
      }
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)))
    {
      // Propagate on D1
      setFilterOutput(D1);
      
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_NQ);
      setFilterInput(0, PropagatorUtils::getSingleton(getContextDomain(D0)));
      
      Domain *outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
    }
    else if (PropagatorUtils::isDomainSingleton(getContextDomain(D1)))
    {
      // Propagate on D0
      setFilterOutput(D0);
      
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_NQ);
      setFilterInput(0, PropagatorUtils::getSingleton(getContextDomain(D1)));
      
      Domain *outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
    }
    
    return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
  }//post
  
  PropagationEvent PropagatorNq::runPropagation()
  {
    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)))
    {
      // Propagate on D1
      setFilterOutput(D1);
      
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_NQ);
      setFilterInput(0, PropagatorUtils::getSingleton(getContextDomain(D0)));
      
      Domain *outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      // Return subsumed event since one of the domains is singleton
      // and it cannot be further propagated on this constraint
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    else if (PropagatorUtils::isDomainSingleton(getContextDomain(D1)))
    {
      // Propagate on D0
      setFilterOutput(D0);
      
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_NQ);
      setFilterInput(0, PropagatorUtils::getSingleton(getContextDomain(D1)));
      
      Domain *outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      // Return subsumed event since one of the domains is singleton
      // and it cannot be further propagated on this constraint
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    
    // Return fix-point event since no domain is singleton here
    // and the constraint reached the fix point after 1 propagation.
    // For example, it can be further propagated if one of the
    // domains become singleton
    return PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagation
  
}// end namespace Core

