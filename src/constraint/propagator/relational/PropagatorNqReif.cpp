// Base class
#include "PropagatorNqReif.hpp"

// Propagators
#include "PropagatorNq.hpp"
#include "PropagatorEq.hpp"

// Utility
#include "PropagatorUtils.hpp"

#include <memory>
#include <cassert>

namespace Core {
  
  PropagatorNqReif::PropagatorNqReif(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif, PropagatorSemanticType aSemanticType)
  : PropagatorBinaryReif(aDom0, aDom1, aDomReif,
    PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
    aSemanticType,
    PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL)
  {
    assert(aDom0);
    assert(aDom1);
    assert(aDomReif);

    // Propagator Nq
    PropagatorSPtr propagatorNq = std::make_shared<PropagatorNq>(getContextDomain(D0), getContextDomain(D1));
    // Propagator Eq
    PropagatorSPtr propagatorNt = std::make_shared<PropagatorEqBound>(getContextDomain(D0), getContextDomain(D1));

    // Register propagators
    registerPropagator(propagatorNq);
    registerPropagator(propagatorNt);
  }
  
  PropagatorNqReif::~PropagatorNqReif()
  {
  }
  
  double PropagatorNqReif::getFitness()
  {
    if(isContextReifTrue())
    {
      return getRegisteredPropagator(PROP_SEMANTIC_TYPE_NQ)->getFitness();
    }
    else if(isContextReifFalse())
    {
      return getRegisteredPropagator(PROP_SEMANTIC_TYPE_EQ)->getFitness();
    }
    else
    {
      return getUnspecFitness();
    }
  }//getFitness
  
  PropagationEvent PropagatorNqReif::post()
  {
    if (isContextReifSingleton())
    {
      if (isContextReifTrue())
      {
        // b -> D0 != D1
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_NQ);
      }
      else
      {
        // !b -> D1 == D0
        assert(isContextReifFalse());
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ);
      }
    }

    PropagationEvent eventUponReification = PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)) && PropagatorUtils::isDomainSingleton(getContextDomain(D1)))
    {
      
      if (PropagatorUtils::getSingleton(getContextDomain(D0))->isNotEqual(PropagatorUtils::getSingleton(getContextDomain(D1))))
      {
        // D0 != D1 -> b
        eventUponReification = setContextReifTrue();
      }
      else
      {
        // D0 == D1 -> !b
        eventUponReification = setContextReifFalse();
      }
    }
    return eventUponReification;
  }//post
  
  PropagationEvent PropagatorNqReif::runPropagation()
  {
    if (isContextReifSingleton())
    {
      if (isContextReifTrue())
      {
        // b -> D0 != D1
        return runInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_NQ);
      }
      else
      {
        // !b -> D0 == D1
        assert(isContextReifFalse());
        return runInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ);
      }
    }

    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)) && PropagatorUtils::isDomainSingleton(getContextDomain(D1)))
    {
      if (PropagatorUtils::getSingleton(getContextDomain(D0))->isNotEqual(PropagatorUtils::getSingleton(getContextDomain(D1))))
      {
        // D0 != D1 -> b
        return setContextReifTrue();
      }
      else
      {
        // D1 == D0 -> !b
        return setContextReifFalse();
      }
    }

    return PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagation
 
  PropagatorHNqReif::PropagatorHNqReif(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif, PropagatorSemanticType aReifType)
    : PropagatorNqReif(aDom0, aDom1, aDomReif, aReifType)
    , pReifType(aReifType)
  {
    assert(pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_NQ_HREIF_L ||
      pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_NQ_HREIF_R);
  }

  PropagatorHNqReif::~PropagatorHNqReif()
  {
  }
  
  double PropagatorHNqReif::getFitness()
  {
    Domain* dom0 = getContextDomain(D0);
    Domain* dom1 = getContextDomain(D1);
    Domain* dom2 = GetContextReifDomain();

    if (PropagatorUtils::isDomainEmpty(dom0) || PropagatorUtils::isDomainEmpty(dom1) ||
        PropagatorUtils::isDomainEmpty(dom2))
    {
      return getInvalidFitness();
    }
    
    if (!PropagatorUtils::isDomainSingleton(dom0) || !PropagatorUtils::isDomainSingleton(dom1) ||
        !PropagatorUtils::isDomainSingleton(dom2))
    {
      return getUnspecFitness();
    }

    if(isHalfReifLeft())
    {
      if(isContextReifTrue())
      {
        return getRegisteredPropagator(PROP_SEMANTIC_TYPE_NQ)->getFitness();
      }
      else if(isContextReifFalse())
      {
        return getMinFitness();
      }
    }
    else if(isHalfReifRight())
    {
      if(dom0->lowerBound()->isEqual(dom1->lowerBound()))
      {
        return getMinFitness();
      }
      else
      {
        if(isContextReifTrue())
        {
          return getMinFitness();
        }
        else
        {
          return 1;
        }
      }
    }
    
    return getUnspecFitness();
  }//getFitness
  
  PropagationEvent PropagatorHNqReif::post()
  {
    if (isContextReifSingleton())
    {
      if (isHalfReifLeft() && isContextReifTrue())
      {
        // b -> D0 != D1
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_NQ);
      }
      else if (isHalfReifRight() && isContextReifFalse())
      {
        // !b -> D0 == D1
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ);
      }
    }

    PropagationEvent eventUponReification = PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)) && PropagatorUtils::isDomainSingleton(getContextDomain(D1)))
    {
      bool domainEquality = PropagatorUtils::getSingleton(getContextDomain(D0))->isNotEqual(PropagatorUtils::getSingleton(getContextDomain(D1)));
      if (domainEquality && isHalfReifRight())
      {
        // D0 != D1 -> b
        eventUponReification = setContextReifTrue();
      }
      else if (!domainEquality && isHalfReifLeft())
      {
        // D0 == D1 -> !b
        eventUponReification = setContextReifFalse();
      }
      if (eventUponReification == PropagationEvent::PROP_EVENT_FAIL)
      {
        return PropagationEvent::PROP_EVENT_FAIL;
      }
    }

    return eventUponReification;
  }//post

  PropagationEvent PropagatorHNqReif::runPropagation()
  {
    if (isContextReifSingleton())
    {
      if (isHalfReifLeft() && isContextReifTrue())
      {
        // b -> D0 != D1
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_NQ);
      }
      else if (isHalfReifRight() && isContextReifFalse())
      {
        // !b -> D1 == D0
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ);
      }
    }

    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)) && PropagatorUtils::isDomainSingleton(getContextDomain(D1)))
    {
      bool domainEquality = PropagatorUtils::getSingleton(getContextDomain(D0))->isNotEqual(PropagatorUtils::getSingleton(getContextDomain(D1)));
      if (domainEquality && isHalfReifRight())
      {
        // D0 != D1 -> b
        return setContextReifTrue();
      }
      else if (!domainEquality && isHalfReifLeft())
      {
        // D1 == D0 -> !b
        return setContextReifFalse();
      }
    }

    return PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagation

  PropagatorNqHReifL::PropagatorNqHReifL(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif)
    : PropagatorHNqReif(aDom0, aDom1, aDomReif, PropagatorSemanticType::PROP_SEMANTIC_TYPE_NQ_HREIF_L)
  {
  }

  PropagatorNqHReifL::~PropagatorNqHReifL()
  {
  }

  PropagatorNqHReifR::PropagatorNqHReifR(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif)
    : PropagatorHNqReif(aDom0, aDom1, aDomReif, PropagatorSemanticType::PROP_SEMANTIC_TYPE_NQ_HREIF_R)
  {
  }

  PropagatorNqHReifR::~PropagatorNqHReifR()
  {
  }

}// end namespace Core
