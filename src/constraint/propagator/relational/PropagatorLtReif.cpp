// Base class
#include "PropagatorLtReif.hpp"

// Propagators
#include "PropagatorLq.hpp"
#include "PropagatorLt.hpp"

// Utility
#include "PropagatorUtils.hpp"

#include <memory>
#include <cassert>

namespace Core {
  
  PropagatorLtReif::PropagatorLtReif(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif, PropagatorSemanticType aSemanticType)
  : PropagatorBinaryReif(aDom0, aDom1, aDomReif,
    PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
    aSemanticType,
    PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL)
  {
    assert(aDom0);
    assert(aDom1);
    assert(aDomReif);

    // Propagator Lq
    PropagatorSPtr propagatorLq = std::make_shared<PropagatorLt>(getContextDomain(D0), getContextDomain(D1));
    // Propagator Lt
    PropagatorSPtr propagatorLt = std::make_shared<PropagatorLq>(getContextDomain(D1), getContextDomain(D0));

    // Register propagators
    registerPropagator(propagatorLq);
    registerPropagator(propagatorLt);
  }
  
  PropagatorLtReif::~PropagatorLtReif()
  {
  }
  
  double PropagatorLtReif::getFitness()
  {
    if(isContextReifTrue())
    {
      return getRegisteredPropagator(PROP_SEMANTIC_TYPE_LT)->getFitness();
    }
    else if(isContextReifFalse())
    {
      return getRegisteredPropagator(PROP_SEMANTIC_TYPE_LQ)->getFitness();
    }
    else
    {
      return getUnspecFitness();
    }
  }//getFitness
  
  PropagationEvent PropagatorLtReif::post()
  {
    if (isContextReifSingleton())
    {
      if (isContextReifTrue())
      {
        // b -> D0 < D1
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LT);
      }
      else
      {
        // !b -> D1 <= D0
        assert(isContextReifFalse());
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ);
      }
    }

    PropagationEvent eventUponReification = PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)) && PropagatorUtils::isDomainSingleton(getContextDomain(D1)))
    {
      
      if (PropagatorUtils::getSingleton(getContextDomain(D0))->isLessThan(PropagatorUtils::getSingleton(getContextDomain(D1))))
      {
        // D0 < D1 -> b
        eventUponReification = setContextReifTrue();
      }
      else
      {
        // D1 <= D0 -> !b
        eventUponReification = setContextReifFalse();
      }
    }
    return eventUponReification;
  }//post
  
  PropagationEvent PropagatorLtReif::runPropagation()
  {
    if (isContextReifSingleton())
    {
      if (isContextReifTrue())
      {
        // b -> D0 < D1
        return runInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LT);
      }
      else
      {
        // !b -> D1 <= D0
        assert(isContextReifFalse());
        return runInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ);
      }
    }

    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)) && PropagatorUtils::isDomainSingleton(getContextDomain(D1)))
    {
      if (PropagatorUtils::getSingleton(getContextDomain(D0))->isLessThan(PropagatorUtils::getSingleton(getContextDomain(D1))))
      {
        // D0 < D1 -> b
        return setContextReifTrue();
      }
      else
      {
        // D1 <= D0 -> !b
        return setContextReifFalse();
      }
    }

    return PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagation
 
  PropagatorHLtReif::PropagatorHLtReif(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif, PropagatorSemanticType aReifType)
    : PropagatorLtReif(aDom0, aDom1, aDomReif, aReifType)
    , pReifType(aReifType)
  {
    assert(pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_LT_HREIF_L ||
      pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_LT_HREIF_R);
  }

  PropagatorHLtReif::~PropagatorHLtReif()
  {
  }

  double PropagatorHLtReif::getFitness()
  {
    Domain* dom0 = getContextDomain(D0);
    Domain* dom1 = getContextDomain(D1);
    Domain* dom2 = GetContextReifDomain();

    if (PropagatorUtils::isDomainEmpty(dom0) || PropagatorUtils::isDomainEmpty(dom1) ||
        PropagatorUtils::isDomainEmpty(dom2))
    {
      return getInvalidFitness();
    }
    
    if (!PropagatorUtils::isDomainSingleton(dom0) || !PropagatorUtils::isDomainSingleton(dom1) ||
        !PropagatorUtils::isDomainSingleton(dom2))
    {
      return getUnspecFitness();
    }

    if(isHalfReifLeft())
    {
      if(isContextReifTrue())
      {
        return getRegisteredPropagator(PROP_SEMANTIC_TYPE_LT)->getFitness();
      }
      else if(isContextReifFalse())
      {
        return getMinFitness();
      }
    }
    else if(isHalfReifRight())
    {
      if(dom0->lowerBound()->isLessThan(dom1->lowerBound()))
      {
        if(isContextReifTrue())
        {
          return getMinFitness();
        }
        else
        {
          return 1;
        }
      }
      else
      {
        return getMinFitness();
      }
    }
    
    return getUnspecFitness();
  }//getFitness
  
  PropagationEvent PropagatorHLtReif::post()
  {
    if (isContextReifSingleton())
    {
      if (isHalfReifLeft() && isContextReifTrue())
      {
        // b -> D0 < D1
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LT);
      }
      else if (isHalfReifRight() && isContextReifFalse())
      {
        // !b -> D1 <= D0
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ);
      }
    }

    PropagationEvent eventUponReification = PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)) && PropagatorUtils::isDomainSingleton(getContextDomain(D1)))
    {
      bool domainEquality = PropagatorUtils::getSingleton(getContextDomain(D0))->isLessThan(PropagatorUtils::getSingleton(getContextDomain(D1)));
      if (domainEquality && isHalfReifRight())
      {
        // D0 < D1 -> b
        eventUponReification = setContextReifTrue();
      }
      else if (!domainEquality && isHalfReifLeft())
      {
        // D1 <= D0 -> !b
        eventUponReification = setContextReifFalse();
      }
      if (eventUponReification == PropagationEvent::PROP_EVENT_FAIL)
      {
        return PropagationEvent::PROP_EVENT_FAIL;
      }
    }

    return eventUponReification;
  }//post

  PropagationEvent PropagatorHLtReif::runPropagation()
  {
    if (isContextReifSingleton())
    {
      if (isHalfReifLeft() && isContextReifTrue())
      {
        // b -> D0 < D1
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LT);
      }
      else if (isHalfReifRight() && isContextReifFalse())
      {
        // !b -> D1 <= D0
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ);
      }
    }

    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)) && PropagatorUtils::isDomainSingleton(getContextDomain(D1)))
    {
      bool domainEquality = PropagatorUtils::getSingleton(getContextDomain(D0))->isLessThan(PropagatorUtils::getSingleton(getContextDomain(D1)));
      if (domainEquality && isHalfReifRight())
      {
        // D0 < D1 -> b
        return setContextReifTrue();
      }
      else if (!domainEquality && isHalfReifLeft())
      {
        // D1 <= D0 -> !b
        return setContextReifFalse();
      }
    }

    return PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagation

  PropagatorLtHReifL::PropagatorLtHReifL(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif)
    : PropagatorHLtReif(aDom0, aDom1, aDomReif, PropagatorSemanticType::PROP_SEMANTIC_TYPE_LT_HREIF_L)
  {
  }

  PropagatorLtHReifL::~PropagatorLtHReifL()
  {
  }

  PropagatorLtHReifR::PropagatorLtHReifR(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif)
    : PropagatorHLtReif(aDom0, aDom1, aDomReif, PropagatorSemanticType::PROP_SEMANTIC_TYPE_LT_HREIF_R)
  {
  }

  PropagatorLtHReifR::~PropagatorLtHReifR()
  {
  }

}// end namespace Core


