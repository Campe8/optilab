// Base class
#include "PropagatorEqReif.hpp"

// Propagators
#include "PropagatorEq.hpp"
#include "PropagatorNq.hpp"

// Utility
#include "PropagatorUtils.hpp"

#include <memory>
#include <cassert>

namespace Core {
  
  PropagatorEqReif::PropagatorEqReif(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif,
                                     PropagatorStrategyType aStrategyType,
                                     PropagatorSemanticType aReifType,
                                     PropagationPriority aPriority)
                                     : PropagatorBinaryReif(aDom0, aDom1, aDomReif,
                                     aStrategyType,
                                     aReifType,
                                     PropagatorSemanticClass::PROP_SEMANTIC_CLASS_RELATIONAL,
                                     aPriority)
  {
    assert(aDom0);
    assert(aDom1);
    
    // Propagator Nq
    PropagatorSPtr propagatorNq = std::make_shared<PropagatorNq>(getContextDomain(D0), getContextDomain(D1));
    // Propagator Eq
    PropagatorSPtr propagatorEq{ nullptr };
    switch (aStrategyType)
    {
    case PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND:
      propagatorEq = std::make_shared<PropagatorEqBound>(getContextDomain(D0), getContextDomain(D1));
      break;
    default:
      assert(aStrategyType == PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN);
      propagatorEq = std::make_shared<PropagatorEqDomain>(getContextDomain(D0), getContextDomain(D1));
    }
    assert(propagatorEq);

    // Register propagators
    registerPropagator(propagatorEq);
    registerPropagator(propagatorNq);
  }
  
  PropagatorEqReif::~PropagatorEqReif()
  {
  }
  
  double PropagatorEqReif::getFitness()
  {
    if(isContextReifTrue())
    {
      return getRegisteredPropagator(PROP_SEMANTIC_TYPE_EQ)->getFitness();
    }
    else if(isContextReifFalse())
    {
      return getRegisteredPropagator(PROP_SEMANTIC_TYPE_NQ)->getFitness();
    }
    else
    {
      return getUnspecFitness();
    }
  }//getFitness
  
  PropagationEvent PropagatorEqReif::post()
  {
    if (isContextReifSingleton())
    {
      if (isContextReifTrue())
      {
        // b -> D0 == D1
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ);
      }
      else
      {
        // !b -> D0 != D1
        assert(isContextReifFalse());
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_NQ);
      }
    }

    PropagationEvent eventUponReification = PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)) && PropagatorUtils::isDomainSingleton(getContextDomain(D1)))
    {
      
      if (PropagatorUtils::getSingleton(getContextDomain(D0))->isEqual(PropagatorUtils::getSingleton(getContextDomain(D1))))
      {
        // D0 == D1 -> b
        eventUponReification = setContextReifTrue();
      }
      else
      {
        // D0 != D1 -> !b
        eventUponReification = setContextReifFalse();
      }
    }
    
    return eventUponReification;
  }//post
  
  PropagationEvent PropagatorEqReif::runPropagation()
  {
    if (isContextReifSingleton())
    {
      if (isContextReifTrue())
      {
        // b -> D0 == D1
        return runInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ);
      }
      else
      {
        // !b -> D0 != D1
        assert(isContextReifFalse());
        return runInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_NQ);
      }
    }

    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)) && PropagatorUtils::isDomainSingleton(getContextDomain(D1)))
    {
      if (PropagatorUtils::getSingleton(getContextDomain(D0))->
          isEqual(PropagatorUtils::getSingleton(getContextDomain(D1))))
      {
        // D0 == D1 -> b
        return setContextReifTrue();
      }
      else
      {
        // D0 != D1 -> !b
        return setContextReifFalse();
      }
    }

    return PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagation
  
  PropagatorEqReifBound::PropagatorEqReifBound(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif)
    : PropagatorEqReif(aDom0, aDom1, aDomReif, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND)
  {
  }

  PropagatorEqReifBound::~PropagatorEqReifBound()
  {
  }

  PropagatorEqReifDomain::PropagatorEqReifDomain(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif)
    : PropagatorEqReif(aDom0, aDom1, aDomReif,
                 PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN,
                 PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ_REIF,
                 PropagationPriority::PROP_PRIORITY_LINEAR)
  {
  }
  
  PropagatorEqReifDomain::~PropagatorEqReifDomain()
  {
  }
 
  PropagatorHEqReif::PropagatorHEqReif(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif,
    PropagatorStrategyType aStrategyType,
    PropagatorSemanticType aReifType,
    PropagationPriority aPriority)
    : PropagatorEqReif(aDom0, aDom1, aDomReif,
                       aStrategyType,
                       aReifType,
                       aPriority)
    , pReifType(aReifType)
  {
    assert(pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ_HREIF_L ||
      pReifType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ_HREIF_R);
  }

  PropagatorHEqReif::~PropagatorHEqReif()
  {
  }
  
  double PropagatorHEqReif::getFitness()
  {
    Domain* dom0 = getContextDomain(D0);
    Domain* dom1 = getContextDomain(D1);
    Domain* dom2 = GetContextReifDomain();

    if (PropagatorUtils::isDomainEmpty(dom0) || PropagatorUtils::isDomainEmpty(dom1) ||
        PropagatorUtils::isDomainEmpty(dom2))
    {
      return getInvalidFitness();
    }
    
    if (!PropagatorUtils::isDomainSingleton(dom0) || !PropagatorUtils::isDomainSingleton(dom1) ||
        !PropagatorUtils::isDomainSingleton(dom2))
    {
      return getUnspecFitness();
    }

    if(isHalfReifLeft())
    {
      if(isContextReifTrue())
      {
        return getRegisteredPropagator(PROP_SEMANTIC_TYPE_EQ)->getFitness();
      }
      else if(isContextReifFalse())
      {
        return getMinFitness();
      }
    }
    else if(isHalfReifRight())
    {
      if(dom0->lowerBound()->isEqual(dom1->lowerBound()))
      {
        if(isContextReifTrue())
        {
          return getMinFitness();
        }
        else
        {
          return 1;
        }
      }
      else
      {
        return getMinFitness();
      }
    }
    
    return getUnspecFitness();
  }//getFitness
  
  PropagationEvent PropagatorHEqReif::post()
  {
    if (isContextReifSingleton())
    {
      if (isHalfReifLeft() && isContextReifTrue())
      {
        // b -> D0 == D1
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ);
      }
      else if (isHalfReifRight() && isContextReifFalse())
      {
        // !b -> D0 != D1
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_NQ);
      }
    }

    PropagationEvent eventUponReification = PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)) && PropagatorUtils::isDomainSingleton(getContextDomain(D1)))
    {
      bool domainEquality = PropagatorUtils::getSingleton(getContextDomain(D0))->isEqual(PropagatorUtils::getSingleton(getContextDomain(D1)));
      if (domainEquality && isHalfReifRight())
      {
        // D0 == D1 -> b
        eventUponReification = setContextReifTrue();
      }
      else if (!domainEquality && isHalfReifLeft())
      {
        // D0 != D1 -> !b
        eventUponReification = setContextReifFalse();
      }
      if (eventUponReification == PropagationEvent::PROP_EVENT_FAIL)
      {
        return PropagationEvent::PROP_EVENT_FAIL;
      }
    }

    return eventUponReification;
  }//post

  PropagationEvent PropagatorHEqReif::runPropagation()
  {
    if (isContextReifSingleton())
    {
      if (isHalfReifLeft() && isContextReifTrue())
      {
        // b -> D0 == D1
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ);
      }
      else if (isHalfReifRight() && isContextReifFalse())
      {
        // !b -> D0 != D1
        return postInternalPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_NQ);
      }
    }

    if (PropagatorUtils::isDomainSingleton(getContextDomain(D0)) && PropagatorUtils::isDomainSingleton(getContextDomain(D1)))
    {
      bool domainEquality = PropagatorUtils::getSingleton(getContextDomain(D0))->isEqual(PropagatorUtils::getSingleton(getContextDomain(D1)));
      if (domainEquality && isHalfReifRight())
      {
        // D0 == D1 -> b
        return setContextReifTrue();
      }
      else if (!domainEquality && isHalfReifLeft())
      {
        // D0 != D1 -> !b
        return setContextReifFalse();
      }
    }

    return PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagation

  PropagatorEqHReifLBound::PropagatorEqHReifLBound(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif)
    : PropagatorHEqReif(aDom0, aDom1, aDomReif, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ_HREIF_L)
  {
  }

  PropagatorEqHReifLBound::~PropagatorEqHReifLBound()
  {
  }

  PropagatorEqHReifLDomain::PropagatorEqHReifLDomain(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif)
    : PropagatorHEqReif(aDom0, aDom1, aDomReif,PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN,
    PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ_HREIF_L,
    PropagationPriority::PROP_PRIORITY_LINEAR)
  {
  }

  PropagatorEqHReifLDomain::~PropagatorEqHReifLDomain()
  {
  }
  
  PropagatorEqHReifRBound::PropagatorEqHReifRBound(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif)
    : PropagatorHEqReif(aDom0, aDom1, aDomReif, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ_HREIF_R)
  {
  }

  PropagatorEqHReifRBound::~PropagatorEqHReifRBound()
  {
  }

  PropagatorEqHReifRDomain::PropagatorEqHReifRDomain(Domain *aDom0, Domain *aDom1, DomainBoolean *aDomReif)
    : PropagatorHEqReif(aDom0, aDom1, aDomReif, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN,
    PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ_HREIF_R,
    PropagationPriority::PROP_PRIORITY_LINEAR)
  {
  }

  PropagatorEqHReifRDomain::~PropagatorEqHReifRDomain()
  {
  }

}// end namespace Core
