// Base class
#include "PropagatorLinearReif.hpp"

#include "PropagatorUtils.hpp"

#include <cassert>

namespace Core {

  PropagatorLinearReif::PropagatorLinearReif(
    DomainElementArray& aCoeffArray,
    DomainArray&        aDomainArray,
    DomainElement*      aConstant,
    DomainBoolean*      aDomBool,
    PropagatorStrategyType aStrategyType,
    PropagatorSemanticType aSemanticType,
    PropagatorSemanticClass aSemanticClass,
    PropagationPriority aPriority,
    bool aUseGCDOnNormalization)
    : PropagatorLinear(aCoeffArray, aDomainArray, aConstant, aStrategyType, aSemanticType, aSemanticClass, aPriority, aUseGCDOnNormalization)
    , PropagatorReif(aDomBool)
    {
    }

  PropagatorLinearReif::~PropagatorLinearReif()
  {
  }
    
  bool PropagatorLinearReif::areAllDomainsSingleton()
  {
    DomainArraySPtr posDoms = getPosDomains();
    for (std::size_t idx = 0; idx < posDoms->size(); ++idx)
    {
      if (!PropagatorUtils::isDomainSingleton(posDoms->operator[](idx)))
      {
        return false;
      }
    }

    DomainArraySPtr negDoms = getNegDomains();
    for (std::size_t idx = 0; idx < negDoms->size(); ++idx)
    {
      if (!PropagatorUtils::isDomainSingleton(negDoms->operator[](idx)))
      {
        return false;
      }
    }

    return true;
  }//areAllDomainsSingleton

  DomainElement* PropagatorLinearReif::getLeftSideExpressionValue()
  {
    // Positive domains
    DomainElementArraySPtr posCoeff = getPosCoefficients();
    DomainArraySPtr posDoms = getPosDomains();
    assert(posCoeff->size() == posDoms->size());

    // Negative domains
    DomainElementArraySPtr negCoeff = getNegCoefficients();
    DomainArraySPtr negDoms = getNegDomains();
    assert(negCoeff->size() == negDoms->size());

    /*
     * Note that
     * posCoeff->size() + negCoeff->size()
     * can be equal to 
     * 0
     * for example, if all domains are singleton.
     * If so, the expression
     * 0 op k
     * olds.
     */
    if (posCoeff->size() + negCoeff->size() == 0)
    {
      assert(getLinearConstant());
      return getLinearConstant()->zero();
    }

    // Calculate sum
    DomainElement* sum = posCoeff->size() > 0 ? posCoeff->operator[](0)->zero() : negCoeff->operator[](0)->zero();
    for (std::size_t idx = 0; idx < posDoms->size(); ++idx)
    {
      sum = sum->plus(posCoeff->operator[](idx)->times(posDoms->operator[](idx)->lowerBound()));
    }

    for (std::size_t idx = 0; idx < negDoms->size(); ++idx)
    {
      sum = sum->plus(negCoeff->operator[](idx)->times(negDoms->operator[](idx)->lowerBound()->mirror()));
    }

    return sum;
  }//getLeftSideExpressionValue

}// end namespace Core
