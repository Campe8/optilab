// Base class
#include "PropagatorBinaryReif.hpp"

#include "PropagatorUtils.hpp"

#include <cassert>

namespace Core {
  
  PropagatorBinaryReif::PropagatorBinaryReif(Domain *aDom0,
                                             Domain *aDom1,
                                             DomainBoolean *aDomBool,
                                             PropagatorStrategyType aStrategyType,
                                             PropagatorSemanticType aSemanticType,
                                             PropagatorSemanticClass aSemanticClass,
                                             PropagationPriority aPriority)
  : PropagatorBinary(aDom0, aDom1, aStrategyType, aSemanticType, aSemanticClass, aPriority)
  , PropagatorReif(aDomBool)
  {
  }
  
  PropagatorBinaryReif::~PropagatorBinaryReif()
  {
  }
  
}// end namespace Core
