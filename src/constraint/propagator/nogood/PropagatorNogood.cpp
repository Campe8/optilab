// Base class
#include "PropagatorNogood.hpp"

#include "PropagatorUtils.hpp"

#include "DArrayMatrix.hpp"

#include "Domain.hpp"
#include "DomainElement.hpp"

#include <cassert>

namespace Core {
  
  PropagatorNogood::PropagatorNogood(const DomainArraySPtr& aDomainArray, const DomainElementArraySPtr& aDomainElementArray)
  : Propagator(PropagatorSemantics(aDomainArray->size(),
                                   PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
                                   PropagationPriority::PROP_PRIORITY_LINEAR,
                                   PropagatorSemanticType::PROP_SEMANTIC_TYPE_NOGOOD,
                                   PropagatorSemanticClass::PROP_SEMANTIC_CLASS_LINEAR))
  , pDomainArray(aDomainArray)
  , pDomainElementArray(aDomainElementArray)
  {
    assert(aDomainArray && aDomainElementArray);
    assert(aDomainArray->size() == aDomainElementArray->size());
    
    // Add domains to context
    for(std::size_t idx{0}; idx < pDomainArray->size(); ++idx)
    {
      addContextDomain(idx, (*pDomainArray)[idx]);
    }
    
    // Register filters
    registerFilteringStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_NQ);
  }
  
  double PropagatorNogood::getFitness()
  {
    // Fitness function:
    // given the variables
    // x_1, x_2, ..., x_n
    // and the domain elements
    // d_1, d_2, ..., d_n
    // the nogood constraint is the following:
    // x_1 != d_1 \/ x_2 != d_2 \/ ... \/ x_n != d_n
    // i.e., it is satified (fitness cost 0) when at least one of
    // the variable x_1, x_2, ..., x_n is different from
    // the correspondent domain d_1, d_2, ..., d_n.
    // This is a binary condition, i.e., the fitness is only 1 or 0.
    // There is no way of saying the constraint is "more satisfied" or "less satisfied"
    // since as soon as 1 variable is different, the constraint is satisfied
    for (std::size_t idx{0}; idx < getContextSize(); ++idx)
    {
      // At least one domain is not singleton -> return unspecified
      if (!PropagatorUtils::isDomainSingleton(getContextDomain(idx)))
      {
        return getUnspecFitness();
      }
      
      // Otherwise check if it is different from the corresponding domain element.
      // If so, return subsumed, the constraint is already satisfied
      if (PropagatorUtils::getSingleton(getContextDomain(idx))->
          isNotEqual((*pDomainElementArray)[idx]))
      {
        return getMinFitness();
      }
    }
    
    return 1.0;
  }//getFitness
  
  PropagationEvent PropagatorNogood::post()
  {
    // Post nogood constraint:
    // - If there is at least one domain singleton different from its corresponding
    //   domain element, the constraint is subsumed;
    // - If all domains are singleton and equal to the corresponding domain element,
    //   the constraint is failed;
    // - otherwise the constraint is unspecified
    for(std::size_t idx{0}; idx < getContextSize(); ++idx)
    {
      // At least one domain is not singleton -> return unspecified
      if(!PropagatorUtils::isDomainSingleton(getContextDomain(idx)))
      {
        return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
      }
      
      // Otherwise check if it is different from the corresponding domain element.
      // If so, return subsumed, the constraint is already satisfied
      if (PropagatorUtils::getSingleton(getContextDomain(idx))->isNotEqual((*pDomainElementArray)[idx]))
      {
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
    }
    
    // In all other cases, i.e, all ground and equal,
    // the constraint is not satisfied
    return PropagationEvent::PROP_EVENT_FAIL;
  }//post
  
  PropagationEvent PropagatorNogood::runPropagation()
  {
    // Propagate nogood constraint:
    // - propagates only when there is only one domain not singleton
    //   and all the other domains are equal to the corresponding domain element

    std::size_t numNotSingletons{0};
    std::size_t notSingletonDomainIdx{0};
    for(std::size_t idx{0}; idx < getContextSize(); ++idx)
    {
      // At least one domain is not singleton -> return unspecified
      if(!PropagatorUtils::isDomainSingleton(getContextDomain(idx)))
      {
        numNotSingletons++;
        notSingletonDomainIdx = idx;
        continue;
      }
      
      // Otherwise check if it is different from the corresponding domain element.
      // If so, return subsumed, the constraint is already satisfied
      if (PropagatorUtils::getSingleton(getContextDomain(idx))->isNotEqual((*pDomainElementArray)[idx]))
      {
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
    }
    
    if(numNotSingletons == 0)
    {
      // All domains are singletons and matching the corresponding element.
      // This constraint is not satisfied
      return PropagationEvent::PROP_EVENT_FAIL;
    }
    else if(numNotSingletons == 1)
    {
      // One domain is not singleton and all the others are matching
      // the corresponding domain elements.
      // Propagate on the remaining non-singleton domain
      
      // Propagate on the (only) non-singleton domain
      setFilterOutput(notSingletonDomainIdx);
      
      setFilterStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_NQ);
      setFilterInput(0, (*pDomainElementArray)[notSingletonDomainIdx]);
      
      Domain *outDomain = applyFilteringStrategy();
      DOMAIN_FAILURE_CHECK(outDomain);
      
      // Return subsumed event since one of the domains is singleton
      // and it cannot be further propagated on this constraint
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    
    // Return fix-point event since no domain is singleton here
    // and the constraint reached the fix point after 1 propagation.
    // For example, it can be further propagated if one of the
    // domains become singleton
    return PropagationEvent::PROP_EVENT_FIXPOINT;
  }//runPropagation

}// end namespace Core
