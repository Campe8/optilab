// Base class
#include "PropagatorReif.hpp"

#include "ConstraintMacro.hpp"

// Filters
#include "FilteringAlgorithmFactory.hpp"

#include <cassert>

namespace Core {
  
  PropagatorReif::PropagatorReif(DomainBoolean *aDomBool)
  : pDomainBool(aDomBool)
  {
    assert(pDomainBool);
    // Register filter algorithms for True and False propagation
    FilteringAlgorithmSemanticType aType = FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_TRUE;
    pFilterAlgorithmRegister[static_cast<int>(aType)] = FilteringAlgorithmFactory::getFilteringAlgorithmInstance(aType);
    
    aType = FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_FALSE;
    pFilterAlgorithmRegister[static_cast<int>(aType)] = FilteringAlgorithmFactory::getFilteringAlgorithmInstance(aType);
    
    pFilterParams.setOutputDomain(pDomainBool);
  }
  
  PropagatorReif::~PropagatorReif()
  {
  }
  
  bool PropagatorReif::isContextReifTrue()
  {
    if (PropagatorUtils::isDomainSingleton(GetContextReifDomain()))
    {
      return GetContextReifDomain()->isTrue();
    }
    return false;
  }//isContextReifTrue
  
  bool PropagatorReif::isContextReifFalse()
  {
    if (PropagatorUtils::isDomainSingleton(GetContextReifDomain()))
    {
      return GetContextReifDomain()->isFalse();
    }
    return false;
  }//isContextReifFalse
  
  PropagationEvent PropagatorReif::setContextReifTrue()
  {
    setFilterReifStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_TRUE);
    
    Domain* outDomain = pFilterToUse->filter(pFilterParams);
    DOMAIN_FAILURE_CHECK(outDomain);
    
    return PropagationEvent::PROP_EVENT_SUBSUMED;
  }//setContextReifTrue
  
  PropagationEvent PropagatorReif::setContextReifFalse()
  {
    setFilterReifStrategy(FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_FALSE);
    
    Domain* outDomain = pFilterToUse->filter(pFilterParams);
    DOMAIN_FAILURE_CHECK(outDomain);
    
    return PropagationEvent::PROP_EVENT_SUBSUMED;
  }//setContextReifFalse
  
}// end namespace Core
