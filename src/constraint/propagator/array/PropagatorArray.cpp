// Base class
#include "PropagatorArray.hpp"

// Domain iterator
#include "DomainIterator.hpp"

// Utility
#include "PropagatorUtils.hpp"

// Domain element
#include "DomainElementInt64.hpp"

#include <memory>
#include <cassert>

namespace Core {

  PropagatorArray::PropagatorArray(const PropagatorSemantics& aPropagatorSemantics, DomainArray& aDomArray)
    : Propagator(aPropagatorSemantics)
    , pDomElemArray(nullptr)
  {
    assert(!aDomArray.empty());

    // Set array size
    pArraySize = DomainElementManager::getInstance().createDomainElementInt64(static_cast<INT_64>(aDomArray.size()));

    // Store array of domains
    pDomArray = DomainArrayUPtr(new DomainArray(aDomArray.size()));
    for (std::size_t idx = 0; idx < aDomArray.size(); ++idx)
    {
      pDomArray->assignElementToCell(idx, aDomArray.operator[](idx));
    }
  }

  PropagatorArray::PropagatorArray(const PropagatorSemantics& aPropagatorSemantics, DomainElementArray& aDomElemArray)
    : Propagator(aPropagatorSemantics)
    , pDomElemArray(nullptr)
  {
    assert(!aDomElemArray.empty());

    // Set array size
    pArraySize = DomainElementManager::getInstance().createDomainElementInt64(static_cast<INT_64>(aDomElemArray.size()));

    // Store array of domains
    pDomElemArray = DomainElementArrayUPtr(new DomainElementArray(aDomElemArray.size()));
    for (std::size_t idx = 0; idx < aDomElemArray.size(); ++idx)
    {
      pDomElemArray->assignElementToCell(idx, aDomElemArray.operator[](idx));
    }
  }

  bool PropagatorArray::isArraySingleton()
  {
    if (hasDomainElementArray())
    {
      return true;
    }
    assert(hasDomainArray());

    for (std::size_t idx = 0; idx < pDomArray->size(); ++idx)
    {
      if (!PropagatorUtils::isDomainSingleton(pDomArray->operator[](idx)))
      {
        return false;
      }
    }
    return true;
  }//isArraySingleton

  Domain* PropagatorArray::domainArrayAtINT64(INT_64 aIdx)
  {
    assert(aIdx >= 0);
    assert(aIdx < arraySizeINT64());
    if (pDomArray == nullptr)
    {
      return nullptr;
    }
    return pDomArray->operator[](static_cast<std::size_t>(aIdx));
  }//domainArrayAtINT64

  Domain* PropagatorArray::domainArrayAt(DomainElement* aDomainElementIdx)
  {
    assert(aDomainElementIdx);
    assert(DomainElementInt64::isa(aDomainElementIdx));
    assert(aDomainElementIdx->zero()->isLessThanOrEqual(aDomainElementIdx));
    assert(aDomainElementIdx->isLessThan(pArraySize));

    if (pDomArray == nullptr)
    {
        return nullptr;
    }
    return pDomArray->operator[](DomainElementInt64::cast(aDomainElementIdx)->getValue());
  }//domainArrayAt

  DomainElement* PropagatorArray::domainElementArrayAtINT64(INT_64 aIdx)
  {
    assert(aIdx >= 0);
    assert(aIdx < arraySizeINT64());
    if (pDomElemArray == nullptr)
    {
      return nullptr;
    }
    return pDomElemArray->operator[](static_cast<std::size_t>(aIdx));
  }//domainArrayAtINT64

  DomainElement* PropagatorArray::domainElementArrayAt(DomainElement* aDomainElementIdx)
  {
      assert(aDomainElementIdx);
      assert(DomainElementInt64::isa(aDomainElementIdx));
      assert(aDomainElementIdx->zero()->isLessThanOrEqual(aDomainElementIdx));
      assert(aDomainElementIdx->isLessThan(pArraySize));

      if (pDomElemArray == nullptr)
      {
          return nullptr;
      }
      return pDomElemArray->operator[](DomainElementInt64::cast(aDomainElementIdx)->getValue());
  }//domainArrayAt

}// end namespace Core

