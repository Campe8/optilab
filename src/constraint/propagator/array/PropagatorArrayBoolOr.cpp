// Base class
#include "PropagatorArrayBoolOr.hpp"

// Domain iterator
#include "DomainIterator.hpp"

// Domain element
#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"

// Utility
#include "PropagatorUtils.hpp"

#include <memory>
#include <cassert>

namespace Core {

  PropagatorArrayBoolOr::PropagatorArrayBoolOr(DomainArray& aDomArray, DomainBoolean *aBoolDom)
    : PropagatorArray(PropagatorSemantics(1,
    PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN,
    PropagationPriority::PROP_PRIORITY_LINEAR,
    PropagatorSemanticType::PROP_SEMANTIC_TYPE_ARRAY_BOOL_OR,
    PropagatorSemanticClass::PROP_SEMANTIC_CLASS_ARRAY),
    aDomArray)
  {
    assert(aBoolDom);
    addContextDomain(D0, aBoolDom);
  }

  PropagatorArrayBoolOr::~PropagatorArrayBoolOr()
  {
  }
  
  double PropagatorArrayBoolOr::getFitness()
  {
    // check if there is any empty or non-ground variable
    if (!isArraySingleton() || !PropagatorUtils::isDomainSingleton(getBoolDomain()))
    {
      return getUnspecFitness();
    }
    for (INT_64 idx = 0; idx < arraySizeINT64(); ++idx)
    {
      Domain* dom = domainArrayAtINT64(idx);
      assert(DomainBoolean::isa(dom));
      if (PropagatorUtils::isDomainEmpty(dom))
      {
        return getInvalidFitness();
      }
    }
    if (PropagatorUtils::isDomainEmpty(getBoolDomain()))
    {
      return getInvalidFitness();
    }

    // if BoolDomain is false,
    // return either 0 or the number of true variables
    if(getBoolDomain()->isFalse())
    {
      std::size_t trueCount = 0;
      for (INT_64 idx = 0; idx < arraySizeINT64(); ++idx)
      {
        Domain* dom = domainArrayAtINT64(idx);
        assert(DomainBoolean::isa(dom));
        if (DomainBoolean::cast(dom)->isTrue())
        {
          ++trueCount;
        }
      }
      return trueCount;
    }
    // if BoolDomain is true,
    // return either 0 or 1
    else
    {
      std::size_t falseCount = 0;
      for (INT_64 idx = 0; idx < arraySizeINT64(); ++idx)
      {
        Domain* dom = domainArrayAtINT64(idx);
        assert(DomainBoolean::isa(dom));
        if (DomainBoolean::cast(dom)->isTrue())
        {
          return getMinFitness();
        }        
      }
      return 1;
    }
  }//getFitness
  
  PropagationEvent PropagatorArrayBoolOr::post()
  {
    /*
     * Post
     * (exists an i in [0..n-1]: as[i]) <-> r
     * r is True  -> check as[i], and work on the number of singletons;
     * r is False -> all domains must be False.
     * as[i] is False for all i -> set r to False
     * as[i] contains at least one True -> set r to True
     */
    if (PropagatorUtils::isDomainSingleton(getBoolDomain()))
    {
      if (getBoolDomain()->isFalse())
      {
        for (std::size_t idx = 0; idx < arraySizeINT64(); ++idx)
        {
          Domain* dom = domainArrayAtINT64(idx);
          assert(DomainBoolean::isa(dom));
          DomainBoolean::cast(dom)->setFalse();
          DOMAIN_FAILURE_CHECK(dom);
        }
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
      
      assert(getBoolDomain()->isTrue());

      std::vector<DomainBoolean*> nonSingletonDomains;
      for (INT_64 idx = 0; idx < arraySizeINT64(); ++idx)
      {
        Domain* dom = domainArrayAtINT64(idx);
        assert(DomainBoolean::isa(dom));

        if (!PropagatorUtils::isDomainSingleton(dom))
        {
          nonSingletonDomains.push_back(DomainBoolean::cast(dom));
        }
        else
        {
          if (DomainBoolean::cast(dom)->isTrue())
          {
            return PropagationEvent::PROP_EVENT_SUBSUMED;
          }
        }
      }

      // All domains are either True or have non singleton domains
      if (nonSingletonDomains.size() == 1)
      {
        nonSingletonDomains[0]->setTrue();
        DOMAIN_FAILURE_CHECK(nonSingletonDomains[0]);
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
      if (nonSingletonDomains.empty())
      {
        // All singleton domains and all True
        return PropagationEvent::PROP_EVENT_FAIL;
      }
      // At least 2 non singleton domains: return PROP_EVENT_RUN_UNSPEC
      return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    }
    
    // r is not singleton, check array as
    std::vector<DomainBoolean*> nonSingletonDomains;
    bool foundNonSingleton = false;
    for (INT_64 idx = 0; idx < arraySizeINT64(); ++idx)
    {
      Domain* dom = domainArrayAtINT64(idx);
      assert(DomainBoolean::isa(dom));

      if (!PropagatorUtils::isDomainSingleton(dom))
      {
        foundNonSingleton = true;
      }
      else if (PropagatorUtils::isDomainSingleton(dom) && DomainBoolean::cast(dom)->isTrue())
      {
        // If there is at least one singleton True domain,
        // set r to True and return
        getBoolDomain()->setTrue();
        DOMAIN_FAILURE_CHECK(getBoolDomain());
        return PropagationEvent::PROP_EVENT_SUBSUMED;
      }
    }
    // At least one domain is not singleton,
    // cannot propagate, return
    if(foundNonSingleton) return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    
    // All domains are singleton and False,
    // set r to False and return
    getBoolDomain()->setFalse();
    DOMAIN_FAILURE_CHECK(getBoolDomain());
    return PropagationEvent::PROP_EVENT_SUBSUMED;
  }//post

  PropagationEvent PropagatorArrayBoolOr::runPropagation()
  {
    auto propagationEvent = post();
    if (propagationEvent == PropagationEvent::PROP_EVENT_RUN_UNSPEC)
    {
      return PropagationEvent::PROP_EVENT_FIXPOINT;
    }
    return propagationEvent;
  }// runPropagation

}// end namespace Core

