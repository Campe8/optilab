// Base class
#include "ConstraintParameterVariable.hpp"

namespace Core {
  
  ConstraintParameterVariable::ConstraintParameterVariable(const VariableSPtr& aVariable)
  : ConstraintParameter(ConstraintParameter::ConstraintParameterClass::CON_PARAM_CLASS_VARIABLE)
  , pVarParameter(aVariable)
  {
    setAsSubject(true);
  }
  
  bool ConstraintParameterVariable::isa(const ConstraintParameter* aParameter)
  {
    return aParameter &&
    aParameter->getParameterClass() == ConstraintParameter::ConstraintParameterClass::CON_PARAM_CLASS_VARIABLE;
  }//isa
  
  const ConstraintParameterVariable* ConstraintParameterVariable::cast(const ConstraintParameter* aParameter)
  {
    if (!isa(aParameter))
    {
      return nullptr;
    }
    return static_cast<const ConstraintParameterVariable*>(aParameter);
  }//cast
  
  ConstraintParameterVariable* ConstraintParameterVariable::cast(ConstraintParameter* aParameter)
  {
    if (!isa(aParameter))
    {
      return nullptr;
    }
    return static_cast<ConstraintParameterVariable*>(aParameter);
  }//cast

}// end namespace Core
