// Base class
#include "ConstraintFactory.hpp"

// Include constraints
#include "ConstraintInc.hpp"

#include <cassert>

namespace Core {

  Constraint* ConstraintFactory::constraint(ConstraintId aCID, const ParamVec& aCP, PropagatorStrategyType aStrategyType)
  {
    switch (aCID)
    {
    case Core::CON_ID_LQ:
      assert(aCP.size() == 2);
      return new ConstraintLq(aCP[0], aCP[1]);
      case Core::CON_ID_GQ:
        assert(aCP.size() == 2);
        return new ConstraintLq(aCP[1], aCP[0]);
    case Core::CON_ID_NQ:
      assert(aCP.size() == 2);
      return new ConstraintNq(aCP[0], aCP[1]);
    case Core::CON_ID_EQ:
        assert(aCP.size() == 2);
        return new ConstraintEq(aCP[0], aCP[1], aStrategyType);
    case Core::CON_ID_LIN_EQ:
        assert(aCP.size() == 3);
        return new ConstraintLinearEq(aCP[0], aCP[1], aCP[2]);
    case Core::CON_ID_LIN_NQ:
        assert(aCP.size() == 3);
        return new ConstraintLinearNq(aCP[0], aCP[1], aCP[2]);
    case Core::CON_ID_LIN_LQ:
        assert(aCP.size() == 3);
        return new ConstraintLinearLq(aCP[0], aCP[1], aCP[2]);
    case Core::CON_ID_LIN_LT:
        assert(aCP.size() == 3);
        return new ConstraintLinearLt(aCP[0], aCP[1], aCP[2]);
    case Core::CON_ID_TIMES:
        assert(aCP.size() == 3);
        return new ConstraintTimes(aCP[0], aCP[1], aCP[2], aStrategyType);
    case Core::CON_ID_ABS:
        assert(aCP.size() == 2);
        return new ConstraintAbs(aCP[0], aCP[1], aStrategyType);
    case Core::CON_ID_DIV:
        assert(aCP.size() == 3);
        return new ConstraintDiv(aCP[0], aCP[1], aCP[2]);
    case Core::CON_ID_LT:
      assert(aCP.size() == 2);
      return new ConstraintLt(aCP[0], aCP[1]);
    case Core::CON_ID_GT:
        assert(aCP.size() == 2);
        return new ConstraintLt(aCP[1], aCP[0]);
    case Core::CON_ID_MAX:
        assert(aCP.size() == 3);
        return new ConstraintMax(aCP[0], aCP[1], aCP[2], aStrategyType);
    case Core::CON_ID_MIN:
        assert(aCP.size() == 3);
        return new ConstraintMin(aCP[0], aCP[1], aCP[2], aStrategyType);
    case Core::CON_ID_PLUS:
        assert(aCP.size() == 3);
        return new ConstraintPlus(aCP[0], aCP[1], aCP[2], aStrategyType);
    case Core::CON_ID_MINUS:
        assert(aCP.size() == 3);
        return new ConstraintPlus(aCP[1], aCP[2], aCP[0], aStrategyType);
    case Core::CON_ID_EQ_REIF:
        assert(aCP.size() == 3);
        return new ConstraintEqReif(aCP[0], aCP[1], aCP[2], aStrategyType);
    case Core::CON_ID_EQ_HREIF_R:
        assert(aCP.size() == 3);
        return new ConstraintEqHalfReifRight(aCP[0], aCP[1], aCP[2], aStrategyType);
    case Core::CON_ID_EQ_HREIF_L:
        assert(aCP.size() == 3);
        return new ConstraintEqHalfReifLeft(aCP[0], aCP[1], aCP[2], aStrategyType);
    case Core::CON_ID_LQ_REIF:
        assert(aCP.size() == 3);
        return new ConstraintLqReif(aCP[0], aCP[1], aCP[2]);
    case Core::CON_ID_LQ_HREIF_R:
        assert(aCP.size() == 3);
        return new ConstraintLqHalfReifRight(aCP[0], aCP[1], aCP[2]);
    case Core::CON_ID_LQ_HREIF_L:
        assert(aCP.size() == 3);
        return new ConstraintLqHalfReifLeft(aCP[0], aCP[1], aCP[2]);
    case Core::CON_ID_GQ_REIF:
        assert(aCP.size() == 3);
        return new ConstraintLqReif(aCP[1], aCP[0], aCP[2]);
    case Core::CON_ID_GQ_HREIF_L:
        assert(aCP.size() == 3);
        return new ConstraintLqHalfReifLeft(aCP[1], aCP[0], aCP[2]);
    case Core::CON_ID_GQ_HREIF_R:
        assert(aCP.size() == 3);
        return new ConstraintLqHalfReifRight(aCP[1], aCP[0], aCP[2]);
    case Core::CON_ID_LIN_EQ_REIF:
      assert(aCP.size() == 4);
      return new ConstraintLinearEqReif(aCP[0], aCP[1], aCP[2], aCP[3]);
    case Core::CON_ID_LIN_EQ_HREIF_R:
        assert(aCP.size() == 4);
        return new ConstraintLinearEqHalfReifRight(aCP[0], aCP[1], aCP[2], aCP[3]);
    case Core::CON_ID_LIN_EQ_HREIF_L:
        assert(aCP.size() == 4);
        return new ConstraintLinearEqHalfReifLeft(aCP[0], aCP[1], aCP[2], aCP[3]);
    case Core::CON_ID_LIN_LQ_REIF:
        assert(aCP.size() == 4);
        return new ConstraintLinearLqReif(aCP[0], aCP[1], aCP[2], aCP[3]);
    case Core::CON_ID_LIN_LQ_HREIF_R:
        assert(aCP.size() == 4);
        return new ConstraintLinearLqHalfReifRight(aCP[0], aCP[1], aCP[2], aCP[3]);
    case Core::CON_ID_LIN_LQ_HREIF_L:
        assert(aCP.size() == 4);
        return new ConstraintLinearLqHalfReifLeft(aCP[0], aCP[1], aCP[2], aCP[3]);
    case Core::CON_ID_LIN_NQ_REIF:
        assert(aCP.size() == 4);
        return new ConstraintLinearNqReif(aCP[0], aCP[1], aCP[2], aCP[3]);
    case Core::CON_ID_LIN_NQ_HREIF_R:
        assert(aCP.size() == 4);
        return new ConstraintLinearNqHalfReifRight(aCP[0], aCP[1], aCP[2], aCP[3]);
    case Core::CON_ID_LIN_NQ_HREIF_L:
        assert(aCP.size() == 4);
        return new ConstraintLinearNqHalfReifLeft(aCP[0], aCP[1], aCP[2], aCP[3]);
    case Core::CON_ID_LT_REIF:
        assert(aCP.size() == 3);
        return new ConstraintLtReif(aCP[0], aCP[1], aCP[2]);
    case Core::CON_ID_LT_HREIF_R:
        assert(aCP.size() == 3);
        return new ConstraintLtHalfReifRight(aCP[0], aCP[1], aCP[2]);
    case Core::CON_ID_LT_HREIF_L:
        assert(aCP.size() == 3);
        return new ConstraintLtHalfReifLeft(aCP[0], aCP[1], aCP[2]);
    case Core::CON_ID_GT_REIF:
        assert(aCP.size() == 3);
        return new ConstraintLtReif(aCP[1], aCP[0], aCP[2]);
    case Core::CON_ID_GT_HREIF_L:
        assert(aCP.size() == 3);
        return new ConstraintLtHalfReifLeft(aCP[1], aCP[0], aCP[2]);
    case Core::CON_ID_GT_HREIF_R:
        assert(aCP.size() == 3);
        return new ConstraintLtHalfReifRight(aCP[1], aCP[0], aCP[2]);
    case Core::CON_ID_NQ_REIF:
        assert(aCP.size() == 3);
        return new ConstraintNqReif(aCP[0], aCP[1], aCP[2]);
    case Core::CON_ID_NQ_HREIF_R:
        assert(aCP.size() == 3);
        return new ConstraintNqHalfReifRight(aCP[0], aCP[1], aCP[2]);
    case Core::CON_ID_NQ_HREIF_L:
        assert(aCP.size() == 3);
        return new ConstraintNqHalfReifLeft(aCP[0], aCP[1], aCP[2]);
    case Core::CON_ID_BRANCHING:
      break;
    case Core::CON_ID_ALLDIFF:
        return new ConstraintAllDiff(aCP[0]);
    case Core::CON_ID_CIRCUIT:
        return new ConstraintCircuit(aCP[0]);
    case Core::CON_ID_DOMAIN:
        assert(aCP.size() == 3);
        return new ConstraintDomain(aCP[0], aCP[1], aCP[2]);
    case Core::CON_ID_ELEMENT:
        assert(aCP.size() == 3);
        return new ConstraintElement(aCP[0], aCP[1], aCP[2]);
    default:
      assert(aCID == Core::CON_ID_MOD);
      break;
    }
    return nullptr;
  }//constraint

}// end namespace Core
