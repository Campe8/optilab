// Base class
#include "ConstraintParameterDomainArray.hpp"

#include "DomainFactory.hpp"

namespace Core {
  
  ConstraintParameterDomainArray::ConstraintParameterDomainArray(const DomainSPtrArraySPtr& aDomainArray)
  : ConstraintParameter(ConstraintParameter::ConstraintParameterClass::CON_PARAM_CLASS_DOMAIN_ARRAY)
  , pDomParameter(aDomainArray)
  {
    pSubjectArray.resize(size(), false);
    
    // Array is not a subject by default
    setAsSubject(false);
  }
  
  ConstraintParameterDomainArray::ConstraintParameterDomainArray(const DomainElementArraySPtr& aDomainElementArray, DomainClass aDomainClass)
  : ConstraintParameter(ConstraintParameter::ConstraintParameterClass::CON_PARAM_CLASS_DOMAIN_ARRAY)
  {
    DomainFactory df;
    pDomParameter = std::make_shared<DomainSPtrArray>(aDomainElementArray->size());
    for(std::size_t idx = 0; idx < aDomainElementArray->size(); ++idx)
    {
      pDomParameter->assignElementToCell(idx, std::shared_ptr<Domain>(df.domain(aDomainElementArray->operator[](idx), aDomainClass)));
    }
    
    pSubjectArray.resize(aDomainElementArray->size(), false);
    
    // Array is not a subject by default
    setAsSubject(false);
  }
  
  bool ConstraintParameterDomainArray::isa(const ConstraintParameter* aParameter)
  {
    return aParameter &&
    aParameter->getParameterClass() == ConstraintParameter::ConstraintParameterClass::CON_PARAM_CLASS_DOMAIN_ARRAY;
  }//isa
  
  ConstraintParameterDomainArray* ConstraintParameterDomainArray::cast(ConstraintParameter* aParameter)
  {
    if (!isa(aParameter))
    {
      return nullptr;
    }
    return static_cast<ConstraintParameterDomainArray*>(aParameter);
  }//cast
  
  const ConstraintParameterDomainArray* ConstraintParameterDomainArray::cast(const ConstraintParameter* aParameter)
  {
    if (!isa(aParameter))
    {
      return nullptr;
    }
    return static_cast<const ConstraintParameterDomainArray*>(aParameter);
  }//cast
  
  std::size_t ConstraintParameterDomainArray::size() const
  {
    if (!pDomParameter) { return 0; }
    return pDomParameter->size();
  }//size
  
  bool ConstraintParameterDomainArray::isSubject(std::size_t aIdx)
  {
    assert(aIdx < pSubjectArray.size());
    return pSubjectArray[aIdx];
  }//isSubject
  
  void ConstraintParameterDomainArray::setAllAsSubject()
  {
    ConstraintParameter::setAsSubject(true);
    for (std::size_t idx{ 0 }; idx < pSubjectArray.size(); ++idx)
    {
      pSubjectArray[idx] = true;
    }
  }//setAllAsSubject
  
  void ConstraintParameterDomainArray::setAsSubjectGivenIndex(std::size_t aIdx)
  {
    assert(aIdx < pSubjectArray.size());
    pSubjectArray[aIdx] = true;
  }//setAsSubjectGivenIndex
  
}// end namespace Core

