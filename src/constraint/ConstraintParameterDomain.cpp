// Base class
#include "ConstraintParameterDomain.hpp"

namespace Core {
  
  ConstraintParameterDomain::ConstraintParameterDomain(const DomainSPtr& aDomain)
  : ConstraintParameter(ConstraintParameter::ConstraintParameterClass::CON_PARAM_CLASS_DOMAIN)
  , pDomParameter(aDomain)
  {
    setAsSubject(false);
  }
  
  bool ConstraintParameterDomain::isa(const ConstraintParameter* aParameter)
  {
    return aParameter &&
    aParameter->getParameterClass() == ConstraintParameter::ConstraintParameterClass::CON_PARAM_CLASS_DOMAIN;
  }//isa
  
  const ConstraintParameterDomain* ConstraintParameterDomain::cast(const ConstraintParameter* aParameter)
  {
    if (!isa(aParameter))
    {
      return nullptr;
    }
    return static_cast<const ConstraintParameterDomain*>(aParameter);
  }//cast
  
  ConstraintParameterDomain* ConstraintParameterDomain::cast(ConstraintParameter* aParameter)
  {
    if (!isa(aParameter))
    {
      return nullptr;
    }
    return static_cast<ConstraintParameterDomain*>(aParameter);
  }//cast
  
}// end namespace Core
