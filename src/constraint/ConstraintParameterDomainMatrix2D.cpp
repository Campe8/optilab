// Base class
#include "ConstraintParameterDomainMatrix2D.hpp"

#include "DomainFactory.hpp"

namespace Core {
  
  ConstraintParameterDomainMatrix2D::ConstraintParameterDomainMatrix2D(const DomainMatrix2DSPtr& aDomainMatrix2D)
  : ConstraintParameter(ConstraintParameter::ConstraintParameterClass::CON_PARAM_CLASS_DOMAIN_MATRIX_2D)
  , pDomParameter(aDomainMatrix2D)
  {
    setAsSubject(false);
  }
  
}// end namespace Core
