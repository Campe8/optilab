// Base class
#include "ConstraintParameter.hpp"

namespace Core {
  
  ConstraintParameter::ConstraintParameter(ConstraintParameterClass aParamClass)
  : pParameterClass(aParamClass)
  , pActAsSubject(false)
  {
  }
  
}// end namespace Core
