// Base class
#include "ConstraintParameterFactory.hpp"

#include "ConstraintParameterDomainArray.hpp"
#include "ConstraintParameterDomainMatrix2D.hpp"
#include "VariableSemanticSupport.hpp"

#include "DArrayMatrix.hpp"

namespace Core {
  
  ConstraintParameterFactory::ConstraintParameterFactory()
  : pVariableFactory(new VariableFactory())
  {
    pDomainFactory = std::make_shared<DomainFactory>();
  }
  
  ConstraintParameterUPtr ConstraintParameterFactory::constraintParameterVariable(const std::vector<VariableSPtr>& aVariableArray)
  {
    auto variable = pVariableFactory->variableArray(aVariableArray, std::unique_ptr<VariableSemanticSupport>(new VariableSemanticSupport()));
    return ConstraintParameterUPtr(new ConstraintParameterVariable(std::shared_ptr<Variable>(variable)));
  }// constraintParameterVariable
  
  ConstraintParameterUPtr ConstraintParameterFactory::constraintParameterDomain(const std::vector<DomainSPtr>& aDomainArray)
  {
    DomainSPtrArraySPtr dArray = std::make_shared<DomainSPtrArray>(aDomainArray.size());
    
    std::size_t idx{0};
    for(auto& domPtr : aDomainArray)
    {
      dArray->assignElementToCell(idx++, domPtr);
    }
    return ConstraintParameterUPtr(new ConstraintParameterDomainArray(dArray));
  }// constraintParameterDomain
  
  ConstraintParameterUPtr ConstraintParameterFactory::constraintParameterDomain(const std::vector<DomainElement*>& aDomainElementArray,
                                                                                DomainClass aDomainClass)
  {
    DomainElementArraySPtr dArray = std::make_shared<DomainElementArray>(aDomainElementArray.size());
    
    std::size_t idx{0};
    for(auto& domElemPtr : aDomainElementArray)
    {
      dArray->assignElementToCell(idx++, domElemPtr);
    }
    return ConstraintParameterUPtr(new ConstraintParameterDomainArray(dArray, aDomainClass));
  }// constraintParameterDomain
  
}// end namespace Core
