// Base class
#include "FilteringAlgorithmINTERSECT.hpp"

#include <stdexcept>
#include <cassert>

namespace Core {
  
  FilteringAlgorithmINTERSECT::FilteringAlgorithmINTERSECT()
  : FilteringAlgorithm(FilteringAlgorithmClass::FILTERING_ALGO_CLASS_SETOP,
                       FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_INTERSECT)
  , pIntersectionSet(IntersectionSetType::IS_DOMAIN)
  {
  }
  
  FilteringAlgorithmINTERSECT::~FilteringAlgorithmINTERSECT()
  {
  }
  
  Domain* FilteringAlgorithmINTERSECT::filter(const FilteringParameters& aFilteringParams) const
  {
    assert(aFilteringParams.getOutputDomain());
    Domain *outDomain = aFilteringParams.getOutputDomain();
    
    // Shrink output domain on abs(domainVal)
    if(pIntersectionSet == IntersectionSetType::IS_DOMAIN)
    {
      assert(aFilteringParams.hasInputDomainArray());
      assert(aFilteringParams.getInputDomainArray()->size() > 0);
      outDomain->intersect(aFilteringParams.getInputDomainArray()->operator[](0));
    }
    else
    {
      assert(pIntersectionSet == IntersectionSetType::IS_DOMAIN_ELEMENT_ARRAY);
      assert(aFilteringParams.hasDomainElementArray());
      outDomain->intersect(aFilteringParams.getDomainElementArray().get());
    }
    
    return outDomain;
  }//filter
  
}// end namespace Core
