// Base class
#include "FilteringAlgorithmTIMES.hpp"

#include <stdexcept>
#include <cassert>

namespace Core {
  
  FilteringAlgorithmTIMES::FilteringAlgorithmTIMES()
  : FilteringAlgorithm(FilteringAlgorithmClass::FILTERING_ALGO_CLASS_ARITHMETIC,
                       FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_TIMES)
  {
  }
  
  FilteringAlgorithmTIMES::~FilteringAlgorithmTIMES()
  {
  }
  
  Domain* FilteringAlgorithmTIMES::filter(const FilteringParameters& aFilteringParams) const
  {
    assert(aFilteringParams.hasDomainElementArray());
    assert(aFilteringParams.getDomainElementArray()->size() >= 2);
    assert(aFilteringParams.getOutputDomain());
    
    Domain *outDomain = aFilteringParams.getOutputDomain();
    
    // Shrink output domain on DomainElement_0 + DomainElement_1
    DomainElement *timesVal = (aFilteringParams.getDomainElementArray()->operator[](0))->
    times(aFilteringParams.getDomainElementArray()->operator[](1));
    
    outDomain->shrink(timesVal, timesVal);
    
    return outDomain;
  }//filter
  
}// end namespace Core
