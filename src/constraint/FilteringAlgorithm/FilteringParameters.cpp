// Base class
#include "FilteringParameters.hpp"

#include <stdexcept>

namespace Core {
  
  FilteringParameters::FilteringParameters()
  : pOutputDomain(nullptr)
  , pInputDomainArray(nullptr)
  , pInputDomainMatrix2D(nullptr)
  , pDomainElementArray(nullptr)
  , pDomainElementMatrix2D(nullptr)
  {
  }
  
  FilteringParameters::~FilteringParameters()
  {
  }
  
}// end namespace Core
