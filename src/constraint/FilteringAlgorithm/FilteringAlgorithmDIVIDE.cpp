// Base class
#include "FilteringAlgorithmDIVIDE.hpp"

#include <stdexcept>
#include <cassert>

namespace Core {
  
  FilteringAlgorithmDIVIDE::FilteringAlgorithmDIVIDE()
  : FilteringAlgorithm(FilteringAlgorithmClass::FILTERING_ALGO_CLASS_ARITHMETIC,
                       FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_DIVIDE)
  , pRoundingMode(DivisionRoundingMode::DIV_ROUNDING_UNSPEC)
  {
  }
  
  FilteringAlgorithmDIVIDE::~FilteringAlgorithmDIVIDE()
  {
  }
  
  Domain* FilteringAlgorithmDIVIDE::filter(const FilteringParameters& aFilteringParams) const
  {
    assert(aFilteringParams.hasDomainElementArray());
    assert(aFilteringParams.getDomainElementArray()->size() >= 2);
    assert(aFilteringParams.getOutputDomain());
    
    Domain *outDomain = aFilteringParams.getOutputDomain();
    
    // Shrink output domain on division(element[0]/element[1])
    DomainElement *dividend = aFilteringParams.getDomainElementArray()->operator[](0);
    
    // Set rounding mode
    switch (pRoundingMode) {
      case DivisionRoundingMode::DIV_ROUNDING_CEILING:
        dividend->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_CEILING);
        break;
      case DivisionRoundingMode::DIV_ROUNDING_FLOOR:
        dividend->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_FLOOR);
        break;
      default:
        assert(pRoundingMode == DivisionRoundingMode::DIV_ROUNDING_UNSPEC);
        dividend->setRoundingMode(DomainElement::RoundingMode::DE_ROUNDING_UNSPEC);
        break;
    }
    
    // Get divion element according with its rounding mode
    DomainElement *divVal = dividend->div(aFilteringParams.getDomainElementArray()->operator[](1));
    
    outDomain->shrink(divVal, divVal);
    
    return outDomain;
  }//filter
  
}// end namespace Core
