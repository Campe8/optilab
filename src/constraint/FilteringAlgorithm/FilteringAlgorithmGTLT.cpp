// Base class
#include "FilteringAlgorithmGTLT.hpp"

#include <stdexcept>
#include <cassert>

namespace Core {
  
  FilteringAlgorithmGTLT::FilteringAlgorithmGTLT()
  : FilteringAlgorithm(FilteringAlgorithmClass::FILTERING_ALGO_CLASS_RELATIONAL,
                       FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GTLT)
  {
  }
  
  FilteringAlgorithmGTLT::~FilteringAlgorithmGTLT()
  {
  }
  
  Domain* FilteringAlgorithmGTLT::filter(const FilteringParameters& aFilteringParams) const
  {
    assert(aFilteringParams.hasDomainElementArray());
    assert(aFilteringParams.getDomainElementArray()->size() > 1);
    assert(aFilteringParams.getOutputDomain());
    
    Domain *outDomain = aFilteringParams.getOutputDomain();
    
    // Shrink output domain on bounds
    outDomain->shrink(aFilteringParams.getDomainElementArray()->operator[](0)->successor(),
                      aFilteringParams.getDomainElementArray()->operator[](1)->predecessor());
    
    return outDomain;
  }//filter
  
}// end namespace Core
