// Base class
#include "FilteringAlgorithmFALSE.hpp"

#include "DomainBoolean.hpp"

#include <stdexcept>
#include <cassert>

namespace Core {
  
  FilteringAlgorithmFALSE::FilteringAlgorithmFALSE()
  : FilteringAlgorithm(FilteringAlgorithmClass::FILTERING_ALGO_CLASS_BOOLEAN,
                       FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_FALSE)
  {
  }
  
  FilteringAlgorithmFALSE::~FilteringAlgorithmFALSE()
  {
  }
  
  Domain* FilteringAlgorithmFALSE::filter(const FilteringParameters& aFilteringParams) const
  {
    assert(aFilteringParams.getOutputDomain());
    
    Domain *outDomain = aFilteringParams.getOutputDomain();
    assert(DomainBoolean::isa(outDomain));
    
    // Shrink output domain on domainVal (included)
    DomainBoolean::cast(outDomain)->setFalse();
    
    return outDomain;
  }//filter
  
}// end namespace Core
