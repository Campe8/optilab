// Base class
#include "FilteringAlgorithmEQ.hpp"

#include <stdexcept>
#include <cassert>

namespace Core {
  
  FilteringAlgorithmEQ::FilteringAlgorithmEQ()
  : FilteringAlgorithm(FilteringAlgorithmClass::FILTERING_ALGO_CLASS_RELATIONAL,
                       FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_EQ)
  {
  }
  
  FilteringAlgorithmEQ::~FilteringAlgorithmEQ()
  {
  }
  
  Domain* FilteringAlgorithmEQ::filter(const FilteringParameters& aFilteringParams) const
  {
    assert(aFilteringParams.hasDomainElementArray());
    assert(aFilteringParams.getDomainElementArray()->size() > 0);
    assert(aFilteringParams.getOutputDomain());
    
    Domain *outDomain = aFilteringParams.getOutputDomain();
    
    // Shrink output domain on domainVal (included)
    DomainElement *singleton = aFilteringParams.getDomainElementArray()->operator[](0);
    outDomain->shrink(singleton, singleton);
    
    return outDomain;
  }//filter
  
}// end namespace Core
