// Base class
#include "FilteringAlgorithmMINUS.hpp"

#include <stdexcept>
#include <cassert>

namespace Core {
  
  FilteringAlgorithmMINUS::FilteringAlgorithmMINUS()
  : FilteringAlgorithm(FilteringAlgorithmClass::FILTERING_ALGO_CLASS_ARITHMETIC,
                       FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_MINUS)
  {
  }
  
  FilteringAlgorithmMINUS::~FilteringAlgorithmMINUS()
  {
  }
  
  Domain* FilteringAlgorithmMINUS::filter(const FilteringParameters& aFilteringParams) const
  {
    assert(aFilteringParams.hasDomainElementArray());
    assert(aFilteringParams.getDomainElementArray()->size() >= 2);
    assert(aFilteringParams.getOutputDomain());
    
    Domain *outDomain = aFilteringParams.getOutputDomain();
    
    // Shrink output domain on DomainElement_0 - DomainElement_1
    DomainElement *minusVal = (aFilteringParams.getDomainElementArray()->operator[](0))->
    plus(DomainElement::mirror(aFilteringParams.getDomainElementArray()->operator[](1)));
    
    outDomain->shrink(minusVal, minusVal);
    
    return outDomain;
  }//filter
  
}// end namespace Core
