// Base class
#include "FilteringAlgorithmGT.hpp"

#include <stdexcept>
#include <cassert>

namespace Core {
  
  FilteringAlgorithmGT::FilteringAlgorithmGT()
  : FilteringAlgorithm(FilteringAlgorithmClass::FILTERING_ALGO_CLASS_RELATIONAL,
                       FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GT)
  {
  }
  
  FilteringAlgorithmGT::~FilteringAlgorithmGT()
  {
  }
  
  Domain* FilteringAlgorithmGT::filter(const FilteringParameters& aFilteringParams) const
  {
    assert(aFilteringParams.hasDomainElementArray());
    assert(aFilteringParams.getDomainElementArray()->size() > 0);
    assert(aFilteringParams.getOutputDomain());
    
    Domain *outDomain = aFilteringParams.getOutputDomain();
    
    // Shrink output domain on domainVal (included)
    outDomain->shrinkOnLowerBound((aFilteringParams.getDomainElementArray()->operator[](0))->successor());
    
    return outDomain;
  }//filter
  
}// end namespace Core
