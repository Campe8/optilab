// Base class
#include "FilteringAlgorithmTRUE.hpp"

#include "DomainBoolean.hpp"

#include <stdexcept>
#include <cassert>

namespace Core {
  
  FilteringAlgorithmTRUE::FilteringAlgorithmTRUE()
  : FilteringAlgorithm(FilteringAlgorithmClass::FILTERING_ALGO_CLASS_BOOLEAN,
                       FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_TRUE)
  {
  }
  
  FilteringAlgorithmTRUE::~FilteringAlgorithmTRUE()
  {
  }
  
  Domain* FilteringAlgorithmTRUE::filter(const FilteringParameters& aFilteringParams) const
  {
    assert(aFilteringParams.getOutputDomain());
    
    Domain *outDomain = aFilteringParams.getOutputDomain();
    assert(DomainBoolean::isa(outDomain));
    
    // Shrink output domain on domainVal (included)
    DomainBoolean::cast(outDomain)->setTrue();
    
    return outDomain;
  }//filter
  
}// end namespace Core
