// Base class
#include "FilteringAlgorithm.hpp"

#include <stdexcept>

namespace Core {
  
  FilteringAlgorithm::FilteringAlgorithm(FilteringAlgorithmClass aFilteringAlgorithmClass,
                                         FilteringAlgorithmSemanticType aFilteringAlgorithmSemanticType)
  : pFilteringAlgorithmClass(aFilteringAlgorithmClass)
  , pFilteringAlgorithmSemanticType(aFilteringAlgorithmSemanticType)
  {
  }
  
  FilteringAlgorithm::~FilteringAlgorithm()
  {
  }
}// end namespace Core
