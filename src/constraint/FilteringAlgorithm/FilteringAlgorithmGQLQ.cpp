// Base class
#include "FilteringAlgorithmGQLQ.hpp"

#include <stdexcept>
#include <cassert>

namespace Core {
  
  FilteringAlgorithmGQLQ::FilteringAlgorithmGQLQ()
  : FilteringAlgorithm(FilteringAlgorithmClass::FILTERING_ALGO_CLASS_RELATIONAL,
                       FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_GQLQ)
  {
  }
  
  FilteringAlgorithmGQLQ::~FilteringAlgorithmGQLQ()
  {
  }
  
  Domain* FilteringAlgorithmGQLQ::filter(const FilteringParameters& aFilteringParams) const
  {
    assert(aFilteringParams.hasDomainElementArray());
    assert(aFilteringParams.getDomainElementArray()->size() > 1);
    assert(aFilteringParams.getOutputDomain());
    
    Domain *outDomain = aFilteringParams.getOutputDomain();
    
    // Shrink output domain on bounds
    outDomain->shrink(aFilteringParams.getDomainElementArray()->operator[](0), aFilteringParams.getDomainElementArray()->operator[](1));
    
    return outDomain;
  }//filter
  
}// end namespace Core
