// Base class
#include "FilteringAlgorithmNQ.hpp"

#include <stdexcept>
#include <cassert>

namespace Core {
  
  FilteringAlgorithmNQ::FilteringAlgorithmNQ()
  : FilteringAlgorithm(FilteringAlgorithmClass::FILTERING_ALGO_CLASS_RELATIONAL,
                       FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_NQ)
  {
  }
  
  FilteringAlgorithmNQ::~FilteringAlgorithmNQ()
  {
  }
  
  Domain* FilteringAlgorithmNQ::filter(const FilteringParameters& aFilteringParams) const
  {
    assert(aFilteringParams.hasDomainElementArray());
    assert(aFilteringParams.getDomainElementArray()->size() > 0);
    assert(aFilteringParams.getOutputDomain());
    
    Domain *outDomain = aFilteringParams.getOutputDomain();
    
    // Subtract element from domain
    outDomain->subtract(aFilteringParams.getDomainElementArray()->operator[](0));
    
    return outDomain;
  }//filter
  
}// end namespace Core
