// Base class
#include "FilteringAlgorithmABS.hpp"

#include <stdexcept>
#include <cassert>

namespace Core {
  
  FilteringAlgorithmABS::FilteringAlgorithmABS()
  : FilteringAlgorithm(FilteringAlgorithmClass::FILTERING_ALGO_CLASS_ARITHMETIC,
                       FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_ABS)
  {
  }
  
  FilteringAlgorithmABS::~FilteringAlgorithmABS()
  {
  }
  
  Domain* FilteringAlgorithmABS::filter(const FilteringParameters& aFilteringParams) const
  {
    assert(aFilteringParams.getOutputDomain());
    assert(aFilteringParams.hasDomainElementArray());
    assert(aFilteringParams.getDomainElementArray()->size() > 0);
    
    Domain *outDomain = aFilteringParams.getOutputDomain();
    
    // Shrink output domain on abs(domainVal)
    DomainElement *absVal = aFilteringParams.getDomainElementArray()->operator[](0)->abs();
    outDomain->shrink(absVal, absVal);
    
    return outDomain;
  }//filter
  
}// end namespace Core
