// Base class
#include "FilteringAlgorithmLT.hpp"

#include <stdexcept>
#include <cassert>

namespace Core {
  
  FilteringAlgorithmLT::FilteringAlgorithmLT()
  : FilteringAlgorithm(FilteringAlgorithmClass::FILTERING_ALGO_CLASS_RELATIONAL,
                       FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_LT)
  {
  }
  
  FilteringAlgorithmLT::~FilteringAlgorithmLT()
  {
  }
  
  Domain* FilteringAlgorithmLT::filter(const FilteringParameters& aFilteringParams) const
  {
    assert(aFilteringParams.hasDomainElementArray());
    assert(aFilteringParams.getDomainElementArray()->size() > 0);
    assert(aFilteringParams.getOutputDomain());
    
    Domain *outDomain = aFilteringParams.getOutputDomain();
    
    // Shrink output domain on domainVal (included)
    outDomain->shrinkOnUpperBound((aFilteringParams.getDomainElementArray()->operator[](0))->predecessor());
    
    return outDomain;
  }//filter
  
}// end namespace Core
