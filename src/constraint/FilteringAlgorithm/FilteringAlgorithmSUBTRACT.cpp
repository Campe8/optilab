// Base class
#include "FilteringAlgorithmSUBTRACT.hpp"

#include <stdexcept>
#include <cassert>

namespace Core {
  
  FilteringAlgorithmSUBTRACT::FilteringAlgorithmSUBTRACT()
  : FilteringAlgorithm(FilteringAlgorithmClass::FILTERING_ALGO_CLASS_SETOP,
                       FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_SUBTRACT)
  , pSubtractSet(SS_DOMAIN)
  {
  }
  
  FilteringAlgorithmSUBTRACT::~FilteringAlgorithmSUBTRACT()
  {
  }
  
  Domain* FilteringAlgorithmSUBTRACT::filter(const FilteringParameters& aFilteringParams) const
  {
    assert(aFilteringParams.getOutputDomain());
    Domain *outDomain = aFilteringParams.getOutputDomain();
    
    // Shrink output domain on abs(domainVal)
    if(pSubtractSet == SubtractSetType::SS_DOMAIN)
    {
      assert(aFilteringParams.hasInputDomainArray());
      assert(aFilteringParams.getInputDomainArray()->size() > 0);
      outDomain->subtract(aFilteringParams.getInputDomainArray()->operator[](0));
    }
    else
    {
      assert(pSubtractSet == SubtractSetType::SS_DOMAIN_ELEMENT_ARRAY);
      assert(aFilteringParams.hasDomainElementArray());
      outDomain->subtract(aFilteringParams.getDomainElementArray().get());
    }
    
    return outDomain;
  }//filter
  
}// end namespace Core
