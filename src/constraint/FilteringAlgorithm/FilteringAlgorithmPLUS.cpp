// Base class
#include "FilteringAlgorithmPLUS.hpp"

#include <stdexcept>
#include <cassert>

namespace Core {
  
  FilteringAlgorithmPLUS::FilteringAlgorithmPLUS()
  : FilteringAlgorithm(FilteringAlgorithmClass::FILTERING_ALGO_CLASS_ARITHMETIC,
                       FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_PLUS)
  {
  }
  
  FilteringAlgorithmPLUS::~FilteringAlgorithmPLUS()
  {
  }
  
  Domain* FilteringAlgorithmPLUS::filter(const FilteringParameters& aFilteringParams) const
  {
    assert(aFilteringParams.hasDomainElementArray());
    assert(aFilteringParams.getDomainElementArray()->size() >= 2);
    assert(aFilteringParams.getOutputDomain());
    
    Domain *outDomain = aFilteringParams.getOutputDomain();
    
    // Shrink output domain on DomainElement_0 + DomainElement_1
    DomainElement *plusVal = (aFilteringParams.getDomainElementArray()->operator[](0))->
    plus(aFilteringParams.getDomainElementArray()->operator[](1));
    
    outDomain->shrink(plusVal, plusVal);
    
    return outDomain;
  }//filter
  
}// end namespace Core
