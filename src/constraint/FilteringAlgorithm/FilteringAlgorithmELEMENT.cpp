// Base class
#include "FilteringAlgorithmELEMENT.hpp"

#include "DomainElementInt64.hpp"

#include <stdexcept>
#include <cassert>

namespace Core {
  
  FilteringAlgorithmELEMENT::FilteringAlgorithmELEMENT()
  : FilteringAlgorithm(FilteringAlgorithmClass::FILTERING_ALGO_CLASS_ELEMENT,
                       FilteringAlgorithmSemanticType::FILTERING_ALGO_SEM_TYPE_ELEMENT)
  {
  }
  
  FilteringAlgorithmELEMENT::~FilteringAlgorithmELEMENT()
  {
  }
  
  Domain* FilteringAlgorithmELEMENT::filter(const FilteringParameters& aFilteringParams) const
  {
    assert(aFilteringParams.hasDomainElementArray());
    assert(aFilteringParams.getDomainElementArray()->size() > 1);
    assert(aFilteringParams.getOutputDomain());
    
    Domain *outDomain = aFilteringParams.getOutputDomain();
    
    DomainElementArray* array = aFilteringParams.getDomainElementArray().get();
    DomainElement *idxElement = array->operator[](0);
    assert(DomainElementInt64::isa(idxElement));
    
    DomainElementInt64 * castIdxElement = DomainElementInt64::cast(idxElement);
    assert(castIdxElement->getValue() >= 0);
    assert(castIdxElement->getValue() <= static_cast<INT_64>(array->size()));
    
    std::size_t idx = static_cast<std::size_t>(castIdxElement->getValue()) + 1;
    outDomain->shrink(array->operator[](idx), array->operator[](idx));
    
    return outDomain;
  }//filter
  
}// end namespace Core
