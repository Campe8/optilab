// Base class
#include "ConstraintBinary.hpp"

// Variable
#include "Variable.hpp"

#include <utility>
#include <cassert>

namespace Core {
  
  ConstraintBinary::ConstraintBinary(ConstraintId aConstraintId,
                                     PropagatorStrategyType aStrategyType,
                                     const ConstraintParameterSPtr& aParam1,
                                     const ConstraintParameterSPtr& aParam2)
  : Constraint(aConstraintId, aStrategyType)
  {
    registerConstraintParameter(aParam1);
    registerConstraintParameter(aParam2);
    assert(numConstraintParameters() == 2);
  }
  
  ConstraintBinary::~ConstraintBinary()
  {
  }
  
  PropagationCost ConstraintBinary::reducedCost(const PropagationCost& aPropagationCost)
  {
    if(scopeSize() <= 1)
    {
      static PropagationCost unaryCost(PropagationPriority::PROP_PRIORITY_UNARY);
      return unaryCost;
    }
    return aPropagationCost;
  }//reducedCost
  
}// end namespace Core
