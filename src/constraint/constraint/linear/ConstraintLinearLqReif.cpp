// Self first
#include "ConstraintLinearLqReif.hpp"

#include "PropagatorLinearLqReif.hpp"


namespace Core {

	ConstraintLinearLqReif::ConstraintLinearLqReif(const ConstraintParameterSPtr& aCoeff,
																								 const ConstraintParameterSPtr& aDomVar,
																				         const ConstraintParameterSPtr& aConst,
		                                             const ConstraintParameterSPtr& aReif)
		: ConstraintLinearReif(ConstraintId::CON_ID_LIN_LQ_REIF, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, aCoeff, aDomVar, aConst, aReif)
	{

		// Register propagator linear lq
		auto propagatorLinLqR = std::make_shared<PropagatorLinearLqReif>(
			getCoeffArray(),
			getDomainArray(),
			getConstant(),
			getReifDomain());

		registerPropagator(propagatorLinLqR);
	}

	ConstraintLinearLqHalfReifLeft::ConstraintLinearLqHalfReifLeft(const ConstraintParameterSPtr& aCoeff,
		                                                             const ConstraintParameterSPtr& aDomVar,
		                                                             const ConstraintParameterSPtr& aConst,
		                                                             const ConstraintParameterSPtr& aReif)
		: ConstraintLinearReif(ConstraintId::CON_ID_LIN_LQ_HREIF_L, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, aCoeff, aDomVar, aConst, aReif)
	{

		// Register propagator linear lq
		auto propagatorLinLqRHL = std::make_shared<PropagatorHLinLqReifL>(
			getCoeffArray(),
			getDomainArray(),
			getConstant(),
			getReifDomain());

		registerPropagator(propagatorLinLqRHL);
	}

	ConstraintLinearLqHalfReifRight::ConstraintLinearLqHalfReifRight(const ConstraintParameterSPtr& aCoeff,
		                                                               const ConstraintParameterSPtr& aDomVar,
		                                                               const ConstraintParameterSPtr& aConst,
		                                                               const ConstraintParameterSPtr& aReif)
		: ConstraintLinearReif(ConstraintId::CON_ID_LIN_LQ_HREIF_R, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, aCoeff, aDomVar, aConst, aReif)
	{

		// Register propagator linear Lq
		auto propagatorLinLqRHR = std::make_shared<PropagatorHLinLqReifR>(
			getCoeffArray(),
			getDomainArray(),
			getConstant(),
			getReifDomain());

		registerPropagator(propagatorLinLqRHR);
	}

}// end namespace Core
