// Self first
#include "ConstraintLinearEqReif.hpp"

#include "PropagatorLinearEqReif.hpp"


namespace Core {

	ConstraintLinearEqReif::ConstraintLinearEqReif(const ConstraintParameterSPtr& aCoeff,
																								 const ConstraintParameterSPtr& aDomVar,
																				         const ConstraintParameterSPtr& aConst,
		                                             const ConstraintParameterSPtr& aReif)
		: ConstraintLinearReif(ConstraintId::CON_ID_LIN_EQ_REIF, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, aCoeff, aDomVar, aConst, aReif)
	{

		// Register propagator linear eq
		auto propagatorLinEqR = std::make_shared<PropagatorLinearEqReif>(
			getCoeffArray(),
			getDomainArray(),
			getConstant(),
			getReifDomain());

		registerPropagator(propagatorLinEqR);
	}

	ConstraintLinearEqHalfReifLeft::ConstraintLinearEqHalfReifLeft(const ConstraintParameterSPtr& aCoeff,
		                                                             const ConstraintParameterSPtr& aDomVar,
		                                                             const ConstraintParameterSPtr& aConst,
		                                                             const ConstraintParameterSPtr& aReif)
		: ConstraintLinearReif(ConstraintId::CON_ID_LIN_EQ_HREIF_L, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, aCoeff, aDomVar, aConst, aReif)
	{

		// Register propagator linear eq
		auto propagatorLinEqRHL = std::make_shared<PropagatorHLinEqReifL>(
			getCoeffArray(),
			getDomainArray(),
			getConstant(),
			getReifDomain());

		registerPropagator(propagatorLinEqRHL);
	}

	ConstraintLinearEqHalfReifRight::ConstraintLinearEqHalfReifRight(const ConstraintParameterSPtr& aCoeff,
		                                                               const ConstraintParameterSPtr& aDomVar,
		                                                               const ConstraintParameterSPtr& aConst,
		                                                               const ConstraintParameterSPtr& aReif)
		: ConstraintLinearReif(ConstraintId::CON_ID_LIN_EQ_HREIF_R, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, aCoeff, aDomVar, aConst, aReif)
	{

		// Register propagator linear eq
		auto propagatorLinEqRHR = std::make_shared<PropagatorHLinEqReifR>(
			getCoeffArray(),
			getDomainArray(),
			getConstant(),
			getReifDomain());

		registerPropagator(propagatorLinEqRHR);
	}

}// end namespace Core
