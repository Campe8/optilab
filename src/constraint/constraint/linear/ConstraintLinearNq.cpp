// Self first
#include "ConstraintLinearNq.hpp"

#include "PropagatorLinearNq.hpp"

namespace Core {

	ConstraintLinearNq::ConstraintLinearNq(const ConstraintParameterSPtr& aCoeff,
																				 const ConstraintParameterSPtr& aDomVar,
																				 const ConstraintParameterSPtr& aConst)
		: ConstraintLinear(ConstraintId::CON_ID_LIN_NQ,
                       PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND,
                       aCoeff, aDomVar, aConst)
	{

		// Register propagator linear nq
		auto propagatorLinNq = std::make_shared<PropagatorLinearNq>(
			getCoeffArray(),
			getDomainArray(),
			getConstant());

		registerPropagator(propagatorLinNq);
	}

}// end namespace Core
