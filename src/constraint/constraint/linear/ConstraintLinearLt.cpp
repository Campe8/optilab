// Self first
#include "ConstraintLinearLt.hpp"

#include "PropagatorLinearLt.hpp"

namespace Core {

	ConstraintLinearLt::ConstraintLinearLt(const ConstraintParameterSPtr& aCoeff,
																				 const ConstraintParameterSPtr& aDomVar,
																				 const ConstraintParameterSPtr& aConst)
		: ConstraintLinear(ConstraintId::CON_ID_LIN_LT, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, aCoeff, aDomVar, aConst)
	{

		// Register propagator linear lt
		auto propagatorLinLt = std::make_shared<PropagatorLinearLt>(
			getCoeffArray(),
			getDomainArray(),
			getConstant());

		registerPropagator(propagatorLinLt);
	}

}// end namespace Core
