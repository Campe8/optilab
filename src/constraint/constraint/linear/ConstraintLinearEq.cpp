// Self first
#include "ConstraintLinearEq.hpp"

// Variable
#include "Variable.hpp"

#include "PropagatorLinearEq.hpp"

#include "ConstraintParameterVariable.hpp"
#include "ConstraintParameterDomain.hpp"

#include <utility>
#include <memory>
#include <cassert>

namespace Core {

	ConstraintLinearEq::ConstraintLinearEq(const ConstraintParameterSPtr& aCoeff,
																				 const ConstraintParameterSPtr& aDomVar,
																				 const ConstraintParameterSPtr& aConst)
		: ConstraintLinear(ConstraintId::CON_ID_LIN_EQ, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, aCoeff, aDomVar, aConst)
	{

		// Register propagator linear eq
		auto propagatorLinEq = std::make_shared<PropagatorLinearEq>(
			getCoeffArray(),
			getDomainArray(),
			getConstant());

		registerPropagator(propagatorLinEq);
	}

}// end namespace Core
