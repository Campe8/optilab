// Self first
#include "ConstraintLinearLq.hpp"

#include "PropagatorLinearLq.hpp"

namespace Core {

	ConstraintLinearLq::ConstraintLinearLq(const ConstraintParameterSPtr& aCoeff,
																				 const ConstraintParameterSPtr& aDomVar,
																				 const ConstraintParameterSPtr& aConst)
		: ConstraintLinear(ConstraintId::CON_ID_LIN_LQ, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, aCoeff, aDomVar, aConst)
	{

		// Register propagator linear lq
		auto propagatorLinLq = std::make_shared<PropagatorLinearLq>(
			getCoeffArray(),
			getDomainArray(),
			getConstant());

		registerPropagator(propagatorLinLq);
	}

}// end namespace Core
