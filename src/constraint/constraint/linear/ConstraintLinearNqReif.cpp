// Self first
#include "ConstraintLinearNqReif.hpp"

#include "PropagatorLinearNqReif.hpp"


namespace Core {

	ConstraintLinearNqReif::ConstraintLinearNqReif(const ConstraintParameterSPtr& aCoeff,
																								 const ConstraintParameterSPtr& aDomVar,
																				         const ConstraintParameterSPtr& aConst,
		                                             const ConstraintParameterSPtr& aReif)
		: ConstraintLinearReif(ConstraintId::CON_ID_LIN_NQ_REIF, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, aCoeff, aDomVar, aConst, aReif)
	{

		// Register propagator linear nq
		auto propagatorLinNqR = std::make_shared<PropagatorLinearNqReif>(
			getCoeffArray(),
			getDomainArray(),
			getConstant(),
			getReifDomain());

		registerPropagator(propagatorLinNqR);
	}

	ConstraintLinearNqHalfReifLeft::ConstraintLinearNqHalfReifLeft(const ConstraintParameterSPtr& aCoeff,
		                                                             const ConstraintParameterSPtr& aDomVar,
		                                                             const ConstraintParameterSPtr& aConst,
		                                                             const ConstraintParameterSPtr& aReif)
		: ConstraintLinearReif(ConstraintId::CON_ID_LIN_NQ_HREIF_L, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, aCoeff, aDomVar, aConst, aReif)
	{

		// Register propagator linear nq
		auto propagatorLinNqRHL = std::make_shared<PropagatorHLinNqReifL>(
			getCoeffArray(),
			getDomainArray(),
			getConstant(),
			getReifDomain());

		registerPropagator(propagatorLinNqRHL);
	}

	ConstraintLinearNqHalfReifRight::ConstraintLinearNqHalfReifRight(const ConstraintParameterSPtr& aCoeff,
		                                                               const ConstraintParameterSPtr& aDomVar,
		                                                               const ConstraintParameterSPtr& aConst,
		                                                               const ConstraintParameterSPtr& aReif)
		: ConstraintLinearReif(ConstraintId::CON_ID_LIN_NQ_HREIF_R, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, aCoeff, aDomVar, aConst, aReif)
	{

		// Register propagator linear nq
		auto propagatorLinNqRHR = std::make_shared<PropagatorHLinNqReifR>(
			getCoeffArray(),
			getDomainArray(),
			getConstant(),
			getReifDomain());

		registerPropagator(propagatorLinNqRHR);
	}

}// end namespace Core
