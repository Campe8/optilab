// Base class
#include "ConstraintPlus.hpp"

#include "PropagatorPlus.hpp"

namespace Core {
  
    ConstraintPlus::ConstraintPlus(const ConstraintParameterSPtr& aParam1,
                                   const ConstraintParameterSPtr& aParam2,
                                   const ConstraintParameterSPtr& aParam3,
                                   PropagatorStrategyType aStrategyType)
  : Core::ConstraintTernary(ConstraintId::CON_ID_PLUS, aStrategyType, aParam1, aParam2, aParam3)
  {
    assert(VERIFY_PRIMITIVE_TYPE(aParam1.get()));
    assert(VERIFY_PRIMITIVE_TYPE(aParam2.get()));
    assert(VERIFY_PRIMITIVE_TYPE(aParam3.get()));
    
    // Register propagator plus
    PropagatorSPtr propagatorPlus{ nullptr };
    if(aStrategyType == PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND)
    {
        propagatorPlus = std::make_shared<PropagatorPlusBound>(getParameterDomain(P0).get(),
                                                               getParameterDomain(P1).get(),
                                                               getParameterDomain(P2).get());
    }
    else
    {
      assert(aStrategyType == PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN);
      propagatorPlus = std::make_shared<PropagatorPlusDomain>(getParameterDomain(P0).get(),
                                                              getParameterDomain(P1).get(),
                                                              getParameterDomain(P2).get());
    }
    
    registerPropagator(propagatorPlus);
  }
  
}// end namespace Core
