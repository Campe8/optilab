// Base class
#include "ConstraintTimes.hpp"
#include "PropagatorTimes.hpp"

#include "ConstraintParameterDomain.hpp"
#include "ConstraintParameterVariable.hpp"

namespace Core {
  
    ConstraintTimes::ConstraintTimes(const ConstraintParameterSPtr& aParam1,
                                     const ConstraintParameterSPtr& aParam2,
                                     const ConstraintParameterSPtr& aParam3,
                                     PropagatorStrategyType aStrategyType)
  : Core::ConstraintTernary(ConstraintId::CON_ID_TIMES, aStrategyType, aParam1, aParam2, aParam3)
  {
    assert(VERIFY_PRIMITIVE_TYPE(aParam1.get()));
    assert(VERIFY_PRIMITIVE_TYPE(aParam2.get()));
    assert(VERIFY_PRIMITIVE_TYPE(aParam3.get()));
    
    // Register propagator eq
    PropagatorSPtr propagatorTimes{ nullptr };
    if(aStrategyType == PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND)
    {
        propagatorTimes = std::make_shared<PropagatorTimesBound>(getParameterDomain(P0).get(),
                                                                 getParameterDomain(P1).get(),
                                                                 getParameterDomain(P2).get());
    }
    else
    {
      assert(aStrategyType == PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN);
      propagatorTimes = std::make_shared<PropagatorTimesDomain>(getParameterDomain(P0).get(),
                                                                getParameterDomain(P1).get(),
                                                                getParameterDomain(P2).get());
    }
    
    registerPropagator(propagatorTimes);
  }

}// end namespace Core
