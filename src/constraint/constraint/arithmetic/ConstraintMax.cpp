// Base class
#include "ConstraintMax.hpp"
#include "PropagatorMax.hpp"

namespace Core {
  
    ConstraintMax::ConstraintMax(const ConstraintParameterSPtr& aParam1,
                                 const ConstraintParameterSPtr& aParam2,
                                 const ConstraintParameterSPtr& aParam3,
                                 PropagatorStrategyType aStrategyType)
  : Core::ConstraintTernary(ConstraintId::CON_ID_MAX, aStrategyType, aParam1, aParam2, aParam3)
  {
    assert(VERIFY_PRIMITIVE_TYPE(aParam1.get()));
    assert(VERIFY_PRIMITIVE_TYPE(aParam2.get()));
    assert(VERIFY_PRIMITIVE_TYPE(aParam3.get()));
    
    // Register propagator max
    PropagatorSPtr propagatorMax{ nullptr };
    if(aStrategyType == PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND)
    {
        propagatorMax = std::make_shared<PropagatorMaxBound>(getParameterDomain(P0).get(),
                                                             getParameterDomain(P1).get(),
                                                             getParameterDomain(P2).get());
    }
    else
    {
      assert(aStrategyType == PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN);
      propagatorMax = std::make_shared<PropagatorMaxDomain>(getParameterDomain(P0).get(),
                                                            getParameterDomain(P1).get(),
                                                            getParameterDomain(P2).get());
    }
    
    registerPropagator(propagatorMax);
  }

}// end namespace Core
