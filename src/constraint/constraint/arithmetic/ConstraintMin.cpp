// Base class
#include "ConstraintMin.hpp"
#include "PropagatorMin.hpp"

namespace Core {
  
    ConstraintMin::ConstraintMin(const ConstraintParameterSPtr& aParam1,
                                 const ConstraintParameterSPtr& aParam2,
                                 const ConstraintParameterSPtr& aParam3,
                                 PropagatorStrategyType aStrategyType)
  : Core::ConstraintTernary(ConstraintId::CON_ID_MAX, aStrategyType, aParam1, aParam2, aParam3)
  {
    assert(VERIFY_PRIMITIVE_TYPE(aParam1.get()));
    assert(VERIFY_PRIMITIVE_TYPE(aParam2.get()));
    assert(VERIFY_PRIMITIVE_TYPE(aParam3.get()));
    
    // Register propagator min
    PropagatorSPtr propagatorMin{ nullptr };
    if(aStrategyType == PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND)
    {
        propagatorMin = std::make_shared<PropagatorMinBound>(getParameterDomain(P0).get(),
                                                             getParameterDomain(P1).get(),
                                                             getParameterDomain(P2).get());
    }
    else
    {
      assert(aStrategyType == PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN);
      propagatorMin = std::make_shared<PropagatorMinDomain>(getParameterDomain(P0).get(),
                                                            getParameterDomain(P1).get(),
                                                            getParameterDomain(P2).get());
    }
    
    registerPropagator(propagatorMin);
  }

}// end namespace Core
