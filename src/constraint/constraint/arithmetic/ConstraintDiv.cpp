// Base class
#include "ConstraintDiv.hpp"
#include "PropagatorDiv.hpp"

namespace Core {
  
    ConstraintDiv::ConstraintDiv(const ConstraintParameterSPtr& aParam1,
                                 const ConstraintParameterSPtr& aParam2,
                                 const ConstraintParameterSPtr& aParam3)
  : Core::ConstraintTernary(ConstraintId::CON_ID_MAX, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, aParam1, aParam2, aParam3)
  {
    assert(VERIFY_PRIMITIVE_TYPE(aParam1.get()));
    assert(VERIFY_PRIMITIVE_TYPE(aParam2.get()));
    assert(VERIFY_PRIMITIVE_TYPE(aParam3.get()));
    
    // Register propagator div
    registerPropagator(std::make_shared<PropagatorDiv>(getParameterDomain(P0).get(),
                                                       getParameterDomain(P1).get(),
                                                       getParameterDomain(P2).get()));
  }

}// end namespace Core
