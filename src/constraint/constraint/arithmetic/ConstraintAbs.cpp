// Base class
#include "ConstraintAbs.hpp"

#include "PropagatorAbs.hpp"

namespace Core {
  
  ConstraintAbs::ConstraintAbs(const ConstraintParameterSPtr& aParam1, const ConstraintParameterSPtr& aParam2,
                               PropagatorStrategyType aStrategyType)
  : ConstraintBinary(ConstraintId::CON_ID_ABS,
                     aStrategyType,
                     aParam1, aParam2)
  {
    assert(VERIFY_PRIMITIVE_TYPE(aParam1.get()));
    assert(VERIFY_PRIMITIVE_TYPE(aParam2.get()));
    
    PropagatorSPtr propagatorAbs{ nullptr };
    if(aStrategyType == PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND)
    {
      propagatorAbs = std::make_shared<PropagatorAbsBound>(getParameterDomain(P0).get(),
                                                           getParameterDomain(P1).get());
    }
    else
    {
      assert(aStrategyType == PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN);
      propagatorAbs = std::make_shared<PropagatorAbsDomain>(getParameterDomain(P0).get(),
                                                            getParameterDomain(P1).get());
    }
    
    registerPropagator(propagatorAbs);
  }
  
}// end namespace Core
