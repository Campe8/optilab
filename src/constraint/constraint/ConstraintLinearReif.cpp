// Self first
#include "ConstraintLinearReif.hpp"

#include "ConstraintParameterVariable.hpp"
#include "ConstraintParameterDomain.hpp"
#include "ConstraintParameterDomainArray.hpp"

#include <cassert>

namespace Core {

	ConstraintLinearReif::ConstraintLinearReif(ConstraintId aConstraintId,
																		         PropagatorStrategyType aStrategyType,
																		         const ConstraintParameterSPtr& aCoeff,
																		         const ConstraintParameterSPtr& aDomVar,
																		         const ConstraintParameterSPtr& aConst,
																		         const ConstraintParameterSPtr& aReif)
		: ConstraintLinear(aConstraintId, aStrategyType, aCoeff, aDomVar, aConst)
	{
		assert(VERIFY_PRIMITIVE_TYPE(aReif.get()));
		assert(ConstraintParameterDomain::isa(aReif.get()));
		assert(DomainBoolean::isa((ConstraintParameterDomain::cast(aReif.get())->getDomain()).get()));

		registerConstraintParameter(aReif);

		// Initialize internal reification domain
		pReifDomain = getReifDomain(aReif);
	}

	std::shared_ptr<DomainBoolean> ConstraintLinearReif::getReifDomain(const ConstraintParameterSPtr& aReif)
	{
		assert(aReif);
		return std::static_pointer_cast<DomainBoolean>(ConstraintParameterDomain::cast(aReif.get())->getDomain());
	}//getCoeffArray

}// end namespace Core
