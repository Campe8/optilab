// Self first
#include "ConstraintLinear.hpp"
#include "ConstraintParameterVariable.hpp"
#include "ConstraintParameterDomain.hpp"
#include "ConstraintParameterDomainArray.hpp"

#include <cassert>

namespace Core {

	ConstraintLinear::ConstraintLinear(ConstraintId aConstraintId,
																		 PropagatorStrategyType aStrategyType,
																		 const ConstraintParameterSPtr& aCoeff,
																		 const ConstraintParameterSPtr& aDomVar,
																		 const ConstraintParameterSPtr& aConst)
		: Constraint(aConstraintId, aStrategyType)
	{
		assert(VERIFY_ARRAY_TYPE(aCoeff.get()));
		assert(VERIFY_ARRAY_TYPE(aDomVar.get()));
		assert(VERIFY_PRIMITIVE_TYPE(aConst.get()));

		assert(aCoeff->getParameterClass() == aDomVar->getParameterClass());
		assert(ConstraintParameterDomainArray::cast(aCoeff.get())->getDomainArray()->size() ==
           ConstraintParameterDomainArray::cast(aDomVar.get())->getDomainArray()->size());
		pLinearSize = ConstraintParameterDomainArray::cast(aCoeff.get())->getDomainArray()->size();

		registerConstraintParameter(aCoeff);
		registerConstraintParameter(aDomVar);
		registerConstraintParameter(aConst);

		// Initialize internal arrays and constant
		pCoeffArray = getCoeffArray(aCoeff);
		pDomArray = getDomainArray(aDomVar);
		pConstant = getConstant(aConst);
	}

	PropagationCost ConstraintLinear::reducedCost(const PropagationCost& aPropagationCost)
	{
		if (pLinearSize <= 1)
		{
			static PropagationCost unaryCost(PropagationPriority::PROP_PRIORITY_UNARY);
			return unaryCost;
		}
		else if (pLinearSize == 2)
		{
			static PropagationCost binaryCost(PropagationPriority::PROP_PRIORITY_BINARY);
			return binaryCost;
		}
		else if (pLinearSize == 3)
		{
			static PropagationCost binaryCost(PropagationPriority::PROP_PRIORITY_TERNARY);
			return binaryCost;
		}

		return aPropagationCost;
	}//reducedCost

	DomainElementArraySPtr ConstraintLinear::getCoeffArray(const ConstraintParameterSPtr& aCoeff)
	{
		assert(aCoeff && ConstraintParameterDomainArray::isa(aCoeff.get()));
		auto parameterDomainArray = ConstraintParameterDomainArray::cast(aCoeff.get());
    
    // Fill domain array with domain elements
    assert(parameterDomainArray->getDomainArray());
    auto domainArray = *(parameterDomainArray->getDomainArray());
    
    // Create a domain element array and assign the value of the (singleton) domains
		auto  elementArray = new DomainElementArray(domainArray.size());
		for (std::size_t idx{ 0 }; idx < domainArray.size(); ++idx)
		{
      // The domain array should contain only singleton domains
			assert(domainArray[idx]->getSize() == 1);
			elementArray->assignElementToCell(idx, domainArray[idx]->lowerBound());
    }
    
    // Return the (shared) pointer to the domain element array
		return DomainElementArraySPtr(elementArray);
	}//getCoeffArray

	DomainArraySPtr ConstraintLinear::getDomainArray(const ConstraintParameterSPtr& aDom)
	{
		assert(aDom);
		assert(ConstraintParameterDomainArray::isa(aDom.get()));
		auto pDEArray = ConstraintParameterDomainArray::cast(aDom.get());

		DomainArraySPtr dArray = std::make_shared<DomainArray>(pDEArray->getDomainArray()->size());
		for (std::size_t idx{ 0 }; idx < dArray->size(); ++idx)
		{
			(*dArray).assignElementToCell(idx, (*(pDEArray->getDomainArray()))[idx].get());
		}

		return dArray;
	}//getDomainArray

	DomainElement* ConstraintLinear::getConstant(const ConstraintParameterSPtr& aConst)
	{
		assert(aConst);
		assert(ConstraintParameterDomain::isa(aConst.get()));
		auto pDom = ConstraintParameterDomain::cast(aConst.get());

		assert(pDom->getDomain()->getSize() == 1);
		return pDom->getDomain()->lowerBound();
	}//getConstant

}// end namespace Core
