// Base class
#include "Constraint.hpp"
#include "ConstraintDomain.hpp"

// Variable
#include "Variable.hpp"
#include "VariableArray.hpp"

#include "Propagator.hpp"
#include "PropagatorDomain.hpp"

#include "ConstraintParameterVariable.hpp"
#include "ConstraintParameterDomain.hpp"
#include "ConstraintParameterDomainArray.hpp"

#include "PropagatorUtils.hpp"

#include <utility>
#include <stdexcept>
#include <memory>
#include <cassert>

namespace Core {
  
  ConstraintDomain::ConstraintDomain(const ConstraintParameterSPtr& aDomainArray,
                                     const ConstraintParameterSPtr& aLowerBound,
                                     const ConstraintParameterSPtr& aUpperBound)
  : Constraint(ConstraintId::CON_ID_DOMAIN, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND)
  {
    assert(VERIFY_ARRAY_TYPE(aDomainArray.get()));
    registerConstraintParameter(aDomainArray);
    
    auto domList = ConstraintParameterDomainArray::cast(aDomainArray.get());
    auto domSize = domList->getDomainArray()->size();
    
    pDomArray = std::make_shared<DomainArray>(domSize);
    for (std::size_t idx{ 0 }; idx < domSize; ++idx)
    {
      (*pDomArray).assignElementToCell(idx, (*(domList->getDomainArray()))[idx].get());
    }
    
		auto pDom = ConstraintParameterDomain::cast(aLowerBound.get());
		pLB = pDom->getDomain()->lowerBound();
    pDom = ConstraintParameterDomain::cast(aUpperBound.get());
		pUB = pDom->getDomain()->lowerBound();
    
    registerPropagator(std::make_shared<PropagatorDomain>(*(pDomArray.get()), pLB, pUB));
  }
  
}// end namespace Core
