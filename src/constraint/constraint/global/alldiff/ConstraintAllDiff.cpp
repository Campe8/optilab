// Self first
#include "ConstraintAllDiff.hpp"

#include "ConstraintParameterDomainArray.hpp"
#include "PropagatorNq.hpp"
#include "PropagatorAllDiff.hpp"

namespace Core {
  
  ConstraintAllDiff::ConstraintAllDiff(const ConstraintParameterSPtr& aDomainList)
  : Constraint(ConstraintId::CON_ID_ALLDIFF, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND)
  {
    assert(VERIFY_ARRAY_TYPE(aDomainList.get()));
    registerConstraintParameter(aDomainList);
    
    auto domList = ConstraintParameterDomainArray::cast(aDomainList.get());
    auto domSize = domList->getDomainArray()->size();
    
    pDomArray = std::make_shared<DomainArray>(domSize);
    for (std::size_t idx{ 0 }; idx < domSize; ++idx)
    {
      (*pDomArray).assignElementToCell(idx, (*(domList->getDomainArray()))[idx].get());
    }
    
    // If the size of the list is 2, use the more efficient constraint nq
    if(domSize == 2)
    {
      registerPropagator(std::make_shared<PropagatorNq>((*pDomArray)[0], (*pDomArray)[1]));
    }
    else
    {
      registerPropagator(std::make_shared<PropagatorAllDiff>(*(pDomArray.get())));
    }
  }
  
}// end namespace Core
