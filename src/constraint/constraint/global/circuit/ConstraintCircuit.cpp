// Self first
#include "ConstraintCircuit.hpp"

#include "ConstraintParameterDomainArray.hpp"
#include "DomainFactory.hpp"
#include "PropagatorAllDiff.hpp"
#include "PropagatorElement.hpp"

namespace Core {
  
  ConstraintCircuit::ConstraintCircuit(const ConstraintParameterSPtr& aDomainList)
  : Constraint(ConstraintId::CON_ID_CIRCUIT, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND)
  {
    assert(VERIFY_ARRAY_TYPE(aDomainList.get()));
    registerConstraintParameter(aDomainList);
    
    auto domList = ConstraintParameterDomainArray::cast(aDomainList.get());
    auto domSize = domList->getDomainArray()->size();
    
    pDomArray = std::make_shared<DomainArray>(domSize);
    for (std::size_t idx{ 0 }; idx < domSize; ++idx)
    {
      (*pDomArray).assignElementToCell(idx, (*(domList->getDomainArray()))[idx].get());
    }
    
    succDomArray = std::make_shared<DomainArray>(domSize-1);
    DomainFactoryPtr domFact = new DomainFactory();
    for (std::size_t idx{ 0 }; idx < domSize-1; ++idx)
    {
      DomainIntegerPtr dom = domFact->domainInteger(2, domSize);
      (*succDomArray).assignElementToCell(idx, dom);
    }
    
    registerPropagator(std::make_shared<PropagatorAllDiff>(*(pDomArray.get())));
    //registerPropagator(std::make_shared<PropagatorAllDiff>(*(succDomArray.get())));
    
    DomainIntegerPtr dom1 = domFact->domainInteger(1);
    
    registerPropagator(std::make_shared<PropagatorElement>(dom1, *(pDomArray.get()), (*succDomArray)[0]));
    //registerPropagator(std::make_shared<PropagatorElement>((*succDomArray)[0], *(pDomArray.get()), (*succDomArray)[1]));
    std::size_t idx{ 0 };
    for(idx = 0; idx < domSize-2; ++idx)
    {
      //registerPropagator(std::make_shared<PropagatorElement>((*succDomArray)[idx], *(pDomArray.get()), (*succDomArray)[idx+1]));
    }
    //registerPropagator(std::make_shared<PropagatorElement>((*succDomArray)[0], *(pDomArray.get()), (*succDomArray)[1]));
  }
  
}// end namespace Core
