// Base class
#include "ConstraintElement.hpp"

// Variable
#include "Variable.hpp"
#include "VariableArray.hpp"

#include "PropagatorElement.hpp"
#include "DomainElementManager.hpp"
#include "DomainElementInt64.hpp"

#include "ConstraintParameterVariable.hpp"
#include "ConstraintParameterDomain.hpp"
#include "ConstraintParameterDomainArray.hpp"

#include "PropagatorUtils.hpp"

#include <utility>
#include <stdexcept>
#include <memory>
#include <cassert>

namespace Core {
  
  Domain* getDomainFromParameter(const ConstraintParameterSPtr& aParam)
  {
    if(ConstraintParameterVariable::isa(aParam.get()))
    {
      VariableSPtr varIdx = ConstraintParameterVariable::cast(aParam.get())->getVariable();
      assert(varIdx->hasPrimitiveType() && varIdx->getType()->getTypeClass() == VariableTypeClass::VAR_TYPE_INTEGER);
      
      return (varIdx->domain()).get();
    }
    else
    {
      assert(ConstraintParameterDomain::isa(aParam.get()));
      return (ConstraintParameterDomain::cast(aParam.get())->getDomain()).get();
    }
  }//getDomainFromParameter
  
  static DomainSPtrArraySPtr getDomainArrayFromParameter(const ConstraintParameterSPtr& aParam)
  {
    DomainSPtrArraySPtr darray{nullptr};
    if(ConstraintParameterVariable::isa(aParam.get()))
    {
      auto varArray = VariableArray::cast((ConstraintParameterVariable::cast(aParam.get())->getVariable()).get());
      
      // Check variable types and fill domain array
      darray = std::make_shared<DomainSPtrArray>(varArray->size());
      
      auto typeClass = varArray->operator[](0)->getType()->getTypeClass();
      for (std::size_t idx{0}; idx < varArray->size(); ++idx)
      {
        assert(varArray->operator[](idx)->hasPrimitiveType() &&
               (typeClass == varArray->operator[](idx)->getType()->getTypeClass()));
        darray->assignElementToCell(idx, varArray->operator[](idx)->domain());
      }
    }
    else
    {
      assert(ConstraintParameterDomainArray::isa(aParam.get()));
      darray = ConstraintParameterDomainArray::cast(aParam.get())->getDomainArray();
      
      // Check domain element types
      auto typeClass = darray->operator[](0)->getDomainType();
      for (std::size_t idx{1}; idx < darray->size(); ++idx)
      {
        assert(typeClass == darray->operator[](idx)->getDomainType());
      }
    }
    
    // Return the domain array
    return darray;
  }//getDomainArrayFromParameter
  
  ConstraintElement::ConstraintElement(const ConstraintParameterSPtr& aParamIdx,
                                       const ConstraintParameterSPtr& aParamArray,
                                       const ConstraintParameterSPtr& aParamAssign)
    : Constraint(ConstraintId::CON_ID_ELEMENT, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN)
  {
    registerConstraintParameter(aParamIdx);
    registerConstraintParameter(aParamArray);
    registerConstraintParameter(aParamAssign);
    
    // Get the domain array of the element constraint
    pDomainArray = getDomainArrayFromParameter(aParamArray);
    assert(pDomainArray && pDomainArray->size() > 0);
    
    // Register the propagator for element constraint
    registerPropagatorElement();
  }
  
  ConstraintElement::~ConstraintElement()
  {
  }
  
  bool ConstraintElement::isArraySingleton() const
  {
    assert(pDomainArray);
    for(std::size_t idx = 0; idx < pDomainArray->size(); ++idx)
    {
      if(!PropagatorUtils::isDomainSingleton(pDomainArray->operator[](idx).get()))
      {
        return false;
      }
    }
    return true;
  }// isArraySingleton
  
  void ConstraintElement::registerPropagatorElement()
  {
    assert(pDomainArray);
    
    // Get domains for index and assignment.
    // @note the assignment domain is the "last domain on the right",
    // i.e., it is the last registered domain
    auto domIdx = getParameterDomain(0).get();
    auto domAssign = getParameterDomain(numConstraintParameters() - 1).get();
    auto dsize = pDomainArray->size();
    if(isArraySingleton())
    {
      DomainElementArray domElemArray(dsize);
      for(std::size_t idx{0}; idx < dsize; ++idx)
      {
        domElemArray.assignElementToCell(idx, PropagatorUtils::getSingleton(pDomainArray->operator[](idx).get()));
      }
      registerPropagator(std::make_shared<PropagatorElement>(domIdx, domElemArray, domAssign));
    }
    else
    {
      // Create array of raw pointers for propagator
      DomainArray domainArray(dsize);
      for(std::size_t idx{0}; idx < dsize; ++idx)
      {
        domainArray.assignElementToCell(idx, pDomainArray->operator[](idx).get());
      }
      registerPropagator(std::make_shared<PropagatorElement>(domIdx, domainArray, domAssign));
    }
  }// registerPropagatorElement
  
}// end namespace Core
