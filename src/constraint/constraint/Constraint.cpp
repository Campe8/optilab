// Base class
#include "Constraint.hpp"

#include "ConstraintParameterVariable.hpp"
#include "ConstraintParameterDomain.hpp"
#include "ConstraintParameterDomainArray.hpp"
#include "ConstraintParameterDomainMatrix2D.hpp"

// Variable
#include "Variable.hpp"

// Events
#include "DomainEventInc.hpp"

// Constraint store
#include "ConstraintStore.hpp"

#include <utility>
#include <cassert>

namespace Core {
  
  const Fitness Constraint::FitnessUndef = Propagator::getUnspecFitness();
  const Fitness Constraint::FitnessMin = Propagator::getMinFitness();
  const Fitness Constraint::FitnessInvalid = Propagator::getInvalidFitness();
  
  Constraint::Constraint(ConstraintId aConstraintId,
                         PropagatorStrategyType aStrategyType)
  : CoreObject(CoreObjectClass::CORE_OBJECT_CONSTRAINT)
  , Semantics::ConstraintObserver(getUUID())
  , pConstraintId(aConstraintId)
  , pStrategyType(aStrategyType)
  , pOptimizationConstraint(false)
  , pConstraintStore(nullptr)
  , pConstraintObserverEvent(new ConstraintObserverEvent(this))
  {
  }
  
  Constraint::~Constraint()
  {
    // Deregister from constraint store
    unsubscribeFromConstraintStore();
    
    // detach from constraint subjects
    for(auto& dom : pRegisteredDomains)
    {
      if(dom.second)
      {
        dom.first->detachConstraintObserver(this);
      }
    }
  }
  
  void Constraint::attachToSubjects()
  {
    // detach from constraint subjects
    for (auto& dom : pRegisteredDomains)
    {
      if (dom.second)
      {
        dom.first->attachConstraintObserver(this);
      }
    }
  }// attachToSubjects
  
  void Constraint::detachFromSubjects()
  {
    // detach from constraint subjects
    for (auto& dom : pRegisteredDomains)
    {
      if (dom.second)
      {
        dom.first->detachConstraintObserver(this);
      }
    }
  }// detachFromSubjects
  
  bool Constraint::isScopeGround() const
  {
    for(auto& it : pScope)
    {
      if(!(it.second->isAssigned())) return false;
    }
    return true;
  }//isScopeGround
  
  void Constraint::subscribeToConstraintStore(const Semantics::ConstraintStoreSPtr& aConstraintStore)
  {
    assert(aConstraintStore);
    
    // Unsubscribe for current constraint store if any
    unsubscribeFromConstraintStore();
    
    pConstraintStore = aConstraintStore;
    pConstraintStore->registerConstraint(this);
  }//subscribeToConstraintStore
  
  void Constraint::removeStoredConstraintFromConstraintStore()
  {
    if (pConstraintStore)
    {
      pConstraintStore->removeStoredConstraint(this);
    }
  }//removeStoredConstraintFromConstraintStore
  
  void Constraint::unsubscribeFromConstraintStore()
  {
    // Deregister from constraint store
    if (pConstraintStore)
    {
      pConstraintStore->deregisterConstraint(this);
    }
    pConstraintStore = nullptr;
  }//unsubscribeFromConstraintStore
  
  void Constraint::forceReevaluation()
  {
    if (pConstraintStore)
    {
      pConstraintStore->reevaluateConstraint(this);
    }
  }//forceReevaluation
  
  void Constraint::registerParameterVariable(ConstraintParameterVariable* aPVar)
  {
    assert(aPVar);
    
    // Register variable as part of the scope of this constraint
    registerScopeVariable(aPVar->getVariable());
    
    // Register corresponding domain
    for (auto& domain : *(aPVar->getVariable()->domainList()))
    {
      pRegisteredDomains.push_back(std::make_pair(domain, aPVar->isSubject()));
      if (pRegisteredDomains.back().second)
      {
        pRegisteredDomains.back().first->attachConstraintObserver(this);
      }
    }
  }//registerParameterVariable
  
  void Constraint::registerParameterDomain(ConstraintParameterDomain* aPDom)
  {
    assert(aPDom);
    pRegisteredDomains.push_back(std::make_pair(aPDom->getDomain(), aPDom->isSubject()));
    if (pRegisteredDomains.back().second)
    {
      pRegisteredDomains.back().first->attachConstraintObserver(this);
    }
  }//registerParameterDomain
  
  void Constraint::registerParameterArray(ConstraintParameterDomainArray* aPArr)
  {
    assert(aPArr);
    auto dArray = aPArr->getDomainArray();
    
    // Register each domain in the parameter and set the subject
    for (std::size_t idx = 0; idx < dArray->size(); ++idx)
    {
      pRegisteredDomains.push_back(std::make_pair(dArray->operator[](idx), aPArr->isSubject(idx)));
      if (pRegisteredDomains.back().second)
      {
        pRegisteredDomains.back().first->attachConstraintObserver(this);
      }
    }
  }//registerParameterArray
  
  void Constraint::registerConstraintParameter(const ConstraintParameterSPtr& aConstraintParameter)
  {
    assert(aConstraintParameter);
    switch(aConstraintParameter->getParameterClass())
    {
      case ConstraintParameter::ConstraintParameterClass::CON_PARAM_CLASS_VARIABLE:
        registerParameterVariable(ConstraintParameterVariable::cast(aConstraintParameter.get()));
        break;
      case ConstraintParameter::ConstraintParameterClass::CON_PARAM_CLASS_DOMAIN:
        registerParameterDomain(ConstraintParameterDomain::cast(aConstraintParameter.get()));
        break;
      default:
        assert(aConstraintParameter->getParameterClass() ==
               ConstraintParameter::ConstraintParameterClass::CON_PARAM_CLASS_DOMAIN_ARRAY);
        registerParameterArray(ConstraintParameterDomainArray::cast(aConstraintParameter.get()));
        break;
    }// switch
  }//registerConstraintParameter
  
  void Constraint::notifyRegisteredDomainsOnEvent()
  {
    for(auto& domain : pRegisteredDomains)
    {
      if(domain.second)
      {
        domain.first->updateOnConstraintEvent(pConstraintObserverEvent.get());
      }
    }
  }//notifyRegisteredDomainsOnEvent
  
  bool Constraint::isEventConsideredForReevaluation(DomainEvent* aDomainEvent) const
  {
    assert(aDomainEvent);
    for(auto& event : reevaluationEventMap)
    {
      if(*aDomainEvent == *event || aDomainEvent->overlaps(event.get()))
      {
        return true;
      }
    }

    return false;
  }//isEventConsideredForReevaluation
  
  bool Constraint::requiresNotification(const Core::ConstraintObserverEvent* aConstraintObserverEvent)
  {
    assert(aConstraintObserverEvent);
    
    /*
     * Observer, i.e., this constraint, must be notified when
     * one of the following situations happens:
     * 1) propagation status INIT: this event is used, for example,to setup subjects.
     *    In particular, to save the domain event before any changes happen;
     * 2) the event owner is different from this owner: this means that another constraint
     *    triggered the given event and this constraint is somehow affected.
     *    @note the event must be notified even if the given event is SUBSUMED since
     *          the given event regards another constraint and not this constraint;
     * 3) the event owner is the same as this owner AND the event is neither
     *    FIXPOINT nor SUBSUMED.
     */
    
    // Case (1)
    auto propStatus = aConstraintObserverEvent->constraintPropagationStatus();
    if (propStatus == Core::ConstraintObserverEvent::CON_EVT_PROPAGATION_INIT) { return true; }
    
    // Case (2)
    assert(propStatus == Core::ConstraintObserverEvent::CON_EVT_PROPAGATION_TERM);
    if (aConstraintObserverEvent->getEventOwner() != pConstraintObserverEvent->getEventOwner()) { return true; }
    
    // Case (3)
    auto propEvent = aConstraintObserverEvent->constraintPropagationEvent();
    if (propEvent != PropagationEvent::PROP_EVENT_FIXPOINT && propEvent != PropagationEvent::PROP_EVENT_SUBSUMED) { return true; }
    
    return false;
  }//requiresNotification
  
  void Constraint::updateOnDomainEvent(DomainEvent* aDomainEvent)
  {
    // If the given event belongs to the set
    // of triggering events of this constraint,
    // notify the set of ConstraintStores this constraint belongs to
    assert(aDomainEvent);
    
    // Return if no constraint store is registered
    if(!pConstraintStore)
    {
      return;
    }
    
    // Check if aDomainEvent belongs to the set of events
    // that trigger reevaluation of this constraint
    if(isEventConsideredForReevaluation(aDomainEvent))
    {
      pConstraintStore->reevaluateConstraint(this);
    }
  }//updateOnDomainEvent
  
  PropagationCost Constraint::reducedCost(const PropagationCost& aPropagationCost)
  {
    return aPropagationCost;
  }//reducedCost

  PropagationCost Constraint::getCost()
  {
    if(pInternalPropagatorMap.size() == 0)
    {
      return PropagationCost(PropagationPriority::PROP_PRIORITY_UNDEF);
    }
    
    PropagationCost constraintCost(PropagationPriority::PROP_PRIORITY_UNARY);
    for (auto& propList : pInternalPropagatorMap)
    {
      auto propCost = getPropagatorFromInternalMap(propList.first)->getCost();
      if(constraintCost < propCost)
      {
        constraintCost = propCost;
      }
    }
    return reducedCost(constraintCost);
  }//getCost
  
  Fitness Constraint::getFitness()
  {
    if (!isScopeGround()) return FitnessUndef;
    
    Fitness fitness{FitnessMin};
    for (auto& propList : pInternalPropagatorMap)
    {
      // Subsumed and disabled propagators have fitness 0
      if (isPropagatorSubsumed(propList.first) ||
          isPropagatorDisabled(static_cast<PropagatorSemanticType>(propList.second.first),
                               propList.second.second))
      {
        continue;
      }
      
      // Get propagator fitness
      const auto pfitness = getPropagatorFromInternalMap(propList.first)->getFitness();
      
      // Return asap if the fitness is undefined
      if (pfitness < 0) return pfitness;
      
      // Otherwise sum up the fitness
      fitness += pfitness;
    }
    
    return fitness;
  }//getFitness
  
  void Constraint::registerScopeVariable(const VariableSPtr& aVariable)
  {
    assert(aVariable);
    
    auto scopeCardinalPosition = pCardinalTagMap.size();
    
    // Register position in map
    pCardinalTagMap[scopeCardinalPosition] = aVariable->getUUID();
    
    // Register variable
    pScope[pCardinalTagMap[scopeCardinalPosition]] = aVariable;
    
    // If at least one optimization variable is registered in the scope of this constraint,
    // this becomes an optimization constraint
    if (aVariable->variableSemantic()->getSemanticClassType() ==
        VariableSemanticClassType::VAR_SEMANTIC_CLASS_OBJECTIVE)
    {
      setOptimizationConstraint();
    }
  }//registerScopeVariable
  
  void Constraint::registerPropagator(const PropagatorSPtr& aPropagator)
  {
    assert(aPropagator);
    auto propagatorType = aPropagator->getPropagatorTypeId();
    assert(propagatorType != PropagatorSemanticType::PROP_SEMANTIC_TYPE_UNDEF);
    
    if((pPropagatorRegister.find(static_cast<int>(propagatorType)) == pPropagatorRegister.end()) ||
        pPropagatorRegister[static_cast<int>(propagatorType)].empty())
    {
      // Register domain reevaluation events for this propagator
      for(auto& event : aPropagator->getReevaluationEventSet())
      {
        
        reevaluationEventMap.push_back(event);
      }
    }
    
    pPropagatorRegister[static_cast<int>(propagatorType)].push_back(aPropagator);
    
    // Map int ids into internal map
    pInternalPropagatorMap[static_cast<int>(pInternalPropagatorMap.size())] =
    std::make_pair<int, std::size_t>(static_cast<int>(propagatorType), pPropagatorRegister[static_cast<int>(propagatorType)].size() - 1);
  }//registerPropagator
  
  void Constraint::resetState()
  {
    // Re-activate all subsumed propagators
    pSubsumedPropagatorSet.clear();
    
    // Enable all propagators if disabled
    for (auto it = pPropagatorRegister.begin(); it != pPropagatorRegister.end(); ++it)
    {
      for (auto& prop : it->second)
      {
        prop->enablePropagator();
      }
    }
  }//resetState
  
  PropagationEvent Constraint::post()
  {
    // Before posting notify subjects about the init of post/propagation
    NOTIFY_CONSTRAINT_OBSERVERS_PROP_INIT(PropagationEvent::PROP_EVENT_UNDEF);
    
    PropagationEvent eventAfterPost{ PropagationEvent::PROP_EVENT_SUBSUMED };
    
    bool allSubsumed{ true };
    for (auto& propList : pInternalPropagatorMap)
    {
      eventAfterPost = getPropagatorFromInternalMap(propList.first)->post();
      if (eventAfterPost == PropagationEvent::PROP_EVENT_FAIL)
      {
        return eventAfterPost;
      }
      if (eventAfterPost == PropagationEvent::PROP_EVENT_SUBSUMED)
      {
        pSubsumedPropagatorSet.insert(propList.first);
      }
      else
      {
        allSubsumed = false;
      }
    }
    
    // Before returning notify subjects about the termination of post/propagation
    NOTIFY_CONSTRAINT_OBSERVERS_PROP_TERM(eventAfterPost);
    
    if (!allSubsumed)
    {
      return PropagationEvent::PROP_EVENT_RUN_UNSPEC;
    }
    return PropagationEvent::PROP_EVENT_SUBSUMED;
  }//post
  
  PropagationEvent Constraint::propagate()
  {
    bool allSubsumed{ true };
    bool allFixPoint{ true };
    
    // Before posting notify subjects about the init of post/propagation
    NOTIFY_CONSTRAINT_OBSERVERS_PROP_INIT(PropagationEvent::PROP_EVENT_UNDEF);
    
    // Before any propagator, this constraint is implicitly subsumed
    PropagationEvent eventAfterProp{ PropagationEvent::PROP_EVENT_SUBSUMED };
    for (auto& propList : pInternalPropagatorMap)
    {
      // Skip subsumed and disabled propagators
      if(isPropagatorSubsumed(propList.first) ||
         isPropagatorDisabled(static_cast<PropagatorSemanticType>(propList.second.first), propList.second.second))
      {
        continue;
      }
      
      // Check event on propagation
      eventAfterProp = getPropagatorFromInternalMap(propList.first)->propagate();
      switch (eventAfterProp)
      {
        case PropagationEvent::PROP_EVENT_FAIL:
          return PropagationEvent::PROP_EVENT_FAIL;
          break;
        case PropagationEvent::PROP_EVENT_FIXPOINT:
          allSubsumed = false;
          break;
        case PropagationEvent::PROP_EVENT_SUBSUMED:
        {
          allFixPoint = false;
          pSubsumedPropagatorSet.insert(propList.first);
        }
          break;
        case PropagationEvent::PROP_EVENT_NO_FIXPOINT:
        {
          allSubsumed = false;
          allFixPoint = false;
        }
          break;
        default:
          assert(eventAfterProp == PropagationEvent::PROP_EVENT_RUN_UNSPEC);
        {
          allSubsumed = false;
          allFixPoint = false;
        }
          break;
      }
    }
    assert(eventAfterProp != PropagationEvent::PROP_EVENT_UNDEF);
    
    // Before returning notify subjects about the termination of post/propagation
    NOTIFY_CONSTRAINT_OBSERVERS_PROP_TERM(eventAfterProp);
    
    if(allSubsumed)
    {
      return PropagationEvent::PROP_EVENT_SUBSUMED;
    }
    if(allFixPoint)
    {
      return PropagationEvent::PROP_EVENT_FIXPOINT;
    }
    
    return PropagationEvent::PROP_EVENT_NO_FIXPOINT;
  }//propagate
  
}// end namespace Core
