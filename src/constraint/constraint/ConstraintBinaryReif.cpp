// Self first
#include "ConstraintBinaryReif.hpp"

#include "ConstraintParameterVariable.hpp"
#include "ConstraintParameterDomain.hpp"
#include "ConstraintParameterDomainArray.hpp"

#include <cassert>

namespace Core {

	ConstraintBinaryReif::ConstraintBinaryReif(ConstraintId aConstraintId,
																		         PropagatorStrategyType aStrategyType,
																		         const ConstraintParameterSPtr& aP1,
																		         const ConstraintParameterSPtr& aP2,
																		         const ConstraintParameterSPtr& aReif)
		: ConstraintBinary(aConstraintId, aStrategyType, aP1, aP2)
	{
		assert(VERIFY_PRIMITIVE_TYPE(aReif.get()));
		assert(ConstraintParameterDomain::isa(aReif.get()));
		assert(DomainBoolean::isa((ConstraintParameterDomain::cast(aReif.get())->getDomain()).get()));

		registerConstraintParameter(aReif);

		// Initialize internal reification domain
		pReifDomain = getReifDomain(aReif);
	}

	std::shared_ptr<DomainBoolean> ConstraintBinaryReif::getReifDomain(const ConstraintParameterSPtr& aReif)
	{
		assert(aReif);
		return std::static_pointer_cast<DomainBoolean>(ConstraintParameterDomain::cast(aReif.get())->getDomain());
	}//getCoeffArray

}// end namespace Core
