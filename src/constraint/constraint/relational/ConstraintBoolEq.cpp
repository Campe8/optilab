// Base class
#include "ConstraintBoolEq.hpp"

// Variable
#include "Variable.hpp"

#include "PropagatorBoolEq.hpp"

#include "ConstraintParameterVariable.hpp"
#include "ConstraintParameterDomain.hpp"
#include "DomainBoolean.hpp"

#include <utility>
#include <memory>
#include <cassert>

namespace Core {
  
  ConstraintBoolEq::ConstraintBoolEq(const ConstraintParameterSPtr& aParam1,
                                     const ConstraintParameterSPtr& aParam2)
  : Core::ConstraintBinary(ConstraintId::CON_ID_BOOL_EQ,
                           PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN,
                           aParam1, aParam2)
  {
    assert(VERIFY_PRIMITIVE_TYPE(aParam1.get()));
    assert(VERIFY_PRIMITIVE_TYPE(aParam2.get()));
    
    assert(DomainBoolean::isa(getParameterDomain(P0).get()));
    assert(DomainBoolean::isa(getParameterDomain(P1).get()));
    
    registerPropagator(std::make_shared<PropagatorBoolEq>(DomainBoolean::cast(getParameterDomain(P0).get()),
                                                          DomainBoolean::cast(getParameterDomain(P1).get())));
  }
  
  ConstraintBoolEq::~ConstraintBoolEq()
  {
  }
  
}// end namespace Core
