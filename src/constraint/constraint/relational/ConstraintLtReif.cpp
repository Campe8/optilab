// Self first
#include "ConstraintLtReif.hpp"

#include "PropagatorLtReif.hpp"


namespace Core {

	ConstraintLtReif::ConstraintLtReif(const ConstraintParameterSPtr& aP1,
																		 const ConstraintParameterSPtr& aP2,
		                                 const ConstraintParameterSPtr& aReif)
		: ConstraintBinaryReif(ConstraintId::CON_ID_LT_REIF, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, aP1, aP2, aReif)
	{

		// Register propagator lt reif
		auto propagatorLrR = std::make_shared<PropagatorLtReif>(
			getParameterDomain(0).get(),
			getParameterDomain(1).get(),
			getReifDomain());

		registerPropagator(propagatorLrR);
	}

	ConstraintLtHalfReifLeft::ConstraintLtHalfReifLeft(const ConstraintParameterSPtr& aP1,
		                                                 const ConstraintParameterSPtr& aP2,
		                                                 const ConstraintParameterSPtr& aReif)
		: ConstraintBinaryReif(ConstraintId::CON_ID_LT_HREIF_L, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, aP1, aP2, aReif)
	{

		// Register propagator lt reif
		auto propagatorLtRHL = std::make_shared<PropagatorLtHReifL>(
			getParameterDomain(0).get(),
			getParameterDomain(1).get(),
			getReifDomain());

		registerPropagator(propagatorLtRHL);
	}

	ConstraintLtHalfReifRight::ConstraintLtHalfReifRight(const ConstraintParameterSPtr& aP1,
		                                                   const ConstraintParameterSPtr& aP2,
		                                                   const ConstraintParameterSPtr& aReif)
		: ConstraintBinaryReif(ConstraintId::CON_ID_LT_HREIF_R, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, aP1, aP2, aReif)
	{

		// Register propagator lt reif
		auto propagatorLtRHR = std::make_shared<PropagatorLtHReifR>(
			getParameterDomain(0).get(),
			getParameterDomain(1).get(),
			getReifDomain());

		registerPropagator(propagatorLtRHR);
	}

}// end namespace Core
