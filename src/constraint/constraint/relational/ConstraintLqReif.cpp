// Self first
#include "ConstraintLqReif.hpp"

#include "PropagatorLqReif.hpp"


namespace Core {

	ConstraintLqReif::ConstraintLqReif(const ConstraintParameterSPtr& aP1,
																		 const ConstraintParameterSPtr& aP2,
		                                 const ConstraintParameterSPtr& aReif)
		: ConstraintBinaryReif(ConstraintId::CON_ID_LQ_REIF, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, aP1, aP2, aReif)
	{

		// Register propagator lq reif
		auto propagatorLqR = std::make_shared<PropagatorLqReif>(
			getParameterDomain(0).get(),
			getParameterDomain(1).get(),
			getReifDomain());

		registerPropagator(propagatorLqR);
	}

	ConstraintLqHalfReifLeft::ConstraintLqHalfReifLeft(const ConstraintParameterSPtr& aP1,
		                                                 const ConstraintParameterSPtr& aP2,
		                                                 const ConstraintParameterSPtr& aReif)
		: ConstraintBinaryReif(ConstraintId::CON_ID_LQ_HREIF_L, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, aP1, aP2, aReif)
	{

		// Register propagator lq reif
		auto propagatorLqRHL = std::make_shared<PropagatorLqHReifL>(
			getParameterDomain(0).get(),
			getParameterDomain(1).get(),
			getReifDomain());

		registerPropagator(propagatorLqRHL);
	}

	ConstraintLqHalfReifRight::ConstraintLqHalfReifRight(const ConstraintParameterSPtr& aP1,
		                                                   const ConstraintParameterSPtr& aP2,
		                                                   const ConstraintParameterSPtr& aReif)
		: ConstraintBinaryReif(ConstraintId::CON_ID_LQ_HREIF_R, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, aP1, aP2, aReif)
	{

		// Register propagator lq reif
		auto propagatorLqRHR = std::make_shared<PropagatorLqHReifR>(
			getParameterDomain(0).get(),
			getParameterDomain(1).get(),
			getReifDomain());

		registerPropagator(propagatorLqRHR);
	}

}// end namespace Core
