// Self first
#include "ConstraintNqReif.hpp"

#include "PropagatorNqReif.hpp"


namespace Core {

	ConstraintNqReif::ConstraintNqReif(const ConstraintParameterSPtr& aP1,
																		 const ConstraintParameterSPtr& aP2,
		                                 const ConstraintParameterSPtr& aReif)
		: ConstraintBinaryReif(ConstraintId::CON_ID_NQ_REIF, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, aP1, aP2, aReif)
	{

		// Register propagator nq reif
		auto propagatorNqR = std::make_shared<PropagatorNqReif>(
			getParameterDomain(0).get(),
			getParameterDomain(1).get(),
			getReifDomain());

		registerPropagator(propagatorNqR);
	}

	ConstraintNqHalfReifLeft::ConstraintNqHalfReifLeft(const ConstraintParameterSPtr& aP1,
		                                                 const ConstraintParameterSPtr& aP2,
		                                                 const ConstraintParameterSPtr& aReif)
		: ConstraintBinaryReif(ConstraintId::CON_ID_NQ_HREIF_L, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, aP1, aP2, aReif)
	{

		// Register propagator nq reif
		auto propagatorNqRHL = std::make_shared<PropagatorNqHReifL>(
			getParameterDomain(0).get(),
			getParameterDomain(1).get(),
			getReifDomain());

		registerPropagator(propagatorNqRHL);
	}

	ConstraintNqHalfReifRight::ConstraintNqHalfReifRight(const ConstraintParameterSPtr& aP1,
		                                                   const ConstraintParameterSPtr& aP2,
		                                                   const ConstraintParameterSPtr& aReif)
		: ConstraintBinaryReif(ConstraintId::CON_ID_NQ_HREIF_R, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, aP1, aP2, aReif)
	{

		// Register propagator nq reif
		auto propagatorNqRHR = std::make_shared<PropagatorNqHReifR>(
			getParameterDomain(0).get(),
			getParameterDomain(1).get(),
			getReifDomain());

		registerPropagator(propagatorNqRHR);
	}

}// end namespace Core
