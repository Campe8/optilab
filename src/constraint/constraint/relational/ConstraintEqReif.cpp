// Self first
#include "ConstraintEqReif.hpp"

#include "PropagatorEqReif.hpp"


namespace Core {

	ConstraintEqReif::ConstraintEqReif(const ConstraintParameterSPtr& aP1,
																		 const ConstraintParameterSPtr& aP2,
		                                 const ConstraintParameterSPtr& aReif,
		                                 PropagatorStrategyType aStrategyType)
		: ConstraintBinaryReif(ConstraintId::CON_ID_EQ_REIF, aStrategyType, aP1, aP2, aReif)
	{

		// Register propagator eq reif
		if (aStrategyType == PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND)
		{
			auto propagatorEqR = std::make_shared<PropagatorEqReifBound>(
				getParameterDomain(0).get(),
				getParameterDomain(1).get(),
				getReifDomain());

			registerPropagator(propagatorEqR);
		}
		else
		{
			assert(aStrategyType == PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN);
			auto propagatorEqR = std::make_shared<PropagatorEqReifDomain>(
				getParameterDomain(0).get(),
				getParameterDomain(1).get(),
				getReifDomain());

			registerPropagator(propagatorEqR);
		}
	}

	ConstraintEqHalfReifLeft::ConstraintEqHalfReifLeft(const ConstraintParameterSPtr& aP1,
		                                                 const ConstraintParameterSPtr& aP2,
		                                                 const ConstraintParameterSPtr& aReif,
		                                                 PropagatorStrategyType aStrategyType)
		: ConstraintBinaryReif(ConstraintId::CON_ID_EQ_HREIF_L, aStrategyType, aP1, aP2, aReif)
	{

		// Register propagator eq reif
		if (aStrategyType == PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND)
		{
			auto propagatorEqRHL = std::make_shared<PropagatorEqHReifLBound>(
				getParameterDomain(0).get(),
				getParameterDomain(1).get(),
				getReifDomain());
			registerPropagator(propagatorEqRHL);
		}
		else
		{
			assert(aStrategyType == PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN);
			auto propagatorEqRHL = std::make_shared<PropagatorEqHReifLDomain>(
				getParameterDomain(0).get(),
				getParameterDomain(1).get(),
				getReifDomain());
			registerPropagator(propagatorEqRHL);
		}
	}

	ConstraintEqHalfReifRight::ConstraintEqHalfReifRight(const ConstraintParameterSPtr& aP1,
		                                                   const ConstraintParameterSPtr& aP2,
		                                                   const ConstraintParameterSPtr& aReif,
		                                                   PropagatorStrategyType aStrategyType)
		: ConstraintBinaryReif(ConstraintId::CON_ID_EQ_HREIF_R, aStrategyType, aP1, aP2, aReif)
	{

		// Register propagator eq reif
		if (aStrategyType == PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND)
		{
			auto propagatorEqRHR = std::make_shared<PropagatorEqHReifRBound>(
				getParameterDomain(0).get(),
				getParameterDomain(1).get(),
				getReifDomain());
			registerPropagator(propagatorEqRHR);
		}
		else
		{
			assert(aStrategyType == PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN);
			auto propagatorEqRHR = std::make_shared<PropagatorEqHReifRDomain>(
				getParameterDomain(0).get(),
				getParameterDomain(1).get(),
				getReifDomain());
			registerPropagator(propagatorEqRHR);
		}
	}

}// end namespace Core
