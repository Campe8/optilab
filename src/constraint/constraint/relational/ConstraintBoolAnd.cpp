// Base class
#include "ConstraintBoolAnd.hpp"

// Variable
#include "Variable.hpp"

#include "PropagatorBoolAnd.hpp"

#include "ConstraintParameterVariable.hpp"
#include "ConstraintParameterDomain.hpp"
#include "DomainBoolean.hpp"

#include <utility>
#include <memory>
#include <cassert>

namespace Core {
  
  ConstraintBoolAndBin::ConstraintBoolAndBin(const ConstraintParameterSPtr& aParam1,
                                             const ConstraintParameterSPtr& aParam2)
  : Core::ConstraintBinary(ConstraintId::CON_ID_BOOL_AND,
                           PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN,
                           aParam1, aParam2)
  {
    assert(VERIFY_PRIMITIVE_TYPE(aParam1.get()));
    assert(VERIFY_PRIMITIVE_TYPE(aParam2.get()));
    
    assert(DomainBoolean::isa(getParameterDomain(P0).get()));
    assert(DomainBoolean::isa(getParameterDomain(P1).get()));
    
    registerPropagator(std::make_shared<PropagatorBoolAndBin>(DomainBoolean::cast(getParameterDomain(P0).get()),
                                                              DomainBoolean::cast(getParameterDomain(P1).get())));
  }
  
  ConstraintBoolAndBin::~ConstraintBoolAndBin()
  {
  }
  
  ConstraintBoolAndTer::ConstraintBoolAndTer(const ConstraintParameterSPtr& aParam1,
                                             const ConstraintParameterSPtr& aParam2,
                                             const ConstraintParameterSPtr& aParam3)
  : Core::ConstraintTernary(ConstraintId::CON_ID_BOOL_AND,
                            PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN,
                            aParam1, aParam2, aParam3)
  {
    assert(VERIFY_PRIMITIVE_TYPE(aParam1.get()));
    assert(VERIFY_PRIMITIVE_TYPE(aParam2.get()));
    assert(VERIFY_PRIMITIVE_TYPE(aParam3.get()));
    
    assert(DomainBoolean::isa(getParameterDomain(P0).get()));
    assert(DomainBoolean::isa(getParameterDomain(P1).get()));
    assert(DomainBoolean::isa(getParameterDomain(P2).get()));
    
    registerPropagator(std::make_shared<PropagatorBoolAndTer>(DomainBoolean::cast(getParameterDomain(P0).get()),
                                                              DomainBoolean::cast(getParameterDomain(P1).get()),
                                                              DomainBoolean::cast(getParameterDomain(P2).get())));
  }
  
  ConstraintBoolAndTer::~ConstraintBoolAndTer()
  {
  }
  
}// end namespace Core
