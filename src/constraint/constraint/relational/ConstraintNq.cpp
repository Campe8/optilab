// Base class
#include "ConstraintNq.hpp"

// Variable
#include "Variable.hpp"

#include "PropagatorNq.hpp"

#include "ConstraintParameterVariable.hpp"
#include "ConstraintParameterDomain.hpp"

#include <utility>
#include <memory>
#include <cassert>

namespace Core {
  
  ConstraintNq::ConstraintNq(const ConstraintParameterSPtr& aParam1,
                             const ConstraintParameterSPtr& aParam2)
  : Core::ConstraintBinary(ConstraintId::CON_ID_NQ,
                           PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN,
                           aParam1, aParam2)
  {
    assert(VERIFY_PRIMITIVE_TYPE(aParam1.get()));
    assert(VERIFY_PRIMITIVE_TYPE(aParam2.get()));
    
    // Register propagator nq
    registerNqPropagator();
  }
  
  void ConstraintNq::registerNqPropagator()
  {
    PropagatorSPtr propagatorNq{nullptr};
    propagatorNq = std::make_shared<PropagatorNq>(getParameterDomain(P0).get(), getParameterDomain(P1).get());
    
    registerPropagator(propagatorNq);
  }//registerPropagator
  
}// end namespace Core
