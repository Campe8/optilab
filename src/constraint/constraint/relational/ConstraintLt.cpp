// Base class
#include "ConstraintLt.hpp"

// Variable
#include "Variable.hpp"

#include "PropagatorLt.hpp"

#include "ConstraintParameterVariable.hpp"
#include "ConstraintParameterDomain.hpp"

#include <utility>
#include <memory>
#include <cassert>

namespace Core {
  
  ConstraintLt::ConstraintLt(const ConstraintParameterSPtr& aParam1,
                             const ConstraintParameterSPtr& aParam2)
  : Core::ConstraintBinary(ConstraintId::CON_ID_LT, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, aParam1, aParam2)
  {
    assert(VERIFY_PRIMITIVE_TYPE(aParam1.get()));
    assert(VERIFY_PRIMITIVE_TYPE(aParam2.get()));
    
    // Register propagator lt
    registerLtPropagator();
  }
  
  ConstraintLt::~ConstraintLt()
  {
  }
  
  void ConstraintLt::registerLtPropagator()
  {
    PropagatorSPtr propagatorLt{nullptr};
    propagatorLt = std::make_shared<PropagatorLt>(getParameterDomain(P0).get(), getParameterDomain(P1).get());
    
    registerPropagator(propagatorLt);
  }//registerPropagator
  
}// end namespace Core
