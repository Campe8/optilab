// Base class
#include "ConstraintEq.hpp"

// Variable
#include "Variable.hpp"

#include "PropagatorEq.hpp"

#include "ConstraintParameterVariable.hpp"
#include "ConstraintParameterDomain.hpp"

#include <utility>
#include <memory>
#include <cassert>

namespace Core {
  
  ConstraintEq::ConstraintEq(const ConstraintParameterSPtr& aParam1,
                             const ConstraintParameterSPtr& aParam2,
                             PropagatorStrategyType aStrategyType)
  : Core::ConstraintBinary(ConstraintId::CON_ID_EQ, aStrategyType, aParam1, aParam2)
  {
    assert(VERIFY_PRIMITIVE_TYPE(aParam1.get()));
    assert(VERIFY_PRIMITIVE_TYPE(aParam2.get()));
    
    // Register propagator eq
    registerEqPropagator(aStrategyType);
  }
  
  ConstraintEq::~ConstraintEq()
  {
  }
  
  void ConstraintEq::registerEqPropagator(PropagatorStrategyType aStrategyType)
  {
    PropagatorSPtr propagatorEq{nullptr};
    if(aStrategyType == PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND)
    {
      propagatorEq = std::make_shared<PropagatorEqBound>(getParameterDomain(P0).get(), getParameterDomain(P1).get());
    }
    else
    {
      assert(aStrategyType == PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN);
      propagatorEq = std::make_shared<PropagatorEqDomain>(getParameterDomain(P0).get(), getParameterDomain(P1).get());
    }
    
    registerPropagator(propagatorEq);
  }//registerPropagator
  
}// end namespace Core
