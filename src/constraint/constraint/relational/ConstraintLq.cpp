// Base class
#include "ConstraintLq.hpp"

// Variable
#include "Variable.hpp"

#include "PropagatorLq.hpp"

#include "ConstraintParameterVariable.hpp"
#include "ConstraintParameterDomain.hpp"

#include <utility>
#include <memory>
#include <cassert>

namespace Core {
  
  ConstraintLq::ConstraintLq(const ConstraintParameterSPtr& aParam1,
                             const ConstraintParameterSPtr& aParam2)
  : Core::ConstraintBinary(ConstraintId::CON_ID_LQ, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND, aParam1, aParam2)
  {
    assert(VERIFY_PRIMITIVE_TYPE(aParam1.get()));
    assert(VERIFY_PRIMITIVE_TYPE(aParam2.get()));
    
    // Register propagator lq
    registerLqPropagator();
  }
  
  ConstraintLq::~ConstraintLq()
  {
  }
  
  void ConstraintLq::registerLqPropagator()
  {
    PropagatorSPtr propagatorLq{nullptr};
    propagatorLq = std::make_shared<PropagatorLq>(getParameterDomain(P0).get(), getParameterDomain(P1).get());
    
    registerPropagator(propagatorLq);
  }//registerPropagator
  
}// end namespace Core
