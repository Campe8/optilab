// Base class
#include "NogoodConstraint.hpp"

// Variable
#include "Variable.hpp"

// Domain
#include "Domain.hpp"
#include "DomainElement.hpp"

#include "ConstraintParameterFactory.hpp"

// Propagator
#include "PropagatorNogood.hpp"

#include "ConstraintStore.hpp"

#include <memory>
#include <cassert>

namespace Core {
 
  NogoodConstraint::NogoodConstraint(const std::vector<VariableSPtr>& aNogoodVars,
                                     const std::vector<DomainSPtr>& aDomains)
  : Constraint(ConstraintId::CON_ID_NOGOOD, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND)
  {
    assert(aNogoodVars.size() == aDomains.size());
    
    // Register parameters
    ConstraintParameterFactory cpf;
    registerConstraintParameter(cpf.constraintParameterVariable(aNogoodVars));
    registerConstraintParameter(cpf.constraintParameterDomain(aDomains));
    
    // Register propagator
    registerNogoodPropagator(aNogoodVars, aDomains);
  }//BranchingConstraint
  
  PropagationCost NogoodConstraint::reducedCost(const PropagationCost& aPropagationCost)
  {
    (void) aPropagationCost;
    static PropagationCost unaryCost(PropagationPriority::PROP_PRIORITY_UNARY);
    return unaryCost;
  }//reducedCost
  
  void NogoodConstraint::subscribeToConstraintStore(const std::shared_ptr<Semantics::ConstraintStore>& aConstraintStore)
  {
    assert(aConstraintStore);
    aConstraintStore->storeConstraint(this);
    Constraint::subscribeToConstraintStore(aConstraintStore);
  }//subscribeToConstraintStore
  
  void NogoodConstraint::unsubscribeFromConstraintStore()
  {
    // Remove this as stored constraint from the constraint store
    removeStoredConstraintFromConstraintStore();
    Constraint::unsubscribeFromConstraintStore();
  }//unsubscribeFromConstraintStore
  
  void NogoodConstraint::registerNogoodPropagator(const std::vector<VariableSPtr>& aNogoodVars,
                                                  const std::vector<DomainSPtr>& aDomains)
  {
    // Prepare array of domains
    DomainArraySPtr dArray = std::make_shared<DomainArray>(aNogoodVars.size());
    for (std::size_t idx{ 0 }; idx < dArray->size(); ++idx)
    {
      assert(aNogoodVars[idx] && aNogoodVars[idx]->domainListSize() == 1);
      (*dArray).assignElementToCell(idx, (aNogoodVars[idx]->domain()).get());
    }
    
    // Prepare array of elements
    DomainElementArraySPtr dElementArray = std::make_shared<DomainElementArray>(aDomains.size());
    for (std::size_t idx{ 0 }; idx < dArray->size(); ++idx)
    {
      assert(aDomains[idx] && aDomains[idx]->getSize() == 1);
      (*dElementArray).assignElementToCell(idx, aDomains[idx]->lowerBound());
    }
    
    registerPropagator(std::make_shared<PropagatorNogood>(dArray, dElementArray));
  }//registerNogoodPropagator

}// end namespace Core
