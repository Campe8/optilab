// Base class
#include "BranchingConstraintDomainSplitting.hpp"

namespace Core {
 
  BranchingConstraintDomainSplitting::BranchingConstraintDomainSplitting(const VariableSPtr& aBranchingVar, const DomainSPtr& aDomain,
                                                                   BranchingConstraintId aBranchingConstraintId)
  : BranchingConstraint(aBranchingVar, aDomain, aBranchingConstraintId)
  {
    if (aBranchingConstraintId == BranchingConstraintId::BRC_CON_ID_DOMAIN_SPLIT_LQ)
    {
      registerBranchingPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ);
    }
    else
    {
      assert(aBranchingConstraintId == BranchingConstraintId::BRC_CON_ID_DOMAIN_SPLIT_GT);
      registerBranchingPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_LT, true);
    }
  }//BranchingConstraint
  
}// end namespace Core
