// Base class
#include "BranchingConstraintFactory.hpp"

// Branching constraints
#include "BranchingConstraintDomainSplitting.hpp"
#include "BranchingConstraintBinaryChoice.hpp"

#include "Variable.hpp"

#include <cassert>

namespace Core {
  
  BranchingConstraintFactory::BranchingConstraintFactory()
  : pDomainFactory(new DomainFactory())
  {
  }
  
  BranchingConstraintFactory::~BranchingConstraintFactory()
  {
  }
  
  BranchingConstraintSPtr BranchingConstraintFactory::branchingConstraint(const VariableSPtr& aBranchingVar,
                                                                          DomainElement* aBranchingDomainElement,
                                                                          BranchingConstraintId aBranchingConstraintId)
  {
    assert(aBranchingVar->hasPrimitiveType());
    auto domainType = aBranchingVar->domain()->getDomainType();
    
    BranchingConstraint *brcConstraint{nullptr};
    switch (aBranchingConstraintId) {
      case BranchingConstraintId::BRC_CON_ID_BINARY_CHOICE_EQ:
      case BranchingConstraintId::BRC_CON_ID_BINARY_CHOICE_NQ:
      {
        brcConstraint = new BranchingConstraintBinaryChoice(aBranchingVar, pDomainFactory->getSharedFromRawDomainPointer(pDomainFactory->domain(aBranchingDomainElement, domainType)), aBranchingConstraintId);
      }
        break;
      default:
      {
        assert(aBranchingConstraintId == BranchingConstraintId::BRC_CON_ID_DOMAIN_SPLIT_LQ ||
               aBranchingConstraintId == BranchingConstraintId::BRC_CON_ID_DOMAIN_SPLIT_GT);
        brcConstraint = new BranchingConstraintDomainSplitting(aBranchingVar, pDomainFactory->getSharedFromRawDomainPointer(pDomainFactory->domain(aBranchingDomainElement, domainType)), aBranchingConstraintId);
      }
        break;
    }
    assert(brcConstraint);
    return BranchingConstraintSPtr(brcConstraint);
  }//getBranchingConstraint
  
}// end namespace Core
