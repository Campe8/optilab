// Base class
#include "BranchingConstraint.hpp"

// Variable
#include "Variable.hpp"

// Domain
#include "Domain.hpp"
#include "DomainElement.hpp"

#include "ConstraintParameterFactory.hpp"

// Propagators
#include "PropagatorEq.hpp"
#include "PropagatorNq.hpp"
#include "PropagatorLq.hpp"
#include "PropagatorLt.hpp"

#include <algorithm>

namespace Core {
 
  BranchingConstraint::BranchingConstraint(const VariableSPtr& aBranchingVar, const DomainSPtr& aDomain,
                                           BranchingConstraintId aBranchingConstraintId)
  : Constraint(ConstraintId::CON_ID_BRANCHING, PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND)
  , pBranchingConstraintId(aBranchingConstraintId)
  , pBranchingDomain(aDomain)
  , pBranchingVariable(aBranchingVar)
  {
    assert(aBranchingVar);
    assert(aDomain);
    assert(aBranchingVar->hasPrimitiveType());
    assert(aDomain->getSize() == 1);
    
    // Register parameters
    ConstraintParameterFactory cpf;
    registerConstraintParameter(cpf.constraintParameterVariable(aBranchingVar));
    registerConstraintParameter(cpf.constraintParameterDomain(aDomain));
  }//BranchingConstraint
  
  std::string BranchingConstraint::prettyPrintConstraint() const
  {
    std::string conSymb;
    switch (static_cast<int>(pBranchingConstraintId)) {
      case static_cast<int>(BRC_CON_ID_BINARY_CHOICE_EQ):
        conSymb = "=";
        break;
      case static_cast<int>(BRC_CON_ID_BINARY_CHOICE_NQ):
        conSymb = "!=";
        break;
      case static_cast<int>(BRC_CON_ID_DOMAIN_SPLIT_LQ):
        conSymb = "<=";
        break;
      default:
        assert(static_cast<int>(pBranchingConstraintId) == static_cast<int>(BRC_CON_ID_DOMAIN_SPLIT_GT));
        conSymb = ">";
        break;
    }
    
    // Get variable id
    assert(pBranchingVariable);
    std::string ppCon = pBranchingVariable->getVariableNameId();
    
    // Set the constraint
    ppCon += " " + conSymb + " ";
    
    // Set the domain value
    assert(pBranchingDomain);
    assert(pBranchingDomain->getSize() == 1);
    ppCon += pBranchingDomain->lowerBound()->toString();
    
    return  ppCon;
  }//prettyPrintConstraint
  
  PropagationCost BranchingConstraint::reducedCost(const PropagationCost& aPropagationCost)
  {
    (void) aPropagationCost;
    static PropagationCost unaryCost(PropagationPriority::PROP_PRIORITY_UNARY);
    return unaryCost;
  }//reducedCost
  
  void BranchingConstraint::registerBranchingPropagator(PropagatorSemanticType aPropagatorType, bool aInverseRelation)
  {
    assert(pBranchingDomain);
    assert(pBranchingVariable && pBranchingVariable->domain());
    
    // Set the two domains involved in the branching propagator:
    // lhs rel rhs
    // where:
    // lhs = variable to branch on
    // rhs = branching point
    // rel = branching relation
    auto lhs = pBranchingVariable->domain().get();
    auto rhs = pBranchingDomain.get();
    if(aInverseRelation)
    {
      // If there is an inverse relation, swap the domains
      std::swap(lhs, rhs);
    }

    // Register the propagtor according to the relation between lhs and rhs
    switch (aPropagatorType)
    {
      case PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ:
        registerPropagator(std::make_shared<PropagatorEqBound>(lhs, rhs));
        break;
      case PropagatorSemanticType::PROP_SEMANTIC_TYPE_NQ:
        registerPropagator(std::make_shared<PropagatorNq>(lhs, rhs));
        break;
      case PropagatorSemanticType::PROP_SEMANTIC_TYPE_LQ:
        registerPropagator(std::make_shared<PropagatorLq>(lhs, rhs));
        break;
      default:
        assert(aPropagatorType == PropagatorSemanticType::PROP_SEMANTIC_TYPE_LT);
        registerPropagator(std::make_shared<PropagatorLt>(lhs, rhs));
        break;
    }
  }//registerBranchingPropagator
  
}// end namespace Core
