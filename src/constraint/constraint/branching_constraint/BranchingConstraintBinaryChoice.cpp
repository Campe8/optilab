// Base class
#include "BranchingConstraintBinaryChoice.hpp"

namespace Core {
 
  BranchingConstraintBinaryChoice::BranchingConstraintBinaryChoice(const VariableSPtr& aBranchingVar, const DomainSPtr& aDomain,
                                                                   BranchingConstraintId aBranchingConstraintId)
  : BranchingConstraint(aBranchingVar, aDomain, aBranchingConstraintId)
  {
    if(aBranchingConstraintId == BranchingConstraintId::BRC_CON_ID_BINARY_CHOICE_EQ)
    {
      registerBranchingPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_EQ);
    }
    else
    {
      assert(aBranchingConstraintId == BranchingConstraintId::BRC_CON_ID_BINARY_CHOICE_NQ);
      registerBranchingPropagator(PropagatorSemanticType::PROP_SEMANTIC_TYPE_NQ);
    }
  }//BranchingConstraint
  
  BranchingConstraintBinaryChoice::~BranchingConstraintBinaryChoice()
  {
  }
  
}// end namespace Core
