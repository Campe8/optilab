// Base class
#include "ConstraintTernary.hpp"

// Variable
#include "Variable.hpp"

#include <utility>
#include <cassert>

namespace Core {
  
    ConstraintTernary::ConstraintTernary(ConstraintId aConstraintId,
                                         PropagatorStrategyType aStrategyType,
                                         const ConstraintParameterSPtr& aParam1,
                                         const ConstraintParameterSPtr& aParam2,
                                         const ConstraintParameterSPtr& aParam3)
  : Constraint(aConstraintId, aStrategyType)
  {
    registerConstraintParameter(aParam1);
    registerConstraintParameter(aParam2);
    registerConstraintParameter(aParam3);
    assert(numConstraintParameters() == 3);
  }
  
    ConstraintTernary::~ConstraintTernary()
  {
  }
  
    PropagationCost ConstraintTernary::reducedCost(const PropagationCost& aPropagationCost)
  {
    auto numVars = scopeSize();
    if (numVars <= 1)
    {
      static PropagationCost unaryCost(PropagationPriority::PROP_PRIORITY_UNARY);
      return unaryCost;
    }
    else if (numVars == 2)
    {
        static PropagationCost binaryCost(PropagationPriority::PROP_PRIORITY_BINARY);
        return binaryCost;
    }

    return aPropagationCost;
  }//reducedCost
  
}// end namespace Core
