// Self first
#include "IRPrettyPrinter.hpp"

#include "IRModule.hpp"
#include "IRConDecl.hpp"
#include "IRVarDecl.hpp"
#include "IRDomDeclBounds.hpp"
#include "IRDomDeclExt.hpp"
#include "IRExprStdVar.hpp"
#include "IRExprVar.hpp"
#include "IRExprConst.hpp"
#include "IRExprAssign.hpp"
#include "IRContext.hpp"
#include "IRType.hpp"
#include "IRSrcDecl.hpp"
#include "IRExprRef.hpp"
#include "IRExprMatrixSub.hpp"
#include "IRSymbolConstArray.hpp"

#include "SearchInformation.hpp"

#include "CLIUtils.hpp"

#include <cassert>

namespace IR {
  
  static std::string getReferenceExprSymbName(IRExprRef* aExpr)
  {
    assert(aExpr);
    auto refExpr = aExpr->getReferenceExpr().get();
    if(IRExprConst::isa(refExpr))
    {
      return IRExprConst::cast(refExpr)->getConst()->getName();
    }
    assert(IRExprVar::isa(refExpr));
    return IRExprVar::cast(refExpr)->getVar()->getName();
  }//getReferenceExprSymbName
  
	IRPrettyPrinter::IRPrettyPrinter()
	{
		initializeStream();
	}

	void IRPrettyPrinter::initializeStream()
	{
		pOS.str(std::string());
	}//initializeStream

	void IRPrettyPrinter::toStream(std::ostream& aOut)
	{
		aOut << pOS.str();
		initializeStream();
	}//toStream

	void IRPrettyPrinter::toOS(const std::string& aStr)
	{
		if (!aStr.empty())
		{
			pOS << aStr;
		}
	}//toOS

	void IRPrettyPrinter::toOSNL()
	{
		toOS("\n");
	}//toOS

	void IRPrettyPrinter::toOSNL(const std::string& aStr)
	{
		toOS(aStr + '\n');
	}//toOS

	void IRPrettyPrinter::visit(IRModule* aMod)
	{
		assert(aMod);
		toOSNL(aMod->getName() + ':');

		// First visit the parent module
		// by exploring its block,
		// i.e., the set of its stmts
		auto blk = aMod->getBlock();
		for (auto& stmt : *blk)
		{
			// For each stmt visit the root node
      // @note this will go rrecursively to sub-nodes
			auto it = stmt->preOrderBegin();
      it->data()->acceptVisitor(this);
      toOSNL(";");
		}

		// Then visit all the sub-modules
		for (auto& mod : *aMod)
		{
			mod.get()->acceptVisitor(this);
			toOSNL();
		}
	}//visit

	void IRPrettyPrinter::visit(IRConDecl* aConDecl)
	{
		assert(aConDecl);
		aConDecl->fcnCon()->acceptVisitor(this);
	}//visit

  void IRPrettyPrinter::visit(IRExprAssign* aAssign)
  {
    assert(aAssign);
    assert(aAssign->lhs());
    assert(aAssign->rhs());
    
    // Print lhs
    aAssign->lhs()->acceptVisitor(this);
    
    toOS("=");
    
    // Print rhs
    aAssign->rhs()->acceptVisitor(this);
  }//visit
  
	void IRPrettyPrinter::visit(IRVarDecl* aVarDecl)
	{
		assert(aVarDecl);
		assert(aVarDecl->lhs());
		assert(aVarDecl->getNumRhs() > 0);

    // Print variable semantic
		toOS("(");
		auto sem = aVarDecl->lhs()->getSemantic();
		switch (sem)
		{
		case IR::IRExprVar::VS_DECISION:
			toOS("d");
			break;
		case IR::IRExprVar::VS_SUPPORT:
			toOS("s");
			break;
		case IR::IRExprVar::VS_OBJECTIVE_MAX:
			toOS("max");
			break;
		default:
			assert(sem == IR::IRExprVar::VS_OBJECTIVE_MIN);
			toOS("min");
			break;
		}
    
    for(auto& ss : *aVarDecl->lhs())
    {
      assert(ss->getSearchDeclaration());
      if(ss->getBranchingPolicy())
      {
        std::string b{", b("};
        b += ss->getSearchDeclaration()->getID() + ")";
        toOS(b);
      }
      if(ss->getReachability())
      {
        std::string r{", r("};
        r += ss->getSearchDeclaration()->getID() + ")";
        toOS(r);
      }
    }
		toOS(")");

    // Print variable name
		aVarDecl->lhs()->acceptVisitor(this);
    
    // Print variable size
    auto var = IRExprVar::cast((aVarDecl->lhs()).get());
    assert(var);
    auto vType = (var->getType()).get();
    if(IRTypeArray::isa(vType))
    {
      toOS("[");
      std::vector<std::string> dims;
      auto baseType = vType;
      while (IRTypeArray::isa(baseType))
      {
        dims.push_back(CLI::Utils::toString<std::size_t>(IRTypeArray::cast(baseType)->getNumElements()));
        baseType = (IRTypeArray::cast(baseType)->getBaseType()).get();
      }
      for(std::size_t idx{0}; idx < dims.size() - 1; ++idx)
      {
        toOS(dims[idx]);
        toOS(", ");
      }
      toOS(dims.back());
      toOS("]");
    }
    
    // Print domain
		toOS(": ");
    
    auto numRhs = aVarDecl->getNumRhs();
    if(numRhs == 1)
    {
      aVarDecl->rhs(0)->acceptVisitor(this);
    }
    else
    {
      toOS("[");
      for(std::size_t idx{0}; idx < numRhs - 1; ++idx)
      {
        aVarDecl->rhs(idx)->acceptVisitor(this);
        toOS(", ");
      }
      aVarDecl->rhs(numRhs - 1)->acceptVisitor(this);
      toOS("]");
    }
	}//visit

	void IRPrettyPrinter::visit(IRDomDeclBounds* aDomDeclB)
	{
		assert(aDomDeclB);
		toOS("[");
		aDomDeclB->getLowerBound()->acceptVisitor(this);
		toOS(", ");
		aDomDeclB->getUpperBound()->acceptVisitor(this);
		toOS("]");
	}//visit

	void IRPrettyPrinter::visit(IRDomDeclExt* aDomDeclE)
	{
		assert(aDomDeclE);
		toOS("{");
		
		std::size_t idx{ 0 };
		auto dSize = (aDomDeclE->getExtensionalDomain()).size();
		for (auto& d : aDomDeclE->getExtensionalDomain())
		{
			d->acceptVisitor(this);
			if (idx++ < dSize - 1)
			{
				toOS(", ");
			}
		}
		toOS("}");
	}//visit

  void IRPrettyPrinter::visit(IRExprStdVar* aEVar)
  {
    assert(aEVar);
    assert(aEVar->getVar());
    aEVar->getVar()->acceptVisitor(this);
  }//visit
  
	void IRPrettyPrinter::visit(IRExprVar* aEVar)
	{
		assert(aEVar);
		assert(aEVar->getVar());
		aEVar->getVar()->acceptVisitor(this);
	}//visit

	void IRPrettyPrinter::visit(IRSymbol* aSymb)
	{
		assert(aSymb);
		toOS(aSymb->getName());
	}//visit

	void IRPrettyPrinter::visit(IRExprConst* aEConst)
	{
		assert(aEConst);
		if (!aEConst->getConst()) { return; }
		aEConst->getConst()->acceptVisitor(this);
	}//visit

	void IRPrettyPrinter::visit(IRExprFcn* aEFcn)
	{
		assert(aEFcn);
		if (!aEFcn->getFcn()) { return; }
		aEFcn->getFcn()->acceptVisitor(this);
		toOS("(");
		for (std::size_t idx{ 0 }; idx < aEFcn->numArgs(); ++idx)
		{
			aEFcn->getArg(idx)->acceptVisitor(this);
			if (idx < aEFcn->numArgs() - 1)
			{
				toOS(", ");
			}
		}
		toOS(")");
	}//visit

	void IRPrettyPrinter::visit(IRType* aType)
	{
		assert(aType);
		std::stringstream ss;
		aType->prettyPrint(ss);
		toOS(ss.str());
	}//visit

	void IRPrettyPrinter::visit(IRContext* aCtx)
	{
		assert(aCtx);
		if (!aCtx->globalModule()) { return; }
		aCtx->globalModule()->acceptVisitor(this);
	}//visit

	void IRPrettyPrinter::visit(IRSrcDecl* aSrc)
	{
		assert(aSrc);
		
		auto srcType = aSrc->getSearchType();
		assert(srcType == IR::IRSrcDecl::SearchType::ST_DFS);
    
		toOS("dfs(");
		for (std::size_t idx{ 0 }; idx < aSrc->numBranchingVars(); ++idx)
		{
			aSrc->getBranchingVar(idx)->acceptVisitor(this);
			if (idx < aSrc->numBranchingVars() - 1)
			{
				toOS(", ");
			}
		}
		toOS("): ");

    auto srcInfo = aSrc->getSearchInformation();
    assert(srcInfo && srcInfo->getMap());
    
    auto infoMap = srcInfo->getMap();
    for(const auto& it : *infoMap)
    {
      toOS(it.first);
      toOS(", ");
    }
	}//visit
  
  void IRPrettyPrinter::visit(IRExprRef* aExprRef)
  {
    assert(aExprRef);
    toOS("&");
    toOS(getReferenceExprSymbName(aExprRef));
  }//visit

  void IRPrettyPrinter::visit(IRExprMatrixSub* aExprMatSub)
  {
    assert(aExprMatSub);
    auto mat = aExprMatSub->getMatrix();
    assert(mat);
    
    // Print only name of the referenced matrix
    auto matPtr = mat.get();
    if(IR::IRExprRef::isa(matPtr))
    {
      toOS(getReferenceExprSymbName(IR::IRExprRef::cast(matPtr)));
    }
    else
    {
      assert(IR::IRExprVar::isa(matPtr));
      toOS(IR::IRExprVar::cast(matPtr)->getVar()->getName());
    }
    toOS("[");
    assert(aExprMatSub->getSubscript());
    aExprMatSub->getSubscript()->acceptVisitor(this);
    toOS("]");
  }//visit
  
  void IRPrettyPrinter::visit(IRSymbolConstArray* aSymbArray)
  {
    assert(aSymbArray);
    
    // Print each array element
    toOS("[");
    
    for(std::size_t idx{0}; idx < aSymbArray->size(); ++idx)
    {
      aSymbArray->getSymbol(idx)->acceptVisitor(this);
      if (idx < aSymbArray->size() - 1)
      {
        toOS(", ");
      }
    }
    toOS("]");
  }//visit
  
}// end namespace IR
