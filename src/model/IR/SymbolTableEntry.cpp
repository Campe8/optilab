// Self first
#include "SymbolTableEntry.hpp"

#include "IRExpr.hpp"

namespace IR {
  
  SymbolTableEntry::SymbolTableEntry(IRSymbol::SymbolName aSymbolName, IRSymbolType aSymbolType)
  : pSymbolName(aSymbolName)
  , pSymbol(nullptr)
  , pSymbolType(aSymbolType)
  , pType(nullptr)
  {
  }
  
  void SymbolTableEntry::setType(const std::shared_ptr<IRType>& aType)
  {
    if(!pType || !aType)
    {
      pType = aType;
    }
    assert(!pType || pType->isEqual(aType.get()));
  }//setType
  
  void SymbolTableEntry::addExprLink(const std::shared_ptr<IRExpr>& aLink)
  {
    assert(aLink);
    assert(aLink->getType());
    if (!pType)
    {
      setType(aLink->getType());
    }
    
    assert(getType()->isEqual((aLink->getType()).get()));
    pExprSet.insert(aLink);
  }//addExprLink
  
  void SymbolTableEntry::removeExprLink(const std::shared_ptr<IRExpr>& aLink)
  {
    if (!aLink) { return; }
    pExprSet.erase(aLink);
    
    if (pExprSet.empty())
    {
      setType(nullptr);
    }
  }//removeExprLink
  
  void SymbolTableEntry::removeAllExprLinks()
  {
    pExprSet.clear();
    setType(nullptr);
  }//removeAllExprLinks
  
}// end namespace IR

