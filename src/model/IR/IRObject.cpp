// Self first
#include "IRObject.hpp"

namespace IR {
  
  IRObject::IRObject(IRObjectType aType)
  : pObjectType(aType)
  , pMark(ObjectMark::DEFAULT)
  {
  }
  
}// end namespace IR
