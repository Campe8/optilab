// Self first
#include "IRExprStdVar.hpp"

#include "IRVisitor.hpp"
#include "IRSrcDecl.hpp"

namespace IR {
  
  IRExprStdVar::IRExprStdVar(const std::shared_ptr<IRBlock>& aParentBlock)
  : IRExpr(aParentBlock, IRExprType::IR_EXPR_STD_VAR)
  {
  }
  
  bool IRExprStdVar::isa(const IRNode* aNode)
  {
    return aNode && IRExpr::isa(aNode) &&
    static_cast<const IRExpr*>(aNode)->getExprType() == IRExprType::IR_EXPR_STD_VAR;
  }//isa
  
  IRExprStdVar* IRExprStdVar::cast(IRNode* aNode)
  {
    if (!isa(aNode)) return nullptr;
    return static_cast<IRExprStdVar*>(aNode);
  }//cast
  
  const IRExprStdVar* IRExprStdVar::cast(const IRNode* aNode)
  {
    if (!isa(aNode)) return nullptr;
    return static_cast<const IRExprStdVar*>(aNode);
  }//cast
  
  void IRExprStdVar::acceptVisitor(IRVisitor* aVisitor)
  {
    assert(aVisitor);
    aVisitor->visit(this);
  }//acceptVisitor
  
  void IRExprStdVar::setType(const std::shared_ptr<IRType>& aType)
  {
    if(!(getVar()->getType()))
    {
      // Set same type for symbol variable
      getVar()->setType(aType);
    }
    // Types must agree
    assert(getVar()->getType() == aType);
    
    IRExpr::setType(aType);
  }//setType
  
  void IRExprStdVar::setVar(SymbVarSPtrCRef aVar)
  {
    assert(aVar);
    addContainedObject(aVar);
    
    if (getVar()->getType())
    {
      setType(getVar()->getType());
    }
    
    // Type must agree
    assert(aVar->getType() == getType());
  }//setVar
  
  IRExprStdVar::SymbVarSPtr IRExprStdVar::getVar() const
  {
    assert(numContainedObj() == 1);
    return std::static_pointer_cast<IRSymbolVar>(getContainedObj(0));
  }//setVar
  
  bool IRExprStdVar::isSemanticallyEqual(const IRExpr* aExpr) const
  {
    if(!isa(aExpr)) return false;
    auto stdvar = cast(aExpr);
    auto var = getVar();
    if(!var) return false;
    
    return var->isEqual((stdvar->getVar()).get());
  }//isSemanticallyEqual
  
}// end namespace IR
