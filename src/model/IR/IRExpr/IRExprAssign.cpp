// Self first
#include "IRExprAssign.hpp"
#include "IRVisitor.hpp"

#include <cassert>

namespace IR {
  
	IRExprAssign::IRExprAssign(const std::shared_ptr<IRBlock>& aParentBlock)
  : IR::IRExprBinary(aParentBlock, IRExprType::IR_EXPR_ASSIGN)
  {
  }
  
  bool IRExprAssign::isa(const IRNode* aNode)
  {
    return aNode && IRExpr::isa(aNode) &&
    static_cast<const IRExpr*>(aNode)->getExprType() == IRExprType::IR_EXPR_ASSIGN;
  }//isa
  
  IRExprAssign* IRExprAssign::cast(IRNode* aNode)
  {
    if (!isa(aNode)) return nullptr;
    return static_cast<IRExprAssign*>(aNode);
  }//cast
  
  const IRExprAssign* IRExprAssign::cast(const IRNode* aNode)
  {
    if (!isa(aNode)) return nullptr;
    return static_cast<const IRExprAssign*>(aNode);
  }//cast
  
  void IRExprAssign::acceptVisitor(IRVisitor* aVisitor)
  {
    assert(aVisitor);
    aVisitor->visit(this);
  }//acceptVisitor
  
  bool IRExprAssign::isSemanticallyEqual(const IRExpr* aExpr) const
  {
    if(!isa(aExpr)) return false;
    auto assignExpr = cast(aExpr);
    
    auto lhsE = lhs();
    auto rhsE = rhs();
    
    if(!lhsE || !rhsE) return false;
    return lhsE->isEqual((assignExpr->lhs()).get()) &&
    rhsE->isEqual((assignExpr->rhs()).get());
  }//isSemanticallyEqual
  
}// end namespace IR
