// Self first
#include "IRExprBinary.hpp"

#include <cassert>

#define LHS_IDX 0
#define RHS_IDX 1

namespace IR {
  
	IRExprBinary::IRExprBinary(const std::shared_ptr<IRBlock>& aParentBlock, IRExprType aExprType)
	: IRExpr(aParentBlock, aExprType)
  {
  }
  
  void IRExprBinary::setLhs(const std::shared_ptr<IRExpr>& aLHS)
  {
    assert(aLHS);
    addChild(aLHS, LHS_IDX);
  }
  
  void IRExprBinary::setRhs(const std::shared_ptr<IRExpr>& aRHS)
  {
    assert(aRHS);
    addChild(aRHS, RHS_IDX);
  }//setRhs
  
  std::shared_ptr<IRExpr> IRExprBinary::lhs() const
  {
    assert(getStmtNode()->numChildren() > 0);
    auto lhsExpr = getChild(LHS_IDX);
    return std::static_pointer_cast<IRExpr>(lhsExpr);
  }//lhs
  
  std::shared_ptr<IRExpr> IRExprBinary::rhs() const
  {
    assert(getStmtNode()->numChildren() > 1);

    auto rhsExpr = getChild(RHS_IDX);
    return std::static_pointer_cast<IRExpr>(rhsExpr);
  }//rhs
  
}// end namespace IR
