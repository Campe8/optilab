// Self first
#include "IRExprMatrixSub.hpp"
#include "IRVisitor.hpp"
#include "IRUtils.hpp"
#include "IRSymbolConstInteger.hpp"

#include <cassert>

#define MAT_OBJ 0
#define SUB_OBJ 1

namespace IR {
  
  IRExprMatrixSub::IRExprMatrixSub(const std::shared_ptr<IRBlock>& aParentBlock)
  : IRExpr(aParentBlock, IRExprType::IR_EXPR_MAT_SUB)
  {
  }
  
  void IRExprMatrixSub::acceptVisitor(IRVisitor* aVisitor)
  {
    assert(aVisitor);
    aVisitor->visit(this);
  }//acceptVisitor
  
  void IRExprMatrixSub::setMatrix(const std::shared_ptr<IRExpr>& aMat)
  {
    assert(aMat);
    addChild(aMat, MAT_OBJ);
    
    auto tStrip = Utils::stripType(aMat->getType());
    assert(tStrip);
    
    // Set the type of the subscript as the base type
    // of the matrix
    setType(tStrip);
  }//setMatrix
  
  std::shared_ptr<IRExpr> IRExprMatrixSub::getMatrix() const
  {
    assert(getStmtNode()->numChildren() > 0);
    auto matExpr = getChild(MAT_OBJ);
    
    return std::static_pointer_cast<IRExpr>(matExpr);
  }//getMatrix
  
  void IRExprMatrixSub::setSubscript(const std::shared_ptr<IRExpr>& aSub)
  {
    assert(aSub);
    addChild(aSub, SUB_OBJ);
  }//setSubscript
  
  std::shared_ptr<IRExpr> IRExprMatrixSub::getSubscript() const
  {
    assert(getStmtNode()->numChildren() > 0);
    auto subExpr = getChild(SUB_OBJ);
    
    return std::static_pointer_cast<IRExpr>(subExpr);
  }//getSubscript
  
  bool IRExprMatrixSub::isSemanticallyEqual(const IRExpr* aExpr) const
  {
    // Pointer equivalence for the matrix
    if(!isa(aExpr)) return false;
    
    auto matSub = IRExprMatrixSub::cast(aExpr);
    if((getMatrix()).get() == (matSub->getMatrix()).get())
    {
      // Check subscript
      auto subscrComp = matSub->getSubscript();
      assert(IR::IRExprConst::isa(getSubscript().get()) && IR::IRExprConst::isa(subscrComp.get()));
      
      auto baseSymbSubscr = (IR::IRExprConst::cast(getSubscript().get())->getConst()).get();
      auto compSymbSubscr = (IR::IRExprConst::cast(subscrComp.get())->getConst()).get();
      assert(IRSymbolConstInteger::isa(baseSymbSubscr) && IRSymbolConstInteger::isa(compSymbSubscr));
      
      return IRSymbolConstInteger::cast(baseSymbSubscr)->getInt() ==
      IRSymbolConstInteger::cast(compSymbSubscr)->getInt();
    }
    
    return false;
  }//isSemanticallyEqual
  
}// end namespace IR

