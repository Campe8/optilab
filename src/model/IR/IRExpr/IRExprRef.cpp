// Self first
#include "IRExprRef.hpp"
#include "IRExprConst.hpp"
#include "IRExprVar.hpp"
#include "IRVisitor.hpp"

#include <cassert>

namespace IR {
  
	IRExprRef::IRExprRef(const std::shared_ptr<IRBlock>& aParentBlock)
	: IRExpr(aParentBlock, IRExprType::IR_EXPR_REF)
  {
  }
  
  bool IRExprRef::isa(const IRNode* aNode)
  {
    return aNode && IRExpr::isa(aNode) &&
    static_cast<const IRExpr*>(aNode)->getExprType() == IRExprType::IR_EXPR_REF;
  }//isa
  
  IRExprRef* IRExprRef::cast(IRNode* aNode)
  {
    if (!isa(aNode)) return nullptr;
    return static_cast<IRExprRef*>(aNode);
  }//cast
  
  const IRExprRef* IRExprRef::cast(const IRNode* aNode)
  {
    if (!isa(aNode)) return nullptr;
    return static_cast<const IRExprRef*>(aNode);
  }//cast
  
  void IRExprRef::acceptVisitor(IRVisitor* aVisitor)
  {
    assert(aVisitor);
    aVisitor->visit(this);
  }//acceptVisitor
  
	void IRExprRef::setReferenceExpr(const std::shared_ptr<IRExpr>& aRef)
	{
    assert(aRef);
    addChild(aRef, 0);
    
		setType(aRef->getType());
	}//setReferenceExpr

	std::shared_ptr<IRExpr> IRExprRef::getReferenceExpr() const
	{
    assert(getStmtNode()->numChildren() > 0);
    auto refExpr = getChild(0);
    
    return std::static_pointer_cast<IRExpr>(refExpr);
	}//getReferenceExpr
  
  bool IRExprRef::isSemanticallyEqual(const IRExpr* aExpr) const
  {
    if(!isa(aExpr)) return false;
    auto refExpr = cast(aExpr);
    
    return getReferenceExpr()->isEqual((refExpr->getReferenceExpr()).get());
  }//isSemanticallyEqual
  
  std::shared_ptr<IR::IRSymbol> getReferencedSymbol(const std::shared_ptr<IRExprRef>& aRef)
  {
    return getReferencedSymbol(aRef.get());
  }//getReferencedSymbol
  
  std::shared_ptr<IR::IRSymbol> getReferencedSymbol(const IRExprRef* aRef)
  {
    if(!aRef) { return nullptr; }
    auto ref = (aRef->getReferenceExpr()).get();
    if(IRExprConst::isa(ref))
    {
      return IRExprConst::cast(ref)->getConst();
    }
    assert(IRExprVar::isa(ref));
    return IRExprVar::cast(ref)->getVar();
  }//getReferencedSymbol
  
}// end namespace IR
