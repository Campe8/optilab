// Self first
#include "IRExprFcn.hpp"
#include "IRVisitor.hpp"
#include "IRExprConst.hpp"

#define FCN_CHILD 0
#define ARG_CHILD 1

namespace IR {
  
  IRExprFcn::IRExprFcn(const std::shared_ptr<IRBlock>& aParentBlock)
  : IRExpr(aParentBlock, IRExprType::IR_EXPR_FCN)
  , pNumArgs(0)
  {
  }
  
  void IRExprFcn::acceptVisitor(IRVisitor* aVisitor)
  {
    assert(aVisitor);
    aVisitor->visit(this);
  }//acceptVisitor
  
  void IRExprFcn::setFcn(IRExprSPtrCRef aFcn)
  {
    // Add first child: callee expr
    addChild(aFcn, FCN_CHILD);
    auto fcn = getFcn();
    
    // Set the type of this expression, i.e., a fcn type
    IRExpr::setType(fcn->getType());
    auto fcnType = std::static_pointer_cast<IRTypeFunction>(getType());

    assert(fcnType);
    pNumArgs = fcnType->getNumParams();
  }//setFcn
  
  IRExprFcn::IRExprSPtr IRExprFcn::fcn() const
  {
    assert(getStmtNode()->numChildren() > 0);
    auto childFcn = ((*getStmtNode())[FCN_CHILD]).data();
    
    assert(IRExpr::isa(childFcn.get()));
    return std::static_pointer_cast<IRExpr>(childFcn);
  }//fcn
  
  IRExprFcn::SymbFcnSPtr IRExprFcn::getFcn() const
  {
    auto f = fcn();
    assert(IRExprConst::isa(f.get()));
    
    auto fsymb = IRExprConst::cast(f.get())->getConst();
    assert(IRSymbolFcn::isa(fsymb.get()));
    
    return std::static_pointer_cast<IRSymbolFcn>(fsymb);
  }//setVar
  
  void IRExprFcn::setType(const std::shared_ptr<IRType>& aType)
  {
    assert(IRTypeFunction::isa(aType.get()));
    assert(!getFcn() || getType()->isEqual(aType.get()));
    IRExpr::setType(aType);
  }//setType
  
  std::shared_ptr<IRType> IRExprFcn::getOutputType() const
  {
    assert(getFcn());
    return std::static_pointer_cast<IRTypeFunction>(getType())->getReturnType();
  }//getOutputType
  
  std::shared_ptr<IRType> IRExprFcn::getArgType(std::size_t aIdx) const
  {
    assert(aIdx < numArgs());
    return std::static_pointer_cast<IRTypeFunction>(getType())->getParamType(aIdx);
  }//getArgType
  
  void IRExprFcn::setArg(std::size_t aIdx, IRExprSPtrCRef aArg)
  {
    assert(aArg);
    assert(aIdx < numArgs());
    
    // The type of "aArg" must be consistent with the
    // type of the "aIdx^th" argument of this fcn
    assert(aArg->getType()->isEqual(getArgType(aIdx).get()));
    addChild(aArg, ARG_CHILD + aIdx);
  }//setArg
  
  IRExprFcn::IRExprSPtr IRExprFcn::getArg(std::size_t aIdx) const
  {
    assert(aIdx < numArgs());
    assert(aIdx <= getStmtNode()->numChildren());
    
    auto node = ((*getStmtNode())[ARG_CHILD + aIdx]).data();
    assert(IRExpr::isa(node.get()));
    
    return std::static_pointer_cast<IRExpr>(node);
  }//getArg
  
}// end namespace IR

