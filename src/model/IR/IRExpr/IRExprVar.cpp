// Self first
#include "IRExprVar.hpp"

#include "IRVisitor.hpp"
#include "IRSrcDecl.hpp"

namespace IR {
  
  VarSearchSemantic::VarSearchSemantic(const SrcDeclSPtr& aSrc)
  : pSearchDecl(aSrc)
  , pReachable(false)
  , pInputOrder(0)
  , pBranchingPolicy(false)
  {
  }
  
  IRExprVar::IRExprVar(const std::shared_ptr<IRBlock>& aParentBlock)
  : IR::IRExprStdVar(aParentBlock)
  , pVarOutput(VarOutput::VO_DEFAULT)
  {
    setExprType(IRExprType::IR_EXPR_VAR);
  }
  
  bool IRExprVar::isa(const IRNode* aNode)
  {
    return aNode && IRExpr::isa(aNode) &&
    static_cast<const IRExpr*>(aNode)->getExprType() == IRExprType::IR_EXPR_VAR;
  }//isa
  
  IRExprVar* IRExprVar::cast(IRNode* aNode)
  {
    if (!isa(aNode)) return nullptr;
    return static_cast<IRExprVar*>(aNode);
  }//cast
  
  const IRExprVar* IRExprVar::cast(const IRNode* aNode)
  {
    if (!isa(aNode)) return nullptr;
    return static_cast<const IRExprVar*>(aNode);
  }//cast
  
  void IRExprVar::acceptVisitor(IRVisitor* aVisitor)
  {
    assert(aVisitor);
    aVisitor->visit(this);
  }
  
  IRExprVar::VarSearchSemanticSPtr IRExprVar::lookupSearchSemantic(const SrcDeclSPtr& aSrcDecl)
  {
    if(pSearchSemantics.size() == 0) return  nullptr;
    for(auto& ss : pSearchSemantics)
    {
      if(ss->getSearchDeclaration() == aSrcDecl) return ss;
    }
    return nullptr;
  }//lookupSearchSemantic
  
  void IRExprVar::addSearchSemantic(const VarSearchSemanticSPtr& aSrcSemantic)
  {
    assert(aSrcSemantic);
    pSearchSemantics.push_back(aSrcSemantic);
    
    // Add this variable as dependent on the given search semantic
    pSearchSemantics.back()->getSearchDeclaration()->addSemanticDependentVar(getVar()->getName());
  }//addSearchSemantic
  
  IRExprVar::VarSearchSemanticSPtr IRExprVar::getSearchSemantic(std::size_t aIdx) const
  {
    assert(aIdx < numSearchSemantics());
    return pSearchSemantics[aIdx];
  }//getSearchSemantic
  
}// end namespace IR
