// Self first
#include "IRExpr.hpp"

#include "IRModule.hpp"

namespace IR {
  
  IRExpr::IRExpr(const std::shared_ptr<IRBlock>& aParentBlock, IRExprType aExprType)
  : IRNode(aParentBlock, IRNodeType::IR_NODE_EXPR)
  , pExprType(aExprType)
  , pType(nullptr)
  {
  }

  bool IRExpr::isa(const IRNode* aNode)
  {
    return aNode != nullptr && aNode->getNodeType() == IRNodeType::IR_NODE_EXPR;
  }//isa
  
  IRExpr* IRExpr::cast(IRNode* aNode)
  {
    if (!isa(aNode)) return nullptr;
    return static_cast<IRExpr*>(aNode);
  }//cast
  
  const IRExpr* IRExpr::cast(const IRNode* aNode)
  {
    if (!isa(aNode)) return nullptr;
    return static_cast<const IRExpr*>(aNode);
  }//cast
  
  void IRExpr::setType(const std::shared_ptr<IRType>& aType)
  {
    pType = aType;
  }
  
  bool IRExpr::isEqual(const IRExpr* aExpr) const
  {
    if(!aExpr) return false;
    if(this == aExpr) return true;
    
    return isSemanticallyEqual(aExpr);
  }//isEqual
  
  MODEL_EXPORT_FUNCTION void linkExprToSymbol(const std::shared_ptr<IRSymbol>& aSymb, const std::shared_ptr<IRExpr>& aExpr)
  {
    if (!aSymb || !aExpr) { return; }
    auto st = aSymb->getModule()->getSymbolTable();
    assert(st);
    
    auto sentry = st->lookup(aSymb->getName());
    assert(sentry);
    
    sentry->addExprLink(aExpr);
  }//linkExprToSymbol
  
}// end namespace IR
