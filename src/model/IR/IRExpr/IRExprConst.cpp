// Self first
#include "IRExprConst.hpp"
#include "IRVisitor.hpp"

namespace IR {
  
	IRExprConst::IRExprConst(const std::shared_ptr<IRBlock>& aParentBlock)
	: IRExpr(aParentBlock, IRExprType::IR_EXPR_CONST)
  {
  }
  
  bool IRExprConst::isa(const IRNode* aNode)
  {
    return aNode != nullptr && IRExpr::isa(aNode) &&
    static_cast<const IRExpr*>(aNode)->getExprType() == IRExprType::IR_EXPR_CONST;
  }//isa
  
  IRExprConst* IRExprConst::cast(IRNode* aNode)
  {
    if (!isa(aNode)) return nullptr;
    return static_cast<IRExprConst*>(aNode);
  }//cast
  
  const IRExprConst* IRExprConst::cast(const IRNode* aNode)
  {
    if (!isa(aNode)) return nullptr;
    return static_cast<const IRExprConst*>(aNode);
  }//cast
  
  void IRExprConst::acceptVisitor(IRVisitor* aVisitor)
  {
    assert(aVisitor);
    aVisitor->visit(this);
  }//acceptVisitor
  
	void IRExprConst::setConst(SymbConstSPtrCRef aConst)
	{
		assert(aConst);
		addContainedObject(aConst);

		assert(aConst->getType());
		setType(aConst->getType());
	}//setConst

	IRExprConst::SymbConstSPtr IRExprConst::getConst() const
	{
		assert(numContainedObj() == 1);
		return std::static_pointer_cast<IRSymbolConst>(getContainedObj(0));
	}//getConst
  
  bool IRExprConst::isSemanticallyEqual(const IRExpr* aExpr) const
  {
    if(!isa(aExpr)) return false;
    auto exprConst = cast(aExpr);
    
    auto constSymb = getConst();
    if(!constSymb) return false;
    
    return constSymb->isEqual((exprConst->getConst()).get());
  }//isSemanticallyEqual
  
}// end namespace IR
