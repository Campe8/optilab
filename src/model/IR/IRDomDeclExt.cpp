// Self first
#include "IRDomDeclExt.hpp"
#include "IRVisitor.hpp"

#include <cassert>

namespace IR {

  IRDomDeclExt::IRDomDeclExt(const std::shared_ptr<IRBlock>& aParentBlock)
  : IRDomDecl(aParentBlock, DomDeclType::DOM_DECL_EXTENSIONAL)
  {
  }
  
  void IRDomDeclExt::acceptVisitor(IRVisitor* aVisitor)
  {
    assert(aVisitor);
    aVisitor->visit(this);
  }//acceptVisitor
  
  void IRDomDeclExt::addDomainConstant(const IRDomDecl::ExprConstSPtr& aConst)
  {
    assert(aConst);
    
    auto cname = aConst->getConst()->getName();
    if(pConstSet.find(cname) == pConstSet.end())
    {
      addConstant(aConst);
      pConstSet.insert(cname);
    }
  }//addDomainConstant
  
  std::set<IRDomDecl::ExprConstSPtr> IRDomDeclExt::getExtensionalDomain()
  {
    std::set<IRDomDecl::ExprConstSPtr> cset;
    for(std::size_t idx = 0; idx < getNumConstants(); ++idx)
    {
      cset.insert(getConstant(idx));
    }
    return cset;
  }//getExtensionalDomain
  
}// end namespace IR
