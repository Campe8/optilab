// Self first
#include "IRSymbolConstArray.hpp"
#include "IRVisitor.hpp"

#include "IRException.hpp"

#include <sstream>
#include <cassert>

namespace IR {
  
  // Utility function: returns the name of the (symbol const) array
  // according to its type
  static std::string getArraySymbolName(const std::string& aName, const std::shared_ptr<IRTypeArray>& aType)
  {
    /*
    if(false && !aName.empty())
    {
      // If "aName" is not empty, the symbol
      // is identified by it name and it gets
      // registered in the symbol table with "aName"
      return aName;
    }
    */
    
    assert(aType);
    std::stringstream ss;
    aType->prettyPrint(ss);
    
    auto name = ss.str();
    if(!aName.empty())
    {
      name = aName + ':' + name;
    }
    return name;
  }//getArraySymbolName
  
  IRSymbolConstArray::IRSymbolConstArray(const std::shared_ptr<IRModule>& aModule, const std::string& aName, const std::shared_ptr<IRTypeArray>& aType)
  : IRSymbolConstComposite(aModule, getArraySymbolName(aName, aType), aType)
  , pNumElements(0)
  {
    pNumElements = aType->getNumElements();
    
    // Initialize the container for symbols
    pArray.resize(pNumElements, nullptr);
  }
  
  void IRSymbolConstArray::acceptVisitor(IRVisitor* aVisitor)
  {
    assert(aVisitor);
    aVisitor->visit(this);
  }//acceptVisitor
  
  bool IRSymbolConstArray::isa(const IRSymbol* aSymb)
  {
    return IRSymbolConstComposite::isa(aSymb) &&
    IR::IRTypeArray::isa((aSymb->getType()).get());
  }//isa
  
  IRSymbolConstArray* IRSymbolConstArray::cast(IRSymbol* aSymb)
  {
    if (!isa(aSymb)) { return nullptr; }
    return static_cast<IRSymbolConstArray*>(aSymb);
  }//const
  
  const IRSymbolConstArray* IRSymbolConstArray::cast(const IRSymbol* aSymb)
  {
    if (!isa(aSymb)) { return nullptr; }
    return static_cast<const IRSymbolConstArray*>(aSymb);
  }//const
  
  std::shared_ptr<IRType> IRSymbolConstArray::getBaseType() const
  {
    return getType()->getBaseType();
  }//getBaseType
  
  void IRSymbolConstArray::addSymbol(const SymbPtr& aSymb, std::size_t aPos)
  {
    assert(aSymb);
    assert(aPos < size());
    
    auto stype = aSymb->getType();
    assert(stype->isEqual(getBaseType().get()));
    
    pArray[aPos] = aSymb;
  }//addSymbol
  
  IRSymbolConstArray::SymbPtr IRSymbolConstArray::getSymbol(std::size_t aPos) const
  {
    ir_assert(aPos < size());
    return pArray.at(aPos);
  }//getSymbol
  
  bool IRSymbolConstArray::isEqual(IRSymbol* aOther)
  {
    if(!isa(aOther)) return false;
    auto array = cast(aOther);
    
    if(size() != array->size()) return false;
    for(std::size_t idx{0}; idx < size(); ++idx)
    {
      if(!getSymbol(idx)->isEqual((array->getSymbol(idx)).get())) return false;
    }
    return true;
  }//isEqual
  
}// end namespace Model

