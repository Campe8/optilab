// Self first
#include "IRSymbolStruct.hpp"
#include "IRVisitor.hpp"

#include "BaseTools.hpp"
#include "IRException.hpp"

#include <sstream>
#include <cassert>

namespace IR {
  
  // Utility function: returns the name of the (symbol) struct
  // according to its type.
  // @note the name of the type is NOT the name of the struct!
  // @note the name of the variable holding the address where the
  // struct is stored is NOT the name of the struct either, i.e., given
  // Struct T;
  // T myStruct;
  // myStruct is NOT the name of the constant struct but is the name of the variable
  // which is a symbolVar.
  // The name of the constant struct should be the memory address where
  // the struct is located or, to be human readable, the pretty print of
  // the type concatenated with a random unique string
  // unless this is not a constant struct and the pretty print could be concatenated with
  // the name of the variable pointing to this struct
  static std::string getStructSymbolName(const std::shared_ptr<IRTypeStruct>& aType)
  {
    assert(aType);
    std::string name = aType->getName();
    return Base::Tools::generateRandomString() + '_' + name;
  }//getArraySymbolName
  
  IRSymbolStruct::IRSymbolStruct(const std::shared_ptr<IRModule>& aModule,
                                 const std::shared_ptr<IRTypeStruct>& aType)
  : IRSymbol(aModule, IRSymbolType::IR_SYMB_STRUCT, getStructSymbolName(aType), aType)
  {
  }
  
  bool IRSymbolStruct::isa(const IRSymbol* aSymb)
  {
    return aSymb &&
    aSymb->getSymbolType() == IRSymbolType::IR_SYMB_STRUCT &&
    IR::IRTypeStruct::isa((aSymb->getType()).get());
  }//isa
  
  IRSymbolStruct* IRSymbolStruct::cast(IRSymbol* aSymb)
  {
    if (!isa(aSymb)) return nullptr;
    return static_cast<IRSymbolStruct*>(aSymb);
  }//const
  
  const IRSymbolStruct* IRSymbolStruct::cast(const IRSymbol* aSymb)
  {
    if (!isa(aSymb)) return nullptr;
    return static_cast<const IRSymbolStruct*>(aSymb);
  }//const
  
  void IRSymbolStruct::acceptVisitor(IRVisitor* aVisitor)
  {
    assert(aVisitor);
    aVisitor->visit(this);
  }//acceptVisitor
  
  IRSymbolStruct::SymbPtr IRSymbolStruct::getMember(const std::string& aMember) const
  {
    if(pStructMap.find(aMember) == pStructMap.end()) return nullptr;
    return pStructMap.at(aMember);
  }//getMember
  
  void IRSymbolStruct::addMember(const std::string& aMember, const SymbPtr& aSymbol, std::size_t aIdx)
  {
    assert(!aMember.empty() && aSymbol);
    
    IRTypeStruct* type = static_cast<IRTypeStruct*>(getType().get());
    assert(type && aIdx < type->getNumElements());
    assert(aSymbol->getType()->isEqual(type->getElement(aIdx).get()));
    
    pStructMap[aMember] = aSymbol;
  }//addMember
  
  bool IRSymbolStruct::isEqual(IRSymbol* aOther)
  {
    if(!isa(aOther) || !getType()->isEqual(aOther->getType().get()) ) return false;
    
    auto structSymbol = cast(aOther);
    for(const auto& it : pStructMap)
    {
      if(!structSymbol->isMember(it.first)) return false;
      if(!it.second->isEqual(structSymbol->getMember(it.first).get())) return false;
    }
    return true;
  }//isEqual
  
}// end namespace Model
