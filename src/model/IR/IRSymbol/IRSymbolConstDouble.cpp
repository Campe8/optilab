// Self first
#include "IRSymbolConstDouble.hpp"

#include <sstream>
#include <string>

namespace IR {

  // Returns the string representing the given double always with decimal point.
  // For example:
  // 3.14 returns 3.14
  // 1    returns 1.0
  static std::string setDoubleString(const std::string& aDouble)
  {
    auto dbl = aDouble;
    if(aDouble.find_first_of(".") == std::string::npos)
    {
      dbl += ".0";
    }
    return dbl;
  }
  
	IRSymbolConstDouble::IRSymbolConstDouble(const std::shared_ptr<IRModule>& aModule,
                                           const std::string& aConst,
                                           const std::shared_ptr<IRTypeDouble>& aType)
	: IRSymbolConstNumeric(aModule, setDoubleString(aConst), aType)
	{
		std::stringstream ss(aConst);
		pSymbolDouble = 0;
		ss >> pSymbolDouble;
	}

}// end namespace IR
