// Self first
#include "IRSymbolConstInteger.hpp"

#include <sstream>

namespace IR {

	IRSymbolConstInteger::IRSymbolConstInteger(const std::shared_ptr<IRModule>& aModule,
																             const std::string& aConst,
                                             const std::shared_ptr<IRTypeInteger>& aType)
	: IRSymbolConstNumeric(aModule, aConst, aType)
	{
		std::stringstream ss(aConst);
		pSymbolInt = 0;
		ss >> pSymbolInt;
  }
  
  bool IRSymbolConstInteger::isa(const IRSymbol* aSymb)
  {
    return IRSymbolConstNumeric::isa(aSymb) &&
    IRTypeInteger::isa(aSymb->getType().get());
  }//isa
  
  bool IRSymbolConstInteger::isa(const std::shared_ptr<IRSymbol>& aSymb)
  {
    return IRSymbolConstNumeric::isa(aSymb.get()) &&
    IRTypeInteger::isa(aSymb->getType().get());
  }//isa
  
  IRSymbolConstInteger* IRSymbolConstInteger::cast(IRSymbol* aSymb)
  {
    if (!isa(aSymb)) { return nullptr; }
    return static_cast<IRSymbolConstInteger*>(aSymb);
  }//cast
  
  const IRSymbolConstInteger* IRSymbolConstInteger::cast(const IRSymbol* aSymb)
  {
    if (!isa(aSymb)) { return nullptr; }
    return static_cast<const IRSymbolConstInteger*>(aSymb);
  }//cast
  
  std::shared_ptr<IRSymbolConstInteger> IRSymbolConstInteger::cast(const std::shared_ptr<IRSymbol>& aSymb)
  {
    if (!isa(aSymb)) { return nullptr; }
    return std::static_pointer_cast<IRSymbolConstInteger>(aSymb);
  }//cast
  
}// end namespace Model
