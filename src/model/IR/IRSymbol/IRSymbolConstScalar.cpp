// Self first
#include "IRSymbolConstScalar.hpp"

namespace IR {

	IRSymbolConstScalar::IRSymbolConstScalar(const std::shared_ptr<IRModule>& aModule,
																           const std::string& aConst,
                                           const std::shared_ptr<IRTypeScalar>& aType)
	: IRSymbolConst(aModule, aConst, aType)
	{
	}
  
  bool IRSymbolConstScalar::isa(const IRSymbol* aSymb)
  {
    return IRSymbolConst::isa(aSymb) && 
    IRTypeScalar::isa(aSymb->getType().get());
  }//isa
  
  IRSymbolConstScalar* IRSymbolConstScalar::cast(IRSymbol* aSymb)
  {
    if(!isa(aSymb)) return nullptr;
    return static_cast<IRSymbolConstScalar*>(aSymb);
  }//cast
  
  const IRSymbolConstScalar* IRSymbolConstScalar::cast(const IRSymbol* aSymb)
  {
    if(!isa(aSymb)) return nullptr;
    return static_cast<const IRSymbolConstScalar*>(aSymb);
  }//cast

}// end namespace Model
