// Self first
#include "IRSymbolConstComposite.hpp"

namespace IR {

	IRSymbolConstComposite::IRSymbolConstComposite(const std::shared_ptr<IRModule>& aModule,
																                 const std::string& aConst,
                                                 const std::shared_ptr<IRTypeComposite>& aType)
	: IRSymbolConst(aModule, aConst, aType)
	{
	}

  bool IRSymbolConstComposite::isa(const IRSymbol* aSymb)
  {
    return IRSymbolConst::isa(aSymb) &&
    IRTypeComposite::isa((aSymb->getType()).get());
  }//isa

}// end namespace Model
