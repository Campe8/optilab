// Self first
#include "IRSymbolConstBoolean.hpp"

#include <sstream>

namespace IR {

  static std::string boolToString(bool aBool)
  {
    return aBool ? "true" : "false";
  }
  
	IRSymbolConstBoolean::IRSymbolConstBoolean(const std::shared_ptr<IRModule>& aModule, bool aBool, const std::shared_ptr<IRTypeBoolean>& aType)
  : IRSymbolConstNumeric(aModule, boolToString(aBool), aType)
  , pSymbolBoolean(aBool)
	{
	}

  bool IRSymbolConstBoolean::isa(const IRSymbol* aSymb)
  {
    return aSymb &&
    IRTypeBoolean::isa(aSymb->getType().get());
  }//isa
  
  bool IRSymbolConstBoolean::isa(const std::shared_ptr<IRSymbol>& aSymb)
  {
    return aSymb &&
    IRTypeBoolean::isa(aSymb->getType().get());
  }//isa
  
  IRSymbolConstBoolean* IRSymbolConstBoolean::cast(IRSymbol* aSymb)
  {
    if (!isa(aSymb)) return nullptr;
    return static_cast<IRSymbolConstBoolean*>(aSymb);
  }//cast
  
  const IRSymbolConstBoolean* IRSymbolConstBoolean::cast(const IRSymbol* aSymb)
  {
    if (!isa(aSymb)) return nullptr;
    return static_cast<const IRSymbolConstBoolean*>(aSymb);
  }//cast
  
  std::shared_ptr<IRSymbolConstBoolean> IRSymbolConstBoolean::cast(const std::shared_ptr<IRSymbol>& aSymb)
  {
    if (!isa(aSymb)) return nullptr;
    return std::static_pointer_cast<IRSymbolConstBoolean>(aSymb);
  }//cast
  
}// end namespace IR
