// Self first
#include "IRSymbolFcn.hpp"

namespace IR {
  
  IRSymbolFcn::IRSymbolFcn(const std::shared_ptr<IRModule>& aModule,
                           const std::string& aFcnName,
                           const std::shared_ptr<IRTypeFunction>& aFcnType)
  : IRSymbolConst(aModule, aFcnName, aFcnType)
  , pType(aFcnType)
  {
  }
  
}// end namespace Model

