// Self first
#include "IRSymbolConstNumeric.hpp"

namespace IR {

	IRSymbolConstNumeric::IRSymbolConstNumeric(const std::shared_ptr<IRModule>& aModule,
																             const std::string& aConst,
                                             const std::shared_ptr<IRTypeNumeric>& aType)
	: IRSymbolConstScalar(aModule, aConst, aType)
	{
	}

  bool IRSymbolConstNumeric::isa(const IRSymbol* aSymb)
  {
    return IRSymbolConstScalar::isa(aSymb) &&
    IRTypeNumeric::isa((aSymb->getType()).get());
  }//isa
  
  IRSymbolConstNumeric* IRSymbolConstNumeric::cast(IRSymbol* aSymb)
  {
    if(!isa(aSymb)) return nullptr;
    return static_cast<IRSymbolConstNumeric*>(aSymb);
  }//cast
  
  const IRSymbolConstNumeric* IRSymbolConstNumeric::cast(const IRSymbol* aSymb)
  {
    if(!isa(aSymb)) return nullptr;
    return static_cast<const IRSymbolConstNumeric*>(aSymb);
  }//cast
  
}// end namespace Model
