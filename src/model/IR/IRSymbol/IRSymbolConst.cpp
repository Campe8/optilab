// Self first
#include "IRSymbolConst.hpp"

namespace IR {

	IRSymbolConst::IRSymbolConst(const std::shared_ptr<IRModule>& aModule,
                               const std::string& aConst,
                               const std::shared_ptr<IRType>& aConstType)
	: IRSymbol(aModule, IRSymbolType::IR_SYMB_CONST, aConst, aConstType)
	, pType(aConstType)
	{
	}
  
  bool IRSymbolConst::isa(const IRSymbol* aSymb)
  {
    return aSymb &&
    aSymb->getSymbolType() == IRSymbolType::IR_SYMB_CONST;
  }

}// end namespace Model
