// Self first
#include "IRSymbolVar.hpp"

namespace IR {
  
  IRSymbolVar::IRSymbolVar(const std::shared_ptr<IRModule>& aModule,
                           const std::string& aVarName,
                           const std::shared_ptr<IRType>& aVarType)
  : IRSymbol(aModule, IRSymbolType::IR_SYMB_VAR, aVarName, aVarType)
  , pType(aVarType)
  {
  }
  
  bool IRSymbolVar::isa(const IRSymbol* aSymb)
  {
    return aSymb &&
    aSymb->getSymbolType() == IRSymbolType::IR_SYMB_VAR;
  }//isa
  
  IRSymbolVar* IRSymbolVar::cast(IRSymbol* aSymb)
  {
    if (isa(aSymb))
    {
      return static_cast<IRSymbolVar*>(aSymb);
    }
    return nullptr;
  }//cast
  
  const IRSymbolVar* IRSymbolVar::cast(const IRSymbol* aSymb)
  {
    if (isa(aSymb))
    {
      return static_cast<const IRSymbolVar*>(aSymb);
    }
    return nullptr;
  }//cast
  
  void IRSymbolVar::setType(const std::shared_ptr<IRType>& aVarType)
  {
    pType = aVarType;
    registerType(pType);
  }
  
}// end namespace Model
