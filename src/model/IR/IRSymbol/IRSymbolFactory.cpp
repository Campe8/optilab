// Self first
#include "IRSymbolFactory.hpp"

#include "IRContext.hpp"
#include "CLIUtils.hpp"

#include <cassert>

#define ADD_SYMBOL_TO_TABLE(aSymb) addSymbolToSymbolTable(aSymb, getModule());

namespace cutils = CLI::Utils;

namespace IR {
  
  // Adds the pointer to the symbol in the correspondent entry of the symbol table
  static void addSymbolToSymbolTable(const IRSymbolFactory::IRSymbolPtr& aSymbol, const IRModuleSPtr& aModule)
  {
    auto symbolName = aSymbol->getName();
    auto entry = aModule->getSymbolTable()->lookup(symbolName);
    assert(entry);
    
    entry->addSymbol(aSymbol);
  }//addSymbolToSymbolTable
  
	IRSymbolFactory::IRSymbolFactory()
  : pIRModule(nullptr)
	{
	}

	IRModuleSPtr IRSymbolFactory::setModule(const IRModuleSPtr& aMod)
	{
		auto mod = pIRModule;
		pIRModule = aMod;
		return mod;
	}

	std::shared_ptr<IRTypeFactory> IRSymbolFactory::getTypeFactory() const
	{
		assert(getModule());
		return getModule()->getContext()->typeFactory();
	}//getTypeFactory

	IRSymbolConstIntegerSPtr IRSymbolFactory::symbolConstInteger(INT_64_T aInt)
	{
    auto sint = cutils::toString<INT_64_T>(aInt);
		auto intType = getTypeFactory()->typeInteger64();
		auto symb = IRSymbolConstIntegerSPtr(new IRSymbolConstInteger(getModule(), sint, intType));
    ADD_SYMBOL_TO_TABLE(symb);

    return symb;
	}//symbolConstInteger
  
  IRSymbolConstDoubleSPtr IRSymbolFactory::symbolConstDouble(REAL_T aDouble)
	{
		auto sdouble = cutils::toString<REAL_T>(aDouble);
		auto doubleType = getTypeFactory()->typeDouble();
		auto symb = IRSymbolConstDoubleSPtr(new IRSymbolConstDouble(getModule(), sdouble, doubleType));
    ADD_SYMBOL_TO_TABLE(symb);
    
		return symb;
	}//symbolConstDouble

  IRSymbolConstBooleanSPtr IRSymbolFactory::symbolConstBoolean(BOOL_T aBool)
	{
		auto boolType = getTypeFactory()->typeBoolean();
		auto symb = IRSymbolConstBooleanSPtr(new IRSymbolConstBoolean(getModule(), aBool, boolType));
    ADD_SYMBOL_TO_TABLE(symb);
    
		return symb;
	}//symbolConstBoolean

  IRSymbolFcnSPtr IRSymbolFactory::symbolFcn(const std::string& aFcnName, const IRTypeFunctionSPtr& aFcnType)
  {
    auto symb = IRSymbolFcnSPtr(new IRSymbolFcn(getModule(), aFcnName, aFcnType));
    ADD_SYMBOL_TO_TABLE(symb);
    
    return symb;
  }//symbolFcn
  
	IRSymbolVarSPtr IRSymbolFactory::symbolVar(const std::string& aName, const IRTypeSPtr& aType)
	{
		auto symb = IRSymbolVarSPtr(new IRSymbolVar(getModule(), aName, aType));
    ADD_SYMBOL_TO_TABLE(symb);
    
		return symb;
	}//symbolVar
  
  IRSymbolVarSPtr IRSymbolFactory::symbolVarInt(const std::string& aName)
  {
    return symbolVar(aName, getTypeFactory()->typeInteger64());
  }//symbolVarInt

  IRSymbolVarSPtr IRSymbolFactory::symbolVarDouble(const std::string& aName)
  {
    return symbolVar(aName, getTypeFactory()->typeDouble());
  }//symbolVarInt
  
  IRSymbolVarSPtr IRSymbolFactory::symbolVarBool(const std::string& aName)
  {
    return symbolVar(aName, getTypeFactory()->typeBoolean());
  }//symbolVarInt
  
  IRSymbolConstArraySPtr IRSymbolFactory::symbolConstArray(const std::vector<INT_64_T>& aIntArray, const std::string& aName)
  {
    auto intT = getTypeFactory()->typeInteger64();
    auto arrT = getTypeFactory()->typeArray(aIntArray.size(), intT);
    
    IRSymbolConstArraySPtr carray(new IRSymbolConstArray(getModule(), aName, arrT));
    
    std::size_t pos{ 0 };
    for (auto e : aIntArray)
    {
      carray->addSymbol(symbolConstInteger(e), pos++);
    }
    ADD_SYMBOL_TO_TABLE(carray);
    
    return carray;
  }//symbolConstArray
  
  IRSymbolConstArraySPtr IRSymbolFactory::symbolConstArray(const std::vector<IRSymbolPtr>& aArray, const std::string& aName)
  {
    std::shared_ptr<IRType> atype{ nullptr };
    if (!aArray.empty())
    {
      atype = aArray[0]->getType();
    }
    else
    {
      atype = pIRModule->getContext()->typeFactory()->typeVoid();
    }
    assert(atype);
    
    auto arrT = getTypeFactory()->typeArray(aArray.size(), atype);
    IRSymbolConstArraySPtr carray(new IRSymbolConstArray(getModule(), aName, arrT));
    
    std::size_t pos{ 0 };
    for (auto e : aArray)
    {
      carray->addSymbol(e, pos++);
    }
    ADD_SYMBOL_TO_TABLE(carray);
    
    return carray;
  }//symbolConstArray
  
  IRSymbolStructSPtr IRSymbolFactory::symbolStruct(const std::string& aStructName,
                                                   const std::vector<std::pair<std::string, IRSymbolPtr>>& aMembers)
  {
    assert(!aStructName.empty());
    
    std::vector<IRTypeSPtr> types;
    for(const auto& mpair : aMembers)
    {
      types.push_back(mpair.second->getType());
    }
    auto structType = getTypeFactory()->typeStruct(aStructName, types);
    IRSymbolStructSPtr structSymb(new IRSymbolStruct(getModule(), structType));
    
    // Add the members to the struct
    std::size_t idx{0};
    for(const auto& mpair : aMembers)
    {
      structSymb->addMember(mpair.first, mpair.second, idx++);
    }
    ADD_SYMBOL_TO_TABLE(structSymb);
    
    return structSymb;
  }//symbolStruct
  
}// end namespace IR
