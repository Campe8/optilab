// Self first
#include "IRSymbol.hpp"

#include "IRModule.hpp"
#include "IRVisitor.hpp"

// Symbol table
#include "SymbolTableEntry.hpp"
#include "SymbolTable.hpp"

namespace IR {
  
  static void updateSymbolMap(const std::shared_ptr<IRModule>& aModule, const IRSymbol::SymbolName& aName, IRSymbolType aType)
  {
    auto stEntry = std::make_shared<SymbolTableEntry>(aName, aType);
    aModule->getSymbolTable()->insert(stEntry);
  }//updateSymbolMap
  
  IRSymbol::IRSymbol(const std::shared_ptr<IRModule>& aModule, IRSymbolType aSymbType, const SymbolName& aSymbolName)
  : IRObject(IRObjectType::IR_OBJ_SYMBOL)
  , pSymbType(aSymbType)
  , pName(aSymbolName)
  , pModule(aModule)
  {
    // Register this symbol in the module's symbol table
    assert(pModule);
    updateSymbolMap(pModule, pName, pSymbType);
  }
  
  IRSymbol::IRSymbol(const std::shared_ptr<IRModule>& aModule,
                     IRSymbolType aSymbType,
                     const SymbolName& aSymbolName,
                     const std::shared_ptr<IRType>& aType)
  : IRSymbol(aModule, aSymbType, aSymbolName)
  {
    registerType(aType);
  }
  
  void IRSymbol::acceptVisitor(IRVisitor* aVisitor)
  {
    assert(aVisitor);
    aVisitor->visit(this);
  }//acceptVisitor
  
  bool IRSymbol::isGlobal() const
  {
    // Get the symbol table containing this symbol
    // The symbol is a global symbol if belongs to the
    // global symbol table
    return getModule()->getSymbolTable()->isGlobal();
  }
  
  bool IRSymbol::isEqual(IRSymbol* aOther)
  {
    if (!aOther) { return false; }
    if (aOther->getSymbolType() == pSymbType && aOther->getName() == pName) { return true; }
    return false;
  }//isEqual
  
  void IRSymbol::setModule(const std::shared_ptr<IRModule>& aModule)
  {
    assert(aModule);
    
    // Find symbol in symbol table
    auto st = pModule->getSymbolTable();
    auto sKey = SymbolTableEntry::getKeyFromName(getName());
    assert(st->contains(sKey));
    
    // Get entry
    auto sEntry = st->lookup(getName());
    
    // Delete symbol from symbol table
    st->erase(sKey);
    
    // Update module and symbol table
    pModule = aModule;
    pModule->getSymbolTable()->insert(sEntry);
  }//setModule
  
  void IRSymbol::registerType(const std::shared_ptr<IRType>& aType)
  {
    auto st = pModule->getSymbolTable();
    auto sKey = SymbolTableEntry::getKeyFromName(getName());
    assert(st->contains(sKey));
    
    st->lookup(getName())->setType(aType);
  }//registerType
  
  void IRSymbol::unlinkType() const
  {
    auto st = pModule->getSymbolTable();
    auto sKey = SymbolTableEntry::getKeyFromName(getName());
    assert(st->contains(sKey));
    
    st->lookup(getName())->setType(nullptr);
  }//unlinkType
  
  std::shared_ptr<IRType> IRSymbol::getType() const
  {
    auto st = pModule->getSymbolTable();
    auto sKey = SymbolTableEntry::getKeyFromName(getName());
    assert(st->contains(sKey));
    
    return st->lookup(getName())->getType();
  }//getType
  
}// end namespace IR
