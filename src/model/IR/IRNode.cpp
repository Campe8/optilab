// Self first
#include "IRNode.hpp"

#include <cassert>

namespace IR {
  
  IRNode::IRStmtNodeSPtr IRNode::StmtNode(const IRNode::IRNodeSPtr& aNode)
  {
    auto stmtNode = aNode->getStmtNode();
    if(!stmtNode)
    {
      stmtNode = std::make_shared<IRStmtNode>(aNode);
      aNode->pStmtNode = stmtNode;
    }
    
    assert(stmtNode);
    return stmtNode;
  }//StmtNode
  
  IRNode::IRNode(const std::shared_ptr<IRBlock>& aParentBlock, IRNodeType aNodeType)
  : IRObject(IRObjectType::IR_OBJ_NODE)
  , pNodeType(aNodeType)
  , pParent(nullptr)
  , pStmtNode(nullptr)
  , pBlock(aParentBlock)
  {
    assert(aParentBlock);
  }
  
  void IRNode::setBlock(const std::shared_ptr<IRBlock>& aBlock)
  {
    pBlock = aBlock;
  }//setBlock
  
  void IRNode::addContainedObject(const std::shared_ptr<IRObject>& aObj, int aIdx)
  {
    if (aIdx < 0)
    {
      pContainedObj.push_back(aObj);
    }
    else
    {
      std::size_t idx = static_cast<std::size_t>(aIdx);
      if (idx >= pContainedObj.size())
      {
        pContainedObj.resize(idx + 1, std::shared_ptr<IRObject>(nullptr));
      }
      pContainedObj[aIdx] = aObj;
    }
  }//addContainedObject
  
  std::shared_ptr<IRObject> IRNode::getContainedObj(std::size_t aIdx) const
  {
    if (aIdx > numContainedObj())
    {
      return std::shared_ptr<IRObject>(nullptr);
    }
    
    return pContainedObj.at(aIdx);
  }
  
  void IRNode::addChild(const std::shared_ptr<IRNode>& aNode, std::size_t aIdx)
  {
    assert(aNode);
    assert(aNode->getStmtNode());
    
    // Add the given node as a child to this stmt node
    auto parentStmtNode = getStmtNode();
    auto childStmtNode = aNode->getStmtNode();
    assert(parentStmtNode);
    
    auto numChildren = parentStmtNode->numChildren();
    if (aIdx == numChildren)
    {
      parentStmtNode->addChild(*childStmtNode);
    }
    else if(aIdx < numChildren)
    {
      parentStmtNode->replaceChild(*childStmtNode, aIdx);
    }
    else
    {
      assert(aIdx > numChildren);
      
      // Insert dummy children
      for (std::size_t ctr{ 0 }; ctr < (aIdx - numChildren); ++ctr)
      {
        parentStmtNode->addChild(nullptr);
      }
      parentStmtNode->addChild(*childStmtNode);
    }
  }//addChild
  
  std::shared_ptr<IRNode> IRNode::getChild(std::size_t aIdx) const
  {
    auto stmtNode = getStmtNode();
    assert(aIdx < stmtNode->numChildren());
    return  ((*stmtNode)[aIdx]).data();
  }//getChild
  
}// end namespace IR

