// Self first
#include "IRModule.hpp"

#include "IRContext.hpp"
#include "IRVisitor.hpp"

#include <cassert>

namespace IR {
  
  static void setSymbolTableParent(SymbolTable::SymbTablePtrConstRef aSymbTable, const IRModuleSPtr& aParent)
  {
    assert(aSymbTable);
    assert(aParent);
    aSymbTable->setParent(aParent->getSymbolTable());
  }
  
  static IRModuleSPtr getSubModuleByName(const std::string& aName, const IRModuleSPtr& aMod)
  {
    assert(aMod);
    if (aMod->getName() == aName)
    {
      return aMod;
    }
    
    auto it = aMod->begin();
    for (; it != aMod->end(); ++it)
    {
      if (getSubModuleByName(aName, *it))
      {
        return *it;
      }
    }
    return IRModuleSPtr(nullptr);
  }//getSubModuleByName
  
  IRModule::ModulePreOrderIter IRModule::preOrderBegin(const IRModuleSPtr& aModule)
  {
    assert(aModule);
    
    IRModule::ModulePreOrderIter it(aModule);
    return it;
  }//preOrderBegin
  
  IRModule::ModulePreOrderIter IRModule::preOrderEnd()
  {
    IRModule::ModulePreOrderIter it;
    return it;
  }//preOrderEnd
  
  IRModule::IRModule(const IRContextSPtr& aCtx, const std::string& aModuleName, const IRModuleSPtr& aParent)
		: IRObject(IRObjectType::IR_OBJ_MODULE)
		, pModuleName(aModuleName)
    , pContext(aCtx)
		, pParent(aParent)
		, pSymbTable(std::make_shared<SymbolTable>())
  {
    // If parent is not nullptr, set parent into symbol table
    if (pParent)
    {
      setSymbolTableParent(pSymbTable, pParent);
    }
  }
  
  void IRModule::acceptVisitor(IRVisitor* aVisitor)
  {
    assert(aVisitor);
    aVisitor->visit(this);
  }
  
  void IRModule::addChild(const IRModuleSPtr& aChild)
  {
    assert(aChild);
    pChildren.push_back(aChild);
  }//addChild
  
  bool IRModule::contains(const IRModuleSPtr& aChild) const
  {
    auto c = aChild;
    while (c)
    {
      if (c.get() == this)
      {
        return true;
      }
      c = c->getParent();
    }
    return false;
  }//contains
  
  bool IRModule::contains(const std::string& aChildName) const
  {
    if (aChildName == getName())
    {
      return true;
    }
    for (auto& c : pChildren)
    {
      if (c->contains(aChildName))
      {
        return true;
      }
    }
    return false;
  }//contains
  
  IRModuleSPtr IRModule::getSubModule(const std::string& aName) const
  {
    for (auto& c : pChildren)
    {
      if (getSubModuleByName(aName, c))
      {
        return c;
      }
    }
    return IRModuleSPtr(nullptr);
  }//getSubModule
  
  IRModule::ModuleIter IRModule::begin()
  {
    return pChildren.begin();
  }//begin
  
  IRModule::ModuleCIter IRModule::begin() const
  {
    return pChildren.begin();
  }//begin
  
  IRModule::ModuleIter IRModule::end()
  {
    return pChildren.end();
  }//end
  
  IRModule::ModuleCIter IRModule::end() const
  {
    return pChildren.end();
  }//end
  
  IRModule::ModuleRIter IRModule::rbegin()
  {
    return pChildren.rbegin();
  }//rbegin
  
  IRModule::ModuleCRIter IRModule::rbegin() const
  {
    return pChildren.rbegin();
  }//rbegin
  
  IRModule::ModuleRIter IRModule::rend()
  {
    return pChildren.rend();
  }//rend
  
  IRModule::ModuleCRIter IRModule::rend() const
  {
    return pChildren.rend();
  }//rend
  
  MODEL_EXPORT_FUNCTION SymbolTable::SymbolTableEntrySPtr lookupSymbol(const IRModuleSPtr& aMod, const IRSymbol::SymbolName& aSymb)
  {
    if (!aMod) { return SymbolTable::SymbolTableEntrySPtr(nullptr); }
    auto st = aMod->getSymbolTable();
    
    SymbolTable::SymbolTableEntrySPtr e(nullptr);
    while (st)
    {
      e = st->lookup(aSymb);
      if (e) { return e; }
      st = st->getParent();
    }
    
    return e;
  }//lookupSymbol
  
  MODEL_EXPORT_FUNCTION bool isLeafModule(const IRModuleSPtr& aMod)
  {
    if (!aMod) { return true; }
    for (IRModuleSPtr& m : *aMod)
    {
      if (m) { return false; }
    }
    return true;
  }//isLeafModule
  
  MODEL_EXPORT_FUNCTION std::vector<IRModuleSPtr> getLeaves(const IRModuleSPtr& aMod)
  {
    std::vector<IRModuleSPtr> v;
    
    if (!aMod) { return v; }
    if (aMod->numChildren() == 0)
    {
      v.push_back(aMod);
      return v;
    }
    
    for (IRModuleSPtr& m : *aMod)
    {
      if (isLeafModule(m) && m)
      {
        v.push_back(m);
      }
      else if (m)
      {
        auto v1 = getLeaves(m);
        if (!v1.empty())
        {
          v.insert(v.end(), v1.begin(), v1.end());
        }
      }
    }
    return v;
  }//getLeaves
  
  MODEL_EXPORT_FUNCTION bool hasEmptyBlock(const IRModuleSPtr& aMod)
  {
    if (!aMod) { return true; }
    return aMod->getBlock() && aMod->getBlock()->isEmpty();
  }//hasEmptyBlock

}// end namespace Model
