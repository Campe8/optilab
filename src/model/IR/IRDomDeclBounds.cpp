// Self first
#include "IRDomDeclBounds.hpp"
#include "IRVisitor.hpp"

#include <cassert>

namespace IR {

  IRDomDeclBounds::IRDomDeclBounds(const std::shared_ptr<IRBlock>& aParentBlock)
  : IRDomDecl(aParentBlock, DomDeclType::DOM_DECL_BOUNDS)
  , pBoolean(false)
  {
  }
  
  void IRDomDeclBounds::acceptVisitor(IRVisitor* aVisitor)
  {
    assert(aVisitor);
    aVisitor->visit(this);
  }//acceptVisitor
  
  void IRDomDeclBounds::setBounds(const IRDomDecl::ExprConstSPtr& aLB, const IRDomDecl::ExprConstSPtr& aUB)
  {
    assert(aLB);
    assert(aUB);
    
    // Clear previous constants
    clearConstants();
    addConstant(aLB);
    addConstant(aUB);
  }
  
  IRDomDecl::ExprConstSPtr IRDomDeclBounds::getLowerBound() const
  {
    if(getNumConstants() != 2)
    {
      return ExprConstSPtr(nullptr);
    }
    return getConstant(0);
  }//getLowerBound
  
  IRDomDecl::ExprConstSPtr IRDomDeclBounds::getUpperBound() const
  {
    if(getNumConstants() != 2)
    {
      return ExprConstSPtr(nullptr);
    }
    return getConstant(1);
  }//getUpperBound
  
}// end namespace IR
