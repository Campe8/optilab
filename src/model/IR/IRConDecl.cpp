// Self first
#include "IRConDecl.hpp"
#include "IRVisitor.hpp"

#include <cassert>

namespace IR {
  
  IRConDecl::IRConDecl(const std::shared_ptr<IRBlock>& aParentBlock)
  : IR::IRNode(aParentBlock, IRNodeType::IR_NODE_CON_DECL)
  , pPropType(PropagationType::PT_BOUNDS)
  , pID("")
  {
  }
  
  void IRConDecl::acceptVisitor(IRVisitor* aVisitor)
  {
    assert(aVisitor);
    aVisitor->visit(this);
  }
  
  void IRConDecl::setFcnCon(const std::shared_ptr<IRExprFcn>& aFcn)
  {
    assert(aFcn);
    addChild(aFcn, 0);
  }//setFcnCon
  
  std::shared_ptr<IRExprFcn> IRConDecl::fcnCon() const
  {
    assert(getStmtNode()->numChildren() > 0);
    auto fcnExpr = getChild(0);
    return std::static_pointer_cast<IRExprFcn>(fcnExpr);
  }//fcnCon
  
}// end namespace IR
