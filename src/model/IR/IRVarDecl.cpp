// Self first
#include "IRVarDecl.hpp"
#include "IRVisitor.hpp"

#include <cassert>

#define LHS_IDX 0
#define RHS_IDX 1

namespace IR {
  
  IRVarDecl::IRVarDecl(const std::shared_ptr<IRBlock>& aParentBlock)
  : IR::IRNode(aParentBlock, IRNodeType::IR_NODE_VAR_DECL)
  {
  }
  
  bool IRVarDecl::isa(const IRNode* aNode)
  {
    return aNode != nullptr && aNode->getNodeType() == IRNodeType::IR_NODE_VAR_DECL;
  }//isa
  
  IRVarDecl* IRVarDecl::cast(IRNode* aNode)
  {
    if (!isa(aNode)) return nullptr;
    return static_cast<IRVarDecl*>(aNode);
  }//cast
  
  const IRVarDecl* IRVarDecl::cast(const IRNode* aNode)
  {
    if (!isa(aNode)) return nullptr;
    return static_cast<const IRVarDecl*>(aNode);
  }//cast
  
  void IRVarDecl::acceptVisitor(IRVisitor* aVisitor)
  {
    assert(aVisitor);
    aVisitor->visit(this);
  }
  
  void IRVarDecl::setLhs(const std::shared_ptr<IRExprVar>& aVar)
  {
    assert(aVar);
    addChild(aVar, LHS_IDX);
  }
  
  void IRVarDecl::setRhs(const std::vector<std::shared_ptr<IRDomDecl>>& aDom)
  {
    assert(!aDom.empty());
    
    std::size_t idx{0};
    for(auto& d : aDom)
    {
      addChild(d, RHS_IDX + idx++);
    }
  }//setRhs
  
  std::size_t IRVarDecl::getNumRhs() const
  {
    assert(lhs());
    return getStmtNode()->numChildren() - 1;
  }//getNumRhs
  
  std::shared_ptr<IRExprVar> IRVarDecl::lhs() const
  {
    assert(getStmtNode()->numChildren() > 0);
    auto lhsExpr = getChild(LHS_IDX);
    return std::static_pointer_cast<IRExprVar>(lhsExpr);
  }//lhs
  
  std::shared_ptr<IRDomDecl> IRVarDecl::rhs(std::size_t aIdx) const
  {
    assert(getStmtNode()->numChildren() > 1);
    assert(aIdx < getNumRhs());
    
    auto rhsExpr = getChild(RHS_IDX + aIdx);
    return std::static_pointer_cast<IRDomDecl>(rhsExpr);
  }//rhs
  
}// end namespace IR
