// Self first
#include "IRConstructionFacade.hpp"

#include "IRContext.hpp"
#include "IRUtils.hpp"

#include <cassert>

#define DEF_STMT_NODE(aNode) IRNode::StmtNode(aNode)

namespace IR {
  
  static void addExprLinkToSymbolTableEntry(const std::shared_ptr<IRExpr>& aExpr, const std::shared_ptr<IRSymbol>& aSymb)
  {
    if (!aExpr || !aSymb) { return; }
    assert(aSymb->getModule());
    
    auto st = aSymb->getModule()->getSymbolTable();
    assert(st);
    
    auto entry = st->lookup(aSymb->getName());
    assert(entry);
    
    // Only one linked expression is allowed for each symbol table entry
    entry->removeAllExprLinks();

    assert(aExpr->getType());
    entry->addExprLink(aExpr);
  }//addExprLinkToSymbolTableEntry
  
  IRConstructionFacade::IRConstructionFacade()
  : pBlock(nullptr)
  , pSymbolFactory(nullptr)
  {
    pSymbolFactory = std::make_shared<IRSymbolFactory>();
  }
  
  IRConstructionFacade::IRConstructionFacade(BlockCPtr aBlock)
  : IRConstructionFacade()
  {
    pBlock = aBlock;
  }
  
  IRBlock::IRStmtSPtr IRConstructionFacade::stmt(const IRNode::IRNodeSPtr& aNode)
  {
    assert(aNode);
    
    auto stmtNode = aNode->getStmtNode();
    if (stmtNode)
    {
      // Return the stmt node if already set
      return IRBlock::Stmt(stmtNode);
    }
    
    // Create a new one and return it
    return IRBlock::Stmt(DEF_STMT_NODE(aNode));
  }//stmtNode
  
  IRConstructionFacade::BlockPtr IRConstructionFacade::setBlock(BlockCPtr aBlock)
  {
    // Update current block
    auto currBlock = pBlock;
    pBlock = aBlock;
    
    if(pBlock && pBlock->getModule())
    {
      assert(pBlock->getModule());
      pSymbolFactory->setModule(pBlock->getModule());
    }
    
    return currBlock;
  }//setBlock
  
  IRModuleSPtr IRConstructionFacade::module(const IRContextSPtr& aCtx, const std::string& aName, const IRModuleSPtr& aParent)
  {
    IRModuleSPtr mod(new IRModule(aCtx, aName, aParent));
    
    // Update block's module, i.e.,
    // internal facade block links to "mod"
    pBlock->setModule(mod);
    
    // Set updated block into the module
    mod->setBlock(pBlock);
    
    return mod;
  }//module
  
  IRExprStdVarSPtr IRConstructionFacade::exprStdVar(IRExprVar::SymbVarSPtrCRef aSVar)
  {
    IRExprStdVarSPtr evar(new IRExprStdVar(pBlock));
    DEF_STMT_NODE(evar);
    
    evar->setVar(aSVar);
    
    // link expr into symbol table
    addExprLinkToSymbolTableEntry(evar, aSVar);
    return evar;
  }//exprStdVar
  
  IRExprVarSPtr IRConstructionFacade::exprVar(IRExprVar::SymbVarSPtrCRef aSVar)
  {
    IRExprVarSPtr evar(new IRExprVar(pBlock));
    DEF_STMT_NODE(evar);
    
    evar->setVar(aSVar);
    
    // link expr into symbol table
    addExprLinkToSymbolTableEntry(evar, aSVar);
    return evar;
  }//exprVar
  
  IRExprConsSPtr IRConstructionFacade::exprConst(IRExprConst::SymbConstSPtrCRef aSConst)
  {
    IRExprConsSPtr econst(new IRExprConst(pBlock));
    DEF_STMT_NODE(econst);
    
    econst->setConst(aSConst);
    
    // link expr into symbol table
    addExprLinkToSymbolTableEntry(econst, aSConst);
    return econst;
  }//exprConst
  
  IRExprAssignSPtr IRConstructionFacade::exprAssign(const IRExprSPtr& aLHS, const IRExprSPtr& aRHS)
  {
    IRExprAssignSPtr assignExpr(new IRExprAssign(pBlock));
    DEF_STMT_NODE(assignExpr);
    
    assignExpr->setLhs(aLHS);
    assignExpr->setRhs(aRHS);
    
    // Check that the types must be the same
    auto vlhs = assignExpr->lhs();
    assert(vlhs->getType());
    assert(aRHS->getType());
    assert(Utils::stripType(vlhs->getType())->isEqual((Utils::stripType(aRHS->getType())).get()));
    
    // Get the LHS symbol to link to the RHS
    assert(IRExprStdVar::isa(aLHS.get()));
    
    // Link the expression to the symbol in the symbol table
    addExprLinkToSymbolTableEntry(aRHS, IRExprStdVar::cast(aLHS.get())->getVar());
    
    return assignExpr;
  }//exprAssign
  
  IRDomDeclBoundsSPtr IRConstructionFacade::domDeclBounds(const IRExprConsSPtr& aLowerBound, const IRExprConsSPtr& aUpperBound)
  {
    IRDomDeclBoundsSPtr dom(new IRDomDeclBounds(pBlock));
    DEF_STMT_NODE(dom);
    
    dom->setBounds(aLowerBound, aUpperBound);
    return dom;
  }//domDeclBounds
  
  IRDomDeclBoundsSPtr IRConstructionFacade::domDeclBoolean()
  {
    auto tf = pBlock->getModule()->getContext()->typeFactory();
    assert(tf);
    
    auto c0 = pSymbolFactory->symbolConstInteger(0);
    auto c1 = pSymbolFactory->symbolConstInteger(1);
    
    auto eConst0 = exprConst(c0);
    auto eConst1 = exprConst(c1);
    auto domDecl = domDeclBounds(eConst0, eConst1);
    
    domDecl->setBoolean();
    DEF_STMT_NODE(domDecl);
    
    return domDecl;
  }//domDeclBounds
  
  IRDomDeclBoundsSPtr IRConstructionFacade::domDeclBoolean(bool aSingVal)
  {
    auto tf = pBlock->getModule()->getContext()->typeFactory();
    assert(tf);
    
    IRExprConst::SymbConstSPtr c;
    if(aSingVal)
    {
      c = pSymbolFactory->symbolConstInteger(1);
    }
    else
    {
      c = pSymbolFactory->symbolConstInteger(0);
    }
    
    auto eConst0 = exprConst(c);
    auto eConst1 = exprConst(c);
    auto domDecl = domDeclBounds(eConst0, eConst1);
    
    domDecl->setBoolean();
    DEF_STMT_NODE(domDecl);
    
    return domDecl;
  }//domDeclBoolean
  
  IRDomDeclExtSPtr IRConstructionFacade::domDeclExt(const std::set<IRExprConsSPtr>& aSet)
  {
    IRDomDeclExtSPtr dom(new IRDomDeclExt(pBlock));
    DEF_STMT_NODE(dom);
    
    for (auto& c : aSet)
    {
      dom->addDomainConstant(c);
    }
    return dom;
  }//domDeclExt
  
  IRVarDeclSPtr IRConstructionFacade::varDecl(const IRExprVarSPtr& aVar, const std::vector<IRDomDeclSPtr>& aDomDecl)
  {
    IRVarDeclSPtr var(new IRVarDecl(pBlock));
    DEF_STMT_NODE(var);
    
    assert(!aDomDecl.empty());
    var->setLhs(aVar);
    var->setRhs(aDomDecl);

    // Check that the types must be the same
    auto vlhs = var->lhs();
    assert(vlhs->getType());
    assert(aDomDecl[0]->getDomainType());
    assert(Utils::stripType(vlhs->getType())->isEqual(Utils::stripType((aDomDecl[0]->getDomainType())).get()));
    
    return var;
  }//varDecl
  
  IRExprFcnSPtr IRConstructionFacade::exprFcn(IRExprFcn::SymbFcnSPtrCRef aSFcn)
  {
    IRExprFcnSPtr efcn(new IRExprFcn(pBlock));
    DEF_STMT_NODE(efcn);
    
    auto fcnExpr = exprConst(aSFcn);
    efcn->setFcn(fcnExpr);
    
    // link expr into symbol table
    addExprLinkToSymbolTableEntry(efcn, aSFcn);
    
    return efcn;
  }//exprFcn
  
  IRExprFcnSPtr IRConstructionFacade::exprFcn(IRExprFcn::SymbFcnSPtrCRef aSFcn, const std::vector<IRExprFcn::IRExprSPtr>& aArgs)
  {
    auto efcn = exprFcn(aSFcn);
    assert(efcn->numArgs() == aArgs.size());
    DEF_STMT_NODE(efcn);
    
    for(std::size_t idx{0}; idx < efcn->numArgs(); ++idx)
    {
      efcn->setArg(idx, aArgs[idx]);
    }
    
    return efcn;
  }//exprFcn
  
  IRExprRefSPtr IRConstructionFacade::exprRef(const std::shared_ptr<IRExpr>& aRefExpr)
  {
    assert(aRefExpr);
    IRExprRefSPtr eref(new IRExprRef(pBlock));
    DEF_STMT_NODE(eref);
    
    eref->setReferenceExpr(aRefExpr);
    return eref;
  }//exprRef
  
  IRExprMatrixSubSPtr IRConstructionFacade::exprMatrixSub(const std::shared_ptr<IRExpr>& aMatrix, const std::shared_ptr<IRExpr>& aSubscript)
  {
    assert(aMatrix);
    assert(aSubscript);
    IRExprMatrixSubSPtr matSub(new IRExprMatrixSub(pBlock));
    DEF_STMT_NODE(matSub);
    
    matSub->setMatrix(aMatrix);
    matSub->setSubscript(aSubscript);
    
    return matSub;
  }//exprMatrixSub
  
  IRConDeclSPtr IRConstructionFacade::conDecl(const IRExprFcnSPtr& aFcn)
  {
    assert(aFcn);
    
    IRConDeclSPtr conDecl(new IRConDecl(pBlock));
    DEF_STMT_NODE(conDecl);
    
    conDecl->setFcnCon(aFcn);
    return conDecl;
  }//conDecl
  
  IRConDeclSPtr IRConstructionFacade::conDecl(const IRExprFcnSPtr& aFcn, std::string& aCID)
  {
    auto cd = conDecl(aFcn);
    cd->setID(aCID);
    return cd;
  }//conDecl
  
  IRSrcDeclSPtr IRConstructionFacade::srcDecl()
  {
    return srcDecl("");
  }//srcDecl
  
  IRSrcDeclSPtr IRConstructionFacade::srcDecl(const std::string& aID)
  {
    IRSrcDeclSPtr srcDecl(new IRSrcDecl(pBlock));
    srcDecl->setID(aID);
    DEF_STMT_NODE(srcDecl);
    
    return srcDecl;
  }//srcDecl
  
}// end namespace IR

