// Self first
#include "IRContext.hpp"

#include "IRBlock.hpp"
#include "IRConstructionFacade.hpp"
#include "IRVisitor.hpp"

#include <cassert>

namespace IR {
  
  // Creates a new module with name "aName" and parent "aParent".
  // @note it creates a new IRBlock for the module
  static IR::IRModuleSPtr createModule(const IR::IRContextSPtr& aCtx,
                                       const std::string& aName,
                                       const IR::IRModuleSPtr& aParent,
                                       IR::IRConstructionFacade* aCF)
  {
    assert(aCF);
    
    // Create a new block
    IR::IRBlockSPtr b = std::make_shared<IR::IRBlock>();
    
    // Set the new block in the facade, the new module
    // will be linked to the newly created block, i.e.,
    // "mod" will have "b" as its block and vice-versa,
    // "b" will have "mod" as its module
    auto oldBlock = aCF->setBlock(b);
    auto mod = aCF->module(aCtx, aName, aParent);
    
    // Reset old block in facade
    aCF->setBlock(oldBlock);
    
    // Return module
    return mod;
  }//createGlbModule
  
  static IR::IRModuleSPtr createGlbModule(const IR::IRContextSPtr& aCtx, IR::IRConstructionFacade* aCF)
  {
    /// Create an IRContext with the modules in it
    assert(aCtx);
    assert(aCF);
    auto glbMod = createModule(aCtx, IRContext::getModuleName(IRContext::ContextModule::CM_GLOBAL), nullptr, aCF);
    
    // Add sub-modules: variable, constraint, parameter, search
    auto varMod = createModule(aCtx, IRContext::getModuleName(IRContext::ContextModule::CM_VARIABLE), glbMod, aCF);
    auto conMod = createModule(aCtx, IRContext::getModuleName(IRContext::ContextModule::CM_CONSTRAINT), glbMod, aCF);
    auto parMod = createModule(aCtx, IRContext::getModuleName(IRContext::ContextModule::CM_PARAMETER), glbMod, aCF);
    auto srcMod = createModule(aCtx, IRContext::getModuleName(IRContext::ContextModule::CM_SEARCH), glbMod, aCF);
    
    glbMod->addChild(varMod);
    glbMod->addChild(conMod);
    glbMod->addChild(parMod);
    glbMod->addChild(srcMod);
    
    return glbMod;
  }//createGlbModule
  
  static IR::IRModuleSPtr createGlobalModule(const IR::IRContextSPtr& aCtx)
  {
    std::unique_ptr<IRConstructionFacade> f(new IRConstructionFacade());
    return createGlbModule(aCtx, f.get());
  }//createGlobalModule
  
  IRContextSpec::IRContextSpec()
  : pModelName("")
  , pSolutionLimit(1)
  {
  }
  
  std::string IRContext::getModuleName(ContextModule aCtxMod)
  {
    switch (aCtxMod)
    {
      case IR::IRContext::CM_GLOBAL:
        return "model_module";
      case IR::IRContext::CM_VARIABLE:
        return "variable_module";
      case IR::IRContext::CM_CONSTRAINT:
        return "constraint_module";
      case IR::IRContext::CM_PARAMETER:
        return "parameter_module";
      default:
        assert(aCtxMod == IR::IRContext::CM_SEARCH);
        return "search_module";
    }
  }//getModuleName
  
  IRContext::IRContext()
  : IRObject(IRObjectType::IR_OBJ_CONTEXT)
  , pContextSpec(new IRContextSpec())
  , pGlobalModule(nullptr)
  , pTypeFactory(nullptr)
  {
    pTypeFactory = std::make_shared<IRTypeFactory>();
  }
  
  void IRContext::acceptVisitor(IRVisitor* aVisitor)
  {
    assert(aVisitor);
    aVisitor->visit(this);
  }
  
  IRContextSPtr createIRContext()
  {
    IRContextSPtr ctx(new IRContext());
    ctx->pGlobalModule = createGlobalModule(ctx);
    
    return ctx;
  }//createIRContext
  
}// end namespace IR
