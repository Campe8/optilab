// Self first
#include "IRTypeSequential.hpp"

namespace IR {
	
	IRTypeSequential::IRTypeSequential(IRTypeID aTypeID, std::shared_ptr<IRType> aBaseType, std::size_t aNumElem)
	: IRTypeComposite(aTypeID)
	, pBaseType(aBaseType)
	, pNumElements(aNumElem)
	{
	}
  
}// end namespace IR
