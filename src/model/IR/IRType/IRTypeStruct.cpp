// Self first
#include "IRTypeStruct.hpp"

#include <cassert>

namespace IR {
	
  IRTypeStruct::IRTypeStruct(const std::string& aName)
  : IRTypeComposite(IRTypeID::IR_TYPE_STRUCT)
  , pName(aName)
  {
    assert(!aName.empty());
  }
  
  bool IRTypeStruct::isa(const IRType* aType)
  {
    return aType && aType->getTypeID() == IR::IRTypeID::IR_TYPE_STRUCT;
  }//isa
  
  IRTypeStruct* IRTypeStruct::cast(IRType* aType)
  {
    if (!isa(aType)) return nullptr;
    return static_cast<IRTypeStruct*>(aType);
  }//cast
  
  const IRTypeStruct* IRTypeStruct::cast(const IRType* aType)
  {
    if (!isa(aType)) return nullptr;
    return static_cast<const IRTypeStruct*>(aType);
  }//cast
  
  void IRTypeStruct::setBody(const std::vector<IRTypeSPtr>& aBody)
  {
    assert(!aBody.empty());
    for(const auto& element : aBody)
    {
      addContainedType(element);
    }
  }//setBody
  
  bool IRTypeStruct::isEqual(const IRType* aOther) const
  {
    if (this == aOther) return true;
    
    auto otherStruct = cast(aOther);
    if ( !otherStruct                                      ||
         getNumElements() != otherStruct->getNumElements() ||
         getName()        != otherStruct->getName())
    {
      return false;
    }

    for(std::size_t index{0}; index < getNumElements(); ++index)
    {
      if (!getElement(index)->isEqual((otherStruct->getElement(index)).get()))
      {
        return false;
      }
    }
    
    return true;
  }//isEqual
  
  void IRTypeStruct::prettyPrint(std::ostream& aOut)
  {
    aOut << getName() << '{';
    for(std::size_t index{0}; index < getNumElements(); ++index)
    {
      // Print type
      getElement(index)->prettyPrint(aOut);
      if(index < getNumElements() - 1) aOut << ";";
      aOut << '\n';
    }
    aOut << "}";
  }//prettyPrint
  
}// end namespace IR
