// Self first
#include "IRTypeVoid.hpp"

namespace IR {
  
	IRTypeVoid::IRTypeVoid()
	: IRType(IRTypeID::IR_TYPE_VOID)
  {
  }
}// end namespace IR
