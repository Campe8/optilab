// Self first
#include "IRType.hpp"
#include "IRVisitor.hpp"

namespace IR {

  IRType::IRType(IRTypeID aTypeID)
  : IR::IRObject(IRObjectType::IR_OBJ_TYPE)
  , pTypeID(aTypeID)
  {
  }
  
  void IRType::acceptVisitor(IRVisitor* aVisitor)
  {
    assert(aVisitor);
    aVisitor->visit(this);
  }//acceptVisitor
  
}// end namespace IR

