// Self first
#include "IRTypeArray.hpp"

namespace IR {
	
	IRTypeArray::IRTypeArray(std::shared_ptr<IRType> aBaseType, std::size_t aNumElem)
	: IRTypeSequential(IRTypeID::IR_TYPE_ARRAY, aBaseType, aNumElem)
	{
	}

	bool IRTypeArray::isa(const IRType* aIRType)
	{
		return aIRType != nullptr &&
			static_cast<const IRType*>(aIRType)->getTypeID() == IRTypeID::IR_TYPE_ARRAY;
	}//isa

	IRTypeArray* IRTypeArray::cast(IRType* aIRType)
	{
		if (!isa(aIRType)) { return nullptr; }
		return static_cast<IRTypeArray*>(aIRType);
	}

	const IRTypeArray* IRTypeArray::cast(const IRType* aIRType)
	{
		if (!isa(aIRType)) return nullptr;
		return static_cast<const IRTypeArray*>(aIRType);
	}//cast

	bool IRTypeArray::isEqual(const IRType* aOther) const
	{
		return isa(aOther) &&
			getNumElements() == cast(aOther)->getNumElements() &&
			getBaseType()->isEqual((cast(aOther)->getBaseType()).get());
	}//isEqual

	void IRTypeArray::prettyPrint(std::ostream& aOut)
	{
		getBaseType()->prettyPrint(aOut);
		aOut << "[" << getNumElements() << "]";
	}//prettyPrint
  
}// end namespace IR
