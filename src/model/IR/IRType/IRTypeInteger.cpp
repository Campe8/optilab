// Self first
#include "IRTypeInteger.hpp"

namespace IR {
  
	IRTypeInteger::IRTypeInteger(NumBits numBits)
	: IRTypeNumeric(IRTypeID::IR_TYPE_INTEGER)
	, pNumBits(numBits)
  {
  }
  
  bool IRTypeInteger::isEqual(const IRType* aOther) const
  {
    if (!IRTypeInteger::isa(aOther))
    {
      return false;
    }
    else
    {
      return getNumBits() == cast(aOther)->getNumBits();
    }
  }
  
}// end namespace IR
