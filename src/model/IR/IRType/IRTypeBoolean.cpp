// Self first
#include "IRTypeBoolean.hpp"

namespace IR {
  
	IRTypeBoolean::IRTypeBoolean()
	: IRTypeNumeric(IRTypeID::IR_TYPE_BOOLEAN)
  {
  }
}// end namespace IR
