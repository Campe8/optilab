// Self first
#include "IRTypeScalar.hpp"

namespace IR {
  
	IRTypeScalar::IRTypeScalar(IRTypeID aTypeID)
	: IRType(aTypeID)
  {
  }
  
  bool IRTypeScalar::isa(const IRType* aType)
  {
    if (!aType) { return false; }
    auto tID = aType->getTypeID();
    switch (tID)
    {
      case IR::IR_TYPE_DOUBLE:
      case IR::IR_TYPE_INTEGER:
      case IR::IR_TYPE_BOOLEAN:
      case IR::IR_TYPE_POINTER:
        return true;
      default:
        return false;
    }
  }//isa
  
}// end namespace IR
