#include "IRTypeFactory.hpp"

namespace IR {
  
  IRType* IRTypeFactory::createPrimitiveType(IRTypeID aID)
  {
    switch (aID)
    {
      case IR::IR_TYPE_VOID:
        return new IRTypeVoid();
      case IR::IR_TYPE_DOUBLE:
        return new IRTypeDouble();
      case IR::IR_TYPE_INTEGER:
        return new IRTypeInteger(64);
      case IR::IR_TYPE_BOOLEAN:
        return new IRTypeBoolean();
      default:
        return nullptr;
    }
  }//getPrimitiveType
  
  IRTypeSPtr IRTypeFactory::getOrCreatePrimitive(IRTypeID aID)
  {
    int key = -1;
    switch (aID)
    {
      case IR::IR_TYPE_VOID:
      case IR::IR_TYPE_INTEGER:
      case IR::IR_TYPE_DOUBLE:
      case IR::IR_TYPE_BOOLEAN:
        key = typeIDToInt(aID);
        break;
      default:
        break;
    }
    
    // Return nullptr for non primitive types
    if (key < 0)
    {
      return IRTypeSPtr(nullptr);
    }
    
    if (pPrimitiveTypeRegister.find(key) == pPrimitiveTypeRegister.end())
    {
      auto type = createPrimitiveType(aID);
      assert(type);
      pPrimitiveTypeRegister[key] = IRTypeSPtr(type);
    }
    
    return pPrimitiveTypeRegister[key];
  }//getOrCreatePrimitive
  
  IRTypeVoidSPtr IRTypeFactory::typeVoid()
  {
    return std::static_pointer_cast<IRTypeVoid>(getOrCreatePrimitive(IRTypeID::IR_TYPE_VOID));
  }
  
  IRTypeBooleanSPtr IRTypeFactory::typeBoolean()
  {
    return std::static_pointer_cast<IRTypeBoolean>(getOrCreatePrimitive(IRTypeID::IR_TYPE_BOOLEAN));
  }
  
  IRTypeIntegerSPtr IRTypeFactory::typeInteger64()
  {
    return std::static_pointer_cast<IRTypeInteger>(getOrCreatePrimitive(IRTypeID::IR_TYPE_INTEGER));
  }
  
  IRTypeDoubleSPtr IRTypeFactory::typeDouble()
  {
    return std::static_pointer_cast<IRTypeDouble>(getOrCreatePrimitive(IRTypeID::IR_TYPE_DOUBLE));
  }
  
  IRTypeFunctionSPtr IRTypeFactory::typeFunction(const IRTypeSPtr& aResultType, const std::vector<IRTypeSPtr>& aParamType)
  {
    IRTypeFunctionSPtr pIRTypePointer(new IRTypeFunction(aResultType, aParamType));
    return pIRTypePointer;
  }
  
  IRTypeFunctionSPtr IRTypeFactory::typeFunctionConstraint(const std::vector<IRTypeSPtr>& aParamType)
  {
    for(auto& entry : pConTypeReg[aParamType.size()])
    {
      if(entry->match(aParamType))
      {
        return entry->fcnConType;
      }
    }
    
    auto newEntry = std::make_shared<FcnConTypeElem>();
    IRTypeFunctionSPtr pIRTypePointer(new IRTypeFunction(typeVoid(), aParamType));
    newEntry->fcnConType = pIRTypePointer;
    newEntry->argTypeVec = aParamType;
    pConTypeReg[ aParamType.size() ].push_back(newEntry);
    
    return pIRTypePointer;
  }
  
  IRTypeArraySPtr IRTypeFactory::typeArray(std::size_t aNumElements, const IRTypeSPtr& aBaseType)
  {
    assert(aBaseType);
    
    auto baseTypeKey = typeIDToInt(aBaseType->getTypeID());
    for (auto& entry : pArrayTypeReg[baseTypeKey])
    {
      if (entry->match(aBaseType, aNumElements))
      {
        return entry->arrayType;
      }
    }
    
    auto newEntry = std::make_shared<ArrayTypeElem>();
    IRTypeArraySPtr pIRTypePointer(new IRTypeArray(aBaseType, aNumElements));
    
    newEntry->arrayBaseType = aBaseType;
    newEntry->arrayNumElements = aNumElements;
    newEntry->arrayType = pIRTypePointer;
    pArrayTypeReg[baseTypeKey].push_back(newEntry);
    
    return pIRTypePointer;
  }//typeArray
  
  IRTypeStructSPtr IRTypeFactory::typeStruct(const std::string& aName,
                                             const std::vector<IRTypeStruct::IRTypeSPtr>& aElements)
  {
    assert(!aName.empty() && !aElements.empty());
    IRTypeStructSPtr structType(new IRTypeStruct(aName));
    structType->setBody(aElements);
    return structType;
  }//typeStruct
  
}// end namespace IR
