// Self first
#include "IRTypeFunction.hpp"

namespace IR {
  
	IRTypeFunction::IRTypeFunction(const IRTypeSPtr& aResultType, const std::vector<IRTypeSPtr>& aParamType)
	: IRType(IRTypeID::IR_TYPE_FUNCTION)
  {
    addContainedType(aResultType);
    for(IRTypeSPtr param : aParamType)
    {
      addContainedType(param);
    }
  }
  
  bool IRTypeFunction::isEqual(const IRType* aOther) const
  {
    auto fcnt = IRTypeFunction::cast(aOther);
    if ( !fcnt ||
         getNumParams() != fcnt->getNumParams() )
    {
      return false;
    }
    
    if(!getReturnType()->isEqual((fcnt->getReturnType()).get()))
    {
      return false;
    }
    
    for(std::size_t index{0}; index < getNumParams(); ++index)
    {
      if (!getParamType(index)->isEqual((fcnt->getParamType(index)).get()))
      {
        return false;
      }
    }
    
    return true;
  }//isEqual
  
  void IRTypeFunction::prettyPrint(std::ostream& aOut)
  {
    getReturnType()->prettyPrint(aOut);
    aOut << '(';
    for(std::size_t index{0}; index < getNumParams(); ++index)
    {
      getParamType(index)->prettyPrint(aOut);
      if(index < getNumParams() - 1) aOut << ", ";
    }
    aOut << ')';
  }//prettyPrint
  
}// end namespace IR
