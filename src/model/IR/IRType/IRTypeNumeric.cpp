// Self first
#include "IRTypeNumeric.hpp"

namespace IR {
  
	IRTypeNumeric::IRTypeNumeric(IRTypeID aTypeID)
	: IRTypeScalar(aTypeID)
  {
  }
  
  bool IRTypeNumeric::isa(const IRType* aType)
  {
    if (!IRTypeScalar::isa(aType)) { return false; }
    
    auto tID = aType->getTypeID();
    switch (tID)
    {
      case IR::IR_TYPE_DOUBLE:
      case IR::IR_TYPE_INTEGER:
      case IR::IR_TYPE_BOOLEAN:
        return true;
      default:
        return false;
    }
  }//isa
  
}// end namespace IR
