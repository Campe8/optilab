// Self first
#include "IRTypeDouble.hpp"

namespace IR {
  
	IRTypeDouble::IRTypeDouble()
	: IRTypeNumeric(IRTypeID::IR_TYPE_DOUBLE)
  {
  }
  
}// end namespace IR
