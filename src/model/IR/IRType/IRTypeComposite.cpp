// Self first
#include "IRTypeComposite.hpp"

namespace IR {
	
	IRTypeComposite::IRTypeComposite(IRTypeID aTypeID)
	: IRType(aTypeID)
	{
	}
  
  bool IRTypeComposite::isa(const IRType* aType)
  {
    if(!aType) { return false; }
    
    auto typeID = aType->getTypeID();
    switch (typeID) {
      case IR::IRTypeID::IR_TYPE_STRUCT:
      case IR::IRTypeID::IR_TYPE_ARRAY:
        return true;
      default:
        return false;
    }
  }//isa
  
  IRTypeComposite* IRTypeComposite::cast(IRType* aType)
  {
    if(!isa(aType)) { return nullptr; }
    return static_cast<IRTypeComposite*>(aType);
  }//cast
  
  const IRTypeComposite* IRTypeComposite::cast(const IRType* aType)
  {
    if(!isa(aType)) { return nullptr; }
    return static_cast<const IRTypeComposite*>(aType);
  }//cast
  
}// end namespace IR
