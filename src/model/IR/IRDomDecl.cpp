// Self first
#include "IRDomDecl.hpp"

#include <cassert>

namespace IR {
  
  IRDomDecl::IRDomDecl(const std::shared_ptr<IRBlock>& aParentBlock, DomDeclType aDomDeclType)
  : IR::IRNode(aParentBlock, IRNodeType::IR_NODE_DOM_DECL)
  , pDeclType(aDomDeclType)
  , pType(nullptr)
  {
  }
  
  void IRDomDecl::clearConstants()
  {
    assert(getStmtNode());
    getStmtNode()->removeChildren();
  }//clearConstants
  
  std::size_t IRDomDecl::getNumConstants() const
  {
    assert(getStmtNode());
    return getStmtNode()->numChildren();
  }//getNumConstants
  
  void IRDomDecl::addConstant(const DomainConst& aExprConst)
  {
    assert(aExprConst);
    if(!pType)
    {
      pType = aExprConst->getConst()->getType();
      assert(pType);
    }
    
    assert(aExprConst->getConst()->getType()->isEqual(pType.get()));
    addChild(aExprConst, getNumConstants());
  }//addConstant
  
  IRDomDecl::DomainConst IRDomDecl::getConstant(std::size_t aIdx) const
  {
    assert(aIdx < getNumConstants());
    auto domElemExpr = getChild(aIdx);
    return std::static_pointer_cast<IRExprConst>(domElemExpr);
  }//getConstants
  
}// end namespace IR
