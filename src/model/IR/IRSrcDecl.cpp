// Self first
#include "IRSrcDecl.hpp"
#include "IRVisitor.hpp"

#include <cassert>

namespace IR {
  
	IRSrcDecl::IRSrcDecl(const std::shared_ptr<IRBlock>& aParentBlock)
  : IR::IRNode(aParentBlock, IRNodeType::IR_NODE_SRC_DECL)
  , pID("")
	, pSrcType(SearchType::ST_UNDEF)
  {
  }
  
  bool IRSrcDecl::isa(const IRNode* aNode)
  {
    return aNode != nullptr && aNode->getNodeType() == IRNodeType::IR_NODE_SRC_DECL;
  }//isa
  
  IRSrcDecl* IRSrcDecl::cast(IRNode* aNode)
  {
    if (!isa(aNode)) return nullptr;
    return static_cast<IRSrcDecl*>(aNode);
  }//cast
  
  const IRSrcDecl* IRSrcDecl::cast(const IRNode* aNode)
  {
    if (!isa(aNode)) return nullptr;
    return static_cast<const IRSrcDecl*>(aNode);
  }//cast
  
  void IRSrcDecl::acceptVisitor(IRVisitor* aVisitor)
  {
    assert(aVisitor);
    aVisitor->visit(this);
  }//acceptVisitor
  
  std::size_t IRSrcDecl::numBranchingVars() const
  {
    assert(getStmtNode());
    return getStmtNode()->numChildren();
  }//numBranchingVars
  
  void IRSrcDecl::setBranchingVarList(const std::vector<std::shared_ptr<IRExprVar>>& aBranchingList)
  {
    std::size_t idx{0};
    for(auto& var : aBranchingList)
    {
      addChild(var, idx++);
    }
  }//setBranchingVarList
  
  std::shared_ptr<IRExprVar> IRSrcDecl::getBranchingVar(std::size_t aIdx) const
  {
    assert(aIdx < numBranchingVars());
    auto var = getChild(aIdx);
    return std::static_pointer_cast<IRExprVar>(var);
  }//getBranchingVar
  
}// end namespace IR
