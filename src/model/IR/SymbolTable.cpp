// Self first
#include "SymbolTable.hpp"

#include <cassert>

namespace IR {
  
  SymbolTable::SymbolTable(SymbTablePtrConstRef aParent)
		: pParent(aParent)
  {
  }
  
  bool SymbolTable::contains(SymbolKey aKey)
  {
    return find(aKey) != end();
  }
  
  SymbolTable::SymbTableIter SymbolTable::find(SymbolKey aKey)
  {
    return pSymbTable.find(aKey);
  }//find
  
  SymbolTable::SymbTableCIter SymbolTable::find(SymbolKey aKey) const
  {
    return pSymbTable.find(aKey);
  }//find
  
  SymbolTable::SymbolTableEntrySPtr SymbolTable::lookup(const IRSymbol::SymbolName& aSymbName)
  {
    auto key = SymbolTableEntry::getKeyFromName(aSymbName);
    if (!contains(key))
    {
      return SymbolTableEntrySPtr(nullptr);
    }
    return find(key)->second;
  }//lookup
  
  bool SymbolTable::insert(const SymbolTableEntrySPtr& aEntry)
  {
    assert(aEntry);
    if (contains(aEntry->getKey()))
    {
      return false;
    }
    
    // Insert new entry in the symbol table
    pSymbTable[aEntry->getKey()] = aEntry;
    return true;
  }//insert
  
  void SymbolTable::erase(SymbTableCIter aIt)
  {
    pSymbTable.erase(aIt);
  }//erase
  
  bool SymbolTable::erase(SymbolKey aKey)
  {
    if (!contains(aKey))
    {
      return false;
    }
    auto it = find(aKey);
    erase(it);
    
    return true;
  }//erase
  
  SymbolTable::SymbTableIter SymbolTable::begin()
  {
    SymbolTable::SymbTableIter it(pSymbTable.begin());
    return it;
  }
  
  SymbolTable::SymbTableCIter SymbolTable::begin() const
  {
    SymbolTable::SymbTableCIter it(pSymbTable.begin());
    return it;
  }
  
  SymbolTable::SymbTableIter SymbolTable::end()
  {
    SymbolTable::SymbTableIter it(pSymbTable.end());
    return it;
  }
  
  SymbolTable::SymbTableCIter SymbolTable::end() const
  {
    SymbolTable::SymbTableCIter it(pSymbTable.end());
    return it;
  }
  
}// end namespace IR
