// Self first
#include "IRUtils.hpp"

#include "IRContext.hpp"
#include "IRExprConst.hpp"
#include "IRExprRef.hpp"
#include "IRSymbolConstInteger.hpp"
#include "IRExprMatrixSub.hpp"
#include "IRTypeArray.hpp"

#include <cassert>

namespace IR {
	namespace Utils {
    
		static std::shared_ptr<IRType> getBaseTypeSymbolConst(const IR::IRExprConst::SymbConstSPtr& aSymb)
		{
			assert(aSymb);
			auto stype = aSymb->getType();
			if (IR::IRTypeArray::isa(stype.get()))
			{
				return IR::IRTypeArray::cast(stype.get())->getBaseType();
			}
			
			assert(!IR::IRTypeComposite::isa(stype.get()));
			return stype;
		}//getBaseTypeSymbolConst

		static IR::SymbolTable::SymbolTableEntrySPtr lookupSymbolEntryOnModules(const std::vector<IR::IRModuleSPtr>& aMods, const IR::IRSymbol::SymbolName& aSymb)
		{
			IR::SymbolTable::SymbolTableEntrySPtr symbEntry(nullptr);
			for (auto& m : aMods)
			{
				symbEntry = lookupSymbol(m, aSymb);
				if (symbEntry)
				{
					return symbEntry;
				}
			}
			return symbEntry;
		}//getSymbolEntryFromModule

    // Lookup "aName" on the symbol tables of the given modules and returns the correspondent
    // symbol table entry if any.
    // Returns nullptr if the entry is not present
    static std::shared_ptr<IR::IRExpr> lookupExprFromSymbolTable(const std::string& aName, const std::vector<IRModuleSPtr>& aMods)
    {
      auto symbEntry = lookupSymbolEntryOnModules(aMods, aName);
      if (!symbEntry) return nullptr;
      
      assert((symbEntry->getLinkSet()).size() == 1);
      return *((symbEntry->getLinkSet()).begin());
    }//lookupExprFromSymbolTable
    
		std::shared_ptr<IR::IRExprVar> lookupExprVar(const std::string& aName, const IR::IRContextSPtr& aCtx)
		{
      assert(aCtx);

			// Look in variable module for given symbol
			//     get all the leaves since their symbol tables are brothers and they do not
			//     share symbols although they share the parents.
			// @note looking on CM_VARIABLE, add to mdll other leaves if other modules can contain symbols
			auto mdlName = IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_VARIABLE);
      auto mdll = getLeaves(aCtx->globalModule()->getSubModule(mdlName));
      if(auto expr = lookupExprFromSymbolTable(aName, mdll))
      {
        return std::dynamic_pointer_cast<IR::IRExprVar>( expr );
      }
      
      return nullptr;
		}//lookupExprVar
    
    std::shared_ptr<IR::IRExprConst> lookupExprConstPar(const std::string& aName, const IR::IRContextSPtr& aCtx)
    {
      assert(aCtx);
      
      // Look in parameter module for given symbol
      //     get all the leaves since their symbol tables are brothers and they do not
      //     share symbols although they share the parents.
      // @note looking on CM_PARAMETER, add to mdll other leaves if other modules can contain symbols
      auto mdlName = IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_PARAMETER);
      auto mdll = getLeaves(aCtx->globalModule()->getSubModule(mdlName));
      
      if(auto expr = lookupExprFromSymbolTable(aName, mdll))
      {
        return std::dynamic_pointer_cast<IR::IRExprConst>( expr );
      }
      
      return nullptr;
    }//lookupExprConstPar
    
    std::shared_ptr<IR::IRExpr> lookupExprByName(const std::string& aName, const IR::IRContextSPtr& aCtx)
    {
      assert(aCtx);
      std::string moduleName;
      
      for(int mdlIdx{0}; mdlIdx < static_cast<int>(IR::IRContext::ContextModule::CM_UNDEF); ++mdlIdx)
      {
        moduleName = IR::IRContext::getModuleName(static_cast<IR::IRContext::ContextModule>(mdlIdx));
        if( auto expr = lookupExprFromSymbolTable(aName, getLeaves(aCtx->globalModule()->getSubModule(moduleName))) )
        {
          return expr;
        }
      }
      
      return nullptr;
    }//lookupExprByName
    
    // Strip "aType" returning the base type
    std::shared_ptr<IRType> stripType(const std::shared_ptr<IRType>& aType)
    {
      std::shared_ptr<IRType> stype = aType;
      while(IRTypeArray::isa(stype.get()))
      {
        stype = IRTypeArray::cast(stype.get())->getBaseType();
      }

      return stype;
    }//stripType
    
		std::shared_ptr<IRType> getBaseType(const std::shared_ptr<IRExpr>& aExpr)
		{
			if (!aExpr) { return nullptr; }
			auto etype = aExpr->getExprType();
			
			switch (etype)
			{
      case IR::IRExprType::IR_EXPR_VAR:
			case IR::IRExprType::IR_EXPR_STD_VAR:
			case IR::IRExprType::IR_EXPR_FCN:
      case IR::IRExprType::IR_EXPR_REF:
				return stripType(aExpr->getType());
      case IR::IRExprType::IR_EXPR_MAT_SUB:
        return IR::IRExprMatrixSub::cast(aExpr.get())->getType();
			default:
				assert(etype == IR::IRExprType::IR_EXPR_CONST);
				return getBaseTypeSymbolConst(IR::IRExprConst::cast(aExpr.get())->getConst());
			}
		}//getBaseType
    
    void swapExpr(const std::shared_ptr<IRExpr>& aExpr1, const std::shared_ptr<IRExpr>& aExpr2)
    {
      if (aExpr1 == aExpr2) { return; }
      assert(aExpr1);
      assert(aExpr1->getStmtNode());
      assert(aExpr2->getStmtNode());
      
      auto stmtNode = aExpr1->getStmtNode();
      auto parentNode = stmtNode->parent();
      assert(parentNode.data());
      
      std::size_t eIdx{ 0 };
      for (const auto& c : parentNode)
      {
        if (c.data() == stmtNode->data())
        {
          break;
        }
        eIdx++;
      }
      assert(eIdx < parentNode.numChildren());
      
      parentNode.replaceChild(*(aExpr2->getStmtNode()), eIdx);
    }//swapExpr
    
    // Returns true if the two expressions are the same, false otherwiser
    bool isEqualExpr(const IR::IRExpr* aBase, const IR::IRExpr* aComp)
    {
      if(!aBase || !aComp) return false;
      return aBase->isEqual(aComp);
    }//isEqualExpr
    
	}// end namespace Utils
}// end namespace IR
