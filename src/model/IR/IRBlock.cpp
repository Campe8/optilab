// Self first
#include "IRBlock.hpp"

#include <cassert>

namespace IR {
  
  static bool containsNode(const IRBlock::IRStmtSPtr& aStmt, const IRNode::IRNodeSPtr& aNode)
  {
    assert(aStmt);
    assert(aNode);
    
    if ((*aStmt).root().contains(aNode))
    {
      return true;
    }
    return false;
  }//contains
  
  IRBlock::IRStmtSPtr IRBlock::Stmt(const IRNode::IRStmtNodeSPtr& aStmtNode)
  {
    assert(aStmtNode);
    return std::make_shared<IRStmt>(aStmtNode);
  }
  
  IRBlock::IRBlock()
		: pStmtList(nullptr)
		, pModule(nullptr)
  {
    pStmtList = std::make_shared<StmtList>();
  }
  
  void IRBlock::clear()
  {
    assert(pStmtList);
    for (auto stmt : *pStmtList)
    {
      stmt->clear();
    }
    
    pStmtList->clear();
    pStmtList = std::make_shared<StmtList>();
  }//clear
  
  bool IRBlock::isEmpty() const
  {
    bool empty = false;
    if (!pStmtList || (pStmtList->size() == 0))
    {
      empty = true;
    }
    
    return empty;
  }//empty
  
  bool IRBlock::contains(const IRStmtSPtr& aStmt) const
  {
    assert(aStmt);
    return find(aStmt) != end();
  }//contains
  
  void IRBlock::appendStmt(const IRStmtSPtr& aStmt)
  {
    assert(aStmt);
    assert(pStmtList);
    pStmtList->push_back(aStmt);
  }//appendStmt
  
  void IRBlock::removeStmt(const IRStmtSPtr& aStmt)
  {
    if (!contains(aStmt))
    {
      return;
    }
    
    StmtListCIter it = find(aStmt);
    removeStmt(it);
  }//removeStmt
  
  void IRBlock::removeStmt(StmtListCIter& aStmtIter)
  {
    assert(pStmtList);
    if (aStmtIter == end())
    {
      return;
    }
    pStmtList->erase(aStmtIter);
  }//removeStmt
  
  IRBlock::StmtListIter IRBlock::find(const IRNode::IRNodeSPtr& aNode)
  {
    assert(aNode);
    auto it = begin();
    for (; it != end(); ++it)
    {
      if (containsNode(*it, aNode))
      {
        return it;
      }
    }
    
    return end();
  }//find
  
  IRBlock::StmtListCIter IRBlock::find(const IRNode::IRNodeSPtr& aNode) const
  {
    assert(aNode);
    auto it = begin();
    for (; it != end(); ++it)
    {
      if (containsNode(*it, aNode))
      {
        return it;
      }
    }
    
    return end();
  }//find
  
  IRBlock::StmtListIter IRBlock::find(const IRStmtSPtr& aStmt)
  {
    assert(aStmt);
    auto it = begin();
    for (; it != end(); ++it)
    {
      if (*it == aStmt)
      {
        return it;
      }
    }
    return end();
  }//find
  
  IRBlock::StmtListCIter IRBlock::find(const IRStmtSPtr& aStmt) const
  {
    assert(aStmt);
    auto it = begin();
    for (; it != end(); ++it)
    {
      if (*it == aStmt)
      {
        return it;
      }
    }
    return end();
  }//find
  
  IRBlock::StmtListIter IRBlock::begin()
  {
    assert(pStmtList);
    return pStmtList->begin();
  }//begin
  
  IRBlock::StmtListCIter IRBlock::begin() const
  {
    assert(pStmtList);
    return pStmtList->cbegin();
  }//begin
  
  IRBlock::StmtListIter IRBlock::end()
  {
    assert(pStmtList);
    return pStmtList->end();
  }//begin
  
  IRBlock::StmtListCIter IRBlock::end() const
  {
    assert(pStmtList);
    return pStmtList->cend();
  }//begin
  
}// end namespace Model
