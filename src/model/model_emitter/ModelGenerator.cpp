// Self first
#include "ModelGenerator.hpp"

// Generators
#include "VariableGenerator.hpp"
#include "ParameterGenerator.hpp"
#include "ConstraintGenerator.hpp"
#include "SearchGenerator.hpp"

// IR
#include "IRVarDecl.hpp"
#include "IRConDecl.hpp"
#include "IRSrcDecl.hpp"
#include "IRExprAssign.hpp"

#include <cassert>

namespace Model {
  
  // Returns true iff "aMod" is
  // 1 - a global module;
  // 2 - a low-lever IR representation.
  static bool isLowIR(const IR::IRModuleSPtr& aMod)
  {
    assert(aMod);
    if (!aMod->isGlobalModule()) return false;
    if (aMod->numChildren() == 4)
    {
      auto m1 = aMod->getSubModule(IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_VARIABLE));
      auto m2 = aMod->getSubModule(IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_CONSTRAINT));
      auto m3 = aMod->getSubModule(IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_PARAMETER));
      auto m4 = aMod->getSubModule(IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_SEARCH));
      if (m1 && m2 && m3 && m4) return true;
    }
    return false;
  }//isLowIR
  
  // Returns the vector of model objects generated using by the given generator on the given model and
  // on the IRNodes specified as the template class
  template<typename T>
  std::vector<ModelObject*> generateObjects(const IR::IRModuleSPtr& aModule,
                                            const std::shared_ptr<ModelObjectGenerator>& aGenerator,
                                            const Model* aModel)
  {
    assert(aModule && aModule->numChildren() == 0);
    auto blk = aModule->getBlock();
    
    assert(blk && !blk->isEmpty());
    std::vector<ModelObject*> modObjects;
    for (auto& n : blk->preOrderVisit<T>())
    {
      assert(T::isa(n.get()));
      auto irNode = T::cast(n.get());
      modObjects.push_back(aGenerator->generateObject(irNode, aModel));
    }
    
    return modObjects;
  }//generateDescriptorObjects
  
  // Returns the context module to use when parsing IR w.r.t. the given class of model object to create
  static IR::IRContext::ContextModule getCtxModuleGivenObjectClass(ModelObjectClass aObjectClass)
  {
    switch (aObjectClass) {
      case ModelObjectClass::MODEL_OBJ_VARIABLE:
        return IR::IRContext::ContextModule::CM_VARIABLE;
      case ModelObjectClass::MODEL_OBJ_CONSTRAINT:
        return IR::IRContext::ContextModule::CM_CONSTRAINT;
      case ModelObjectClass::MODEL_OBJ_PARAMETER:
        return IR::IRContext::ContextModule::CM_PARAMETER;
      default:
        assert(aObjectClass == ModelObjectClass::MODEL_OBJ_SEARCH_SEMANTIC);
        return IR::IRContext::ContextModule::CM_SEARCH;
    }
  }//getCtxModuleGivenObjectClass
  
  static void addObjectsToModel(const std::vector<ModelObject*>& aObjects,  Model* aModel)
  {
    for(auto& obj : aObjects)
    {
      assert(obj);
      aModel->addModelObject(ModelObjectUPtr(obj));
    }
  }//addObjectsToModel
  
  ModelGenerator::ModelGenerator()
  {
    registerGenerators();
  }
  
  void ModelGenerator::registerGenerators()
  {
    /*
     * The following model object generators are instantiated
     * and pushed into the map w.r.t. their order of execution.
     * In other words, each generator has its on priority on when
     * it must be run.
     * This is because model objects have dependencies on each other so
     * they must be generated in a topological order:
     * 1) object variables
     * 2) object parameters
     * 3) object constraints
     * 4) object search
     * The above order must be preserved by the generator priorities.
     */
    pGeneratorKeyMap.push_back( {static_cast<int>(ModelObjectClass::MODEL_OBJ_VARIABLE), pGeneratorMap.size()} );
    pGeneratorMap.push_back(std::make_shared<VariableGenerator>());
    
    pGeneratorKeyMap.push_back( {static_cast<int>(ModelObjectClass::MODEL_OBJ_PARAMETER), pGeneratorMap.size()} );
    pGeneratorMap.push_back(std::make_shared<ParameterGenerator>());
    
    pGeneratorKeyMap.push_back( {static_cast<int>(ModelObjectClass::MODEL_OBJ_CONSTRAINT), pGeneratorMap.size()} );
    pGeneratorMap.push_back(std::make_shared<ConstraintGenerator>());
    
    pGeneratorKeyMap.push_back( {static_cast<int>(ModelObjectClass::MODEL_OBJ_SEARCH_SEMANTIC), pGeneratorMap.size()} );
    pGeneratorMap.push_back(std::make_shared<SearchGenerator>());
  }//registerBuilders
  
  // Returns the context module to use when parsing IR w.r.t. the given class of model object to create
  ModelGenerator::ObjectGenerator ModelGenerator::getGeneratorGivenObjectClass(ModelObjectClass aObjectClass)
  {
    for(const auto& genPair : pGeneratorKeyMap)
    {
      if(genPair.first == static_cast<int>(aObjectClass))
      {
        assert(genPair.second < pGeneratorMap.size());
        return pGeneratorMap[genPair.second];
      }
    }
    
    return nullptr;
  }//getGeneratorGivenObjectClass
  
  std::vector<ModelObject*> ModelGenerator::generateObjectsGivenModelObjectClass(ModelObjectClass aObjectClass,
                                                                                 const IR::IRModuleSPtr& aModule,
                                                                                 const Model* aModel)
  {
    // Get the context module to use for the given class
    auto ctxModel = getCtxModuleGivenObjectClass(aObjectClass);
    
    // Get the generator to use for the given class
    auto generator = getGeneratorGivenObjectClass(aObjectClass);
    assert(generator);
    
    // Get the corresponding IR module.
    // @note generate object only if the module is not empty
    auto mod = aModule->getSubModule(IR::IRContext::getModuleName(ctxModel));
    if(!IR::hasEmptyBlock(mod))
    {
      switch (aObjectClass) {
        case ModelObjectClass::MODEL_OBJ_VARIABLE:
          return generateObjects<IR::IRVarDecl>(mod, generator, aModel);
        case ModelObjectClass::MODEL_OBJ_CONSTRAINT:
          return generateObjects<IR::IRConDecl>(mod, generator, aModel);
        case ModelObjectClass::MODEL_OBJ_PARAMETER:
          return generateObjects<IR::IRExprAssign>(mod, generator, aModel);
        default:
          assert(aObjectClass == ModelObjectClass::MODEL_OBJ_SEARCH_SEMANTIC);
          return generateObjects<IR::IRSrcDecl>(mod, generator, aModel);
      }
    }//if
    
    return std::vector<ModelObject*>();
  }//generateObjectsOfClass
  
  void ModelGenerator::addModelObjectsToModel(ModelSPtr& aModel, const IR::IRContextSPtr& aCtx)
  {
    // Walk the global module and create model object descriptors
    auto glbModule = aCtx->globalModule();
    assert(glbModule && isLowIR(glbModule));
    
    // Traverse the IRmodules and generate model objects to add to the model.
    // Model generators are run in the order they have been instantiated in the map
    for(const auto& genKey : pGeneratorKeyMap)
    {
      auto objectClass = static_cast<ModelObjectClass>(genKey.first);
      auto objectList = generateObjectsGivenModelObjectClass(objectClass, glbModule, aModel.get());
      
      // Add the model objects to the model
      addObjectsToModel(objectList, aModel.get());
    }
  }//addModelObjectsToModel
  
	ModelSPtr ModelGenerator::generateModel(const IR::IRContextSPtr& aIRCtx)
	{
		assert(aIRCtx);

		// Create meta information to generate the model
    auto modelMetaInfo = std::make_shared<ModelMetaInfo>(aIRCtx->getContextSpec()->getModelName(),
                                                         aIRCtx->getContextSpec()->getSolutionLimit());
    
    // Create a new instance of a model based on the meta-information from the context
    auto model = std::shared_ptr<Model>(new Model(modelMetaInfo));
    
    // Parse IR, generate model objects and add them to the model
    addModelObjectsToModel(model, aIRCtx);
    
    return model;
	}//buildModel
  
}// end namespace Model
