// Self first
#include "VariableGenerator.hpp"

#include "DomainDescriptor.hpp"
#include "ModelVariable.hpp"

// IR
#include "IRVarDecl.hpp"
#include "IRTypeInteger.hpp"
#include "IRTypeBoolean.hpp"
#include "IRTypeArray.hpp"
#include "IRDomDeclBounds.hpp"
#include "IRDomDeclExt.hpp"
#include "IRSrcDecl.hpp"

// Variable
#include "Variable.hpp"

// Semantics
#include "VariableSemanticDecision.hpp"
#include "VariableSemanticSupport.hpp"
#include "VariableSemanticObjective.hpp"

// Utilities
#include "CLIUtils.hpp"

#include <cassert>

using DomainType = INT_64;
using Semantic = Core::VariableSemanticUPtr;
using SemanticList = Model::ModelVariable::SemanticList;

namespace Model {

  static Core::VariableSemantic* getBaseSemantic(const std::shared_ptr<IR::IRExprVar>& aVar)
  {
    Core::VariableSemantic* vs{ nullptr };

    auto id = aVar->getVar()->getName();
    auto sType = aVar->getSemantic();
    switch (sType)
    {
      case IR::IRExprVar::VS_DECISION:
        vs = new Core::VariableSemanticDecision(id);
        break;
      case IR::IRExprVar::VS_SUPPORT:
        vs = new Core::VariableSemanticSupport(id);
        break;
      case IR::IRExprVar::VS_OBJECTIVE_MAX:
        vs = new Core::VariableSemanticObjective(Core::VariableSemanticObjective::ObjectiveExtremum::OBJ_EXT_MAXIMIZE, id);
        break;
      default:
        assert(sType == IR::IRExprVar::VS_OBJECTIVE_MIN);
        vs = new Core::VariableSemanticObjective(Core::VariableSemanticObjective::ObjectiveExtremum::OBJ_EXT_MINIMIZE, id);
        break;
    }
    
    // Set force output true or false according to the expr var semantics.
    // If neither OFF nor ON is set, use default output semantics
    if(aVar->getOutput() == IR::IRExprVar::VarOutput::VO_FORCED_OFF)
    {
      vs->forceOutput(false);
    }
    else if(aVar->getOutput() == IR::IRExprVar::VarOutput::VO_FORCED_ON)
    {
      vs->forceOutput(true);
    }
    
    return vs;
  }//getBaseSemantic
  
  static SemanticList getSemanticList(IR::IRVarDecl* aVDecl)
	{
		auto eVar = aVDecl->lhs();
		assert(eVar);
    
    SemanticList semanticList;
    for(auto& ss : *eVar)
    {
      // Create new semantic
      auto semantic = std::shared_ptr<Core::VariableSemantic>(getBaseSemantic(eVar));
      
      // Set search semantic
      semantic->setInputOrder(ss->getInputOrder());
      semantic->setBranchingPolicy(ss->getBranchingPolicy());
      semantic->setReachability(ss->getReachability());
      
      semanticList.push_back({ss->getSearchDeclaration()->getID(), semantic});
    }
    
		return semanticList;
	}//getSemanticList
  
  static Core::Variable* createScalarVariable(const std::shared_ptr<DomainDescriptor<DomainType>>& aDomainDescriptor, Core::VariableSemantic* aVariableSemantic, Core::VariableFactory& aVarFactory)
  {
    // Get the elements of the domain
    const auto& domElems = aDomainDescriptor->getDomainElements();
    
    // Get the representation of the domain
    auto domRep = aDomainDescriptor->getDomainRepresentation();
    
    // Get domain type
    auto domType = aDomainDescriptor->getDomainType();
    if(domRep == DomainDescriptor<DomainType>::DomainRepresentation::SINGLETON)
    {
      assert(aDomainDescriptor->getNumDomainElements() == 1);
      if(domType == Core::VariableTypeClass::VAR_TYPE_BOOLEAN)
      {
        // Boolean singleton
        return aVarFactory.variableBoolean(domElems[0], Core::VariableSemanticUPtr(aVariableSemantic));
      }
      else
      {
        // Integer singleton
        assert(domType == Core::VariableTypeClass::VAR_TYPE_INTEGER);
        return aVarFactory.variableInteger(domElems[0], Core::VariableSemanticUPtr(aVariableSemantic));
      }
    }
    else if(domRep == DomainDescriptor<DomainType>::DomainRepresentation::BOUNDS)
    {
      assert(aDomainDescriptor->getNumDomainElements() == 2);
      if(domType == Core::VariableTypeClass::VAR_TYPE_BOOLEAN)
      {
        // Boolean {false, true}
        return aVarFactory.variableBoolean(Core::VariableSemanticUPtr(aVariableSemantic));
      }
      else
      {
        // Integer [lower, upper]
        assert(domType == Core::VariableTypeClass::VAR_TYPE_INTEGER);
        return aVarFactory.variableInteger(domElems[0], domElems[1], Core::VariableSemanticUPtr(aVariableSemantic));
      }
    }
    else
    {
      // List of elements
      assert(domRep == DomainDescriptor<DomainType>::DomainRepresentation::LIST);
      std::unordered_set<DomainType> setOfElements(domElems.begin(), domElems.end());
      return aVarFactory.variableInteger(setOfElements, Core::VariableSemanticUPtr(aVariableSemantic));
    }
  }//createScalarVariable
  
  static Core::Variable* generateMatrixVariable(const std::vector<std::shared_ptr<DomainDescriptor<DomainType>>>& aDomainDescriptorList,
                                                const std::vector<std::size_t>& aDims, Core::VariableSemantic* aVariableSemantic, Core::VariableFactory& aVarFactory)
  {
    // Generate vectorized matrix according to its dimensions
    assert(aDims.size() > 1 && aDims[1] > 0);
    auto nrows = aDims[0] > 0 ? aDims[0] : 1;
    auto ncols = aDims[1];
    
    // Set the same semantic for each matrix element
    auto semantic = Semantic(aVariableSemantic->clone());
    
    // Create the vector of primitive variables representing
    // the vectorized matrix
    std::vector<Core::VariableSPtr> arrayVars;
    assert(aDomainDescriptorList.size() == nrows * ncols);
    for(std::size_t nrow{0}; nrow < nrows; ++nrow)
    {
      for(std::size_t ncol{0}; ncol < ncols; ++ncol)
      {
        auto domPtr = aDomainDescriptorList[ncols * nrow + ncol];
        auto semCpy = aVariableSemantic->clone();
        
        // Create a primitive variable for each element in the matrix
        // and set the base semantic.
        // The proper semantic will be set later from the semantic list according
        // to the running branch statement
        arrayVars.push_back(Core::VariableSPtr(createScalarVariable(domPtr, semCpy, aVarFactory)));
      }//ncols
    }//nrows
    
    // @note the following applies to matrices:
    // std::vector<std::vector<Core::VariableSPtr>> matrixVars;
    // Core::VariableSPtr(aVariableFactory->variableMatrix2D(matrixVars, aDescriptorVariable->releaseSemantic()));
    // For the time being we consider vectorized (1-D) matrices:
    // matrix[ i ][ j ] = array[ i*m + j ]
    return aVarFactory.variableArray(arrayVars, Core::VariableSemanticUPtr(aVariableSemantic));
  }//generateMatrixVariable
  
  static std::shared_ptr<DomainDescriptor<DomainType>> getDomainDeclExtensional(IR::IRDomDeclExt* aDom)
  {
    assert(aDom);
    
    // Domain descriptor
    auto domDescriptor = std::make_shared<DomainDescriptor<DomainType>>();
    
    // Set domain representation
    domDescriptor->setDomainRepresentaton(DomainDescriptor<DomainType>::DomainRepresentation::LIST);
    
    // Set domain type
    domDescriptor->setDomainType(Core::VariableTypeClass::VAR_TYPE_INTEGER);
    
    for (auto& c : aDom->getExtensionalDomain())
    {
      assert(c->getConst());
      assert(CLI::Utils::isInteger(c->getConst()->getName()));
      auto val = CLI::Utils::stoi(c->getConst()->getName());
      domDescriptor->addDomainElement(static_cast<DomainType>(val));
    }
    
    // Check if there is only one domain element to re-set
    // the representation as singleton domain
    if((domDescriptor->getDomainElements()).size() == 1)
    {
      domDescriptor->setDomainRepresentaton(DomainDescriptor<DomainType>::DomainRepresentation::SINGLETON);
    }
    
    return domDescriptor;
  }//getDomainDeclExtensional
  
  static std::shared_ptr<DomainDescriptor<DomainType>> getDomainDeclBounds(IR::IRDomDeclBounds* aDom)
  {
    assert(aDom);
    assert(aDom->getLowerBound());
    assert(aDom->getUpperBound());
    
    auto lb = aDom->getLowerBound()->getConst();
    auto ub = aDom->getUpperBound()->getConst();
    assert(lb);
    assert(ub);
    
    // Domain descriptor
    auto domDescriptor = std::make_shared<DomainDescriptor<DomainType>>();
    
    // Set domain type
    if (aDom->isBoolean())
    {
      domDescriptor->setDomainType(Core::VariableTypeClass::VAR_TYPE_BOOLEAN);
    }
    else
    {
      domDescriptor->setDomainType(Core::VariableTypeClass::VAR_TYPE_INTEGER);
    }
    
    assert(CLI::Utils::isInteger(lb->getName()));
    assert(CLI::Utils::isInteger(ub->getName()));
    auto vlb = CLI::Utils::stoi(lb->getName());
    auto vub = CLI::Utils::stoi(ub->getName());
    if (lb->isEqual(ub.get()))
    {
      // Singleton domain
      domDescriptor->setDomainRepresentaton(DomainDescriptor<DomainType>::DomainRepresentation::SINGLETON);
      domDescriptor->addDomainElement(static_cast<DomainType>(vlb));
    }
    else
    {
      // Bounds domain
      domDescriptor->setDomainRepresentaton(DomainDescriptor<DomainType>::DomainRepresentation::BOUNDS);
      domDescriptor->addDomainElement(static_cast<DomainType>(vlb));
      domDescriptor->addDomainElement(static_cast<DomainType>(vub));
    }
    
    return domDescriptor;
  }//getDomainDeclBounds
  
  static std::vector<std::shared_ptr<DomainDescriptor<DomainType>>> getDomainDescriptorList(IR::IRVarDecl* aVDecl)
	{
    // @note the only allowed type for domains is INT_64.
    // If other versions of OptiLab (current 1.0) will allow the clients to specify
    // different types of domains (e.g., double, struct, etc.), replace INT_64 with a
    // base class wrapper that represents a general domain type
    std::vector<std::shared_ptr<DomainDescriptor<DomainType>>> domainDescriptorList;
    
    for(std::size_t idx{0}; idx < aVDecl->getNumRhs(); ++idx)
    {
      auto eDom = aVDecl->rhs(idx);
      assert(eDom);
      
      auto declType = eDom->getDeclarationType();
      switch (declType)
      {
        case IR::IRDomDecl::DOM_DECL_BOUNDS:
          domainDescriptorList.push_back(getDomainDeclBounds(IR::IRDomDeclBounds::cast(eDom.get())));
          break;
        default:
          assert(declType == IR::IRDomDecl::DOM_DECL_EXTENSIONAL);
          domainDescriptorList.push_back(getDomainDeclExtensional(IR::IRDomDeclExt::cast(eDom.get())));
          break;
      }
    }
    
    // Return the list of domain descriptors
    return domainDescriptorList;
	}//getDomainDescriptorList
  
  static std::vector<std::size_t> getDomainDimensions(IR::IRVarDecl* aVDecl)
  {
    auto eVar = aVDecl->lhs();
    assert(eVar);
    
    auto vtype = (eVar->getType()).get();
    
    std::vector<std::size_t> dims;
    if(!IR::IRTypeArray::isa(vtype)) { return dims; }
    while(IR::IRTypeArray::isa(vtype))
    {
      dims.push_back(IR::IRTypeArray::cast(vtype)->getNumElements());
      vtype = (IR::IRTypeArray::cast(vtype)->getBaseType()).get();
    }
    
    if(dims.size() == 1)
    {
      // If only cols, set number of rows to be 1
      dims.insert(dims.begin(), 1);
    }
    
    return dims;
  }//getDomainDimensions
  
  VariableGenerator::VariableGenerator()
  : pVarFactory(new Core::VariableFactory())
  {
  }
  
	ModelObject* VariableGenerator::generateObject(IR::IRNode* aNode, const Model*)
	{
    if(!IR::IRVarDecl::isa(aNode)) return nullptr;

    // Use the IRVarDecl to generate the model object variable
    auto vdecl = IR::IRVarDecl::cast(aNode);
    
    // The variable declaraton is used to generate a Variable Core object
    Core::Variable* variable{nullptr};
    
    // Get the list of the semantics of the variable
    // @note the semantic list can be empty, for example for non reachable variables.
    // We need here at least the base semantic
    auto semanticBase = getBaseSemantic(vdecl->lhs());
    auto semanticList = getSemanticList(vdecl);
    assert(semanticBase);
    
    // Get the list of domain descriptors.
    // @note a list is required since composite variables (e.g., matrices)
    // may be represented by different domain descriptors
    auto domainDescriptorList = getDomainDescriptorList(vdecl);
    
    // Get dimensions of the domain of the variable
    auto dims = getDomainDimensions(vdecl);
    if(dims.empty())
    {
      // Return a variable object on a scalar variable using the base semantic to create
      // the semantic of the variable and the (only) domain descriptor to generate
      // the domain of the variable
      assert(domainDescriptorList.size() == 1);
      variable = createScalarVariable(domainDescriptorList[0], semanticBase, getVarFactory());
    }
    else
    {
      // Return a variable object on a matrix variable
      variable = generateMatrixVariable(domainDescriptorList, dims, semanticBase, getVarFactory());
    }
    assert(variable);
    
    // Generate the model object
    auto varObject = new ModelVariable(ModelVariable::VariableSPtr(variable));
    
    // Set the semantic list
    varObject->setSemanticList(semanticList);
    
    // Return the model onbject
    return varObject;
	}//generateObjectDescriptor

}// end namespace Model
