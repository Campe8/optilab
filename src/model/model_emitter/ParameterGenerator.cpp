// Self first
#include "ParameterGenerator.hpp"

#include "ModelParameter.hpp"
#include "ConstraintGenerator.hpp"

// IR
#include "IRExprAssign.hpp"
#include "IRExprStdVar.hpp"

// Model and model objects
#include "Model.hpp"

#include <cassert>

namespace Model {
  
  static std::shared_ptr<Core::ConstraintParameter> getParameter(const std::shared_ptr<IR::IRExpr>& aExpr,
                                                                 const Model* aModel,
                                                                 Core::ConstraintParameterFactory& aCPF)
  {
    assert(aModel);
    return getConstraintParameter(aExpr, aModel, aCPF);
  }//getParameter
  
  ParameterGenerator::ParameterGenerator()
  : pParamFactory(new Core::ConstraintParameterFactory())
  {
  }
  
	ModelObject* ParameterGenerator::generateObject(IR::IRNode* aNode, const Model* aModel)
	{
    assert(IR::IRExprAssign::isa(aNode));
    
    auto assignExpr = IR::IRExprAssign::cast(aNode);
    assert(IR::IRExprStdVar::isa((assignExpr->lhs()).get()));

    auto varExpr = IR::IRExprStdVar::cast((assignExpr->lhs()).get());
    auto paramID = varExpr->getVar()->getName();
    
    // Generate model object and return
    return new ModelParameter(paramID, getParameter(assignExpr->rhs(), aModel, *pParamFactory));
	}//generateObject

}// end namespace Model
