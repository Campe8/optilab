// Self first
#include "SearchGenerator.hpp"

#include "ModelSearch.hpp"

// IR
#include "IRSrcDecl.hpp"

// Model and model objects
#include "Model.hpp"
#include "ModelVariable.hpp"

// Search information and descriptors
#include "SearchInformation.hpp"
#include "DFSDescriptor.hpp"
#include "GeneticDescriptor.hpp"

#include <cassert>

namespace Model {
  
  static SearchDescriptor::SemanticList getSemanticList(const std::string& aSearchID,
                                                        const std::vector<std::string>& aDepVars,
                                                        const Model* aModel)
  {
    SearchDescriptor::SemanticList semanticList;
    
    for(auto& vID : aDepVars)
    {
      // Get the variable object from the model.
      // @note the variable must be present in the model
      auto varObj = getModelObjectBySID(aModel, vID);
      assert(ModelVariable::isa(varObj));
      
      for(const auto& semantic : ModelVariable::cast(varObj)->getSemanticList())
      {
        if(semantic.first == aSearchID)
        {
          semanticList.push_back({vID, semantic.second});
          break;
        }
      }
    }
    
    return semanticList;
  }//getSemanticList
  
  std::shared_ptr<SearchDescriptor> getSearchDescriptor(const std::shared_ptr<SearchInformation>& aSrcInfo)
  {
    assert(aSrcInfo);
    
    std::shared_ptr<SearchDescriptor> srcDescriptor{nullptr};
    auto srcInfoType = aSrcInfo->getSearchInfoType();
    if(srcInfoType == SearchInformation::SearchInformationType::SI_DFS)
    {
      srcDescriptor = std::make_shared<DFSDescriptor>();
    }
    else
    {
      assert(srcInfoType == SearchInformation::SearchInformationType::SI_GENETIC);
      srcDescriptor = std::make_shared<GeneticDescriptor>();
    }
    
    // Initalize the search descriptor with the given search information
    srcDescriptor->initSearchDescriptor(aSrcInfo.get());

    return srcDescriptor;
  }//getSearchDescriptor
  
	ModelObject* SearchGenerator::generateObject(IR::IRNode* aNode, const Model* aModel)
	{
    assert(aModel);
    if (!IR::IRSrcDecl::isa(aNode)) return nullptr;
    
    // Use the IRSrcDecl to generate the model object variable
    auto sdecl = IR::IRSrcDecl::cast(aNode);
    assert(sdecl);
    
    // Get search ID
    auto searchID = sdecl->getID();
    
    // Create a search descriptor for the search object
    auto searchDescriptor = getSearchDescriptor(sdecl->getSearchInformation());
    
    // Get the list of semantically dependent variables
    auto depVarList = sdecl->getSemanticDependentVarList();
    searchDescriptor->setSemanticList(getSemanticList(searchID, depVarList, aModel));
    
    // Generate and return the model object
    return new ModelSearch(searchID, searchDescriptor);
	}//generateObjectDescriptor

}// end namespace Model
