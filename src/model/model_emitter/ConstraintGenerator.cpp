// Self first
#include "ConstraintGenerator.hpp"

#include "ModelConstraint.hpp"

// IR
#include "IRConDecl.hpp"
#include "IRExprVar.hpp"
#include "IRExprConst.hpp"
#include "IRExprRef.hpp"
#include "IRExprMatrixSub.hpp"
#include "IRTypeArray.hpp"
#include "IRSymbolConstArray.hpp"
#include "IRSymbolConstInteger.hpp"
#include "IRSymbolConstBoolean.hpp"
#include "IRSymbolVar.hpp"
#include "IRSymbolFactory.hpp"

// Model and model objects
#include "Model.hpp"
#include "ModelVariable.hpp"
#include "ModelParameter.hpp"
#include "VariableArray.hpp"
#include "ConstraintParameterDomainArray.hpp"

// Utilities
#include "ModelUtils.hpp"

#include <cassert>
#include <utility> // for std::pair

using ConParamSPtr = std::shared_ptr<Core::ConstraintParameter>;
using Scope = std::vector<ConParamSPtr>;

namespace {
  // Global flag for optimization constraints
  bool OptimizationConstraint;
  
  void setConstraintOptimizationCheck(bool aCheck) {OptimizationConstraint = aCheck; }
  bool getConstraintOptimizationCheck() { return OptimizationConstraint; }
}  // end namespace

namespace Model {
  
  static ConParamSPtr getParamFromSymbol(const std::shared_ptr<IR::IRSymbol>& aSymb,
                                         const Model* aModel,
                                         Core::ConstraintParameterFactory& aCPF);
  
  static ConParamSPtr getIntParam(const IR::IRSymbolConstInteger* aSymb,
                                  Core::ConstraintParameterFactory& aCPF);
  
  static ConParamSPtr getBoolParam(const IR::IRSymbolConstBoolean* aSymb,
                                   Core::ConstraintParameterFactory& aCPF);
  
  static Core::ConstraintId getConstraintType(IR::IRConDecl* aDecl)
  {
    assert(aDecl->fcnCon());
    assert(aDecl->fcnCon()->getFcn());
    auto cID = aDecl->fcnCon()->getFcn()->getName();
    return Utils::ConstraintHelper::getInstance().stringIDToCoreID(cID);
  }//getConstraintType
  
  static Core::PropagatorStrategyType getPropagatorType(IR::IRConDecl* aDecl)
  {
    auto pType = aDecl->getPropagationType();
    switch (pType)
    {
      case IR::IRConDecl::PropagationType::PT_BOUNDS:
        return Core::PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_BOUND;
      default:
        assert(pType == IR::IRConDecl::PropagationType::PT_DOMAIN);
        return Core::PropagatorStrategyType::PROPAGATOR_STRATEGY_TYPE_DOMAIN;
    }
  }//getPropagatorType
  
  ConParamSPtr getParamFromSymbol(const std::shared_ptr<IR::IRSymbol>& aSymb, const Model* aModel,
                                  Core::ConstraintParameterFactory& aCPF)
  {
    assert(aSymb);
    if (IR::IRSymbolConstInteger::isa(aSymb.get()))
    {
      return getIntParam(IR::IRSymbolConstInteger::cast(aSymb.get()), aCPF);
    }
    else if (IR::IRSymbolConstBoolean::isa(aSymb))
    {
      return getBoolParam(IR::IRSymbolConstBoolean::cast(aSymb.get()), aCPF);
    }
    else
    {
      assert(IR::IRSymbolVar::isa(aSymb.get()));
      assert(aSymb->getModule()->getSymbolTable());
      
      // Retrieve the IR expression linked to this symbol
      const auto& entry = aSymb->getModule()->getSymbolTable()->lookup(aSymb->getName());
      
      assert(entry && (entry->getLinkSet()).size() == 1);
      const auto& expr = *((entry->getLinkSet()).begin());
      
      // Recursively create the parameter descriptor
      return getConstraintParameter(expr, aModel, aCPF);
    }
  }//getDescriptorFromSymbol
  
  static ConParamSPtr createCompositeParamFromParamArray(const std::vector<ConParamSPtr>& aParamArry,
                                                         const Model* aModel,
                                                         Core::ConstraintParameterFactory& aCPF)
  {
    using PType = Core::ConstraintParameter::ConstraintParameterClass;
    /*
     * Type of leaf parameters:
     * CON_PARAM_CLASS_VARIABLE
     * CON_PARAM_CLASS_DOMAIN
     * CON_PARAM_CLASS_DOMAIN_ARRAY
     * CON_PARAM_CLASS_DOMAIN_MATRIX_2D
     * The input vector should contain only leaf parameters, i.e.,
     * i.e., only domain or variable parameters.
     */
    
    // Create the domain array that will be the parameter to return
    std::vector<Core::DomainSPtr> domainArray;
    
    // Array of indeces of the domains to set as subject domains
    std::vector<std::size_t> subjectDomains;
    
    // Index for subject domains
    std::size_t idx{0};
    for (auto& param : aParamArry)
    {
      auto paramClass = param->getParameterClass();
      if (paramClass == PType::CON_PARAM_CLASS_VARIABLE)
      {
        // Get the variable from the constraint parameter and, from the variable,
        // retrieve its domain
        assert(Core::ConstraintParameterVariable::isa(param.get()));
        auto paramVar = Core::ConstraintParameterVariable::cast(param.get());
        for(auto& dom : *(paramVar->getVariable()->domainList()))
        {
          domainArray.push_back(dom);
          subjectDomains.push_back(idx++);
        }
      }
      else
      {
        assert(paramClass == PType::CON_PARAM_CLASS_DOMAIN);
        auto paramDom = Core::ConstraintParameterDomain::cast(param.get());
        domainArray.push_back(paramDom->getDomain());
        idx++;
      }
    }
    
    // Create the constraint parameter on the array of domains
    auto constraintParameterArray = aCPF.constraintParameterDomain(domainArray);
    
    // Set the variable domains as subjects for the constraint owning this parameter
    assert(Core::ConstraintParameterDomainArray::isa(constraintParameterArray.get()));
    auto cpda = Core::ConstraintParameterDomainArray::cast(constraintParameterArray.get());
    for(auto domIdx : subjectDomains)
    {
      assert(domIdx < cpda->size());
      cpda->setAsSubjectGivenIndex(domIdx);
    }
    
    return constraintParameterArray;
  }//createCompositeParamFromParamArray
  
  // Utility function: returns a parameter array
  static ConParamSPtr getArrayParam(IR::IRSymbolConstArray* aArray, const Model* aModel,
                                    Core::ConstraintParameterFactory& aCPF)
  {
    assert(aArray);
    if(aArray->size() == 0) return nullptr;
    
    // The input array contains IR symbols of variables or subscript variables.
    // This must be taken into account while creating the parameter
    std::vector<ConParamSPtr> paramArray;
    for (std::size_t idx{ 0 }; idx < aArray->size(); ++idx)
    {
      paramArray.push_back(getParamFromSymbol(aArray->getSymbol(idx), aModel, aCPF));
    }
    
    return createCompositeParamFromParamArray(paramArray, aModel, aCPF);
  }//getParamArrayDesc
  
  ConParamSPtr getIntParam(const IR::IRSymbolConstInteger* aSymb,
                           Core::ConstraintParameterFactory& aCPF)
  {
    assert(aSymb);
    auto val = aSymb->getInt();
    
    // Create new constraint parameter
    return ConParamSPtr(aCPF.constraintParameterDomain(std::shared_ptr<Core::Domain>((aCPF.domainFactory())->domainInteger(val))));
  }//getIntParamDesc
  
  ConParamSPtr getBoolParam(const IR::IRSymbolConstBoolean* aSymb,
                            Core::ConstraintParameterFactory& aCPF)
  {
    assert(aSymb);
    auto val = aSymb->getBool();

    // Create new constraint parameter
    return ConParamSPtr(aCPF.constraintParameterDomain(std::shared_ptr<Core::Domain>((aCPF.domainFactory())->domainBoolean(val))));
  }//getBoolParam
  
  static ConParamSPtr getVarParam(const std::string aVarID, const Model* aModel,
                                  Core::ConstraintParameterFactory& aCPF)
  {
    auto vobj = getModelObjectBySID(aModel, aVarID);
    assert(ModelVariable::isa(vobj));
    
    if (ModelVariable::cast(vobj)->variable()->variableSemantic()->getSemanticClassType() ==
        Core::VariableSemanticClassType::VAR_SEMANTIC_CLASS_OBJECTIVE)
    {
      setConstraintOptimizationCheck(true);
    }
    auto param = aCPF.constraintParameterVariable(ModelVariable::cast(vobj)->variable()).release();
    return ConParamSPtr(param);
  }//getVarParam
  
  static ConParamSPtr getRefParam(const std::string aRefID, const Model* aModel,
                                  Core::ConstraintParameterFactory& aCPF)
  {
    auto pobj = getModelObjectBySID(aModel, aRefID);
    assert(ModelParameter::isa(pobj));
    
    // Return the constraint parameter contained in the model object parameter
    return ModelParameter::cast(pobj)->parameter();
  }//getRefParam
  
  static ConParamSPtr getSubscrParam(IR::IRExprMatrixSub* aSubExpr, const Model* aModel,
                                     Core::ConstraintParameterFactory& aCPF)
  {
    // Get matrix name
    auto mat = aSubExpr->getMatrix();
    assert(IR::IRExprVar::isa(mat.get()));
    auto varID = IR::IRExprVar::cast(mat.get())->getVar()->getName();
    
    // Get subscript
    auto sub = aSubExpr->getSubscript();
    assert(IR::IRExprConst::isa(sub.get()));
    assert(IR::IRSymbolConstInteger::isa((IR::IRExprConst::cast(sub.get())->getConst()).get()));
    auto subIdx =
    static_cast<std::size_t>(IR::IRSymbolConstInteger::cast((IR::IRExprConst::cast(
                                                          sub.get())->getConst()).get())->getInt());
    
    auto vobj = getModelObjectBySID(aModel, varID);
    assert(ModelVariable::isa(vobj));
    
    auto var = (ModelVariable::cast(vobj)->variable()).get();
    assert(Core::VariableArray::isa(var));
    assert(subIdx < Core::VariableArray::cast(var)->size());
    
    return aCPF.constraintParameterVariable(Core::VariableArray::cast(var)->at(subIdx));
  }//getSUbscrParam
  
  static ConParamSPtr getParamFromConstSymbol(const IR::IRExprConst::SymbConstSPtr& aSymb,
                                              const Model* aModel,
                                              Core::ConstraintParameterFactory& aCPF)
  {
    assert(aSymb);
    auto sType = aSymb->getType();
    auto sTypeID = sType->getTypeID();
    
    switch (sTypeID)
    {
      case IR::IRTypeID::IR_TYPE_INTEGER:
        return getIntParam( IR::IRSymbolConstInteger::cast(aSymb.get()), aCPF );
      case IR::IRTypeID::IR_TYPE_BOOLEAN:
        return getBoolParam( IR::IRSymbolConstBoolean::cast(aSymb.get()), aCPF );
      default:
        assert(sTypeID == IR::IRTypeID::IR_TYPE_ARRAY);
        return getArrayParam( IR::IRSymbolConstArray::cast(aSymb.get()), aModel, aCPF );
    }
  }//getParamFromConstSymbol
  
  ConParamSPtr getConstraintParameter(const IR::IRExprFcn::IRExprSPtr& aExpr, const Model* aModel,
                                      Core::ConstraintParameterFactory& aCPF)
  {
    assert(aExpr);
    
    if (IR::IRExprVar::isa(aExpr.get()))
    {
      // If the expression is a variable expression,
      // create a constraint varID parameter
      auto e = IR::IRExprVar::cast(aExpr.get());
      assert(e && e->getVar());
      
      return getVarParam(e->getVar()->getName(), aModel, aCPF);
    }
    else if (IR::IRExprStdVar::isa(aExpr.get()))
    {
      // If the expression is a standard variable expression,
      // it represents a model parameter
      auto e = IR::IRExprStdVar::cast(aExpr.get());
      assert(e && e->getVar());
      
      auto vobj = getModelObjectBySID(aModel, e->getVar()->getName());
      assert(ModelParameter::isa(vobj));
      
      // Return the constraint parameter hold by the model parameter
      return ModelParameter::cast(vobj)->parameter();
    }
    else if (IR::IRExprRef::isa(aExpr.get()))
    {
      // Reference expressions create a parameter expression
      auto refSymb = getReferencedSymbol(IR::IRExprRef::cast(aExpr.get()));
      assert(refSymb);
      
      return getRefParam(refSymb->getName(), aModel, aCPF);
    }
    else if (IR::IRExprMatrixSub::isa(aExpr.get()))
    {
      auto sExpr = IR::IRExprMatrixSub::cast(aExpr.get());
      return getSubscrParam(sExpr, aModel, aCPF);
    }
    else
    {
      // All the other parameters are constant parameters,
      // switch the generation according to the const symbol
      assert(IR::IRExprConst::isa(aExpr.get()));
      auto symb = IR::IRExprConst::cast(aExpr.get())->getConst();
      return getParamFromConstSymbol(symb, aModel, aCPF);
    }
  }//getConstraintParameter
  
  // Returns a pair <scope, optimization>
  static Scope getConstraintScope(IR::IRConDecl* aDecl, const Model* aModel,
                                  Core::ConstraintParameterFactory& aCPF)
  {
    Scope scope;
    auto fcn = aDecl->fcnCon();
    assert(fcn);
    
    // Reset constraint optimization flag
    setConstraintOptimizationCheck(false);
    
    scope.reserve(fcn->numArgs());
    for (std::size_t idx{ 0 }; idx < fcn->numArgs(); ++idx)
    {
      auto argExpr = fcn->getArg(idx);
      assert(argExpr);
      
      // Create the constraint parameter base on the
      // corresponding IRExpr argument
      scope.push_back(getConstraintParameter(argExpr, aModel, aCPF));
    }//for
    
    return scope;
  }//getConstraintScope
  
  ConstraintGenerator::ConstraintGenerator()
  : pParamFactory(new Core::ConstraintParameterFactory())
  , pConstraintFactory(new Core::ConstraintFactory())
  {
  }
  
  Core::ConstraintSPtr ConstraintGenerator::generateConstraint(Core::ConstraintId aConID,
                                                               const std::vector<ConParamSPtr>& aScope,
                                                               Core::PropagatorStrategyType aPropType,
                                                               bool aOptimization)
  {
    auto constraint = getConstraintFactory().constraint(aConID, aScope, aPropType);
    if (aOptimization)
    {
      // Set the constraint as an optimization constraint.
      // @note theoretically, this should be set by the constructor of the constraint itself
      // analyzing its parameters.
      // This cannot be done (at the moment) since list of variables (that can be optimization
      // variables) are translated into list of domains as parameters and domains do not
      // hold any information about the semantics of the variables their are linked to
      constraint->setOptimizationConstraint();
    }
    return Core::ConstraintSPtr(constraint);
  }//generateConstraint
  
	ModelObject* ConstraintGenerator::generateObject(IR::IRNode* aNode, const Model* aModel)
	{
    assert(aModel);
    if(!IR::IRConDecl::isa(aNode)) return nullptr;
    
    // Use the IRConDecl to generate the model object variable
    auto conDecl = IR::IRConDecl::cast(aNode);
    
    // Get constraint type
    auto cType = getConstraintType(conDecl);
    
    // Get propagator strategy type
    auto pType = getPropagatorType(conDecl);
    
    // Get descriptors for constraint parameters (i.e., the arguments of the constraint)
    auto cscope = getConstraintScope(conDecl, aModel, getParamFactory());
    
    // Set the context ID which is carried by the constraint declaration node
    // on its ID if non empty
    return new ModelConstraint(conDecl->getID(), generateConstraint(cType, cscope, pType,
                                                                  getConstraintOptimizationCheck()));
	}//generateObjectDescriptor

}// end namespace Model
