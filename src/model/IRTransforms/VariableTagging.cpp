// Self first
#include "VariableTagging.hpp"

// IR
#include "IRContext.hpp"
#include "IRVarDecl.hpp"
#include "IRConDecl.hpp"
#include "IRSrcDecl.hpp"
#include "IRExprVar.hpp"
#include "IRExprMatrixSub.hpp"
#include "IRExprRef.hpp"
#include "IRSymbolConstArray.hpp"
#include "IRSymbolVar.hpp"
#include "IRSymbolConstNumeric.hpp"
#include "IRUtils.hpp"

#include <algorithm>
#include <vector>
#include <unordered_set>
#include <cassert>

namespace IR {
  namespace Transforms {
    
    static IR::IRExprVar* getExprVarFromID(const std::string& aID, const IR::IRModuleSPtr& aMod)
    {
      auto expr = IR::Utils::lookupExprByName(aID, aMod->getContext());
      if(auto var = IR::IRExprVar::cast(expr.get()))
      {
        return var;
      }
      
      if(auto mat = IR::IRExprMatrixSub::isa(expr.get()))
      {
        // If  the expression is a matrix subscript for a matrix variable,
        // return the pointer to the matrix variable
        auto matExpr = IR::IRExprMatrixSub::cast(expr.get())->getMatrix();
        assert(IR::IRExprVar::isa(matExpr.get()));
        
        return static_cast<IR::IRExprVar*>(matExpr.get());
      }
      
      return nullptr;
    }//getExprVarFromID
    
    // Tag the variables in the search declaration "aSrcDecl"
    static void tagVariablesOnSearchDeclaration(const std::shared_ptr<IRSrcDecl>& aSrc)
    {
      assert(aSrc);
      for (std::size_t vidx{ 0 }; vidx < aSrc->numBranchingVars(); ++vidx)
      {
        // Create a new search semantic based on "aSrc"
        auto searchSemantic = std::make_shared<VarSearchSemantic>(aSrc);
        
        // Set input order according to the ordered list of branching variables
        searchSemantic->setInputOrder(vidx);
        
        // Set the reachability and branching policy to true since the variable is a branching variable
        searchSemantic->setReachability();
        searchSemantic->setBranchingPolicy(true);
        
        // Add the semantic to the variable
        aSrc->getBranchingVar(vidx)->addSearchSemantic(searchSemantic);
      }
    }//tagVariablesOnSearchDeclaration
    
    // Tags as branching variables all the variables in the search module
    static void tagBranchingVariables(const std::shared_ptr<IRModule>& aSrcMod)
    {
      assert(!aSrcMod->isGlobalModule());
      auto blk = aSrcMod->getBlock();
      
      // Mark the variables according to each search declaration
      std::size_t idxOrder{0};
      for(auto& srcDecl : blk->preOrderVisit<IRSrcDecl>())
      {
        tagVariablesOnSearchDeclaration(srcDecl);
        
        // Set the maximum input order
        idxOrder = idxOrder > srcDecl->numBranchingVars() ? idxOrder : srcDecl->numBranchingVars();
      }
    }//tagBranchingVariables
    
		// Collects the list of unique IDs of the branching variables
		static void collectBranchingList(const std::shared_ptr<IRSrcDecl>& aSrc, std::vector<std::string>& aList)
		{
      for (std::size_t vidx{ 0 }; vidx < aSrc->numBranchingVars(); ++vidx)
      {
        auto id = aSrc->getBranchingVar(vidx)->getVar()->getName();
        if(std::find(aList.begin(), aList.end(), id) == aList.end()) aList.push_back(id);
      }
		}//collectBranchingSet

    // Returns the "IDs" encoded in the given symbol, if any
    static std::vector<std::string> getConArgID(IRSymbol* aSymb)
    {
      if(IRSymbolConstNumeric::isa(aSymb)) return {""};
      if(IRSymbolVar::isa(aSymb)) return { IRSymbolVar::cast(aSymb)->getName() };
      
      assert(IRSymbolConstArray::isa(aSymb));
      auto array = IRSymbolConstArray::cast(aSymb);
      
      std::vector<std::string> ids;
      for(std::size_t idx{0}; idx < array->size(); ++idx)
      {
        for(const auto& id : getConArgID((array->getSymbol(idx)).get()))
        {
          if(!id.empty()) ids.push_back(id);
        }
      }
      
      return ids;
    }//getConArgID
    
		// Returns the "IDs" of the constraint argument "aArg"
    static std::vector<std::string> getConArgID(IRExpr* aArg)
		{
      if (IRExprVar::isa(aArg))
			{
        return { IRExprVar::cast(aArg)->getVar()->getName() };
			}
      else if (IRExprStdVar::isa(aArg))
      {
        return { IRExprStdVar::cast(aArg)->getVar()->getName() };
      }
			else if (IRExprMatrixSub::isa(aArg))
			{
        return { getConArgID((IRExprMatrixSub::cast(aArg)->getMatrix()).get()) };
			}
      else if (IRExprConst::isa(aArg))
      {
        return getConArgID((IRExprConst::cast(aArg)->getConst()).get());
      }
			else if (IRExprRef::isa(aArg))
			{
        return {""};
				//assert(false);
			}
      assert(false);
      return {""};
		}//getConArgID

		static std::vector<std::string> getAllScopeIDs(IRExprFcn* aFcn)
		{
			std::unordered_set<std::string> set;
			for (std::size_t argIdx{ 0 }; argIdx < aFcn->numArgs(); ++argIdx)
			{
				auto arg = (aFcn->getArg(argIdx)).get();
        for(const auto& id : getConArgID(arg))
        {
          if(!id.empty()) set.insert(id);
        }
			}
			return {set.begin(), set.end()};
		}//getAllScopeIDs

		// Returns the variable in "aExpr" or nullptr if "aExpr"
		// does not contain any scope variable
    /*
		static IRExprVar* getVariableFromExpr(IRExpr* aExpr)
		{
			if (!aExpr) { return nullptr; }
			if (IRExprVar::isa(aExpr))
			{
				return IRExprVar::cast(aExpr);
			}
			if (IRExprMatrixSub::isa(aExpr))
			{
				auto mat = ((IRExprMatrixSub::cast(aExpr)->getMatrix())).get();
				return getVariableFromExpr(mat);
			}
      if (IRExprConst::isa(aExpr))
      {
        assert(false);
      }
			if (IRExprRef::isa(aExpr))
			{
				assert(false);
			}
      assert(false);
			return nullptr;
		}//getVariableFromExpr
     */

    // Tag the scope of this constraint as reachable w.r.t. the current search declaration
    static void tagReachableScope(const std::shared_ptr<IRSrcDecl>& aSrcMod,
                                  const std::vector<std::string>& aScope, std::size_t& aInputOrder)
		{
      for(const auto& id : aScope)
      {
        auto varExpr = getExprVarFromID(id, aSrcMod->getBlock()->getModule());
        if(!varExpr)
        {
          // Continue if the expression var is nullptr.
          // This can happen if the id is not identifying a variable but,
          // for example, it identifies a standard variable
          continue;
        }
        assert(varExpr);
        
        auto srcSemantic = varExpr->lookupSearchSemantic(aSrcMod);
        if(!srcSemantic)
        {
          // Create a new search semantic if the current variable
          // does not contain a search semantic for the current search declaration
          srcSemantic = std::make_shared<VarSearchSemantic>(aSrcMod);
          
          // A new search semantic has been created, which means that the
          // current variable wasn't in the branching set,
          // set input order and increase the counter
          srcSemantic->setInputOrder(aInputOrder++);
          varExpr->addSearchSemantic(srcSemantic);
        }
        srcSemantic->setReachability();
      }// for
		}//tagReachableScope

		// Runs reachability check from "aSet" and mark reachable variables
		static void runReachabilityCheck(const std::shared_ptr<IRSrcDecl>& aSrcMod, const std::shared_ptr<IRModule>& aConMod, std::vector<std::string>& aReachableList)
		{
      if (aReachableList.empty()) return;
      
			auto blk = aConMod->getBlock();

			// Set of already visited constraints
			std::unordered_set<IRExprFcn*> visitedConSet;

			// Change flag
			bool changed{ true };
      
      // Input order on semantics
      std::size_t inputOrder = aSrcMod->numBranchingVars();
			const auto& clist = blk->preOrderVisit<IRConDecl>();
			while (changed)
			{
        if(visitedConSet.size() == clist.size())
        {
          // If all constraints have been visited, break
          break;
        }
        
				// This can be the last iteration
				changed = false;
				for (const auto& con : clist)
				{
					auto fcn = (con->fcnCon()).get();

					// Continue if the current constraint has been already visited and set as reachable
					if (visitedConSet.find(fcn) != visitedConSet.end()) { continue; }

					for (std::size_t argIdx{ 0 }; argIdx < fcn->numArgs(); ++argIdx)
					{
						auto arg = (fcn->getArg(argIdx)).get();
						auto idList = getConArgID(arg);

						// Empty ID (for example, for constants) continue
						if (idList.empty()) { continue; }

						// If the id is an id of one of the reachable variables:
						// 1 - add all the scope ids to the set of reachable variables;
						// 2 - mark the variables in the scope of this constraint as reachable
						// 3 - add this constraint as visited constraint
						// 4 - set the change flag to true
						// 5 - break the current scope exploration
            for(const auto& id : idList)
            {
              // The id is in the list of reachable variables
              if (std::find(aReachableList.begin(), aReachableList.end(), id) != aReachableList.end())
              {
                // 1 - add the scope variables into the set of reachable variables
                auto scopeIDs = getAllScopeIDs(fcn);
                for(const auto& scopeVarID : scopeIDs)
                {
                  if (std::find(aReachableList.begin(), aReachableList.end(), scopeVarID) == aReachableList.end())
                    aReachableList.push_back(scopeVarID);
                }
                
                // 2 - mark the scope as reachable
                tagReachableScope(aSrcMod, scopeIDs, inputOrder);
                
                // 3 - set visited constraint
                visitedConSet.insert(fcn);
                
                // 4 - change flag
                changed = true;
                
                // 5 - break current for loop
                break;
              }
            }//for
					}// argIdx
				}//con
			}// while
		}//runReachabilityCheck

		// Tags as reachable variables all the variables  in the search module
		static void tagReachableVariables(const std::shared_ptr<IRModule>& aSrcMod,
                                      const std::shared_ptr<IRModule>& aConMod)
		{
			// Run on all the search declarations
      auto blk = aSrcMod->getBlock();
      for(auto& srcDecl : blk->preOrderVisit<IRSrcDecl>())
      {
        // For each search declaration collect the set of branching variables
        std::vector<std::string> branchingList;
        collectBranchingList(srcDecl, branchingList);
        
        // Run a reachability test to tag reachable variables w.r.t. the current search declaration
        runReachabilityCheck(srcDecl, aConMod, branchingList);
      }
		}//tagReachableVariables

    void VariableTagging::transformModule(std::shared_ptr<IRModule>& aModule) const
    {
      assert(aModule);
      auto ctx = aModule->getContext();
      
			{
				// Get search module
				auto mdlSrcName = IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_SEARCH);
				auto srcMod = ctx->globalModule()->getSubModule(mdlSrcName);
				assert(srcMod);

				// Tag branching variables
				tagBranchingVariables(srcMod);
			}
			
			{
				// Get variable and constraint module
        auto mdlSrcName = IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_SEARCH);
				auto mdlConName = IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_CONSTRAINT);
        auto srcMod = ctx->globalModule()->getSubModule(mdlSrcName);
				auto conMod = ctx->globalModule()->getSubModule(mdlConName);
				assert(srcMod && conMod);

				// Tag reachable variables
				tagReachableVariables(srcMod, conMod);
			}
    }//transformModule
    
    std::string VariableTagging::transformName() const
    {
      return "VariableTagging";
    }//transformName
    
  }// end namespace Transforms
}// end namespace IR
