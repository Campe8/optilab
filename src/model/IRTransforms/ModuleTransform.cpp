// Self first
#include "ModuleTransform.hpp"

#include <cassert>

namespace IR {
	namespace Transforms {

		void ModuleTransform::apply(std::shared_ptr<IRModule>& aModule) const
		{
			assert(aModule);
			transformModule(aModule);
		}//apply

	}// end namespace Transforms
}// end namespace IR
