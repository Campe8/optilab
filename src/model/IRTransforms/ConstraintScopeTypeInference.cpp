// Self first
#include "ConstraintScopeTypeInference.hpp"

#include "IRContext.hpp"
#include "IRConDecl.hpp"
#include "IRExprVar.hpp"
#include "IRExprConst.hpp"
#include "IRSymbolConstInteger.hpp"
#include "IRTypeFunction.hpp"
#include "IRConstructionFacade.hpp"

#include "ModelUtils.hpp"

#include <cassert>

namespace IR {
  namespace Transforms {
    
    // Gets the last parameter of "aFcn" and sets its type to Boolean
    // if it is an ExprConst.
    // @note "aFcn" must be an ExprFcn of a reif constraint
    static void setReifType(const std::shared_ptr<IRExprFcn>& aFcn, IRConstructionFacade& aCF)
    {
      assert(aFcn->numArgs() - 1 > 0);
      
      auto reifArgIdx = aFcn->numArgs() - 1;
      auto reifArg = aFcn->getArg(reifArgIdx);
      assert(IRExprConst::isa(reifArg.get()));
      assert(IRSymbolConstInteger::isa((IRExprConst::cast(reifArg.get())->getConst()).get()));
      
      // Create new function type and set it to the IRExpr
      // @note once a fcn type is created it cannot be modified
      auto tf = aFcn->getBlock()->getModule()->getContext()->typeFactory();
      
      std::vector<IRTypeSPtr> reifTypeArgs;
      for(std::size_t tIdx{0}; tIdx < reifArgIdx; ++tIdx)
      {
        reifTypeArgs.push_back(aFcn->getArgType(tIdx));
      }
      // Last type is Boolean for reif parameter
      reifTypeArgs.push_back(tf->typeBoolean());
      auto reifFcnType = tf->typeFunctionConstraint(reifTypeArgs);
      
      // Create a new symbol fcn and unlink type
      auto symbFcn = aCF.symbolFactory()->symbolFcn(aFcn->getFcn()->getName(), reifFcnType);
      symbFcn->unlinkType();
      
      // Reset symbol of fcn expr
      // Reset symbol of fcn expr
      auto constExpr = aCF.exprConst(symbFcn);
      aFcn->setFcn(constExpr);
    }//setReifType
    
    static void setReifFcnArgs(const std::shared_ptr<IRExprFcn>& aFcn, const std::vector<std::shared_ptr<IRExpr>>& aArgs, IRConstructionFacade& aCF)
    {
      assert(aFcn->numArgs() == aArgs.size());
      assert(aArgs.size() > 1);
      
      // Set the function arguments
      auto reifIdx = aArgs.size() - 1;
      for(std::size_t idx{0}; idx < reifIdx; ++idx)
      {
        aFcn->setArg(idx, aArgs[idx]);
      }
      
      auto reifArg = aArgs[reifIdx];
      assert(IRExprConst::isa(reifArg.get()));
      
      // Set a new bool expr for the reif parameter
      auto symbConst = IRSymbolConstInteger::cast((IRExprConst::cast(reifArg.get())->getConst()).get());
      assert(symbConst);
      
      IRExprFcn::IRExprSPtr reifExpr{nullptr};
      if(symbConst->getName() == "0")
      {
        reifExpr = aCF.exprConst(aCF.symbolFactory()->symbolConstBoolean(false));
      }
      else
      {
        reifExpr = aCF.exprConst(aCF.symbolFactory()->symbolConstBoolean(true));
      }
      
      aFcn->setArg(reifIdx, reifExpr);
    }//setReifFcnArgs
    
    static void setTypesOnFcnScope(const std::shared_ptr<IRExprFcn>& aFcn, IRConstructionFacade& aCF)
    {
      assert(aFcn);
      
      // 1 - Set Boolean types on reif constraints for reif parameters
      if(Model::Utils::ConstraintHelper::getInstance().isConstraintReified(aFcn->getFcn()->getName()))
      {
        if(IRExprVar::isa(aFcn->getArg(aFcn->numArgs() - 1).get()))
        {
          // Return if the reif parameter is an expr var
          return;
        }
          
        // a) Get the fcn expr arguments
        std::vector<std::shared_ptr<IRExpr>> fcnArgs;
        for(std::size_t idx{0}; idx < aFcn->numArgs(); ++idx)
        {
          fcnArgs.push_back(aFcn->getArg(idx));
        }
        
        // b) Change expr fcn symbol and type.
        //    @note this resets all the fcn arguments to nullptr
        setReifType(aFcn, aCF);
        
        // b) Change reif symbol
        setReifFcnArgs(aFcn, fcnArgs, aCF);
      }
    }//IRConstructionFacade
    
    // Set the type of the expressions in the constraint scope
    static void inferenceTypeOnScope(std::shared_ptr<IRModule>& aModule)
    {
      assert(!aModule->isGlobalModule());
      auto blk = aModule->getBlock();
      
      // Get the constraint declaration node
      std::unique_ptr<IRConstructionFacade> cfacade(new IRConstructionFacade());
      
      for(auto& n : blk->preOrderVisit<IRConDecl>())
      {
        auto conDecl = IRConDecl::cast(n.get());
        assert(conDecl);
        
        cfacade->setBlock(conDecl->getBlock());
        setTypesOnFcnScope(conDecl->fcnCon(), *cfacade);
      }
    }//inferenceTypeOnScope
    
    void ConstraintScopeTypeInference::transformModule(std::shared_ptr<IRModule>& aModule) const
    {
      assert(aModule);
      auto ctx = aModule->getContext();
      
      // Get search module
      auto mdlName = IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_CONSTRAINT);
      auto conMod = ctx->globalModule()->getSubModule(mdlName);
      assert(conMod);
      
      inferenceTypeOnScope(conMod);
    }//transformModule
    
    std::string ConstraintScopeTypeInference::transformName() const
    {
      return "ConstraintScopeTypeInference";
    }//transformName
    
  }// end namespace Transforms
}// end namespace IR
