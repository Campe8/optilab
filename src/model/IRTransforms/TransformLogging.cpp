// Self first
#include "TransformLogging.hpp"
#include "IRPrettyPrinter.hpp"

#include <boost/filesystem.hpp>
#include <fstream>

namespace IR {
	namespace Transforms {

		void logTransform(const std::string& aFileName, const std::shared_ptr<IRModule>& aModule)
		{
      using Path = boost::filesystem::path;
      static std::string folder{"IR_debug"};
			static std::string ext{ ".txt" };
      
      // Create folder path
      Path pDir(folder.c_str());
      
      // Check for existence of folder p
      if(!boost::filesystem::exists(pDir))
      {
        // Create folder if it doesn't exists
        boost::filesystem::create_directory(pDir);
      }
      
      // Create file name w.r.t. folder name
      std::string fileName = aFileName + ext;
      auto pathFile = pDir / fileName;
      
      // Open file stream for output
			std::ofstream file(pathFile.c_str());
			std::stringstream ss;

			IRPrettyPrinter pp;
			aModule->acceptVisitor(&pp);
			pp.toStream(ss);

			file << ss.str();
		}//printIROnTransfrom

	}// end namespace Transforms
}// end namespace IR
