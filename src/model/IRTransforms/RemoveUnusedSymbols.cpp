// Self first
#include "RemoveUnusedSymbols.hpp"

#include "IRContext.hpp"
#include "IRConDecl.hpp"
#include "IRExprVar.hpp"
#include "IRExprConst.hpp"
#include "IRSymbolConstInteger.hpp"
#include "IRSymbolConstArray.hpp"
#include "IRTypeFunction.hpp"
#include "IRConstructionFacade.hpp"
#include "IRUtils.hpp"

#include "IRException.hpp"

#include "ModelUtils.hpp"

#include <cassert>

namespace IR {
  namespace Transforms {
    static void markConstraintArgument(IRExpr* aExpr);
    
    static void markExprRef(IRExprRef* aExpr)
    {
      assert(aExpr);
      markConstraintArgument((aExpr->getReferenceExpr()).get());
    }//markExprRef
    
    static void markSymbol(IRSymbol* aSymb)
    {
      assert(aSymb);
      if(IRSymbolVar::isa(aSymb))
      {
        aSymb->markObject(IRObject::ObjectMark::VISITED);
      }
      else if (IRSymbolConstArray::isa(aSymb))
      {
        auto symbArray = IRSymbolConstArray::cast(aSymb);
        
        // Return if the object has been already marked
        if (symbArray->getMark() == IRObject::ObjectMark::VISITED) return;
        
        // Recursively mark array elements.
        // Array is marked for referenced arrays.
        // For example:
        // - c = [1, 2] => mark c symbol
        // - [x, y] => mark x and y recursively
        symbArray->markObject(IRObject::ObjectMark::VISITED);
        for(std::size_t idx{0}; idx < symbArray->size(); ++idx)
        {
          markSymbol((symbArray->getSymbol(idx)).get());
        }
      }
    }//markSymbol
    
    static void markExprConst(IRExprConst* aExpr)
    {
      assert(aExpr);
      markSymbol((aExpr->getConst()).get());
    }//markExprConst
    
    static void markExprStdVar(IRExprStdVar* aExpr)
    {
      assert(aExpr);
      markSymbol((aExpr->getVar()).get());
    }//markExprStdVar
    
    static void markExprVar(IRExprVar* aExpr)
    {
      assert(aExpr);
      markSymbol((aExpr->getVar()).get());
    }//markExprVar
    
    void markConstraintArgument(IRExpr* aExpr)
    {
      assert(aExpr);
      if(IRExprRef::isa(aExpr))
      {
        markExprRef(IRExprRef::cast(aExpr));
      }
      else if (IRExprConst::isa(aExpr))
      {
        markExprConst(IRExprConst::cast(aExpr));
      }
      else if (IRExprStdVar::isa(aExpr))
      {
        markExprStdVar(IRExprStdVar::cast(aExpr));
      }
      else if (IRExprVar::isa(aExpr))
      {
        markExprVar(IRExprVar::cast(aExpr));
      }
    }//markConstraintArgument
    
    static void markConstraintScope(const std::shared_ptr<IRExprFcn>& aFcn)
    {
      assert(aFcn);
      
      for (std::size_t idx{ 0 }; idx < aFcn->numArgs(); ++idx)
      {
        // Mark the symbols of each constraint argument
        markConstraintArgument((aFcn->getArg(idx)).get());
      }
    }//markConstraintScope
    
    static void markUsedSymbols(std::shared_ptr<IRModule>& aModule)
    {
      assert(!aModule->isGlobalModule());
      auto blk = aModule->getBlock();
      
      // Get the constraint declaration nodes
      auto nodeList = blk->preOrderVisit<IRConDecl>();
      for (auto& n : nodeList)
      {
        auto conDecl = IRConDecl::cast(n.get());
        assert(conDecl);
        
        markConstraintScope(conDecl->fcnCon());
      }
    }//markUsedSymbols
    
    static void removeUnusedAssignment(std::shared_ptr<IRModule>& aModule)
    {
      assert(!aModule->isGlobalModule());
      auto blk = aModule->getBlock();
      
      // Get the parameter nodes to remove
      auto nodeList = blk->preOrderVisit<IRExprAssign>();
      std::vector<std::shared_ptr<IRExprAssign>> nodesToRemove;
      for (auto& n : nodeList)
      {
        auto assignExpr = IRExprAssign::cast(n.get());
        assert(assignExpr);
        
        auto lhs = (assignExpr->lhs()).get();
        assert(IRExprStdVar::isa(lhs));
        
        auto mark = IRExprStdVar::cast(lhs)->getVar()->getMark();
        if(mark != IRObject::ObjectMark::VISITED)
        {
          nodesToRemove.push_back(n);
        }
      }//for
      
      // Remove from the block the statements containing the nodes to remove
      for(auto& n : nodesToRemove)
      {
        // Find the stmt containing the node to remove
        auto it = blk->find(n);
        assert(it != blk->end());
        
        // Remove the statement from the block
        blk->removeStmt(*it);
      }
    }//removeUnusedAssignment
    
    void RemoveUnusedSymbols::transformModule(std::shared_ptr<IRModule>& aModule) const
    {
      assert(aModule);
      auto ctx = aModule->getContext();
      
      // Get search module
      auto mdlName = IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_CONSTRAINT);
      auto mod = ctx->globalModule()->getSubModule(mdlName);
      assert(mod);
      
      // Mark the symbols that belong to a constraint scope
      markUsedSymbols(mod);
      
      // Delete the assignment expression where the LHS is a symbol which is not used
      mdlName = IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_PARAMETER);
      mod = ctx->globalModule()->getSubModule(mdlName);
      assert(mod);
      removeUnusedAssignment(mod);
    }//transformModule
    
    std::string RemoveUnusedSymbols::transformName() const
    {
      return "RemoveUnusedSymbols";
    }//transformName
    
  }// end namespace Transforms
}// end namespace IR
