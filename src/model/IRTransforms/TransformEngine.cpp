// Self first
#include "TransformEngine.hpp"

// IR module transforms
#include "VariableTagging.hpp"
#include "ConstantPropagation.hpp"
#include "ConstraintScopeTypeInference.hpp"
#include "TransformLogging.hpp"
#include "RemoveUnusedSymbols.hpp"

#include <sstream>
#include <cassert>

namespace IR {
  namespace Transforms {
    
    // Prints "aModule" on the file "aFileName"
    static void logIR(const std::string& aFileName, std::size_t aXFromIdx, const std::shared_ptr<IRModule>& aModule, bool aDebug)
    {
      if (aDebug)
      {
        std::stringstream ss;
        ss << aXFromIdx;
        
        logTransform(ss.str() + "_" + aFileName, aModule);
      }
    }//printIROnTransfrom
    
    template<typename T>
    void registerSingleTransform(TransformEngine* aDriver)
    {
      std::shared_ptr<T> xform(new T());
      aDriver->registerModuleTransform(xform);
    }//registerSingleTransform
    
    TransformEngine::TransformEngine()
    : pCtx(nullptr)
    , pDebug(false)
    {
      initialzieTransformEngine();
    }
    
    void TransformEngine::initialzieTransformEngine()
    {
      registerSingleTransform<ConstantPropagation>(this);
      registerSingleTransform<ConstraintScopeTypeInference>(this);
      registerSingleTransform<VariableTagging>(this);
      registerSingleTransform<RemoveUnusedSymbols>(this);
    }//initialzieTransformEngine
    
    void TransformEngine::runTransforms()
    {
      assert(pCtx);
      
      auto glbModule = pCtx->globalModule();
      
      // Pretty print before any transformation happens
      LOG_XFORM("pre_transform", 0)
      
      std::size_t xformIdx{ 0 };
      for (auto& fcn : pTransformEngine.first)
      {
        fcn(glbModule);
        
        // Pretty print IR
        auto xformName = pTransformEngine.second[xformIdx++].first;
        LOG_XFORM(xformName, xformIdx)
      }
    }//runTransforms
    
    void TransformEngine::registerModuleTransform(const TransformPtr& aTransform)
    {
      assert(aTransform);
      
      TransformInfo tinfo;
      tinfo.first = aTransform->transformName();
      tinfo.second = aTransform;
      
      // Step 1: connect the transform to the engine
      pTransformEngine.first.push_back(std::bind(&ModuleTransform::apply, aTransform.get(), std::placeholders::_1));
      
      // Step 2: add the transform to the register
      pTransformEngine.second.push_back(tinfo);
    }//registerModuleTransform
    
  }// end namespace Transforms
}// end namespace IR
