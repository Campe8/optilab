// Self first
#include "ConstantPropagation.hpp"

#include "IRContext.hpp"
#include "IRConDecl.hpp"
#include "IRExprVar.hpp"
#include "IRExprConst.hpp"
#include "IRSymbolConstInteger.hpp"
#include "IRSymbolConstArray.hpp"
#include "IRTypeFunction.hpp"
#include "IRConstructionFacade.hpp"
#include "IRUtils.hpp"

#include "IRException.hpp"

#include "ModelUtils.hpp"

#include <cassert>

namespace IR {
  namespace Transforms {
    
    // Replaces "aConstExpr" with the value of "aMatrixExpr[aConstExpr]" in "aMatrixSubExpr"
    static void swapConstSubscript(const std::shared_ptr<IRExpr>& aMatrixSubExpr,
                                   IRExprRef* aMatrixExpr, IRExprConst* aConstExpr,
                                   IRConstructionFacade& aCF)
    {
      
      auto symbConstSubscript = aConstExpr->getConst();
      assert(IRSymbolConstInteger::isa(symbConstSubscript.get()));
      
      // From the subscript constant get the indexing subscript
      auto idxSubscript = IRSymbolConstInteger::cast(symbConstSubscript.get())->getInt();
      
      // Reference expressions create a parameter expression
      auto symbConstMatrix = getReferencedSymbol(aMatrixExpr);
      assert(IRSymbolConstArray::isa(symbConstMatrix.get()));
      
      std::shared_ptr<IRSymbol> matrixSymbElem{nullptr};
      IR_TRY_BLOCK
        matrixSymbElem = IRSymbolConstArray::cast(symbConstMatrix.get())->getSymbol(static_cast<std::size_t>(idxSubscript));
      IR_CATCH_BLOCK("Out of bound: " +
                     IRSymbolConstArray::cast(symbConstMatrix.get())->getName() + "[" +
                     IRSymbolConstInteger::cast(symbConstSubscript.get())->getName() + "]")
      
      assert(matrixSymbElem);
      assert(IRSymbolConst::isa(matrixSymbElem.get()));
      auto toSwapExpr = aCF.exprConst(std::static_pointer_cast<IRSymbolConst>(matrixSymbElem));
      
      Utils::swapExpr(aMatrixSubExpr, toSwapExpr);
    }//getSubscriptConst
    
    static bool propagateConstantOnSubExpr(const std::shared_ptr<IRExpr>& aExpr, IRConstructionFacade& aCF)
    {
      // Base case
      if (!aExpr || !IRExprMatrixSub::isa(aExpr.get()))
      {
        return false;
      }
      
      // Get the matrix
      auto matrixExpr = IRExprMatrixSub::cast(aExpr.get())->getMatrix();
      if(!IRExprRef::isa(matrixExpr.get()))
      {
        // Return for non-reference expression,
        // for example for matrices (not constant)
        return false;
      }
      
      auto subscriptExpr = IRExprMatrixSub::cast(aExpr.get())->getSubscript();
      if (IRExprConst::isa(subscriptExpr.get()))
      {
        // Swap the subscription with its value
        assert(IRExprRef::isa(matrixExpr.get()));
        swapConstSubscript(aExpr, IRExprRef::cast(matrixExpr.get()), IRExprConst::cast(subscriptExpr.get()), aCF);
        return false;
      }
      
      // Proceed recursively into the subscription
      assert(IRExprMatrixSub::isa(subscriptExpr.get()));
      propagateConstantOnSubExpr(subscriptExpr, aCF);
      
      return true;
    }//propagateConstantOnSubExpr
    
    static void propagateConstOnFcnScope(const std::shared_ptr<IRExprFcn>& aFcn, IRConstructionFacade& aCF)
    {
      assert(aFcn);
      
      for (std::size_t idx{ 0 }; idx < aFcn->numArgs(); ++idx)
      {
        // Run propagation only for matrix subscript expr
        auto aexpr = aFcn->getArg(idx);
        if (IRExprMatrixSub::isa(aexpr.get()))
        {
          bool change = true;
          while (change)
          {
            change = propagateConstantOnSubExpr(aexpr, aCF);
          }
        }
      }
    }//propagateConstOnFcnScope
    
    static void propagateOnSubscripts(std::shared_ptr<IRModule>& aModule)
    {
      assert(!aModule->isGlobalModule());
      auto blk = aModule->getBlock();
      
      // Get the constraint declaration node
      auto nodeList = blk->preOrderVisit<IRConDecl>();
      
      std::unique_ptr<IRConstructionFacade> cfacade(new IRConstructionFacade());
      
      for (auto& n : nodeList)
      {
        auto conDecl = IRConDecl::cast(n.get());
        assert(conDecl);
        
        cfacade->setBlock(conDecl->getBlock());
        propagateConstOnFcnScope(conDecl->fcnCon(), *cfacade);
      }
    }//propagateOnSubscripts
    
    void ConstantPropagation::transformModule(std::shared_ptr<IRModule>& aModule) const
    {
      assert(aModule);
      auto ctx = aModule->getContext();
      
      // Get search module
      auto mdlName = IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_CONSTRAINT);
      auto conMod = ctx->globalModule()->getSubModule(mdlName);
      assert(conMod);
      
      // Constant propagation on matrix subscript
      propagateOnSubscripts(conMod);
    }//transformModule
    
    std::string ConstantPropagation::transformName() const
    {
      return "ConstantPropagation";
    }//transformName
    
  }// end namespace Transforms
}// end namespace IR
