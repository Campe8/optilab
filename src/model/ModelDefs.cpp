//
// Copyright OptiLab 2017. All rights reserved.
//
// Created by Federico Campeotto on 09/20/2017
//

#include "ModelDefs.hpp"

#include <iostream>

namespace Model {
 
	MODEL_EXPORT_FUNCTION void throwLogicErrorWithMessage(const char* aMsg)
	{
		throw std::logic_error(aMsg);
	}//throwWithMessage

} // end namespace Model
