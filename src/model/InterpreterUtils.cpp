// Self first
#include "InterpreterUtils.hpp"

#include "ModelMacro.hpp"

// View
#include "ViewEntryArray.hpp"

// Expression parser
#include "ExprTk/exprtk.hpp"

#include <boost/regex.hpp>

#include <math.h>
#include <cassert>

namespace cutils = CLI::Utils;

namespace Model {
  namespace Utils {
    namespace Interpreter {
      
      // Interprets "aExpr" w.r.t. the current view "aView", i.e.,
      // the current "symbol table" and returns the interpreted expression.
      // @note returns nan if "aExpr" can't be interpreted in the current view
      template <typename T>
      static T interpretExprOnView(const std::string& aView, const std::string& aExpr)
      {
        std::string program = aView + aExpr;
        
        using expression_t = exprtk::expression<T>;
        using parser_t = exprtk::parser<T>;
        
        expression_t expression;
        parser_t parser;
        parser.compile(program, expression);
        
        return expression.value();
      }//interpretExpr
      
      // Regex for integers numbers
      static std::string intRegex()
      {
        static std::string re("^((\\+|-)?[\\d]+)$");
        return re;
      }//intRegex
      
      // Regex for ids
      static std::string idRegex()
      {
        static std::string re("([a-zA-Z_]+[a-zA-Z_0-9]*)");
        return re;
      }//idRegex
      
      // Regex for arrays [...]
      static std::string arrayRegex()
      {
        static std::string re("^((\\[))(.)*\\]$");
        return re;
      }//arrayRegex
      
      // Regex for subscription s[...]
      static std::string subscriptRegex()
      {
        static std::string re("^(([a-zA-Z_]+[a-zA-Z_0-9]*)(\\[))(.)*\\]$");
        return re;
      }//subscriptRegex
      
      // Returns true if "aExpr" matches the regular expression "aRegex",
      // false otherwise
      static bool matchRegex(const boost::regex& aRegex, const std::string& aExpr)
      {
        boost::smatch what;
        return boost::regex_match(aExpr, what, aRegex);
      }//matchRegex
      
      // Returns true if "aExpr" is a integer expression,
      // false otherwise
      static bool isIntExpr(const std::string& aExpr)
      {
        return matchRegex(boost::regex(intRegex()), aExpr);
      }//isInt
      
      // Returns true if "aExpr" is an id expression,
      // false otherwise
      static bool isIDExpr(const std::string& aExpr)
      {
        return matchRegex(boost::regex(idRegex()), aExpr);
      }//isIDExpr
      
      // Returns true if "aExpr" is an array "[...]" expression,
      // false otherwise
      static bool isArrayExpr(const std::string& aExpr)
      {
        return matchRegex(boost::regex(arrayRegex()), aExpr);
      }//isArrayExpr
      
      // Returns true if "aExpr" is a subscription "x[...]" expression,
      // false otherwise
      static bool isSubscriptExpr(const std::string& aExpr)
      {
        return matchRegex(boost::regex(subscriptRegex()), aExpr);
      }//isArrayExpr
      
      static boost::optional<std::string> interpretScalarExpr(const std::string& aExpr, const ContextView& aView, ExprType aExprType)
      {
        switch (aExprType)
        {
          case Model::Utils::Interpreter::ExprType::AT_ID:
          {
            // Check what the ID represents.
            // For the time being, returns the ID itself
            /*
            if (aView.findAlias(aExpr))
            {
              return aExpr;
            }
            */
            return aExpr;
          }
            break;
          case Model::Utils::Interpreter::ExprType::AT_INT:
            return aExpr;
          default:
          {
            model_assert(aExprType == ExprType::AT_SUBSCRIPT);
            auto iExpr = interpretExprOnView<double>(aView.getSerializedView(), aExpr);
            if (!std::isnan(iExpr))
            {
              return cutils::toString<INT_64_T>(static_cast<INT_64_T>(iExpr));
            }
          }
            break;
        }// switch
        
        return boost::none;
      }//interpretScalarExpr
      
      // Parses "aExpr" as an expression and returns its interpreted version.
      // If "aExpr" can't be interpreted w.r.t. the current view, returns boost::none.
      // This is a shallow interpretation since it runs the expr interpreter on "aExpr" "as-is",
      // e.g., it does not run interpretation on sub-expressions
      static boost::optional<std::string> shallowInterpretation(const std::string& aExpr, const ContextView& aView)
      {
        auto eType = getExprType(aExpr);
        switch (eType)
        {
          case ExprType::AT_ID:
            return interpretScalarExpr(aExpr, aView, eType);
          case ExprType::AT_INT:
            // Optimization 1: if "aExpr" is integer no need to run the interpreter
            return aExpr;
          case ExprType::AT_SUBSCRIPT:
            // Optimization 2: if the matrix is not contained in the current view
            // the expression cannot be interpreted, return none
          {
            auto matSub = getSubscriptMatrixAndIndex(aExpr);
            if (!aView.lookup(matSub.first))
            {
              return boost::none;
            }
            return interpretScalarExpr(aExpr, aView, eType);
          }
          default:
            model_assert(eType == AT_ARRAY);
            return boost::none;
        }
      }//shallowInterpretation
      
      // Interpret a subscript expression
      static bool interpretSubscriptExpr(std::string& aExpr, const ContextView& aView, ReferenceMap* aRefMap)
      {
        model_assert(aRefMap);
        
        auto subPair = getSubscriptMatrixAndIndex(aExpr);
        model_assert(subPair.second.size() == 1);
        
        // Interpret the subscript expression
        auto subExpr = subPair.second.at(0);
        auto isexpr = interpretExpr(subExpr, aView, aRefMap);
        if (!isexpr)
        {
          return false;
        }
        
        // Check for circular dependencies
        model_assert_msg(cutils::isInteger(subExpr), "Indexing not valid: " + subExpr);
        std::size_t subscriptIdx = cutils::stoi(subExpr);
        
        if (aRefMap->find(subPair.first) != aRefMap->end() &&
            (aRefMap->at(subPair.first)).find(subscriptIdx) != (aRefMap->at(subPair.first)).end())
        {
          model_assert_msg(false, "Circular dependency");
        }
        
        if (auto mat = aView.lookup(subPair.first))
        {
          model_assert(Model::ViewEntryArray::isa(mat));
          model_assert_msg(subscriptIdx >= 0 &&subscriptIdx < Model::ViewEntryArray::cast(mat)->getArraySize(),
                           "Out of bound indexing on " + subPair.first);
          if (mat->isFullyInterpreted() || !(Model::ViewEntryArray::cast(mat)->needsInterpretation(subscriptIdx)))
          {
            // The matrix or the value are already fully interpreted
            aExpr = (Model::ViewEntryArray::cast(mat)->getArray())[subscriptIdx];
            return true;
          }
          
          // Here mat is in the view, it is not fully interpreted and
          // subExpr is an index of an element in mat which is not fully interpreted.
          // Following steps are needed:
          // 1 - get the element in mat in position subExpr from the view
          // 2 - interpret the element in (1), now the view is updated
          // 3 - get the element in mat[subExpr] and swap it with "aExpr"
          (*aRefMap)[subPair.first].insert(subscriptIdx);
          auto subExpr = Model::ViewEntryArray::cast(mat)->getNeedsInterpretationValue(subscriptIdx);
          if (auto isubExpr = interpretExpr(subExpr, aView, aRefMap))
          {
            // Interpreting was successful,
            // change/update the view with the current value
            aExpr = *isubExpr;
            Model::ViewEntryArray::cast(mat)->setInterpretedValue(subscriptIdx, aExpr);
            return true;
          }
          return false;
        }
        
        // Can't interpret if the matrix is not in the view
        return false;
      }//interpretSubscriptExpr
      
      // Returns the ID that identifies the object interpreted from "aStmt" if any
      boost::optional<std::string> getIDFromStmt(const CLI::CLIStmt& aStmt)
      {
        auto msgType = CLI::Utils::getMsgType(aStmt);
        switch (msgType)
        {
          case CLI::CLIMsgType::CLI_MSG_VAR_DECL:
            return cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_VAR_ID);
          case CLI::CLIMsgType::CLI_MSG_MATRIX_DECL:
            return cutils::getCLIStmtVal(aStmt, CLI_MSG_TAG_MAT_ID);
          default:
            return boost::none;
        }
      }//getIDFromStmt
      
      boost::optional<std::string> interpretExpr(const std::string& aExpr, const ContextView& aView, ReferenceMap* aRefMap)
      {
        auto etype = getExprType(aExpr);
        switch (etype)
        {
          case Model::Utils::Interpreter::AT_ID:
            return shallowInterpretation(aExpr, aView);
          case Model::Utils::Interpreter::AT_INT:
            return aExpr;
          case Model::Utils::Interpreter::AT_ARRAY:
          {
            // Try to interpret each expression in the list
            // to deteremine if the array is fully interpreted or not
            for(auto& e : getArrayElements(aExpr))
            {
              if(auto ie = interpretExpr(e, aView, aRefMap))
              {
                e = *ie;
              }
              else
              {
                return boost::none;
              }
            }
            return aExpr;
          }
          default:
          {
            model_assert(etype == Model::Utils::Interpreter::AT_SUBSCRIPT);
            
            // Optimization: try with shallow first
            if (auto subscriptExpr = shallowInterpretation(aExpr, aView))
            {
              return subscriptExpr;
            }
            
            // If shallow interpretation is not able to interpret,
            // run deep interpretation
            ReferenceMap rmap;
            if (!aRefMap)
            {
              aRefMap = &rmap;
            }
            auto exprCpy = aExpr;
            if (interpretSubscriptExpr(exprCpy, aView, aRefMap))
            {
              return exprCpy;
            }
            return boost::none;
          }
        }
      }//interpretExpr
      
      ExprType getExprType(const std::string& aExpr)
      {
        if (aExpr.empty())
        {
          return ExprType::AT_UNDEF;
        }
        
        if (isIntExpr(aExpr)) { return ExprType::AT_INT; }
        if (isIDExpr(aExpr)) { return ExprType::AT_ID; }
        if (isArrayExpr(aExpr)) { return ExprType::AT_ARRAY; }
        if (isSubscriptExpr(aExpr)) { return ExprType::AT_SUBSCRIPT; }
        
        return ExprType::AT_UNDEF;
      }
      
      std::pair<std::string, std::vector<std::string>> getSubscriptMatrixAndIndex(const std::string& aSubscript)
      {
        std::pair<std::string, std::vector<std::string>> subscript;
        if (getExprType(aSubscript) != ExprType::AT_SUBSCRIPT) { return subscript; }
        
        auto openPar = aSubscript.find_first_of('[');
        auto closePar = aSubscript.find_last_of(']');
        model_assert(openPar != std::string::npos);
        model_assert(closePar != std::string::npos);
        
        // Get matrix expression
        subscript.first = aSubscript.substr(0, openPar);
        
        // Get matrix subscription should be an integer or another matrix subscription
        auto matrixSub = aSubscript.substr(openPar + 1, closePar - openPar - 1);
        subscript.second = CLI::Utils::splitArguments(matrixSub);
        
        return subscript;
      }//getSubscriptMatrixAndIndex
      
      std::vector<std::string> getArrayElements(const std::string& aArray)
      {
        if (getExprType(aArray) != ExprType::AT_ARRAY) { return std::vector<std::string>(); }
        
        model_assert(aArray.size() > 2);
        model_assert(aArray[0] == '[' && aArray[aArray.size() - 1] == ']');
        
        auto arrayArgs = (aArray.substr(1));
        arrayArgs.pop_back();
        
        return CLI::Utils::splitArguments(arrayArgs);
      }//getArrayElements
      
    }
  }
} // end namespace Model Utils Parser
