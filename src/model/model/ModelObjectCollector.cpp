// Self first
#include "ModelObjectCollector.hpp"

#include "ModelVariable.hpp"
#include "ModelConstraint.hpp"
#include "ModelParameter.hpp"

#include "Variable.hpp"
#include "Constraint.hpp"

#include <cassert>

namespace Model {
  
  /// Returns true if "aObj" carries an UUID, false otherwise
  static bool containsUUID(ModelObject* aObj)
  {
    if(!aObj) { return false; }
    
    auto objClass = aObj->getModelObjectClass();
    switch (objClass)
    {
      case ModelObjectClass::MODEL_OBJ_CONSTRAINT:
      case ModelObjectClass::MODEL_OBJ_VARIABLE:
        return true;
      default:
        assert(objClass == ModelObjectClass::MODEL_OBJ_PARAMETER ||
               objClass == ModelObjectClass::MODEL_OBJ_SEARCH_SEMANTIC);
        return false;
    }
  }//containsUUID
  
  /// Returns the UUID carried by the given model object if any
  static Core::UUIDCoreObj getObjUUID(ModelObject* aObj)
  {
    assert(aObj);
    
    auto objClass = aObj->getModelObjectClass();
    switch (objClass) {
      case ModelObjectClass::MODEL_OBJ_CONSTRAINT:
        return static_cast<ModelConstraint*>(aObj)->constraint()->getUUID();
      default:
        assert(objClass == ModelObjectClass::MODEL_OBJ_VARIABLE);
        return static_cast<ModelVariable*>(aObj)->variable()->getUUID();
    }
  }//getObjUUID
  
  static int mdlObjClassToInt(ModelObjectClass aClass)
  {
    return static_cast<int>(aClass);
  }//mdlObjClassToInt
  
  ModelObjectCollector::ObjMapKey ModelObjectCollector::mapObject(ModelObjectUPtr aMObj)
  {
    assert(aMObj);
    auto obj = aMObj.release();
    
    // Map keys to objects
    // Map based on object class
    auto mapKey = mapObjectToClass(obj);
    
    // Map based on UUIDs
    if(containsUUID(obj))
    {
      mapUUID(getObjUUID(obj), mapKey);
    }
    
    // Map based on string IDs
     mapStringID(obj->getID(), mapKey);
    
    return mapKey;
  }//mapObject

  std::size_t ModelObjectCollector::numObjOfClass(ModelObjectClass aClass) const
  {
    auto intClass = mdlObjClassToInt(aClass);
    if(pClassToObjMap.find(intClass) == pClassToObjMap.end())
    {
      return 0;
    }
    
    return pClassToObjMap.at(intClass).size();
  }//numObjOfClass
  
  /// Returns the aMdlObjIdx^th - 1 instance "aModelObjectClass" object
  const ModelObject* ModelObjectCollector::getObjOfClass(ModelObjectClass aClass, std::size_t aIdx) const
  {
    auto intClass = mdlObjClassToInt(aClass);
    if(pClassToObjMap.find(intClass) == pClassToObjMap.end() ||
       aIdx >= pClassToObjMap.at(intClass).size())
    {
      return nullptr;
    }
    
    return pClassToObjMap.at(intClass).at(aIdx).get();
  }//getObjOfClass
  
  ModelObjectCollector::ObjMapKey ModelObjectCollector::mapObjectToClass(ModelObject* aObj)
  {
    auto intClass = mdlObjClassToInt(aObj->getModelObjectClass());
    pClassToObjMap[intClass].push_back(ModelObjSPtr(aObj));
    
    return std::make_pair(intClass, pClassToObjMap[intClass].size() - 1);
  }//mapObjectToClass
  
  ModelObject* ModelObjectCollector::getObjectFromClass(const ObjMapKey& aObjMapKey) const
  {
    if(pClassToObjMap.find(aObjMapKey.first) == pClassToObjMap.end() ||
       aObjMapKey.second >= pClassToObjMap.at(aObjMapKey.first).size())
    {
      return nullptr;
    }
    
    return pClassToObjMap.at(aObjMapKey.first)[aObjMapKey.second].get();
  }//getObjectFromClass
  
  void ModelObjectCollector::mapUUID(const UUID& aUUID, const ObjMapKey& aObjMapKey)
  {
    pUUIDToObjKeyMap[aUUID] = aObjMapKey;
  }//mapUUID
  
  void ModelObjectCollector::mapStringID(const std::string& aSID, const ObjMapKey& aObjMapKey)
  {
    pStringIDToObjKeyMap[aSID] = aObjMapKey;
  }//mapStringID
  
  ModelObject* ModelObjectCollector::lookup(const Core::UUIDCoreObj& aUUID) const
  {
    if(pUUIDToObjKeyMap.find(aUUID) == pUUIDToObjKeyMap.end())
    {
      return nullptr;
    }
    
    return getObjectFromClass(pUUIDToObjKeyMap.at(aUUID));
  }//lookup
  
  ModelObject* ModelObjectCollector::lookup(const std::string& aSID) const
  {
    if(aSID.empty() || pStringIDToObjKeyMap.find(aSID) == pStringIDToObjKeyMap.end())
    {
      return nullptr;
    }
    
    return getObjectFromClass(pStringIDToObjKeyMap.at(aSID));
  }//lookup
  
}// end namespace Model
