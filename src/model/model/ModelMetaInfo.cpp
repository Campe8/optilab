// Self first
#include "ModelMetaInfo.hpp"

#include "ModelUtils.hpp"

#include <cassert>

namespace Model {
  
  ModelMetaInfo::ModelMetaInfo(const std::string& aModelName, long long aSolutionLimit)
  : pModelName(aModelName)
  , pSolutionLimit(aSolutionLimit)
  , pMetricsEnabled(false)
  {
    pModelName = pModelName.empty() ? Utils::getUndefModelName() : pModelName;
  }
  
}// end namespace Model
