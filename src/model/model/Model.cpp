// Base class
#include "Model.hpp"

#include "ModelUtils.hpp"

#include "Constraint.hpp"
#include "VariableSemantic.hpp"

#include "ModelSearch.hpp"

#include <cassert>

namespace Model {
  
  Model::Model(const ModelMetaInfoSPtr& aMetaInfo)
  : pMetaInfo(aMetaInfo)
  , pVarIDMap(nullptr)
  , pConIDMap(nullptr)
  {
    pVarIDMap = std::make_shared<ModelVarMap>();
    pConIDMap = std::make_shared<ModelConMap>();
  }
  
  void Model::addModelObject(ModelObjectUPtr aMObj)
  {
    auto objClass = aMObj->getModelObjectClass();
    switch (objClass) {
      case ModelObjectClass::MODEL_OBJ_VARIABLE:
      {
        auto varPtr = ModelVariable::cast(aMObj.get())->variable();
        (*pVarIDMap)[varPtr->getVariableNameId()] = varPtr;
      }
        break;
      case ModelObjectClass::MODEL_OBJ_CONSTRAINT:
      {
        auto conObj = ModelConstraint::cast(aMObj.get());
        auto conPtr = conObj->constraint();
        (*pConIDMap)[conObj->getID()] = conPtr;
      }
        break;
      default:
        break;
    }
    
    // Map the object in the collector
    pCollector.mapObject(std::move(aMObj));
  }//addModelObject
  
  const ModelObject* getModelObjectBySID(const Model* aMdl, const std::string& aID)
  {
    if (!aMdl) return nullptr;
    return (aMdl->pCollector).lookup(aID);
  }//getModelObjectBySID
  
}// end namespace Model
