// Self first
#include "ParserUtils.hpp"

#include "ModelUtils.hpp"
#include "MVCUtils.hpp"
#include "IRUtils.hpp"
#include "IRContext.hpp"
#include "ObjectHelper.hpp"

namespace otools = Interpreter::ObjectTools;

namespace Model { namespace Utils { namespace Parser {
  
  static bool isAnyObject(const Interpreter::DataObject& aDO)
  {
    return Interpreter::isPrimitiveType(aDO) &&
    (aDO.dataObjectType() == Interpreter::DataObject::DataObjectType::DOT_STRING) &&
    (aDO.getDataValue<std::string>() == Interpreter::getAnyObjectData());
  }//isAnyObject
  
  // Returns true if the given data object can take a reference, i.e., is a lvalue,
  // return false otherwise
  static bool canTakeReference(const Interpreter::DataObject& aDO)
  {
    // Different objects can take references.
    // For example:
    // 1) x:[1, 2, 3]
    //    where the object is a matrix parameter and the payload is > 1,
    //    i.e., object type and the reference to "x";
    // 2) x:1..2:[3]
    //   where the object is a variable and the payload is (always) 4;
    // 3) x:1..2
    //  where the object is a variable and the payload is (always) 4.
    // Other object cannot take a reference because they are "rvalues", i.e., temporary values.
    // For example:
    // 1) [1, 2, 3]
    //   where the object is a list and the payload is == 1,
    //   i.e., only the object type
    // 2) 1
    //   where the object is a scalar parameter
    if(Interpreter::isClassObjectType(aDO))
    {
      // If objects have a name, then they are lvalues and they can be referenced
      auto obj = aDO.getClassObject().get();
      if(otools::isVariableObject(obj))
      {
        return !otools::getVarID(obj).empty();
      }
      else
      {
        assert(otools::isParameterObject(obj));
        auto id = MVC::Utils::getIDFromDataObject(obj);
        return !id.empty() &&
            (id.substr(0, MVC::MVCModelContext::MVCModelContextBaseObjectValue.size()) !=
             MVC::MVCModelContext::MVCModelContextBaseObjectValue);
      }
    }
    
    return false;
  }//canTakeReference
  
  static std::shared_ptr<IR::IRExpr> getIRSymbolForAllVars(const IR::IRModuleSPtr& aMod,
                                                           IR::IRConstructionFacade& aCF)
  {
    std::vector<IR::IRSymbolFactory::IRSymbolPtr> varSymbolList;
    
    // Get the variable module
    const auto varModName = IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_VARIABLE);
    const auto& varMod = aMod->getContext()->globalModule()->getSubModule(varModName);
    
    // Add each symbol var in the vatiable module
    for (const auto& it : *varMod->getSymbolTable())
    {
      if (IR::IRSymbolVar::isa(it.second->getSymbol().get()))
      {
        varSymbolList.push_back(it.second->getSymbol());
      }
    }
    
    auto symbArray = aCF.symbolFactory()->symbolConstArray(varSymbolList);
    return aCF.exprConst(symbArray);
  }//getIRSymbolForAllVars
  
  static std::shared_ptr<IR::IRExpr> getIRConstExprArray(const Interpreter::DataObject& aArg,
                                                         const IR::IRModuleSPtr& aMod,
                                                         IR::IRConstructionFacade& aCF)
  {
    // Precondition: input argument is a list object and list elements don't contain
    // (sub) arrays as arguments
    assert(Interpreter::isClassObjectType(aArg) && otools::isListObject(aArg.getClassObject().get()));
    
    // Array of symbols: iterate over all the elements of the array,
    // create a symbol for each and return the symbol array
    std::vector<IR::IRSymbolFactory::IRSymbolPtr> symbols;
    for (const auto& arg : otools::getListValues(aArg.getClassObject().get()))
    {
      // Precondition: array elements don't contain (sub) arrays as arguments
      assert(Interpreter::isPrimitiveType(arg) ||
             (Interpreter::isClassObjectType(arg) &&
              (!otools::isMatrixParameterObject(arg.getClassObject().get()) &&
               !otools::isListObject(arg.getClassObject().get()))));
      
      if (isAnyObject(arg))
      {
        // If any of the object in the list is "any" return a vector of symbols with
        // a symbol per variable in the model
        // Return a synbol array containing a symbol for each variable in the model
        return getIRSymbolForAllVars(aMod, aCF);
      }
      
      // Otherwise continue and push back into the array
      symbols.push_back(getSymbolForDataObject(arg, aMod, aCF));
    }
    auto symbArray = aCF.symbolFactory()->symbolConstArray(symbols);
    return aCF.exprConst(symbArray);
  }//getIRConstExprArray
  
  // Returns the expression (argument) for the given ID.
  // @note it can be a expr var or a expr ref
  static std::shared_ptr<IR::IRExpr> getExprFromID(const std::string& aID,
                                                   const IR::IRModuleSPtr& aMod,
                                                   IR::IRConstructionFacade& aCF)
  {
    // Get the expression associated with the symbol entry in the symbol table whose
    // key is the given input ID
    auto expr = IR::Utils::lookupExprByName(aID, aMod->getContext());
    assert(expr);
    
    if(IR::IRExprVar::isa(expr.get()) || IR::IRExprStdVar::isa(expr.get()))
    {
      // For variables never return the reference to the variable but return
      // the expression representing the variable itself
      return expr;
    }
    
    // For const expr lookup the expression lhs in the assignment expression
    // whose rhs is the const expression.
    // @note such assignment expression must exist in the parameter module since
    // there is an ID representing the reference of the const expression
    assert(IR::IRExprConst::isa(expr.get()));
    
    auto paramModName = IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_PARAMETER);
    auto pmod = aMod->getContext()->globalModule()->getSubModule(paramModName);
    auto blk = pmod->getBlock();
    
    // Get the parameter nodes
    auto nodeList = blk->preOrderVisit<IR::IRExprAssign>();
    for (auto& n : nodeList)
    {
      auto assignExpr = IR::IRExprAssign::cast(n.get());
      assert(assignExpr);
      
      if(IR::Utils::isEqualExpr(expr.get(), (assignExpr->rhs()).get()))
      {
        return assignExpr->lhs();
      }
    }
    
    return aCF.exprRef(expr);
  }//getExprFromID
  
  static std::shared_ptr<IR::IRExpr> getIRExprForAtomicTypes(const Interpreter::DataObject& aArg,
                                                             const IR::IRModuleSPtr& aMod,
                                                             IR::IRConstructionFacade& aCF)
  {
    switch (aArg.dataObjectType()) {
      case Interpreter::DataObject::DataObjectType::DOT_INT:
        return aCF.exprConst(aCF.symbolFactory()->symbolConstInteger(aArg.getDataValue<int>()));
      case Interpreter::DataObject::DataObjectType::DOT_BOOL:
        return aCF.exprConst(aCF.symbolFactory()->symbolConstBoolean(aArg.getDataValue<bool>()));
      default:
        assert(aArg.dataObjectType() == Interpreter::DataObject::DataObjectType::DOT_STRING);
        return getExprFromID(aArg.getDataValue<std::string>(), aMod, aCF);
    }
  }//getIRExprForAtomicTypes
  
  static std::shared_ptr<IR::IRExpr> getExprForMatrixSubscr(const Interpreter::DataObject& aDO,
                                                            const IR::IRModuleSPtr& aMod,
                                                            IR::IRConstructionFacade& aCF)
  {
    assert(otools::isVarSubscript(aDO.getClassObject().get()));
    
    // Get matrix expression, it should be:
    // - a reference to the matrix
    // - a variable of type array
    auto matrixExpr = getExprFromID(getVarIDFromVarObj(aDO), aMod, aCF);
    assert(IR::IRExprRef::isa(matrixExpr.get()) ||
           (IR::IRExprVar::isa(matrixExpr.get()) && IR::IRTypeArray::isa((matrixExpr->getType()).get())));
    
    auto dimsList = (otools::getVarDomainDimensions(aDO.getClassObject().get())).getClassObject().get();
    assert(otools::isListObject(dimsList));
    
    auto numDims = otools::getListSize(dimsList);
    assert(numDims > 0 && numDims < 3);
    
    auto indices = otools::getVarDomainSubscriptIndicesList(aDO.getClassObject().get());
    assert(otools::getListSize(indices) == numDims);
    
    auto index = otools::getListElement(indices, 0);
    if(numDims == 2)
    {
      // The matrix is vectorized, get the correspondent index
      //  m[i][j] = array[ i*m + j ], where M is a n x m matrix
      auto numCols = otools::getListElement(dimsList, 1);
      index = Interpreter::DataObject( (index->op_mul(numCols.getData()))->op_add(otools::getListElement(indices, 1).getData()),
                                       index.dataObjectType());
    }
    
    // Generate the subscript expression
    auto subscriptExpr = getExprForDataObject(index, aMod, aCF);
    
    assert(IR::IRExprConst::isa(subscriptExpr.get()) || IR::IRExprMatrixSub::isa(subscriptExpr.get()));
    return aCF.exprMatrixSub(matrixExpr, subscriptExpr);
  }//getExprForMatrixSubscr
  
  // Creates an IRSymbol representing the given DataObject which encodes a model variable
  static IR::IRSymbolFactory::IRSymbolPtr getIRSymbolForVar(const Interpreter::DataObject& aDO,
                                                            const IR::IRModuleSPtr& aMod, IR::IRConstructionFacade& aCF)
  {
    assert(aDO.isClassObject() && otools::isVariableObject(aDO.getClassObject().get()));
    
    // Variable can be looked up by ID from the symbol table.
    // However, subscript variables must be handled differently:
    // x[1] will return "x" as a symbol which represents the entire variable and not the subscript variable.
    // To solve this problem, in case of a subscript variables, a new auxiliary standard variable
    // is create and it is assigned the subscript expression, then the returned symbol is the
    // symbol of the auxiliary variable.
    // For example:
    // x[1] =>
    // _v1 = x[1];
    if (MVC::Utils::isVarMatrixSubscr(aDO))
    {
      // Create a subscript expression
      auto subscrExpr = getExprForMatrixSubscr(aDO, aMod, aCF);
      
      // Look in the input module if there is already a symbol for the above expression.
      // If so, use its lhs before creating a new one.
      if(auto symb = IR::Utils::lookupSymbolFromExpr<IR::IRExprMatrixSub>(subscrExpr, aMod))
      {
        return symb;
      }
      
      // Create a new standard auxiliary variable
      auto var = aCF.symbolFactory()->symbolVar(Utils::getUndefVarName(), subscrExpr->getType());
      
      // Create the assignment expression
      auto assignExpr = aCF.exprAssign(aCF.exprStdVar(var), subscrExpr);
      
      // Add the assignment expression to the parameter's module
      auto mdlName = IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_PARAMETER);
      auto pMod = aMod->getContext()->globalModule()->getSubModule(mdlName);
      pMod->getBlock()->appendStmt(aCF.stmt(assignExpr));

      return var;
    }
    
    // Return the symbol var representing the variable
    auto exprVar = IR::Utils::lookupExprVar(getVarIDFromVarObj(aDO), aMod->getContext());
    assert(exprVar);
    
    return exprVar->getVar();
  }//getIRSymbolForVar
  
  static IR::IRSymbolFactory::IRSymbolPtr
  getSymbolForScalarObject(const Interpreter::DataObject& aDO, IR::IRSymbolFactory* aSymbFac)
  {
    switch (aDO.dataObjectType())
    {
      case Interpreter::DataObject::DataObjectType::DOT_INT:
        return aSymbFac->symbolConstInteger(aDO.getDataValue<int>());
      case Interpreter::DataObject::DataObjectType::DOT_DOUBLE:
        return aSymbFac->symbolConstDouble(aDO.getDataValue<double>());
      default:
        assert(aDO.dataObjectType() == Interpreter::DataObject::DataObjectType::DOT_BOOL);
        return aSymbFac->symbolConstBoolean(aDO.getDataValue<bool>());
    }//switch
  }//getSymbolForScalarObject
  
  /// Returns the expression constructed from parsing the given string
  /// which should represent a function argument
  MODEL_EXPORT_FUNCTION std::shared_ptr<IR::IRExpr>
  getExprForDataObject(const Interpreter::DataObject& aArg, const IR::IRModuleSPtr& aMod,
                       IR::IRConstructionFacade& aCF)
  {
    using IRExprSPtr = std::shared_ptr<IR::IRExpr>;
    
    // Create pointer to the expression to return
    IRExprSPtr exprArg{ nullptr };
    if (aArg.isClassObject())
    {
      auto obj = aArg.getClassObject().get();
      if (otools::isVariableObject(obj))
      {
        // If the object represents a variable from a subscript operator
        // create a matrix subscript expression.
        // If the object represents a variable, get expression from its ID instead
        exprArg = otools::isVarSubscript(obj) ?
        getExprForMatrixSubscr(aArg, aMod, aCF) :
        getExprFromID(getVarIDFromVarObj(aArg), aMod, aCF);
      }
      else if (otools::isMatrixParameterObject(obj) || otools::isListObject(obj))
      {
        // If the matrix or list is an lvalue, use its reference,
        // i.e., its ID to generate the expression.
        // If the matrix or list is an rvalue, construct the corresponding IR expression instead
        exprArg = canTakeReference(aArg) ?
        getExprFromID(MVC::Utils::getIDFromDataObject(aArg.getClassObject().get()), aMod, aCF) :
        getIRConstExprArray(aArg, aMod, aCF);
      }
      else
      {
        assert(otools::isScalarObject(obj));
        exprArg = getIRExprForAtomicTypes(otools::getScalarValue(obj), aMod, aCF);
      }
    }
    else
    {
      exprArg = getIRExprForAtomicTypes(aArg, aMod, aCF);
    }
    assert(exprArg);
    
    return exprArg;
  }//getExprFromArgument
  
  MODEL_EXPORT_FUNCTION std::string getVarIDFromVarObj(const Interpreter::DataObject& aDO)
  {
    assert(aDO.isClassObject() && otools::isVariableObject(aDO.getClassObject().get()));
    return otools::getVarID(aDO.getClassObject().get());
  }//getVarIDFromVarObj
  
  MODEL_EXPORT_FUNCTION IR::IRSymbolFactory::IRSymbolPtr
  getSymbolForDataObject(const Interpreter::DataObject& aDO, const IR::IRModuleSPtr& aMod,
                         IR::IRConstructionFacade& aCF)
  {
    if (aDO.isClassObject())
    {
      auto obj = aDO.getClassObject().get();
      if (otools::isVariableObject(obj))
      {
        // The DataObject is a variable, get correspondent symbol
        return getIRSymbolForVar(aDO, aMod, aCF);
      }
      else
      {
        assert(otools::isScalarObject(obj));
        return getSymbolForScalarObject(otools::getScalarValue(obj), aCF.symbolFactory().get());
      }
    }
    assert(Interpreter::isPrimitiveType(aDO));
    
    return getSymbolForScalarObject(aDO, aCF.symbolFactory().get());
  }//getSymbolForDataObject
  
  MODEL_EXPORT_FUNCTION const Interpreter::DataObject&
  getDataObjectFromPrimitiveOrScalarType(const Interpreter::DataObject& aDO)
  {
    if (Interpreter::isPrimitiveType(aDO)) return aDO;
    assert(Interpreter::isClassObjectType(aDO) &&
           otools::isScalarObject(aDO.getClassObject().get()));
    return otools::getScalarValue(aDO.getClassObject().get());
  }//getDataObjectFromPrimitiveOrScalarType
  
}}} // end namespace Model Utils Parser
