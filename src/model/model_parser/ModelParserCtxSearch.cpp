// Self first
#include "ModelParserCtxSearch.hpp"

#include "SearchContextObject.hpp"
#include "MVCUtils.hpp"

// Search specific information
#include "DFSInformation.hpp"

// AST Macros
#include "ModelMacro.hpp"

// Utilities
#include "ParserUtils.hpp"
#include "InterpreterUtils.hpp"
#include "ObjectHelper.hpp"

#include <cassert>

namespace iutils = Model::Utils::Interpreter;
namespace otools = Interpreter::ObjectTools;

namespace Model {

  static IR::SymbolTable::SymbolTableEntrySPtr
  lookupSymbolEntryOnModules(const std::vector<IR::IRModuleSPtr>& aMods,
                             const IR::IRSymbol::SymbolName& aSymb)
  {
    IR::SymbolTable::SymbolTableEntrySPtr symbEntry(nullptr);
    for (auto& m : aMods)
    {
      symbEntry = lookupSymbol(m, aSymb);
      if (symbEntry)
      {
        return symbEntry;
      }
    }
    return symbEntry;
  }//getSymbolEntryFromModule
  
  // Returns the var expression for "aVarID" from the variable module "aMod".
  // Returns nullptr if no variable expression is present
  static std::shared_ptr<IR::IRExprVar> getVarExprFromID(const std::string aVarID,
                                                         const IR::IRModuleSPtr& aMod)
  {
    // Look in variable module for each "aVarID"
    // get all the leaves since their symbol tables are brothers and they do not
    // share symbols although they share the parents.
    auto modSet = getLeaves(aMod);
    assert(!modSet.empty());
    
    // Lookup for module containing the given symbol
    auto symbEntry = lookupSymbolEntryOnModules(modSet, aVarID);
    if (!symbEntry) { return nullptr; }
    
    // Get the (unique) expr containing the symbol
    assert((symbEntry->getLinkSet()).size() == 1);
    auto varExpr = *((symbEntry->getLinkSet()).begin());
    
    assert(IR::IRExprVar::isa(varExpr.get()));
    return std::static_pointer_cast<IR::IRExprVar>(varExpr);
  }//getVarExprFromID
  
  static std::vector<IR::IRExprVarSPtr> getModelVarExprList(const IR::IRModuleSPtr& aVarModule)
  {
    // Get each symbol var and the correspondent expression from the symbol name
    std::vector<IR::IRExprVarSPtr> varExprList;
    for (const auto& it : *aVarModule->getSymbolTable())
    {
      if (IR::IRSymbolVar::isa(it.second->getSymbol().get()))
      {
        varExprList.push_back(getVarExprFromID(it.first, aVarModule));
      }
    }
    
    return varExprList;
  }//getModelVarExprList
  
  static std::vector<IR::IRExprVarSPtr> getEnvironmentList(const SearchContextObject* aSrc,
                                                           const IR::IRModuleSPtr& aMod)
  {
    using IRExprSPtr = IR::IRExprVarSPtr;
    std::vector<IR::IRExprVarSPtr> envVars;
    
    // Reference to a symbol, get the expression containing it
    auto ctx = aMod->getContext();
    auto mdlName = IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_VARIABLE);
    auto varModule = ctx->globalModule()->getSubModule(mdlName);
    assert(varModule);
    
    // For each branching variable, get its (IR) expression.
    // First, get the search object
    assert(Interpreter::isClassObjectType(aSrc->payload()) &&
           otools::isSearchObject((aSrc->payload()).getClassObject().get()));
    auto srcObjPtr = (aSrc->payload()).getClassObject().get();
    
    // Then, get the search scope from the search object
    auto srcScope = otools::getSearchScope(srcObjPtr).getClassObject().get();
    if (otools::isVariableObject(srcScope))
    {
      // Add IR variable ID into the set of branching variables
      envVars.push_back(getVarExprFromID(otools::getVarID(srcScope), varModule));
    }
    else
    {
      // The other type argument of the branching constraint must be a list
      assert(otools::isListObject(srcScope));
      for (const auto& var : otools::getListValues(srcScope))
      {
        if (Interpreter::isAnyObjectType(var))
        {
          return getModelVarExprList(varModule);
        }
        assert(Interpreter::isClassObjectType(var) &&
               otools::isVariableObject(var.getClassObject().get()));
        envVars.push_back(getVarExprFromID(otools::getVarID(var.getClassObject().get()),
                                           varModule));
      }
    }
    
    return envVars;
  }//getEnvironmentList
  
  static void parseEnvironmentSetVariables(const SearchContextObject* aSrc,
                                           const IR::IRModuleSPtr& aMod,
                                           const IR::IRSrcDeclSPtr& aSrcDecl)
  {
    assert(aSrc && aSrcDecl);
    
    // Get branching list if any
    auto envList = getEnvironmentList(aSrc, aMod);
    if (!envList.empty())
    {
      aSrcDecl->setBranchingVarList(envList);
    }
  }//parseEnvironmentSetVariables
  
  static void parseSearchStrategyInformation(const std::shared_ptr<SearchInformation>& aSrcInfo,
                                             const IR::IRSrcDeclSPtr& aSrcDecl)
  {
    assert(aSrcInfo && aSrcDecl);
    
    // Set the information type
    auto infoType = aSrcInfo->getSearchInfoType();
    switch (infoType) {
      case DFSInformation::SearchInformationType::SI_DFS:
      {
        aSrcDecl->setSearchType(IR::IRSrcDecl::SearchType::ST_DFS);
        break;
      }
      default:
      {
        assert(infoType == DFSInformation::SearchInformationType::SI_GENETIC);
        aSrcDecl->setSearchType(IR::IRSrcDecl::SearchType::ST_GENETIC);
      }
        break;
    }
    
    // Set the pointer to the information instance
    aSrcDecl->setSearchInformation(aSrcInfo);
  }//parseSearchStrategyInformation
  
  ModelParserCtxSearch::ModelParserCtxSearch()
  : ModelParserCtx(ModelParserCtxType::MDLPRS_SEARCH)
  {
  }
  
  void ModelParserCtxSearch::parseModelContext(const ModelContext& aCtx,
                                               const IR::IRModuleSPtr& aMod)
  {
    assert(aMod);
    
    // Get search declaration module
    auto srcMod =
    aMod->getSubModule(IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_SEARCH));
    assert(srcMod);
    
    // Parse each search context object and add a correspondent search declaration node into IR
    const auto& ctxSrcList = aCtx.getContextObjectVectorByType(ContextObject::Type::CTX_OBJ_SRC);
    if(ctxSrcList.empty())
    {
      // No search context object, add a default one into IR and return
      addDefaultIRSearchDeclNode(srcMod);
      return;
    }
    
    // Add a search node in IR for each search context object
    for (auto& srcObj : ctxSrcList)
    {
      addIRSearchDeclNode(srcObj, srcMod);
    }
  }//parseModelContext
  
  void ModelParserCtxSearch::addDefaultIRSearchDeclNode(const IR::IRModuleSPtr& aMod)
  {
    assert(aMod && !aMod->isGlobalModule());
    
    auto f = getConstructionFacade();
    auto oldBlk = f->setBlock(aMod->getBlock());
    
    // Create a search declaration with default id and index 1
    auto srcDecl = f->srcDecl(SearchContextObject::getSearchContextObjectIdPrefix() + "1");
    assert(srcDecl);
    
    // Set default search type: DFS
    srcDecl->setSearchType(IR::IRSrcDecl::SearchType::ST_DFS);
    
    // Create a new search information instance with default DFS values
    auto srcInfo = std::make_shared<DFSInformation>();
    
    srcInfo->setVariableChoice(DFSInformation::VariableSelectionChoiceList[0]);
    srcInfo->setValueChoice(DFSInformation::ValueSelectionChoiceList[0]);
    srcDecl->setSearchInformation(srcInfo);
    
    aMod->getBlock()->appendStmt(f->stmt(srcDecl));
    
    f->setBlock(oldBlk);
  }//addIRBranchingDeclNode
  
  void ModelParserCtxSearch::addIRSearchDeclNode(const ModelContext::CtxObjSPtr& aSrc,
                                                 const IR::IRModuleSPtr& aMod)
  {
    assert(aMod && aSrc && !aMod->isGlobalModule());
    
    auto f = getConstructionFacade();
    auto oldBlk = f->setBlock(aMod->getBlock());
    
    assert(aSrc->getType() == ContextObject::Type::CTX_OBJ_SRC && (!(aSrc->getID()).empty()));
    auto src = static_cast<const SearchContextObject*>(aSrc.get());

    auto srcDecl = f->srcDecl(src->getID());
    assert(srcDecl);
    
    // Register the set of variables that are part of the environment
    // of the search context object
    parseEnvironmentSetVariables(src, aMod, srcDecl);
    
    // Parse search-specific information
    parseSearchStrategyInformation(src->getSearchStrategyInformation(), srcDecl);

    aMod->getBlock()->appendStmt(f->stmt(srcDecl));
    
    f->setBlock(oldBlk);
  }//addIRBranchingDeclNode
  
}// end namespace Model
