// Base class
#include "LeafConstraintParameter.hpp"

#include <cassert>

namespace Model {
  
  LeafConstraintParameterPrimitive::LeafConstraintParameterPrimitive(LeafConstraintParameter::LeafConstraintParameterType aLeafConstraintParameterType)
  : pLeafConstraintParameterType(aLeafConstraintParameterType)
  {
  }

  LeafConstraintParameterPrimitiveInt64::LeafConstraintParameterPrimitiveInt64(INT_64 aScalarInt)
  : LeafConstraintParameterPrimitive(LeafConstraintParameter::LeafConstraintParameterType::LC_PARAM_INT64)
  , pValue(aScalarInt)
  {
  }
  
  LeafConstraintParameterPrimitiveBool::LeafConstraintParameterPrimitiveBool()
  : LeafConstraintParameterPrimitive(LeafConstraintParameter::LeafConstraintParameterType::LC_PARAM_BOOL)
  , pValue(boost::logic::indeterminate)
  {
  }
  
  LeafConstraintParameterPrimitiveVarId::LeafConstraintParameterPrimitiveVarId(const std::string& aVarId)
  : LeafConstraintParameterPrimitive(LeafConstraintParameter::LeafConstraintParameterType::LC_PARAM_VAR_ID)
  , pValue(aVarId)
  {
  }
  
}// end namespace Model
