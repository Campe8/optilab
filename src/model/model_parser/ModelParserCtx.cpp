// Self first
#include "ModelParserCtx.hpp"

#include "ModelUtils.hpp"
#include "MVCUtils.hpp"
#include "IRUtils.hpp"

#include <cassert>

namespace Model {
  
	ModelParserCtx::ModelParserCtx(ModelParserCtxType aType)
	: pModelParserType(aType)
	, pConstructionFacade(nullptr)
	{
		pConstructionFacade.reset(new IR::IRConstructionFacade());
	}
  
}// end namespace Model
