// Base class
#include "ModelParserCtxVariable.hpp"

// Context utility functions from the interpreter
#include "VariableContextObject.hpp"
#include "OPCode.hpp"
#include "ObjectHelper.hpp"
#include "BaseObject.hpp"
#include "ParserUtils.hpp"

// IR
#include "IRDefs.hpp"
#include "IRExprVar.hpp"
#include "IRDomDeclBounds.hpp"
#include "IRDomDeclExt.hpp"
#include "IRSymbolVar.hpp"
#include "IRTypeArray.hpp"

// AST Macros
#include "ModelMacro.hpp"

#include "CLIUtils.hpp"
#include "GlobalsType.hpp"

#include <algorithm>
#include <cassert>

namespace cutils = CLI::Utils;
namespace otools = Interpreter::ObjectTools;

namespace Model {
  
  static IR::IRExprVarSPtr const parseVariableSemantic(const VariableContextObject* aVar,
                                                       const IR::IRModuleSPtr& aMod,
                                                       const std::vector<IR::IRDomDeclSPtr>& aDomDecl,
                                                       IR::IRConstructionFacade& aFacade)
  {
    assert(aVar && (aVar->payload()).isClassObject());
    assert(!aDomDecl.empty() && aDomDecl[0]->getDomainType());
    
    auto oldBlk = aFacade.setBlock(aMod->getBlock());
    
    // Get the object variable
    auto objVar = (aVar->payload()).getClassObject().get();
    
    // Get variable ID and create the corresponding symbol variable
    auto id = otools::getVarID(objVar);
    
    // Get variable dimensions
    std::shared_ptr<IR::IRType> varType = aDomDecl[0]->getDomainType();
    
    // Set dimensions of the variable (scalar, array, matrix)
    auto size = otools::getVarDomainDimensionsSize(objVar);
    auto dims = otools::getVarDomainDimensionsVals(objVar);
    
    if(size > 0)
    {
      // First dimension is number of rows
      if(dims.second == 0)
      {
        // Array
        varType = aMod->getContext()->typeFactory()->
        typeArray(static_cast<std::size_t>(dims.first), varType);
      }
      else
      {
        // Matrix 2D
        auto arrayType = aMod->getContext()->typeFactory()->
        typeArray(static_cast<std::size_t>(dims.second), varType);
        varType = aMod->getContext()->typeFactory()->
        typeArray(static_cast<std::size_t>(dims.first), arrayType);
      }
    }
    
    // Create new expression variable
    IR::IRExprVar::SymbVarSPtr svar = aFacade.symbolFactory()->symbolVar(id, varType);
    auto varExpr = aFacade.exprVar(svar);
    
    // Parse semantics
    auto semantic = otools::getVarSemantic(objVar);
    switch (semantic) {
      case VAR_SPEC_DECISION:
        varExpr->setSemantic(IR::IRExprVar::VarSemantic::VS_DECISION);
        break;
      case VAR_SPEC_SUPPORT:
        varExpr->setSemantic(IR::IRExprVar::VarSemantic::VS_SUPPORT);
        break;
      case VAR_SPEC_OPT_MAX:
      {
        varExpr->setSemantic(IR::IRExprVar::VarSemantic::VS_OBJECTIVE_MAX);
        model_assert_msg(size == 0, "Multi-dimensional objective variables not supported");
      }
        break;
      default:
      {
        assert(semantic == VAR_SPEC_OPT_MIN);
        varExpr->setSemantic(IR::IRExprVar::VarSemantic::VS_OBJECTIVE_MIN);
        model_assert_msg(size == 0, "Multi-dimensional objective variables not supported");
      }
        break;
    }
    
    // Check whether or not output should be suppressed
    if(aVar->isOutput())
    {
      varExpr->setOutput(IR::IRExprVar::VarOutput::VO_FORCED_ON);
    }
    else if(!aVar->isOutput())
    {
      varExpr->setOutput(IR::IRExprVar::VarOutput::VO_FORCED_OFF);
    }
    
    aFacade.setBlock(oldBlk);
    
    return varExpr;
  }//parseVariableSemantic

  static IR::IRDomDeclSPtr const parseDomain(const Interpreter::DataObject& aDO,
                                             IR::IRTypeID aType, const IR::IRModuleSPtr& aMod,
                                             IR::IRConstructionFacade& aCF)
  {
    assert(aDO.composeSize() > 0);
    
    // Set current block in construction facade
    auto oldBlk = aCF.setBlock(aMod->getBlock());
    
    if(aType == IR::IRTypeID::IR_TYPE_BOOLEAN)
    {
      const auto& delem1 = Utils::Parser::getDataObjectFromPrimitiveOrScalarType(aDO[0]);
      auto base = delem1.getData();
      for(std::size_t idx{1}; idx < aDO.composeSize(); ++idx)
      {
        auto elem = Utils::Parser::getDataObjectFromPrimitiveOrScalarType(aDO[idx]).getData();
        if(Interpreter::DataObject(base->op_ne(elem),
                            Interpreter::DataObject::DataObjectType::DOT_BOOL).getDataValue<bool>())
        {
          // At least one element different than the other.
          // It implies that one value is true and the other is false.
          // Therefore, create a new Boolean domain and return
          auto domain = aCF.domDeclBoolean();
          aCF.setBlock(oldBlk);
          
          return  domain;
        }
      }//for
      
      // The domain is a Boolean singleton.
      // Check the value (true/false), create the domain and return.
      // @note casting to integer, even if it is of bool type, it will return a number
      // different than 0 (i.e., 1) for true values
      bool singletonValue{false};
      if (delem1.castDataValue<int>() != 0)
      {
        singletonValue = true;
      }
      
      // Create the domain and return
      auto domain = aCF.domDeclBoolean(singletonValue);
      aCF.setBlock(oldBlk);
      
      return  domain;
    }
    
    // Here the domain can only be of integer type
    assert(aType == IR::IRTypeID::IR_TYPE_INTEGER);
    
    std::vector<int> domainVals;
    domainVals.reserve(aDO.composeSize());
    for(const auto& dobj : aDO)
    {
      auto delem = Utils::Parser::getDataObjectFromPrimitiveOrScalarType(dobj).castDataValue<int>();
      domainVals.push_back(delem);
    }
    
    if(domainVals.size() == 1)
    {
      // Singleton domain
      auto ec = aCF.exprConst( aCF.symbolFactory()->symbolConstInteger(domainVals[0]) );
      auto domain = aCF.domDeclBounds(ec, ec);
      
      aCF.setBlock(oldBlk);
      return domain;
    }
    else if(domainVals.size() == 2)
    {
      if(domainVals[0] > domainVals[1]) std::swap(domainVals[0], domainVals[1]);
      auto lb = aCF.exprConst( aCF.symbolFactory()->symbolConstInteger( domainVals[0] ) );
      auto ub = aCF.exprConst( aCF.symbolFactory()->symbolConstInteger( domainVals[1] ) );
      auto domain = aCF.domDeclBounds(lb, ub);
      
      aCF.setBlock(oldBlk);
      return domain;
    }
    else
    {
      std::set<IR::IRExprConsSPtr> domSet;
      for(const auto& val : domainVals)
      {
        domSet.insert(aCF.exprConst(aCF.symbolFactory()->symbolConstInteger(val)));
      }
      auto domain = aCF.domDeclExt(domSet);
      
      aCF.setBlock(oldBlk);
      return domain;
    }
  }//parseDomain
  
  static IR::IRTypeID typeInferenceOnDomain(const Interpreter::DataObject& aDO)
  {
    IR::IRTypeID type = IR::IRTypeID::IR_TYPE_VOID;
    if(!aDO.isComposite())
    {
      // Single element, return its type
      auto doType = aDO.dataObjectType();
      if (Interpreter::isClassObjectType(aDO))
      {
        auto obj = aDO.getClassObject().get();
        assert(otools::isScalarObject(obj));
        
        doType = otools::getScalarValue(obj).dataObjectType();
      }
      switch (doType) {
        case Interpreter::DataObject::DataObjectType::DOT_INT:
          return IR::IRTypeID::IR_TYPE_INTEGER;
        case Interpreter::DataObject::DataObjectType::DOT_BOOL:
          return IR::IRTypeID::IR_TYPE_BOOLEAN;
        default:
          assert(doType == Interpreter::DataObject::DataObjectType::DOT_DOUBLE);
          return IR::IRTypeID::IR_TYPE_DOUBLE;
      }
    }
    
    // Check the type recursively
    for(const auto& dobj : aDO)
    {
      auto recType = typeInferenceOnDomain(dobj);
      if(type == IR::IRTypeID::IR_TYPE_VOID || recType == IR::IRTypeID::IR_TYPE_BOOLEAN)
      {
        // Apply promotion to Boolean types
        type = recType;
      }
      else if(recType == IR::IRTypeID::IR_TYPE_DOUBLE && type != IR::IRTypeID::IR_TYPE_BOOLEAN)
      {
        // Apply promotion to Double types
        type = recType;
      }
      type = recType;
    }
    
    return type;
  }//typeInferenceOnDomain
  
  static std::vector<IR::IRDomDeclSPtr> const parseDomainDeclaration(const VariableContextObject* aVar,
                                                                     const IR::IRModuleSPtr& aMod,
                                                                     IR::IRConstructionFacade& aCF)
  {
    // Domain declaration
    std::vector<IR::IRDomDeclSPtr> domDecl;
    auto objVar = (aVar->payload()).getClassObject().get();
    
    // Variable type: Integer, Boolean, etc.
    // it must be inferred from the domain of the variable
    auto domain = otools::getVarDomainList(objVar);
    auto domainArray = otools::getVarDomainArrayList(objVar);
    auto domainArraySize = otools::getListSize(domainArray);
    if(domainArraySize > 0)
    {
      // If there are domains in the domain array, use them to get the type
      domain = domainArray;
    }
    IR::IRTypeID varType = typeInferenceOnDomain(otools::getListValues(domain));
    
    // Get the number of domains to generate
    auto size = otools::getVarDomainDimensionsSize(objVar);
    if(size > 0)
    {
      // Create an IR domain declaration for each sub-domain in the matrix
      // 1) Check correctness in the number of domains
      assert(domainArraySize == 0 || domainArraySize == size);
      
      // 2) create a domain descriptor of given type for each sub-domain
      if(domainArraySize > 0)
      {
        for (const auto& dobj : otools::getListValues(domainArray))
        {
          domDecl.push_back(parseDomain(dobj, varType, aMod, aCF));
        }
      }
      else
      {
        // Use the same domain for all dimensions
        domDecl.resize(size, parseDomain(otools::getListValues(domain), varType, aMod, aCF));
      }
    }
    else
    {
      // Create an IR domain declaration using the base domain
      domDecl.push_back(parseDomain(otools::getListValues(domain), varType, aMod, aCF));
    }
    
    return domDecl;
  }//parseDomainDeclaration
  
  ModelParserCtxVariable::ModelParserCtxVariable()
  : ModelParserCtx(ModelParserCtxType::MDLPRS_VARIABLE)
  {
  }
  
  void ModelParserCtxVariable::parseModelContext(const ModelContext& aCtx,
                                                 const IR::IRModuleSPtr& aMod)
  {
    assert(aMod);
    
    // Parse variable context objects

    // Get variable declaration module
    auto varMod = aMod->
              getSubModule(IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_VARIABLE));
    assert(varMod);
    
    // Parse each leaf node and add a variable declaration node into IR
    for (auto& vObj : aCtx.getContextObjectVectorByType(ContextObject::Type::CTX_OBJ_VAR))
    {
      addIRVarDeclNode(vObj, varMod);
    }
  }//parseModelContext
  
  void ModelParserCtxVariable::addIRVarDeclNode(const ModelContext::CtxObjSPtr& aVar,
                                                const IR::IRModuleSPtr& aMod)
  {
    assert(aMod && !aMod->isGlobalModule());

    auto f = getConstructionFacade();
    auto oldBlk = f->setBlock(aMod->getBlock());
    
    assert(aVar && aVar->getType() == ContextObject::Type::CTX_OBJ_VAR);
    assert((aVar->payload()).isClassObject());
    auto var = static_cast<const VariableContextObject*>(aVar.get());
    
    // Get domain declaration.
    // @note the domain of the variable sets its type
    auto domDecl = parseDomainDeclaration(var, aMod, *f);
    assert(!domDecl.empty());
    
    // Get expression variable from the context and
    // set its type to the type of its domain
    auto varExpr = parseVariableSemantic(var, aMod, domDecl, *f);
    assert(varExpr);
    
    auto varDecl = f->varDecl(varExpr, domDecl);
    assert(varDecl);
    
    aMod->getBlock()->appendStmt(f->stmt(varDecl));
    
    f->setBlock(oldBlk);
  }//addIRVarDeclNode
  
}// end namespace Model
