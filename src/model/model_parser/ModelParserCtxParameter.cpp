// Self first
#include "ModelParserCtxParameter.hpp"

#include "MVCUtils.hpp"
#include "ParserUtils.hpp"
#include "ObjectHelper.hpp"

// IR
#include "IRInc.hpp"
#include "IRDefs.hpp"
#include "IRSymbolFcn.hpp"
#include "IRSymbolVar.hpp"
#include "IRExprAssign.hpp"

// AST Macros
#include "ModelMacro.hpp"
#include "GlobalsType.hpp"
#include "IRUtils.hpp"
#include "IRExprConst.hpp"

#include <vector>
#include <cassert>

namespace otools = Interpreter::ObjectTools;

namespace Model {
  
  static IR::IRConstructionFacade::IRExprSPtr getAssignExpr(const std::string& aLHSID,
                                                            const std::shared_ptr<IR::IRExpr>& aRHS, IR::IRConstructionFacade& aCF)
  {
    // Create a new symbol const for the ID.
    auto symb = aCF.symbolFactory()->symbolVar(aLHSID, aRHS->getType());
    
    /// Return the assignment expression:
    /// lhs = rhs
    // @note the assignment expression links the symbol of the LHS to the RHS expression
    // in the symbol table
    return aCF.exprAssign(aCF.exprStdVar(symb), aRHS);
  }//getAssignExpr
  
  static IR::IRConstructionFacade::IRExprSPtr getScalarExpr(Interpreter::BaseObject* aParam,
                                                            const IR::IRModuleSPtr& aMod,
                                                            IR::IRConstructionFacade& aCF)
  {
    // Temporary check to be removed when using multi-dimensional parameters
    assert(otools::isScalarParameterObject(aParam));
    auto scalar = otools::getScalarParameterValue(aParam);
    
    // Get the IR expression for the scalar
    auto exprConst = Utils::Parser::getExprForDataObject(scalar, aMod, aCF);
    assert(exprConst);
    
    // Returns the assignment expression ID = scalar
    return getAssignExpr(MVC::Utils::getIDFromDataObject(aParam), exprConst, aCF);
  }//getScalarExpr
  
  static IR::IRConstructionFacade::IRExprSPtr getMatrixExpr(Interpreter::BaseObject* aParam,
                                                            const IR::IRModuleSPtr& aMod,
                                                            IR::IRConstructionFacade& aCF)
  {
    // Temporary check to be removed when using multi-dimensional parameters
    assert(otools::getParameterDimensions(aParam).size() == 1);
    assert(otools::isListObject(aParam));
    
    std::vector<IR::IRSymbolFactory::IRSymbolPtr> array;
    for(const auto& val : otools::getListValues(aParam))
    {
      array.push_back( Utils::Parser::getSymbolForDataObject(val, aMod, aCF) );
    }//for
    
    auto arrayID = MVC::Utils::getIDFromDataObject(aParam);
    auto exprArray = aCF.exprConst(aCF.symbolFactory()->symbolConstArray(array, arrayID));
    
    // Returns the assignment expression ID = array
    return getAssignExpr(arrayID, exprArray, aCF);
  }//getMatrixExpr
  
  ModelParserCtxParameter::ModelParserCtxParameter()
  : ModelParserCtx(ModelParserCtxType::MDLPRS_PARAMETER)
  {
  }
  
  void ModelParserCtxParameter::parseModelContext(const ModelContext& aCtx, const IR::IRModuleSPtr& aMod)
  {
    assert(aMod);
  
    // Get constraint declaration module
    auto parMod = aMod->getSubModule(IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_PARAMETER));
    assert(parMod);
    
    // Parse each context object and add a constraint declaration node into IR
    for (auto& parObj : aCtx.getContextObjectVectorByType(ContextObject::Type::CTX_OBJ_STD))
    {
      addIRParamNode(parObj, parMod);
    }
  }//parseModelContext
  
  void ModelParserCtxParameter::addIRParamNode(const ModelContext::CtxObjSPtr& aPar, const IR::IRModuleSPtr& aMod)
  {
    assert(aMod);
    assert(!aMod->isGlobalModule());
    
    auto f = getConstructionFacade();
    auto oldBlk = f->setBlock(aMod->getBlock());

    // Get the class object
    auto parObj = (aPar->payload()).getClassObject().get();
    
    // Get const symbol
    IR::IRConstructionFacade::IRExprSPtr expr{nullptr};
    if(otools::isScalarParameterObject(parObj))
    {
      expr = getScalarExpr(parObj, aMod, *f);
    }
    else
    {
      expr = getMatrixExpr(parObj, aMod, *f);
    }
    assert(expr);
    
    // Append a new stmt to the block
    aMod->getBlock()->appendStmt(f->stmt(expr));
    
    f->setBlock(oldBlk);
  }//addIRMatDeclNode
  
}// end namespace Model
