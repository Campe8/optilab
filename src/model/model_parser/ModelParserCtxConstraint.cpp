// Self first
#include "ModelParserCtxConstraint.hpp"

#include "ConstraintContextObject.hpp"
#include "MVCUtils.hpp"
#include "ObjectHelper.hpp"
#include "ModelUtils.hpp"
#include "ParserUtils.hpp"

// IR
#include "IRInc.hpp"
#include "IRDefs.hpp"
#include "IRSymbolFcn.hpp"

// AST Macros
#include "ModelMacro.hpp"

#include "GlobalsType.hpp"

#include "IRUtils.hpp"
#include "InterpreterUtils.hpp"
#include "CLIUtils.hpp"

#include <vector>
#include <cassert>

namespace iutils = Model::Utils::Interpreter;
namespace otools = Interpreter::ObjectTools;

namespace Model {

  static std::vector<IR::IRExprFcn::IRExprSPtr> getConstraintArguments(const ConstraintContextObject* aCon,
                                                                       const IR::IRModuleSPtr& aMod,
                                                                       IR::IRConstructionFacade& aCF)
  {
    using IRExprSPtr = std::shared_ptr<IR::IRExpr>;
    
    auto tf = aMod->getContext()->typeFactory();
    
    assert(Interpreter::isClassObjectType(aCon->payload()));
    const auto& postFcn = otools::getConstraintPostFcn((aCon->payload()).getClassObject().get());
    
    std::vector<IRExprSPtr> args;
    for (const auto& arg : postFcn)
    {
      auto exprArg = Utils::Parser::getExprForDataObject(arg, aMod, aCF);
      assert(exprArg);
      
      args.push_back(exprArg);
    }
    
    return args;
  }//getConstraintArguments
  
  static std::shared_ptr<IR::IRTypeFunction> getConFcnType(
                                                    std::vector<IR::IRExprFcn::IRExprSPtr> aFcnArgs,
                                                    const IR::IRModuleSPtr& aMod)
  {
    auto tf = aMod->getContext()->typeFactory();
    
    // Vector of types to return
    std::vector<IR::IRTypeSPtr> vt;
    for (auto& arg : aFcnArgs)
    {
      vt.push_back(arg->getType());
    }
    
    return  tf->typeFunctionConstraint(vt);
  }//getConFcnType
  
  static IR::IRExprFcn::SymbFcnSPtr getSymbolFcn(const ConstraintContextObject* aCon,
                                                 std::vector<IR::IRExprFcn::IRExprSPtr> aFcnArgs,
                                                 const IR::IRModuleSPtr& aMod,
                                                 IR::IRConstructionFacade& aCF)
  {
    assert(aCon && Interpreter::isClassObjectType(aCon->payload()));
    auto conObject = (aCon->payload()).getClassObject().get();
    
    assert(otools::isConstraintObject(conObject));
    
    // Get constraint ID
    std::string conID = otools::getConstraintPostFcn(conObject).getFcn().getFcnName();
    
    // Get constraint fcn type
    auto fcnType = getConFcnType(aFcnArgs, aMod);
    assert(fcnType);
    
    return aCF.symbolFactory()->symbolFcn(conID, fcnType);
  }//getSymbolFcn
  
  static IR::IRExprFcnSPtr getConFcnExpr(const ConstraintContextObject* aCon,
                                         const IR::IRModuleSPtr& aMod,
                                         IR::IRConstructionFacade& aCF)
  {
    assert(aCon);
    
    // 1 - Get vector of fcn arguments
    auto cargs = getConstraintArguments(aCon, aMod, aCF);
    
    // 2 - Create a symbol fcn
    auto symbFcn = getSymbolFcn(aCon, cargs, aMod, aCF);
    
    // 3 - Create a constraint function expression.
    // @note this will set the type of the symbol
    // into the correspondent symbol table entry
    auto eFcn = aCF.exprFcn(symbFcn, cargs);
    
    // 4 - Unlink constraint symbol from symbol table
    // @note the same symbol, e.g., lin_eq may have different
    // types for example during IR transformation, e.g., reif constraints
    symbFcn->unlinkType();
    
    return eFcn;
  }//getConFcnExpr
  
  static void setPropagationType(const ConstraintContextObject* aCon, const IR::IRConDeclSPtr& aDecl)
  {
    // Constraint propagation type
    if (aCon->getPropagationType() == ConstraintContextObject::PropagationType::PT_BOUNDS)
    {
      aDecl->setPropagationType(IR::IRConDecl::PropagationType::PT_BOUNDS);
      return;
    }
    
    assert(aCon->getPropagationType() == ConstraintContextObject::PropagationType::PT_DOMAIN);
    aDecl->setPropagationType(IR::IRConDecl::PropagationType::PT_DOMAIN);
  }//setPropagationType
  
  ModelParserCtxConstraint::ModelParserCtxConstraint()
  : ModelParserCtx(ModelParserCtxType::MDLPRS_CONSTRAINT)
  {
  }
  
  void ModelParserCtxConstraint::parseModelContext(const ModelContext& aCtx, const IR::IRModuleSPtr& aMod)
  {
    assert(aMod);
    
    // Get constraint declaration module
    auto conMdl = aMod->getSubModule(IR::IRContext::getModuleName(IR::IRContext::ContextModule::CM_CONSTRAINT));
    assert(conMdl);
    
    // Parse each context object and add a constraint declaration node into IR
    for (auto& conObj : aCtx.getContextObjectVectorByType(ContextObject::Type::CTX_OBJ_CON))
    {
      addIRConDeclNode(conObj, conMdl);
    }
  }//parseModelContext
  
  void ModelParserCtxConstraint::addIRConDeclNode(const ModelContext::CtxObjSPtr& aCon, const IR::IRModuleSPtr& aMod)
  {
    assert(aMod && !aMod->isGlobalModule());
    
    auto f = getConstructionFacade();
    auto oldBlk = f->setBlock(aMod->getBlock());
    
    // Get constraint fcn call expr
    assert(aCon && aCon->getType() == ContextObject::Type::CTX_OBJ_CON);
    auto con = static_cast<const ConstraintContextObject*>(aCon.get());
    
    // Get the function expression representing the constraint
    auto efcn = getConFcnExpr(con, aMod, *f);
    
    // Get constraint's context ID
    auto ctxID = aCon->getID();
    
    auto conDecl = f->conDecl(efcn, ctxID);
    assert(conDecl);
    
    // Set propagation type
    setPropagationType(con, conDecl);
    
    // Append a new stmt to the block
    aMod->getBlock()->appendStmt(f->stmt(conDecl));
    
    f->setBlock(oldBlk);
  }//addIRConDeclNode
  
}// end namespace Model
