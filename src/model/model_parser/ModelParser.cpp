// Base class
#include "ModelParser.hpp"

// Ctx parsers
#include "ModelParserCtxConstraint.hpp"
#include "ModelParserCtxParameter.hpp"
#include "ModelParserCtxSearch.hpp"
#include "ModelParserCtxVariable.hpp"

#include "ModelUtils.hpp"

#include "ModelDefs.hpp"
#include "ModelMacro.hpp"

#include <cassert>

namespace Model {
  
  static void setContextSpec(const IR::IRContextSPtr& aIRCtx, const ModelContext& aMdlCtx)
  {
    aIRCtx->getContextSpec()->setModelName(aMdlCtx.getModelName());
    aIRCtx->getContextSpec()->setSolutionLimit(0);
  }//setContextSpec
  
  ModelParser::ModelParser()
  : pIRConstructionFacade(new IR::IRConstructionFacade())
  {
    // Initialize parser register with ctx parsers
    initializeParserRegister();
  }
  
  void ModelParser::initializeParserRegister()
  {
    auto parserID = ModelParserCtx::ModelParserCtxTypeToInt(ModelParserCtx::ModelParserCtxType::MDLPRS_VARIABLE);
    pParserCtxRegister[parserID] = std::shared_ptr<ModelParserCtxVariable>(new ModelParserCtxVariable());
    
    parserID = ModelParserCtx::ModelParserCtxTypeToInt(ModelParserCtx::ModelParserCtxType::MDLPRS_CONSTRAINT);
    pParserCtxRegister[parserID] = std::shared_ptr<ModelParserCtxConstraint>(new ModelParserCtxConstraint());
    
    parserID = ModelParserCtx::ModelParserCtxTypeToInt(ModelParserCtx::ModelParserCtxType::MDLPRS_PARAMETER);
    pParserCtxRegister[parserID] = std::shared_ptr<ModelParserCtxParameter>(new ModelParserCtxParameter());
    
    parserID = ModelParserCtx::ModelParserCtxTypeToInt(ModelParserCtx::ModelParserCtxType::MDLPRS_SEARCH);
    pParserCtxRegister[parserID] = std::shared_ptr<ModelParserCtxSearch>(new ModelParserCtxSearch());
  }//initializeParserRegister
  
  IR::IRContextSPtr ModelParser::parse(const ModelContext& aCtx)
  {
    // Create a new IR context
    auto ctx = IR::createIRContext();
    assert(ctx);
    
    // Set model specific information into IR context
    setContextSpec(ctx, aCtx);
    
    // Get the global module
    auto glbMod = ctx->globalModule();
    assert(glbMod);
    
    // Here starts the actual parsing of the model context
    // in order to fill the IR with the information contained in the context
    parseModelContext(aCtx, glbMod);
    
    return ctx;
  }//parse
  
  std::shared_ptr<ModelParserCtx> ModelParser::getParserCtx(ModelParserCtx::ModelParserCtxType aType) const
  {
    auto parserID = ModelParserCtx::ModelParserCtxTypeToInt(aType);
    if (pParserCtxRegister.find(parserID) == pParserCtxRegister.end())
    {
      return std::shared_ptr<ModelParserCtx>(nullptr);
    }
    return pParserCtxRegister.at(parserID);
  }//getParserCtx
  
  void ModelParser::parseModelContext(const ModelContext& aMdlCtx, const IR::IRModuleSPtr& aMod)
  {
    assert(aMod);
    assert(aMod->isGlobalModule());
    
    // Parse the variable context and generates corresponding IR
    auto parser = getParserCtx(ModelParserCtx::ModelParserCtxType::MDLPRS_VARIABLE);
    assert(parser);
    parser->parseModelContext(aMdlCtx, aMod);
    
    // Parse the parameter context and generates corresponding IR
    parser = getParserCtx(ModelParserCtx::ModelParserCtxType::MDLPRS_PARAMETER);
    assert(parser);
    parser->parseModelContext(aMdlCtx, aMod);
    
    // Parse the constraint context and generates corresponding IR
    // @note this must happen after building IR for variables and parameters since
    //       constraint arguments may depend on both
    parser = getParserCtx(ModelParserCtx::ModelParserCtxType::MDLPRS_CONSTRAINT);
    assert(parser);
    parser->parseModelContext(aMdlCtx, aMod);
    
    // Parse the search context containing search sub-context and generate corresponding IR
    parser = getParserCtx(ModelParserCtx::ModelParserCtxType::MDLPRS_SEARCH);
    assert(parser);
    parser->parseModelContext(aMdlCtx, aMod);
  }//parseModelContext
  
}// end namespace Model

