// Self first
#include "CBLSDescriptor.hpp"

#include "CBLSInformation.hpp"

#define MEM_LIMIT 100000000


namespace Model {
  
  CBLSDescriptor::CBLSDescriptor(Search::SearchEngineStrategyType aSrcType)
  : SearchDescriptor(aSrcType)
  {
  }
  
  void CBLSDescriptor::initInnerSearchDescriptor(const SearchInformation* const aSrcInfo)
  {
    assert(aSrcInfo);
    auto map = aSrcInfo->getMap();
    
    assert(map->isType<int>(SearchInformation::TimeoutKey));
    assert(map->isType<int>(CBLSInformation::RandomSeedKey));
    assert(map->isType<bool>(CBLSInformation::WarmStartKey));
    assert(map->isType<int>(CBLSInformation::NeighborhoodSizeKey));
    assert(map->isType<int>(CBLSInformation::NumNeighborhoodsKey));
    assert(map->isType<bool>(CBLSInformation::AggressiveCBLSKey));
    
    setTimeout(map->lookupValue<int>(SearchInformation::TimeoutKey));
    
    pRandomSeed = map->lookupValue<int>(CBLSInformation::RandomSeedKey);
    
    pWarmStart = map->lookupValue<bool>(CBLSInformation::WarmStartKey);
    
    pNeighbohroodSize = map->lookupValue<int>(CBLSInformation::NeighborhoodSizeKey);
    checkAndThrow<int>(pNeighbohroodSize, MEM_LIMIT);
    
    pNumNeighborhoods = map->lookupValue<int>(CBLSInformation::NumNeighborhoodsKey);
    checkAndThrow<int>(pNumNeighborhoods, MEM_LIMIT);
    
    pAggressiveCBLS = map->lookupValue<bool>(CBLSInformation::AggressiveCBLSKey);
    
    // Initialize CBLS strategy specific information
    initCBLSSearchDescriptor(aSrcInfo);
  }//initInnerSearchDescriptor
  
}// end namespace Search
