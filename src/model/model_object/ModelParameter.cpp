// Self first
#include "ModelParameter.hpp"

namespace Model {
  
	ModelParameter::ModelParameter(const std::string& aParamID, const ParameterSPtr& aParam)
  : ModelObject(ModelObjectClass::MODEL_OBJ_PARAMETER)
  , pID(aParamID)
	, pParameter(aParam)
  {
  }
  
	bool ModelParameter::isa(const ModelObject* aModelObject)
	{
		return aModelObject &&
			aModelObject->getModelObjectClass() == ModelObjectClass::MODEL_OBJ_PARAMETER;
	}//isa

	ModelParameter* ModelParameter::cast(ModelObject* aModelObject)
	{
		if (!isa(aModelObject)) return nullptr;
		return static_cast<ModelParameter*>(aModelObject);
	}//cast

	const ModelParameter* ModelParameter::cast(const ModelObject* aModelObject)
	{
		if (!isa(aModelObject)) return nullptr;
		return static_cast<const ModelParameter*>(aModelObject);
	}//cast
  
  std::string ModelParameter::getID() const
  {
    return pID;
  }//getID
  
}// end namespace Search
