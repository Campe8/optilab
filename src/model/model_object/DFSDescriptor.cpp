// Self first
#include "DFSDescriptor.hpp"

#include "DFSInformation.hpp"
#include "ModelASTMacroBranch.hpp"

#include <string>

namespace Model {
  
  static Search::ValueChoiceMetricType getValChoice(const std::string& aValChoice)
  {
    if(aValChoice == MDL_AST_LEAF_BRC_VAL_INDOMAIN_MAX) return Search::ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MAX;
    else if(aValChoice == MDL_AST_LEAF_BRC_VAL_INDOMAIN_MIN) return Search::ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MIN;
    else
    {
      assert(aValChoice == MDL_AST_LEAF_BRC_VAL_INDOMAIN_RANDOM);
      return Search::ValueChoiceMetricType::VAL_METRIC_INDOMAIN_RANDOM;
    }
  }//getValueChoice
  
  static Search::VariableChoiceMetricType getVarChoice(const std::string& aVarChoice)
  {
    if(aVarChoice == MDL_AST_LEAF_BRC_VAR_INPUT_ORDER) return Search::VariableChoiceMetricType::VAR_CM_INPUT_ORDER;
    else if(aVarChoice == MDL_AST_LEAF_BRC_VAR_LARGEST) return Search::VariableChoiceMetricType::VAR_CM_LARGEST;
    else if(aVarChoice == MDL_AST_LEAF_BRC_VAR_SMALLEST) return Search::VariableChoiceMetricType::VAR_CM_SMALLEST;
    else if(aVarChoice == MDL_AST_LEAF_BRC_VAR_FIRST_FAIL) return Search::VariableChoiceMetricType::VAR_CM_FIRST_FAIL;
    else
    {
      assert(aVarChoice == MDL_AST_LEAF_BRC_VAR_ANTI_FIRST_FAIL);
      return Search::VariableChoiceMetricType::VAR_CM_ANTI_FIRST_FAIL;
    }
  }//getVariableChoice
  
  DFSDescriptor::DFSDescriptor()
  : SearchDescriptor(Search::SearchEngineStrategyType::SE_DEPTH_FIRST_SEARCH)
  {
  }
  
  void DFSDescriptor::initInnerSearchDescriptor(const SearchInformation* const aSrcInfo)
  {
    assert(aSrcInfo && aSrcInfo->getSearchInfoType() == SearchInformation::SearchInformationType::SI_DFS);
    auto map = aSrcInfo->getMap();
    
    assert(map->isType<int>(SearchInformation::TimeoutKey));
    assert(map->isType<std::string>(DFSInformation::VariableSelectionKey));
    assert(map->isType<std::string>(DFSInformation::ValueSelectionKey));
    
    auto varChoice = map->lookupValue<std::string>(DFSInformation::VariableSelectionKey);
    auto valChoice = map->lookupValue<std::string>(DFSInformation::ValueSelectionKey);
    
    // Set the timeout
    setTimeout(map->lookupValue<int>(SearchInformation::TimeoutKey));
    
    // Set the heuristic for the variable and value selection
    setVarValChoiceHeuristic(getVarChoice(varChoice), getValChoice(valChoice));
  }//initInnerSearchDescriptor
  
}// end namespace Model
