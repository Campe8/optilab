// Base class
#include "ModelObject.hpp"

#include "BacktrackManager.hpp"

namespace Model {
  
  ModelObject::ModelObject(ModelObjectClass aClass)
  : pClass(aClass)
  {
  }
  
}// end namespace Model
