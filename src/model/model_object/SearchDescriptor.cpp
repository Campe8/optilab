// Self first
#include "SearchDescriptor.hpp"

namespace Model {
  
  SearchDescriptor::SearchDescriptor(Search::SearchEngineStrategyType aStrategyType)
  : pSearchStrategyType(aStrategyType)
  , pVarChoice(Search::VariableChoiceMetricType::VAR_CM_UNDEF)
  , pValChoice(Search::ValueChoiceMetricType::VAL_METRIC_UNDEF)
  {
  }
  
  void SearchDescriptor::setVarValChoiceHeuristic(Search::VariableChoiceMetricType aVarChoice,
                                                  Search::ValueChoiceMetricType aValChoice)
  {
    pVarChoice = aVarChoice;
    pValChoice = aValChoice;
  }//setVarValChoiceHeuristic
  
  void SearchDescriptor::initSearchDescriptor(const SearchInformation* const aSrcInfo)
  {
    // Initialize default heuristic
    setVarValChoiceHeuristic(Search::VariableChoiceMetricType::VAR_CM_INPUT_ORDER,
                             Search::ValueChoiceMetricType::VAL_METRIC_INDOMAIN_MIN);
    
    // Initialize information for derived classes
    initInnerSearchDescriptor(aSrcInfo);
  }//initSearchDescriptor
  
}// end namespace Search
