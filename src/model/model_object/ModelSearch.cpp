// Self first
#include "ModelSearch.hpp"

namespace Model {
  
	ModelSearch::ModelSearch(const std::string& aID, const std::shared_ptr<SearchDescriptor>& aDescriptor)
  : ModelObject(ModelObjectClass::MODEL_OBJ_SEARCH_SEMANTIC)
  , pID(aID)
  , pDescriptor(aDescriptor)
  {
  }
  
	bool ModelSearch::isa(const ModelObject* aModelObject)
	{
		return aModelObject &&
			aModelObject->getModelObjectClass() == ModelObjectClass::MODEL_OBJ_SEARCH_SEMANTIC;
	}//isa

	ModelSearch* ModelSearch::cast(ModelObject* aModelObject)
	{
		if (!isa(aModelObject)) return nullptr;
		return static_cast<ModelSearch*>(aModelObject);
	}//cast

	const ModelSearch* ModelSearch::cast(const ModelObject* aModelObject)
	{
		if (!isa(aModelObject)) return nullptr;
		return static_cast<const ModelSearch*>(aModelObject);
	}//cast
  
  std::string ModelSearch::getID() const
  {
    return pID;
  }//getID
  
}// end namespace Search
