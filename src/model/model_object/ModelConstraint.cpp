// Self first
#include "ModelConstraint.hpp"

namespace Model {
  
	ModelConstraint::ModelConstraint(const std::string& aCtxID, const ConstraintSPtr& aConstraint)
  : ModelObject(ModelObjectClass::MODEL_OBJ_CONSTRAINT)
  , pCtxID(aCtxID)
  , pConstraint(aConstraint)
  {
  }

	bool ModelConstraint::isa(const ModelObject* aModelObject)
	{
		return aModelObject &&
			aModelObject->getModelObjectClass() == ModelObjectClass::MODEL_OBJ_CONSTRAINT;
	}//isa

	ModelConstraint* ModelConstraint::cast(ModelObject* aModelObject)
	{
		if (!isa(aModelObject)) return nullptr;
		return static_cast<ModelConstraint*>(aModelObject);
	}//cast

	const ModelConstraint* ModelConstraint::cast(const ModelObject* aModelObject)
	{
		if (!isa(aModelObject)) return nullptr;
		return static_cast<const ModelConstraint*>(aModelObject);
	}//cast

	std::string ModelConstraint::getID() const
	{
    return pCtxID;
	}//getID

}// end namespace Search
