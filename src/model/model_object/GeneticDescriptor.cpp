// Self first
#include "GeneticDescriptor.hpp"

#include "GeneticInformation.hpp"

#define MEM_LIMIT 100000000

namespace Model {
  
  GeneticDescriptor::GeneticDescriptor()
  : CBLSDescriptor(Search::SearchEngineStrategyType::SE_GENETIC)
  {
    setVarValChoiceHeuristic(Search::VariableChoiceMetricType::VAR_CM_INPUT_ORDER,
                             Search::ValueChoiceMetricType::VAL_METRIC_INDOMAIN_RANDOM);
  }

  void GeneticDescriptor::initCBLSSearchDescriptor(const SearchInformation* const aSrcInfo)
  {
    assert(aSrcInfo);
    auto map = aSrcInfo->getMap();
    
    assert(map->isType<std::size_t>(GeneticInformation::PopulationSizeKey));
    assert(map->isType<std::size_t>(GeneticInformation::NumGenerationsKey));
    assert(map->isType<std::size_t>(GeneticInformation::MutationChanceKey));
    assert(map->isType<std::size_t>(GeneticInformation::MutationSizeKey));
    
    pPopulationSize = map->lookupValue<std::size_t>(GeneticInformation::PopulationSizeKey);
    checkAndThrow(pPopulationSize, MEM_LIMIT);
    
    pNumGenerations = map->lookupValue<std::size_t>(GeneticInformation::NumGenerationsKey);
    checkAndThrow(pNumGenerations, MEM_LIMIT);
    
    pMutationChance = map->lookupValue<std::size_t>(GeneticInformation::MutationChanceKey);
    pMutationChance = pMutationChance > 100 ? 100 : pMutationChance;
    
    pMutationSize   = map->lookupValue<std::size_t>(GeneticInformation::MutationSizeKey);
  }//initCBLSSearchDescriptor
  
}// end namespace Search
