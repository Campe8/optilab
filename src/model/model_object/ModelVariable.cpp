// Self first
#include "ModelVariable.hpp"

#include "Variable.hpp"

#include <cassert>

namespace Model {
  
	ModelVariable::ModelVariable(const VariableSPtr& aVar)
  : ModelObject(ModelObjectClass::MODEL_OBJ_VARIABLE)
	, pVariable(nullptr)
  {
    pVariable = aVar;
  }
  
	bool ModelVariable::isa(const ModelObject* aModelObject)
	{
		return aModelObject &&
			aModelObject->getModelObjectClass() == ModelObjectClass::MODEL_OBJ_VARIABLE;
	}//isa

	ModelVariable* ModelVariable::cast(ModelObject* aModelObject)
	{
		if (!isa(aModelObject)) return nullptr;
		return static_cast<ModelVariable*>(aModelObject);
	}//cast

	const ModelVariable* ModelVariable::cast(const ModelObject* aModelObject)
	{
		if (!isa(aModelObject)) return nullptr;
		return static_cast<const ModelVariable*>(aModelObject);
	}//cast
  
	std::string ModelVariable::getID() const
	{
		assert(pVariable);
		return pVariable->getVariableNameId();
	}//getID

}// end namespace Search
