// Self first
#include "MCPPVariable.hpp"

#include "VariableContextObject.hpp"

#include "ModelASTMacro.hpp"
#include "InterpreterUtils.hpp"
#include "CLIUtils.hpp"
#include "OPCode.hpp"
#include "ObjectHelper.hpp"
#include "ObjectTools.hpp"
#include "PrettyPrinterTools.hpp"

#include <string>
#include <algorithm>
#include <utility>

#include <cassert>

namespace cutils = CLI::Utils;
namespace iutils = Model::Utils::Interpreter;
namespace otools = Interpreter::ObjectTools;

namespace Model {
  
  void MCPPVariable::prettyPrintInputOrder(Interpreter::BaseObject* aVar, std::ostream& aOut)
  {
    outTag("inputOrder", aOut);
    aOut << otools::getVarInputOrder(aVar);
  }//prettyPrintInputOrder
  
  void MCPPVariable::prettyPrintSemantic(Interpreter::BaseObject* aVar, std::ostream& aOut,
                                         bool aFullPrettyPrint)
  {
    auto semantic = otools::getVarSemantic(aVar);
    if(!aFullPrettyPrint && !(semantic == VAR_SPEC_OPT_MIN || semantic == VAR_SPEC_OPT_MAX))
    {
      // Don't print if not fully pretty print or semantic is different than optimization.
      // If it is optimization, print regarderless
      return;
    }
    outTag("semantic", aOut);
  
    switch (semantic)
    {
      case VAR_SPEC_DECISION:
        aOut << CLI_MSG_VAL_VAR_SEMANTIC_TYPE_DECISION;
        break;
      case VAR_SPEC_SUPPORT:
        aOut << CLI_MSG_VAL_VAR_SEMANTIC_TYPE_SUPPORT;
        break;
      case VAR_SPEC_OPT_MAX:
      {
        aOut << '\n';
        outTag(MDL_AST_LEAF_VAR_SEMANTIC_TYPE_OBJECTIVE, aOut, 1);
        aOut << MDL_AST_LEAF_VAR_SEMANTIC_OPT_EXTREMUM_MAX;
      }
        break;
      default:
        assert(VAR_SPEC_OPT_MIN);
      {
        aOut << '\n';
        outTag(MDL_AST_LEAF_VAR_SEMANTIC_TYPE_OBJECTIVE, aOut, 1);
        aOut << MDL_AST_LEAF_VAR_SEMANTIC_OPT_EXTREMUM_MIN;
      }
        break;
    }
    
    // New line needed here
    aOut << '\n';
  }//prettyPrintSemantic
  
  void MCPPVariable::prettyPrintDomain(Interpreter::BaseObject* aVar, std::ostream& aOut)
  {
    outTag("domain", aOut);
    
    auto domainType = otools::getVarDomainType(aVar);
    auto numDims = otools::getVarDomainNumDimensions(aVar);
    auto numDoms = otools::getListValues(otools::getVarDomainArrayList(aVar)).composeSize();
    
    // Pretty print the domain of the variable
    std::stringstream ss;
    PrettyPrinter::Tools::toStream(otools::getVarDomain(aVar), ss);
    
    std::string domPP = ss.str();
    assert(domPP.size() > 1);
    
    // Replace brackets for set domains
    if(domainType == DOM_LIST || domainType == DOM_LIST_M)
    {
      domPP[0]     = '{';
      domPP.back() = '}';
    }
    
    if(numDims == 0)
    {
      // No dimensions, the variable is neither a vector nor a matrix
      aOut << domPP;
    }
    else
    {
      // Get the dimensions
      auto pairDim = otools::getVarDomainDimensionsVals(aVar);

      // Print each single composite domain of the matrix
      if(numDims == 1)
      {
        // Print a vector
        if(numDoms == 0)
        {
          // Use the domain of the variable, each sub-domain is the same
          aOut << '[';
          for(std::size_t idx{0}; idx < pairDim.first; ++idx)
          {
            aOut << domPP;
            if(idx < pairDim.first - 1) aOut << ", ";
          }
          aOut << ']';
        }
        else
        {
          PrettyPrinter::Tools::toStream(otools::getVarDomainArray(aVar), aOut);
        }
      }
      else
      {
        // Print a matrix
        assert(numDims == 2);
        if(numDoms == 0)
        {
          // Use the domain of the variable, each sub-domain is the same
          aOut << '\n' << getIndent(1) << '[' << '\n';
          for(std::size_t idx1{0}; idx1 < pairDim.first; ++idx1)
          {
            aOut << getIndent(1) << '[';
            for(std::size_t idx2{0}; idx2 < pairDim.second; ++idx2)
            {
              aOut << domPP;
              if(idx2 < pairDim.second - 1) { aOut << ", "; }
            }
            aOut << ']';
            if(idx1 < pairDim.first - 1) { aOut << ",\n"; }
          }
          aOut << '\n' << getIndent(1) << ']';
        }
        else
        {
          // Use the array of domains
          std::size_t domCtr{0};
          const auto& domainArray = otools::getListValues(otools::getVarDomainArrayList(aVar));
          
          aOut << '\n' << getIndent(1) << '[' << '\n';
          for(std::size_t idx1{0}; idx1 < pairDim.first; ++idx1)
          {
            aOut << getIndent(1) << '[';
            for(std::size_t idx2{0}; idx2 < pairDim.second; ++idx2)
            {
              assert(domCtr < domainArray.composeSize());
              PrettyPrinter::Tools::toStream(domainArray[domCtr++], aOut);
              if(idx2 < pairDim.second - 1) { aOut << ", "; }
            }
            aOut << ']';
            if(idx1 < pairDim.first - 1) { aOut << ",\n"; }
          }
          aOut << '\n' << getIndent(1) << ']';
        }
      }
    }
  }//prettyPrintDomain
  
  void MCPPVariable::prettyPrintDimensions(Interpreter::BaseObject* aVar, std::ostream& aOut)
  {
    assert(aVar && aVar->hasProperty("domainDimensions"));
    outTag("domainDimensions", aOut);
    PrettyPrinter::Tools::toStream(otools::getVarDomainDimensions(aVar), aOut);
  }//prettyPrintDimensions
  
  void MCPPVariable::prettyPrintVarDomain(Interpreter::BaseObject* aVar, std::ostream& aOut)
  {
    // Pretty print the domain of the variable
    prettyPrintDomain(aVar, aOut);
    
    // Pretty pring the dimensions of the variable
    if(otools::getVarDomainDimensionsSize(aVar) > 0)
    {
      aOut << '\n';
      prettyPrintDimensions(aVar, aOut);
    }
  }//prettyPrintVarDomain
  
  void MCPPVariable::prettyPrintSubscript(Interpreter::BaseObject* aVar, std::ostream& aOut)
  {
    outTag("domain", aOut);
    
    // Pretty print the domain of the variable
    auto domainType = otools::getVarDomainType(aVar);
    
    std::stringstream ss;
    PrettyPrinter::Tools::toStream(otools::getVarDomain(aVar), ss);
    
    auto domPP = ss.str();
    assert(domPP.size() > 1);
    
    // Replace brackets for set domains
    if(domainType == DOM_LIST || domainType == DOM_LIST_M)
    {
      domPP[0]     = '{';
      domPP.back() = '}';
    }
    
    aOut << domPP;
  }//prettyPrintSubscript
  
  void MCPPVariable::prettyPrintVarSubscriptDomain(Interpreter::BaseObject* aVar, std::ostream& aOut)
  {
    outTag("subscript", aOut);
    PrettyPrinter::Tools::toStream(otools::getVarDomainSubscriptIndices(aVar), aOut);
  }//prettyPrintVarSubscriptDomain
  
  void MCPPVariable::prettyPrint(const std::shared_ptr<ContextObject>& aObj, std::ostream& aOut,
                                 bool aFullPrettyPrint)
  {
    assert(aObj && (aObj->payload()).isClassObject());
    printIDAndType(aObj, aOut, aFullPrettyPrint);
    
    auto var = (aObj->payload()).getClassObject().get();
    assert(otools::isVariableObject(var));
    
    // New line in inside prettyPrintSemantic since
    // this function takes the parameter "aFullPrettyPrint"
    prettyPrintSemantic(var, aOut, aFullPrettyPrint);
    if (aFullPrettyPrint)
    {
      prettyPrintInputOrder(var, aOut);
      aOut << '\n';
      // prettyPrintFullyInterpreted(var, aOut);
      // aOut << '\n';
    }
    
    if(otools::isVarSubscript(var))
    {
      if (aFullPrettyPrint)
      {
        prettyPrintVarSubscriptDomain(var, aOut);
        aOut << '\n';
      }
      prettyPrintSubscript(var, aOut);
    }
    else
    {
      prettyPrintVarDomain(var, aOut);
    }
  }//prettyPrint
  
}// end namespace Model
