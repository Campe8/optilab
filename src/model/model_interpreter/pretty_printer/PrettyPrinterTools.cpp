// Self first
#include "PrettyPrinterTools.hpp"

#include "ObjectTools.hpp"
#include "ObjectHelper.hpp"

#include "CLIUtils.hpp"
#include "MVCUtils.hpp"

#include <algorithm>  // for std::find

namespace otools = Interpreter::ObjectTools;

namespace PrettyPrinter { namespace Tools {
  
  std::ostream& toStreamImpl(Interpreter::BaseObject* aObj, std::ostream& aOut, int aIndent,
                             const std::vector<std::string>& aBlackList={}, bool aForceNL=false,
                             bool aForceUseOfIdentifier=false);
  
  std::ostream& toStreamImpl(const Interpreter::DataObject& aObj, std::ostream& aOut, int aIndent,
                             const std::vector<std::string>& aBlackList={}, bool aForceNL=false,
                             bool aForceUseOfIdentifier=false);
  
  // Returns the maximum length allowed in one line
  static std::size_t maxLineLength()
  {
    static std::string longString = "[1000, 1000, ..., 1000, 1000]";
    static std::size_t length = longString.size();
    return length;
  }//maxLineLength
  
  static const std::string& getWhiteSpace()
  {
    static std::string whiteSpaces{" \t"};
    return whiteSpaces;
  }//getWhiteSpace
  
  static void ltrim(std::string& aStr)
  {
    auto idx = aStr.find_first_not_of(getWhiteSpace());
    if (idx != std::string::npos)
    {
      aStr = aStr.substr(idx);
    }
  }//ltrim
  
  static std::string ltrimmed(const std::string& aStr)
  {
    auto idx = aStr.find_first_not_of(getWhiteSpace());
    if (idx != std::string::npos)
    {
      return aStr.substr(idx);
    }
    return aStr;
  }//ltrimmed
  
  static void rtrim(std::string& aStr)
  {
    auto idx = aStr.find_last_not_of(getWhiteSpace());
    if (idx != std::string::npos)
    {
      aStr.erase(idx + 1);
    }
  }//rtrim
  
  static std::string rtrimmed(const std::string& aStr)
  {
    auto idx = aStr.find_last_not_of(getWhiteSpace());
    if (idx != std::string::npos)
    {
      auto strCopy{aStr};
      strCopy.erase(idx + 1);
      return strCopy;
    }
    return aStr;
  }//rtrimmed
  
  static void trim(std::string& aStr)
  {
    ltrim(aStr);
    rtrim(aStr);
  }//trim
  
  static std::string trimmed(const std::string& aStr)
  {
    return rtrimmed(ltrimmed(aStr));
  }//trimmed
  
  // Utility function: returns the string with "aIndent" indetations
  static std::string getIndent(int aIndent)
  {
    if (aIndent <= 0) return "";
    return std::string(aIndent * 2, ' ');
  }//getIndent
  
  static std::string getIndentLn(int aIndent)
  {
    return std::string("\n") + getIndent(aIndent);
  }//getIndentLn
  
  // Utility function: prints "aIndent" double spaces followed by "aTag" into "aOut"
  static void setIndent(const std::string& aTag, std::ostream& aOut, int aIndent)
  {
    static std::string tagSep{": "};
    aOut << getIndent(aIndent);
    aOut << aTag << tagSep;
  }//setIndent
  
  static void setIndentLn(const std::string& aTag, std::ostream& aOut, int aIndent)
  {
    setIndent(aTag, aOut, aIndent);
    aOut << '\n';
  }//setIndentLn
  
  // Returns the minimum indentation level of the given string
  static int findMinIndentation(const std::string& aStr)
  {
    static const int indentSize = static_cast<int>(getIndent(1).size());
    
    int minIndent = static_cast<int>(aStr.size()) + 1;
    
    // Find the minimun number of white spaces following a new line and followed by a char
    std::size_t pos = aStr.find("\n");
    while (pos != std::string::npos)
    {
      pos++;
      if (pos >= aStr.size()) break;
      
      int ctr{0};
      while (pos < aStr.size() && aStr[pos] == ' ')
      {
        pos++;
        ctr++;
      }
      
      // Update minimum
      if (ctr < minIndent)
      {
        minIndent = ctr;
      }
      pos = aStr.find("\n", pos);
    }
    // Return 0 if there is no new line
    if (minIndent == static_cast<int>(aStr.size()) + 1) return 0;
    
    // 
    return minIndent / indentSize;
  }//findMinIndentation
  
  static void findAndReplace(std::string& aData, const std::string& aToSearch,
                             const std::string& aReplace)
  {
    // Get the first occurrence
    std::size_t pos = aData.find(aToSearch);
    
    // Repeat till end is reached
    while (pos != std::string::npos)
    {
      // Replace this occurrence of Sub String
      aData.replace(pos, aToSearch.size(), aReplace);
      
      // Get the next occurrence from the current position
      pos = aData.find(aToSearch, pos + aToSearch.size());
    }
  }//findAndReplace
  
  // Returns the index to the last non-white (i.e., ' ' or '\t') char in "aStr"
  static std::size_t findLastNonWhiteChar(const std::string& aStr)
  {
    assert(!aStr.empty());
    
    std::size_t backIdx{aStr.size()-1};
    while (backIdx > 0 && (aStr[backIdx] == ' ' || aStr[backIdx] == '\t'))
    {
      backIdx--;
    }
    return backIdx;
  }//findLastNonWhiteChar
  
  static std::ostream& primitiveObjectTypeToStream(const Interpreter::DataObject& aObj,
                                                   std::ostream& aOut, int aIndent=0)
  {
    (void) aIndent;
    
    auto objDT = aObj.dataObjectType();
    switch (objDT)
    {
      case Interpreter::DataObject::DataObjectType::DOT_INT:
        aOut << aObj.getDataValue<int>();
        break;
      case Interpreter::DataObject::DataObjectType::DOT_BOOL:
        aOut << std::boolalpha << aObj.getDataValue<bool>();
        break;
      case Interpreter::DataObject::DataObjectType::DOT_DOUBLE:
        aOut << aObj.getDataValue<double>();
        break;
      case Interpreter::DataObject::DataObjectType::DOT_SIZE_T:
        aOut << aObj.getDataValue<std::size_t>();
        break;
      case Interpreter::DataObject::DataObjectType::DOT_STRING:
        aOut << aObj.getDataValue<std::string>();
        break;
      default:
        assert(objDT == Interpreter::DataObject::DataObjectType::DOT_VOID);
        break;
    }
    return aOut;
  }//primitiveObjectTypeToStream
  
  // Utility function: pretty print into a string the composite object range [aIdx1, aIdx2]
  static std::string compositeListObjectToString(const Interpreter::DataObject& aList,
                                                 std::size_t aIdx1, std::size_t aIdx2,
                                                 int aIndent,
                                                 const std::vector<std::string>& aBlackList={},
                                                 bool aForceNL=false)
  {
    // String to return
    std::string outString;
    std::size_t lenCtr{0};
    for (; aIdx1 < aIdx2; ++aIdx1)
    {
      // Temporary stream to collect printings
      std::stringstream ss;
      toStreamImpl(aList[aIdx1], ss, aIndent, aBlackList, aForceNL);
      
      // Set a comma between all objects in the list
      std::string oss = ss.str();
      if (aForceNL && oss.back() == ']')
      {
        oss += "," + getIndentLn(aIndent);
      }
      else
      {
        oss += ", ";
      }
      
      // Get the size of the current line and, if greater than max allowed line length,
      // add a newline char and reset the counter.
      // @note this must be done only if the last non space/tab char is not a new line
      lenCtr += trimmed(oss).size();
      if (lenCtr >= maxLineLength())
      {
        lenCtr = 0;
        if (oss[findLastNonWhiteChar(oss)] != '\n')
        {
          oss += getIndentLn(aIndent);
        }
      }
      
      // Concatenate the lines found so far.
      // If last non white space of outString is not a new line, trim oss
      if (!outString.empty() && outString[findLastNonWhiteChar(outString)] != '\n')
      {
        ltrim(oss);
      }
      else
      {
        // Instead, if las non white space of outString is a new line, trim outString on the right
        // since all spaces have been already pushed into oss
        while (!outString.empty() && outString.back() == ' ')
        {
          outString.pop_back();
        }
      }
      outString += oss;
    }
    
    return outString;
  }//compositeListObjectToString
  
  static std::ostream& compositeObjectTypeToStream(const Interpreter::DataObject& aObj,
                                                   std::ostream& aOut, int aIndent=0,
                                                   const std::vector<std::string>& aBlackList={},
                                                   bool aForceNL=false)
  {
    const auto listSize = aObj.composeSize();
    if (listSize == 0)
    {
      aOut << getIndent(aIndent) << "[]";
      return aOut;
    }
    
    // Start printing the list on the ouput string
    std::string outString;
    if (listSize <= 10)
    {
      outString += compositeListObjectToString(aObj, 0, listSize, aIndent, aBlackList, aForceNL);
    }
    else
    {
      // Don't clutter the output, if the list size is greater than 10 elements,
      // print the first three elements, print "...",  and print the last three elements.
      // For example:
      // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
      // becomes
      // [1, 2, 3, ..., 9, 10, 11]
      outString += compositeListObjectToString(aObj, 0, 3, aIndent, aBlackList, aForceNL);
      
      // Print "..." on its own line if it starts on a new line.
      // To check if it starts on a new line, go back and find the first non ' ' or '\t' char.
      // If it is not a new line char, it means that "..." is not printed on a new line
      if (outString[findLastNonWhiteChar(outString)] == '\n')
      {
        outString +=
        "...," +
        getIndentLn(aIndent) +
        compositeListObjectToString(aObj, listSize - 3, listSize, aIndent, aBlackList, aForceNL);
        
      }
      else
      {
        // Add the right side on the same line only if, after trimming, the left side plust
        // the dots, plust the right side is still less than the max allowed line length
        auto rightHand = trimmed(compositeListObjectToString(aObj, listSize - 3, listSize, aIndent,
                                                             aBlackList, aForceNL));
        if (outString.size() + rightHand.size() + 5 < maxLineLength())
        {
          outString += "..., " + rightHand;
        }
        else
        {
          outString +=
          getIndentLn(aIndent) +
          "...," +
          getIndentLn(aIndent) +
          rightHand;
        }
      }
    }
    assert(outString.size() > 1);
    
    // Avoid first new line char
    if (outString[0] == '\n') outString.erase(0, 1);
    
    // End of the list, don't print "\n]" or ", ]"
    std::size_t backIdx{outString.size()-1};
    while (backIdx > 0 &&
           (outString[backIdx] == ' '  ||
            outString[backIdx] == '\n' ||
            outString[backIdx] == '\t' ||
            outString[backIdx] == ','))
    {
      backIdx--;
    }
    outString.erase(backIdx+1);
    trim(outString);
    
    aOut << getIndent(aIndent) << '[' << outString << ']';
    return aOut;
  }//compositeObjectTypeToStream
  
  static std::ostream& listObjToStream(Interpreter::BaseObject* aObj, std::ostream& aOut,
                                       int aIndent=0, const std::vector<std::string>& aBlackList={},
                                       bool aForceNL=false)
  {
    assert(otools::isListObject(aObj));
    
    // Get the values from the object and print the composite object
    return compositeObjectTypeToStream(otools::getListValues(aObj), aOut, aIndent, aBlackList,
                                       aForceNL);
  }//listObjToStream
  
  static std::ostream& matrixObjToStream(Interpreter::BaseObject* aObj, std::ostream& aOut,
                                         int aIndent=0, const std::vector<std::string>& aBlackList={},
                                         bool aForceNL=false)
  {
    assert(otools::isMatrixObject(aObj));

    const auto& listOfLists = otools::getParameterValues(aObj);
    assert(Interpreter::isClassObjectType(listOfLists) &&
           otools::isListObject(listOfLists.getClassObject().get()));
    
    return listObjToStream(listOfLists.getClassObject().get(), aOut, aIndent, aBlackList, true);
  }//matrixObjToStream
  
  std::ostream& toStreamImpl(Interpreter::BaseObject* aObj, std::ostream& aOut, int aIndent,
                             const std::vector<std::string>& aBlackList, bool aForceNL,
                             bool aForceUseOfIdentifier)
  {
    if (!aObj) return aOut;
    
    if (otools::isScalarObject(aObj))
    {
      // For scalar values, print their content as a single DataObject
      return toStreamImpl(otools::getScalarValue(aObj), aOut, aIndent, {}, aForceNL);
    }
    else if (otools::isListObject(aObj))
    {
      return listObjToStream(aObj, aOut, aIndent, aBlackList, aForceNL);
    }
    else if (aForceUseOfIdentifier && !MVC::Utils::getIDFromDataObject(aObj).empty())
    {
      aOut << getIndent(aIndent) << MVC::Utils::getIDFromDataObject(aObj);
      return aOut;
    }
    else if (otools::isVarSubscript(aObj))
    {
      // Print id[indices]
      aOut << otools::getVarID(aObj);
      return toStreamImpl(otools::getVarDomainSubscriptIndicesList(aObj), aOut, aIndent, {},
                          aForceNL);
    }
    else if (otools::isMatrixObject(aObj))
    {
      return matrixObjToStream(aObj, aOut, aIndent, aBlackList, aForceNL);
    }
    else if (otools::isVariableObject(aObj))
    {
      // Print id of the variable
      aOut << otools::getVarID(aObj);
      return aOut;
    }
    
    // Else print the pairs <key, value>
    for (const auto& key : aObj->getVisibleKeys())
    {
      if (std::find(aBlackList.begin(), aBlackList.end(), key) != aBlackList.end()) continue;
      
      // Check for subobjects, any subobject should go on a new line
      bool useId = true;
      
      // Print the value on a new stream
      std::stringstream ss;
      toStreamImpl(aObj->getProperty(key), ss, aIndent, aBlackList, aForceNL, useId);
      
      // Check if the string in the stream spans on multiple lines
      auto propertyPrint = ss.str();
      if (spansMultipleLines(propertyPrint))
      {
        // If so, print the whole property on a new line with indentation
        setIndentLn(key, aOut, aIndent);
        
        // Indent + 1 w.r.t. the left side, i.e., w.r.t. not what is have already been indented
        auto indent = findMinIndentation(propertyPrint);
        indentString(propertyPrint, (aIndent + 1) - indent);
      }
      else
      {
        // Otherwise print the property on the same line
        setIndent(key, aOut, aIndent);
        
        // Trim the string on the left since there is no need for indentation on the same line
        ltrim(propertyPrint);
      }
      aOut << propertyPrint << '\n';
    }
    return aOut;
  }//toStreamImpl
  
  std::ostream& toStreamImpl(const Interpreter::DataObject& aObj, std::ostream& aOut, int aIndent,
                             const std::vector<std::string>& aBlackList, bool aForceNL,
                             bool aForceUseOfIdentifier)
  {
    if (aObj.isClassObject())
    {
      return toStreamImpl(aObj.getClassObject().get(), aOut, aIndent, aBlackList, aForceNL,
                          aForceUseOfIdentifier);
    }
    else if(aObj.isFcnObject())
    {
      // For functions print function name and () operator
      aOut << aObj.getFcnPtr()->getFcnName() << "()";
      return aOut;
    }
    else if (!aObj.isFullyInterpreted() && !aObj.isComposite())
    {
      // If the object is not fully interpreted print its string representation
      aOut << aObj.getDataValue<std::string>();
      return aOut;
    }
    else if (aObj.isComposite())
    {
      return compositeObjectTypeToStream(aObj, aOut, aIndent);
    }
    
    // All other cases must be primitive data objects
    assert(Interpreter::isPrimitiveType(aObj) || isVoidObjectType(aObj));
    return primitiveObjectTypeToStream(aObj, aOut, aIndent);
  }//toStreamImpl
  
  std::ostream& toStream(Interpreter::BaseObject* aObj, std::ostream& aOut, int aIndent,
                         const std::vector<std::string>& aBlackList)
  {
    return toStreamImpl(aObj, aOut, aIndent, aBlackList, false);
  }//toStream
  
  std::ostream& toStream(const Interpreter::DataObject& aObj, std::ostream& aOut, int aIndent,
                         const std::vector<std::string>& aBlackList)
  {
    return toStreamImpl(aObj, aOut, aIndent, aBlackList, false);
  }//toStream
  
  std::ostream& typeToStream(const Interpreter::DataObject& aObj, std::ostream& aOut)
  {
    if(aObj.isClassObject())
    {
      auto baseObj = aObj.getClassObject().get();
      if (otools::isScalarObject(baseObj))
      {
        return typeToStream(otools::getScalarValue(baseObj), aOut);
      }
      assert(baseObj->hasProperty(otools::getTypePropName()));
      aOut << Interpreter::ObjectTools::getObjectPropertyValue<std::string>(baseObj,
                                                                         otools::getTypePropName());
      return aOut;
    }
    
    // All other cases must be primitive data objects
    assert(Interpreter::isPrimitiveType(aObj) || isVoidObjectType(aObj));
    
    auto objDT = aObj.dataObjectType();
    switch (objDT) {
      case Interpreter::DataObject::DataObjectType::DOT_INT:
        aOut << "int";
        break;
      case Interpreter::DataObject::DataObjectType::DOT_BOOL:
        aOut << "bool";
        break;
      case Interpreter::DataObject::DataObjectType::DOT_DOUBLE:
        aOut << "double";
        break;
      case Interpreter::DataObject::DataObjectType::DOT_SIZE_T:
        aOut << "uint";
        break;
      case Interpreter::DataObject::DataObjectType::DOT_STRING:
        aOut << "string";
        break;
      default:
        assert(objDT == Interpreter::DataObject::DataObjectType::DOT_VOID);
        aOut << "void";
        break;
    }
    return aOut;
  }
  
  bool spansMultipleLines(const std::string& aString)
  {
    return aString.find_first_of('\n') != std::string::npos;
  }//spansMultipleLines
  
  void indentString(std::string& aString, int aIndent)
  {
    static const std::string toFind{"\n"};
    
    if (aIndent <= 0) return;
    std::string toReplace = "\n" + getIndent(aIndent);
    findAndReplace(aString, toFind, toReplace);
    aString = getIndent(aIndent) + aString;
  }//indentString
  
}} // end namespace Tools/PrettyPrinter
