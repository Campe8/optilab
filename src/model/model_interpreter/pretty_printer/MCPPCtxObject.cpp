// Self first
#include "MCPPCtxObject.hpp"

#include "ModelASTMacro.hpp"
#include "CLIUtils.hpp"
#include "ObjectHelper.hpp"
#include "ObjectTools.hpp"
#include "PrettyPrinterTools.hpp"

#include <cassert>
#include <sstream>
#include <string>

namespace cutils = CLI::Utils;
namespace otools = Interpreter::ObjectTools;

namespace Model {

	void MCPPCtxObject::prettyPrintDimensions(Interpreter::BaseObject* aPar, std::ostream& aOut,
                                            int aLevel)
	{
    
    outTag("dims", aOut, aLevel);
    PrettyPrinter::Tools::toStream(otools::getParameterDimensionsList(aPar), aOut);
	}//prettyPrintNCols
  
	void MCPPCtxObject::prettyPrint(const std::shared_ptr<ContextObject>& aObj, std::ostream& aOut,
                                  bool aFullPrettyPrint)
	{
    assert(aObj && (aObj->payload()).isClassObject());
    auto obj = (aObj->payload()).getClassObject().get();
    
    // Scalar, list, and matrix objects require a particular handling
    if (otools::isParameterObject(obj))
    {
      // Print the identifier of the parameter followed by its pretty print.
      printID(aObj, aOut);
      
      // Pretty print the parameter on a new string stream
      std::stringstream ss;
      PrettyPrinter::Tools::toStream(aObj->payload(), ss);
      
      // Check if the string stream contains any new line.
      // If so, move the pretty print into a new line and indent everything by 1
      auto prettyPrint = ss.str();
      if (PrettyPrinter::Tools::spansMultipleLines(prettyPrint))
      {
        aOut << '\n';
        PrettyPrinter::Tools::indentString(prettyPrint, 1);
      }
      aOut << prettyPrint;
      aOut << '\n';
      
      if (aFullPrettyPrint)
      {
        printType(aObj, aOut, -1);
        aOut << '\n';
        
        if (otools::isMatrixParameterObject(obj))
        {
          prettyPrintDimensions(obj, aOut, -1);
          aOut << '\n';
        }
      }
    }
    else
    {
      printIDAndType(aObj, aOut, aFullPrettyPrint);
      
      if (aFullPrettyPrint)
      {
        prettyPrintFullyInterpreted(obj, aOut);
        aOut << '\n';
      }
    }
	}//prettyPrint

}// end namespace Model

