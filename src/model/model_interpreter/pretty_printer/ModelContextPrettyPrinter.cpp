// Self first
#include "ModelContextPrettyPrinter.hpp"

#include "MVCUtils.hpp"
#include "InterpUtils.hpp"
#include "ObjectTools.hpp"
#include "ObjectHelper.hpp"
#include "PrettyPrinterTools.hpp"

#include <algorithm> // for std::find
#include <typeinfo>  // for typeid
#include <cstddef>   // for std::size_t
#include <sstream>

namespace otools = Interpreter::ObjectTools;

namespace Model {
  
  std::string ModelContextPrettyPrinter::getIndent(int aLevel)
  {
    std::string indent;
    for (int lvl = 0; lvl <= aLevel; ++lvl)
    {
      indent += "  ";
    }
    return indent;
  }//getIndent
  
  std::string ModelContextPrettyPrinter::getIndentLn(int aLevel)
  {
    return "\n" + getIndent(aLevel);
  }//getIndentLn
  
	void ModelContextPrettyPrinter::outTag(const std::string& aTag, std::ostream& aOut, int aLevel)
	{
    aOut << getIndent(aLevel);
		aOut << aTag << ": ";
	}//outTag

	void ModelContextPrettyPrinter::outTagLn(const std::string& aTag, std::ostream& aOut, int aLevel)
	{
		outTag(aTag, aOut, aLevel);
		aOut << '\n';
	}//outTag
  
  void ModelContextPrettyPrinter::printIDAndType(const std::shared_ptr<ContextObject>& aObj,
                                                 std::ostream& aOut, bool aFullPrettyPrint)
  {
    if(!aObj) return;
    
    // Print context object key
    printID(aObj, aOut);
    aOut << '\n';
    
    // Print type
    if(aFullPrettyPrint)
    {
      printType(aObj, aOut);
      aOut << '\n';
    }
  }//printIDAndType
  
  void ModelContextPrettyPrinter::printID(const std::shared_ptr<ContextObject>& aObj,
                                          std::ostream& aOut)
  {
    if(!aObj) return;
    
    // Print context object key
    outTag(aObj->getID(), aOut, -1);
  }//printID
  
  void ModelContextPrettyPrinter::printType(const std::shared_ptr<ContextObject>& aObj,
                                            std::ostream& aOut, int aLevel)
  {
    if(!aObj) return;
    
    outTag("type", aOut, aLevel);
    PrettyPrinter::Tools::typeToStream(aObj->payload(), aOut);
  }//printType
  
  void ModelContextPrettyPrinter::prettyPrintFullyInterpreted(Interpreter::BaseObject* aObj, std::ostream& aOut)
  {
    if(!aObj || !aObj->hasProperty(Interpreter::ObjectTools::getObjectInterpretedPropName())) return;
    outTag(Interpreter::ObjectTools::getObjectInterpretedPropName(), aOut);
    aOut << std::boolalpha << otools::getObjectPropertyValue<bool>(aObj, otools::getObjectInterpretedPropName());
  }//prettyPrintFullyInterpreted
  
  void ModelContextPrettyPrinter::prettyPrintFullyInterpreted(bool aFullyInterpreted, std::ostream& aOut)
  {
    outTag(otools::getObjectInterpretedPropName(), aOut);
    aOut << std::boolalpha << aFullyInterpreted;
  }//prettyPrintFullyInterpreted
  
}// end namespace Model
