// Self first
#include "MCPPConstraint.hpp"

#include "ModelASTMacro.hpp"
#include "DataObjectFcn.hpp"
#include "ConstraintContextObject.hpp"
#include "ObjectHelper.hpp"
#include "PrettyPrinterTools.hpp"

#include <string>
#include <sstream>
#include <utility>

#include <cassert>

namespace Model {
  
  void MCPPConstraint::prettyPrintType(const ContextObject* aObj, std::ostream& aOut)
  {
    auto conSpec = static_cast<const ConstraintContextObject*>(aObj);
    outTag(MDL_AST_LEAF_PROPAGATION_TYPE, aOut);
    
    auto propType = conSpec->getPropagationType();
    if(propType == ConstraintContextObject::PropagationType::PT_BOUNDS)
    {
      aOut << MDL_AST_LEAF_PROPAGATION_TYPE_BOUND;
    }
    else
    {
      assert(propType == ConstraintContextObject::PropagationType::PT_DOMAIN);
      aOut << MDL_AST_LEAF_PROPAGATION_TYPE_DOMAIN;
    }
  }//prettyPrintType
  
  void MCPPConstraint::prettyPrintScope(const Interpreter::DataObject& aObj, std::ostream& aOut)
  {
    // Get scope
    outTag(MDL_AST_LEAF_CON_SCOPE, aOut);
    
    Interpreter::DataObject scope;
    scope.compose(aObj.getCompose());
    PrettyPrinter::Tools::toStream(scope, aOut);
  }//prettyPrintScope
  
	void MCPPConstraint::prettyPrint(const std::shared_ptr<ContextObject>& aObj, std::ostream& aOut,
                                   bool aFullPrettyPrint)
	{
    assert(aObj);
    printIDAndType(aObj, aOut, aFullPrettyPrint);
    
    // const auto& payload = aObj->payload();
    assert(Interpreter::isClassObjectType(aObj->payload()));
    
    // Print constraint parameters
    auto conObj = (aObj->payload()).getClassObject().get();
    PrettyPrinter::Tools::toStream(conObj, aOut, 1, {"fullyInterpreted"});

    // Print the scope of the constraint
    const auto& postFcn = Interpreter::ObjectTools::getConstraintPostFcn(conObj);
    prettyPrintScope(postFcn, aOut);
    
		if (aFullPrettyPrint)
		{
      aOut << '\n';
			prettyPrintType(aObj.get(), aOut);
      
      //aOut << '\n';
      //prettyPrintFullyInterpreted(payload.isFullyInterpreted(), aOut);
		}
	}//prettyPrint
  
}// end namespace Model

