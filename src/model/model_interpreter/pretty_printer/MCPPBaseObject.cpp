// Self first
#include "MCPPBaseObject.hpp"

#include "PrettyPrinterTools.hpp"

#include <cassert>

namespace Model {
  
	void MCPPBaseObject::prettyPrint(const std::shared_ptr<ContextObject>& aObj, std::ostream& aOut,
                                   bool aFullPrettyPrint)
	{
    assert(aObj && (aObj->payload()).isClassObject());
    
    // Print context object identifier
    printID(aObj, aOut);
    aOut << '\n';
    
    // Get the object and print its properties
    auto obj = (aObj->payload()).getClassObject().get();
    PrettyPrinter::Tools::toStream(obj, aOut, 1, {"fullyInterpreted"});
	}//prettyPrint

}// end namespace Model
