// Self first
#include "ContextView.hpp"

#include <algorithm>
#include <cassert>

namespace Model {
  
  ContextView::ContextView()
  : pDirtyCache(true)
  , pViewCache()
  {
  }
  
  void ContextView::mergeView(const std::shared_ptr<ContextView>& aOther)
  {
    if(!aOther) return;
    
    // Merge alias set
    auto preSize = pAliasSet.size();
    for(auto& alias : aOther->pAliasSet)
    {
      if(!findAlias(alias))
      {
        pAliasSet.push_back(alias);
      }
    }
    if(preSize < pAliasSet.size()) pDirtyCache = true;
    
    preSize = pViewMap.size();
    for(auto& it : aOther->pViewMap)
    {
      if(pViewMap.find(it.first) == pViewMap.end())
      {
        pViewMap[it.first] = it.second;
      }
    }
    if(preSize < pViewMap.size()) pDirtyCache = true;
  }//mergeView
  
  void ContextView::addAlias(const std::string& aAlias)
  {
    if(!aAlias.empty() && !findAlias(aAlias))
    {
      pAliasSet.push_back(aAlias);
    }
  }//addAlias
  
  bool ContextView::findAlias(const std::string& aAlias) const
  {
    if(aAlias.empty()) return false;
    return std::find(pAliasSet.begin(), pAliasSet.end(), aAlias) != pAliasSet.end();
  }//findAlias
  
  void ContextView::addEntry(const ViewObjSPtr& aEntry)
  {
    assert(aEntry);
    auto id = aEntry->getID();
    assert(pViewMap.find(id) == pViewMap.end());
    
    pViewMap[id] = aEntry;
    pDirtyCache = true;
  }//addEntry
  
  void ContextView::removeEntry(std::string& aID)
  {
    // Remove entry from alias set
    for(auto& a : pAliasSet)
    {
      if (a == aID)
      {
        a.clear();
        break;
      }
    }
    
    // Remove entry from map
    auto it = pViewMap.find(aID);
    if (it == pViewMap.end()) { return; }
    pViewMap.erase(it);
  }//removeEntry

  ContextView::ViewObj* ContextView::lookup(std::string& aID) const
  {
    if (pViewMap.find(aID) == pViewMap.end()) { return nullptr; }
    return pViewMap.at(aID).get();
  }//lookup
  
  ContextView::ViewObjSPtr ContextView::getEntry(std::string& aID) const
  {
    if (pViewMap.find(aID) == pViewMap.end()) { return nullptr; }
    return pViewMap.at(aID);
  }//getEntry
  
  std::string ContextView::getSerializedView() const
  {
    if (!pDirtyCache)
    {
      return pViewCache;
    }
    
    pViewCache = "";
    for (auto& mapEntry : pViewMap)
    {
      pViewCache += mapEntry.second->serialize();
    }
    
    pDirtyCache = false;
    return pViewCache;
  }//getSerializedView
  
}//end namespace Model

