// Self first
#include "GeneticInformation.hpp"

#include "SearchOptionsTools.hpp"
#include "ParserUtils.hpp"

namespace otools  = Interpreter::ObjectTools;
namespace sotools = Interpreter::SearchOptionsTools;
namespace putils  = Model::Utils::Parser;

namespace Model {
  
  const std::string GeneticInformation::StrategyName = "genetic";
  
  const std::string GeneticInformation::PopulationSizeKey = "populationSize";
  
  const std::string GeneticInformation::NumGenerationsKey = "numGenerations";
  
  const std::string GeneticInformation::MutationChanceKey = "mutationChance";
  
  const std::string GeneticInformation::MutationSizeKey = "mutationSize";
  
  GeneticInformation::GeneticInformation()
  : CBLSInformation(SearchInformationType::SI_GENETIC)
  {
  }
  
  void GeneticInformation::readStrategySearchOptionsObject(Interpreter::BaseObject* aObj)
  {
    // Check object and its type
    assert(aObj && sotools::getClassType(aObj) ==
           static_cast<int>(SearchInformation::SearchInformationType::SI_GENETIC));
    
    // Add Genetic properties
    
    // 1) Population size
    assert(aObj->hasProperty(PopulationSizeKey));
    addMember(PopulationSizeKey,
              putils::getDataObjectFromPrimitiveOrScalarType(aObj->
                                      getProperty(PopulationSizeKey)).castDataValue<std::size_t>());
    
    // 2) Number of generations
    assert(aObj->hasProperty(NumGenerationsKey));
    addMember(NumGenerationsKey,
              putils::getDataObjectFromPrimitiveOrScalarType(aObj->
                                      getProperty(NumGenerationsKey)).castDataValue<std::size_t>());
              
    // 3) Mutation chance
    assert(aObj->hasProperty(MutationChanceKey));
    addMember(MutationChanceKey,
              putils::getDataObjectFromPrimitiveOrScalarType(aObj->
                                      getProperty(MutationChanceKey)).castDataValue<std::size_t>());
    
    // 4) Mutation size
    assert(aObj->hasProperty(MutationSizeKey));
    addMember(MutationSizeKey,
              putils::getDataObjectFromPrimitiveOrScalarType(aObj->
                                        getProperty(MutationSizeKey)).castDataValue<std::size_t>());
  }//readSearchOptionsObject
  
}//end namespace Model
