// Self first
#include "ConstraintContextObject.hpp"

namespace Model {
  
  ConstraintContextObject::ConstraintContextObject(const std::string& aID, const Interpreter::DataObject& aDO)
  : ContextObject(aID, aDO)
  , pPropType(PropagationType::PT_UNDEF)
  {
    // Set type
    setType(Type::CTX_OBJ_CON);
  }
  
}//end namespace Model

