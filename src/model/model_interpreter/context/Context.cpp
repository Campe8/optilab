// Base class
#include "Context.hpp"

#include <sstream>
#include <utility>
#include <cassert>

namespace Model {
  
  Context::Context()
		: Context("")
  {
  }
  
  Context::Context(const std::string& aContextId)
		: pCtxId(aContextId)
  {
    if (!aContextId.empty())
    {
      PTContext ctx;
      pCtx.add_child(pCtxId, ctx);
    }
  }
  
  Context::Context(const Context& aOther)
  {
    pCtxId = aOther.pCtxId;
    pCtx = aOther.pCtx;
  }
  
  Context::Context(Context&& aOther)
  {
    pCtxId = aOther.pCtxId;
    pCtx = aOther.pCtx;
    
    aOther.pCtxId = "";
    aOther.pCtx = getEmptyContext();
  }
  
  Context& Context::operator=(const Context& aOther)
  {
    if (&aOther == this)
    {
      return *this;
    }
    
    pCtxId = aOther.pCtxId;
    pCtx = aOther.pCtx;
    
    return *this;
  }
  
  Context& Context::operator=(Context&& aOther)
  {
    if (&aOther == this)
    {
      return *this;
    }
    
    pCtxId = aOther.pCtxId;
    pCtx = aOther.pCtx;
    
    aOther.pCtxId = "";
    aOther.pCtx = getEmptyContext();
    
    return *this;
  }
  
  bool Context::isEmpty() const
  {
    bool emptyChild = false;
    
    std::size_t ctr{0};
    for(auto& sb : pCtx)
    {
      if(ctr > 0)
      {
        // More than 1 child, non empty context
        return false;
      }
      
      // @note "empty" on property tree checks whether there
      // are any direct children.
      // We also need to check if sb.second has empty data
      emptyChild = sb.second.empty();
      if (emptyChild)
      {
        emptyChild = sb.second.data().empty();
      }
      
      ctr++;
    }
    return pCtx.empty() || emptyChild;
  }//isEmpty
  
  Context::PTContext Context::getEmptyContext() const
  {
    std::stringstream ss;
    
    PTContext emptyCtx;
    pt::write_json(ss, emptyCtx);
    
    return emptyCtx;
  }
  
  void Context::addSubContext(const Path& aPath, const Context& aCtx)
  {
    assert(!aPath.empty());
    auto p = getExtendedPath(aPath);
    
    assert(!p.empty());
    pCtx.put_child(p, aCtx.pCtx);
  }//addSubContext
  
  void Context::expandSubContext(const Path& aPath, const Context& aCtx)
  {
    assert(!aPath.empty());
    auto p = getExtendedPath(aPath);
    
    pCtx.get_child(p).push_back(std::make_pair("", aCtx.pCtx));
  }//expandSubContext
  
  Context Context::getSubContext(const Path& aPath) const
  {
    assert(!aPath.empty());
    
    auto leaf = aPath.back();
    auto path = getExtendedPath(aPath);
    auto subCtx = pCtx.get_child_optional(path);
    if (!subCtx)
    {
      // Return empty context if the child is not present
      Context emptyCtx;
      emptyCtx.pCtx = getEmptyContext();
      return emptyCtx;
    }
    
    // Create context to return with leaf id
    Context leafCtx(leaf);
    
    // For each sub-context, add it to leaf context
    PTContext ctx = *subCtx;
    for (auto& c : ctx)
    {
      Context mdlCtxContent;
      mdlCtxContent.pCtx = c.second;
      
      if (!c.first.empty())
      {
        leafCtx.addSubContext({ c.first }, mdlCtxContent);
      }
      else
      {
        // Sub-context does not have an address, expand current leaf context
        // with the sub-context: [ sub-context, ... ]
        leafCtx.expandSubContext({ c.first }, mdlCtxContent);
      }
    }
    
    return leafCtx;
  }//getSubContext
  
  Context Context::createUnnamedContext(const pt::ptree::value_type& aValType) const
  {
    Context mc;
    mc.pCtx = aValType.second;
    
    // Set context id after setting Context
    // to avoid creating-adding a child with name "c.first",
    // otherwise in lists like
    // [
    //   {
    //    "c1": "v1"
    //   }, ... ]
    // the returned context would be
    // {
    //   c1 : {
    //     "c1": "v1"
    //   }
    // }
    mc.pCtxId = aValType.first;
    
    return mc;
  }//createUnnamedContext
  
  Context Context::createNamedContext(const pt::ptree::value_type& aValType) const
  {
    Context mc;
    
    mc.pCtx = aValType.second;
    mc.pCtxId = aValType.first;
    
    // Add mc unnamed context as sub-context to mc2
    Context mc2;
    mc2.addSubContext({aValType.first}, mc);
    
    return mc2;
  }//createNamedContext
  
  std::vector<Context> Context::getSubContextList(const Path& aPath) const
  {
    // Copy path until immediate parent of the leaf node
    std::string p{""};
    if(aPath.size() == 1 && aPath[0] == pCtxId)
    {
      p = getExtendedPath({""});
    }
    else
    {
      p = getExtendedPath(aPath);
    }
      
    std::vector<Context> v;
    for(auto& c : pCtx.get_child(p))
    {
      if(c.first.empty())
      {
        auto ct = createUnnamedContext(c);
        v.push_back(ct);
      }
      else
      {
        auto ct = createNamedContext(c);
        v.push_back(ct);
      }
    }
    
    return v;
  }//getSubContextList
  
  void Context::deleteSubContext(const Path& aPath)
  {
    if(aPath.empty())
    {
      return;
    }
    
    // Get the leaf path
    auto leaf = aPath.back();
    
    // Get the path without leaf, i.e.,
    // until its parent, start deleting from there
    Path sp = aPath;
    sp.pop_back();
    
    std::string p{""};
    if(sp.size() == 1 && sp[0] == pCtxId)
    {
      p = getExtendedPath({""});
    }
    else
    {
      p = getExtendedPath(sp);
    }

    pCtx.get_child(p).erase(leaf);
  }//deleteSubContext
  
  void Context::serializeContext(std::ostream& aOut) const
  {
    pt::write_json(aOut, pCtx);
  }//serializeContext
  
  std::string Context::getExtendedPath(const Path& aPath) const
  {
    if (aPath.empty())
    {
      return pCtxId;
    }
    
    std::string path{ "" };
    for (auto& p : aPath)
    {
      path += p + '.';
    }
    path.pop_back();
    
    if (!path.empty() && !pCtxId.empty())
    {
      path = pCtxId + '.' + path;
    }
    else if (!pCtxId.empty())
    {
      path = pCtxId;
    }
    
    return path;
  }//getExtendedPath
  
}//end namespace Model
