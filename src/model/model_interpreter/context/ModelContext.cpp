// Base class
#include "ModelContext.hpp"
#include "ModelMacro.hpp"
#include "ModelUtils.hpp"

#include <cassert>

namespace Model {
  
  ModelContext::ModelContext()
  : ModelContext(Utils::getUndefModelName())
  {
  }
  
  ModelContext::ModelContext(const std::string& aName)
  : pModelName(aName)
  {
    pObjReg.resize(ContextObject::Type::CTX_OBJ_TYPE_LAST);
  }
  
  void ModelContext::addContextObject(const CtxObjSPtr& aCtxObj)
  {
    assert(aCtxObj && static_cast<std::size_t>(aCtxObj->getType()) < pObjReg.size());
    pObjReg[static_cast<std::size_t>(aCtxObj->getType())].push_back(aCtxObj);
  }//addContextObject
  
  const ModelContext::CtxObjVector& ModelContext::getContextObjectVectorByType(ContextObject::Type aType) const
  {
    auto idx = static_cast<std::size_t>(aType);
    assert(idx < pObjReg.size());
    
    return pObjReg.at(idx);
  }//getContextObjectVectorByType
  
}//end namespace Model

