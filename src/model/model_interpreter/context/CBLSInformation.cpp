// Self first
#include "CBLSInformation.hpp"

#include "SearchOptionsTools.hpp"
#include "ObjectHelper.hpp"
#include "ParserUtils.hpp"

#include <cstddef>  // for std::size_t

namespace otools  = Interpreter::ObjectTools;
namespace sotools = Interpreter::SearchOptionsTools;
namespace putils  = Model::Utils::Parser;

namespace Model {
  
  const std::string CBLSInformation::NeighborhoodSizeKey = "neighborhoodSize";
  
  const std::string CBLSInformation::NumNeighborhoodsKey = "numNeighborhoods";
  
  const std::string CBLSInformation::AggressiveCBLSKey = "aggressiveCBLS";
  
  const std::string CBLSInformation::RandomSeedKey = "seed";
  
  const std::string CBLSInformation::WarmStartKey = "warmStart";
  
  CBLSInformation::CBLSInformation(SearchInformationType aType)
  : SearchInformation(aType)
  {
  }
  
  void CBLSInformation::readSearchOptionsObject(Interpreter::BaseObject* aObj)
  {
    assert(aObj);
    
    // Set CBLS information
    
    // 1) Random seed
    assert(aObj->hasProperty(RandomSeedKey));
    addMember(RandomSeedKey,
              putils::getDataObjectFromPrimitiveOrScalarType(aObj->
                                                  getProperty(RandomSeedKey)).castDataValue<int>());
    
    // 2) Neighborhood size
    assert(aObj->hasProperty(NeighborhoodSizeKey));
    addMember(NeighborhoodSizeKey,
              putils::getDataObjectFromPrimitiveOrScalarType(aObj->
                                    getProperty(NeighborhoodSizeKey)).castDataValue<int>());
    
    // 3) Number of neighborhoods
    assert(aObj->hasProperty(NumNeighborhoodsKey));
    addMember(NumNeighborhoodsKey,
    putils::getDataObjectFromPrimitiveOrScalarType(aObj->
                                    getProperty(NumNeighborhoodsKey)).castDataValue<int>());
    
    // 4) Aggressive CBLS
    assert(aObj->hasProperty(AggressiveCBLSKey));
    addMember(AggressiveCBLSKey,
              putils::getDataObjectFromPrimitiveOrScalarType(aObj->
                                             getProperty(AggressiveCBLSKey)).castDataValue<bool>());
    
    // 5) Warm start
    assert(aObj->hasProperty(WarmStartKey));
    addMember(WarmStartKey,
              putils::getDataObjectFromPrimitiveOrScalarType(aObj->
                                                  getProperty(WarmStartKey)).castDataValue<bool>());
    
    // 6) Timeout
    assert(aObj->hasProperty(SearchInformation::TimeoutKey));
    updateMember(SearchInformation::TimeoutKey,
                 putils::getDataObjectFromPrimitiveOrScalarType(aObj->
                                  getProperty(SearchInformation::TimeoutKey)).castDataValue<int>());
    
    // Read strategy-specific options
    readStrategySearchOptionsObject(aObj);
  }//readSearchOptionsObject
  
}//end namespace Model
