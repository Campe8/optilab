// Self first
#include "DFSInformation.hpp"

#include "ModelASTMacroBranch.hpp"

#include "SearchOptionsTools.hpp"

#include <algorithm>  // for std::find
#include <stdexcept>  // for std::logic_error

namespace otools  = Interpreter::ObjectTools;
namespace sotools = Interpreter::SearchOptionsTools;

namespace Model {
  
  const std::string DFSInformation::StrategyName = "dfs";
  
  const std::string DFSInformation::VariableSelectionKey = "variableSelection";
  
  const std::string DFSInformation::ValueSelectionKey = "valueSelection";
  
  const std::vector<std::string> DFSInformation::VariableSelectionChoiceList =
  {
      std::string(MDL_AST_LEAF_BRC_VAR_DEFAULT_VAL)
    , std::string(MDL_AST_LEAF_BRC_VAR_INPUT_ORDER)
    , std::string(MDL_AST_LEAF_BRC_VAR_LARGEST)
    , std::string(MDL_AST_LEAF_BRC_VAR_SMALLEST)
    , std::string(MDL_AST_LEAF_BRC_VAR_FIRST_FAIL)
    , std::string(MDL_AST_LEAF_BRC_VAR_ANTI_FIRST_FAIL)
    , std::string(MDL_AST_LEAF_BRC_VAR_INPUT_ORDER)
  };
  
  const std::vector<std::string> DFSInformation::ValueSelectionChoiceList =
  {
      std::string(MDL_AST_LEAF_BRC_VAL_DEFAULT_VAL)
    , std::string(MDL_AST_LEAF_BRC_VAL_INDOMAIN_MAX)
    , std::string(MDL_AST_LEAF_BRC_VAL_INDOMAIN_MIN)
    , std::string(MDL_AST_LEAF_BRC_VAL_INDOMAIN_RANDOM)
  };
  
  DFSInformation::DFSInformation()
  : SearchInformation(SearchInformation::SearchInformationType::SI_DFS)
  {
  }
  
  void DFSInformation::setVariableChoice(const std::string& aVarChoice)
  {
    if(std::find(VariableSelectionChoiceList.begin(), VariableSelectionChoiceList.end(), aVarChoice) ==
                 VariableSelectionChoiceList.end())
    {
      throw std::logic_error("Variable selection choice not supported");
    }
    addMember(VariableSelectionKey, aVarChoice);
  }//setVariableChoice
  
  void DFSInformation::setValueChoice(const std::string& aValChoice)
  {
    if(std::find(ValueSelectionChoiceList.begin(), ValueSelectionChoiceList.end(), aValChoice) ==
       ValueSelectionChoiceList.end())
    {
      throw std::logic_error("Domain value selection choice not supported");
    }
    addMember(ValueSelectionKey, aValChoice);
  }//setValueChoice
  
  void DFSInformation::readSearchOptionsObject(Interpreter::BaseObject* aObj)
  {
    // Check object and its type
    assert(aObj && sotools::getClassType(aObj) == static_cast<int>(SearchInformation::SearchInformationType::SI_DFS));
    
    // Set variable and values choice
    setVariableChoice(otools::getObjectPropertyValue<std::string>(aObj, VariableSelectionKey));
    setValueChoice(otools::getObjectPropertyValue<std::string>(aObj, ValueSelectionKey));
    
    // Add timeout
    auto timeout = otools::getObjectPropertyValue<int>(aObj, SearchInformation::TimeoutKey);
    updateMember(SearchInformation::TimeoutKey, timeout);
  }//readSearchOptionsObject
  
}//end namespace Model
