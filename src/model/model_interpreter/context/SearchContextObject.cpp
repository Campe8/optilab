// Self first
#include "SearchContextObject.hpp"

#include "ModelMacro.hpp"
#include "MVCUtils.hpp"

// Object tools
#include "SearchOptionsTools.hpp"
#include "ObjectHelper.hpp"

// Search information
#include "DFSInformation.hpp"
#include "GeneticInformation.hpp"

#include <algorithm>

namespace otools  = Interpreter::ObjectTools;
namespace sotools = Interpreter::SearchOptionsTools;

namespace Model {
  
  /// Utility function: removes duplicate variables from the branching variable list
  static void removeDuplicates(Interpreter::DataObject& aDO)
  {
    assert(aDO.isComposite());
    
    std::vector<std::string> environmentVarIDs;
    std::vector<Interpreter::DataObject> uniqueEnvironmentVars;
    
    // Single variable, nothing to remove
    if (!aDO[0].isClassObject()) return;
    
    auto obj = aDO[0].getClassObject().get();
    if(otools::isVariableObject(obj)) return;
    
    assert(otools::isMatrixParameterObject(obj) || otools::isListObject(obj));
    for (const auto& var : aDO[0])
    {
      assert(var.isClassObject());
      assert(otools::isVariableObject(var.getClassObject().get()) || !var.isFullyInterpreted());
      
      std::string ID;
      if(var.isFullyInterpreted())
      {
        // Do not allow subscriptions
        assert(!MVC::Utils::isVarMatrixSubscr(var));
        ID = MVC::Utils::getLinkedVariableIDFromDomainDataObject(var);
      }
      else
      {
        assert(var.dataObjectType() == Interpreter::DataObject::DataObjectType::DOT_STRING);
        ID = var.getDataValue<std::string>();
      }
      
      if(std::find(environmentVarIDs.begin(), environmentVarIDs.end(), ID) ==
         environmentVarIDs.end())
      {
        environmentVarIDs.push_back(ID);
        uniqueEnvironmentVars.push_back(var);
      }
    }//for
    
    // Remplace branching variables with the unique set of branching variables
    aDO[0].compose(uniqueEnvironmentVars);
  }// removeDuplicates
  
  std::string SearchContextObject::getSearchContextObjectIdPrefix()
  {
    static std::string id{"_s"};
    return id;
  }//getSearchContextObjectIdPrefix
  
  SearchContextObject::SearchContextObject(const std::string& aID,
                                           const Interpreter::DataObject& aDO)
  : ContextObject(aID, aDO)
  , pSrcInformation(nullptr)
  {
    // Set type
    setType(Type::CTX_OBJ_SRC);
    
    // The given data object should be a search object
    assert(Interpreter::isClassObjectType(aDO) &&
           otools::isSearchObject(aDO.getClassObject().get()));
    
    // Get the search obejct
    auto searchObj = aDO.getClassObject().get();
    const auto& scope = otools::getSearchScope(searchObj);
    const auto& sopts = otools::getSearchOptions(searchObj);
    
    // Check the given data object.
    // Search statements should be translated into a DataObject instance
    // as follows:
    // - aDO[0]: should contain a list object representing the search scope variable(s)
    // - aDO[1]: it is optional and should contain a class object with the
    //           encapsulating the information about the search strategy
    assert(Interpreter::isClassObjectType(scope) &&
           otools::isListObject(scope.getClassObject().get()));
    auto& scopeVarsList = otools::getListValues(scope.getClassObject().get());
    assert(scopeVarsList.isComposite());
    
    if(!scopeVarsList.isFullyInterpreted()) return;
    
    // Remove any duplicate variables in the payload (aDO[0])
    removeDuplicates(scopeVarsList);
    
    // Set the search information
    setSearchInformation(sopts);
  }
  
  void SearchContextObject::setSearchInformation(const Interpreter::DataObject& aDO)
  {
    // Get the object representing the search information
    assert(aDO.isClassObject());
    auto srcObj = aDO.getClassObject().get();
    
    assert(sotools::isSearchOptionsObject(srcObj));
    auto objClass =
    static_cast<SearchInformation::SearchInformationType>(sotools::getClassType(srcObj));
    
    switch (objClass) {
      case SearchInformation::SearchInformationType::SI_DFS:
        pSrcInformation = std::make_shared<DFSInformation>();
        break;
      default:
        assert(objClass == SearchInformation::SearchInformationType::SI_GENETIC);
        pSrcInformation = std::make_shared<GeneticInformation>();
        break;
    }
    assert(pSrcInformation);
    
    // Add all the properties of the object to the search information
    pSrcInformation->readSearchOptionsObject(srcObj);
  }// setSearchInformation
  
}// end namespace Model
