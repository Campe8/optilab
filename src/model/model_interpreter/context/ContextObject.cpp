// Self first
#include "ContextObject.hpp"

#include "ObjectHelper.hpp"

namespace otools = Interpreter::ObjectTools;

namespace Model {
  
  // Returns the correct type for a standard context object.
  // It distinguishes between:
  // - Type::CTX_OBJ_STD: BaseObjects derived from ContextObjects class, e.g., parameters
  // - Type::CTX_OBJ_DBO: any BaseObject instance which is not derived from ContextObject class,
  //                      e.g., SearchOptions
  static ContextObject::Type getTypeForStandardObject(const Interpreter::DataObject& aDO)
  {
    if(!Interpreter::isClassObjectType(aDO)) return ContextObject::Type::CTX_OBJ_STD;
    
    // Scalars, lists, and matrices are "standard" context objects
    auto baseObj = aDO.getClassObject().get();
    if (otools::isScalarObject(baseObj) ||
        otools::isListObject(baseObj)   ||
        otools::isMatrixObject(baseObj))
    {
      return ContextObject::Type::CTX_OBJ_STD;
    }
    
    // Any other object is a generic base object
    return ContextObject::Type::CTX_OBJ_DBO;
  }//getTypeForStandardObject
  
  ContextObject::ContextObject(const std::string& aID, const Interpreter::DataObject& aDO)
  : pID(aID)
  , pType(getTypeForStandardObject(aDO))
  , pPayload(aDO)
  {
  }
  
  ContextObject::~ContextObject()
  {
  }
  
}//end namespace Model
