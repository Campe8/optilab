// Self first
#include "BaseObjectContextObject.hpp"

#include "MVCUtils.hpp"

namespace Model {
  
  BaseObjectContextObject::BaseObjectContextObject(const std::string& aID,
                                                   const Interpreter::DataObject& aDO)
  : ContextObject(aID, aDO)
  {
    setType(Type::CTX_OBJ_DBO);
  }
  
}//end namespace Model

