// Self first
#include "ViewEntryArray.hpp"

#include <cassert>

namespace Model {
  
  bool ViewEntryArray::isa(const ViewEntry* aEntry)
  {
    return aEntry &&
    aEntry->getCategory() == EntryCategory::EC_ARRAY;
  }//isa
  
  ViewEntryArray* ViewEntryArray::cast(ViewEntry* aEntry)
  {
    if (!isa(aEntry))
    {
      return nullptr;
    }
    return static_cast<ViewEntryArray*>(aEntry);
  }//cast
  
  ViewEntryArray::ViewEntryArray(const std::string& aID, const std::vector<EntryValue>& aArray)
  : ViewEntry(aID, EntryCategory::EC_ARRAY)
  , pDirtyCache(false)
  , pSerialization()
  {
    parseArray(aArray);
    
    // Serialize this entry after parsing the array
    serializeEntry();
    
    // Set needs interpretation flag
    if (pNonInterpMap.empty())
    {
      setInterpreted(true);
    }
    else
    {
      // Some values needs to be further interpreted
      setInterpreted(false);
    }
  }
  
  ViewEntry* ViewEntryArray::clone()
  {
    auto entry = new ViewEntryArray(getID(), {});
    entry->pDirtyCache = pDirtyCache;
    entry->pSerialization = pSerialization;
    entry->pArray = pArray;
    entry->pNonInterpMap = pNonInterpMap;
    entry->setInterpreted(isFullyInterpreted());
    return entry;
  }//clone
  
  std::string ViewEntryArray::serialize() const
  {
    if (pDirtyCache || pSerialization.empty())
    {
      serializeEntry();
    }
    return pSerialization;
  }//serialize
  
  void ViewEntryArray::parseArray(const std::vector<EntryValue>& aArray)
  {
    std::size_t idx{ 0 };
    pArray.resize(aArray.size());
    for (auto& vp : aArray)
    {
      if (vp.second)
      {
        // Value is fully interpreted
        pArray[idx++] = vp.first;
        continue;
      }
      // Value is not fully interpreted
      // store it into the map
      pNonInterpMap[idx] = vp.first;
      // Set non-interpreted value into the array
      pArray[idx++] = ViewEntry::ninterp;
    }
  }//serializeArray
  
  void ViewEntryArray::serializeEntry() const
  {
    assert(!pArray.empty());
    
    // Serialize array
    pSerialization = "var " + getID() + "[" + CLI::Utils::toString<std::size_t>(pArray.size()) + "] := ";
    pSerialization += "{";
    for (auto& v : pArray)
    {
      pSerialization += v + ", ";
    }
    if (pArray.size() > 0)
    {
      pSerialization.pop_back();
      pSerialization.pop_back();
    }
    pSerialization += "};";
    
    // Reset cache flag
    pDirtyCache = false;
  }//serializeEntry
  
  void ViewEntryArray::setInterpretedValue(std::size_t aIdx, const std::string& aValue)
  {
    assert(aIdx < pArray.size());
    
    // If the index of the interpreted value is an index of a value
    // that needed interpretation, remove it from the map
    if (needsInterpretation(aIdx))
    {
      pNonInterpMap.erase(aIdx);
      
      // Set interpreted flag if the map is now empty
      if (pNonInterpMap.empty())
      {
        setInterpreted(true);
      }
    }
    
    // Substitute value into the internal array
    pArray.at(aIdx) = aValue;
    pDirtyCache = true;
  }//setInterpretedValue
  
}//end namespace Model
