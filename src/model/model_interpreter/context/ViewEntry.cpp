// Self first
#include "ViewEntry.hpp"

#include <cassert>

namespace Model {
  
  std::string ViewEntry::ninterp = "0/0";
  
  ViewEntry::ViewEntry(const std::string& aID, EntryCategory aCat)
  : pID(aID)
  , pCategory(aCat)
  {
    assert(!pID.empty());
  }
  
}//end namespace Model

