// Self first
#include "SearchInformation.hpp"

namespace Model {
  
  const std::string SearchInformation::TimeoutKey = "timeout";
  
  SearchInformation::SearchInformation(SearchInformationType aType)
  : pType(aType)
  , pInfoMap(std::make_shared<SearchInformation::InfoMap>())
  {
    // Add no timeout by default.
    // @note add it here since all derived classes of SearchInformation
    // expect a timeout member
    addMember<int>(TimeoutKey, -1);
  }
  
}//end namespace Model
