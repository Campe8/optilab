// Self first
#include "VariableContextObject.hpp"

#include "OPCode.hpp"
#include "BaseObject.hpp"
#include "ObjectTools.hpp"
#include "ObjectHelper.hpp"
#include "ModelMacro.hpp"

#include <algorithm>
#include <sstream>

namespace otools = Interpreter::ObjectTools;

namespace Model {
  
  // Checks the correctness of the dimensions of the variable.
  // @note throws if the dimensions are not correct
  static void checkDimensions(Interpreter::BaseObject* aVar)
  {
    auto size = otools::getVarDomainNumDimensions(aVar);
    model_assert_msg(size <= 2, std::to_string(size) + "D variable matrices not supported");
    
    auto semantic = otools::getVarSemantic(aVar);
    model_assert_msg((semantic != VAR_SPEC_OPT_MIN && semantic != VAR_SPEC_OPT_MAX) || size == 0,
                     "Multi-dimensional objective variables not supported");
  }//checkDimensions
  
  VariableContextObject::VariableContextObject(const std::string& aVarName,
                                               const Interpreter::DataObject& aDO)
  : ContextObject(aVarName, aDO)
  , pOutput(boost::logic::indeterminate)
  {
    // Set type
    setType(Type::CTX_OBJ_VAR);
    
    // Check that the data object owns a class object instance and that
    // the type of the class instance is a variable
    assert(aDO.isClassObject());
    auto var = aDO.getClassObject().get();
    auto id =(var->getProperty("id")).getDataValue<std::string>();
    
    assert(otools::isVariableObject(var));
    if(otools::getVarSemantic(var) == VAR_SPEC_NONE)
    {
      otools::setVarSemantic(var, VAR_SPEC_DECISION);
    }
    
    if(var->getProperty(Interpreter::ObjectTools::getObjectInterpretedPropName()).getDataValue<bool>())
    {
      checkDimensions(var);
    }
  }
  
}//end namespace Model
