// Self first
#include "ViewEntryScalar.hpp"

namespace Model {
  
  bool ViewEntryScalar::isa(const ViewEntry* aEntry)
  {
    return aEntry &&
    aEntry->getCategory() == EntryCategory::EC_SCALAR;
  }//isa
  
  ViewEntryScalar* ViewEntryScalar::cast(ViewEntry* aEntry)
  {
    if (!isa(aEntry))
    {
      return nullptr;
    }
    return static_cast<ViewEntryScalar*>(aEntry);
  }//cast
  
  ViewEntryScalar::ViewEntryScalar(const std::string& aID, const EntryValue& aScalar)
  : ViewEntry(aID, EntryCategory::EC_SCALAR)
  , pSerialization()
  , pScalar(aScalar.first)
  {
    setInterpreted(aScalar.second);
  }
  
  ViewEntry* ViewEntryScalar::clone()
  {
    return new ViewEntryScalar(getID(), {pScalar, isFullyInterpreted()});
  }//clone
  
  void ViewEntryScalar::setInterpretedValue(const std::string& aScalar)
  {
    setInterpreted(true);
    
    pScalar = aScalar;
    serializeScalar();
  }//setInterpretedValue
  
  void ViewEntryScalar::serializeScalar() const
  {
    pSerialization = "var " + getID() + " := ";
    pSerialization += pScalar;
  }//serializeScalar
  
  std::string ViewEntryScalar::serialize() const
  {
    if (pSerialization.empty())
    {
      serializeScalar();
    }
    return pSerialization;
  }//serialize
  
}//end namespace Model

