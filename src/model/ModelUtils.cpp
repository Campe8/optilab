// Self first
#include "ModelUtils.hpp"

#include "AES.hpp"
#include "CLIUtils.hpp"
#include "BaseUtils.hpp"
#include "ModelASTMacroConstraint.hpp"

#include "rapidxml-1.13/rapidxml.hpp"
#include <boost/filesystem.hpp>
#include <boost/dll.hpp>

#include <cctype>  // for isdigit
#include <iostream>
#include <sstream>
#include <unordered_set>
#include <algorithm>
#include <cassert>


namespace Model {
  namespace Utils {
    
    spp::sparse_hash_map<std::string, Core::ConstraintId> ConstraintHelper::StringToCoreIDMap = {
        {"lq", Core::ConstraintId::CON_ID_LQ}
      , {"gq", Core::ConstraintId::CON_ID_GQ}
      , {"nq", Core::ConstraintId::CON_ID_NQ}
      , {"eq", Core::ConstraintId::CON_ID_EQ}
      , {"lin_eq", Core::ConstraintId::CON_ID_LIN_EQ}
      , {"lin_nq", Core::ConstraintId::CON_ID_LIN_NQ}
      , {"lin_lq", Core::ConstraintId::CON_ID_LIN_LQ}
      , {"lin_lt", Core::ConstraintId::CON_ID_LIN_LT}
      , {"times", Core::ConstraintId::CON_ID_TIMES}
      , {"abs", Core::ConstraintId::CON_ID_ABS}
      , {"div", Core::ConstraintId::CON_ID_DIV}
      , {"lt", Core::ConstraintId::CON_ID_LT}
      , {"gt", Core::ConstraintId::CON_ID_GT}
      , {"max", Core::ConstraintId::CON_ID_MAX}
      , {"min", Core::ConstraintId::CON_ID_MIN}
      , {"plus", Core::ConstraintId::CON_ID_PLUS}
      , {"minus", Core::ConstraintId::CON_ID_MINUS}
      , {"eq_reif", Core::ConstraintId::CON_ID_EQ_REIF}
      , {"eq_reif_r", Core::ConstraintId::CON_ID_EQ_HREIF_R}
      , {"eq_reif_l", Core::ConstraintId::CON_ID_EQ_HREIF_L}
      , {"lq_reif", Core::ConstraintId::CON_ID_LQ_REIF}
      , {"lq_reif_r", Core::ConstraintId::CON_ID_LQ_HREIF_R}
      , {"lq_reif_l", Core::ConstraintId::CON_ID_LQ_HREIF_L}
      , {"gq_reif", Core::ConstraintId::CON_ID_GQ_REIF}
      , {"gq_reif_l", Core::ConstraintId::CON_ID_GQ_HREIF_L}
      , {"gq_reif_r", Core::ConstraintId::CON_ID_GQ_HREIF_R}
      , {"lin_eq_reif", Core::ConstraintId::CON_ID_LIN_EQ_REIF}
      , {"lin_eq_reif_r", Core::ConstraintId::CON_ID_LIN_EQ_HREIF_R}
      , {"lin_eq_reif_l", Core::ConstraintId::CON_ID_LIN_EQ_HREIF_L}
      , {"lin_lq_reif", Core::ConstraintId::CON_ID_LIN_LQ_REIF}
      , {"lin_lq_reif_r", Core::ConstraintId::CON_ID_LIN_LQ_HREIF_R}
      , {"lin_lq_reif_l", Core::ConstraintId::CON_ID_LIN_LQ_HREIF_L}
      , {"lin_nq_reif", Core::ConstraintId::CON_ID_LIN_NQ_REIF}
      , {"lin_nq_reif_r", Core::ConstraintId::CON_ID_LIN_NQ_HREIF_R}
      , {"lin_nq_reif_l", Core::ConstraintId::CON_ID_LIN_NQ_HREIF_L}
      , {"lt_reif", Core::ConstraintId::CON_ID_LT_REIF}
      , {"lt_reif_r", Core::ConstraintId::CON_ID_LT_HREIF_R}
      , {"lt_reif_l", Core::ConstraintId::CON_ID_LT_HREIF_L}
      , {"gt_reif", Core::ConstraintId::CON_ID_GT_REIF}
      , {"gt_reif_l", Core::ConstraintId::CON_ID_GT_HREIF_L}
      , {"gt_reif_r", Core::ConstraintId::CON_ID_GT_HREIF_R}
      , {"nq_reif", Core::ConstraintId::CON_ID_NQ_REIF}
      , {"nq_reif_r", Core::ConstraintId::CON_ID_NQ_HREIF_R}
      , {"nq_reif_l", Core::ConstraintId::CON_ID_NQ_HREIF_L}
      , {"and", Core::ConstraintId::CON_ID_BOOL_AND}
      , {"or", Core::ConstraintId::CON_ID_BOOL_EQ}
      , {"mod", Core::ConstraintId::CON_ID_MOD}
      , {"alldiff", Core::ConstraintId::CON_ID_ALLDIFF}
      , {"circuit", Core::ConstraintId::CON_ID_CIRCUIT}
      , {"domain", Core::ConstraintId::CON_ID_DOMAIN}
      , {"element", Core::ConstraintId::CON_ID_ELEMENT}
    };
    
    static std::vector<std::string> tokenizeStringOnSymbol(const std::string& aString, char aChar)
    {
      std::vector<std::string> tokens;
      if (!aString.empty())
      {
        std::istringstream ss(aString);
        std::string token;
        while (std::getline(ss, token, aChar))
        {
          tokens.push_back(token);
        }
      }
      return tokens;
    }//tokenizeStringOnSymbol
    
    static ConstraintHelper::ArgType::Type getArgType(const std::string& aType)
    {
      if (aType == "var") return ConstraintHelper::ArgType::Type::T_VAR;
      if (aType == "int") return ConstraintHelper::ArgType::Type::T_INT;
      if (aType == "bool") return ConstraintHelper::ArgType::Type::T_BOOL;
      return ConstraintHelper::ArgType::Type::T_UNDEF;
    }//getArgType
    
    static std::vector<std::string> getXMLFileNames()
    {
      using Path = boost::filesystem::path;
      
      std::vector<std::string> fileNameList;
      
      auto exePath = boost::dll::program_location().parent_path();
      auto resPath = exePath / "resources";

      Path rootPath;
      // if "resources" is near executable use exe path as rootpath
      // else use getcwd
      if( boost::filesystem::is_directory(resPath) ){
        rootPath = exePath;
      } else {
        rootPath = Path(CLI::Utils::getRootPath().c_str());
      }

      rootPath = rootPath / "resources";
      rootPath = rootPath / "r0";
      //rootPath = rootPath / "xml";
      //rootPath = rootPath / "constraints";
      
      // Create folder path
      Path pDir(rootPath.c_str());
      
      boost::filesystem::directory_iterator endIt;
      
      // Cycle through the directory
      for (boost::filesystem::directory_iterator it(pDir); it != endIt; ++it)
      {
        // If it's not a directory, list it
        if (is_regular_file(it->path()))
        {
          // Check the extension and report only XML files
          /*
          if (it->path().extension().string() == ".xml")
          {
            fileNameList.push_back(it->path().string());
          }*/
          if (isdigit(it->path().string().back()))
          {
            fileNameList.push_back(it->path().string());
          }
        }
      }// for
      
      return fileNameList;
    }//getXMLFileNames
    
    ConstraintHelper& ConstraintHelper::getInstance()
    {
      static ConstraintHelper instance;
      return instance;
    }//getInstance
    
    ConstraintHelper::ConstraintHelper()
    {
      // Load the specifications for the constraints
      // into the internal data structures
      loadConstraintSpecifications();
    }
    
    void ConstraintHelper::addConstraintSpecToMap(ConstraintSpec* aSpec)
    {
      assert(aSpec);
      
      auto& entry = pConSpecMap[aSpec->name];
      if (entry)
      {
        entry.reset(aSpec);
        return;
      }
      entry = std::unique_ptr<ConstraintSpec>(aSpec);
    }//addConstraintSpecToMap
    
    ConstraintHelper::ConstraintSpec* ConstraintHelper::lookup(const std::string& aName) const
    {
      if (pConSpecMap.find(aName) == pConSpecMap.end()) return nullptr;
      return pConSpecMap.at(aName).get();
    }//lookup
    
    void ConstraintHelper::loadConstraintSpecifications()
    {
      // Load all the filenames of the xml constraint description files
      // under the given folder
      
      // Prepare the decryptor
      Base::Tools::AES aes("OptiLab-Beta-v01");
      auto xmlFileList = getXMLFileNames();
      for (auto& xml : xmlFileList)
      {
        // Read the xml file into a vector
        std::ifstream xmlFile(xml, std::ios::binary);
        assert(xmlFile.is_open());
        
        // Get the string file representation
        std::string fileContent((std::istreambuf_iterator<char>(xmlFile)),
                                std::istreambuf_iterator<char>());
        
        // Decode the content of the file
        fileContent = aes.decryptMsg(fileContent);
        std::vector<char> buffer(fileContent.begin(), fileContent.end());
        buffer.push_back('\0');
        
        // For each xml load the corresponding constraint specification
        auto cdesc = parseConstraintSpecification(buffer);
        assert(cdesc);
        
        // Add the specification into the internal map
        addConstraintSpecToMap(cdesc);
      }
    }//loadConstraintSpecifications
    
    ConstraintHelper::ConstraintSpec* ConstraintHelper::parseConstraintSpecification(std::vector<char>& aSpec)
    {
      rapidxml::xml_document<> doc;
      rapidxml::xml_node<> * rootNode;
      
      // Parse the buffer using the xml file parsing library into doc
      doc.parse<0>(&aSpec[0]);
      
      // Find root node
      rootNode = doc.first_node();
      if (!rootNode) return nullptr;
      
      // Parse the document
      ConstraintSpec* cspec = new ConstraintSpec();
      for (rapidxml::xml_node<>* node = rootNode->first_node(); node; node = node->next_sibling())
      {
        std::string name(node->name());
        if (name == "name")
        {
          cspec->name = std::string(node->value());
        }
        else if (name == "global")
        {
          std::string val(node->value());
          cspec->global = val == "true";
        }
        else if (name == "reif")
        {
          std::string val(node->value());
          cspec->reif = val == "true";
        }
        else if (name == "arguments")
        {
          for (rapidxml::xml_node<> * argNode = node->first_node("arg"); argNode; argNode = argNode->next_sibling())
          {
            ConstraintHelper::ArgTypeList typeList;
            for (rapidxml::xml_node<> * argType = argNode->first_node("type"); argType; argType = argType->next_sibling())
            {
              ArgType atype;
              if (argType->first_attribute("array"))
              {
                std::string arrVal(argType->first_attribute("array")->value());
                atype.argArray = arrVal == "true";
              }
              
              std::string val(argType->value());
              auto types = tokenizeStringOnSymbol(val, '_');
              
              // If the argument is not an array, there is only one type allowed
              assert(atype.argArray || types.size() == 1);
              for (auto& tp : types)
              {
                atype.argType.push_back(getArgType(tp));
                assert(atype.argType.back() != ConstraintHelper::ArgType::Type::T_UNDEF);
              }
              typeList.push_back(atype);
            }//for
            cspec->argList.push_back(typeList);
          }//for
        }
        else if (name == "constraints")
        {
          // Look for constraints on the declaration
          for (rapidxml::xml_node<> * conNode = node->first_node("samesize"); conNode; conNode = conNode->next_sibling())
          {
            // Same size arguments constraint
            for (rapidxml::xml_node<> * sizeNode = conNode->first_node("arg"); sizeNode; sizeNode = sizeNode->next_sibling())
            {
              auto listVal = CLI::Utils::tokenizeStringOnSymbol(sizeNode->value(), ' ') ;
              std::vector<int> listValInt;
              std::transform(listVal.begin(), listVal.end(), std::back_inserter(listValInt),
                             [](const std::string& str) { return std::stoi(str) - 1; });
              cspec->sameSizeArgList.push_back(listValInt);
            }
          }//for
        }
        else if (name == "syntax")
        {
          cspec->syntax = std::string(node->value());
        }
        else if (name == "description")
        {
          cspec->description = std::string(node->value());
        }
        else if (name == "example")
        {
          cspec->example = std::string(node->value());
        }
      }//for
      
      return cspec;
    }//parseConstraintSpecification
    
    bool ConstraintHelper::isConstraintGlobal(const std::string& aName) const
    {
      auto spec = lookup(aName);
      assert(spec);
      
      return spec->global;
    }//isConstraintGlobal
    
    bool ConstraintHelper::isConstraintReified(const std::string& aName) const
    {
      auto spec = lookup(aName);
      assert(spec);
      
      return spec->reif;
    }//isConstraintReified
    
    std::size_t ConstraintHelper::numConstraintArguments(const std::string& aName) const
    {
      auto spec = lookup(aName);
      assert(spec);
      
      return (spec->argList).size();
    }//numConstraintArguments
    
    bool ConstraintHelper::hasSameSizeArgumentConstraint(const std::string& aName) const
    {
      auto spec = lookup(aName);
      assert(spec);
      
      return !spec->sameSizeArgList.empty();
    }//constraintSyntax
    
    std::vector<std::vector<int>> ConstraintHelper::getSameSizeArgumentConstraint(const std::string& aName) const
    {
      auto spec = lookup(aName);
      assert(spec);
      
      return spec->sameSizeArgList;
    }//getSameSizeArgumentConstraint
    
    std::string ConstraintHelper::constraintSyntax(const std::string& aName) const
    {
      auto spec = lookup(aName);
      assert(spec);
      
      return spec->syntax;
    }//constraintSyntax
    
    std::string ConstraintHelper::constraintDescription(const std::string& aName) const
    {
      auto spec = lookup(aName);
      assert(spec);
      
      return spec->description;
    }//constraintDescription
    
    std::string ConstraintHelper::constraintExample(const std::string& aName) const
    {
      auto spec = lookup(aName);
      assert(spec);
      
      return spec->example;
    }//constraintExample
    
    const ConstraintHelper::ArgTypeList& ConstraintHelper::getArgTypeList(const std::string& aName, std::size_t aIdx) const
    {
      auto spec = lookup(aName);
      assert(spec);
      assert(aIdx < numConstraintArguments(aName));
      
      return (spec->argList).at(aIdx);
    }//getArgType
    
    bool ConstraintHelper::isConstraintArgumentArrayAllowed(const std::string& aName, std::size_t aIdx) const
    {
      auto argList = getArgTypeList(aName, aIdx);
      for (auto& arg : argList)
      {
        if (arg.argArray) return true;
      }
      return false;
    }//isConstraintArgumentArrayAllowed
    
    std::vector<ConstraintHelper::ArgType::Type> ConstraintHelper::getConstraintArgumentNonArrayAllowedTypes(const std::string& aName, std::size_t aIdx) const
    {
      std::vector<ConstraintHelper::ArgType::Type> types;
      
      auto argList = getArgTypeList(aName, aIdx);
      for (auto& arg : argList)
      {
        if (arg.argArray) continue;
        assert(arg.argType.size() == 1);
        types.push_back(arg.argType[0]);
      }
      return types;
    }//getConstraintArgumentNonArrayAllowedTypes
    
    std::vector<ConstraintHelper::ArgType::Type> ConstraintHelper::getConstraintArgumentArrayAllowedTypes(const std::string& aName, std::size_t aIdx) const
    {
      std::vector<ConstraintHelper::ArgType::Type> types;
      
      auto argList = getArgTypeList(aName, aIdx);
      for (auto& arg : argList)
      {
        if (!arg.argArray) continue;
        assert(!arg.argType.empty());
        for (auto& tp : arg.argType)
        {
          types.push_back(tp);
        }
      }
      return types;
    }//getConstraintArgumentArrayAllowedTypes
    
    std::vector<std::string> ConstraintHelper::getAllRegisteredConstraintNames() const
    {
      std::vector<std::string> regNames;
      for(const auto& it : pConSpecMap) regNames.push_back(it.first);
      return regNames;
    }//getAllRegisteredConstraintNames
    
    Core::ConstraintId ConstraintHelper::stringIDToCoreID(const std::string& aID) const
    {
      if(StringToCoreIDMap.find(aID) != StringToCoreIDMap.end())
      {
        return StringToCoreIDMap.at(aID);
      }
      return Core::ConstraintId::CON_ID_UNDEF;
    }//stringIDToCoreID
   
    MODEL_EXPORT_FUNCTION std::string getUndefModelName()
    {
      std::string undefName{"__"};
      return undefName += generateRandomString();
    }//getUndefModelName
    
    MODEL_EXPORT_FUNCTION bool isUndefinedModelName(const std::string& aModelName)
    {
      if (aModelName.size() < 2)
      {
        return false;
      }
      
      return aModelName[0] == '_' && aModelName[1] == '_';
    }//isUndefinedModelName
    
    MODEL_EXPORT_FUNCTION std::string getUndefVarName(bool aResetIdx)
    {
      static std::string varID{ "_v" };
      static std::size_t seed{ 0 };
      if (aResetIdx)
      {
        seed = 0;
      }
      std::stringstream ss;
      ss << seed++;
      
      return varID + ss.str();
    }//getUndefVarName
    
  } // end namespace Utils
}// end namespace Model
