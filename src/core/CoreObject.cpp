// Base class
#include "CoreObject.hpp"

namespace Core {
  
  CoreObject::CoreObject(CoreObjectClass aCoreObjectClass)
  : pCoreObjectClass(aCoreObjectClass)
  {
    pUUIDTag = generateUUIDTag();
  }
  
  CoreObject::~CoreObject()
  {
  }
  
}// end namespace Core
