// Self first
#include "InitSys.hpp"

#include "utest.hpp"

#include <string>

namespace Init {
  
  InitSystem::InitSystem(int argc, char* argv[])
  : pArgc(argc)
  , pArgv(argv)
  , pStatelessSOpt('f')
  , pStatelessOpt("file")
  , pSmartCLISOpt('s')
  , pSmartCLIOpt("smartcli")
  , pRunSTest('t')
  , pRunTest("test")
  , pGUISOpt('g')
  , pGUIOpt("gui")
  , pHelpSOpt('h')
  , pHelpOpt("help")
  , pSmartCLI(false)
  , pFilePath("")
  , pRunMode(RunMode::INTERACTIVE)
  , pProgramOptions(std::make_shared<Base::ProgramOptions>())
  , pView(nullptr)
  , pModel(nullptr)
  , pController(nullptr)
  {
    // Setup the command line interface options
    registerCLIOptions();
    
    // Parse the CLI options
    setupCallingArguments(argc, argv);
  }
  
  void InitSystem::registerCLIOptions()
  {
    getProgramOptions().registerOption(pStatelessOpt, pStatelessSOpt, true,
                                       "run OptiLab on the specified opt model");
    
    getProgramOptions().registerOption(pSmartCLIOpt, pSmartCLISOpt, false,
                                       "run OptiLab with smart CLI support");
    
    getProgramOptions().registerOption(pRunTest, pRunSTest, false, "run OptiLab tests");
    
    getProgramOptions().registerOption(pGUIOpt, pGUISOpt, false,
                                       "run OptiLab with graphical interface");
    
    getProgramOptions().registerOption(pHelpOpt, pHelpSOpt, false, "print this help message");
  }//registerCLIOptions
  
  void InitSystem::setupCallingArguments(int argc, char* argv[])
  {
    // Parse the CLI options
    getProgramOptions().parseOptions(argc, argv);
    
    // Setup internal state.
    // Check if stateless (file) option is selected
    if (getProgramOptions().isOptionSelected(pStatelessOpt))
    {
      if (pRunMode == RunMode::GUI)
      {
        // Two exclusive modes cannot be selected at the same time
        pRunMode = RunMode::NONE;
        return;
      }
      
      // Set stateless mode
      pRunMode = RunMode::STATELESS;
      
      // Get the path to the file to run on
      pFilePath = getProgramOptions().getOptionValue(pStatelessOpt);
      
      // If there is no file, set the mode as VOID (help message) and return asap
      if (pFilePath.empty())
      {
        pRunMode = RunMode::NONE;
        return;
      }
    }
    
    // Check if smart cli must be used
    if (getProgramOptions().isOptionSelected(pSmartCLISOpt))
    {
      pSmartCLI = true;
    }
    
    // Check GUI option
    if (getProgramOptions().isOptionSelected(pGUIOpt))
    {
      if (pRunMode == RunMode::STATELESS)
      {
        // Two exclusive modes cannot be selected at the same time
        pRunMode = RunMode::NONE;
        return;
      }
      pRunMode = RunMode::GUI;
    }
    
    // Check test option.
    // Note that this check must be last (before help) since it overrides all other modes
    if (getProgramOptions().isOptionSelected(pRunTest))
    {
      pRunMode = RunMode::TEST;
    }
    
    // Check help option
    if (getProgramOptions().isOptionSelected(pHelpOpt))
    {
      pRunMode = RunMode::NONE;
    }
  }//setupCallingArguments
  
  int InitSystem::runSystem()
  {
    // Init the view
    if (getRunMode() == RunMode::INTERACTIVE)
    {
      pView = std::make_shared<MVC::MVCViewCLI>(useSmartCLI());
    }
    else if (getRunMode() == RunMode::STATELESS)
    {
      pView = std::make_shared<MVC::MVCViewFile>(getFilePath());
    }
    
    // Call the executable
    return exeSystem();
  }//runSystem
  
  int InitSystem::exeSystem()
  {
    if (getRunMode() == RunMode::NONE)
    {
      getProgramOptions().toStream(std::cout);
      return 1;
    }
    
    // If run test mode is selected, run Google test framework and return
    if (getRunMode() == RunMode::TEST)
    {
      int returnVal{0};
      
      std::cout << "\nRunning GoogleUnitTest\n";
      returnVal += runGoogleUnitTest(&pArgc, pArgv);
      
      return returnVal;
    }
    
    // Run the system
    if (!pView) return 1;
    
    // Generate the Model
    auto pModel = std::make_shared<MVC::MVCModel>();
    
    // Generate the Controller
    auto pController = std::make_shared<MVC::MVCControllerIO>(pView, pModel);
    
    // View observes the controller
    pView->observe(MVC::MVCController::getSubject(pController.get()));
    
    // View observes the model
    pView->observe(MVC::MVCModel::getSubject(pModel.get()));
    
    // Execute controller    
    pController->exe();
    
    return 0;
  }//exeSystem
  
}// end namespace Init
