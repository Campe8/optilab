// Base class
#include "JSON.hpp"

#include <stdexcept>  // for std::invalid_argument

#include <rapidjson/writer.h>

namespace DataStructure {
  
  JSON::JSON()
  {
    pDocument.SetObject();
  }
  
  JSON::JSON(const std::string& json)
  {
    if (pDocument.Parse(json.c_str()).HasParseError())
    {
      throw std::invalid_argument("The given string is not in JSON format: " + json);
    }
  }
  
  std::string JSON::toString() const
  {
    rapidjson::StringBuffer strbuf;
    rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
    pDocument.Accept(writer);
    
    return strbuf.GetString();
  }// toString
  
  JSON::Object& JSON::getObject(const std::string& key)
  {
    JSON::Object& result = pDocument[key.c_str()];
    return result;
  }// getObject
  
  template<>
  void JSON::addValue<std::string>(const std::string& name, std::string& value)
  {
    Object key(name.c_str(), getAllocator());
    Object val(value.c_str(), getAllocator());
    pDocument.AddMember(key, val, getAllocator());
  }
  
  template<>
  JSON::Object JSON::createObject<std::string>(const std::string& value)
  {
    return JSON::Object(value.c_str(), getAllocator());
  }
  
}// end namespace DataStructure
