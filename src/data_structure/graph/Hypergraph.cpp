// Base class
#include "Hypergraph.hpp"

#include <cassert>

namespace DataStructure {
  namespace Graph {
    
    Hypergraph::Node::Node(const NodeCoreSPtr& aNodeCore, Hypergraph::NodeId aNodeId)
    : pNodeId(aNodeId)
    , pNodeCore(aNodeCore)
    {
    }
    
    Hypergraph::Edge::Edge(Hypergraph::EdgeId aEdgeId)
    : pEdgeId(aEdgeId)
    {
    }
    
    void Hypergraph::Edge::addNode(Hypergraph* aGraph, Hypergraph::NodeId aNodeId)
    {
      assert(aGraph);
      assert(aGraph->getNode(aNodeId));
      
      /// Add the node to the set of nodes
      pNodeSet.insert(aNodeId);
      
      /// Link the node to this node
      aGraph->getNode(aNodeId)->connectToEdge(pEdgeId);
    }//addNode
    
    Hypergraph::EdgeDescriptor Hypergraph::addEdge(const std::set<NodeCoreSPtr>& aNodeTypeSet)
    {
      assert(!aNodeTypeSet.empty());
      
      EdgeDescriptor edgeDescriptor;
      
      // Create new edge
      std::shared_ptr<Edge> edgePtr = std::make_shared<Edge>(generateId());
      
      // Iterate on all the nodes and add link them to the edge
      for (auto& ptr : aNodeTypeSet)
      {
        assert(ptr);
        
        // Get or create node
        NodeId nodeId;
        if (pUUIDNodeSet.find(ptr->getId()) == pUUIDNodeSet.end())
        {
          std::shared_ptr<Node> nodePtr = std::make_shared<Node>(ptr, generateId());
          nodeId = nodePtr->getNodeId();
          
          assert(pNodeSet.find(nodeId) == pNodeSet.end());
          pNodeSet[nodeId] = nodePtr;
          pUUIDNodeSet[ptr->getId()] = nodeId;
        }
        else
        {
          nodeId = pUUIDNodeSet[ptr->getId()];
        }
        (edgeDescriptor.second)[ptr->getId()] = nodeId;
        
        // Add the node to the edge
        edgePtr->addNode(this, nodeId);
      }//ptr
      
      // Add edge to the set of edges on the graph
      pEdgeSet[edgePtr->getEdgeId()] = edgePtr;
      edgeDescriptor.first = edgePtr->getEdgeId();
      
      return edgeDescriptor;
    }//addEdge
    
    Hypergraph::Node* Hypergraph::getNode(Hypergraph::NodeId aNodeId) const
    {
      if (pNodeSet.find(aNodeId) == pNodeSet.end())
      {
        return nullptr;
      }
      return pNodeSet.at(aNodeId).get();
    }//getNode
    
    bool Hypergraph::connectedNodesRec(Hypergraph::NodeId aNodeU, Hypergraph::NodeId aNodeV, std::unordered_set<Hypergraph::NodeId>* aVisitedNodes)
    {
      assert(pNodeSet.find(aNodeU) != pNodeSet.end());
      assert(pNodeSet.find(aNodeV) != pNodeSet.end());
      
      // Mark node as visited
      aVisitedNodes->insert(aNodeU);

      // Base case: same node it is connected to itself
      if (aNodeU == aNodeV)
      {
        return true;
      }

      // For each edge linked to aNodeU, check if
      // the edge connects also aNodeV
      for (auto edgeId : pNodeSet[aNodeU]->getIncidentEdges())
      {
        assert(pEdgeSet.find(edgeId) != pEdgeSet.end());
        
        // Check nodes linked by edge edgeId
        for (auto nodeId : pEdgeSet[edgeId]->getLinkedNodes())
        {
          // Skip current node
          if (nodeId == aNodeU)
          {
            continue;
          }
          
          // Connected node: return true
          if (nodeId == aNodeV)
          {
            return true;
          }
        }
        
        // aNodeV wasn't found in the list of adjacent nodes,
        // recursively check the next level
        for (auto nodeId : pEdgeSet[edgeId]->getLinkedNodes())
        {
          // Skip current node
          if (nodeId == aNodeU || aVisitedNodes->find(nodeId) != aVisitedNodes->end())
          {
            continue;
          }
          
          // Recursive call
          auto recVisitResult = connectedNodesRec(nodeId, aNodeV, aVisitedNodes);
          
          // Mark node as visited
          //aVisitedNodes->insert(nodeId);
          return recVisitResult;
        }
      }
      
      // No connections found: return false
      return false;
    }//connectedNodesRec
    
  }
}// end namespace DataStructure/Graph
