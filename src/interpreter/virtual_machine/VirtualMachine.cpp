// Self first
#include "VirtualMachine.hpp"

#include "CLIUtils.hpp"
#include "OPCode.hpp"
#include "CodeObjectDefs.hpp"
#include "InlineFcn.hpp"
#include "BaseObject.hpp"
#include "ObjectTools.hpp"
#include "ObjectHelper.hpp"

#include <algorithm>  // for std::swap
#include <cstdlib>    // for std::abs
#include <iterator>
#include <sstream>
#include <numeric>
#include <exception>
#include <stdlib.h>
#include <cassert>

#include <boost/optional.hpp>

#define UNUSED_ARG(x) (void)(x)

//#define INTERPRETER_DEBUG

namespace iutils = Interpreter::Utils;
namespace otools = Interpreter::ObjectTools;

namespace Interpreter {
  
  std::vector<std::pair<std::string, std::size_t>> VirtualMachine::OpFcnIdxMap = {
     { "NOT", 0 }
    ,{ "NEGATIVE", 1 }
    ,{ "POSITIVE", 2 }
    ,{ "POWER", 0 }
    ,{ "MULTIPLY", 1 }
    ,{ "DIVIDE", 2 }
    ,{ "MODULO", 3 }
    ,{ "ADD", 4 }
    ,{ "SUBTRACT", 5 }
    ,{ "AND", 6 }
    ,{ "OR", 7 }
    ,{ "XOR", 8 }
    ,{ "SUBSCR", 9 }
  };
  
  std::vector<std::string> VirtualMachine::ByteFcnIdxMap = {
      "byte_fcn_LOAD_CONST"
    , "byte_fcn_RETURN_VALUE"
    , "byte_fcn_POP_TOP"
    , "byte_fcn_DUP_TOP"
    , "byte_fcn_DUP_TOPX"
    , "byte_fcn_DUP_TOP_TWO"
    , "byte_fcn_ROT_TWO"
    , "byte_fcn_LOAD_NAME"
    , "byte_fcn_STORE_NAME"
    , "byte_fcn_DELETE_NAME"
    , "byte_fcn_LOAD_FAST"
    , "byte_fcn_STORE_FAST"
    , "byte_fcn_DELETE_FAST"
    , "byte_fcn_LOAD_GLOBAL"
    , "byte_fcn_STORE_GLOBAL"
    , "byte_fcn_DELETE_GLOBAL"
    , "byte_fcn_CALL_FUNCTION"
    , "byte_fcn_COMPARE_OP"
    , "byte_fcn_BUILD_LIST"
    , "byte_fcn_JUMP_FORWARD"
    , "byte_fcn_JUMP_ABSOLUTE"
    , "byte_fcn_POP_JUMP_IF_TRUE"
    , "byte_fcn_POP_JUMP_IF_FALSE"
    , "byte_fcn_JUMP_IF_TRUE_OR_POP"
    , "byte_fcn_JUMP_IF_FALSE_OR_POP"
    , "byte_fcn_SETUP_LOOP"
    , "byte_fcn_GET_ITER"
    , "byte_fcn_FOR_ITER"
    , "byte_fcn_BREAK_LOOP"
    , "byte_fcn_CONTINUE_LOOP"
    , "byte_fcn_POP_BLOCK"
    , "byte_fcn_BUILD_DOMAIN"
    , "byte_fcn_STORE_VAR"
    , "byte_fcn_ADD_MARK"
    , "byte_fcn_PRINT_TOP"
    , "byte_fcn_LOAD_CONST_BOOL"
    , "byte_fcn_RANGE_LIST"
    , "byte_fcn_SET_VAR_SPEC"
    , "byte_fcn_LOAD_ATTR"
    , "byte_fcn_STORE_ATTR"
    , "byte_fcn_BUILD_TUPLE"
    , "byte_fcn_STORE_SUBSCR"
  };
  
  /// Returns the data type coercion between the two given data objects
  static DataObject::DataObjectType getTypeCoercion(const DataObject& aDO1, const DataObject& aDO2)
  {
    auto dt1 = aDO1.dataObjectType();
    auto dt2 = aDO2.dataObjectType();
    
    // Void wins over any type
    if (dt1 == DataObject::DataObjectType::DOT_VOID || dt2 == DataObject::DataObjectType::DOT_VOID)
    {
      return DataObject::DataObjectType::DOT_VOID;
    }
    
    // Boolean wins over any type
    if (dt1 == DataObject::DataObjectType::DOT_BOOL || dt2 == DataObject::DataObjectType::DOT_BOOL)
    {
      return DataObject::DataObjectType::DOT_BOOL;
    }
    
    // Double wins over any type
    if (dt1 == DataObject::DataObjectType::DOT_DOUBLE || dt2 == DataObject::DataObjectType::DOT_DOUBLE)
    {
      return DataObject::DataObjectType::DOT_DOUBLE;
    }
    
    if (dt1 == DataObject::DataObjectType::DOT_INT || dt2 == DataObject::DataObjectType::DOT_INT)
    {
      return DataObject::DataObjectType::DOT_INT;
    }
    
    return dt1;
  }//getTypeCoercion
  
  // Returns true if the given DataObject represents a domain declaration.
  // false otherwise
  static bool isDomainDeclarationDataObject(const DataObject& aDO)
  {
    return aDO.isClassObject() &&
    otools::isVariableObject(aDO.getClassObject().get());
  }//isDomainDeclarationDataObject
  
  // Returns true if the given DataObject is a parameter list or a parameter matrix.
  // Returns false otherwise.
  static bool isMultiDimensionalParameterDeclarationDataObject(const DataObject& aDO)
  {
    if (isClassObjectType(aDO))
    {
      auto obj = aDO.getClassObject().get();
      return otools::isMatrixObject(obj) || otools::isListObject(obj);
    }
    return false;
  }//isMultiDimensionalParameterDeclarationDataObject
  
  // Returns true if the given DataObject is a primitive object or a scalar BaseObject
  static bool isPrimitiveOrScalarDataObject(const DataObject& aDO)
  {
    return isPrimitiveType(aDO) ||
    (isClassObjectType(aDO) && otools::isScalarObject(aDO.getClassObject().get()));
  }//isPrimitiveOrScalar
  
  // Returns the data object from a primitive object or a scalar base object.
  // Asserts on other types of DataObject
  static const DataObject& getDataFromPrimitiveOrScalarDataObject(const DataObject& aDO)
  {
    if (isPrimitiveType(aDO)) return aDO;
    else
    {
      assert(isClassObjectType(aDO));
      auto obj = aDO.getClassObject().get();
      
      assert(otools::isScalarObject(obj));
      return otools::getScalarValue(obj);
    }
  }//getDataFromPrimitiveOrScalarDataObject
  
  // Sets up the object to be sent to the model context
  static void setupNamedContextObject(const std::string& aName, DataObject& aObj)
  {
    assert(isDomainDeclarationDataObject(aObj));
    aObj.getClassObject()->lookupProperty(otools::getObjectIDPropName()) =
    DataObject(aName.c_str());
  }//setupNamedContextObject
  
  // Try to apply the subscr operator on the input element and return boost::none if
  // the elements cannot be used because not fully interpreted
  static boost::optional<DataObject> tryOpSubScr(const DataObject& aData1, const DataObject& aData2)
  {
    if(aData2.isFullyInterpreted())
    {
      itp_assert(aData2.isComposite(), InterpreterException::ExceptionType::ET_OUT_OF_BOUND);
      
      // Subscript operator can be applied on an list, matrix, or on a variable array
      if (isMultiDimensionalParameterDeclarationDataObject(aData1))
      {
        // Get the element from a parameter
        assert(!otools::isScalarParameterObject(aData1.getClassObject().get()));
        auto elem = otools::getParameterElement(aData1.getClassObject().get(), aData2);
        if(elem.isFullyInterpreted()) return elem;
        return boost::none;
      }
      else if(isDomainDeclarationDataObject(aData1))
      {
        // Get the element from a matrix
        auto varObj = aData1.getClassObject().get();
        auto subscrDomain = otools::getVarElement(varObj, aData2);
        if(!subscrDomain.isFullyInterpreted()) return boost::none;
        
        // Create a new object variable representing the subscript variable
        DataObject varSubscr;
        varSubscr.setClassObject(BaseObjectSPtr(otools::createObjectMatrixVariableSubscr(varObj, aData2)));
        return varSubscr;
      }
    }
    
    return boost::none;
  }//trySubscr
  
  VirtualMachine::VirtualMachine()
  : pFrame(nullptr)
  , pReturnValue(DataObject())
  , pExeResult(ExeRes::ER_NONE)
  , pReinterpretationModeOn(false)
  , pSkipNextInstruction(false)
  , pCallbackPrint(false)
  , pModelDeclCallback({})
  , pModelObjReportCallback({})
  {
    // Register all the vm functions
    registerFcns();
    
    // Reset exception information
    resetExceptionInfo();
  }
  
  void VirtualMachine::registerFcns()
  {
    // The following functions are ordered w.r.t. their vector-maps
    
    // Operators
    pUnaryFcnReg.push_back(std::bind(&VirtualMachine::opNot, this, std::placeholders::_1));
    pUnaryFcnReg.push_back(std::bind(&VirtualMachine::opNeg, this, std::placeholders::_1));
    pUnaryFcnReg.push_back(std::bind(&VirtualMachine::opPos, this, std::placeholders::_1));
    pBinaryFcnReg.push_back(std::bind(&VirtualMachine::opPow, this, std::placeholders::_1, std::placeholders::_2));
    pBinaryFcnReg.push_back(std::bind(&VirtualMachine::opMul, this, std::placeholders::_1, std::placeholders::_2));
    pBinaryFcnReg.push_back(std::bind(&VirtualMachine::opDiv, this, std::placeholders::_1, std::placeholders::_2));
    pBinaryFcnReg.push_back(std::bind(&VirtualMachine::opMod, this, std::placeholders::_1, std::placeholders::_2));
    pBinaryFcnReg.push_back(std::bind(&VirtualMachine::opAdd, this, std::placeholders::_1, std::placeholders::_2));
    pBinaryFcnReg.push_back(std::bind(&VirtualMachine::opSub, this, std::placeholders::_1, std::placeholders::_2));
    pBinaryFcnReg.push_back(std::bind(&VirtualMachine::opAnd, this, std::placeholders::_1, std::placeholders::_2));
    pBinaryFcnReg.push_back(std::bind(&VirtualMachine::opOr, this, std::placeholders::_1, std::placeholders::_2));
    pBinaryFcnReg.push_back(std::bind(&VirtualMachine::opXor, this, std::placeholders::_1, std::placeholders::_2));
    pBinaryFcnReg.push_back(std::bind(&VirtualMachine::opSubScr, this, std::placeholders::_1, std::placeholders::_2));
    
    // Byte functions
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_LOAD_CONST, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_RETURN_VALUE, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_POP_TOP, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_DUP_TOP, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_DUP_TOPX, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_DUP_TOP_TWO, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_ROT_TWO, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_LOAD_NAME, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_STORE_NAME, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_DELETE_NAME, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_LOAD_FAST, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_STORE_FAST, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_DELETE_FAST, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_LOAD_GLOBAL, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_STORE_GLOBAL, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_DELETE_GLOBAL, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_CALL_FUNCTION, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_COMPARE_OP, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_BUILD_LIST, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_JUMP_FORWARD, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_JUMP_ABSOLUTE, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_POP_JUMP_IF_TRUE, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_POP_JUMP_IF_FALSE, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_JUMP_IF_TRUE_OR_POP, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_JUMP_IF_FALSE_OR_POP, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_SETUP_LOOP, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_GET_ITER, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_FOR_ITER, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_BREAK_LOOP, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_CONTINUE_LOOP, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_POP_BLOCK, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_BUILD_DOMAIN, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_STORE_VAR, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_ADD_MARK, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_PRINT_TOP, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_LOAD_CONST_BOOL, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_RANGE_LIST, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_SET_VAR_SPEC, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_LOAD_ATTR, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_STORE_ATTR, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_BUILD_TUPLE, this, std::placeholders::_1));
    pByteFcnReg.push_back(std::bind(&VirtualMachine::byte_fcn_STORE_SUBSCR, this, std::placeholders::_1));
  }//registerFcns
  
  void VirtualMachine::registerCallbackForModelCtxDeclarations(std::function<void(const std::string&, const DataObject&)> aCallback)
  {
    pModelDeclCallback = aCallback;
  }//registerCallbackForModelCtxDeclarations
  
  void VirtualMachine::registerCallbackForObjectReporting(std::function<void(const DataObject&, bool)> aCallback)
  {
    pModelObjReportCallback = aCallback;
  }//registerCallbackForObjectReporting
  
  void VirtualMachine::registerInlineCallbackFcn(const std::string& aFcnName, FcnHandler aFcn,
                                                 bool aFcnObjectAsInput, bool aHasOutput)
  {
    // Create an inline function data object storing the handler
    auto fcn = new InlineFcn(aFcnName, this, aFcnObjectAsInput);
    
    // Set function as output function
    if (aHasOutput)
    {
      fcn->setAsOutputFcn();
    }
    
    // Set the callback function handler
    fcn->setCallbackFcn(aFcn);
    
    // Create a DataObject holding the function object
    DataObject doFcn(aFcnName.c_str());
    doFcn.setFcnObject(fcn);
    
    // Register the callback object into the internal register
    pCallbackReg[aFcnName] = doFcn;
  }//registerInlineCallbackFcn
  
  bool VirtualMachine::isInlineCallbackRegistered(const std::string& aFcnName) const
  {
    return pCallbackReg.find(aFcnName) != pCallbackReg.end() &&
    (pCallbackReg.at(aFcnName).getFcn().getFcnType() == DataObjectFcn::FcnType::FCN_INLINE);
  }//isInlineCallbackRegistered
  
  VirtualMachine::FcnHandler VirtualMachine::getInlineCallbackFcn(const std::string& aFcnName)
  {
    assert(isInlineCallbackRegistered(aFcnName));
    return static_cast<InlineFcn*>(&(pCallbackReg[aFcnName].getFcn()))->getCallbackFcn();
  }//getInlineCallbackFcn
  
  bool VirtualMachine::setReinterpetationMode(bool aReinterpret)
  {
    auto oldVal = pReinterpretationModeOn;
    pReinterpretationModeOn = aReinterpret;
    return oldVal;
  }//setReinterpetationMode
  
  FramePtr VirtualMachine::makeFrame(const CodeObject& aCodeObject,
                                     const Frame::NameMapSPtr& aGlbNames,
                                     const Frame::NameMapSPtr& aLocNames)
  {
    if (!pFrameStack.empty())
    {
      assert(pFrame);
      return new Frame(aCodeObject, pFrame->globalNamesPtr(), aLocNames, pFrame);
    }
    
    return new Frame(aCodeObject, aGlbNames, aLocNames, pFrame);
  }//makeFrame
  
  void VirtualMachine::pushFrame(const FrameSPtr& aFrame)
  {
    assert(aFrame);
    pFrameStack.push_back(aFrame);
    pFrame = aFrame.get();
  }//pushFrame
  
  void VirtualMachine::popFrame()
  {
    FramePtr frame{ nullptr };
    if (!pFrameStack.empty())
    {
      pFrameStack.pop_back();
      if (!pFrameStack.empty())
      {
        frame = pFrameStack.back().get();
      }
    }
    pFrame = frame;
  }//popFrame
  
  void VirtualMachine::pushBlock(Block::BlockType aType, Frame::PC_T aHandler, std::size_t aLevel)
  {
    assert(pFrame);
    Frame::PC_T handler = aHandler == 0 ? static_cast<Frame::PC_T>(getFrame().getDataStackSize()) : aHandler;
    getFrame().getBlockStack().push_back(Block(aType, handler, aLevel));
  }//pushBlock
  
  DataObject& VirtualMachine::top()
  {
    assert(pFrame && !(pFrame->getDataStack()).empty());
    return (*pFrame).getDataStack().back();
  }//top
  
  std::vector<DataObject> VirtualMachine::topn(std::size_t aNum)
  {
    assert(pFrame && !(pFrame->getDataStack()).empty());
    
    std::vector<DataObject> vlist;
    if (aNum == 0) return vlist;
    
    vlist.reserve(aNum);
    auto& stack = (*pFrame).getDataStack();
    aNum = aNum > stack.size() ? stack.size() : aNum;
    
    vlist.assign(vlist.end() - aNum, vlist.end());
    return vlist;
  }//topn
  
  DataObject VirtualMachine::pop()
  {
    auto obj = top();
    (*pFrame).getDataStack().pop_back();
    return obj;
  }//pop
  
  std::vector<DataObject> VirtualMachine::popn(std::size_t aNum)
  {
    assert(pFrame);
    
    std::vector<DataObject> vlist;
    if (aNum == 0) return vlist;
    
    try
    {
      vlist.reserve(aNum);
    }
    catch (...)
    {
      itp_assert(false, InterpreterException::ExceptionType::ET_BAD_ALLOC);
    }
    
    auto& stack = (*pFrame).getDataStack();
    aNum = aNum > stack.size() ? stack.size() : aNum;
    
    vlist.assign(stack.end() - aNum, stack.end());
    stack.resize(stack.size() - aNum);
    
    return vlist;
  }//popn
  
  void VirtualMachine::push(const DataObject& aDO)
  {
    assert(pFrame);
    /*
    // The given DataObject may be a scalar parameter, i.e., a wrapper around an actual
    // primitive DataObject type.
    // If that is the case, unwrap it from the class object and push the raw DataObject on the stack
    if (isClassObjectType(aDO) && otools::isScalarParameterObject(aDO.getClassObject().get()))
    {
      // Push the raw DataObject onto the stack
      (*pFrame).getDataStack().push_back(otools::getScalarParameterValue(aDO.getClassObject().get()));
    }
    else
    {
      (*pFrame).getDataStack().push_back(aDO);
    }*/
    
    // Update 11/7/2018
    // Push all DataObjects as they are on the stack.
    // Let the client code to handle types properly
    (*pFrame).getDataStack().push_back(aDO);
  }//push
  
  void VirtualMachine::push(const std::vector<DataObject>& aDS)
  {
    assert(pFrame);
    DataObject DObj;
    DObj.compose(aDS);
    (*pFrame).getDataStack().push_back(DObj);
  }//push
  
  std::string VirtualMachine::getNextByteCodeInstruction() const
  {
    assert(pFrame);
    auto PC = pFrame->getPC();
    auto byteCode = iutils::byteToInt(pFrame->getByteFromCode(PC));
    
    if (byteCode == EXCEPTION_CODE) return "";
    return CO_OPName[byteCode];
  }//getNextByteCodeInstruction
  
  VirtualMachine::ByteCodeInstr VirtualMachine::parseBytes()
  {
    assert(pFrame);
    auto PC = pFrame->getPC();
    auto byteCode = iutils::byteToInt(pFrame->getByteFromCode(PC));
    
    // Return if the code is a valid code
    if (byteCode == EXCEPTION_CODE) return { "", DataObject() };
    
    pFrame->advancePC();
    
    assert(CO_OPName.find(byteCode) != CO_OPName.end());
    auto byteName = CO_OPName[byteCode];
    
    // Calculate the arguments, if any
    if (byteCode >= CO_OPHaveArguments)
    {
      // Get the 2 bytes describing the arguments
      auto byte1 = pFrame->getByteFromCode(PC + 1);
      auto byte2 = pFrame->getByteFromCode(PC + 2);
      
      // Calculate the 2 byte argument integer value
      auto arg = iutils::byteToInt(byte1) + (iutils::byteToInt(byte2) << 8);
      
      // Advance the program counter of 2 since the
      // last 2 bytes where the 2 arguments
      pFrame->advancePC(2);
      
      // Switch w.r.t. the type of argument
      if (std::find(CO_OPConstArg.begin(), CO_OPConstArg.end(), byteCode) != CO_OPConstArg.end())
      {
         assert(arg < (pFrame->getCodeObject()).COConstants.size());
        return { byteName, (pFrame->getCodeObject()).COConstants.at(arg) };
      }
      else if (std::find(CO_OPGlbArg.begin(), CO_OPGlbArg.end(), byteCode) != CO_OPGlbArg.end())
      {
        assert(arg < (pFrame->getCodeObject()).COGlbNames.size());
        return { byteName, (pFrame->getCodeObject()).COGlbNames.at(arg) };
      }
      else if (std::find(CO_OPLocArg.begin(), CO_OPLocArg.end(), byteCode) != CO_OPLocArg.end())
      {
        assert(arg < (pFrame->getCodeObject()).COLocNames.size());
        return { byteName, (pFrame->getCodeObject()).COLocNames.at(arg) };
      }
      else if (std::find(CO_OPHasComp.begin(), CO_OPHasComp.end(), byteCode) != CO_OPHasComp.end())
      {
        return { byteName, DataObject(static_cast<Frame::PC_T>(arg)) };
      }
      else if (std::find(CO_OPRelJmp.begin(), CO_OPRelJmp.end(), byteCode) != CO_OPRelJmp.end())
      {
        return { byteName, DataObject(PC + static_cast<Frame::PC_T>(arg)) };
      }
      else if (std::find(CO_OPAbsJmp.begin(), CO_OPAbsJmp.end(), byteCode) != CO_OPAbsJmp.end())
      {
        return { byteName, DataObject(static_cast<Frame::PC_T>(arg)) };
      }
      else
      {
        // Everything else return the "integer" argument as is
        return { byteName, DataObject(static_cast<int>(arg)) };
      }
    }
    
    return { byteName, DataObject() };
  }//parseBytes
  
  DataObject VirtualMachine::runFrame(const FrameSPtr& aFrame)
  {
    assert(aFrame);
    
    // Push given frame onto the stack
    pushFrame(aFrame);
    
    // Loop until there is something to do
    // on the current frame
    while (true)
    {
      // Get next bytecode instruction
      const auto& byteCode = parseBytes();
      if (byteCode.first.empty()) break;
      
#ifdef INTERPRETER_DEBUG
      std::cout << byteCode.first << std::endl;
#endif
      
      // Dispatch to the right method
      auto er = dispatch(byteCode.first, byteCode.second);
      assert(er != ExeRes::ER_EXCEPTION);
      
      if (er == ExeRes::ER_CONTROLLED_EXCEPTION) break;
      if (pFrame)
      {
        while (er != ExeRes::ER_NONE && pFrame->getBlockStackSize() > 0)
        {
          er = manageBlockStack(er);
        }
      }
    }//while
    
    // Reset skip instruction flag
    pSkipNextInstruction = false;
    
    // Pop current frame since its computation is done
    popFrame();
    
    return pReturnValue;
  }//runFrame
  
  VirtualMachine::ExeRes VirtualMachine::dispatch(const std::string& aByteName, const DataObject& aArg)
  {
    VirtualMachine::ExeRes res = ExeRes::ER_NONE;
    
    // Skip this instruction if flag is set
    if(pSkipNextInstruction)
    {
      pSkipNextInstruction = false;
      return res;
    }
    
    try
    {
      if (aByteName.substr(0, 6) == "UNARY_")
      {
        unaryOperator(aByteName.substr(6));
      }
      else if (aByteName.substr(0, 7) == "BINARY_")
      {
        binaryOperator(aByteName.substr(7));
      }
      else if (aByteName.substr(0, 8) == "INPLACE_")
      {
        inplaceOperator(aByteName.substr(8));
      }
      else
      {
        getByteFcn("byte_fcn_" + aByteName)(aArg);
        res = pExeResult;
      }
    }
    catch (InterpreterException& ie)
    {
      // Store exception information
      setExceptionInfo(ie);
      res = ExeRes::ER_CONTROLLED_EXCEPTION;
    }
    catch (...)
    {
      res = ExeRes::ER_EXCEPTION;
    }
    
    return res;
  }//dispatch
  
  VirtualMachine::ExeRes VirtualMachine::manageBlockStack(ExeRes aException)
  {
    assert(pFrame);
    auto block = (pFrame->getBlockStack()).back();
    
    assert(block.btype == Block::BlockType::BT_LOOP);
    if (aException == ExeRes::ER_CONTINUE)
    {
      // Set the PC to the instruction to jump to and return no exception
      jump(pReturnValue.getDataValue<Frame::PC_T>());
      return ExeRes::ER_NONE;
    }
    
    // The exception is not a continue exception:
    // remove the block from the stack, unwind all variables
    // and handle the exception
    (pFrame->getBlockStack()).pop_back();
    unwindBlock(block);
    
    // The only other possible case is a BREAK exception.
    // Set the PC to jump to the handler of the block and return no exception
    assert(aException == ExeRes::ER_BREAK);
    jump(block.bhandler);
    return ExeRes::ER_NONE;
  }//manageBlockStack
  
  void VirtualMachine::unwindBlock(Block& aBlock)
  {
    while (pFrame->getDataStackSize() > aBlock.blevel) pop();
  }//unwindBlock
  
  VirtualMachine::VMEnv VirtualMachine::runCode(const CodeObject& aCode, const VMEnv& aVMEnv, bool aReinterpretationCode)
  {
    // Create a new frame on the given environment
    auto framePtr = makeFrame(aCode, aVMEnv.first, aVMEnv.second);
    auto frame = FrameSPtr(framePtr);
    auto frameStackSize = pFrameStack.size();
    
    auto oldReinterpMode = setReinterpetationMode(aReinterpretationCode);
    
    // Reset exception information
    resetExceptionInfo();
    
    // Run the frame
    runFrame(frame);
    
    /*
     * Check that either the code was not a reinterpreted code and the frame stack is empty
     * or, if reinterpreted, the frame stack size has the size it had before reinterpretation.
     * Previously, this check was performed as follow:
     * if ((!isReinterpretationModeOn() && !pFrameStack.empty()) ||
     *   (isReinterpretationModeOn() && pFrameStack.size() != frameStackSize))
     * However this does not consider codes that call recursively the virtual machine again,
     * for example, the inline function "run" which run bytecode while interpreting the
     * run function itself.
     * Hence, the check should only be on the frame stack size being equal to the
     * one on entering the virtual machine.
     */
    if (pFrameStack.size() != frameStackSize)
    {
      setReinterpetationMode(oldReinterpMode);
      throw std::logic_error("Frame stack not empty");
    }
    
    if (frame && frame->getDataStackSize() > 0)
    {
      setReinterpetationMode(oldReinterpMode);
      throw std::logic_error("Data stack not empty");
    }
    
    // Reset the interpretation mode as it was before running the code
    setReinterpetationMode(oldReinterpMode);
    
    // Return the result DataObject and the interpreted environment
    // which is stored on the frame that started the computation
    return { frame->globalNamesPtr(), frame->localNamesPtr() };
  }//runCode
  
  void VirtualMachine::clearState()
  {
    if (!pFrame) return;
    pFrame->globalNamesPtr()->clear();
    pFrame->localNamesPtr()->clear();
  }//clearState
  
  void VirtualMachine::setExceptionInfo(InterpreterException& aIE)
  {
    pControllerExceptionType = aIE.getType();
    pControllerExceptionValue = std::string(aIE.what());
  }//setExceptionInfo
  
  void VirtualMachine::resetExceptionInfo()
  {
    pControllerExceptionType = InterpreterException::ExceptionType::ET_UNSPEC;
    pControllerExceptionValue.clear();
  }//resetExceptionInfo
  
  void VirtualMachine::addMarkToPCStack(int aOffset)
  {
    auto markedPC = static_cast<int>(pFrame->getPC()) + aOffset;
    //std::min<int>(0, static_cast<int>(pFrame->getPC()) + aOffset);
    pPCStack.push_back(static_cast<Frame::PC_T>(markedPC >= 0 ? markedPC : 0));
  }//addMarkToPCStack
  
  CodeObject* VirtualMachine::getMarkedCodeObject(Frame::PC_T aBegin, Frame::PC_T aEnd)
  {
    assert(aEnd <= getFrame().getPC() && aBegin < aEnd);
    
    // Create a copy of the current code object and environment
    const auto& frame = getFrame();
    
    // Create a new code image copy of the code object and storing the current environment
    auto code = new CodeObjectImage(frame.getCodeObject(), frame.getGlobalNames(),
                                    frame.getLocalNames());
    
    // Copy the bytecode [aBeing, aEnd)
    code->COCode = { getFrame().getCodeObject().COCode.begin() + aBegin,
      getFrame().getCodeObject().COCode.begin() + aEnd };
    
    return code;
  }//getMarkedCodeObject
  
  void VirtualMachine::storeProperty(const DataObject& aObj, const std::string& aPropKey,
                                     const DataObject& aPropValue)
  {
    // Property name should not be empty
    itp_assert_type_msg(!aPropKey.empty(),
                        InterpreterException::ExceptionType::ET_PROP_INVALID_KEY, aPropKey);
    assert(aObj.getClassObject().get());
    auto obj = aObj.getClassObject().get();
    
    // Check if the property exists, if not add it here and return
    if (!obj->hasProperty(aPropKey))
    {
      obj->addProperty(aPropKey, aPropValue);
      return;
    }
    
    // Otherwise check if the property is const, modify only if it is not
    itp_assert_type_msg(!obj->isConstProperty(aPropKey),
                        InterpreterException::ExceptionType::ET_PROP_CONST, aPropKey);
    
    // Check the type of the property and set it here
    obj->setProperty(aPropKey, aPropValue);
  }//storeProperty
  
  void VirtualMachine::storeName(const std::string& aName, const DataObject& aObj)
  {
    // Class objects must be "enhanced" with more information (e.g., bytecode for re-interpretation)
    // therefore mapping must be done on a copy of the original object
    DataObject copyObj;
    if (isPrimitiveType(aObj))
    {
      // Primitive type objects must be wrapped into an object class
      UNUSED_ARG(aName);
      copyObj.setClassObject(BaseObjectSPtr( otools::createScalarObject(aObj) ));
    }
    else
    {
      // Copy the original object since the copy is used to add extra information into it
      copyObj = aObj;
    }
    
    if (copyObj.isClassObject())
    {
      mapNameClassObjectIntoModelContext(aName, copyObj);
    }
    
    // Map the data into the local names of the current frame
    getFrame().storeLocalName(aName, copyObj);
  }//storeName
  
  void VirtualMachine::mapNameClassObjectIntoModelContext(const std::string& aName,
                                                          DataObject& aObj)
  {
    // Check that the callback is registered and there is at least one mark
    // in the bytecode corresponding to this name
    assert(aObj.isClassObject());
    assert(pModelDeclCallback);
    
    // Setting the name context object here is not the right way to link context objects
    // to objects in the virtual machine.
    // The following if statement should be deprecated and setting the link with the
    // name should happen in one place, i.e., using the MVCModelContextBaseObjectKey key.
    // Update 11/9/18: changed from isNamedContextObject(aObj) to
    //                 isDomainDeclarationDataObject(aObj). Only domain declaration objects will be
    //                 given a name here instead of all the parameters.
    //                 Eventually, also the domain declaration objects will be removed from here.
    if (isDomainDeclarationDataObject(aObj))
    {
      try
      {
        setupNamedContextObject(aName, aObj);
      }
      catch (std::logic_error& e)
      {
        itp_assert_type_msg(false, InterpreterException::ExceptionType::ET_LOGIC_ERROR, e.what());
      }
    }

    // If the object is not fully interpreted, store its bytecode for later re-interpretation
    /*
    if (!aObj.isFullyInterpreted())
    {
      assert(!pPCStack.empty());
      
      auto co = getMarkedCodeObject(popPCStack(), getFrame().getPC());
      aObj.setByteCode(std::shared_ptr<CodeObject>(co));
    }
    */
    
    // The object must be mapped into the model context.
    // Call the correspondent callback function
    try
    {
      pModelDeclCallback(aName, aObj);
    }
    catch (...)
    {
      itp_assert(false, InterpreterException::ExceptionType::ET_FCN_ERR);
    }
  }//mapNameClassObjectIntoModelContext
  
  void VirtualMachine::callFunction(const DataObject& aDO)
  {
    auto divRes = std::div(aDO.getDataValue<int>(), 256);
    auto args = popn(divRes.rem);
    auto fcn = pop();
    
    // The object representing the function mush be a fcn object,
    // it must be fully interpreted and there should be at least one mark
    // in the bytecode corresponding to this function call
    // assert(!pPCStack.empty());
    auto fcnName = fcn.getDataValue<std::string>();
    
    //auto markedPC = popPCStack();
    auto markedPC = 0;
    
#ifdef INTERPRETER_DEBUG
    std::cout << "\tCALL " << fcn.getDataValue<std::string>() << std::endl;
#endif
    
    // Check first if the fcnName is a registered function, if not load name pushed
    // a not fully interpreted object on the stack
    itp_assert_type_msg(pCallbackReg.find(fcnName) != pCallbackReg.end(), InterpreterException::ExceptionType::ET_FCN_UNDEF, fcnName);
    itp_assert(fcn.isFullyInterpreted(), InterpreterException::ExceptionType::ET_FCN_UNDEF);
    assert(fcn.isFcnObject());
    
    // Check if the fcn object represents an inline function, if so prepare the input
    // arguments and call the inline function otherwise call the intrinsic function as is
    if (fcn.getFcn().getFcnType() == DataObjectFcn::FcnType::FCN_INLINE)
    {
      // Create a copy of the function object and set it as the argument
      // of the inline function callback
      auto fcnCopy = fcn;
      
      // Set the fcn arguments
      fcnCopy.compose(args);
      
      // Check if all the arguments are fully interpreted,
      // if not set the (copy of the) fcn as not fully interpreted
      for (const auto& arg : fcnCopy)
      {
        if (!arg.isFullyInterpreted())
        {
          fcnCopy.setFullyInterpretedTag(false);
          break;
        }
      }
      
      // If the object is not fully interpreted, add the byte code into it
      // to let the vm interpret it again at a later time
      if (!fcnCopy.isFullyInterpreted())
      {
        // Set re-interpretation off
        itp_assert_type_msg(false, InterpreterException::ExceptionType::ET_INVALID_INTERP, fcnName);
        
        // Set bytecode here: current PC - marked PC
        auto co = getMarkedCodeObject(markedPC, getFrame().getPC());
        fcnCopy.setByteCode(std::shared_ptr<CodeObject>(co));
      }
      
      // Run the inline fcn
      // @note the result of the inline function is not handled by
      // the virtual machine
      const auto& resCall = fcn({ fcnCopy });
      assert(resCall.size() == 2);
      assert(resCall[0].dataObjectType() == DataObject::DataObjectType::DOT_INT);
      itp_assert_type_msg(resCall[0].getDataValue<int>() == EXIT_SUCCESS,
                          InterpreterException::ExceptionType::ET_FCN_ERR, fcnName);
      
      // If function returns a value, push value here //
      if (fcn.getFcn().hasOutput())
      {
        push(resCall[1]);
        
        // If this byte_code is followed by a print_top instruction, and the
        // verbose option is NONE skip it
        if(getNextByteCodeInstruction() == "PRINT_TOP")
        {
          setCallbackPrintTop();
        }
      }
      else
      {
        // Set an empty data object on top of the stack.
        // If function returns a value, push a void object after its result
        // to get the value out from pop_top.
        // @note in reinterpreted mode there is no first push since
        // the reinterpret code is set without the pop_top bytecode
        if (!isReinterpretationModeOn())
        {
          push(DataObject());
        }
        
        // If the function does not have an output and this byte_code is followed by
        // a print_top instruction, skip it.
        // This avoids to print empty objects
        if(getNextByteCodeInstruction() == "PRINT_TOP")
        {
          skipNextInstruction();
        }
      }
    }
    else
    {
      // @TODO
      // Intrinsic functions not supported yet
      assert(false);
      
      // Fake push on top of the stack.
      // If function returns a value, push a void object after its result
      // to get the value out from pop_top
      push(DataObject(1));
      // If function returns a value, push value here //
      
      // If this byte_code is followed by a print_top instruction, and the
      // verbose option is NONE skip it
      if(getNextByteCodeInstruction() == "PRINT_TOP")
      {
        setCallbackPrintTop();
      }
    }
  }//callFunction
  
  VirtualMachine::ByteFcn& VirtualMachine::getByteFcn(const std::string& aByteFcnCode)
  {
    std::size_t ctr{ 0 };
    for (const auto& fcnCode : ByteFcnIdxMap)
    {
      if (fcnCode == aByteFcnCode) return pByteFcnReg[ctr];
      ctr++;
    }
    assert(false);
    
    return pByteFcnReg[0];
  }//getByteFcn
  
  std::size_t VirtualMachine::getFunctionEntryKey(const std::string& aOP)
  {
    for (const auto& idIdx : OpFcnIdxMap)
    {
      if (idIdx.first == aOP) return idIdx.second;
    }
    // The entry for "aOP" must be registered
    assert(false);
    
    return 0;
  }//getFunctionEntryKey
  
  void VirtualMachine::unaryOperator(const std::string& aOP)
  {
    auto fcnKey = getFunctionEntryKey(aOP);
    itp_assert_type_msg(top().isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP, top().getDataValue<std::string>());
    push(pUnaryFcnReg[fcnKey](pop()));
  }//unaryOperator
  
  void VirtualMachine::binaryOperator(const std::string& aOP)
  {
    auto fcnKey = getFunctionEntryKey(aOP);
    assert(pFrame->getDataStack().size() > 1);
    
    auto top2 = popn(2);
    push(pBinaryFcnReg[fcnKey](top2[0], top2[1]));
  }//binaryOperator
  
  void VirtualMachine::inplaceOperator(const std::string& aOP)
  {
    auto inData = popn(2);
    itp_assert(inData[0].isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    itp_assert(inData[1].isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    
    if (aOP == "POWER")
    {
      push(opPow(inData[0], inData[1]));
    }
    else if (aOP == "MULTIPLY")
    {
      push(opMul(inData[0], inData[1]));
    }
    else if (aOP == "DIVIDE")
    {
      push(opDiv(inData[0], inData[1]));
    }
    else if (aOP == "MODULO")
    {
      push(opMod(inData[0], inData[1]));
    }
    else if (aOP == "ADD")
    {
      push(opAdd(inData[0], inData[1]));
    }
    else if (aOP == "SUBTRACT")
    {
      push(opSub(inData[0], inData[1]));
    }
    else if (aOP == "AND")
    {
      push(opAnd(inData[0], inData[1]));
    }
    else if (aOP == "OR")
    {
      push(opOr(inData[0], inData[1]));
    }
    else if (aOP == "XOR")
    {
      push(opXor(inData[0], inData[1]));
    }
  }//inplaceOperator
  
  /********************************************************
   *                                                      *
   *                    Unary Operators                   *
   *                                                      *
   ********************************************************/
  
  DataObject VirtualMachine::opNeg(const DataObject& aData)
  {
    itp_assert(isPrimitiveOrScalarDataObject(aData),
               InterpreterException::ExceptionType::ET_WRONG_TYPE);
    itp_assert(aData.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    
    const DataObject& data = getDataFromPrimitiveOrScalarDataObject(aData);
    return DataObject(data->op_neg(), data.dataObjectType());
  }//opNeg
  
  DataObject VirtualMachine::opPos(const DataObject& aData)
  {
    itp_assert(isPrimitiveOrScalarDataObject(aData),
               InterpreterException::ExceptionType::ET_WRONG_TYPE);
    itp_assert(aData.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    return getDataFromPrimitiveOrScalarDataObject(aData);
  }//opPos
  
  DataObject VirtualMachine::opNot(const DataObject& aData)
  {
    itp_assert(isPrimitiveOrScalarDataObject(aData),
               InterpreterException::ExceptionType::ET_WRONG_TYPE);
    itp_assert(aData.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    return DataObject(getDataFromPrimitiveOrScalarDataObject(aData)->op_not());
  }//opNot
  
  /********************************************************
   *                                                      *
   *                    Binary Operators                  *
   *                                                      *
   ********************************************************/
  
  DataObject VirtualMachine::opPow(const DataObject& aData1, const DataObject& aData2)
  {
    itp_assert(isPrimitiveOrScalarDataObject(aData1),
               InterpreterException::ExceptionType::ET_WRONG_TYPE);
    itp_assert(isPrimitiveOrScalarDataObject(aData2),
               InterpreterException::ExceptionType::ET_WRONG_TYPE);
    itp_assert(aData1.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    itp_assert(aData2.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    
    const DataObject& data1 = getDataFromPrimitiveOrScalarDataObject(aData1);
    const DataObject& data2 = getDataFromPrimitiveOrScalarDataObject(aData2);
    auto typeCoercion = getTypeCoercion(data1, data2);
    try
    {
      return DataObject(data1->op_pow(data2.getData()), typeCoercion);
    }
    catch (std::logic_error& e)
    {
      itp_assert_msg(false, e.what());
    }
  }//opPow
  
  DataObject VirtualMachine::opMul(const DataObject& aData1, const DataObject& aData2)
  {
    itp_assert(isPrimitiveOrScalarDataObject(aData1),
               InterpreterException::ExceptionType::ET_WRONG_TYPE);
    itp_assert(isPrimitiveOrScalarDataObject(aData2),
               InterpreterException::ExceptionType::ET_WRONG_TYPE);
    itp_assert(aData1.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    itp_assert(aData2.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    
    const DataObject& data1 = getDataFromPrimitiveOrScalarDataObject(aData1);
    const DataObject& data2 = getDataFromPrimitiveOrScalarDataObject(aData2);
    auto typeCoercion = getTypeCoercion(data1, data2);
    try
    {
      return DataObject(data1->op_mul(data2.getData()), typeCoercion);
    }
    catch (std::logic_error& e)
    {
      itp_assert_msg(false, e.what());
    }
  }//opMul
  
  DataObject VirtualMachine::opDiv(const DataObject& aData1, const DataObject& aData2)
  {
    itp_assert(isPrimitiveOrScalarDataObject(aData1), InterpreterException::ExceptionType::ET_WRONG_TYPE);
    itp_assert(isPrimitiveOrScalarDataObject(aData2), InterpreterException::ExceptionType::ET_WRONG_TYPE);
    itp_assert(aData1.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    itp_assert(aData2.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    
    const DataObject& data1 = getDataFromPrimitiveOrScalarDataObject(aData1);
    const DataObject& data2 = getDataFromPrimitiveOrScalarDataObject(aData2);
    auto typeCoercion = getTypeCoercion(data1, data2);
    try
    {
      return DataObject(data1->op_div(data2.getData()), typeCoercion);
    }
    catch (std::logic_error& e)
    {
      itp_assert_msg(false, e.what());
    }
  }//opDiv
  
  DataObject VirtualMachine::opMod(const DataObject& aData1, const DataObject& aData2)
  {
    itp_assert(isPrimitiveOrScalarDataObject(aData1), InterpreterException::ExceptionType::ET_WRONG_TYPE);
    itp_assert(isPrimitiveOrScalarDataObject(aData2), InterpreterException::ExceptionType::ET_WRONG_TYPE);
    itp_assert(aData1.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    itp_assert(aData2.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    
    const DataObject& data1 = getDataFromPrimitiveOrScalarDataObject(aData1);
    const DataObject& data2 = getDataFromPrimitiveOrScalarDataObject(aData2);
    auto typeCoercion = getTypeCoercion(data1, data2);
    try
    {
      return DataObject(data1->op_mod(data2.getData()), typeCoercion);
    }
    catch (std::logic_error& e)
    {
      itp_assert_msg(false, e.what());
    }
  }//opMod
  
  DataObject VirtualMachine::opAdd(const DataObject& aData1, const DataObject& aData2)
  {
    // Operators on functions are not supported
    itp_assert(!isFunctionType(aData1) && !isFunctionType(aData2),
               InterpreterException::ExceptionType::ET_WRONG_TYPE);
    
    if (isClassObjectType(aData1) || isClassObjectType(aData2))
    {
      return iutils::opAddOnClassObjects(aData1, aData2);
    }
    
    if (isCompositeType(aData1) || isClassObjectType(aData2))
    {
      return iutils::opAddOnCompositeObjects(aData1, aData2);
    }
    
    // All other objects must be primitive and use their native operators
    itp_assert(isPrimitiveOrScalarDataObject(aData1),
               InterpreterException::ExceptionType::ET_WRONG_TYPE);
    itp_assert(isPrimitiveOrScalarDataObject(aData2),
               InterpreterException::ExceptionType::ET_WRONG_TYPE);
    itp_assert(aData1.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    itp_assert(aData2.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    
    const DataObject& data1 = getDataFromPrimitiveOrScalarDataObject(aData1);
    const DataObject& data2 = getDataFromPrimitiveOrScalarDataObject(aData2);
    auto typeCoercion = getTypeCoercion(data1, data2);
    try
    {
      return DataObject(data1->op_add(data2.getData()), typeCoercion);
    }
    catch (std::logic_error& e)
    {
      itp_assert_msg(false, e.what());
    }
  }//opAdd
  
  DataObject VirtualMachine::opSub(const DataObject& aData1, const DataObject& aData2)
  {
    itp_assert(isPrimitiveOrScalarDataObject(aData1),
               InterpreterException::ExceptionType::ET_WRONG_TYPE);
    itp_assert(isPrimitiveOrScalarDataObject(aData2),
               InterpreterException::ExceptionType::ET_WRONG_TYPE);
    itp_assert(aData1.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    itp_assert(aData2.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    
    const DataObject& data1 = getDataFromPrimitiveOrScalarDataObject(aData1);
    const DataObject& data2 = getDataFromPrimitiveOrScalarDataObject(aData2);
    auto typeCoercion = getTypeCoercion(data1, data2);
    try
    {
      return DataObject(data1->op_sub(data2.getData()), typeCoercion);
    }
    catch (std::logic_error& e)
    {
      itp_assert_msg(false, e.what());
    }
  }//opSub
  
  DataObject VirtualMachine::opAnd(const DataObject& aData1, const DataObject& aData2)
  {
    itp_assert(isPrimitiveOrScalarDataObject(aData1),
               InterpreterException::ExceptionType::ET_WRONG_TYPE);
    itp_assert(isPrimitiveOrScalarDataObject(aData2),
               InterpreterException::ExceptionType::ET_WRONG_TYPE);
    itp_assert(aData1.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    itp_assert(aData2.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    
    const DataObject& data1 = getDataFromPrimitiveOrScalarDataObject(aData1);
    const DataObject& data2 = getDataFromPrimitiveOrScalarDataObject(aData2);
    auto typeCoercion = getTypeCoercion(data1, data2);
    try
    {
      return DataObject(data1->op_and(data2.getData()), typeCoercion);
    }
    catch (std::logic_error& e)
    {
      itp_assert_msg(false, e.what());
    }
  }//opAnd
  
  DataObject VirtualMachine::opOr(const DataObject& aData1, const DataObject& aData2)
  {
    itp_assert(isPrimitiveOrScalarDataObject(aData1),
               InterpreterException::ExceptionType::ET_WRONG_TYPE);
    itp_assert(isPrimitiveOrScalarDataObject(aData2),
               InterpreterException::ExceptionType::ET_WRONG_TYPE);
    itp_assert(aData1.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    itp_assert(aData2.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    
    const DataObject& data1 = getDataFromPrimitiveOrScalarDataObject(aData1);
    const DataObject& data2 = getDataFromPrimitiveOrScalarDataObject(aData2);
    auto typeCoercion = getTypeCoercion(data1, data2);
    try
    {
      return DataObject(data1->op_or(data2.getData()), typeCoercion);
    }
    catch (std::logic_error& e)
    {
      itp_assert_msg(false, e.what());
    }
  }//opOr
  
  DataObject VirtualMachine::opXor(const DataObject& aData1, const DataObject& aData2)
  {
    itp_assert(isPrimitiveOrScalarDataObject(aData1),
               InterpreterException::ExceptionType::ET_WRONG_TYPE);
    itp_assert(isPrimitiveOrScalarDataObject(aData2),
               InterpreterException::ExceptionType::ET_WRONG_TYPE);
    itp_assert(aData1.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    itp_assert(aData2.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    
    const DataObject& data1 = getDataFromPrimitiveOrScalarDataObject(aData1);
    const DataObject& data2 = getDataFromPrimitiveOrScalarDataObject(aData2);
    auto typeCoercion = getTypeCoercion(data1, data2);
    try
    {
      return DataObject(data1->op_xor(data2.getData()), typeCoercion);
    }
    catch (std::logic_error& e)
    {
      itp_assert_msg(false, e.what());
    }
  }//opXor

  DataObject VirtualMachine::opSubScr(const DataObject& aData1, const DataObject& aData2)
  {
    // For subscript operators the correspondent bytecode should be
    // LOAD_NAME
    // LOAD_CONST
    // which sets the stack with the const at the top.
    // Note that "aData2" should be a composite object representing the tuple of indices.
    // This should come from binaryOperator, meaning that "aData1" should be
    // the array to subscript in or the object to lookup, and "aData2" should be the index
    // or the property to query.
    // @note the index is represented as a tuple, i.e., a composite object
    
    if (auto elem = tryOpSubScr(aData1, aData2))
    {
      return *elem;
    }
    
    // Handle the case where the object is not fully interpreted
    std::string doRep;
    
    // Set identifier for the object
    if (aData1.dataObjectType() == DataObject::DataObjectType::DOT_STRING)
    {
      doRep = aData1.getDataValue<std::string>();
    }
    else
    {
      assert(aData1.isClassObject());
      doRep = otools::getObjectPropertyValue<std::string>(aData1.getClassObject().get(),
                                                          otools::getObjectIDPropName());
    }
    
    // Set identifier for the indices
    if (!aData2.isFullyInterpreted())
    {
      assert(aData2.dataObjectType() == DataObject::DataObjectType::DOT_STRING);
      doRep += "[" + aData2.getDataValue<std::string>() + "]";
    }
    else
    {
      // The object is not fully interpreted
      itp_assert(aData2.isComposite(), InterpreterException::ExceptionType::ET_OUT_OF_BOUND);
      std::stringstream ss;
      for (std::size_t idx{ 0 }; idx < aData2.composeSize(); ++idx)
      {
        itp_assert(aData2[idx].dataObjectType() == DataObject::DataObjectType::DOT_INT,
                   InterpreterException::ExceptionType::ET_BAD_SUBSCR);
        ss << aData2[idx].getDataValue<int>();
        if (idx < aData2.composeSize() - 1) ss << ", ";
      }
      doRep += "[" + ss.str() + "]";
    }
    
    DataObject doToInterp(doRep.c_str());
    doToInterp.setFullyInterpretedTag(false);
    return doToInterp;
  }//opSubScr
  
  DataObject VirtualMachine::opLT(const DataObject& aData1, const DataObject& aData2)
  {
    itp_assert(aData1.isFullyInterpreted() && aData2.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    try
    {
      return DataObject(aData1->op_lt(aData2.getData()), DataObject::DataObjectType::DOT_BOOL);
    }
    catch (std::logic_error& e)
    {
      itp_assert_msg(false, e.what());
    }
  }//opLT
  
  DataObject VirtualMachine::opLE(const DataObject& aData1, const DataObject& aData2)
  {
    itp_assert(aData1.isFullyInterpreted() && aData2.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    try
    {
      return DataObject(aData1->op_le(aData2.getData()), DataObject::DataObjectType::DOT_BOOL);
    }
    catch (std::logic_error& e)
    {
      itp_assert_msg(false, e.what());
    }
  }//opLE
  
  DataObject VirtualMachine::opEQ(const DataObject& aData1, const DataObject& aData2)
  {
    itp_assert(aData1.isFullyInterpreted() && aData2.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    try
    {
      return DataObject(aData1->op_eq(aData2.getData()), DataObject::DataObjectType::DOT_BOOL);
    }
    catch (std::logic_error& e)
    {
      itp_assert_msg(false, e.what());
    }
  }//opEQ
  
  DataObject VirtualMachine::opNE(const DataObject& aData1, const DataObject& aData2)
  {
    itp_assert(aData1.isFullyInterpreted() && aData2.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    try
    {
      return DataObject(aData1->op_ne(aData2.getData()), DataObject::DataObjectType::DOT_BOOL);
    }
    catch (std::logic_error& e)
    {
      itp_assert_msg(false, e.what());
    }
  }//opNE
  
  DataObject VirtualMachine::opGT(const DataObject& aData1, const DataObject& aData2)
  {
    itp_assert(aData1.isFullyInterpreted() && aData2.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    try
    {
      return DataObject(aData1->op_gt(aData2.getData()), DataObject::DataObjectType::DOT_BOOL);
    }
    catch (std::logic_error& e)
    {
      itp_assert_msg(false, e.what());
    }
  }//opGT
  
  DataObject VirtualMachine::opGE(const DataObject& aData1, const DataObject& aData2)
  {
    itp_assert(aData1.isFullyInterpreted() && aData2.isFullyInterpreted(), InterpreterException::ExceptionType::ET_NON_INTERP);
    try
    {
      return DataObject(aData1->op_ge(aData2.getData()), DataObject::DataObjectType::DOT_BOOL);
    }
    catch (std::logic_error& e)
    {
      itp_assert_msg(false, e.what());
    }
  }//opGE
  
  void VirtualMachine::byte_fcn_COMPARE_OP(const DataObject& aDO)
  {
    assert(aDO.dataObjectType() == DataObject::DataObjectType::DOT_SIZE_T);
    auto op = aDO.getDataValue<std::size_t>();
    auto data = popn(2);
    if (op == OP_LT) { push(opLT(data[0], data[1])); }
    else if (op == OP_LE) { push(opLE(data[0], data[1])); }
    else if (op == OP_EQ) { push(opEQ(data[0], data[1])); }
    else if (op == OP_NE) { push(opNE(data[0], data[1])); }
    else if (op == OP_GT) { push(opGT(data[0], data[1])); }
    else if (op == OP_GE) { push(opGE(data[0], data[1])); }
  }//byte_fcn_COMPARE_OP
  
  void VirtualMachine::byte_fcn_LOAD_CONST(const DataObject& aDO)
  {
    assert(aDO.isFullyInterpreted());
    push(aDO);
  }
  
  void VirtualMachine::byte_fcn_POP_TOP(const DataObject& aDO) { UNUSED_ARG(aDO); pop(); }
  void VirtualMachine::byte_fcn_DUP_TOP(const DataObject& aDO) { UNUSED_ARG(aDO); push(top()); }
  
  void VirtualMachine::byte_fcn_LOAD_CONST_BOOL(const DataObject& aDO)
  {
    assert(aDO.isFullyInterpreted() && aDO.dataObjectType() == DataObject::DataObjectType::DOT_INT);
    if (aDO.getDataValue<int>() != 0)
    {
      push(DataObject(true));
    }
    else
    {
      push(DataObject(false));
    }
  }//byte_fcn_LOAD_CONST_BOOL
  
  void VirtualMachine::byte_fcn_DUP_TOPX(const DataObject& aDO)
  {
    assert(aDO.dataObjectType() == DataObject::DataObjectType::DOT_INT);
    push(topn(aDO.getDataValue<int>()));
  }//byte_fcn_DUP_TOPX
  
  void VirtualMachine::byte_fcn_DUP_TOP_TWO(const DataObject& aDO) { UNUSED_ARG(aDO); push(topn(2)); }
  
  void VirtualMachine::byte_fcn_ROT_TWO(const DataObject& aDO)
  {
    UNUSED_ARG(aDO);
    const auto& vals = popn(2);
    push(vals[1]);
    push(vals[0]);
  }//byte_fcn_ROT_TWO
  
  void VirtualMachine::byte_fcn_LOAD_NAME(const DataObject& aDO)
  {
    const auto& name = aDO.getDataValue<std::string>();
    if (getFrame().containsLocalName(name))
    {
      push(getFrame().getLocalName(name));
      return;
    }
    else if (getFrame().containsGlobalName(name))
    {
      push(getFrame().getGlobalName(name));
    }
    else if (pCallbackReg.find(name) != pCallbackReg.end())
    {
      // Check if the name is one of the internal registered functions
      push(pCallbackReg[name]);
    }
    else
    {
      // Treat name as not fully interpreted
      push(DataObject(name.c_str()));
      top().setFullyInterpretedTag(false);
    }
  }//byte_fcn_LOAD_NAME
  
  void VirtualMachine::byte_fcn_STORE_NAME(const DataObject& aDO)
  {
    const auto& name = aDO.getDataValue<std::string>();
    assert(!name.empty());
    
    storeName(name, pop());
  }//byte_fcn_STORE_NAME
  
  void VirtualMachine::byte_fcn_DELETE_NAME(const DataObject& aDO)
  {
    const auto& name = aDO.getDataValue<std::string>();
    assert(!name.empty());
    
    getFrame().deleteLocalName(name);
  }//byte_fcn_DELETE_NAME
  
  void VirtualMachine::byte_fcn_LOAD_FAST(const DataObject& aDO)
  {
    const auto& name = aDO.getDataValue<std::string>();
    if (getFrame().containsLocalName(name))
    {
      push(getFrame().getLocalName(name));
    }
    else if (pCallbackReg.find(name) != pCallbackReg.end())
    {
      // Check if the name is one of the internal registered functions
      push(pCallbackReg[name]);
    }
    else
    {
      // Treat name as not fully interpreted
      push(DataObject(name.c_str()));
      top().setFullyInterpretedTag(false);
    }
  }//byte_fcn_LOAD_FAST
  
  void VirtualMachine::byte_fcn_STORE_FAST(const DataObject& aDO)
  {
    const auto& name = aDO.getDataValue<std::string>();
    assert(!name.empty());
    
    getFrame().storeLocalName(name, pop());
  }//byte_fcn_STORE_FAST
  
  void VirtualMachine::byte_fcn_DELETE_FAST(const DataObject& aDO)
  {
    const auto& name = aDO.getDataValue<std::string>();
    assert(!name.empty());
    
    getFrame().deleteLocalName(name);
  }//byte_fcn_DELETE_FAST
  
  void VirtualMachine::byte_fcn_LOAD_GLOBAL(const DataObject& aDO)
  {
    const auto& name = aDO.getDataValue<std::string>();
    assert(getFrame().containsGlobalName(name));
    
    push(getFrame().getGlobalName(name));
  }//byte_fcn_LOAD_GLOBAL
  
  void VirtualMachine::byte_fcn_STORE_GLOBAL(const DataObject& aDO)
  {
    const auto& name = aDO.getDataValue<std::string>();
    assert(getFrame().containsGlobalName(name));
    
    getFrame().storeGlobalName(name, pop());
  }//byte_fcn_STORE_GLOBAL
  
  void VirtualMachine::byte_fcn_DELETE_GLOBAL(const DataObject& aDO)
  {
    const auto& name = aDO.getDataValue<std::string>();
    assert(getFrame().containsGlobalName(name));
    
    getFrame().deleteGlobalName(name);
  }//byte_fcn_DELETE_GLOBAL
  
  void VirtualMachine::byte_fcn_LOAD_ATTR(const DataObject& aDO)
  {
    assert(aDO.dataObjectType() == DataObject::DataObjectType::DOT_STRING);
    std::string propName = aDO.getDataValue<std::string>();
    if (CLI::Utils::isQuotedString(propName))
    {
      propName = CLI::Utils::removeQuotesFromQuotedString(propName);
    }
    
    // Check if the top of the stack contains an object and if that object
    // has a property with key equal to the property to load
    itp_assert(top().isClassObject(), InterpreterException::ExceptionType::ET_WRONG_TYPE_OBJ);
    itp_assert_type_msg(top().classObject().hasProperty(propName) &&
                        top().classObject().isPropertyVisibile(propName),
                        InterpreterException::ExceptionType::ET_PROP_NOT_FOUND,
                        propName);
    
    // If everything is fine, load the property on the stack
    push(top().classObject().getProperty(propName));
  }//byte_fcn_LOAD_ATTR
  
  void VirtualMachine::byte_fcn_STORE_ATTR(const DataObject& aDO)
  {
    // Implements TOS.name = TOS1
    assert(aDO.dataObjectType() == DataObject::DataObjectType::DOT_STRING);
    const auto& propName = aDO.getDataValue<std::string>();
    
    // Check if the top of the stack contains an object and if that object
    // has a property with key equal to the property to load
    itp_assert(top().isClassObject(), InterpreterException::ExceptionType::ET_WRONG_TYPE_OBJ);
    
    // The stack should containt:
    // - the object (first pop())
    // - the property value (second pop())
    assert((*pFrame).getDataStack().size() >= 2);
    
    // Forcing pop() calls to be in order
    // gcc and msvc execute pop()s in reverse order if directly
    // passed as function parameters
    const auto& do1 = pop();
    const auto& do2 = pop();

    storeProperty(do1, propName, do2);
  }//byte_fcn_LOAD_ATTR
  
  void VirtualMachine::byte_fcn_STORE_SUBSCR(const DataObject&)
  {
    // Implements TOS1[TOS] = TOS2
    assert((*pFrame).getDataStack().size() >= 3);
    
    // Get top3 = [TOS2, TOS1, TOS]
    auto top3 = popn(3);
    itp_assert(top3[0].isComposite(), InterpreterException::ExceptionType::ET_NON_INTERP);
    itp_assert(top3[1].isComposite(), InterpreterException::ExceptionType::ET_NON_INTERP);
    itp_assert(top3[2].isComposite(), InterpreterException::ExceptionType::ET_NON_INTERP);
    
    // Implement top3[1][top3[2]] = top3[0]
    itp_assert(top3[1].isComposite(), InterpreterException::ExceptionType::ET_OUT_OF_BOUND);
    
    // Subscript operator can be applied on an list, matrix, or on a variable array
    if (!isMultiDimensionalParameterDeclarationDataObject(top3[1]))
    {
      itp_assert(false, InterpreterException::ExceptionType::ET_WRONG_TYPE);
    }
    
    // Set the element
    otools::setParameterElement(top3[1].getClassObject().get(), top3[2], top3[0]);
    
    // Push the modified object on top of the stack
    push(top3[1]);
  }// byte_fcn_STORE_SUBSCR
  
  void VirtualMachine::byte_fcn_BUILD_TUPLE(const DataObject& aDO)
  {
    // Get the number of elements that will be part of the tuple
    auto numel = static_cast<std::size_t>(aDO.getDataValue<int>());
    assert((*pFrame).getDataStack().size() >= numel);
    
    // Push a composite object on top of the stack representing the tuple
    push(popn(numel));
    
    if (numel > 0)
    {
      // Check if the elements in the list are fully interpreted
      assert(top().isComposite());
      for (const auto& dobj : top())
      {
        if (!dobj.isFullyInterpreted())
        {
          top().setFullyInterpretedTag(false);
          break;
        }
      }
    }
  }//byte_fcn_BUILD_TUPLE
  
  void VirtualMachine::byte_fcn_BUILD_LIST(const DataObject& aDO)
  {
    // Get the number of elements that will be part of the list
    auto numel = static_cast<std::size_t>(aDO.getDataValue<int>());
    assert((*pFrame).getDataStack().size() >= numel);
    
    // Get the elements of the list
    auto list = popn(numel);
    bool fullyInterp{ true };
    if (numel > 0)
    {
      // Check if the elements in the list are fully interpreted
      for (const auto& dobj : list)
      {
        if (!dobj.isFullyInterpreted())
        {
          fullyInterp = false;
          break;
        }
      }//for
    }//if
    
    // Instantiate a list object
    BaseObjectSPtr listObj(otools::createListObject(list));
    
    // Instantiate the owning DataObject
    DataObject doList;
    doList.setClassObject(listObj);
    if (!fullyInterp) doList.setFullyInterpretedTag(false);
    
    // Push the object on the top of the stack
    push(doList);
  }//byte_fcn_BUILD_LIST
  
  void VirtualMachine::byte_fcn_JUMP_FORWARD(const DataObject& aDO)
  {
    jump(aDO.getDataValue<Frame::PC_T>());
  }//byte_fcn_JUMP_FORWARD
  
  void VirtualMachine::byte_fcn_JUMP_ABSOLUTE(const DataObject& aDO)
  {
    jump(aDO.getDataValue<Frame::PC_T>());
  }//byte_fcn_JUMP_ABSOLUTE
  
  void VirtualMachine::byte_fcn_POP_JUMP_IF_TRUE(const DataObject& aDO)
  {
    assert(aDO.dataObjectType() == DataObject::DataObjectType::DOT_SIZE_T);
    if (pop().getDataValue<bool>()) jump(aDO.getDataValue<std::size_t>());
  }//byte_fcn_JUMP_IF_TRUE
  
  void VirtualMachine::byte_fcn_POP_JUMP_IF_FALSE(const DataObject& aDO)
  {
    assert(aDO.dataObjectType() == DataObject::DataObjectType::DOT_SIZE_T);
    if (!pop().getDataValue<bool>()) jump(aDO.getDataValue<std::size_t>());
  }//byte_fcn_JUMP_IF_FALSE
  
  void VirtualMachine::byte_fcn_JUMP_IF_TRUE_OR_POP(const DataObject& aDO)
  {
    assert(aDO.dataObjectType() == DataObject::DataObjectType::DOT_SIZE_T);
    auto val = top().getDataValue<bool>();
    if (val) jump(aDO.getDataValue<std::size_t>());
    else pop();
  }//byte_fcn_JUMP_IF_TRUE
  
  void VirtualMachine::byte_fcn_JUMP_IF_FALSE_OR_POP(const DataObject& aDO)
  {
    assert(aDO.dataObjectType() == DataObject::DataObjectType::DOT_SIZE_T);
    auto val = top().getDataValue<bool>();
    if (!val) jump(aDO.getDataValue<std::size_t>());
    else pop();
  }//byte_fcn_JUMP_IF_FALSE
  
  void VirtualMachine::byte_fcn_SETUP_LOOP(const DataObject& aDO)
  {
    assert(aDO.dataObjectType() == DataObject::DataObjectType::DOT_SIZE_T);
    pushBlock(Block::BlockType::BT_LOOP, aDO.getDataValue<std::size_t>());
  }//byte_fcn_SETUP_LOOP
  
  void VirtualMachine::byte_fcn_GET_ITER(const DataObject& aDO)
  {
    UNUSED_ARG(aDO);
    
    // Create a data object iterator and push it on the stack
    DataObject dobj;
    if (top().isComposite())
    {
      dobj.setIterator(pop());
    }
    else
    {
      itp_assert_type_msg((isClassObjectType(top()) &&
                           otools::isListObject(top().getClassObject().get())),
                          InterpreterException::ExceptionType::ET_WRONG_TYPE,
                          "Wrong iterator type");
      dobj.setIterator(otools::getListValues(top().getClassObject().get()));
      
      // Remove the top list object before pushing hte new iterator object
      pop();
    }
    
    push(dobj);
  }//byte_fcn_GET_ITER
  
  void VirtualMachine::byte_fcn_RANGE_LIST(const DataObject& aDO)
  {
    UNUSED_ARG(aDO);
    assert((*pFrame).getDataStack().size() > 1);
    auto rangeBounds = popn(2);
    
    // Check fully interpreted bounds
    itp_assert_type_msg(rangeBounds[0].isFullyInterpreted() && rangeBounds[1].isFullyInterpreted(),
                        InterpreterException::ExceptionType::ET_NON_INTERP, "indexes undefined");
    
    // Check correct types for bounds
    itp_assert((rangeBounds[0].dataObjectType() == DataObject::DataObjectType::DOT_INT ||
                rangeBounds[0].dataObjectType() == DataObject::DataObjectType::DOT_BOOL) &&
               (rangeBounds[0].dataObjectType() == DataObject::DataObjectType::DOT_INT ||
                rangeBounds[1].dataObjectType() == DataObject::DataObjectType::DOT_BOOL),
               InterpreterException::ExceptionType::ET_WRONG_TYPE);
    
    // Get start and end values for the range
    int start = rangeBounds[0].dataObjectType() == DataObject::DataObjectType::DOT_INT ?
    rangeBounds[0].getDataValue<int>() :
    (rangeBounds[0].getDataValue<bool>() ? 1 : 0);
    
    int end = rangeBounds[1].dataObjectType() == DataObject::DataObjectType::DOT_INT ?
    rangeBounds[1].getDataValue<int>() :
    (rangeBounds[1].getDataValue<bool>() ? 1 : 0);
    
    // Create the list to iterate onto
    std::vector<int> listLoop(std::abs(start - end) + 1);
    
    bool reverse{false};
    if (start > end)
    {
      reverse = true;
      std::swap(start, end);
    }
    
    // Fill the array
    std::iota(listLoop.begin(), listLoop.end(), start);

    std::vector<DataObject> list;
    list.reserve(listLoop.size());
    if (!reverse)
    {
      for (auto& cVal : listLoop)
      {
        list.push_back(Interpreter::DataObject(static_cast<int>(cVal)));
      }
    }
    else
    {
      // Reverse the array for descending list
      /*
      auto rit = listLoop.rbegin();
      for (; rit!= listLoop.rend(); ++rit)
      {
        list.push_back(Interpreter::DataObject(static_cast<int>(*rit)));
      }
      */
      // Update 4/7/19
      // Do not reverse the list since it won't work
      // on for-loops.
      // For example:
      // for i in [0..3]:
      //   for j in [i+1, 2]:
      //    // not working here
      //   end
      // end
      list.clear();
    }
    
    // Instantiate a list object
    BaseObjectSPtr listObj(otools::createListObject(list));
    
    // Instantiate the owning DataObject
    DataObject doList;
    doList.setClassObject(listObj);
    
    // Push the object on the top of the stack
    push(doList);
  }//byte_fcn_RANGE_LIST
  
  void VirtualMachine::byte_fcn_FOR_ITER(const DataObject& aDO)
  {
    if (top().hasNext())
    {
      // If the top data object is an iterator with
      // a next element, push next on top of the stack
      push(top().next());
    }
    else
    {
      // If there is no next, i.e., the loop is done,
      // pop the top of the stack and jump to the given PC
      pop();
      
      assert(aDO.dataObjectType() == DataObject::DataObjectType::DOT_SIZE_T);
      auto jumpTo = static_cast<std::size_t>(aDO.getDataValue<int>());
      jump(jumpTo);
    }
  }//byte_fcn_FOR_ITER
  
  void VirtualMachine::byte_fcn_BREAK_LOOP(const DataObject& aDO) { UNUSED_ARG(aDO); pExeResult = ExeRes::ER_BREAK; }
  
  void VirtualMachine::byte_fcn_CONTINUE_LOOP(const DataObject& aDO)
  {
    // As for python, continue is used for the return value,
    // it pushes the jump destination in the return value which is then
    // read by the correspondent return function
    pReturnValue = aDO;
    pExeResult = ExeRes::ER_CONTINUE;
  }//byte_fcn_CONTINUE_LOOP
  
  void VirtualMachine::byte_fcn_POP_BLOCK(const DataObject& aDO) { UNUSED_ARG(aDO); popBlock(); }
  void VirtualMachine::byte_fcn_CALL_FUNCTION(const DataObject& aDO) { callFunction(aDO); }
  
  void VirtualMachine::byte_fcn_RETURN_VALUE(const DataObject& aDO)
  {
    UNUSED_ARG(aDO);
    auto dataStackSize = getFrame().getDataStackSize();
    if (dataStackSize > 0)
    {
      assert(dataStackSize == 1);
      pReturnValue = pop();
    }
    else
    {
      pReturnValue = DataObject();
    }
  }//byte_fcn_RETURN_VALUE
  
  void VirtualMachine::byte_fcn_BUILD_DOMAIN(const DataObject& aDO)
  {
    // This instruction builds a "Variable" object (a BaseObject)
    // and sets it on top of the stack
    assert(aDO.dataObjectType() == DataObject::DataObjectType::DOT_INT);
    auto count = aDO.getDataValue<int>();
    
    // Get the list of values and type defining the domain
    auto vals = popn(count);
    
    // Check if domain is fully interpreted
    bool fullyInterp = true;
    for (const auto& d : vals)
    {
      if (!d.isFullyInterpreted())
      {
        fullyInterp = false;
        break;
      }
    }
    
    // Create a data object "Domain" of vals[last] type
    // At least one domain element defines the domain
    assert(vals.size() - 1 > 0 &&
           vals.back().dataObjectType() == DataObject::DataObjectType::DOT_INT);
    
    // Check data type here
    auto domType = vals.back().getDataValue<int>();
    
    bool isMatrixDom{ false };
    if (domType == DOM_SINGLETON_M ||
        domType == DOM_BOUNDS_M ||
        domType == DOM_LIST_M)
    {
      // The second element from the end
      // is the list of dimensions of the domain
      assert(vals.size() - 1 > 1);
      isMatrixDom = true;
    }
    
    // Set the right type for the object
    int domTypeForObj = domType;
    if (domTypeForObj == DOM_SINGLETON_M) domTypeForObj = DOM_SINGLETON;
    if (domTypeForObj == DOM_BOUNDS_M) domTypeForObj = DOM_BOUNDS;
    if (domTypeForObj == DOM_LIST_M) domTypeForObj = DOM_LIST;
    
    BaseObject* obj{ nullptr };
    if (isMatrixDom)
    {
      // For matrix types, create a matrix variable object:
      // 1) the name will be set later when storing the name
      // 2) the domain type is given
      // 3) the values of the domain are given
      // 4) the dimensions of the domain are stored in the composite object at position "vals.size() - 2"
      // 5) the array of domains is empty since all the sub-domains are the same as the domain (2)
      // 6) set default semantic
      // @note dimensions are already store in a list.
      // For example:
      // x:1..2:[2, 3]
      // where
      // [2, 3]
      // is a list object
      const auto& listDims = (vals.at(vals.size() - 2));
      assert(otools::isListObject( listDims.getClassObject().get() ));
      assert(otools::getListSize(  listDims.getClassObject().get() ) > 0);
      
      const auto& compDims = otools::getListValues(listDims.getClassObject().get()).getCompose();
      obj = otools::createObjectMatrixVariable("", domTypeForObj, { vals.begin(), vals.end() - 2 },
                                               compDims, {}, VAR_SPEC_DECISION);
    }
    else
    {
      // For non matrix types, create a variable object:
      // 1) the name will be set later when storing the name
      // 2) the domain type is given
      // 3) the values of the domain are given
      // 4) set default semantic
      obj = otools::createObjectVariable("", domTypeForObj, { vals.begin(), vals.end() - 1 },
                                         VAR_SPEC_DECISION);
    }
    assert(obj);
    
    // Set the object instance
    std::shared_ptr<BaseObject> objPtr(obj);
    
    // Push on the stack a new object and give it ownership of the variable class object
    push(DataObject());
    top().setClassObject(objPtr);
    
    // Set interpretation tag onto the top object
    top().setFullyInterpretedTag(fullyInterp);
  }//byte_fcn_BUILD_DOMAIN
  
  void VirtualMachine::byte_fcn_SET_VAR_SPEC(const DataObject& aDO)
  {
    assert(aDO.dataObjectType() == DataObject::DataObjectType::DOT_INT);
    assert(isDomainDeclarationDataObject(top()));
    
    // Set the specification on the object
    top().getClassObject()->setProperty("semantic", aDO);
  }//byte_fcn_SET_VAR_SPEC
  
  void VirtualMachine::byte_fcn_STORE_VAR(const DataObject& aDO)
  {
    const auto& name = aDO.getDataValue<std::string>();
    assert(!name.empty());
    
    // Will add <name> to the set of variables in the context.
    // When printing the context or the variable <name>
    // the context will get the DataObject from local names in frame
    // and print it as a variable.
    // This is basically the same as STORE_NAME
    storeName(name, pop());
  }//byte_fcn_STORE_VAR
  
  void VirtualMachine::byte_fcn_ADD_MARK(const DataObject& aDO)
  {
    UNUSED_ARG(aDO);
    
    // Push the current PC on the markers stack to "tag" the current
    // position in the bytecode.
    // @note adding -1 to mark the "previous" PC since the bytecode for MARK
    // (current byte code) must be the first marked bytecode
    addMarkToPCStack(-1);
  }//byte_fcn_ADD_MARK
  
  void VirtualMachine::byte_fcn_PRINT_TOP(const DataObject& aDO)
  {
    UNUSED_ARG(aDO);
    
    assert(pModelObjReportCallback && !(*pFrame).getDataStack().empty());
    pModelObjReportCallback(top(), pCallbackPrint);
    
    // Reset callback print flag
    pCallbackPrint = false;
  }//byte_fcn_PRINT_TOP
  
}// end namespace Interpreter
