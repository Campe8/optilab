// Self first
#include "InterpUtils.hpp"

#include "InterpreterDefs.hpp"
#include "ObjectTools.hpp"
#include "ObjectHelper.hpp"

namespace otools = Interpreter::ObjectTools;

namespace Interpreter { namespace Utils {

	int byteToInt(byte_T aByte)
	{
		return static_cast<int>(aByte);
	}//byteToInt

	ByteArray intToByte(int aInt)
	{
		ByteArray bytes(4, 0);
		bytes[3] = (aInt >> 24) & 0xFF;
		bytes[2] = (aInt >> 16) & 0xFF;
		bytes[1] = (aInt >> 8)  & 0xFF;
		bytes[0] =  aInt & 0xFF;

		return bytes;
	}//intToByte

	byte_T intToLSByte(int aInt)
	{
		byte_T b = aInt & 0xFF;
		return b;
	}//intToLSByte
  
  DataObject opAddOnCompositeObjects(const DataObject& aData1, const DataObject& aData2)
  {
    assert(isCompositeType(aData1) || isCompositeType(aData2));
    auto compositeObj = isCompositeType(aData1) ? aData1 : DataObject();
    
    if (!isCompositeType(compositeObj))
    {
      // Data1 wasn't a composite object, Data2 must be.
      // Merge the two objects
      compositeObj.compose({ aData1 });
      compositeObj.getCompose().insert(compositeObj.getCompose().end(), aData2.getCompose().begin(),
                                       aData2.getCompose().end());
    }
    else if(!isCompositeType(aData2))
    {
      // aData1 is composite and aData2 isn't
      compositeObj.getCompose().push_back(aData2);
    }
    else
    {
      compositeObj.getCompose().insert(compositeObj.getCompose().end(), aData2.getCompose().begin(),
                                       aData2.getCompose().end());
    }
    
    return compositeObj;
  }//opAddOnCompositeObjects
  
  DataObject opAddOnListObjects(const DataObject& aData1, const DataObject& aData2)
  {
    // List with type
    // - primitive
    // - scalar
    // - list
    // - composite
    assert(isClassObjectType(aData1) && otools::isListObject(aData1.getClassObject().get()));
    if (isClassObjectType(aData2) && otools::isListObject(aData2.getClassObject().get()))
    {
      DataObject addList;
      addList.setClassObject(BaseObjectSPtr(otools::concatLists(aData1.getClassObject().get(),
                                                                aData2.getClassObject().get())));
      return addList;
    }
    else if (isPrimitiveType(aData2))
    {
      DataObject addList;
      addList.setClassObject(BaseObjectSPtr(otools::concatLists(aData1.getClassObject().get(),
                                                            otools::createListObject({ aData2 }))));
      return addList;
    }
    else if (isClassObjectType(aData2) && otools::isScalarObject(aData2.getClassObject().get()))
    {
      auto ll = otools::createListObject({otools::getScalarValue(aData2.getClassObject().get())});
      BaseObjectSPtr list2 = BaseObjectSPtr(ll);
      
      DataObject addList;
      addList.
      setClassObject(BaseObjectSPtr(otools::concatLists(aData1.getClassObject().get(), ll)));
      return addList;
    }
    else if (isCompositeType(aData2))
    {
      BaseObjectSPtr list2(otools::createListObject(aData2.getCompose()));
      DataObject addList;
      addList.setClassObject(BaseObjectSPtr(otools::concatLists(aData1.getClassObject().get(),
                                                                list2.get())));
      return addList;
    }
    
    // Everything else is not supported
    itp_assert_msg(false, "Operator not supported");
  }//opAddOnListObjects
  
  DataObject opAddOnParameterObjects(const DataObject& aData1, const DataObject& aData2)
  {
    assert(isClassObjectType(aData1));
    
    // @note inObj1 can be either a scalar, a list, or a matrix
    auto inObj1 = aData1.getClassObject().get();
    assert(otools::isParameterObject(inObj1));
    
    Interpreter::BaseObject* inObj2{nullptr};
    if (isClassObjectType(aData2))
    {
      inObj2 = aData2.getClassObject().get();
    }
    else if (isPrimitiveType(aData2))
    {
      inObj2 = otools::createListObject({ aData2 });
    }
    else if (isCompositeType(aData2))
    {
      inObj2 = otools::createListObject(aData2.getCompose());
    }
    
    // Everything else is not supported
    itp_assert_msg(inObj2, "Operator not supported");
    
    DataObject addParam;
    addParam.setClassObject(BaseObjectSPtr(otools::concatParameters(inObj1, inObj2)));
    return addParam;
  }//opAddOnParameterObjects
  
  DataObject opAddOnClassObjects(const DataObject& aData1, const DataObject& aData2)
  {
    assert(isClassObjectType(aData1) || isClassObjectType(aData2));
    
    // First data is an object
    auto obj1 = aData1.getClassObject().get();
    if (isClassObjectType(aData1))
    {
      // First input is a scalar, list, or matrix
      if (otools::isParameterObject(obj1))
      {
        return opAddOnParameterObjects(aData1, aData2);
      }
      
      // Everything else is not supported
      itp_assert_msg(false, "Operator not supported");
    }// first input is an object
    
    // Second input is an object
    return opAddOnClassObjects(aData2, aData1);
  }//opAddOnClassObjects
  
}}// end namespace Interpreter
