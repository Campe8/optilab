// Self first
#include "InlineFcn.hpp"

namespace Interpreter {

	InlineFcn::InlineFcn(const std::string& aFcnName, VirtualMachine* aVM, bool aNeedsFcnObject)
  : DataObjectFcn(aFcnName, FcnType::FCN_INLINE, aVM)
  , pMetaFunction(aNeedsFcnObject)
  , pCallBack({})
	{
	}

	DataObjectFcn* InlineFcn::clone()
	{
		auto fcn = new InlineFcn(getFcnName(), getVM(), pMetaFunction);
    
    // Clone base class memebrs into the new instance "fcn"
    cloneInto(fcn);
    
		fcn->setCallbackFcn(pCallBack);
		return fcn;
	}//clone

	std::pair<DataObject, DataObject> InlineFcn::operator()(const std::vector<DataObject>& aArgs)
	{
		assert(pCallBack);
		try
		{
      return { DataObject(EXIT_SUCCESS), pCallBack(aArgs) };
		}
		catch (...)
		{
      DataObject voidObj;
      return { DataObject(EXIT_FAILURE), voidObj };
		}
	}//()

}// end namespace Interpreter
