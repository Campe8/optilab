// Self first
#include "DataObjectFcn.hpp"

#include "VirtualMachine.hpp"

#include <cassert>

namespace Interpreter {

	DataObjectFcn::DataObjectFcn(const std::string& aFcnName, FcnType aFcnType, VirtualMachine* aVM)
  : pFcnName(aFcnName)
  , pHasOutput(false)
  , pFcnType(aFcnType)
  , pVirtualMachine(aVM)
	{
	}
  
  void DataObjectFcn::cloneInto(DataObjectFcn* aFcn)
  {
    assert(aFcn);
    aFcn->pHasOutput = this->hasOutput();
  }//cloneInto

}// end namespace Interpreter
