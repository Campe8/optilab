// Self first
#include "ObjectToolsAux.hpp"

#include "ObjectHelper.hpp"


#include <cstddef>  // for std::size_t

namespace Interpreter { namespace ObjectTools {
  
  static std::string getMetricsOptionsTypeName()
  {
    static std::string metricsOptionsName{"Metrics"};
    return metricsOptionsName;
  }//getMetricsOptionsTypeName
  
  std::string getMetricsTimeOptionsName()
  {
    static std::string param{"timeMetrics"};
    return param;
  }//getMetricsTimeOptionsName
  
  std::string getMetricsSearchOptionsName()
  {
    static std::string param{"searchMetrics"};
    return param;
  }//getMetricsSearchOptionsName
  
  BaseObject* getMetricsOptionsObject(bool aTimeOptions, bool aSearchOptions)
  {
    auto obj = createObject(getMetricsOptionsTypeName());
    
    // Base class search options properties
    obj->addProperty(getMetricsTimeOptionsName(), aTimeOptions);
    obj->addProperty(getMetricsSearchOptionsName(), aSearchOptions);
    
    return obj;
  }//getMetricsOptionsObject
  
}}// end namespace ObjectTools/Interpreter
