// Self first
#include "ObjectFcn.hpp"

namespace Interpreter {

	ObjectFcn::ObjectFcn(const std::string& aFcnName)
  : DataObjectFcn(aFcnName, FcnType::FCN_OBJECT, nullptr)
	{
	}
  
  DataObjectFcn* ObjectFcn::clone()
  {
    auto fcn = new ObjectFcn(getFcnName());
    
    // Clone base class memebrs into the new instance "fcn"
    cloneInto(fcn);
    
    // Set the callback function
    fcn->setCallbackFcn(pCallBack);
    return fcn;
  }//clone
  
  DataObject ObjectFcn::operator()(BaseObject* aObj, const std::vector<DataObject>& aArgs)
  {
    assert(pCallBack);
    return pCallBack(aObj, aArgs);
  }//()

}// end namespace Interpreter
