// Self first
#include "ObjectTools.hpp"

#include "ObjectHelper.hpp"
#include "ObjectTools.hpp"
#include "SearchOptionsTools.hpp"
#include "InterpUtils.hpp"
#include "InterpreterDefs.hpp"

#include <algorithm>
#include <map>
#include <exception>

namespace Interpreter { namespace ObjectTools {
  
  // Returns the type of values for the matrix construction.
  // It returns:
  // -1 if values are not valid
  // 0  if "aValues" is empty
  // 1  if "aValues" are all primitive objects
  // 2  if "aValues" are all composite DataObjects
  // 3  if "aValues" are all list BaseObjects
  // 4  if "aValues" are all matrix BaseObjects
  static int getMatrixVectorType(const std::vector<DataObject>& aValues)
  {
    if (aValues.empty()) return 0;
    
    int vectorType{-1};
    const auto& firstElem = aValues[0];
    if (isPrimitiveType(firstElem))
    {
      vectorType = 1;
    }
    else if (isCompositeType(firstElem))
    {
      vectorType = 2;
    }
    else if (isClassObjectType(firstElem))
    {
      if (isScalarObject(firstElem.getClassObject().get()))
      {
        vectorType = 1;
      }
      else if (isListObject(firstElem.getClassObject().get()))
      {
        vectorType = 3;
      }
      else if (isMatrixObject(firstElem.getClassObject().get()))
      {
        vectorType = 4;
      }
    }
    if (vectorType == -1) return -1;
    
    for (std::size_t idx{1}; idx < aValues.size(); ++idx)
    {
      const auto& nextElem = aValues[idx];
      if (vectorType == 1)
      {
        if (!isPrimitiveType(nextElem) && !(isClassObjectType(nextElem) &&
                                            isScalarObject(nextElem.getClassObject().get())))
        {
          return -1;
        }
      }
      else if (vectorType == 2 && !isCompositeType(nextElem)) return -1;
      else if (vectorType == 3 && !(isClassObjectType(nextElem) &&
                                    isListObject(nextElem.getClassObject().get()))) return -1;
      else if (vectorType == 4 && !(isClassObjectType(nextElem) &&
                                    isMatrixObject(nextElem.getClassObject().get()))) return -1;
    }
    return vectorType;
  }//getMatrixVectorType
  
  // Checks the dimensions of the object which is supposed to be the payload of
  // a matrix domain object
  static void checkMatrixDomainDimensions(const std::vector<DataObject>& aDomainDims)
  {
    for (const auto& dim : aDomainDims)
    {
      int dimension{-1};
      if (isClassObjectType(dim))
      {
        auto obj = dim.getClassObject().get();
        assert(isScalarObject(obj));
        itp_assert(getScalarValue(obj).dataObjectType() != DataObject::DOT_STRING,
                   InterpreterException::ExceptionType::ET_WRONG_TYPE);
        dimension = getScalarValue(obj).castDataValue<int>();
      }
      else
      {
        assert(isPrimitiveType(dim));
        itp_assert(dim.dataObjectType() != DataObject::DOT_STRING,
                   InterpreterException::ExceptionType::ET_WRONG_TYPE);
        dimension = dim.castDataValue<int>();
      }
      itp_assert(dimension >= 0, InterpreterException::ExceptionType::ET_BAD_ALLOC);
    }
  }//checkMatrixDimensions
  
  // Returns true if the values in "aValues" are either all primitive objects, or
  // are all matrices with the same dimensions
  static bool rowsMatchDimensions(const std::vector<DataObject>& aValues, int aValuesType)
  {
    assert(aValuesType == 2 || aValuesType == 3 || aValuesType == 4);
    
    // Check composite objects
    if (aValuesType == 2)
    {
      auto baseLine = aValues[0].composeSize();
      
      // Check consistency of dimensions for following rows
      for(std::size_t idx{1}; idx < aValues.size(); ++idx)
      {
        if (baseLine != aValues[idx].composeSize()) return false;
      }
      return true;
    }
    
    // Only list and matrix objects are possible here
    const auto& baseLineDims = getParameterDimensions(aValues[0].getClassObject().get());
    if (baseLineDims.empty()) return false;
    
    // Check consistency of dimensions for following rows
    for(std::size_t idx{1}; idx < aValues.size(); ++idx)
    {
      // @note lists and matrices must have the same dimensions
      if (baseLineDims != getParameterDimensions(aValues[idx].getClassObject().get())) return false;
    }
    return true;
  }//rowsMatchDimensions
  
  static double hashDomainElement(const DataObject& aElem)
  {
    assert(isPrimitiveType(aElem) || isClassObjectType(aElem));
    
    BaseObject* scalar{nullptr};
    if (isClassObjectType(aElem))
    {
      scalar = aElem.getClassObject().get();
      itp_assert(isScalarObject(scalar), InterpreterException::ExceptionType::ET_WRONG_TYPE);
    }
    
    if (scalar)
    {
      itp_assert(getScalarValue(scalar).dataObjectType() != DataObject::DataObjectType::DOT_STRING,
                 InterpreterException::ExceptionType::ET_WRONG_TYPE);
      return getScalarValue(scalar).castDataValue<double>();
    }
    else
    {
      itp_assert(aElem.dataObjectType() != DataObject::DataObjectType::DOT_STRING,
                 InterpreterException::ExceptionType::ET_WRONG_TYPE);
      return aElem.castDataValue<double>();
    }
  }//hashDomainElement
  
  static std::vector<DataObject> normalizeDomain(const std::vector<DataObject>& aDomain)
  {
    std::map<double, std::size_t> uniqueElements;
    
    std::size_t idx{0};
    for(const auto& elem : aDomain)
    {
      auto key = hashDomainElement(elem);
      uniqueElements[key] = idx++;
    }
    
    std::vector<DataObject> ndom;
    for(auto& it : uniqueElements)
    {
      assert(it.second < aDomain.size());
      ndom.push_back(aDomain[it.second]);
    }
    
    return ndom;
  }//normalizeDomain
  
  BaseObject::PropertyKey getTypePropName()
  {
    static std::string propName = "type";
    return propName;
  }//getTypePropName
  
	BaseObject::PropertyKey getObjectTypePropName()
	{
		static std::string propName = "objectType";
		return propName;
	}//getObjectTypePropName

	BaseObject::PropertyKey getObjectInterpretedPropName()
	{
		static std::string propName = "fullyInterpreted";
		return propName;
	}//getObjectInterpretedPropName
  
  BaseObject::PropertyKey getObjectIDPropName()
  {
    static std::string propName = "id";
    return propName;
  }//getObjectIDPropName
  
  BaseObject* createObject()
  {
    static std::string baseObject("Object");
    return createObject(baseObject);
  }
  
  BaseObject* createObject(const std::string& aType)
  {
    assert(!aType.empty());
    auto obj = newObject();
    
    // Add type property
    obj->addProperty(getTypePropName(), aType.c_str());
    
    // The type of the object is, by default, const and non visibile
    obj->setConstProperty(getTypePropName(), true);
    obj->setPropertyVisibility(getTypePropName(), false);
    
    return obj;
  }//createObject
  
  BaseObject* createModelContextObject(int aObjType)
  {
    // Create an object of type "CtxObj"
    auto obj = createObject("CtxObj");
    
    // Add type property
    obj->addProperty(getObjectTypePropName(), aObjType);
    obj->setPropertyVisibility(getObjectTypePropName(), false);
    
    // Add fully interpreted property
    obj->addProperty(getObjectInterpretedPropName(), true);
    obj->setPropertyVisibility(getObjectTypePropName(), false);
    
    return obj;
  }//createModelContextObject
  
  ObjectFcn* createObjectFcn(const std::string& aFcnName, ObjectFcn::ObjFcnHandler aFcn,
                             bool aHasOutput)
  {
    // Create an object function data object storing the handler
    auto fcn = new ObjectFcn(aFcnName);
    
    // Set function as output function
    if (aHasOutput)
    {
      fcn->setAsOutputFcn();
    }
    
    // Set the callback function handler
    fcn->setCallbackFcn(aFcn);
    
    return fcn;
  }//registerInlineCallbackFcn
  
  DataObject getSizeOfObjectList(BaseObject* aObj, const std::vector<DataObject>&)
  {
    assert(aObj && isListObject(aObj));
    return static_cast<int>(aObj->lookupProperty("data").composeSize());
  }//getListSize
  
  DataObject setSizeOfObjectList(BaseObject* aObj, const std::vector<DataObject>& aArgs)
  {
    assert(aObj && isListObject(aObj));
    assert(aArgs.size() == 1 && aArgs[0].dataObjectType() == DataObject::DataObjectType::DOT_INT);
    auto size   = aArgs[0].getDataValue<int>();
    size = size < 0 ? 0 : size;
    
    // Check the sizes and return if they match
    if((aObj->lookupProperty("data")).composeSize() == static_cast<std::size_t>(size)) return DataObject();
    
    // Otherwise set the size of the values according to the given size
    auto values = aObj->lookupProperty("data");
    auto valuesSize = static_cast<int>(values.composeSize());
    
    std::vector<DataObject> resizedValues;
    resizedValues.reserve(size);
    for(int i = 0; i < size; ++i)
    {
      if(i < valuesSize)
      {
        resizedValues.push_back(values[static_cast<std::size_t>(i)]);
      }
      else
      {
        // Void object to fill the resized values
        resizedValues.push_back(DataObject());
      }
    }
    
    // Reset the values array
    DataObject newArray;
    newArray.compose(resizedValues);
    aObj->lookupProperty("data") = newArray;
    
    return DataObject();
  }//getListSize
  
  BaseObject* createScalarObject(const DataObject& aScalar)
  {
    const auto& oh = ObjectHelper::getInstance();
    
    std::string className = getScalarClassName();
    assert(oh.isObjectClassRegistered(className));
    
    auto scalar = oh.createObjectInstance(className);
    
    // Set the value
    assert(scalar->hasProperty("data") && isPrimitiveType(aScalar));
    scalar->lookupProperty("data") = aScalar;
    
    return scalar;
  }//createScalarObject
  
  BaseObject* createListObject(const std::vector<DataObject>& aList)
  {
    const auto& oh = ObjectHelper::getInstance();
    
    std::string className = getListClassName();
    assert(oh.isObjectClassRegistered(className));
    
    auto list = oh.createObjectInstance(className);
    
    // Set the values
    assert(list->hasProperty("data"));
    DataObject doList;
    if (!aList.empty())
    {
      doList.compose(aList);
    }
    list->lookupProperty("data") = doList;
    
    // Set property "get" size
    auto getFcn = createObjectFcn("getSize", getSizeOfObjectList, true);
    list->attachFcnToGetProperty("size", getFcn);
    
    // Set property "set" size
    auto setFcn = createObjectFcn("setSize", setSizeOfObjectList, false);
    list->attachFcnToSetProperty("size", setFcn);
    
    return list;
  }//createListObject
  
  BaseObjectSPtr createListObjectSPtr(const std::vector<DataObject>& aList)
  {
    return BaseObjectSPtr(createListObject(aList));
  }//createListObjectSPtr

  BaseObject* createMatrixObject(const std::vector<DataObject>& aVals)
  {
    // A Matrix is a list of lists of elements
    const auto& oh = ObjectHelper::getInstance();
    
    std::string className = getMatrixClassName();
    assert(oh.isObjectClassRegistered(className));
    
    auto matrix = oh.createObjectInstance(className);
    
    // Check if all the values have primitive type,
    // if so create a list reprsenting the vector (i.e., a matrix of dimensions [1 x size])
    assert(matrix->hasProperty("dims"));
    assert(matrix->hasProperty("data"));

    // Get the type of the values used to create the matrix
    auto valsType = getMatrixVectorType(aVals);
    
    // Empty matrix
    if (valsType == 0)
    {
      // Create an empty matrix
      (matrix->lookupProperty("data")).
      setClassObject(std::shared_ptr<BaseObject>(createListObject({})));
      
      (matrix->lookupProperty("dims")).
      setClassObject(std::shared_ptr<BaseObject>(createListObject({})));
      
      return matrix;
    }
    
    // Primitive data objects
    if (valsType == 1)
    {
      // Create a list vector and set it as the data of the matrix
      std::vector<DataObject> listOfLists;
      listOfLists.push_back(Interpreter::DataObject());
      listOfLists.back().setClassObject(std::shared_ptr<BaseObject>(createListObject(aVals)));
      
      (matrix->lookupProperty("data")).
      setClassObject(std::shared_ptr<BaseObject>(createListObject(listOfLists)));
      
      // Set dimensions [1 x size]
      auto valsSize = static_cast<int>(aVals.size());
      std::shared_ptr<BaseObject>listDims(createListObject({DataObject(1), DataObject(valsSize)}));
      (matrix->lookupProperty("dims")).setClassObject(listDims);
      
      return matrix;
    }
    
    // Composite type, list type, or matrix type
    if (valsType == 2 || valsType == 3 || valsType == 4)
    {
      // All rows must have the same dimensions
      if (!rowsMatchDimensions(aVals, valsType))
      {
        throw std::logic_error("Invalid matrix arguments");
      }
      
      // Create a list of list representing the data
      std::vector<DataObject> listOfLists;
      listOfLists.reserve(aVals.size());
      
      // Create a dimension vector (each element represents one dimension)
      std::vector<DataObject> dimensions;
      
      // Create a matrix out of composite objects
      if (valsType == 2)
      {
        // There are |aVals| rows of aVals[0].compositeSize() columns
        dimensions.push_back(DataObject(static_cast<int>(aVals.size())));
        dimensions.push_back(DataObject(static_cast<int>(aVals[0].composeSize())));
        for (const auto& row : aVals)
        {
          listOfLists.push_back(Interpreter::DataObject());
          listOfLists.back().setClassObject(createListObjectSPtr(row.getCompose()));
        }
      }
      else if (valsType == 3)
      {
        // There are |aVals| rows of |aVals[0].list| columns
        dimensions.push_back(DataObject(static_cast<int>(aVals.size())));
        dimensions.push_back(DataObject(getListSize(aVals[0].getClassObject().get())));
        
        // Create a matrix out of list objects
        listOfLists.resize(aVals.size());
        std::copy(aVals.begin(), aVals.end(), listOfLists.begin());
      }
      else
      {
        // Create a matrix out of matrix objects
        assert(valsType == 4);
        
        // Get dimensions of each sub-matrix
        auto baseDims = getParameterDimensions(aVals[0].getClassObject().get());
        for (std::size_t idx{0}; idx < aVals.size(); ++idx)
        {
          const auto& rowList = getParameterValues(aVals[idx].getClassObject().get());
          assert(rowList.isClassObject() && isListObject(rowList.getClassObject().get()));
          
          // Create the list of lists of each sub-matrix
          listOfLists.push_back(rowList);
        }
        
        // Convert dimensions into BaseObject(s)
        dimensions.push_back(DataObject(static_cast<int>(aVals.size())));
        for (auto dim : baseDims)
        {
          dimensions.push_back(DataObject(dim));
        }
      }
      
      // Create a list of lists and set it as a property of the parameter
      (matrix->lookupProperty("data")).
      setClassObject(std::shared_ptr<BaseObject>(createListObject(listOfLists)));
      
      // Set dimensions
      (matrix->lookupProperty("dims")).
      setClassObject(std::shared_ptr<BaseObject>(createListObject(dimensions)));
      
      return matrix;
    }
    
    assert(valsType == -1);
    throw std::logic_error("Invalid matrix arguments");
  }//createMatrixObject
  
  BaseObject* createObjectSearch(const DataObject& aVarsList)
  {
    std::shared_ptr<BaseObject>srcOption(SearchOptionsTools::getDFSSearchOptionsObject());
    DataObject options;
    options.setClassObject(srcOption);
    return createObjectSearch(aVarsList, options);
  }//createObjectSearch
  
  BaseObject* createObjectSearch(const DataObject& aVarsList, const DataObject& aSearchOptions)
  {
    const auto& oh = ObjectHelper::getInstance();
    
    // Create the base class object
    std::string className = getSearchClassName();
    assert(oh.isObjectClassRegistered(className));
    
    itp_assert((isClassObjectType(aVarsList) && isListObject(aVarsList.getClassObject().get())) ||
               isCompositeType(aVarsList), InterpreterException::ExceptionType::ET_WRONG_TYPE);
    itp_assert((isClassObjectType(aSearchOptions) &&
                SearchOptionsTools::isSearchOptionsObject(aSearchOptions.getClassObject().get())),
               InterpreterException::ExceptionType::ET_WRONG_TYPE);
    
    auto classID = oh.getIDForClassName(className);
    std::unique_ptr<BaseObject> ctxObj(createModelContextObject(static_cast<int>(classID)));
    auto src = oh.createObjectInstance(className, ctxObj.get());
    
    // Scope
    assert(src->hasProperty("scope"));
    if (isCompositeType(aVarsList))
    {
      DataObject list;
      list.setClassObject(createListObjectSPtr(aVarsList.getCompose()));
      src->setProperty("scope", list);
    }
    else
    {
      src->setProperty("scope", aVarsList);
    }
    
    // Search options
    assert(src->hasProperty("searchOptions"));
    src->setProperty("searchOptions", aSearchOptions);
    
    return src;
  }//createObjectSearch
  
  BaseObject* createObjectConstraint(const DataObject& aPostFcn)
  {
    const auto& oh = ObjectHelper::getInstance();
    
    // Create the base class object
    std::string className = getConstraintClassName();
    assert(oh.isObjectClassRegistered(className));
    assert(isFunctionType(aPostFcn));
    assert(aPostFcn.dataObjectType() == DataObject::DataObjectType::DOT_STRING);
    
    auto constraintName = aPostFcn.getDataValue<std::string>();
    assert(!constraintName.empty());
    
    auto classID = oh.getIDForClassName(className);
    std::unique_ptr<BaseObject> ctxObj(createModelContextObject(static_cast<int>(classID)));
    auto con = oh.createObjectInstance(className, ctxObj.get());
    
    // Name
    assert(con->hasProperty("name"));
    setObjectPropertyValue<std::string>(con, "name", constraintName);

    // Post function
    assert(con->hasProperty("__post_constraint_fcn"));
    con->setProperty("__post_constraint_fcn", aPostFcn);
    
    return con;
  }//createObjectConstraint
  
  BaseObject* createObjectVariable(  const std::string& aVarID
                                   , int aDomainType
                                   , const std::vector<DataObject>& aDomain
                                   , int aSemantic)
  {
    return createObjectMatrixVariable(aVarID, aDomainType, aDomain, {}, {}, aSemantic);
  }//createObjectVariable
  
  BaseObject* createObjectMatrixVariable(  const std::string& aVarID
                                         , int aDomainType
                                         , const std::vector<DataObject>& aDomain
                                         , const std::vector<DataObject>& aDomainDims
                                         , const std::vector<DataObject>& aDomainArray
                                         , int aSemantic)
  {
    const auto& oh = ObjectHelper::getInstance();
    
    std::string className = getVariableClassName();
    assert(oh.isObjectClassRegistered(className));
    
    // Create the base class object
    std::unique_ptr<BaseObject> ctxObj(
                       createModelContextObject(static_cast<int>(oh.getIDForClassName(className))));
    auto var = oh.createObjectInstance(className, ctxObj.get());
    
    // Variable ID
    assert(var->hasProperty(getObjectIDPropName()));
    var->lookupProperty(getObjectIDPropName()) = DataObject(aVarID.c_str());
    
    // Domain type
    // - Singleton
    // - Bounds
    // - List
    assert(var->hasProperty("domainType"));
    auto normDomain = normalizeDomain(aDomain);
    
    if(normDomain.size() < 2) aDomainType = DOM_SINGLETON;
    var->lookupProperty("domainType") = DataObject(aDomainType);
    
    // Domain values
    assert(var->hasProperty("domain"));
    (var->lookupProperty("domain")).setClassObject( createListObjectSPtr(normDomain) );
    
    // Domain dimensions
    checkMatrixDomainDimensions(aDomainDims);
    
    assert(var->hasProperty("domainSubscript"));
    (var->lookupProperty("domainSubscript")).setClassObject( createListObjectSPtr({}) );
    
    assert(var->hasProperty("domainDimensions"));
    (var->lookupProperty("domainDimensions")).setClassObject( createListObjectSPtr(aDomainDims) );
    
    // Composite domains
    assert(var->hasProperty("domainArray"));
    if(!aDomainArray.empty())
    {
      for(const auto& aDomain : aDomainArray)
      {
        assert(aDomain.isClassObject() && isListObject(aDomain.getClassObject().get()));
      }
    }
    (var->lookupProperty("domainArray")).setClassObject( createListObjectSPtr(aDomainArray) );
    
    // Semantic
    assert(var->hasProperty("semantic"));
    var->lookupProperty("semantic") = DataObject(aSemantic);
    
    return var;
  }//createObjectMatrixVariable
  
  BaseObject* createObjectMatrixVariableSubscr(BaseObject* aVar, const DataObject& aIndices)
  {
    assert(isVariableObject(aVar));
    
    const auto& oh = ObjectHelper::getInstance();
    
    std::string className = getVariableClassName();
    assert(oh.isObjectClassRegistered(className));
    
    // Create the base class object
    std::unique_ptr<BaseObject> ctxObj(createModelContextObject(
                                                static_cast<int>(oh.getIDForClassName(className))));
    auto subscrVar = oh.createObjectInstance(className, ctxObj.get());
    
    // Variable ID
    assert(subscrVar->hasProperty(getObjectIDPropName()));
    subscrVar->lookupProperty(getObjectIDPropName()) = DataObject(getVarID(aVar).c_str());
    
    assert(subscrVar->hasProperty("domainType"));
    subscrVar->lookupProperty("domainType") = DataObject(getVarDomainType(aVar));
    
    assert(subscrVar->hasProperty("domain"));
    auto subscrDomain = getVarElement(aVar, aIndices);
    
    assert(subscrDomain.isClassObject() && isListObject(subscrDomain.getClassObject().get()));
    (subscrVar->lookupProperty("domain")).setClassObject( subscrDomain.getClassObject() );
    
    // Share the original domain dimensions
    assert(subscrVar->hasProperty("domainDimensions") && aVar->hasProperty("domainDimensions"));
    (subscrVar->lookupProperty("domainDimensions")).setClassObject(
                                                    getVarDomainDimensions(aVar).getClassObject());
    
    // Empty domain array list of values
    assert(subscrVar->hasProperty("domainArray"));
    (subscrVar->lookupProperty("domainArray")).setClassObject( createListObjectSPtr({}) );
    
    // Subscript variable
    assert(subscrVar->hasProperty("domainSubscript"));
    std::vector<DataObject> indices;
    for(const auto& idx : aIndices)
    {
      indices.push_back(idx);
    }
    (subscrVar->lookupProperty("domainSubscript")).setClassObject( createListObjectSPtr(indices) );
    
    assert(aVar->hasProperty("semantic"));
    subscrVar->lookupProperty("semantic") = DataObject(getVarSemantic(aVar));
    
    return subscrVar;
  }//createObjectMatrixVariable
  
}}// end namespace Interpreter/ObjectTools
