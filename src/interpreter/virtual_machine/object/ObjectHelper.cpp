// Self first
#include "ObjectHelper.hpp"

#include "AES.hpp"
#include "CLIUtils.hpp"
#include "ObjectTools.hpp"
#include "InterpreterDefs.hpp"

#include "rapidxml-1.13/rapidxml.hpp"
#include <boost/filesystem.hpp>
#include <boost/dll.hpp>

#include <iostream>
#include <sstream>
#include <exception>
#include <algorithm>
#include <fstream>
#include <cassert>

namespace otools = Interpreter::ObjectTools;

namespace Interpreter { namespace ObjectTools {

  // Prototypes for "isObjectInstance" //
  
  // Returns a prototype "dummy" object for a Scalar object
  static const BaseObjectSPtr& getPrototypeScalar()
  {
    static BaseObjectSPtr proto(otools::createScalarObject(DataObject(1)));
    return proto;
  }//getPrototypeScalar
  
  // Returns a prototype "dummy" object for a List object
  static const BaseObjectSPtr& getPrototypeList()
  {
    static BaseObjectSPtr proto = otools::createListObjectSPtr({});
    return proto;
  }//getPrototypeList
  
  // Returns a prototype "dummy" object for a Matrix object
  static const BaseObjectSPtr& getPrototypeMatrix()
  {
    static BaseObjectSPtr proto(otools::createMatrixObject({DataObject(1)}));
    return proto;
  }//getPrototypeMatrix
  
  static const BaseObjectSPtr& getPrototypeSearch()
  {
    static DataObject dobj;
    if (!isClassObjectType(dobj))
    {
      dobj.setClassObject(std::shared_ptr<BaseObject>(createListObject({})));
    }
    static BaseObjectSPtr proto(otools::createObjectSearch(dobj));
    return proto;
  }//getPrototypeSearch
  
  static const BaseObjectSPtr& getPrototypeConstraint()
  {
    static ObjectFcn* doFcn = new ObjectFcn("fcn");
    static DataObject dobj("callbackFcn");
    if(!isFunctionType(dobj)) dobj.setFcnObject(doFcn);
    
    static BaseObjectSPtr proto(otools::createObjectConstraint(dobj));
    return proto;
  }//getPrototypeVariable
  
  static const BaseObjectSPtr& getPrototypeVariable()
  {
    static BaseObjectSPtr proto(otools::createObjectVariable("var", 0, {}));
    return proto;
  }//getPrototypeVariable
  
  static void vectorizeMatrixRow(const Interpreter::DataObject& aDO, std::vector<DataObject>& aMat)
  {
    if (Interpreter::isPrimitiveType(aDO) ||
        (isClassObjectType(aDO) && otools::isScalarObject(aDO.getClassObject().get())))
    {
      aMat.push_back(aDO);
      return;
    }
    
    assert(Interpreter::isClassObjectType(aDO) && isListObject(aDO.getClassObject().get()));
    for (const auto& list : otools::getListValues((aDO.getClassObject().get())).getCompose())
    {
      vectorizeMatrixRow(list, aMat);
    }
  }//vectorizeMatrixRow
  
	static std::vector<std::string> getXMLFileNames()
	{
		using Path = boost::filesystem::path;

		std::vector<std::string> fileNameList;

    auto exePath = boost::dll::program_location().parent_path();
    auto resPath = exePath / "resources";

    Path rootPath;
    // if "resources" is near executable use exe path as rootpath
    // else use getcwd
    if( boost::filesystem::is_directory(resPath) ){
      rootPath = exePath;
    } else {
      rootPath = Path(CLI::Utils::getRootPath().c_str());
    }

		// Get path to root
    // Path rootPath(CLI::Utils::getRootPath().c_str());
    rootPath = rootPath / "resources";
    rootPath = rootPath / "r1";
    //rootPath = rootPath / "xml";
    //rootPath = rootPath / "objects";

		// Create folder path
		Path pDir(rootPath.c_str());

		boost::filesystem::directory_iterator endIt;

    // Cycle through the directory
    for (boost::filesystem::directory_iterator it(pDir); it != endIt; ++it)
    {
      // If it's not a directory, list it
      if (is_regular_file(it->path()))
      {
        // Check the extension and report only XML files
        /*
         if (it->path().extension().string() == ".xml")
         {
         fileNameList.push_back(it->path().string());
         }*/
        if (isdigit(it->path().string().back()))
        {
          fileNameList.push_back(it->path().string());
        }
      }
    }// for
  
		return fileNameList;
	}//getXMLFileNames

	ObjectHelper& ObjectHelper::getInstance()
  {
    static ObjectHelper instance;
    return instance;
  }//getInstance

	ObjectHelper::ObjectHelper()
	{
		// Load the specifications for the objects
		// into the internal data structures
		loadObjectSpecifications();
	}
  
  void initStaticObjectInstances()
  {
    getPrototypeScalar();
    getPrototypeList();
    getPrototypeMatrix();
    getPrototypeSearch();
    getPrototypeConstraint();
    getPrototypeVariable();
  }//initStaticObjectInstances

	void ObjectHelper::loadObjectSpecifications()
	{
		// Load all the filenames of the xml constraint description files
		// under the given folder
    
    // Prepare the decryptor
    Base::Tools::AES aes("OptiLab-Beta-v01");
		auto xmlFileList = getXMLFileNames();
		for (auto& xml : xmlFileList)
		{
			// Read the xml file into a vector
			std::ifstream xmlFile(xml, std::ios::binary);
			assert(xmlFile.is_open());
      
      // Get the string file representation
      std::string fileContent((std::istreambuf_iterator<char>(xmlFile)),
                              std::istreambuf_iterator<char>());
      
      // Decode the content of the file
      fileContent = aes.decryptMsg(fileContent);
      std::vector<char> buffer(fileContent.begin(), fileContent.end());
      buffer.push_back('\0');

			// For each xml load the corresponding constraint specification
			 parseObjectSpecification(buffer);
		}
	}//loadObjectSpecifications

	std::size_t ObjectHelper::getIDForClassName(const std::string& aClassName) const
	{
		for (const auto& obj : pObjRegister)
		{
			if (obj.first == aClassName) return obj.second->id;
		}
		throw std::logic_error("Class not found");
	}//getIDForClassName

	std::string ObjectHelper::getClassNameForObjectID(std::size_t aID) const 
	{
		if (aID >= pObjRegister.size()) return "";
		return pObjRegister[aID].second->name;
	}//getClassNameForObjectID

	bool ObjectHelper::isObjectClassRegistered(const std::string& aClassName) const
	{
		for (const auto& obj : pObjRegister)
		{
			if (obj.first == aClassName) return true;
		}
		return false;
	}//isObjectClassRegistered

	const ObjectHelper::PropertySpecList& ObjectHelper::getPropertyListForObjectClass(const std::string& aClassName) const
	{
		for (const auto& obj : pObjRegister)
		{
			if (obj.first == aClassName) return obj.second->properties;
		}
		throw std::logic_error("Class not found");
	}//getPropertyListForObjectClass

	const ObjectHelper::PropertySpecList& ObjectHelper::getPropertyListForObjectClass(std::size_t aID) const
	{
		auto className = getClassNameForObjectID(aID);
		return getPropertyListForObjectClass(className);
	}//getPropertyListForObjectClass

	void ObjectHelper::parseObjectSpecification(std::vector<char>& aSpec)
	{
		rapidxml::xml_document<> doc;
		rapidxml::xml_node<> * rootNode;

		// Parse the buffer using the xml file parsing library into doc 
		doc.parse<0>(&aSpec[0]);

		// Find root node
		rootNode = doc.first_node();
		if (!rootNode) return;

		// Parse the document
		auto spec = std::make_shared<ObjectSpec>();

		// Get the name of the class and make sure it is not already registered
		std::string rootName(rootNode->first_attribute()->value());
		assert(!isObjectClassRegistered(rootName));

		// Set the name of the object class
		spec->name = rootName;

		// Set the id for the object 
		spec->id = pObjRegister.size();
		for (rapidxml::xml_node<>* node = rootNode->first_node(); node; node = node->next_sibling())
		{
			std::string name(node->name());
			if (name == "properties")
			{
				// Iterate over the properties
				for (rapidxml::xml_node<> * argNode = node->first_node("property"); argNode; argNode = argNode->next_sibling())
				{

					PropertySpec propSpec;
					for (rapidxml::xml_node<> * propNode = argNode->first_node(); propNode; propNode = propNode->next_sibling())
					{
						std::string prop(propNode->name());
						if (prop == "name")
						{
							std::string val(propNode->value());
							propSpec.name = val;
						}
						else if (prop == "hidden")
						{
							std::string val(propNode->value());
							propSpec.hidden = val == "true";
						}
						else if (prop == "constant")
						{
							std::string val(propNode->value());
							propSpec.constant = val == "true";
						}
						else
						{
							assert(false);
						}
					}
					(spec->properties).push_back(propSpec);
				}// property

			}// properties

		}//rootNode

		 // Add the object to the object register
		pObjRegister.push_back({ rootName, spec });
	}//parseObjectSpecification

	BaseObject* ObjectHelper::createObjectInstance(const std::string& aClassName,
                                                 BaseObject* aBaseClass) const
	{
		auto propList = getPropertyListForObjectClass(aClassName);
    
		// Create a new base object deriving from a base class if not nullptr
		auto obj = otools::createObject(aClassName);
		if (aBaseClass)
		{
			obj->deriveFrom(aBaseClass);
		}
    
		// Add all the properties specified in the list
		for (const auto& prop : propList)
		{
      obj->addProperty(prop.name);
			if (prop.hidden)
			{
				obj->setPropertyVisibility(prop.name, false);
			}
		}//for

		return obj;
	}//createObjectInstance
  
  std::string getScalarClassName()
  {
    static std::string className{"Scalar"};
    return className;
  }//getScalarClassName
  
  bool isScalarObject(BaseObject* aObj)
  {
    return aObj &&
    aObj->isInstanceOf(getPrototypeScalar().get()) &&
    (aObj->lookupProperty(getTypePropName())).getDataValue<std::string>() == getScalarClassName();
  }//isScalarObject
  
  DataObject& getScalarValue(BaseObject* aObj)
  {
    assert(isScalarObject(aObj));
    return aObj->lookupProperty("data");
  }//getScalarValue
  
  std::string getListClassName()
  {
    static std::string className{"List"};
    return className;
  }//getListClassName
  
  bool isListObject(BaseObject* aObj)
  {
    return aObj &&
    aObj->isInstanceOf(getPrototypeList().get()) &&
    (aObj->lookupProperty(getTypePropName())).getDataValue<std::string>() == getListClassName();
  }//isListObject
  
  int getListSize(BaseObject* aObj)
  {
    assert(isListObject(aObj));
    auto size = aObj->getProperty("size");
    
    assert(size.dataObjectType() == DataObject::DataObjectType::DOT_INT);
    return size.getDataValue<int>();
  }//getListSize
  
  void setListSize(BaseObject* aObj, int aSize)
  {
    assert(isListObject(aObj));
    
    // Negative sizes truncate to zero
    aSize = aSize < 0 ? 0 : aSize;
    aObj->setProperty("size", aSize);
  }//getListSize
  
  DataObject& getListValues(BaseObject* aObj)
  {
    assert(isListObject(aObj));
    assert(aObj->hasProperty("data"));
    return aObj->lookupProperty("data");
  }//getListValues
  
  BaseObject* concatLists(BaseObject* aList1, BaseObject* aList2)
  {
    assert(isListObject(aList1) && isListObject(aList2));
    DataObject concatList = getListValues(aList1);
    
    auto& compConcat = concatList.getCompose();
    for (const auto& aDOList2 : getListValues(aList2))
    {
      compConcat.push_back(aDOList2);
    }
    
    return createListObject(compConcat);
  }//concatLists
  
  DataObject& getListElement(BaseObject* aList, const DataObject& aIdx)
  {
    assert(isListObject(aList));
    itp_assert(aIdx.dataObjectType() != DataObject::DataObjectType::DOT_STRING,
               InterpreterException::ExceptionType::ET_WRONG_TYPE);
    
    auto idx = aIdx.castDataValue<int>();
    itp_assert(idx >= 0 && idx < getListSize(aList), InterpreterException::ExceptionType::ET_OUT_OF_BOUND);
    
    return getListValues(aList)[static_cast<int>(idx)];
  }//getListElement
  
  void setListElement(BaseObject* aList, const DataObject& aIdx, const DataObject& aElem)
  {
    assert(isListObject(aList));
    itp_assert(aIdx.dataObjectType() != DataObject::DataObjectType::DOT_STRING,
               InterpreterException::ExceptionType::ET_WRONG_TYPE);
    
    auto idx = aIdx.castDataValue<int>();
    itp_assert(idx >= 0 && idx < getListSize(aList),
               InterpreterException::ExceptionType::ET_OUT_OF_BOUND);
    
    // Set the element in the list
    getListValues(aList)[static_cast<int>(idx)] = aElem;
  }//getListElement
  
  std::string getMatrixClassName()
  {
    static std::string className{"Matrix"};
    return className;
  }//getMatrixClassName
  
  bool isMatrixObject(BaseObject* aObj)
  {
    return aObj &&
    aObj->isInstanceOf(getPrototypeMatrix().get()) &&
    (aObj->lookupProperty(getTypePropName())).getDataValue<std::string>() == getMatrixClassName();
  }//isMatrixObject
  
  std::vector<int> getMatrixDimensions(BaseObject* aObj)
  {
    assert(isMatrixObject(aObj));
    const auto& matDims = aObj->getProperty("dims");
    
    std::vector<int> dims;
    assert(isClassObjectType(matDims) && otools::isListObject(matDims.getClassObject().get()));
    for (const auto& dim : getListValues(matDims.getClassObject().get()))
    {
      dims.push_back(dim.getDataValue<int>());
    }
    return dims;
  }//getMatrixDimensions
  
  std::vector<DataObject> getVectorizedMatrixData(BaseObject* aObj)
  {
    assert(isMatrixObject(aObj));
    std::vector<DataObject> mat;
    
    // Get the data from the matrix
    const auto& data = aObj->getProperty("data");
    assert(Interpreter::isClassObjectType(data) && isListObject(data.getClassObject().get()));
    
    vectorizeMatrixRow(data, mat);
    return mat;
  }//getVectorizedMatrixData
  
  DataObject& getMatrixElement(BaseObject* aMatrix, const DataObject& aIdx)
  {
    assert(isMatrixObject(aMatrix));
    
    auto dims = getMatrixDimensions(aMatrix);
    itp_assert(!dims.empty() && dims[0] > 0, InterpreterException::ExceptionType::ET_OUT_OF_BOUND);
    
    if (isPrimitiveType(aIdx))
    {
      Interpreter::DataObject indexing;
      if (dims.size() == 2 && dims[0] == 1)
      {
        // Set the first index to 0 in case of vectors
        indexing.compose({Interpreter::DataObject(0), aIdx});
      }
      else
      {
        indexing.compose({aIdx});
      }
      
    
      return getMatrixElement(aMatrix, indexing);
    }
    
    if (isClassObjectType(aIdx))
    {
      itp_assert(otools::isScalarObject(aIdx.getClassObject().get()),
                 InterpreterException::ExceptionType::ET_WRONG_TYPE);
      
      Interpreter::DataObject indexing;
      if (dims.size() == 2 && dims[0] == 1)
      {
        // Set the first index to 0 in case of vectors
        indexing.compose({Interpreter::DataObject(0),
          otools::getScalarValue(aIdx.getClassObject().get())});
      }
      else
      {
        indexing.compose({otools::getScalarValue(aIdx.getClassObject().get())});
      }

      return getMatrixElement(aMatrix, indexing);
    }
    
    // Indexing must be a composite object
    itp_assert(isCompositeType(aIdx), InterpreterException::ExceptionType::ET_WRONG_TYPE);
    if (aIdx.composeSize() == 1 && dims.size() == 2 && dims[0] == 1)
    {
      // If the index argument is a single scalar object and the matrix is actually a vector,
      // add a dimension (the row dimension) to the indexing
      Interpreter::DataObject indexing;
      indexing.compose({Interpreter::DataObject(0), aIdx.getCompose()[0]});
      return getMatrixElement(aMatrix, indexing);
    }
    
    // Recursively descend into the inner lists identified by their indices
    assert(isClassObjectType(aMatrix->lookupProperty("data")));
    auto dataList = (aMatrix->lookupProperty("data")).getClassObject().get();
    
    assert(isListObject(dataList));
    for (std::size_t idx{ 0 }; idx < aIdx.composeSize() - 1; ++idx)
    {
      dataList = getListElement(dataList, aIdx[idx]).getClassObject().get();
      assert(isListObject(dataList));
    }
    return getListElement(dataList, aIdx.getCompose().back());
  }//getMatrixElement
  
  void setMatrixElement(BaseObject* aMatrix, const DataObject& aIdx, const DataObject& aElem)
  {
    assert(isMatrixObject(aMatrix));
    
    auto dims = getMatrixDimensions(aMatrix);
    itp_assert(!dims.empty() && dims[0] > 0, InterpreterException::ExceptionType::ET_OUT_OF_BOUND);
    
    if (isPrimitiveType(aIdx))
    {
      Interpreter::DataObject indexing;
      if (dims.size() == 2 && dims[0] == 1)
      {
        // Set the first index to 0 in case of vectors
        indexing.compose({Interpreter::DataObject(0), aIdx});
      }
      else
      {
        indexing.compose({aIdx});
      }
      
      setMatrixElement(aMatrix, indexing, aElem);
      return;
    }
    
    if (isClassObjectType(aIdx))
    {
      itp_assert(otools::isScalarObject(aIdx.getClassObject().get()),
                 InterpreterException::ExceptionType::ET_WRONG_TYPE);
      
      Interpreter::DataObject indexing;
      if (dims.size() == 2 && dims[0] == 1)
      {
        // Set the first index to 0 in case of vectors
        indexing.compose({Interpreter::DataObject(0),
          otools::getScalarValue(aIdx.getClassObject().get())});
      }
      else
      {
        indexing.compose({otools::getScalarValue(aIdx.getClassObject().get())});
      }
      
      setMatrixElement(aMatrix, indexing, aElem);
      return;
    }
    
    // Indexing must be a composite object
    itp_assert(isCompositeType(aIdx), InterpreterException::ExceptionType::ET_WRONG_TYPE);
    if (aIdx.composeSize() == 1 && dims.size() == 2 && dims[0] == 1)
    {
      // If the index argument is a single scalar object and the matrix is actually a vector,
      // add a dimension (the row dimension) to the indexing
      Interpreter::DataObject indexing;
      indexing.compose({Interpreter::DataObject(0), aIdx.getCompose()[0]});
      setMatrixElement(aMatrix, indexing, aElem);
    }
    
    // Recursively descend into the inner lists identified by their indices
    assert(isClassObjectType(aMatrix->lookupProperty("data")));
    auto dataList = (aMatrix->lookupProperty("data")).getClassObject().get();
    
    assert(isListObject(dataList));
    for (std::size_t idx{ 0 }; idx < aIdx.composeSize() - 1; ++idx)
    {
      dataList = getListElement(dataList, aIdx[idx]).getClassObject().get();
      assert(isListObject(dataList));
    }
    setListElement(dataList, aIdx.getCompose().back(), aElem);
  }//setMatrixElement
  
  BaseObject* concatMatrix(BaseObject* aMatrix1, BaseObject* aMatrix2)
  {
    assert(isMatrixObject(aMatrix1) && isMatrixObject(aMatrix2));
    const auto& dims1 = getParameterDimensions(aMatrix1);
    const auto& dims2 = getParameterDimensions(aMatrix2);
    itp_assert(dims1.size() == dims2.size(), InterpreterException::ExceptionType::ET_OUT_OF_BOUND);
    for (std::size_t idx{1}; idx < dims1.size(); ++idx)
    {
      itp_assert(dims1[idx] == dims2[idx], InterpreterException::ExceptionType::ET_OUT_OF_BOUND);
    }
    const auto& data1 = aMatrix1->lookupProperty("data");
    const auto& data2 = aMatrix2->lookupProperty("data");
    assert(isClassObjectType(data1) && isClassObjectType(data2));
    assert(isListObject(data1.getClassObject().get()) &&
           isListObject(data2.getClassObject().get()));
    const auto& list1 = getListValues(data1.getClassObject().get());
    const auto& list2 = getListValues(data2.getClassObject().get());
    
    std::vector<DataObject> composite;
    composite.reserve(list1.composeSize() + list2.composeSize());
    for (std::size_t idx{0}; idx < list1.composeSize(); ++idx)
    {
      composite.push_back(list1[idx]);
    }
    for (std::size_t idx{0}; idx < list2.composeSize(); ++idx)
    {
      composite.push_back(list2[idx]);
    }
    return createMatrixObject(composite);
  }//concatMatrix
  
  std::string getSearchClassName()
  {
    static std::string className{"Search"};
    return className;
  }//getSearchClassName
  
  bool isSearchObject(BaseObject* aObj)
  {
    return aObj &&
    aObj->isInstanceOf(getPrototypeSearch().get()) &&
    (aObj->lookupProperty(getTypePropName())).getDataValue<std::string>() ==
    getSearchClassName();
  }//isSearchObject
  
  DataObject& getSearchScope(BaseObject* aObj)
  {
    assert(isSearchObject(aObj));
    return aObj->lookupProperty("scope");
  }//getSearchScope
  
  DataObject& getSearchOptions(BaseObject* aObj)
  {
    assert(isSearchObject(aObj));
    return aObj->lookupProperty("searchOptions");
  }//getSearchOptions
  
  std::string getConstraintClassName()
  {
    static std::string className{"Constraint"};
    return className;
  }//getConstraintClassName
  
  bool isConstraintObject(BaseObject* aObj)
  {
    return aObj &&
    aObj->isInstanceOf(getPrototypeConstraint().get()) &&
    (aObj->lookupProperty(getTypePropName())).getDataValue<std::string>() ==
    getConstraintClassName();
  }//isConstraintObject
  
  DataObject& getConstraintPostFcn(BaseObject* aObj)
  {
    assert(isConstraintObject(aObj));
    return aObj->lookupProperty("__post_constraint_fcn");
  }//getConstraintPostFcn
  
  std::string getVariableClassName()
  {
    static std::string className{"Variable"};
    return className;
  }//getVariableClassName
  
  bool isVariableObject(BaseObject* aObj)
  {
    return aObj &&
    aObj->isInstanceOf(getPrototypeVariable().get()) &&
    (aObj->lookupProperty(getTypePropName())).getDataValue<std::string>() ==
    getVariableClassName();
  }//isVariableObject
  
  std::string getVarID(BaseObject* aObj)
  {
    assert(isVariableObject(aObj));
    assert(aObj->hasProperty(getObjectIDPropName()));
    return otools::getObjectPropertyValue<std::string>(aObj, getObjectIDPropName());
  }//getVarID
  
  void setVarID(BaseObject* aObj, const std::string& aID)
  {
    assert(isVariableObject(aObj));
    assert(aObj->hasProperty(getObjectIDPropName()));
    setObjectPropertyValue(aObj, getObjectIDPropName(), aID);
  }//setVarID
  
  int getVarSemantic(BaseObject* aObj)
  {
    assert(isVariableObject(aObj));
    assert(aObj->hasProperty("semantic"));
    return otools::getObjectPropertyValue<int>(aObj, "semantic");
  }//getVarSemantic
  
  void setVarSemantic(BaseObject* aObj, int aSem)
  {
    assert(isVariableObject(aObj));
    assert(aObj->hasProperty("semantic"));
    setObjectPropertyValue(aObj, "semantic", aSem);
  }//setVarSemantic
  
  std::size_t getVarInputOrder(BaseObject* aObj)
  {
    assert(isVariableObject(aObj));
    assert(aObj->hasProperty("inputOrder"));
    return otools::getObjectPropertyValue<std::size_t>(aObj, "inputOrder");
  }//getVarInputOrder
  
  void setVarInputOrder(BaseObject* aObj, std::size_t aInputOrder)
  {
    assert(isVariableObject(aObj));
    assert(aObj->hasProperty("inputOrder"));
    setObjectPropertyValue(aObj, "inputOrder", aInputOrder);
  }//setVarInputOrder
  
  int getVarDomainType(BaseObject* aObj)
  {
    assert(isVariableObject(aObj));
    assert(aObj->hasProperty("domainType"));
    return otools::getObjectPropertyValue<int>(aObj, "domainType");
  }//getVarDomainType
  
  void setVarDomainType(BaseObject* aObj, int aDomType)
  {
    assert(isVariableObject(aObj));
    assert(aObj->hasProperty("domainType"));
    setObjectPropertyValue(aObj, "domainType", aDomType);
  }//setVarDomainType
  
  DataObject& getVarDomain(BaseObject* aObj)
  {
    assert(isVariableObject(aObj));
    assert(aObj->hasProperty("domain"));
    return aObj->lookupProperty("domain");
  }//getVarDomain
  
  BaseObject* getVarDomainList(BaseObject* aObj)
  {
    assert(isVariableObject(aObj));
    return getVarDomain(aObj).getClassObject().get();
  }//getVarDomainList
  
  DataObject& getVarDomainDimensions(BaseObject* aObj)
  {
    assert(isVariableObject(aObj));
    assert(aObj->hasProperty("domainDimensions"));
    return aObj->lookupProperty("domainDimensions");
  }//getVarDomainDimensions
  
  BaseObject* getVarDomainDimensionsList(BaseObject* aObj)
  {
    assert(isVariableObject(aObj));
    return getVarDomainDimensions(aObj).getClassObject().get();
  }//getVarDomainDimensionsList
  
  std::size_t getVarDomainNumDimensions(BaseObject* aObj)
  {
    assert(isVariableObject(aObj));
    return getListSize(getVarDomainDimensionsList(aObj));
  }//getVarDomainNumDimensions
  
  std::pair<int, int> getVarDomainDimensionsVals(BaseObject* aObj)
  {
    auto numDims = getVarDomainNumDimensions(aObj);
    int dim1{0};
      int dim2{0};
    if(numDims > 0)
    {
      const auto& dims = getListValues(getVarDomainDimensionsList(aObj));
      if (isClassObjectType(dims[0]))
      {
        auto obj = dims[0].getClassObject().get();
        assert(isScalarObject(obj) &&
               (getScalarValue(obj).dataObjectType() == DataObject::DataObjectType::DOT_INT));
        dim1 = getScalarValue(obj).getDataValue<int>();
      }
      else
      {
        assert(dims[0].dataObjectType() == DataObject::DataObjectType::DOT_INT);
        dim1 = dims[0].getDataValue<int>();
      }
      
      if (numDims == 2)
      {
        if (isClassObjectType(dims[1]))
        {
          auto obj = dims[1].getClassObject().get();
          assert(isScalarObject(obj) &&
                 (getScalarValue(obj).dataObjectType() == DataObject::DataObjectType::DOT_INT));
          dim2 = getScalarValue(obj).getDataValue<int>();
        }
        else
        {
          assert(dims[1].dataObjectType() == DataObject::DataObjectType::DOT_INT);
          dim2 = dims[1].getDataValue<int>();
        }
      }
    }
    return {dim1, dim2};
  }//getVarDomainDimensionsVals
  
  int getVarDomainDimensionsSize(BaseObject* aObj)
  {
    auto dims = getVarDomainDimensionsVals(aObj);
    auto size = 0;
    if(dims.first > 0)
    {
      size = dims.second > 0 ? dims.first * dims.second : dims.first;
    }
    return size;
  }//getVarDomainDimensionsSize
  
  DataObject& getVarDomainArray(BaseObject* aObj)
  {
    assert(isVariableObject(aObj));
    assert(aObj->hasProperty("domainArray"));
    return aObj->lookupProperty("domainArray");
  }//getVarDomainArray
  
  BaseObject* getVarDomainArrayList(BaseObject* aObj)
  {
    assert(isVariableObject(aObj));
    return getVarDomainArray(aObj).getClassObject().get();
  }//getVarDomainArrayList
  
  DataObject& getVarDomainSubscriptIndices(BaseObject* aObj)
  {
    assert(isVariableObject(aObj));
    assert(aObj->hasProperty("domainSubscript"));
    return aObj->lookupProperty("domainSubscript");
  }//getVarDomainSubscriptIndices
  
  BaseObject* getVarDomainSubscriptIndicesList(BaseObject* aObj)
  {
    assert(isVariableObject(aObj));
    return getVarDomainSubscriptIndices(aObj).getClassObject().get();
  }//getVarDomainSubscriptIndicesList
  
  bool isVarSubscript(BaseObject* aObj)
  {
    return isVariableObject(aObj) &&
    getListSize(getVarDomainSubscriptIndicesList(aObj)) > 0;
  }//isVarSubscript
  
  DataObject& getVarElement(BaseObject* aObj, const DataObject& aIndices)
  {
    assert(isVariableObject(aObj));
    itp_assert(aIndices.isComposite(), InterpreterException::ExceptionType::ET_WRONG_TYPE);
    itp_assert(aIndices.composeSize() == getVarDomainNumDimensions(aObj),
               InterpreterException::ExceptionType::ET_OUT_OF_BOUND);
    auto pairDims = getVarDomainDimensionsVals(aObj);
    
    auto domainArray = getVarDomainArrayList(aObj);
    auto domainArraySize = getListSize(domainArray);
    int idx = -1;
    
    // Set the index into the array
    if (aIndices.composeSize() == 1)
    {
      itp_assert(aIndices[0].dataObjectType() == DataObject::DataObjectType::DOT_INT,
                 InterpreterException::ExceptionType::ET_WRONG_TYPE);
      idx = aIndices[0].getDataValue<int>();
      
      itp_assert(idx < pairDims.first, InterpreterException::ExceptionType::ET_OUT_OF_BOUND);
    }
    else
    {
      assert(aIndices.composeSize() == 2);
      itp_assert(aIndices[0].dataObjectType() == DataObject::DataObjectType::DOT_INT,
                 InterpreterException::ExceptionType::ET_WRONG_TYPE);
      itp_assert(aIndices[1].dataObjectType() == DataObject::DataObjectType::DOT_INT,
                 InterpreterException::ExceptionType::ET_WRONG_TYPE);
      auto idx1 = aIndices[0].getDataValue<int>();
      auto idx2 = aIndices[1].getDataValue<int>();
      
      itp_assert(idx1 < pairDims.first, InterpreterException::ExceptionType::ET_OUT_OF_BOUND);
      itp_assert(idx2 < pairDims.second, InterpreterException::ExceptionType::ET_OUT_OF_BOUND);
      
      // @note value == array[width * row + col];
      idx = pairDims.second * idx1 + idx2;
    }
    assert(idx >= 0);
    
    // If the domain array is empty, use the variable domain
    if (domainArraySize == 0)
    {
      return getVarDomain(aObj);
    }
    
    // Otherwise return the domain at given position
    return getListElement(domainArray, DataObject(idx));
  }//getVarElement
  
  bool isParameterObject(BaseObject* aObj)
  {
    return isMatrixObject(aObj) ||
    isListObject(aObj)          ||
    isScalarObject(aObj);
  }//isParameterObject
  
  bool isScalarParameterObject(BaseObject* aObj)
  {
    return isScalarObject(aObj);
  }//isScalarParameterObject
  
  bool isListParameterObject(BaseObject* aObj)
  {
    return isListObject(aObj);
  }//isScalarParameterObject
  
  bool isMatrixParameterObject(BaseObject* aObj)
  {
    return isMatrixObject(aObj);
  }//isMatrixParameterObject
  
  DataObject& getScalarParameterValue(BaseObject* aObj)
  {
    return getScalarValue(aObj);
  }//getScalarParameterValue
  
  std::vector<int> getParameterDimensions(BaseObject* aObj)
  {
    if (isMatrixParameterObject(aObj))
    {
      auto dimsList = getParameterDimensionsList(aObj);
      
      std::vector<int> dims;
      for(const auto& dim : getListValues(dimsList))
      {
        assert(dim.dataObjectType() == DataObject::DataObjectType::DOT_INT);
        dims.push_back(dim.getDataValue<int>());
      }
      return dims;
    }
    else if (isScalarObject(aObj))
    {
      return {1};
    }
    else if (isListObject(aObj))
    {
      // List objects have 1 dimension (number of columns)
      // For example: [1, 2, 3, 4] => [4]
      // Technically it should be [1, 4] but the conventions used in the code
      // is that when there is only one dimensions, that refers to the number of rows
      return {getListSize(aObj)};
    }
    else
    {
      // Anything else doesn't have dimensions
      return {};
    }
  }//getParameterDimensions
  
  BaseObject* getParameterDimensionsList(BaseObject* aObj)
  {
    // Get the dimensions which are represented as a list object
    assert(isParameterObject(aObj));
    if (isMatrixObject(aObj))
    {
      assert(aObj->hasProperty("dims") && (aObj->lookupProperty("dims")).isClassObject() &&
             isListObject((aObj->lookupProperty("dims")).getClassObject().get()));
      
      return (aObj->lookupProperty("dims")).getClassObject().get();
    }
    else if (isListObject(aObj))
    {
      return createListObject({ DataObject(getListSize(aObj)) });
    }
    else
    {
      return createListObject({ DataObject(1) });
    }
  }//getParameterDimensionsList
  
  DataObject& getParameterValues(BaseObject* aObj)
  {
    assert(isParameterObject(aObj));
    assert(aObj->hasProperty("data"));
    return aObj->lookupProperty("data");
  }//getParameterValues
  
  BaseObject* getParameterValuesList(BaseObject* aObj)
  {
    assert(isParameterObject(aObj));
    return getParameterValues(aObj).getClassObject().get();
  }//getParameterValuesList
  
  BaseObject* concatParameters(BaseObject* aParam1, BaseObject* aParam2)
  {
    assert(isParameterObject(aParam1) && isParameterObject(aParam2));
    
    // Concatenation of parameters must be done considering different combinations:
    // a) scalar-scalar -> list - list
    // b) scalar-list   -> list - list
    // c) scalar-matrix -> list - matrix -> matrix - matrix
    if (isScalarObject(aParam1))
    {
      // a -> [a]
      aParam1 = createListObject({getScalarValue(aParam1)});
    }
    
    if (isScalarObject(aParam2))
    {
      // b -> [b]
      aParam2 = createListObject({getScalarValue(aParam2)});
    }
    
    if (isListObject(aParam1) && isListObject(aParam2))
    {
      // [a] . [b] = [a, b]
      return concatLists(aParam1, aParam2);
    }
    
    if (isListObject(aParam1))
    {
      Interpreter::DataObject listParam1;
      listParam1.setClassObject(std::shared_ptr<BaseObject>(aParam1->clone()));
      aParam1 = createMatrixObject({listParam1});
    }
    
    if (isListObject(aParam2))
    {
      Interpreter::DataObject listParam2;
      listParam2.setClassObject(std::shared_ptr<BaseObject>(aParam2->clone()));
      aParam2 = createMatrixObject({listParam2});
    }
    assert(isMatrixObject(aParam1) && isMatrixObject(aParam1));
    return concatMatrix(aParam1, aParam2);
  }//concatParameters
  
  DataObject& getParameterElement(BaseObject* aParam, const DataObject& aIndices)
  {
    if (isListObject(aParam))
    {
      itp_assert(aIndices.isComposite() && (aIndices.composeSize() == 1),
                 InterpreterException::ExceptionType::ET_WRONG_TYPE);
      return getListElement(aParam, aIndices[0]);
    }
    else
    {
      assert(isMatrixObject(aParam));
      return getMatrixElement(aParam, aIndices);
    }
  }//getParameterElement
  
  void setParameterElement(BaseObject* aParam, const DataObject& aIndices,
                           const DataObject& aElement)
  {
    if (isListObject(aParam))
    {
      itp_assert(aIndices.isComposite() && (aIndices.composeSize() == 1),
                 InterpreterException::ExceptionType::ET_WRONG_TYPE);
      setListElement(aParam, aIndices[0], aElement);
    }
    else
    {
      assert(isMatrixObject(aParam));
      setMatrixElement(aParam, aIndices, aElement);
    }
  }//getParameterElement
  
  std::string getSearchOptionsBaseClassName()
  {
    static std::string className{"SearchOptions"};
    return className;
  }//getSearchOptionsBaseClassName
  
}}// end namespace Interpreter/ObjectTools
