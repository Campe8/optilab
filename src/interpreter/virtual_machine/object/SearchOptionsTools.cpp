// Self first
#include "SearchOptionsTools.hpp"

#include "ObjectHelper.hpp"

// SearchInformation object specifications
#include "DFSInformation.hpp"
#include "GeneticInformation.hpp"

#include <cstddef>  // for std::size_t

namespace otools = Interpreter::ObjectTools;

namespace Interpreter { namespace SearchOptionsTools {
  
  /// Creates an object that derives from BaseObject and represents a
  /// search option object, holding information about search strategies.
  /// Properties of the object:
  /// 1) "objectClass": class of this object which is "SearchOptions"
  ///                  This property is not visible.
  ///
  static BaseObject* createSearchOptionsObject(Model::SearchInformation::SearchInformationType aType,
                                               const std::string& aStrategyName)
  {
    // Create an object of type "SearchOptions"
    auto obj = otools::createObject(getSearchOptionsTypeName());
    
    // Base class search options properties
    obj->addProperty(getStrategyPropName(), aStrategyName.c_str());
    obj->setConstProperty(getStrategyPropName(), true);
    
    obj->addProperty(getClassTypePropName(), static_cast<char>(aType));
    obj->setPropertyVisibility(getClassTypePropName(), false);
    
    obj->addProperty(getTimeoutPropName(), static_cast<int>(-1));
    
    return obj;
  }//createSearchOptionsObject
  
  // Returns a prototype "dummy" object for a SearchOptions object
  static const std::unique_ptr<BaseObject>& getPrototype()
  {
    static Model::SearchInformation::SearchInformationType type =
           Model::SearchInformation::SearchInformationType::SI_DFS;
    static std::unique_ptr<BaseObject> proto(createSearchOptionsObject(type, ""));
    return proto;
  }//getPrototype
  
  std::string getSearchOptionsTypeName()
  {
    static std::string searchOptionsBaseClassName{"SearchOptions"};
    return searchOptionsBaseClassName;
  }//getSearchOptionsTypeName
  
  std::string getCBLSSearchOptionsTypeName()
  {
    static std::string className{"CBLSSearchOptions"};
    return className;
  }//getCBLSSearchOptionsTypeName
  
  std::string getDFSSearchOptionsTypeName()
  {
    static std::string className{"DFSSearchOptions"};
    return className;
  }//getDFSSearchOptionsTypeName
  
  std::string getGeneticSearchOptionsTypeName()
  {
    static std::string className{"GeneticSearchOptions"};
    return className;
  }//getGeneticSearchOptionsTypeName
  
  BaseObject::PropertyKey getStrategyPropName()
  {
    static std::string propName = "strategy";
    return propName;
  }//getStrategyPropName
  
  BaseObject::PropertyKey getClassTypePropName()
  {
    static std::string propName = "classType";
    return propName;
  }//getClassTypePropName
  
  BaseObject::PropertyKey getTimeoutPropName()
  {
    static std::string propName = Model::SearchInformation::TimeoutKey;
    return propName;
  }//getStrategyPropName
  
  /// Creates an object that derives from BaseObject and represents a
  /// CBLS search option object.
  /// Properties of the object:
  /// 1) NeighborhoodSizeKey: neighborhood size
  /// 2) NumNeighborhoodsKey: number of neighborhoods
  /// 3) AggressiveCBLSKey: aggressive CBLS flag
  ///
  static BaseObject* createCBLSSearchOptionsObject(Model::SearchInformation::SearchInformationType aType,
                                                   const std::string& aStrategyName)
  {
    // Create a new object deriving from a search options object
    auto obj = otools::createObject(getCBLSSearchOptionsTypeName());
    std::unique_ptr<BaseObject> srcOptionsObj(createSearchOptionsObject(aType, aStrategyName));
    
    obj->deriveFrom(srcOptionsObj.get());
    
    // Set CBLS default properties
    obj->addProperty(Model::CBLSInformation::RandomSeedKey, static_cast<int>(0));
    obj->addProperty(Model::CBLSInformation::WarmStartKey, static_cast<bool>(true));
    obj->addProperty(Model::CBLSInformation::NeighborhoodSizeKey, static_cast<int>(-1));
    obj->addProperty(Model::CBLSInformation::NumNeighborhoodsKey, static_cast<int>(1));
    obj->addProperty(Model::CBLSInformation::AggressiveCBLSKey, false);
    
    return obj;
  }//createCBLSSearchOptionsObject
  
  bool isSearchOptionsObject(BaseObject* aObj)
  {
    if(!aObj) return false;
    return aObj->isInstanceOf(getPrototype().get());
  }//isSearchOptionsObject
  
  char getClassType(BaseObject* aObj)
  {
    assert(isSearchOptionsObject(aObj));
    return otools::getObjectPropertyValue<char>(aObj, getClassTypePropName());
  }//getClassType
  
  BaseObject* getDFSSearchOptionsObject()
  {
    // Create a new object deriving from a search options object
    auto obj = otools::createObject(getDFSSearchOptionsTypeName());
    auto searchOptions =
    createSearchOptionsObject(Model::SearchInformation::SearchInformationType::SI_DFS,
                              Model::DFSInformation::StrategyName);
    std::unique_ptr<BaseObject>srcOptionsObj(searchOptions);
    obj->deriveFrom(srcOptionsObj.get());
    
    // Set DFS information default properties
    obj->addProperty(Model::DFSInformation::VariableSelectionKey,
                     Model::DFSInformation::getDefaultVariableChoice().c_str());
    obj->addProperty(Model::DFSInformation::ValueSelectionKey,
                     Model::DFSInformation::getDefaultValueChoice().c_str());
    
    return obj;
  }//getDFSSearchOptionsObject
  
  BaseObject* getGeneticSearchOptionsObject()
  {
    // Create a new object deriving from a search options object
    auto obj = otools::createObject(getGeneticSearchOptionsTypeName());
    obj->deriveFrom(createCBLSSearchOptionsObject(Model::SearchInformation::SearchInformationType::SI_GENETIC,
                                                  Model::GeneticInformation::StrategyName));
    
    // Set genetic information default properties
    obj->addProperty(Model::GeneticInformation::PopulationSizeKey, static_cast<std::size_t>(100));
    obj->addProperty(Model::GeneticInformation::NumGenerationsKey, static_cast<std::size_t>(500));
    obj->addProperty(Model::GeneticInformation::MutationChanceKey, static_cast<std::size_t>(70));
    obj->addProperty(Model::GeneticInformation::MutationSizeKey,   static_cast<std::size_t>(1));
    
    return obj;
  }//getGeneticSearchOptionsObject
  
}}// end namespace SearchOptionsTools/Interpreter
