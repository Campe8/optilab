// Self first
#include "BaseObject.hpp"

#include "ObjectFcn.hpp"

#include <algorithm>  // for std::find
#include <exception>

namespace Interpreter {
  
  BaseObject* newObject()
  {
    return new BaseObject();
  }//newObject
  
  BaseObject::~BaseObject()
  {
    if (pParent) delete pParent;
    pParent = nullptr;
  }
  
  BaseObject::PropertyKey BaseObject::getPrefixPropertyForRegisteringGetMethod()
  {
    static std::string prefix{ "__get__" };
    return prefix;
  }//getPrefixPropertyForRegisteringGetMethod
  
  BaseObject::PropertyKey BaseObject::getPrefixPropertyForRegisteringSetMethod()
  {
    static std::string prefix{ "__set__" };
    return prefix;
  }//getPrefixPropertyForRegisteringSetMethod
  
  BaseObject::PropertyKey BaseObject::getFullPropertyNameForRegisteringGetMethod(const std::string& aProp)
  {
    return getPrefixPropertyForRegisteringGetMethod() + aProp;
  }//getFullPropertyNameForRegisteringGetMethod
  
  BaseObject::PropertyKey BaseObject::getFullPropertyNameForRegisteringSetMethod(const std::string& aProp)
  {
    return getPrefixPropertyForRegisteringSetMethod() + aProp;
  }//getFullPropertyNameForRegisteringSetMethod
  
  void BaseObject::deriveFrom(BaseObject* aParent)
  {
    // Return if the parent is nullptr, nothing to derive from
    if (aParent == nullptr) return;
    
    // Create a clone of the parent object
    auto clonedParent = aParent->clone();
    
    // Set the clone as new parent of this object
    pParent = clonedParent;
    
    // Remove all the properties from this instance that are already
    // properties of the parent
    std::vector<PropertyKey> dupProps;
    for(const auto& it : pPropertyMap)
    {
      // Collect all duplicate properties
      if(pParent->hasProperty(it.first)) dupProps.push_back(it.first);
    }
    
    // For each duplicate property, set the parent property to this
    // current value and remove it from the property map
    for(const auto& prop : dupProps)
    {
      // Get the current value of the property
      auto propVal = getProperty(prop);
      
      // Erase the property from the map of the child
      pPropertyMap.erase(prop);
      
      // Set const according to the child
      if(std::find(pConstProperties.begin(), pConstProperties.end(), prop) ==
         pConstProperties.end())
      {
        // The property wasn't const, set it as non const in the parent
        pParent->setConstProperty(prop, false);
      }
      
      // Set visibility according to the child
      if(std::find(pHiddenProperties.begin(), pHiddenProperties.end(), prop) ==
         pHiddenProperties.end())
      {
        // The property wasn't const, set it as non const in the parent
        pParent->setPropertyVisibility(prop, true);
      }
      
      // Set the property in the parent class (even if it is constant)
      auto isConst = isConstProperty(prop);
      if(isConst) setConstProperty(prop, false);
      setProperty(prop, propVal);
      
      // Reset const property if it was const
      if(isConst) setConstProperty(prop, true);
    }
  }//deriveFrom
  
  bool BaseObject::isInstanceOf(BaseObject* aObj)
  {
    if(!aObj) return false;
    
    // For this object to be instance of the given object,
    // this object must have at least all the properties
    // that the given object has
    for(const auto& key : aObj->getKeys())
    {
      if(!hasProperty(key)) return false;
    }
    return true;
  }//isInstanceOf
  
  bool BaseObject::hasProperty(const PropertyKey& aKey) const
  {
    // Look at this object
    if (pPropertyMap.find(aKey) != pPropertyMap.end()) return true;
    
    // If not found try going up on the parent's chain
    if (pParent && pParent->hasProperty(aKey)) return true;
    
    // Property not found
    return false;
  }//hasProperty
  
  void BaseObject::removeProperty(const PropertyKey& aKey)
  {
    if (pPropertyMap.find(aKey) != pPropertyMap.end())
    {
      pPropertyMap.erase(aKey);
      return;
    }
    
    // Property is not found on this object, try on the parent if any
    if (pParent)
    {
      pParent->removeProperty(aKey);
    }
  }//removeProperty
  
  BaseObject::PropertyValue& BaseObject::lookupProperty(const PropertyKey& aKey)
  {
    // Lookup the property on this object
    if (pPropertyMap.find(aKey) != pPropertyMap.end())
    {
      return pPropertyMap[aKey];
    }
    
    if (!pParent)
    {
      throw std::logic_error("Property not found");
    }
    
    // If not found go up on the parent's chain
    return pParent->lookupProperty(aKey);
  }//getProperty
  
  BaseObject::PropertyValue BaseObject::getProperty(const PropertyKey& aKey) const
  {
    auto getFcnkey = getFullPropertyNameForRegisteringGetMethod(aKey);
    if (pPropertyMap.find(getFcnkey) != pPropertyMap.end())
    {
      // There is a get fcn available, use it to get the property value
      // This object owns the function
      assert(pPropertyMap.at(getFcnkey).isFcnObject());
      
      // Apply function
      auto fcn = pPropertyMap.at(getFcnkey).getFcnPtr();
      assert(fcn->getFcnType() == DataObjectFcn::FcnType::FCN_OBJECT);
      
      return (*(static_cast<ObjectFcn*>(fcn)))(const_cast<BaseObject*>(this), {});
      
    }
    else if (pPropertyMap.find(aKey) != pPropertyMap.end())
    {
      // This object has the property
      return pPropertyMap.at(aKey);
    }
    
    // Go up on the parent's chain
    if (!pParent)
    {
      throw std::logic_error("Property not found");
    }
    
    // If not found go up on the parent's chain
    return pParent->getProperty(aKey);
  }//getProperty
  
  void BaseObject::setProperty(const PropertyKey& aKey, const PropertyValue& aValue)
  {
    auto setFcnkey = getFullPropertyNameForRegisteringSetMethod(aKey);
    if (pPropertyMap.find(setFcnkey) != pPropertyMap.end())
    {
      // There is a set fcn available, use it to set the property value
      // This object owns the function
      assert(pPropertyMap.at(setFcnkey).isFcnObject());
      
      // Apply function
      auto fcn = pPropertyMap.at(setFcnkey).getFcnPtr();
      assert(fcn->getFcnType() == DataObjectFcn::FcnType::FCN_OBJECT);
      
      (*(static_cast<ObjectFcn*>(fcn)))(const_cast<BaseObject*>(this), { aValue });
      return;
    }
    else if (pPropertyMap.find(aKey) != pPropertyMap.end())
    {
      // This object has the property.
      // Set if it is non const
      if(!isConstProperty(aKey))
      {
        pPropertyMap.at(aKey) = aValue;
      }
      return;
    }
    
    // Go up on the parent's chain
    if (!pParent)
    {
      throw std::logic_error("Property not found");
    }
    
    // If not found go up on the parent's chain
    pParent->setProperty(aKey, aValue);
  }//setProperty
  
  void BaseObject::addProperty(const PropertyKey& aKey)
  {
    if (aKey.empty()) throw std::logic_error("Empty property name non allowed");
    
    // If the parent already has the given property, return there is no need to add
    // the same property to the child
    if (pParent && pParent->hasProperty(aKey)) return;
    
    // Adds a new property
    addProperty(aKey, DataObject());
  }//addProperty
  
  void BaseObject::addProperty(const PropertyKey& aKey, const PropertyValue& aVal)
  {
    if (aKey.empty()) throw std::logic_error("Empty property name non allowed");
    
    // If the parent already has the given property, set it to the new value
    if (pParent && pParent->hasProperty(aKey))
    {
      pParent->setProperty(aKey, aVal);
      return;
    }
    
    // Add a new property to the map
    pPropertyMap[aKey] = aVal;
  }//addProperty
  
  void BaseObject::attachFcnToGetProperty(const PropertyKey& aKey, ObjectFcn* aFcn)
  {
    // Create a fcn data object from the given fcn
    assert(aFcn);
    
    // Create a DataObject holding the function object
    DataObject fcn;
    fcn.setFcnObject(aFcn);
    
    // Look for the property with given key and remove it.
    // The property needs to be replaced with its "get" name
    //removeProperty(aKey);
    
    auto getPropKey = getFullPropertyNameForRegisteringGetMethod(aKey);
    if (hasProperty(getPropKey))
    {
      lookupProperty(getPropKey) = fcn;
    }
    else
    {
      addProperty(getPropKey, fcn);
    }
  }//attachFcnToGetProperty
  
  void BaseObject::attachFcnToSetProperty(const PropertyKey& aKey, ObjectFcn* aFcn)
  {
    // Create a fcn data object from the given fcn
    assert(aFcn);
    
    // Create a DataObject holding the function object
    DataObject fcn;
    fcn.setFcnObject(aFcn);
    
    // Look for the property with given key and remove it.
    // The property needs to be replaced with its "get" name
    //removeProperty(aKey);
    
    auto setPropKey = getFullPropertyNameForRegisteringSetMethod(aKey);
    if (hasProperty(setPropKey))
    {
      lookupProperty(setPropKey) = fcn;
    }
    else
    {
      addProperty(setPropKey, fcn);
    }
  }//attachFcnToSetProperty
  
  std::string BaseObject::toString()
  {
    return "";
  }//toString
  
  BaseObject* BaseObject::clone()
  {
    auto cloneObj = new BaseObject();
    if (pParent)
    {
      cloneObj->pParent = pParent->clone();
    }
    else
    {
      cloneObj->pParent = nullptr;
    }
    cloneObj->pPropertyMap = pPropertyMap;
    cloneObj->pHiddenProperties = pHiddenProperties;
    cloneObj->pConstProperties = pConstProperties;
    
    return cloneObj;
  }//clone
  
  std::vector<BaseObject::PropertyKey> BaseObject::getKeys()
  {
    std::vector<BaseObject::PropertyKey> props;
    
    // Get parent properties
    if (pParent)
    {
      const auto& parentProps = pParent->getKeys();
      props.assign(parentProps.begin(), parentProps.end());
    }
    
    // Get this (child) properties
    for (const auto& it : pPropertyMap)
    {
      props.push_back(it.first);
    }
    
    return props;
  }//getKeys
  
  std::vector<BaseObject::PropertyKey> BaseObject::getVisibleKeys()
  {
    std::vector<BaseObject::PropertyKey> props;
    
    // Get parent properties
    if (pParent)
    {
      const auto& parentProps = pParent->getVisibleKeys();
      props.assign(parentProps.begin(), parentProps.end());
    }
    
    // Get this (child) properties
    for (const auto& it : pPropertyMap)
    {
      if (isPropertyVisibile(it.first))
      {
        props.push_back(it.first);
      }
    }
    
    return props;
  }//getVisibleKeys
  
  void BaseObject::setPropertyVisibility(const PropertyKey& aKey, bool aIsVisible)
  {
    if (pParent && pParent->hasProperty(aKey))
    {
      pParent->setPropertyVisibility(aKey, aIsVisible);
      return;
    }
    
    if (pPropertyMap.find(aKey) == pPropertyMap.end()) return;
    if (aIsVisible && !isPropertyVisibile(aKey))
    {
      auto it = std::find(pHiddenProperties.begin(), pHiddenProperties.end(), aKey);
      assert(it != pHiddenProperties.end());
      
      pHiddenProperties.erase(it);
      return;
    }
    if (!aIsVisible && isPropertyVisibile(aKey))
    {
      pHiddenProperties.push_back(aKey);
    }
  }//setPropertyVisibility
  
  bool BaseObject::isPropertyVisibile(const PropertyKey& aKey) const
  {
    if (pParent && pParent->hasProperty(aKey))
    {
      return pParent->isPropertyVisibile(aKey);
    }
    if (!hasProperty(aKey)) { return false; }
    
    return std::find(pHiddenProperties.begin(), pHiddenProperties.end(), aKey) ==
                     pHiddenProperties.end();
  }//isPropertyVisibile
  
  void BaseObject::setConstProperty(const PropertyKey& aKey, bool aIsConst)
  {
    if (pParent && pParent->hasProperty(aKey))
    {
      pParent->setConstProperty(aKey, aIsConst);
      return;
    }
    
    if (pPropertyMap.find(aKey) == pPropertyMap.end()) return;
    if (aIsConst && !isConstProperty(aKey))
    {
      auto it = std::find(pConstProperties.begin(), pConstProperties.end(), aKey);
      assert(it == pConstProperties.end());
      
      pConstProperties.push_back(aKey);
      return;
    }
    
    if (!aIsConst && isConstProperty(aKey))
    {
      auto it = std::find(pConstProperties.begin(), pConstProperties.end(), aKey);
      assert(it != pConstProperties.end());
      pConstProperties.erase(it);
    }
  }//setConstProperty
  
  bool BaseObject::isConstProperty(const PropertyKey& aKey) const
  {
    // Check the parent first, if the property was also part
    // of the child, that property has been removed from the child
    // and re-set into the parent
    if (pParent && pParent->hasProperty(aKey))
    {
      return pParent->isConstProperty(aKey);
    }
    if (!hasProperty(aKey)) { return false; }
    
    return std::find(pConstProperties.begin(), pConstProperties.end(), aKey) !=
                     pConstProperties.end();
  }//isConstProperty
  
}// end namespace Interpreter
