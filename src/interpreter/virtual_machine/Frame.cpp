// Self first
#include "Frame.hpp"

namespace Interpreter {
  
  Frame::Frame(const CodeObject& aCodeObject, const NameMapSPtr& aGlbNames, const NameMapSPtr& aLocNames, FramePtr aPrevFrame)
  : pPC(0)
  , pPrevFrame(aPrevFrame)
  , pGlobalNames(aGlbNames)
  , pLocalNames(aLocNames)
  , pCode(aCodeObject)
  {
    if (!pGlobalNames) pGlobalNames = std::make_shared<NameMap>();
    if (!pLocalNames)  pLocalNames  = std::make_shared<NameMap>();
  }
  
  void Frame::storeLocalName(const std::string& aName, const DataObject& aObj)
  {
    if (aName.empty()) return;
    (*pLocalNames)[aName] = aObj;
  }//storeLocalName
  
  void Frame::storeGlobalName(const std::string& aName, const DataObject& aObj)
  {
    if (aName.empty()) return;
    (*pGlobalNames)[aName] = aObj;
  }//storeGlobalName
  
  void Frame::deleteLocalName(const std::string& aName)
  {
    if (containsLocalName(aName))
    {
      pLocalNames->erase(aName);
    }
  }//deleteLocalName
  
  void Frame::deleteGlobalName(const std::string& aName)
  {
    if (containsGlobalName(aName))
    {
      pGlobalNames->erase(aName);
    }
  }//deleteGlobalName
  
}// end namespace Interpreter
