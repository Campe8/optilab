// Self first
#include "DataObject.hpp"

#include "BaseObject.hpp"
#include "InlineFcn.hpp"
#include "ObjectFcn.hpp"

namespace Interpreter {
  
  DataObject::DataObject()
  : pFullyInterpreted(true)
  , pDOType(DOT_VOID)
  , pIter(0)
  , pData(nullptr)
  , pFcn(nullptr)
  , pObjCode(nullptr)
  {
  }
  
  DataObject::DataObject(const DataObject& aOther)
  : pFullyInterpreted(aOther.pFullyInterpreted)
  , pDOType(aOther.pDOType)
  , pIter(aOther.pIter)
  , pData(aOther.empty() ? nullptr : aOther.pData->clone())
  , pFcn(aOther.isFcnObject() ? aOther.pFcn->clone() : nullptr)
  , pObjCode(aOther.pObjCode)
  , pClassObjInstance(aOther.pClassObjInstance)
  {
    for (const auto& e : aOther.pComposite)
    {
      pComposite.push_back(e);
    }
    for (const auto& e : aOther.pIterArray)
    {
      pIterArray.push_back(e);
    }
  }
  
  DataObject::DataObject(DataObject&& aOther)
  : pFullyInterpreted(aOther.pFullyInterpreted)
  , pDOType(aOther.pDOType)
  , pIter(aOther.pIter)
  , pData(aOther.empty() ? nullptr : aOther.pData->clone())
  , pFcn(aOther.isFcnObject() ? aOther.pFcn->clone() : nullptr)
  , pObjCode(aOther.pObjCode)
  , pClassObjInstance(aOther.pClassObjInstance)
  {
    for (const auto& e : aOther.pComposite)
    {
      pComposite.push_back(e);
    }
    for (const auto& e : aOther.pIterArray)
    {
      pIterArray.push_back(e);
    }
    aOther.pObjCode = nullptr;
    aOther.pClassObjInstance = nullptr;
    aOther.pData.reset(nullptr);
    aOther.pFcn.reset(nullptr);
    aOther.pComposite.clear();
    aOther.pIterArray.clear();
    aOther.pIter = 0;
    aOther.pDOType = DOT_VOID;
  }
  
  DataObject& DataObject::operator=(const DataObject& aOther)
  {
    if (this != &aOther)
    {
      pFullyInterpreted = aOther.pFullyInterpreted;
      pDOType = aOther.pDOType;
      pIter = aOther.pIter;
      pData.reset(aOther.empty() ? nullptr : aOther.pData->clone());
      pFcn.reset(aOther.isFcnObject() ? aOther.pFcn->clone() : nullptr);
      pObjCode = aOther.pObjCode;
      pClassObjInstance = aOther.pClassObjInstance;
      pComposite.clear();
      for (const auto& e : aOther.pComposite)
      {
        pComposite.push_back(e);
      }
      pIterArray.clear();
      for (const auto& e : aOther.pIterArray)
      {
        pIterArray.push_back(e);
      }
    }
    return *this;
  }
  
  DataObject& DataObject::operator=(DataObject&& aOther)
  {
    if (this != &aOther)
    {
      pFullyInterpreted = aOther.pFullyInterpreted;
      pDOType = aOther.pDOType;
      pIter = aOther.pIter;
      pData.reset(aOther.empty() ? nullptr : aOther.pData->clone());
      pFcn.reset(aOther.isFcnObject() ? aOther.pFcn->clone() : nullptr);
      pObjCode = aOther.pObjCode;
      pClassObjInstance = aOther.pClassObjInstance;
      pComposite.clear();
      for (const auto& e : aOther.pComposite)
      {
        pComposite.push_back(e);
      }
      pIterArray.clear();
      for (const auto& e : aOther.pIterArray)
      {
        pIterArray.push_back(e);
      }
      aOther.pObjCode = nullptr;
      aOther.pClassObjInstance = nullptr;
      aOther.pFcn.reset(nullptr);
      aOther.pData.reset(nullptr);
      aOther.pComposite.clear();
      aOther.pIterArray.clear();
      aOther.pIter = 0;
      aOther.pDOType = DOT_VOID;
    }
    return *this;
  }
  
  void DataObject::compose(const std::vector<DataObject>& aDS)
  {
    pComposite.clear();
    pComposite = aDS;
  }//compose
  
  DataObject& DataObject::operator[](std::size_t aIdx)
  {
    assert(aIdx < pComposite.size());
    return pComposite[aIdx];
  }
  
  const DataObject& DataObject::operator[](std::size_t aIdx) const
  {
    assert(aIdx < pComposite.size());
    return pComposite.at(aIdx);
  }

  std::vector<DataObject> DataObject::operator()(const std::vector<DataObject>& aArgs)
  {
    // Call the function data object on the list of input arguments
    assert(pFcn);
    if (pFcn->getFcnType() == DataObjectFcn::FcnType::FCN_INLINE)
    {
      // The one and only argument for inline function is the
      // object representation of the function itself
      assert(aArgs.size() == 1);
      assert(aArgs[0].isFcnObject());
      
      // Get and call the inline function
      auto fcn = static_cast<InlineFcn*>(pFcn.get());
      if (fcn->needsFcnObject())
      {
        // Run the callback inline function on the its representation
        // as a function object
        const auto& resPair = (*fcn)(aArgs);
        return { resPair.first, resPair.second };
      }
      else
      {
        // Run the callback function on the arguments of the function
        // taken from the DataObject representation of the function
        const auto& resPair = (*fcn)(aArgs[0].pComposite);
        return { resPair.first, resPair.second };
      }
    }
    else if(pFcn->getFcnType() == DataObjectFcn::FcnType::FCN_OBJECT)
    {
      auto fcn = static_cast<ObjectFcn*>(pFcn.get());
      
      // Apply the function object on the arguments
      // @note the first argument must be the pointer to the object
      assert(!aArgs.empty() && aArgs[0].isClassObject());
      return { (*fcn)(aArgs[0].getClassObject().get() , { aArgs.begin()+1 , aArgs.end() }) };
    }
    else
    {
      // @TODO
      // Intrinsic functions not supported yet
      assert(pFcn->getFcnType() == DataObjectFcn::FcnType::FCN_INTRINSIC);
      assert(false);
      return {DataObject(0)};
    }
  }//()
  
  void DataObject::setIterator(const DataObject& aDO)
  {
    if (!aDO.isComposite())
    {
      return;
      //pIterArray.push_back(aDO);
    }
    else
    {
      // Use the list of composite objects
      // as the iterator array
      pIterArray = aDO.pComposite;
    }
  }//setIterator
  
  template<>
  std::string DataObject::castDataValue<std::string>() const
  {
    assert(pData);
    
    switch (dataObjectType())
    {
      case DataObjectType::DOT_BOOL:
      {
        std::stringstream ss;
        ss << std::boolalpha << getDataValue<bool>();
        return ss.str();
      }
      case DataObjectType::DOT_INT:
      {
        std::stringstream ss;
        ss << getDataValue<int>();
        return ss.str();
      }
      case DataObjectType::DOT_DOUBLE:
      {
        std::stringstream ss;
        ss << getDataValue<double>();
        return ss.str();
      }
      case DataObjectType::DOT_SIZE_T:
      {
        std::stringstream ss;
        ss << getDataValue<std::size_t>();
        return ss.str();
      }
      default:
      {
        assert(dataObjectType() == DataObjectType::DOT_STRING);
        return getDataValue<std::string>();
      }
        break;
    }
  }//castDataValue
  
}// end namespace Interpreter
