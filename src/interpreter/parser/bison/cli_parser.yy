%{ /*** C/C++ Declarations ***/

#include <string>
#include <vector>
#include <utility>

//#define DEBUG(msg) driver.debug(msg);
#define DEBUG(msg) (void)msg;

%}

%skeleton "lalr1.cc"
%require  "3.0"
%debug
%defines
%define api.namespace {CLI}
%define parser_class_name {CLIParser}

%code requires{

#include "CLICoreObj.hpp"
#include "OPCode.hpp"

namespace CLI {
class CLIDriver;
class CLIScanner;

//windows compatibility
#undef TRUE
#undef FALSE
#undef IN
}

// The following definitions is missing when %locations isn't used
# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

}

%parse-param { CLIScanner& scanner  }
%parse-param { CLIDriver&  driver   }

%code{
#include <iostream>
#include <cstdlib>
#include <fstream>

/* include for all driver functions */
#include "CLIDriver.hpp"

#undef yylex
#define yylex scanner.yylex
}

%define api.value.type variant
%define parse.assert

%token          ENDF    0  "end of file"
%token <CLI_P_INT>    INUMBER    "CLIInt"
%token <CLI_P_DOUBLE>  DNUMBER    "CLIDouble"
%token          TRUE    "CLITrue"
%token          FALSE    "CLIFalse"
%token <CLI_P_STR>    IDENTIFIER  "CLIID"
%token          NEWLINE
%token          QUOTE
%token          COMMA    ","
%token          SEMICOLON  ";"
%token          LT      "<"
%token          LE      "<="
%token          GT      ">"
%token          GE      ">="
%token          AND      "&&"
%token          OR      "||"
%token          REIF    "<=>"
%token          EQ      "=="
%token          NQ      "!="
%token          PLUS    "+"
%token          MINUS    "-"
%token          PROD    "*"
%token          DIV      "/"
%token          LEFTPAR    "("
%token          RIGHTPAR  ")"
%token          LEFTGPAR  "{"
%token          RIGHTGPAR  "}"
%token          LEFTQPAR  "["
%token          RIGHTQPAR  "]"
%token          COLON    ":"
%token          ASSIGN    "="
%token          DOT      "."
%token          RANGE    ".."
%token          LET      "let"
%token          IF       "if"
%token          ELSEIF   "elseif"
%token          ELSE     "else"
%token          FOR      "for"
%token          WHILE    "while"
%token          IN       "in"
%token          BREAK    "break"
%token          CONTINUE "continue"
%token          RETURN   "return"
%token          END      "end"
%token <CLI_P_STR>    STRING  "CLIString"
%token          SYMBOL
%token          MINIMIZE  "minimize"
%token          MAXIMIZE  "maximize"
%token          START_LINE
%token          START_FILE
%token <CLI_P_STR>    LEXERROR

// Number of element in the list
%type <CLI_P_INT> expr_list

// Number of element in the subscript
%type <CLI_P_INT> subscript_expr

// Number of arguments
%type <CLI_P_INT> arg_expr

// Pair of ID (string) and list of signs
%type <std::pair<CLI_P_STR, std::vector<bool>>> signed_id

// Boolean flag indicating whether or not this is the end of the block list
%type <bool> block_list

// Boolean flag indicating whether or not this is the end of the block
%type <bool> block

// Value of the number
%type <CLI_P_INT> signed_number

// Number of elseif cases
%type <CLI_P_INT> if_body

// Type of for-loop iterator: pair <type of iterator, num elements in the list>
%type <std::pair<CLICoreObj::ObjType, CLI_P_INT>> for_loop_iter

// Type of optimization (0: no_opt, 1: min, 2: max)
%type <CLI_P_INT> minmaxopt

// Type of the domain declaration as a pair: <type of declaration, numbe of elements>
%type <std::pair<CLICoreObj::ObjType, int>> dom_decl

// Number of elements in the set
%type <CLI_P_INT> set_domain

%start entry_point

%locations

%%

entry_point
  : START_LINE input_line
  | START_FILE input_file
  ;

input_line
  : single_input
  | LEXERROR { driver.setLexerError($1); }
  ;

input_file
  : file_input { DEBUG("input_file -> file_input"); }
  | LEXERROR { driver.setLexerError($1); }
  ;

single_input
  : line_input { DEBUG("single_input -> line_input"); }
  ;

file_input
  : multi_line_input ENDF
  ;

line_input
  : NEWLINE
  | simple_stmt_list_marked
  | block_stmt NEWLINE
  ;

multi_line_input
  : NEWLINE
  | stmt
  | multi_line_input stmt
  | multi_line_input NEWLINE
  ;

stmt
  : simple_stmt_list_marked
  | block_stmt
  ;

simple_stmt_list_marked
  : simple_stmt_list NEWLINE
  | simple_stmt_list ";" NEWLINE
  ;

simple_stmt_list
  : simple_stmt { driver.markEndOfStmt(); }
  | simple_stmt_list ";" { driver.markEndOfStmt(); } simple_stmt { driver.markEndOfStmt(); }
  ;

delim_stmt
  : %empty
  | delimiter
  ;

delimiter
  : null_lines
  | empty_lines
  | null_lines empty_lines
  ;

null_lines
  : null_line
  | null_lines null_line
  ;

null_line
  : ";"
  | empty_lines ";"
  ;

empty_lines
  : NEWLINE
  | empty_lines NEWLINE
  ;

simple_stmt
  : expr_stmt { DEBUG("simple_stmt -> expr_stmt"); }
  | flow_stmt { DEBUG("simple_stmt -> flow_stmt"); }
  ;

block_stmt
  : for_stmt
  | if_stmt
  ;

for_stmt
  : "for" "CLIID" "in"
    { driver.addForLoopBlock(); }
  for_loop_iter ":"
    {
      /* Give the name of the iterator and its type */
      driver.addForLoopRangeIter($2, $5.first, $5.second);
    }
  block
    { driver.markEndOfStmt($8); }
  "end"
    { driver.addEndBlock(); }
  ;

for_loop_iter
  : range_expr
    {
      $$.first = CLICoreObj::ObjType::OT_RNG_LIST;
      $$.second = 2;
    }
  | subscript_expr
    {
      $$.first = CLICoreObj::ObjType::OT_LIST_ARRAY;
      $$.second = $1;
    }
  ;

if_stmt
  : "if" expr ":"
    { driver.addIfBlock(); }
  if_body
    {
      /* Close all the elseif cases here */
      for(int numElseIf = 0; numElseIf < $5; ++numElseIf)
      {
        driver.addEndIfBlock();
      }
    }
  "end"
  ;

if_body
  : block
    {
      /* Mark the end of if stmt */
      driver.markEndOfStmt($1);

      /* Set the PC to jump to here from wherever it was */
      driver.addEndIfBlock();
      $$ = 0;
    }
  | block
    {
      driver.markEndOfStmt($1);

      /* Set the PC to jump to here on prev if stmt (if not true jump here) */
      driver.addEndIfBlock(true);

      /* Add a jump forward (if PC is at this point, jump forward to end)
         before evaluating the following block */
      driver.addElseBlock();
    }
  "elseif" expr ":"
    {
      /* Add an if condition to jump to next elseif/else if not true */
      driver.addIfBlock();
    }
  if_body
    { $$ = 1 + $7; }
  | block
    {
      /* Mark the end of if stmt */
      driver.markEndOfStmt($1);

      /* Set the PC to jump to here from wherever it was */
      driver.addEndIfBlock(true);
    }
  "else" ":"
    {
      /* Add a jump forward (if PC is at this point, jump forward to end),
         before evaluating the following block */
      driver.addElseBlock();
    }
  block
    {
      /* Mark the end of if stmt */
      driver.markEndOfStmt($6);

      /* Set the PC to jump to here from wherever it was */
      driver.addEndIfBlock();
      $$ = 0;
    }
  ;

block
  : delim_stmt            {  $$ = true; /* Empty block */ }
  | delim_stmt block_list {  $$ = $2; }
  ;

block_list
  : simple_stmt delimiter { driver.markEndOfStmt(); $$ = false; /* Simple stmt: not empty*/ }
  | block_stmt delimiter  { $$ = false; }
  | simple_stmt delimiter { driver.markEndOfStmt(); } block_list { driver.markEndOfStmt(); $$ = false; }
  | block_stmt delimiter block_list { driver.markEndOfStmt(); $$ = false; }
  ;

expr_stmt
  : subroutine_lhs
    expr
    {
      /* Remove the LHSStart marker since this is not an assignment */
      driver.popLHSStartAssign();
      DEBUG("expr_stmt -> expr");
    }
  | subroutine_lhs
    expr "="
    {
      /* Mark the end of the LHS expression */
      driver.setLHSEndAssign();

      /* Mark the beginning of the RHS expression */
      driver.setRHSStartAssign();

      DEBUG("Done with LHS");
      DEBUG("Before RHS");
    }
    expr
    {
      /* Mark the end of the RHS expression */
      driver.setRHSEndAssign();

      /* Add assignment instruction on stack */
      driver.addAssignExpr();

      DEBUG("Done with RHS");
      DEBUG("expr_stmt -> expr : expr");
    }
  | subroutine_lhs
    dec_var_decl
    {
      /* Remove the LHSStart marker since this is not an assignment */
      driver.popLHSStartAssign();

      DEBUG("expr_stmt -> dec_var_decl");
    }
  ;

%right "dvar" ":";
dec_var_decl
  : "CLIID" ":" dom_decl minmaxopt
    {
      /* Decision variable declaration */
      driver.addVarDecl($1, $3.first, $3.second, $4);

      DEBUG("dec_var_decl -> id : dom_decl minmaxopt");
    }
  | "CLIID" ":" dom_decl ":" subscript_expr minmaxopt
    {
      /* Decision matrix variable declaration:
         - add the list for the matrix variable
         - add the matrix variable declaration */
      driver.addList($5, CLICoreObj::ObjType::OT_LIST_ARRAY);
      driver.addMatrixVarDecl($1, $3.first, $3.second, $6);

      DEBUG("dec_var_decl -> id : dom_decl : subscript_expr minmaxopt");
    }
  ;

minmaxopt
  : %empty         { /* No optimization */ $$ = 0; DEBUG("minmaxopt -> ");    }
  | ":" "minimize" { /* Minimize */ $$ = 1; DEBUG("minmaxopt -> : minimize"); }
  | ":" "maximize" { /* Maximize */ $$ = 2; DEBUG("minmaxopt -> : maximize"); }
  ;

dom_decl
  : range_domain
    {
      /* $1 elements.
        @note range domain pushed a range list on the stack */
      $$ = {CLICoreObj::ObjType::OT_RNG, 2};
    }
  | set_domain
    {
      /* $1 elements. */
      $$ = {CLICoreObj::ObjType::OT_SET, $1};
    }
  ;

subroutine_lhs
  : %empty
    {
      /* Mark the beginning of an LHS expr in an assignment */
      driver.setLHSStartAssign();
      DEBUG("Before LHS");
    }
  ;

%left "<=>";
%left "||";
%left "&&";
%left "==" "!=";
%left "<" "<=" ">" ">=";
%left "*" "/";
%left "+" "-";
expr
  : atom_expr       { DEBUG("expr -> atom_expr"); }
  | expr "+" expr   { driver.addExpr(CLICoreObj::ObjType::OT_PLUS); DEBUG("expr -> expr + expr");  }
  | expr "-" expr   { driver.addExpr(CLICoreObj::ObjType::OT_MIN);  DEBUG("expr -> expr - expr");  }
  | expr "*" expr   { driver.addExpr(CLICoreObj::ObjType::OT_PROD); DEBUG("expr -> expr * expr");  }
  | expr "/" expr   { driver.addExpr(CLICoreObj::ObjType::OT_DIV);  DEBUG("expr -> expr / expr");  }
  | expr "<" expr   { driver.addExpr(CLICoreObj::ObjType::OT_LT);   DEBUG("expr -> expr < expr");  }
  | expr "<=" expr  { driver.addExpr(CLICoreObj::ObjType::OT_LE);   DEBUG("expr -> expr <= expr"); }
  | expr ">" expr   { driver.addExpr(CLICoreObj::ObjType::OT_GT);   DEBUG("expr -> expr > expr");  }
  | expr ">=" expr  { driver.addExpr(CLICoreObj::ObjType::OT_GE);   DEBUG("expr -> expr >= expr"); }
  | expr "==" expr  { driver.addExpr(CLICoreObj::ObjType::OT_EQ);   DEBUG("expr -> expr == expr"); }
  | expr "!=" expr  { driver.addExpr(CLICoreObj::ObjType::OT_NQ);   DEBUG("expr -> expr != expr"); }
  | expr "&&" expr  { driver.addExpr(CLICoreObj::ObjType::OT_AND);  DEBUG("expr -> expr && expr"); }
  | expr "||" expr  { driver.addExpr(CLICoreObj::ObjType::OT_OR);   DEBUG("expr -> expr || expr"); }
  | expr "<=>" expr { driver.addExpr(CLICoreObj::ObjType::OT_REIF); DEBUG("expr -> expr <=> expr");}
  ;

atom_expr
  : core_expr     { DEBUG("atom_expr -> core_expr"); }
  | range_expr    { DEBUG("atom_expr -> range_expr"); }
  | sequence_expr { DEBUG("atom_expr -> sequence_expr") }
  | atom_expr
    {
      /* Mark the beginning of the sequence expression */
      driver.setSeqExprStart();

      DEBUG("atom_expr -> *atom_expr sequence_expr");
    }
    sequence_expr
    {
      /* Check the atom_expr type and the sequence and set it to the proper
         type of object accordingly */
      driver.setObjectWithList();

      DEBUG("atom_expr -> atom_expr sequence_expr");
    }
  ;

core_expr
  : signed_number { driver.addCoreExpr($1); DEBUG("core_expr -> signed_number");   }
  | signed_id
    {
      driver.addCoreExpr($1.first);
      driver.addUnaryOperators($1.second);
      DEBUG("core_expr -> signed_id");
    }
  | "CLIString"   { driver.addCoreExpr($1); DEBUG("core_expr -> CLIString");   }
  | "CLITrue"     { driver.addCoreExpr(true); DEBUG("core_expr -> CLITrue");   }
  | "CLIFalse"    { driver.addCoreExpr(false); DEBUG("core_expr -> CLIFalse"); }
  ;

signed_number
  : INUMBER           { std::swap($$, $1); DEBUG("signed_number -> INUMBER");         }
  | "-" signed_number { $$ = (-1 * $2);    DEBUG("signed_number -> - signed_number"); }
  | "+" signed_number { std::swap($$, $2); DEBUG("signed_number -> + signed_number"); }
  ;

signed_id
  : "CLIID" %prec "dvar"
    {
      std::swap($$.first, $1);
      DEBUG("signed_id -> CLIID");
    }
  | "-" signed_id
    {
      std::swap($$.first, $2.first);
      $$.second = $2.second;
      $$.second.push_back(false);
      DEBUG("signed_id -> - signed_id");
    }
  | "+" signed_id
    {
      std::swap($$.first, $2.first);
      $$.second = $2.second;
      $$.second.push_back(true);
      DEBUG("signed_id -> + signed_id");
    }
  ;

sequence_expr
  : arg_expr
    {
      driver.addList($1, CLICoreObj::ObjType::OT_LIST_ARGS);
      DEBUG("sequence_expr -> arg_expr");
    }
  | subscript_expr
    {
      driver.addList($1, CLICoreObj::ObjType::OT_LIST_ARRAY);
      DEBUG("sequence_expr -> subscript_expr");
    }
  | "." "CLIID"
    {
      /* @note this is under "sequence_expr" because it can form a sequence of properties
         to query. For example, .prop1.prop2.prop3, etc.*/
      driver.addLoadProperty($2);
      DEBUG("sequence_expr -> . CLIID");
    }
  ;

arg_expr
  : "(" ")"
    {
      /* No arguments */
      $$ = 0;
      DEBUG("arg_expr -> ( )");
    }
  | "(" expr_list ")"
    {
      /* |list| number of arguments */
      $$ = $2;
      DEBUG("arg_expr -> ( expr_list )");
    }
  ;

subscript_expr
  : "[" "]"
    {
      /* No arguments */
      $$ = 0;
      DEBUG("subscript_expr -> [ ]");
    }
  | "[" expr_list "]"
    {
      $$ = $2;
      DEBUG("subscript_expr -> [ expr_list ]");
    }
  ;

expr_list
  : expr                            { $$ = 1;      DEBUG("expr_list -> expr");             }
  | expr_list "," expr              { $$ = $1 + 1; DEBUG("expr_list -> expr_list , expr"); }
  | expr_list empty_lines "," expr  { $$ = $1 + 1; DEBUG("expr_list -> expr_list , expr"); }
  | expr_list "," empty_lines expr  { $$ = $1 + 1; DEBUG("expr_list -> expr_list , expr"); }
  | expr_list empty_lines "," empty_lines expr
    {
      $$ = $1 + 1;
      DEBUG("expr_list -> expr_list , expr");
    }
  ;

range_expr
  : "[" expr ".." expr "]"
    {
      driver.addList(2, CLICoreObj::ObjType::OT_RNG_LIST);
      DEBUG("range_expr");
    }
  ;

range_domain
  : ".."
    {
      driver.addList(DOM_BOUNDS_OPEN, CLICoreObj::ObjType::OT_RNG_DOMAIN);
      DEBUG("range_domain -> ..");
    }
  | ".." expr
    {
      driver.addList(DOM_BOUNDS_OPEN_LEFT, CLICoreObj::ObjType::OT_RNG_DOMAIN);
      DEBUG("range_domain -> .. expr");
    }
  | expr ".."
    {
      driver.addList(DOM_BOUNDS_OPEN_RIGHT, CLICoreObj::ObjType::OT_RNG_DOMAIN);
      DEBUG("range_domain -> expr ..");
    }
  | expr ".." expr
    {
      driver.addList(2, CLICoreObj::ObjType::OT_RNG_DOMAIN);
      DEBUG("range_domain -> expr .. expr");
    }
  ;

set_domain
  : "{" expr_list "}" { $$ = $2; }
  ;

flow_stmt
  : "break" { driver.addExpr(CLICoreObj::ObjType::OT_BREAK); DEBUG("flow_stmt -> break"); }
  | "continue"
    {
      driver.addExpr(CLICoreObj::ObjType::OT_CONTINUE);
      DEBUG("flow_stmt -> continue");
    }
  | "return" { driver.addExpr(CLICoreObj::ObjType::OT_RETURN);  DEBUG("flow_stmt -> return"); }
  ;

%%

void
CLI::CLIParser::error( const location_type &l, const std::string &err_message )
{
  driver.setParserError(err_message, l);
}
