// Self first
#include "CLIDriver.hpp"

#include "OPCode.hpp"

#include <cctype>
#include <fstream>
#include <algorithm>
#include <sstream>
#include <numeric>
#include <limits>
#include <iterator>
#include <exception>
#include <cassert>
#include <stdexcept>  // for std::runtime_error

#define MAX_REP_NUM 65536

namespace
{
  // Returns true if the given string is a quoted string, false otherwise.
  // For example:
  // \"abc\"
  // is a quoted string
  bool isQuotedString(const std::string& aString)
  {
    if (aString.size() < 2 || aString[0] != '"' || aString[aString.size() - 1] != '"')
    {
      return false;
    }
    
    return true;
  }//isQuotedString
  
  /// Given "aInt" as integer returns the corresponding byte
  CLI::CLIDriver::byte_T intToByte(int aInt)
  {
    assert(aInt >= 0 && aInt <= 256);
    CLI::CLIDriver::byte_T b = aInt & 0xFF;
    return b;
  }//intToByte
  
  /// Given "aInt" as integer returns the corresponding 4 byte set
  CLI::CLIDriver::ByteArray intToByteArray(int aInt)
  {
    assert(aInt >= 0 && aInt <= 65536);
    CLI::CLIDriver::ByteArray bytes(2, 0);
    bytes[1] = (aInt >> 8) & 0xFF;
    bytes[0] = aInt & 0xFF;
    
    return bytes;
  }//intToByte
  
  /// Converts byte to int
  int byteToInt(CLI::CLIDriver::byte_T aByte)
  {
    return static_cast<int>(aByte);
  }//byteToInt
  
  /// Returns the value to be use for negative infinity
  CLI_P_INT getNegInfinity()
  {
    static CLI_P_INT negInf = static_cast<CLI_P_INT>(std::numeric_limits<int>::min());
    return negInf;
  }//getNegInfinity
  
  /// Returns the value to be use for positive infinity
  CLI_P_INT getPosInfinity()
  {
    static CLI_P_INT posInf = static_cast<CLI_P_INT>(std::numeric_limits<int>::max());
    return posInf;
  }//getPosInfinity
  
  /// Returns true if "aObj" is a core object.
  /// Returns false otherwise
  bool isCoreObject(const CLI::CLICoreObj& aObj)
  {
    return
    aObj.objType == CLI::CLICoreObj::ObjType::OT_INT ||
    aObj.objType == CLI::CLICoreObj::ObjType::OT_BOOL ||
    aObj.objType == CLI::CLICoreObj::ObjType::OT_STRING;
  }//isCoreObject
  
  /// Returns true if "aObj" is a core non-string object.
  /// Returns false otherwise
  bool isScalarObject(const CLI::CLICoreObj& aObj)
  {
    return
    (aObj.objType == CLI::CLICoreObj::ObjType::OT_INT ||
     aObj.objType == CLI::CLICoreObj::ObjType::OT_BOOL) &&
    aObj.objType != CLI::CLICoreObj::ObjType::OT_STRING;
  }//isScalarObject
  
  /// Adds a scalar integer LOAD byte-code instruction into the given byte-code
  void bcCoreObjInt(const CLI::CLICoreObj& aObj, CLI::CLIDriver::ByteCode& aBC)
  {
    // Set opcode
    auto code = (aObj.objType == CLI::CLICoreObj::ObjType::OT_BOOL) ? LOAD_CONST_BOOL : LOAD_CONST;
    aBC.byte_code.push_back(intToByte(code));
    
    // Set constant
    int idx = -1;
    for (std::size_t it{ 0 }; it < aBC.const_array.size(); ++it)
    {
      // Look if the constant aObj.intObj is already present in the array of constants
      const auto& subArr = aBC.const_array[it];
      if (subArr.size() == 1 && subArr[0].getType() == 0 && subArr[0].getInt() == aObj.intObj)
      {
        // The constant is found, set its index
        idx = static_cast<int>(it);
        break;
      }
    }
    
    if (idx < 0)
    {
      // The constant has not been found, add it as new in the array
      // of constants and set its index
      aBC.const_array.push_back({ aObj.intObj });
      const auto& ba = intToByteArray(static_cast<int>(aBC.const_array.size() - 1));
      aBC.byte_code.push_back(ba[0]);
      aBC.byte_code.push_back(ba[1]);
    }
    else
    {
      // The constant was already present in the array of constants,
      // set its index
      const auto& ba = intToByteArray(idx);
      aBC.byte_code.push_back(ba[0]);
      aBC.byte_code.push_back(ba[1]);
    }
  }//bcCoreObjInt
  
  /// Adds a LOAD_CONST instruction for a string type to the byte-code
  void bcCoreObjStringConst(const CLI::CLICoreObj& aObj, CLI::CLIDriver::ByteCode& aBC)
  {
    // Set opcode
    aBC.byte_code.push_back(intToByte(LOAD_CONST));
    
    int idx = -1;
    for (std::size_t it{ 0 }; it < aBC.const_array.size(); ++it)
    {
      // Look if the constant aObj.intObj is already present
      // in the array of constants
      const auto& subArr = aBC.const_array[it];
      if (subArr.size() == 1 && subArr[0].getType() == 1 && subArr[0].getString() == aObj.strObj)
      {
        // The constant is found, set its index
        idx = static_cast<int>(it);
        break;
      }
    }
    
    if (idx < 0)
    {
      // The constant has not been found, add it as new in the array
      // of constants and set its index
      aBC.const_array.push_back({ aObj.strObj });
      const auto& ba = intToByteArray(static_cast<int>(aBC.const_array.size() - 1));
      aBC.byte_code.push_back(ba[0]);
      aBC.byte_code.push_back(ba[1]);
    }
    else
    {
      // The constant was already present in the array of constants,
      // set its index
      const auto& ba = intToByteArray(idx);
      aBC.byte_code.push_back(ba[0]);
      aBC.byte_code.push_back(ba[1]);
    }
  }//bcCoreObjStringConst
  
  /// Adds a LOAD_NAME instruction for a string type to the byte-code
  void bcCoreObjStringName(const CLI::CLICoreObj& aObj, CLI::CLIDriver::ByteCode& aBC)
  {
    // Set opcode
    aBC.byte_code.push_back(intToByte(LOAD_NAME));
    
    // Set name
    auto it = std::find(aBC.global_array.begin(), aBC.global_array.end(), aObj.strObj);
    if (it == aBC.global_array.end())
    {
      // The name is not present in the global array of names, add it and
      // set its index as parameter in the byte code
      aBC.global_array.push_back(aObj.strObj);
      const auto& ba = intToByteArray(static_cast<int>(aBC.global_array.size() - 1));
      aBC.byte_code.push_back(ba[0]);
      aBC.byte_code.push_back(ba[1]);
    }
    else
    {
      // The name is present in the global array of names,
      // and set its index as parameter in the byte code
      const auto& ba = intToByteArray(static_cast<int>(it - aBC.global_array.begin()));
      aBC.byte_code.push_back(ba[0]);
      aBC.byte_code.push_back(ba[1]);
    }
  }//bcCoreObjStringName
  
  /// Converts a string object to a LOAD string byte-code instruction
  void bcCoreObjString(const CLI::CLICoreObj& aObj, CLI::CLIDriver::ByteCode& aBC)
  {
    // Set opcode
    if (isQuotedString(aObj.strObj))
    {
      // Quoted strings are constants and not names
      // of objects to load from the stack
      bcCoreObjStringConst(aObj, aBC);
    }
    else
    {
      // Unquoted strings represent names of objects
      // to load (LOAD_NAME) from the stack
      bcCoreObjStringName(aObj, aBC);
    }
  }//bcCoreObjString
  
  /// Adds a core object of primitive or string type into the given byte-code
  void bcCoreObj(const CLI::CLICoreObj& aObj, CLI::CLIDriver::ByteCode& aBC)
  {
    switch (aObj.objType)
    {
      case CLI::CLICoreObj::ObjType::OT_INT:
      case CLI::CLICoreObj::ObjType::OT_BOOL:
        bcCoreObjInt(aObj, aBC);
        break;
      default:
        assert(aObj.objType == CLI::CLICoreObj::ObjType::OT_STRING);
        bcCoreObjString(aObj, aBC);
        break;
    }
  }//bcCoreObj
  
  /// Returns true if the given object is of a control flow type, returns false otherwise
  bool isFlowObject(const CLI::CLICoreObj& aObj)
  {
    return
    aObj.objType == CLI::CLICoreObj::ObjType::OT_CONTINUE ||
    aObj.objType == CLI::CLICoreObj::ObjType::OT_BREAK;
  }//isFlowObject
  
  int findLoopInstructionFromBlockStackPC(CLI::CLIDriver::ByteCode& aBC,
                                          std::vector<std::size_t>& aBlockStack)
  {
    auto loopByte = intToByte(FOR_ITER);
    for (auto it = aBlockStack.rbegin(); it != aBlockStack.rend(); ++it)
    {
      assert(*it <= aBC.byte_code.size());
      if (aBC.byte_code[*it] == loopByte) return static_cast<int>(*it);
    }
    
    return -1;
  }//findLoopInstructionFromBlockStackPC
  
  /// Adds a JUMP_ABSOLUTE instruction  or a BREAK_LOOP instruction in the byte-code
  void bcFlowObj(const CLI::CLICoreObj& aObj, CLI::CLIDriver::ByteCode& aBC,
                 std::vector<std::size_t>& aBlockStack)
  {
    if (aObj.objType == CLI::CLICoreObj::ObjType::OT_CONTINUE)
    {
      // Set jump back to the for_iter opcode
      aBC.byte_code.push_back(intToByte(JUMP_ABSOLUTE));
      
      // Find the jump back instruction: look for the first loop instruction
      // in the stack to jump to since aBlockStack.back() may not contain the right PC.
      // @note not all the jump back instructions in the block stack are loops,
      // some may be, for example, if statements.
      auto pc_ja = findLoopInstructionFromBlockStackPC(aBC, aBlockStack);
      assert(pc_ja >= 0);
      
      const auto& ba_ja = intToByteArray(pc_ja);
      aBC.byte_code.push_back(ba_ja[0]);
      aBC.byte_code.push_back(ba_ja[1]);
    }
    else
    {
      assert(aObj.objType == CLI::CLICoreObj::ObjType::OT_BREAK);
      aBC.byte_code.push_back(intToByte(BREAK_LOOP));
    }
  }//bcFlowObj
  
  /// Adds an instruction for loading an object attribute in the byte-code
  void bcLoadAttr(const CLI::CLICoreObj& aObj, CLI::CLIDriver::ByteCode& aBC)
  {
    // Set opcode
    aBC.byte_code.push_back(intToByte(LOAD_ATTR));
    
    // Set name
    auto it = std::find(aBC.global_array.begin(), aBC.global_array.end(), aObj.strObj);
    if (it == aBC.global_array.end())
    {
      aBC.global_array.push_back(aObj.strObj);
      const auto& ba = intToByteArray(static_cast<int>(aBC.global_array.size() - 1));
      aBC.byte_code.push_back(ba[0]);
      aBC.byte_code.push_back(ba[1]);
    }
    else
    {
      const auto& ba = intToByteArray(static_cast<int>(it - aBC.global_array.begin()));
      aBC.byte_code.push_back(ba[0]);
      aBC.byte_code.push_back(ba[1]);
    }
    
    // After loading the attribute there are two objects
    // on the stack:
    // 1) the object the attribute was loaded from;
    // 2) the attribute itself.
    // However, at the end of loading an attribute the stack must contain
    // only the loaded attribute.
    // Therefore the following applies:
    // - swap the two top elements (object_with_attribute, attribute)
    // - delete the top of the stack
    aBC.byte_code.push_back(intToByte(ROT_TWO));
    aBC.byte_code.push_back(intToByte(POP_TOP));
  }//bcLoadAttr
  
  /// Adds an instruction for storing an object attribute in the byte-code
  void bcStoreAttr(const CLI::CLICoreObj& aObj, CLI::CLIDriver::ByteCode& aBC)
  {
    // Set opcode
    aBC.byte_code.push_back(intToByte(STORE_ATTR));
    
    // Set name
    auto it = std::find(aBC.global_array.begin(), aBC.global_array.end(), aObj.strObj);
    if (it == aBC.global_array.end())
    {
      aBC.global_array.push_back(aObj.strObj);
      const auto& ba = intToByteArray(static_cast<int>(aBC.global_array.size() - 1));
      aBC.byte_code.push_back(ba[0]);
      aBC.byte_code.push_back(ba[1]);
    }
    else
    {
      const auto& ba = intToByteArray(static_cast<int>(it - aBC.global_array.begin()));
      aBC.byte_code.push_back(ba[0]);
      aBC.byte_code.push_back(ba[1]);
    }
  }//bcStoreAttr
  
  /// Adds a call function instruction into the byte-code
  void bcCallFcn(const CLI::CLICoreObj& aObj, CLI::CLIDriver::ByteCode& aBC)
  {
    // Set opcode
    aBC.byte_code.push_back(intToByte(CALL_FUNCTION));
    
    // Set number of arguments
    const auto& ba = intToByteArray(aObj.size);
    aBC.byte_code.push_back(ba[0]);
    aBC.byte_code.push_back(ba[1]);
  }//bcCallFcn
  
  /// Adds a build tuple instruction in the byte-code
  void bcBuildTuple(const CLI::CLICoreObj& aObj, CLI::CLIDriver::ByteCode& aBC)
  {
    // Set opcode
    aBC.byte_code.push_back(intToByte(BUILD_TUPLE));
    
    // Set number of parameters
    const auto& ba = intToByteArray(aObj.size);
    aBC.byte_code.push_back(ba[0]);
    aBC.byte_code.push_back(ba[1]);
  }//bcBuildTuple
  
  // If "aObj" is a binary operator object returns the
  // correspondent opcode. Otherwise returns 0
  int isBinOpObject(const CLI::CLICoreObj& aObj)
  {
    const auto& ot = aObj.objType;
    switch (ot)
    {
      case CLI::CLICoreObj::ObjType::OT_PLUS:
        return BINARY_ADD;
      case  CLI::CLICoreObj::ObjType::OT_MIN:
        return BINARY_SUBTRACT;
      case  CLI::CLICoreObj::ObjType::OT_PROD:
        return BINARY_MULTIPLY;
      case  CLI::CLICoreObj::ObjType::OT_DIV:
        return BINARY_DIVIDE;
      case  CLI::CLICoreObj::ObjType::OT_LT:
        return COMPARE_OP;
      case  CLI::CLICoreObj::ObjType::OT_LE:
        return COMPARE_OP;
      case  CLI::CLICoreObj::ObjType::OT_GT:
        return COMPARE_OP;
      case  CLI::CLICoreObj::ObjType::OT_GE:
        return COMPARE_OP;
      case  CLI::CLICoreObj::ObjType::OT_EQ:
        return COMPARE_OP;
      case  CLI::CLICoreObj::ObjType::OT_NQ:
        return COMPARE_OP;
      case  CLI::CLICoreObj::ObjType::OT_AND:
        return BINARY_AND;
      case  CLI::CLICoreObj::ObjType::OT_OR:
        return BINARY_OR;
      case  CLI::CLICoreObj::ObjType::OT_SUBSCR:
        return BINARY_SUBSCR;
      default:
        return 0;
    }
  }//isBinOpObject
  
  // Returns the code for the comparison operator "aObj" or
  // -1 if "aObj" is not a comparison operator
  int isComparisonOperator(const CLI::CLICoreObj& aObj)
  {
    const auto& ot = aObj.objType;
    switch (ot)
    {
      case CLI::CLICoreObj::ObjType::OT_LT:
        return OP_LT;
      case CLI::CLICoreObj::ObjType::OT_LE:
        return OP_LE;
      case CLI::CLICoreObj::ObjType::OT_GT:
        return OP_GT;
      case CLI::CLICoreObj::ObjType::OT_GE:
        return OP_GE;
      case CLI::CLICoreObj::ObjType::OT_EQ:
        return OP_EQ;
      case CLI::CLICoreObj::ObjType::OT_NQ:
        return OP_NE;
      default:
        return -1;
    }
  }//isComparisonOperator
  
  /// Returns the code for the variable assignment object
  /// or -1 if "aObj" is not a variable assignment object
  static int isVarDeclObject(const CLI::CLICoreObj& aObj)
  {
    if (aObj.objType == CLI::CLICoreObj::ObjType::OT_SNG) return DOM_SINGLETON;
    if (aObj.objType == CLI::CLICoreObj::ObjType::OT_RNG) return DOM_BOUNDS;
    if (aObj.objType == CLI::CLICoreObj::ObjType::OT_SET) return DOM_LIST;
    if (aObj.objType == CLI::CLICoreObj::ObjType::OT_SNG_M) return DOM_SINGLETON_M;
    if (aObj.objType == CLI::CLICoreObj::ObjType::OT_RNG_M) return DOM_BOUNDS_M;
    if (aObj.objType == CLI::CLICoreObj::ObjType::OT_SET_M) return DOM_LIST_M;
    return -1;
  }//isVarDeclObject
  
  /// Adds a variable declaration instruction in the byte-code
  void btVarDecl(const CLI::CLICoreObj& aObj, CLI::CLIDriver::ByteCode& aBC)
  {
    int domType = isVarDeclObject(aObj);
    assert(domType >= 0);
    assert(aObj.size + 1 < MAX_REP_NUM);
    
    // Push domain type on the stack
    bcCoreObj(CLI::CLICoreObj(static_cast<CLI_P_INT>(domType)), aBC);
    
    // Push build domain opcode
    aBC.byte_code.push_back(intToByte(BUILD_DOMAIN));
    
    // Push size of domain
    const auto& ba = intToByteArray(aObj.size + 1);
    aBC.byte_code.push_back(ba[0]);
    aBC.byte_code.push_back(ba[1]);
    
    // Push variable specification opcode
    // Set opcode
    CLI::CLIDriver::ByteArray varSpec;
    aBC.byte_code.push_back(intToByte(SET_VAR_SPEC));
    if (aObj.objOptimization == CLI::CLICoreObj::ObjType::OT_OPT_MAX)
    {
      varSpec = intToByteArray(static_cast<int>(VAR_SPEC_OPT_MAX));
    }
    else if (aObj.objOptimization == CLI::CLICoreObj::ObjType::OT_OPT_MIN)
    {
      varSpec = intToByteArray(static_cast<int>(VAR_SPEC_OPT_MIN));
    }
    else
    {
      varSpec = intToByteArray(static_cast<int>(VAR_SPEC_NONE));
    }
    aBC.byte_code.push_back(varSpec[0]);
    aBC.byte_code.push_back(varSpec[1]);
    
    // Push variable declaration
    // Set opcode
    aBC.byte_code.push_back(intToByte(STORE_VAR));
    
    // Set name
    auto it = std::find(aBC.global_array.begin(), aBC.global_array.end(), aObj.strObj);
    if (it == aBC.global_array.end())
    {
      aBC.global_array.push_back(aObj.strObj);
      const auto& ba = intToByteArray(static_cast<int>(aBC.global_array.size() - 1));
      aBC.byte_code.push_back(ba[0]);
      aBC.byte_code.push_back(ba[1]);
    }
    else
    {
      const auto& ba = intToByteArray(static_cast<int>(it - aBC.global_array.begin()));
      aBC.byte_code.push_back(ba[0]);
      aBC.byte_code.push_back(ba[1]);
    }
  }//btVarDecl
  
  /// Adds an instruction to build a variable domain from a range min..max in the byte-code
  void btBuildRangeDomain(const CLI::CLICoreObj& aObj, CLI::CLIDriver::ByteCode& aBC)
  {
    // Check if the range is (un)bounded
    if (aObj.size == DOM_BOUNDS_OPEN)
    {
      // Range type "..".
      // Set the two infinity bounds
      bcCoreObjInt(CLI::CLICoreObj(getNegInfinity()), aBC);
      bcCoreObjInt(CLI::CLICoreObj(getPosInfinity()), aBC);
    }
    else if (aObj.size == DOM_BOUNDS_OPEN_RIGHT)
    {
      // Range type "n..".
      // Set the upper infinity bound.
      // @note the lower bound is already on the stack
      bcCoreObjInt(CLI::CLICoreObj(getPosInfinity()), aBC);
    }
    else if (aObj.size == DOM_BOUNDS_OPEN_LEFT)
    {
      // Range type "..n".
      // Set the lower infinity bound.
      // @note the upper bound is on the stack and it must be swapped with the lower bound
      bcCoreObjInt(CLI::CLICoreObj(getNegInfinity()), aBC);
      
      // Swap the last bytecodes
      aBC.byte_code.push_back(intToByte(ROT_TWO));
    }
  }//btBuildRangeDomain
  
  /// Add an instruction to build a range list object as [a..b]
  void btBuildRangeExprList(const CLI::CLICoreObj& aObj, CLI::CLIDriver::ByteCode& aBC)
  {
    // Creates a list from TOS1 to TOS
    aBC.byte_code.push_back(intToByte(RANGE_LIST));
  }//btBuildRangeExprList
  
  /// Add an instruction to build a list object as [a, b, c, d]
  void btBuildList(const CLI::CLICoreObj& aObj, CLI::CLIDriver::ByteCode& aBC)
  {
    // Set opcode
    aBC.byte_code.push_back(intToByte(BUILD_LIST));
    
    // Set list size
    const auto& ba = intToByteArray(aObj.size);
    aBC.byte_code.push_back(ba[0]);
    aBC.byte_code.push_back(ba[1]);
  }//btBuildList
  
  /// Add an assignment instruction in the byte-code
  void btAssign(const CLI::CLICoreObj& aObj, CLI::CLIDriver::ByteCode& aBC, bool aStoreFast=false)
  {
    // Set opcode
    if(aStoreFast)
    {
      aBC.byte_code.push_back(intToByte(STORE_FAST));
    }
    else
    {
      aBC.byte_code.push_back(intToByte(STORE_NAME));
    }
    
    // Set name
    auto it = std::find(aBC.global_array.begin(), aBC.global_array.end(), aObj.strObj);
    if (it == aBC.global_array.end())
    {
      if(aStoreFast)
      {
        aBC.local_array.push_back(aObj.strObj);
        const auto& ba = intToByteArray(static_cast<int>(aBC.local_array.size() - 1));
        aBC.byte_code.push_back(ba[0]);
        aBC.byte_code.push_back(ba[1]);
      }
      else
      {
        aBC.global_array.push_back(aObj.strObj);
        const auto& ba = intToByteArray(static_cast<int>(aBC.global_array.size() - 1));
        aBC.byte_code.push_back(ba[0]);
        aBC.byte_code.push_back(ba[1]);
      }
    }
    else
    {
      if(aStoreFast)
      {
        const auto& ba = intToByteArray(static_cast<int>(it - aBC.local_array.begin()));
        aBC.byte_code.push_back(ba[0]);
        aBC.byte_code.push_back(ba[1]);
      }
      else
      {
        const auto& ba = intToByteArray(static_cast<int>(it - aBC.global_array.begin()));
        aBC.byte_code.push_back(ba[0]);
        aBC.byte_code.push_back(ba[1]);
      }
    }
  }//btAssign
  
  /// Adds a for-loop instruction in the byte-code
  void btForLoop(CLI::CLIDriver::ByteCode& aBC, std::vector<std::size_t>& aBlockStack)
  {
    /*
     * A for-loop generates the following byte code:
     * SETUP_LOOP
     */
    
    // Add PC of SETUP_LOOP opcode on block stack
    aBlockStack.push_back(aBC.byte_code.size());
    
    // Setup loop opcode
    aBC.byte_code.push_back(intToByte(SETUP_LOOP));
    
    // The following will be replaced later when a corresponding
    // end object is found
    aBC.byte_code.push_back(0);
    aBC.byte_code.push_back(0);
  }//btForLoop
  
  /// Adds an if block instruction in the byte-code
  void btIfBlock(CLI::CLIDriver::ByteCode& aBC, std::vector<std::size_t>& aBlockStack)
  {
    /*
     * An if-block generates the following byte code:
     * POP_JUMP_IF_FALSE
     */
    
    // Add PC of POP_JUMP_IF_FALSE opcode on block stack
    aBlockStack.push_back(aBC.byte_code.size());
    
    // Setup loop opcode
    aBC.byte_code.push_back(intToByte(POP_JUMP_IF_FALSE));
    
    // The following will be replaced later when a corresponding
    // end object is found
    aBC.byte_code.push_back(0);
    aBC.byte_code.push_back(0);
  }//btIfBlock
  
  /// Adds an else block instruction in the byte-code
  void btElseBlock(CLI::CLIDriver::ByteCode& aBC, std::vector<std::size_t>& aBlockStack)
  {
    /*
     * An else-block generates the following byte code:
     * JUMP_FORWARD
     */
    
    // Add PC of POP_JUMP_IF_FALSE opcode on block stack
    aBlockStack.push_back(aBC.byte_code.size());
    
    // Setup loop opcode
    aBC.byte_code.push_back(intToByte(JUMP_FORWARD));
    
    // The following will be replaced later when a corresponding
    // end object is found
    aBC.byte_code.push_back(0);
    aBC.byte_code.push_back(0);
  }//btElseBlock
  
  /// Adds an end if block instruction in the byte-code
  void btEndIfBlock(CLI::CLIDriver::ByteCode& aBC, std::vector<std::size_t>& aBlockStack,
                    bool aJumpToNextInstruction)
  {
    if (aBlockStack.empty())
    {
      throw std::runtime_error("empty if block");
    }
    
    // Get the PC where the if (else) block is set
    auto pc_ja = aBlockStack.back();
    aBlockStack.pop_back();
    
    assert(aBC.byte_code.size() >= pc_ja);
    
    // Set the value to the current PC when jumping from the if (else) statement:
    // @note this is a relative or absolute jump depending on the instruction at position pc_ja:
    // - POP_JUMP_IF_FALSE is an absolute jump
    // - JUMP_FORWARD is a relative jump
    auto jumpTo = static_cast<int>(aBC.byte_code.size());
    if (aBC.byte_code[pc_ja] != intToByte(POP_JUMP_IF_FALSE))
    {
      // Relative jump
      jumpTo -= static_cast<int>(pc_ja);
    }
    
    if (aJumpToNextInstruction)
    {
      jumpTo += 3;
    }
    
    const auto& ba = intToByteArray(jumpTo);
    aBC.byte_code[pc_ja + 1] = ba[0];
    aBC.byte_code[pc_ja + 2] = ba[1];
  }//btEndIfBlock
  
  /// Adds a for-loop range iterator to the byte-code
  void btForLoopRangeIter(const CLI::CLICoreObj& aIter, CLI::CLIDriver::ByteCode& aBC,
                          std::vector<std::size_t>& aBlockStack, bool aIsList, int aListLength)
  {
    /*
     * A for-loop generates the following byte code:
     * LOAD_CONST...
     * RANGE_LIST
     * GET_ITER
     * FOR_ITER
     * STORE_NAME
     *
     * Update 4/7/19
     * The list object is already created and present on the stack.
     */
    
    // Update 4/7/19
    //aIsList = true;
    
    // Call the function to create the range list [aStart, aEnd]
    // @note add a range list instruction only if the object is a range list,
    // otherwise keep the list as is
    if (aIsList)
    {
      // Update 4/17/19
      
      // Set opcode
      aBC.byte_code.push_back(intToByte(BUILD_LIST));
      
      // Set list size
      const auto& ba = intToByteArray(aListLength);
      aBC.byte_code.push_back(ba[0]);
      aBC.byte_code.push_back(ba[1]);
      
      // aBC.byte_code.push_back(intToByte(RANGE_LIST));
    }
    
    // Setup get_iter
    aBC.byte_code.push_back(intToByte(GET_ITER));
    
    // Add PC of FOR_ITER opcode on block stack
    aBlockStack.push_back(aBC.byte_code.size());
    
    // Setup loop opcode
    aBC.byte_code.push_back(intToByte(FOR_ITER));
    // The following will be replaced later when a corresponding
    // end object is found
    aBC.byte_code.push_back(0);
    aBC.byte_code.push_back(0);
    
    // Store name (fast), i.e., no reporting into the context
    btAssign(aIter, aBC, true);
  }//btForLoopRangeIter
  
  /// Returns true of the given object is a list type object, returns false otherwise
  bool isListObject(const CLI::CLICoreObj& aObj)
  {
    return
    aObj.objType == CLI::CLICoreObj::ObjType::OT_LIST_ARGS ||
    aObj.objType == CLI::CLICoreObj::ObjType::OT_LIST_ARRAY ||
    aObj.objType == CLI::CLICoreObj::ObjType::OT_LIST_DOMAIN;
  }//isListObject
  
  /// Adds an end block instruction to the byte-code
  void btEndBlock(CLI::CLIDriver::ByteCode& aBC, std::vector<std::size_t>& aBlockStack)
  {
    if (aBlockStack.size() <= 1)
    {
      throw std::runtime_error("Invalid block");
    }
    auto pc_ja = aBlockStack.back();
    aBlockStack.pop_back();
    
    // Get the correspondent for_iter and set this PC
    assert(aBC.byte_code.size() >= pc_ja);
    
    // Set the delta to add to the current PC when jumping relative fro the for_iter statement:
    // aBC.byte_code.size() - pc_ja + 3
    // calculate the delta needed to be added to the current PC to jump after
    // the for-loop block where this current point (size) is the end of the block
    // and +3 is the next instruction
    const auto& ba = intToByteArray(static_cast<int>(aBC.byte_code.size() - pc_ja + 3));
    aBC.byte_code[pc_ja + 1] = ba[0];
    aBC.byte_code[pc_ja + 2] = ba[1];
    
    // Set jump back to the for_iter opcode
    aBC.byte_code.push_back(intToByte(JUMP_ABSOLUTE));
    
    // Set number of parameters
    const auto& ba_ja = intToByteArray(static_cast<int>(pc_ja));
    aBC.byte_code.push_back(ba_ja[0]);
    aBC.byte_code.push_back(ba_ja[1]);
    
    auto pc_pb = aBlockStack.back();
    aBlockStack.pop_back();
    
    // Get the correspondent for_iter and set this PC
    assert(aBC.byte_code.size() > pc_pb + 1);
    const auto& ba_pb = intToByteArray(static_cast<int>(aBC.byte_code.size() + 1));
    aBC.byte_code[pc_pb + 1] = ba_pb[0];
    aBC.byte_code[pc_pb + 2] = ba_pb[1];
    
    // Set pop_block
    aBC.byte_code.push_back(intToByte(POP_BLOCK));
  }//btEndBlock
  
  // Returns true if the given object is a "printable" object, false otherwise.
  // Printable objects are:
  // - Core objects
  // - Binary operator objects
  // - arrays
  // - function calls
  // - attributes
  // - assignments
  bool isPrintableObject(const CLI::CLICoreObj& aObj)
  {
    auto objType = aObj.objType;
    if (isCoreObject(aObj) || isBinOpObject(aObj) ||
        (objType == CLI::CLICoreObj::ObjType::OT_LIST_ARRAY) ||
        (objType == CLI::CLICoreObj::ObjType::OT_CALL_FCN)   ||
        (objType == CLI::CLICoreObj::ObjType::OT_ATTR)       ||
        (objType == CLI::CLICoreObj::ObjType::OT_RNG_LIST))
    {
      return true;
    }
    
    return false;
  }//isPrintableObject
  
}// namespace

namespace CLI {

  CLIDriver::CLIDriver()
  : pInStreamIsLine(true)
  , pLexerError(false)
  , pParserError(false)
  , pParser(nullptr)
  , pScanner(nullptr)
  {
  }
  
  void CLIDriver::debug(const std::string& str)
  {
    std::cout << str << std::endl;
  }//debug
  
  void CLIDriver::setLHSStartAssign()
  {
    pStack.push(CLICoreObj(CLICoreObj::ObjType::OT_LHS_START_MARKER));
    pLHSStartAssignStack.push_back(pStack.getLastElementPtr());
  }// setLHSStartAssign
  
  void CLIDriver::popLHSStartAssign()
  {
    auto ptr = pLHSStartAssignStack.back();
    pStack.remove(ptr);
    
    pLHSStartAssignStack.pop_back();
  }// popLHSStartAssign
  
  void CLIDriver::setLHSEndAssign()
  {
    pStack.push(CLICoreObj(CLICoreObj::ObjType::OT_LHS_END_MARKER));
    pLHSEndAssignStack.push_back(pStack.getLastElementPtr());
  }// setLHSEndAssign
  
  void CLIDriver::setRHSStartAssign()
  {
    pStack.push(CLICoreObj(CLICoreObj::ObjType::OT_RHS_START_MARKER));
    pRHSStartAssignStack.push_back(pStack.getLastElementPtr());
  }// setRHSStartAssign
  
  void CLIDriver::setRHSEndAssign()
  {
    pStack.push(CLICoreObj(CLICoreObj::ObjType::OT_RHS_END_MARKER));
    pRHSEndAssignStack.push_back(pStack.getLastElementPtr());
  }// setRHSEndAssign
  
  void CLIDriver::setSeqExprStart()
  {
    pStack.push(CLICoreObj(CLICoreObj::ObjType::OT_SEQ_EXPR_START_MARKER));
    pSeqExprStartStack.push_back(pStack.getLastElementPtr());
  }// setSeqExprStart
  
  void CLIDriver::addAssignExpr()
  {
    // Get the sublist from RHSStart to RHSEnd
    if (pRHSStartAssignStack.empty())
    {
      throw std::runtime_error("RHS Start stack empty");
    }
    
    if (pRHSEndAssignStack.empty())
    {
      throw std::runtime_error("RHS End stack empty");
    }
    auto RHSStart = pRHSStartAssignStack.back();
    auto RHSEnd = pRHSEndAssignStack.back();
    pRHSStartAssignStack.pop_back();
    pRHSEndAssignStack.pop_back();
    
    // Get the RHS substack and remove the first and last tags
    auto rhsSubStack = pStack.getSubStack(RHSStart, RHSEnd);
    rhsSubStack.popFront();
    rhsSubStack.pop();
    
    // RHS stack should not be empty
    if (rhsSubStack.isEmpty())
    {
      throw std::runtime_error("Empty RHS expression");
    }
    
    // Get the sublist from LHSStart to LHSEnd
    if (pLHSStartAssignStack.empty())
    {
      throw std::runtime_error("LHS Start stack empty");
    }
    
    if (pLHSEndAssignStack.empty())
    {
      throw std::runtime_error("LHS End stack empty");
    }
    auto LHSStart = pLHSStartAssignStack.back();
    auto LHSEnd = pLHSEndAssignStack.back();
    pLHSStartAssignStack.pop_back();
    pLHSEndAssignStack.pop_back();
    
    // Get the pointer into the stack where to insert the two merged substacks.
    // @note the merged substack will be inserted "before" insertionPtr.
    // For more information see "insert(...)" method.
    // To preserve the right pointer, keep track of the preceding immutable pointer.
    auto insertionPtr = LHSStart;
    insertionPtr--;
    
    // Get the LHS substack and remove the first and last tags
    auto lhsSubStack = pStack.getSubStack(LHSStart, LHSEnd);
    lhsSubStack.popFront();
    lhsSubStack.pop();
    
    // Find the first OT_STRING on the lhs and use it as lhs expression identifier
    // in the assignment
    auto lhsStackPtr = lhsSubStack.begin();
    
    CLI_P_STR lhsId;
    for (; lhsStackPtr != lhsSubStack.end(); ++lhsStackPtr)
    {
      if ((*lhsStackPtr).objType == CLICoreObj::ObjType::OT_STRING)
      {
        lhsId = (*lhsStackPtr).strObj;
        break;
      }
    }
    
    
    if (lhsSubStack.isEmpty())
    {
      throw std::runtime_error("Empty LHS expression");
    }
    
    // Connect RHS with LHS
    rhsSubStack.merge(lhsSubStack);
    
    // Set assign code.
    // There are three possible cases:
    // 1) attribute assignment: the back of the stack has a load attribute type;
    // 2) subscr assignment: the back of the stack as a tuple representing the subscript indices;
    // 3) everything else: standard assignment
    if (rhsSubStack.top().objType == CLICoreObj::ObjType::OT_ATTR)
    {
      // Convert it into assign attribute.
      // @note the object on top already has the identifier of the property to assign the value to
      rhsSubStack.top().objType = CLICoreObj::ObjType::OT_ASSIGN_ATTR;
    }
    else if (rhsSubStack.top().objType == CLICoreObj::ObjType::OT_SUBSCR)
    {
      // Add an new object of subscript assignement type
      rhsSubStack.top().objType = CLICoreObj::ObjType::OT_ASSIGN_SUBSCR;
      
      // Add the id of the lhs assignment
      rhsSubStack.top().strObj = lhsId;
    }
    else
    {
      // Add an new object of general assignement type.
      // @note avoid loading the same name as the assigning name. This comes from the
      // expr -> ID rule
      if (rhsSubStack.top().objType == CLICoreObj::ObjType::OT_STRING &&
          rhsSubStack.top().strObj == lhsId)
      {
        // Convert the type to an ASSIGN type instead of loading the object
        rhsSubStack.top().objType = CLICoreObj::ObjType::OT_ASSIGN;
      }
      else
      {
        rhsSubStack.push(CLICoreObj(CLICoreObj::ObjType::OT_ASSIGN));
      }
      
      // Add the id of the lhs assignment
      rhsSubStack.top().strObj = lhsId;
    }
    
    // Insert RHS substack into the driver's stack.
    // @note insert the stack right before the current pointer.
    // Increase it since previously it was decreased
    insertionPtr++;
    pStack.insert(rhsSubStack, insertionPtr);
  }// addAssignExpr
  
  void CLIDriver::addLoadProperty(const std::string& aProp)
  {
    if (aProp.empty())
    {
      throw std::runtime_error("Empty property name");
    }
    
    pStack.push(CLICoreObj(aProp));
    pStack.top().objType = CLICoreObj::ObjType::OT_ATTR;
  }// addLoadProperty
  
  void CLIDriver::addUnaryOperators(const std::vector<bool>& aOperators)
  {
    // Add a unary pos or neg on the stack according to the Boolean value,
    // for each element in the list
    for (auto op : aOperators)
    {
      if (op)
      {
        pStack.push(CLICoreObj(CLICoreObj::ObjType::OT_UNARY_POS));
      }
      else
      {
        pStack.push(CLICoreObj(CLICoreObj::ObjType::OT_UNARY_NEG));
      }
    }
  }//addUnaryOperators
  
  void CLIDriver::addExpr(CLICoreObj::ObjType aEOP)
  {
    // Check the stack size, expressions are higher level than core expressions
    assert(pStack.getSize() > 1);
    
    // If at least one of the expressions is not a core scalar expression,
    // push it on the top of the stack and return asap
    if (!isScalarObject(pStack.top()) || !isScalarObject(pStack.peek(1)))
    {
      pStack.push(CLICoreObj(aEOP));
      return;
    }
    
    // Otherwise reduce the objects
    auto obj1 = pStack.top().intObj;
    auto obj1T = pStack.top().objType;
    pStack.pop();
    
    auto obj2 = pStack.top().intObj;
    auto obj2T = pStack.top().objType;
    pStack.pop();
    
    // Check Boolean types for type promotion
    auto isBool = obj1T == CLICoreObj::ObjType::OT_BOOL || obj2T == CLICoreObj::ObjType::OT_BOOL;
    switch (aEOP)
    {
      case CLICoreObj::ObjType::OT_PLUS:
        pStack.push(CLICoreObj(obj2 + obj1));
        break;
      case CLICoreObj::ObjType::OT_MIN:
        pStack.push(CLICoreObj(obj2 - obj1));
        break;
      case CLICoreObj::ObjType::OT_PROD:
        pStack.push(CLICoreObj(obj2 * obj1));
        break;
      case CLICoreObj::ObjType::OT_DIV:
      {
        assert(obj1 != 0);
        pStack.push(CLICoreObj(obj2 / obj1));
      }
        break;
      case CLICoreObj::ObjType::OT_LT:
        pStack.push(CLICoreObj(obj2 < obj1));
        break;
      case CLICoreObj::ObjType::OT_LE:
        pStack.push(CLICoreObj(obj2 <= obj1));
        break;
      case CLICoreObj::ObjType::OT_GT:
        pStack.push(CLICoreObj(obj2 > obj1));
        break;
      case CLICoreObj::ObjType::OT_GE:
        pStack.push(CLICoreObj(obj2 >= obj1));
        break;
      case CLICoreObj::ObjType::OT_EQ:
        pStack.push(CLICoreObj(obj2 == obj1));
        break;
      case CLICoreObj::ObjType::OT_NQ:
        pStack.push(CLICoreObj(obj2 != obj1));
        break;
      case CLICoreObj::ObjType::OT_AND:
        pStack.push(CLICoreObj(obj2 && obj1));
        break;
      case CLICoreObj::ObjType::OT_OR:
        pStack.push(CLICoreObj(obj2 || obj1));
        break;
      case CLICoreObj::ObjType::OT_REIF:
        pStack.push(CLICoreObj(obj2 == obj1));
        break;
      default:
        assert(aEOP == CLICoreObj::ObjType::OT_REIF);
        break;
    }// switch
    
    // Perform type promotion to Boolean type
    pStack.top().objType = isBool ? CLICoreObj::ObjType::OT_BOOL : CLICoreObj::ObjType::OT_INT;
  }//addExpr
  
  void CLIDriver::markEndOfStmt(bool aIsEmpty)
  {
    // Check if the top of the stack needs to be printed
    auto needsPrint = !aIsEmpty && isPrintableObject(pStack.top());
    if (needsPrint)
    {
      // If so, add a PRINT instruction
      pStack.push(CLICoreObj(CLICoreObj::ObjType::OT_PRINT_TOP));
    }
    
    // If the object needs to be printed or it is a function call, remove the top of the stack
    // by adding a POP instruction
    if (needsPrint || (pStack.top().objType == CLICoreObj::ObjType::OT_CALL_FCN))
    {
      pStack.push(CLICoreObj(CLICoreObj::ObjType::OT_POP_TOP));
    }
  }//markEndOfStmt
  
  void CLIDriver::addEndIfBlock(bool aJumpToNextInstruction)
  {
    // Push end block object on stack
    pStack.push(CLICoreObj(CLICoreObj::ObjType::OT_END_IF_BLK));
    pStack.top().intObj = 0;
    if (aJumpToNextInstruction)
    {
      pStack.top().intObj = 1;
    }
  }//addEndIfBlock
  
  void CLIDriver::setObjectWithList()
  {
    if (pSeqExprStartStack.empty())
    {
      throw std::runtime_error("Empty sequence expression marker stack");
    }
    
    // Get the pointer to the beginning of this sequence and remove the marker from the stack.
    // @note the following operation returns the element following the removed element
    auto ptr = pStack.remove(pSeqExprStartStack.back());
    
    // Remove the pointer from the stack
    pSeqExprStartStack.pop_back();
    
    // Get the pointer to the element preceding the sequence expression and check if it is
    // a core object of type OT_STRING.
    ptr--;
    if ((*ptr).objType == CLICoreObj::ObjType::OT_STRING)
    {
      // If the object preceding the list is a string, check the last object of the sequence.
      // If the type is:
      // - OT_LIST_ARGS: mark id(...) as a function call
      // - OT_LIST_ARRAY: mark id[...] as a subscript operator
      auto seqType = pStack.top().objType;
      if (seqType == CLICoreObj::ObjType::OT_LIST_ARRAY)
      {
        // If list size is equal to 1 and the only element in the list is a OT_STRING object,
        // this is a object load property, i.e., obj["prop"]
        if (pStack.top().size == 1 && pStack.peek(1).objType == CLICoreObj::ObjType::OT_STRING &&
            isQuotedString(pStack.peek(1).strObj))
        {
          // Remove the list and transform the previous in a store_attribute object
          pStack.pop();
          pStack.top().objType = CLICoreObj::ObjType::OT_ATTR;
          
          return;
        }
        
        // Otherwise create a tuple as subscript index and a subscript object type
        // on top of the stack.
        // 1) Copy the object on top of the stack
        CLICoreObj topCopy = pStack.top();
        
        // 2) Change the top of the stack to be a tuple.
        //    @note the size of the tuple is set in the object which was previously a OT_LIST_ARRAY
        pStack.top().objType = CLICoreObj::ObjType::OT_TUPLE;
        
        // 3) Change the copied object type to SUBSCR
        topCopy.objType = CLICoreObj::ObjType::OT_SUBSCR;
        
        // 4) Push the object on top of the stack
        pStack.push(topCopy);
        
        // 5) Mark the object preceding the sequence as an object with list
        (*ptr).objWithList = true;
      }
      else if (seqType == CLICoreObj::ObjType::OT_LIST_ARGS)
      {
        // Create a function call.
        // 1) mark the top of the stack as function call
        pStack.top().objType = CLICoreObj::ObjType::OT_CALL_FCN;
        
        // 2) Mark the object preceding the sequence as an object with list
        (*ptr).objWithList = true;
      }
    }
  }//setObjectWithList
  
  void CLIDriver::addEndBlock()
  {
    // Push end block object on stack
    pStack.push(CLICoreObj(CLICoreObj::ObjType::OT_END_BLK));
  }//addEndBlock
  
  void CLIDriver::addForLoopRangeIter(const std::string& aID, CLICoreObj::ObjType aIterType,
                                      int aListLength)
  {
    if (aID.empty())
    {
      throw std::runtime_error("Empty for-loop iterator");
    }
    pStack.push(CLICoreObj(CLICoreObj::ObjType::OT_FOR_LOOP_RANGE_ITER));
    
    if (aIterType != CLICoreObj::ObjType::OT_RNG_LIST &&
        aIterType != CLICoreObj::ObjType::OT_LIST_ARRAY)
    {
      throw std::runtime_error("Unrecognized list in for-loop statement");
    }
    
    if (aIterType == CLICoreObj::ObjType::OT_LIST_ARRAY)
    {
      pStack.top().objWithList = true;
      pStack.top().size = aListLength;
    }
    
    pStack.push(CLICoreObj(aID));
  }//addForLoopRangeIter
  
  void CLIDriver::addLetExpr(const std::string& aID)
  {
    // Push on stack a new object id
    pStack.push(CLICoreObj(aID));
    
    // Set its type
    pStack.top().objType = CLICoreObj::ObjType::OT_LET;
  }//addLetExpr
  
  void CLIDriver::addVarDecl(const std::string& aID, CLICoreObj::ObjType aType, int aDomSize,
                             int aOpt)
  {
    if (aID.empty())
    {
      throw std::runtime_error("Empty variable identifier");
    }
    
    // Push on the stack a new object id
    pStack.push(CLICoreObj(aID));
    
    // Set the type of the domain (i.e., list or set)
    pStack.top().objType = aType;
    
    // Set domain's size
    pStack.top().size = aDomSize;
    
    // Set optimization if any
    if (aOpt)
    {
      pStack.top().objOptimization = (aOpt == 1) ?
      CLICoreObj::ObjType::OT_OPT_MIN :
      CLICoreObj::ObjType::OT_OPT_MAX;
    }
  }//addVarDecl
  
  void CLIDriver::addMatrixVarDecl(const std::string& aID, CLICoreObj::ObjType aType, int aDomSize,
                                   int aOpt)
  {
    if (aID.empty())
    {
      throw std::runtime_error("Empty variable identifier");
    }
    assert(pStack.top().objType == CLICoreObj::ObjType::OT_LIST_ARRAY);
    
    // Push on the stack a new object id
    pStack.push(CLICoreObj(aID));
    
    // Set the type of its domain
    switch (aType)
    {
      case CLI::CLICoreObj::ObjType::OT_RNG:
        aType = CLI::CLICoreObj::ObjType::OT_RNG_M;
        break;
      default:
        assert(aType == CLI::CLICoreObj::ObjType::OT_SET);
        aType = CLI::CLICoreObj::ObjType::OT_SET_M;
        break;
    }
    pStack.top().objType = aType;
    
    // Set its size.
    // @note add 1 to the size of the domain since
    // for matrix domains there is one more element, i.e., the size of the matrix
    pStack.top().size = aDomSize + 1;
    
    // Set optimization if any
    if (aOpt)
    {
      pStack.top().objOptimization = (aOpt == 1) ?
      CLICoreObj::ObjType::OT_OPT_MIN :
      CLICoreObj::ObjType::OT_OPT_MAX;
    }
  }//addMatrixVarDecl
  
  bool CLIDriver::isFlowStmtWithinLoop(DriverStack::StackIter it)
  {
    // Look for the OT_FOR_LOOP or OT_WHILE_LOOP backwards
    bool found{ false };
    DriverStack::StackIter itCopy = it;
    for (; it != pStack.begin(); --it)
    {
      if ((*it).objType == CLICoreObj::ObjType::OT_FOR_LOOP ||
          (*it).objType == CLICoreObj::ObjType::OT_WHILE_LOOP)
      {
        found = true;
        break;
      }
    }
    if (!found) return false;
    
    // Look for the OT_END_BLK forward
    found = false;
    for (; itCopy != pStack.end(); ++itCopy)
    {
      if ((*itCopy).objType == CLICoreObj::ObjType::OT_END_BLK)
      {
        found = true;
        break;
      }
    }
    return found;
  }//isFlowStmtWithinLoop
  
  CLIDriver::ByteCode CLIDriver::byteCode(const ByteCode& aEnvironment)
  {
    ByteCode bc = aEnvironment;
    
    // Remove return value from previous environment if any
    if (bc.byte_code.size() > 0 && byteToInt(bc.byte_code[bc.byte_code.size() - 1]) == RETURN_VALUE)
    {
      bc.byte_code.resize(bc.byte_code.size() - 1);
    }
    
    // Pick each element from the bottom of the stack to the top and generate the
    // corresponding byte-code.
    // @note skip the first marker on the bottom of the stack
    auto stackIter = pStack.begin();
    assert((*stackIter).objType == CLICoreObj::ObjType::OT_STACK_MARKER);
    stackIter++;
    
    for (; stackIter != pStack.end(); ++stackIter)
    {
      auto& obj = *stackIter;
      if (isCoreObject(obj))
      {
        bcCoreObj(obj, bc);
      }
      else if (isFlowObject(obj))
      {
        // Check if the control flow statement is within a loop-statement
        if (!isFlowStmtWithinLoop(stackIter))
        {
          error("Flow statement not within a loop");
          throw std::logic_error(std::string("Flow statement not within a loop"));
        }
        bcFlowObj(obj, bc, pBlockStackPC);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_ATTR)
      {
        bcLoadAttr(obj, bc);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_ASSIGN_ATTR)
      {
        bcStoreAttr(obj, bc);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_CALL_FCN)
      {
        bcCallFcn(obj, bc);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_POP_TOP)
      {
        bc.byte_code.push_back(intToByte(POP_TOP));
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_PRINT_TOP)
      {
        bc.byte_code.push_back(intToByte(PRINT_TOP));
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_MARK)
      {
        bc.byte_code.push_back(intToByte(ADD_MARK));
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_UNARY_NEG)
      {
        bc.byte_code.push_back(intToByte(UNARY_NEGATIVE));
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_UNARY_POS)
      {
        bc.byte_code.push_back(intToByte(UNARY_POSITIVE));
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_TUPLE)
      {
        bcBuildTuple(obj, bc);
      }
      else if (int binOp = isBinOpObject(obj))
      {
        bc.byte_code.push_back(intToByte(binOp));
        int compOP = isComparisonOperator(obj);
        if (compOP >= 0)
        {
          const auto& ba = intToByteArray(compOP);
          bc.byte_code.push_back(ba[0]);
          bc.byte_code.push_back(ba[1]);
        }
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_SNG ||
               obj.objType == CLICoreObj::ObjType::OT_RNG ||
               obj.objType == CLICoreObj::ObjType::OT_SET ||
               obj.objType == CLICoreObj::ObjType::OT_SNG_M ||
               obj.objType == CLICoreObj::ObjType::OT_RNG_M ||
               obj.objType == CLICoreObj::ObjType::OT_SET_M)
      {
        // Variable declaration object
        btVarDecl(obj, bc);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_RNG_DOMAIN)
      {
        // Domain declaration as min..max
        btBuildRangeDomain(obj, bc);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_RNG_LIST)
      {
        // Range list as [a..b]
        btBuildRangeExprList(obj, bc);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_LIST_ARRAY)
      {
        // List array as [a, b, c, d]
        btBuildList(obj, bc);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_ASSIGN ||
               obj.objType == CLICoreObj::ObjType::OT_LET)
      {
        btAssign(obj, bc);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_ASSIGN_SUBSCR)
      {
        bc.byte_code.push_back(intToByte(STORE_SUBSCR));
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_FOR_LOOP)
      {
        btForLoop(bc, pBlockStackPC);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_IF_BLK)
      {
        btIfBlock(bc, pBlockStackPC);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_ELSE_BLK)
      {
        btElseBlock(bc, pBlockStackPC);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_END_IF_BLK)
      {
        btEndIfBlock(bc, pBlockStackPC, obj.intObj == 1);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_FOR_LOOP_RANGE_ITER)
      {
        stackIter++;
        if (stackIter == pStack.end())
        {
          throw std::runtime_error("invalid for-loop block");
        }
        
        btForLoopRangeIter((*stackIter), bc, pBlockStackPC, obj.objWithList, obj.size);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_END_BLK)
      {
        if (pBlockStackPC.empty())
        {
          // Parse "end" as a string
          obj.objType = CLICoreObj::ObjType::OT_STRING;
          bcCoreObj(obj, bc);
        }
        else
        {
          btEndBlock(bc, pBlockStackPC);
        }
      }
    } // for
    
    // Add return opcode
    bc.byte_code.push_back(intToByte(RETURN_VALUE));
    
    // Return the byte-code
    return bc;
  }//byteCode
  
  void printByteCode(const CLIDriver::ByteCode& aBC)
  {
    bool nl{ false };
    if (!aBC.const_array.empty())
    {
      nl = true;
      std::cout << "Constants:\n";
      for (auto& it : aBC.const_array)
      {
        assert(!it.empty());
        if (it.size() == 1)
        {
          if (it[0].getType() == 0)
          {
            std::cout << it[0].getInt() << ' ';
          }
          else
          {
            assert(it[0].getType() == 1);
            std::cout << it[0].getString() << ' ';
          }
        }
        else
        {
          std::cout << '(';
          std::string arr;
          for (const auto& e : it)
          {
            std::stringstream ss;
            if (e.getType() == 0)
            {
              std::cout << e.getInt() << ' ';
            }
            else
            {
              assert(e.getType() == 1);
              std::cout << e.getString() << ' ';
            }
            arr += ss.str() + ", ";
          }
          arr.pop_back();
          arr.pop_back();
          std::cout << arr << ") ";
        }
      }//for
      std::cout << '\n';
    }
    
    if (!aBC.global_array.empty())
    {
      nl = true;
      std::cout << "Global names:\n";
      for (auto it : aBC.global_array)
      {
        std::cout << it << ' ';
      }
      std::cout << '\n';
    }
    
    if (!aBC.local_array.empty())
    {
      nl = true;
      std::cout << "Local names:\n";
      for (auto it : aBC.local_array)
      {
        std::cout << it << ' ';
      }
      std::cout << '\n';
    }
    
    if (nl) std::cout << '\n';
    for (std::size_t it{ 0 }; it < aBC.byte_code.size(); ++it)
    {
      std::cout << it << '\t';
      auto opcode = byteToInt(aBC.byte_code.at(it));
      
      std::string code;
      if (opcode == ADD_MARK) code = "ADD_MARK";
      if (opcode == POP_BLOCK) code = "POP_BLOCK";
      if (opcode == UNARY_NOT) code = "UNARY_NOT";
      if (opcode == UNARY_NEGATIVE) code = "UNARY_NEGATIVE";
      if (opcode == UNARY_POSITIVE) code = "UNARY_POSITIVE";
      if (opcode == BINARY_MULTIPLY) code = "BINARY_MULTIPLY";
      if (opcode == BINARY_DIVIDE) code = "BINARY_DIVIDE";
      if (opcode == BINARY_MODULO) code = "BINARY_MODULO";
      if (opcode == BINARY_ADD) code = "BINARY_ADD";
      if (opcode == BINARY_SUBTRACT) code = "BINARY_SUBTRACT";
      if (opcode == BINARY_AND) code = "BINARY_AND";
      if (opcode == BINARY_OR) code = "BINARY_OR";
      if (opcode == BINARY_XOR) code = "BINARY_XOR";
      if (opcode == BINARY_SUBSCR) code = "BINARY_SUBSCR";
      if (opcode == BREAK_LOOP) code = "BREAK_LOOP";
      if (opcode == LOAD_CONST) code = "LOAD_CONST";
      if (opcode == LOAD_CONST_BOOL) code = "LOAD_CONST_BOOL";
      if (opcode == LOAD_NAME) code = "LOAD_NAME";
      if (opcode == LOAD_ATTR) code = "LOAD_ATTR";
      if (opcode == STORE_ATTR) code = "STORE_ATTR";
      if (opcode == STORE_NAME) code = "STORE_NAME";
      if (opcode == STORE_FAST) code = "STORE_FAST";
      if (opcode == COMPARE_OP) code = "COMPARE_OP";
      if (opcode == BUILD_LIST) code = "BUILD_LIST";
      if (opcode == JUMP_ABSOLUTE) code = "JUMP_ABSOLUTE";
      if (opcode == SETUP_LOOP) code = "SETUP_LOOP";
      if (opcode == FOR_ITER) code = "FOR_ITER";
      if (opcode == POP_JUMP_IF_FALSE) code = "POP_JUMP_IF_FALSE";
      if (opcode == JUMP_FORWARD) code = "JUMP_FORWARD";
      if (opcode == CALL_FUNCTION) code = "CALL_FUNCTION";
      if (opcode == BUILD_DOMAIN) code = "BUILD_DOMAIN";
      if (opcode == BUILD_TUPLE) code = "BUILD_TUPLE";
      if (opcode == STORE_VAR) code = "STORE_VAR ";
      if (opcode == POP_TOP) code = "POP_TOP";
      if (opcode == ROT_TWO) code = "ROT_TWO";
      if (opcode == PRINT_TOP) code = "PRINT_TOP";
      if (opcode == RETURN_VALUE) code = "RETURN_VALUE";
      if (opcode == GET_ITER) code = "GET_ITER";
      if (opcode == RANGE_LIST) code = "RANGE_LIST";
      if (opcode == SET_VAR_SPEC) code = "SET_VAR_SPEC";
      if (opcode == STORE_SUBSCR) code = "STORE_SUBSCR";
      assert(!code.empty());
      
      std::cout << code << '\t';
      if (opcode > HAVE_ARGUMENT)
      {
        int argLower = aBC.byte_code.at(++it);
        int argUpper = aBC.byte_code.at(++it) << 8;
        auto arg = argUpper + argLower;
        
        std::cout << arg;
        if (opcode == LOAD_CONST || opcode == LOAD_CONST_BOOL)
        {
          assert(arg < aBC.const_array.size());
          std::cout << " (";
          std::string arr;
          for (auto& e : aBC.const_array.at(arg))
          {
            std::stringstream ss;
            if (e.getType() == 0)
            {
              ss << e.getInt();
            }
            else
            {
              assert(e.getType() == 1);
              ss << e.getString();
            }
            arr += ss.str() + ", ";
          }
          arr.pop_back();
          arr.pop_back();
          std::cout << arr << ')';
        }
        else if (opcode == LOAD_NAME)
        {
          assert(arg < aBC.global_array.size());
          std::cout << " (" << aBC.global_array.at(arg) << ')';
        }
        else if (opcode == STORE_NAME || opcode == STORE_ATTR)
        {
          assert(arg < aBC.global_array.size());
          std::cout << " (" << aBC.global_array.at(arg) << ')';
        }
        else if (opcode == STORE_FAST)
        {
          assert(arg < aBC.local_array.size());
          std::cout << " (" << aBC.local_array.at(arg) << ')';
        }
        else if (opcode == STORE_VAR)
        {
          assert(arg < aBC.global_array.size());
          std::cout << " (" << aBC.global_array.at(arg) << ')';
        }
      }
      std::cout << '\n';
    }
  }//printByteCode
  
  void CLIDriver::printStack()
  {
    for (auto it = pStack.begin(); it != pStack.end(); ++it)
    {
      if (isCoreObject(*it))
      {
        switch ((*it).objType)
        {
          case CLICoreObj::ObjType::OT_INT:
            std::cout << "LOAD_CONST\t" << it->intObj;
            break;
          case CLICoreObj::ObjType::OT_BOOL:
          {
            std::string boolVal = it->intObj ? "true" : "false";
            std::cout << "LOAD_CONST_BOOL\t" << boolVal;
          }
            break;
          default:
            assert((*it).objType == CLICoreObj::ObjType::OT_STRING);
            std::cout << "LOAD_NAME\t" << it->strObj;
            break;
        }
      }//isCoreObj
      else if (isListObject(*it))
      {
        assert(it->size >= 0);
        switch ((*it).objType)
        {
          case CLICoreObj::ObjType::OT_LIST_ARGS:
            std::cout << "CALL_FUNCTION\t" << it->size;
            break;
          case CLICoreObj::ObjType::OT_LIST_ARRAY:
            std::cout << "BUILD_LIST\t" << it->size;
            break;
          default:
            assert((*it).objType == CLICoreObj::ObjType::OT_LIST_DOMAIN);
            std::cout << "{} s:" << it->size;
            break;
        }
      }
      else if ((*it).objType == CLICoreObj::ObjType::OT_RNG_LIST) std::cout << "OT_RNG_LIST";
      else if ((*it).objType == CLICoreObj::ObjType::OT_ATTR) std::cout << "LOAD_ATTR";
      else if ((*it).objType == CLICoreObj::ObjType::OT_ASSIGN_ATTR) std::cout << "STORE_ATTR";
      else if ((*it).objType == CLICoreObj::ObjType::OT_CALL_FCN) std::cout << "CALL_FUNCTION\t" <<
        it->size;
      else if ((*it).objType == CLICoreObj::ObjType::OT_UNARY_NEG) std::cout << "UNARY_NEG";
      else if ((*it).objType == CLICoreObj::ObjType::OT_UNARY_POS) std::cout << "UNARY_POS";
      else if ((*it).objType == CLICoreObj::ObjType::OT_PLUS) std::cout << "BINARY_ADD";
      else if ((*it).objType == CLICoreObj::ObjType::OT_MIN) std::cout << "BINARY_SUBTRACT";
      else if ((*it).objType == CLICoreObj::ObjType::OT_PROD) std::cout << "BINARY_MULTIPLY";
      else if ((*it).objType == CLICoreObj::ObjType::OT_DIV) std::cout << "BINARY_TRUE_DIVIDE";
      else if ((*it).objType == CLICoreObj::ObjType::OT_LT) std::cout << "COMPARE_OP\t<";
      else if ((*it).objType == CLICoreObj::ObjType::OT_LE) std::cout << "COMPARE_OP\t<=";
      else if ((*it).objType == CLICoreObj::ObjType::OT_GT) std::cout << "COMPARE_OP\t>";
      else if ((*it).objType == CLICoreObj::ObjType::OT_GE) std::cout << "COMPARE_OP\t>=";
      else if ((*it).objType == CLICoreObj::ObjType::OT_EQ) std::cout << "COMPARE_OP\t==";
      else if ((*it).objType == CLICoreObj::ObjType::OT_NQ) std::cout << "COMPARE_OP\t!=";
      else if ((*it).objType == CLICoreObj::ObjType::OT_AND) std::cout << "BINARY_AND";
      else if ((*it).objType == CLICoreObj::ObjType::OT_OR) std::cout << "BINARY_OR";
      else if ((*it).objType == CLICoreObj::ObjType::OT_REIF) std::cout << "BINARY_REIF";
      else if ((*it).objType == CLICoreObj::ObjType::OT_SUBSCR) std::cout << "BINARY_SUBSCR";
      else if ((*it).objType == CLICoreObj::ObjType::OT_ASSIGN_SUBSCR) std::cout << "STORE_SUBSCR";
      else if ((*it).objType == CLICoreObj::ObjType::OT_POP_TOP) std::cout << "POP_TOP";
      else if ((*it).objType == CLICoreObj::ObjType::OT_PRINT_TOP) std::cout << "PRINT_TOP";
      else if ((*it).objType == CLICoreObj::ObjType::OT_LET) std::cout << "LET";
      else if ((*it).objType == CLICoreObj::ObjType::OT_BREAK) std::cout << "BREAK";
      else if ((*it).objType == CLICoreObj::ObjType::OT_CONTINUE) std::cout << "CONTINUE";
      else if ((*it).objType == CLICoreObj::ObjType::OT_RETURN) std::cout << "RETURN";
      else if ((*it).objType == CLICoreObj::ObjType::OT_ASSIGN) std::cout << "STORE_NAME\t" <<
        it->strObj;
      else if ((*it).objType == CLICoreObj::ObjType::OT_SNG) std::cout <<
        "BUILD_DOMAIN\t([d])\n" << "STORE_VAR\t" << it->strObj;
      else if ((*it).objType == CLICoreObj::ObjType::OT_RNG) std::cout <<
        "BUILD_DOMAIN\t([d1, d2])\n" << "STORE_VAR\t" << it->strObj;
      else if ((*it).objType == CLICoreObj::ObjType::OT_SET) std::cout <<
        "BUILD_DOMAIN\t({d1, ...})\n" << "STORE_VAR\t" << it->strObj;
      else if ((*it).objType == CLICoreObj::ObjType::OT_SNG_M) std::cout <<
        "BUILD_DOMAIN\t([d][])\n" << "STORE_VAR\t" << it->strObj;
      else if ((*it).objType == CLICoreObj::ObjType::OT_RNG_M) std::cout <<
        "BUILD_DOMAIN\t([d1, d2][])\n" << "STORE_VAR\t" << it->strObj;
      else if ((*it).objType == CLICoreObj::ObjType::OT_SET_M) std::cout <<
        "BUILD_DOMAIN\t({d1, ...}[])\n" << "STORE_VAR\t" << it->strObj;
      else if ((*it).objType == CLICoreObj::ObjType::OT_TUPLE) std::cout << "BUILD_TUPLE";
      else if ((*it).objType == CLICoreObj::ObjType::OT_IF_BLK) std::cout << "JUMP_IF";
      else if ((*it).objType == CLICoreObj::ObjType::OT_ELSE_BLK) std::cout << "ELSE";
      else if ((*it).objType == CLICoreObj::ObjType::OT_FOR_LOOP) std::cout << "SETUP_LOOP";
      else if ((*it).objType == CLICoreObj::ObjType::OT_FOR_LOOP_RANGE_ITER) std::cout <<
        "FOR_LOOP_RANGE_ITER";
      else if ((*it).objType == CLICoreObj::ObjType::OT_END_BLK) std::cout <<
        "JUMP_ABSOLUTE\nPOP_BLOCK";
      else if ((*it).objType == CLICoreObj::ObjType::OT_END_IF_BLK) std::cout << "END_IF";
      else if ((*it).objType == CLICoreObj::ObjType::OT_RNG_LIST) std::cout << "RANGE_LIST";
      else if ((*it).objType == CLICoreObj::ObjType::OT_LHS_START_MARKER) std::cout
        << "OT_LHS_START_MARKER";
      else if ((*it).objType == CLICoreObj::ObjType::OT_LHS_END_MARKER) std::cout
        << "OT_LHS_END_MARKER";
      else if ((*it).objType == CLICoreObj::ObjType::OT_RHS_START_MARKER) std::cout
        << "OT_RHS_START_MARKER";
      else if ((*it).objType == CLICoreObj::ObjType::OT_RHS_END_MARKER) std::cout
        << "OT_RHS_END_MARKER";
      else if ((*it).objType == CLICoreObj::ObjType::OT_SEQ_EXPR_START_MARKER) std::cout
        << "OT_SEQ_EXPR_START_MARKER";
      else if ((*it).objType == CLICoreObj::ObjType::OT_STACK_MARKER) std::cout
        << "OT_STACK_MARKER";
      else if ((*it).objType == CLICoreObj::ObjType::OT_RNG_DOMAIN) std::cout
        << "OT_RNG_DOMAIN";
      else
      {
        std::cout << "Unrecognized code: " << static_cast<int>((*it).objType);
      }
      std::cout << '\n';
    }
  }//printStack
  
  void CLIDriver::resetDriver()
  {
    pStack.clear();
    pLHSStartAssignStack.clear();
    pLHSEndAssignStack.clear();
    pRHSStartAssignStack.clear();
    pRHSEndAssignStack.clear();
    pSeqExprStartStack.clear();
    pBlockStackPC.clear();
    pLexerError = false;
    pParserError = false;
    pErrMsg.clear();
    
    // Add a first dummy object at the bottom of the stack
    pStack.push(CLICoreObj(CLICoreObj::ObjType::OT_STACK_MARKER));
  }//resetDriver
  
  void CLIDriver::parse(std::istream& aISS)
  {
    if (!aISS.good() && aISS.eof()) return;
    
    // Clean internal data structures before parsing
    resetDriver();
    
    // Run parser
    parseHelper(aISS);
  }//parse
  
  void CLIDriver::parseHelper(std::istream &aStream)
  {
    try
    {
      pScanner.reset(new CLI::CLIScanner(&aStream, pInStreamIsLine));
    }
    catch (std::bad_alloc &ba)
    {
      std::cerr << "Failed to allocate scanner: (" <<
      ba.what() << "), exiting\n";
      exit(EXIT_FAILURE);
    }
    
    try
    {
      pParser.reset(new CLI::CLIParser((*(pScanner.get())) /* scanner */,
                                       (*this)              /* driver */));
    }
    catch (std::bad_alloc &ba)
    {
      std::cerr << "Failed to allocate parser: (" <<
      ba.what() << "), exiting\n";
      exit(EXIT_FAILURE);
    }
    
    try
    {
      pParser->parse();
    }
    catch (std::exception& e)
    {
      std::cerr << e.what();
      exit(EXIT_FAILURE);
    }
    catch (...)
    {
      std::cerr << "Failed to parse, exiting\n";
      exit(EXIT_FAILURE);
    }
  }
  
  void CLIDriver::setLexerError(const std::string& aStr)
  {
    pLexerError = true;
    error("Unrecognized symbol: " + aStr);
  }//setLexerError
  
  void CLIDriver::setParserError(const std::string& aStr, const CLIParser::location_type& aLoc)
  {
    pParserError = true;
    error(aStr, aLoc);
  }//setParserError
  
  void CLIDriver::error(const std::string& aStr, const CLIParser::location_type& aLoc)
  {
    std::stringstream ss;
    ss << aStr << " at " << aLoc;
    pErrMsg = ss.str();
  }//error
  
  void CLIDriver::error(const std::string& aStr)
  {
    pErrMsg = aStr;
  }//error
  
}// end namespace CLI
