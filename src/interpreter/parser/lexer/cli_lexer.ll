%{
/* C++ string header, for string ops below */
#include <string>
#include <limits>

#include "CLICoreObj.hpp"

/* Implementation of yyFlexScanner */ 
#include "CLIScanner.hpp"
#undef  YY_DECL
#define YY_DECL int CLI::CLIScanner::yylex( CLI::CLIParser::semantic_type * const lval, CLI::CLIParser::location_type *loc )

/* typedef to make the returns for the tokens shorter */
using token = CLI::CLIParser::token;

/* define yyterminate as this instead of NULL */
#define yyterminate() return( token::ENDF )

/* msvc2010 requires that we exclude this header file. */
#define YY_NO_UNISTD_H

/* update location on matching */
#define YY_USER_ACTION loc->step(); loc->columns(yyleng);

%}

%option debug
%option nodefault
%option yyclass="CLI::CLIScanner"
%option noyywrap
%option c++

%%
%{
            /** Code executed at the beginning of yylex **/
            yylval = lval;
            
            /* Get entry point from scanner */
            if(CLI::CLIScanner::needsCheckOnEntryPoint())
            {
              // Reset entry point check: not needed anymore
              CLI::CLIScanner::resetCheckOnEntryPoint();
              
              if(CLI::CLIScanner::isLineEntryPoint())
              {
                return( token::START_LINE );
              }
              return( token::START_FILE );
            }
%}

","	  {
				return( token::COMMA );
			}

";"	  {
				return( token::SEMICOLON );
			}

"\n"        {
               // Update line number
               loc->lines();
               return( token::NEWLINE );
            }
\\n         {
               // Update line number
               loc->lines();
               return( token::NEWLINE );
            }

[ \t\r]+  {
				    // Consume white spaces
            loc->step();
			    }

[0-9]+  {
          errno = 0;
          auto n = strtol (yytext, nullptr, 10);
          if (errno == ERANGE)
          {
            yylval->build<CLI_P_INT>(-1);
          }
          else
          {
            yylval->build<CLI_P_INT>(n);
          }
          return( token::INUMBER );
        }

"INF"   {
				  yylval->build<CLI_P_INT>(CLI_P_INT_INF);
          return( token::INUMBER );
        }

"let"   {
          return( token::LET );
        }

"true"  {
				  return( token::TRUE );
        }

"false" {
				  return( token::FALSE );
			  }

"minimize"  {
              return( token::MINIMIZE );
            }

"maximize"  {
              return( token::MAXIMIZE );
            }

"if"        {
              return( token::IF );
            }

"elseif"    {
              return( token::ELSEIF );
            }

"else"      {
              return( token::ELSE );
            }

"for"	      {
				      return( token::FOR );
            }

"in"        {
              return( token::IN );
            }

"break"     {
              return( token::BREAK );
            }

"continue"  {
              return( token::CONTINUE );
            }

"end"       {
				      return( token::END );
			      }

[_a-zA-Z][_a-zA-Z0-9]*		{
								/**
								 * Section 10.1.5.1 of the 3.0.2 Bison Manual says the 
								 * following should work:
								 * yylval.build( yytext );
								 * but it doesn't.
								 * ref: http://goo.gl/KLn0w2
								 */
								yylval->build< std::string >( yytext );
								return( token::IDENTIFIER );
							}

[/][/].*	{
				// Skip comments on a single line
			}

"\""	{
				return( token::QUOTE );
			}

\<		{
				return( token::LT );
			}

\<=		{
				return( token::LE );
			}

>			{
				return( token::GT );
			}

>=		{
				return( token::GE );
			}

"&&"	{
				return( token::AND );
			}

"||"	{
				return( token::OR );
			}

\<=>	{
				return( token::REIF );
			}

"=="	{
				return( token::EQ );
			}

=			{
				return( token::ASSIGN );
			}

!=		{
				return( token::NQ );
			}

\+		{
				return( token::PLUS );
			}

-			{
				return( token::MINUS );
			}

\*		{
				return( token::PROD );
			}

\/		{
				return( token::DIV );
			}

\(		{
				return( token::LEFTPAR );
			}

\)		{
				return( token::RIGHTPAR );
			}

\{		{
				return( token::LEFTGPAR );
			}

\}		{
				return( token::RIGHTGPAR );
			}

\[		{
				return( token::LEFTQPAR );
			}

\]		{
				return( token::RIGHTQPAR );
			}

\:		{
				return( token::COLON );
			}

"."   {
        return( token::DOT );
      }

".."	{
				return( token::RANGE );
			}

\"[^'\n\r\f]*\"		{
						/**
						* Section 10.1.5.1 of the 3.0.2 Bison Manual says the 
				    	* following should work:
						* yylval.build( yytext );
						* but it doesn't.
						* ref: http://goo.gl/KLn0w2
						*/
						yylval->build< std::string >( yytext );
						return( token::STRING );
					}

[&%$#@!_~<>=]+	{
					return( token::SYMBOL );
				}

.           {
				// Treat everything else as an error
				yylval->build< std::string >( yytext );
                yy_flush_buffer(YY_CURRENT_BUFFER);
                BEGIN(INITIAL);
                return( token::LEXERROR );
            }
%%
