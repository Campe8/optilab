// Base class
#include "Variable.hpp"

// Domain
#include "Domain.hpp"

// Domain element
#include "DomainElementManager.hpp"
#include "DomainElement.hpp"

// Domain events
#include "DomainEvent.hpp"
#include "DomainEventInc.hpp"

// Semantics
#include "VariableSemanticDecision.hpp"
#include "VariableSemanticSupport.hpp"
#include "VariableSemanticObjective.hpp"

#include <utility>
#include <cassert>

namespace Core {
  
  Variable::Variable(VariableSemanticUPtr aVariableSemantic)
  : CoreObject(CoreObjectClass::CORE_OBJECT_VARIABLE)
  , pVariableSemantic(std::move(aVariableSemantic))
  , pBaseVariableSemantic(nullptr)
  {
    assert(pVariableSemantic.get());
  }
  
  Variable::Variable(const Variable& aOther)
  : CoreObject(CoreObjectClass::CORE_OBJECT_VARIABLE)
  {
    pVariableType = new VariableType(*aOther.pVariableType);
    
    auto varSemanticClass = aOther.pVariableSemantic->getSemanticClassType();
    switch(varSemanticClass)
    {
      case VariableSemanticClassType::VAR_SEMANTIC_CLASS_DECISION:
        pVariableSemantic.reset(new VariableSemanticDecision(*(VariableSemanticDecision::cast(aOther.pVariableSemantic.get()))));
        break;
      case VariableSemanticClassType::VAR_SEMANTIC_CLASS_OBJECTIVE:
        pVariableSemantic.reset(new VariableSemanticObjective(*(VariableSemanticObjective::cast(aOther.pVariableSemantic.get()))));
        break;
      default:
        assert(varSemanticClass == VariableSemanticClassType::VAR_SEMANTIC_CLASS_SUPPORT);
        pVariableSemantic.reset(new VariableSemanticSupport(*(VariableSemanticSupport::cast(aOther.pVariableSemantic.get()))));
    }
  }
  
  Variable::Variable(Variable&& aOther)
  : CoreObject(CoreObjectClass::CORE_OBJECT_VARIABLE)
  {
    pVariableType = new VariableType(*aOther.pVariableType);
    
    auto varSemanticClass = aOther.pVariableSemantic->getSemanticClassType();
    switch (varSemanticClass)
    {
      case VariableSemanticClassType::VAR_SEMANTIC_CLASS_DECISION:
        pVariableSemantic.reset(new VariableSemanticDecision(*(VariableSemanticDecision::cast(aOther.pVariableSemantic.get()))));
        break;
      case VariableSemanticClassType::VAR_SEMANTIC_CLASS_OBJECTIVE:
        pVariableSemantic.reset(new VariableSemanticObjective(*(VariableSemanticObjective::cast(aOther.pVariableSemantic.get()))));
        break;
      default:
        assert(varSemanticClass == VariableSemanticClassType::VAR_SEMANTIC_CLASS_SUPPORT);
        pVariableSemantic.reset(new VariableSemanticSupport(*(VariableSemanticSupport::cast(aOther.pVariableSemantic.get()))));
    }
    
    delete aOther.pVariableType;
    aOther.pVariableType = nullptr;
    aOther.pVariableSemantic.reset(nullptr);
  }
  
  Variable::~Variable()
  {
    delete pVariableType;
  }
  
  Variable& Variable::operator= (const Variable& aOther)
  {
    if (this != &aOther)
    {
      pVariableType = new VariableType(*aOther.pVariableType);
      auto varSemanticClass = aOther.pVariableSemantic->getSemanticClassType();
      switch (varSemanticClass)
      {
        case VariableSemanticClassType::VAR_SEMANTIC_CLASS_DECISION:
          pVariableSemantic.reset(new VariableSemanticDecision(*(VariableSemanticDecision::cast(aOther.pVariableSemantic.get()))));
          break;
        case VariableSemanticClassType::VAR_SEMANTIC_CLASS_OBJECTIVE:
          pVariableSemantic.reset(new VariableSemanticObjective(*(VariableSemanticObjective::cast(aOther.pVariableSemantic.get()))));
          break;
        default:
          assert(varSemanticClass == VariableSemanticClassType::VAR_SEMANTIC_CLASS_SUPPORT);
          pVariableSemantic.reset(new VariableSemanticSupport(*(VariableSemanticSupport::cast(aOther.pVariableSemantic.get()))));
      }
    }
    return *this;
  }
  
  Variable& Variable::operator= (Variable&& aOther)
  {
    if (this != &aOther)
    {
      pVariableType = new VariableType(*aOther.pVariableType);
      auto varSemanticClass = aOther.pVariableSemantic->getSemanticClassType();
      switch (varSemanticClass)
      {
        case VariableSemanticClassType::VAR_SEMANTIC_CLASS_DECISION:
          pVariableSemantic.reset(new VariableSemanticDecision(*(VariableSemanticDecision::cast(aOther.pVariableSemantic.get()))));
          break;
        case VariableSemanticClassType::VAR_SEMANTIC_CLASS_OBJECTIVE:
          pVariableSemantic.reset(new VariableSemanticObjective(*(VariableSemanticObjective::cast(aOther.pVariableSemantic.get()))));
          break;
        default:
          assert(varSemanticClass == VariableSemanticClassType::VAR_SEMANTIC_CLASS_SUPPORT);
          pVariableSemantic.reset(new VariableSemanticSupport(*(VariableSemanticSupport::cast(aOther.pVariableSemantic.get()))));
      }
      
      delete aOther.pVariableType;
      aOther.pVariableType = nullptr;
      aOther.pVariableSemantic.reset(nullptr);
    }
    return *this;
  }
  
  void Variable::uploadSemantic(const VariableSemanticSPtr& aSemantic)
  {
    assert(aSemantic);
    
    // Use lazy initialization pattern, copy the original semantic
    // only on first upload
    if(!pBaseVariableSemantic)
    {
      pBaseVariableSemantic.reset(pVariableSemantic->clone());
    }
    
    pVariableSemantic->setBranchingPolicy(aSemantic->isBranching());
    pVariableSemantic->setInputOrder(aSemantic->getInputOrder());
    pVariableSemantic->setReachability(aSemantic->isReachable());
    pVariableSemantic->setBranchingStrategyType(aSemantic->getBranchingStrategyType());
  }//uploadSemantic
  
  std::size_t Variable::getDegree() const
  {
    return 0;
  }//getDegree
  
}// end namespace Core
