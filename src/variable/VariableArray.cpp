// Base class
#include "VariableArray.hpp"

#include <cassert>
#include <stdexcept>

namespace Core {
  
  VariableArray::VariableArray(std::size_t aNumCells,
                               VariableSemanticUPtr aVariableSemantic)
  : VariableMatrix(std::move(aVariableSemantic))
  , pVariableArray(boost::extents[aNumCells])
  , pDomainListMod(true)
  {
    initArray();
  }
  
  void VariableArray::initArray()
  {
    for (std::size_t cellIdx = 0; cellIdx < size(); ++cellIdx)
    {
      // assign nullptr to each array cell
      assignVariableToCell(cellIdx, nullptr);
    }
    pDomainListMod = true;
  }//initMatrix
  
  bool VariableArray::isa(const Variable* aVariable)
  {
    return aVariable &&
    aVariable->getType()->getTypeClass() == VariableTypeClass::VAR_TYPE_ARRAY;
  }//isa
  
  VariableArray* VariableArray::cast(const Variable* aVariable)
  {
    if(!isa(aVariable)) return nullptr;
    return static_cast<VariableArray*>(const_cast<Variable*>(aVariable));
  }//cast
  
  void VariableArray::uploadSemanticToDimensions(const VariableSemanticSPtr& aSemantic)
  {
    for(std::size_t idx{0}; idx < size(); ++idx)
    {
      pVariableArray[idx]->uploadSemantic(aSemantic);
    }
  }//uploadSemanticToDimensions
  
  void VariableArray::assignVariableToCell(std::size_t aCell, VariableSPtr aVarToAssign)
  {
    // Avoid recursion
    assert(!(aVarToAssign != nullptr && aVarToAssign.get() == this));
    
    if (aCell >= size())
    {
      throw std::out_of_range("VariableArray out_of_range exception");
    }
    
    if (!aVarToAssign)
    {
      pVariableArray[aCell] = aVarToAssign;
      return;
    }
    
    pVariableArray[aCell] = aVarToAssign;
    pDomainListMod = true;
  }//assignVariableToCell
  
  VariableSPtr VariableArray::at(std::size_t aCell) const
  {
    boost::array<arrayType::index, 1> idx = { { static_cast<long>(aCell) } };
    return pVariableArray(idx);
  }//at
  
  const DomainListPtr VariableArray::domainList() const
  {
    if(pDomainListMod)
    {
      pDomainList.clear();
      for(std::size_t idx = 0; idx < size(); ++idx)
      {
        for(auto& domPtr : *at(idx)->domainList())
        {
          pDomainList.push_back(domPtr);
        }
      }
      pDomainListMod = false;
    }
    
    return &pDomainList;
  }//getDomain
  
  bool VariableArray::isAssigned() const
  {
    for (std::size_t cellIdx = 0; cellIdx < size(); ++cellIdx)
    {
      if (!at(cellIdx) || !at(cellIdx)->isAssigned())
      {
        return false;
      }
    }
    
    return true;
  }//isAssigned
  
}// end namespace Core
