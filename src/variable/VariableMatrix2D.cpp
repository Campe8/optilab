// Base class
#include "VariableMatrix2D.hpp"

#include <stdexcept>

namespace Core {
  
  VariableMatrix2D::VariableMatrix2D(std::size_t aNumRows,
                                     std::size_t aNumCols,
                                     VariableSemanticUPtr aVariableSemantic)
  : VariableMatrix(std::move(aVariableSemantic))
  , pNumRows(aNumRows)
  , pNumCols(aNumCols)
  , pVariableMatrix(boost::extents[aNumRows][aNumCols])
  , pDomainListMod(true)
  {
    initMatrix();
  }
  
  void VariableMatrix2D::initMatrix()
  {
    for (std::size_t rowIdx = 0; rowIdx < pNumRows; ++rowIdx)
    {
      for (std::size_t colIdx = 0; colIdx < pNumCols; ++colIdx)
      {
        // assign nullptr to each matrix cell
        assignVariableToCell(rowIdx, colIdx, nullptr);
      }
    }
    
    pDomainListMod = true;
  }//initMatrix
  
  bool VariableMatrix2D::isa(const Variable* aVariable)
  {
    return aVariable &&
    aVariable->getType()->getTypeClass() == VariableTypeClass::VAR_TYPE_MATRIX2D;
  }//isa
  
  VariableMatrix2D* VariableMatrix2D::cast(const Variable* aVariable)
  {
    if(!isa(aVariable)) return nullptr;
    return static_cast<VariableMatrix2D*>(const_cast<Variable*>(aVariable));
  }//cast
  
  void VariableMatrix2D::uploadSemanticToDimensions(const VariableSemanticSPtr& aSemantic)
  {
    for (std::size_t rowIdx = 0; rowIdx < pNumRows; ++rowIdx)
    {
      for (std::size_t colIdx = 0; colIdx < pNumCols; ++colIdx)
      {
        pVariableMatrix[rowIdx][colIdx]->uploadSemantic(aSemantic);
      }
    }
  }//uploadSemanticToDimensions
  
  void VariableMatrix2D::assignVariableToCell(std::size_t aRowIdx, std::size_t aColIdx, VariableSPtr aVarToAssign)
  {
    // Avoid recursion
    assert(!(aVarToAssign != nullptr && aVarToAssign.get() == this));
    
    if (aRowIdx >= pNumRows || aColIdx >= pNumCols)
    {
      throw std::out_of_range("VariableMatrix2D out_of_range exception");
    }
    
    if (!aVarToAssign)
    {
      pVariableMatrix[aRowIdx][aColIdx] = aVarToAssign;
      return;
    }
    
    pVariableMatrix[aRowIdx][aColIdx] = aVarToAssign;
    pDomainListMod = true;
  }//assignVariableToCell
  
  VariableSPtr VariableMatrix2D::at(std::size_t aRowIdx, std::size_t aColIdx) const
  {
    boost::array<matrix2Dtype::index, 2> idx = { { static_cast<long>(aRowIdx), static_cast<long>(aColIdx) } };
    return pVariableMatrix(idx);
  }//at
  
  const DomainListPtr VariableMatrix2D::domainList() const
  {
    if(pDomainListMod)
    {
      for (std::size_t rowIdx = 0; rowIdx < pNumRows; ++rowIdx)
      {
        for (std::size_t colIdx = 0; colIdx < pNumCols; ++colIdx)
        {
          for(auto& domPtr : *at(rowIdx, colIdx)->domainList())
          {
            pDomainList.push_back(domPtr);
          }
        }
      }
      pDomainListMod = false;
    }
    
    return &pDomainList;
  }//getDomain
  
  bool VariableMatrix2D::isAssigned() const
  {
    for (std::size_t rowIdx = 0; rowIdx < pNumRows; ++rowIdx)
    {
      for (std::size_t colIdx = 0; colIdx < pNumCols; ++colIdx)
      {
        if (!at(rowIdx, colIdx) || !at(rowIdx, colIdx)->isAssigned())
        {
          return false;
        }
      }
    }
    
    return true;
  }//isAssigned
  
}// end namespace Core
