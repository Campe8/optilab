// Base class
#include "VariableBoolean.hpp"

// Domain
#include "DomainBoolean.hpp"

// Domain element
#include "DomainElementManager.hpp"
#include "DomainElementInt64.hpp"

// Domain events
#include "DomainEvent.hpp"

#include <utility>

namespace Core {
  
  VariableBoolean::VariableBoolean(VariableSemanticUPtr aVariableSemantic)
  : Variable(std::move(aVariableSemantic))
  {
    pDomain = std::make_shared<DomainBoolean>();
    pDomainList.push_back(pDomain);
  }
  
  VariableBoolean::VariableBoolean(const VariableBoolean& aOther)
  : Variable(aOther)
  {
    pDomain.reset(new DomainBoolean(*(static_cast<DomainBoolean*>(aOther.pDomain.get()))));
  }
  
  VariableBoolean::VariableBoolean(VariableBoolean&& aOther)
  : Variable(aOther)
  {
    pDomain.reset(new DomainBoolean(*(static_cast<DomainBoolean*>(aOther.pDomain.get()))));
    aOther.pDomain = nullptr;
  }
  
  VariableBoolean::~VariableBoolean()
  {
  }
  
  VariableBoolean& VariableBoolean::operator= (const VariableBoolean& aOther)
  {
    if (this != &aOther)
    {
      Variable::operator=(aOther);
      pDomain.reset(new DomainBoolean(*(static_cast<DomainBoolean*>(aOther.pDomain.get()))));
    }
    return *this;
  }
  
  VariableBoolean& VariableBoolean::operator= (VariableBoolean&& aOther)
  {
    if (this != &aOther)
    {
      Variable::operator=(aOther);
      pDomain.reset(new DomainBoolean(*(static_cast<DomainBoolean*>(aOther.pDomain.get()))));
      
      aOther.pDomain = nullptr;
    }
    return *this;
  }
  
  bool VariableBoolean::isAssigned() const
  {
    bool singletonEvent = DomainEventSingleton::isa(pDomain->getEvent());
    if (singletonEvent)
    {
      assert(pDomain->getSize() == 1);
      return true;
    }
    return false;
  }//isAssigned
  
}// end namespace Core
