// Base class
#include "VariableFactory.hpp"

// Domain
#include "Domain.hpp"
#include "DomainBoolean.hpp"

// Domain element
#include "DomainElementManager.hpp"
#include "DomainElement.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementVoid.hpp"

// Domain events
#include "DomainEvent.hpp"
#include "DomainEventInc.hpp"

// Variable
#include "VariableInteger.hpp"
#include "VariableBoolean.hpp"
#include "VariableArray.hpp"
#include "VariableMatrix2D.hpp"

#include <utility>
#include <cassert>

namespace Core {
  
  VariableFactory::VariableFactory()
  {
  }
  
  VariableFactory::~VariableFactory()
  {
  }
  
  Variable* VariableFactory::variableInteger(INT_64 aSingleton, VariableSemanticUPtr aVariableSemantic)
  {
    return variableInteger(DomainElementManager::getInstance().createDomainElementInt64(aSingleton),
                           std::move(aVariableSemantic));
  }//variableInteger
  
  Variable* VariableFactory::variableInteger(INT_64 aLowerBound, INT_64 aUpperBound, VariableSemanticUPtr aVariableSemantic)
  {
    return variableInteger(DomainElementManager::getInstance().createDomainElementInt64(aLowerBound),
                           DomainElementManager::getInstance().createDomainElementInt64(aUpperBound),
                           std::move(aVariableSemantic));
  }//variableInteger
  
  Variable* VariableFactory::variableInteger(const std::unordered_set<INT_64>& aSetOfElements, VariableSemanticUPtr aVariableSemantic)
  {
    std::unordered_set<DomainElementInt64 *> elems;
    for(auto& elem : aSetOfElements)
    {
      elems.insert(DomainElementManager::getInstance().createDomainElementInt64(elem));
    }
    
    return variableInteger(elems, std::move(aVariableSemantic));
  }//variableInteger
  
  Variable* VariableFactory::variableInteger(DomainElementInt64 *aSingleton, VariableSemanticUPtr aVariableSemantic)
  {
    assert(aSingleton);
    return variableInteger(aSingleton, aSingleton, std::move(aVariableSemantic));
  }//variableInteger
  
  Variable* VariableFactory::variableInteger(DomainElementInt64 *aLowerBound,
                                             DomainElementInt64 *aUpperBound,
                                             VariableSemanticUPtr aVariableSemantic)
  {
    assert(aLowerBound);
    assert(aUpperBound);
    auto var = new VariableInteger(aLowerBound, aUpperBound, std::move(aVariableSemantic));
    
    var->setType(new VariableType(VariableTypeClass::VAR_TYPE_INTEGER));
    
    return var;
  }//variableInteger
  
  Variable* VariableFactory::variableInteger(const std::unordered_set<DomainElementInt64 *>& aSetOfElements,
                                             VariableSemanticUPtr aVariableSemantic)
  {
    auto var = new VariableInteger(aSetOfElements, std::move(aVariableSemantic));
    var->setType(new VariableType(VariableTypeClass::VAR_TYPE_INTEGER));
    return var;
  }//variableInteger
  
  Variable* VariableFactory::variableBoolean(VariableSemanticUPtr aVariableSemantic)
  {
    auto var = new VariableBoolean(std::move(aVariableSemantic));
    var->setType(new VariableType(VariableTypeClass::VAR_TYPE_BOOLEAN));
    return var;
  }//variableBoolean
  
  Variable* VariableFactory::variableBoolean(bool aSingleton, VariableSemanticUPtr aVariableSemantic)
  {
    VariableBoolean* var = new VariableBoolean(std::move(aVariableSemantic));
    assert(var->domainList()->size() == 1);
    
    auto domainBool = var->domain();
    if(aSingleton)
    {
      DomainBoolean::cast(domainBool.get())->setTrue();
    }
    else
    {
      DomainBoolean::cast(domainBool.get())->setFalse();
    }
    
    var->setType(new VariableType(VariableTypeClass::VAR_TYPE_BOOLEAN));
    return var;
  }//variableBoolean
  
  Variable* VariableFactory::variableArray(const std::vector<VariableSPtr>& aVarArray, VariableSemanticUPtr aVariableSemantic)
  {
    VariableArray* varArray = new VariableArray(aVarArray.size(), std::move(aVariableSemantic));
    varArray->setType(new VariableType(VariableTypeClass::VAR_TYPE_ARRAY));
    
    std::size_t cell{0};
    for(auto& var : aVarArray)
    {
      varArray->assignVariableToCell(cell++, var);
    }
    
    return varArray;
  }//variableArray
  
  Variable* VariableFactory::variableArray(std::size_t aNumCells, VariableSemanticUPtr aVariableSemantic)
  {
    VariableArray* var = new VariableArray(aNumCells, std::move(aVariableSemantic));
    var->setType(new VariableType(VariableTypeClass::VAR_TYPE_ARRAY));
    
    return var;
  }//variableArray
  
  Variable* VariableFactory::variableMatrix2D(const std::vector<std::vector<VariableSPtr>>& aVarArray, VariableSemanticUPtr aVariableSemantic)
  {
    if(aVarArray.empty())
    {
      return variableMatrix2D(0, 0, std::move(aVariableSemantic));
    }
    auto colSize = aVarArray[0].size();
    auto rowSize = aVarArray.size();
    
    VariableMatrix2D* varMatrix2D = new VariableMatrix2D(rowSize, colSize, std::move(aVariableSemantic));
    varMatrix2D->setType(new VariableType(VariableTypeClass::VAR_TYPE_MATRIX2D));

    std::size_t rowIdx {0};
    for(auto& varArray : aVarArray)
    {
      assert(varArray.size() == colSize);
      std::size_t colIdx {0};
      for(auto& var : varArray)
      {
        varMatrix2D->assignVariableToCell(rowIdx, colIdx++, var);
      }
      rowIdx++;
    }
    
    return varMatrix2D;
  }//variableMatrix2D
  
  Variable* VariableFactory::variableMatrix2D(std::size_t aNumRows, std::size_t aNumCols, VariableSemanticUPtr aVariableSemantic)
  {
    VariableMatrix2D* var = new VariableMatrix2D(aNumRows, aNumCols, std::move(aVariableSemantic));
    var->setType(new VariableType(VariableTypeClass::VAR_TYPE_MATRIX2D));
    
    return var;
  }//variableMatrix2D
  
}// end namespace Core
