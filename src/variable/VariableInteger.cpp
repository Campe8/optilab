// Base class
#include "VariableInteger.hpp"

// Domain
#include "DomainInteger.hpp"

// Domain element
#include "DomainElementManager.hpp"
#include "DomainElementInt64.hpp"

// Domain events
#include "DomainEvent.hpp"

#include <utility>

namespace Core {
  
  VariableInteger::VariableInteger(DomainElementInt64 *aLowerBound,
                                   DomainElementInt64 *aUpperBound,
                                   VariableSemanticUPtr aVariableSemantic)
  : Variable(std::move(aVariableSemantic))
  {
    pDomain = std::make_shared<DomainInteger>(aLowerBound, aUpperBound);
    pDomainList.push_back(pDomain);
  }
  
  VariableInteger::VariableInteger(const std::unordered_set<DomainElementInt64 *>& aSetOfElements,
                                   VariableSemanticUPtr aVariableSemantic)
  : Variable(std::move(aVariableSemantic))
  {
    pDomain = std::make_shared<DomainInteger>(aSetOfElements);
    pDomainList.push_back(pDomain);
  }
  
  VariableInteger::VariableInteger(const VariableInteger& aOther)
  : Variable(aOther)
  {
    pDomain.reset(new DomainInteger(*(static_cast<DomainInteger*>(aOther.pDomain.get()))));
  }
  
  VariableInteger::VariableInteger(VariableInteger&& aOther)
  : Variable(aOther)
  {
    pDomain.reset(new DomainInteger(*(static_cast<DomainInteger*>(aOther.pDomain.get()))));
    aOther.pDomain = nullptr;
  }
  
  VariableInteger::~VariableInteger()
  {
  }
  
  VariableInteger& VariableInteger::operator= (const VariableInteger& aOther)
  {
    if (this != &aOther)
    {
      Variable::operator=(aOther);
      pDomain.reset(new DomainInteger(*(static_cast<DomainInteger*>(aOther.pDomain.get()))));
    }
    return *this;
  }
  
  VariableInteger& VariableInteger::operator= (VariableInteger&& aOther)
  {
    if (this != &aOther)
    {
      Variable::operator=(aOther);
      pDomain.reset(new DomainInteger(*(static_cast<DomainInteger*>(aOther.pDomain.get()))));
      
      aOther.pDomain = nullptr;
    }
    return *this;
  }
  
  bool VariableInteger::isAssigned() const
  {
    bool singletonEvent = DomainEventSingleton::isa(pDomain->getEvent());
    if (singletonEvent)
    {
      assert(pDomain->getSize() == 1);
      return true;
    }
    return false;
  }//isAssigned
  
}// end namespace Core
