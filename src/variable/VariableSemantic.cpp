// Base class
#include "VariableSemantic.hpp"

namespace Core {
  
  VariableSemantic::VariableSemantic(VariableSemanticClassType aVarSemanticClassType)
  : pBranchingStrategyType(Search::BranchingStrategyType::BRANCHING_BINARY_CHOICE_POINT)
  , pVariableSemanticClassType(aVarSemanticClassType)
  , pOutput(boost::logic::indeterminate)
  , pBranchingPolicy(false)
  , pReachability(false)
  , pInputOrderDef(false)
  , pInputOrder(0)
  {
  }
  
  bool VariableSemantic::isReachable() const
  {
    return pReachability || isBranching();
  }//isReachable
  
  void VariableSemantic::forceOutput(bool aOutput)
  {
    pOutput = aOutput;
  }//forceOutput
  
  void VariableSemantic::setBranchingPolicy(bool aBranchingSemanticPolicy)
  {
    pBranchingPolicy = getBranchingPolicy(aBranchingSemanticPolicy);
    if(aBranchingSemanticPolicy)
    {
      forceOutput(true);
    }
  }//setBranchingPolicy
  
  void VariableSemantic::setReachability(bool aReachability)
  {
    pReachability = aReachability;
    if(aReachability)
    {
      forceOutput(true);
    }
  }//setReachability
  
  void VariableSemantic::setInputOrder(std::size_t aOrderValue)
  {
    pInputOrder = aOrderValue;
    pInputOrderDef = true;
  }//setInputOrder
  
  boost::logic::tribool VariableSemantic::isOutput() const
  {
    return pOutput;
  }//isOutput
  
}// end namespace Core
