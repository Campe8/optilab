// Base class
#include "VariableSemanticSupport.hpp"

// UUID generators
#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>

namespace Core {
  
  VariableSemanticSupport::VariableSemanticSupport(const std::string& aVariableName)
  : Core::VariableSemantic(VariableSemanticClassType::VAR_SEMANTIC_CLASS_SUPPORT)
  {
    setVariableNameIdentifier(aVariableName);
  }
  
  void VariableSemanticSupport::setVariableNameIdentifier(const std::string& aVariableName)
  {
    if(aVariableName.empty())
    {
      boost::uuids::random_generator gen;
      pVariableStringNameIdentifier = "_" + boost::lexical_cast<std::string>(gen());
    }
    else
    {
      // Check if it begins with '_', if not, add it as a prefix
      if(aVariableName[0] != '_')
      {
        pVariableStringNameIdentifier = "_";
      }
      
      pVariableStringNameIdentifier += aVariableName;
    }
  }//setVariableNameIdentifier
  
  VariableSemantic* VariableSemanticSupport::clone()
  {
    return new VariableSemanticSupport(*this);
  }//clone
  
}// end namespace Core
