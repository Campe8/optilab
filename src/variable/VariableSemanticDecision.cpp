// Base class
#include "VariableSemanticDecision.hpp"

// UUID generators
#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>

#include <cassert>

namespace Core {
  
  VariableSemanticDecision::VariableSemanticDecision(const std::string& aVariableName)
  : Core::VariableSemantic(VariableSemanticClassType::VAR_SEMANTIC_CLASS_DECISION)
  , pVariableStringNameIdentifier(aVariableName)
  {
    setAsSolutionElement(true);
    setBranchingPolicy(false);
    
    if (aVariableName.empty())
    {
      boost::uuids::random_generator gen;
      pVariableStringNameIdentifier = "D" + boost::lexical_cast<std::string>(gen());
    }
  }
  
  bool VariableSemanticDecision::isa(const VariableSemantic* aSemantic)
  {
    return aSemantic &&
    aSemantic->getSemanticClassType() == VariableSemanticClassType::VAR_SEMANTIC_CLASS_DECISION;
  }//isa
  
  VariableSemanticDecision* VariableSemanticDecision::cast(const VariableSemantic* aSemantic)
  {
    if(!isa(aSemantic)) return nullptr;
    return static_cast<VariableSemanticDecision*>(const_cast<VariableSemantic*>(aSemantic));
  }//cast
  
  VariableSemantic* VariableSemanticDecision::clone()
  {
    return new VariableSemanticDecision(*this);
  }//clone
  
}// end namespace Core
