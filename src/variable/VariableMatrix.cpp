// Base class
#include "VariableMatrix.hpp"

namespace Core {
  
  VariableMatrix::VariableMatrix(VariableSemanticUPtr aVariableSemantic)
  : Variable(std::move(aVariableSemantic))
  {
  }
  
  void VariableMatrix::uploadSemantic(const VariableSemanticSPtr& aSemantic)
  {
    // Upload the semantic on the base class
    Variable::uploadSemantic(aSemantic);
    
    // Upload the semantic on each component of the matrix
    uploadSemanticToDimensions(aSemantic);
  }//uploadSemantic
  
}// end namespace Core
