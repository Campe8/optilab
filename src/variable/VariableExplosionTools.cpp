#include "VariableExplosionTools.hpp"

// Non primitive variable types
#include "VariableArray.hpp"
#include "VariableMatrix2D.hpp"

#include <cassert>

namespace Core { namespace Tools {
  
  std::vector<Core::VariableSPtr> explodeVariable(const VariableSPtr& aVar)
  {
    std::vector<Core::VariableSPtr> explodedVarSet;
    if (aVar->hasPrimitiveType())
    {
      explodedVarSet.push_back(aVar);
      return explodedVarSet;
    }

    auto type = aVar->getType()->getTypeClass();
    switch (type)
    {
      case Core::VAR_TYPE_ARRAY:
      {
        assert(Core::VariableArray::isa(aVar.get()));
        Core::VariableArray* varArray = Core::VariableArray::cast(aVar.get());
        for (std::size_t idx = 0; idx < varArray->size(); ++idx)
        {
          assert(varArray->at(idx));
          auto subSet = explodeVariable(varArray->at(idx));
          for (auto& var : subSet)
          {
            assert(var);
            explodedVarSet.push_back(var);
          }
        }
      }
        break;
      default:
        assert(type == Core::VariableTypeClass::VAR_TYPE_MATRIX2D);
      {
        assert(Core::VariableMatrix2D::isa(aVar.get()));
        Core::VariableMatrix2D* varMatrix = Core::VariableMatrix2D::cast(aVar.get());
        auto numRows = varMatrix->numRows();
        auto numCols = varMatrix->numCols();
        for (std::size_t rowIdx = 0; rowIdx < numRows; ++rowIdx)
        {
          for (std::size_t colIdx = 0; colIdx < numCols; ++colIdx)
          {
            assert(varMatrix->at(rowIdx, colIdx));
            auto subSet = explodeVariable(varMatrix->at(rowIdx, colIdx));
            for (auto& var : subSet)
            {
              assert(var);
              explodedVarSet.push_back(var);
            }
          }
        }
      }
        break;
    }
    
    return explodedVarSet;
  }//explodeVariable
  
  std::vector<Core::VariableSPtr> explodeVariables(const std::vector<Core::VariableSPtr>& aVarSet)
  {
    std::vector<Core::VariableSPtr> explodedVarSet;
    for (auto& var : aVarSet)
    {
      assert(var);
      for (auto& varSub : explodeVariable(var))
      {
        assert(varSub);
        explodedVarSet.push_back(varSub);
      }
    }
    return explodedVarSet;
  }//explodeVariables
  
}}// end namespace Tools end namespace Core
