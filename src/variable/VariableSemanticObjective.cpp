// Base class
#include "VariableSemanticObjective.hpp"

// UUID generators
#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>

#include <cassert>

namespace Core {
  
  VariableSemanticObjective::VariableSemanticObjective(ObjectiveExtremum aObjExtremum, const std::string& aVariableName)
  : Core::VariableSemantic(VariableSemanticClassType::VAR_SEMANTIC_CLASS_OBJECTIVE)
  , pVariableStringNameIdentifier(aVariableName)
  , pObjExtremum(aObjExtremum)
  {
    assert(pObjExtremum != ObjectiveExtremum::OBJ_EXT_UNDEF);
    
    setAsSolutionElement(true);
    
    // Default branching policy is false for objective variables
    setBranchingPolicy(false);
    
    if (aVariableName.empty())
    {
      boost::uuids::random_generator gen;
      pVariableStringNameIdentifier = "O" + boost::lexical_cast<std::string>(gen());
    }
  }

  VariableSemantic* VariableSemanticObjective::clone()
  {
    return new VariableSemanticObjective(*this);
  }//clone
  
}// end namespace Core
