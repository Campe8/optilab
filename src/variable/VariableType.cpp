// Base class
#include "VariableType.hpp"

#include <cassert>

namespace Core {
  
  VariableType::VariableType(VariableTypeClass aVarTypeClass)
  : pVariableTypeClass(aVarTypeClass)
  {
  }
  
  VariableType::~VariableType()
  {
  }
  
  bool VariableType::isPrimitiveType() const
  {
    switch (getTypeClass())
    {
      case VariableTypeClass::VAR_TYPE_BOOLEAN:
      case VariableTypeClass::VAR_TYPE_INTEGER:
      case VariableTypeClass::VAR_TYPE_REAL:
        return true;
      case VariableTypeClass::VAR_TYPE_MATRIX:
        return false;
      default:
        assert(getTypeClass() == VariableTypeClass::VAR_TYPE_ARRAY);
        return false;
    }
  }//hasPrimitiveType

}// end namespace Core
