// Base class
#include "DomainBoolean.hpp"

// Container for integers
#include "DomainElementPairContainer.hpp"

#include "DomainElementInt64.hpp"
#include "DomainElementManager.hpp"

#include <cassert>

#include "DomainEventInc.hpp"

namespace Core {
  
  DomainBoolean::DomainBoolean()
  : DomainInteger(DomainElementManager::getInstance().createDomainElementInt64(0),
                  DomainElementManager::getInstance().createDomainElementInt64(1))
  {
    castToClass(DomainClass::DOM_BOOL);
    
    // Instantiate new container
    pElementContainer.reset(new DomainElementPairContainer(DomainElementManager::getInstance().createDomainElementInt64(0),
                                                           DomainElementManager::getInstance().createDomainElementInt64(1)));
    
    // Instantiate new decorator
    pDecorator.reset(new DomainDecorator(pElementContainer.get()));
  }
  
  DomainBoolean::DomainBoolean(const DomainBoolean& aOther)
  : DomainInteger(DomainElementManager::getInstance().createDomainElementInt64(0),
                  DomainElementManager::getInstance().createDomainElementInt64(1))
  {
    castToClass(DomainClass::DOM_BOOL);
    
    // Instantiate new container
    auto *container = aOther.pElementContainer.get();
    assert(container);
    
    pElementContainer.reset(new DomainElementPairContainer(*(static_cast<DomainElementPairContainer*>(aOther.pElementContainer.get()))));
    pDecorator.reset(new DomainDecorator(*(aOther.pDecorator.get())));
  }
  
  DomainBoolean::DomainBoolean(DomainBoolean&& aOther)
  : DomainInteger(DomainElementManager::getInstance().createDomainElementInt64(0),
                  DomainElementManager::getInstance().createDomainElementInt64(1))
  {
    castToClass(DomainClass::DOM_BOOL);
    
    // Instantiate new container
    auto *container = aOther.pElementContainer.get();
    assert(container);
    
    pElementContainer.reset(new DomainElementPairContainer(*(static_cast<DomainElementPairContainer*>(aOther.pElementContainer.get()))));
    pDecorator.reset(new DomainDecorator(*(aOther.pDecorator.get())));
    
    aOther.pElementContainer.reset(nullptr);
    aOther.pDecorator.reset(nullptr);
  }
  
  DomainBoolean::~DomainBoolean()
  {
  }
  
  DomainBoolean& DomainBoolean::operator= (const DomainBoolean& aOther)
  {
    if (this != &aOther)
    {
      DomainInteger::operator=(aOther);
      
      auto *container = aOther.pElementContainer.get();
      assert(container);
      
      pElementContainer.reset(new DomainElementPairContainer(*(static_cast<DomainElementPairContainer*>(aOther.pElementContainer.get()))));
      pDecorator.reset(new DomainDecorator(*(aOther.pDecorator.get())));
    }
    
    return *this;
  }
  
  DomainBoolean& DomainBoolean::operator= (DomainBoolean&& aOther)
  {
    if (this != &aOther)
    {
      DomainInteger::operator=(aOther);
      
      auto *container = aOther.pElementContainer.get();
      assert(container);
      
      pElementContainer.reset(new DomainElementPairContainer(*(static_cast<DomainElementPairContainer*>(aOther.pElementContainer.get()))));
      pDecorator.reset(new DomainDecorator(*(aOther.pDecorator.get())));
      
      aOther.pElementContainer.reset(nullptr);
      aOther.pDecorator.reset(nullptr);
    }
    
    return *this;
  }
  
  void DomainBoolean::setTrue()
  {
    pElementContainer->subtract(DomainElementManager::getInstance().createDomainElementInt64(0));
  }//setTrue
  
  void DomainBoolean::setFalse()
  {
    pElementContainer->subtract(DomainElementManager::getInstance().createDomainElementInt64(1));
  }//setTrue
  
  bool DomainBoolean::isTrue() const
  {
    if(getSize() == 1)
    {
      if(pDecorator->getLowerBound()->isEqual(DomainElementManager::getInstance().createDomainElementInt64(1)))
      {
        return true;
      }
    }
    return false;
  }//isTrue
  
  bool DomainBoolean::isFalse() const
  {
    if(getSize() == 1)
    {
      if(pDecorator->getLowerBound()->isEqual(DomainElementManager::getInstance().createDomainElementInt64(0)))
      {
        return true;
      }
    }
    return false;
  }//isTrue
  
}// end namespace Core
