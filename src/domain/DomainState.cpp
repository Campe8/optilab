// Base class
#include "DomainState.hpp"

#include <cassert>

namespace Core {
  
  DomainState::DomainState(DomainStateType aDomStateType)
  : pDomStateType(aDomStateType)
  {
  }
  
  DomainState::~DomainState()
  {
  }
  
}// end namespace Core
