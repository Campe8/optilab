// Base class
#include "DomainEventChange.hpp"

#include <cassert>

namespace Core {
  
  DomainEventChange::DomainEventChange()
  : DomainEvent(DomainEventType::CHANGE_EVENT)
  {
  }
  
  DomainEventChange::~DomainEventChange()
  {
  }
  
  bool DomainEventChange::isa(DomainEvent *aEvent)
  {
    assert(aEvent);
    return aEvent->getEventType() == DomainEventType::CHANGE_EVENT;
  }//isa
  
  DomainEventChange* DomainEventChange::cast(DomainEvent *aEvent)
  {
    assert(aEvent);
    if (isa(aEvent))
    {
      return static_cast<DomainEventChange*>(aEvent);
    }
    
    return nullptr;
  }//cast
  
  bool DomainEventChange::overlaps(const DomainEvent *aDomainEvent) const
  {
    assert(aDomainEvent);
    
    auto event = aDomainEvent->getEventType();
    switch (event) {
      case DomainEventType::BOUND_EVENT:
        return false; // not enough info
      case DomainEventType::CHANGE_EVENT:
        return true;
      case DomainEventType::LWB_EVENT:
        return false; // not enough info
      case DomainEventType::UPB_EVENT:
        return false; // not enough info
      case DomainEventType::SINGLETON_EVENT:
        return false; // not enough info
      case DomainEventType::FAIL_EVENT:
        return false;
      case DomainEventType::VOID_EVENT:
        return false;
      default:
        assert(event == DomainEventType::UNDEF_EVENT);
        return false;
    }
  }//overlaps
  
}// end namespace Core
