// Base class
#include "DomainElementManager.hpp"

// DomainElement
#include "DomainElementInt64.hpp"
#include "DomainElementReal.hpp"
#include "DomainElementVoid.hpp"

namespace Core {
  
  DomainElementManager::DomainElementManager()
  : pDomainElementMap(new DomainElementCategoryMap())
  , pDomainElementInt64Map(new DomainElementInt64CategoryMap())
  , pDomainElementRealMap(new DomainElementRealCategoryMap())
  {
    voidElement = new DomainElementVoid();
  }
  
  DomainElementManager::~DomainElementManager()
  {
    clear();
    delete voidElement;
  }
  
  DomainElementManager& DomainElementManager::getInstance()
  {
    static DomainElementManager managerInstance;
    return managerInstance;
  }
  
  void DomainElementManager::clear()
  {
    for(auto& dElem : *pDomainElementMap)
    {
      delete dElem.second;
      dElem.second = nullptr;
    }
    
    pDomainElementMap->clear();
    pDomainElementInt64Map->clear();
  }//clear
  
  DomainElement* DomainElementManager::clone(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    if(DomainElementVoid::isa(aDomainElement))
    {
      return getVoidElement();
    }
    
    auto domElemFind = find(aDomainElement->getDomainKey());
    if (domElemFind)
    {
      return domElemFind;
    }
    
    DomainElement *domainElementToReturn{nullptr};
    auto domainElementType = aDomainElement->getType();
    switch (domainElementType)
    {
      case DomainElementType::DET_REAL:
      {
        const DomainElementReal* castElementReal = DomainElementReal::cast(aDomainElement);
        domainElementToReturn = createDomainElementReal(castElementReal->getValue());
        break;
      }
      default:
        assert(domainElementType == DomainElementType::DET_INT_64);
        domainElementToReturn = createDomainElementInt64(DomainElementInt64::cast(aDomainElement)->getValue());
    }
    
    assert(domainElementToReturn);
    return domainElementToReturn;
  }//clone
  
  DomainElement* DomainElementManager::find(DomainElementKey *aDomainElementKey)
  {
    assert(aDomainElementKey);
    
    if(pDomainElementMap->find(*aDomainElementKey) != pDomainElementMap->end())
    {
      return pDomainElementMap->at(*aDomainElementKey);
    }
    return nullptr;
  }//find
  
  DomainElementInt64* DomainElementManager::findDomainElementInt64(INT_64 aVal)
  {
    if(pDomainElementInt64Map->find(aVal) != pDomainElementInt64Map->end())
    {
      return pDomainElementInt64Map->at(aVal);
    }
    return nullptr;
  }// findDomainElementInt64
  
  DomainElementInt64* DomainElementManager::createDomainElementInt64(INT_64 aVal)
  {
    auto dElement = findDomainElementInt64(aVal);
    if (dElement)
    {
      // Delete newly created DomainElement and return the existing one
      return DomainElementInt64::cast(dElement);
    }

    // If not present, create new element and register it into the map
    DomainElement *newElement = new DomainElementInt64(aVal);
    pDomainElementMap->operator[](*(newElement->getDomainKey())) = newElement;
    pDomainElementInt64Map->operator[](aVal) = DomainElementInt64::cast(newElement);
    return DomainElementInt64::cast(newElement);
  }//createDomainElementInt64
  
  DomainElementInt64 DomainElementManager::createLocalDomainElementInt64(INT_64 aVal)
  {
    return DomainElementInt64(aVal);
  }//createLocalDomainElementInt64
  
  DomainElementReal* DomainElementManager::createDomainElementReal(REAL aVal)
  {
    auto dElement = findDomainElementReal(aVal);
    if (dElement)
    {
      // Delete newly created DomainElement and return the existing one
      return DomainElementReal::cast(dElement);
    }
    
    // If not present, create new element and register it into the map
    DomainElement *newElement = new DomainElementReal(aVal);
    pDomainElementMap->operator[](*(newElement->getDomainKey())) = newElement;
    pDomainElementRealMap->operator[](aVal) = DomainElementReal::cast(newElement);
    return DomainElementReal::cast(newElement);
  }//createDomainElementReal
  
  DomainElementReal DomainElementManager::createLocalDomainElementReal(REAL aVal)
  {
    return DomainElementReal(aVal);
  }//createLocalDomainElementReal
  
  DomainElementReal* DomainElementManager::findDomainElementReal(REAL aVal)
  {
    if(pDomainElementRealMap->find(aVal) != pDomainElementRealMap->end())
    {
      return pDomainElementRealMap->at(aVal);
    }
    return nullptr;
  }// findDomainElementInt64
  
  DomainElement *DomainElementManager::getVoidElement()
  {
    return voidElement;
  }//getVoidElement
  
  std::size_t DomainElementManager::size() const
  {
    return pDomainElementMap->size();
  }// size
  
}// end namespace Core
