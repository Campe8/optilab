// Base class
#include "DomainConcreteStorage.hpp"

#include <cassert>

namespace Core {

  DomainConcreteStorage::DomainConcreteStorage(DomainConcreteStorageType aStorageType,
                                               DomainElementManager *aDomainElementManager)
  : pStorageType(aStorageType)
  , pDomainElementManager(aDomainElementManager)
  {
  }
  
  DomainConcreteStorage::~DomainConcreteStorage()
  {
  }
  
  DomainElementManager& DomainConcreteStorage::getDomainElementManager()
  {
    assert(pDomainElementManager);
    return *pDomainElementManager;
  }//getDomainElementManager
  
}// end namespace Core
