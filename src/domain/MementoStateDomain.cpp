// Base class
#include "MementoStateDomain.hpp"

#include <cassert>

namespace Search {
  
  MementoStateDomain::MementoStateDomain(std::unique_ptr<Core::DomainState> aDomainState)
  : MementoState(MementoStateId::MS_DOMAIN)
  , pDomainState(std::move(aDomainState))
  {
  }
  
  MementoStateDomain::~MementoStateDomain()
  {
  }
  
}// end namespace Search
