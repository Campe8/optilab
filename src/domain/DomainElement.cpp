// Base class
#include "DomainElement.hpp"

// Element Key
#include "DomainElementKey.hpp"

// Void domain element
#include "DomainElementVoid.hpp"

// Domain element manager
#include "DomainElementManager.hpp"

namespace Core {
  
  DomainElement::DomainElement(DomainElementType aType)
  : pDomainElementType(aType)
  , pRoundingMode(RoundingMode::DE_ROUNDING_UNSPEC)
  {
  }
  
  DomainElement::DomainElement(const DomainElement& aOther)
  {
    pDomainElementType = aOther.pDomainElementType;
  }
  
  DomainElement::DomainElement(DomainElement&& aOther)
  {
    pDomainElementType = aOther.pDomainElementType;
    aOther.pDomainElementType = DomainElementType::DET_UNDEF;
  }
  
  DomainElement::~DomainElement()
  {
  }
  
  DomainElement& DomainElement::operator=(const DomainElement& aOther)
  {
    if (&aOther == this)
    {
      return *this;
    }
    
    pDomainElementType = aOther.pDomainElementType;
    
    return *this;
  }
  
  DomainElement& DomainElement::operator=(DomainElement&& aOther)
  {
    if (&aOther == this)
    {
      return *this;
    }
    
    pDomainElementType = aOther.pDomainElementType;
    aOther.pDomainElementType = DomainElementType::DET_UNDEF;
    
    return *this;
  }
  
  DomainElementManager& DomainElement::getDomainElementManager() const
  {
    return DomainElementManager::getInstance();
  }//getDomainElemenentManager
  
  std::ostream& operator<<(std::ostream &out, DomainElement &aDomainElement)
  {
    out << aDomainElement.toString();
    return out;
  }
  
}// end namespace Core
