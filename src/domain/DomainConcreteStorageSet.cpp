// Base class
#include "DomainConcreteStorageSet.hpp"

#include "DomainElementVoid.hpp"
#include "DomainElementInt64.hpp"

#include <algorithm>
#include <cassert>
#include <stdlib.h>

namespace Core {
  
  DomainConcreteStorageSet::DomainConcreteStorageSet(const std::unordered_set<DomainElement *>& aSetOfElements,
                                                     DomainElementManager *aDomainElementManager)
  : DomainConcreteStorage(DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_SET, aDomainElementManager)
  , pBitsetOffset(0)
  , pLowerBound(0)
  , pUpperBound(0)
  {
    // Allocate bitset to represent the given set of elements
    pBitset = boost::dynamic_bitset<>(aSetOfElements.size());
    // Flip bits all to 1
    pBitset.flip();
    
    if(aSetOfElements.empty())
    {
      return;
    }

    auto lowerBound = *(aSetOfElements.begin());
    auto upperBound = lowerBound;
    
    // Map bitset positions to elements
    std::size_t mapIdx{ 0 };
    pLowerBound = mapIdx;
    pUpperBound = mapIdx;
    for (DomainElement *dElem : aSetOfElements)
    {
      // Update bounds on init
      if(DomainElement::lessThan(dElem, lowerBound))
      {
        pLowerBound = mapIdx;
        lowerBound = dElem;
      }
      
      if(DomainElement::lessThan(upperBound, dElem))
      {
        pUpperBound = mapIdx;
        upperBound = dElem;
      }
      
      pBitsetToDomainElementMap[mapIdx] = dElem;
      pDomainElementKeyToBitmapMap[*dElem->getDomainKey()] = mapIdx;
      mapIdx++;
    }
    
    // Update bounds
    updateBounds();
  }
  
  DomainConcreteStorageSet::~DomainConcreteStorageSet()
  {
  }
  
  void DomainConcreteStorageSet::updateBounds()
  {
    if(getSize() == 0)
    {
      return;
    }

    auto lowerBound = pBitsetToDomainElementMap[pLowerBound];
    auto upperBound = pBitsetToDomainElementMap[pUpperBound];
    
    bool lowerBoundSet{false};
    bool upperBoundSet{false};
    if(!pBitset.test(pDomainElementKeyToBitmapMap[*upperBound->getDomainKey()]))
    {
      upperBoundSet = true;
      upperBound = lowerBound;
    }
    
    if(!pBitset.test(pDomainElementKeyToBitmapMap[*lowerBound->getDomainKey()]))
    {
      lowerBoundSet = true;
      lowerBound = pBitsetToDomainElementMap[pUpperBound];
    }
    
    if(upperBoundSet)
    {
      pUpperBound = pLowerBound;
    }
    
    if(lowerBoundSet)
    {
      pLowerBound = pUpperBound;
    }
    
    for (std::size_t idx = 0; idx < pBitset.size(); ++idx)
    {
      if (pBitset.test(idx))
      {
        auto currentDomainElement = pBitsetToDomainElementMap[idx];
        if(DomainElement::lessThan(currentDomainElement, lowerBound))
        {
          pLowerBound = idx;
          lowerBound = currentDomainElement;
        }
        
        if(DomainElement::lessThan(upperBound, currentDomainElement))
        {
          pUpperBound = idx;
          upperBound = currentDomainElement;
        }
      }
    }
  }//updateBounds
  
  DomainElement *DomainConcreteStorageSet::getLowerBound() const
  {
    assert(getSize());
    assert(pBitsetToDomainElementMap.find(pLowerBound) != pBitsetToDomainElementMap.end());
    
    return pBitsetToDomainElementMap.at(pLowerBound);
  }//getLowerBound
  
  DomainElement *DomainConcreteStorageSet::getUpperBound() const
  {
    assert(getSize());
    assert(pBitsetToDomainElementMap.find(pUpperBound) != pBitsetToDomainElementMap.end());
    
    return pBitsetToDomainElementMap.at(pUpperBound);
  }//getUpperBound
  
  DomainElement *DomainConcreteStorageSet::getNextElement(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    assert(!DomainElementVoid::isa(aDomainElement));
    
    auto upperBoundElement = getUpperBound();
    if(upperBoundElement->isEqual(aDomainElement))
    {
      return upperBoundElement;
    }
    
    DomainElement *elementToReturn = nullptr;
    DomainElement *currentUpperBound = getUpperBound();
    for (std::size_t idx = 0; idx < pBitset.size(); ++idx)
    {
      if (pBitset.test(idx))
      {
        // Choose the lowest of the greatest
        auto currentDomainElement = pBitsetToDomainElementMap[idx];
        if (aDomainElement->isLessThan(currentDomainElement) && currentDomainElement->isLessThanOrEqual(currentUpperBound))
        {
          elementToReturn = currentDomainElement;
          currentUpperBound = elementToReturn;
        }
      }
    }

    assert(elementToReturn);
    return elementToReturn;
  }//getNextElement
  
  DomainElement *DomainConcreteStorageSet::getPrevElement(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    assert(!DomainElementVoid::isa(aDomainElement));
    
    auto lowerBoundElement = getLowerBound();
    if(lowerBoundElement->isEqual(aDomainElement))
    {
      return lowerBoundElement;
    }
    
    DomainElement *elementToReturn = nullptr;
    DomainElement *currentLowerBound = getLowerBound();
    for (std::size_t idx = 0; idx < pBitset.size(); ++idx)
    {
      if (pBitset.test(idx))
      {
        // Choose the greatest of the lowest
        auto currentDomainElement = pBitsetToDomainElementMap[idx];
        if (currentDomainElement->isLessThan(aDomainElement) && currentLowerBound->isLessThanOrEqual(currentDomainElement))
        {
          elementToReturn = currentDomainElement;
          currentLowerBound = elementToReturn;
        }
      }
    }
    
    assert(elementToReturn);
    return elementToReturn;
  }//getPrevElement
  
  DomainElement *DomainConcreteStorageSet::getElementByIndex(std::size_t aElementIndex)
  {
    assert(aElementIndex < getSize());
    DomainElement *elementToReturn = nullptr;
    
    for(std::size_t idx = 0; idx <= pBitset.size(); ++idx)
    {
      if(aElementIndex == 0 && pBitset.test(idx))
      {
        elementToReturn = pBitsetToDomainElementMap[idx];
        break;
      }
      
      if (pBitset.test(idx))
      {
        aElementIndex--;
      }
    }
    
    assert(elementToReturn);
    return elementToReturn;
  }//getElementByIndex
  
  void DomainConcreteStorageSet::shrink(DomainElement *aLowerBound, DomainElement *aUpperBound)
  {
    assert(aLowerBound);
    assert(aUpperBound);

    auto currentLowerBound = getLowerBound();
    auto currentUpperBound = getUpperBound();
    
    // If aLowerBound < lowerBound and aUpperBound > upperBound, return
    if (aLowerBound->isLessThanOrEqual(currentLowerBound) && currentUpperBound->isLessThanOrEqual(aUpperBound))
    {
      return;
    }
    
    // If aLowerBound > aUpperBound, empty domain
    if(aUpperBound->isLessThan(aLowerBound))
    {
      emptyDomain();
      return;
    }
    
    // If aUpperBound < lowerBound, empty domain
    if(aUpperBound->isLessThan(currentLowerBound))
    {
      emptyDomain();
      return;
    }
    
    // If aLowerBound > upperBound, empty domain
    if (aUpperBound->isLessThan(aLowerBound))
    {
      emptyDomain();
      return;
    }
    
    for (std::size_t idx = 0; idx < pBitset.size(); ++idx)
    {
      if (pBitset.test(idx))
      {
        auto currentDomainElement = pBitsetToDomainElementMap[idx];
        if(currentDomainElement->isLessThan(aLowerBound) || aUpperBound->isLessThan(currentDomainElement))
        {
          pBitset.reset(idx);
        }
      }
    }
    
    // Update bounds after shrink
    updateBounds();
  }//shrink
  
  void DomainConcreteStorageSet::subtract(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    if (!contains(aDomainElement))
    {
      return;
    }
    
    auto bitmapIdx = pDomainElementKeyToBitmapMap[*aDomainElement->getDomainKey()];
    if (pBitset.test(bitmapIdx))
    {
      pBitset.reset(bitmapIdx);
    }
    
    // Update bounds after subtract
    if(bitmapIdx == pLowerBound || bitmapIdx == pUpperBound)
    {
      updateBounds();
    }
  }//subtract
  
  void DomainConcreteStorageSet::inMin(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    
    if(aDomainElement->isLessThanOrEqual(getLowerBound()))
    {
      return;
    }
    
    if(getUpperBound()->isLessThan(aDomainElement))
    {
      emptyDomain();
      return;
    }
    
    for (std::size_t idx = 0; idx < pBitset.size(); ++idx)
    {
      if (pBitset.test(idx))
      {
        auto currentDomainElement = pBitsetToDomainElementMap[idx];
        if (currentDomainElement->isLessThan(aDomainElement))
        {
          pBitset.reset(idx);
        }
      }
    }
    
    updateBounds();
  }//inMin
  
  void DomainConcreteStorageSet::inMax(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    
    if (getUpperBound()->isLessThanOrEqual(aDomainElement))
    {
      return;
    }
    
    if(aDomainElement->isLessThan(getLowerBound()))
    {
      emptyDomain();
      return;
    }
    
    for (std::size_t idx = 0; idx < pBitset.size(); ++idx)
    {
      if (pBitset.test(idx))
      {
        auto currentDomainElement = pBitsetToDomainElementMap[idx];
        if (aDomainElement->isLessThan(currentDomainElement))
        {
          pBitset.reset(idx);
        }
      }
    }

    updateBounds();
  }//inMax
  
  bool DomainConcreteStorageSet::contains(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    
    if(pDomainElementKeyToBitmapMap.find(*aDomainElement->getDomainKey()) == pDomainElementKeyToBitmapMap.end())
    {
      return false;
    }
    
    return pBitset.test(pDomainElementKeyToBitmapMap[*aDomainElement->getDomainKey()]);
  }//contains
  
  void DomainConcreteStorageSet::emptyDomain()
  {
    pLowerBound = 0;
    pUpperBound = pBitset.size() - 1;
    
    pBitset.reset();
  }//emptyDomain
  
}// end namespace Core
