// Base class
#include "DomainEvent.hpp"

#include <cassert>

namespace Core {
  
  DomainEvent::DomainEvent(DomainEventType aEvent)
  : pEventType(aEvent)
  {
  }
  
  DomainEvent::~DomainEvent()
  {
  }
  
}// end namespace Core
