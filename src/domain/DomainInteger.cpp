// Base class
#include "DomainInteger.hpp"

// Container for integers
#include "DomainElementInt64Container.hpp"

#include <cassert>

#include "DomainEventInc.hpp"

namespace Core {
  
  DomainInteger::DomainInteger(DomainElementInt64 *aLowerBound, DomainElementInt64 *aUpperBound)
  : Domain(DomainClass::DOM_INT64)
  {
    assert(aLowerBound);
    assert(aUpperBound);
    
    // Instantiate new container
    pElementContainer.reset(new DomainElementInt64Container(aLowerBound, aUpperBound));
    
    // Instantiate new decorator
    pDecorator.reset(new DomainDecorator(pElementContainer.get()));
  }
  
  DomainInteger::DomainInteger(const std::unordered_set<DomainElementInt64 *>& aSetOfElements)
  : Domain(DomainClass::DOM_INT64)
  {
    // Instantiate new container
    pElementContainer.reset(new DomainElementInt64Container(aSetOfElements));
    
    // Instantiate new decorator
    pDecorator.reset(new DomainDecorator(pElementContainer.get()));
  }
  
  DomainInteger::DomainInteger(const DomainInteger& aOther)
  : Domain(DomainClass::DOM_INT64)
  {
    // Instantiate new container
    auto *container = aOther.pElementContainer.get();
    assert(container);
    
    pElementContainer.reset(new DomainElementInt64Container(*(static_cast<DomainElementInt64Container*>(aOther.pElementContainer.get()))));
    pDecorator.reset(new DomainDecorator(*(aOther.pDecorator.get())));
  }
  
  DomainInteger::DomainInteger(DomainInteger&& aOther)
  : Domain(DomainClass::DOM_INT64)
  {
    // Instantiate new container
    auto *container = aOther.pElementContainer.get();
    assert(container);
    
    pElementContainer.reset(new DomainElementInt64Container(*(static_cast<DomainElementInt64Container*>(aOther.pElementContainer.get()))));
    pDecorator.reset(new DomainDecorator(*(aOther.pDecorator.get())));
    
    aOther.pElementContainer.reset(nullptr);
    aOther.pDecorator.reset(nullptr);
  }
  
  DomainInteger::~DomainInteger()
  {
  }
  
  DomainInteger& DomainInteger::operator= (const DomainInteger& aOther)
  {
    if (this != &aOther)
    {
      Domain::operator=(aOther);
      
      auto *container = aOther.pElementContainer.get();
      assert(container);
      
      pElementContainer.reset(new DomainElementInt64Container(*(static_cast<DomainElementInt64Container*>(aOther.pElementContainer.get()))));
      pDecorator.reset(new DomainDecorator(*(aOther.pDecorator.get())));
    }
    
    return *this;
  }
  
  DomainInteger& DomainInteger::operator= (DomainInteger&& aOther)
  {
    if (this != &aOther)
    {
      Domain::operator=(aOther);
      
      auto *container = aOther.pElementContainer.get();
      assert(container);
      
      pElementContainer.reset(new DomainElementInt64Container(*(static_cast<DomainElementInt64Container*>(aOther.pElementContainer.get()))));
      pDecorator.reset(new DomainDecorator(*(aOther.pDecorator.get())));
      
      aOther.pElementContainer.reset(nullptr);
      aOther.pDecorator.reset(nullptr);
    }
    
    return *this;
  }
}// end namespace Core
