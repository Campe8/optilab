// Base class
#include "DomainEventSingleton.hpp"

#include <cassert>

namespace Core {
  
  DomainEventSingleton::DomainEventSingleton()
  : DomainEvent(DomainEventType::SINGLETON_EVENT)
  {
  }
  
  DomainEventSingleton::~DomainEventSingleton()
  {
  }
  
  bool DomainEventSingleton::isa(DomainEvent *aEvent)
  {
    assert(aEvent);
    return aEvent->getEventType() == DomainEventType::SINGLETON_EVENT;
  }//isa
  
  DomainEventSingleton* DomainEventSingleton::cast(DomainEvent *aEvent)
  {
    assert(aEvent);
    if (isa(aEvent))
    {
      return static_cast<DomainEventSingleton*>(aEvent);
    }
    
    return nullptr;
  }//cast
  
  bool DomainEventSingleton::overlaps(const DomainEvent *aDomainEvent) const
  {
    assert(aDomainEvent);
    
    auto event = aDomainEvent->getEventType();
    switch (event) {
      case DomainEventType::BOUND_EVENT:
        return true;
      case DomainEventType::CHANGE_EVENT:
        return true;
      case DomainEventType::LWB_EVENT:
        return false;
      case DomainEventType::UPB_EVENT:
        return false;
      case DomainEventType::SINGLETON_EVENT:
        return true;
      case DomainEventType::FAIL_EVENT:
        return false;
      case DomainEventType::VOID_EVENT:
        return false;
      default:
        assert(event == DomainEventType::UNDEF_EVENT);
        return false;
    }
  }//overlaps
  
}// end namespace Core
