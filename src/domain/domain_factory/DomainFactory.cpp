// Base class
#include "DomainFactory.hpp"
#include "DomainElementManager.hpp"
#include "DomainElementInt64.hpp"
#include "DomainElementVoid.hpp"

#include <cassert>

namespace Core {

  DomainFactory::DomainFactory()
  {
  }

  DomainFactory::~DomainFactory()
  {
  }
  
  bool DomainFactory::isValidTypeCoercion(DomainClass aDomainClass, DomainElementType aDomainElementType)
  {
    assert(aDomainClass != DomainClass::DOM_UNDEF);
    
    if(aDomainElementType == DomainElementType::DET_VOID)
    {
      return false;
    }
    
    switch (aDomainClass)
    {
      case DomainClass::DOM_INT64:
      case DomainClass::DOM_BOOL:
        return aDomainElementType == DomainElementType::DET_INT_64 ? true : false;
      default:
        assert(aDomainClass == DomainClass::DOM_REAL);
        return aDomainElementType == DomainElementType::DET_REAL ? true : false;
    }
  }//isValidTypeCoercion
  
  DomainSPtr DomainFactory::getSharedFromRawDomainPointer(DomainPtr aDomainPtr)
  {
    if(!aDomainPtr)
    {
      return std::shared_ptr<DomainInteger>();
    }
    
    DomainClass domainClass = aDomainPtr->getDomainType();
    switch (domainClass) {
      case DomainClass::DOM_INT64:
        return std::shared_ptr<DomainInteger>(DomainInteger::cast(aDomainPtr));
      default:
        assert(domainClass == DomainClass::DOM_BOOL);
        return std::shared_ptr<DomainBoolean>(DomainBoolean::cast(aDomainPtr));
    }
  }//getSharedFromRawDomainPointer
  
  DomainPtr DomainFactory::domain(DomainElement* aDomainElement, DomainClass aDomainClass)
  {
    assert(aDomainElement);
    assert(!DomainElementVoid::isa(aDomainElement));
    return domain(aDomainElement, aDomainElement, aDomainClass);
  }//domain
  
  DomainPtr DomainFactory::domain(DomainElement* aDomainElementLowerBound, DomainElement* aDomainElementUpperBound,
                                  DomainClass aDomainClass)
  {
    assert(aDomainElementLowerBound);
    assert(aDomainElementUpperBound);
    assert(!DomainElementVoid::isa(aDomainElementLowerBound));
    assert(!DomainElementVoid::isa(aDomainElementUpperBound));
    assert(aDomainElementLowerBound->getType() == aDomainElementUpperBound->getType());
    
    auto domainElementType = aDomainElementLowerBound->getType();
    if(!isValidTypeCoercion(aDomainClass, domainElementType))
    {
      return nullptr;
    }
    
    switch (aDomainClass) {
      case DomainClass::DOM_INT64:
      {
        assert(DomainElementInt64::isa(aDomainElementLowerBound));
        assert(DomainElementInt64::isa(aDomainElementUpperBound));
        return domainInteger(DomainElementInt64::cast(aDomainElementLowerBound),
                             DomainElementInt64::cast(aDomainElementUpperBound));
      }
      default:
        assert(aDomainClass == DomainClass::DOM_BOOL);
      {
        assert(DomainElementInt64::isa(aDomainElementLowerBound));
        assert(DomainElementInt64::isa(aDomainElementUpperBound));
        DomainElementInt64* lowerBound = DomainElementInt64::cast(aDomainElementLowerBound);
        DomainElementInt64* upperBound = DomainElementInt64::cast(aDomainElementUpperBound);
        if(lowerBound->isEqual(upperBound))
        {
          if(lowerBound->isEqual(lowerBound->zero()))
          {
            return domainBoolean(false);
          }
          return domainBoolean(true);
        }
        return domainBoolean();
      }
    }
  }// domain
  
  DomainIntegerPtr DomainFactory::domainInteger(INT_64 aDomainElement)
  {
    DomainElementInt64* element = DomainElementManager::getInstance().createDomainElementInt64(aDomainElement);
    return domainInteger(element, element);
  }//domainInteger
  
  DomainIntegerPtr DomainFactory::domainInteger(INT_64 aLowerBound, INT_64 aUpperBound)
  {
    return domainInteger(DomainElementManager::getInstance().createDomainElementInt64(aLowerBound),
                         DomainElementManager::getInstance().createDomainElementInt64(aUpperBound));
  }//domainInteger
  
  DomainIntegerPtr DomainFactory::domainInteger(const std::unordered_set<INT_64>& aSetOfElements)
  {
    std::unordered_set<DomainElementInt64*> setOfElement;
    for(auto elem : aSetOfElements)
    {
      setOfElement.insert(DomainElementManager::getInstance().createDomainElementInt64(elem));
    }
    return domainInteger(setOfElement);
  }//domainInteger
  
  DomainIntegerPtr DomainFactory::domainInteger(const std::unordered_set<INT_64>&& aSetOfElements)
  {
    return domainInteger(aSetOfElements);
  }//domainInteger
  
  DomainIntegerPtr DomainFactory::domainInteger(Core::DomainElementInt64 *aLowerBound, Core::DomainElementInt64 *aUpperBound)
  {
    assert(aLowerBound);
    assert(aUpperBound);
    return new DomainInteger(aLowerBound, aUpperBound);
  }//domainInteger
  
  DomainIntegerPtr DomainFactory::domainInteger(std::unordered_set<DomainElementInt64 *>&& aSetOfElements)
  {
    return new DomainInteger(aSetOfElements);
  }//domainInteger
  
  DomainIntegerPtr DomainFactory::domainInteger(const std::unordered_set<DomainElementInt64 *>& aSetOfElements)
  {
    return new DomainInteger(aSetOfElements);
  }//domainInteger
  
  DomainBooleanPtr DomainFactory::domainBoolean()
  {
    return new DomainBoolean();
  }//domainBoolean
  
  DomainBooleanPtr DomainFactory::domainBoolean(bool aBoolVal)
  {
    DomainBooleanPtr domBool = new DomainBoolean();
    if(aBoolVal)
    {
      domBool->setTrue();
    }
    else
    {
      domBool->setFalse();
    }
    return domBool;
  }//domainBoolean
  
}// end namespace Core
