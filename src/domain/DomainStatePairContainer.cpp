// Base class
#include "DomainStatePairContainer.hpp"

// Concrete storage
#include "DomainConcreteStorageBitmap.hpp"
#include "DomainConcreteStorageBounds.hpp"
#include "DomainConcreteStorageSet.hpp"

#include <cassert>

namespace Core {
  
  DomainStatePairContainer::DomainStatePairContainer()
    : DomainState(DomainStateType::DOM_STATE_PAIR_CONT)
  {
    resetInternalState();
  }
  
  DomainStatePairContainer::~DomainStatePairContainer()
  {
  }

}// end namespace Core
