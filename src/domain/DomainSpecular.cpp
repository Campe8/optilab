// Base class
#include "DomainSpecular.hpp"

#include <cassert>

namespace Core {
  
  DomainSpecular::DomainSpecular(Domain *aDomainBase)
  : Core::Domain(aDomainBase->getDomainType())
  , pDomainBase(aDomainBase)
  {
  }
  
  DomainSpecular::~DomainSpecular()
  {
  }

}// end namespace Core
