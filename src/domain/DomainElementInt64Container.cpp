// Base class
#include "DomainElementInt64Container.hpp"

// Domain concrete storage
#include "DomainConcreteStorageBitmap.hpp"
#include "DomainConcreteStorageBounds.hpp"
#include "DomainConcreteStorageSet.hpp"

// Domain element
#include "DomainElementVoid.hpp"
#include "DomainElementInt64.hpp"

// Decorator
#include "DomainDecorator.hpp"

// Domain state
#include "DomainStateInt64Container.hpp"

#include <algorithm>

#include <map>

namespace Core {
  
  DomainElementInt64Container::DomainElementInt64Container(DomainElementInt64 *aLowerBound, DomainElementInt64 *aUpperBound)
  : DomainElementContainer(DomainElementContainerClass::DOM_ELEM_CONTAINER_INT64)
  {
    assert(aLowerBound);
    assert(aUpperBound);
    
    UINT_64 domainSize = getDomainSizeFromBounds(aLowerBound->getValue(), aUpperBound->getValue());
    
    if (domainSize <= getDomainConcreteStorageBitmapMaxAllocSize())
    {
      pConcreteStorage.reset(new DomainConcreteStorageBitmap(aLowerBound, aUpperBound, getDomainElementManager()));
    }
    else
    {
      pConcreteStorage.reset(new DomainConcreteStorageBounds(aLowerBound, aUpperBound, getDomainElementManager()));
    }
  }
  
  DomainElementInt64Container::DomainElementInt64Container(const std::unordered_set<DomainElementInt64 *>& aSetOfElements)
  : DomainElementContainer(DomainElementContainerClass::DOM_ELEM_CONTAINER_INT64)
  {
    std::unordered_set<DomainElement *> setOfDomainElements;
    for(auto elem : aSetOfElements)
    {
      setOfDomainElements.insert(elem);
    }
    pConcreteStorage.reset(new DomainConcreteStorageSet(setOfDomainElements, getDomainElementManager()));
    
    // Switch to bitmap if possibile
    switchCurrentStorageToStorageBitmap();
  }
  
  DomainElementInt64Container::DomainElementInt64Container(const DomainElementInt64Container& aOther)
  : DomainElementContainer(DomainElementContainerClass::DOM_ELEM_CONTAINER_INT64)
  {
    auto storageType = aOther.getStorageType();
    switch (storageType) {
      case DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_BITMAP:
      {
        DomainConcreteStorageBitmap* otherStorage = static_cast<DomainConcreteStorageBitmap*>(aOther.pConcreteStorage.get());
        pConcreteStorage.reset(new DomainConcreteStorageBitmap(*otherStorage));
      }
        break;
      case DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_BOUNDS:
      {
        DomainConcreteStorageBounds* otherStorage = static_cast<DomainConcreteStorageBounds*>(aOther.pConcreteStorage.get());
        pConcreteStorage.reset(new DomainConcreteStorageBounds(*otherStorage));
      }
        break;
      default:
        assert(storageType == DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_SET);
      {
        DomainConcreteStorageSet* otherStorage = static_cast<DomainConcreteStorageSet*>(aOther.pConcreteStorage.get());
        pConcreteStorage.reset(new DomainConcreteStorageSet(*otherStorage));
      }
        break;
    }
  }
  
  DomainElementInt64Container::DomainElementInt64Container(DomainElementInt64Container&& aOther)
  : DomainElementContainer(DomainElementContainerClass::DOM_ELEM_CONTAINER_INT64)
  {
    auto storageType = aOther.getStorageType();
    switch (storageType) {
      case DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_BITMAP:
      {
        DomainConcreteStorageBitmap* otherStorage = static_cast<DomainConcreteStorageBitmap*>(aOther.pConcreteStorage.get());
        pConcreteStorage.reset(new DomainConcreteStorageBitmap(*otherStorage));
      }
        break;
      case DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_BOUNDS:
      {
        DomainConcreteStorageBounds* otherStorage = static_cast<DomainConcreteStorageBounds*>(aOther.pConcreteStorage.get());
        pConcreteStorage.reset(new DomainConcreteStorageBounds(*otherStorage));
      }
        break;
      default:
        assert(storageType == DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_SET);
      {
        DomainConcreteStorageSet* otherStorage = static_cast<DomainConcreteStorageSet*>(aOther.pConcreteStorage.get());
        pConcreteStorage.reset(new DomainConcreteStorageSet(*otherStorage));
      }
        break;
    }
    
    aOther.pConcreteStorage.reset(nullptr);
  }
  
  DomainElementInt64Container::~DomainElementInt64Container()
  {
  }
  
  DomainElementInt64Container& DomainElementInt64Container::operator= (const DomainElementInt64Container& aOther)
  {
    if(this != &aOther)
    {
      DomainElementContainer::operator=(aOther);
      
      auto storageType = aOther.getStorageType();
      switch (storageType) {
        case DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_BITMAP:
        {
          DomainConcreteStorageBitmap* otherStorage = static_cast<DomainConcreteStorageBitmap*>(aOther.pConcreteStorage.get());
          pConcreteStorage.reset(new DomainConcreteStorageBitmap(*otherStorage));
        }
          break;
        case DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_BOUNDS:
        {
          DomainConcreteStorageBounds* otherStorage = static_cast<DomainConcreteStorageBounds*>(aOther.pConcreteStorage.get());
          pConcreteStorage.reset(new DomainConcreteStorageBounds(*otherStorage));
        }
          break;
        default:
          assert(storageType == DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_SET);
        {
          DomainConcreteStorageSet* otherStorage = static_cast<DomainConcreteStorageSet*>(aOther.pConcreteStorage.get());
          pConcreteStorage.reset(new DomainConcreteStorageSet(*otherStorage));
        }
          break;
      }
    }
    return *this;
  }
  
  DomainElementInt64Container& DomainElementInt64Container::operator= (DomainElementInt64Container&& aOther)
  {
    if(this != &aOther)
    {
      DomainElementContainer::operator=(aOther);
      
      auto storageType = aOther.getStorageType();
      switch (storageType) {
        case DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_BITMAP:
        {
          DomainConcreteStorageBitmap* otherStorage = static_cast<DomainConcreteStorageBitmap*>(aOther.pConcreteStorage.get());
          pConcreteStorage.reset(new DomainConcreteStorageBitmap(*otherStorage));
        }
          break;
        case DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_BOUNDS:
        {
          DomainConcreteStorageBounds* otherStorage = static_cast<DomainConcreteStorageBounds*>(aOther.pConcreteStorage.get());
          pConcreteStorage.reset(new DomainConcreteStorageBounds(*otherStorage));
        }
          break;
        default:
          assert(storageType == DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_SET);
        {
          DomainConcreteStorageSet* otherStorage = static_cast<DomainConcreteStorageSet*>(aOther.pConcreteStorage.get());
          pConcreteStorage.reset(new DomainConcreteStorageSet(*otherStorage));
        }
          break;
      }
      aOther.pConcreteStorage.reset(nullptr);
    }
    return *this;
  }
  
  void DomainElementInt64Container::intersectWithDomainElementContainer(DomainElementContainer *aDomainElementContainer)
  {
    assert(aDomainElementContainer);
    assert(DomainElementInt64Container::isa(aDomainElementContainer));
    
    DomainConcreteStorageType storageType = pConcreteStorage->getConcreteStorageType();
    DomainElementInt64Container otherContainer = *(DomainElementInt64Container::cast(aDomainElementContainer));
    
    // If container to intersect with is empty, empty this container
    if(otherContainer.getDomainSize() == 0)
    {
      emptyDomain();
      return;
    }
    
    // Out of bounds for subtract: empty domain
    DomainElement * minDomElem = otherContainer.getLowerBound();
    DomainElement * maxDomElem = otherContainer.getUpperBound();
    if(getUpperBound()->isLessThan(minDomElem) ||
       maxDomElem->isLessThan(getLowerBound()))
    {
      emptyDomain();
      return;
    }
    
    otherContainer.inMin(getLowerBound());
    otherContainer.inMax(getUpperBound());
    
    switch (storageType)
    {
      case DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_SET:
      case DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_BITMAP:
      {
        std::size_t arraySize = otherContainer.getDomainSize();
        DomainElementArray array(arraySize);
        for (std::size_t idx = 1; idx <= arraySize; ++idx)
        {
          array.assignElementToCell(idx - 1, otherContainer.getThElement(idx));
        }
        intersect(&array);
        break;
      }
      default:
      {
        assert(storageType == DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_BOUNDS);
        
        pConcreteStorage->shrink(otherContainer.getLowerBound(), otherContainer.getUpperBound());
        
        // Return on empty domain
        if (getDomainSize() == 0)
        {
          return;
        }
        
        switchStorageSetToStorageBitmap();
        break;
      }
    }
  }//intersectWithDomainElementContainer
  
  void DomainElementInt64Container::intersectWithArray(DomainElementArray *aDomainElementArray)
  {
    assert(aDomainElementArray);
    
    // If array to intersect with is empty, empty this container
    if (aDomainElementArray->empty())
    {
      emptyDomain();
      return;
    }
    
    auto pairMinMax = getMinMaxFromArray(aDomainElementArray);
    DomainElement * minDomElem = pairMinMax.first;
    DomainElement * maxDomElem = pairMinMax.second;
    
    // Get min element from aDomainElementArray,
    // if greater than upper bound -> empty domain
    if (getUpperBound()->isLessThan(minDomElem))
    {
      emptyDomain();
      return;
    }
    
    // Get max element from aDomainElementArray,
    // if less than lower bound -> empty domain
    if (maxDomElem->isLessThan(getLowerBound()))
    {
      emptyDomain();
      return;
    }
    
    std::unordered_set<DomainElement *> setDomain;
    for (std::size_t idx = 0; idx < aDomainElementArray->size(); ++idx)
    {
      DomainElement *elem = aDomainElementArray->operator[](idx);
      
      assert(DomainElementInt64::isa(elem));
      if (contains(elem))
      {
        setDomain.insert(elem);
      }
    }
    
    // Create new concrete storage with given set of elements
    pConcreteStorage.reset(new DomainConcreteStorageSet(setDomain, getDomainElementManager()));
    
    // Convert to bitmap if possible
    switchCurrentStorageToStorageBitmap();
  }//intersectWithArray
  
  void DomainElementInt64Container::subtractDomainElementContainer(DomainElementContainer *aDomainElementContainer)
  {
    assert(aDomainElementContainer);
    assert(DomainElementInt64Container::isa(aDomainElementContainer));
    
    DomainConcreteStorageType storageType = pConcreteStorage->getConcreteStorageType();
    DomainElementInt64Container *otherContainer = DomainElementInt64Container::cast(aDomainElementContainer);
    
    // If container to subtract is empty, return
    if(otherContainer->getDomainSize() == 0)
    {
      return;
    }
    
    // Out of bounds for subtract: return
    DomainElement * minDomElem = otherContainer->getLowerBound();
    DomainElement * maxDomElem = otherContainer->getUpperBound();
    
    if(getUpperBound()->isLessThan(minDomElem) ||
       maxDomElem->isLessThan(getLowerBound()))
    {
      return;
    }
    
    switch (storageType)
    {
      case DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_SET:
      {
        subtractOnStorageSet(otherContainer);
        
        switchCurrentStorageToStorageBitmap();
        break;
      }
      case DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_BITMAP:
      {
        std::size_t arraySize = otherContainer->getDomainSize();
        DomainElementArray array(arraySize);
        for (std::size_t idx = 1; idx <= arraySize; ++idx)
        {
          array.assignElementToCell(idx - 1, otherContainer->getThElement(idx));
        }
        subtract(&array);
        break;
      }
      default:
      {
        assert(storageType == DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_BOUNDS);
        subtractOnStorageBounds(otherContainer->getLowerBound(), otherContainer->getUpperBound());
        
        // Return on empty domain
        if (getDomainSize() == 0)
        {
          return;
        }
        
        switchCurrentStorageToStorageBitmap();
        break;
      }
    }
  }//subtractDomainElementContainer
  
  void DomainElementInt64Container::subtractOnStorageSet(DomainElementContainer *aContainer)
  {
    assert(aContainer);
    assert(DomainElementInt64Container::isa(aContainer));
    assert(getStorageType() == DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_SET);
    
    DomainElementInt64Container otherContainer = *DomainElementInt64Container::cast(aContainer);
    otherContainer.inMin(getLowerBound());
    otherContainer.inMax(getUpperBound());
    
    std::size_t arraySize = otherContainer.getDomainSize();
    for (std::size_t idx = 1; idx <= arraySize; ++idx)
    {
      pConcreteStorage->subtract(otherContainer.getThElement(idx));
      if (getDomainSize() == 0)
      {
        return;
      }
    }
  }//subtractOnStorageSet
  
  void DomainElementInt64Container::subtractOnStorageBounds(DomainElement *aLowerBound, DomainElement* aUpperBound)
  {
    assert(aLowerBound);
    assert(aUpperBound);
    assert(aLowerBound->isLessThanOrEqual(aUpperBound));
    assert(getStorageType() == DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_BOUNDS);
    
    if(aLowerBound->isEqual(aUpperBound))
    {
      subtract(aLowerBound);
      return;
    }
    
    DomainElement *currentLowerBound = getLowerBound();
    DomainElement *currentUpperBound = getUpperBound();
    
    // Return if new container is fully contained in this container:
    //       [   ]
    //        { }
    if(currentLowerBound->isLessThan(aLowerBound) && aUpperBound->isLessThan(currentUpperBound))
    {
      return;
    }
    
    // Possible cases:
    // 1 -   [   ]
    //     {   }
    // 2 -   [   ]
    //         {   }
    // 3 - { [   ]
    //     { }
    // 4 -   [   ]
    //           { }
    // 5 -   [   ]
    //       {   }
    // 6 -   [   ]
    //         { }
    // 7 -   [   ]
    //       { }
    
    // Case 5
    if(currentLowerBound->isEqual(aLowerBound) && currentUpperBound->isEqual(aUpperBound))
    {
      emptyDomain();
      return;
    }
    
    // Case 6
    if(currentUpperBound->isEqual(aUpperBound) && currentLowerBound->isLessThan(aLowerBound))
    {
      pConcreteStorage->inMax(aLowerBound->predecessor(aLowerBound));
      return;
    }
    
    // Case 7
    if(aUpperBound->isLessThan(currentUpperBound) && currentLowerBound->isEqual(aLowerBound))
    {
      pConcreteStorage->inMin(aUpperBound->successor(aUpperBound));
      return;
    }
    
    // Cases 2 and 4
    if(currentUpperBound->isLessThan(aUpperBound))
    {
      assert(aLowerBound->isLessThanOrEqual(currentUpperBound));
      
      pConcreteStorage->inMax(aLowerBound->predecessor(aLowerBound));
      return;
    }
    
    // Cases 1 and 3
    assert(aLowerBound->isLessThan(currentLowerBound));
    assert(aUpperBound->isLessThanOrEqual(currentUpperBound));
    pConcreteStorage->inMin(aUpperBound->successor(aUpperBound));
  }//subtractOnStorageBounds
  
  void DomainElementInt64Container::subtractArray(DomainElementArray *aDomainElementArray)
  {
    assert(aDomainElementArray);
    
    // If array to subtract is empty, return
    if (aDomainElementArray->empty())
    {
      return;
    }
    
    auto pairMinMax = getMinMaxFromArray(aDomainElementArray);
    DomainElement * minDomElem = pairMinMax.first;
    DomainElement * maxDomElem = pairMinMax.second;
    
    // Get min element from aDomainElementArray,
    // if greater than upper bound -> return
    if (getUpperBound()->isLessThan(minDomElem))
    {
      return;
    }
    
    // Get max element from aDomainElementArray,
    // if less than lower bound -> return
    if (maxDomElem->isLessThan(getLowerBound()))
    {
      return;
    }
    
    DomainConcreteStorageType storageType = pConcreteStorage->getConcreteStorageType();
    switch (storageType)
    {
      case DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_SET:
      case DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_BITMAP:
      {
        DomainElement *currentLowerBound = getLowerBound();
        DomainElement *currentUpperBound = getUpperBound();
        for (std::size_t idx = 0; idx < aDomainElementArray->size(); ++idx)
        {
          DomainElement *elem = aDomainElementArray->operator[](idx);
          if(elem->isLessThan(currentLowerBound) || currentUpperBound->isLessThan(elem))
          {
            continue;
          }
          
          assert(DomainElementInt64::isa(elem));
          if (contains(elem))
          {
            subtractElement(elem);
          }
          
          if (pConcreteStorage->getSize() == 0)
          {
            return;
          }
        }
        break;
      }
      default:
      {
        assert(storageType == DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_BOUNDS);
        
        subtractOnStorageBounds(aDomainElementArray);
        
        // Return on empty domain
        if (getDomainSize() == 0)
        {
          return;
        }

        break;
      }
    }
    
    // Convert to bitmap if possible
    switchCurrentStorageToStorageBitmap();
  }//subtractArray
  
  void DomainElementInt64Container::subtractOnStorageBounds(DomainElementArray *aDomainElementArray)
  {
    // Sort aDomainElementArray in order to remove as much as possible
    // from the current storage
    assert(aDomainElementArray);
    if(aDomainElementArray->empty())
    {
      return;
    }
    
    // Using std map to sort the elements
    std::map<INT_64, DomainElementInt64 *> rbtMap;
    for(std::size_t idx = 0; idx < aDomainElementArray->size(); ++idx)
    {
      assert(DomainElementInt64::isa(aDomainElementArray->operator[](idx)));
      DomainElementInt64 *elem = DomainElementInt64::cast(aDomainElementArray->operator[](idx));
      
      rbtMap[elem->getValue()] = elem;
    }
    
    // Traverse RBT and
    DomainElement *currentLowerBound = getLowerBound();
    DomainElement *currentUpperBound = getUpperBound();
    
    if(currentUpperBound->isLessThan(rbtMap.begin()->second) ||
       rbtMap.rbegin()->second->isLessThan(currentLowerBound) )
    {
      return;
    }
    
    for(auto it = rbtMap.rbegin(); it != rbtMap.rend(); ++it)
    {
      DomainElementInt64 *elem = it->second;
      if(currentUpperBound->isLessThan(elem))
      {
        continue;
      }
      
      if (contains(elem))
      {
        subtractElement(elem);
      }
      else
      {
        // Holes in bewteen boundaries do not
        // change the number of elements
        break;
      }
    }
    
    if(currentUpperBound->isLessThan(rbtMap.begin()->second) ||
       rbtMap.rbegin()->second->isLessThan(currentLowerBound) )
    {
      return;
    }
    for(auto it = rbtMap.begin(); it != rbtMap.end(); ++it)
    {
      DomainElementInt64 *elem = it->second;
      if(elem->isLessThan(currentLowerBound))
      {
        continue;
      }
      
      if (contains(elem))
      {
        subtractElement(elem);
      }
      else
      {
        // Holes in bewteen boundaries do not
        // change the number of elements
        break;
      }
    }
    
  }//subtractOnStorageBounds
  
  bool DomainElementInt64Container::contains(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    assert(pConcreteStorage);
    
    // Void element does not compare with other elements
    if(DomainElementVoid::isa(aDomainElement))
    {
      return false;
    }
    
    return pConcreteStorage->contains(aDomainElement);
  }//contains
  
  void DomainElementInt64Container::switchCurrentStorageToStorageBitmap()
  {
    assert(pConcreteStorage);
    
    if(getDomainSize() == 0)
    {
      return;
    }
    
    // Switch to bitmap only if current set can be represented by the bitmap
    auto lowerBound = pConcreteStorage->getLowerBound();
    auto upperBound = pConcreteStorage->getUpperBound();
    assert(DomainElementInt64::isa(lowerBound));
    assert(DomainElementInt64::isa(upperBound));
    
    UINT_64 domainSize = getDomainSizeFromBounds(DomainElementInt64::cast(lowerBound)->getValue(),
                                                 DomainElementInt64::cast(upperBound)->getValue());
    
    if (domainSize > getDomainConcreteStorageBitmapMaxAllocSize())
    {
      return;
    }
    
    // Only bound representation can be converted into bitmap representation
    if (getStorageType() == DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_BOUNDS)
    {
      switchStorageBoundsToStorageBitmap();
    }
    else if ((getStorageType() == DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_SET) &&
             (domainSize > getDomainConcreteStorageBitmapMaxAllocSizeStrict()))
    {
      switchStorageSetToStorageBitmap();
    }
  }//switchCurrentStorageToStorageBitmap
  
  void DomainElementInt64Container::switchStorageSetToStorageBitmap()
  {
    assert(pConcreteStorage);
    
    if(getDomainSize() == 0)
    {
      return;
    }
    
    // Only set representation can be converted into bitmap representation
    if (getStorageType() != DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_SET)
    {
      return;
    }
    
    // Switch to bitmap only if current set can be represented by the bitmap
    auto lowerBound = pConcreteStorage->getLowerBound();
    auto upperBound = pConcreteStorage->getUpperBound();
    assert(DomainElementInt64::isa(lowerBound));
    assert(DomainElementInt64::isa(upperBound));
    
    // Store the elements of the set
    std::vector<INT_64> valuesInSet(getDomainSize());
    for(std::size_t idx = 1; idx <= getDomainSize(); idx++)
    {
      valuesInSet[idx-1] = DomainElementInt64::cast(getThElement(idx))->getValue();
    }
    
    // Convert current storage into bitmap
    pConcreteStorage.reset(new DomainConcreteStorageBitmap(DomainElementInt64::cast(lowerBound),
                                                           DomainElementInt64::cast(upperBound),
                                                           getDomainElementManager()));
    
    // Remove from current storage all elements not present in the sorted set/vector
    for(INT_64 idx = DomainElementInt64::cast(lowerBound)->getValue();
        idx <= DomainElementInt64::cast(upperBound)->getValue(); ++idx)
    {
      auto it = std::find(valuesInSet.begin(), valuesInSet.end(), idx);
      if(it == valuesInSet.end())
      {
        DomainElementInt64 *tempVal = getDomainElementManager()->createDomainElementInt64(idx);
        assert(pConcreteStorage->contains(tempVal));
        
        pConcreteStorage->subtract(tempVal);
      }
    }
  }//switchStorageSetToStorageBitmap
  
  void DomainElementInt64Container::switchStorageBoundsToStorageBitmap()
  {
    assert(pConcreteStorage);
    
    if(getDomainSize() == 0)
    {
      return;
    }
    
    // Only bound representation can be converted into bitmap representation
    if (getStorageType() != DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_BOUNDS)
    {
      return;
    }
    
    auto lowerBound = pConcreteStorage->getLowerBound();
    auto upperBound = pConcreteStorage->getUpperBound();
    assert(DomainElementInt64::isa(lowerBound));
    assert(DomainElementInt64::isa(upperBound));
    
    pConcreteStorage.reset(new DomainConcreteStorageBitmap(DomainElementInt64::cast(lowerBound),
                                                           DomainElementInt64::cast(upperBound),
                                                           getDomainElementManager()));
  }//switchStorageBoundsToStorageBitmap
  
  std::size_t DomainElementInt64Container::getDomainSize() const
  {
    assert(pConcreteStorage);
    return pConcreteStorage->getSize();
  }//getDomainSize
  
  std::unique_ptr<DomainState> DomainElementInt64Container::getDomainSnapshot()
  {
    std::unique_ptr<DomainStateInt64Container> domainState(new DomainStateInt64Container());
    DomainStateInt64Container::cast(domainState.get())->setInternalState(pConcreteStorage.get());
    return std::move(domainState);
  }//getDomainSnapshot
  
  void DomainElementInt64Container::reinstateInternalDomainSnapshot(const DomainState* aDomainSnapshot)
  {
    assert(aDomainSnapshot);
    assert(DomainStateInt64Container::isa(aDomainSnapshot));
    assert(DomainStateInt64Container::cast(aDomainSnapshot)->validState());
    
    pConcreteStorage = DomainStateInt64Container::cast(aDomainSnapshot)->getInternalStateCopy();
  }//reinstateInternalDomainSnapshot
  
  void DomainElementInt64Container::shrinkOnBounds(DomainElement *aLowerBound, DomainElement *aUpperBound)
  {
    assert(aLowerBound);
    assert(aUpperBound);
    assert(pConcreteStorage);
    
    assert(!DomainElementVoid::isa(aLowerBound));
    assert(!DomainElementVoid::isa(aUpperBound));
    
    if (pConcreteStorage->getSize() == 0)
    {
      return;
    }
    
    if(aUpperBound->isLessThan(aLowerBound))
    {
      emptyDomain();
      return;
    }

    assert(aLowerBound->isLessThanOrEqual(aUpperBound));
    
    pConcreteStorage->shrink(aLowerBound, aUpperBound);
    
    // Change internal storage to bitmap if domain is not empty
    auto domainSize = pConcreteStorage->getSize();
    if (domainSize > 0 && domainSize <= getDomainConcreteStorageBitmapMaxAllocSize())
    {
      switchCurrentStorageToStorageBitmap();
    }
  }//shrinkOnBounds
  
  void DomainElementInt64Container::subtractElement(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    assert(pConcreteStorage);
    
    assert(!DomainElementVoid::isa(aDomainElement));
    
    if (pConcreteStorage->getSize() == 0)
    {
      return;
    }
    
    pConcreteStorage->subtract(aDomainElement);
    
    // Change internal storage to bitmap if domain is not empty
    auto domainSize = pConcreteStorage->getSize();
    if (domainSize > 0 && domainSize <= getDomainConcreteStorageBitmapMaxAllocSize())
    {
      switchCurrentStorageToStorageBitmap();
    }
  }//subtractElement
  
  void DomainElementInt64Container::shrinkOnMinBound(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    assert(pConcreteStorage);
    
    assert(!DomainElementVoid::isa(aDomainElement));
    
    if (pConcreteStorage->getSize() == 0)
    {
      return;
    }
    
    pConcreteStorage->inMin(aDomainElement);
    
    // Change internal storage to bitmap if domain is not empty
    auto domainSize = pConcreteStorage->getSize();
    if (domainSize > 0 && domainSize <= getDomainConcreteStorageBitmapMaxAllocSize())
    {
      switchCurrentStorageToStorageBitmap();
    }
  }//shrinkOnMinBound
  
  void DomainElementInt64Container::shrinkOnMaxBound(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    assert(pConcreteStorage);
    
    assert(!DomainElementVoid::isa(aDomainElement));
    
    if (pConcreteStorage->getSize() == 0)
    {
      return;
    }
    
    pConcreteStorage->inMax(aDomainElement);
    
    // Change internal storage to bitmap if domain is not empty
    auto domainSize = pConcreteStorage->getSize();
    if (domainSize > 0 && domainSize <= getDomainConcreteStorageBitmapMaxAllocSize())
    {
      switchCurrentStorageToStorageBitmap();
    }
  }//shrinkOnMaxBound
  
  UINT_64 DomainElementInt64Container::getDomainSizeFromBounds(INT_64 aLowerBound, INT_64 aUpperBound) const
  {
    UINT_64 pDomainSize = 0;
    if (aUpperBound < aLowerBound)
    {
      return pDomainSize;
    }
    
    if (aUpperBound >= 0)
    {
      if (aLowerBound >= 0)
      {
        pDomainSize = static_cast<UINT_64>(aUpperBound - aLowerBound);
      }
      else
      {
        pDomainSize = static_cast<UINT_64>(aUpperBound + std::abs(aLowerBound));
      }
    }
    else
    {
      pDomainSize = static_cast<UINT_64>(std::abs(aLowerBound) - std::abs(aUpperBound));
    }
    
    return ++pDomainSize;
  }//getDomainSizeFromBounds
  
  DomainElement *DomainElementInt64Container::getElementGreaterThan(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    
    // Void element does not compare with other elements
    if(DomainElementVoid::isa(aDomainElement))
    {
      return aDomainElement;
    }
    
    // If "aDomainElement" is upper bound, return void element
    if(DomainElement::lessThanOrEqual(getUpperBound(), aDomainElement))
    {
      return (getDomainElementManager())->getVoidElement();
    }
    
    assert(pConcreteStorage);
    return pConcreteStorage->getNextElement(aDomainElement);
  }//getElementGreaterThan
  
  DomainElement *DomainElementInt64Container::getElementLessThan(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    
    // Void element does not compare with other elements
    if(DomainElementVoid::isa(aDomainElement))
    {
      return aDomainElement;
    }
    
    // If "aDomainElement" is lower bound, return void element
    if(DomainElement::lessThanOrEqual(aDomainElement, getLowerBound()))
    {
      return (getDomainElementManager())->getVoidElement();
    }
    
    assert(pConcreteStorage);
    return pConcreteStorage->getPrevElement(aDomainElement);
  }//getElementLessThan
  
  DomainElement *DomainElementInt64Container::getThElement(std::size_t aElementThIndex)
  {
    if(aElementThIndex == 0 || aElementThIndex > getDomainSize())
    {
      return getDomainElementManager()->getVoidElement();
    }
    
    assert(pConcreteStorage);
    return pConcreteStorage->getElementByIndex(aElementThIndex-1);
  }//getThElement
  
  DomainElement *DomainElementInt64Container::getLowerBound()
  {
    assert(pConcreteStorage);
    if (!pConcreteStorage->getSize())
    {
      return nullptr;
    }
    return pConcreteStorage->getLowerBound();
  }//getLowerBound
  
  DomainElement *DomainElementInt64Container::getUpperBound()
  {
    assert(pConcreteStorage);
    if (!pConcreteStorage->getSize())
    {
      return nullptr;
    }
    return pConcreteStorage->getUpperBound();
  }//getUpperBound
  
  void DomainElementInt64Container::emptyDomain()
  {
    DomainElement *minElem = getLowerBound();
    minElem = minElem->predecessor();
    
    inMax(minElem);
  }//emptyDomain
  
}// end namespace Core
