#include "DomainDefs.hpp"

namespace Core {

  namespace Limits {
    
    UBOOL boolMax()
    {
      return 1;
    }//boolMax
    
    UBOOL boolMin()
    {
      return 0;
    }//boolMin
    
    INT_64 int64Max()
    {
      return std::numeric_limits<INT_64>::max();
    }//int64Max
    
    INT_64 int64Min()
    {
      return std::numeric_limits<INT_64>::lowest() + 1;
    }//int64Min
    
    REAL realMax()
    {
      return std::numeric_limits<REAL>::max();
    }//realMax
    
    REAL realMin()
    {
      return std::numeric_limits<REAL>::lowest() + std::numeric_limits<REAL>::min();
    }//realMin
    
  }// end namespace Limits

}// end namespace Core
