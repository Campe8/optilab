// Base class
#include "DomainElementContainer.hpp"

// Domain element
#include "DomainElement.hpp"

// Decorator
#include "DomainDecorator.hpp"

#include <cassert>

namespace Core {
  
  DomainElementContainer::DomainElementContainer(DomainElementContainerClass aClass)
  : pContainerClass(aClass)
  {
    pDomainElementManager = &(DomainElementManager::getInstance());
  }
  
  DomainElementContainer::DomainElementContainer(const DomainElementContainer& aOther)
  {
    pContainerClass = aOther.pContainerClass;
    pDomainElementManager = &(DomainElementManager::getInstance());
    pDecoratorRegister = aOther.pDecoratorRegister;
  }
  
  DomainElementContainer::DomainElementContainer(DomainElementContainer&& aOther)
  {
    pContainerClass = aOther.pContainerClass;
    pDomainElementManager = &(DomainElementManager::getInstance());
    pDecoratorRegister = aOther.pDecoratorRegister;
    
    aOther.pContainerClass = DomainElementContainerClass::DOM_ELEM_CONTAINER_UNDEF;
    aOther.pDomainElementManager = nullptr;
    aOther.pDecoratorRegister.clear();
  }
  
  DomainElementContainer::~DomainElementContainer()
  {
  }
  
  DomainElementContainer& DomainElementContainer::operator= (const DomainElementContainer& aOther)
  {
    if(this != &aOther)
    {
      pContainerClass = aOther.pContainerClass;
      pDomainElementManager = &(DomainElementManager::getInstance());
      pDecoratorRegister = aOther.pDecoratorRegister;
    }
    return *this;
  }
  
  DomainElementContainer& DomainElementContainer::operator= (DomainElementContainer&& aOther)
  {
    if(this != &aOther)
    {
      pContainerClass = aOther.pContainerClass;
      pDomainElementManager = &(DomainElementManager::getInstance());
      pDecoratorRegister = aOther.pDecoratorRegister;
      
      aOther.pContainerClass = DomainElementContainerClass::DOM_ELEM_CONTAINER_UNDEF;
      aOther.pDomainElementManager = nullptr;
      aOther.pDecoratorRegister.clear();
    }
    return *this;
  }
  
  void DomainElementContainer::registerDecorator(DomainDecorator *aDecorator)
  {
    assert(aDecorator);
    pDecoratorRegister.push_back(aDecorator);
    
    // Update domain decorator values
    notifyDomainDecorators(getLowerBound(), getUpperBound());
  }// registerDecorator
  
  void DomainElementContainer::reinstateDomainSnapshot(const DomainState* aDomainSnapshot)
  {
    assert(aDomainSnapshot);
    reinstateInternalDomainSnapshot(aDomainSnapshot);
    
    // Update decorators
    notifyDomainDecorators(getLowerBound(), getUpperBound());
  }//reinstateDomainSnapshot
  
  void DomainElementContainer::shrink(DomainElement *aLowerBound, DomainElement *aUpperBound)
  {
    assert(aLowerBound);
    assert(aUpperBound);
    
    if (getDomainSize() == 0)
    {
      notifyDomainDecorators(pDomainElementManager->getVoidElement(),
                             pDomainElementManager->getVoidElement());
      return;
    }
    
    // Shrink domain
    shrinkOnBounds(aLowerBound, aUpperBound);
    
    // Notify domain decorators
    notifyDomainDecorators(getLowerBound(), getUpperBound());
  }//shrink

  void DomainElementContainer::subtract(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    
    if (getDomainSize() == 0)
    {
      notifyDomainDecorators(pDomainElementManager->getVoidElement(),
                             pDomainElementManager->getVoidElement());
      return;
    }
    
    // Shrink domain
    subtractElement(aDomainElement);
    
    // Notify domain decorators
    notifyDomainDecorators(getLowerBound(), getUpperBound());
  }//subtract
  
  void DomainElementContainer::inMin(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    
    if (getDomainSize() == 0)
    {
      notifyDomainDecorators(pDomainElementManager->getVoidElement(),
                             pDomainElementManager->getVoidElement());
      return;
    }
    
    shrinkOnMinBound(aDomainElement);
    
    // Notify domain decorators
    notifyDomainDecorators(getLowerBound(), getUpperBound());
  }//inMin
  
  void DomainElementContainer::inMax(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    
    if (getDomainSize() == 0)
    {
      notifyDomainDecorators(pDomainElementManager->getVoidElement(),
                             pDomainElementManager->getVoidElement());
      return;
    }
    
    shrinkOnMaxBound(aDomainElement);
    
    // Notify domain decorators
    notifyDomainDecorators(getLowerBound(), getUpperBound());
  }//inMax
  
  void DomainElementContainer::subtract(DomainElementContainer *aDomainElementContainer)
  {
    assert(aDomainElementContainer);
    
    if (getDomainSize() == 0)
    {
      notifyDomainDecorators(pDomainElementManager->getVoidElement(),
                             pDomainElementManager->getVoidElement());
      return;
    }
    
    subtractDomainElementContainer(aDomainElementContainer);
    
    // Notify domain decorators
    notifyDomainDecorators(getLowerBound(), getUpperBound());
  }//subtract
  
  void DomainElementContainer::subtract(DomainElementArray *aDomainElementArray)
  {
    assert(aDomainElementArray);
    
    if (getDomainSize() == 0)
    {
      notifyDomainDecorators(pDomainElementManager->getVoidElement(),
                             pDomainElementManager->getVoidElement());
      return;
    }
    
    subtractArray(aDomainElementArray);
    
    // Notify domain decorators
    notifyDomainDecorators(getLowerBound(), getUpperBound());
  }//subtract
  
  void DomainElementContainer::intersect(DomainElementContainer *aDomainElementContainer)
  {
    assert(aDomainElementContainer);
    
    if (getDomainSize() == 0)
    {
      notifyDomainDecorators(pDomainElementManager->getVoidElement(),
                             pDomainElementManager->getVoidElement());
      return;
    }
    
    intersectWithDomainElementContainer(aDomainElementContainer);
    
    // Notify domain decorators
    notifyDomainDecorators(getLowerBound(), getUpperBound());
  }//intersect
  
  void DomainElementContainer::intersect(DomainElementArray *aDomainElementArray)
  {
    assert(aDomainElementArray);
    
    if (getDomainSize() == 0)
    {
      notifyDomainDecorators(pDomainElementManager->getVoidElement(),
                             pDomainElementManager->getVoidElement());
      return;
    }
    
    intersectWithArray(aDomainElementArray);
    
    // Notify domain decorators
    notifyDomainDecorators(getLowerBound(), getUpperBound());
  }//intersect
  
  void DomainElementContainer::notifyDomainDecorators(DomainElement *aLowerBound, DomainElement *aUpperBound)
  {
    // Do not allow to use nullptr as lower/upper bounds: use void elements
    if(!aLowerBound)
    {
      aLowerBound = getDomainElementManager()->getVoidElement();
    }
    
    if(!aUpperBound)
    {
      aUpperBound = getDomainElementManager()->getVoidElement();
    }
    
    for (auto decorator : pDecoratorRegister)
    {
      decorator->update(getDomainSize(), aLowerBound, aUpperBound);
    }
  }//notifyDomainDecorators
  
  std::pair<DomainElement *, DomainElement*> DomainElementContainer::getMinMaxFromArray(DomainElementArray *aArray)
  {
    assert(aArray);
    if(aArray->empty())
    {
      return std::make_pair(nullptr, nullptr);
    }
    DomainElement* minElem = aArray->operator[](0);
    DomainElement* maxElem = minElem;
    
    for(std::size_t idx = 1; idx < aArray->size(); ++idx)
    {
      DomainElement *elem = aArray->operator[](idx);
      if(DomainElement::lessThan(elem, minElem))
      {
        minElem = elem;
      }
      if (DomainElement::lessThan(maxElem, elem))
      {
        maxElem = elem;
      }
    }
    
    return std::make_pair(minElem, maxElem);
  }//getMinMaxFromArray
  
}// end namespace Core
