// Base class
#include "DomainElementReal.hpp"
#include "DomainElementVoid.hpp"

// Domain element key
#include "DomainElementKey.hpp"

// Domain element manager
#include "DomainElementManager.hpp"

#include <iostream>
#include <iomanip>
#include <string>
#include <math.h>
#include <algorithm>
#include <stdlib.h>
#include <limits>
#include <cassert>

#include <boost/math/special_functions/next.hpp>

namespace Core {
  
  DomainElementReal::DomainElementReal()
  : DomainElement(DomainElementType::DET_REAL)
  , pDomainKey(nullptr)
  {
    pDomainValue = std::numeric_limits<REAL>::lowest();
    pRealStep = REAL_DFT_STEP;
  }
  
  DomainElementReal::DomainElementReal(REAL aVal)
  : DomainElementReal()
  {
    pDomainValue = aVal;
    pDomainKey.reset(new DomainElementKey(aVal, DomainElementType::DET_REAL));
  }
  
  DomainElementReal::DomainElementReal(const DomainElementReal& aOther)
  : DomainElement(aOther)
  {
    pDomainValue = aOther.pDomainValue;
    pRealStep = aOther.pRealStep;
    pDomainKey.reset(new DomainElementKey(*(aOther.pDomainKey.get())));
  }
  
  DomainElementReal::DomainElementReal(DomainElementReal&& aOther)
  : DomainElement(aOther)
  {
    pDomainValue = aOther.pDomainValue;
    pRealStep = aOther.pRealStep;
    pDomainKey.reset(new DomainElementKey(*(aOther.pDomainKey.get())));
    
    aOther.pDomainValue = std::numeric_limits<REAL>::lowest();
    aOther.pRealStep = REAL_DFT_STEP;
    aOther.pDomainKey.reset(nullptr);
  }
  
  DomainElementReal::~DomainElementReal()
  {
  }
  
  DomainElementReal& DomainElementReal::operator= (const DomainElementReal& aOther)
  {
    if (&aOther == this)
    {
      return *this;
    }
    
    // Base class assignment operator
    DomainElement::operator=(aOther);
    
    pDomainValue = aOther.pDomainValue;
    pRealStep = aOther.pRealStep;
    pDomainKey.reset(new DomainElementKey(*(aOther.pDomainKey.get())));
    
    return *this;
  }
  
  DomainElementReal& DomainElementReal::operator= (DomainElementReal&& aOther)
  {
    if (&aOther == this)
    {
      return *this;
    }
    
    // Base class assignment operator
    DomainElement::operator=(aOther);
    
    pDomainValue = aOther.pDomainValue;
    pRealStep = aOther.pRealStep;
    pDomainKey.reset(new DomainElementKey(*(aOther.pDomainKey.get())));
    
    aOther.pDomainValue = std::numeric_limits<REAL>::lowest();
    aOther.pRealStep = REAL_DFT_STEP;
    aOther.pDomainKey.reset(nullptr);
    
    return *this;
  }
  
  bool DomainElementReal::isa(const DomainElement* aDomainElement)
  {
    assert(aDomainElement);
    return aDomainElement->getType() == DomainElementType::DET_REAL;
  }//isa
  
  DomainElementReal* DomainElementReal::cast(DomainElement* aDomainElement)
  {
    assert(aDomainElement);
    
    if (!isa(aDomainElement))
    {
      return nullptr;
    }
    
    return static_cast<DomainElementReal *>(aDomainElement);
  }//cast
  
  DomainElementReal* DomainElementReal::cast(const DomainElement* aDomainElement)
  {
    assert(aDomainElement);
    
    if (!isa(aDomainElement))
    {
      return nullptr;
    }
    
    return static_cast<DomainElementReal *>(const_cast<DomainElement*>(aDomainElement));
  }//cast
  
  REAL DomainElementReal::getValueTruncateAfterPrecision(REAL aVal, REAL aPrecision)
  {
    REAL inversePrecision = 1.0/aPrecision;
    aVal = trunc(aVal * inversePrecision);
    REAL valToRet = aVal / inversePrecision;
    return valToRet;
  }//getValueTruncateAfterPrecision
  
  bool DomainElementReal::isEqual(const DomainElement* aOther) const
  {
    assert(aOther);
    if (DomainElementVoid::isa(aOther))
    {
      return false;
    }
    
    assert(isa(aOther));
    if (!isa(aOther))
    {
      return false;
    }
    
    return pDomainValue == DomainElementReal::cast(aOther)->getValue();
  }//isEqual
  
  bool DomainElementReal::isNotEqual(const DomainElement* aOther) const
  {
    assert(aOther);
    return !isEqual(aOther);
  }//isEqual
  
  bool DomainElementReal::isLessThan(const DomainElement* aOther) const
  {
    if (!isa(aOther))
    {
      return false;
    }
  
    return pDomainValue < cast(aOther)->getValue();
  }//isLessThan
  
  bool DomainElementReal::isLessThanOrEqual(const DomainElement* aOther) const
  {
    if (!isa(aOther))
    {
      return false;
    }
    
    return pDomainValue <= cast(aOther)->getValue();
  }//isLessThanOrEqual
  
  DomainElement* DomainElementReal::min(const DomainElement* aOther) const
  {
    assert(isa(aOther));
    
    if(isLessThanOrEqual(aOther))
    {
      auto elementToReturn = getDomainElementManager().findDomainElementReal(pDomainValue);
      if(!elementToReturn)
      {
        elementToReturn = getDomainElementManager().createDomainElementReal(pDomainValue);
      }
      return elementToReturn;
    }
    else
    {
      DomainElementReal *otherDomainReal = cast(aOther);
      auto elementToReturn = getDomainElementManager().findDomainElementReal(otherDomainReal->getValue());
      if(!elementToReturn)
      {
        elementToReturn = getDomainElementManager().createDomainElementReal(otherDomainReal->getValue());
      }
      return elementToReturn;
    }
  }//min
  
  DomainElement* DomainElementReal::max(const DomainElement* aOther) const
  {
    assert(isa(aOther));
    
    if(!isLessThan(aOther))
    {
      auto elementToReturn = getDomainElementManager().findDomainElementReal(pDomainValue);
      if(!elementToReturn)
      {
        elementToReturn = getDomainElementManager().createDomainElementReal(pDomainValue);
      }
      return elementToReturn;
    }
    else
    {
      DomainElementReal *otherDomainReal = cast(aOther);
      auto elementToReturn = getDomainElementManager().findDomainElementReal(otherDomainReal->getValue());
      if(!elementToReturn)
      {
        elementToReturn = getDomainElementManager().createDomainElementReal(otherDomainReal->getValue());
      }
      return elementToReturn;
    }
  }//max
  
  DomainElement* DomainElementReal::abs() const
  {
    auto thisValue = getValue();
    auto elementToReturn = getDomainElementManager().findDomainElementReal(pDomainValue);
    if (thisValue >= getZeroDomainElementReal().getValue())
    {
      if(!elementToReturn)
      {
        elementToReturn = getDomainElementManager().createDomainElementReal(pDomainValue);
      }
      return elementToReturn;
    }
    
    auto domainValueToReturn = static_cast<REAL>(-1.0) * getValue();
    elementToReturn = getDomainElementManager().findDomainElementReal(domainValueToReturn);
    if(!elementToReturn)
    {
      elementToReturn = getDomainElementManager().createDomainElementReal(domainValueToReturn);
    }
    
    assert(elementToReturn);
    return elementToReturn;
  }//abs
  
  DomainElement* DomainElementReal::zero() const
  {
    auto elementToReturn = getDomainElementManager().findDomainElementReal(0.0);
    if(!elementToReturn)
    {
      elementToReturn = getDomainElementManager().createDomainElementReal(0.0);
    }
    
    assert(elementToReturn);
    return elementToReturn;
  }//mirror
  
  DomainElement* DomainElementReal::mirror() const
  {
    auto domainValueToReturn = static_cast<REAL>(-1.0) * getValue();
    auto elementToReturn = getDomainElementManager().findDomainElementReal(domainValueToReturn);
    if(!elementToReturn)
    {
      elementToReturn = getDomainElementManager().createDomainElementReal(domainValueToReturn);
    }
    
    assert(elementToReturn);
    return elementToReturn;
  }//mirror
  
  DomainElement* DomainElementReal::successor() const
  {
    if (getValue() >= getMaxDomainElementReal().getValue() - getRealStep())
    {
      return getDomainElementManager().createDomainElementReal(getMaxDomainElementReal().getValue());
    }
    
    auto domainValueToReturn = getValue() + getRealStep();
    DomainElement* elementToReturn = getDomainElementManager().findDomainElementReal(domainValueToReturn);
    if(!elementToReturn)
    {
      elementToReturn = getDomainElementManager().createDomainElementReal(domainValueToReturn);
    }
    
    assert(elementToReturn);
    return elementToReturn;
  }//successor
  
  DomainElement* DomainElementReal::predecessor() const
  {
    if (getValue() <= getMinDomainElementReal().getValue() + getRealStep())
    {
      return getDomainElementManager().createDomainElementReal(getMinDomainElementReal().getValue());
    }
    
    auto domainValueToReturn = getValue() - getRealStep();
    DomainElement* elementToReturn = getDomainElementManager().findDomainElementReal(domainValueToReturn);
    if(!elementToReturn)
    {
      elementToReturn = getDomainElementManager().createDomainElementReal(domainValueToReturn);
    }

    assert(elementToReturn);
    return elementToReturn;
  }//predecessor
  
  DomainElement* DomainElementReal::div(const DomainElement* aOther) const
  {
    assert(isa(aOther));
    
    if(DomainElementReal::cast(aOther)->getValue() == getZeroDomainElementReal().getValue())
    {
      return getDomainElementManager().getVoidElement();
    }
    
    auto domainValueToReturn = static_cast<REAL>(getValue() / cast(aOther)->getValue());
    
    DomainElement* elementToReturn = getDomainElementManager().findDomainElementReal(domainValueToReturn);
    if(!elementToReturn)
    {
      elementToReturn = getDomainElementManager().createDomainElementReal(domainValueToReturn);
    }
    
    assert(elementToReturn);
    return elementToReturn;
  }//div
  
  DomainElement* DomainElementReal::plus(const DomainElement* aOther) const
  {
    assert(isa(aOther));
    
    DomainElementReal *otherElement = cast(aOther);
    if(std::abs(getMaxDomainElementReal().getValue() - otherElement->getValue()) < std::abs(getValue()))
    {
      return getDomainElementManager().createDomainElementReal(getMaxDomainElementReal().getValue());
    }
    
    auto domainValueToReturn = static_cast<REAL>(getValue() + cast(aOther)->getValue());
    DomainElement* elementToReturn = getDomainElementManager().findDomainElementReal(domainValueToReturn);
    if(!elementToReturn)
    {
      elementToReturn = getDomainElementManager().createDomainElementReal(domainValueToReturn);
    }
    
    assert(elementToReturn);
    return elementToReturn;
  }//plus
  
  DomainElement* DomainElementReal::times(const DomainElement* aOther) const
  {
    assert(isa(aOther));
    
    DomainElementReal *otherElement = cast(aOther);
    if(DomainElement::isEqual(&getZeroDomainElementReal(), aOther))
    {
      DomainElement* elementToReturn = getDomainElementManager().findDomainElementReal(getZeroDomainElementReal().getValue());
      if(!elementToReturn)
      {
        elementToReturn = getDomainElementManager().createDomainElementReal(getZeroDomainElementReal().getValue());
      }
      assert(elementToReturn);
      return elementToReturn;
    }
    
    if(std::abs(getMaxDomainElementReal().getValue() / otherElement->getValue()) < std::abs(getValue()))
    {
      return getDomainElementManager().createDomainElementReal(getMaxDomainElementReal().getValue());
    }
    
    if(std::abs(getMinDomainElementReal().getValue() / otherElement->getValue()) < std::abs(getValue()))
    {
      return getDomainElementManager().createDomainElementReal(getMinDomainElementReal().getValue());
    }
    
    auto domainValueToReturn = static_cast<REAL>(getValue() * cast(aOther)->getValue());
    DomainElement* elementToReturn = getDomainElementManager().findDomainElementReal(domainValueToReturn);
    if(!elementToReturn)
    {
      elementToReturn = getDomainElementManager().createDomainElementReal(domainValueToReturn);
    }
    
    assert(elementToReturn);
    return elementToReturn;
  }//times
  
  std::size_t DomainElementReal::sizeSpan(const DomainElement* aOther) const
  {
    assert(isa(aOther));
    
    if(isEqual(aOther))
    {
      return 0;
    }
    
    DomainElementReal *otherElement = cast(aOther);
    
    auto thisValue = getValue();
    auto otherValue = otherElement->getValue();
    auto zeroValue = getZeroDomainElementReal().getValue();
    
    REAL spanElems;
    if ((thisValue <= zeroValue && otherValue <= zeroValue) ||
        (thisValue >= zeroValue && otherValue >= zeroValue))
    {
      spanElems = std::abs(std::abs(thisValue) - std::abs(otherValue)) + 1;
    }
    else
    {
      if(thisValue < 0)
      {
        spanElems = std::abs(thisValue) + otherValue + 1;
      }
      else
      {
        spanElems = std::abs(otherValue) + thisValue + 1;
      }
    }
    
    REAL bestPrec = std::min(getRealStep(), otherElement->getRealStep());
    REAL elemInUnit = 1.0/bestPrec - 1;
    
    if(getMaxDomainElementReal().getValue() / elemInUnit < spanElems)
    {
      return std::numeric_limits<std::size_t>::max();
    }
    
    if(static_cast<REAL>(std::numeric_limits<std::size_t>::max()) / elemInUnit < spanElems)
    {
      return std::numeric_limits<std::size_t>::max();
    }
    
    return static_cast<std::size_t>(spanElems * elemInUnit);
  }//DomainElementInt64
  
}// end namespace Core

