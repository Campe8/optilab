// Base class
#include "DomainElementPairContainer.hpp"

// Domain element
#include "DomainElementVoid.hpp"

// Decorator
#include "DomainDecorator.hpp"

// Domain state
#include "DomainStatePairContainer.hpp"

#include <algorithm>

#include <map>

namespace Core {
  
  DomainElementPairContainer::DomainElementPairContainer(DomainElement *aFirst, DomainElement *aSecond)
  : DomainElementContainer(DomainElementContainerClass::DOM_ELEM_CONTAINER_PAIR)
  {
    assert(aFirst);
    assert(aSecond);
    
    // Set <aFirst, aSecond> where aFirst < aSecond
    // to speedup internal operations
    if(aFirst->isLessThanOrEqual(aSecond))
    {
      pDomainElementPair.first  = aFirst;
      pDomainElementPair.second = aSecond;
    }
    else
    {
      pDomainElementPair.first  = aSecond;
      pDomainElementPair.second = aFirst;
    }
  }
  
  DomainElementPairContainer::DomainElementPairContainer(const DomainElementPairContainer& aOther)
  : DomainElementContainer(DomainElementContainerClass::DOM_ELEM_CONTAINER_PAIR)
  {
    pDomainElementPair = aOther.pDomainElementPair;
  }
  
  DomainElementPairContainer::DomainElementPairContainer(DomainElementPairContainer&& aOther)
  : DomainElementContainer(DomainElementContainerClass::DOM_ELEM_CONTAINER_PAIR)
  {
    pDomainElementPair = aOther.pDomainElementPair;
    aOther.emptyDomain();
  }
  
  DomainElementPairContainer::~DomainElementPairContainer()
  {
  }
  
  DomainElementPairContainer& DomainElementPairContainer::operator= (const DomainElementPairContainer& aOther)
  {
    if(this != &aOther)
    {
      DomainElementContainer::operator=(aOther);
      
      pDomainElementPair = aOther.pDomainElementPair;
    }
    return *this;
  }
  
  DomainElementPairContainer& DomainElementPairContainer::operator= (DomainElementPairContainer&& aOther)
  {
    if(this != &aOther)
    {
      DomainElementContainer::operator=(aOther);
      
      pDomainElementPair = aOther.pDomainElementPair;
      aOther.emptyDomain();
    }
    return *this;
  }
  
  std::unique_ptr<DomainState> DomainElementPairContainer::getDomainSnapshot()
  {
    std::unique_ptr<DomainStatePairContainer> domainState(new DomainStatePairContainer());
    DomainStatePairContainer::cast(domainState.get())->setInternalState(pDomainElementPair);
    return std::move(domainState);
  }//getDomainSnapshot
  
  void DomainElementPairContainer::reinstateInternalDomainSnapshot(const DomainState* aDomainSnapshot)
  {
    assert(aDomainSnapshot);
    assert(DomainStatePairContainer::isa(aDomainSnapshot));
    pDomainElementPair = DomainStatePairContainer::cast(aDomainSnapshot)->getInternalStateCopy();
  }//reinstateInternalDomainSnapshot
  
  void DomainElementPairContainer::emptyDomain()
  {
    pDomainElementPair.first = DomainElementManager::getInstance().getVoidElement();
    pDomainElementPair.second = DomainElementManager::getInstance().getVoidElement();
  }//emptyDomain
  
  std::size_t DomainElementPairContainer::getDomainSize() const
  {
    std::size_t size = DomainElementVoid::isa(getFirst()) ? 0 : 1;
    size += DomainElementVoid::isa(getSecond()) ? 0 : 1;
    return size;
  }//getDomainSize
  
  void DomainElementPairContainer::subtractElement(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    assert(!DomainElementVoid::isa(aDomainElement));
    
    if (getDomainSize() > 0)
    {
      // Subtract first matching element
      if(getFirst()->isEqual(aDomainElement))
      {
        pDomainElementPair.first = DomainElementManager::getInstance().getVoidElement();
      }
      // Subtract second matching element
      if(getSecond()->isEqual(aDomainElement))
      {
        pDomainElementPair.second = DomainElementManager::getInstance().getVoidElement();
      }
    }
  }//subtractElement
  
  void DomainElementPairContainer::shrinkOnBounds(DomainElement *aLowerBound, DomainElement *aUpperBound)
  {
    assert(aLowerBound);
    assert(aUpperBound);
    
    assert(!DomainElementVoid::isa(aLowerBound));
    assert(!DomainElementVoid::isa(aUpperBound));
    
    std::size_t dSize = getDomainSize();
    if (dSize == 0)
    {
      // Return if domain is already empty
      return;
    }
    
    if(aUpperBound->isLessThan(aLowerBound))
    {
      emptyDomain();
      return;
    }
    
    if(dSize == 1)
    {
      // Only 1 element in the pair
      DomainElement* singleElement{nullptr};
      if(!DomainElementVoid::isa(getFirst()))
      {
        assert(DomainElementVoid::isa(getSecond()));
        singleElement = getFirst();
      }
      else
      {
        assert(DomainElementVoid::isa(getFirst()));
        singleElement = getSecond();
      }
      assert(singleElement);
      
      if(aUpperBound->isLessThan(singleElement) ||
         singleElement->isLessThan(aLowerBound))
      {
        emptyDomain();
      }
      
      return;
    }
    assert(dSize == 2);
    
    //  Empty domin for empty intersection
    if(aUpperBound->isLessThan(getFirst()) || getSecond()->isLessThan(aLowerBound) ||
       (getFirst()->isLessThan(aLowerBound) && aUpperBound->isLessThan(getSecond())))
    {
      emptyDomain();
      return;
    }
    
    if(aLowerBound->isLessThanOrEqual(getFirst()) && getSecond()->isLessThanOrEqual(aUpperBound))
    {
      return;
    }
    
    if(aLowerBound->isLessThanOrEqual(getFirst()))
    {
      subtract(getSecond());
      return;
    }
    
    if(getSecond()->isLessThanOrEqual(aUpperBound))
    {
      subtract(getFirst());
      return;
    }
  }//shrinkOnBounds
  
  bool DomainElementPairContainer::contains(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    
    // Void element does not compare with other elements
    if(DomainElementVoid::isa(aDomainElement))
    {
      return false;
    }
    
    return (!DomainElementVoid::isa(getFirst()) && getFirst()->isEqual(aDomainElement)) ||
           (!DomainElementVoid::isa(getSecond()) && getSecond()->isEqual(aDomainElement));
  }//contains
  
  void DomainElementPairContainer::shrinkOnMinBound(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    assert(!DomainElementVoid::isa(aDomainElement));
    
    shrinkOnBounds(aDomainElement, getUpperBound());
  }//shrinkOnMinBound
  
  void DomainElementPairContainer::shrinkOnMaxBound(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    assert(!DomainElementVoid::isa(aDomainElement));
    
    shrinkOnBounds(getLowerBound(), aDomainElement);
  }//shrinkOnMaxBound
  
  void DomainElementPairContainer::intersectWithDomainElementContainer(DomainElementContainer *aDomainElementContainer)
  {
    assert(aDomainElementContainer);
    assert(DomainElementPairContainer::isa(aDomainElementContainer));
    
    DomainElementPairContainer otherContainer = *(DomainElementPairContainer::cast(aDomainElementContainer));
    
    // If other container is empty, empty this container
    if(otherContainer.getDomainSize() == 0)
    {
      emptyDomain();
      return;
    }
    
    // First must be in both containers in intersection
    if(!getFirst()->isEqual(otherContainer.getFirst()) && !getFirst()->isEqual(otherContainer.getSecond()))
    {
      subtract(getFirst());
    }
    
    // Second must be in both containers in intersection
    if(!getSecond()->isEqual(otherContainer.getFirst()) && !getSecond()->isEqual(otherContainer.getSecond()))
    {
      subtract(getSecond());
    }
    
    // Out of bounds for subtract: empty domain
    DomainElement * minDomElem = otherContainer.getLowerBound();
    DomainElement * maxDomElem = otherContainer.getUpperBound();
    if(getUpperBound()->isLessThan(minDomElem) ||
       maxDomElem->isLessThan(getLowerBound()))
    {
      emptyDomain();
      return;
    }
  }//intersectWithDomainElementContainer
  
  void DomainElementPairContainer::intersectWithArray(DomainElementArray *aDomainElementArray)
  {
    assert(aDomainElementArray);
    
    // If array is empty, intersection is empty
    if (aDomainElementArray->empty())
    {
      emptyDomain();
      return;
    }
    
    if(aDomainElementArray->find(getFirst()) == aDomainElementArray->size())
    {
      subtract(getFirst());
    }
    
    if(aDomainElementArray->find(getSecond()) == aDomainElementArray->size())
    {
      subtract(getSecond());
    }
  }//intersectWithArray
  
  void DomainElementPairContainer::subtractDomainElementContainer(DomainElementContainer *aDomainElementContainer)
  {
    assert(aDomainElementContainer);
    assert(DomainElementPairContainer::isa(aDomainElementContainer));
    
    DomainElementPairContainer otherContainer = *(DomainElementPairContainer::cast(aDomainElementContainer));
    
    // Return if nothing to subtract
    if(otherContainer.getDomainSize() == 0)
    {
      return;
    }
    
    if(!DomainElementVoid::isa(otherContainer.getFirst()))
    {
      subtract(otherContainer.getFirst());
    }
    
    if(!DomainElementVoid::isa(otherContainer.getSecond()))
    {
      subtract(otherContainer.getSecond());
    }
  }//subtractDomainElementContainer
  
  void DomainElementPairContainer::subtractArray(DomainElementArray *aDomainElementArray)
  {
    assert(aDomainElementArray);
    
    // Return if nothing to subtract
    if (aDomainElementArray->empty())
    {
      return;
    }
    
    if(aDomainElementArray->find(getFirst()) != aDomainElementArray->size())
    {
      subtract(getFirst());
    }
    
    if(aDomainElementArray->find(getSecond()) != aDomainElementArray->size())
    {
      subtract(getSecond());
    }
  }//subtractArray
  
  DomainElement *DomainElementPairContainer::getElementGreaterThan(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    
    // Void element does not compare with other elements
    if(DomainElementVoid::isa(aDomainElement))
    {
      return aDomainElement;
    }
    
    if(getDomainSize() == 0)
    {
      return (getDomainElementManager())->getVoidElement();
    }
    
    // If "aDomainElement" is upper bound, return void element
    if(!DomainElementVoid::isa(getSecond()) && DomainElement::lessThanOrEqual(getSecond(), aDomainElement))
    {
      return (getDomainElementManager())->getVoidElement();
    }
    
    if(DomainElementVoid::isa(getSecond()))
    {
      if(aDomainElement->isLessThan(getFirst()))
      {
        return getFirst();
      }
      
      return (getDomainElementManager())->getVoidElement();
    }
    else
    {
      if(DomainElementVoid::isa(getFirst()))
      {
        if(aDomainElement->isLessThan(getSecond()))
        {
          return getSecond();
        }
      }
      else
      {
        // Both non void
        if(aDomainElement->isLessThan(getFirst()))
        {
          return getFirst();
        }
        else if (aDomainElement->isLessThan(getSecond()))
        {
          return getSecond();
        }
      }
      return (getDomainElementManager())->getVoidElement();
    }
  }//getElementGreaterThan
  
  DomainElement *DomainElementPairContainer::getElementLessThan(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    
    // Void element does not compare with other elements
    if(DomainElementVoid::isa(aDomainElement))
    {
      return aDomainElement;
    }
    
    if(getDomainSize() == 0)
    {
      return (getDomainElementManager())->getVoidElement();
    }
    
    // If "aDomainElement" is lower bound, return void element
    if(!DomainElementVoid::isa(getFirst()) && DomainElement::lessThanOrEqual(aDomainElement, getFirst()))
    {
      return (getDomainElementManager())->getVoidElement();
    }
    
    if(DomainElementVoid::isa(getSecond()))
    {
      if(getFirst()->isLessThan(aDomainElement))
      {
        return getFirst();
      }
      
      return (getDomainElementManager())->getVoidElement();
    }
    else
    {
      if(DomainElementVoid::isa(getFirst()))
      {
        if(getSecond()->isLessThan(aDomainElement))
        {
          return getSecond();
        }
      }
      else
      {
        // Both non void
        if(getSecond()->isLessThan(aDomainElement))
        {
          return getSecond();
        }
        else if (getFirst()->isLessThan(aDomainElement))
        {
          return getFirst();
        }
      }
      return (getDomainElementManager())->getVoidElement();
    }
    
  }//getElementLessThan
  
  DomainElement *DomainElementPairContainer::getThElement(std::size_t aElementThIndex)
  {
    if(aElementThIndex == 0 || aElementThIndex > getDomainSize())
    {
      return getDomainElementManager()->getVoidElement();
    }
    
    if(aElementThIndex == 1)
    {
      return getFirst();
    }
    assert(aElementThIndex == 2);
    return getSecond();
  }//getThElement
  
  DomainElement *DomainElementPairContainer::getLowerBound()
  {
    if (getDomainSize() == 0)
    {
      return DomainElementManager::getInstance().getVoidElement();
    }
    
    if(!DomainElementVoid::isa(getFirst()))
    {
      return getFirst();
    }
    return getSecond();
  }//getLowerBound
  
  DomainElement *DomainElementPairContainer::getUpperBound()
  {
    if (getDomainSize() == 0)
    {
      return DomainElementManager::getInstance().getVoidElement();
    }
    
    if(!DomainElementVoid::isa(getSecond()))
    {
      return getSecond();
    }
    return getFirst();
  }//getUpperBound
  
}// end namespace Core
