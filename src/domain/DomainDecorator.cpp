// Base class
#include "DomainDecorator.hpp"

#include "DomainElementVoid.hpp"

#include <limits>
#include <cassert>

namespace Core {
  
  DomainDecorator::DomainDecorator(DomainElementContainer *aContainer)
  : pEventManager(new DomainEventManager())
  , pLowerBound(nullptr)
  , pUpperBound(nullptr)
  , pSize(0)
  {
    /*
     * @note registering the decorator will call update
     * as side effect, actually setting size
     * and lower and upper bound.
     */
    assert(aContainer);
    aContainer->registerDecorator(this);
    
    // Reset event if changed, since in instantiation
    // event should be void
    assert(pEvent);
    if(pEvent->getEventType() == DomainEventType::CHANGE_EVENT)
    {
      pEvent = pEventManager->getEvent(DomainEventType::VOID_EVENT);
    }
  }

  DomainDecorator::DomainDecorator(const DomainDecorator& aOther)
  {
    pEventManager.reset(new DomainEventManager());
    pEvent = aOther.pEvent;
    pLowerBound = aOther.pLowerBound;
    pUpperBound = aOther.pUpperBound;
    pSize = aOther.pSize;
  }
  
  DomainDecorator::DomainDecorator(DomainDecorator&& aOther)
  {
    pEventManager.reset(new DomainEventManager());
    pEvent = aOther.pEvent;
    pLowerBound = aOther.pLowerBound;
    pUpperBound = aOther.pUpperBound;
    pSize = aOther.pSize;
    
    aOther.pEventManager.reset(nullptr);
    aOther.pEvent = nullptr;
    aOther.pLowerBound = nullptr;
    aOther.pUpperBound = nullptr;
    aOther.pSize = 0;
  }
  
  DomainDecorator::~DomainDecorator()
  {
  }
  
  DomainDecorator& DomainDecorator::operator= (const DomainDecorator& aOther)
  {
    if(this != &aOther)
    {
      pEventManager.reset(new DomainEventManager());
      pEvent = aOther.pEvent;
      pLowerBound = aOther.pLowerBound;
      pUpperBound = aOther.pUpperBound;
      pSize = aOther.pSize;
    }
    return *this;
  }
  
  DomainDecorator& DomainDecorator::operator= (DomainDecorator&& aOther)
  {
    if(this != &aOther)
    {
      pEventManager.reset(new DomainEventManager());
      pEvent = aOther.pEvent;
      pLowerBound = aOther.pLowerBound;
      pUpperBound = aOther.pUpperBound;
      pSize = aOther.pSize;
      
      aOther.pEventManager.reset(nullptr);
      aOther.pEvent = nullptr;
      aOther.pLowerBound = nullptr;
      aOther.pUpperBound = nullptr;
      aOther.pSize = 0;
    }
    return *this;
  }
  
  void DomainDecorator::resetInternalStatus(std::size_t aDomainSize,
                                            DomainElement *aLowerBound,
                                            DomainElement *aUpperBound)
  {
    assert(aLowerBound);
    assert(aUpperBound);
    assert(!DomainElementVoid::isa(aLowerBound));
    assert(!DomainElementVoid::isa(aUpperBound));
    
    // Reset old lower and upper bounds
    pLowerBound = nullptr;
    pUpperBound = nullptr;
    
    if(aDomainSize == 0)
    {
      pEvent = pEventManager->getEvent(DomainEventType::FAIL_EVENT);
    }
    else if (aDomainSize == 1)
    {
      assert(aLowerBound->isEqual(aUpperBound));
      pEvent = pEventManager->getEvent(DomainEventType::SINGLETON_EVENT);
    }
    else if (aDomainSize > pSize)
    {
      pEvent = pEventManager->getEvent(DomainEventType::CHANGE_EVENT);
    }
    else
    {
      // Default void event on initialization
      pEvent = pEventManager->getEvent(DomainEventType::VOID_EVENT);
    }
  }//resetInternalStatus
  
  void DomainDecorator::update(std::size_t aDomainSize,
                               DomainElement *aLowerBound,
                               DomainElement *aUpperBound)
  {
    assert(pEventManager);
    
    // Given size is greater than current size,
    // reset internal status
    if(aDomainSize > pSize)
    {
      resetInternalStatus(aDomainSize, aLowerBound, aUpperBound);
    }
    
    // Update size
    std::size_t oldSize = pSize;
    pSize = aDomainSize;
    
    // @note Upon initialization both lower and upper bounds are nullptr
    if(pLowerBound == nullptr || pUpperBound == nullptr)
    {
      assert(!pLowerBound);
      assert(!pUpperBound);
      pLowerBound = aLowerBound;
      pUpperBound = aUpperBound;
      return;
    }
    
    // Check empty domain
    if (pSize == 0)
    {
      pEvent = pEventManager->getEvent(DomainEventType::FAIL_EVENT);
      pLowerBound = aLowerBound;
      pUpperBound = aUpperBound;
      return;
    }
    
    // If domain's size is not changed: nothing happened on the domain
    if (oldSize == pSize)
    {
      return;
    }
    
    // Check singleton
    if (pSize == 1)
    {
      assert(aLowerBound->isEqual(aUpperBound));
      
      pEvent = pEventManager->getEvent(DomainEventType::SINGLETON_EVENT);
      pLowerBound = aLowerBound;
      pUpperBound = aUpperBound;
      return;
    }
    
    // Check bound changes
    if (pLowerBound->isLessThan(aLowerBound))
    {
      if (aUpperBound->isLessThan(pUpperBound))
      {
        // Domain is shrink on both lower and upper bounds
        pEvent = pEventManager->getEvent(DomainEventType::BOUND_EVENT);
        pLowerBound = aLowerBound;
        pUpperBound = aUpperBound;
        return;
      }
      else
      {
        // Domain is shrink only on lower bound
        assert(aUpperBound->isEqual(pUpperBound));
        
        pEvent = pEventManager->getEvent(DomainEventType::LWB_EVENT);
        pLowerBound = aLowerBound;
        return;
      }
    }
    
    if (aUpperBound->isLessThan(pUpperBound))
    {
      // Domain is shrink only on upper bound
      assert(aLowerBound->isEqual(pLowerBound));
      
      pEvent = pEventManager->getEvent(DomainEventType::UPB_EVENT);
      pUpperBound = aUpperBound;
      return;
    }
    
    // Domain has one or more removed internal elements but bounds are still the same
    assert(pSize < oldSize);
    assert(aLowerBound->isEqual(pLowerBound));
    assert(aUpperBound->isEqual(pUpperBound));
    
    pEvent = pEventManager->getEvent(DomainEventType::CHANGE_EVENT);
  }//update
  
}// end namespace Core
