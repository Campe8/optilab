// Base class
#include "DomainEventLwb.hpp"

#include <cassert>

namespace Core {
  
  DomainEventLwb::DomainEventLwb()
  : DomainEvent(DomainEventType::LWB_EVENT)
  {
  }
  
  DomainEventLwb::~DomainEventLwb()
  {
  }
  
  bool DomainEventLwb::isa(DomainEvent *aEvent)
  {
    assert(aEvent);
    return aEvent->getEventType() == DomainEventType::LWB_EVENT;
  }//isa
  
  DomainEventLwb* DomainEventLwb::cast(DomainEvent *aEvent)
  {
    assert(aEvent);
    if (isa(aEvent))
    {
      return static_cast<DomainEventLwb*>(aEvent);
    }
    
    return nullptr;
  }//cast
  
  bool DomainEventLwb::overlaps(const DomainEvent *aDomainEvent) const
  {
    assert(aDomainEvent);
    
    auto event = aDomainEvent->getEventType();
    switch (event) {
      case DomainEventType::BOUND_EVENT:
        return true;
      case DomainEventType::CHANGE_EVENT:
        return true;
      case DomainEventType::LWB_EVENT:
        return true;
      case DomainEventType::UPB_EVENT:
        return false;
      case DomainEventType::SINGLETON_EVENT:
        return false;
      case DomainEventType::FAIL_EVENT:
        return false;
      case DomainEventType::VOID_EVENT:
        return false;
      default:
        assert(event == DomainEventType::UNDEF_EVENT);
        return false;
    }
  }//overlaps
  
}// end namespace Core
