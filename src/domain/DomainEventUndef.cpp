// Base class
#include "DomainEventUndef.hpp"

#include <cassert>

namespace Core {
  
  DomainEventUndef::DomainEventUndef()
  : DomainEvent(DomainEventType::UNDEF_EVENT)
  {
  }
  
  DomainEventUndef::~DomainEventUndef()
  {
  }
  
  bool DomainEventUndef::isa(DomainEvent *aEvent)
  {
    assert(aEvent);
    return aEvent->getEventType() == DomainEventType::UNDEF_EVENT;
  }//isa
  
  DomainEventUndef* DomainEventUndef::cast(DomainEvent *aEvent)
  {
    assert(aEvent);
    if (isa(aEvent))
    {
      return static_cast<DomainEventUndef*>(aEvent);
    }
    
    return nullptr;
  }//cast
  
  bool DomainEventUndef::overlaps(const DomainEvent *aDomainEvent) const
  {
    assert(aDomainEvent);
    auto event = aDomainEvent->getEventType();
    if(event == DomainEventType::UNDEF_EVENT)
    {
      return true;
    }
    return false;
  }//overlaps
  
}// end namespace Core
