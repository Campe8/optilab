// Base class
#include "DomainElementVoid.hpp"

// Domain element key
#include "DomainElementKey.hpp"

#include <iostream>
#include <limits>
#include <cassert>

namespace Core {

    DomainElementVoid::DomainElementVoid()
        : DomainElement(DomainElementType::DET_VOID)
    {
    }

    DomainElementVoid::~DomainElementVoid()
    {
    }

    bool DomainElementVoid::isa(const DomainElement* aDomainElement)
    {
        assert(aDomainElement);
        return aDomainElement->getType() == DomainElementType::DET_VOID;
    }//isa

    DomainElementVoid* DomainElementVoid::cast(DomainElement* aDomainElement)
    {
        assert(aDomainElement);

        if (!isa(aDomainElement))
        {
            return nullptr;
        }

        return static_cast<DomainElementVoid *>(aDomainElement);
    }//cast

}// end namespace Core
