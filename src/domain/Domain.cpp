// Base class
#include "Domain.hpp"

// Iterator
#include "DomainIterator.hpp"

// Constraint for notification
#include "Constraint.hpp"

// Backtrack Semantics
#include "MementoStateDomain.hpp"

#include <cassert>

namespace Core {
  
  Domain::Domain(DomainClass aDomainClass)
  : pDomainClass(aDomainClass)
  , pIterator(nullptr)
  , pTemporaryEvent(nullptr)
  {
  }
  
  Domain::Domain(const Domain& aOther)
  {
    pDomainClass = aOther.pDomainClass;
    pIterator.reset(new DomainIterator(*(aOther.pIterator.get())));
  }
  
  Domain::Domain(Domain&& aOther)
  {
    pDomainClass = aOther.pDomainClass;
    pIterator.reset(new DomainIterator(*(aOther.pIterator.get())));
    
    aOther.pDomainClass = DomainClass::DOM_UNDEF;
    aOther.pIterator.reset(nullptr);
  }
  
  Domain::~Domain()
  {
  }
  
  Domain& Domain::operator= (const Domain& aOther)
  {
    if(this != &aOther)
    {
      pDomainClass = aOther.pDomainClass;
      if(aOther.pIterator != nullptr)
      {
        pIterator.reset(new DomainIterator(*(aOther.pIterator.get())));
      }
    }
    return *this;
  }
  
  Domain& Domain::operator= (Domain&& aOther)
  {
    if(this != &aOther)
    {
      pDomainClass = aOther.pDomainClass;
      if(aOther.pIterator != nullptr)
      {
        pIterator.reset(new DomainIterator(*(aOther.pIterator.get())));
      }
      
      aOther.pDomainClass = DomainClass::DOM_UNDEF;
      aOther.pIterator.reset(nullptr);
    }
    return *this;
  }
  
  DomainIterator* const Domain::getIterator()
  {
    if(pIterator == nullptr)
    {
      pIterator.reset(new DomainIterator(getDomainContainer()));
    }
    
    return pIterator.get();
  }
  
  std::size_t Domain::getSize() const
  {
    return getDomainDecorator()->getDomainSize();
  }//getSize
  
  DomainEvent* Domain::getEvent()
  {
    return getDomainDecorator()->getEvent();
  }//getEvent
  
  std::unique_ptr<Search::MementoState> Domain::snapshot()
  {
    return std::unique_ptr<Search::MementoStateDomain>(new Search::MementoStateDomain(getDomainContainer()->getDomainSnapshot()));
  }//snapshot
  
  void Domain::reinstateSnapshot(const Search::MementoState* aSnapshot)
  {
    assert(aSnapshot);
    assert(Search::MementoStateDomain::isa(aSnapshot));
    
    getDomainContainer()->reinstateDomainSnapshot(Search::MementoStateDomain::cast(aSnapshot)->getDomainState());
  }//reinstateSnapshot
  
  void Domain::triggerActionBasedOnEvent(Semantics::ConstraintObserver* aConstraintObserver, const Core::ConstraintObserverEvent* aConstraintEvent)
  {
    assert(aConstraintObserver);
    assert(aConstraintEvent);
    
    // If the given event does not require notification, return
    if (!aConstraintObserver->requiresNotification(aConstraintEvent))
    {
      return;
    }
    
    // For optimization reasons, update/notify
    // observers only on changed event
    switch (aConstraintEvent->constraintPropagationStatus())
    {
      case ConstraintObserverEvent::ConstraintObserverPropagationStatus::CON_EVT_PROPAGATION_INIT:
      {
        // Save current event pre-propagation
        pTemporaryEvent = getEvent();
      }
        break;
      default:
      {
        assert(aConstraintEvent->constraintPropagationStatus() == ConstraintObserverEvent::ConstraintObserverPropagationStatus::CON_EVT_PROPAGATION_TERM);
        assert(pTemporaryEvent);
        
        // On termination notify all the observers if
        // the event changed
        DomainEvent* currentEvent = getEvent();
        if(*currentEvent != *pTemporaryEvent)
        {
          aConstraintObserver->updateOnDomainEvent(currentEvent);
        }
      }
        break;
    }
  }//triggerActionBasedOnEvent
  
  bool Domain::contains(Core::DomainElement *aElement)
  {
    assert(aElement);
    return getDomainContainer()->contains(aElement);
  }//contains
  
  DomainElement* Domain::lowerBound()
  {
    return getDomainDecorator()->getLowerBound();
  }//lowerBound
  
  DomainElement* Domain::upperBound()
  {
    return getDomainDecorator()->getUpperBound();
  }//lowerBound
  
  void Domain::shrink(DomainElement* aLowerBound, DomainElement* aUpperBound)
  {
    assert(aLowerBound);
    assert(aUpperBound);
    getDomainContainer()->shrink(aLowerBound, aUpperBound);
    notifyChange();
  }//shrink
  
  void Domain::subtract(Core::DomainElement *aElement)
  {
    assert(aElement);
    getDomainContainer()->subtract(aElement);
    notifyChange();
  }//subtract
  
  void Domain::shrinkOnLowerBound(Core::DomainElement *aElement)
  {
    assert(aElement);
    getDomainContainer()->inMin(aElement);
    notifyChange();
  }//shrinkOnLowerBound
  
  void Domain::shrinkOnUpperBound(Core::DomainElement *aElement)
  {
    assert(aElement);
    getDomainContainer()->inMax(aElement);
    notifyChange();
  }//shrinkOnUpperBound
  
  void Domain::intersect(Domain *aDomain)
  {
    assert(aDomain);
    assert(pDomainClass == aDomain->pDomainClass);
    
    getDomainContainer()->intersect(aDomain->getDomainContainer());
    notifyChange();
  }//intersect
  
  void Domain::intersect(DomainElementArray *aDomainElementArray)
  {
    assert(aDomainElementArray);
    getDomainContainer()->intersect(aDomainElementArray);
    notifyChange();
  }//intersect
  
  void Domain::subtract(Domain *aDomain)
  {
    assert(aDomain);
    assert(pDomainClass == aDomain->pDomainClass);
    
    getDomainContainer()->subtract(aDomain->getDomainContainer());
    notifyChange();
  }//subtract
  
  void Domain::subtract(DomainElementArray *aDomainElementArray)
  {
    assert(aDomainElementArray);
    
    getDomainContainer()->subtract(aDomainElementArray);
    notifyChange();
  }//subtract
  
  void Domain::notifyChange()
  {
    assert(getEvent());
    
    auto domainEvent = getEvent()->getEventType();
    assert(domainEvent != DomainEventType::UNDEF_EVENT);
    assert(domainEvent != DomainEventType::NUM_EVENTS);
    if (domainEvent != DomainEventType::FAIL_EVENT && domainEvent != DomainEventType::VOID_EVENT)
    {
      notifyBacktrackManager();
    }
  }//notifyChange
  
}// end namespace Core
