// Base class
#include "DomainElementInt64.hpp"
#include "DomainElementVoid.hpp"

// Domain element key
#include "DomainElementKey.hpp"

// Domain element manager
#include "DomainElementManager.hpp"

// Utilities
#include "DomainUtils.hpp"

#include <iostream>
#include <stdlib.h>
#include <limits>
#include <cassert>

namespace Core {
  
  DomainElementInt64::DomainElementInt64()
  : DomainElement(DomainElementType::DET_INT_64)
  , pDomainKey(nullptr)
  {
    pDomainValue = std::numeric_limits<INT_64>::lowest();
  }
  
  DomainElementInt64::DomainElementInt64(INT_64 aVal)
  : DomainElementInt64()
  {
    pDomainValue = aVal;
    pDomainKey.reset(new DomainElementKey(aVal, DomainElementType::DET_INT_64));
  }
  
  DomainElementInt64::DomainElementInt64(const DomainElementInt64& aOther)
  : DomainElement(aOther)
  {
    pDomainValue = aOther.pDomainValue;
    pDomainKey.reset(new DomainElementKey(*(aOther.pDomainKey.get())));
  }
  
  DomainElementInt64::DomainElementInt64(DomainElementInt64&& aOther)
  : DomainElement(aOther)
  {
    pDomainValue = aOther.pDomainValue;
    pDomainKey.reset(new DomainElementKey(*(aOther.pDomainKey.get())));
    
    aOther.pDomainKey.reset(nullptr);
  }
  
  DomainElementInt64& DomainElementInt64::operator= (const DomainElementInt64& aOther)
  {
    if (&aOther == this)
    {
      return *this;
    }
    
    // Base class assignment operator
    DomainElement::operator=(aOther);
    
    pDomainValue = aOther.pDomainValue;
    pDomainKey.reset(new DomainElementKey(*(aOther.pDomainKey.get())));
    
    return *this;
  }
  
  DomainElementInt64& DomainElementInt64::operator= (DomainElementInt64&& aOther)
  {
    if (&aOther == this)
    {
      return *this;
    }
    
    // Base class assignment operator
    DomainElement::operator=(aOther);
    
    pDomainValue = aOther.pDomainValue;
    pDomainKey.reset(new DomainElementKey(*(aOther.pDomainKey.get())));
    
    aOther.pDomainKey.reset(nullptr);
    
    return *this;
  }
  
  DomainElementInt64::~DomainElementInt64()
  {
  }
  
  bool DomainElementInt64::isa(const DomainElement* aDomainElement)
  {
    assert(aDomainElement);
    return aDomainElement->getType() == DomainElementType::DET_INT_64;
  }//isa
  
  DomainElementInt64* DomainElementInt64::cast(DomainElement* aDomainElement)
  {
    assert(aDomainElement);
    
    if (!isa(aDomainElement))
    {
      return nullptr;
    }
    
    return static_cast<DomainElementInt64 *>(aDomainElement);
  }//cast
  
  DomainElementInt64* DomainElementInt64::cast(const DomainElement* aDomainElement)
  {
    assert(aDomainElement);
    
    if (!isa(aDomainElement))
    {
      return nullptr;
    }
    
    return static_cast<DomainElementInt64 *>(const_cast<DomainElement*>(aDomainElement));
  }//cast
  
  bool DomainElementInt64::isEqual(const DomainElement* aOther) const
  {
    assert(aOther);
    if (DomainElementVoid::isa(aOther))
    {
      return false;
    }
    
    assert(isa(aOther));
    
    if (!isa(aOther))
    {
      return false;
    }
    
    return pDomainValue == cast(aOther)->getValue();
  }//isEqual
  
  bool DomainElementInt64::isNotEqual(const DomainElement* aOther) const
  {
    assert(aOther);
    
    if (DomainElementVoid::isa(aOther))
    {
      return true;
    }
    
    assert(isa(aOther));
    
    if (!isa(aOther))
    {
      return false;
    }
    
    return pDomainValue != cast(aOther)->getValue();
  }//isEqual
  
  bool DomainElementInt64::isLessThan(const DomainElement* aOther) const
  {
    if (!isa(aOther))
    {
      return false;
    }
    
    return getValue() < cast(aOther)->getValue();
  }//isLessThan
  
  bool DomainElementInt64::isLessThanOrEqual(const DomainElement* aOther) const
  {
    if (!isa(aOther))
    {
      return false;
    }
    
    return getValue() <= cast(aOther)->getValue();
  }//isLessThanOrEqual
  
  DomainElement* DomainElementInt64::min(const DomainElement* aOther) const
  {
    assert(isa(aOther));
    
    auto thisValue = getValue();
    auto elementToReturn = getDomainElementManager().findDomainElementInt64(thisValue);
    if (thisValue <= cast(aOther)->getValue())
    {
      if(!elementToReturn)
      {
        elementToReturn = getDomainElementManager().createDomainElementInt64(thisValue);
      }
      return elementToReturn;
    }
    
    elementToReturn = getDomainElementManager().findDomainElementInt64(cast(aOther)->getValue());
    if(!elementToReturn)
    {
      elementToReturn = getDomainElementManager().createDomainElementInt64(cast(aOther)->getValue());
    }
    
    assert(elementToReturn);
    return elementToReturn;
  }//min
  
  DomainElement* DomainElementInt64::max(const DomainElement* aOther) const
  {
    assert(isa(aOther));
    
    auto thisValue = getValue();
    auto elementToReturn = getDomainElementManager().findDomainElementInt64(thisValue);
    if (thisValue >= cast(aOther)->getValue())
    {
      if(!elementToReturn)
      {
        elementToReturn = getDomainElementManager().createDomainElementInt64(thisValue);
      }
      return elementToReturn;
    }
    
    elementToReturn = getDomainElementManager().findDomainElementInt64(cast(aOther)->getValue());
    if(!elementToReturn)
    {
      elementToReturn = getDomainElementManager().createDomainElementInt64(cast(aOther)->getValue());
    }
    
    assert(elementToReturn);
    return elementToReturn;
  }//max
  
  DomainElement* DomainElementInt64::zero() const
  {
    auto elementToReturn = getDomainElementManager().findDomainElementInt64(0);
    if(!elementToReturn)
    {
      elementToReturn = getDomainElementManager().createDomainElementInt64(0);
    }
    
    assert(elementToReturn);
    return elementToReturn;
  }//zero
  
  DomainElement* DomainElementInt64::mirror() const
  {
    auto thisValue = getValue();
    auto domainValueToReturn = static_cast<INT_64>(-1) * thisValue;
    
    auto elementToReturn = getDomainElementManager().findDomainElementInt64(domainValueToReturn);
    if(!elementToReturn)
    {
      elementToReturn = getDomainElementManager().createDomainElementInt64(domainValueToReturn);
    }
    
    assert(elementToReturn);
    return elementToReturn;
  }//mirror
  
  DomainElement* DomainElementInt64::abs() const
  {
    auto thisValue = getValue();
    auto elementToReturn = getDomainElementManager().findDomainElementInt64(thisValue);
    if (thisValue >= getZeroDomainElementInt64().getValue())
    {
      if(!elementToReturn)
      {
        elementToReturn = getDomainElementManager().createDomainElementInt64(thisValue);
      }
      return elementToReturn;
    }
    
    auto domainValueToReturn = static_cast<INT_64>(-1) * getValue();
    elementToReturn = getDomainElementManager().findDomainElementInt64(domainValueToReturn);
    if(!elementToReturn)
    {
      elementToReturn = getDomainElementManager().createDomainElementInt64(domainValueToReturn);
    }
    
    assert(elementToReturn);
    return elementToReturn;
  }//abs
  
  DomainElement* DomainElementInt64::successor() const
  {
    auto thisValue = getValue();
    if (thisValue >= getMaxDomainElementInt64().getValue())
    {
      return getDomainElementManager().createDomainElementInt64(getMaxDomainElementInt64().getValue());
    }
    
    auto domainValueToReturn = static_cast<INT_64>(1) + thisValue;
    DomainElement* elementToReturn = getDomainElementManager().findDomainElementInt64(domainValueToReturn);
    if(!elementToReturn)
    {
      elementToReturn = getDomainElementManager().createDomainElementInt64(domainValueToReturn);
    }
    
    assert(elementToReturn);
    return elementToReturn;
  }//successor
  
  DomainElement* DomainElementInt64::predecessor() const
  {
    auto thisValue = getValue();
    if (thisValue <= getMinDomainElementInt64().getValue())
    {
      return getDomainElementManager().createDomainElementInt64(getMinDomainElementInt64().getValue());
    }
    
    auto domainValueToReturn = static_cast<INT_64>(-1) + thisValue;
    DomainElement* elementToReturn = getDomainElementManager().findDomainElementInt64(domainValueToReturn);
    if(!elementToReturn)
    {
      elementToReturn = getDomainElementManager().createDomainElementInt64(domainValueToReturn);
    }

    assert(elementToReturn);
    return elementToReturn;
  }//predecessor
  
  DomainElement* DomainElementInt64::div(const DomainElement* aOther) const
  {
    assert(isa(aOther));
    
    if(DomainElementInt64::cast(aOther)->getValue() == getZeroDomainElementInt64().getValue())
    {
      return getDomainElementManager().getVoidElement();
    }
    
    INT_64 domainValueToReturn;
    RoundingMode rm = getRoundingMode();
    
    INT_64 dividend = static_cast<INT_64>(getValue());
    INT_64 divisor = cast(aOther)->getValue();
    if (rm == RoundingMode::DE_ROUNDING_UNSPEC)
    {
      domainValueToReturn = dividend / divisor;
    }
    else if (rm == RoundingMode::DE_ROUNDING_CEILING)
    {
      if(dividend >= 0 && divisor >= 0)
      {
        domainValueToReturn = DomainUtils::ceilDivPP(dividend, divisor);
      }
      else if (dividend >= 0)
      {
        domainValueToReturn = DomainUtils::ceilDivPX(dividend, divisor);
      }
      else if (divisor >= 0)
      {
        domainValueToReturn = DomainUtils::ceilDivXP(dividend, divisor);
      }
      else
      {
        domainValueToReturn = DomainUtils::ceilDivXX(dividend, divisor);
      }
    }
    else
    {
      assert(rm == RoundingMode::DE_ROUNDING_FLOOR);
      if(dividend >= 0 && divisor >= 0)
      {
        domainValueToReturn = DomainUtils::floorDivPP(dividend, divisor);
      }
      else if (dividend >= 0)
      {
        domainValueToReturn = DomainUtils::floorDivPX(dividend, divisor);
      }
      else if (divisor >= 0)
      {
        domainValueToReturn = DomainUtils::floorDivXP(dividend, divisor);
      }
      else
      {
        domainValueToReturn = DomainUtils::floorDivXX(dividend, divisor);
      }
    }
    
    DomainElement* elementToReturn = getDomainElementManager().findDomainElementInt64(domainValueToReturn);
    if(!elementToReturn)
    {
      elementToReturn = getDomainElementManager().createDomainElementInt64(domainValueToReturn);
    }
    
    assert(elementToReturn);
    return elementToReturn;
  }//div
  
  DomainElement* DomainElementInt64::plus(const DomainElement* aOther) const
  {
    assert(isa(aOther));
    
    INT_64 thisValue = getValue();
    INT_64 otherValue = cast(aOther)->getValue();
    
    INT_64 toReturn{};
    if(thisValue == Limits::int64Min() && otherValue <= 0)
    {
      toReturn = Limits::int64Min();
    }
    else if (thisValue == Limits::int64Max() && otherValue >= 0)
    {
      toReturn = Limits::int64Max();
    }
    else
    {
      toReturn = thisValue + otherValue;
    }
    
    DomainElement *elementToReturn = getDomainElementManager().findDomainElementInt64(toReturn);
    if(!elementToReturn)
    {
      elementToReturn = getDomainElementManager().createDomainElementInt64(toReturn);
    }
    
    assert(elementToReturn);
    return elementToReturn;
  }//plus
  
  DomainElement* DomainElementInt64::times(const DomainElement* aOther) const
  {
    assert(isa(aOther));
    
    REAL toReturn = static_cast<REAL>(getValue()) * cast(aOther)->getValue();
    DomainElement* elementToReturn = getElementOverSaturation(toReturn);
    
    assert(elementToReturn);
    return elementToReturn;
  }//times
  
  DomainElement *DomainElementInt64::getElementOverSaturation(REAL aVal) const
  {
    DomainElement* elementToReturn{nullptr};
    if(aVal >= Limits::int64Max())
    {
      // Saturate on int64Max
      elementToReturn = getDomainElementManager().findDomainElementInt64(getMaxDomainElementInt64().getValue());
      if(!elementToReturn)
      {
        elementToReturn = getDomainElementManager().createDomainElementInt64(getMaxDomainElementInt64().getValue());
      }
    }
    else if (aVal <= Limits::int64Min())
    {
      // Saturate on int64Min
      elementToReturn = getDomainElementManager().findDomainElementInt64(getMinDomainElementInt64().getValue());
      if(!elementToReturn)
      {
        elementToReturn = getDomainElementManager().createDomainElementInt64(getMinDomainElementInt64().getValue());
      }
    }
    else
    {
      INT_64 valueToRetun = static_cast<INT_64>(aVal);
      elementToReturn = getDomainElementManager().findDomainElementInt64(valueToRetun);
      if(!elementToReturn)
      {
        elementToReturn = getDomainElementManager().createDomainElementInt64(valueToRetun);
      }
    }
    
    assert(elementToReturn);
    return elementToReturn;
  }//getElementOverSaturation
  
  std::size_t DomainElementInt64::sizeSpan(const DomainElement* aOther) const
  {
    assert(isa(aOther));
    
    auto thisValue = getValue();
    auto otherValue = DomainElementInt64::cast(aOther)->getValue();
    auto zeroValue = getZeroDomainElementInt64().getValue();
    
    if ((thisValue <= zeroValue && otherValue <= zeroValue) ||
        (thisValue >= zeroValue && otherValue >= zeroValue))
    {
      return static_cast<std::size_t>(std::abs(std::abs(thisValue) - std::abs(otherValue)) + 1);
    }
    else
    {
      if(thisValue < 0)
      {
        return static_cast<std::size_t>((std::abs(thisValue) + otherValue + 1));
      }
      else
      {
        return static_cast<std::size_t>((std::abs(otherValue) + thisValue + 1));
      }
    }
  }//DomainElementInt64
  
}// end namespace Core

