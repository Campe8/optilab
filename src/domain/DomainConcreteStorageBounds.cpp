// Base class
#include "DomainConcreteStorageBounds.hpp"

#include "DomainElementVoid.hpp"
#include "DomainElementInt64.hpp"

#include <stdlib.h>

namespace Core {
  
  DomainConcreteStorageBounds::DomainConcreteStorageBounds(DomainElement *aLowerBound,
                                                           DomainElement *aUpperBound,
                                                           DomainElementManager *aDomainElementManager)
  : DomainConcreteStorage(DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_BOUNDS, aDomainElementManager)
  {
    assert(aLowerBound);
    assert(aUpperBound);
    
    pLowerBound = aLowerBound;
    pUpperBound = aUpperBound;
    
    setSize();
  }
  
  DomainConcreteStorageBounds::~DomainConcreteStorageBounds()
  {
  }
  
  DomainElement *DomainConcreteStorageBounds::getNextElement(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    assert(!DomainElementVoid::isa(aDomainElement));
    assert(getLowerBound()->isLessThanOrEqual(aDomainElement));
    
    auto upperBoundElement = getUpperBound();
    if(upperBoundElement->isEqual(aDomainElement))
    {
      return upperBoundElement;
    }
    
    return aDomainElement->successor();
  }//getNextElement
  
  DomainElement *DomainConcreteStorageBounds::getPrevElement(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    assert(!DomainElementVoid::isa(aDomainElement));
    assert(aDomainElement->isLessThanOrEqual(getUpperBound()));
    
    auto lowerBoundElement = getLowerBound();
    if(lowerBoundElement->isEqual(aDomainElement))
    {
      return lowerBoundElement;
    }
    
    return aDomainElement->predecessor();
  }//getPrevElement
  
  DomainElement *DomainConcreteStorageBounds::getElementByIndex(std::size_t aElementIndex)
  {
    assert(aElementIndex < getSize());
    
    if(aElementIndex == 0)
    {
      return pLowerBound;
    }
    
    if(aElementIndex == getSize() - 1)
    {
      return pUpperBound;
    }
    
    auto elementToReturn = pLowerBound;
    for (std::size_t ctr = 0; ctr < aElementIndex; ++ctr)
    {
      elementToReturn = elementToReturn->successor();
      //val = DomainElementKey::getKeyValSuccessor(val);
    }
    
    return elementToReturn;
  }//getElementByIndex
  
  void DomainConcreteStorageBounds::shrink(DomainElement *aLowerBound, DomainElement *aUpperBound)
  {
    assert(aLowerBound);
    assert(aUpperBound);
    
    // If aLowerBound < lowerBound and aUpperBound > upperBound, return
    if (aLowerBound->isLessThanOrEqual(pLowerBound) && pUpperBound->isLessThanOrEqual(aUpperBound))
    {
      return;
    }
    
    // If aLowerBound > aUpperBound, empty domain
    if (aUpperBound->isLessThan(aLowerBound))
    {
      emptyDomain();
      return;
    }
    // If aUpperBound < lowerBound, empty domain
    if (aUpperBound->isLessThan(pLowerBound))
    {
      emptyDomain();
      return;
    }
    // If aLowerBound > upperBound, empty domain
    if (pUpperBound->isLessThan(aLowerBound))
    {
      emptyDomain();
      return;
    }
    
    if (pLowerBound->isLessThan(aLowerBound))
    {
      pLowerBound = aLowerBound;
    }
    
    if (aUpperBound->isLessThan(pUpperBound))
    {
      pUpperBound = aUpperBound;
    }
    
    setSize();
  }//shrink
  
  void DomainConcreteStorageBounds::subtract(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    
    // Subtract elements only at the bounds
    if (aDomainElement->isNotEqual(pLowerBound) && aDomainElement->isNotEqual(pUpperBound))
    {
      return;
    }
    
    // Empty domain if removing the last element in the domain
    if (aDomainElement->isEqual(pLowerBound) && aDomainElement->isEqual(pUpperBound))
    {
      emptyDomain();
      return;
    }
    
    if (aDomainElement->isEqual(pLowerBound))
    {
      pLowerBound = pLowerBound->successor();
      setSize();
      
      return;
    }
    
    if (aDomainElement->isEqual(pUpperBound))
    {
      pUpperBound = pUpperBound->predecessor();
      setSize();
    }
  }//subtract
  
  void DomainConcreteStorageBounds::inMin(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    
    // if aDomainElement <= lowerBound return
    if (aDomainElement->isLessThanOrEqual(pLowerBound))
    {
      return;
    }
    
    // if aDomainElement > upperBound, empty domain
    if (pUpperBound->isLessThan(aDomainElement))
    {
      emptyDomain();
      return;
    }
    
    // Update with new lower bound and size
    if (aDomainElement->isEqual(pUpperBound))
    {
      pLowerBound = pUpperBound;
    }
    else
    {
      auto newLowerBound = getDomainElementManager().find(aDomainElement->getDomainKey());
      if(newLowerBound)
      {
        pLowerBound = newLowerBound;
      }
      else
      {
        pLowerBound = getDomainElementManager().clone(aDomainElement);
      }
    }
    
    setSize();
  }//inMin
  
  void DomainConcreteStorageBounds::inMax(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    
    // if aDomainElement <= lowerBound return
    if (pUpperBound->isLessThanOrEqual(aDomainElement))
    {
      return;
    }
    
    // if aDomainElement > upperBound, empty domain
    if (aDomainElement->isLessThan(pLowerBound))
    {
      emptyDomain();
      return;
    }
    
    // Update with new upper bound and size
    if (aDomainElement->isEqual(pLowerBound))
    {
      pUpperBound = pLowerBound;
    }
    else
    {
      auto newUpperBound = getDomainElementManager().find(aDomainElement->getDomainKey());
      if(newUpperBound)
      {
        pUpperBound = newUpperBound;
      }
      else
      {
        pUpperBound = getDomainElementManager().clone(aDomainElement);
      }
    }
    
    setSize();
  }//inMax
  
  bool DomainConcreteStorageBounds::contains(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    return pLowerBound->isLessThanOrEqual(aDomainElement) && aDomainElement->isLessThanOrEqual(pUpperBound);
  }//contains
  
  void DomainConcreteStorageBounds::emptyDomain()
  {
    pDomainSize = 0;
  }//emptyDomain
  
  void DomainConcreteStorageBounds::setSize()
  {
    assert(pLowerBound);
    assert(pUpperBound);
    
    if(DomainElement::lessThan(pUpperBound, pLowerBound))
    {
      pDomainSize = 0;
      return;
    }
    
    pDomainSize = pUpperBound->sizeSpan(pLowerBound);
  }//setSize
  
}// end namespace Core
