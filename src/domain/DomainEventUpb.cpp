// Base class
#include "DomainEventUpb.hpp"

#include <cassert>

namespace Core {
  
  DomainEventUpb::DomainEventUpb()
  : DomainEvent(DomainEventType::UPB_EVENT)
  {
  }
  
  DomainEventUpb::~DomainEventUpb()
  {
  }
  
  bool DomainEventUpb::isa(DomainEvent *aEvent)
  {
    assert(aEvent);
    return aEvent->getEventType() == DomainEventType::UPB_EVENT;
  }//isa
  
  DomainEventUpb* DomainEventUpb::cast(DomainEvent *aEvent)
  {
    assert(aEvent);
    if (isa(aEvent))
    {
      return static_cast<DomainEventUpb*>(aEvent);
    }
    
    return nullptr;
  }//cast
  
  bool DomainEventUpb::overlaps(const DomainEvent *aDomainEvent) const
  {
    assert(aDomainEvent);
    
    auto event = aDomainEvent->getEventType();
    switch (event) {
      case DomainEventType::BOUND_EVENT:
        return true;
      case DomainEventType::CHANGE_EVENT:
        return true;
      case DomainEventType::LWB_EVENT:
        return false;
      case DomainEventType::UPB_EVENT:
        return true;
      case DomainEventType::SINGLETON_EVENT:
        return false;
      case DomainEventType::FAIL_EVENT:
        return false;
      case DomainEventType::VOID_EVENT:
        return false;
      default:
        assert(event == DomainEventType::UNDEF_EVENT);
        return false;
    }
  }//overlaps
  
}// end namespace Core
