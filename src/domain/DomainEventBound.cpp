// Base class
#include "DomainEventBound.hpp"

#include <cassert>

namespace Core {
  
  DomainEventBound::DomainEventBound()
  : DomainEvent(DomainEventType::BOUND_EVENT)
  {
  }
  
  DomainEventBound::~DomainEventBound()
  {
  }
  
  bool DomainEventBound::isa(DomainEvent *aEvent)
  {
    assert(aEvent);
    return aEvent->getEventType() == DomainEventType::BOUND_EVENT;
  }//isa
  
  DomainEventBound* DomainEventBound::cast(DomainEvent *aEvent)
  {
    assert(aEvent);
    if (isa(aEvent))
    {
      return static_cast<DomainEventBound*>(aEvent);
    }
    
    return nullptr;
  }//cast
  
  bool DomainEventBound::overlaps(const DomainEvent *aDomainEvent) const
  {
    assert(aDomainEvent);
    
    auto event = aDomainEvent->getEventType();
    switch (event) {
      case DomainEventType::BOUND_EVENT:
        return true;
      case DomainEventType::CHANGE_EVENT:
        return true;
      case DomainEventType::LWB_EVENT:
        return false; // not enough info
      case DomainEventType::UPB_EVENT:
        return false; // not enough info
      case DomainEventType::SINGLETON_EVENT:
        return false; // not enough info
      case DomainEventType::FAIL_EVENT:
        return false;
      case DomainEventType::VOID_EVENT:
        return false;
      default:
        assert(event == DomainEventType::UNDEF_EVENT);
        return false;
    }
  }//overlaps
  
}// end namespace Core
