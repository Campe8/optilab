// Base class
#include "DomainEventFail.hpp"

#include <cassert>

namespace Core {
  
  DomainEventFail::DomainEventFail()
  : DomainEvent(DomainEventType::FAIL_EVENT)
  {
  }
  
  DomainEventFail::~DomainEventFail()
  {
  }
  
  bool DomainEventFail::isa(DomainEvent *aEvent)
  {
    assert(aEvent);
    return aEvent->getEventType() == DomainEventType::FAIL_EVENT;
  }//isa
  
  DomainEventFail* DomainEventFail::cast(DomainEvent *aEvent)
  {
    assert(aEvent);
    if (isa(aEvent))
    {
      return static_cast<DomainEventFail*>(aEvent);
    }
    
    return nullptr;
  }//cast
  
  bool DomainEventFail::overlaps(const DomainEvent *aDomainEvent) const
  {
    assert(aDomainEvent);
    
    auto event = aDomainEvent->getEventType();
    switch (event) {
      case DomainEventType::BOUND_EVENT:
        return false;
      case DomainEventType::CHANGE_EVENT:
        return false;
      case DomainEventType::LWB_EVENT:
        return false;
      case DomainEventType::UPB_EVENT:
        return false;
      case DomainEventType::SINGLETON_EVENT:
        return false;
      case DomainEventType::FAIL_EVENT:
        return true;
      case DomainEventType::VOID_EVENT:
        return false;
      default:
        assert(event == DomainEventType::UNDEF_EVENT);
        return false;
    }
  }//overlaps
  
}// end namespace Core
