#include "DomainElementKey.hpp"

#include <limits>

namespace Core {
  
  DomainElementKey::DomainElementKey(INT_64 aKeyVal, DomainElementType aDomainElementType)
  : pKeyValInt64(aKeyVal)
  , pKeyValReal(std::numeric_limits<REAL>::min())
  , pDomainElementType(aDomainElementType)
  {
  }
  
  DomainElementKey::DomainElementKey(REAL aKeyVal, DomainElementType aDomainElementType)
  : pKeyValInt64(std::numeric_limits<INT_64>::min())
  , pKeyValReal(aKeyVal)
  , pDomainElementType(aDomainElementType)
  {
  }
  
  DomainElementKey::~DomainElementKey()
  {
  }
  
  DomainElementKey::DomainElementKey(const DomainElementKey& aOther)
  {
    pKeyValInt64 = aOther.pKeyValInt64;
    pKeyValReal = aOther.pKeyValReal;
    pDomainElementType = aOther.pDomainElementType;
  }
  
  DomainElementKey::DomainElementKey(DomainElementKey&& aOther)
  {
    pKeyValInt64 = aOther.pKeyValInt64;
    pKeyValReal = aOther.pKeyValReal;
    pDomainElementType = aOther.pDomainElementType;
    
    aOther.pKeyValInt64 = std::numeric_limits<INT_64>::min();
    aOther.pKeyValReal = std::numeric_limits<REAL>::min();
    aOther.pDomainElementType = DomainElementType::DET_UNDEF;
  }
  
  DomainElementKey& DomainElementKey::operator= (const DomainElementKey& aOther)
  {
    if (&aOther == this)
    {
      return *this;
    }
    
    pKeyValInt64 = aOther.pKeyValInt64;
    pKeyValReal = aOther.pKeyValReal;
    pDomainElementType = aOther.pDomainElementType;
    
    return *this;
  }
  
  DomainElementKey& DomainElementKey::operator= (DomainElementKey&& aOther)
  {
    if (&aOther == this)
    {
      return *this;
    }
    
    pKeyValInt64 = aOther.pKeyValInt64;
    pKeyValReal = aOther.pKeyValReal;
    pDomainElementType = aOther.pDomainElementType;
    
    aOther.pKeyValInt64 = std::numeric_limits<INT_64>::min();
    aOther.pKeyValReal = std::numeric_limits<REAL>::min();
    aOther.pDomainElementType = DomainElementType::DET_UNDEF;
    
    return *this;
  }
  
  bool DomainElementKey::operator== (const DomainElementKey& aOther) const
  {
    if(pDomainElementType != aOther.getDomainElementType())
    {
      return false;
    }
    
    switch(pDomainElementType)
    {
      case DomainElementType::DET_REAL:
      {
        return pKeyValReal == aOther.pKeyValReal;
      }
      default:
        assert(pDomainElementType == DomainElementType::DET_INT_64);
        return pKeyValInt64 == aOther.pKeyValInt64;
    }//switch
  }
  
}// end namespace Core
