// Base class
#include "DomainEventVoid.hpp"

#include <cassert>

namespace Core {
  
  DomainEventVoid::DomainEventVoid()
  : DomainEvent(DomainEventType::VOID_EVENT)
  {
  }
  
  DomainEventVoid::~DomainEventVoid()
  {
  }
  
  bool DomainEventVoid::isa(DomainEvent *aEvent)
  {
    assert(aEvent);
    return aEvent->getEventType() == DomainEventType::VOID_EVENT;
  }//isa
  
  DomainEventVoid* DomainEventVoid::cast(DomainEvent *aEvent)
  {
    assert(aEvent);
    if (isa(aEvent))
    {
      return static_cast<DomainEventVoid*>(aEvent);
    }
    
    return nullptr;
  }//cast
  
  bool DomainEventVoid::overlaps(const DomainEvent *aDomainEvent) const
  {
    assert(aDomainEvent);
    
    auto event = aDomainEvent->getEventType();
    switch (event) {
      case DomainEventType::BOUND_EVENT:
        return false;
      case DomainEventType::CHANGE_EVENT:
        return false;
      case DomainEventType::LWB_EVENT:
        return false;
      case DomainEventType::UPB_EVENT:
        return false;
      case DomainEventType::SINGLETON_EVENT:
        return false;
      case DomainEventType::FAIL_EVENT:
        return false;
      case DomainEventType::VOID_EVENT:
        return true;
      default:
        assert(event == DomainEventType::UNDEF_EVENT);
        return false;
    }
  }//overlaps
  
}// end namespace Core
