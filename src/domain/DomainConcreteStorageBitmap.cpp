// Base class
#include "DomainConcreteStorageBitmap.hpp"

#include "DomainElementVoid.hpp"
#include "DomainElementInt64.hpp"

#include <stdlib.h>

namespace Core {
  
  DomainConcreteStorageBitmap::DomainConcreteStorageBitmap(DomainElementInt64 *aLowerBound,
                                                           DomainElementInt64 *aUpperBound,
                                                           DomainElementManager *aDomainElementManager)
  : DomainConcreteStorage(DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_BITMAP, aDomainElementManager)
  , pBitsetOffset(0)
  , pLowerBound(0)
  , pUpperBound(0)
  {
    assert(aLowerBound);
    assert(aUpperBound);
    
    auto keyValLowerBound = aLowerBound->getValue();
    auto keyValUpperBound = aUpperBound->getValue();
    
    // Set offset
    pBitsetOffset = keyValLowerBound == std::numeric_limits<INT_64>::min() ?
    std::numeric_limits<INT_64>::max() :
    keyValLowerBound * -1;
    
    std::size_t domainSize{ 0 };
    if (keyValUpperBound >= keyValLowerBound)
    {
      auto offsetLowerBound = getOffsetValue(keyValLowerBound);
      auto offsetUpperBound = getOffsetValue(keyValUpperBound);
      
      assert(offsetLowerBound);
      assert(offsetUpperBound);
      
      // If domain size is > 0, store indexes and keys
      assert((*offsetUpperBound - *offsetLowerBound + 1) < std::numeric_limits<std::size_t>::max());
      domainSize = static_cast<std::size_t>(*offsetUpperBound - *offsetLowerBound + 1);
      pBitmapToDomainElementInt64Map[*offsetLowerBound] = aLowerBound;
      pBitmapToDomainElementInt64Map[*offsetUpperBound] = aUpperBound;
      
      pLowerBound = *offsetLowerBound;
      pUpperBound = *offsetUpperBound;
    }
    
    pBitset = boost::dynamic_bitset<>(domainSize);
    
    // Flip bits all to 1
    pBitset.flip();
  }
  
  DomainConcreteStorageBitmap::~DomainConcreteStorageBitmap()
  {
  }
  
  boost::optional<std::size_t> DomainConcreteStorageBitmap::getOffsetValue(INT_64 aValue) const
  {
    INT_64 sumVal{0};
    if(aValue > 0 && pBitsetOffset == std::numeric_limits<INT_64>::max())
    {
      // Saturate on overflow
      sumVal = std::numeric_limits<INT_64>::max();
    }
    else if (aValue == std::numeric_limits<INT_64>::max() && pBitsetOffset >= 0)
    {
      // Saturate on overflow
      sumVal = std::numeric_limits<INT_64>::max();
    }
    else if(aValue < 0 && pBitsetOffset <= 0)
    {
      sumVal = std::numeric_limits<INT_64>::min();
    }
    else
    {
      sumVal = aValue + pBitsetOffset;
    }
    
    assert(sumVal <= std::numeric_limits<INT_64>::max());
    
    if (sumVal < 0)
    {
      return boost::optional< std::size_t >{};
    }
    return boost::optional<std::size_t>(static_cast<std::size_t>(sumVal));
  }//getOffsetValue

  
  DomainElement *DomainConcreteStorageBitmap::getLowerBound() const
  {
    assert(getSize());
    assert(pBitmapToDomainElementInt64Map.find(pLowerBound) != pBitmapToDomainElementInt64Map.end());
    
    return pBitmapToDomainElementInt64Map.at(pLowerBound);
  }//getLowerBound
  
  DomainElement *DomainConcreteStorageBitmap::getUpperBound() const
  {
    assert(getSize());
    assert(pBitmapToDomainElementInt64Map.find(pUpperBound) != pBitmapToDomainElementInt64Map.end());
    
    return pBitmapToDomainElementInt64Map.at(pUpperBound);
  }//getUpperBound
  
  DomainElement *DomainConcreteStorageBitmap::getNextElement(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    assert(DomainElementInt64::isa(aDomainElement));
    
    auto upperBoundElement = getUpperBound();
    if(upperBoundElement->isEqual(aDomainElement))
    {
      return upperBoundElement;
    }
    
    auto domainElement = getOffsetValue(DomainElementInt64::cast(aDomainElement)->getValue());
    assert(domainElement);
    
    auto domainElementVal = *domainElement;
    assert(domainElementVal >= pLowerBound);
    assert(domainElementVal < pUpperBound);
    DomainElement *elementToReturn = nullptr;
    
    domainElementVal++;
    for(; domainElementVal <= pUpperBound; ++domainElementVal)
    {
        if(pBitset.test(domainElementVal))
        {
          if (pBitmapToDomainElementInt64Map.find(domainElementVal) == pBitmapToDomainElementInt64Map.end())
          {
            pBitmapToDomainElementInt64Map[domainElementVal] =
            getDomainElementManager().createDomainElementInt64(getOriginalValueFromOffsetValue(domainElementVal));
          }
          elementToReturn = pBitmapToDomainElementInt64Map[domainElementVal];
          break;
        }
    }
    
    assert(elementToReturn);
    return elementToReturn;
  }//getNextElement
  
  DomainElement *DomainConcreteStorageBitmap::getPrevElement(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    assert(DomainElementInt64::isa(aDomainElement));
    
    auto lowerBoundElement = getLowerBound();
    if(lowerBoundElement->isEqual(aDomainElement))
    {
      return lowerBoundElement;
    }
    
    auto domainElement = getOffsetValue(DomainElementInt64::cast(aDomainElement)->getValue());
    assert(domainElement);
    
    auto domainElementVal = *domainElement;
    assert(domainElementVal > pLowerBound);
    assert(domainElementVal <= pUpperBound);
    DomainElement *elementToReturn = nullptr;
    
    domainElementVal--;
    for(; domainElementVal >= pLowerBound; --domainElementVal)
    {
      if(pBitset.test(domainElementVal))
      {
        if (pBitmapToDomainElementInt64Map.find(domainElementVal) == pBitmapToDomainElementInt64Map.end())
        {
          pBitmapToDomainElementInt64Map[domainElementVal] =
          getDomainElementManager().createDomainElementInt64(getOriginalValueFromOffsetValue(domainElementVal));
        }
        elementToReturn = pBitmapToDomainElementInt64Map[domainElementVal];
        break;
      }
    }

    assert(elementToReturn);
    return elementToReturn;
  }//getPrevElement
  
  DomainElement *DomainConcreteStorageBitmap::getElementByIndex(std::size_t aElementIndex)
  {
    assert(aElementIndex < getSize());
    DomainElement *elementToReturn = nullptr;
    
    for(std::size_t idx = pLowerBound; idx <= pUpperBound; ++idx)
    {
      if(aElementIndex == 0 && pBitset.test(idx))
      {
        if (pBitmapToDomainElementInt64Map.find(idx) == pBitmapToDomainElementInt64Map.end())
        {
          pBitmapToDomainElementInt64Map[idx] =
          getDomainElementManager().createDomainElementInt64(getOriginalValueFromOffsetValue(idx));
        }
        elementToReturn = pBitmapToDomainElementInt64Map[idx];
        break;
      }
      
      if (pBitset.test(idx))
      {
        aElementIndex--;
      }
    }
    
    assert(elementToReturn);
    return elementToReturn;
  }//getElementByIndex
  
  void DomainConcreteStorageBitmap::shrink(DomainElement *aLowerBound, DomainElement *aUpperBound)
  {
    assert(aLowerBound);
    assert(aUpperBound);
    
    assert(DomainElementInt64::isa(aLowerBound));
    assert(DomainElementInt64::isa(aUpperBound));
    
    auto lowerBound = getOffsetValue(DomainElementInt64::cast(aLowerBound)->getValue());
    auto upperBound = getOffsetValue(DomainElementInt64::cast(aUpperBound)->getValue());
    
    if (!upperBound)
    {
      emptyDomain();
      return;
    }
    
    if (!lowerBound)
    {
      // If aLowerBound < lowerBound set aLowerBound = lowerBound;
      lowerBound = getOffsetValue(DomainElementInt64::cast(getLowerBound())->getValue());
      assert(lowerBound);
    }
    
    // If aLowerBound < lowerBound and aUpperBound > upperBound, return
    if (*lowerBound <= pLowerBound && *upperBound >= pUpperBound)
    {
      return;
    }
    
    // If aLowerBound > aUpperBound, empty domain
    if (*lowerBound > *upperBound)
    {
      emptyDomain();
      return;
    }
    // If aUpperBound < lowerBound, empty domain
    if (*upperBound < pLowerBound)
    {
      emptyDomain();
      return;
    }
    // If aLowerBound > upperBound, empty domain
    if (*lowerBound > pUpperBound)
    {
      emptyDomain();
      return;
    }
    
    assert(*lowerBound < pBitset.size());
    assert(pUpperBound < pBitset.size());
    
    // Shrink domain on lower bound
    for (std::size_t bitIdx = pLowerBound; bitIdx < *lowerBound; ++bitIdx)
    {
      pBitset.reset(bitIdx);
    }
    
    // Shrink domain on upper bound
    for (std::size_t bitIdx = *upperBound + 1; bitIdx <= pUpperBound; ++bitIdx)
    {
      pBitset.reset(bitIdx);
    }
    
    // Update elements in map if not present
    checkAndStoreDomainElement(aLowerBound);
    checkAndStoreDomainElement(aUpperBound);
    
    // Nothing to update if domain is empty
    if (!getSize())
    {
      return;
    }
    
    // Find the new lower bound
    auto minUpperBound = (*upperBound < pUpperBound) ? *upperBound : pUpperBound;
    updateLowerBound(*lowerBound, minUpperBound);

    // Find the new upper bound
    // @note pLowerBound is already updated
    updateUpperBound(pLowerBound, *upperBound);
  }//shrink
  
  void DomainConcreteStorageBitmap::subtract(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    assert(DomainElementInt64::isa(aDomainElement));
    
    if (!contains(aDomainElement))
    {
      return;
    }
    
    auto val = getOffsetValue(DomainElementInt64::cast(aDomainElement)->getValue());
    if (val)
    {
      assert(*val < pBitset.size());
      pBitset.reset(*val);
      
      // Return if nothing to update
      if(getSize() == 0)
      {
        return;
      }
      
      // Update lower bound
      if (*val == pLowerBound)
      {
        updateLowerBound(*val, pUpperBound);
      }
      
      // Update upper bound
      if (*val == pUpperBound)
      {
        updateUpperBound(pLowerBound, *val);
      }
    }
  }//subtract
  
  void DomainConcreteStorageBitmap::inMin(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    assert(DomainElementInt64::isa(aDomainElement));
    
    // if aDomainElement <= lowerBound return
    auto lowerBound = getOffsetValue(DomainElementInt64::cast(aDomainElement)->getValue());
    if (!lowerBound || *lowerBound <= pLowerBound)
    {
      return;
    }
    
    // if aDomainElement > upperBound, empty domain
    if (*lowerBound > pUpperBound)
    {
      emptyDomain();
      return;
    }
    
    // Update elements in map if not present
    checkAndStoreDomainElement(aDomainElement);
    
    for (std::size_t bitIdx = pLowerBound; bitIdx < *lowerBound; ++bitIdx)
    {
      pBitset.reset(bitIdx);
    }
    
    // Find the new lower bound
    updateLowerBound(*lowerBound, pUpperBound);
  }//inMin
  
  void DomainConcreteStorageBitmap::inMax(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    assert(DomainElementInt64::isa(aDomainElement));
    
    // if aDomainElement <= lowerBound return
    auto upperBound = getOffsetValue(DomainElementInt64::cast(aDomainElement)->getValue());
    if (!upperBound)
    {
      // If empty => upperBound < 0 and hence < lowerBound == offset
      emptyDomain();
      return;
    }
    
    if (*upperBound >= pUpperBound)
    {
      return;
    }
    
    // if aDomainElement > upperBound, empty domain
    if (*upperBound < pLowerBound)
    {
      emptyDomain();
      return;
    }
    
    // Update elements in map if not present
    checkAndStoreDomainElement(aDomainElement);
    
    // Shrink domain on upper bound
    for (std::size_t bitIdx = *upperBound + 1; bitIdx <= pUpperBound; ++bitIdx)
    {
      pBitset.reset(bitIdx);
    }
    
    // Find the new upper bound
    updateUpperBound(pLowerBound, *upperBound);
  }//inMax
  
  bool DomainConcreteStorageBitmap::contains(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    assert(DomainElementInt64::isa(aDomainElement));
    
    auto domainElement = getOffsetValue(DomainElementInt64::cast(aDomainElement)->getValue());
    if (domainElement && *domainElement < pBitset.size())
    {
      return pBitset.test(*domainElement);
    }
    
    return false;
  }//contains
  
  void DomainConcreteStorageBitmap::updateLowerBound(std::size_t aLowerBound, std::size_t aUpperBound)
  {
    // There must be at least one element
    assert(getSize());
    
    // Get the current lower bound as a starting point
    auto currentLowerBound = getOriginalValueFromOffsetValue(aLowerBound);
    
    for (std::size_t bitIdx = aLowerBound; bitIdx <= aUpperBound + 1; ++bitIdx)
    {
      // Find the first set bit from lower bound as new lower bound
      if (bitIdx < pBitset.size() && pBitset.test(bitIdx))
      {
        pLowerBound = bitIdx;
        
        if (pBitmapToDomainElementInt64Map.find(pLowerBound) == pBitmapToDomainElementInt64Map.end())
        {
          pBitmapToDomainElementInt64Map[pLowerBound] =
          getDomainElementManager().createDomainElementInt64(currentLowerBound);
        }
        
        break;
      }
      
      // If there are no bit set, empty domain
      if (bitIdx == aUpperBound + 1)
      {
        emptyDomain();
        return;
      }
      
      currentLowerBound++;
    }
  }//updateLowerBound
  
  void DomainConcreteStorageBitmap::updateUpperBound(std::size_t aLowerBound, std::size_t aUpperBound)
  {
    // There must be at least one element
    assert(getSize());
    
    // Get the current upper bound as a starting point
    auto currentUpperBound = getOriginalValueFromOffsetValue(aUpperBound);
    
    // If aUpperBound >= number of bits,
    // nothing to update, return
    if(aUpperBound >= pBitset.size())
    {
      return;
    }

    for (std::size_t bitIdx = aUpperBound; bitIdx >= aLowerBound; --bitIdx)
    {
      // Find the first set bit from upper bound as new upper bound
      if (pBitset.test(bitIdx))
      {
        pUpperBound = bitIdx;
        
        if (pBitmapToDomainElementInt64Map.find(pUpperBound) == pBitmapToDomainElementInt64Map.end())
        {
          pBitmapToDomainElementInt64Map[pUpperBound] =
          getDomainElementManager().createDomainElementInt64(currentUpperBound);
        }
        
        break;
      }
      
      // If there are no bit set, empty domain
      if (bitIdx == aLowerBound)
      {
        emptyDomain();
        return;
      }
      
      currentUpperBound--;
    }
  }//updateUpperBound
  
  void DomainConcreteStorageBitmap::emptyDomain()
  {
    pLowerBound = 0;
    pUpperBound = pBitset.size() - 1;
    
    pBitset.reset();
  }//emptyDomain
  
  void DomainConcreteStorageBitmap::checkAndStoreDomainElement(DomainElement *aDomainElement)
  {
    assert(aDomainElement);
    assert(DomainElementInt64::isa(aDomainElement));
    
    auto keyOffset = getOffsetValue(DomainElementInt64::cast(aDomainElement)->getValue());
    if (!keyOffset || *keyOffset >= pBitset.size())
    {
      return;
    }
    
    if (pBitmapToDomainElementInt64Map.find(*keyOffset) == pBitmapToDomainElementInt64Map.end())
    {
      pBitmapToDomainElementInt64Map[*keyOffset] = DomainElementInt64::cast(aDomainElement);
    }
  }//checkAndStoreDomainElement
  
}// end namespace Core
