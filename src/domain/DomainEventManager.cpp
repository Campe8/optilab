// Base class
#include "DomainEventManager.hpp"

#include <cassert>

namespace Core {
  
  DomainEventManager::DomainEventManager()
  {
    pEventStore = new EventStoreMap();
  }
  
  DomainEventManager::~DomainEventManager()
  {
    for (auto it = pEventStore->begin(); it != pEventStore->end(); ++it)
    {
      delete it->second;
    }
    
    delete pEventStore;
  }
  
  DomainEvent *DomainEventManager::getEvent(DomainEventType aEventType)
  {
    assert(aEventType != DomainEventType::NUM_EVENTS);
    if (pEventStore->find(aEventType) == pEventStore->end())
    {
      pEventStore->operator[](aEventType) = getNewEventInstance(aEventType);
    }
    
    return pEventStore->operator[](aEventType);
  }//getEvent
  
  DomainEvent *DomainEventManager::getNewEventInstance(DomainEventType aEventType)
  {
    assert(aEventType != DomainEventType::NUM_EVENTS);
    switch (aEventType)
    {
      case Core::VOID_EVENT:
        return new DomainEventVoid();
      case Core::SINGLETON_EVENT:
        return new DomainEventSingleton();
      case Core::BOUND_EVENT:
        return new DomainEventBound();
      case Core::LWB_EVENT:
        return new DomainEventLwb();
      case Core::UPB_EVENT:
        return new DomainEventUpb();
      case Core::CHANGE_EVENT:
        return new DomainEventChange();
      case Core::FAIL_EVENT:
        return new DomainEventFail();
      case Core::UNDEF_EVENT:
        return new DomainEventUndef();
      case Core::NUM_EVENTS:
      default:
        return nullptr;
    }
  }//getNewEventInstance
  
}// end namespace Core
