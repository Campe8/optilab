// Base class
#include "DomainIterator.hpp"
#include "DomainElementVoid.hpp"

#include "BaseTools.hpp"

#include <stdlib.h>
#include <cassert>

namespace Core {
  
  namespace Internal {
    
    /// Returns a random value in [aLB, aUB]
    int randomNumberGenerator(int aLB, int aUB)
    {
      return Base::Tools::randomNumberGenerator(aLB, aUB);
    }//randomNumberGenerator
    
  }//end namespace internal
  
  DomainIterator::DomainIterator(DomainElementContainer *aDomainContainer)
  : pReverseIter(false)
  , pDomainContainer(aDomainContainer)
  , pDomainElementManager(&(DomainElementManager::getInstance()))
  {
    assert(pDomainContainer);
    pDomainElement = pDomainContainer->getLowerBound();
  }
  
  DomainIterator::DomainIterator(const iterator& aDomainIterator)
  {
    pReverseIter = aDomainIterator.pReverseIter;
    pDomainContainer = aDomainIterator.pDomainContainer;
    pDomainElement = aDomainIterator.pDomainElement;
    pDomainElementManager = aDomainIterator.pDomainElementManager;
  }
  
  DomainIterator::DomainIterator(iterator&& aDomainIterator)
  {
    pReverseIter = aDomainIterator.pReverseIter;
    pDomainContainer = aDomainIterator.pDomainContainer;
    pDomainElement = aDomainIterator.pDomainElement;
    pDomainElementManager = aDomainIterator.pDomainElementManager;
    
    aDomainIterator.pReverseIter = false;
    aDomainIterator.pDomainContainer = nullptr;
    aDomainIterator.pDomainElement = nullptr;
    aDomainIterator.pDomainElementManager = nullptr;
  }//DomainIterator
  
  DomainIterator::~DomainIterator()
  {
  }
  
  iterator& DomainIterator::operator= (iterator&& aDomainIterator)
  {
    if (this != &aDomainIterator)
    {
      pReverseIter = aDomainIterator.pReverseIter;
      pDomainContainer = aDomainIterator.pDomainContainer;
      pDomainElement = aDomainIterator.pDomainElement;
      pDomainElementManager = aDomainIterator.pDomainElementManager;
      
      aDomainIterator.pReverseIter = false;
      aDomainIterator.pDomainContainer = nullptr;
      aDomainIterator.pDomainElement = nullptr;
      aDomainIterator.pDomainElementManager = nullptr;
    }
    
    return *this;
  }
  
  iterator& DomainIterator::operator= (const iterator& aDomainIterator)
  {
    if(this != &aDomainIterator)
    {
      pReverseIter = aDomainIterator.pReverseIter;
      pDomainContainer = aDomainIterator.pDomainContainer;
      pDomainElement = aDomainIterator.pDomainElement;
      pDomainElementManager = aDomainIterator.pDomainElementManager;
    }
    return *this;
  }
  
  bool DomainIterator::operator==(const iterator& aDomainIterator)
  {
    // Check if the iterators iterate over the same container
    bool eqComparison = pDomainContainer == aDomainIterator.pDomainContainer;
    // Check if the iterators are on the same element
    eqComparison = eqComparison && DomainElement::isEqual(pDomainElement, aDomainIterator.pDomainElement);
    // Check if the iterators have the same "direction"
    eqComparison = eqComparison && (pReverseIter == aDomainIterator.pReverseIter);
    
    return eqComparison;
  }
  
  iterator DomainIterator::begin()
  {
    assert(pDomainContainer);
    pDomainElement = pDomainContainer->getLowerBound();
    return *this;
  }//begin
  
  iterator DomainIterator::rbegin()
  {
    assert(pDomainContainer);
    pReverseIter = true;
    pDomainElement = pDomainContainer->getUpperBound();
    return *this;
  }//rbegin
  
  iterator DomainIterator::end()
  {
    assert(pDomainElementManager);
    pDomainElement = pDomainElementManager->getVoidElement();
    return *this;
  }//end
  
  iterator DomainIterator::randBegin(Base::Tools::RandomGenerator* aRandGen)
  {
    assert(pDomainContainer);
    auto size = pDomainContainer->getDomainSize();
    if(size == 0) { return end(); }
    if(size == 1) { return begin(); }

    int randElement = aRandGen ?
    aRandGen->randInt(0, static_cast<int>(size) - 1) :
    Internal::randomNumberGenerator(0, static_cast<int>(size) - 1);
    assert(0 <= randElement && randElement < size);

    pDomainElement = pDomainContainer->getThElement(static_cast<std::size_t>(randElement + 1));
    return *this;
  }//randBegin
  
  iterator & DomainIterator::operator++()
  {
    assert(pDomainElement);
    if(!DomainElementVoid::isa(pDomainElement))
    {
      assert(pDomainContainer);
      if(pReverseIter)
      {
        pDomainElement = pDomainContainer->getElementLessThan(pDomainElement);
      }
      else
      {
        pDomainElement = pDomainContainer->getElementGreaterThan(pDomainElement);
      }
    }
    return *this;
  }
  
  iterator DomainIterator::operator++ ( int )
  {
    DomainIterator clonedIterator(*this);
    operator++();
    return clonedIterator;
  }
  
  reference DomainIterator::operator*()
  {
    assert(pDomainElement);
    return *pDomainElement;
  }
  
  pointer DomainIterator::operator->() const
  {
    return pDomainElement;
  }
  
}// end namespace Core
