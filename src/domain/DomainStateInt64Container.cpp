// Base class
#include "DomainStateInt64Container.hpp"

// Concrete storage
#include "DomainConcreteStorageBitmap.hpp"
#include "DomainConcreteStorageBounds.hpp"
#include "DomainConcreteStorageSet.hpp"

#include <cassert>

namespace Core {
  
  DomainStateInt64Container::DomainStateInt64Container()
    : DomainState(DomainStateType::DOM_STATE_INT64_CONT)
    , pConcreteStorageInstance(nullptr)
  {
  }
  
  DomainStateInt64Container::~DomainStateInt64Container()
  {
    delete pConcreteStorageInstance;
    pConcreteStorageInstance = nullptr;
  }

  void DomainStateInt64Container::createState(const DomainConcreteStorage* aStorage) const
  {
    assert(aStorage);
    switch (aStorage->getConcreteStorageType())
    {
    case DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_BITMAP:
      pConcreteStorageInstance = new DomainConcreteStorageBitmap(*(static_cast<const DomainConcreteStorageBitmap*>(aStorage)));
      break;
    case DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_BOUNDS:
      pConcreteStorageInstance = new DomainConcreteStorageBounds(*(static_cast<const DomainConcreteStorageBounds*>(aStorage)));
      break;
    default:
      assert(aStorage->getConcreteStorageType() == DomainConcreteStorageType::DOMAIN_CONCRETE_STORAGE_SET);
      pConcreteStorageInstance = new DomainConcreteStorageSet(*(static_cast<const DomainConcreteStorageSet*>(aStorage)));
      break;
    }
  }//setInternalState

}// end namespace Core
