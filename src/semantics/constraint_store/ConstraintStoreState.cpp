// Base class
#include "ConstraintStoreState.hpp"

#include <cassert>

namespace Semantics {
  
  ConstraintStoreState::ConstraintStoreState(std::size_t aReevaluationCost,
    const spp::sparse_hash_set<Core::UUIDCoreObj>& aSubsumedConstraintSet,
    const spp::sparse_hash_map<Core::PropagationCost, spp::sparse_hash_set<Core::UUIDCoreObj>>& aReevaluationSet)
    : pCurrentReevaluationCost(aReevaluationCost)
    , pSubsumedConstraintSet(aSubsumedConstraintSet)
    , pReevaluationSet(aReevaluationSet)
  {
  }
  
}// end namespace Semantics

