// Base class
#include "ConstraintStore.hpp"

// Memento state
#include "ConstraintStoreState.hpp"
#include "MementoStateConstraintStore.hpp"

// Constraint
#include "Constraint.hpp"
#include "NogoodConstraint.hpp"

#include <algorithm>

namespace Semantics {

  ConstraintStore::ConstraintStore()
  : pCurrentReevaluationCost(0)
  , pStateSnapshot(nullptr)
  {
    // Init cost vector
    initPropagationCostOrderVector(pPropagationCostOrderVector);
  }

  ConstraintStore::ConstraintStore(const ConstraintStore& aOther)
  {
    pStdConstraintFitness = aOther.pStdConstraintFitness;
    pNogoodConstraintFitness = aOther.pNogoodConstraintFitness;
    pConstraintRegister = aOther.pConstraintRegister;
    pStateSnapshot = aOther.pStateSnapshot;
    pCurrentReevaluationCost = aOther.pCurrentReevaluationCost;
    pPropagationCostOrderVector = aOther.pPropagationCostOrderVector;
    pSubsumedConstraintSet = aOther.pSubsumedConstraintSet;
    pReevaluationSet = aOther.pReevaluationSet;
  }

  ConstraintStore::ConstraintStore(ConstraintStore&& aOther)
  {
    pStdConstraintFitness = aOther.pStdConstraintFitness;
    pNogoodConstraintFitness = aOther.pNogoodConstraintFitness;
    pConstraintRegister = aOther.pConstraintRegister;
    pStateSnapshot = aOther.pStateSnapshot;
    pCurrentReevaluationCost = aOther.pCurrentReevaluationCost;
    pPropagationCostOrderVector = aOther.pPropagationCostOrderVector;
    pSubsumedConstraintSet = aOther.pSubsumedConstraintSet;
    pReevaluationSet = aOther.pReevaluationSet;
    
    aOther.pStdConstraintFitness = 0;
    aOther.pNogoodConstraintFitness = 0;
    aOther.pConstraintRegister.deepClearRegister();
    aOther.pStateSnapshot = nullptr;
    aOther.pCurrentReevaluationCost = 0;
    aOther.pPropagationCostOrderVector.clear();
    aOther.pSubsumedConstraintSet.clear();
    aOther.pReevaluationSet.clear();
  }

  ConstraintStore& ConstraintStore::operator= (const ConstraintStore& aOther)
  {
    if (&aOther == this)
    {
      return *this;
    }

    pStdConstraintFitness = aOther.pStdConstraintFitness;
    pNogoodConstraintFitness = aOther.pNogoodConstraintFitness;
    pConstraintRegister = aOther.pConstraintRegister;
    pStateSnapshot = aOther.pStateSnapshot;
    pCurrentReevaluationCost = aOther.pCurrentReevaluationCost;
    pPropagationCostOrderVector = aOther.pPropagationCostOrderVector;
    pSubsumedConstraintSet = aOther.pSubsumedConstraintSet;
    pReevaluationSet = aOther.pReevaluationSet;
    return *this;
  }

  ConstraintStore& ConstraintStore::operator= (ConstraintStore&& aOther)
  {
    if (&aOther == this)
    {
      return *this;
    }

    pStdConstraintFitness = aOther.pStdConstraintFitness;
    pNogoodConstraintFitness = aOther.pNogoodConstraintFitness;
    pConstraintRegister = aOther.pConstraintRegister;
    pStateSnapshot = aOther.pStateSnapshot;
    pCurrentReevaluationCost = aOther.pCurrentReevaluationCost;
    pPropagationCostOrderVector = aOther.pPropagationCostOrderVector;
    pSubsumedConstraintSet = aOther.pSubsumedConstraintSet;
    pReevaluationSet = aOther.pReevaluationSet;
    
    aOther.pStdConstraintFitness = 0;
    aOther.pNogoodConstraintFitness = 0;
    aOther.pConstraintRegister.deepClearRegister();
    aOther.pStateSnapshot = nullptr;
    aOther.pCurrentReevaluationCost = 0;
    aOther.pPropagationCostOrderVector.clear();
    aOther.pSubsumedConstraintSet.clear();
    aOther.pReevaluationSet.clear();
    return *this;
  }
  
  void ConstraintStore::initPropagationCostOrderVector(std::vector<Core::PropagationCost>& aCostVector)
  {
    aCostVector.push_back(Core::PropagationCost(Core::PropagationPriority::PROP_PRIORITY_UNARY));
    aCostVector.push_back(Core::PropagationCost(Core::PropagationPriority::PROP_PRIORITY_BINARY));
    aCostVector.push_back(Core::PropagationCost(Core::PropagationPriority::PROP_PRIORITY_TERNARY));
    aCostVector.push_back(Core::PropagationCost(Core::PropagationPriority::PROP_PRIORITY_LINEAR));
    aCostVector.push_back(Core::PropagationCost(Core::PropagationPriority::PROP_PRIORITY_QUADRATIC));
    aCostVector.push_back(Core::PropagationCost(Core::PropagationPriority::PROP_PRIORITY_CUBIC));
    aCostVector.push_back(Core::PropagationCost(Core::PropagationPriority::PROP_PRIORITY_VERY_SLOW));
  }// initPropagationCostOrderVector
  
  void ConstraintStore::revertSubsumedConstraints(const spp::sparse_hash_set<Core::UUIDCoreObj>& aSubsumedConstraints)
  {
    for (auto& conId : pSubsumedConstraintSet)
    {
      /*
       * If a constraint is not need to be subsumed anymore, reset its state.
       * @note this constraint could have been a temporary constraint (e.g., optimization constraint)
       * which may have been removed already from the constraint register but its id is still in
       * the subsumed constraint set (and not in "aSubsumedConstraints").
       * If this is the case: skip the constraint.
       */
      if (aSubsumedConstraints.find(conId) == aSubsumedConstraints.end() &&
          pConstraintRegister.isKeyRegistered(conId))
      {
        // This constraint must be reverted.
        // Reset internal constraint state
        pConstraintRegister.getConstraint(conId)->resetState();
      }
    }
    pSubsumedConstraintSet = aSubsumedConstraints;
  }//revertSubsumedConstraints
  
  void ConstraintStore::saveSemanticState()
  {
    pStateSnapshot.reset(snapshot().release());
  }//saveSemanticState
  
  void  ConstraintStore::uploadSemanticState()
  {
    if (!pStateSnapshot) return;
    reinstateSnapshot(pStateSnapshot.get());
  }//uploadSemanticState
  
  std::unique_ptr<Search::MementoState> ConstraintStore::snapshot()
  {
    std::unique_ptr<ConstraintStoreState> csState(new ConstraintStoreState(pCurrentReevaluationCost,
                                                                           pSubsumedConstraintSet,
                                                                           pReevaluationSet));
    
    auto state =  new Search::MementoStateConstraintStore(std::move(csState));
    return std::unique_ptr<Search::MementoStateConstraintStore>(state);
  }//snapshot
  
  void ConstraintStore::reinstateSnapshot(const Search::MementoState* aSnapshot)
  {
    assert(aSnapshot);
    assert(Search::MementoStateConstraintStore::isa(aSnapshot));
    
    const ConstraintStoreState* mementoState =
                    Search::MementoStateConstraintStore::cast(aSnapshot)->getConstraintStoreState();
    assert(mementoState);
    
    pCurrentReevaluationCost = mementoState->reevaluationCost();
    pReevaluationSet = mementoState->reevaluationSet();
    
    // Revert subsumed constraints taking into account propagators
    revertSubsumedConstraints(mementoState->subsumedConstraintSet());
  }//reinstateSnapshot
  
  void ConstraintStore::registerConstraint(Core::Constraint* aConstraint)
  {
    assert(aConstraint);
    pConstraintRegister.registerConstraint(aConstraint);
    reevaluateConstraint(aConstraint);

    // Constraint to post
    pPostConstraintSet.insert(aConstraint->getUUID());
  }//registerConstraint
  
  void ConstraintStore::storeConstraint(Core::Constraint* aConstraint)
  {
    assert(aConstraint);
    pConstraintRegister.storeConstraint(aConstraint);
    registerConstraint(aConstraint);
  }//storeConstraint
  
  void ConstraintStore::deregisterConstraint(Core::Constraint* aConstraint)
  {
    assert(aConstraint);
    pConstraintRegister.removeConstraint(aConstraint->getUUID());
    
    // Remove any constraint which is posted
    pPostConstraintSet.erase(aConstraint->getUUID());
  }//deregisterConstraint
  
  void ConstraintStore::removeStoredConstraint(Core::Constraint* aConstraint)
  {
    assert(aConstraint);
    pConstraintRegister.removeStoredConstraint(aConstraint->getUUID());
  }//deregisterConstraint
  
  bool ConstraintStore::isConstraintRegistered(Core::Constraint* aConstraint) const
  {
    assert(aConstraint);
    return isConstraintRegistered(aConstraint->getUUID());
  }//isConstraintRegistered
  
  Core::Constraint* ConstraintStore::getNextConstraintToReevaluate()
  {
    if(pConstraintRegister.getNumRegisteredConstraints() == 0)
    {
      return nullptr;
    }
    
    // Find first non empty set of constraints ready for reevaluation
    while (pCurrentReevaluationCost < pPropagationCostOrderVector.size() &&
           !moreConstraintToReevaluateGivenCost(pCurrentReevaluationCost))
    {
      pCurrentReevaluationCost++;
    }
    
    // If no set is available -> return nullptr
    if(pCurrentReevaluationCost >= pPropagationCostOrderVector.size())
    {
      return nullptr;
    }
    
    auto& setOfAvailableConstraints =
                      pReevaluationSet.at(pPropagationCostOrderVector.at(pCurrentReevaluationCost));
    assert(!setOfAvailableConstraints.empty());
    
    // Find first not subsumed constraint
    auto itConId = setOfAvailableConstraints.begin();
    for (; itConId != setOfAvailableConstraints.end();)
    {
      // Remove constraint if it is subsumed or not registered
      if (isConstraintSubsumed(*itConId) || !isConstraintRegistered(*itConId))
      {
        // Remove subsumed constraints from the reevaluation set
        itConId = setOfAvailableConstraints.erase(itConId);
      }
      else
      {
        // Available constraint
        break;
      }
    }
    
    if(itConId == setOfAvailableConstraints.end())
    {
      // Try next reevaluation cost set
      pCurrentReevaluationCost++;
      return getNextConstraintToReevaluate();
    }
    
    auto nextConstraintToReevaluate = pConstraintRegister.getConstraint(*itConId);
    assert(nextConstraintToReevaluate);
    
    // Remove current constraint id from constraints set to reevaluate
    // @note erasing from setOfAvailableConstraints invalidates "itConId"
    setOfAvailableConstraints.erase(itConId);
    
    // Return next constraint to reevaluate
    return nextConstraintToReevaluate;
  }//getNextConstraintToReevaluate
  
  void ConstraintStore::reevaluateConstraint(Core::Constraint* aConstraint)
  {
    assert(aConstraint);
    assert(isConstraintRegistered(aConstraint));
    
    const auto& constraintCost = aConstraint->getCost();
    pReevaluationSet[constraintCost].insert(aConstraint->getUUID());
    
    // Reset the current re-evaluation cost to the minimum between
    // the cost of the constraint to re-evaluate and the current cost
    // otherwise constraints with cost lower than current cost
    // will never be re-evaluated
    std::size_t conCostIdx{0};
    while(conCostIdx < pPropagationCostOrderVector.size() &&
          pPropagationCostOrderVector[conCostIdx] != constraintCost ) conCostIdx++;
    
    // The index in the ordered vector of costs sets the current re-evaluation cost
    pCurrentReevaluationCost = std::min<std::size_t>( pCurrentReevaluationCost, conCostIdx );
  }//reevaluateConstraint

  void ConstraintStore::cleanStatus()
  {
    pReevaluationSet.clear();
  }//cleanStatus
  
  std::vector<ConstraintRegister::RegisterKey> ConstraintStore::getConstraintQueueKeys()
  {
    // Index of costraint key in the queue
    std::size_t cidx{0};
    
    // List of constraint keys
    std::vector<ConstraintRegister::RegisterKey> queueKeyList;
    
    // Return an empty list if there are no registered constraints
    if(pConstraintRegister.getNumRegisteredConstraints() == 0)
    {
      return queueKeyList;
    }
    
    // Find non empty set of constraints ready in the queue/reevaluation set
    for (; cidx < pPropagationCostOrderVector.size(); ++cidx)
    {
      if (!moreConstraintToReevaluateGivenCost(cidx)) continue;
      
      auto& setOfAvailableConstraints = pReevaluationSet.at(pPropagationCostOrderVector.at(cidx));
      assert(!setOfAvailableConstraints.empty());
      
      for (auto conIter = setOfAvailableConstraints.begin();
           conIter != setOfAvailableConstraints.end();
           ++conIter)
      {
        queueKeyList.push_back(*conIter);
      }
    }

    return queueKeyList;
  }//getConstraintQueueKeys
  
  spp::sparse_hash_set<Core::UUIDCoreObj> ConstraintStore::aggressivePostConstraints()
  {
    // Set of failed constraints
    spp::sparse_hash_set<Core::UUIDCoreObj> failedPostSet;
    
    // Loop over the set of constraint to post and check
    // if they lead to failure
    for(auto& cID : pPostConstraintSet)
    {
      assert(isConstraintRegistered(cID));
      auto constraint = pConstraintRegister.getConstraint(cID);
      
      // Post constraint
      switch (constraint->post())
      {
        case Core::PropagationEvent::PROP_EVENT_FAIL:
        {
          failedPostSet.insert(cID);
          break;
        }
        case Core::PropagationEvent::PROP_EVENT_SUBSUMED:
          // @note do nothing for subsumed constraints.
          // They may have to re-run on other neighborhoods
          // so do not remove them from the constraint store
        case Core::PropagationEvent::PROP_EVENT_RUN_UNSPEC:
        default:
          break;
      }
    }//for
    
    // Remove all post constraints from set
    pPostConstraintSet.clear();
    
    // Return the set of constraints failed during posting
    return failedPostSet;
  }//aggressivePostConstraints
  
  StoreConsistencyStatus ConstraintStore::runAggressiveConsistency()
  {
    // Reset counter unsat constraints
    resetUnsatConstraintsCtr();
    
    // Check if any constraint needs to be posted,
    // if so run post constraint
    spp::sparse_hash_set<Core::UUIDCoreObj> failedConstraints;
    if(!pPostConstraintSet.empty())
    {
      failedConstraints = aggressivePostConstraints();
      addUnsatConstraints(failedConstraints.size());
    }
    
    // Start by the constraints with lower cost
    resetReevaluationCost();
    
    // Flags for returning event
    bool subsumed  { true  };
    bool fixpoint  { true  };
    bool noFixpoint{ false };
    
    // Loop on th set of constraints to reevaluate until failure
    // or fixpoint is reached
    Core::Constraint* constraint = getNextConstraintToReevaluate();
    if(!constraint)
    {
      if(failedConstraints.empty())
      {
        // Notify backtrack manager since reevaluation cost has been reset
        notifyBacktrackManager();
        return StoreConsistencyStatus::CONSISTENCY_SUBSUMED;
      }
      
      // Otherwise return the failure
      cleanStatus();
      return StoreConsistencyStatus::CONSISTENCY_FAILED;
    }
    
    Core::PropagationEvent event = Core::PropagationEvent::PROP_EVENT_SUBSUMED;
    do {
      if (failedConstraints.find(constraint->getUUID()) != failedConstraints.end())
      {
        // This constraint was already failed,
        // continue with next constraint to re-evaluate if any.
        // Otherwise break the loop and return
        constraint = getNextConstraintToReevaluate();
        if(constraint) continue;
        
        break;
      }
      
      // Propagate current constraint.
      // @note may trigger re-evaluation
      event = constraint->propagate();
      
      // Take actions according to propagation event
      switch (event)
      {
        case Core::PropagationEvent::PROP_EVENT_FAIL:
        {
          // On failure:
          // 1 - add +1 to unsatisfied constraints
          // 2 - continue
          addUnsatConstraints();
        }
          break;
        case Core::PropagationEvent::PROP_EVENT_SUBSUMED:
        {
          // Move current constraint into set of subsumed constraints
          pSubsumedConstraintSet.insert(constraint->getUUID());
          // At least one subsumed
          fixpoint = false;
        }
          break;
        case Core::PropagationEvent::PROP_EVENT_FIXPOINT:
        {
          // At least one subsumed
          subsumed = false;
        }
          break;
        default:
          assert(event == Core::PropagationEvent::PROP_EVENT_NO_FIXPOINT ||
                 event == Core::PropagationEvent::PROP_EVENT_RUN_UNSPEC);
          noFixpoint = true;
          break;
      }
      
      // Get next constraint to reevaluate
      constraint = getNextConstraintToReevaluate();
    } while (constraint);
    
    // If not fail, reevaluation set should be empty
    assert(pCurrentReevaluationCost >= pPropagationCostOrderVector.size());
    
    // Return asap on failure
    if(getNumUnsatConstraint() > 0)
    {
      cleanStatus();
      return StoreConsistencyStatus::CONSISTENCY_FAILED;
    }
    
    // Notify backtrack manager of changes on constraint store
    notifyBacktrackManager();
    
    if(noFixpoint)
    {
      return StoreConsistencyStatus::CONSISTENCY_NO_FIXPOINT;
    }
    
    if(subsumed)
    {
      assert(!fixpoint);
      return StoreConsistencyStatus::CONSISTENCY_SUBSUMED;
    }
    
    assert(!subsumed);
    return StoreConsistencyStatus::CONSISTENCY_FIXPOINT;
  }//runAggressiveConsistency
  
  double ConstraintStore::calculateSemanticFitness()
  {
    // Reset fitness values
    pStdConstraintFitness = 0;
    pNogoodConstraintFitness = 0;
    
    for (const auto& key : getConstraintQueueKeys())
    {
      auto con = pConstraintRegister.getConstraint(key);
      auto fit = con->getFitness();
      
      if (con->getConstraintId() == Core::ConstraintId::CON_ID_NOGOOD)
      {
        // Return a negative value for invalid fitness, otherwise sum the fitness value
        if (fit == Core::Constraint::FitnessInvalid)
        {
          pNogoodConstraintFitness = -1;
          return -1;
        }
        pNogoodConstraintFitness += fit;
      }
      else
      {
        // Return a negative value for invalid fitness, otherwise sum the fitness value
        if (fit == Core::Constraint::FitnessInvalid)
        {
          pStdConstraintFitness = -1;
          return -1;
        }
        pStdConstraintFitness += fit;
      }
    }
    
    // Return the fitness as the sum of standard constraint fitness and nogood constraint fitness
    return pStdConstraintFitness + pNogoodConstraintFitness;
  }//calculateSemanticFitness
  
  StoreConsistencyStatus ConstraintStore::postConstraints()
  {
    // Flags for returning event
    bool subsumed { true  };
    
    // Set of subsumed constraints
    std::unordered_set<Core::UUIDCoreObj> subsumedConstraintSet;

    Core::PropagationEvent event = Core::PropagationEvent::PROP_EVENT_SUBSUMED;
    for(auto& cID : pPostConstraintSet)
    {
      assert(isConstraintRegistered(cID));
      auto constraint = pConstraintRegister.getConstraint(cID);
      
      // Post constraint.
      // @note it may trigger re-evaluation
      //       of constraint propagation
      event = constraint->post();
      
      // Take actions according to propagation event
      switch (event)
      {
        case Core::PropagationEvent::PROP_EVENT_FAIL:
        {
          pPostConstraintSet.clear();
          return StoreConsistencyStatus::CONSISTENCY_FAILED;
        }
        case Core::PropagationEvent::PROP_EVENT_SUBSUMED:
          // Move current constraint into set of subsumed constraints
          subsumedConstraintSet.insert(constraint->getUUID());
          break;
        case Core::PropagationEvent::PROP_EVENT_RUN_UNSPEC:
          // At least one subsumed
          subsumed = false;
          break;
        default:
          assert(event == Core::PropagationEvent::PROP_EVENT_NO_FIXPOINT);
          subsumed = false;
          break;
      }
    }
    
    // Remove all post constraints from set
    pPostConstraintSet.clear();
    
    // Remove all subsumed constraints from register
    for(auto& cID : subsumedConstraintSet)
    {
      assert(pConstraintRegister.isKeyRegistered(cID));
      pConstraintRegister.removeConstraint(cID);
    }
    
    if(!subsumed)
    {
      return StoreConsistencyStatus::CONSISTENCY_FIXPOINT;
    }
    
    return StoreConsistencyStatus::CONSISTENCY_SUBSUMED;
  }//postConstraints
  
  StoreConsistencyStatus ConstraintStore::runConsistency()
  {
    // Reset counter unsat constraints
    resetUnsatConstraintsCtr();
    
    // Check if any constraint needs to be posted,
    // if so run post constraint
    if(!pPostConstraintSet.empty())
    {
      switch (postConstraints())
      {
        case Semantics::StoreConsistencyStatus::CONSISTENCY_FAILED:
        {
          // On failure:
          // 1 - add +1 to unsatisfied constraints
          // 2 - empty the queue of constraints
          // 3 - return
          addUnsatConstraints();
          cleanStatus();
          return StoreConsistencyStatus::CONSISTENCY_FAILED;
        }
        case Semantics::StoreConsistencyStatus::CONSISTENCY_SUBSUMED:
        default:
          // Continue after posting with dfs search
          break;
      }
    }
    
    // Start by the constraints with lower cost
    resetReevaluationCost();
    
    // Flags for returning event
    bool subsumed  { true  };
    bool fixpoint  { true  };
    bool noFixpoint{ false };
    
    // Loop on th set of constraints to reevaluate until failure
    // or fixpoint is reached
    Core::Constraint* constraint = getNextConstraintToReevaluate();
    if(!constraint)
    {
      // Notify backtrack manager since reevaluation cost has been reset
      notifyBacktrackManager();
      return StoreConsistencyStatus::CONSISTENCY_SUBSUMED;
    }
    
    Core::PropagationEvent event = Core::PropagationEvent::PROP_EVENT_SUBSUMED;
    do {
      // Propagate current constraint.
      // @note may trigger re-evaluation
      event = constraint->propagate();
      
      // Take actions according to propagation event
      switch (event)
      {
        case Core::PropagationEvent::PROP_EVENT_FAIL:
        {
          // On failure:
          // 1 - add +1 to unsatisfied constraints
          // 2 - empty the queue of constraints
          // 3 - return
          addUnsatConstraints();
          cleanStatus();
          return StoreConsistencyStatus::CONSISTENCY_FAILED;
        }
          break;
        case Core::PropagationEvent::PROP_EVENT_SUBSUMED:
        {
          // Move current constraint into set of subsumed constraints
          pSubsumedConstraintSet.insert(constraint->getUUID());
          
          // At least one subsumed
          fixpoint = false;
        }
          break;
        case Core::PropagationEvent::PROP_EVENT_FIXPOINT:
        {
          // At least one subsumed
          subsumed = false;
        }
          break;
        default:
          assert(event == Core::PropagationEvent::PROP_EVENT_NO_FIXPOINT ||
                 event == Core::PropagationEvent::PROP_EVENT_RUN_UNSPEC);
          noFixpoint = true;
          break;
      }
      
      // Get next constraint to reevaluate
      constraint = getNextConstraintToReevaluate();
    } while (constraint);
    
    // If not fail, reevaluation set should be empty
    assert(pCurrentReevaluationCost >= pPropagationCostOrderVector.size());
    
    // Notify backtrack manager of changes on constraint store
    notifyBacktrackManager();
    
    if(noFixpoint)
    {
      return StoreConsistencyStatus::CONSISTENCY_NO_FIXPOINT;
    }
    
    if(subsumed)
    {
      assert(!fixpoint);
      return StoreConsistencyStatus::CONSISTENCY_SUBSUMED;
    }
    
    assert(!subsumed);
    return StoreConsistencyStatus::CONSISTENCY_FIXPOINT;
  }//runConsistency

  std::pair<StoreConsistencyStatus, double> ConstraintStore::postOptimizationConstraints()
  {
    // Flags for returning event
    bool subsumed { true  };
    
    spp::sparse_hash_set<Core::UUIDCoreObj> subsumedConstraints;
    Core::PropagationEvent event = Core::PropagationEvent::PROP_EVENT_SUBSUMED;
    for (auto& cID : pPostConstraintSet)
    {
      assert(isConstraintRegistered(cID));
      auto constraint = pConstraintRegister.getConstraint(cID);
      
      // Skip all non optimization constraints
      if (!constraint->isOptimizationConstraint()) continue;
      
      // Post constraint.
      // @note it may trigger re-evaluation
      //       of constraint propagation
      event = constraint->post();
      
      // Take actions according to propagation event
      switch (event)
      {
        case Core::PropagationEvent::PROP_EVENT_FAIL:
        {
          pPostConstraintSet.clear();
          auto fitness = constraint->getFitness();
          return {StoreConsistencyStatus::CONSISTENCY_FAILED, fitness};
        }
        case Core::PropagationEvent::PROP_EVENT_SUBSUMED:
          // Move current constraint into set of subsumed constraints
          subsumedConstraints.insert(constraint->getUUID());
          break;
        case Core::PropagationEvent::PROP_EVENT_RUN_UNSPEC:
          // At least one subsumed
          subsumed = false;
          break;
        default:
          assert(event == Core::PropagationEvent::PROP_EVENT_NO_FIXPOINT);
          subsumed = false;
          break;
      }
    }
    
    // Remove all post constraints from set
    pPostConstraintSet.clear();
    
    // Remove all subsumed constraints from register
    for(auto& cID : subsumedConstraints)
    {
      assert(pConstraintRegister.isKeyRegistered(cID));
      pConstraintRegister.removeConstraint(cID);
    }
    
    auto fitness = Core::Constraint::FitnessMin;
    if(!subsumed)
    {
      return {StoreConsistencyStatus::CONSISTENCY_FIXPOINT, fitness};
    }
    
    return {StoreConsistencyStatus::CONSISTENCY_SUBSUMED, fitness};
  }//postOptimizationConstraints
  
  std::pair<StoreConsistencyStatus, double> ConstraintStore::runOptimizationConsistency()
  {
    // Check if any constraint needs to be posted,
    // if so run post constraint
    if (!pPostConstraintSet.empty())
    {
      const auto& postResult = postOptimizationConstraints();
      switch (postResult.first)
      {
        case Semantics::StoreConsistencyStatus::CONSISTENCY_FAILED:
        {
          cleanStatus();
          return postResult;
        }
        case Semantics::StoreConsistencyStatus::CONSISTENCY_SUBSUMED:
        default:
          // Continue after posting with dfs search
          break;
      }
    }
    
    // Start by the constraints with lower cost
    resetReevaluationCost();
    
    // Flags for returning event
    bool subsumed  { true  };
    bool fixpoint  { true  };
    bool noFixpoint{ false };
    
    // Loop on th set of constraints to reevaluate until failure
    // or fixpoint is reached
    auto fitness = Core::Constraint::FitnessMin;
    
    Core::Constraint* constraint = getNextConstraintToReevaluate();
    if (!constraint)
    {
      return {StoreConsistencyStatus::CONSISTENCY_SUBSUMED, fitness};
    }
    
    // Keep track of the current state of the set of subsumed constraints.
    // This state will be reinstantiate after this propagation
    auto subsumedConstraints = pSubsumedConstraintSet;
    
    Core::PropagationEvent event = Core::PropagationEvent::PROP_EVENT_SUBSUMED;
    do {
      // Skip if current constraint is not an optimization constraint.
      // TODO store all optimization constraints in their own queue and run this on that queue
      while (constraint && !constraint->isOptimizationConstraint())
      {
        constraint = getNextConstraintToReevaluate();
      }
      
      if (!constraint) break;
      
      // Here "constraint" is the first optimization constraint
      
      // Propagate current constraint.
      // @note may trigger re-evaluation
      event = constraint->propagate();
      
      // Take actions according to propagation event
      switch (event)
      {
        case Core::PropagationEvent::PROP_EVENT_FAIL:
        {
          // On failure:
          // 1 - empty the queue of constraints
          // 2 - return
          cleanStatus();
          fitness = constraint->getFitness();
          return {StoreConsistencyStatus::CONSISTENCY_FAILED, fitness};
        }
          break;
        case Core::PropagationEvent::PROP_EVENT_SUBSUMED:
        {
          // Keep track of which constraint is added as subsumed constraint
          pSubsumedConstraintSet.insert(constraint->getUUID());
          
          // At least one subsumed
          fixpoint = false;
        }
          break;
        case Core::PropagationEvent::PROP_EVENT_FIXPOINT:
        {
          // At least one subsumed
          subsumed = false;
        }
          break;
        default:
          assert(event == Core::PropagationEvent::PROP_EVENT_NO_FIXPOINT ||
                 event == Core::PropagationEvent::PROP_EVENT_RUN_UNSPEC);
          noFixpoint = true;
          break;
      }
      
      // Get next constraint to reevaluate
      constraint = getNextConstraintToReevaluate();
    } while (constraint);
    
    // If not fail, reevaluation set should be empty
    assert(pCurrentReevaluationCost >= pPropagationCostOrderVector.size());
    
    // Revert the set of subsumed constraints as it was before optimization consistency
    revertSubsumedConstraints(subsumedConstraints);
    
    if (noFixpoint)
    {
      return {StoreConsistencyStatus::CONSISTENCY_NO_FIXPOINT, fitness};
    }
    
    if (subsumed)
    {
      return {StoreConsistencyStatus::CONSISTENCY_SUBSUMED, fitness};
    }
    
    assert(!subsumed);
    return {StoreConsistencyStatus::CONSISTENCY_FIXPOINT, fitness};
  }//runOptimizationConsistency
  
}// end namespace Semantics
