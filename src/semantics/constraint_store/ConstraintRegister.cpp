// Self first
#include "ConstraintRegister.hpp"

#include "Constraint.hpp"

#include <vector>
#include <cassert>

namespace Semantics {
  
  void ConstraintRegister::clearRegister()
  {
    std::vector<Core::Constraint*> storedConstraints;
    for(const auto& it : pKeyStore)
    {
      storedConstraints.push_back(getConstraint(it));
    }
    
    // Clear the register
    deepClearRegister();
    
    // Store the constraints
    for(auto c : storedConstraints)
    {
      storeConstraint(c);
    }
  }//clearRegister
  
  void ConstraintRegister::deepClearRegister()
  {
    pRegister.clear();
    pKeyStore.clear();
  }//deepClearRegister
  
  void ConstraintRegister::registerConstraint(Core::Constraint* aConstraint)
  {
    assert(aConstraint);
    
    auto key = aConstraint->getUUID();
    if(isKeyRegistered(aConstraint->getUUID())) return;
    pRegister[key] = aConstraint;
  }//registerConstraint
  
  void ConstraintRegister::storeConstraint(Core::Constraint* aConstraint)
  {
    assert(aConstraint);
  
    pKeyStore.insert(aConstraint->getUUID());
    registerConstraint(aConstraint);
  }//storeConstraint
  
  Core::Constraint* ConstraintRegister::getConstraint(const RegisterKey& aKey) const
  {
    assert(isKeyRegistered(aKey));
    return pRegister.at(aKey);
  }//getConstraint
  
  void ConstraintRegister::removeConstraint(const RegisterKey& aKey)
  {
    if(pKeyStore.find(aKey) == pKeyStore.end())
    {
      pRegister.erase(aKey);
    }
  }//removeConstraint
  
  void ConstraintRegister::removeStoredConstraint(const RegisterKey& aKey)
  {
    pKeyStore.erase(aKey);
    removeConstraint(aKey);
  }//removeConstraint
  
}// end namespace Semantics

