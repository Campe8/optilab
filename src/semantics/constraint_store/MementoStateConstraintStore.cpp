// Base class
#include "MementoStateConstraintStore.hpp"

#include <cassert>

namespace Search {
  
  MementoStateConstraintStore::MementoStateConstraintStore(std::unique_ptr<Semantics::ConstraintStoreState> aConstraintStoreState)
  : MementoState(MementoStateId::MS_CONSTRAINT_STORE)
  , pConstraintStoreState(std::move(aConstraintStoreState))
  {
  }
  
  MementoStateConstraintStore::~MementoStateConstraintStore()
  {
  }
  
}// end namespace Search
