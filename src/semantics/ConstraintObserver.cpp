// Base class
#include "ConstraintObserver.hpp"

#include "Constraint.hpp"

namespace Core {
  ConstraintObserverEvent::ConstraintObserverEvent(const Constraint* aConstraint)
  : pConstraintObserverID(aConstraint->getUUID())
  {
  }
  
  ConstraintObserverEvent::~ConstraintObserverEvent()
  {
  }
}// end namespace Core

namespace Semantics {
  
  ConstraintObserver::ConstraintObserver(Core::UUIDCoreObj aConstraintID)
  : pConstraintID(aConstraintID)
  {
  }
  
  ConstraintObserver::~ConstraintObserver()
  {
  }
  
  ConstraintSubject::~ConstraintSubject()
  {
  }
  
  void ConstraintSubject::updateOnConstraintEvent(const Core::ConstraintObserverEvent* aConstraintEvent)
  {
    for (auto constraintObserver : pConstraintObserverSet)
    {
      triggerActionBasedOnEvent(constraintObserver, aConstraintEvent);
    }
  }//updateOnConstraintEvent
  
}// end namespace Semantics
