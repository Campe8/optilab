// Self first
#include "MetricsRegister.hpp"

#include <stdexcept>  // for invalid_argument

namespace Base { namespace Tools {
 
  void MetricsRegister::addMetric(const MetricId& aMetricId, MetricValue aValue)
  {
    if (isMetricRegistered(aMetricId))
    {
      throw std::invalid_argument(std::string("Metric ") + aMetricId + " already registered");
    }
    pRegister[aMetricId] = aValue;
  }//addMetric
  
  void MetricsRegister::updateMetric(const MetricId& aMetricId, MetricValue aValue)
  {
    if (!isMetricRegistered(aMetricId))
    {
      throw std::invalid_argument(std::string("Metric ") + aMetricId + " not registered");
    }
    pRegister[aMetricId] = aValue;
  }//updateMetric
  
  MetricsRegister::MetricValue MetricsRegister::getMetric(const MetricId& aMetricId)
  {
    if (!isMetricRegistered(aMetricId))
    {
      throw std::invalid_argument(std::string("Metric ") + aMetricId + " not registered");
    }
    return pRegister[aMetricId];
  }//getMetric
  
}}//end namespace Tools/Base
