// Self first
#include "MetricsManager.hpp"

#include "ObjectTools.hpp"
#include "ObjectHelper.hpp"

#include <stdexcept>  // for invalid_argument

namespace otools = Interpreter::ObjectTools;

namespace Base { namespace Tools {
  
  MetricsManager& MetricsManager::getInstance()
  {
    static MetricsManager instance;
    return instance;
  }//getInstance
  
  MetricsManager::MetricsManager()
  {
    for (int idx{0}; idx < static_cast<int>(MetricsCategory::LAST_METRICS_CATEGORY); ++idx)
    {
      pMetricsCollection[idx].IsActive  = false;
    }
  }
  
  void MetricsManager::clearMetrics()
  {
    for (auto& mreg : pMetricsCollection)
    {
      mreg.second.IsActive = false;
      mreg.second.Collection.clear();
    }
  }//clearMetrics
  
  void MetricsManager::enableMetricCategory(MetricsCategory aCategory, bool aEnabled)
  {
    assert(aCategory != MetricsCategory::LAST_METRICS_CATEGORY);
    pMetricsCollection.at(static_cast<int>(aCategory)).IsActive = aEnabled;
  }//enableMetricCategory
  
  void MetricsManager::addMetricsOnCategory(MetricsCategory aCategory, const std::string& aMId,
                                            const MetricsRegister::SPtr& aMReg)
  {
    assert(aCategory != MetricsCategory::LAST_METRICS_CATEGORY);
    if (!isCategoryEnabled(aCategory)) return;
    
    pMetricsCollection[static_cast<int>(aCategory)].Collection[aMId] = aMReg;
  }//addMetricsOnCategory
  
  Interpreter::BaseObject* MetricsManager::toObject() const
  {
    // Create a new object of type "Metrics"
    auto metricsObject = otools::createObject("Metrics");
    
    // Add the metrics as properties to the object
    for (const auto& mreg : pMetricsCollection)
    {
      if (!mreg.second.IsActive) continue;
      for (const auto& met : mreg.second.Collection)
      {
        // Create a property which is another (sub) object containing the metrics values
        auto metrics = otools::createObject("MetricsValues");
        for (const auto& metVals : *(met.second))
        {
          metrics->addProperty(metVals.first, static_cast<int>(metVals.second));
        }
        
        // Set the object as a property with the MetricsRegister key as key of the property
        Interpreter::DataObject metricsValuesObj;
        metricsValuesObj.setClassObject(std::shared_ptr<Interpreter::BaseObject>(metrics));
        metricsObject->addProperty(met.first, metricsValuesObj);
      }
    }
    
    return metricsObject;
  }//toObject
  
}}//end namespace Tools/Base

