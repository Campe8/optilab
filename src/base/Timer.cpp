// Self first
#include "Timer.hpp"

namespace Base { namespace Tools {
 
  Timer::Timer(bool aStart)
  {
    reset();
    if (aStart)
    {
      start();
    }
  }
  
  void Timer::start()
  {
    pTimePointStart = Clock::now();
    pTimePointCurrent = pTimePointStart;
    pRunning = true;
  }//start
  
  void Timer::stop()
  {
    updateTimeCounter();
    pRunning = false;
  }//stop
  
  void Timer::reset()
  {
    // Reset elapsed time
    pElapsedTime = pElapsedTime.zero();
    pRunning = false;
  }//reset
  
  void Timer::updateTimeCounter()
  {
    // Timer must be running to update the counter
    if (!pRunning) return;
    
    TimePoint timePointNow = Clock::now();
    
    // Add elapsed time as the difference between now and current time point,
    // i.e., the most recent updated time point
    pElapsedTime += std::chrono::duration_cast<MSec>(timePointNow - pTimePointCurrent);
    
    // Update the current time point to now
    pTimePointCurrent = timePointNow;
  }//updateTimeCounter
  
  uint64_t Timer::getWallClockTime()
  {
    // Update the elapsed time to this moment
    updateTimeCounter();
    return pElapsedTime.count();
  }//getWallClockTime
  
}}//end namespace Tools/Base
