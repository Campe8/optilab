// Base class
#include "ProgramOptions.hpp"

#include <cctype>     // for isalpha
#include <stdexcept>  // for std::logic_error

namespace po = boost::program_options;

static bool isValidShortName(char aName)
{
  return isalpha(aName);
}//isValidShortName

namespace Base {
  
ProgramOptions::ProgramOptions()
: pProgramOptions(std::make_shared<OptionsDescription>("OptiLab options"))
{
}

void ProgramOptions::registerOption(const std::string& aName, char aShortName, bool aRequired,
                                    const std::string& aOptInfoMessage)
{
  // Return for invalid option names
  if (aName.empty() || !isValidShortName(aShortName)) return;
  
  // Add short to long mapping to the lookup table
  pShortToNameOptLookup[aShortName] = aName;
  
  // Create the name used by boost to register the option
  std::string fullOptName = aName + "," + aShortName;
  if (aRequired)
  {
    getOptionDescription().add_options()(fullOptName.c_str(),
                                         po::value<std::vector<std::string>>()->composing(),
                                         aOptInfoMessage.c_str());
    
    pOptionsWithArgumentLookup[aName] = true;
  }
  else
  {
    getOptionDescription().add_options()(fullOptName.c_str(),aOptInfoMessage.c_str());
    pOptionsWithArgumentLookup[aName] = false;
  }
}//registerOption

void ProgramOptions::parseOptions(int argc, char* argv[])
{
  if (argc < 1)
  {
    throw std::logic_error("parseOptions: argc < 1");
  }
  
  if (!argv)
  {
    throw std::logic_error("parseOptions: argv is nullptr");
  }

  // Clear the map recording the option values to avoid stale states
  pOptionValuesMap.clear();
  try
  {
    // Create a parser
    po::command_line_parser parser(argc, argv);
    
    // Set the parser so it doesn't throw on unregistered options
    parser.options(getOptionDescription()).allow_unregistered();
    
    // Run the parser on the CLI input
    po::parsed_options parsedOptions = parser.run();
    
    // Store the parsed options into the map
    po::store(parsedOptions, pOptionValuesMap);
  }
  catch (const po::error& err)
  {
    throw std::logic_error(err.what());
  }
}//parseOptions

std::vector<std::string> ProgramOptions::getOptionValues(const std::string& aName) const
{
  if (!isOptionSelected(aName) || !isArgumentRequired(aName)) return std::vector<std::string>();
  return pOptionValuesMap.at(aName).as<std::vector<std::string>>();
}//getOptionValues

std::ostream& ProgramOptions::toStream(std::ostream& aStream)
{
  aStream << getOptionDescription();
  return aStream;
}//toStream

}// end namespace Base
