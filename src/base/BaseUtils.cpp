// Base class
#include "BaseUtils.hpp"

UUIDTag generateUUIDTag()
{
  static boost::uuids::random_generator generator;
  return generator();
}//generateUUIDTag

std::string generateRandomString()
{
  return boost::uuids::to_string(generateUUIDTag());
}//generateRandomString

void terminate(const char* aMsg, const char* aFileName, const std::size_t aLineNumber, const char* aCurrFun)
{
  std::cerr << aMsg << '\n';
  std::cerr << "at\n";
  std::cerr << '\t' << aFileName << '\n';
  std::cerr << '\t' << "line: " << aLineNumber << '\n';
  std::cerr << '\t' << "in: " << aCurrFun << '\n';
  std::terminate();
}//terminate
