// Self first
#include "BaseTools.hpp"

// Boost random
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/uuid/uuid.hpp>            // uuid class
#include <boost/uuid/uuid_generators.hpp> // generators
#include <boost/uuid/uuid_io.hpp>         // streaming operators etc.

#include <cassert>

namespace Base { namespace Tools {
  
  using UUIDTag = boost::uuids::uuid;
  
  // Generates UUIDTag
  static UUIDTag generateUUIDTag()
  {
    static boost::uuids::random_generator generator;
    return generator();
  }//generateUUIDTag
  
  std::string generateRandomString()
  {
    return boost::uuids::to_string(generateUUIDTag());
  }//generateRandomString
  
  static boost::random::mt19937& getGenerator()
  {
    // Static random generator
#ifdef PRODUCTION_CODE
    static std::time_t now = std::time(0);
    static boost::random::mt19937 rndGenerator{static_cast<std::uint32_t>(now)};
#else
    static boost::random::mt19937 rndGenerator{static_cast<std::uint32_t>(0)};
#endif
    return rndGenerator;
  }//getGenerator
  
  int randomNumberGenerator(int aLB, int aUB)
  {
    assert(aLB <= aUB);
    
    // Initialize the distribution
    boost::random::uniform_int_distribution<> dist(aLB, aUB);
    
    // Return a random sample
    return dist(getGenerator());
  }//randomNumberGenerator
  
  double randomRealNumberGenerator(double aLB, double aUB)
  {
    assert(aLB <= aUB);
    
    // Initialize the distribution
    boost::random::uniform_real_distribution<> dist(aLB, aUB);
    
    // Return a random sample
    return dist(getGenerator());
  }//randomRealNumberGenerator
  
  RandomGenerator::RandomGenerator(int aSeed)
  : pSeed(aSeed)
  , pGenerator(nullptr)
  {
    // Instantiate random generator
    if (aSeed < 0)
    {
      std::time_t now = std::time(0);
      pGenerator.reset(new boost::random::mt19937(static_cast<std::uint32_t>(now)));
    }
    else
    {
      pGenerator.reset(new boost::random::mt19937(static_cast<std::uint32_t>(pSeed)));
    }
  }
  
  int RandomGenerator::randInt(int aLB, int aUB) const
  {
    assert(aLB <= aUB);
    
    // Initialize the distribution
    boost::random::uniform_int_distribution<> dist(aLB, aUB);
    
    // Return a random sample
    return dist(generator());
  }//randInt
  
  double RandomGenerator::randReal(double aLB, double aUB) const
  {
    assert(aLB <= aUB);
    
    // Initialize the distribution
    boost::random::uniform_real_distribution<> dist(aLB, aUB);
    
    // Return a random sample
    return dist(generator());
  }//randReal
  
  SimpleRandomGenerator::SimpleRandomGenerator()
  : pRng(pRd())
  {
  }
  
  SimpleRandomGenerator::SimpleRandomGenerator(const SimpleRandomGenerator &other)
  : pRng(pRd())
  {
  }
  
  /// Copy assignment operator following the copy-and-swap idiom.
  SimpleRandomGenerator& SimpleRandomGenerator::operator=(SimpleRandomGenerator other)
  {
    this->swap( other );
    return *this;
  }
  
}}//end namespace Tools/Base
