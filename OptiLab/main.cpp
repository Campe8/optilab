//
//  Copyright OptiLab 2016-2019. All rights reserved.
//  OptiLab entry point.
//
//  Created by Federico Campeotto on 8/13/16.
//

#include "InitSys.hpp"

#define UTEST

#ifdef UTEST

#include "utest.hpp"

GTEST_API_ int main(int argc, char* argv[]) {
  
#else
  
int main (int argc, char* argv[]) {
  
#endif
  
  Init::InitSystem sys(argc, argv);
  return sys.runSystem();
}
