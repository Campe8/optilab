# README #

### What is this repository for? ###

* OptiLab is a solver for satisfaction and optimization problems.
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Read this tutorial to know how to use git and Bitbucket: https://www.atlassian.com/git/tutorials/setting-up-a-repository

* Clone repository every time you want to start working on new features (and delete it after the push): 
git clone https://<your_bitbucket_username>@bitbucket.org/Campe8/optilab.git

* Installation Steps
To "out of source" build OptiLab do the following steps:

1. mkdir build (this creates "build" sub-directory at the top level directory)

2. cd build

3. cmake ../

4. make -j<N> (substitue N with the number of your cpu supported threads, including hyperthreaded ones)

If the installation succeed then all the generated files will be
in the "build" folder along with the executable. 

* Uninstall
To remove all the contents installed type the following

1. cd build

2. make clean

3. rm -rf *

### How do I get contribute? ###

Have a look at the Wiki page where there is a list of todo(s) before the new release of OptiLab.
For more information about OptiLab, check it out at www.optilabtech.com.

To work on a feature without messing up with the remote master branch do the following from the master branch:

1. git checkout -b my_feature
1. work on my feature...
1. git commit (first add/stage the files you want to push)
1. git checkout master
1. git pull
1. git checkout my_feature
1. git rebase master
1. git checkout master
1. git merge my_feature
1. git push origin master

A good tool to have that would help you with git (if you are working on mac or windows) is "fork".
See here https://git-fork.com/ for more information.

### CMake building guide ###
It's now available a guide that helps
[Building out of source with CMake](https://bitbucket.org/Campe8/optilab/wiki/Building%20out%20of%20source%20with%20CMake)

### Windows support ###
It's now available a guide that helps 
[Building out of source in Windows](https://bitbucket.org/Campe8/optilab/wiki/Building%20out%20of%20Source%20in%20Windows)

### Preparing an image for AWS lambda ###
* Setup

1. Download and install docker from https://www.docker.com/ (free).

2. Install the aws cli (see instructions on aws website). For Mac use brew (brew install awscli)

* Preparing the image
Once the docker is running, run the following steps:

1. docker pull amazonlinux (pulls the linux image into the docker)

2. docker run -v <global_path_to_optilab>:/working -it amazonlinux:latest /bin/bash (this will make OptiLab available under the folder "working")

3. yum install gcc-c++

4. yum install wget

5. yum install tar

6. yum install gzip

7. yum install make

8. yum install bison

9. yum install flex

10. yum install readline-devel

11. mkdir cmake

12. cd cmake

13. wget https://cmake.org/files/v3.10/cmake-3.10.0.tar.gz (download cmake 3.10 needed by OptiLab)

14. tar -xvzf cmake-3.10.0.tar.gz

15. cd cmake-3.10.0

16. ./bootstrap

17. make

18. make install (now cmake is present in the environment)

19. cd ..; cd ..; cd working

20. follow the instructions for building OptiLab (mkdir build, cd build, etc., see above)

Part of the installation commands above can be replaced with

yum -y update;\

yum -y groupinstall "Development Tools"

To save the image of the docker exit the docker (exit) and run

sudo docker ps -a

to get the container ID. Now, save the changes as a new image with given name by running the following command:

sudo docker commit <id_like_df888c72ed5a> <docker-image-name>

To verify new image is running or not, Run:

sudo docker images

### Contribution guidelines ###

* Writing tests
Every new module, component or file added must be UNIT TESTED and 
the code must be covered.
Moreover, it is advisable to check the complexity complexity of the 
code which MUST be under 10.
Check http://oclint.org/ for a cyclomatic complexity tool.

* Code review
Before submitting any code send a code review request to the owners of
this repository.

### Other guidelines ###

* Use C++11 NOT C style programming.

* Follow the current syntax if possible, i.e., CamelCase for classes, arguments name for functions with 'a' as a prefix, private members with 'p' as a prefix, etc.

* Exported classes, i.e., classes with API that should be linked and used by other components must have the EXPORT macro and the interface header should be places in the export folder. Moreover, the file name should be present in the correspondend headers.mk file.

### Who do I talk to? ###

* Repo owners