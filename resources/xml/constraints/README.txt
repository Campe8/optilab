<!-- This is the specification template for constraints -->
<!-- Following there is an example and comments on how
a constraint specification should look like.
 -->
<?xml version="1.0" encoding="utf-8"?>
<ModelObject type="Constraint">
  <!-- Constraint name, e.g., nq, lin_eq, alldiff. -->
	<name>nq</name>
  <!-- Is a global constraint? -->
	<global>false</global>
  <!-- Is a reified constraint? -->
	<reif>false</reif>
  <!-- Following there is the ordered list of all
   the constraint arguments.
   -->
	<arguments>
    <!-- First argument... -->
		<arg>
      <!-- An argument may have different types.
       Following there is the list of all the types
       an argument can have. For example, var, int, bool, etc.
       In this example, the first constrain argument can be
       either a variable or an integer.
       -->
			<type>var</type>
			<type>int</type>
		</arg>
    <!-- Second argument... -->
		<arg>
      <!-- The attribute "array" says that
       the argument can be an array of the specified types.
       @note only array-type arguments can have multiple
       types in their declaraton.
       For example, this argument can be an array of
       mixed arguments: variables, integers and booleans.
       Furthermore, this argument can be an integer.
       -->
      <type array="true">var_int_bool</type>
      <type>int</type>
		</arg>
    <!-- More arguments here... -->
	</arguments>
    // Specify further constraints on the input arguments
    <constraints>
      // samesize constraints the size of the arguments
      <samesize>
        // constraints argument 1 and 2 to have same size
        <arg>1 2</arg>
      </samesize>
  </constraints>
	<syntax>
    <!-- Syntax description of how to post the constraint -->
    nq(v1, v2)
	</syntax>
	<description>
    <!-- Description of the constraint -->
    Posts a "not equal" constraint between
    the first argument and the second argument.
	</description>
	<example>
    <!-- Example of how to post this constraint -->
    nq(x, y)
	</example>
</ModelObject>
