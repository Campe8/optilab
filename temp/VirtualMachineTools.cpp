// Self first
#include "VirtualMachine.hpp"

#include "OPCode.hpp"
#include "CodeObjectDefs.hpp"
#include "InlineFcn.hpp"

#include <algorithm>
#include <iterator>
#include <numeric>
#include <stdlib.h>
#include <cassert>

#define UNUSED_ARG(x) (void)(x)

//#define INTERPRETER_DEBUG

namespace iutils = Interpreter::Utils;

namespace Interpreter { namespace Tools {
  
  
  // Returns true if the given DataObject represents a domain declaration.
  // false otherwise
  bool isDomainDeclarationDataObject(const DataObject& aDO)
  {
    if(aDO.empty()) return false;
    if(!aDO.isFcnObject() && aDO.isComposite())
    {
      assert(!aDO.getPayload().empty() && aDO.getPayload()[0].dataObjectType() == DataObject::DOT_INT);
      assert(aDO.getPayload()[0].getDataValue<int>() == MDL_OBJ_TYPE_DOM);
      if(aDO.dataObjectType() == DataObject::DataObjectType::DOT_INT) return true;
    }
    return false;
  }//isDomainDeclarationDataObject
  
  // Returns true if the given DataObject represents an array or matrix declaration.
  // false otherwise
  bool isMatrixDeclarationDataObject(const DataObject& aDO)
  {
    if(aDO.getPayload().empty()) return false;
    if(aDO.getPayload()[0].dataObjectType() != DataObject::DOT_INT) return false;
    return aDO.getPayload()[0].getDataValue<int>() == MDL_OBJ_TYPE_LIST;
  }//isMatrixDeclarationDataObject
  
  // Checks the dimensions of the object which is supposed to be the payload of
  // a matrix domain object
  void checkMatrixDomainDimensions(const DataObject& aDO)
  {
    for(const auto& dim : aDO)
    {
      assert(dim.dataObjectType() == DataObject::DOT_INT);
      itp_assert(dim.getDataValue<int>() >= 0, InterpreterException::ExceptionType::ET_BAD_ALLOC);
    }
  }//checkMatrixDimensions
  
  // Returns the size of the matrix domain object given in input
  std::size_t getMatrixDomainSize(const DataObject& aDO)
  {
    std::size_t size{1};
    assert(aDO.composeSize() > 0);
    for(const auto& dim : aDO)
    {
      assert(dim.dataObjectType() == DataObject::DOT_INT);
      auto dimSize = dim.getDataValue<int>();
      if(dimSize > 0) size *= dimSize;
    }
    
    return size;
  }//getDomainMatrixSize
  
  DataObject createVariableDataObject(const std::string& aVarName, int aLB, int aUB, int aVarSpec)
  {
    // @todo use a unique function together with BUILD_DOMAIN
    // to avoid duplicate code
    int domType = DOM_BOUNDS;
    if(aUB < aLB) std::swap(aLB, aUB);
    if(aLB == aUB) domType = DOM_SINGLETON;
    
    DataObject var(domType);
    if(domType == DOM_SINGLETON)
    {
      var.compose({DataObject(aLB)});
    }
    else
    {
      var.compose({DataObject(aLB), DataObject(aUB)});
    }
    
    // Domain type
    var.getPayload().push_back(DataObject(static_cast<int>(MDL_OBJ_TYPE_DOM)));
    // Dimensions
    var.getPayload().push_back(DataObject());
    // Matrix of domains
    var.getPayload().push_back(DataObject(false));
    // Variable specification
    var.getPayload().push_back(DataObject(aVarSpec));
    // Variable name
    var.getPayload().push_back(DataObject(aVarName.c_str()));
    
    return var;
  }//createVariableDataObject
  
}}// end namespace Interpreter/Tools
