//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 1/23/2018
//
// Virtual machine implementing the
// interpreter of the OptiLab language.
// Manages the call stack of frames and contains
// a mapping of instructions to operations.
//

#pragma once

#include "InterpreterExportDefs.hpp"

#include "DataObject.hpp"

#include <string>

namespace Interpreter { namespace Tools {
  
  /// Returns a data object representing a variable with domaind [aLB, aUB]
  INTERPRETER_EXPORT_FUNCTION DataObject createVariableDataObject(const std::string& aVarName, int aLB, int aUB, int aVarSpec=0);
  
  /// Returns true if the given DataObject represents a domain declaration.
  /// Returns false otherwise
  INTERPRETER_EXPORT_FUNCTION bool isDomainDeclarationDataObject(const DataObject& aDO);
  
  /// Returns true if the given DataObject represents an array or matrix declaration.
  /// Returns false otherwise
  INTERPRETER_EXPORT_FUNCTION bool isMatrixDeclarationDataObject(const DataObject& aDO);
  
  // Checks the dimensions of the object which is supposed to be the payload of
  // a matrix domain object
  INTERPRETER_EXPORT_FUNCTION void checkMatrixDomainDimensions(const DataObject& aDO);
  
  // Returns the size of the matrix domain object given in input
  INTERPRETER_EXPORT_FUNCTION std::size_t getMatrixDomainSize(const DataObject& aDO);
  
}} // end namespace Interpreter/Tools
