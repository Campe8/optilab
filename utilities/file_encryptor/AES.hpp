//
// Copyright OptiLab 2018. All rights reserved.
//
// AES encryption algorithm.
// @note this code has been taken 1-to-1 from
// https://github.com/calccrypto/Encryptions
//

#pragma once

#include <cstdint>  // for uint32_t
#include <cstdio>   // for std::size_t
#include <string>
#include <vector>

class AES {
public:
  /// Constructor for a new instance of the encryption algorithm.
  /// It takes "aKey" as key for the AES encryption.
  /// @note throws std::runtime_error if key is not long 16, 24, or 32 bytes
  AES(const std::string& aKey);
  
  /// Returns the encrypted message "aMsg" according to AES algorithm with given key.
  /// @note message can have any length
  std::string encryptMsg(const std::string& aMsg);
  
  /// Returns the decrypted message "aMsg" according to AES algorithm with given key.
  /// @note message should have been previously encoded with "encryptMsg(...)".
  /// @note message must be have size which is a multiple of 16 chars
  std::string decryptMsg(const std::string& aMsg);
  
  /// Returns the encrypted data "aData" according to AES algorithm with given key.
  /// @note throws std::runtime_error if data is not 128 bit long
  std::string encrypt(const std::string& aData);
  
  /// Decrypts and returns the decrypted data "aData" according to AES algorithm with given key.
  /// @note throws std::runtime_error if data is not 128 bit long
  std::string decrypt(const std::string& aData);
  
private:
  /// Rounds of encryption and decryption
  std::size_t pRounds;
  
  /// Columns of the arrays used for encryption and decryption
  std::size_t pColumns;
  
  /// Index used to prepare the key for the AES algorithm
  std::size_t pIndex;
  
  /// Keys used for AES algorithm
  std::vector<std::vector<uint32_t>> pKeys;
  
  /// Utility function: sets the internal keys used by the AES algorithm
  void setkey(const std::string& aKey);
};

  

