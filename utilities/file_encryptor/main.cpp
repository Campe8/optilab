#include <dirent.h>

#include <cassert>
#include <fstream>
#include <iostream>
#include <streambuf>
#include <string>

#include "AES.hpp"

int runOnFile(const std::string aKey, const std::string& aInFile, const std::string& aOutFile)
{
  std::ifstream inFileStream(aInFile.c_str());
  if (!inFileStream.is_open())
  {
    std::cerr << "Cannot open " << aInFile << std::endl;
    return 2;
  }
  
  // Save the content of the file into a string
  std::string fileContent((std::istreambuf_iterator<char>(inFileStream)),
                          std::istreambuf_iterator<char>());
  
  inFileStream.close();
  
  // Encode the content of the file
  AES aes(aKey);
  
  // Store the content into a new file
  std::ofstream outFile{aOutFile.c_str()};
  
  // Store the encoded content
  outFile << aes.encryptMsg(fileContent);
  
  // Close the output file
  outFile.close();
  
  return 0;
}//runOnFile

int runOnFolder(const std::string aKey, const std::string& aInFolder)
{
  DIR* dir;
  struct dirent *ent;
  if ((dir = opendir(aInFolder.c_str())) != nullptr)
  {
    std::size_t ctr{0};
    while ((ent= readdir(dir)) != nullptr)
    {
      std::string fileName(ent->d_name);
      if (fileName.size() < 5)
      {
        continue;
      }
      if (fileName.substr(fileName.size() - 3, 3) != "xml")
      {
        continue;
      }
      
      fileName = aInFolder + "/" + fileName;
      
      
      std::string outFileName = "r" + std::to_string(ctr++);
      runOnFile(aKey, fileName, outFileName);
    }
    
    std::cout << "Processed " << ctr << " files\n";
    closedir(dir);
  }
  else
  {
    std::cerr << "Cannot open directory " << aInFolder << std::endl;
    return 2;
  }
  
  return 0;
}//runOnFolder

int main(int argc, char* argv[]) {
  
  if ((argc != 3) && (argc != 4))
  {
    std::cout << "Usage:\n";
    std::cout << argv[0] << " <key> <input_folder>\n";
    std::cout << "or\n";
    std::cout << argv[0] << " <key> <input_file> <output_file_name>\n";
    return 1;
  }
  
  int res{0};
  if (argc == 3)
  {
    res = runOnFolder(std::string(argv[1]), std::string(argv[2]));
  }
  else
  {
    assert(argc == 4);
    res = runOnFile(std::string(argv[1]), std::string(argv[2]), std::string(argv[3]));
  }
  
  return res;
}
