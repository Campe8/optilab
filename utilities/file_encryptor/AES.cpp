// Self first
#include "AES.hpp"

#include <cassert>
#include <stdexcept>    // for std::runtime_error
#include <sstream>
#include <type_traits>  // for std::is_integral

#include "AESConst.hpp"
#include <iostream>
namespace {
  
  uint64_t toInt(const std::string& aStr, const int& aBase)
  {
    // Changees strings to uint64_t
    uint64_t value = 0;
    switch (aBase)
    {
      case 2:
        for (const unsigned char c : aStr)
        {
          value = (value << 1) + (static_cast <uint8_t> (c) - '\x30');
        }
        break;
      case 8:
        std::stringstream(aStr) >> std::oct >> value;
        break;
      case 10:
        std::stringstream(aStr) >> std::dec >> value;
        break;
      case 16:
        std::stringstream(aStr) >> std::hex >> value;
        break;
      case 256:
        for (const unsigned char c : aStr)
        {
          value = (value << 8) + static_cast<uint8_t>(c);
        }
        break;
      default:
        std::stringstream ss; ss << std::dec << aBase;
        throw std::runtime_error("Error: toint() undefined for base: " + ss.str());
        break;
    };
    return value;
  }//toInt
  
  // From a Twofish code (modified)
  // Bitwise rotation to the right
  template <typename T>
  T ROR(T x, const uint64_t & n, const uint64_t & bits){
    static_assert(std::is_integral <T>::value, "Error: Value being rotated should be integral.");
    return (x >> n) | ((x & ((1ULL << n) - 1)) << (bits - n));
  }
  
  /// Rotate Left
  /// Bitwise rotation to the left
  template <typename T>
  T ROL(const T & x, const uint64_t & n, const uint64_t & bits){
    static_assert(std::is_integral <T>::value, "Error: Value being rotated should be integral.");
    return ROR(x, bits - n, bits);
  }
  
  void shiftRow(std::vector<uint32_t> & data)
  {
    std::vector <uint32_t> temp;
    for(uint8_t x = 0; x < 4; x++){
      temp.push_back(0);
      for(uint8_t y = 0; y < 4; y++){
        temp[x] += ((data[y] >> ((3 - x) << 3)) & 255) << ((3 - y) << 3);
      }
    }
    data = temp;
    
    for(uint8_t x = 0; x < 4; x++){
      data[x] = ROL(data[x], x << 3, 32);
    }
    
    temp.clear();
    for(uint8_t x = 0; x < 4; x++){
      temp.push_back(0);
      for(uint8_t y = 0; y < 4; y++){
        temp[x] += ((data[y] >> ((3 - x) << 3)) & 255) << ((3 - y) << 3);
      }
    }
    data = temp;
  }//shiftRow
  
  uint8_t GF(uint8_t a, uint8_t b)
  {
    /*Rijndael Finite Field multiplication
     Fast but Vulnerable to Timing Attacks
     From Wikipedia*/
    uint8_t prim = 0x1b;
    uint8_t p = 0, i = 0;
    while ((i < 8) && (a != 0) && (b != 0)){
      if (b & 1){
        p ^= a;
      }
      uint8_t hi = a & 0x80;
      a = (a << 1) & 255;
      if (hi){
        a ^= prim;
      }
      b >>= 1;
      i += 1;
    }
    return p;
  }
  
  void mixColumns(std::vector<uint32_t>& data)
  {
    std::vector <uint32_t> temp;
    for (uint8_t i = 0; i < 4; i++)
    {
      temp.push_back( ((GF(2, (data[i] >> 24) & 255) ^ GF(3, (data[i] >> 16) & 255) ^
                        ((data[i] >> 8) & 255) ^ (data[i] & 255)) << 24) +
                     ((GF(2, (data[i] >> 16) & 255) ^ GF(3, (data[i] >> 8) & 255) ^
                       (data[i] & 255) ^ ((data[i] >> 24) & 255)) << 16) +
                     ((GF(2, (data[i] >> 8) & 255) ^ GF(3, data[i] & 255) ^
                       ((data[i] >> 24) & 255) ^ ((data[i] >> 16) & 255)) << 8 ) +
                     ((GF(2, data[i] & 255) ^ GF(3, (data[i] >> 24) & 255) ^
                       ((data[i] >> 16) & 255) ^ ((data[i] >> 8) & 255))));
    }
    
    data = temp;
  }//mixColumns
  
  void invmixcolumns(std::vector <uint32_t> & data)
  {
    std::vector <uint32_t> temp;
    for(uint8_t i = 0; i < 4; i++){
      temp.push_back( ((GF(14, (data[i] >> 24) & 255) ^ GF(9, (data[i] & 255)) ^
                        GF(13, (data[i] >> 8) & 255) ^ GF(11, (data[i] >> 16) & 255)) << 24) +
                     ((GF(14, (data[i] >> 16) & 255) ^ GF(9, (data[i] >> 24) & 255) ^
                       GF(13, (data[i] & 255)) ^ GF(11, (data[i] >> 8) & 255)) << 16) +
                     ((GF(14, (data[i] >> 8) & 255) ^ GF(9, (data[i] >> 16) & 255) ^
                       GF(13, (data[i] >> 24) & 255) ^ GF(11, (data[i] & 255))) << 8 ) +
                     ((GF(14, (data[i] & 255)) ^ GF(9, (data[i] >> 8) & 255) ^
                       GF(13, (data[i] >> 16) & 255) ^ GF(11, (data[i] >> 24) & 255))));
    }
    data = temp;
  }//invmixcolumns

  template <typename T>
  std::string makehex(T value, unsigned int size = 2 * sizeof(T), bool caps = false)
  {
    if (!size){
      std::stringstream out;
      out << std::hex << value;
      return out.str();
    }
    
    std::string out(size, '0');
    while (value && size){
      if (caps){
        out[--size] = "0123456789ABCDEF"[value & 15];
      }
      else{
        out[--size] = "0123456789abcdef"[value & 15];
      }
      value >>= 4;
    }
    return out;
  }
  
  std::string unhexlify(const std::string & in)
  {
    // Reverse hexlify
    if (in.size() & 1){
      throw std::runtime_error("Error: input string of odd length.");
    }
    std::string out(in.size() >> 1, 0);
    for(unsigned int x = 0; x < in.size(); x += 2){
      if (('0' <= in[x]) && (in[x] <= '9')){
        out[x >> 1] = static_cast <uint8_t> ((in[x] - '0') << 4);
      }
      else if(('a' <= in[x]) && (in[x] <= 'f')){
        out[x >> 1] = static_cast <uint8_t> ((in[x] - 'a' + 10) << 4);
      }
      else if(('A' <= in[x]) && (in[x] <= 'F')){
        out[x >> 1] = static_cast <uint8_t> ((in[x] - 'A' + 10) << 4);
      }
      else{
        throw std::runtime_error("Error: Invalid character found: " + std::string(1, in[x]));
      }
      if (('0' <= in[x + 1]) && (in[x + 1] <= '9')){
        out[x >> 1] |= static_cast <uint8_t> (in[x + 1] - '0');
      }
      else if(('a' <= in[x + 1]) && (in[x + 1] <= 'f')){
        out[x >> 1] |= static_cast <uint8_t> (in[x + 1] - 'a' + 10);
      }
      else if(('A' <= in[x + 1]) && (in[x + 1] <= 'F')){
        out[x >> 1] |= static_cast <uint8_t> (in[x + 1] - 'A' + 10);
      }
      else{
        throw std::runtime_error("Error: Invalid character found: " + std::string(1, in[x + 1]));
      }
    }
    return out;
  }
  
  void invshiftrow(std::vector<uint32_t>& data)
  {
    std::vector <uint32_t> temp;
    for(uint8_t x = 0; x < 4; x++){
      temp.push_back(0);
      for(uint8_t y = 0; y < 4; y++){
        temp[x] += ((data[y] >> ((3 - x) << 3)) & 255) << ((3 - y) << 3);
      }
    }
    data = temp;
    
    for(uint8_t x = 0; x < 4; x++){
      data[x] = ROR(data[x], x << 3, 32);
    }
    
    temp.clear();
    for(uint8_t x = 0; x < 4; x++){
      temp.push_back(0);
      for(uint8_t y = 0; y < 4; y++){
        temp[x] += ((data[y] >> ((3 - x) << 3)) & 255) << ((3 - y) << 3);
      }
    }
    data = temp;
  }//invshiftrow
  
  std::string OUT(std::vector <uint32_t>& data)
  {
    std::string out = "";
    for (uint8_t x = 0; x < 4; x++)
    {
      out += makehex(data[x], 8);
    }
    return unhexlify(out);
  }
    
}  // namespace

AES::AES(const std::string& aKey)
{
  setkey(aKey);
}

void AES::setkey(const std::string& aKey)
{
  auto keySize = aKey.size();
  if ((keySize != 16) && (keySize != 24) && (keySize != 32))
  {
    throw std::runtime_error("Error: Key size does not fit defined sizes: " +
                             std::to_string(keySize));
  }
  
  pRounds = keySize / 4 + 6;
  pColumns = pRounds - 6;
  pIndex = (pRounds + 1) << 4;
  keySize >>= 2;
  
  std::vector<uint32_t> key;
  for (int col = 0; col < pColumns; col++)
  {
    key.push_back(static_cast<uint32_t>(toInt(aKey.substr(col << 2, 4), 256)));
  }
  
  int8_t idx = 1;
  while ((key.size() << 2) < pIndex)
  {
    uint32_t t = ROL(key[key.size() - 1], 8, 32);
    uint32_t s = 0;
    
    for (uint8_t j = 0; j < 4; j++)
    {
      s += AESSubbytes[static_cast<uint8_t>(t >> (j << 3))] << (j << 3);
    }
    
    t = s ^ key[key.size() - keySize];
    t ^= ((1 << (idx++ - 1)) % 229) << 24;
    key.push_back(t);
    
    for (uint8_t j = 0; j < 3; j++)
    {
      key.push_back(key[key.size() - 1]^ key[key.size() - keySize]);
    }
    
    if (keySize == 8)
    {
      s = 0;
      for(uint8_t j = 0; j < 4; j++)
      {
        s += AESSubbytes[static_cast <uint8_t> (key[key.size() - 1] >> (j << 3))] << (j << 3);
      }
      key.push_back(s ^ key[key.size() - keySize]);
    }
    
    for (uint8_t j = 0; j < 0 * (keySize == 4) + 2 * (keySize == 6) + 3 * (keySize == 8); j++)
    {
      key.push_back(key[key.size() - 1] ^ key[key.size() - keySize]);
    }
  }//while
  
  for (uint8_t j = 0; j < (pIndex >> 4); j++)
  {
    std::vector<uint32_t> temp;
    for (uint8_t k = 0; k < 4; k++)
    {
      temp.push_back(key[(j << 2) + k]);
    }
    pKeys.push_back(temp);
  }
}//setkey

std::string AES::encryptMsg(const std::string& aMsg)
{
  if (aMsg.empty())
  {
    return aMsg;
  }
  
  const std::size_t blockSize{16};
  if (aMsg.size() <= blockSize)
  {
    auto eMsg = aMsg;
    eMsg += std::string(blockSize - aMsg.length(), ' ');
    assert(eMsg.size() == blockSize);
    
    return encrypt(eMsg);
  }
  
  // Split the string in blocks of 16 chars and encode them
  std::string eMsg;
  for (unsigned iter = 0; iter < aMsg.length(); iter += blockSize)
  {
    auto subStr = aMsg.substr(iter, blockSize);
    if (subStr.size() < blockSize)
    {
      subStr += std::string(blockSize - subStr.length(), ' ');
    }
    assert(subStr.size() == blockSize);
    
    eMsg += encrypt(subStr);
  }
  return eMsg;
}//encryptMsg

std::string AES::decryptMsg(const std::string& aMsg)
{
  const std::size_t blockSize{16};
  assert((aMsg.size() % blockSize) == 0);
  
  std::string dMsg;
  for (unsigned iter = 0; iter < aMsg.length(); iter += blockSize)
  {
    auto subStr = aMsg.substr(iter, blockSize);
    assert(subStr.size() == blockSize);
    
    dMsg += decrypt(subStr);
  }
  
  // Trim the end
  while (dMsg.back() == ' ')
  {
    dMsg.pop_back();
  }
  
  return dMsg;
}//decryptMsg

std::string AES::encrypt(const std::string& aData)
{
  if (aData.size() != 16)
  {
    throw std::runtime_error("Error: Data must be 128 bits long.");
  }
  
  std::vector<uint32_t> data;
  for (uint8_t x = 0; x < 4; x++)
  {
    std::string d = "";
    for (uint8_t y = 0; y < 4; y++)
    {
      d += aData[(x << 2) + y];
    }
    data.push_back(static_cast <uint32_t> (toInt(d, 256)));
  }
  
  for (uint8_t x = 0; x < 4; x++)
  {
    data[x] ^= pKeys[0][x];
  }
  
  for (uint8_t r = 1; r < pRounds; r++)
  {
    for (uint8_t x = 0; x < 4; x++)
    {
      data[x] = (AESSubbytes[data[x] >> 24] << 24) +
      (AESSubbytes[(data[x] >> 16) & 255] << 16) +
      (AESSubbytes[(data[x] >> 8) & 255] << 8) +
      AESSubbytes[data[x] & 255];
    }
    shiftRow(data);
    mixColumns(data);
    
    for (uint8_t x = 0; x < 4; x++)
    {
      data[x] ^= pKeys[r][x];
    }
  }
  
  for (uint8_t x = 0; x < 4; x++)
  {
    data[x] = (AESSubbytes[data[x] >> 24] << 24) +
    (AESSubbytes[(data[x] >> 16) & 255] << 16) +
    (AESSubbytes[(data[x] >> 8) & 255] << 8) +
    AESSubbytes[data[x] & 255];
  }
  shiftRow(data);
  
  for (uint8_t x = 0; x < 4; x++)
  {
    data[x] ^= pKeys[pRounds][x];
  }
  
  return OUT(data);
}//encrypt

std::string AES::decrypt(const std::string & aData)
{
  if (aData.size() != 16)
  {
    throw std::runtime_error("Error: Data must be 128 bits long.");
  }
  std::reverse(pKeys.begin(), pKeys.end());
  
  std::vector <uint32_t> data;
  for(uint8_t x = 0; x < 4; x++){
    std::string d = "";
    for(uint8_t y = 0; y < 4; y++){
      d += aData[(x << 2) + y];
    }
    data.push_back(static_cast <uint32_t> (toInt(d, 256)));
  }
  
  for(uint8_t x = 0; x < 4; x++)
  {
    data[x] ^= pKeys[0][x];
  }
  
  for(uint8_t r = 1; r < pRounds; r++)
  {
    invshiftrow(data);
    for(uint8_t x = 0; x < 4; x++)
    {
      data[x] = (AESInvSubbytes[data[x] >> 24] << 24) +
      (AESInvSubbytes[(data[x] >> 16) & 255] << 16) +
      (AESInvSubbytes[(data[x] >> 8) & 255] << 8) +
      AESInvSubbytes[data[x] & 255];
    }
    
    for(uint8_t x = 0; x < 4; x++){
      data[x] ^= pKeys[r][x];
    }
    invmixcolumns(data);
  }
  
  invshiftrow(data);
  
  for(uint8_t x = 0; x < 4; x++){
    data[x] = (AESInvSubbytes[data[x] >> 24] << 24) +
    (AESInvSubbytes[(data[x] >> 16) & 255] << 16) +
    (AESInvSubbytes[(data[x] >> 8) & 255] << 8) +
    AESInvSubbytes[data[x] & 255];
  }
  
  for (uint8_t x = 0; x < 4; x++)
  {
    data[x] ^= pKeys[pRounds][x];
  }
  
  std::reverse(pKeys.begin(), pKeys.end());
  return OUT(data);
}//decrypt
