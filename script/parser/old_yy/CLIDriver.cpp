// Self first
#include "CLIDriver.hpp"

#include "OPCode.hpp"

#include <cctype>
#include <fstream>
#include <algorithm>
#include <sstream>
#include <numeric>
#include <limits>
#include <iterator>
#include <exception>
#include <cassert>

#define MAX_REP_NUM 65536

namespace CLI {
  
  // Returns true if the given string is a quoted string,
  // false otherwise.
  // For example:
  // \"abc\"
  // is a quoted string
  static bool isQuotedString(const std::string& aString)
  {
    if (aString.size() < 2 || aString[0] != '"' || aString[aString.size() - 1] != '"')
    {
      return false;
    }
    return true;
  }//isQuotedString
  
  static CLIDriver::byte_T intToByte(int aInt)
  {
    assert(aInt >= 0 && aInt <= 256);
    CLIDriver::byte_T b = aInt & 0xFF;
    return b;
  }//intToByte
  
  static CLIDriver::ByteArray intToByteArray(int aInt)
  {
    assert(aInt >= 0 && aInt <= 65536);
    CLIDriver::ByteArray bytes(2, 0);
    bytes[1] = (aInt >> 8) & 0xFF;
    bytes[0] = aInt & 0xFF;
    
    return bytes;
  }//intToByte
  
  static int byteToInt(CLIDriver::byte_T aByte)
  {
    return static_cast<int>(aByte);
  }//byteToInt
  
  static CLI_P_INT getNegInfinity()
  {
    static CLI_P_INT negInf = static_cast<CLI_P_INT>(std::numeric_limits<int>::min());
    return negInf;
  }//getNegInfinity
  
  static CLI_P_INT getPosInfinity()
  {
    static CLI_P_INT posInf = static_cast<CLI_P_INT>(std::numeric_limits<int>::max());
    return posInf;
  }//getPosInfinity
  
  static bool isListObject(const CLICoreObj& aObj)
  {
    return aObj.objType == CLICoreObj::ObjType::OT_LIST_ARGS ||
    aObj.objType == CLICoreObj::ObjType::OT_LIST_ARRAY ||
    aObj.objType == CLICoreObj::ObjType::OT_LIST_DOMAIN;
  }//isListObject
  
  static bool isCoreObject(const CLICoreObj& aObj)
  {
    return aObj.objType == CLICoreObj::ObjType::OT_INT ||
    aObj.objType == CLICoreObj::ObjType::OT_BOOL ||
    aObj.objType == CLICoreObj::ObjType::OT_STRING;
  }//isCoreObject
  
  static bool isFlowObject(const CLICoreObj& aObj)
  {
    return aObj.objType == CLICoreObj::ObjType::OT_CONTINUE ||
    aObj.objType == CLICoreObj::ObjType::OT_BREAK;
  }//isFlowObject
  
  // Returns the code for the variable assignment object
  // or -1 if "aObj" is not a variable assignment object
  static int isVarDeclObject(const CLICoreObj& aObj)
  {
    if (aObj.objType == CLICoreObj::ObjType::OT_SNG) return DOM_SINGLETON;
    if (aObj.objType == CLICoreObj::ObjType::OT_RNG) return DOM_BOUNDS;
    if (aObj.objType == CLICoreObj::ObjType::OT_SET) return DOM_LIST;
    if (aObj.objType == CLICoreObj::ObjType::OT_SNG_M) return DOM_SINGLETON_M;
    if (aObj.objType == CLICoreObj::ObjType::OT_RNG_M) return DOM_BOUNDS_M;
    if (aObj.objType == CLICoreObj::ObjType::OT_SET_M) return DOM_LIST_M;
    return -1;
  }//isVarDeclObject
  
  // If "aObj" is a binary operator object returns the
  // correspondent opcode. Otherwise returns 0
  static int isBinOpObject(const CLICoreObj& aObj)
  {
    const auto& ot = aObj.objType;
    switch (ot)
    {
      case CLICoreObj::ObjType::OT_PLUS:
        return BINARY_ADD;
      case CLICoreObj::ObjType::OT_MIN:
        return BINARY_SUBTRACT;
      case CLICoreObj::ObjType::OT_PROD:
        return BINARY_MULTIPLY;
      case CLICoreObj::ObjType::OT_DIV:
        return BINARY_DIVIDE;
      case CLICoreObj::ObjType::OT_LT:
        return COMPARE_OP;
      case CLICoreObj::ObjType::OT_LE:
        return COMPARE_OP;
      case CLICoreObj::ObjType::OT_GT:
        return COMPARE_OP;
      case CLICoreObj::ObjType::OT_GE:
        return COMPARE_OP;
      case CLICoreObj::ObjType::OT_EQ:
        return COMPARE_OP;
      case CLICoreObj::ObjType::OT_NQ:
        return COMPARE_OP;
      case CLICoreObj::ObjType::OT_AND:
        return BINARY_AND;
      case CLICoreObj::ObjType::OT_OR:
        return BINARY_OR;
      case CLICoreObj::ObjType::OT_SUBSCR:
        return BINARY_SUBSCR;
      default:
        return 0;
    }
  }//isBinOpObject
  
  // Returns the code for the comparison operator "aObj" or
  // -1 if "aObj" is not a comparison operator
  static int isComparisonOperator(const CLICoreObj& aObj)
  {
    const auto& ot = aObj.objType;
    switch (ot)
    {
      case CLICoreObj::ObjType::OT_LT:
        return OP_LT;
      case CLICoreObj::ObjType::OT_LE:
        return OP_LE;
      case CLICoreObj::ObjType::OT_GT:
        return OP_GT;
      case CLICoreObj::ObjType::OT_GE:
        return OP_GE;
      case CLICoreObj::ObjType::OT_EQ:
        return OP_EQ;
      case CLICoreObj::ObjType::OT_NQ:
        return OP_NE;
      default:
        return -1;
    }
  }//isComparisonOperator
  
  static void bcCoreObjInt(const CLICoreObj& aObj, CLIDriver::ByteCode& aBC)
  {
    // Set opcode
    auto code = aObj.objType == CLICoreObj::ObjType::OT_BOOL ? LOAD_CONST_BOOL : LOAD_CONST;
    aBC.byte_code.push_back(intToByte(code));
    
    // Set constant
    int idx = -1;
    for (std::size_t it{ 0 }; it < aBC.const_array.size(); ++it)
    {
      // Look if the constant aObj.intObj is already present
      // in the array of constants
      const auto& subArr = aBC.const_array[it];
      if (subArr.size() == 1 && subArr[0].getType() == 0 && subArr[0].getInt() == aObj.intObj)
      {
        // The constant is found, set its index
        idx = static_cast<int>(it);
        break;
      }
    }
    
    if (idx < 0)
    {
      // The constant has not been found, add it as new in the array
      // of constants and set its index
      aBC.const_array.push_back({ aObj.intObj });
      const auto& ba = intToByteArray(static_cast<int>(aBC.const_array.size() - 1));
      aBC.byte_code.push_back(ba[0]);
      aBC.byte_code.push_back(ba[1]);
    }
    else
    {
      // The constant was already present in the array of constants,
      // set its index
      const auto& ba = intToByteArray(idx);
      aBC.byte_code.push_back(ba[0]);
      aBC.byte_code.push_back(ba[1]);
    }
  }//bcCoreObjInt
  
  static void bcLoadAttr(const CLICoreObj& aObj, CLIDriver::ByteCode& aBC)
  {
    // Set opcode
    aBC.byte_code.push_back(intToByte(LOAD_ATTR));
    
    // Set name
    auto it = std::find(aBC.global_array.begin(), aBC.global_array.end(), aObj.strObj);
    if (it == aBC.global_array.end())
    {
      aBC.global_array.push_back(aObj.strObj);
      const auto& ba = intToByteArray(static_cast<int>(aBC.global_array.size() - 1));
      aBC.byte_code.push_back(ba[0]);
      aBC.byte_code.push_back(ba[1]);
    }
    else
    {
      const auto& ba = intToByteArray(static_cast<int>(it - aBC.global_array.begin()));
      aBC.byte_code.push_back(ba[0]);
      aBC.byte_code.push_back(ba[1]);
    }
    
    // After loading the attribute there are two objects
    // on the stack:
    // 1) the object the attribute was loaded from;
    // 2) the attribute itself.
    // However, at the end of loading an attribute the stack must contain
    // only the loaded attribute.
    // Therefore the following applies:
    // - swap the two top elements (object_with_attribute, attribute)
    // - delete the top of the stack
    aBC.byte_code.push_back(intToByte(ROT_TWO));
    aBC.byte_code.push_back(intToByte(POP_TOP));
  }//bcLoadAttr
  
  static void bcStoreAttr(const CLICoreObj& aObj, CLIDriver::ByteCode& aBC)
  {
    // Set opcode
    aBC.byte_code.push_back(intToByte(STORE_ATTR));
    
    // Set name
    auto it = std::find(aBC.global_array.begin(), aBC.global_array.end(), aObj.strObj);
    if (it == aBC.global_array.end())
    {
      aBC.global_array.push_back(aObj.strObj);
      const auto& ba = intToByteArray(static_cast<int>(aBC.global_array.size() - 1));
      aBC.byte_code.push_back(ba[0]);
      aBC.byte_code.push_back(ba[1]);
    }
    else
    {
      const auto& ba = intToByteArray(static_cast<int>(it - aBC.global_array.begin()));
      aBC.byte_code.push_back(ba[0]);
      aBC.byte_code.push_back(ba[1]);
    }
  }//bcStoreAttr
  
  static void bcCoreObjStringName(const CLICoreObj& aObj, CLIDriver::ByteCode& aBC)
  {
    // Set opcode
    aBC.byte_code.push_back(intToByte(LOAD_NAME));
    
    // Set name
    auto it = std::find(aBC.global_array.begin(), aBC.global_array.end(), aObj.strObj);
    if (it == aBC.global_array.end())
    {
      // The name is not present in the global array of names, add it and
      // set its index as parameter in the byte code
      aBC.global_array.push_back(aObj.strObj);
      const auto& ba = intToByteArray(static_cast<int>(aBC.global_array.size() - 1));
      aBC.byte_code.push_back(ba[0]);
      aBC.byte_code.push_back(ba[1]);
    }
    else
    {
      // The name is present in the global array of names,
      // and set its index as parameter in the byte code
      const auto& ba = intToByteArray(static_cast<int>(it - aBC.global_array.begin()));
      aBC.byte_code.push_back(ba[0]);
      aBC.byte_code.push_back(ba[1]);
    }
  }//bcCoreObjStringName
  
  static void bcCoreObjStringConst(const CLICoreObj& aObj, CLIDriver::ByteCode& aBC)
  {
    // Set opcode
    aBC.byte_code.push_back(intToByte(LOAD_CONST));
    
    int idx = -1;
    for (std::size_t it{ 0 }; it < aBC.const_array.size(); ++it)
    {
      // Look if the constant aObj.intObj is already present
      // in the array of constants
      const auto& subArr = aBC.const_array[it];
      if (subArr.size() == 1 && subArr[0].getType() == 1 && subArr[0].getString() == aObj.strObj)
      {
        // The constant is found, set its index
        idx = static_cast<int>(it);
        break;
      }
    }
    
    if (idx < 0)
    {
      // The constant has not been found, add it as new in the array
      // of constants and set its index
      aBC.const_array.push_back({ aObj.strObj });
      const auto& ba = intToByteArray(static_cast<int>(aBC.const_array.size() - 1));
      aBC.byte_code.push_back(ba[0]);
      aBC.byte_code.push_back(ba[1]);
    }
    else
    {
      // The constant was already present in the array of constants,
      // set its index
      const auto& ba = intToByteArray(idx);
      aBC.byte_code.push_back(ba[0]);
      aBC.byte_code.push_back(ba[1]);
    }
  }//bcCoreObjStringConst
  
  static void bcCoreObjString(const CLICoreObj& aObj, CLIDriver::ByteCode& aBC)
  {
    // Set opcode
    if (isQuotedString(aObj.strObj))
    {
      // Quoted strings are constants and not names
      // of objects to load from the stack
      bcCoreObjStringConst(aObj, aBC);
    }
    else
    {
      // Unquoted strings represent names of objects
      // to load (LOAD_NAME) from the stack
      bcCoreObjStringName(aObj, aBC);
    }
  }//bcCoreObjString
  
  static void bcCoreObj(const CLICoreObj& aObj, CLIDriver::ByteCode& aBC)
  {
    switch (aObj.objType)
    {
      case CLICoreObj::ObjType::OT_INT:
      case CLICoreObj::ObjType::OT_BOOL:
        bcCoreObjInt(aObj, aBC);
        break;
      default:
        assert(aObj.objType == CLICoreObj::ObjType::OT_STRING);
        bcCoreObjString(aObj, aBC);
        break;
    }
  }//bcCoreObj
  
  static void bcCallFcn(const CLICoreObj& aObj, CLIDriver::ByteCode& aBC)
  {
    // Set opcode
    aBC.byte_code.push_back(intToByte(CALL_FUNCTION));
    
    // Set number of parameters
    const auto& ba = intToByteArray(aObj.size);
    aBC.byte_code.push_back(ba[0]);
    aBC.byte_code.push_back(ba[1]);
  }//bcCallFcn
  
  static void bcBuildTuple(const CLICoreObj& aObj, CLIDriver::ByteCode& aBC)
  {
    // Set opcode
    aBC.byte_code.push_back(intToByte(BUILD_TUPLE));
    
    // Set number of parameters
    const auto& ba = intToByteArray(aObj.size);
    aBC.byte_code.push_back(ba[0]);
    aBC.byte_code.push_back(ba[1]);
  }//bcBuildTuple
  
  static void btVarDecl(const CLICoreObj& aObj, CLIDriver::ByteCode& aBC)
  {
    int domType = isVarDeclObject(aObj);
    assert(domType >= 0);
    assert(aObj.size + 1 < MAX_REP_NUM);
    
    // Push domain type on the stack
    bcCoreObj(CLICoreObj(static_cast<CLI_P_INT>(domType)), aBC);
    
    // Push build domain opcode
    aBC.byte_code.push_back(intToByte(BUILD_DOMAIN));
    
    // Push size of domain
    const auto& ba = intToByteArray(aObj.size + 1);
    aBC.byte_code.push_back(ba[0]);
    aBC.byte_code.push_back(ba[1]);
    
    // Push variable specification opcode
    // Set opcode
    CLIDriver::ByteArray varSpec;
    aBC.byte_code.push_back(intToByte(SET_VAR_SPEC));
    if (aObj.objOptimization == CLICoreObj::ObjType::OT_OPT_MAX)
    {
      varSpec = intToByteArray(static_cast<int>(VAR_SPEC_OPT_MAX));
    }
    else if (aObj.objOptimization == CLICoreObj::ObjType::OT_OPT_MIN)
    {
      varSpec = intToByteArray(static_cast<int>(VAR_SPEC_OPT_MIN));
    }
    else
    {
      varSpec = intToByteArray(static_cast<int>(VAR_SPEC_NONE));
    }
    aBC.byte_code.push_back(varSpec[0]);
    aBC.byte_code.push_back(varSpec[1]);
    
    // Push variable declaration
    // Set opcode
    aBC.byte_code.push_back(intToByte(STORE_VAR));
    
    // Set name
    auto it = std::find(aBC.global_array.begin(), aBC.global_array.end(), aObj.strObj);
    if (it == aBC.global_array.end())
    {
      aBC.global_array.push_back(aObj.strObj);
      const auto& ba = intToByteArray(static_cast<int>(aBC.global_array.size() - 1));
      aBC.byte_code.push_back(ba[0]);
      aBC.byte_code.push_back(ba[1]);
    }
    else
    {
      const auto& ba = intToByteArray(static_cast<int>(it - aBC.global_array.begin()));
      aBC.byte_code.push_back(ba[0]);
      aBC.byte_code.push_back(ba[1]);
    }
  }//btVarDecl
  
  static void btBuildRangeList(const CLICoreObj& aObj, CLIDriver::ByteCode& aBC)
  {
    // Check if the range is (un)bounded
    if (aObj.size == DOM_BOUNDS_OPEN)
    {
      // Range type "..".
      // Set the two infinity bounds
      bcCoreObjInt(CLICoreObj(getNegInfinity()), aBC);
      bcCoreObjInt(CLICoreObj(getPosInfinity()), aBC);
    }
    else if (aObj.size == DOM_BOUNDS_OPEN_RIGHT)
    {
      // Range type "n..".
      // Set the upper infinity bound.
      // @note the lower bound is already on the stack
      bcCoreObjInt(CLICoreObj(getPosInfinity()), aBC);
    }
    else if (aObj.size == DOM_BOUNDS_OPEN_LEFT)
    {
      // Range type "..n".
      // Set the lower infinity bound.
      // @note the upper bound is on the stack and it must be swapped with the lower bound
      bcCoreObjInt(CLICoreObj(getNegInfinity()), aBC);
      
      // Swap the last bytecodes
      aBC.byte_code.push_back(intToByte(ROT_TWO));
    }
  }//btBuildRangeList
  
  static void btBuildList(const CLICoreObj& aObj, CLIDriver::ByteCode& aBC)
  {
    // Set opcode
    aBC.byte_code.push_back(intToByte(BUILD_LIST));
    
    // Set list size
    const auto& ba = intToByteArray(aObj.size);
    aBC.byte_code.push_back(ba[0]);
    aBC.byte_code.push_back(ba[1]);
  }//btBuildList
  
  static void btAssign(const CLICoreObj& aObj, CLIDriver::ByteCode& aBC, bool aStoreFast=false)
  {
    // Set opcode
    if(aStoreFast)
    {
      aBC.byte_code.push_back(intToByte(STORE_FAST));
    }
    else
    {
      aBC.byte_code.push_back(intToByte(STORE_NAME));
    }
    
    // Set name
    auto it = std::find(aBC.global_array.begin(), aBC.global_array.end(), aObj.strObj);
    if (it == aBC.global_array.end())
    {
      if(aStoreFast)
      {
        aBC.local_array.push_back(aObj.strObj);
        const auto& ba = intToByteArray(static_cast<int>(aBC.local_array.size() - 1));
        aBC.byte_code.push_back(ba[0]);
        aBC.byte_code.push_back(ba[1]);
      }
      else
      {
        aBC.global_array.push_back(aObj.strObj);
        const auto& ba = intToByteArray(static_cast<int>(aBC.global_array.size() - 1));
        aBC.byte_code.push_back(ba[0]);
        aBC.byte_code.push_back(ba[1]);
      }
    }
    else
    {
      if(aStoreFast)
      {
        const auto& ba = intToByteArray(static_cast<int>(it - aBC.local_array.begin()));
        aBC.byte_code.push_back(ba[0]);
        aBC.byte_code.push_back(ba[1]);
      }
      else
      {
        const auto& ba = intToByteArray(static_cast<int>(it - aBC.global_array.begin()));
        aBC.byte_code.push_back(ba[0]);
        aBC.byte_code.push_back(ba[1]);
      }
    }
  }//btAssign
  
  static void btForLoop(CLIDriver::ByteCode& aBC, std::vector<std::size_t>& aBlockStack)
  {
    /*
     * A for-loop generates the following byte code:
     * SETUP_LOOP
     */
    
    // Add PC of SETUP_LOOP opcode on block stack
    aBlockStack.push_back(aBC.byte_code.size());
    
    // Setup loop opcode
    aBC.byte_code.push_back(intToByte(SETUP_LOOP));
    
    // The following will be replaced later when a corresponding
    // end object is found
    aBC.byte_code.push_back(0);
    aBC.byte_code.push_back(0);
  }//btForLoop
  
  static void btIfBlock(CLIDriver::ByteCode& aBC, std::vector<std::size_t>& aBlockStack)
  {
    /*
     * An if-block generates the following byte code:
     * POP_JUMP_IF_FALSE
     */
    
    // Add PC of POP_JUMP_IF_FALSE opcode on block stack
    aBlockStack.push_back(aBC.byte_code.size());
    
    // Setup loop opcode
    aBC.byte_code.push_back(intToByte(POP_JUMP_IF_FALSE));
    
    // The following will be replaced later when a corresponding
    // end object is found
    aBC.byte_code.push_back(0);
    aBC.byte_code.push_back(0);
  }//btIfBlock
  
  static void btElseBlock(CLIDriver::ByteCode& aBC, std::vector<std::size_t>& aBlockStack)
  {
    /*
     * An else-block generates the following byte code:
     * JUMP_FORWARD
     */
    
    // Add PC of POP_JUMP_IF_FALSE opcode on block stack
    aBlockStack.push_back(aBC.byte_code.size());
    
    // Setup loop opcode
    aBC.byte_code.push_back(intToByte(JUMP_FORWARD));
    
    // The following will be replaced later when a corresponding
    // end object is found
    aBC.byte_code.push_back(0);
    aBC.byte_code.push_back(0);
  }//btElseBlock
  
  static void btForLoopRangeIter(const CLICoreObj& aIter, CLIDriver::ByteCode& aBC, std::vector<std::size_t>& aBlockStack, bool aIsList)
  {
    /*
     * A for-loop generates the following byte code:
     * LOAD_CONST...
     * RANGE_LIST
     * GET_ITER
     * FOR_ITER
     * STORE_NAME
     */
    
    // Call the function to create the range list [aStart, aEnd]
    // @note add a range list instruction only if the object is a range list,
    // otherwise keep the list as is
    if (!aIsList)
    {
      aBC.byte_code.push_back(intToByte(RANGE_LIST));
    }
    
    // Setup get_iter
    aBC.byte_code.push_back(intToByte(GET_ITER));
    
    // Add PC of FOR_ITER opcode on block stack
    aBlockStack.push_back(aBC.byte_code.size());
    
    // Setup loop opcode
    aBC.byte_code.push_back(intToByte(FOR_ITER));
    // The following will be replaced later when a corresponding
    // end object is found
    aBC.byte_code.push_back(0);
    aBC.byte_code.push_back(0);
    
    // Store name (fast), i.e., no reporting into the context
    btAssign(aIter, aBC, true);
  }//btForLoopRangeIter
  
  int findLoopInstructionFromBlockStackPC(CLIDriver::ByteCode& aBC, std::vector<std::size_t>& aBlockStack)
  {
    auto loopByte = intToByte(FOR_ITER);
    for (auto it = aBlockStack.rbegin(); it != aBlockStack.rend(); ++it)
    {
      assert(*it <= aBC.byte_code.size());
      if (aBC.byte_code[*it] == loopByte) return static_cast<int>(*it);
    }
    return -1;
  }//findLoopInstructionFromBlockStackPC
  
  static void bcFlowObj(const CLICoreObj& aObj, CLIDriver::ByteCode& aBC, std::vector<std::size_t>& aBlockStack)
  {
    if (aObj.objType == CLICoreObj::ObjType::OT_CONTINUE)
    {
      // Set jump back to the for_iter opcode
      aBC.byte_code.push_back(intToByte(JUMP_ABSOLUTE));
      // Find the jump back instruction: look for the first loop instruction
      // in the stack to jump to since aBlockStack.back() may not contain the right PC.
      // @note not all the jump back instructions in the block stack are loops,
      // some may be, for example, if statements.
      auto pc_ja = findLoopInstructionFromBlockStackPC(aBC, aBlockStack);
      assert(pc_ja >= 0);
      
      const auto& ba_ja = intToByteArray(pc_ja);
      aBC.byte_code.push_back(ba_ja[0]);
      aBC.byte_code.push_back(ba_ja[1]);
    }
    else
    {
      assert(aObj.objType == CLICoreObj::ObjType::OT_BREAK);
      aBC.byte_code.push_back(intToByte(BREAK_LOOP));
    }
  }//bcFlowObj
  
  static void btEndIfBlock(CLIDriver::ByteCode& aBC, std::vector<std::size_t>& aBlockStack, bool aJumpToNextInstruction)
  {
    assert(!aBlockStack.empty());
    
    // Get the PC where the if (else) block is set
    auto pc_ja = aBlockStack.back();
    aBlockStack.pop_back();
    
    assert(aBC.byte_code.size() >= pc_ja);
    
    // Set the value to the current PC when jumping from the if (else) statement:
    // @note this is a relative or absolute jump depending on the instruction at position pc_ja:
    // - POP_JUMP_IF_FALSE is an absolute jump
    // - JUMP_FORWARD is a relative jump
    auto jumpTo = static_cast<int>(aBC.byte_code.size());
    if (aBC.byte_code[pc_ja] != intToByte(POP_JUMP_IF_FALSE))
    {
      // Relative jump
      jumpTo -= static_cast<int>(pc_ja);
    }
    
    if (aJumpToNextInstruction)
    {
      jumpTo += 3;
    }
    
    const auto& ba = intToByteArray(jumpTo);
    aBC.byte_code[pc_ja + 1] = ba[0];
    aBC.byte_code[pc_ja + 2] = ba[1];
  }//btEndIfBlock
  
  static void btEndBlock(CLIDriver::ByteCode& aBC, std::vector<std::size_t>& aBlockStack)
  {
    assert(aBlockStack.size() > 1);
    auto pc_ja = aBlockStack.back();
    aBlockStack.pop_back();
    
    // Get the correspondent for_iter and set this PC
    assert(aBC.byte_code.size() >= pc_ja);
    
    // Set the delta to add to the current PC when jumping relative fro the for_iter statement:
    // aBC.byte_code.size() - pc_ja + 3
    // calculate the delta needed to be added to the current PC to jump after
    // the for-loop block where this current point (size) is the end of the block
    // and +3 is the next instruction
    const auto& ba = intToByteArray(static_cast<int>(aBC.byte_code.size() - pc_ja + 3));
    aBC.byte_code[pc_ja + 1] = ba[0];
    aBC.byte_code[pc_ja + 2] = ba[1];
    
    // Set jump back to the for_iter opcode
    aBC.byte_code.push_back(intToByte(JUMP_ABSOLUTE));
    
    // Set number of parameters
    const auto& ba_ja = intToByteArray(static_cast<int>(pc_ja));
    aBC.byte_code.push_back(ba_ja[0]);
    aBC.byte_code.push_back(ba_ja[1]);
    
    auto pc_pb = aBlockStack.back();
    aBlockStack.pop_back();
    
    // Get the correspondent for_iter and set this PC
    assert(aBC.byte_code.size() > pc_pb + 1);
    const auto& ba_pb = intToByteArray(static_cast<int>(aBC.byte_code.size() + 1));
    aBC.byte_code[pc_pb + 1] = ba_pb[0];
    aBC.byte_code[pc_pb + 2] = ba_pb[1];
    
    // Set pop_block
    aBC.byte_code.push_back(intToByte(POP_BLOCK));
  }//btEndBlock
  
  // Check that the position "aIdx" in the stack is within a loop block,
  // i.e., it must be within SETUP_LOOP and a JUMP_ABSOLUTE instructions
  static bool checkFlowStmtWithinLoop(const std::vector<CLICoreObj>& aStack, std::size_t aIdx)
  {
    // Look for the SETUP_LOOP backwards
    bool found{ false };
    for (int it = static_cast<int>(aIdx); it >= 0; --it)
    {
      if (aStack[it].objType == CLICoreObj::ObjType::OT_FOR_LOOP ||
          aStack[it].objType == CLICoreObj::ObjType::OT_WHILE_LOOP)
      {
        found = true;
        break;
      }
    }
    if (!found) return false;
    
    // Look for the JUMP_ABSOLUTE forward
    found = false;
    for (auto it = aIdx; it < aStack.size(); ++it)
    {
      if (aStack[it].objType == CLICoreObj::ObjType::OT_END_BLK)
      {
        found = true;
        break;
      }
    }
    return found;
  }//checkFlowExprWithinLoop
  
  // Returns true if the top stack needs a PRINT_TOP instruction,
  // false otherwise
  static bool topStackNeedsPrint(const std::vector<CLICoreObj>& aStack)
  {
    const auto& topObj = aStack.back();
    
    auto objType = topObj.objType;
    if (isCoreObject(topObj) ||
        isBinOpObject(topObj) ||
        (objType == CLICoreObj::ObjType::OT_LIST_ARRAY) ||
        (objType == CLICoreObj::ObjType::OT_CALL_FCN)   ||
        (objType == CLICoreObj::ObjType::OT_ATTR)       ||
        (objType == CLICoreObj::ObjType::OT_ASSIGN_ATTR))
    {
      return true;
    }
    
    return false;
  }//topStackNeedsPrint
  
  CLIDriver::CLIDriver()
  : pInStreamIsLine(true)
  , pLexerError(false)
  , pParserError(false)
  , pParser(nullptr)
  , pScanner(nullptr)
  {
  }
  
  void CLIDriver::debug(const std::string& str)
  {
    std::cout << str << std::endl;
  }//debug
  
  void CLIDriver::setObjectWithList()
  {
    assert(!stack().empty() && isListObject(stack().back()));
    assert(stack().size() >= stack().back().size + 2);
    stack()[stack().size() - (stack().back().size + 2)].objWithList = true;
    
    // Set list object type
    if (stack().back().objType == CLICoreObj::ObjType::OT_LIST_ARRAY)
    {
      // A tuple must be built to store the indices of the subscript operator:
      // 1) copy the back of the stack
      auto backStack = stack().back();
      
      // 2) replace its type with the subscript operator type
      backStack.objType = CLICoreObj::ObjType::OT_SUBSCR;
      
      // 3) push it back onto the stack
      stack().push_back(backStack);
      
      // 4) replace the n-1 element of the stack with a BUILD_TUPLE instruction
      //    @note the size of the tuple is already set, i.e., "stack().back().size"
      stack()[stack().size() - 2].objType = CLICoreObj::ObjType::OT_TUPLE;
    }
    else if (stack().back().objType == CLICoreObj::ObjType::OT_LIST_ARGS)
    {
      stack().back().objType = CLICoreObj::ObjType::OT_CALL_FCN;
    }
  }//setObjectWithList
  
  void CLIDriver::markEndOfStmt(bool aIsEmpty)
  {
    auto needsPrint = !aIsEmpty && topStackNeedsPrint(stack());
    if (needsPrint)
    {
      stack().push_back(CLICoreObj(CLICoreObj::ObjType::OT_PRINT_TOP));
    }
    
    if (needsPrint || (stack().back().objType == CLICoreObj::ObjType::OT_CALL_FCN))
    {
      stack().push_back(CLICoreObj(CLICoreObj::ObjType::OT_POP_TOP));
    }
  }//markEndOfStmt
  
  void CLIDriver::addUnaryOperators(const std::vector<bool>& aOperators)
  {
    for (auto op : aOperators)
    {
      if (op)
      {
        stack().push_back(CLICoreObj(CLICoreObj::ObjType::OT_UNARY_POS));
      }
      else
      {
        stack().push_back(CLICoreObj(CLICoreObj::ObjType::OT_UNARY_NEG));
      }
    }
  }//addUnaryOperators
  
  void CLIDriver::addExpr(CLICoreObj::ObjType aEOP)
  {
    assert(stack().size() > 1);
    if (isCoreObject(stack().back()) &&
        isCoreObject(stack()[stack().size() - 2]) &&
        stack().back().objType != CLICoreObj::ObjType::OT_STRING &&
        stack()[stack().size() - 2].objType != CLICoreObj::ObjType::OT_STRING)
    {
      // Reduce the objects
      auto obj1 = stack().back().intObj;
      auto obj1T = stack().back().objType;
      stack().pop_back();
      
      auto obj2 = stack().back().intObj;
      auto obj2T = stack().back().objType;
      stack().pop_back();
      
      // Check Boolean types for type promotion
      auto isBool = obj1T == CLICoreObj::ObjType::OT_BOOL ||
      obj2T == CLICoreObj::ObjType::OT_BOOL;
      
      switch (aEOP)
      {
        case CLICoreObj::ObjType::OT_PLUS:
          stack().push_back(CLICoreObj(obj2 + obj1));
          break;
        case CLICoreObj::ObjType::OT_MIN:
          stack().push_back(CLICoreObj(obj2 - obj1));
          break;
        case CLICoreObj::ObjType::OT_PROD:
          stack().push_back(CLICoreObj(obj2 * obj1));
          break;
        case CLICoreObj::ObjType::OT_DIV:
        {
          assert(obj1 != 0);
          stack().push_back(CLICoreObj(obj2 / obj1));
        }
          break;
        case CLICoreObj::ObjType::OT_LT:
          stack().push_back(CLICoreObj(obj2 < obj1));
          break;
        case CLICoreObj::ObjType::OT_LE:
          stack().push_back(CLICoreObj(obj2 <= obj1));
          break;
        case CLICoreObj::ObjType::OT_GT:
          stack().push_back(CLICoreObj(obj2 > obj1));
          break;
        case CLICoreObj::ObjType::OT_GE:
          stack().push_back(CLICoreObj(obj2 >= obj1));
          break;
        case CLICoreObj::ObjType::OT_EQ:
          stack().push_back(CLICoreObj(obj2 == obj1));
          break;
        case CLICoreObj::ObjType::OT_NQ:
          stack().push_back(CLICoreObj(obj2 != obj1));
          break;
        case CLICoreObj::ObjType::OT_AND:
          stack().push_back(CLICoreObj(obj2 && obj1));
          break;
        case CLICoreObj::ObjType::OT_OR:
          stack().push_back(CLICoreObj(obj2 || obj1));
          break;
        case CLICoreObj::ObjType::OT_REIF:
          stack().push_back(CLICoreObj(obj2 == obj1));
          break;
        default:
          assert(aEOP == CLICoreObj::ObjType::OT_REIF);
          break;
      }// switch
      // Perform type promotion to Boolean type
      stack().back().objType = isBool ? CLICoreObj::ObjType::OT_BOOL : CLICoreObj::ObjType::OT_INT;
      
      return;
    }
    stack().push_back(CLICoreObj(aEOP));
  }//addExpr
  
  void CLIDriver::addLoadProperty(const std::string& aObj, const std::vector<std::string>& aProp)
  {
    assert(!aObj.empty());
    assert(!stack().empty());
    
    // Load name
    stack().push_back(CLICoreObj(aObj));
    
    // Load attribute
    for (const auto& prop : aProp)
    {
      // Create a "string" object similar to load name
      stack().push_back(CLICoreObj(prop));
      
      // Change the object type from OT_STRING to OT_ATTR
      // so it will produce not LOAD_NAME bytecode anymore but LOAD_ATTR
      stack().back().objType = CLICoreObj::ObjType::OT_ATTR;
    }
    
  }//addLoadProperty
  
  void CLIDriver::addStoreProperty(const std::string& aObj, const std::vector<std::string>& aProp)
  {
    assert(!aObj.empty());
    assert(!stack().empty());
    
    // Load name
    stack().push_back(CLICoreObj(aObj));
    
    // All props - 1 must be loaded
    for(std::size_t propIdx{0}; propIdx < aProp.size() - 1; ++propIdx)
    {
      // Create a "string" object similar to load name
      stack().push_back(CLICoreObj(aProp[propIdx]));
      
      // Change the object type from OT_STRING to OT_ATTR
      // so it will produce not LOAD_NAME bytecode anymore but LOAD_ATTR
      stack().back().objType = CLICoreObj::ObjType::OT_ATTR;
    }
    
    // Last property is the one used for assignment
    stack().push_back(CLICoreObj(aProp.back()));
    stack().back().objType = CLICoreObj::ObjType::OT_ASSIGN_ATTR;
    
    // Set on top of the stack the updated object
    stack().push_back(CLICoreObj(aObj));
  }//addStoreProperty
  
  void CLIDriver::addAssignExpr(const std::string& aID, CLICoreObj::ObjType aType, int aExprSize,
                                int aMinMaxOpt)
  {
    assert(!aID.empty());
    assert(!stack().empty());
    
    // Push on stack a new object id
    stack().push_back(CLICoreObj(aID));
    
    // Set its type
    stack().back().objType = aType;
    
    // Set its size
    stack().back().size = aExprSize;
    
    // Set optimization if any
    if (aMinMaxOpt)
    {
      stack().back().objOptimization = aMinMaxOpt == 1 ?
      CLICoreObj::ObjType::OT_OPT_MIN : CLICoreObj::ObjType::OT_OPT_MAX;
    }
  }//addAssignExpr
  
  void CLIDriver::addAssignVarMatrix(const std::string& aID, CLICoreObj::ObjType aType, int aExprSize,
                                     int aMinMaxOpt)
  {
    assert(!aID.empty());
    assert(!stack().empty());
    
    // If the back of the stack is a constructed list,
    // this assignment is not a variable declaration but an expression assignment
    assert(stack().back().objType == CLICoreObj::ObjType::OT_LIST_ARRAY);
    
    // Push on stack a new object id
    stack().push_back(CLICoreObj(aID));
    
    // Set its type
    switch (aType)
    {
      case CLI::CLICoreObj::ObjType::OT_SNG:
        aType = CLI::CLICoreObj::ObjType::OT_SNG_M;
        break;
      case CLI::CLICoreObj::ObjType::OT_RNG:
        aType = CLI::CLICoreObj::ObjType::OT_RNG_M;
        break;
      default:
        assert(aType == CLI::CLICoreObj::ObjType::OT_SET);
        aType = CLI::CLICoreObj::ObjType::OT_SET_M;
        break;
    }
    stack().back().objType = aType;
    
    // Set its size.
    // @note add 1 to the size of the domain since
    // for matrix domains there is one more element,
    // i.e., the size of the matrix
    stack().back().size = aExprSize + 1;
    
    // Set optimization if any
    if (aMinMaxOpt)
    {
      stack().back().objOptimization = aMinMaxOpt == 1 ?
      CLICoreObj::ObjType::OT_OPT_MIN : CLICoreObj::ObjType::OT_OPT_MAX;
    }
  }//addAssignVarMatrix
  
  void CLIDriver::addLetExpr(const std::string& aID)
  {
    // Push on stack a new object id
    stack().push_back(CLICoreObj(aID));
    // Set its type
    stack().back().objType = CLICoreObj::ObjType::OT_LET;
  }//addLetExpr
  
  void CLIDriver::addForLoopBlock()
  {
    // Mark the beginning of a for-loop stmt on the stack
    stack().push_back(CLICoreObj(CLICoreObj::ObjType::OT_FOR_LOOP));
  }//addForLoopBlock
  
  void CLIDriver::addForLoopRangeIter(const std::string& aID, CLICoreObj::ObjType aIterType)
  {
    assert(!aID.empty());
    stack().push_back(CLICoreObj(CLICoreObj::ObjType::OT_FOR_LOOP_RANGE_ITER));
    
    assert(aIterType == CLICoreObj::ObjType::OT_RNG_LIST || aIterType == CLICoreObj::ObjType::OT_LIST_ARRAY);
    if (aIterType == CLICoreObj::ObjType::OT_LIST_ARRAY)
    {
      stack().back().objWithList = true;
    }
    
    stack().push_back(CLICoreObj(aID));
  }//addForLoopRangeIter
  
  void CLIDriver::addIfBlock()
  {
    // Mark the beginning of an if stmt on the stack
    stack().push_back(CLICoreObj(CLICoreObj::ObjType::OT_IF_BLK));
  }//addIfBlock
  
  void CLIDriver::addElseBlock()
  {
    // Mark the beginning of an if stmt on the stack
    stack().push_back(CLICoreObj(CLICoreObj::ObjType::OT_ELSE_BLK));
  }//addIfBlock
  
  void CLIDriver::addEndIfBlock(bool aJumpToNextInstruction)
  {
    // Push end block object on stack
    stack().push_back(CLICoreObj(CLICoreObj::ObjType::OT_END_IF_BLK));
    stack().back().intObj = 0;
    if (aJumpToNextInstruction)
    {
      stack().back().intObj = 1;
    }
  }//addEndIfBlock
  
  void CLIDriver::addEndBlock()
  {
    // Push end block object on stack
    stack().push_back(CLICoreObj(CLICoreObj::ObjType::OT_END_BLK));
  }//addEndBlock
  
  CLIDriver::ByteCode CLIDriver::byteCode(const ByteCode& aEnvironment)
  {
    ByteCode bc = aEnvironment;
    
    // Remove return value from previous environment if any
    if (bc.byte_code.size() > 0 && byteToInt(bc.byte_code[bc.byte_code.size() - 1]) == RETURN_VALUE)
    {
      bc.byte_code.resize(bc.byte_code.size() - 1);
    }
    
    for (auto it{ 0 }; it < stack().size(); ++it)
    {
      auto& obj = stack()[it];
      if (isCoreObject(obj))
      {
        bcCoreObj(obj, bc);
      }
      else if (isFlowObject(obj))
      {
        if (!checkFlowStmtWithinLoop(stack(), it))
        {
          error("Flow statement not within a loop");
          throw std::logic_error(std::string("Flow statement not within a loop"));
        }
        bcFlowObj(obj, bc, pBlockStackPC);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_ATTR)
      {
        bcLoadAttr(obj, bc);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_ASSIGN_ATTR)
      {
        bcStoreAttr(obj, bc);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_CALL_FCN)
      {
        bcCallFcn(obj, bc);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_POP_TOP)
      {
        bc.byte_code.push_back(intToByte(POP_TOP));
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_PRINT_TOP)
      {
        bc.byte_code.push_back(intToByte(PRINT_TOP));
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_MARK)
      {
        bc.byte_code.push_back(intToByte(ADD_MARK));
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_UNARY_NEG)
      {
        bc.byte_code.push_back(intToByte(UNARY_NEGATIVE));
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_UNARY_POS)
      {
        bc.byte_code.push_back(intToByte(UNARY_POSITIVE));
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_TUPLE)
      {
        bcBuildTuple(obj, bc);
      }
      else if (int binOp = isBinOpObject(obj))
      {
        bc.byte_code.push_back(intToByte(binOp));
        int compOP = isComparisonOperator(obj);
        if (compOP >= 0)
        {
          const auto& ba = intToByteArray(compOP);
          bc.byte_code.push_back(ba[0]);
          bc.byte_code.push_back(ba[1]);
        }
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_SNG ||
               obj.objType == CLICoreObj::ObjType::OT_RNG ||
               obj.objType == CLICoreObj::ObjType::OT_SET ||
               obj.objType == CLICoreObj::ObjType::OT_SNG_M ||
               obj.objType == CLICoreObj::ObjType::OT_RNG_M ||
               obj.objType == CLICoreObj::ObjType::OT_SET_M)
      {
        // Variable declaration object
        btVarDecl(obj, bc);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_RNG_LIST)
      {
        btBuildRangeList(obj, bc);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_LIST_ARRAY)
      {
        btBuildList(obj, bc);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_ASSIGN || obj.objType == CLICoreObj::ObjType::OT_LET)
      {
        btAssign(obj, bc);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_FOR_LOOP)
      {
        btForLoop(bc, pBlockStackPC);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_IF_BLK)
      {
        btIfBlock(bc, pBlockStackPC);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_ELSE_BLK)
      {
        btElseBlock(bc, pBlockStackPC);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_END_IF_BLK)
      {
        btEndIfBlock(bc, pBlockStackPC, obj.intObj == 1);
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_FOR_LOOP_RANGE_ITER)
      {
        assert(it + 1 < stack().size());
        btForLoopRangeIter(stack()[it + 1], bc, pBlockStackPC, obj.objWithList);
        it += 1;
      }
      else if (obj.objType == CLICoreObj::ObjType::OT_END_BLK)
      {
        if (pBlockStackPC.empty())
        {
          // Parse "end" as a string
          obj.objType = CLICoreObj::ObjType::OT_STRING;
          bcCoreObj(obj, bc);
        }
        else
        {
          btEndBlock(bc, pBlockStackPC);
        }
      }
    }// for it
    
    // Add return opcode
    bc.byte_code.push_back(intToByte(RETURN_VALUE));
    return bc;
  }//byteCode
  
  void printByteCode(const CLIDriver::ByteCode& aBC)
  {
    bool nl{ false };
    if (!aBC.const_array.empty())
    {
      nl = true;
      std::cout << "Constants:\n";
      for (auto& it : aBC.const_array)
      {
        assert(!it.empty());
        if (it.size() == 1)
        {
          if (it[0].getType() == 0)
          {
            std::cout << it[0].getInt() << ' ';
          }
          else
          {
            assert(it[0].getType() == 1);
            std::cout << it[0].getString() << ' ';
          }
        }
        else
        {
          std::cout << '(';
          std::string arr;
          for (const auto& e : it)
          {
            std::stringstream ss;
            if (e.getType() == 0)
            {
              std::cout << e.getInt() << ' ';
            }
            else
            {
              assert(e.getType() == 1);
              std::cout << e.getString() << ' ';
            }
            arr += ss.str() + ", ";
          }
          arr.pop_back();
          arr.pop_back();
          std::cout << arr << ") ";
        }
      }//for
      std::cout << '\n';
    }
    
    if (!aBC.global_array.empty())
    {
      nl = true;
      std::cout << "Global names:\n";
      for (auto it : aBC.global_array)
      {
        std::cout << it << ' ';
      }
      std::cout << '\n';
    }
    
    if (!aBC.local_array.empty())
    {
      nl = true;
      std::cout << "Local names:\n";
      for (auto it : aBC.local_array)
      {
        std::cout << it << ' ';
      }
      std::cout << '\n';
    }
    
    if (nl) std::cout << '\n';
    for (std::size_t it{ 0 }; it < aBC.byte_code.size(); ++it)
    {
      std::cout << it << '\t';
      auto opcode = byteToInt(aBC.byte_code.at(it));
      
      std::string code;
      if (opcode == ADD_MARK) code = "ADD_MARK";
      if (opcode == POP_BLOCK) code = "POP_BLOCK";
      if (opcode == UNARY_NOT) code = "UNARY_NOT";
      if (opcode == UNARY_NEGATIVE) code = "UNARY_NEGATIVE";
      if (opcode == UNARY_POSITIVE) code = "UNARY_POSITIVE";
      if (opcode == BINARY_MULTIPLY) code = "BINARY_MULTIPLY";
      if (opcode == BINARY_DIVIDE) code = "BINARY_DIVIDE";
      if (opcode == BINARY_MODULO) code = "BINARY_MODULO";
      if (opcode == BINARY_ADD) code = "BINARY_ADD";
      if (opcode == BINARY_SUBTRACT) code = "BINARY_SUBTRACT";
      if (opcode == BINARY_AND) code = "BINARY_AND";
      if (opcode == BINARY_OR) code = "BINARY_OR";
      if (opcode == BINARY_XOR) code = "BINARY_XOR";
      if (opcode == BINARY_SUBSCR) code = "BINARY_SUBSCR";
      if (opcode == BREAK_LOOP) code = "BREAK_LOOP";
      if (opcode == LOAD_CONST) code = "LOAD_CONST";
      if (opcode == LOAD_CONST_BOOL) code = "LOAD_CONST_BOOL";
      if (opcode == LOAD_NAME) code = "LOAD_NAME";
      if (opcode == LOAD_ATTR) code = "LOAD_ATTR";
      if (opcode == STORE_ATTR) code = "STORE_ATTR";
      if (opcode == STORE_NAME) code = "STORE_NAME";
      if (opcode == STORE_FAST) code = "STORE_FAST";
      if (opcode == COMPARE_OP) code = "COMPARE_OP";
      if (opcode == BUILD_LIST) code = "BUILD_LIST";
      if (opcode == JUMP_ABSOLUTE) code = "JUMP_ABSOLUTE";
      if (opcode == SETUP_LOOP) code = "SETUP_LOOP";
      if (opcode == FOR_ITER) code = "FOR_ITER";
      if (opcode == POP_JUMP_IF_FALSE) code = "POP_JUMP_IF_FALSE";
      if (opcode == JUMP_FORWARD) code = "JUMP_FORWARD";
      if (opcode == CALL_FUNCTION) code = "CALL_FUNCTION";
      if (opcode == BUILD_DOMAIN) code = "BUILD_DOMAIN";
      if (opcode == BUILD_TUPLE) code = "BUILD_TUPLE";
      if (opcode == STORE_VAR) code = "STORE_VAR ";
      if (opcode == POP_TOP) code = "POP_TOP";
      if (opcode == ROT_TWO) code = "ROT_TWO";
      if (opcode == PRINT_TOP) code = "PRINT_TOP";
      if (opcode == RETURN_VALUE) code = "RETURN_VALUE";
      if (opcode == GET_ITER) code = "GET_ITER";
      if (opcode == RANGE_LIST) code = "RANGE_LIST";
      if (opcode == SET_VAR_SPEC) code = "SET_VAR_SPEC";
      assert(!code.empty());
      
      std::cout << code << '\t';
      if (opcode > HAVE_ARGUMENT)
      {
        int argLower = aBC.byte_code.at(++it);
        int argUpper = aBC.byte_code.at(++it) << 8;
        auto arg = argUpper + argLower;
        
        std::cout << arg;
        if (opcode == LOAD_CONST || opcode == LOAD_CONST_BOOL)
        {
          assert(arg < aBC.const_array.size());
          std::cout << " (";
          std::string arr;
          for (auto& e : aBC.const_array.at(arg))
          {
            std::stringstream ss;
            if (e.getType() == 0)
            {
              ss << e.getInt();
            }
            else
            {
              assert(e.getType() == 1);
              ss << e.getString();
            }
            arr += ss.str() + ", ";
          }
          arr.pop_back();
          arr.pop_back();
          std::cout << arr << ')';
        }
        else if (opcode == LOAD_NAME)
        {
          assert(arg < aBC.global_array.size());
          std::cout << " (" << aBC.global_array.at(arg) << ')';
        }
        else if (opcode == STORE_NAME || opcode == STORE_ATTR)
        {
          assert(arg < aBC.global_array.size());
          std::cout << " (" << aBC.global_array.at(arg) << ')';
        }
        else if (opcode == STORE_FAST)
        {
          assert(arg < aBC.local_array.size());
          std::cout << " (" << aBC.local_array.at(arg) << ')';
        }
        else if (opcode == STORE_VAR)
        {
          assert(arg < aBC.global_array.size());
          std::cout << " (" << aBC.global_array.at(arg) << ')';
        }
      }
      std::cout << '\n';
    }
  }//printByteCode
  
  void CLIDriver::printStack()
  {
    for (auto it = stack().begin(); it != stack().end(); ++it)
    {
      if (isCoreObject(*it))
      {
        switch ((*it).objType)
        {
          case CLICoreObj::ObjType::OT_INT:
            std::cout << "LOAD_CONST\t" << it->intObj;
            break;
          case CLICoreObj::ObjType::OT_BOOL:
          {
            std::string boolVal = it->intObj ? "true" : "false";
            std::cout << "LOAD_CONST_BOOL\t" << boolVal;
          }
            break;
          default:
            assert((*it).objType == CLICoreObj::ObjType::OT_STRING);
            std::cout << "LOAD_NAME\t" << it->strObj;
            break;
        }
      }//isCoreObj
      else if (isListObject(*it))
      {
        assert(it->size >= 0);
        switch ((*it).objType)
        {
          case CLICoreObj::ObjType::OT_LIST_ARGS:
            std::cout << "CALL_FUNCTION\t" << it->size;
            break;
          case CLICoreObj::ObjType::OT_LIST_ARRAY:
            std::cout << "BUILD_LIST\t" << it->size;
            break;
          default:
            assert((*it).objType == CLICoreObj::ObjType::OT_LIST_DOMAIN);
            std::cout << "{} s:" << it->size;
            break;
        }
      }
      else if ((*it).objType == CLICoreObj::ObjType::OT_ATTR) std::cout << "LOAD_ATTR";
      else if ((*it).objType == CLICoreObj::ObjType::OT_ASSIGN_ATTR) std::cout << "STORE_ATTR";
      else if ((*it).objType == CLICoreObj::ObjType::OT_CALL_FCN) std::cout << "CALL_FUNCTION\t" << it->size;
      else if ((*it).objType == CLICoreObj::ObjType::OT_UNARY_NEG) std::cout << "UNARY_NEG";
      else if ((*it).objType == CLICoreObj::ObjType::OT_UNARY_POS) std::cout << "UNARY_POS";
      else if ((*it).objType == CLICoreObj::ObjType::OT_PLUS) std::cout << "BINARY_ADD";
      else if ((*it).objType == CLICoreObj::ObjType::OT_MIN) std::cout << "BINARY_SUBTRACT";
      else if ((*it).objType == CLICoreObj::ObjType::OT_PROD) std::cout << "BINARY_MULTIPLY";
      else if ((*it).objType == CLICoreObj::ObjType::OT_DIV) std::cout << "BINARY_TRUE_DIVIDE";
      else if ((*it).objType == CLICoreObj::ObjType::OT_LT) std::cout << "COMPARE_OP\t<";
      else if ((*it).objType == CLICoreObj::ObjType::OT_LE) std::cout << "COMPARE_OP\t<=";
      else if ((*it).objType == CLICoreObj::ObjType::OT_GT) std::cout << "COMPARE_OP\t>";
      else if ((*it).objType == CLICoreObj::ObjType::OT_GE) std::cout << "COMPARE_OP\t>=";
      else if ((*it).objType == CLICoreObj::ObjType::OT_EQ) std::cout << "COMPARE_OP\t==";
      else if ((*it).objType == CLICoreObj::ObjType::OT_NQ) std::cout << "COMPARE_OP\t!=";
      else if ((*it).objType == CLICoreObj::ObjType::OT_AND) std::cout << "BINARY_AND";
      else if ((*it).objType == CLICoreObj::ObjType::OT_OR) std::cout << "BINARY_OR";
      else if ((*it).objType == CLICoreObj::ObjType::OT_REIF) std::cout << "BINARY_REIF";
      else if ((*it).objType == CLICoreObj::ObjType::OT_SUBSCR) std::cout << "BINARY_SUBSCR";
      else if ((*it).objType == CLICoreObj::ObjType::OT_POP_TOP) std::cout << "POP_TOP";
      else if ((*it).objType == CLICoreObj::ObjType::OT_PRINT_TOP) std::cout << "PRINT_TOP";
      else if ((*it).objType == CLICoreObj::ObjType::OT_LET) std::cout << "LET";
      else if ((*it).objType == CLICoreObj::ObjType::OT_BREAK) std::cout << "BREAK";
      else if ((*it).objType == CLICoreObj::ObjType::OT_CONTINUE) std::cout << "CONTINUE";
      else if ((*it).objType == CLICoreObj::ObjType::OT_RETURN) std::cout << "RETURN";
      else if ((*it).objType == CLICoreObj::ObjType::OT_ASSIGN) std::cout << "STORE_NAME\t" << it->strObj;
      else if ((*it).objType == CLICoreObj::ObjType::OT_SNG) std::cout << "BUILD_DOMAIN\t([d])\n" << "STORE_VAR\t" << it->strObj;
      else if ((*it).objType == CLICoreObj::ObjType::OT_RNG) std::cout << "BUILD_DOMAIN\t([d1, d2])\n" << "STORE_VAR\t" << it->strObj;
      else if ((*it).objType == CLICoreObj::ObjType::OT_SET) std::cout << "BUILD_DOMAIN\t({d1, ...})\n" << "STORE_VAR\t" << it->strObj;
      else if ((*it).objType == CLICoreObj::ObjType::OT_SNG_M) std::cout << "BUILD_DOMAIN\t([d][])\n" << "STORE_VAR\t" << it->strObj;
      else if ((*it).objType == CLICoreObj::ObjType::OT_RNG_M) std::cout << "BUILD_DOMAIN\t([d1, d2][])\n" << "STORE_VAR\t" << it->strObj;
      else if ((*it).objType == CLICoreObj::ObjType::OT_SET_M) std::cout << "BUILD_DOMAIN\t({d1, ...}[])\n" << "STORE_VAR\t" << it->strObj;
      else if ((*it).objType == CLICoreObj::ObjType::OT_TUPLE) std::cout << "BUILD_TUPLE";
      else if ((*it).objType == CLICoreObj::ObjType::OT_IF_BLK) std::cout << "JUMP_IF";
      else if ((*it).objType == CLICoreObj::ObjType::OT_ELSE_BLK) std::cout << "ELSE";
      else if ((*it).objType == CLICoreObj::ObjType::OT_FOR_LOOP) std::cout << "SETUP_LOOP";
      else if ((*it).objType == CLICoreObj::ObjType::OT_FOR_LOOP_RANGE_ITER) std::cout << "FOR_LOOP_RANGE_ITER";
      else if ((*it).objType == CLICoreObj::ObjType::OT_END_BLK) std::cout << "JUMP_ABSOLUTE\nPOP_BLOCK";
      else if ((*it).objType == CLICoreObj::ObjType::OT_END_IF_BLK) std::cout << "END_IF";
      else if ((*it).objType == CLICoreObj::ObjType::OT_RNG_LIST) std::cout << "RANGE_LIST";
      else
      {
        std::cout << "Unrecognized code: " << static_cast<int>((*it).objType);
      }
      std::cout << '\n';
    }
  }//printStack
  
  void CLIDriver::resetDriver()
  {
    pStack.clear();
    pBlockStackPC.clear();
    pLexerError = false;
    pParserError = false;
    pErrMsg.clear();
  }//resetDriver
  
  void CLIDriver::parse(std::istream& aISS)
  {
    if (!aISS.good() && aISS.eof()) return;
    
    // Clean internal data structures before parsing
    resetDriver();
    
    // Run parser
    parseHelper(aISS);
  }//parse
  
  void CLIDriver::parseHelper(std::istream &aStream)
  {
    try
    {
      pScanner.reset(new CLI::CLIScanner(&aStream, pInStreamIsLine));
    }
    catch (std::bad_alloc &ba)
    {
      std::cerr << "Failed to allocate scanner: (" <<
      ba.what() << "), exiting\n";
      exit(EXIT_FAILURE);
    }
    
    try
    {
      pParser.reset(new CLI::CLIParser((*(pScanner.get())) /* scanner */,
                                       (*this)              /* driver */));
    }
    catch (std::bad_alloc &ba)
    {
      std::cerr << "Failed to allocate parser: (" <<
      ba.what() << "), exiting\n";
      exit(EXIT_FAILURE);
    }
    
    try
    {
      pParser->parse();
    }
    catch (...)
    {
      std::cerr << "Failed to parse, exiting\n";
      exit(EXIT_FAILURE);
    }
  }
  
  void CLIDriver::setLexerError(const std::string& aStr)
  {
    pLexerError = true;
    error("Unrecognized symbol: " + aStr);
  }//setLexerError
  
  void CLIDriver::setParserError(const std::string& aStr, const CLIParser::location_type& aLoc)
  {
    pParserError = true;
    error(aStr, aLoc);
  }//setParserError
  
  void CLIDriver::error(const std::string& aStr, const CLIParser::location_type& aLoc)
  {
    std::stringstream ss;
    ss << aStr << " at " << aLoc;
    pErrMsg = ss.str();
  }//error
  
  void CLIDriver::error(const std::string& aStr)
  {
    pErrMsg = aStr;
  }//error
  
}// end namespace CLI
