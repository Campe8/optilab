//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 01/30/2018
//

#pragma once

#include "CLIParserExportDefs.hpp"

#include "CLIScanner.hpp"
#include "CLICoreObj.hpp"
#include "cli_parser.tab.hh"

#include <istream>
#include <string>
#include <vector>
#include <cstddef>
#include <memory>
#include <cassert>

namespace CLI {
  
  class CLI_PARSER_EXPORT_CLASS CLIDriver {
  public:
    class ConstType {
    public:
      ConstType(CLI_P_INT aVal)
      : pInt(aVal)
      , pType(0) {}
      
      ConstType(const CLI_P_STR& aVal)
      : pString(aVal)
      , pType(1) {}
      
      /// Returns the type of the constant as:
      /// 0 - integer
      /// 1 - string
      inline int getType() const { return pType; }
      
      /// Returns internal values
      inline CLI_P_INT getInt() const { return pInt; }
      inline CLI_P_STR getString() const { return pString; }
      
    private:
      int pType;
      CLI_P_INT pInt;
      CLI_P_STR pString;
    };
    
    using byte_T = unsigned char;
    using ByteArray = std::vector<byte_T>;
    using NameArray = std::vector<CLI_P_STR>;
    using ConstList = std::vector<ConstType>;
    using ConstArray = std::vector<ConstList>;
    
    struct CLI_PARSER_EXPORT_STRUCT ByteCode {
      /// Global names
      NameArray global_array;
      /// Local names
      NameArray local_array;
      /// Constants: array of tuples
      /// every tuple as usually size one
      /// but for some cases, e.g., lists,
      /// it can have size greater than one
      ConstArray const_array;
      /// Byte code
      ByteArray byte_code;
    };
    
  public:
    CLIDriver();
    
    virtual ~CLIDriver() = default;
    
    /// parse - parse from a c++ input stream
    /// @param is - std::istream&, valid input stream
    void parse(std::istream& aISS);
    
    /// Sets the internal stream source flag:
    /// true: the input stream to parse is from a line
    /// false: the input stream to parse is from a file
    inline void setInputStreamLine(bool aInStreamIsLine) {
      pInStreamIsLine = aInStreamIsLine;
    };
    
    /// Returns the byte code representation of the internal stack
    /// after parsing the input stream.
    /// It takes a (possibly empty) bytecode as current environment
    ByteCode byteCode(const ByteCode& aEnvironment = ByteCode());
    
    /// Returns true if the driver encountered an error during parsing. false otherwise
    inline bool hasError() const { return pLexerError || pParserError; }
    
    /// Returns an error message if any (check with "hasError()")
    inline std::string getErrorMsg() const { return pErrMsg; }
    
    /// Prints the internal stack on std out
    void printStack();
    
  protected:
    friend class CLIParser;
    
    /// Prints "str" on standard output
    void debug(const std::string& str);
    
    /// Adds unary operators (false: minus, true: plus) on the stack
    void addUnaryOperators(const std::vector<bool>& aOperators);
    
    /// Adds a scalar object type
    inline void addCoreExpr(CLI_P_INT aObj) { stack().push_back(CLICoreObj(aObj)); }
    inline void addCoreExpr(CLI_P_BOOL aObj) { stack().push_back(CLICoreObj(aObj)); }
    inline void addCoreExpr(const CLI_P_STR& aObj) { stack().push_back(CLICoreObj(aObj)); }
    
    /// Adds a list object type where "aSize" is the number of elements in the stack
    /// that are part of the list axnd "aType" is the type of list
    inline void addList(int aSize, CLICoreObj::ObjType aType)
    {
      stack().push_back(CLICoreObj(aSize, aType));
    }
    
    /// Adds the bytecode to load the property "aProp" (as a list of prop. names) from
    /// the object "aObj"
    void addLoadProperty(const std::string& aObj, const std::vector<std::string>& aProp);
    
    /// Adds the bytecode to store the property "aProp" (as a list of prop. names) on
    /// the object "aObj"
    void addStoreProperty(const std::string& aObj, const std::vector<std::string>& aProp);
    
    /// Adds an assignment expression on the stack provided the lhs "aID",
    /// and the rhs expression with its size
    void addAssignExpr(const std::string& aID, CLICoreObj::ObjType aType, int aExprSize,
                       int aMinMaxOpt = 0);
    
    /// Adds an assignment expression for a matrix decision variable on the stack,
    /// provided the lhs "aID", and the rhs expression with its size
    void addAssignVarMatrix(const std::string& aID, CLICoreObj::ObjType aType, int aExprSize,
                            int aMinMaxOpt = 0);
    
    /// Adds a "let" (assignment) expression on the stack provided the "aID",
    /// and the rhs expression
    void addLetExpr(const std::string& aID);
    
    /// Sets expression on the stack.
    /// Reduces elements if possible
    void addExpr(CLICoreObj::ObjType aEOP);
    
    /// Adds a for-loop block object on "aID" iterator on the stack
    void addForLoopBlock();
    
    /// Adds a if block object on "aID" iterator on the stack
    void addIfBlock();
    
    /// Adds an end if block object on the stack
    void addElseBlock();
    
    /// Adds an end if block object on the stack.
    /// This sets the PC at the beginning of the IF block
    /// to jump to the current instruction (aJumpToNextInstruction = false)
    /// or to the next instruction (aJumpToNextInstruction = true)
    void addEndIfBlock(bool aJumpToNextInstruction = false);
    
    /// Adds a for-loop range iterator w.r.t. the 2 values
    /// representing the range bounds on the stack
    void addForLoopRangeIter(const std::string& aID, CLICoreObj::ObjType aIterType);
    
    /// Adds a end block object on the stack
    void addEndBlock();
    
    /// Marks the end of a statement taking actions in byte code generation.
    /// Set the input argument to true if the statement is an empty statement,
    /// false otherwise
    void markEndOfStmt(bool aIsEmpty = false);
    
    /// Sets the top of the stack as an object with a list,
    /// for example in x[1, 2, 3], x is the object with the list [1, 2, 3]
    void setObjectWithList();
    
    /// Sets lexer error
    void setLexerError(const std::string& aStr);
    
    /// Sets parser error
    void setParserError(const std::string& aStr, const CLIParser::location_type& aLoc);
    
    /// Marks the object code by inserting a mark opcode
    inline void markObjectCode() { stack().push_back(CLICoreObj(CLICoreObj::ObjType::OT_MARK)); }
    
  private:
    /// Input stream flag
    bool pInStreamIsLine;
    
    /// Error flags
    bool pLexerError;
    bool pParserError;
    
    /// Error message
    std::string pErrMsg;
    
    /// Parser
    std::unique_ptr<CLI::CLIParser> pParser;
    
    /// Scanner
    std::unique_ptr<CLI::CLIScanner> pScanner;
    
    /// Stack of parsed objects
    std::vector<CLICoreObj> pStack;
    
    /// Stack of program counter for handling blocks with jumps
    std::vector<std::size_t> pBlockStackPC;
    
    /// Utility function: returns current active stack
    inline std::vector<CLICoreObj>& stack() { return pStack; }
    inline const std::vector<CLICoreObj>& stack() const { return pStack; }
    
    /// Resets internal data structures
    void resetDriver();
    
    /// Helper method for parsing the given input stream
    void parseHelper(std::istream &aStream);
    
    /// Error handling.
    void error(const std::string& aStr, const CLIParser::location_type& aLoc);
    void error(const std::string& aStr);
  };
  
  void printByteCode(const CLIDriver::ByteCode& aBC);
  
} /* end namespace CLI */

