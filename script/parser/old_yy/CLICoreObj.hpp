//
// Copyright OptiLab 2018. All rights reserved.
//
// Created by Federico Campeotto on 2/2/2018
//

#pragma once

#include "CLIParserExportDefs.hpp"

#include "CLITypes.hpp"

#include <string>

namespace CLI {
  
  struct CLI_PARSER_EXPORT_STRUCT CLICoreObj {
    enum class ObjType : char {
        OT_INT = 0
      , OT_BOOL
      , OT_STRING
      , OT_ATTR        // Load an attribute name
      , OT_ASSIGN_ATTR // Store an attribue
      , OT_LIST_ARGS
      , OT_LIST_ARRAY
      , OT_LIST_DOMAIN
      , OT_LIST_PROP   // List of object properties
      , OT_UNARY_NEG
      , OT_UNARY_POS
      , OT_PLUS
      , OT_MIN
      , OT_PROD
      , OT_DIV
      , OT_LT
      , OT_LE
      , OT_GT
      , OT_GE
      , OT_EQ
      , OT_NQ
      , OT_AND
      , OT_OR
      , OT_REIF
      , OT_SUBSCR
      , OT_CALL_FCN
      , OT_TUPLE
      , OT_POP_TOP
      , OT_LET
      , OT_RETURN
      , OT_CONTINUE
      , OT_BREAK
      , OT_IF_BLK     // If block
      , OT_ELSE_BLK   // Else block
      , OT_END_IF_BLK // End if block object type
      , OT_RNG_LIST   // Range list expression type
      , OT_ASSIGN     // Assignment expression type
      , OT_SNG        // Singleton expression type
      , OT_RNG        // Range expression type
      , OT_SET        // Set expression type
      , OT_SNG_M      // Singleton matrix expression type
      , OT_RNG_M      // Range matrix expression type
      , OT_SET_M      // Set matrix expression type
      , OT_FOR_LOOP   // For loop object type
      , OT_WHILE_LOOP // While loop object type
      , OT_FOR_LOOP_RANGE_ITER   // For loop range iterator object type
      , OT_END_BLK    // End block object type
      , OT_PRINT_TOP  // Prints the object at the top of the stack
      , OT_MARK
      , OT_OPT_MIN    // Optimization minimize
      , OT_OPT_MAX    // Optimization maximize
      , OT_UNDEF
    };
    
    /// Scalar object
    CLICoreObj() : objType(ObjType::OT_UNDEF){}
    CLICoreObj(ObjType aType) : objType(aType) {}
    CLICoreObj(CLI_P_INT aObj) : objType(ObjType::OT_INT), intObj(aObj) {}
    CLICoreObj(bool aObj) : objType(ObjType::OT_BOOL), intObj(aObj ? CLI_P_TRUE : CLI_P_FALSE) {}
    CLICoreObj(const CLI_P_STR& aObj) : objType(ObjType::OT_STRING), strObj(aObj) {}
    
    /// Composite object
    CLICoreObj(int aSize, ObjType aType) : size(aSize), objType(aType) {}
    
    ObjType objType;
    CLI_P_INT intObj;
    CLI_P_STR strObj;
    int size = -1;
    bool objWithList = false;
    ObjType objOptimization = ObjType::OT_UNDEF;
  };
  
}// end namespace CLI
