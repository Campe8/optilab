%{ /*** C/C++ Declarations ***/

#include <string>
#include <vector>
#include <utility>

%}

%skeleton "lalr1.cc"
%require  "3.0"
%debug
%defines
%define api.namespace {CLI}
%define parser_class_name {CLIParser}

%code requires{

#include "CLICoreObj.hpp"
#include "OPCode.hpp"

namespace CLI {
class CLIDriver;
class CLIScanner;

//windows compatibility
#undef TRUE
#undef FALSE
#undef IN
}

// The following definitions is missing when %locations isn't used
# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

}

%parse-param { CLIScanner& scanner  }
%parse-param { CLIDriver&  driver   }

%code{
#include <iostream>
#include <cstdlib>
#include <fstream>

/* include for all driver functions */
#include "CLIDriver.hpp"

#undef yylex
#define yylex scanner.yylex
}

%define api.value.type variant
%define parse.assert

%token          ENDF    0  "end of file"
%token <CLI_P_INT>    INUMBER    "CLIInt"
%token <CLI_P_DOUBLE>  DNUMBER    "CLIDouble"
%token          TRUE    "CLITrue"
%token          FALSE    "CLIFalse"
%token <CLI_P_STR>    IDENTIFIER  "CLIID"
%token          NEWLINE
%token          QUOTE
%token          COMMA    ","
%token          SEMICOLON  ";"
%token          LT      "<"
%token          LE      "<="
%token          GT      ">"
%token          GE      ">="
%token          AND      "&&"
%token          OR      "||"
%token          REIF    "<=>"
%token          EQ      "=="
%token          NQ      "!="
%token          PLUS    "+"
%token          MINUS    "-"
%token          PROD    "*"
%token          DIV      "/"
%token          LEFTPAR    "("
%token          RIGHTPAR  ")"
%token          LEFTGPAR  "{"
%token          RIGHTGPAR  "}"
%token          LEFTQPAR  "["
%token          RIGHTQPAR  "]"
%token          COLON    ":"
%token          ASSIGN    "="
%token          DOT      "."
%token          RANGE    ".."
%token          LET      "let"
%token          IF       "if"
%token          ELSEIF   "elseif"
%token          ELSE     "else"
%token          FOR      "for"
%token          WHILE    "while"
%token          IN       "in"
%token          BREAK    "break"
%token          CONTINUE "continue"
%token          RETURN   "return"
%token          END      "end"
%token <CLI_P_STR>    STRING  "CLIString"
%token          SYMBOL
%token          MINIMIZE  "minimize"
%token          MAXIMIZE  "maximize"
%token          START_LINE
%token          START_FILE
%token <CLI_P_STR>    LEXERROR

%type <CLI_P_INT> signed_number expr_list arg_expr subscript_expr dom_expr minmaxopt if_body
%type <std::pair<CLI_P_STR, std::vector<bool>>> signed_id
%type <CLI_P_STR> lhs_expr
%type <std::vector<CLI_P_STR>> dot_prop
%type <std::pair<CLI_P_STR, std::vector<CLI_P_STR>>> prop_expr
%type <std::pair<CLICoreObj::ObjType, int>> rhs_expr rhs_dom_expr
%type <CLICoreObj::ObjType> for_loop_iter
%type <bool> block block_list

%start entry_point

%locations

%%

entry_point
  : START_LINE input_line
  | START_FILE input_file
  ;

input_line
  : single_input
  | LEXERROR { driver.setLexerError($1); }
  ;

input_file
  : file_input
  | LEXERROR { driver.setLexerError($1); }
  ;

single_input
  : line_input
  ;

file_input
  : multi_line_input ENDF
  ;

line_input
  : NEWLINE
  | simple_stmt_list_marked
  | block_stmt NEWLINE
  ;

multi_line_input
  : NEWLINE
  | stmt
  | multi_line_input stmt
  | multi_line_input NEWLINE
  ;

stmt
  : simple_stmt_list_marked
  | block_stmt
  ;

simple_stmt_list_marked
  : simple_stmt_list NEWLINE
  | simple_stmt_list ";" NEWLINE
  ;

simple_stmt_list
  : simple_stmt { driver.markEndOfStmt(); }
  | simple_stmt_list ";" { driver.markEndOfStmt(); } simple_stmt { driver.markEndOfStmt(); }
  ;

delim_stmt
  :
  | delimiter
  ;

delimiter
  : null_lines
  | empty_lines
  | null_lines empty_lines
  ;

null_lines
  : null_line
  | null_lines null_line
  ;

null_line
  : ";"
  | empty_lines ";"
  ;

empty_lines
  : NEWLINE
  | empty_lines NEWLINE
  ;

simple_stmt
  : expr_stmt
  | flow_stmt
  ;

block_stmt
  : for_stmt
  | if_stmt
  ;

for_stmt
  : "for" "CLIID" "in"
    { driver.addForLoopBlock(); }
  for_loop_iter ":"
    { driver.addForLoopRangeIter($2, $5); /* Give the name of the iterator and its type */ }
  block
    { driver.markEndOfStmt($8); }
  "end"
    { driver.addEndBlock(); }
  ;

for_loop_iter
  : range_expr  { $$ = CLICoreObj::ObjType::OT_RNG_LIST;   }
  | subscr_list { $$ = CLICoreObj::ObjType::OT_LIST_ARRAY; }
  ;

if_stmt
  : "if" expr ":"
    { driver.addIfBlock(); }
  if_body
    {
      /* Close all the elseif cases here */
      for(int numElseIf = 0; numElseIf < $5; ++numElseIf)
      {
        driver.addEndIfBlock();
      }
    }
  "end"
  ;

if_body
  : block
    {
      /* Mark the end of if stmt */
      driver.markEndOfStmt($1);
      /* Set the PC to jump to here from wherever it was */
      driver.addEndIfBlock();
      $$ = 0;
    }
  | block
    { driver.markEndOfStmt($1);
      /* Set the PC to jump to here on prev if stmt (if not true jump here) */
      driver.addEndIfBlock(true);
      /* Add a jump forward (if PC is at this point, jump forward to end)
      before evaluating the following block */
      driver.addElseBlock();
    }
  "elseif" expr ":"
    {
      /* Add an if condition to jump to next elseif/else if not true */
      driver.addIfBlock();
    }
  if_body
    { $$ = 1 + $7; }
  | block
    {
      /* Mark the end of if stmt */
      driver.markEndOfStmt($1);
      /* Set the PC to jump to here from wherever it was */
      driver.addEndIfBlock(true);
    }
  "else" ":"
    {
      /* Add a jump forward (if PC is at this point, jump forward to end),
      before evaluating the following block */
      driver.addElseBlock();
    }
  block
    {
      /* Mark the end of if stmt */
      driver.markEndOfStmt($6);
      /* Set the PC to jump to here from wherever it was */
      driver.addEndIfBlock();
      $$ = 0;
    }
  ;

block
  : delim_stmt            {  $$ = true; /* Empty block */ }
  | delim_stmt block_list {  $$ = $2; }
  ;

block_list
  : simple_stmt delimiter { driver.markEndOfStmt(); $$ = false; /* Simple stmt: not empty*/ }
  | block_stmt delimiter  { $$ = false; }
  | simple_stmt delimiter { driver.markEndOfStmt(); } block_list { driver.markEndOfStmt(); $$ = false; }
  | block_stmt delimiter block_list { driver.markEndOfStmt(); $$ = false; }
  ;

expr_stmt
  : expr
  | prop_expr {/* Load the list of properties ($2) for the object ($1) */
                  driver.addLoadProperty($1.first, $1.second);}
  | assign_expr
  | let_expr
  ;

prop_expr
  : "CLIID" dot_prop {  driver.markObjectCode();
                        std::swap($$.first, $1); std::swap($$.second, $2);
                      }
  ;

dot_prop
  : "." "CLIID"           { $$.push_back($2);          }
  | dot_prop "." "CLIID"  { $1.push_back($3); $$ = $1; }
  ;

assign_expr
  : lhs_expr ":" rhs_dom_expr minmaxopt { driver.addAssignExpr($1, $3.first, $3.second, $4);
                                          /* Decision variable declaration */ }
  | lhs_expr ":" rhs_dom_expr ":" subscript_expr minmaxopt  { driver.addList($5, CLICoreObj::ObjType::OT_LIST_ARRAY);
                                                              driver.addAssignVarMatrix($1, $3.first, $3.second, $6);
                                                              /* Decision variable declaration */ }
  | lhs_expr ":" rhs_expr { driver.addAssignExpr($1, $3.first, $3.second);
                            /* Variable assignment */ }
  | prop_expr ":" rhs_expr { driver.addStoreProperty($1.first, $1.second);
                             /* Store attribute/property */}
  ;

minmaxopt
  :                 { $$ = 0; /* No optimization */ }
  | ":" "minimize"  { $$ = 1; /* Minimize */ }
  | ":" "maximize"  { $$ = 2; /* Maximize */ }
  ;

let_expr
  : "let" lhs_expr ":" expr { driver.addLetExpr($2); }
  ;

lhs_expr
  : "CLIID" { driver.markObjectCode(); std::swap($$, $1); }
  ;

rhs_expr
  : expr { /* 1 element */  $$ = {CLICoreObj::ObjType::OT_ASSIGN,  1};  }
  | prop_expr { /* Load the list of properties ($2) for the object ($1) */
                driver.addLoadProperty($1.first, $1.second);
                /* 1 element */
                $$ = {CLICoreObj::ObjType::OT_ASSIGN,  1}; }
  ;

rhs_dom_expr
  : range_expr  { /* $1 elements */  $$ = {CLICoreObj::ObjType::OT_RNG,   2};
                /*  Range expression pushed a range list on the stack */ }
  | dom_expr    { /* $1 elements */  $$ = {CLICoreObj::ObjType::OT_SET,  $1}; }
  ;

flow_stmt
  : "break"     { driver.addExpr(CLICoreObj::ObjType::OT_BREAK);    }
  | "continue"  { driver.addExpr(CLICoreObj::ObjType::OT_CONTINUE); }
  | "return"    { driver.addExpr(CLICoreObj::ObjType::OT_RETURN);   }
  ;

%left "<=>";
%left "||";
%left "&&";
%left "==" "!=";
%left "<" "<=" ">" ">=";
%left "*" "/";
%left "+" "-";
expr
  : atom_expr
  | expr "+" expr   { driver.addExpr(CLICoreObj::ObjType::OT_PLUS); }
  | expr "-" expr   { driver.addExpr(CLICoreObj::ObjType::OT_MIN);  }
  | expr "*" expr   { driver.addExpr(CLICoreObj::ObjType::OT_PROD); }
  | expr "/" expr   { driver.addExpr(CLICoreObj::ObjType::OT_DIV);  }
  | expr "<" expr   { driver.addExpr(CLICoreObj::ObjType::OT_LT);   }
  | expr "<=" expr  { driver.addExpr(CLICoreObj::ObjType::OT_LE);   }
  | expr ">" expr   { driver.addExpr(CLICoreObj::ObjType::OT_GT);   }
  | expr ">=" expr  { driver.addExpr(CLICoreObj::ObjType::OT_GE);   }
  | expr "==" expr  { driver.addExpr(CLICoreObj::ObjType::OT_EQ);   }
  | expr "!=" expr  { driver.addExpr(CLICoreObj::ObjType::OT_NQ);   }
  | expr "&&" expr  { driver.addExpr(CLICoreObj::ObjType::OT_AND);  }
  | expr "||" expr  { driver.addExpr(CLICoreObj::ObjType::OT_OR);   }
  | expr "<=>" expr { driver.addExpr(CLICoreObj::ObjType::OT_REIF); }
  ;

atom_expr
  : core_expr
  | sequence_expr
  | core_expr subscr_list { driver.setObjectWithList(); }
  | call_name args_list   { driver.setObjectWithList(); }
  ;

range_expr
  : ".."       { driver.addList(DOM_BOUNDS_OPEN, CLICoreObj::ObjType::OT_RNG_LIST); }
  | ".." expr  { driver.addList(DOM_BOUNDS_OPEN_LEFT, CLICoreObj::ObjType::OT_RNG_LIST);  /* positive one encodes bounded right */ }
  | expr ".."  { driver.addList(DOM_BOUNDS_OPEN_RIGHT, CLICoreObj::ObjType::OT_RNG_LIST); /* negative one encodes bounded left  */ }
  | expr ".." expr { driver.addList(2, CLICoreObj::ObjType::OT_RNG_LIST); }
  ;

sequence_expr
  : args_list
  | subscr_list
  ;

args_list
  : arg_expr { driver.addList($1, CLICoreObj::ObjType::OT_LIST_ARGS); }
  ;

subscr_list
  : subscript_expr { driver.addList($1, CLICoreObj::ObjType::OT_LIST_ARRAY); }
  ;

call_name
  : "CLIID"       { driver.markObjectCode(); driver.addCoreExpr($1); }
  | "CLIString"   { driver.markObjectCode(); driver.addCoreExpr($1); }
  ;

core_expr
  : signed_number { driver.addCoreExpr($1);    }
  | signed_id     { driver.addCoreExpr($1.first); driver.addUnaryOperators($1.second); }
  | "CLIString"   { driver.addCoreExpr($1);    }
  | "CLITrue"     { driver.addCoreExpr(true);  }
  | "CLIFalse"    { driver.addCoreExpr(false); }
  ;

signed_number
  : INUMBER           { std::swap($$, $1); }
  | "-" signed_number {  $$ = (-1 * $2);   }
  | "+" signed_number { std::swap($$, $2); }
  ;

signed_id
  : "CLIID"       { std::swap($$.first, $1);                                                          }
  | "-" signed_id { std::swap($$.first, $2.first); $$.second = $2.second; $$.second.push_back(false); }
  | "+" signed_id { std::swap($$.first, $2.first); $$.second = $2.second; $$.second.push_back(true);  }
  ;

arg_expr
  : "(" ")"             { /* No arguments */ $$ = 0; }
  | "(" expr_list ")"   { /* |list| number of arguments */ $$ = $2; }
  ;

subscript_expr
  : "[" "]"            { /* No arguments */ $$ = 0; }
  | "[" expr_list "]"  { $$ = $2; }
  ;

dom_expr
  : "{" expr_list "}"  { $$ = $2; }
  ;

expr_list
  : expr                            { /* One element in the list */ $$ = 1; }
  | expr_list "," expr              { /* Recusively add 1 element in the list */ $$ = $1 + 1; }
  | expr_list empty_lines "," expr  { /* Recusively add 1 element in the list */ $$ = $1 + 1; }
  | expr_list "," empty_lines expr  { /* Recusively add 1 element in the list */ $$ = $1 + 1; }
  | expr_list empty_lines "," empty_lines expr  { /* Recusively add 1 element in the list */ $$ = $1 + 1; }
  ;

%%

void
CLI::CLIParser::error( const location_type &l, const std::string &err_message )
{
  driver.setParserError(err_message, l);
}
