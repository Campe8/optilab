ECHO OFF
call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat"
win_bison -d -v cli_parser.yy
cl /EHsc -I. -c cli_parser.tab.cc
win_flex --outfile=cli_lexer.yy.cc cli_lexer.ll
cl /EHsc -I. -c cli_lexer.yy.cc
cl /EHsc -I. -c CLIDriver.cpp
lib -nologo -out:liboptilab_parser.lib CLIDriver.obj cli_lexer.yy.obj cli_parser.tab.obj
rm CLIDriver.obj
rm cli_lexer.yy.obj
rm cli_parser.tab.obj
cl -I. main.cpp liboptilab_parser.lib
